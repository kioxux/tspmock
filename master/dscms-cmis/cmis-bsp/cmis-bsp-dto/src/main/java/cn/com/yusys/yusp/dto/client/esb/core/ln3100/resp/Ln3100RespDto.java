package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：根据借据号或者贷款账号查询单笔借据的详细信息，主要包括借据信息、定价信息、还款信息、放款信息等
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3100RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//贷款账号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "zhwujgmc")
    private String zhwujgmc;//账务机构名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "duowtrbz")
    private String duowtrbz;//多委托人标志
    @JsonProperty(value = "dlhesfsh")
    private String dlhesfsh;//代理核算方式
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "kuaijilb")
    private String kuaijilb;//会计类别
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限(月)
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "daikxtai")
    private String daikxtai;//贷款形态
    @JsonProperty(value = "yjfyjzht")
    private String yjfyjzht;//应计非应计状态
    @JsonProperty(value = "dbdkkksx")
    private Integer dbdkkksx;//多笔贷款扣款顺序
    @JsonProperty(value = "dkzhhzht")
    private String dkzhhzht;//贷款账户状态
    @JsonProperty(value = "zhcwjyrq")
    private String zhcwjyrq;//最后财务交易日
    @JsonProperty(value = "mingxixh")
    private Integer mingxixh;//明细序号
    @JsonProperty(value = "kaihujig")
    private String kaihujig;//开户机构
    @JsonProperty(value = "kaihguiy")
    private String kaihguiy;//开户柜员
    @JsonProperty(value = "xiaohurq")
    private String xiaohurq;//销户日期
    @JsonProperty(value = "xiaohugy")
    private String xiaohugy;//销户柜员
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "fafangje")
    private BigDecimal fafangje;//已发放金额
    @JsonProperty(value = "djiekfje")
    private BigDecimal djiekfje;//冻结可放金额
    @JsonProperty(value = "kffangje")
    private BigDecimal kffangje;//可发放金额
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "dkuanjij")
    private BigDecimal dkuanjij;//贷款基金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "yingjitx")
    private BigDecimal yingjitx;//应计贴息
    @JsonProperty(value = "yingshtx")
    private BigDecimal yingshtx;//应收贴息
    @JsonProperty(value = "tiexfuli")
    private BigDecimal tiexfuli;//贴息复利
    @JsonProperty(value = "daitanlx")
    private BigDecimal daitanlx;//待摊利息
    @JsonProperty(value = "hexiaobj")
    private BigDecimal hexiaobj;//核销本金
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "lixishru")
    private BigDecimal lixishru;//利息收入
    @JsonProperty(value = "yingshfy")
    private BigDecimal yingshfy;//应收费用
    @JsonProperty(value = "feiyshru")
    private BigDecimal feiyshru;//费用收入
    @JsonProperty(value = "yingshfj")
    private BigDecimal yingshfj;//应收罚金
    @JsonProperty(value = "fjinshru")
    private BigDecimal fjinshru;//罚金收入
    @JsonProperty(value = "zhunbeij")
    private BigDecimal zhunbeij;//准备金
    @JsonProperty(value = "daikduix")
    private String daikduix;//贷款对象
    @JsonProperty(value = "yewufenl")
    private String yewufenl;//业务分类
    @JsonProperty(value = "ysywbhao")
    private String ysywbhao;//衍生业务编号
    @JsonProperty(value = "ysywleix")
    private String ysywleix;//衍生业务类型
    @JsonProperty(value = "yjfyjhes")
    private String yjfyjhes;//按应计非应计核算
    @JsonProperty(value = "yiyldhes")
    private String yiyldhes;//按一逾两呆核算
    @JsonProperty(value = "dkxtkmhs")
    private String dkxtkmhs;//形态分科目核算
    @JsonProperty(value = "zhqifkbz")
    private String zhqifkbz;//周期性放款标志
    @JsonProperty(value = "fkzhouqi")
    private String fkzhouqi;//放款周期
    @JsonProperty(value = "fkfangsh")
    private String fkfangsh;//放款金额方式
    @JsonProperty(value = "bencfkje")
    private BigDecimal bencfkje;//本次放款金额
    @JsonProperty(value = "mcfkjebl")
    private BigDecimal mcfkjebl;//每次放款金额或比例
    @JsonProperty(value = "scfkriqi")
    private String scfkriqi;//上次放款日期
    @JsonProperty(value = "jxhjhkkz")
    private String jxhjhkkz;//借新还旧还款控制
    @JsonProperty(value = "jxhjdkkz")
    private String jxhjdkkz;//借新还旧控制
    @JsonProperty(value = "hjiuleix")
    private String hjiuleix;//还旧类型
    @JsonProperty(value = "dzhifkjh")
    private String dzhifkjh;//定制放款计划
    @JsonProperty(value = "fkzjclfs")
    private String fkzjclfs;//放款资金处理方式
    @JsonProperty(value = "jixiguiz")
    private String jixiguiz;//计息规则
    @JsonProperty(value = "ltjixigz")
    private String ltjixigz;//零头计息规则
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "whlilvzl")
    private String whlilvzl;//外汇利率种类
    @JsonProperty(value = "whllqxzl")
    private String whllqxzl;//外汇利率期限种类
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "zclilvbh")
    private String zclilvbh;//正常利率编号
    @JsonProperty(value = "zaoqixbz")
    private String zaoqixbz;//早起息标志
    @JsonProperty(value = "wanqixbz")
    private String wanqixbz;//晚起息标志
    @JsonProperty(value = "qixitshu")
    private Integer qixitshu;//起息天数
    @JsonProperty(value = "yushxfsh")
    private String yushxfsh;//预收息方式
    @JsonProperty(value = "yushxize")
    private BigDecimal yushxize;//预收息总额
    @JsonProperty(value = "lixitxzq")
    private String lixitxzq;//利息摊销周期
    @JsonProperty(value = "meictxfs")
    private String meictxfs;//每次摊销方式
    @JsonProperty(value = "meictxbl")
    private BigDecimal meictxbl;//每次摊销比例
    @JsonProperty(value = "xiacitxr")
    private String xiacitxr;//下次摊销日
    @JsonProperty(value = "kxqjjrgz")
    private String kxqjjrgz;//宽限期节假日规则
    @JsonProperty(value = "kxqzyqgz")
    private String kxqzyqgz;//宽限期转逾期规则
    @JsonProperty(value = "kxqshxgz")
    private String kxqshxgz;//宽限期收息规则
    @JsonProperty(value = "jixibzhi")
    private String jixibzhi;//计息标志
    @JsonProperty(value = "jfxibzhi")
    private String jfxibzhi;//计复息标志
    @JsonProperty(value = "fxjxbzhi")
    private String fxjxbzhi;//复息计息标志
    @JsonProperty(value = "yuqillbh")
    private String yuqillbh;//逾期利率编号
    @JsonProperty(value = "yqfxfdzh")
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    @JsonProperty(value = "yqfxfdfs")
    private String yqfxfdfs;//逾期罚息浮动方式
    @JsonProperty(value = "yuqinyll")
    private String yuqinyll;//逾期年月利率
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "sctxriqi")
    private String sctxriqi;//上次调息日
    @JsonProperty(value = "xctxriqi")
    private String xctxriqi;//下次调息日
    @JsonProperty(value = "scyqtxrq")
    private String scyqtxrq;//上次逾期调息日
    @JsonProperty(value = "xcyqtxrq")
    private String xcyqtxrq;//下次逾期调息日
    @JsonProperty(value = "scfxtxrq")
    private String scfxtxrq;//上次复利调息日
    @JsonProperty(value = "xcfxtxrq")
    private String xcfxtxrq;//下次复利调息日
    @JsonProperty(value = "scnytxrq")
    private String scnytxrq;//上次挪用调息日
    @JsonProperty(value = "xcnytxrq")
    private String xcnytxrq;//下次挪用调息日
    @JsonProperty(value = "scjixirq")
    private String scjixirq;//上次计息日
    @JsonProperty(value = "jiximxxh")
    private Integer jiximxxh;//计息明细序号
    @JsonProperty(value = "lilvfend")
    private String lilvfend;//利率分段
    @JsonProperty(value = "tiexibzh")
    private String tiexibzh;//贴息标志
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "leijinzh")
    private BigDecimal leijinzh;//累进值
    @JsonProperty(value = "leijqjsh")
    private Integer leijqjsh;//累进区间期数
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "hezuofbh")
    private String hezuofbh;//合作方编号
    @JsonProperty(value = "hezuofmc")
    private String hezuofmc;//合作方名称
    @JsonProperty(value = "duophkbz")
    private String duophkbz;//多频率还款标志
    @JsonProperty(value = "huanbzhq")
    private String huanbzhq;//还本周期
    @JsonProperty(value = "shunylix")
    private BigDecimal shunylix;//顺延利息
    @JsonProperty(value = "yuqhkzhq")
    private String yuqhkzhq;//逾期还款周期
    @JsonProperty(value = "schkriqi")
    private String schkriqi;//上次还款日
    @JsonProperty(value = "xchkriqi")
    private String xchkriqi;//下次还款日
    @JsonProperty(value = "hkqixian")
    private String hkqixian;//还款期限(月)
    @JsonProperty(value = "baoliuje")
    private BigDecimal baoliuje;//保留金额
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额
    @JsonProperty(value = "zongqish")
    private Integer zongqish;//总期数
    @JsonProperty(value = "benqqish")
    private Integer benqqish;//本期期数
    @JsonProperty(value = "benqizqs")
    private Integer benqizqs;//本期子期数
    @JsonProperty(value = "jixizqsh")
    private Integer jixizqsh;//计息总期数
    @JsonProperty(value = "jixiqish")
    private Integer jixiqish;//计息期数
    @JsonProperty(value = "jixibenj")
    private BigDecimal jixibenj;//计息本金
    @JsonProperty(value = "danqbjds")
    private Integer danqbjds;//当前本金段数
    @JsonProperty(value = "hkshxubh")
    private String hkshxubh;//还款顺序编号
    @JsonProperty(value = "yunxtqhk")
    private String yunxtqhk;//允许提前还款
    @JsonProperty(value = "qxbgtzjh")
    private String qxbgtzjh;//期限变更调整计划
    @JsonProperty(value = "llbgtzjh")
    private String llbgtzjh;//利率变更调整计划
    @JsonProperty(value = "dcfktzjh")
    private String dcfktzjh;//多次放款调整计划
    @JsonProperty(value = "tqhktzjh")
    private String tqhktzjh;//提前还款调整计划
    @JsonProperty(value = "zdkoukbz")
    private String zdkoukbz;//自动扣款标志
    @JsonProperty(value = "zdplkkbz")
    private String zdplkkbz;//指定文件批量扣款标识
    @JsonProperty(value = "zdjqdkbz")
    private String zdjqdkbz;//自动结清贷款标志
    @JsonProperty(value = "qyxhdkbz")
    private String qyxhdkbz;//签约循环贷款标志
    @JsonProperty(value = "xhdkqyzh")
    private String xhdkqyzh;//签约账号
    @JsonProperty(value = "dhkzhhbz")
    private String dhkzhhbz;//多还款账户标志
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "dzhhkjih")
    private String dzhhkjih;//定制还款计划
    @JsonProperty(value = "yunxdkzq")
    private String yunxdkzq;//允许贷款展期
    @JsonProperty(value = "zhqxzekk")
    private String zhqxzekk;//展期需足额扣款
    @JsonProperty(value = "zhqzdcsh")
    private Integer zhqzdcsh;//展期最大次数
    @JsonProperty(value = "zqgzbhao")
    private String zqgzbhao;//展期规则编号
    @JsonProperty(value = "yizhqcsh")
    private Integer yizhqcsh;//已展期次数
    @JsonProperty(value = "zhanqixh")
    private Integer zhanqixh;//展期序号
    @JsonProperty(value = "zbjrsygz")
    private String zbjrsygz;//贷款到期假日规则
    @JsonProperty(value = "zidxtzhy")
    private String zidxtzhy;//自动形态转移
    @JsonProperty(value = "lixizcgz")
    private String lixizcgz;//利息转出规则
    @JsonProperty(value = "lixizhgz")
    private String lixizhgz;//利息转回规则
    @JsonProperty(value = "tqhkfjbh")
    private String tqhkfjbh;//提前还款罚金编号
    @JsonProperty(value = "tqhkfjmc")
    private String tqhkfjmc;//提前还款罚金名称
    @JsonProperty(value = "tqhkfjfj")
    private BigDecimal tqhkfjfj;//提前还款附加罚金金额
    @JsonProperty(value = "dlxxzdgz")
    private String dlxxzdgz;//代理信息指定规则
    @JsonProperty(value = "dlxxqzgz")
    private String dlxxqzgz;//代理信息取值规则
    @JsonProperty(value = "wtckywbm")
    private String wtckywbm;//委托存款业务编码
    @JsonProperty(value = "dailixuh")
    private Integer dailixuh;//代理序号
    @JsonProperty(value = "dailimsh")
    private String dailimsh;//代理描述
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号
    @JsonProperty(value = "bjghrzxh")
    private String bjghrzxh;//本金归还入账账号子序号
    @JsonProperty(value = "lxghrzxh")
    private String lxghrzxh;//利息归还入账账号子序号
    @JsonProperty(value = "shynedbz")
    private String shynedbz;//使用额度标志
    @JsonProperty(value = "edbizhgz")
    private String edbizhgz;//额度币种规则
    @JsonProperty(value = "edzdbizh")
    private String edzdbizh;//额度指定币种
    @JsonProperty(value = "edshyngz")
    private String edshyngz;//额度使用规则
    @JsonProperty(value = "sydkcnuo")
    private String sydkcnuo;//使用贷款承诺
    @JsonProperty(value = "cndkjjho")
    private String cndkjjho;//承诺贷款借据号
    @JsonProperty(value = "zhjzhyfs")
    private String zhjzhyfs;//质押方式
    @JsonProperty(value = "dkgljgou")
    private String dkgljgou;//贷款管理机构
    @JsonProperty(value = "gljgleib")
    private String gljgleib;//管理机构类别
    @JsonProperty(value = "fuhejgou")
    private String fuhejgou;//复核机构
    @JsonProperty(value = "wujiflbz")
    private String wujiflbz;//五级分类标志
    @JsonProperty(value = "wujiflrq")
    private String wujiflrq;//五级分类日期
    @JsonProperty(value = "lmdkbzhi")
    private String lmdkbzhi;//联名贷款标志
    @JsonProperty(value = "leijfkje")
    private BigDecimal leijfkje;//累计放款金额
    @JsonProperty(value = "snljfkje")
    private BigDecimal snljfkje;//上年累计放款金额
    @JsonProperty(value = "bnljfkje")
    private BigDecimal bnljfkje;//本年累计放款金额
    @JsonProperty(value = "zuigdkye")
    private BigDecimal zuigdkye;//最高贷款余额
    @JsonProperty(value = "snzgzhye")
    private BigDecimal snzgzhye;//上年最高账户余额
    @JsonProperty(value = "bnzgzhye")
    private BigDecimal bnzgzhye;//本年最高账户余额
    @JsonProperty(value = "leijyhbj")
    private BigDecimal leijyhbj;//累计已还本金
    @JsonProperty(value = "snljyhbj")
    private BigDecimal snljyhbj;//上年累计已还本金
    @JsonProperty(value = "bnljyhbj")
    private BigDecimal bnljyhbj;//本年累计已还本金
    @JsonProperty(value = "yishhxbj")
    private BigDecimal yishhxbj;//已收回核销本金
    @JsonProperty(value = "yishhxlx")
    private BigDecimal yishhxlx;//已收回核销利息
    @JsonProperty(value = "leijyhlx")
    private BigDecimal leijyhlx;//累计已还利息
    @JsonProperty(value = "snljyhlx")
    private BigDecimal snljyhlx;//上年累计已还利息
    @JsonProperty(value = "bnljyhlx")
    private BigDecimal bnljyhlx;//本年累计已还利息
    @JsonProperty(value = "syljyhlx")
    private BigDecimal syljyhlx;//上月累计已还利息
    @JsonProperty(value = "byljyhlx")
    private BigDecimal byljyhlx;//本月累计已还利息
    @JsonProperty(value = "leijcslx")
    private BigDecimal leijcslx;//累计产生利息
    @JsonProperty(value = "snljcslx")
    private BigDecimal snljcslx;//上年累计产生利息
    @JsonProperty(value = "bnljcslx")
    private BigDecimal bnljcslx;//本年累计产生利息
    @JsonProperty(value = "byljxxsh")
    private BigDecimal byljxxsh;//本月累计产生利息
    @JsonProperty(value = "zhchtqtz")
    private String zhchtqtz;//正常提前通知
    @JsonProperty(value = "tzhtqtsh")
    private Integer tzhtqtsh;//通知提前天数
    @JsonProperty(value = "yqcshtzh")
    private String yqcshtzh;//逾期催收通知
    @JsonProperty(value = "tzhjgtsh")
    private Integer tzhjgtsh;//通知间隔天数
    @JsonProperty(value = "lilvbgtz")
    private String lilvbgtz;//利率变更通知
    @JsonProperty(value = "yuebgtzh")
    private String yuebgtzh;//余额变更通知
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息
    @JsonProperty(value = "bzhrdbbz")
    private String bzhrdbbz;//保证人担保标志
    @JsonProperty(value = "benjheji")
    private BigDecimal benjheji;//本金合计
    @JsonProperty(value = "lixiheji")
    private BigDecimal lixiheji;//利息合计
    @JsonProperty(value = "lilvqixx")
    private String lilvqixx;//利率期限
    @JsonProperty(value = "dkrzhzxh")
    private String dkrzhzxh;//贷款入账账号子序号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "yewusx01")
    private String yewusx01;//业务属性1
    @JsonProperty(value = "yewusx02")
    private String yewusx02;//业务属性2
    @JsonProperty(value = "yewusx03")
    private String yewusx03;//业务属性3
    @JsonProperty(value = "yewusx04")
    private String yewusx04;//业务属性4
    @JsonProperty(value = "yewusx05")
    private String yewusx05;//业务属性5
    @JsonProperty(value = "yewusx06")
    private String yewusx06;//业务属性6
    @JsonProperty(value = "yewusx07")
    private String yewusx07;//业务属性7
    @JsonProperty(value = "yewusx08")
    private String yewusx08;//业务属性8
    @JsonProperty(value = "yewusx09")
    private String yewusx09;//业务属性9
    @JsonProperty(value = "yewusx10")
    private String yewusx10;//业务属性10
    @JsonProperty(value = "hetongll")
    private BigDecimal hetongll;//合同利率
    @JsonProperty(value = "sfpinlvv")
    private String sfpinlvv;//收费频率
    @JsonProperty(value = "sfzhouqi")
    private String sfzhouqi;//收费周期
    @JsonProperty(value = "shoufzhl")
    private String shoufzhl;//收费种类
    @JsonProperty(value = "txzhouqi")
    private String txzhouqi;//摊销周期
    @JsonProperty(value = "shoufdma")
    private String shoufdma;//收费代码
    @JsonProperty(value = "shfdmamc")
    private String shfdmamc;//收费代码名称
    @JsonProperty(value = "shoufjee")
    private BigDecimal shoufjee;//收费金额/比例
    @JsonProperty(value = "fufeizhh")
    private String fufeizhh;//付费账号
    @JsonProperty(value = "ffzhhzxh")
    private String ffzhhzxh;//付费账号子序号
    @JsonProperty(value = "ffzhhmch")
    private String ffzhhmch;//付费账号名称
    @JsonProperty(value = "sfrzhzhh")
    private String sfrzhzhh;//收费入账账号
    @JsonProperty(value = "sfrzhzxh")
    private String sfrzhzxh;//收费入账账号子序号
    @JsonProperty(value = "mqjqsfbz")
    private String mqjqsfbz;//末期已结清是否收费
    @JsonProperty(value = "fybkdqbz")
    private String fybkdqbz;//费用首期是否包括当前期
    @JsonProperty(value = "sfljsfei")
    private String sfljsfei;//是否立即收费
    @JsonProperty(value = "ywsxms01")
    private String ywsxms01;//业务属性描述1
    @JsonProperty(value = "ywsxms02")
    private String ywsxms02;//业务属性描述2
    @JsonProperty(value = "ywsxms03")
    private String ywsxms03;//业务属性描述3
    @JsonProperty(value = "ywsxms04")
    private String ywsxms04;//业务属性描述4
    @JsonProperty(value = "ywsxms05")
    private String ywsxms05;//业务属性描述5
    @JsonProperty(value = "ywsxms06")
    private String ywsxms06;//业务属性描述6
    @JsonProperty(value = "ywsxms07")
    private String ywsxms07;//业务属性描述7
    @JsonProperty(value = "ywsxms08")
    private String ywsxms08;//业务属性描述8
    @JsonProperty(value = "ywsxms09")
    private String ywsxms09;//业务属性描述9
    @JsonProperty(value = "ywsxms10")
    private String ywsxms10;//业务属性描述10
    @JsonProperty(value = "hesuanfs")
    private String hesuanfs;//核算方式
    @JsonProperty(value = "kxqzdcsh")
    private Integer kxqzdcsh;//宽限期最大次数
    @JsonProperty(value = "yxtsfkbz")
    private String yxtsfkbz;//允许特殊放款标志
    @JsonProperty(value = "zdfkjjms")
    private String zdfkjjms;//放款借据管理模式
    @JsonProperty(value = "xhdkzhxh")
    private String xhdkzhxh;//签约账号子序号
    @JsonProperty(value = "shfleixi")
    private String shfleixi;//收费类型
    @JsonProperty(value = "pingzhzl")
    private String pingzhzl;//凭证种类
    @JsonProperty(value = "khjingli")
    private String khjingli;//客户经理
    @JsonProperty(value = "pingzhma")
    private String pingzhma;//凭证批号
    @JsonProperty(value = "yinhshje")
    private BigDecimal yinhshje;//印花税金额
    @JsonProperty(value = "yinhshlv")
    private BigDecimal yinhshlv;//印花税率
    @JsonProperty(value = "chanpzdm")
    private String chanpzdm;//产品组代码
    @JsonProperty(value = "chanpzmc")
    private String chanpzmc;//产品组名称
    @JsonProperty(value = "wtrckuzh")
    private String wtrckuzh;//委托人存款账号
    @JsonProperty(value = "wtrckzxh")
    private String wtrckzxh;//委托人存款账号子序号
    @JsonProperty(value = "wtckzhao")
    private String wtckzhao;//委托存款账号
    @JsonProperty(value = "wtckzixh")
    private String wtckzixh;//委托存款账号子序号
    @JsonProperty(value = "daikdxxf")
    private String daikdxxf;//贷款对象细分
    @JsonProperty(value = "bwchapbz")
    private String bwchapbz;//表外产品
    @JsonProperty(value = "xunhdaik")
    private String xunhdaik;//循环贷款
    @JsonProperty(value = "yansdaik")
    private String yansdaik;//衍生贷款
    @JsonProperty(value = "chendaik")
    private String chendaik;//承诺贷款
    @JsonProperty(value = "butidaik")
    private String butidaik;//补贴贷款
    @JsonProperty(value = "yintdkbz")
    private String yintdkbz;//银团贷款标志
    @JsonProperty(value = "yintdkfs")
    private String yintdkfs;//银团贷款方式
    @JsonProperty(value = "yintleib")
    private String yintleib;//银团类别
    @JsonProperty(value = "yintnbcy")
    private String yintnbcy;//内部银团成员类型
    @JsonProperty(value = "yintwbcy")
    private String yintwbcy;//外部银团成员类型
    @JsonProperty(value = "jiangulx")
    private String jiangulx;//产品适用监管类型
    @JsonProperty(value = "zhqizcqx")
    private String zhqizcqx;//展期最长期限(月)
    @JsonProperty(value = "fangkulx")
    private String fangkulx;//放款类型
    @JsonProperty(value = "hntmifku")
    private String hntmifku;//允许对行内同名账户放款
    @JsonProperty(value = "hnftmfku")
    private String hnftmfku;//允许对行内非同名账户放款
    @JsonProperty(value = "neibufku")
    private String neibufku;//允许对内部账户放款
    @JsonProperty(value = "jixibjgz")
    private String jixibjgz;//计息本金规则
    @JsonProperty(value = "lixijsff")
    private String lixijsff;//利息计算方法
    @JsonProperty(value = "jixitwgz")
    private String jixitwgz;//计息头尾规则
    @JsonProperty(value = "jixizxje")
    private BigDecimal jixizxje;//计息最小金额
    @JsonProperty(value = "jixisrgz")
    private String jixisrgz;//计息舍入规则
    @JsonProperty(value = "srzxdanw")
    private String srzxdanw;//舍入最小单位
    @JsonProperty(value = "llqxkdfs")
    private String llqxkdfs;//利率期限靠档方式
    @JsonProperty(value = "fdjixibz")
    private String fdjixibz;//分段计息标志
    @JsonProperty(value = "yuqitzfs")
    private String yuqitzfs;//逾期利率调整方式
    @JsonProperty(value = "yuqitzzq")
    private String yuqitzzq;//逾期利率调整周期
    @JsonProperty(value = "fulilvbh")
    private String fulilvbh;//复利利率编号
    @JsonProperty(value = "fulilvny")
    private String fulilvny;//复利利率年月标识
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "fulitzfs")
    private String fulitzfs;//复利利率调整方式
    @JsonProperty(value = "fulitzzq")
    private String fulitzzq;//复利利率调整周期
    @JsonProperty(value = "fulifdfs")
    private String fulifdfs;//复利利率浮动方式
    @JsonProperty(value = "fulifdzh")
    private BigDecimal fulifdzh;//复利利率浮动值
    @JsonProperty(value = "jitiguiz")
    private String jitiguiz;//计提规则
    @JsonProperty(value = "zqxizdds")
    private Integer zqxizdds;//早起息最大天数
    @JsonProperty(value = "wqxizdds")
    private Integer wqxizdds;//晚起息最大天数
    @JsonProperty(value = "butijejs")
    private String butijejs;//补贴金额计算方式
    @JsonProperty(value = "tiaozhkf")
    private String tiaozhkf;//允许调整还款方式
    @JsonProperty(value = "scihkrbz")
    private String scihkrbz;//首次还款日模式
    @JsonProperty(value = "mqihkfsh")
    private String mqihkfsh;//末期还款方式
    @JsonProperty(value = "yunxsuoq")
    private String yunxsuoq;//允许缩期
    @JsonProperty(value = "suoqcish")
    private Integer suoqcish;//缩期次数
    @JsonProperty(value = "bzuekkfs")
    private String bzuekkfs;//不足额扣款方式
    @JsonProperty(value = "yqbzkkfs")
    private String yqbzkkfs;//逾期不足额扣款方式
    @JsonProperty(value = "hntmihku")
    private String hntmihku;//允许行内同名账户还款
    @JsonProperty(value = "hnftmhku")
    private String hnftmhku;//允许行内非同名帐户还款
    @JsonProperty(value = "nbuzhhku")
    private String nbuzhhku;//允许内部账户还款
    @JsonProperty(value = "tiqhksdq")
    private Integer tiqhksdq;//提前还款锁定期
    @JsonProperty(value = "sfyxkuxq")
    private String sfyxkuxq;//是否有宽限期
    @JsonProperty(value = "kuanxqts")
    private Integer kuanxqts;//宽限期天数
    @JsonProperty(value = "kxqjixgz")
    private String kxqjixgz;//宽限期计息方式
    @JsonProperty(value = "kxqhjxgz")
    private String kxqhjxgz;//宽限期后计息规则
    @JsonProperty(value = "ysfyywbm")
    private String ysfyywbm;//应收费用业务编码
    @JsonProperty(value = "ysfjywbm")
    private String ysfjywbm;//应收罚金业务编码
    @JsonProperty(value = "yffyywbm")
    private String yffyywbm;//应付费用业务编码
    @JsonProperty(value = "yffjywbm")
    private String yffjywbm;//应付罚金业务编码
    @JsonProperty(value = "dfbjywbm")
    private String dfbjywbm;//待付款本金业务编码
    @JsonProperty(value = "dflxywbm")
    private String dflxywbm;//待付款利息业务编码
    @JsonProperty(value = "yywsywbm")
    private String yywsywbm;//营业外收入业务编码
    @JsonProperty(value = "yywzywbm")
    private String yywzywbm;//营业外支出业务编码
    @JsonProperty(value = "bjghywbm")
    private String bjghywbm;//本金归还入账业务编码
    @JsonProperty(value = "lxghywbm")
    private String lxghywbm;//利息归还入账业务编码
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "flllcklx")
    private String flllcklx;//复利利率参考类型
    @JsonProperty(value = "qglxleix")
    private String qglxleix;//期供利息类型
    @JsonProperty(value = "qigscfsh")
    private String qigscfsh;//期供生成方式
    @JsonProperty(value = "dktiansh")
    private Integer dktiansh;//贷款天数
    @JsonProperty(value = "yijtdklx")
    private BigDecimal yijtdklx;//已计提贷款利息
    @JsonProperty(value = "yihxbjlx")
    private BigDecimal yihxbjlx;//已核销本金利息
    @JsonProperty(value = "kshchpdm")
    private String kshchpdm;//可售产品代码
    @JsonProperty(value = "kshchpmc")
    private String kshchpmc;//可售产品名称
    @JsonProperty(value = "dkczhzhh")
    private String dkczhzhh;//贷款出账号
    @JsonProperty(value = "dkdbfshi")
    private String dkdbfshi;//贷款担保方式
    @JsonProperty(value = "dkyongtu")
    private String dkyongtu;//贷款用途
    @JsonProperty(value = "jiejuxzh")
    private String jiejuxzh;//借据性质
    @JsonProperty(value = "fkjzhfsh")
    private String fkjzhfsh;//放款记账方式
    @JsonProperty(value = "shtzfhxm")
    private String shtzfhxm;//受托支付业务编码
    @JsonProperty(value = "bwhesdma")
    private String bwhesdma;//表外核算码
    @JsonProperty(value = "ziczhtai")
    private String ziczhtai;//资产状态
    @JsonProperty(value = "shlxsfhq")
    private String shlxsfhq;//赎回时点利息还清标志
    @JsonProperty(value = "wtrmingc")
    private String wtrmingc;//委托人名称
    @JsonProperty(value = "dkjzcslx")
    private String dkjzcslx;//贷款减值测试类型
    @JsonProperty(value = "jzhdkbzh")
    private String jzhdkbzh;//减值贷款标志
    @JsonProperty(value = "jianzhrq")
    private String jianzhrq;//减值日期
    @JsonProperty(value = "daikbenj")
    private BigDecimal daikbenj;//贷款本金
    @JsonProperty(value = "jianzhbj")
    private BigDecimal jianzhbj;//减值本金
    @JsonProperty(value = "bnyjlixi")
    private BigDecimal bnyjlixi;//表内应计利息
    @JsonProperty(value = "bnyslixi")
    private BigDecimal bnyslixi;//表内应收利息
    @JsonProperty(value = "bwyjlixi")
    private BigDecimal bwyjlixi;//表外应计利息
    @JsonProperty(value = "bwyslixi")
    private BigDecimal bwyslixi;//表外应收利息
    @JsonProperty(value = "lxtiaozh")
    private BigDecimal lxtiaozh;//利息调整
    @JsonProperty(value = "jzsdlxtz")
    private BigDecimal jzsdlxtz;//减值时点利息调整
    @JsonProperty(value = "bobebili")
    private BigDecimal bobebili;//拨备比例
    @JsonProperty(value = "jianzssh")
    private BigDecimal jianzssh;//减值损失
    @JsonProperty(value = "jianzzhb")
    private BigDecimal jianzzhb;//减值准备
    @JsonProperty(value = "jzhlxqsr")
    private String jzhlxqsr;//减值利息计提起始日
    @JsonProperty(value = "jzhlxjit")
    private BigDecimal jzhlxjit;//减值利息计提
    @JsonProperty(value = "jzdklxhs")
    private BigDecimal jzdklxhs;//减值贷款利息回收
    @JsonProperty(value = "scbbjtrq")
    private String scbbjtrq;//上次拨备计提日期
    @JsonProperty(value = "ysxlyzhh")
    private String ysxlyzhh;//预收息扣息来源账号
    @JsonProperty(value = "ysxlyzxh")
    private String ysxlyzxh;//预收息扣息来源账号子序号
    @JsonProperty(value = "llfdcycf")
    private String llfdcycf;//利率分段次月调整触发日
    @JsonProperty(value = "llfdcytz")
    private String llfdcytz;//利率分段次月调整调整日
    @JsonProperty(value = "ljsxqish")
    private Integer ljsxqish;//累进首段期数
    @JsonProperty(value = "mqhbbili")
    private BigDecimal mqhbbili;//每期还本比例
    @JsonProperty(value = "jrlxsybz")
    private String jrlxsybz;//节假日利息顺延标志
    @JsonProperty(value = "yqzdzjbz")
    private String yqzdzjbz;//逾期自动追缴标志
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期
    @JsonProperty(value = "benjinfd")
    private String benjinfd;//本金分段
    @JsonProperty(value = "lstydkjjh")
    private java.util.List<Lstydkjjh> lstydkjjh;//借新还旧原贷款借据号列表
    @JsonProperty(value = "lstdkfkjh")
    private java.util.List<Lstdkfkjh> lstdkfkjh;//贷款账户放款计划
    @JsonProperty(value = "lstdkllfd")
    private java.util.List<Lstdkllfd> lstdkllfd;//贷款利率分段登记
    @JsonProperty(value = "lstdktxzh")
    private java.util.List<Lstdktxzh> lstdktxzh;//贷款贴息账户属性
    @JsonProperty(value = "lstdkhbjh")
    private java.util.List<Lstdkhbjh> lstdkhbjh;//贷款指定还本计划
    @JsonProperty(value = "lstdkbjfd")
    private java.util.List<Lstdkbjfd> lstdkbjfd;//本金分段登记
    @JsonProperty(value = "lstdkhkzh")
    private java.util.List<Lstdkhkzh> lstdkhkzh;//贷款多还款账户
    @JsonProperty(value = "Lstdkzhqg")
    private java.util.List<Lstdkzhqg> lstdkzhqg;//贷款账户期供
    @JsonProperty(value = "lstdkzhlm")
    private java.util.List<Lstdkzhlm> lstdkzhlm;//贷款账户联名信息
    @JsonProperty(value = "lstdkzhbz")
    private java.util.List<Lstdkzhbz> lstdkzhbz;//贷款账户保证信息
    @JsonProperty(value = "lstdklhmxe")
    private java.util.List<Lstdklhmx> lstdklhmxe;//贷款联合放款
    @JsonProperty(value = "lstdksfsj")
    private java.util.List<Lstdksfsj> lstdksfsj;//贷款收费事件
    @JsonProperty(value = "lstdkfwdj")
    private java.util.List<Lstdkfwdj> lstdkfwdj;//贷款服务登记
    @JsonProperty(value = "lstdkwtxxe")
    private java.util.List<Lstdkwtxx> lstdkwtxxe;//贷款多委托人账户
    @JsonProperty(value = "lstdkstzf")
    private java.util.List<Lstdkstzf> lstdkstzf;//贷款受托支付
    @JsonProperty(value = "lstdzqgjh")
    private java.util.List<Lstdzqgjh> lstdzqgjh;//贷款定制期供计划表
    @JsonProperty(value = "lstdknbgl")
    private java.util.List<Lstdknbgl> lstdknbgl;//对客借据与内部借据关联关系
    @JsonProperty(value = "lstdkzhzie")
    private java.util.List<Lstdkzhzy> lstdkzhzie;//贷款账户质押信息

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getZhwujgmc() {
        return zhwujgmc;
    }

    public void setZhwujgmc(String zhwujgmc) {
        this.zhwujgmc = zhwujgmc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getDuowtrbz() {
        return duowtrbz;
    }

    public void setDuowtrbz(String duowtrbz) {
        this.duowtrbz = duowtrbz;
    }

    public String getDlhesfsh() {
        return dlhesfsh;
    }

    public void setDlhesfsh(String dlhesfsh) {
        this.dlhesfsh = dlhesfsh;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public Integer getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Integer dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public String getZhcwjyrq() {
        return zhcwjyrq;
    }

    public void setZhcwjyrq(String zhcwjyrq) {
        this.zhcwjyrq = zhcwjyrq;
    }

    public Integer getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Integer mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getKaihujig() {
        return kaihujig;
    }

    public void setKaihujig(String kaihujig) {
        this.kaihujig = kaihujig;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getXiaohurq() {
        return xiaohurq;
    }

    public void setXiaohurq(String xiaohurq) {
        this.xiaohurq = xiaohurq;
    }

    public String getXiaohugy() {
        return xiaohugy;
    }

    public void setXiaohugy(String xiaohugy) {
        this.xiaohugy = xiaohugy;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getFafangje() {
        return fafangje;
    }

    public void setFafangje(BigDecimal fafangje) {
        this.fafangje = fafangje;
    }

    public BigDecimal getDjiekfje() {
        return djiekfje;
    }

    public void setDjiekfje(BigDecimal djiekfje) {
        this.djiekfje = djiekfje;
    }

    public BigDecimal getKffangje() {
        return kffangje;
    }

    public void setKffangje(BigDecimal kffangje) {
        this.kffangje = kffangje;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getDkuanjij() {
        return dkuanjij;
    }

    public void setDkuanjij(BigDecimal dkuanjij) {
        this.dkuanjij = dkuanjij;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getTiexfuli() {
        return tiexfuli;
    }

    public void setTiexfuli(BigDecimal tiexfuli) {
        this.tiexfuli = tiexfuli;
    }

    public BigDecimal getDaitanlx() {
        return daitanlx;
    }

    public void setDaitanlx(BigDecimal daitanlx) {
        this.daitanlx = daitanlx;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getLixishru() {
        return lixishru;
    }

    public void setLixishru(BigDecimal lixishru) {
        this.lixishru = lixishru;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getFeiyshru() {
        return feiyshru;
    }

    public void setFeiyshru(BigDecimal feiyshru) {
        this.feiyshru = feiyshru;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getFjinshru() {
        return fjinshru;
    }

    public void setFjinshru(BigDecimal fjinshru) {
        this.fjinshru = fjinshru;
    }

    public BigDecimal getZhunbeij() {
        return zhunbeij;
    }

    public void setZhunbeij(BigDecimal zhunbeij) {
        this.zhunbeij = zhunbeij;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public String getYewufenl() {
        return yewufenl;
    }

    public void setYewufenl(String yewufenl) {
        this.yewufenl = yewufenl;
    }

    public String getYsywbhao() {
        return ysywbhao;
    }

    public void setYsywbhao(String ysywbhao) {
        this.ysywbhao = ysywbhao;
    }

    public String getYsywleix() {
        return ysywleix;
    }

    public void setYsywleix(String ysywleix) {
        this.ysywleix = ysywleix;
    }

    public String getYjfyjhes() {
        return yjfyjhes;
    }

    public void setYjfyjhes(String yjfyjhes) {
        this.yjfyjhes = yjfyjhes;
    }

    public String getYiyldhes() {
        return yiyldhes;
    }

    public void setYiyldhes(String yiyldhes) {
        this.yiyldhes = yiyldhes;
    }

    public String getDkxtkmhs() {
        return dkxtkmhs;
    }

    public void setDkxtkmhs(String dkxtkmhs) {
        this.dkxtkmhs = dkxtkmhs;
    }

    public String getZhqifkbz() {
        return zhqifkbz;
    }

    public void setZhqifkbz(String zhqifkbz) {
        this.zhqifkbz = zhqifkbz;
    }

    public String getFkzhouqi() {
        return fkzhouqi;
    }

    public void setFkzhouqi(String fkzhouqi) {
        this.fkzhouqi = fkzhouqi;
    }

    public String getFkfangsh() {
        return fkfangsh;
    }

    public void setFkfangsh(String fkfangsh) {
        this.fkfangsh = fkfangsh;
    }

    public BigDecimal getBencfkje() {
        return bencfkje;
    }

    public void setBencfkje(BigDecimal bencfkje) {
        this.bencfkje = bencfkje;
    }

    public BigDecimal getMcfkjebl() {
        return mcfkjebl;
    }

    public void setMcfkjebl(BigDecimal mcfkjebl) {
        this.mcfkjebl = mcfkjebl;
    }

    public String getScfkriqi() {
        return scfkriqi;
    }

    public void setScfkriqi(String scfkriqi) {
        this.scfkriqi = scfkriqi;
    }

    public String getJxhjhkkz() {
        return jxhjhkkz;
    }

    public void setJxhjhkkz(String jxhjhkkz) {
        this.jxhjhkkz = jxhjhkkz;
    }

    public String getJxhjdkkz() {
        return jxhjdkkz;
    }

    public void setJxhjdkkz(String jxhjdkkz) {
        this.jxhjdkkz = jxhjdkkz;
    }

    public String getHjiuleix() {
        return hjiuleix;
    }

    public void setHjiuleix(String hjiuleix) {
        this.hjiuleix = hjiuleix;
    }

    public String getDzhifkjh() {
        return dzhifkjh;
    }

    public void setDzhifkjh(String dzhifkjh) {
        this.dzhifkjh = dzhifkjh;
    }

    public String getFkzjclfs() {
        return fkzjclfs;
    }

    public void setFkzjclfs(String fkzjclfs) {
        this.fkzjclfs = fkzjclfs;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public String getLtjixigz() {
        return ltjixigz;
    }

    public void setLtjixigz(String ltjixigz) {
        this.ltjixigz = ltjixigz;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getWhlilvzl() {
        return whlilvzl;
    }

    public void setWhlilvzl(String whlilvzl) {
        this.whlilvzl = whlilvzl;
    }

    public String getWhllqxzl() {
        return whllqxzl;
    }

    public void setWhllqxzl(String whllqxzl) {
        this.whllqxzl = whllqxzl;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getZaoqixbz() {
        return zaoqixbz;
    }

    public void setZaoqixbz(String zaoqixbz) {
        this.zaoqixbz = zaoqixbz;
    }

    public String getWanqixbz() {
        return wanqixbz;
    }

    public void setWanqixbz(String wanqixbz) {
        this.wanqixbz = wanqixbz;
    }

    public Integer getQixitshu() {
        return qixitshu;
    }

    public void setQixitshu(Integer qixitshu) {
        this.qixitshu = qixitshu;
    }

    public String getYushxfsh() {
        return yushxfsh;
    }

    public void setYushxfsh(String yushxfsh) {
        this.yushxfsh = yushxfsh;
    }

    public BigDecimal getYushxize() {
        return yushxize;
    }

    public void setYushxize(BigDecimal yushxize) {
        this.yushxize = yushxize;
    }

    public String getLixitxzq() {
        return lixitxzq;
    }

    public void setLixitxzq(String lixitxzq) {
        this.lixitxzq = lixitxzq;
    }

    public String getMeictxfs() {
        return meictxfs;
    }

    public void setMeictxfs(String meictxfs) {
        this.meictxfs = meictxfs;
    }

    public BigDecimal getMeictxbl() {
        return meictxbl;
    }

    public void setMeictxbl(BigDecimal meictxbl) {
        this.meictxbl = meictxbl;
    }

    public String getXiacitxr() {
        return xiacitxr;
    }

    public void setXiacitxr(String xiacitxr) {
        this.xiacitxr = xiacitxr;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    public String getKxqzyqgz() {
        return kxqzyqgz;
    }

    public void setKxqzyqgz(String kxqzyqgz) {
        this.kxqzyqgz = kxqzyqgz;
    }

    public String getKxqshxgz() {
        return kxqshxgz;
    }

    public void setKxqshxgz(String kxqshxgz) {
        this.kxqshxgz = kxqshxgz;
    }

    public String getJixibzhi() {
        return jixibzhi;
    }

    public void setJixibzhi(String jixibzhi) {
        this.jixibzhi = jixibzhi;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFxjxbzhi() {
        return fxjxbzhi;
    }

    public void setFxjxbzhi(String fxjxbzhi) {
        this.fxjxbzhi = fxjxbzhi;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getSctxriqi() {
        return sctxriqi;
    }

    public void setSctxriqi(String sctxriqi) {
        this.sctxriqi = sctxriqi;
    }

    public String getXctxriqi() {
        return xctxriqi;
    }

    public void setXctxriqi(String xctxriqi) {
        this.xctxriqi = xctxriqi;
    }

    public String getScyqtxrq() {
        return scyqtxrq;
    }

    public void setScyqtxrq(String scyqtxrq) {
        this.scyqtxrq = scyqtxrq;
    }

    public String getXcyqtxrq() {
        return xcyqtxrq;
    }

    public void setXcyqtxrq(String xcyqtxrq) {
        this.xcyqtxrq = xcyqtxrq;
    }

    public String getScfxtxrq() {
        return scfxtxrq;
    }

    public void setScfxtxrq(String scfxtxrq) {
        this.scfxtxrq = scfxtxrq;
    }

    public String getXcfxtxrq() {
        return xcfxtxrq;
    }

    public void setXcfxtxrq(String xcfxtxrq) {
        this.xcfxtxrq = xcfxtxrq;
    }

    public String getScnytxrq() {
        return scnytxrq;
    }

    public void setScnytxrq(String scnytxrq) {
        this.scnytxrq = scnytxrq;
    }

    public String getXcnytxrq() {
        return xcnytxrq;
    }

    public void setXcnytxrq(String xcnytxrq) {
        this.xcnytxrq = xcnytxrq;
    }

    public String getScjixirq() {
        return scjixirq;
    }

    public void setScjixirq(String scjixirq) {
        this.scjixirq = scjixirq;
    }

    public Integer getJiximxxh() {
        return jiximxxh;
    }

    public void setJiximxxh(Integer jiximxxh) {
        this.jiximxxh = jiximxxh;
    }

    public String getLilvfend() {
        return lilvfend;
    }

    public void setLilvfend(String lilvfend) {
        this.lilvfend = lilvfend;
    }

    public String getTiexibzh() {
        return tiexibzh;
    }

    public void setTiexibzh(String tiexibzh) {
        this.tiexibzh = tiexibzh;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHezuofbh() {
        return hezuofbh;
    }

    public void setHezuofbh(String hezuofbh) {
        this.hezuofbh = hezuofbh;
    }

    public String getHezuofmc() {
        return hezuofmc;
    }

    public void setHezuofmc(String hezuofmc) {
        this.hezuofmc = hezuofmc;
    }

    public String getDuophkbz() {
        return duophkbz;
    }

    public void setDuophkbz(String duophkbz) {
        this.duophkbz = duophkbz;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public BigDecimal getShunylix() {
        return shunylix;
    }

    public void setShunylix(BigDecimal shunylix) {
        this.shunylix = shunylix;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    public String getSchkriqi() {
        return schkriqi;
    }

    public void setSchkriqi(String schkriqi) {
        this.schkriqi = schkriqi;
    }

    public String getXchkriqi() {
        return xchkriqi;
    }

    public void setXchkriqi(String xchkriqi) {
        this.xchkriqi = xchkriqi;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public Integer getZongqish() {
        return zongqish;
    }

    public void setZongqish(Integer zongqish) {
        this.zongqish = zongqish;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public Integer getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(Integer benqizqs) {
        this.benqizqs = benqizqs;
    }

    public Integer getJixizqsh() {
        return jixizqsh;
    }

    public void setJixizqsh(Integer jixizqsh) {
        this.jixizqsh = jixizqsh;
    }

    public Integer getJixiqish() {
        return jixiqish;
    }

    public void setJixiqish(Integer jixiqish) {
        this.jixiqish = jixiqish;
    }

    public BigDecimal getJixibenj() {
        return jixibenj;
    }

    public void setJixibenj(BigDecimal jixibenj) {
        this.jixibenj = jixibenj;
    }

    public Integer getDanqbjds() {
        return danqbjds;
    }

    public void setDanqbjds(Integer danqbjds) {
        this.danqbjds = danqbjds;
    }

    public String getHkshxubh() {
        return hkshxubh;
    }

    public void setHkshxubh(String hkshxubh) {
        this.hkshxubh = hkshxubh;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getZdplkkbz() {
        return zdplkkbz;
    }

    public void setZdplkkbz(String zdplkkbz) {
        this.zdplkkbz = zdplkkbz;
    }

    public String getZdjqdkbz() {
        return zdjqdkbz;
    }

    public void setZdjqdkbz(String zdjqdkbz) {
        this.zdjqdkbz = zdjqdkbz;
    }

    public String getQyxhdkbz() {
        return qyxhdkbz;
    }

    public void setQyxhdkbz(String qyxhdkbz) {
        this.qyxhdkbz = qyxhdkbz;
    }

    public String getXhdkqyzh() {
        return xhdkqyzh;
    }

    public void setXhdkqyzh(String xhdkqyzh) {
        this.xhdkqyzh = xhdkqyzh;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getYunxdkzq() {
        return yunxdkzq;
    }

    public void setYunxdkzq(String yunxdkzq) {
        this.yunxdkzq = yunxdkzq;
    }

    public String getZhqxzekk() {
        return zhqxzekk;
    }

    public void setZhqxzekk(String zhqxzekk) {
        this.zhqxzekk = zhqxzekk;
    }

    public Integer getZhqzdcsh() {
        return zhqzdcsh;
    }

    public void setZhqzdcsh(Integer zhqzdcsh) {
        this.zhqzdcsh = zhqzdcsh;
    }

    public String getZqgzbhao() {
        return zqgzbhao;
    }

    public void setZqgzbhao(String zqgzbhao) {
        this.zqgzbhao = zqgzbhao;
    }

    public Integer getYizhqcsh() {
        return yizhqcsh;
    }

    public void setYizhqcsh(Integer yizhqcsh) {
        this.yizhqcsh = yizhqcsh;
    }

    public Integer getZhanqixh() {
        return zhanqixh;
    }

    public void setZhanqixh(Integer zhanqixh) {
        this.zhanqixh = zhanqixh;
    }

    public String getZbjrsygz() {
        return zbjrsygz;
    }

    public void setZbjrsygz(String zbjrsygz) {
        this.zbjrsygz = zbjrsygz;
    }

    public String getZidxtzhy() {
        return zidxtzhy;
    }

    public void setZidxtzhy(String zidxtzhy) {
        this.zidxtzhy = zidxtzhy;
    }

    public String getLixizcgz() {
        return lixizcgz;
    }

    public void setLixizcgz(String lixizcgz) {
        this.lixizcgz = lixizcgz;
    }

    public String getLixizhgz() {
        return lixizhgz;
    }

    public void setLixizhgz(String lixizhgz) {
        this.lixizhgz = lixizhgz;
    }

    public String getTqhkfjbh() {
        return tqhkfjbh;
    }

    public void setTqhkfjbh(String tqhkfjbh) {
        this.tqhkfjbh = tqhkfjbh;
    }

    public String getTqhkfjmc() {
        return tqhkfjmc;
    }

    public void setTqhkfjmc(String tqhkfjmc) {
        this.tqhkfjmc = tqhkfjmc;
    }

    public BigDecimal getTqhkfjfj() {
        return tqhkfjfj;
    }

    public void setTqhkfjfj(BigDecimal tqhkfjfj) {
        this.tqhkfjfj = tqhkfjfj;
    }

    public String getDlxxzdgz() {
        return dlxxzdgz;
    }

    public void setDlxxzdgz(String dlxxzdgz) {
        this.dlxxzdgz = dlxxzdgz;
    }

    public String getDlxxqzgz() {
        return dlxxqzgz;
    }

    public void setDlxxqzgz(String dlxxqzgz) {
        this.dlxxqzgz = dlxxqzgz;
    }

    public String getWtckywbm() {
        return wtckywbm;
    }

    public void setWtckywbm(String wtckywbm) {
        this.wtckywbm = wtckywbm;
    }

    public Integer getDailixuh() {
        return dailixuh;
    }

    public void setDailixuh(Integer dailixuh) {
        this.dailixuh = dailixuh;
    }

    public String getDailimsh() {
        return dailimsh;
    }

    public void setDailimsh(String dailimsh) {
        this.dailimsh = dailimsh;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public String getShynedbz() {
        return shynedbz;
    }

    public void setShynedbz(String shynedbz) {
        this.shynedbz = shynedbz;
    }

    public String getEdbizhgz() {
        return edbizhgz;
    }

    public void setEdbizhgz(String edbizhgz) {
        this.edbizhgz = edbizhgz;
    }

    public String getEdzdbizh() {
        return edzdbizh;
    }

    public void setEdzdbizh(String edzdbizh) {
        this.edzdbizh = edzdbizh;
    }

    public String getEdshyngz() {
        return edshyngz;
    }

    public void setEdshyngz(String edshyngz) {
        this.edshyngz = edshyngz;
    }

    public String getSydkcnuo() {
        return sydkcnuo;
    }

    public void setSydkcnuo(String sydkcnuo) {
        this.sydkcnuo = sydkcnuo;
    }

    public String getCndkjjho() {
        return cndkjjho;
    }

    public void setCndkjjho(String cndkjjho) {
        this.cndkjjho = cndkjjho;
    }

    public String getZhjzhyfs() {
        return zhjzhyfs;
    }

    public void setZhjzhyfs(String zhjzhyfs) {
        this.zhjzhyfs = zhjzhyfs;
    }

    public String getDkgljgou() {
        return dkgljgou;
    }

    public void setDkgljgou(String dkgljgou) {
        this.dkgljgou = dkgljgou;
    }

    public String getGljgleib() {
        return gljgleib;
    }

    public void setGljgleib(String gljgleib) {
        this.gljgleib = gljgleib;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public String getWujiflbz() {
        return wujiflbz;
    }

    public void setWujiflbz(String wujiflbz) {
        this.wujiflbz = wujiflbz;
    }

    public String getWujiflrq() {
        return wujiflrq;
    }

    public void setWujiflrq(String wujiflrq) {
        this.wujiflrq = wujiflrq;
    }

    public String getLmdkbzhi() {
        return lmdkbzhi;
    }

    public void setLmdkbzhi(String lmdkbzhi) {
        this.lmdkbzhi = lmdkbzhi;
    }

    public BigDecimal getLeijfkje() {
        return leijfkje;
    }

    public void setLeijfkje(BigDecimal leijfkje) {
        this.leijfkje = leijfkje;
    }

    public BigDecimal getSnljfkje() {
        return snljfkje;
    }

    public void setSnljfkje(BigDecimal snljfkje) {
        this.snljfkje = snljfkje;
    }

    public BigDecimal getBnljfkje() {
        return bnljfkje;
    }

    public void setBnljfkje(BigDecimal bnljfkje) {
        this.bnljfkje = bnljfkje;
    }

    public BigDecimal getZuigdkye() {
        return zuigdkye;
    }

    public void setZuigdkye(BigDecimal zuigdkye) {
        this.zuigdkye = zuigdkye;
    }

    public BigDecimal getSnzgzhye() {
        return snzgzhye;
    }

    public void setSnzgzhye(BigDecimal snzgzhye) {
        this.snzgzhye = snzgzhye;
    }

    public BigDecimal getBnzgzhye() {
        return bnzgzhye;
    }

    public void setBnzgzhye(BigDecimal bnzgzhye) {
        this.bnzgzhye = bnzgzhye;
    }

    public BigDecimal getLeijyhbj() {
        return leijyhbj;
    }

    public void setLeijyhbj(BigDecimal leijyhbj) {
        this.leijyhbj = leijyhbj;
    }

    public BigDecimal getSnljyhbj() {
        return snljyhbj;
    }

    public void setSnljyhbj(BigDecimal snljyhbj) {
        this.snljyhbj = snljyhbj;
    }

    public BigDecimal getBnljyhbj() {
        return bnljyhbj;
    }

    public void setBnljyhbj(BigDecimal bnljyhbj) {
        this.bnljyhbj = bnljyhbj;
    }

    public BigDecimal getYishhxbj() {
        return yishhxbj;
    }

    public void setYishhxbj(BigDecimal yishhxbj) {
        this.yishhxbj = yishhxbj;
    }

    public BigDecimal getYishhxlx() {
        return yishhxlx;
    }

    public void setYishhxlx(BigDecimal yishhxlx) {
        this.yishhxlx = yishhxlx;
    }

    public BigDecimal getLeijyhlx() {
        return leijyhlx;
    }

    public void setLeijyhlx(BigDecimal leijyhlx) {
        this.leijyhlx = leijyhlx;
    }

    public BigDecimal getSnljyhlx() {
        return snljyhlx;
    }

    public void setSnljyhlx(BigDecimal snljyhlx) {
        this.snljyhlx = snljyhlx;
    }

    public BigDecimal getBnljyhlx() {
        return bnljyhlx;
    }

    public void setBnljyhlx(BigDecimal bnljyhlx) {
        this.bnljyhlx = bnljyhlx;
    }

    public BigDecimal getSyljyhlx() {
        return syljyhlx;
    }

    public void setSyljyhlx(BigDecimal syljyhlx) {
        this.syljyhlx = syljyhlx;
    }

    public BigDecimal getByljyhlx() {
        return byljyhlx;
    }

    public void setByljyhlx(BigDecimal byljyhlx) {
        this.byljyhlx = byljyhlx;
    }

    public BigDecimal getLeijcslx() {
        return leijcslx;
    }

    public void setLeijcslx(BigDecimal leijcslx) {
        this.leijcslx = leijcslx;
    }

    public BigDecimal getSnljcslx() {
        return snljcslx;
    }

    public void setSnljcslx(BigDecimal snljcslx) {
        this.snljcslx = snljcslx;
    }

    public BigDecimal getBnljcslx() {
        return bnljcslx;
    }

    public void setBnljcslx(BigDecimal bnljcslx) {
        this.bnljcslx = bnljcslx;
    }

    public BigDecimal getByljxxsh() {
        return byljxxsh;
    }

    public void setByljxxsh(BigDecimal byljxxsh) {
        this.byljxxsh = byljxxsh;
    }

    public String getZhchtqtz() {
        return zhchtqtz;
    }

    public void setZhchtqtz(String zhchtqtz) {
        this.zhchtqtz = zhchtqtz;
    }

    public Integer getTzhtqtsh() {
        return tzhtqtsh;
    }

    public void setTzhtqtsh(Integer tzhtqtsh) {
        this.tzhtqtsh = tzhtqtsh;
    }

    public String getYqcshtzh() {
        return yqcshtzh;
    }

    public void setYqcshtzh(String yqcshtzh) {
        this.yqcshtzh = yqcshtzh;
    }

    public Integer getTzhjgtsh() {
        return tzhjgtsh;
    }

    public void setTzhjgtsh(Integer tzhjgtsh) {
        this.tzhjgtsh = tzhjgtsh;
    }

    public String getLilvbgtz() {
        return lilvbgtz;
    }

    public void setLilvbgtz(String lilvbgtz) {
        this.lilvbgtz = lilvbgtz;
    }

    public String getYuebgtzh() {
        return yuebgtzh;
    }

    public void setYuebgtzh(String yuebgtzh) {
        this.yuebgtzh = yuebgtzh;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getBzhrdbbz() {
        return bzhrdbbz;
    }

    public void setBzhrdbbz(String bzhrdbbz) {
        this.bzhrdbbz = bzhrdbbz;
    }

    public BigDecimal getBenjheji() {
        return benjheji;
    }

    public void setBenjheji(BigDecimal benjheji) {
        this.benjheji = benjheji;
    }

    public BigDecimal getLixiheji() {
        return lixiheji;
    }

    public void setLixiheji(BigDecimal lixiheji) {
        this.lixiheji = lixiheji;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getYewusx02() {
        return yewusx02;
    }

    public void setYewusx02(String yewusx02) {
        this.yewusx02 = yewusx02;
    }

    public String getYewusx03() {
        return yewusx03;
    }

    public void setYewusx03(String yewusx03) {
        this.yewusx03 = yewusx03;
    }

    public String getYewusx04() {
        return yewusx04;
    }

    public void setYewusx04(String yewusx04) {
        this.yewusx04 = yewusx04;
    }

    public String getYewusx05() {
        return yewusx05;
    }

    public void setYewusx05(String yewusx05) {
        this.yewusx05 = yewusx05;
    }

    public String getYewusx06() {
        return yewusx06;
    }

    public void setYewusx06(String yewusx06) {
        this.yewusx06 = yewusx06;
    }

    public String getYewusx07() {
        return yewusx07;
    }

    public void setYewusx07(String yewusx07) {
        this.yewusx07 = yewusx07;
    }

    public String getYewusx08() {
        return yewusx08;
    }

    public void setYewusx08(String yewusx08) {
        this.yewusx08 = yewusx08;
    }

    public String getYewusx09() {
        return yewusx09;
    }

    public void setYewusx09(String yewusx09) {
        this.yewusx09 = yewusx09;
    }

    public String getYewusx10() {
        return yewusx10;
    }

    public void setYewusx10(String yewusx10) {
        this.yewusx10 = yewusx10;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getSfpinlvv() {
        return sfpinlvv;
    }

    public void setSfpinlvv(String sfpinlvv) {
        this.sfpinlvv = sfpinlvv;
    }

    public String getSfzhouqi() {
        return sfzhouqi;
    }

    public void setSfzhouqi(String sfzhouqi) {
        this.sfzhouqi = sfzhouqi;
    }

    public String getShoufzhl() {
        return shoufzhl;
    }

    public void setShoufzhl(String shoufzhl) {
        this.shoufzhl = shoufzhl;
    }

    public String getTxzhouqi() {
        return txzhouqi;
    }

    public void setTxzhouqi(String txzhouqi) {
        this.txzhouqi = txzhouqi;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    public String getFufeizhh() {
        return fufeizhh;
    }

    public void setFufeizhh(String fufeizhh) {
        this.fufeizhh = fufeizhh;
    }

    public String getFfzhhzxh() {
        return ffzhhzxh;
    }

    public void setFfzhhzxh(String ffzhhzxh) {
        this.ffzhhzxh = ffzhhzxh;
    }

    public String getFfzhhmch() {
        return ffzhhmch;
    }

    public void setFfzhhmch(String ffzhhmch) {
        this.ffzhhmch = ffzhhmch;
    }

    public String getSfrzhzhh() {
        return sfrzhzhh;
    }

    public void setSfrzhzhh(String sfrzhzhh) {
        this.sfrzhzhh = sfrzhzhh;
    }

    public String getSfrzhzxh() {
        return sfrzhzxh;
    }

    public void setSfrzhzxh(String sfrzhzxh) {
        this.sfrzhzxh = sfrzhzxh;
    }

    public String getMqjqsfbz() {
        return mqjqsfbz;
    }

    public void setMqjqsfbz(String mqjqsfbz) {
        this.mqjqsfbz = mqjqsfbz;
    }

    public String getFybkdqbz() {
        return fybkdqbz;
    }

    public void setFybkdqbz(String fybkdqbz) {
        this.fybkdqbz = fybkdqbz;
    }

    public String getSfljsfei() {
        return sfljsfei;
    }

    public void setSfljsfei(String sfljsfei) {
        this.sfljsfei = sfljsfei;
    }

    public String getYwsxms01() {
        return ywsxms01;
    }

    public void setYwsxms01(String ywsxms01) {
        this.ywsxms01 = ywsxms01;
    }

    public String getYwsxms02() {
        return ywsxms02;
    }

    public void setYwsxms02(String ywsxms02) {
        this.ywsxms02 = ywsxms02;
    }

    public String getYwsxms03() {
        return ywsxms03;
    }

    public void setYwsxms03(String ywsxms03) {
        this.ywsxms03 = ywsxms03;
    }

    public String getYwsxms04() {
        return ywsxms04;
    }

    public void setYwsxms04(String ywsxms04) {
        this.ywsxms04 = ywsxms04;
    }

    public String getYwsxms05() {
        return ywsxms05;
    }

    public void setYwsxms05(String ywsxms05) {
        this.ywsxms05 = ywsxms05;
    }

    public String getYwsxms06() {
        return ywsxms06;
    }

    public void setYwsxms06(String ywsxms06) {
        this.ywsxms06 = ywsxms06;
    }

    public String getYwsxms07() {
        return ywsxms07;
    }

    public void setYwsxms07(String ywsxms07) {
        this.ywsxms07 = ywsxms07;
    }

    public String getYwsxms08() {
        return ywsxms08;
    }

    public void setYwsxms08(String ywsxms08) {
        this.ywsxms08 = ywsxms08;
    }

    public String getYwsxms09() {
        return ywsxms09;
    }

    public void setYwsxms09(String ywsxms09) {
        this.ywsxms09 = ywsxms09;
    }

    public String getYwsxms10() {
        return ywsxms10;
    }

    public void setYwsxms10(String ywsxms10) {
        this.ywsxms10 = ywsxms10;
    }

    public String getHesuanfs() {
        return hesuanfs;
    }

    public void setHesuanfs(String hesuanfs) {
        this.hesuanfs = hesuanfs;
    }

    public Integer getKxqzdcsh() {
        return kxqzdcsh;
    }

    public void setKxqzdcsh(Integer kxqzdcsh) {
        this.kxqzdcsh = kxqzdcsh;
    }

    public String getYxtsfkbz() {
        return yxtsfkbz;
    }

    public void setYxtsfkbz(String yxtsfkbz) {
        this.yxtsfkbz = yxtsfkbz;
    }

    public String getZdfkjjms() {
        return zdfkjjms;
    }

    public void setZdfkjjms(String zdfkjjms) {
        this.zdfkjjms = zdfkjjms;
    }

    public String getXhdkzhxh() {
        return xhdkzhxh;
    }

    public void setXhdkzhxh(String xhdkzhxh) {
        this.xhdkzhxh = xhdkzhxh;
    }

    public String getShfleixi() {
        return shfleixi;
    }

    public void setShfleixi(String shfleixi) {
        this.shfleixi = shfleixi;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getKhjingli() {
        return khjingli;
    }

    public void setKhjingli(String khjingli) {
        this.khjingli = khjingli;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public BigDecimal getYinhshje() {
        return yinhshje;
    }

    public void setYinhshje(BigDecimal yinhshje) {
        this.yinhshje = yinhshje;
    }

    public BigDecimal getYinhshlv() {
        return yinhshlv;
    }

    public void setYinhshlv(BigDecimal yinhshlv) {
        this.yinhshlv = yinhshlv;
    }

    public String getChanpzdm() {
        return chanpzdm;
    }

    public void setChanpzdm(String chanpzdm) {
        this.chanpzdm = chanpzdm;
    }

    public String getChanpzmc() {
        return chanpzmc;
    }

    public void setChanpzmc(String chanpzmc) {
        this.chanpzmc = chanpzmc;
    }

    public String getWtrckuzh() {
        return wtrckuzh;
    }

    public void setWtrckuzh(String wtrckuzh) {
        this.wtrckuzh = wtrckuzh;
    }

    public String getWtrckzxh() {
        return wtrckzxh;
    }

    public void setWtrckzxh(String wtrckzxh) {
        this.wtrckzxh = wtrckzxh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getDaikdxxf() {
        return daikdxxf;
    }

    public void setDaikdxxf(String daikdxxf) {
        this.daikdxxf = daikdxxf;
    }

    public String getBwchapbz() {
        return bwchapbz;
    }

    public void setBwchapbz(String bwchapbz) {
        this.bwchapbz = bwchapbz;
    }

    public String getXunhdaik() {
        return xunhdaik;
    }

    public void setXunhdaik(String xunhdaik) {
        this.xunhdaik = xunhdaik;
    }

    public String getYansdaik() {
        return yansdaik;
    }

    public void setYansdaik(String yansdaik) {
        this.yansdaik = yansdaik;
    }

    public String getChendaik() {
        return chendaik;
    }

    public void setChendaik(String chendaik) {
        this.chendaik = chendaik;
    }

    public String getButidaik() {
        return butidaik;
    }

    public void setButidaik(String butidaik) {
        this.butidaik = butidaik;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getYintdkfs() {
        return yintdkfs;
    }

    public void setYintdkfs(String yintdkfs) {
        this.yintdkfs = yintdkfs;
    }

    public String getYintleib() {
        return yintleib;
    }

    public void setYintleib(String yintleib) {
        this.yintleib = yintleib;
    }

    public String getYintnbcy() {
        return yintnbcy;
    }

    public void setYintnbcy(String yintnbcy) {
        this.yintnbcy = yintnbcy;
    }

    public String getYintwbcy() {
        return yintwbcy;
    }

    public void setYintwbcy(String yintwbcy) {
        this.yintwbcy = yintwbcy;
    }

    public String getJiangulx() {
        return jiangulx;
    }

    public void setJiangulx(String jiangulx) {
        this.jiangulx = jiangulx;
    }

    public String getZhqizcqx() {
        return zhqizcqx;
    }

    public void setZhqizcqx(String zhqizcqx) {
        this.zhqizcqx = zhqizcqx;
    }

    public String getFangkulx() {
        return fangkulx;
    }

    public void setFangkulx(String fangkulx) {
        this.fangkulx = fangkulx;
    }

    public String getHntmifku() {
        return hntmifku;
    }

    public void setHntmifku(String hntmifku) {
        this.hntmifku = hntmifku;
    }

    public String getHnftmfku() {
        return hnftmfku;
    }

    public void setHnftmfku(String hnftmfku) {
        this.hnftmfku = hnftmfku;
    }

    public String getNeibufku() {
        return neibufku;
    }

    public void setNeibufku(String neibufku) {
        this.neibufku = neibufku;
    }

    public String getJixibjgz() {
        return jixibjgz;
    }

    public void setJixibjgz(String jixibjgz) {
        this.jixibjgz = jixibjgz;
    }

    public String getLixijsff() {
        return lixijsff;
    }

    public void setLixijsff(String lixijsff) {
        this.lixijsff = lixijsff;
    }

    public String getJixitwgz() {
        return jixitwgz;
    }

    public void setJixitwgz(String jixitwgz) {
        this.jixitwgz = jixitwgz;
    }

    public BigDecimal getJixizxje() {
        return jixizxje;
    }

    public void setJixizxje(BigDecimal jixizxje) {
        this.jixizxje = jixizxje;
    }

    public String getJixisrgz() {
        return jixisrgz;
    }

    public void setJixisrgz(String jixisrgz) {
        this.jixisrgz = jixisrgz;
    }

    public String getSrzxdanw() {
        return srzxdanw;
    }

    public void setSrzxdanw(String srzxdanw) {
        this.srzxdanw = srzxdanw;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getFdjixibz() {
        return fdjixibz;
    }

    public void setFdjixibz(String fdjixibz) {
        this.fdjixibz = fdjixibz;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getJitiguiz() {
        return jitiguiz;
    }

    public void setJitiguiz(String jitiguiz) {
        this.jitiguiz = jitiguiz;
    }

    public Integer getZqxizdds() {
        return zqxizdds;
    }

    public void setZqxizdds(Integer zqxizdds) {
        this.zqxizdds = zqxizdds;
    }

    public Integer getWqxizdds() {
        return wqxizdds;
    }

    public void setWqxizdds(Integer wqxizdds) {
        this.wqxizdds = wqxizdds;
    }

    public String getButijejs() {
        return butijejs;
    }

    public void setButijejs(String butijejs) {
        this.butijejs = butijejs;
    }

    public String getTiaozhkf() {
        return tiaozhkf;
    }

    public void setTiaozhkf(String tiaozhkf) {
        this.tiaozhkf = tiaozhkf;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public String getYunxsuoq() {
        return yunxsuoq;
    }

    public void setYunxsuoq(String yunxsuoq) {
        this.yunxsuoq = yunxsuoq;
    }

    public Integer getSuoqcish() {
        return suoqcish;
    }

    public void setSuoqcish(Integer suoqcish) {
        this.suoqcish = suoqcish;
    }

    public String getBzuekkfs() {
        return bzuekkfs;
    }

    public void setBzuekkfs(String bzuekkfs) {
        this.bzuekkfs = bzuekkfs;
    }

    public String getYqbzkkfs() {
        return yqbzkkfs;
    }

    public void setYqbzkkfs(String yqbzkkfs) {
        this.yqbzkkfs = yqbzkkfs;
    }

    public String getHntmihku() {
        return hntmihku;
    }

    public void setHntmihku(String hntmihku) {
        this.hntmihku = hntmihku;
    }

    public String getHnftmhku() {
        return hnftmhku;
    }

    public void setHnftmhku(String hnftmhku) {
        this.hnftmhku = hnftmhku;
    }

    public String getNbuzhhku() {
        return nbuzhhku;
    }

    public void setNbuzhhku(String nbuzhhku) {
        this.nbuzhhku = nbuzhhku;
    }

    public Integer getTiqhksdq() {
        return tiqhksdq;
    }

    public void setTiqhksdq(Integer tiqhksdq) {
        this.tiqhksdq = tiqhksdq;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public String getKxqjixgz() {
        return kxqjixgz;
    }

    public void setKxqjixgz(String kxqjixgz) {
        this.kxqjixgz = kxqjixgz;
    }

    public String getKxqhjxgz() {
        return kxqhjxgz;
    }

    public void setKxqhjxgz(String kxqhjxgz) {
        this.kxqhjxgz = kxqhjxgz;
    }

    public String getYsfyywbm() {
        return ysfyywbm;
    }

    public void setYsfyywbm(String ysfyywbm) {
        this.ysfyywbm = ysfyywbm;
    }

    public String getYsfjywbm() {
        return ysfjywbm;
    }

    public void setYsfjywbm(String ysfjywbm) {
        this.ysfjywbm = ysfjywbm;
    }

    public String getYffyywbm() {
        return yffyywbm;
    }

    public void setYffyywbm(String yffyywbm) {
        this.yffyywbm = yffyywbm;
    }

    public String getYffjywbm() {
        return yffjywbm;
    }

    public void setYffjywbm(String yffjywbm) {
        this.yffjywbm = yffjywbm;
    }

    public String getDfbjywbm() {
        return dfbjywbm;
    }

    public void setDfbjywbm(String dfbjywbm) {
        this.dfbjywbm = dfbjywbm;
    }

    public String getDflxywbm() {
        return dflxywbm;
    }

    public void setDflxywbm(String dflxywbm) {
        this.dflxywbm = dflxywbm;
    }

    public String getYywsywbm() {
        return yywsywbm;
    }

    public void setYywsywbm(String yywsywbm) {
        this.yywsywbm = yywsywbm;
    }

    public String getYywzywbm() {
        return yywzywbm;
    }

    public void setYywzywbm(String yywzywbm) {
        this.yywzywbm = yywzywbm;
    }

    public String getBjghywbm() {
        return bjghywbm;
    }

    public void setBjghywbm(String bjghywbm) {
        this.bjghywbm = bjghywbm;
    }

    public String getLxghywbm() {
        return lxghywbm;
    }

    public void setLxghywbm(String lxghywbm) {
        this.lxghywbm = lxghywbm;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getQglxleix() {
        return qglxleix;
    }

    public void setQglxleix(String qglxleix) {
        this.qglxleix = qglxleix;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public Integer getDktiansh() {
        return dktiansh;
    }

    public void setDktiansh(Integer dktiansh) {
        this.dktiansh = dktiansh;
    }

    public BigDecimal getYijtdklx() {
        return yijtdklx;
    }

    public void setYijtdklx(BigDecimal yijtdklx) {
        this.yijtdklx = yijtdklx;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getDkczhzhh() {
        return dkczhzhh;
    }

    public void setDkczhzhh(String dkczhzhh) {
        this.dkczhzhh = dkczhzhh;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getJiejuxzh() {
        return jiejuxzh;
    }

    public void setJiejuxzh(String jiejuxzh) {
        this.jiejuxzh = jiejuxzh;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public String getShtzfhxm() {
        return shtzfhxm;
    }

    public void setShtzfhxm(String shtzfhxm) {
        this.shtzfhxm = shtzfhxm;
    }

    public String getBwhesdma() {
        return bwhesdma;
    }

    public void setBwhesdma(String bwhesdma) {
        this.bwhesdma = bwhesdma;
    }

    public String getZiczhtai() {
        return ziczhtai;
    }

    public void setZiczhtai(String ziczhtai) {
        this.ziczhtai = ziczhtai;
    }

    public String getShlxsfhq() {
        return shlxsfhq;
    }

    public void setShlxsfhq(String shlxsfhq) {
        this.shlxsfhq = shlxsfhq;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    public String getDkjzcslx() {
        return dkjzcslx;
    }

    public void setDkjzcslx(String dkjzcslx) {
        this.dkjzcslx = dkjzcslx;
    }

    public String getJzhdkbzh() {
        return jzhdkbzh;
    }

    public void setJzhdkbzh(String jzhdkbzh) {
        this.jzhdkbzh = jzhdkbzh;
    }

    public String getJianzhrq() {
        return jianzhrq;
    }

    public void setJianzhrq(String jianzhrq) {
        this.jianzhrq = jianzhrq;
    }

    public BigDecimal getDaikbenj() {
        return daikbenj;
    }

    public void setDaikbenj(BigDecimal daikbenj) {
        this.daikbenj = daikbenj;
    }

    public BigDecimal getJianzhbj() {
        return jianzhbj;
    }

    public void setJianzhbj(BigDecimal jianzhbj) {
        this.jianzhbj = jianzhbj;
    }

    public BigDecimal getBnyjlixi() {
        return bnyjlixi;
    }

    public void setBnyjlixi(BigDecimal bnyjlixi) {
        this.bnyjlixi = bnyjlixi;
    }

    public BigDecimal getBnyslixi() {
        return bnyslixi;
    }

    public void setBnyslixi(BigDecimal bnyslixi) {
        this.bnyslixi = bnyslixi;
    }

    public BigDecimal getBwyjlixi() {
        return bwyjlixi;
    }

    public void setBwyjlixi(BigDecimal bwyjlixi) {
        this.bwyjlixi = bwyjlixi;
    }

    public BigDecimal getBwyslixi() {
        return bwyslixi;
    }

    public void setBwyslixi(BigDecimal bwyslixi) {
        this.bwyslixi = bwyslixi;
    }

    public BigDecimal getLxtiaozh() {
        return lxtiaozh;
    }

    public void setLxtiaozh(BigDecimal lxtiaozh) {
        this.lxtiaozh = lxtiaozh;
    }

    public BigDecimal getJzsdlxtz() {
        return jzsdlxtz;
    }

    public void setJzsdlxtz(BigDecimal jzsdlxtz) {
        this.jzsdlxtz = jzsdlxtz;
    }

    public BigDecimal getBobebili() {
        return bobebili;
    }

    public void setBobebili(BigDecimal bobebili) {
        this.bobebili = bobebili;
    }

    public BigDecimal getJianzssh() {
        return jianzssh;
    }

    public void setJianzssh(BigDecimal jianzssh) {
        this.jianzssh = jianzssh;
    }

    public BigDecimal getJianzzhb() {
        return jianzzhb;
    }

    public void setJianzzhb(BigDecimal jianzzhb) {
        this.jianzzhb = jianzzhb;
    }

    public String getJzhlxqsr() {
        return jzhlxqsr;
    }

    public void setJzhlxqsr(String jzhlxqsr) {
        this.jzhlxqsr = jzhlxqsr;
    }

    public BigDecimal getJzhlxjit() {
        return jzhlxjit;
    }

    public void setJzhlxjit(BigDecimal jzhlxjit) {
        this.jzhlxjit = jzhlxjit;
    }

    public BigDecimal getJzdklxhs() {
        return jzdklxhs;
    }

    public void setJzdklxhs(BigDecimal jzdklxhs) {
        this.jzdklxhs = jzdklxhs;
    }

    public String getScbbjtrq() {
        return scbbjtrq;
    }

    public void setScbbjtrq(String scbbjtrq) {
        this.scbbjtrq = scbbjtrq;
    }

    public String getYsxlyzhh() {
        return ysxlyzhh;
    }

    public void setYsxlyzhh(String ysxlyzhh) {
        this.ysxlyzhh = ysxlyzhh;
    }

    public String getYsxlyzxh() {
        return ysxlyzxh;
    }

    public void setYsxlyzxh(String ysxlyzxh) {
        this.ysxlyzxh = ysxlyzxh;
    }

    public String getLlfdcycf() {
        return llfdcycf;
    }

    public void setLlfdcycf(String llfdcycf) {
        this.llfdcycf = llfdcycf;
    }

    public String getLlfdcytz() {
        return llfdcytz;
    }

    public void setLlfdcytz(String llfdcytz) {
        this.llfdcytz = llfdcytz;
    }

    public Integer getLjsxqish() {
        return ljsxqish;
    }

    public void setLjsxqish(Integer ljsxqish) {
        this.ljsxqish = ljsxqish;
    }

    public BigDecimal getMqhbbili() {
        return mqhbbili;
    }

    public void setMqhbbili(BigDecimal mqhbbili) {
        this.mqhbbili = mqhbbili;
    }

    public String getJrlxsybz() {
        return jrlxsybz;
    }

    public void setJrlxsybz(String jrlxsybz) {
        this.jrlxsybz = jrlxsybz;
    }

    public String getYqzdzjbz() {
        return yqzdzjbz;
    }

    public void setYqzdzjbz(String yqzdzjbz) {
        this.yqzdzjbz = yqzdzjbz;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public String getBenjinfd() {
        return benjinfd;
    }

    public void setBenjinfd(String benjinfd) {
        this.benjinfd = benjinfd;
    }

    public List<Lstydkjjh> getLstydkjjh() {
        return lstydkjjh;
    }

    public void setLstydkjjh(List<Lstydkjjh> lstydkjjh) {
        this.lstydkjjh = lstydkjjh;
    }

    public List<Lstdkfkjh> getLstdkfkjh() {
        return lstdkfkjh;
    }

    public void setLstdkfkjh(List<Lstdkfkjh> lstdkfkjh) {
        this.lstdkfkjh = lstdkfkjh;
    }

    public List<Lstdkllfd> getLstdkllfd() {
        return lstdkllfd;
    }

    public void setLstdkllfd(List<Lstdkllfd> lstdkllfd) {
        this.lstdkllfd = lstdkllfd;
    }

    public List<Lstdktxzh> getLstdktxzh() {
        return lstdktxzh;
    }

    public void setLstdktxzh(List<Lstdktxzh> lstdktxzh) {
        this.lstdktxzh = lstdktxzh;
    }

    public List<Lstdkhbjh> getLstdkhbjh() {
        return lstdkhbjh;
    }

    public void setLstdkhbjh(List<Lstdkhbjh> lstdkhbjh) {
        this.lstdkhbjh = lstdkhbjh;
    }

    public List<Lstdkbjfd> getLstdkbjfd() {
        return lstdkbjfd;
    }

    public void setLstdkbjfd(List<Lstdkbjfd> lstdkbjfd) {
        this.lstdkbjfd = lstdkbjfd;
    }

    public List<Lstdkhkzh> getLstdkhkzh() {
        return lstdkhkzh;
    }

    public void setLstdkhkzh(List<Lstdkhkzh> lstdkhkzh) {
        this.lstdkhkzh = lstdkhkzh;
    }

    public List<Lstdkzhqg> getLstdkzhqg() {
        return lstdkzhqg;
    }

    public void setLstdkzhqg(List<Lstdkzhqg> lstdkzhqg) {
        this.lstdkzhqg = lstdkzhqg;
    }

    public List<Lstdkzhlm> getLstdkzhlm() {
        return lstdkzhlm;
    }

    public void setLstdkzhlm(List<Lstdkzhlm> lstdkzhlm) {
        this.lstdkzhlm = lstdkzhlm;
    }

    public List<Lstdkzhbz> getLstdkzhbz() {
        return lstdkzhbz;
    }

    public void setLstdkzhbz(List<Lstdkzhbz> lstdkzhbz) {
        this.lstdkzhbz = lstdkzhbz;
    }

    public List<Lstdklhmx> getLstdklhmxe() {
        return lstdklhmxe;
    }

    public void setLstdklhmxe(List<Lstdklhmx> lstdklhmxe) {
        this.lstdklhmxe = lstdklhmxe;
    }

    public List<Lstdksfsj> getLstdksfsj() {
        return lstdksfsj;
    }

    public void setLstdksfsj(List<Lstdksfsj> lstdksfsj) {
        this.lstdksfsj = lstdksfsj;
    }

    public List<Lstdkfwdj> getLstdkfwdj() {
        return lstdkfwdj;
    }

    public void setLstdkfwdj(List<Lstdkfwdj> lstdkfwdj) {
        this.lstdkfwdj = lstdkfwdj;
    }

    public List<Lstdkwtxx> getLstdkwtxxe() {
        return lstdkwtxxe;
    }

    public void setLstdkwtxxe(List<Lstdkwtxx> lstdkwtxxe) {
        this.lstdkwtxxe = lstdkwtxxe;
    }

    public List<Lstdkstzf> getLstdkstzf() {
        return lstdkstzf;
    }

    public void setLstdkstzf(List<Lstdkstzf> lstdkstzf) {
        this.lstdkstzf = lstdkstzf;
    }

    public List<Lstdzqgjh> getLstdzqgjh() {
        return lstdzqgjh;
    }

    public void setLstdzqgjh(List<Lstdzqgjh> lstdzqgjh) {
        this.lstdzqgjh = lstdzqgjh;
    }

    public List<Lstdknbgl> getLstdknbgl() {
        return lstdknbgl;
    }

    public void setLstdknbgl(List<Lstdknbgl> lstdknbgl) {
        this.lstdknbgl = lstdknbgl;
    }

    public List<Lstdkzhzy> getLstdkzhzie() {
        return lstdkzhzie;
    }

    public void setLstdkzhzie(List<Lstdkzhzy> lstdkzhzie) {
        this.lstdkzhzie = lstdkzhzie;
    }

    @Override
    public String toString() {
        return "Ln3100RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", zhwujgmc='" + zhwujgmc + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", duowtrbz='" + duowtrbz + '\'' +
                ", dlhesfsh='" + dlhesfsh + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", kuaijilb='" + kuaijilb + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", dkqixian='" + dkqixian + '\'' +
                ", dkrzhzhh='" + dkrzhzhh + '\'' +
                ", daikxtai='" + daikxtai + '\'' +
                ", yjfyjzht='" + yjfyjzht + '\'' +
                ", dbdkkksx=" + dbdkkksx +
                ", dkzhhzht='" + dkzhhzht + '\'' +
                ", zhcwjyrq='" + zhcwjyrq + '\'' +
                ", mingxixh=" + mingxixh +
                ", kaihujig='" + kaihujig + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", xiaohurq='" + xiaohurq + '\'' +
                ", xiaohugy='" + xiaohugy + '\'' +
                ", hetongje=" + hetongje +
                ", jiejuuje=" + jiejuuje +
                ", fafangje=" + fafangje +
                ", djiekfje=" + djiekfje +
                ", kffangje=" + kffangje +
                ", zhchbjin=" + zhchbjin +
                ", yuqibjin=" + yuqibjin +
                ", dzhibjin=" + dzhibjin +
                ", daizbjin=" + daizbjin +
                ", dkuanjij=" + dkuanjij +
                ", ysyjlixi=" + ysyjlixi +
                ", csyjlixi=" + csyjlixi +
                ", ysqianxi=" + ysqianxi +
                ", csqianxi=" + csqianxi +
                ", ysyjfaxi=" + ysyjfaxi +
                ", csyjfaxi=" + csyjfaxi +
                ", yshofaxi=" + yshofaxi +
                ", cshofaxi=" + cshofaxi +
                ", yingjifx=" + yingjifx +
                ", fuxiiiii=" + fuxiiiii +
                ", yingjitx=" + yingjitx +
                ", yingshtx=" + yingshtx +
                ", tiexfuli=" + tiexfuli +
                ", daitanlx=" + daitanlx +
                ", hexiaobj=" + hexiaobj +
                ", hexiaolx=" + hexiaolx +
                ", lixishru=" + lixishru +
                ", yingshfy=" + yingshfy +
                ", feiyshru=" + feiyshru +
                ", yingshfj=" + yingshfj +
                ", fjinshru=" + fjinshru +
                ", zhunbeij=" + zhunbeij +
                ", daikduix='" + daikduix + '\'' +
                ", yewufenl='" + yewufenl + '\'' +
                ", ysywbhao='" + ysywbhao + '\'' +
                ", ysywleix='" + ysywleix + '\'' +
                ", yjfyjhes='" + yjfyjhes + '\'' +
                ", yiyldhes='" + yiyldhes + '\'' +
                ", dkxtkmhs='" + dkxtkmhs + '\'' +
                ", zhqifkbz='" + zhqifkbz + '\'' +
                ", fkzhouqi='" + fkzhouqi + '\'' +
                ", fkfangsh='" + fkfangsh + '\'' +
                ", bencfkje=" + bencfkje +
                ", mcfkjebl=" + mcfkjebl +
                ", scfkriqi='" + scfkriqi + '\'' +
                ", jxhjhkkz='" + jxhjhkkz + '\'' +
                ", jxhjdkkz='" + jxhjdkkz + '\'' +
                ", hjiuleix='" + hjiuleix + '\'' +
                ", dzhifkjh='" + dzhifkjh + '\'' +
                ", fkzjclfs='" + fkzjclfs + '\'' +
                ", jixiguiz='" + jixiguiz + '\'' +
                ", ltjixigz='" + ltjixigz + '\'' +
                ", lilvleix='" + lilvleix + '\'' +
                ", whlilvzl='" + whlilvzl + '\'' +
                ", whllqxzl='" + whllqxzl + '\'' +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", zhchlilv=" + zhchlilv +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", lilvfdfs='" + lilvfdfs + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", zclilvbh='" + zclilvbh + '\'' +
                ", zaoqixbz='" + zaoqixbz + '\'' +
                ", wanqixbz='" + wanqixbz + '\'' +
                ", qixitshu=" + qixitshu +
                ", yushxfsh='" + yushxfsh + '\'' +
                ", yushxize=" + yushxize +
                ", lixitxzq='" + lixitxzq + '\'' +
                ", meictxfs='" + meictxfs + '\'' +
                ", meictxbl=" + meictxbl +
                ", xiacitxr='" + xiacitxr + '\'' +
                ", kxqjjrgz='" + kxqjjrgz + '\'' +
                ", kxqzyqgz='" + kxqzyqgz + '\'' +
                ", kxqshxgz='" + kxqshxgz + '\'' +
                ", jixibzhi='" + jixibzhi + '\'' +
                ", jfxibzhi='" + jfxibzhi + '\'' +
                ", fxjxbzhi='" + fxjxbzhi + '\'' +
                ", yuqillbh='" + yuqillbh + '\'' +
                ", yqfxfdzh=" + yqfxfdzh +
                ", yqfxfdfs='" + yqfxfdfs + '\'' +
                ", yuqinyll='" + yuqinyll + '\'' +
                ", yuqililv=" + yuqililv +
                ", sctxriqi='" + sctxriqi + '\'' +
                ", xctxriqi='" + xctxriqi + '\'' +
                ", scyqtxrq='" + scyqtxrq + '\'' +
                ", xcyqtxrq='" + xcyqtxrq + '\'' +
                ", scfxtxrq='" + scfxtxrq + '\'' +
                ", xcfxtxrq='" + xcfxtxrq + '\'' +
                ", scnytxrq='" + scnytxrq + '\'' +
                ", xcnytxrq='" + xcnytxrq + '\'' +
                ", scjixirq='" + scjixirq + '\'' +
                ", jiximxxh=" + jiximxxh +
                ", lilvfend='" + lilvfend + '\'' +
                ", tiexibzh='" + tiexibzh + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", dechligz='" + dechligz + '\'' +
                ", leijinzh=" + leijinzh +
                ", leijqjsh=" + leijqjsh +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", hezuofbh='" + hezuofbh + '\'' +
                ", hezuofmc='" + hezuofmc + '\'' +
                ", duophkbz='" + duophkbz + '\'' +
                ", huanbzhq='" + huanbzhq + '\'' +
                ", shunylix=" + shunylix +
                ", yuqhkzhq='" + yuqhkzhq + '\'' +
                ", schkriqi='" + schkriqi + '\'' +
                ", xchkriqi='" + xchkriqi + '\'' +
                ", hkqixian='" + hkqixian + '\'' +
                ", baoliuje=" + baoliuje +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", zongqish=" + zongqish +
                ", benqqish=" + benqqish +
                ", benqizqs=" + benqizqs +
                ", jixizqsh=" + jixizqsh +
                ", jixiqish=" + jixiqish +
                ", jixibenj=" + jixibenj +
                ", danqbjds=" + danqbjds +
                ", hkshxubh='" + hkshxubh + '\'' +
                ", yunxtqhk='" + yunxtqhk + '\'' +
                ", qxbgtzjh='" + qxbgtzjh + '\'' +
                ", llbgtzjh='" + llbgtzjh + '\'' +
                ", dcfktzjh='" + dcfktzjh + '\'' +
                ", tqhktzjh='" + tqhktzjh + '\'' +
                ", zdkoukbz='" + zdkoukbz + '\'' +
                ", zdplkkbz='" + zdplkkbz + '\'' +
                ", zdjqdkbz='" + zdjqdkbz + '\'' +
                ", qyxhdkbz='" + qyxhdkbz + '\'' +
                ", xhdkqyzh='" + xhdkqyzh + '\'' +
                ", dhkzhhbz='" + dhkzhhbz + '\'' +
                ", huankzhh='" + huankzhh + '\'' +
                ", dzhhkjih='" + dzhhkjih + '\'' +
                ", yunxdkzq='" + yunxdkzq + '\'' +
                ", zhqxzekk='" + zhqxzekk + '\'' +
                ", zhqzdcsh=" + zhqzdcsh +
                ", zqgzbhao='" + zqgzbhao + '\'' +
                ", yizhqcsh=" + yizhqcsh +
                ", zhanqixh=" + zhanqixh +
                ", zbjrsygz='" + zbjrsygz + '\'' +
                ", zidxtzhy='" + zidxtzhy + '\'' +
                ", lixizcgz='" + lixizcgz + '\'' +
                ", lixizhgz='" + lixizhgz + '\'' +
                ", tqhkfjbh='" + tqhkfjbh + '\'' +
                ", tqhkfjmc='" + tqhkfjmc + '\'' +
                ", tqhkfjfj=" + tqhkfjfj +
                ", dlxxzdgz='" + dlxxzdgz + '\'' +
                ", dlxxqzgz='" + dlxxqzgz + '\'' +
                ", wtckywbm='" + wtckywbm + '\'' +
                ", dailixuh=" + dailixuh +
                ", dailimsh='" + dailimsh + '\'' +
                ", bjghrzzh='" + bjghrzzh + '\'' +
                ", lxghrzzh='" + lxghrzzh + '\'' +
                ", bjghrzxh='" + bjghrzxh + '\'' +
                ", lxghrzxh='" + lxghrzxh + '\'' +
                ", shynedbz='" + shynedbz + '\'' +
                ", edbizhgz='" + edbizhgz + '\'' +
                ", edzdbizh='" + edzdbizh + '\'' +
                ", edshyngz='" + edshyngz + '\'' +
                ", sydkcnuo='" + sydkcnuo + '\'' +
                ", cndkjjho='" + cndkjjho + '\'' +
                ", zhjzhyfs='" + zhjzhyfs + '\'' +
                ", dkgljgou='" + dkgljgou + '\'' +
                ", gljgleib='" + gljgleib + '\'' +
                ", fuhejgou='" + fuhejgou + '\'' +
                ", wujiflbz='" + wujiflbz + '\'' +
                ", wujiflrq='" + wujiflrq + '\'' +
                ", lmdkbzhi='" + lmdkbzhi + '\'' +
                ", leijfkje=" + leijfkje +
                ", snljfkje=" + snljfkje +
                ", bnljfkje=" + bnljfkje +
                ", zuigdkye=" + zuigdkye +
                ", snzgzhye=" + snzgzhye +
                ", bnzgzhye=" + bnzgzhye +
                ", leijyhbj=" + leijyhbj +
                ", snljyhbj=" + snljyhbj +
                ", bnljyhbj=" + bnljyhbj +
                ", yishhxbj=" + yishhxbj +
                ", yishhxlx=" + yishhxlx +
                ", leijyhlx=" + leijyhlx +
                ", snljyhlx=" + snljyhlx +
                ", bnljyhlx=" + bnljyhlx +
                ", syljyhlx=" + syljyhlx +
                ", byljyhlx=" + byljyhlx +
                ", leijcslx=" + leijcslx +
                ", snljcslx=" + snljcslx +
                ", bnljcslx=" + bnljcslx +
                ", byljxxsh=" + byljxxsh +
                ", zhchtqtz='" + zhchtqtz + '\'' +
                ", tzhtqtsh=" + tzhtqtsh +
                ", yqcshtzh='" + yqcshtzh + '\'' +
                ", tzhjgtsh=" + tzhjgtsh +
                ", lilvbgtz='" + lilvbgtz + '\'' +
                ", yuebgtzh='" + yuebgtzh + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", bzhrdbbz='" + bzhrdbbz + '\'' +
                ", benjheji=" + benjheji +
                ", lixiheji=" + lixiheji +
                ", lilvqixx='" + lilvqixx + '\'' +
                ", dkrzhzxh='" + dkrzhzxh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", yewusx01='" + yewusx01 + '\'' +
                ", yewusx02='" + yewusx02 + '\'' +
                ", yewusx03='" + yewusx03 + '\'' +
                ", yewusx04='" + yewusx04 + '\'' +
                ", yewusx05='" + yewusx05 + '\'' +
                ", yewusx06='" + yewusx06 + '\'' +
                ", yewusx07='" + yewusx07 + '\'' +
                ", yewusx08='" + yewusx08 + '\'' +
                ", yewusx09='" + yewusx09 + '\'' +
                ", yewusx10='" + yewusx10 + '\'' +
                ", hetongll=" + hetongll +
                ", sfpinlvv='" + sfpinlvv + '\'' +
                ", sfzhouqi='" + sfzhouqi + '\'' +
                ", shoufzhl='" + shoufzhl + '\'' +
                ", txzhouqi='" + txzhouqi + '\'' +
                ", shoufdma='" + shoufdma + '\'' +
                ", shfdmamc='" + shfdmamc + '\'' +
                ", shoufjee=" + shoufjee +
                ", fufeizhh='" + fufeizhh + '\'' +
                ", ffzhhzxh='" + ffzhhzxh + '\'' +
                ", ffzhhmch='" + ffzhhmch + '\'' +
                ", sfrzhzhh='" + sfrzhzhh + '\'' +
                ", sfrzhzxh='" + sfrzhzxh + '\'' +
                ", mqjqsfbz='" + mqjqsfbz + '\'' +
                ", fybkdqbz='" + fybkdqbz + '\'' +
                ", sfljsfei='" + sfljsfei + '\'' +
                ", ywsxms01='" + ywsxms01 + '\'' +
                ", ywsxms02='" + ywsxms02 + '\'' +
                ", ywsxms03='" + ywsxms03 + '\'' +
                ", ywsxms04='" + ywsxms04 + '\'' +
                ", ywsxms05='" + ywsxms05 + '\'' +
                ", ywsxms06='" + ywsxms06 + '\'' +
                ", ywsxms07='" + ywsxms07 + '\'' +
                ", ywsxms08='" + ywsxms08 + '\'' +
                ", ywsxms09='" + ywsxms09 + '\'' +
                ", ywsxms10='" + ywsxms10 + '\'' +
                ", hesuanfs='" + hesuanfs + '\'' +
                ", kxqzdcsh=" + kxqzdcsh +
                ", yxtsfkbz='" + yxtsfkbz + '\'' +
                ", zdfkjjms='" + zdfkjjms + '\'' +
                ", xhdkzhxh='" + xhdkzhxh + '\'' +
                ", shfleixi='" + shfleixi + '\'' +
                ", pingzhzl='" + pingzhzl + '\'' +
                ", khjingli='" + khjingli + '\'' +
                ", pingzhma='" + pingzhma + '\'' +
                ", yinhshje=" + yinhshje +
                ", yinhshlv=" + yinhshlv +
                ", chanpzdm='" + chanpzdm + '\'' +
                ", chanpzmc='" + chanpzmc + '\'' +
                ", wtrckuzh='" + wtrckuzh + '\'' +
                ", wtrckzxh='" + wtrckzxh + '\'' +
                ", wtckzhao='" + wtckzhao + '\'' +
                ", wtckzixh='" + wtckzixh + '\'' +
                ", daikdxxf='" + daikdxxf + '\'' +
                ", bwchapbz='" + bwchapbz + '\'' +
                ", xunhdaik='" + xunhdaik + '\'' +
                ", yansdaik='" + yansdaik + '\'' +
                ", chendaik='" + chendaik + '\'' +
                ", butidaik='" + butidaik + '\'' +
                ", yintdkbz='" + yintdkbz + '\'' +
                ", yintdkfs='" + yintdkfs + '\'' +
                ", yintleib='" + yintleib + '\'' +
                ", yintnbcy='" + yintnbcy + '\'' +
                ", yintwbcy='" + yintwbcy + '\'' +
                ", jiangulx='" + jiangulx + '\'' +
                ", zhqizcqx='" + zhqizcqx + '\'' +
                ", fangkulx='" + fangkulx + '\'' +
                ", hntmifku='" + hntmifku + '\'' +
                ", hnftmfku='" + hnftmfku + '\'' +
                ", neibufku='" + neibufku + '\'' +
                ", jixibjgz='" + jixibjgz + '\'' +
                ", lixijsff='" + lixijsff + '\'' +
                ", jixitwgz='" + jixitwgz + '\'' +
                ", jixizxje=" + jixizxje +
                ", jixisrgz='" + jixisrgz + '\'' +
                ", srzxdanw='" + srzxdanw + '\'' +
                ", llqxkdfs='" + llqxkdfs + '\'' +
                ", fdjixibz='" + fdjixibz + '\'' +
                ", yuqitzfs='" + yuqitzfs + '\'' +
                ", yuqitzzq='" + yuqitzzq + '\'' +
                ", fulilvbh='" + fulilvbh + '\'' +
                ", fulilvny='" + fulilvny + '\'' +
                ", fulililv=" + fulililv +
                ", fulitzfs='" + fulitzfs + '\'' +
                ", fulitzzq='" + fulitzzq + '\'' +
                ", fulifdfs='" + fulifdfs + '\'' +
                ", fulifdzh=" + fulifdzh +
                ", jitiguiz='" + jitiguiz + '\'' +
                ", zqxizdds=" + zqxizdds +
                ", wqxizdds=" + wqxizdds +
                ", butijejs='" + butijejs + '\'' +
                ", tiaozhkf='" + tiaozhkf + '\'' +
                ", scihkrbz='" + scihkrbz + '\'' +
                ", mqihkfsh='" + mqihkfsh + '\'' +
                ", yunxsuoq='" + yunxsuoq + '\'' +
                ", suoqcish=" + suoqcish +
                ", bzuekkfs='" + bzuekkfs + '\'' +
                ", yqbzkkfs='" + yqbzkkfs + '\'' +
                ", hntmihku='" + hntmihku + '\'' +
                ", hnftmhku='" + hnftmhku + '\'' +
                ", nbuzhhku='" + nbuzhhku + '\'' +
                ", tiqhksdq=" + tiqhksdq +
                ", sfyxkuxq='" + sfyxkuxq + '\'' +
                ", kuanxqts=" + kuanxqts +
                ", kxqjixgz='" + kxqjixgz + '\'' +
                ", kxqhjxgz='" + kxqhjxgz + '\'' +
                ", ysfyywbm='" + ysfyywbm + '\'' +
                ", ysfjywbm='" + ysfjywbm + '\'' +
                ", yffyywbm='" + yffyywbm + '\'' +
                ", yffjywbm='" + yffjywbm + '\'' +
                ", dfbjywbm='" + dfbjywbm + '\'' +
                ", dflxywbm='" + dflxywbm + '\'' +
                ", yywsywbm='" + yywsywbm + '\'' +
                ", yywzywbm='" + yywzywbm + '\'' +
                ", bjghywbm='" + bjghywbm + '\'' +
                ", lxghywbm='" + lxghywbm + '\'' +
                ", yqllcklx='" + yqllcklx + '\'' +
                ", flllcklx='" + flllcklx + '\'' +
                ", qglxleix='" + qglxleix + '\'' +
                ", qigscfsh='" + qigscfsh + '\'' +
                ", dktiansh=" + dktiansh +
                ", yijtdklx=" + yijtdklx +
                ", yihxbjlx=" + yihxbjlx +
                ", kshchpdm='" + kshchpdm + '\'' +
                ", kshchpmc='" + kshchpmc + '\'' +
                ", dkczhzhh='" + dkczhzhh + '\'' +
                ", dkdbfshi='" + dkdbfshi + '\'' +
                ", dkyongtu='" + dkyongtu + '\'' +
                ", jiejuxzh='" + jiejuxzh + '\'' +
                ", fkjzhfsh='" + fkjzhfsh + '\'' +
                ", shtzfhxm='" + shtzfhxm + '\'' +
                ", bwhesdma='" + bwhesdma + '\'' +
                ", ziczhtai='" + ziczhtai + '\'' +
                ", shlxsfhq='" + shlxsfhq + '\'' +
                ", wtrmingc='" + wtrmingc + '\'' +
                ", dkjzcslx='" + dkjzcslx + '\'' +
                ", jzhdkbzh='" + jzhdkbzh + '\'' +
                ", jianzhrq='" + jianzhrq + '\'' +
                ", daikbenj=" + daikbenj +
                ", jianzhbj=" + jianzhbj +
                ", bnyjlixi=" + bnyjlixi +
                ", bnyslixi=" + bnyslixi +
                ", bwyjlixi=" + bwyjlixi +
                ", bwyslixi=" + bwyslixi +
                ", lxtiaozh=" + lxtiaozh +
                ", jzsdlxtz=" + jzsdlxtz +
                ", bobebili=" + bobebili +
                ", jianzssh=" + jianzssh +
                ", jianzzhb=" + jianzzhb +
                ", jzhlxqsr='" + jzhlxqsr + '\'' +
                ", jzhlxjit=" + jzhlxjit +
                ", jzdklxhs=" + jzdklxhs +
                ", scbbjtrq='" + scbbjtrq + '\'' +
                ", ysxlyzhh='" + ysxlyzhh + '\'' +
                ", ysxlyzxh='" + ysxlyzxh + '\'' +
                ", llfdcycf='" + llfdcycf + '\'' +
                ", llfdcytz='" + llfdcytz + '\'' +
                ", ljsxqish=" + ljsxqish +
                ", mqhbbili=" + mqhbbili +
                ", jrlxsybz='" + jrlxsybz + '\'' +
                ", yqzdzjbz='" + yqzdzjbz + '\'' +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", benjinfd='" + benjinfd + '\'' +
                ", lstydkjjh=" + lstydkjjh +
                ", lstdkfkjh=" + lstdkfkjh +
                ", lstdkllfd=" + lstdkllfd +
                ", lstdktxzh=" + lstdktxzh +
                ", lstdkhbjh=" + lstdkhbjh +
                ", lstdkbjfd=" + lstdkbjfd +
                ", lstdkhkzh=" + lstdkhkzh +
                ", lstdkzhqg=" + lstdkzhqg +
                ", lstdkzhlm=" + lstdkzhlm +
                ", lstdkzhbz=" + lstdkzhbz +
                ", lstdklhmxe=" + lstdklhmxe +
                ", lstdksfsj=" + lstdksfsj +
                ", lstdkfwdj=" + lstdkfwdj +
                ", lstdkwtxxe=" + lstdkwtxxe +
                ", lstdkstzf=" + lstdkstzf +
                ", lstdzqgjh=" + lstdzqgjh +
                ", lstdknbgl=" + lstdknbgl +
                ", lstdkzhzie=" + lstdkzhzie +
                '}';
    }
}
