package cn.com.yusys.yusp.dto.client.esb.irs.irs18;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：评级主办权更新
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs18ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custid")
    private String custid;//客户编号
    @JsonProperty(value = "userid1")
    private String userid1;//管户人编号
    @JsonProperty(value = "username")
    private String username;//管户人名称
    @JsonProperty(value = "orgid")
    private String orgid;//管户机构编号
    @JsonProperty(value = "orgname")
    private String orgname;//管户机构名称

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getUserid1() {
        return userid1;
    }

    public void setUserid1(String userid1) {
        this.userid1 = userid1;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    @Override
    public String toString() {
        return "Irs18ReqDto{" +
                "custid='" + custid + '\'' +
                "userid1='" + userid1 + '\'' +
                "username='" + username + '\'' +
                "orgid='" + orgid + '\'' +
                "orgname='" + orgname + '\'' +
                '}';
    }
}  
