package cn.com.yusys.yusp.dto.server.biz.xdxw0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户调查撤销
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "serno")
    private String serno;//流水号

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}


	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	@Override
	public String toString() {
		return "Data{" +
				"cusName='" + cusName + '\'' +
				", cusId='" + cusId + '\'' +
				", serno='" + serno + '\'' +
				'}';
	}
}
