package cn.com.yusys.yusp.dto.client.esb.core.ln3032.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：受托支付信息维护
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3032ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dkkhczbz")
    private String dkkhczbz;//业务操作方式
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "chuzhhao")
    private String chuzhhao;//出账号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "fuhejgou")
    private String fuhejgou;//复核机构
    @JsonProperty(value = "brchno")
    private String brchno;//    部门号,取账务机构号
    @JsonProperty(value = "lstStzf")
    private java.util.List<LstStzf> lstStzf;//受托支付信息


    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getChuzhhao() {
        return chuzhhao;
    }

    public void setChuzhhao(String chuzhhao) {
        this.chuzhhao = chuzhhao;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public List<LstStzf> getLstStzf() {
        return lstStzf;
    }

    public void setLstStzf(List<LstStzf> lstStzf) {
        this.lstStzf = lstStzf;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    @Override
    public String toString() {
        return "Ln3032ReqDto{" +
                "dkkhczbz='" + dkkhczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", chuzhhao='" + chuzhhao + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", fuhejgou='" + fuhejgou + '\'' +
                ", brchno='" + brchno + '\'' +
                ", lstStzf=" + lstStzf +
                '}';
    }
}
