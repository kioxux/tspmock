package cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：市民贷优惠券核销锁定
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Yx0003RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "guarno")
    private String guarno;//抵押物编号
    @JsonProperty(value = "yxzjls")
    private String yxzjls;//影像主键流水号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    public String getYxzjls() {
        return yxzjls;
    }

    public void setYxzjls(String yxzjls) {
        this.yxzjls = yxzjls;
    }

    @Override
    public String toString() {
        return "Yx0003RespDto{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "guarno='" + guarno + '\'' +
                "yxzjls='" + yxzjls + '\'' +
                '}';
    }
}  
