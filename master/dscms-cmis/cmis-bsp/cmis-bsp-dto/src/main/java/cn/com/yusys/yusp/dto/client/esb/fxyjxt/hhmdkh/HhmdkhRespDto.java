package cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询客户是否为黑灰名单客户
 */
@JsonPropertyOrder(alphabetic = true)
public class HhmdkhRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ifBlack")
    private String ifBlack;//是否黑名单
    @JsonProperty(value = "ifGrey")
    private String ifGrey;//是否灰名单

    public String getIfBlack() {
        return ifBlack;
    }

    public void setIfBlack(String ifBlack) {
        this.ifBlack = ifBlack;
    }

    public String getIfGrey() {
        return ifGrey;
    }

    public void setIfGrey(String ifGrey) {
        this.ifGrey = ifGrey;
    }

    @Override
    public String toString() {
        return "HhmdkhRespDto{" +
                "ifBlack='" + ifBlack + '\'' +
                ", ifGrey='" + ifGrey + '\'' +
                '}';
    }
}
