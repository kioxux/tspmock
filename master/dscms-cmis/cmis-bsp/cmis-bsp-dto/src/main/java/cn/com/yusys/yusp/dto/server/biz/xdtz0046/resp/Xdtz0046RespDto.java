package cn.com.yusys.yusp.dto.server.biz.xdtz0046.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据借据号获取共同借款人信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0046RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdtz0046RespDto{" +
                "data=" + data +
                '}';
    }
}
