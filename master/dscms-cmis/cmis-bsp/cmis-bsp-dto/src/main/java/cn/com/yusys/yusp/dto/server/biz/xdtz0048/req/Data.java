package cn.com.yusys.yusp.dto.server.biz.xdtz0048.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：小贷借款借据文本生成pdf
 *
 * @author zoubiao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "loanAmt")
    private String loanAmt;//放款金额
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//借款用途
    @JsonProperty(value = "yearRate")
    private String yearRate;//年利率
    @JsonProperty(value = "payType")
    private String payType;//支付方式
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//借款起始日期
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//借款到期日期
    @JsonProperty(value = "loanAcctNo")
    private String loanAcctNo;//放款账号
    @JsonProperty(value = "huser")
    private String huser;//经办人
    @JsonProperty(value = "applyDate")
    private String applyDate;//申请日期
    @JsonProperty(value = "isdzqy")
    private String isdzqy;//是否电子签约:1-是 0-否

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(String loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getYearRate() {
        return yearRate;
    }

    public void setYearRate(String yearRate) {
        this.yearRate = yearRate;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanAcctNo() {
        return loanAcctNo;
    }

    public void setLoanAcctNo(String loanAcctNo) {
        this.loanAcctNo = loanAcctNo;
    }

    public String getHuser() {
        return huser;
    }

    public void setHuser(String huser) {
        this.huser = huser;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getIsdzqy() {
        return isdzqy;
    }

    public void setIsdzqy(String isdzqy) {
        this.isdzqy = isdzqy;
    }

    @Override
    public String toString() {
        return "Xdtz0048ReqDto{" +
                "cusName='" + cusName + '\'' +
                "contNo='" + contNo + '\'' +
                "cnContNo='" + cnContNo + '\'' +
                "billNo='" + billNo + '\'' +
                "loanAmt='" + loanAmt + '\'' +
                "curType='" + curType + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "yearRate='" + yearRate + '\'' +
                "payType='" + payType + '\'' +
                "loanStartDate='" + loanStartDate + '\'' +
                "loanEndDate='" + loanEndDate + '\'' +
                "loanAcctNo='" + loanAcctNo + '\'' +
                "huser='" + huser + '\'' +
                "applyDate='" + applyDate + '\'' +
                "isdzqy='" + isdzqy + '\'' +
                '}';
    }
}
