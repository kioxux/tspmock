package cn.com.yusys.yusp.dto.client.esb.core.co3222;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：抵质押物单笔查询
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Co3222RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzywbhao")
    private String dzywbhao;//抵质押物编号
    @JsonProperty(value = "dzywminc")
    private String dzywminc;//抵质押物名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "dzywzlei")
    private String dzywzlei;//抵质押物种类
    @JsonProperty(value = "dizyfshi")
    private String dizyfshi;//抵质押方式
    @JsonProperty(value = "huowuhth")
    private String huowuhth;//货物合同号
    @JsonProperty(value = "yewmingc")
    private String yewmingc;//业务名称
    @JsonProperty(value = "syrkhhao")
    private String syrkhhao;//受益人客户号
    @JsonProperty(value = "syrkhmin")
    private String syrkhmin;//受益人客户名
    @JsonProperty(value = "syqrkehh")
    private String syqrkehh;//所有权人客户号
    @JsonProperty(value = "syqrkehm")
    private String syqrkehm;//所有权人客户名
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "minyjiaz")
    private BigDecimal minyjiaz;//名义价值
    @JsonProperty(value = "shijjiaz")
    private BigDecimal shijjiaz;//实际价值
    @JsonProperty(value = "pingjiaz")
    private BigDecimal pingjiaz;//评估价值
    @JsonProperty(value = "dizybilv")
    private BigDecimal dizybilv;//抵质押比率
    @JsonProperty(value = "keyongje")
    private BigDecimal keyongje;//可用金额
    @JsonProperty(value = "yiyongje")
    private BigDecimal yiyongje;//已用金额
    @JsonProperty(value = "shengxrq")
    private String shengxrq;//生效日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dzywztai")
    private String dzywztai;//抵质押物状态
    @JsonProperty(value = "glywbhao")
    private String glywbhao;//关联业务编号
    @JsonProperty(value = "mingxixh")
    private Long mingxixh;//明细序号
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "kaihguiy")
    private String kaihguiy;//开户柜员
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "yewusx01")
    private String yewusx01;//记账余额属性
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要
    @JsonProperty(value = "lstnum1")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.co3222.Lstnum1> lstnum1;//贷款账户质押控制表
    @JsonProperty(value = "lstnum2")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.co3222.Lstnum2> lstnum2;//抵质押物担保关联

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDzywminc() {
        return dzywminc;
    }

    public void setDzywminc(String dzywminc) {
        this.dzywminc = dzywminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getDzywzlei() {
        return dzywzlei;
    }

    public void setDzywzlei(String dzywzlei) {
        this.dzywzlei = dzywzlei;
    }

    public String getDizyfshi() {
        return dizyfshi;
    }

    public void setDizyfshi(String dizyfshi) {
        this.dizyfshi = dizyfshi;
    }

    public String getHuowuhth() {
        return huowuhth;
    }

    public void setHuowuhth(String huowuhth) {
        this.huowuhth = huowuhth;
    }

    public String getYewmingc() {
        return yewmingc;
    }

    public void setYewmingc(String yewmingc) {
        this.yewmingc = yewmingc;
    }

    public String getSyrkhhao() {
        return syrkhhao;
    }

    public void setSyrkhhao(String syrkhhao) {
        this.syrkhhao = syrkhhao;
    }

    public String getSyrkhmin() {
        return syrkhmin;
    }

    public void setSyrkhmin(String syrkhmin) {
        this.syrkhmin = syrkhmin;
    }

    public String getSyqrkehh() {
        return syqrkehh;
    }

    public void setSyqrkehh(String syqrkehh) {
        this.syqrkehh = syqrkehh;
    }

    public String getSyqrkehm() {
        return syqrkehm;
    }

    public void setSyqrkehm(String syqrkehm) {
        this.syqrkehm = syqrkehm;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getMinyjiaz() {
        return minyjiaz;
    }

    public void setMinyjiaz(BigDecimal minyjiaz) {
        this.minyjiaz = minyjiaz;
    }

    public BigDecimal getShijjiaz() {
        return shijjiaz;
    }

    public void setShijjiaz(BigDecimal shijjiaz) {
        this.shijjiaz = shijjiaz;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getDizybilv() {
        return dizybilv;
    }

    public void setDizybilv(BigDecimal dizybilv) {
        this.dizybilv = dizybilv;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public BigDecimal getYiyongje() {
        return yiyongje;
    }

    public void setYiyongje(BigDecimal yiyongje) {
        this.yiyongje = yiyongje;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDzywztai() {
        return dzywztai;
    }

    public void setDzywztai(String dzywztai) {
        this.dzywztai = dzywztai;
    }

    public String getGlywbhao() {
        return glywbhao;
    }

    public void setGlywbhao(String glywbhao) {
        this.glywbhao = glywbhao;
    }

    public Long getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Long mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public List<Lstnum1> getLstnum1() {
        return lstnum1;
    }

    public void setLstnum1(List<Lstnum1> lstnum1) {
        this.lstnum1 = lstnum1;
    }

    public List<Lstnum2> getLstnum2() {
        return lstnum2;
    }

    public void setLstnum2(List<Lstnum2> lstnum2) {
        this.lstnum2 = lstnum2;
    }

    @Override
    public String toString() {
        return "Co3222RespDto{" +
                "dzywbhao='" + dzywbhao + '\'' +
                ", dzywminc='" + dzywminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", dzywzlei='" + dzywzlei + '\'' +
                ", dizyfshi='" + dizyfshi + '\'' +
                ", huowuhth='" + huowuhth + '\'' +
                ", yewmingc='" + yewmingc + '\'' +
                ", syrkhhao='" + syrkhhao + '\'' +
                ", syrkhmin='" + syrkhmin + '\'' +
                ", syqrkehh='" + syqrkehh + '\'' +
                ", syqrkehm='" + syqrkehm + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", minyjiaz=" + minyjiaz +
                ", shijjiaz=" + shijjiaz +
                ", pingjiaz=" + pingjiaz +
                ", dizybilv=" + dizybilv +
                ", keyongje=" + keyongje +
                ", yiyongje=" + yiyongje +
                ", shengxrq='" + shengxrq + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", dzywztai='" + dzywztai + '\'' +
                ", glywbhao='" + glywbhao + '\'' +
                ", mingxixh=" + mingxixh +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", yewusx01='" + yewusx01 + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                ", lstnum1=" + lstnum1 +
                ", lstnum2=" + lstnum2 +
                '}';
    }
}
