package cn.com.yusys.yusp.dto.client.esb.core.ln3065;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：资产转让资金划转
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3065ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkkhczbz")
    private String dkkhczbz;//开户操作标志
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "dhfkywlx")
    private String dhfkywlx;//业务类型
    @JsonProperty(value = "zrfukzhh")
    private String zrfukzhh;//对外付款账号
    @JsonProperty(value = "zrfukzxh")
    private String zrfukzxh;//对外付款账号子序号
    @JsonProperty(value = "bcfkjine")
    private BigDecimal bcfkjine;//本次付款金额
    @JsonProperty(value = "fukuanrq")
    private String fukuanrq;//付款日期
    @JsonProperty(value = "zrfukzht")
    private String zrfukzht;//转让付款状态
    @JsonProperty(value = "huazhfax")
    private String huazhfax;//资金划转方向
    @JsonProperty(value = "zhanghye")
    private BigDecimal zhanghye;//账户余额
    @JsonProperty(value = "yueeeeee")
    private BigDecimal yueeeeee;//余额
    @JsonProperty(value = "benjinje")
    private BigDecimal benjinje;//本金金额
    @JsonProperty(value = "lixijine")
    private BigDecimal lixijine;//利息金额
    @JsonProperty(value = "jydszhmc")
    private String jydszhmc;//交易对手账户名称
    @JsonProperty(value = "jydsleix")
    private String jydsleix;//交易对手类型
    @JsonProperty(value = "jydshmch")
    private String jydshmch;//交易对手名称
    @JsonProperty(value = "zhkaihhh")
    private String zhkaihhh;//账户开户行行号
    @JsonProperty(value = "zhkaihhm")
    private String zhkaihhm;//账户开户行行名
    @JsonProperty(value = "jiaoyije")
    private BigDecimal jiaoyije;//交易金额
    @JsonProperty(value = "sunyrzzh")
    private String sunyrzzh;//损益入账账号
    @JsonProperty(value = "syrzzhxh")
    private String syrzzhxh;//损益入账账号子序号
    @JsonProperty(value = "bjyskzhh")
    private String bjyskzhh;//其他应收款账号(本金)
    @JsonProperty(value = "qyskzhzh")
    private String qyskzhzh;//其他应收款账号子序号(本金)
    @JsonProperty(value = "lxyskzhh")
    private String lxyskzhh;//其他应收款账号(利息)
    @JsonProperty(value = "lxyskzxh")
    private String lxyskzxh;//其他应收款账号子序号(利息)
    @JsonProperty(value = "bjysfzhh")
    private String bjysfzhh;//其他应付款账号(本金)
    @JsonProperty(value = "bjysfzxh")
    private String bjysfzxh;//其他应付款账号子序号(本金)
    @JsonProperty(value = "lxysfzhh")
    private String lxysfzhh;//其他应付款账号(利息)
    @JsonProperty(value = "qtyfzhzx")
    private String qtyfzhzx;//其他应付款账号子序号(利息)
    @JsonProperty(value = "jydszhao")
    private String jydszhao;//交易对手账号
    @JsonProperty(value = "jydszhzh")
    private String jydszhzh;//交易对手账号子序号
    @JsonProperty(value = "nbuzhhao")
    private String nbuzhhao;//内部账号
    @JsonProperty(value = "nbuzhzxh")
    private String nbuzhzxh;//内部账号子序号

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getDhfkywlx() {
        return dhfkywlx;
    }

    public void setDhfkywlx(String dhfkywlx) {
        this.dhfkywlx = dhfkywlx;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    public BigDecimal getBcfkjine() {
        return bcfkjine;
    }

    public void setBcfkjine(BigDecimal bcfkjine) {
        this.bcfkjine = bcfkjine;
    }

    public String getFukuanrq() {
        return fukuanrq;
    }

    public void setFukuanrq(String fukuanrq) {
        this.fukuanrq = fukuanrq;
    }

    public String getZrfukzht() {
        return zrfukzht;
    }

    public void setZrfukzht(String zrfukzht) {
        this.zrfukzht = zrfukzht;
    }

    public String getHuazhfax() {
        return huazhfax;
    }

    public void setHuazhfax(String huazhfax) {
        this.huazhfax = huazhfax;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public BigDecimal getYueeeeee() {
        return yueeeeee;
    }

    public void setYueeeeee(BigDecimal yueeeeee) {
        this.yueeeeee = yueeeeee;
    }

    public BigDecimal getBenjinje() {
        return benjinje;
    }

    public void setBenjinje(BigDecimal benjinje) {
        this.benjinje = benjinje;
    }

    public BigDecimal getLixijine() {
        return lixijine;
    }

    public void setLixijine(BigDecimal lixijine) {
        this.lixijine = lixijine;
    }

    public String getJydszhmc() {
        return jydszhmc;
    }

    public void setJydszhmc(String jydszhmc) {
        this.jydszhmc = jydszhmc;
    }

    public String getJydsleix() {
        return jydsleix;
    }

    public void setJydsleix(String jydsleix) {
        this.jydsleix = jydsleix;
    }

    public String getJydshmch() {
        return jydshmch;
    }

    public void setJydshmch(String jydshmch) {
        this.jydshmch = jydshmch;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getSunyrzzh() {
        return sunyrzzh;
    }

    public void setSunyrzzh(String sunyrzzh) {
        this.sunyrzzh = sunyrzzh;
    }

    public String getSyrzzhxh() {
        return syrzzhxh;
    }

    public void setSyrzzhxh(String syrzzhxh) {
        this.syrzzhxh = syrzzhxh;
    }

    public String getBjyskzhh() {
        return bjyskzhh;
    }

    public void setBjyskzhh(String bjyskzhh) {
        this.bjyskzhh = bjyskzhh;
    }

    public String getQyskzhzh() {
        return qyskzhzh;
    }

    public void setQyskzhzh(String qyskzhzh) {
        this.qyskzhzh = qyskzhzh;
    }

    public String getLxyskzhh() {
        return lxyskzhh;
    }

    public void setLxyskzhh(String lxyskzhh) {
        this.lxyskzhh = lxyskzhh;
    }

    public String getLxyskzxh() {
        return lxyskzxh;
    }

    public void setLxyskzxh(String lxyskzxh) {
        this.lxyskzxh = lxyskzxh;
    }

    public String getBjysfzhh() {
        return bjysfzhh;
    }

    public void setBjysfzhh(String bjysfzhh) {
        this.bjysfzhh = bjysfzhh;
    }

    public String getBjysfzxh() {
        return bjysfzxh;
    }

    public void setBjysfzxh(String bjysfzxh) {
        this.bjysfzxh = bjysfzxh;
    }

    public String getLxysfzhh() {
        return lxysfzhh;
    }

    public void setLxysfzhh(String lxysfzhh) {
        this.lxysfzhh = lxysfzhh;
    }

    public String getQtyfzhzx() {
        return qtyfzhzx;
    }

    public void setQtyfzhzx(String qtyfzhzx) {
        this.qtyfzhzx = qtyfzhzx;
    }

    public String getJydszhao() {
        return jydszhao;
    }

    public void setJydszhao(String jydszhao) {
        this.jydszhao = jydszhao;
    }

    public String getJydszhzh() {
        return jydszhzh;
    }

    public void setJydszhzh(String jydszhzh) {
        this.jydszhzh = jydszhzh;
    }

    public String getNbuzhhao() {
        return nbuzhhao;
    }

    public void setNbuzhhao(String nbuzhhao) {
        this.nbuzhhao = nbuzhhao;
    }

    public String getNbuzhzxh() {
        return nbuzhzxh;
    }

    public void setNbuzhzxh(String nbuzhzxh) {
        this.nbuzhzxh = nbuzhzxh;
    }

    @Override
    public String toString() {
        return "Ln3065ReqDto{" +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "xieybhao='" + xieybhao + '\'' +
                "xieyimch='" + xieyimch + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "dhfkywlx='" + dhfkywlx + '\'' +
                "zrfukzhh='" + zrfukzhh + '\'' +
                "zrfukzxh='" + zrfukzxh + '\'' +
                "bcfkjine='" + bcfkjine + '\'' +
                "fukuanrq='" + fukuanrq + '\'' +
                "zrfukzht='" + zrfukzht + '\'' +
                "huazhfax='" + huazhfax + '\'' +
                "zhanghye='" + zhanghye + '\'' +
                "yueeeeee='" + yueeeeee + '\'' +
                "benjinje='" + benjinje + '\'' +
                "lixijine='" + lixijine + '\'' +
                "jydszhmc='" + jydszhmc + '\'' +
                "jydsleix='" + jydsleix + '\'' +
                "jydshmch='" + jydshmch + '\'' +
                "zhkaihhh='" + zhkaihhh + '\'' +
                "zhkaihhm='" + zhkaihhm + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "sunyrzzh='" + sunyrzzh + '\'' +
                "syrzzhxh='" + syrzzhxh + '\'' +
                "bjyskzhh='" + bjyskzhh + '\'' +
                "qyskzhzh='" + qyskzhzh + '\'' +
                "lxyskzhh='" + lxyskzhh + '\'' +
                "lxyskzxh='" + lxyskzxh + '\'' +
                "bjysfzhh='" + bjysfzhh + '\'' +
                "bjysfzxh='" + bjysfzxh + '\'' +
                "lxysfzhh='" + lxysfzhh + '\'' +
                "qtyfzhzx='" + qtyfzhzx + '\'' +
                "jydszhao='" + jydszhao + '\'' +
                "jydszhzh='" + jydszhzh + '\'' +
                "nbuzhhao='" + nbuzhhao + '\'' +
                "nbuzhzxh='" + nbuzhzxh + '\'' +
                '}';
    }
}  
