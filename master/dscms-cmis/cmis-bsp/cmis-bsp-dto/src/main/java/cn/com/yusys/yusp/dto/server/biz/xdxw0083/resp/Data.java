package cn.com.yusys.yusp.dto.server.biz.xdxw0083.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：优企贷还款账号变更
 * @author wzy
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @JsonProperty(value = "opFlag")
    private String opFlag;//操作标志

    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息
    @Override
    public String toString() {
        return "Data{" +
                "opFlag=" + opFlag +
                "opMsg=" + opMsg +
                '}';
    }
}
