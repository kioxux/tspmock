package cn.com.yusys.yusp.dto.server.biz.xdht0011.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class HxdLoanContList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同号
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同开始日期
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同结束日期
    @JsonProperty(value = "contSignDate")
    private String contSignDate;//合同签订日期
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "debitName")
    private String debitName;//借款人姓名
    @JsonProperty(value = "debitCertNo")
    private String debitCertNo;//借款人证件号
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "serno")
    private String serno;//惠享贷业务流水
    @JsonProperty(value = "contTerm")
    private String contTerm;//合同期限
    @JsonProperty(value = "signStatus")
    private String signStatus;//签约状态
    @JsonProperty(value = "rela")
    private String rela;//关系XD_KHGX_RE
    @JsonProperty(value = "commonDebitName")
    private String commonDebitName;//共借人姓名
    @JsonProperty(value = "commonDebitPhone")
    private String commonDebitPhone;//共借人电话号码
    @JsonProperty(value = "commonDebitAddr")
    private String commonDebitAddr;//共借人地址
    @JsonProperty(value = "commonDebitSignStatus")
    private String commonDebitSignStatus;//共借人签约状态

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getContSignDate() {
        return contSignDate;
    }

    public void setContSignDate(String contSignDate) {
        this.contSignDate = contSignDate;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getDebitName() {
        return debitName;
    }

    public void setDebitName(String debitName) {
        this.debitName = debitName;
    }

    public String getDebitCertNo() {
        return debitCertNo;
    }

    public void setDebitCertNo(String debitCertNo) {
        this.debitCertNo = debitCertNo;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getContTerm() {
        return contTerm;
    }

    public void setContTerm(String contTerm) {
        this.contTerm = contTerm;
    }

    public String getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(String signStatus) {
        this.signStatus = signStatus;
    }

    public String getRela() {
        return rela;
    }

    public void setRela(String rela) {
        this.rela = rela;
    }

    public String getCommonDebitName() {
        return commonDebitName;
    }

    public void setCommonDebitName(String commonDebitName) {
        this.commonDebitName = commonDebitName;
    }

    public String getCommonDebitPhone() {
        return commonDebitPhone;
    }

    public void setCommonDebitPhone(String commonDebitPhone) {
        this.commonDebitPhone = commonDebitPhone;
    }

    public String getCommonDebitAddr() {
        return commonDebitAddr;
    }

    public void setCommonDebitAddr(String commonDebitAddr) {
        this.commonDebitAddr = commonDebitAddr;
    }

    public String getCommonDebitSignStatus() {
        return commonDebitSignStatus;
    }

    public void setCommonDebitSignStatus(String commonDebitSignStatus) {
        this.commonDebitSignStatus = commonDebitSignStatus;
    }

    @Override
    public String toString() {
        return "HxdLoanContList{" +
                "contNo='" + contNo + '\'' +
                "cnContNo='" + cnContNo + '\'' +
                "contStartDate='" + contStartDate + '\'' +
                "contEndDate='" + contEndDate + '\'' +
                "contSignDate='" + contSignDate + '\'' +
                "contAmt='" + contAmt + '\'' +
                "debitName='" + debitName + '\'' +
                "debitCertNo='" + debitCertNo + '\'' +
                "contStatus='" + contStatus + '\'' +
                "serno='" + serno + '\'' +
                "contTerm='" + contTerm + '\'' +
                "signStatus='" + signStatus + '\'' +
                "rela='" + rela + '\'' +
                "commonDebitName='" + commonDebitName + '\'' +
                "commonDebitPhone='" + commonDebitPhone + '\'' +
                "commonDebitAddr='" + commonDebitAddr + '\'' +
                "commonDebitSignStatus='" + commonDebitSignStatus + '\'' +
                '}';
    }
}
