package cn.com.yusys.yusp.dto.client.esb.core.ln3103;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款账户交易明细查询
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3103RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "listnm")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3103.Listnm> listnm;//贷款账户交易明细[LIST]

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }


    public List<Listnm> getListnm() {
        return listnm;
    }

    public void setListnm(List<Listnm> listnm) {
        this.listnm = listnm;
    }

    @Override
    public String toString() {
        return "Ln3103RespDto{" +
                "zongbish=" + zongbish +
                ", listnm=" + listnm +
                '}';
    }
}
