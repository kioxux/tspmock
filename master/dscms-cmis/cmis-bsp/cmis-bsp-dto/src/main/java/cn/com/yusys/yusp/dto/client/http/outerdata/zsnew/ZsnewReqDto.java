package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：企业工商信息查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class ZsnewReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "creditcode")
    private String creditcode;   //统一信用代码
    @JsonProperty(value = "entstatus")
    private String entstatus;    //企业经营状态，1：在营，2：非在营
    @JsonProperty(value = "enttype")
    private String enttype;      //企业类型:1-企业 2-个体
    @JsonProperty(value = "id")
    private String id;           //中数企业ID
    @JsonProperty(value = "name")
    private String name;         //企业名称
    @JsonProperty(value = "orgcode")
    private String orgcode;      //组织机构代码
    @JsonProperty(value = "regno")
    private String regno;        //企业注册号
    @JsonProperty(value = "version")
    private String version;      //高管识别码版本号,返回高管识别码时生效

    public String getCreditcode() {
        return creditcode;
    }

    public void setCreditcode(String creditcode) {
        this.creditcode = creditcode;
    }

    public String getEntstatus() {
        return entstatus;
    }

    public void setEntstatus(String entstatus) {
        this.entstatus = entstatus;
    }

    public String getEnttype() {
        return enttype;
    }

    public void setEnttype(String enttype) {
        this.enttype = enttype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "ZsnewReqDto{" +
                "creditcode='" + creditcode + '\'' +
                ", entstatus='" + entstatus + '\'' +
                ", enttype='" + enttype + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", orgcode='" + orgcode + '\'' +
                ", regno='" + regno + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}
