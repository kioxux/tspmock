package cn.com.yusys.yusp.dto.client.esb.core.ln3163;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：资产证券化处理
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3163ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zichculi")
    private String zichculi;//资产处理类型
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "xieyshje")
    private BigDecimal xieyshje;//协议实际金额
    @JsonProperty(value = "fengbriq")
    private String fengbriq;//封包日期
    @JsonProperty(value = "jiebriqi")
    private String jiebriqi;//解包日期
    @JsonProperty(value = "ruchiriq")
    private String ruchiriq;//入池日期
    @JsonProperty(value = "huigriqi")
    private String huigriqi;//回购日期
    @JsonProperty(value = "zchzhtai")
    private String zchzhtai;//资产处理状态

    public String getZichculi() {
        return zichculi;
    }

    public void setZichculi(String zichculi) {
        this.zichculi = zichculi;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getXieyshje() {
        return xieyshje;
    }

    public void setXieyshje(BigDecimal xieyshje) {
        this.xieyshje = xieyshje;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getJiebriqi() {
        return jiebriqi;
    }

    public void setJiebriqi(String jiebriqi) {
        this.jiebriqi = jiebriqi;
    }

    public String getRuchiriq() {
        return ruchiriq;
    }

    public void setRuchiriq(String ruchiriq) {
        this.ruchiriq = ruchiriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public String getZchzhtai() {
        return zchzhtai;
    }

    public void setZchzhtai(String zchzhtai) {
        this.zchzhtai = zchzhtai;
    }

    @Override
    public String toString() {
        return "Ln3163ReqDto{" +
                "zichculi='" + zichculi + '\'' +
                "xieybhao='" + xieybhao + '\'' +
                "xieyimch='" + xieyimch + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "xieyshje='" + xieyshje + '\'' +
                "fengbriq='" + fengbriq + '\'' +
                "jiebriqi='" + jiebriqi + '\'' +
                "ruchiriq='" + ruchiriq + '\'' +
                "huigriqi='" + huigriqi + '\'' +
                "zchzhtai='" + zchzhtai + '\'' +
                '}';
    }
}  
