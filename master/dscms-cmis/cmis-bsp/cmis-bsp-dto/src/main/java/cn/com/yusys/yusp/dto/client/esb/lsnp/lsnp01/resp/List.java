package cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "busi_seq")
    private String busi_seq;//业务流水号
    @JsonProperty(value = "cred_sub_no")
    private String cred_sub_no;//授信分项编号
    @JsonProperty(value = "riskData")
    private java.util.List<RiskData> riskData;//riskList

    public String getBusi_seq() {
        return busi_seq;
    }

    public void setBusi_seq(String busi_seq) {
        this.busi_seq = busi_seq;
    }

    public String getCred_sub_no() {
        return cred_sub_no;
    }

    public void setCred_sub_no(String cred_sub_no) {
        this.cred_sub_no = cred_sub_no;
    }

    public java.util.List<RiskData> getRiskData() {
        return riskData;
    }

    public void setRiskData(java.util.List<RiskData> riskData) {
        this.riskData = riskData;
    }

    @Override
    public String toString() {
        return "List{" +
                "busi_seq='" + busi_seq + '\'' +
                ", cred_sub_no='" + cred_sub_no + '\'' +
                ", riskData=" + riskData +
                '}';
    }
}
