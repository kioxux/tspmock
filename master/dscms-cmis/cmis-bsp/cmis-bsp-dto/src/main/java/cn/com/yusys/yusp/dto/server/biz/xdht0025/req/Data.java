package cn.com.yusys.yusp.dto.server.biz.xdht0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：合同签订/撤销（优享贷和增享贷）
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "Deal_type")
    private String Deal_type;//请求类型
    @JsonProperty(value = "creditImageNo")
    private String creditImageNo;//个人征信影像编号
    @JsonProperty(value = "bigDataCreditNo")
    private String bigDataCreditNo;//大数据信用影像编号
    @JsonProperty(value = "loanDirectionNo")
    private String loanDirectionNo;//贷款用途影像编号
    @JsonProperty(value = "contImageNo")
    private String contImageNo;//电子合同影像编号
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    public String  getCont_no() { return cont_no; }
    public void setCont_no(String cont_no ) { this.cont_no = cont_no;}
    public String  get$eal_type() { return Deal_type; }
    public void set$eal_type(String Deal_type ) { this.Deal_type = Deal_type;}
    public String  getCreditImageNo() { return creditImageNo; }
    public void setCreditImageNo(String creditImageNo ) { this.creditImageNo = creditImageNo;}
    public String  getBigDataCreditNo() { return bigDataCreditNo; }
    public void setBigDataCreditNo(String bigDataCreditNo ) { this.bigDataCreditNo = bigDataCreditNo;}
    public String  getLoanDirectionNo() { return loanDirectionNo; }
    public void setLoanDirectionNo(String loanDirectionNo ) { this.loanDirectionNo = loanDirectionNo;}
    public String  getContImageNo() { return contImageNo; }
    public void setContImageNo(String contImageNo ) { this.contImageNo = contImageNo;}
    public String  getRepayType() { return repayType; }
    public void setRepayType(String repayType ) { this.repayType = repayType;}
    @Override
    public String toString() {
        return "Xdht0025ReqDto{" +
                "cont_no='" + cont_no+ '\'' +
                "Deal_type='" + Deal_type+ '\'' +
                "creditImageNo='" + creditImageNo+ '\'' +
                "bigDataCreditNo='" + bigDataCreditNo+ '\'' +
                "loanDirectionNo='" + loanDirectionNo+ '\'' +
                "contImageNo='" + contImageNo+ '\'' +
                "repayType='" + repayType+ '\'' +
                '}';
    }
}
