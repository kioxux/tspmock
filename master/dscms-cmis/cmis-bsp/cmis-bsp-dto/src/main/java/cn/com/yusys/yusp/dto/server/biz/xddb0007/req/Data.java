package cn.com.yusys.yusp.dto.server.biz.xddb0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 14:37
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "opType")
    private String opType;//操作类型
    @JsonProperty(value = "serno")
    private String serno;//入池批次号
    @JsonProperty(value = "eBillNo")
    private String eBillNo;//电票号码
    @JsonProperty(value = "imnManCusId")
    private String imnManCusId;//质押人客户号
    @JsonProperty(value = "imnCusName")
    private String imnCusName;//质押客户名称
    @JsonProperty(value = "collName")
    private String collName;//质押品名称
    @JsonProperty(value = "ownCertNo")
    private String ownCertNo;//权属证号
    @JsonProperty(value = "drftAmt")
    private BigDecimal drftAmt;//票面金额
    @JsonProperty(value = "eBillCurType")
    private String eBillCurType;//电票币种
    @JsonProperty(value = "isseDate")
    private String isseDate;//出票日
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "pyeeName")
    private String pyeeName;//收款人名称
    @JsonProperty(value = "pyeeAcctbNm")
    private String pyeeAcctbNm;//收款人开户行行名
    @JsonProperty(value = "pyeeAcctbNo")
    private String pyeeAcctbNo;//收款人开户行行号
    @JsonProperty(value = "pyeeAcctNo")
    private String pyeeAcctNo;//收款人账号
    @JsonProperty(value = "drwrCertNo")
    private String drwrCertNo;//出票人证件号码
    @JsonProperty(value = "drwrName")
    private String drwrName;//出票人名称
    @JsonProperty(value = "drwrAcctbNo")
    private String drwrAcctbNo;//出票人开户行行号
    @JsonProperty(value = "drwrAcctbNm")
    private String drwrAcctbNm;//出票人开户行行名
    @JsonProperty(value = "drwrAcctbAcctNo")
    private String drwrAcctbAcctNo;//出票人开户行帐号
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//管理机构
    @JsonProperty(value = "aorgType")
    private String aorgType;//承兑行类型
    @JsonProperty(value = "aorgNo")
    private String aorgNo;//承兑行行号
    @JsonProperty(value = "accptr")
    private String accptr;//承兑人
    @JsonProperty(value = "evalType")
    private String evalType;//评估方式
    @JsonProperty(value = "evalAmt")
    private BigDecimal evalAmt;//评估金额（元）
    @JsonProperty(value = "evalDate")
    private String evalDate;//评估日期
    @JsonProperty(value = "evalOrg")
    private String evalOrg;//评估机构
    @JsonProperty(value = "evalOrgCmonNo")
    private String evalOrgCmonNo;//评估机构组织机构代码
    @JsonProperty(value = "claimAmt")
    private BigDecimal claimAmt;//权利金额(元)
    @JsonProperty(value = "drfpoImnFlag")
    private String drfpoImnFlag;//票据池质押标记

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String geteBillNo() {
        return eBillNo;
    }

    public void seteBillNo(String eBillNo) {
        this.eBillNo = eBillNo;
    }

    public String getImnManCusId() {
        return imnManCusId;
    }

    public void setImnManCusId(String imnManCusId) {
        this.imnManCusId = imnManCusId;
    }

    public String getImnCusName() {
        return imnCusName;
    }

    public void setImnCusName(String imnCusName) {
        this.imnCusName = imnCusName;
    }

    public String getCollName() {
        return collName;
    }

    public void setCollName(String collName) {
        this.collName = collName;
    }

    public String getOwnCertNo() {
        return ownCertNo;
    }

    public void setOwnCertNo(String ownCertNo) {
        this.ownCertNo = ownCertNo;
    }

    public BigDecimal getDrftAmt() {
        return drftAmt;
    }

    public void setDrftAmt(BigDecimal drftAmt) {
        this.drftAmt = drftAmt;
    }

    public String geteBillCurType() {
        return eBillCurType;
    }

    public void seteBillCurType(String eBillCurType) {
        this.eBillCurType = eBillCurType;
    }

    public String getIsseDate() {
        return isseDate;
    }

    public void setIsseDate(String isseDate) {
        this.isseDate = isseDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPyeeName() {
        return pyeeName;
    }

    public void setPyeeName(String pyeeName) {
        this.pyeeName = pyeeName;
    }

    public String getPyeeAcctbNm() {
        return pyeeAcctbNm;
    }

    public void setPyeeAcctbNm(String pyeeAcctbNm) {
        this.pyeeAcctbNm = pyeeAcctbNm;
    }

    public String getPyeeAcctbNo() {
        return pyeeAcctbNo;
    }

    public void setPyeeAcctbNo(String pyeeAcctbNo) {
        this.pyeeAcctbNo = pyeeAcctbNo;
    }

    public String getPyeeAcctNo() {
        return pyeeAcctNo;
    }

    public void setPyeeAcctNo(String pyeeAcctNo) {
        this.pyeeAcctNo = pyeeAcctNo;
    }

    public String getDrwrCertNo() {
        return drwrCertNo;
    }

    public void setDrwrCertNo(String drwrCertNo) {
        this.drwrCertNo = drwrCertNo;
    }

    public String getDrwrName() {
        return drwrName;
    }

    public void setDrwrName(String drwrName) {
        this.drwrName = drwrName;
    }

    public String getDrwrAcctbNo() {
        return drwrAcctbNo;
    }

    public void setDrwrAcctbNo(String drwrAcctbNo) {
        this.drwrAcctbNo = drwrAcctbNo;
    }

    public String getDrwrAcctbNm() {
        return drwrAcctbNm;
    }

    public void setDrwrAcctbNm(String drwrAcctbNm) {
        this.drwrAcctbNm = drwrAcctbNm;
    }

    public String getDrwrAcctbAcctNo() {
        return drwrAcctbAcctNo;
    }

    public void setDrwrAcctbAcctNo(String drwrAcctbAcctNo) {
        this.drwrAcctbAcctNo = drwrAcctbAcctNo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getAorgType() {
        return aorgType;
    }

    public void setAorgType(String aorgType) {
        this.aorgType = aorgType;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getAccptr() {
        return accptr;
    }

    public void setAccptr(String accptr) {
        this.accptr = accptr;
    }

    public String getEvalType() {
        return evalType;
    }

    public void setEvalType(String evalType) {
        this.evalType = evalType;
    }

    public BigDecimal getEvalAmt() {
        return evalAmt;
    }

    public void setEvalAmt(BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    public String getEvalDate() {
        return evalDate;
    }

    public void setEvalDate(String evalDate) {
        this.evalDate = evalDate;
    }

    public String getEvalOrg() {
        return evalOrg;
    }

    public void setEvalOrg(String evalOrg) {
        this.evalOrg = evalOrg;
    }

    public String getEvalOrgCmonNo() {
        return evalOrgCmonNo;
    }

    public void setEvalOrgCmonNo(String evalOrgCmonNo) {
        this.evalOrgCmonNo = evalOrgCmonNo;
    }

    public BigDecimal getClaimAmt() {
        return claimAmt;
    }

    public void setClaimAmt(BigDecimal claimAmt) {
        this.claimAmt = claimAmt;
    }

    public String getDrfpoImnFlag() {
        return drfpoImnFlag;
    }

    public void setDrfpoImnFlag(String drfpoImnFlag) {
        this.drfpoImnFlag = drfpoImnFlag;
    }

    @Override
    public String toString() {
        return "Xddb0007DataReqDto{" +
                "opType='" + opType + '\'' +
                ", serno='" + serno + '\'' +
                ", eBillNo='" + eBillNo + '\'' +
                ", imnManCusId='" + imnManCusId + '\'' +
                ", imnCusName='" + imnCusName + '\'' +
                ", collName='" + collName + '\'' +
                ", ownCertNo='" + ownCertNo + '\'' +
                ", drftAmt=" + drftAmt +
                ", eBillCurType='" + eBillCurType + '\'' +
                ", isseDate='" + isseDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", pyeeName='" + pyeeName + '\'' +
                ", pyeeAcctbNm='" + pyeeAcctbNm + '\'' +
                ", pyeeAcctbNo='" + pyeeAcctbNo + '\'' +
                ", pyeeAcctNo='" + pyeeAcctNo + '\'' +
                ", drwrCertNo='" + drwrCertNo + '\'' +
                ", drwrName='" + drwrName + '\'' +
                ", drwrAcctbNo='" + drwrAcctbNo + '\'' +
                ", drwrAcctbNm='" + drwrAcctbNm + '\'' +
                ", drwrAcctbAcctNo='" + drwrAcctbAcctNo + '\'' +
                ", managerId='" + managerId + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", aorgType='" + aorgType + '\'' +
                ", aorgNo='" + aorgNo + '\'' +
                ", accptr='" + accptr + '\'' +
                ", evalType='" + evalType + '\'' +
                ", evalAmt=" + evalAmt +
                ", evalDate='" + evalDate + '\'' +
                ", evalOrg='" + evalOrg + '\'' +
                ", evalOrgCmonNo='" + evalOrgCmonNo + '\'' +
                ", claimAmt=" + claimAmt +
                ", drfpoImnFlag='" + drfpoImnFlag + '\'' +
                '}';
    }
}
