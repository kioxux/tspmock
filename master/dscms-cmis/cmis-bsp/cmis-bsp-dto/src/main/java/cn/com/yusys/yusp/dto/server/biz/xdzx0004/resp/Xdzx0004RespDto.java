package cn.com.yusys.yusp.dto.server.biz.xdzx0004.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：授权结果反馈接口
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzx0004RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdzx0003RespDto{" +
                "data=" + data +
                '}';
    }
}
