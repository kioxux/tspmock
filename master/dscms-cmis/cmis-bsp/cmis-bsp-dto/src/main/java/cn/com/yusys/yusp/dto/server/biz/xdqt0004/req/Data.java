package cn.com.yusys.yusp.dto.server.biz.xdqt0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：贷款申请预约（个人客户）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "loanBank")
    private String loanBank;//贷款服务行
    @JsonProperty(value = "addr")
    private String addr;//地址
    @JsonProperty(value = "procde")
    private String procde;//邮编
    @JsonProperty(value = "phone")
    private String phone;//电话
    @JsonProperty(value = "loanUse")
    private String loanUse;//贷款用途
    @JsonProperty(value = "loanBal")
    private BigDecimal loanBal;//贷款金额
    @JsonProperty(value = "loanTerm")
    private String loanTerm;//贷款期限
    @JsonProperty(value = "guarType")
    private String guarType;//担保方式
    @JsonProperty(value = "cusName")
    private String cusName;//姓名
    @JsonProperty(value = "sex")
    private String sex;//性别
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "mobile")
    private String mobile;//手机号
    @JsonProperty(value = "edu")
    private String edu;//学历
    @JsonProperty(value = "marStatus")
    private String marStatus;//婚姻状况
    @JsonProperty(value = "indivRelComName")
    private String indivRelComName;//单位名称
    @JsonProperty(value = "indivComTyp")
    private String indivComTyp;//单位性质
    @JsonProperty(value = "isLocal")
    private String isLocal;//是否本地户口
    @JsonProperty(value = "yearn")
    private BigDecimal yearn;//年收入
    @JsonProperty(value = "pldType")
    private String pldType;//抵押物类型
    @JsonProperty(value = "collType")
    private String collType;//质押物类型
    @JsonProperty(value = "duty")
    private String duty;//职务
    @JsonProperty(value = "indivRsdAddr")
    private String indivRsdAddr;//居住地址
    @JsonProperty(value = "infoSour")
    private String infoSour;//信息来源
    @JsonProperty(value = "loanType")
    private String loanType;//经营或消费性贷款


    public String getLoanBank() {
        return loanBank;
    }

    public void setLoanBank(String loanBank) {
        this.loanBank = loanBank;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getProcde() {
        return procde;
    }

    public void setProcde(String procde) {
        this.procde = procde;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLoanUse() {
        return loanUse;
    }

    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse;
    }

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getGuarType() {
        return guarType;
    }

    public void setGuarType(String guarType) {
        this.guarType = guarType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getIndivRelComName() {
        return indivRelComName;
    }

    public void setIndivRelComName(String indivRelComName) {
        this.indivRelComName = indivRelComName;
    }

    public String getIndivComTyp() {
        return indivComTyp;
    }

    public void setIndivComTyp(String indivComTyp) {
        this.indivComTyp = indivComTyp;
    }

    public String getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(String isLocal) {
        this.isLocal = isLocal;
    }

    public BigDecimal getYearn() {
        return yearn;
    }

    public void setYearn(BigDecimal yearn) {
        this.yearn = yearn;
    }

    public String getPldType() {
        return pldType;
    }

    public void setPldType(String pldType) {
        this.pldType = pldType;
    }

    public String getCollType() {
        return collType;
    }

    public void setCollType(String collType) {
        this.collType = collType;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getIndivRsdAddr() {
        return indivRsdAddr;
    }

    public void setIndivRsdAddr(String indivRsdAddr) {
        this.indivRsdAddr = indivRsdAddr;
    }

    public String getInfoSour() {
        return infoSour;
    }

    public void setInfoSour(String infoSour) {
        this.infoSour = infoSour;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "loanBank='" + loanBank + '\'' +
                ", addr='" + addr + '\'' +
                ", procde='" + procde + '\'' +
                ", phone='" + phone + '\'' +
                ", loanUse='" + loanUse + '\'' +
                ", loanBal=" + loanBal +
                ", loanTerm='" + loanTerm + '\'' +
                ", guarType='" + guarType + '\'' +
                ", cusName='" + cusName + '\'' +
                ", sex='" + sex + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", mobile='" + mobile + '\'' +
                ", edu='" + edu + '\'' +
                ", marStatus='" + marStatus + '\'' +
                ", indivRelComName='" + indivRelComName + '\'' +
                ", indivComTyp='" + indivComTyp + '\'' +
                ", isLocal='" + isLocal + '\'' +
                ", yearn=" + yearn +
                ", pldType='" + pldType + '\'' +
                ", collType='" + collType + '\'' +
                ", duty='" + duty + '\'' +
                ", indivRsdAddr='" + indivRsdAddr + '\'' +
                ", infoSour='" + infoSour + '\'' +
                ", loanType='" + loanType + '\'' +
                '}';
    }
}
