package cn.com.yusys.yusp.dto.client.esb.core.ln3007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：资产产品币种查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3007RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "listnm")
    private java.util.List<Listnm> listnm;

    public List<Listnm> getListnm() {
        return listnm;
    }

    public void setListnm(List<Listnm> listnm) {
        this.listnm = listnm;
    }

    @Override
    public String toString() {
        return "Ln3007RespDto{" +
                "listnm=" + listnm +
                '}';
    }
}
