package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷签约通知
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj14RespDto implements Serializable {
    private static final long serialVersionUID = 1L;


}  
