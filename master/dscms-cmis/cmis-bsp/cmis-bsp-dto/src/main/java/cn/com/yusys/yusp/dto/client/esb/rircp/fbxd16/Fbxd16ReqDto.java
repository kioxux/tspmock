package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd16;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：取得蚂蚁核销记录表的核销借据号一览
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd16ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "trade_code")
    private String trade_code;//交易码
    @JsonProperty(value = "contract_no")
    private String contract_no;

    public String getTrade_code() {
        return trade_code;
    }

    public void setTrade_code(String trade_code) {
        this.trade_code = trade_code;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Fbxd16ReqDto{" +
                "trade_code='" + trade_code + '\'' +
                ", contract_no='" + contract_no + '\'' +
                '}';
    }
}
