package cn.com.yusys.yusp.dto.server.biz.xdtz0047.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/18 14:21:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/18 14:21
 * @since 2021/5/18 14:21
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "query_type")
    private String query_type;//查询类型
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户编号

    public String getQuery_type() {
        return query_type;
    }

    public void setQuery_type(String query_type) {
        this.query_type = query_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    @Override
    public String toString() {
        return "Data{" +
                "query_type='" + query_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cus_id='" + cus_id + '\'' +
                '}';
    }
}
