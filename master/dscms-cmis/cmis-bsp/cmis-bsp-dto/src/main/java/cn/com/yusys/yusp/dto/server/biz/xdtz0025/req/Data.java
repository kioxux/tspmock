package cn.com.yusys.yusp.dto.server.biz.xdtz0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/21 15:31:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 15:31
 * @since 2021/5/21 15:31
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款开始日

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "loanStartDate='" + loanStartDate + '\'' +
                '}';
    }
}
