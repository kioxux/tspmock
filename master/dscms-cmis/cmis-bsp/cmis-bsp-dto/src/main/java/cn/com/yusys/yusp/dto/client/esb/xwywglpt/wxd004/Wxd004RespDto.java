package cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷系统获取征信报送监管信息接口
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxd004RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "reqid")
    private String reqid;//系统流水号
    @JsonProperty(value = "sexcode")
    private String sexcode;//性别
    @JsonProperty(value = "borndate")
    private String borndate;//出生日期
    @JsonProperty(value = "marryinfocode")
    private String marryinfocode;//婚姻状况
    @JsonProperty(value = "educode")
    private String educode;//文化程度
    @JsonProperty(value = "conaddress")
    private String conaddress;//通讯地址
    @JsonProperty(value = "postcode")
    private String postcode;//邮政编码
    @JsonProperty(value = "positionworkcode")
    private String positionworkcode;//从事职业
    @JsonProperty(value = "comname")
    private String comname;//工作单位
    @JsonProperty(value = "positioncode")
    private String positioncode;//职务
    @JsonProperty(value = "houseaddress")
    private String houseaddress;//居住地址
    @JsonProperty(value = "housepost")
    private String housepost;//居住地址邮编
    @JsonProperty(value = "houseinfocode")
    private String houseinfocode;//居住状况
    @JsonProperty(value = "areacode")
    private String areacode;//区域编号
    @JsonProperty(value = "sfnhcode")
    private String sfnhcode;//是否农户
    @JsonProperty(value = "issuccess")
    private String issuccess;//是否加工完成

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getReqid() {
        return reqid;
    }

    public void setReqid(String reqid) {
        this.reqid = reqid;
    }

    public String getSexcode() {
        return sexcode;
    }

    public void setSexcode(String sexcode) {
        this.sexcode = sexcode;
    }

    public String getBorndate() {
        return borndate;
    }

    public void setBorndate(String borndate) {
        this.borndate = borndate;
    }

    public String getMarryinfocode() {
        return marryinfocode;
    }

    public void setMarryinfocode(String marryinfocode) {
        this.marryinfocode = marryinfocode;
    }

    public String getEducode() {
        return educode;
    }

    public void setEducode(String educode) {
        this.educode = educode;
    }

    public String getConaddress() {
        return conaddress;
    }

    public void setConaddress(String conaddress) {
        this.conaddress = conaddress;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPositionworkcode() {
        return positionworkcode;
    }

    public void setPositionworkcode(String positionworkcode) {
        this.positionworkcode = positionworkcode;
    }

    public String getComname() {
        return comname;
    }

    public void setComname(String comname) {
        this.comname = comname;
    }

    public String getPositioncode() {
        return positioncode;
    }

    public void setPositioncode(String positioncode) {
        this.positioncode = positioncode;
    }

    public String getHouseaddress() {
        return houseaddress;
    }

    public void setHouseaddress(String houseaddress) {
        this.houseaddress = houseaddress;
    }

    public String getHousepost() {
        return housepost;
    }

    public void setHousepost(String housepost) {
        this.housepost = housepost;
    }

    public String getHouseinfocode() {
        return houseinfocode;
    }

    public void setHouseinfocode(String houseinfocode) {
        this.houseinfocode = houseinfocode;
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode;
    }

    public String getSfnhcode() {
        return sfnhcode;
    }

    public void setSfnhcode(String sfnhcode) {
        this.sfnhcode = sfnhcode;
    }

    public String getIssuccess() {
        return issuccess;
    }

    public void setIssuccess(String issuccess) {
        this.issuccess = issuccess;
    }

    @Override
    public String toString() {
        return "Wxd004RespDto{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "reqid='" + reqid + '\'' +
                "sexcode ='" + sexcode + '\'' +
                "borndate ='" + borndate + '\'' +
                "marryinfocode='" + marryinfocode + '\'' +
                "educode='" + educode + '\'' +
                "conaddress ='" + conaddress + '\'' +
                "postcode ='" + postcode + '\'' +
                "positionworkcode='" + positionworkcode + '\'' +
                "comname='" + comname + '\'' +
                "positioncode='" + positioncode + '\'' +
                "houseaddress ='" + houseaddress + '\'' +
                "housepost='" + housepost + '\'' +
                "houseinfocode='" + houseinfocode + '\'' +
                "areacode='" + areacode + '\'' +
                "sfnhcode ='" + sfnhcode + '\'' +
                "issuccess ='" + issuccess + '\'' +
                '}';
    }
}
