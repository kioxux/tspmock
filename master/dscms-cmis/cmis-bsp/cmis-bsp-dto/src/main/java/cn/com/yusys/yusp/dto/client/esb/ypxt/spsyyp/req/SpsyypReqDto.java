package cn.com.yusys.yusp.dto.client.esb.ypxt.spsyyp.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷审批结果同步接口
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SpsyypReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yptybh")
    private String yptybh;//押品统一编号
    @JsonProperty(value = "splxyp")
    private String splxyp;//审批类型
    @JsonProperty(value = "cljgyp")
    private String cljgyp;//处理结果
    @JsonProperty(value = "clrghy")
    private String clrghy;//处理人工号
    @JsonProperty(value = "clrjgy")
    private String clrjgy;//处理人所属机构ID
    @JsonProperty(value = "remark")
    private String remark;//备注

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getSplxyp() {
        return splxyp;
    }

    public void setSplxyp(String splxyp) {
        this.splxyp = splxyp;
    }

    public String getCljgyp() {
        return cljgyp;
    }

    public void setCljgyp(String cljgyp) {
        this.cljgyp = cljgyp;
    }

    public String getClrghy() {
        return clrghy;
    }

    public void setClrghy(String clrghy) {
        this.clrghy = clrghy;
    }

    public String getClrjgy() {
        return clrjgy;
    }

    public void setClrjgy(String clrjgy) {
        this.clrjgy = clrjgy;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "SpsyypReqDto{" +
                "yptybh='" + yptybh + '\'' +
                "splxyp='" + splxyp + '\'' +
                "cljgyp='" + cljgyp + '\'' +
                "clrghy='" + clrghy + '\'' +
                "clrjgy='" + clrjgy + '\'' +
                "remark='" + remark + '\'' +
                '}';
    }
}  
