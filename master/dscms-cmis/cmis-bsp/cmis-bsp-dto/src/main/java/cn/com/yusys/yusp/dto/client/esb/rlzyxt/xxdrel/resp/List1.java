package cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询人员基本信息岗位信息家庭信息
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List1 implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "psncde")
    private String psncde;//工号
    @JsonProperty(value = "psnnam")
    private String psnnam;//姓名
    @JsonProperty(value = "dpcode")
    private String dpcode;//人力机构编号
    @JsonProperty(value = "dpname")
    private String dpname;//人力机构名称
    @JsonProperty(value = "jobcod")
    private String jobcod;//岗位编号
    @JsonProperty(value = "jobnam")
    private String jobnam;//岗位名称
    @JsonProperty(value = "defstr")
    private String defstr;//员工状态
    @JsonProperty(value = "psncl")
    private String psncl;//用工性质
    @JsonProperty(value = "idtftp")
    private String idtftp;//证件类型编号
    @JsonProperty(value = "idtfno")
    private String idtfno;//证件号
    @JsonProperty(value = "psnsex")
    private String psnsex;//性别编号
    @JsonProperty(value = "borndt")
    private String borndt;//出生日期

    public String getPsncde() {
        return psncde;
    }

    public void setPsncde(String psncde) {
        this.psncde = psncde;
    }

    public String getPsnnam() {
        return psnnam;
    }

    public void setPsnnam(String psnnam) {
        this.psnnam = psnnam;
    }

    public String getDpcode() {
        return dpcode;
    }

    public void setDpcode(String dpcode) {
        this.dpcode = dpcode;
    }

    public String getDpname() {
        return dpname;
    }

    public void setDpname(String dpname) {
        this.dpname = dpname;
    }

    public String getJobcod() {
        return jobcod;
    }

    public void setJobcod(String jobcod) {
        this.jobcod = jobcod;
    }

    public String getJobnam() {
        return jobnam;
    }

    public void setJobnam(String jobnam) {
        this.jobnam = jobnam;
    }

    public String getDefstr() {
        return defstr;
    }

    public void setDefstr(String defstr) {
        this.defstr = defstr;
    }

    public String getPsncl() {
        return psncl;
    }

    public void setPsncl(String psncl) {
        this.psncl = psncl;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getPsnsex() {
        return psnsex;
    }

    public void setPsnsex(String psnsex) {
        this.psnsex = psnsex;
    }

    public String getBorndt() {
        return borndt;
    }

    public void setBorndt(String borndt) {
        this.borndt = borndt;
    }


    @Override
    public String toString() {
        return "List1{" +
                "psncde  ='" + psncde + '\'' +
                "psnnam  ='" + psnnam + '\'' +
                "dpcode  ='" + dpcode + '\'' +
                "dpname  ='" + dpname + '\'' +
                "jobcod ='" + jobcod + '\'' +
                "jobnam  ='" + jobnam + '\'' +
                "defstr  ='" + defstr + '\'' +
                "psncl ='" + psncl + '\'' +
                "idtftp='" + idtftp + '\'' +
                "idtfno='" + idtfno + '\'' +
                "psnsex='" + psnsex + '\'' +
                "borndt='" + borndt + '\'' +
                '}';
    }
}  
