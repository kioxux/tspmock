package cn.com.yusys.yusp.dto.client.esb.core.ln3244;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款客户经理批量移交
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LstKehuhaoo  implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    @Override
    public String toString() {
        return "LstKehuhaoo{" +
                "kehuhaoo='" + kehuhaoo + '\'' +
                '}';
    }
}
