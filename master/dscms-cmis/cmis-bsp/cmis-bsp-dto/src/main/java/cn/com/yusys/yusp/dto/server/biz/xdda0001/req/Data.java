package cn.com.yusys.yusp.dto.server.biz.xdda0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：扫描人信息登记
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "oprType")
    private String oprType;//操作类型
    @JsonProperty(value = "docNo")
    private String docNo;//档案编号
    @JsonProperty(value = "scanId")
    private String scanId;//扫描人编号
    @JsonProperty(value = "scanName")
    private String scanName;//扫描人
    @JsonProperty(value = "scanTime")
    private String scanTime;//扫描时间
    @JsonProperty(value = "location")
    private String location;//库位号


    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getScanId() {
        return scanId;
    }

    public void setScanId(String scanId) {
        this.scanId = scanId;
    }

    public String getScanName() {
        return scanName;
    }

    public void setScanName(String scanName) {
        this.scanName = scanName;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Data{" +
                "oprType='" + oprType + '\'' +
                ", docNo='" + docNo + '\'' +
                ", scanId='" + scanId + '\'' +
                ", scanName='" + scanName + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
