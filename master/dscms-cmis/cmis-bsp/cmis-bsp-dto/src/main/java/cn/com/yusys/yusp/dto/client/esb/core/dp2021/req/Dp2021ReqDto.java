package cn.com.yusys.yusp.dto.client.esb.core.dp2021.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户账号-子账号互查
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2021ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xzsxnzhi")
    private String xzsxnzhi;//限制属性值
    @JsonProperty(value = "chaxleix")
    private String chaxleix;//查询类型
    @JsonProperty(value = "chaxfanw")
    private String chaxfanw;//查询范围
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "zhanghao")
    private String zhanghao;//负债账号
    @JsonProperty(value = "dinhuobz")
    private String dinhuobz;//产品定活标志
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh;//子账户序号
    @JsonProperty(value = "cunkzlei")
    private String cunkzlei;//存款种类
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//币种
    @JsonProperty(value = "chaohubz")
    private String chaohubz;//账户钞汇标志
    @JsonProperty(value = "zhcxzhzt")
    private String zhcxzhzt;//客户账户查询状态
    @JsonProperty(value = "chaxmima")
    private String chaxmima;//查询密码
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数
    @JsonProperty(value = "shifoudy")
    private String shifoudy;//是否打印
    @JsonProperty(value = "jigouhao")
    private String jigouhao;//机构号
    @JsonProperty(value = "mimazlei")
    private String mimazlei;//密码种类
    @JsonProperty(value = "sfcxglzh")
    private String sfcxglzh;//是否查询关联账户
    @JsonProperty(value = "zhjnzlei")
    private String zhjnzlei;//证件种类
    @JsonProperty(value = "zhjhaoma")
    private String zhjhaoma;//证件号码
    @JsonProperty(value = "kehuzhmc")
    private String kehuzhmc;//客户账户名称
    @JsonProperty(value = "beiyzd01")
    private String beiyzd01;//备用字段01
    @JsonProperty(value = "beiyzd02")
    private String beiyzd02;//备用字段02
    @JsonProperty(value = "jiymkzbz")
    private String jiymkzbz;//限制标志
    @JsonProperty(value = "zhhufenl")
    private String zhhufenl;//账户分类
    @JsonProperty(value = "zhshuxin")
    private String zhshuxin;//账户属性


    public String getXzsxnzhi() {
        return xzsxnzhi;
    }

    public void setXzsxnzhi(String xzsxnzhi) {
        this.xzsxnzhi = xzsxnzhi;
    }

    public String getChaxleix() {
        return chaxleix;
    }

    public void setChaxleix(String chaxleix) {
        this.chaxleix = chaxleix;
    }

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getDinhuobz() {
        return dinhuobz;
    }

    public void setDinhuobz(String dinhuobz) {
        this.dinhuobz = dinhuobz;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getZhcxzhzt() {
        return zhcxzhzt;
    }

    public void setZhcxzhzt(String zhcxzhzt) {
        this.zhcxzhzt = zhcxzhzt;
    }

    public String getChaxmima() {
        return chaxmima;
    }

    public void setChaxmima(String chaxmima) {
        this.chaxmima = chaxmima;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    public String getJigouhao() {
        return jigouhao;
    }

    public void setJigouhao(String jigouhao) {
        this.jigouhao = jigouhao;
    }

    public String getMimazlei() {
        return mimazlei;
    }

    public void setMimazlei(String mimazlei) {
        this.mimazlei = mimazlei;
    }

    public String getSfcxglzh() {
        return sfcxglzh;
    }

    public void setSfcxglzh(String sfcxglzh) {
        this.sfcxglzh = sfcxglzh;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getBeiyzd01() {
        return beiyzd01;
    }

    public void setBeiyzd01(String beiyzd01) {
        this.beiyzd01 = beiyzd01;
    }

    public String getBeiyzd02() {
        return beiyzd02;
    }

    public void setBeiyzd02(String beiyzd02) {
        this.beiyzd02 = beiyzd02;
    }

    public String getJiymkzbz() {
        return jiymkzbz;
    }

    public void setJiymkzbz(String jiymkzbz) {
        this.jiymkzbz = jiymkzbz;
    }

    public String getZhhufenl() {
        return zhhufenl;
    }

    public void setZhhufenl(String zhhufenl) {
        this.zhhufenl = zhhufenl;
    }

    public String getZhshuxin() {
        return zhshuxin;
    }

    public void setZhshuxin(String zhshuxin) {
        this.zhshuxin = zhshuxin;
    }


    @Override
    public String toString() {
        return "Dp2021ReqDto{" +
                "xzsxnzhi='" + xzsxnzhi + '\'' +
                ", chaxleix='" + chaxleix + '\'' +
                ", chaxfanw='" + chaxfanw + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", zhanghao='" + zhanghao + '\'' +
                ", dinhuobz='" + dinhuobz + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", cunkzlei='" + cunkzlei + '\'' +
                ", huobdaih='" + huobdaih + '\'' +
                ", chaohubz='" + chaohubz + '\'' +
                ", zhcxzhzt='" + zhcxzhzt + '\'' +
                ", chaxmima='" + chaxmima + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", shifoudy='" + shifoudy + '\'' +
                ", jigouhao='" + jigouhao + '\'' +
                ", mimazlei='" + mimazlei + '\'' +
                ", sfcxglzh='" + sfcxglzh + '\'' +
                ", zhjnzlei='" + zhjnzlei + '\'' +
                ", zhjhaoma='" + zhjhaoma + '\'' +
                ", kehuzhmc='" + kehuzhmc + '\'' +
                ", beiyzd01='" + beiyzd01 + '\'' +
                ", beiyzd02='" + beiyzd02 + '\'' +
                ", jiymkzbz='" + jiymkzbz + '\'' +
                ", zhhufenl='" + zhhufenl + '\'' +
                ", zhshuxin='" + zhshuxin + '\'' +
                '}';
    }
}
