package cn.com.yusys.yusp.dto.server.biz.xdht0037.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "firstpayPerc")
    private BigDecimal firstpayPerc;//首付款比例

    public BigDecimal getFirstpayPerc() {
        return firstpayPerc;
    }

    public void setFirstpayPerc(BigDecimal firstpayPerc) {
        this.firstpayPerc = firstpayPerc;
    }

    @Override
    public String toString() {
        return "Data{" +
                "firstpayPerc=" + firstpayPerc +
                '}';
    }
}
