package cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/28 12:39
 * @since 2021/8/28 12:39
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "hstrdt")
    private String hstrdt;//业务受理日期
    @JsonProperty(value = "hstrsq")
    private String hstrsq;//业务受理编号
    @JsonProperty(value = "opertp")
    private String opertp;//业务类型
    @JsonProperty(value = "tranam")
    private BigDecimal tranam;//交易金额
    @JsonProperty(value = "jysts")
    private String jysts;//新交易状态（未翻译）
    @JsonProperty(value = "transt")
    private String transt;//交易状态

    public String getHstrdt() {
        return hstrdt;
    }

    public void setHstrdt(String hstrdt) {
        this.hstrdt = hstrdt;
    }

    public String getHstrsq() {
        return hstrsq;
    }

    public void setHstrsq(String hstrsq) {
        this.hstrsq = hstrsq;
    }

    public String getOpertp() {
        return opertp;
    }

    public void setOpertp(String opertp) {
        this.opertp = opertp;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getJysts() {
        return jysts;
    }

    public void setJysts(String jysts) {
        this.jysts = jysts;
    }

    public String getTranst() {
        return transt;
    }

    public void setTranst(String transt) {
        this.transt = transt;
    }

    @Override
    public String toString() {
        return "List{" +
                "hstrdt='" + hstrdt + '\'' +
                ", hstrsq='" + hstrsq + '\'' +
                ", opertp='" + opertp + '\'' +
                ", tranam=" + tranam +
                ", jysts='" + jysts + '\'' +
                ", transt='" + transt + '\'' +
                '}';
    }
}
