package cn.com.yusys.yusp.dto.client.esb.core.ln3079;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款产品变更
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3079ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "xinchpdm")
    private String xinchpdm;//新产品代码
    @JsonProperty(value = "xinchpmc")
    private String xinchpmc;//新产品名称
    @JsonProperty(value = "chuzhhao")
    private String chuzhhao;//出账号
    @JsonProperty(value = "kuaijilb")
    private String kuaijilb;//会计类别
    @JsonProperty(value = "fuhejgou")
    private String fuhejgou;//复核机构
    @JsonProperty(value = "yewusx01")
    private String yewusx01;//业务属性1
    @JsonProperty(value = "yewusx02")
    private String yewusx02;//业务属性2
    @JsonProperty(value = "yewusx03")
    private String yewusx03;//业务属性3
    @JsonProperty(value = "yewusx04")
    private String yewusx04;//业务属性4
    @JsonProperty(value = "yewusx05")
    private String yewusx05;//业务属性5

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getXinchpdm() {
        return xinchpdm;
    }

    public void setXinchpdm(String xinchpdm) {
        this.xinchpdm = xinchpdm;
    }

    public String getXinchpmc() {
        return xinchpmc;
    }

    public void setXinchpmc(String xinchpmc) {
        this.xinchpmc = xinchpmc;
    }

    public String getChuzhhao() {
        return chuzhhao;
    }

    public void setChuzhhao(String chuzhhao) {
        this.chuzhhao = chuzhhao;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getYewusx02() {
        return yewusx02;
    }

    public void setYewusx02(String yewusx02) {
        this.yewusx02 = yewusx02;
    }

    public String getYewusx03() {
        return yewusx03;
    }

    public void setYewusx03(String yewusx03) {
        this.yewusx03 = yewusx03;
    }

    public String getYewusx04() {
        return yewusx04;
    }

    public void setYewusx04(String yewusx04) {
        this.yewusx04 = yewusx04;
    }

    public String getYewusx05() {
        return yewusx05;
    }

    public void setYewusx05(String yewusx05) {
        this.yewusx05 = yewusx05;
    }

    @Override
    public String toString() {
        return "Ln3079ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "xinchpdm='" + xinchpdm + '\'' +
                "xinchpmc='" + xinchpmc + '\'' +
                "chuzhhao='" + chuzhhao + '\'' +
                "kuaijilb='" + kuaijilb + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                "yewusx01='" + yewusx01 + '\'' +
                "yewusx02='" + yewusx02 + '\'' +
                "yewusx03='" + yewusx03 + '\'' +
                "yewusx04='" + yewusx04 + '\'' +
                "yewusx05='" + yewusx05 + '\'' +
                '}';
    }
}  
