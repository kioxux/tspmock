package cn.com.yusys.yusp.dto.client.esb.irs.xirs15.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：评级在途申请标识
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xirs15RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "wayflag")
    private String wayflag;//评级在途标识

    public String getWayflag() {
        return wayflag;
    }

    public void setWayflag(String wayflag) {
        this.wayflag = wayflag;
    }

    @Override
    public String toString() {
        return "Xirs15RespDto{" +
                "wayflag='" + wayflag + '\'' +
                '}';
    }
}  
