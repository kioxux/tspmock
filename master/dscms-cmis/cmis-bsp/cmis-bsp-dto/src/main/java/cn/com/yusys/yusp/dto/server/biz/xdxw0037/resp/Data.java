package cn.com.yusys.yusp.dto.server.biz.xdxw0037.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询借款人是否存在未完成的贷后检查任务
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isExist")
    private String isExist;//是否存在未完成的贷后检查任务
    @JsonProperty(value = "firstCheckNum")
    private Integer firstCheckNum;//首检笔数
    @JsonProperty(value = "regCheckNum")
    private Integer regCheckNum;//定检笔数
    @JsonProperty(value = "noRegCheckNum")
    private Integer noRegCheckNum;//不定期检查笔数

    public String getIsExist() {
        return isExist;
    }

    public void setIsExist(String isExist) {
        this.isExist = isExist;
    }

    public Integer getFirstCheckNum() {
        return firstCheckNum;
    }

    public void setFirstCheckNum(Integer firstCheckNum) {
        this.firstCheckNum = firstCheckNum;
    }

    public Integer getRegCheckNum() {
        return regCheckNum;
    }

    public void setRegCheckNum(Integer regCheckNum) {
        this.regCheckNum = regCheckNum;
    }

    public Integer getNoRegCheckNum() {
        return noRegCheckNum;
    }

    public void setNoRegCheckNum(Integer noRegCheckNum) {
        this.noRegCheckNum = noRegCheckNum;
    }

    @Override
    public String toString() {
        return "Xdxw0037DataRespDto{" +
                "isExist='" + isExist + '\'' +
                "firstCheckNum='" + firstCheckNum + '\'' +
                "regCheckNum='" + regCheckNum + '\'' +
                "noRegCheckNum='" + noRegCheckNum + '\'' +
                '}';
    }
}  
