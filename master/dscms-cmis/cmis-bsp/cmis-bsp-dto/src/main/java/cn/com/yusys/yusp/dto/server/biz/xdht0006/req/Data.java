package cn.com.yusys.yusp.dto.server.biz.xdht0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;


/**
 * 请求Dto：双录流水与合同号同步
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "docNo")
    private String docNo;//档案号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "date")
    private String date;//日期

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Data{" +
                "docNo='" + docNo + '\'' +
                "contNo='" + contNo + '\'' +
                "date='" + date + '\'' +
                '}';
    }
}
