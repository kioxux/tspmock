package cn.com.yusys.yusp.dto.client.esb.ecif.g00202;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/*
 * 响应DTO：同业客户开户
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class G00202RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custno")
    private String custno;//客户编号
    @JsonProperty(value = "tsywsq")
    private String tsywsq;//特殊业务流水号
    @JsonProperty(value = "prcstx")
    private String prcstx;//业务对比字段
    @JsonProperty(value = "cropcd")
    private String cropcd;//组织机构代码
    @JsonProperty(value = "nnjytp")
    private String nnjytp;//交易类型
    @JsonProperty(value = "nnjyna")
    private String nnjyna;//交易名称
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//核心流水

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getTsywsq() {
        return tsywsq;
    }

    public void setTsywsq(String tsywsq) {
        this.tsywsq = tsywsq;
    }

    public String getPrcstx() {
        return prcstx;
    }

    public void setPrcstx(String prcstx) {
        this.prcstx = prcstx;
    }

    public String getCropcd() {
        return cropcd;
    }

    public void setCropcd(String cropcd) {
        this.cropcd = cropcd;
    }

    public String getNnjytp() {
        return nnjytp;
    }

    public void setNnjytp(String nnjytp) {
        this.nnjytp = nnjytp;
    }

    public String getNnjyna() {
        return nnjyna;
    }

    public void setNnjyna(String nnjyna) {
        this.nnjyna = nnjyna;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    @Override
    public String toString() {
        return "G00202RespDto{" +
                "custno='" + custno + '\'' +
                ", tsywsq='" + tsywsq + '\'' +
                ", prcstx='" + prcstx + '\'' +
                ", cropcd='" + cropcd + '\'' +
                ", nnjytp='" + nnjytp + '\'' +
                ", nnjyna='" + nnjyna + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                '}';
    }
}
