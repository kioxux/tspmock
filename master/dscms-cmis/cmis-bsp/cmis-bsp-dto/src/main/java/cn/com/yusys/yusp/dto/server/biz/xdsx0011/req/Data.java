package cn.com.yusys.yusp.dto.server.biz.xdsx0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：专业贷款评级结果同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//贷款用途
    @JsonProperty(value = "cusId")
    private String cusId;//客户号

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contNo='" + contNo + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
