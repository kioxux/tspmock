package cn.com.yusys.yusp.dto.server.biz.xdcz0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：省心E付放款记录列表查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名
    @JsonProperty(value = "billNo")
    private String billNo;//借据名
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//借款到期日
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//借款用途
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "preferSurplusTimes")
    private String preferSurplusTimes;//优会剩余次数
    @JsonProperty(value = "preferPoints")
    private BigDecimal preferPoints;//优惠点数
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//借据金额
    @JsonProperty(value = "repayAcctNo")
    private String repayAcctNo;//还款账号
    @JsonProperty(value = "acctName")
    private String acctName;//账户名称
    @JsonProperty(value = "replacePayoutMon")
    private String replacePayoutMon;//代发月份
    @JsonProperty(value = "indivSalAMt")
    private BigDecimal indivSalAMt;//工资总金额
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行利率
    @JsonProperty(value = "amtUcase")
    private String amtUcase;//金额大写
    @JsonProperty(value = "loan")
    private String loan;//放款时间

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getPreferSurplusTimes() {
        return preferSurplusTimes;
    }

    public void setPreferSurplusTimes(String preferSurplusTimes) {
        this.preferSurplusTimes = preferSurplusTimes;
    }

    public BigDecimal getPreferPoints() {
        return preferPoints;
    }

    public void setPreferPoints(BigDecimal preferPoints) {
        this.preferPoints = preferPoints;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getRepayAcctNo() {
        return repayAcctNo;
    }

    public void setRepayAcctNo(String repayAcctNo) {
        this.repayAcctNo = repayAcctNo;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getReplacePayoutMon() {
        return replacePayoutMon;
    }

    public void setReplacePayoutMon(String replacePayoutMon) {
        this.replacePayoutMon = replacePayoutMon;
    }

    public BigDecimal getIndivSalAMt() {
        return indivSalAMt;
    }

    public void setIndivSalAMt(BigDecimal indivSalAMt) {
        this.indivSalAMt = indivSalAMt;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public String getAmtUcase() {
        return amtUcase;
    }

    public void setAmtUcase(String amtUcase) {
        this.amtUcase = amtUcase;
    }

    public String getLoan() {
        return loan;
    }

    public void setLoan(String loan) {
        this.loan = loan;
    }

    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", billNo='" + billNo + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", loanUseType='" + loanUseType + '\'' +
                ", contAmt=" + contAmt +
                ", preferSurplusTimes='" + preferSurplusTimes + '\'' +
                ", preferPoints=" + preferPoints +
                ", loanAmt=" + loanAmt +
                ", repayAcctNo='" + repayAcctNo + '\'' +
                ", acctName='" + acctName + '\'' +
                ", replacePayoutMon='" + replacePayoutMon + '\'' +
                ", indivSalAMt=" + indivSalAMt +
                ", realityIrY=" + realityIrY +
                ", amtUcase='" + amtUcase + '\'' +
                ", loan='" + loan + '\'' +
                '}';
    }
}
