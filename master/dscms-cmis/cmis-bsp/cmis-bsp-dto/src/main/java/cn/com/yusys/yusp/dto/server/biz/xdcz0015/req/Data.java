package cn.com.yusys.yusp.dto.server.biz.xdcz0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 请求Dto：银票影像补录同步
 *
 * @author xull
 * @version 1.0
 */
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "videoNo")
    private String videoNo;//影像编号
    @JsonProperty(value = "isCollect")
    private String isCollect;//是否收集

    public String getVideoNo() {
        return videoNo;
    }

    public void setVideoNo(String videoNo) {
        this.videoNo = videoNo;
    }

    public String getIsCollect() {
        return isCollect;
    }

    public void setIsCollect(String isCollect) {
        this.isCollect = isCollect;
    }

    @Override
    public String toString() {
        return "Data{" +
                "videoNo='" + videoNo + '\'' +
                "isCollect='" + isCollect + '\'' +
                '}';
    }
}
