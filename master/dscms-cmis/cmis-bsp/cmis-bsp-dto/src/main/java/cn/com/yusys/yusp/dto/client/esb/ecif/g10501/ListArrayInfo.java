package cn.com.yusys.yusp.dto.client.esb.ecif.g10501;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：对公及同业客户清单查询-清单客户信息_ARRAY
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class ListArrayInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custno")
    private String custno;//       客户编号
    @JsonProperty(value = "custna")
    private String custna;//       客户名称
    @JsonProperty(value = "custst")
    private String custst;//       客户状态
    @JsonProperty(value = "cridno")
    private String cridno;//       社会统一信用代码
    @JsonProperty(value = "cpopcd")
    private String cpopcd;//       组织机构代码
    @JsonProperty(value = "gvrgno")
    private String gvrgno;//       营业执照号码
    @JsonProperty(value = "creabr")
    private String creabr;//       开户机构
    @JsonProperty(value = "userid")
    private String userid;//       开户柜员
    @JsonProperty(value = "opendt")
    private String opendt;//       开户时间
    @JsonProperty(value = "homead")
    private String homead;//       地址
    @JsonProperty(value = "locpho")
    private String locpho;//       电话

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    public String getCridno() {
        return cridno;
    }

    public void setCridno(String cridno) {
        this.cridno = cridno;
    }

    public String getCpopcd() {
        return cpopcd;
    }

    public void setCpopcd(String cpopcd) {
        this.cpopcd = cpopcd;
    }

    public String getGvrgno() {
        return gvrgno;
    }

    public void setGvrgno(String gvrgno) {
        this.gvrgno = gvrgno;
    }

    public String getCreabr() {
        return creabr;
    }

    public void setCreabr(String creabr) {
        this.creabr = creabr;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOpendt() {
        return opendt;
    }

    public void setOpendt(String opendt) {
        this.opendt = opendt;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getLocpho() {
        return locpho;
    }

    public void setLocpho(String locpho) {
        this.locpho = locpho;
    }

    @Override
    public String toString() {
        return "ListArrayInfo{" +
                "custno='" + custno + '\'' +
                ", custna='" + custna + '\'' +
                ", custst='" + custst + '\'' +
                ", cridno='" + cridno + '\'' +
                ", cpopcd='" + cpopcd + '\'' +
                ", gvrgno='" + gvrgno + '\'' +
                ", creabr='" + creabr + '\'' +
                ", userid='" + userid + '\'' +
                ", opendt='" + opendt + '\'' +
                ", homead='" + homead + '\'' +
                ", locpho='" + locpho + '\'' +
                '}';
    }
}
