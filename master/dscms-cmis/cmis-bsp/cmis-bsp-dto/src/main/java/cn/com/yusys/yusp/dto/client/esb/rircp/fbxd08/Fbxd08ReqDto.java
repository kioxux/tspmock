package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd08;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查找（利翃）实还正常本金和逾期本金之和
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd08ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contract_no")
    private String contract_no;//利羽平台贷款合同号

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Fbxd08ReqDto{" +
                "contract_no='" + contract_no + '\'' +
                '}';
    }
}  
