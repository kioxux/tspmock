package cn.com.yusys.yusp.dto.client.esb.ecif.s00101;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：对私客户综合信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class S00101ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "resotp")
    private String resotp;//识别方式
    @JsonProperty(value = "custno")
    private String custno;//客户编号
    @JsonProperty(value = "idtftp")
    private String idtftp;//证件类型
    @JsonProperty(value = "idtfno")
    private String idtfno;//证件号码
    @JsonProperty(value = "custna")
    private String custna;//客户名称
    @JsonProperty(value = "custst")
    private String custst;//客户状态

    public String getResotp() {
        return resotp;
    }

    public void setResotp(String resotp) {
        this.resotp = resotp;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    @Override
    public String toString() {
        return "S00101ReqDto{" +
                "resotp='" + resotp + '\'' +
                ", custno='" + custno + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", custna='" + custna + '\'' +
                ", custst='" + custst + '\'' +
                '}';
    }
}