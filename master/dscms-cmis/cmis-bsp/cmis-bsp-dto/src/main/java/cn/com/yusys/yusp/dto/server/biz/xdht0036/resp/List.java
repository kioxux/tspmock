package cn.com.yusys.yusp.dto.server.biz.xdht0036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "serno")
    private String serno;//业务申请流水号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "prdCode")
    private String prdCode;//产品主键标识
    @JsonProperty(value = "bizType")
    private String bizType;//业务品种
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getPrdCode() {
        return prdCode;
    }

    public void setPrdCode(String prdCode) {
        this.prdCode = prdCode;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    @Override
    public String toString() {
        return "List{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", contNo='" + contNo + '\'' +
                ", serno='" + serno + '\'' +
                ", contType='" + contType + '\'' +
                ", prdCode='" + prdCode + '\'' +
                ", bizType='" + bizType + '\'' +
                ", prdName='" + prdName + '\'' +
                '}';
    }
}
