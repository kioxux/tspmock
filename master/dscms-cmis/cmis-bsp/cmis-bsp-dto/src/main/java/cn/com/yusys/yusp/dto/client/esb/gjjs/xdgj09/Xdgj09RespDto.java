package cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：信贷查询牌价信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdgj09RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "rpp")
    private String rpp;//汇买价
    @JsonProperty(value = "rsp")
    private String rsp;//汇卖价
    @JsonProperty(value = "cpp")
    private BigDecimal cpp;//钞买价
    @JsonProperty(value = "csp")
    private BigDecimal csp;//钞卖价
    @JsonProperty(value = "baserate")
    private BigDecimal baserate;//基准价
    @JsonProperty(value = "midrate")
    private BigDecimal midrate;//中间价

    public String getRpp() {
        return rpp;
    }

    public void setRpp(String rpp) {
        this.rpp = rpp;
    }

    public String getRsp() {
        return rsp;
    }

    public void setRsp(String rsp) {
        this.rsp = rsp;
    }

    public BigDecimal getCpp() {
        return cpp;
    }

    public void setCpp(BigDecimal cpp) {
        this.cpp = cpp;
    }

    public BigDecimal getCsp() {
        return csp;
    }

    public void setCsp(BigDecimal csp) {
        this.csp = csp;
    }

    public BigDecimal getBaserate() {
        return baserate;
    }

    public void setBaserate(BigDecimal baserate) {
        this.baserate = baserate;
    }

    public BigDecimal getMidrate() {
        return midrate;
    }

    public void setMidrate(BigDecimal midrate) {
        this.midrate = midrate;
    }

    @Override
    public String toString() {
        return "Xdgj09RespDto{" +
                "rpp='" + rpp + '\'' +
                "rsp='" + rsp + '\'' +
                "cpp='" + cpp + '\'' +
                "csp='" + csp + '\'' +
                "baserate='" + baserate + '\'' +
                "midrate='" + midrate + '\'' +
                '}';
    }
}  
