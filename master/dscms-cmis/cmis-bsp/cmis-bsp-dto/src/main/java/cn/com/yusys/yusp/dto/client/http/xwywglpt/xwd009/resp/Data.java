package cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/17 14:08
 * @since 2021/7/17 14:08
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    private String jsonObject;

    public String getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(String jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    public String toString() {
        return "Data{" +
                "jsonObject='" + jsonObject + '\'' +
                '}';
    }
}
