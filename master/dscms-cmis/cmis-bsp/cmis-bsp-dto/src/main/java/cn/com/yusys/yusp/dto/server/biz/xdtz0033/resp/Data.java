package cn.com.yusys.yusp.dto.server.biz.xdtz0033.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 查询客户所担保的行内贷款五级分类非正常状态件数
 * @Author zhangpeng
 * @Date 2021/4/27 15:12
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "grtLoanNotNormalCnt")
    private String grtLoanNotNormalCnt;//所担保的行内贷款五级分类非正常状态件数

    public String getGrtLoanNotNormalCnt() {
        return grtLoanNotNormalCnt;
    }

    public void setGrtLoanNotNormalCnt(String grtLoanNotNormalCnt) {
        this.grtLoanNotNormalCnt = grtLoanNotNormalCnt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "grtLoanNotNormalCnt='" + grtLoanNotNormalCnt + '\'' +
                '}';
    }
}
