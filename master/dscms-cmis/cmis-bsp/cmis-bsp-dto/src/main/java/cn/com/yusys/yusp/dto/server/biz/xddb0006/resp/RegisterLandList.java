package cn.com.yusys.yusp.dto.server.biz.xddb0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 10:03
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class RegisterLandList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "laarea")
    private String laarea;//登记簿土地信息-土地面积
    @JsonProperty(value = "lanuse")
    private String lanuse;//登记簿土地信息-土地用途
    @JsonProperty(value = "landna")
    private String landna;//登记簿土地信息-土地性质
    @JsonProperty(value = "lanrsd")
    private String lanrsd;//登记簿土地信息-土地使用权开始
    @JsonProperty(value = "lanred")
    private String lanred;//登记簿土地信息-土地使用权结束
    @JsonProperty(value = "lanloc")
    private String lanloc;//登记簿土地信息-坐落

    public String getLaarea() {
        return laarea;
    }

    public void setLaarea(String laarea) {
        this.laarea = laarea;
    }

    public String getLanuse() {
        return lanuse;
    }

    public void setLanuse(String lanuse) {
        this.lanuse = lanuse;
    }

    public String getLandna() {
        return landna;
    }

    public void setLandna(String landna) {
        this.landna = landna;
    }

    public String getLanrsd() {
        return lanrsd;
    }

    public void setLanrsd(String lanrsd) {
        this.lanrsd = lanrsd;
    }

    public String getLanred() {
        return lanred;
    }

    public void setLanred(String lanred) {
        this.lanred = lanred;
    }

    public String getLanloc() {
        return lanloc;
    }

    public void setLanloc(String lanloc) {
        this.lanloc = lanloc;
    }

    @Override
    public String toString() {
        return "RegisterLandList{" +
                "laarea='" + laarea + '\'' +
                ", lanuse='" + lanuse + '\'' +
                ", landna='" + landna + '\'' +
                ", lanrsd='" + lanrsd + '\'' +
                ", lanred='" + lanred + '\'' +
                ", lanloc='" + lanloc + '\'' +
                '}';
    }
}
