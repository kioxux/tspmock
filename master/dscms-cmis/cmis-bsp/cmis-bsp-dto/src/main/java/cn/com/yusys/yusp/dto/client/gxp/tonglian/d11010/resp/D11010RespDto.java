package cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：委托清收变更
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D11010RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "idtftp")
    private String idtftp;//证件类型
    @JsonProperty(value = "idtfno")
    private String idtfno;//证件号码
    @JsonProperty(value = "cardno")
    private String cardno;//卡号
    @JsonProperty(value = "opt")
    private String opt;//操作行动码
    @JsonProperty(value = "title")
    private String title;//职务
    @JsonProperty(value = "custnm")
    private String custnm;//姓名
    @JsonProperty(value = "gender")
    private String gender;//性别
    @JsonProperty(value = "ocpatn")
    private String ocpatn;//职业
    @JsonProperty(value = "bkmrno")
    private String bkmrno;//本行员工号
    @JsonProperty(value = "ntnaty")
    private String ntnaty;//国籍
    @JsonProperty(value = "mtstat")
    private String mtstat;//婚姻状况
    @JsonProperty(value = "qlftin")
    private String qlftin;//教育状况
    @JsonProperty(value = "hmphon")
    private String hmphon;//家庭电话
    @JsonProperty(value = "mobile")
    private String mobile;//移动电话
    @JsonProperty(value = "email")
    private String email;//电子邮箱
    @JsonProperty(value = "corpnm")
    private String corpnm;//公司名称
    @JsonProperty(value = "embnam")
    private String embnam;//凸印姓名
    @JsonProperty(value = "iedate")
    private String iedate;//证件到期日
    @JsonProperty(value = "idaddr")
    private String idaddr;//发证机关所在地址
    @JsonProperty(value = "brtday")
    private String brtday;//生日
    @JsonProperty(value = "hourst")
    private String hourst;//房屋持有类型
    @JsonProperty(value = "hourpe")
    private String hourpe;//住宅类型
    @JsonProperty(value = "homrom")
    private String homrom;//现住址居住起始年月
    @JsonProperty(value = "emptus")
    private String emptus;//就业状态
    @JsonProperty(value = "empity")
    private String empity;//工作稳定性
    @JsonProperty(value = "titlal")
    private String titlal;//职称
    @JsonProperty(value = "income")
    private String income;//年收入
    @JsonProperty(value = "isdate")
    private String isdate;//证件起始日

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getOpt() {
        return opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOcpatn() {
        return ocpatn;
    }

    public void setOcpatn(String ocpatn) {
        this.ocpatn = ocpatn;
    }

    public String getBkmrno() {
        return bkmrno;
    }

    public void setBkmrno(String bkmrno) {
        this.bkmrno = bkmrno;
    }

    public String getNtnaty() {
        return ntnaty;
    }

    public void setNtnaty(String ntnaty) {
        this.ntnaty = ntnaty;
    }

    public String getMtstat() {
        return mtstat;
    }

    public void setMtstat(String mtstat) {
        this.mtstat = mtstat;
    }

    public String getQlftin() {
        return qlftin;
    }

    public void setQlftin(String qlftin) {
        this.qlftin = qlftin;
    }

    public String getHmphon() {
        return hmphon;
    }

    public void setHmphon(String hmphon) {
        this.hmphon = hmphon;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCorpnm() {
        return corpnm;
    }

    public void setCorpnm(String corpnm) {
        this.corpnm = corpnm;
    }

    public String getEmbnam() {
        return embnam;
    }

    public void setEmbnam(String embnam) {
        this.embnam = embnam;
    }

    public String getIedate() {
        return iedate;
    }

    public void setIedate(String iedate) {
        this.iedate = iedate;
    }

    public String getIdaddr() {
        return idaddr;
    }

    public void setIdaddr(String idaddr) {
        this.idaddr = idaddr;
    }

    public String getBrtday() {
        return brtday;
    }

    public void setBrtday(String brtday) {
        this.brtday = brtday;
    }

    public String getHourst() {
        return hourst;
    }

    public void setHourst(String hourst) {
        this.hourst = hourst;
    }

    public String getHourpe() {
        return hourpe;
    }

    public void setHourpe(String hourpe) {
        this.hourpe = hourpe;
    }

    public String getHomrom() {
        return homrom;
    }

    public void setHomrom(String homrom) {
        this.homrom = homrom;
    }

    public String getEmptus() {
        return emptus;
    }

    public void setEmptus(String emptus) {
        this.emptus = emptus;
    }

    public String getEmpity() {
        return empity;
    }

    public void setEmpity(String empity) {
        this.empity = empity;
    }

    public String getTitlal() {
        return titlal;
    }

    public void setTitlal(String titlal) {
        this.titlal = titlal;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getIsdate() {
        return isdate;
    }

    public void setIsdate(String isdate) {
        this.isdate = isdate;
    }

    @Override
    public String toString() {
        return "D11010RespDto{" +
                "idtftp='" + idtftp + '\'' +
                "idtfno='" + idtfno + '\'' +
                "cardno='" + cardno + '\'' +
                "opt='" + opt + '\'' +
                "title='" + title + '\'' +
                "custnm='" + custnm + '\'' +
                "gender='" + gender + '\'' +
                "ocpatn='" + ocpatn + '\'' +
                "bkmrno='" + bkmrno + '\'' +
                "ntnaty='" + ntnaty + '\'' +
                "mtstat='" + mtstat + '\'' +
                "qlftin='" + qlftin + '\'' +
                "hmphon='" + hmphon + '\'' +
                "mobile='" + mobile + '\'' +
                "email='" + email + '\'' +
                "corpnm='" + corpnm + '\'' +
                "embnam='" + embnam + '\'' +
                "iedate='" + iedate + '\'' +
                "idaddr='" + idaddr + '\'' +
                "brtday='" + brtday + '\'' +
                "hourst='" + hourst + '\'' +
                "hourpe='" + hourpe + '\'' +
                "homrom='" + homrom + '\'' +
                "emptus='" + emptus + '\'' +
                "empity='" + empity + '\'' +
                "titlal='" + titlal + '\'' +
                "income='" + income + '\'' +
                "isdate='" + isdate + '\'' +
                '}';
    }
}  
