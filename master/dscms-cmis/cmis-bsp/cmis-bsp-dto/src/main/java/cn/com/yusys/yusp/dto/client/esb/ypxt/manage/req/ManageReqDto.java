package cn.com.yusys.yusp.dto.client.esb.ypxt.manage.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：管护权移交信息同步
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ManageReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yptybh")
    private String yptybh;//押品统一编号
    @JsonProperty(value = "yghxth")
    private String yghxth;//原管护人ID
    @JsonProperty(value = "yjghxt")
    private String yjghxt;//移交后管护系统ID
    @JsonProperty(value = "yjghid")
    private String yjghid;//移交后管户人ID
    @JsonProperty(value = "yjjgid")
    private String yjjgid;//移交后管户机构ID

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getYghxth() {
        return yghxth;
    }

    public void setYghxth(String yghxth) {
        this.yghxth = yghxth;
    }

    public String getYjghxt() {
        return yjghxt;
    }

    public void setYjghxt(String yjghxt) {
        this.yjghxt = yjghxt;
    }

    public String getYjghid() {
        return yjghid;
    }

    public void setYjghid(String yjghid) {
        this.yjghid = yjghid;
    }

    public String getYjjgid() {
        return yjjgid;
    }

    public void setYjjgid(String yjjgid) {
        this.yjjgid = yjjgid;
    }

    @Override
    public String toString() {
        return "ManageReqDto{" +
                "yptybh='" + yptybh + '\'' +
                "yghxth='" + yghxth + '\'' +
                "yjghxt='" + yjghxt + '\'' +
                "yjghid='" + yjghid + '\'' +
                "yjjgid='" + yjjgid + '\'' +
                '}';
    }
}  
