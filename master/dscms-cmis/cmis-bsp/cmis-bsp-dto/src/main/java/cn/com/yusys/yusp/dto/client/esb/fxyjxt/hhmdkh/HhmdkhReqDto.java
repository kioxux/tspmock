package cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询客户是否为黑灰名单客户
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class HhmdkhReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custnm")
    private String custnm;//客户名称

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    @Override
    public String toString() {
        return "HhmdkhReqDto{" +
                "custnm='" + custnm + '\'' +
                '}';
    }
}
