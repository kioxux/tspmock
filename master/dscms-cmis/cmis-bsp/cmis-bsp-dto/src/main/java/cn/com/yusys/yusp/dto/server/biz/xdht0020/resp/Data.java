package cn.com.yusys.yusp.dto.server.biz.xdht0020.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 11:27
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同号
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "dayInt")
    private BigDecimal dayInt;//日息
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同结束日期
    @JsonProperty(value = "rate")
    private BigDecimal rate;//执行利率
    @JsonProperty(value = "avlAmt")
    private BigDecimal avlAmt;//可用额度
    @JsonProperty(value = "lprRate")
    private BigDecimal lprRate;//lpr基准利率
    @JsonProperty(value = "lprBp")
    private BigDecimal lprBp;//lprBP
    @JsonProperty(value = "lprTerm")
    private String lprTerm;//lpr期限
    @JsonProperty(value = "lprBpType")
    private String lprBpType;//lprBp类型
    @JsonProperty(value = "repayDate")
    private String repayDate;//还款日
    @JsonProperty(value = "preferCode")
    private String preferCode;//优惠券编码
    @JsonProperty(value = "replyContEndDate")
    private String replyContEndDate;//批复表中合同到期日
    @JsonProperty(value = "repayMode")
    private String repayMode;//还款方式
    @JsonProperty(value = "cusName")
    private String cusName;//共借人姓名
    @JsonProperty(value = "certNo")
    private String certNo;//共借人证件号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "contSignStatus")
    private String contSignStatus;//合同签订状态
    @JsonProperty(value = "pkId")
    private String pkId;//客户调查主键
    @JsonProperty(value = "oldContNo")
    private String oldContNo;//原合同号
    @JsonProperty(value = "oldSurveySerno")
    private String oldSurveySerno;//原调查流水

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public BigDecimal getDayInt() {
        return dayInt;
    }

    public void setDayInt(BigDecimal dayInt) {
        this.dayInt = dayInt;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getAvlAmt() {
        return avlAmt;
    }

    public void setAvlAmt(BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    public BigDecimal getLprRate() {
        return lprRate;
    }

    public void setLprRate(BigDecimal lprRate) {
        this.lprRate = lprRate;
    }

    public BigDecimal getLprBp() {
        return lprBp;
    }

    public void setLprBp(BigDecimal lprBp) {
        this.lprBp = lprBp;
    }

    public String getLprTerm() {
        return lprTerm;
    }

    public void setLprTerm(String lprTerm) {
        this.lprTerm = lprTerm;
    }

    public String getLprBpType() {
        return lprBpType;
    }

    public void setLprBpType(String lprBpType) {
        this.lprBpType = lprBpType;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }

    public String getPreferCode() {
        return preferCode;
    }

    public void setPreferCode(String preferCode) {
        this.preferCode = preferCode;
    }

    public String getReplyContEndDate() {
        return replyContEndDate;
    }

    public void setReplyContEndDate(String replyContEndDate) {
        this.replyContEndDate = replyContEndDate;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getContSignStatus() {
        return contSignStatus;
    }

    public void setContSignStatus(String contSignStatus) {
        this.contSignStatus = contSignStatus;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getOldContNo() {
        return oldContNo;
    }

    public void setOldContNo(String oldContNo) {
        this.oldContNo = oldContNo;
    }

    public String getOldSurveySerno() {
        return oldSurveySerno;
    }

    public void setOldSurveySerno(String oldSurveySerno) {
        this.oldSurveySerno = oldSurveySerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contNo='" + contNo + '\'' +
                "cnContNo='" + cnContNo + '\'' +
                "curType='" + curType + '\'' +
                "applyAmt='" + applyAmt + '\'' +
                "dayInt='" + dayInt + '\'' +
                "contStartDate='" + contStartDate + '\'' +
                "contEndDate='" + contEndDate + '\'' +
                "rate='" + rate + '\'' +
                "avlAmt='" + avlAmt + '\'' +
                "lprRate='" + lprRate + '\'' +
                "lprBp='" + lprBp + '\'' +
                "lprTerm='" + lprTerm + '\'' +
                "lprBpType='" + lprBpType + '\'' +
                "repayDate='" + repayDate + '\'' +
                "preferCode='" + preferCode + '\'' +
                "replyContEndDate='" + replyContEndDate + '\'' +
                "repayMode='" + repayMode + '\'' +
                "cusName='" + cusName + '\'' +
                "certNo='" + certNo + '\'' +
                "contType='" + contType + '\'' +
                "contSignStatus='" + contSignStatus + '\'' +
                "pkId='" + pkId + '\'' +
                '}';
    }
}
