package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi12;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：ESB通用查询接口（处理码credi12）
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Credi12RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "reportId")
    private String reportId;//	报告编号	;	否	;	报告编号
    @JsonProperty(value = "reqId")
    private String reqId;//	业务明细ID	;	是	;	本次查询交易流水号，区分查询交易的唯一标识

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }
}
