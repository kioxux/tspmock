package cn.com.yusys.yusp.dto.client.esb.core.ln3020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：抵押关联信息
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkdygl implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "glyewuzl")
    private String glyewuzl;//关联业务种类
    @JsonProperty(value = "dbaofshi")
    private String dbaofshi;//担保方式
    @JsonProperty(value = "dzywbhao")
    private String dzywbhao;//抵质押物编号
    @JsonProperty(value = "dbaokzzl")
    private String dbaokzzl;//担保控制种类
    @JsonProperty(value = "dbaobili")
    private BigDecimal dbaobili;//担保比例
    @JsonProperty(value = "lddbkzhi")
    private String lddbkzhi;//联动担保控制
    @JsonProperty(value = "dbaobizh")
    private String dbaobizh;//担保币种
    @JsonProperty(value = "dbaojine")
    private BigDecimal dbaojine;//担保金额
    @JsonProperty(value = "jijubizh")
    private String jijubizh;//借据币种
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "zhshuilv")
    private BigDecimal zhshuilv;//折算汇率
    @JsonProperty(value = "guanlzht")
    private String guanlzht;//关联状态
    @JsonProperty(value = "mingxixh")
    private String mingxixh;//明细序号

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getGlyewuzl() {
        return glyewuzl;
    }

    public void setGlyewuzl(String glyewuzl) {
        this.glyewuzl = glyewuzl;
    }

    public String getDbaofshi() {
        return dbaofshi;
    }

    public void setDbaofshi(String dbaofshi) {
        this.dbaofshi = dbaofshi;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDbaokzzl() {
        return dbaokzzl;
    }

    public void setDbaokzzl(String dbaokzzl) {
        this.dbaokzzl = dbaokzzl;
    }

    public BigDecimal getDbaobili() {
        return dbaobili;
    }

    public void setDbaobili(BigDecimal dbaobili) {
        this.dbaobili = dbaobili;
    }

    public String getLddbkzhi() {
        return lddbkzhi;
    }

    public void setLddbkzhi(String lddbkzhi) {
        this.lddbkzhi = lddbkzhi;
    }

    public String getDbaobizh() {
        return dbaobizh;
    }

    public void setDbaobizh(String dbaobizh) {
        this.dbaobizh = dbaobizh;
    }

    public BigDecimal getDbaojine() {
        return dbaojine;
    }

    public void setDbaojine(BigDecimal dbaojine) {
        this.dbaojine = dbaojine;
    }

    public String getJijubizh() {
        return jijubizh;
    }

    public void setJijubizh(String jijubizh) {
        this.jijubizh = jijubizh;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getZhshuilv() {
        return zhshuilv;
    }

    public void setZhshuilv(BigDecimal zhshuilv) {
        this.zhshuilv = zhshuilv;
    }

    public String getGuanlzht() {
        return guanlzht;
    }

    public void setGuanlzht(String guanlzht) {
        this.guanlzht = guanlzht;
    }

    public String getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(String mingxixh) {
        this.mingxixh = mingxixh;
    }

    @Override
    public String toString() {
        return "Lstdkdygl{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", glyewuzl='" + glyewuzl + '\'' +
                ", dbaofshi='" + dbaofshi + '\'' +
                ", dzywbhao='" + dzywbhao + '\'' +
                ", dbaokzzl='" + dbaokzzl + '\'' +
                ", dbaobili=" + dbaobili +
                ", lddbkzhi='" + lddbkzhi + '\'' +
                ", dbaobizh='" + dbaobizh + '\'' +
                ", dbaojine=" + dbaojine +
                ", jijubizh='" + jijubizh + '\'' +
                ", jiejuuje=" + jiejuuje +
                ", zhshuilv=" + zhshuilv +
                ", guanlzht='" + guanlzht + '\'' +
                ", mingxixh='" + mingxixh + '\'' +
                '}';
    }
}