package cn.com.yusys.yusp.dto.client.esb.core.ln3026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：贷款多还款账户
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkhkzh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ruzjigou")
    private String ruzjigou;//入账机构
    @JsonProperty(value = "bhchzibz")
    private String bhchzibz;//本行出资标志
    @JsonProperty(value = "lhdkleix")
    private String lhdkleix;//联合贷款类型
    @JsonProperty(value = "chzfkehh")
    private String chzfkehh;//出资方客户号
    @JsonProperty(value = "chzfkhmc")
    private String chzfkhmc;//出资方客户名称
    @JsonProperty(value = "chzfhhao")
    private String chzfhhao;//出资方行号
    @JsonProperty(value = "czfhming")
    private String czfhming;//出资方行名
    @JsonProperty(value = "chuzfzhh")
    private String chuzfzhh;//出资方账号
    @JsonProperty(value = "czfzhzxh")
    private String czfzhzxh;//出资方账号子序号
    @JsonProperty(value = "chzfzhmc")
    private String chzfzhmc;//出资方账户名称
    @JsonProperty(value = "chuzbili")
    private BigDecimal chuzbili;//出资比例
    @JsonProperty(value = "chuzjine")
    private BigDecimal chuzjine;//出资金额
    @JsonProperty(value = "shoukzhh")
    private String shoukzhh;//收款账号
    @JsonProperty(value = "skzhhzxh")
    private String skzhhzxh;//收款账号子序号
    @JsonProperty(value = "skzhhmch")
    private String skzhhmch;//收款账户名称
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "bjghrzxh")
    private String bjghrzxh;//本金归还入账账号子序号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号
    @JsonProperty(value = "lxghrzxh")
    private String lxghrzxh;//利息归还入账账号子序号
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "zclilvbh")
    private String zclilvbh;//正常利率编号
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "hetongll")
    private BigDecimal hetongll;//合同利率
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "yuqillbh")
    private String yuqillbh;//逾期利率编号
    @JsonProperty(value = "yuqinyll")
    private String yuqinyll;//逾期年月利率
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "yuqitzfs")
    private String yuqitzfs;//逾期利率调整方式
    @JsonProperty(value = "hkjshzhl")
    private String hkjshzhl;//还款基数种类
    @JsonProperty(value = "hkzhhgze")
    private String hkzhhgze;//还款账户规则
    @JsonProperty(value = "hkzhhzhl")
    private String hkzhhzhl;//还款账户种类
    @JsonProperty(value = "huankbli")
    private BigDecimal huankbli;//还款比例
    @JsonProperty(value = "shengxrq")
    private String shengxrq;//生效日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "youxianj")
    private Integer youxianj;//优先级
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "hkzhhmch")
    private String hkzhhmch;//还款账户名称

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getBhchzibz() {
        return bhchzibz;
    }

    public void setBhchzibz(String bhchzibz) {
        this.bhchzibz = bhchzibz;
    }

    public String getLhdkleix() {
        return lhdkleix;
    }

    public void setLhdkleix(String lhdkleix) {
        this.lhdkleix = lhdkleix;
    }

    public String getChzfkehh() {
        return chzfkehh;
    }

    public void setChzfkehh(String chzfkehh) {
        this.chzfkehh = chzfkehh;
    }

    public String getChzfkhmc() {
        return chzfkhmc;
    }

    public void setChzfkhmc(String chzfkhmc) {
        this.chzfkhmc = chzfkhmc;
    }

    public String getChzfhhao() {
        return chzfhhao;
    }

    public void setChzfhhao(String chzfhhao) {
        this.chzfhhao = chzfhhao;
    }

    public String getCzfhming() {
        return czfhming;
    }

    public void setCzfhming(String czfhming) {
        this.czfhming = czfhming;
    }

    public String getChuzfzhh() {
        return chuzfzhh;
    }

    public void setChuzfzhh(String chuzfzhh) {
        this.chuzfzhh = chuzfzhh;
    }

    public String getCzfzhzxh() {
        return czfzhzxh;
    }

    public void setCzfzhzxh(String czfzhzxh) {
        this.czfzhzxh = czfzhzxh;
    }

    public String getChzfzhmc() {
        return chzfzhmc;
    }

    public void setChzfzhmc(String chzfzhmc) {
        this.chzfzhmc = chzfzhmc;
    }

    public BigDecimal getChuzbili() {
        return chuzbili;
    }

    public void setChuzbili(BigDecimal chuzbili) {
        this.chuzbili = chuzbili;
    }

    public BigDecimal getChuzjine() {
        return chuzjine;
    }

    public void setChuzjine(BigDecimal chuzjine) {
        this.chuzjine = chuzjine;
    }

    public String getShoukzhh() {
        return shoukzhh;
    }

    public void setShoukzhh(String shoukzhh) {
        this.shoukzhh = shoukzhh;
    }

    public String getSkzhhzxh() {
        return skzhhzxh;
    }

    public void setSkzhhzxh(String skzhhzxh) {
        this.skzhhzxh = skzhhzxh;
    }

    public String getSkzhhmch() {
        return skzhhmch;
    }

    public void setSkzhhmch(String skzhhmch) {
        this.skzhhmch = skzhhmch;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getHkjshzhl() {
        return hkjshzhl;
    }

    public void setHkjshzhl(String hkjshzhl) {
        this.hkjshzhl = hkjshzhl;
    }

    public String getHkzhhgze() {
        return hkzhhgze;
    }

    public void setHkzhhgze(String hkzhhgze) {
        this.hkzhhgze = hkzhhgze;
    }

    public String getHkzhhzhl() {
        return hkzhhzhl;
    }

    public void setHkzhhzhl(String hkzhhzhl) {
        this.hkzhhzhl = hkzhhzhl;
    }

    public BigDecimal getHuankbli() {
        return huankbli;
    }

    public void setHuankbli(BigDecimal huankbli) {
        this.huankbli = huankbli;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public Integer getYouxianj() {
        return youxianj;
    }

    public void setYouxianj(Integer youxianj) {
        this.youxianj = youxianj;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHkzhhmch() {
        return hkzhhmch;
    }

    public void setHkzhhmch(String hkzhhmch) {
        this.hkzhhmch = hkzhhmch;
    }

    @Override
    public String toString() {
        return "Lstdkhkzh{" +
                "ruzjigou='" + ruzjigou + '\'' +
                "bhchzibz='" + bhchzibz + '\'' +
                "lhdkleix='" + lhdkleix + '\'' +
                "chzfkehh='" + chzfkehh + '\'' +
                "chzfkhmc='" + chzfkhmc + '\'' +
                "chzfhhao='" + chzfhhao + '\'' +
                "czfhming='" + czfhming + '\'' +
                "chuzfzhh='" + chuzfzhh + '\'' +
                "czfzhzxh='" + czfzhzxh + '\'' +
                "chzfzhmc='" + chzfzhmc + '\'' +
                "chuzbili='" + chuzbili + '\'' +
                "chuzjine='" + chuzjine + '\'' +
                "shoukzhh='" + shoukzhh + '\'' +
                "skzhhzxh='" + skzhhzxh + '\'' +
                "skzhhmch='" + skzhhmch + '\'' +
                "bjghrzzh='" + bjghrzzh + '\'' +
                "bjghrzxh='" + bjghrzxh + '\'' +
                "lxghrzzh='" + lxghrzzh + '\'' +
                "lxghrzxh='" + lxghrzxh + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                "nyuelilv='" + nyuelilv + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "hetongll='" + hetongll + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "yuqillbh='" + yuqillbh + '\'' +
                "yuqinyll='" + yuqinyll + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "yuqitzfs='" + yuqitzfs + '\'' +
                "hkjshzhl='" + hkjshzhl + '\'' +
                "hkzhhgze='" + hkzhhgze + '\'' +
                "hkzhhzhl='" + hkzhhzhl + '\'' +
                "huankbli='" + huankbli + '\'' +
                "shengxrq='" + shengxrq + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "youxianj='" + youxianj + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "hkzhhmch='" + hkzhhmch + '\'' +
                '}';
    }
}  
