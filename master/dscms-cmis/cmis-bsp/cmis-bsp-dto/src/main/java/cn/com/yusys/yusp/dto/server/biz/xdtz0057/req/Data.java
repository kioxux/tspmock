package cn.com.yusys.yusp.dto.server.biz.xdtz0057.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据流水号查询客户调查的放款信息（在途需求）
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "surveySerno")
    private String surveySerno;//调查流水号

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "surveySerno='" + surveySerno + '\'' +
                '}';
    }
}

