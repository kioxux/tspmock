package cn.com.yusys.yusp.dto.client.esb.core.ln3030;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：贷款受托支付
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkstzf implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dfzhhzhl")
    private String dfzhhzhl;//对方账号种类
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注
    @JsonProperty(value = "zjzrzhao")
    private String zjzrzhao;//资金转入账号
    @JsonProperty(value = "dfzhangh")
    private String dfzhangh;//对方账号
    @JsonProperty(value = "dfzhhkhh")
    private String dfzhhkhh;//对方账号开户行
    @JsonProperty(value = "dfzhkhhm")
    private String dfzhkhhm;//对方账号开户行名
    @JsonProperty(value = "zjzrzzxh")
    private String zjzrzzxh;//资金转入账号子序号
    @JsonProperty(value = "dfzhhzxh")
    private String dfzhhzxh;//对方账号子序号
    @JsonProperty(value = "zjzrzhmc")
    private String zjzrzhmc;//资金转入账号名称
    @JsonProperty(value = "dfzhhmch")
    private String dfzhhmch;//对方账号名称
    @JsonProperty(value = "shtzfbli")
    private BigDecimal shtzfbli;//受托比例

    public String getDfzhhzhl() {
        return dfzhhzhl;
    }

    public void setDfzhhzhl(String dfzhhzhl) {
        this.dfzhhzhl = dfzhhzhl;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getDfzhangh() {
        return dfzhangh;
    }

    public void setDfzhangh(String dfzhangh) {
        this.dfzhangh = dfzhangh;
    }

    public String getDfzhhkhh() {
        return dfzhhkhh;
    }

    public void setDfzhhkhh(String dfzhhkhh) {
        this.dfzhhkhh = dfzhhkhh;
    }

    public String getDfzhkhhm() {
        return dfzhkhhm;
    }

    public void setDfzhkhhm(String dfzhkhhm) {
        this.dfzhkhhm = dfzhkhhm;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public String getDfzhhzxh() {
        return dfzhhzxh;
    }

    public void setDfzhhzxh(String dfzhhzxh) {
        this.dfzhhzxh = dfzhhzxh;
    }

    public String getZjzrzhmc() {
        return zjzrzhmc;
    }

    public void setZjzrzhmc(String zjzrzhmc) {
        this.zjzrzhmc = zjzrzhmc;
    }

    public String getDfzhhmch() {
        return dfzhhmch;
    }

    public void setDfzhhmch(String dfzhhmch) {
        this.dfzhhmch = dfzhhmch;
    }

    public BigDecimal getShtzfbli() {
        return shtzfbli;
    }

    public void setShtzfbli(BigDecimal shtzfbli) {
        this.shtzfbli = shtzfbli;
    }

    @Override
    public String toString() {
        return "Lstdkstzf{" +
                "dfzhhzhl='" + dfzhhzhl + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "zjzrzhao='" + zjzrzhao + '\'' +
                "dfzhangh='" + dfzhangh + '\'' +
                "dfzhhkhh='" + dfzhhkhh + '\'' +
                "dfzhkhhm='" + dfzhkhhm + '\'' +
                "zjzrzzxh='" + zjzrzzxh + '\'' +
                "dfzhhzxh='" + dfzhhzxh + '\'' +
                "zjzrzhmc='" + zjzrzhmc + '\'' +
                "dfzhhmch='" + dfzhhmch + '\'' +
                "shtzfbli='" + shtzfbli + '\'' +
                '}';
    }
}  
