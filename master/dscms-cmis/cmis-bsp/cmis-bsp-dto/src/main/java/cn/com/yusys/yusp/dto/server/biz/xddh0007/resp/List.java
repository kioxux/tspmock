package cn.com.yusys.yusp.dto.server.biz.xddh0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：还款记录列表查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "cusCertNo")
    private String cusCertNo;//客户身份证号
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "loanStatus")
    private String loanStatus;//贷款状态
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//借据金额
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//贷款余额
    @JsonProperty(value = "hasbcCap")
    private BigDecimal hasbcCap;//已还本金
    @JsonProperty(value = "hasbcInt")
    private BigDecimal hasbcInt;//已还利息
    @JsonProperty(value = "hasbcOdint")
    private BigDecimal hasbcOdint;//已还罚息
    @JsonProperty(value = "hasbcCi")
    private BigDecimal hasbcCi;//已还复利
    @JsonProperty(value = "repayAmt")
    private BigDecimal repayAmt;//还款金额
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "loanDate")
    private String loanDate;//贷款日期
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款到期日
    @JsonProperty(value = "loanRate")
    private BigDecimal loanRate;//贷款利率
    @JsonProperty(value = "repayTime")
    private String repayTime;//还款时间


    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusCertNo() {
        return cusCertNo;
    }

    public void setCusCertNo(String cusCertNo) {
        this.cusCertNo = cusCertNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getHasbcCap() {
        return hasbcCap;
    }

    public void setHasbcCap(BigDecimal hasbcCap) {
        this.hasbcCap = hasbcCap;
    }

    public BigDecimal getHasbcInt() {
        return hasbcInt;
    }

    public void setHasbcInt(BigDecimal hasbcInt) {
        this.hasbcInt = hasbcInt;
    }

    public BigDecimal getHasbcOdint() {
        return hasbcOdint;
    }

    public void setHasbcOdint(BigDecimal hasbcOdint) {
        this.hasbcOdint = hasbcOdint;
    }

    public BigDecimal getHasbcCi() {
        return hasbcCi;
    }

    public void setHasbcCi(BigDecimal hasbcCi) {
        this.hasbcCi = hasbcCi;
    }

    public BigDecimal getRepayAmt() {
        return repayAmt;
    }

    public void setRepayAmt(BigDecimal repayAmt) {
        this.repayAmt = repayAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(String loanDate) {
        this.loanDate = loanDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public BigDecimal getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(BigDecimal loanRate) {
        this.loanRate = loanRate;
    }

    public String getRepayTime() {
        return repayTime;
    }

    public void setRepayTime(String repayTime) {
        this.repayTime = repayTime;
    }

    @Override
    public String toString() {
        return "List{" +
                "cusName='" + cusName + '\'' +
                ", cusCertNo='" + cusCertNo + '\'' +
                ", billNo='" + billNo + '\'' +
                ", loanStatus='" + loanStatus + '\'' +
                ", loanAmt=" + loanAmt +
                ", loanBalance=" + loanBalance +
                ", hasbcCap=" + hasbcCap +
                ", hasbcInt=" + hasbcInt +
                ", hasbcOdint=" + hasbcOdint +
                ", hasbcCi=" + hasbcCi +
                ", repayAmt=" + repayAmt +
                ", curType='" + curType + '\'' +
                ", loanDate='" + loanDate + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", loanRate=" + loanRate +
                ", repayTime='" + repayTime + '\'' +
                '}';
    }
}
