package cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求DTO：权证信息同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class CetinfReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dbhtbh")
    private String dbhtbh; // 担保合同编号
    @JsonProperty(value = "sernoy")
    private String sernoy; // 核心担保编号
    @JsonProperty(value = "dyswbs")
    private String dyswbs; // 抵押顺位标识
    @JsonProperty(value = "qzlxyp")
    private String qzlxyp; // 权证类型
    @JsonProperty(value = "qlpzhm")
    private String qlpzhm; // 权利凭证号
    @JsonProperty(value = "qzfzjg")
    private String qzfzjg; // 权证发证机关名称
    @JsonProperty(value = "qzffrq")
    private String qzffrq; // 权证发证日期
    @JsonProperty(value = "qzdqrq")
    private String qzdqrq; // 权证到期日期
    @JsonProperty(value = "qljeyp")
    private String qljeyp; // 权利金额
    @JsonProperty(value = "qzztyp")
    private String qzztyp; // 权证状态
    @JsonProperty(value = "qzbzxx")
    private String qzbzxx; // 权证备注信息
    @JsonProperty(value = "djrmyp")
    private String djrmyp; // 登记人
    @JsonProperty(value = "djjgyp")
    private String djjgyp; // 登记机构
    @JsonProperty(value = "djrqyp")
    private String djrqyp; // 登记日期
    @JsonProperty(value = "operat")
    private String operat; // 操作
    @JsonProperty(value = "list")
    private List<CetinfListInfo> list;

    public String getDbhtbh() {
        return dbhtbh;
    }

    public void setDbhtbh(String dbhtbh) {
        this.dbhtbh = dbhtbh;
    }

    public String getSernoy() {
        return sernoy;
    }

    public void setSernoy(String sernoy) {
        this.sernoy = sernoy;
    }

    public String getDyswbs() {
        return dyswbs;
    }

    public void setDyswbs(String dyswbs) {
        this.dyswbs = dyswbs;
    }

    public String getQzlxyp() {
        return qzlxyp;
    }

    public void setQzlxyp(String qzlxyp) {
        this.qzlxyp = qzlxyp;
    }

    public String getQlpzhm() {
        return qlpzhm;
    }

    public void setQlpzhm(String qlpzhm) {
        this.qlpzhm = qlpzhm;
    }

    public String getQzfzjg() {
        return qzfzjg;
    }

    public void setQzfzjg(String qzfzjg) {
        this.qzfzjg = qzfzjg;
    }

    public String getQzffrq() {
        return qzffrq;
    }

    public void setQzffrq(String qzffrq) {
        this.qzffrq = qzffrq;
    }

    public String getQzdqrq() {
        return qzdqrq;
    }

    public void setQzdqrq(String qzdqrq) {
        this.qzdqrq = qzdqrq;
    }

    public String getQljeyp() {
        return qljeyp;
    }

    public void setQljeyp(String qljeyp) {
        this.qljeyp = qljeyp;
    }

    public String getQzztyp() {
        return qzztyp;
    }

    public void setQzztyp(String qzztyp) {
        this.qzztyp = qzztyp;
    }

    public String getQzbzxx() {
        return qzbzxx;
    }

    public void setQzbzxx(String qzbzxx) {
        this.qzbzxx = qzbzxx;
    }

    public String getDjrmyp() {
        return djrmyp;
    }

    public void setDjrmyp(String djrmyp) {
        this.djrmyp = djrmyp;
    }

    public String getDjjgyp() {
        return djjgyp;
    }

    public void setDjjgyp(String djjgyp) {
        this.djjgyp = djjgyp;
    }

    public String getDjrqyp() {
        return djrqyp;
    }

    public void setDjrqyp(String djrqyp) {
        this.djrqyp = djrqyp;
    }

    public String getOperat() {
        return operat;
    }

    public void setOperat(String operat) {
        this.operat = operat;
    }

    public List<CetinfListInfo> getList() {
        return list;
    }

    public void setList(List<CetinfListInfo> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "CetinfReqDto{" +
                "dbhtbh='" + dbhtbh + '\'' +
                ", sernoy='" + sernoy + '\'' +
                ", dyswbs='" + dyswbs + '\'' +
                ", qzlxyp='" + qzlxyp + '\'' +
                ", qlpzhm='" + qlpzhm + '\'' +
                ", qzfzjg='" + qzfzjg + '\'' +
                ", qzffrq='" + qzffrq + '\'' +
                ", qzdqrq='" + qzdqrq + '\'' +
                ", qljeyp='" + qljeyp + '\'' +
                ", qzztyp='" + qzztyp + '\'' +
                ", qzbzxx='" + qzbzxx + '\'' +
                ", djrmyp='" + djrmyp + '\'' +
                ", djjgyp='" + djjgyp + '\'' +
                ", djrqyp='" + djrqyp + '\'' +
                ", operat='" + operat + '\'' +
                ", list=" + list +
                '}';
    }
}
