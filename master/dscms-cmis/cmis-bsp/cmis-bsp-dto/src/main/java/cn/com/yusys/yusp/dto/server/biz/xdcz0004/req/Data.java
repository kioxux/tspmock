package cn.com.yusys.yusp.dto.server.biz.xdcz0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：承兑签发审批结果综合服务
 * @author
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "paySecurityAmt")
    private BigDecimal paySecurityAmt;//已交保证金金额
    @JsonProperty(value = "pvpStatus")
    private String pvpStatus;//出账状态
    @JsonProperty(value = "drfpoIsseMk")
    private String drfpoIsseMk;//票据池出票标记
    @JsonProperty(value = "serno")
    private String serno;//批次号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }


    public BigDecimal getPaySecurityAmt() {
        return paySecurityAmt;
    }

    public void setPaySecurityAmt(BigDecimal paySecurityAmt) {
        this.paySecurityAmt = paySecurityAmt;
    }

    public String getPvpStatus() {
        return pvpStatus;
    }

    public void setPvpStatus(String pvpStatus) {
        this.pvpStatus = pvpStatus;
    }

    public String getDrfpoIsseMk() {
        return drfpoIsseMk;
    }

    public void setDrfpoIsseMk(String drfpoIsseMk) {
        this.drfpoIsseMk = drfpoIsseMk;
    }

    @Override
    public String toString() {
        return "Xdcz0004DataReqDto{" +
                "paySecurityAmt='" + paySecurityAmt + '\'' +
                "pvpStatus='" + pvpStatus + '\'' +
                "drfpoIsseMk='" + drfpoIsseMk + '\'' +
                "serno='" + serno + '\'' +
                '}';
    }
}
