package cn.com.yusys.yusp.dto.server.biz.xdxw0058.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据流水号查询借据号
 * @Author zhangpeng
 * @Date 2021/4/24 17:48
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applySerno")
    private String applySerno;//申请流水号

    public String getApplySerno() {
        return applySerno;
    }

    public void setApplySerno(String applySerno) {
        this.applySerno = applySerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "applySerno='" + applySerno + '\'' +
                '}';
    }
}
