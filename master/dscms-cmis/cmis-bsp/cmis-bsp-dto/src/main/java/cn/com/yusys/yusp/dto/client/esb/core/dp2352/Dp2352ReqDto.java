package cn.com.yusys.yusp.dto.client.esb.core.dp2352;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：组合账户子账户开立
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2352ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh;//子账户序号
    @JsonProperty(value = "zhhuzwmc")
    private String zhhuzwmc;//账户名称
    @JsonProperty(value = "suoshudx")
    private String suoshudx;//产品所属对象
    @JsonProperty(value = "chapbhao")
    private String chapbhao;//产品编号
    @JsonProperty(value = "yezztbbz")
    private String yezztbbz;//余额总账同步标志
    @JsonProperty(value = "chanpshm")
    private String chanpshm;//产品说明
    @JsonProperty(value = "cunqiiii")
    private String cunqiiii;//存期
    @JsonProperty(value = "dinhuobz")
    private String dinhuobz;//产品定活标志
    @JsonProperty(value = "cunkzlei")
    private String cunkzlei;//存款种类
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//货币代号
    @JsonProperty(value = "chaohubz")
    private String chaohubz;//账户钞汇标志
    @JsonProperty(value = "zhhufenl")
    private String zhhufenl;//账户分类
    @JsonProperty(value = "zhshuxin")
    private String zhshuxin;//账户属性
    @JsonProperty(value = "shfozxhm")
    private String shfozxhm;//是否自选号码
    @JsonProperty(value = "khzxhaom")
    private String khzxhaom;//客户自选号码
    @JsonProperty(value = "qixifans")
    private String qixifans;//起息方式
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "doqiriqi")
    private String doqiriqi;//到期日期
    @JsonProperty(value = "jixibioz")
    private String jixibioz;//计息标志
    @JsonProperty(value = "lilvbhao")
    private String lilvbhao;//利率编号
    @JsonProperty(value = "jizhunll")
    private BigDecimal jizhunll;//基准利率
    @JsonProperty(value = "llfdonbz")
    private String llfdonbz;//利率浮动标志1
    @JsonProperty(value = "lilvfdbl")
    private BigDecimal lilvfdbl;//利率浮动比例
    @JsonProperty(value = "lilvfdsz")
    private BigDecimal lilvfdsz;//利率浮动值
    @JsonProperty(value = "youhuibz")
    private String youhuibz;//优惠标志
    @JsonProperty(value = "shijlilv")
    private BigDecimal shijlilv;//实际利率
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源
    @JsonProperty(value = "kaihjine")
    private BigDecimal kaihjine;//开户金额
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuleix")
    private String kehuleix;//客户类型
    @JsonProperty(value = "zmwjzlei")
    private String zmwjzlei;//证明文件种类
    @JsonProperty(value = "zmwjbhao")
    private String zmwjbhao;//证明文件编号
    @JsonProperty(value = "zhcunfsh")
    private String zhcunfsh;//转存方式
    @JsonProperty(value = "zcuncqii")
    private String zcuncqii;//转存存期
    @JsonProperty(value = "zcunjine")
    private BigDecimal zcunjine;//转存金额
    @JsonProperty(value = "bjlxzrzh")
    private String bjlxzrzh;//本金/利息转入账号
    @JsonProperty(value = "bxzrzhao")
    private String bxzrzhao;//本金/利息转入系统账号
    @JsonProperty(value = "quxijnge")
    private String quxijnge;//取息间隔
    @JsonProperty(value = "pngzzlei")
    private String pngzzlei;//凭证种类
    @JsonProperty(value = "pngzphao")
    private String pngzphao;//凭证批号
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao;//凭证序号
    @JsonProperty(value = "tduifwei")
    private String tduifwei;//通兑范围
    @JsonProperty(value = "chaxmima")
    private String chaxmima;//查询密码
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn;//支付条件
    @JsonProperty(value = "jiaoymma")
    private String jiaoymma;//交易密码
    @JsonProperty(value = "zckhzhao")
    private String zckhzhao;//资金来源账号
    @JsonProperty(value = "kehuzhlx")
    private String kehuzhlx;//账号类型
    @JsonProperty(value = "kehuzhmc")
    private String kehuzhmc;//账户名称
    @JsonProperty(value = "kehumich")
    private String kehumich;//客户名称
    @JsonProperty(value = "zhzhxhao")
    private String zhzhxhao;//子账户序号
    @JsonProperty(value = "zhzhabiz")
    private String zhzhabiz;//货币代号
    @JsonProperty(value = "bzchhbzi")
    private String bzchhbzi;//钞汇标志
    @JsonProperty(value = "xchpbhao")
    private String xchpbhao;//产品编号
    @JsonProperty(value = "yzhftojn")
    private String yzhftojn;//支付条件
    @JsonProperty(value = "fpgzzlei")
    private String fpgzzlei;//凭证种类
    @JsonProperty(value = "mimazlei")
    private String mimazlei;//密码种类
    @JsonProperty(value = "fpgzphao")
    private String fpgzphao;//凭证批号
    @JsonProperty(value = "fpgzxhao")
    private String fpgzxhao;//凭证序号
    @JsonProperty(value = "qianfarq")
    private String qianfarq;//签发日期
    @JsonProperty(value = "zhfumima")
    private String zhfumima;//支付密码
    @JsonProperty(value = "waigzhxz")
    private String waigzhxz;//外管账户性质
    @JsonProperty(value = "daifljxe")
    private BigDecimal daifljxe;//贷方累计限额
    @JsonProperty(value = "hzjbhaoo")
    private String hzjbhaoo;//核准件编号
    @JsonProperty(value = "htonbhao")
    private String htonbhao;//合同编号
    @JsonProperty(value = "sfwhjgbz")
    private String sfwhjgbz;//是否外汇监管标志
    @JsonProperty(value = "waihhcbz")
    private String waihhcbz;//外汇核查标志
    @JsonProperty(value = "beiyjine")
    private BigDecimal beiyjine;//备用金额
    @JsonProperty(value = "youxriqi")
    private String youxriqi;//账户有效期
    @JsonProperty(value = "xnjnxmdm")
    private String xnjnxmdm;//现金项目代码
    @JsonProperty(value = "sfsfbzhi")
    private String sfsfbzhi;//是否收费标志
    @JsonProperty(value = "yingshfy")
    private BigDecimal yingshfy;//应收费用
    @JsonProperty(value = "zhufldm1")
    private String zhufldm1;//账户分类代码1
    @JsonProperty(value = "zhufldm2")
    private String zhufldm2;//账户分类代码2
    @JsonProperty(value = "zhufldm3")
    private String zhufldm3;//账户分类代码3
    @JsonProperty(value = "lancreny")
    private String lancreny;//揽存人员
    @JsonProperty(value = "lancrymc")
    private String lancrymc;//账户经理名称
    @JsonProperty(value = "zhgshhao")
    private String zhgshhao;//账户归属行行号
    @JsonProperty(value = "kaihdjbz")
    private String kaihdjbz;//开户冻结标志
    @JsonProperty(value = "dongjbho")
    private String dongjbho;//冻结编号
    @JsonProperty(value = "keschpbh")
    private String keschpbh;//可售产品编号
    @JsonProperty(value = "keschpmc")
    private String keschpmc;//可售产品名称
    @JsonProperty(value = "zhywenmc")
    private String zhywenmc;//账户英文名称
    @JsonProperty(value = "zhywenjc")
    private String zhywenjc;//账户英文简称
    @JsonProperty(value = "zhzwenjc")
    private String zhzwenjc;//账户中文简称
    @JsonProperty(value = "xgywbhao")
    private String xgywbhao;//相关业务编号
    @JsonProperty(value = "lyzhipbz")
    private String lyzhipbz;//领用支票标志
    @JsonProperty(value = "dfljxebz")
    private String dfljxebz;//贷方累计限额币种
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注信息
    @JsonProperty(value = "lilvdanc")
    private BigDecimal lilvdanc;//利率档次
    @JsonProperty(value = "zhdaoqir")
    private String zhdaoqir;//账户到期日
    @JsonProperty(value = "lyzhzhao")
    private String lyzhzhao;//来源账户账号
    @JsonProperty(value = "lyzhumcc")
    private String lyzhumcc;//来源账户名称
    @JsonProperty(value = "lyzhhmin")
    private String lyzhhmin;//来源账户行名
    @JsonProperty(value = "lyzhhhao")
    private String lyzhhhao;//来源账户行号
    @JsonProperty(value = "fuxipinl")
    private String fuxipinl;//付息频率
    @JsonProperty(value = "zhaiyodm")
    private String zhaiyodm;//摘要代码
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要描述
    @JsonProperty(value = "fkzhahmc")
    private String fkzhahmc;//付款账户名称
    @JsonProperty(value = "listnm01")
    private List<Listnm01> listnm01;//推荐人信息列表
    @JsonProperty(value = "baimdbzh")
    private String baimdbzh;//白名单标志
    @JsonProperty(value = "bfujzhbz")
    private String bfujzhbz;//备付金账户标志
    @JsonProperty(value = "xjglqybz")
    private String xjglqybz;//现金管理签约标志
    @JsonProperty(value = "tglzhubz")
    private String tglzhubz;//托管类账户标志
    @JsonProperty(value = "zmqzhubz")
    private String zmqzhubz;//自贸区账户标志
    @JsonProperty(value = "czzhhubz")
    private String czzhhubz;//财政账户标志
    @JsonProperty(value = "duifjgdm")
    private String duifjgdm;//对方金融机构代码
    @JsonProperty(value = "duifjgmc")
    private String duifjgmc;//对方金融机构名称
    @JsonProperty(value = "jgzhhulx")
    private String jgzhhulx;//监管账户类型
    @JsonProperty(value = "jgzhhubz")
    private String jgzhhubz;//监管账户标志
    @JsonProperty(value = "bfjzhhlx")
    private String bfjzhhlx;//备付金账户类型
    @JsonProperty(value = "tycfzhlx")
    private String tycfzhlx;//同业存放账户类型
    @JsonProperty(value = "czckzhlx")
    private String czckzhlx;//财政存款账户类型
    @JsonProperty(value = "tuogzhlx")
    private String tuogzhlx;//托管账户类型
    @JsonProperty(value = "zmswiflx")
    private String zmswiflx;//自贸区账户类型
    @JsonProperty(value = "sfszxebz")
    private String sfszxebz;//是否设置限额标志
    @JsonProperty(value = "kaihjigo")
    private String kaihjigo;//开户机构
    @JsonProperty(value = "jifeibzh")
    private String jifeibzh;//计费标志
    @JsonProperty(value = "zijijgbz")
    private String zijijgbz;//资金监管标志
    @JsonProperty(value = "hzbabzhi")
    private String hzbabzhi;//核准备案标志
    @JsonProperty(value = "jibhkhho")
    private String jibhkhho;//基本户开户行行号
    @JsonProperty(value = "jibhkhhm")
    private String jibhkhhm;//基本户开户行行名
    @JsonProperty(value = "jibhzhho")
    private String jibhzhho;//基本户账户
    @JsonProperty(value = "jibhhzho")
    private String jibhhzho;//基本账户开户许可证核准号
    @JsonProperty(value = "lszhxkzh")
    private String lszhxkzh;//临时/专户许可证号
    @JsonProperty(value = "beiyjie1")
    private BigDecimal beiyjie1;//备用金额01
    @JsonProperty(value = "beiyjie2")
    private BigDecimal beiyjie2;//备用金额02
    @JsonProperty(value = "beiyjie3")
    private BigDecimal beiyjie3;//备用金额03
    @JsonProperty(value = "beiyjie4")
    private BigDecimal beiyjie4;//备用金额04
    @JsonProperty(value = "beiyjie5")
    private BigDecimal beiyjie5;//备用金额05
    @JsonProperty(value = "beiyzif1")
    private String beiyzif1;//备用字符01
    @JsonProperty(value = "beiyzif2")
    private String beiyzif2;//备用字符02
    @JsonProperty(value = "beiyzif3")
    private String beiyzif3;//备用字符03
    @JsonProperty(value = "beiyzif4")
    private String beiyzif4;//备用字符04
    @JsonProperty(value = "beiyzif5")
    private String beiyzif5;//备用字符05
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "weiylilv")
    private BigDecimal weiylilv;//违约利率
    @JsonProperty(value = "xieyibho")
    private String xieyibho;//协议编号
    @JsonProperty(value = "sfkldqsh")
    private String sfkldqsh;//是否开立开立待算分户
    @JsonProperty(value = "lilvbcfd")
    private BigDecimal lilvbcfd;//利率补差浮动比例
    @JsonProperty(value = "dhdjbioz")
    private String dhdjbioz;//单户定价标志
    @JsonProperty(value = "gdlvsfdz")
    private BigDecimal gdlvsfdz;//固定利率上浮点值
    @JsonProperty(value = "zhableix")
    private String zhableix;//招标保证金类型


    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getSuoshudx() {
        return suoshudx;
    }

    public void setSuoshudx(String suoshudx) {
        this.suoshudx = suoshudx;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getYezztbbz() {
        return yezztbbz;
    }

    public void setYezztbbz(String yezztbbz) {
        this.yezztbbz = yezztbbz;
    }

    public String getChanpshm() {
        return chanpshm;
    }

    public void setChanpshm(String chanpshm) {
        this.chanpshm = chanpshm;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public String getDinhuobz() {
        return dinhuobz;
    }

    public void setDinhuobz(String dinhuobz) {
        this.dinhuobz = dinhuobz;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getZhhufenl() {
        return zhhufenl;
    }

    public void setZhhufenl(String zhhufenl) {
        this.zhhufenl = zhhufenl;
    }

    public String getZhshuxin() {
        return zhshuxin;
    }

    public void setZhshuxin(String zhshuxin) {
        this.zhshuxin = zhshuxin;
    }

    public String getShfozxhm() {
        return shfozxhm;
    }

    public void setShfozxhm(String shfozxhm) {
        this.shfozxhm = shfozxhm;
    }

    public String getKhzxhaom() {
        return khzxhaom;
    }

    public void setKhzxhaom(String khzxhaom) {
        this.khzxhaom = khzxhaom;
    }

    public String getQixifans() {
        return qixifans;
    }

    public void setQixifans(String qixifans) {
        this.qixifans = qixifans;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getJixibioz() {
        return jixibioz;
    }

    public void setJixibioz(String jixibioz) {
        this.jixibioz = jixibioz;
    }

    public String getLilvbhao() {
        return lilvbhao;
    }

    public void setLilvbhao(String lilvbhao) {
        this.lilvbhao = lilvbhao;
    }

    public BigDecimal getJizhunll() {
        return jizhunll;
    }

    public void setJizhunll(BigDecimal jizhunll) {
        this.jizhunll = jizhunll;
    }

    public String getLlfdonbz() {
        return llfdonbz;
    }

    public void setLlfdonbz(String llfdonbz) {
        this.llfdonbz = llfdonbz;
    }

    public BigDecimal getLilvfdbl() {
        return lilvfdbl;
    }

    public void setLilvfdbl(BigDecimal lilvfdbl) {
        this.lilvfdbl = lilvfdbl;
    }

    public BigDecimal getLilvfdsz() {
        return lilvfdsz;
    }

    public void setLilvfdsz(BigDecimal lilvfdsz) {
        this.lilvfdsz = lilvfdsz;
    }

    public String getYouhuibz() {
        return youhuibz;
    }

    public void setYouhuibz(String youhuibz) {
        this.youhuibz = youhuibz;
    }

    public BigDecimal getShijlilv() {
        return shijlilv;
    }

    public void setShijlilv(BigDecimal shijlilv) {
        this.shijlilv = shijlilv;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public BigDecimal getKaihjine() {
        return kaihjine;
    }

    public void setKaihjine(BigDecimal kaihjine) {
        this.kaihjine = kaihjine;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuleix() {
        return kehuleix;
    }

    public void setKehuleix(String kehuleix) {
        this.kehuleix = kehuleix;
    }

    public String getZmwjzlei() {
        return zmwjzlei;
    }

    public void setZmwjzlei(String zmwjzlei) {
        this.zmwjzlei = zmwjzlei;
    }

    public String getZmwjbhao() {
        return zmwjbhao;
    }

    public void setZmwjbhao(String zmwjbhao) {
        this.zmwjbhao = zmwjbhao;
    }

    public String getZhcunfsh() {
        return zhcunfsh;
    }

    public void setZhcunfsh(String zhcunfsh) {
        this.zhcunfsh = zhcunfsh;
    }

    public String getZcuncqii() {
        return zcuncqii;
    }

    public void setZcuncqii(String zcuncqii) {
        this.zcuncqii = zcuncqii;
    }

    public BigDecimal getZcunjine() {
        return zcunjine;
    }

    public void setZcunjine(BigDecimal zcunjine) {
        this.zcunjine = zcunjine;
    }

    public String getBjlxzrzh() {
        return bjlxzrzh;
    }

    public void setBjlxzrzh(String bjlxzrzh) {
        this.bjlxzrzh = bjlxzrzh;
    }

    public String getBxzrzhao() {
        return bxzrzhao;
    }

    public void setBxzrzhao(String bxzrzhao) {
        this.bxzrzhao = bxzrzhao;
    }

    public String getQuxijnge() {
        return quxijnge;
    }

    public void setQuxijnge(String quxijnge) {
        this.quxijnge = quxijnge;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPngzphao() {
        return pngzphao;
    }

    public void setPngzphao(String pngzphao) {
        this.pngzphao = pngzphao;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getTduifwei() {
        return tduifwei;
    }

    public void setTduifwei(String tduifwei) {
        this.tduifwei = tduifwei;
    }

    public String getChaxmima() {
        return chaxmima;
    }

    public void setChaxmima(String chaxmima) {
        this.chaxmima = chaxmima;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getJiaoymma() {
        return jiaoymma;
    }

    public void setJiaoymma(String jiaoymma) {
        this.jiaoymma = jiaoymma;
    }

    public String getZckhzhao() {
        return zckhzhao;
    }

    public void setZckhzhao(String zckhzhao) {
        this.zckhzhao = zckhzhao;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getKehumich() {
        return kehumich;
    }

    public void setKehumich(String kehumich) {
        this.kehumich = kehumich;
    }

    public String getZhzhxhao() {
        return zhzhxhao;
    }

    public void setZhzhxhao(String zhzhxhao) {
        this.zhzhxhao = zhzhxhao;
    }

    public String getZhzhabiz() {
        return zhzhabiz;
    }

    public void setZhzhabiz(String zhzhabiz) {
        this.zhzhabiz = zhzhabiz;
    }

    public String getBzchhbzi() {
        return bzchhbzi;
    }

    public void setBzchhbzi(String bzchhbzi) {
        this.bzchhbzi = bzchhbzi;
    }

    public String getXchpbhao() {
        return xchpbhao;
    }

    public void setXchpbhao(String xchpbhao) {
        this.xchpbhao = xchpbhao;
    }

    public String getYzhftojn() {
        return yzhftojn;
    }

    public void setYzhftojn(String yzhftojn) {
        this.yzhftojn = yzhftojn;
    }

    public String getFpgzzlei() {
        return fpgzzlei;
    }

    public void setFpgzzlei(String fpgzzlei) {
        this.fpgzzlei = fpgzzlei;
    }

    public String getMimazlei() {
        return mimazlei;
    }

    public void setMimazlei(String mimazlei) {
        this.mimazlei = mimazlei;
    }

    public String getFpgzphao() {
        return fpgzphao;
    }

    public void setFpgzphao(String fpgzphao) {
        this.fpgzphao = fpgzphao;
    }

    public String getFpgzxhao() {
        return fpgzxhao;
    }

    public void setFpgzxhao(String fpgzxhao) {
        this.fpgzxhao = fpgzxhao;
    }

    public String getQianfarq() {
        return qianfarq;
    }

    public void setQianfarq(String qianfarq) {
        this.qianfarq = qianfarq;
    }

    public String getZhfumima() {
        return zhfumima;
    }

    public void setZhfumima(String zhfumima) {
        this.zhfumima = zhfumima;
    }

    public String getWaigzhxz() {
        return waigzhxz;
    }

    public void setWaigzhxz(String waigzhxz) {
        this.waigzhxz = waigzhxz;
    }

    public BigDecimal getDaifljxe() {
        return daifljxe;
    }

    public void setDaifljxe(BigDecimal daifljxe) {
        this.daifljxe = daifljxe;
    }

    public String getHzjbhaoo() {
        return hzjbhaoo;
    }

    public void setHzjbhaoo(String hzjbhaoo) {
        this.hzjbhaoo = hzjbhaoo;
    }

    public String getHtonbhao() {
        return htonbhao;
    }

    public void setHtonbhao(String htonbhao) {
        this.htonbhao = htonbhao;
    }

    public String getSfwhjgbz() {
        return sfwhjgbz;
    }

    public void setSfwhjgbz(String sfwhjgbz) {
        this.sfwhjgbz = sfwhjgbz;
    }

    public String getWaihhcbz() {
        return waihhcbz;
    }

    public void setWaihhcbz(String waihhcbz) {
        this.waihhcbz = waihhcbz;
    }

    public BigDecimal getBeiyjine() {
        return beiyjine;
    }

    public void setBeiyjine(BigDecimal beiyjine) {
        this.beiyjine = beiyjine;
    }

    public String getYouxriqi() {
        return youxriqi;
    }

    public void setYouxriqi(String youxriqi) {
        this.youxriqi = youxriqi;
    }

    public String getXnjnxmdm() {
        return xnjnxmdm;
    }

    public void setXnjnxmdm(String xnjnxmdm) {
        this.xnjnxmdm = xnjnxmdm;
    }

    public String getSfsfbzhi() {
        return sfsfbzhi;
    }

    public void setSfsfbzhi(String sfsfbzhi) {
        this.sfsfbzhi = sfsfbzhi;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public String getZhufldm1() {
        return zhufldm1;
    }

    public void setZhufldm1(String zhufldm1) {
        this.zhufldm1 = zhufldm1;
    }

    public String getZhufldm2() {
        return zhufldm2;
    }

    public void setZhufldm2(String zhufldm2) {
        this.zhufldm2 = zhufldm2;
    }

    public String getZhufldm3() {
        return zhufldm3;
    }

    public void setZhufldm3(String zhufldm3) {
        this.zhufldm3 = zhufldm3;
    }

    public String getLancreny() {
        return lancreny;
    }

    public void setLancreny(String lancreny) {
        this.lancreny = lancreny;
    }

    public String getLancrymc() {
        return lancrymc;
    }

    public void setLancrymc(String lancrymc) {
        this.lancrymc = lancrymc;
    }

    public String getZhgshhao() {
        return zhgshhao;
    }

    public void setZhgshhao(String zhgshhao) {
        this.zhgshhao = zhgshhao;
    }

    public String getKaihdjbz() {
        return kaihdjbz;
    }

    public void setKaihdjbz(String kaihdjbz) {
        this.kaihdjbz = kaihdjbz;
    }

    public String getDongjbho() {
        return dongjbho;
    }

    public void setDongjbho(String dongjbho) {
        this.dongjbho = dongjbho;
    }

    public String getKeschpbh() {
        return keschpbh;
    }

    public void setKeschpbh(String keschpbh) {
        this.keschpbh = keschpbh;
    }

    public String getKeschpmc() {
        return keschpmc;
    }

    public void setKeschpmc(String keschpmc) {
        this.keschpmc = keschpmc;
    }

    public String getZhywenmc() {
        return zhywenmc;
    }

    public void setZhywenmc(String zhywenmc) {
        this.zhywenmc = zhywenmc;
    }

    public String getZhywenjc() {
        return zhywenjc;
    }

    public void setZhywenjc(String zhywenjc) {
        this.zhywenjc = zhywenjc;
    }

    public String getZhzwenjc() {
        return zhzwenjc;
    }

    public void setZhzwenjc(String zhzwenjc) {
        this.zhzwenjc = zhzwenjc;
    }

    public String getXgywbhao() {
        return xgywbhao;
    }

    public void setXgywbhao(String xgywbhao) {
        this.xgywbhao = xgywbhao;
    }

    public String getLyzhipbz() {
        return lyzhipbz;
    }

    public void setLyzhipbz(String lyzhipbz) {
        this.lyzhipbz = lyzhipbz;
    }

    public String getDfljxebz() {
        return dfljxebz;
    }

    public void setDfljxebz(String dfljxebz) {
        this.dfljxebz = dfljxebz;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public BigDecimal getLilvdanc() {
        return lilvdanc;
    }

    public void setLilvdanc(BigDecimal lilvdanc) {
        this.lilvdanc = lilvdanc;
    }

    public String getZhdaoqir() {
        return zhdaoqir;
    }

    public void setZhdaoqir(String zhdaoqir) {
        this.zhdaoqir = zhdaoqir;
    }

    public String getLyzhzhao() {
        return lyzhzhao;
    }

    public void setLyzhzhao(String lyzhzhao) {
        this.lyzhzhao = lyzhzhao;
    }

    public String getLyzhumcc() {
        return lyzhumcc;
    }

    public void setLyzhumcc(String lyzhumcc) {
        this.lyzhumcc = lyzhumcc;
    }

    public String getLyzhhmin() {
        return lyzhhmin;
    }

    public void setLyzhhmin(String lyzhhmin) {
        this.lyzhhmin = lyzhhmin;
    }

    public String getLyzhhhao() {
        return lyzhhhao;
    }

    public void setLyzhhhao(String lyzhhhao) {
        this.lyzhhhao = lyzhhhao;
    }

    public String getFuxipinl() {
        return fuxipinl;
    }

    public void setFuxipinl(String fuxipinl) {
        this.fuxipinl = fuxipinl;
    }

    public String getZhaiyodm() {
        return zhaiyodm;
    }

    public void setZhaiyodm(String zhaiyodm) {
        this.zhaiyodm = zhaiyodm;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getFkzhahmc() {
        return fkzhahmc;
    }

    public void setFkzhahmc(String fkzhahmc) {
        this.fkzhahmc = fkzhahmc;
    }

    public List<Listnm01> getListnm01() {
        return listnm01;
    }

    public void setListnm01(List<Listnm01> listnm01) {
        this.listnm01 = listnm01;
    }

    public String getBaimdbzh() {
        return baimdbzh;
    }

    public void setBaimdbzh(String baimdbzh) {
        this.baimdbzh = baimdbzh;
    }

    public String getBfujzhbz() {
        return bfujzhbz;
    }

    public void setBfujzhbz(String bfujzhbz) {
        this.bfujzhbz = bfujzhbz;
    }

    public String getXjglqybz() {
        return xjglqybz;
    }

    public void setXjglqybz(String xjglqybz) {
        this.xjglqybz = xjglqybz;
    }

    public String getTglzhubz() {
        return tglzhubz;
    }

    public void setTglzhubz(String tglzhubz) {
        this.tglzhubz = tglzhubz;
    }

    public String getZmqzhubz() {
        return zmqzhubz;
    }

    public void setZmqzhubz(String zmqzhubz) {
        this.zmqzhubz = zmqzhubz;
    }

    public String getCzzhhubz() {
        return czzhhubz;
    }

    public void setCzzhhubz(String czzhhubz) {
        this.czzhhubz = czzhhubz;
    }

    public String getDuifjgdm() {
        return duifjgdm;
    }

    public void setDuifjgdm(String duifjgdm) {
        this.duifjgdm = duifjgdm;
    }

    public String getDuifjgmc() {
        return duifjgmc;
    }

    public void setDuifjgmc(String duifjgmc) {
        this.duifjgmc = duifjgmc;
    }

    public String getJgzhhulx() {
        return jgzhhulx;
    }

    public void setJgzhhulx(String jgzhhulx) {
        this.jgzhhulx = jgzhhulx;
    }

    public String getJgzhhubz() {
        return jgzhhubz;
    }

    public void setJgzhhubz(String jgzhhubz) {
        this.jgzhhubz = jgzhhubz;
    }

    public String getBfjzhhlx() {
        return bfjzhhlx;
    }

    public void setBfjzhhlx(String bfjzhhlx) {
        this.bfjzhhlx = bfjzhhlx;
    }

    public String getTycfzhlx() {
        return tycfzhlx;
    }

    public void setTycfzhlx(String tycfzhlx) {
        this.tycfzhlx = tycfzhlx;
    }

    public String getCzckzhlx() {
        return czckzhlx;
    }

    public void setCzckzhlx(String czckzhlx) {
        this.czckzhlx = czckzhlx;
    }

    public String getTuogzhlx() {
        return tuogzhlx;
    }

    public void setTuogzhlx(String tuogzhlx) {
        this.tuogzhlx = tuogzhlx;
    }

    public String getZmswiflx() {
        return zmswiflx;
    }

    public void setZmswiflx(String zmswiflx) {
        this.zmswiflx = zmswiflx;
    }

    public String getSfszxebz() {
        return sfszxebz;
    }

    public void setSfszxebz(String sfszxebz) {
        this.sfszxebz = sfszxebz;
    }

    public String getKaihjigo() {
        return kaihjigo;
    }

    public void setKaihjigo(String kaihjigo) {
        this.kaihjigo = kaihjigo;
    }

    public String getJifeibzh() {
        return jifeibzh;
    }

    public void setJifeibzh(String jifeibzh) {
        this.jifeibzh = jifeibzh;
    }

    public String getZijijgbz() {
        return zijijgbz;
    }

    public void setZijijgbz(String zijijgbz) {
        this.zijijgbz = zijijgbz;
    }

    public String getHzbabzhi() {
        return hzbabzhi;
    }

    public void setHzbabzhi(String hzbabzhi) {
        this.hzbabzhi = hzbabzhi;
    }

    public String getJibhkhho() {
        return jibhkhho;
    }

    public void setJibhkhho(String jibhkhho) {
        this.jibhkhho = jibhkhho;
    }

    public String getJibhkhhm() {
        return jibhkhhm;
    }

    public void setJibhkhhm(String jibhkhhm) {
        this.jibhkhhm = jibhkhhm;
    }

    public String getJibhzhho() {
        return jibhzhho;
    }

    public void setJibhzhho(String jibhzhho) {
        this.jibhzhho = jibhzhho;
    }

    public String getJibhhzho() {
        return jibhhzho;
    }

    public void setJibhhzho(String jibhhzho) {
        this.jibhhzho = jibhhzho;
    }

    public String getLszhxkzh() {
        return lszhxkzh;
    }

    public void setLszhxkzh(String lszhxkzh) {
        this.lszhxkzh = lszhxkzh;
    }

    public BigDecimal getBeiyjie1() {
        return beiyjie1;
    }

    public void setBeiyjie1(BigDecimal beiyjie1) {
        this.beiyjie1 = beiyjie1;
    }

    public BigDecimal getBeiyjie2() {
        return beiyjie2;
    }

    public void setBeiyjie2(BigDecimal beiyjie2) {
        this.beiyjie2 = beiyjie2;
    }

    public BigDecimal getBeiyjie3() {
        return beiyjie3;
    }

    public void setBeiyjie3(BigDecimal beiyjie3) {
        this.beiyjie3 = beiyjie3;
    }

    public BigDecimal getBeiyjie4() {
        return beiyjie4;
    }

    public void setBeiyjie4(BigDecimal beiyjie4) {
        this.beiyjie4 = beiyjie4;
    }

    public BigDecimal getBeiyjie5() {
        return beiyjie5;
    }

    public void setBeiyjie5(BigDecimal beiyjie5) {
        this.beiyjie5 = beiyjie5;
    }

    public String getBeiyzif1() {
        return beiyzif1;
    }

    public void setBeiyzif1(String beiyzif1) {
        this.beiyzif1 = beiyzif1;
    }

    public String getBeiyzif2() {
        return beiyzif2;
    }

    public void setBeiyzif2(String beiyzif2) {
        this.beiyzif2 = beiyzif2;
    }

    public String getBeiyzif3() {
        return beiyzif3;
    }

    public void setBeiyzif3(String beiyzif3) {
        this.beiyzif3 = beiyzif3;
    }

    public String getBeiyzif4() {
        return beiyzif4;
    }

    public void setBeiyzif4(String beiyzif4) {
        this.beiyzif4 = beiyzif4;
    }

    public String getBeiyzif5() {
        return beiyzif5;
    }

    public void setBeiyzif5(String beiyzif5) {
        this.beiyzif5 = beiyzif5;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public BigDecimal getWeiylilv() {
        return weiylilv;
    }

    public void setWeiylilv(BigDecimal weiylilv) {
        this.weiylilv = weiylilv;
    }

    public String getXieyibho() {
        return xieyibho;
    }

    public void setXieyibho(String xieyibho) {
        this.xieyibho = xieyibho;
    }

    public String getSfkldqsh() {
        return sfkldqsh;
    }

    public void setSfkldqsh(String sfkldqsh) {
        this.sfkldqsh = sfkldqsh;
    }

    public BigDecimal getLilvbcfd() {
        return lilvbcfd;
    }

    public void setLilvbcfd(BigDecimal lilvbcfd) {
        this.lilvbcfd = lilvbcfd;
    }

    public String getDhdjbioz() {
        return dhdjbioz;
    }

    public void setDhdjbioz(String dhdjbioz) {
        this.dhdjbioz = dhdjbioz;
    }

    public BigDecimal getGdlvsfdz() {
        return gdlvsfdz;
    }

    public void setGdlvsfdz(BigDecimal gdlvsfdz) {
        this.gdlvsfdz = gdlvsfdz;
    }

    public String getZhableix() {
        return zhableix;
    }

    public void setZhableix(String zhableix) {
        this.zhableix = zhableix;
    }

    @Override
    public String toString() {
        return "Dp2352ReqDto{" +
                "kehuzhao='" + kehuzhao + '\'' +
                "zhhaoxuh='" + zhhaoxuh + '\'' +
                "zhhuzwmc='" + zhhuzwmc + '\'' +
                "suoshudx='" + suoshudx + '\'' +
                "chapbhao='" + chapbhao + '\'' +
                "yezztbbz='" + yezztbbz + '\'' +
                "chanpshm='" + chanpshm + '\'' +
                "cunqiiii='" + cunqiiii + '\'' +
                "dinhuobz='" + dinhuobz + '\'' +
                "cunkzlei='" + cunkzlei + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                "chaohubz='" + chaohubz + '\'' +
                "zhhufenl='" + zhhufenl + '\'' +
                "zhshuxin='" + zhshuxin + '\'' +
                "shfozxhm='" + shfozxhm + '\'' +
                "khzxhaom='" + khzxhaom + '\'' +
                "qixifans='" + qixifans + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "doqiriqi='" + doqiriqi + '\'' +
                "jixibioz='" + jixibioz + '\'' +
                "lilvbhao='" + lilvbhao + '\'' +
                "jizhunll='" + jizhunll + '\'' +
                "llfdonbz='" + llfdonbz + '\'' +
                "lilvfdbl='" + lilvfdbl + '\'' +
                "lilvfdsz='" + lilvfdsz + '\'' +
                "youhuibz='" + youhuibz + '\'' +
                "shijlilv='" + shijlilv + '\'' +
                "zijnlaiy='" + zijnlaiy + '\'' +
                "kaihjine='" + kaihjine + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehuleix='" + kehuleix + '\'' +
                "zmwjzlei='" + zmwjzlei + '\'' +
                "zmwjbhao='" + zmwjbhao + '\'' +
                "zhcunfsh='" + zhcunfsh + '\'' +
                "zcuncqii='" + zcuncqii + '\'' +
                "zcunjine='" + zcunjine + '\'' +
                "bjlxzrzh='" + bjlxzrzh + '\'' +
                "bxzrzhao='" + bxzrzhao + '\'' +
                "quxijnge='" + quxijnge + '\'' +
                "pngzzlei='" + pngzzlei + '\'' +
                "pngzphao='" + pngzphao + '\'' +
                "pngzxhao='" + pngzxhao + '\'' +
                "tduifwei='" + tduifwei + '\'' +
                "chaxmima='" + chaxmima + '\'' +
                "zhfutojn='" + zhfutojn + '\'' +
                "jiaoymma='" + jiaoymma + '\'' +
                "zckhzhao='" + zckhzhao + '\'' +
                "kehuzhlx='" + kehuzhlx + '\'' +
                "kehuzhmc='" + kehuzhmc + '\'' +
                "kehumich='" + kehumich + '\'' +
                "zhzhxhao='" + zhzhxhao + '\'' +
                "zhzhabiz='" + zhzhabiz + '\'' +
                "bzchhbzi='" + bzchhbzi + '\'' +
                "xchpbhao='" + xchpbhao + '\'' +
                "yzhftojn='" + yzhftojn + '\'' +
                "fpgzzlei='" + fpgzzlei + '\'' +
                "mimazlei='" + mimazlei + '\'' +
                "fpgzphao='" + fpgzphao + '\'' +
                "fpgzxhao='" + fpgzxhao + '\'' +
                "qianfarq='" + qianfarq + '\'' +
                "zhfumima='" + zhfumima + '\'' +
                "waigzhxz='" + waigzhxz + '\'' +
                "daifljxe='" + daifljxe + '\'' +
                "hzjbhaoo='" + hzjbhaoo + '\'' +
                "htonbhao='" + htonbhao + '\'' +
                "sfwhjgbz='" + sfwhjgbz + '\'' +
                "waihhcbz='" + waihhcbz + '\'' +
                "beiyjine='" + beiyjine + '\'' +
                "youxriqi='" + youxriqi + '\'' +
                "xnjnxmdm='" + xnjnxmdm + '\'' +
                "sfsfbzhi='" + sfsfbzhi + '\'' +
                "yingshfy='" + yingshfy + '\'' +
                "zhufldm1='" + zhufldm1 + '\'' +
                "zhufldm2='" + zhufldm2 + '\'' +
                "zhufldm3='" + zhufldm3 + '\'' +
                "lancreny='" + lancreny + '\'' +
                "lancrymc='" + lancrymc + '\'' +
                "zhgshhao='" + zhgshhao + '\'' +
                "kaihdjbz='" + kaihdjbz + '\'' +
                "dongjbho='" + dongjbho + '\'' +
                "keschpbh='" + keschpbh + '\'' +
                "keschpmc='" + keschpmc + '\'' +
                "zhywenmc='" + zhywenmc + '\'' +
                "zhywenjc='" + zhywenjc + '\'' +
                "zhzwenjc='" + zhzwenjc + '\'' +
                "xgywbhao='" + xgywbhao + '\'' +
                "lyzhipbz='" + lyzhipbz + '\'' +
                "dfljxebz='" + dfljxebz + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "lilvdanc='" + lilvdanc + '\'' +
                "zhdaoqir='" + zhdaoqir + '\'' +
                "lyzhzhao='" + lyzhzhao + '\'' +
                "lyzhumcc='" + lyzhumcc + '\'' +
                "lyzhhmin='" + lyzhhmin + '\'' +
                "lyzhhhao='" + lyzhhhao + '\'' +
                "fuxipinl='" + fuxipinl + '\'' +
                "zhaiyodm='" + zhaiyodm + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "fkzhahmc='" + fkzhahmc + '\'' +
                "listnm01='" + listnm01 + '\'' +
                "baimdbzh='" + baimdbzh + '\'' +
                "bfujzhbz='" + bfujzhbz + '\'' +
                "xjglqybz='" + xjglqybz + '\'' +
                "tglzhubz='" + tglzhubz + '\'' +
                "zmqzhubz='" + zmqzhubz + '\'' +
                "czzhhubz='" + czzhhubz + '\'' +
                "duifjgdm='" + duifjgdm + '\'' +
                "duifjgmc='" + duifjgmc + '\'' +
                "jgzhhulx='" + jgzhhulx + '\'' +
                "jgzhhubz='" + jgzhhubz + '\'' +
                "bfjzhhlx='" + bfjzhhlx + '\'' +
                "tycfzhlx='" + tycfzhlx + '\'' +
                "czckzhlx='" + czckzhlx + '\'' +
                "tuogzhlx='" + tuogzhlx + '\'' +
                "zmswiflx='" + zmswiflx + '\'' +
                "sfszxebz='" + sfszxebz + '\'' +
                "kaihjigo='" + kaihjigo + '\'' +
                "jifeibzh='" + jifeibzh + '\'' +
                "zijijgbz='" + zijijgbz + '\'' +
                "hzbabzhi='" + hzbabzhi + '\'' +
                "jibhkhho='" + jibhkhho + '\'' +
                "jibhkhhm='" + jibhkhhm + '\'' +
                "jibhzhho='" + jibhzhho + '\'' +
                "jibhhzho='" + jibhhzho + '\'' +
                "lszhxkzh='" + lszhxkzh + '\'' +
                "beiyjie1='" + beiyjie1 + '\'' +
                "beiyjie2='" + beiyjie2 + '\'' +
                "beiyjie3='" + beiyjie3 + '\'' +
                "beiyjie4='" + beiyjie4 + '\'' +
                "beiyjie5='" + beiyjie5 + '\'' +
                "beiyzif1='" + beiyzif1 + '\'' +
                "beiyzif2='" + beiyzif2 + '\'' +
                "beiyzif3='" + beiyzif3 + '\'' +
                "beiyzif4='" + beiyzif4 + '\'' +
                "beiyzif5='" + beiyzif5 + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "weiylilv='" + weiylilv + '\'' +
                "xieyibho='" + xieyibho + '\'' +
                "sfkldqsh='" + sfkldqsh + '\'' +
                "lilvbcfd='" + lilvbcfd + '\'' +
                "dhdjbioz='" + dhdjbioz + '\'' +
                "gdlvsfdz='" + gdlvsfdz + '\'' +
                "zhableix='" + zhableix + '\'' +
                '}';
    }
}  
