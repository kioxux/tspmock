package cn.com.yusys.yusp.dto.server.biz.xdtz0044.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据客户号前往信贷查找房贷借据信息
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款结束日

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billNo='" + billNo + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                '}';
    }
}
