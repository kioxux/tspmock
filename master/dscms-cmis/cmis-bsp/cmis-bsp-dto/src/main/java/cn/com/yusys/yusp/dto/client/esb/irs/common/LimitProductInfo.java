package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：交易请求信息域:产品额度信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日15:10:55
 */
@JsonPropertyOrder(alphabetic = true)
public class LimitProductInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "detail_serno")
    private String detail_serno;//	授信分项流水号
    @JsonProperty(value = "item_id")
    private String item_id;//	额度台账编号
    @JsonProperty(value = "pro_no")
    private String pro_no;//	业务品种
    @JsonProperty(value = "pro_name")
    private String pro_name;//	业务品种名称
    @JsonProperty(value = "crd_lmt")
    private BigDecimal crd_lmt;//	授信额度（元）
    @JsonProperty(value = "sum_balance")
    private BigDecimal sum_balance;//	额度项下合同金额之和（含结清??
    @JsonProperty(value = "margin_ratio")
    private BigDecimal margin_ratio;//	保证金比例

    public String getDetail_serno() {
        return detail_serno;
    }

    public void setDetail_serno(String detail_serno) {
        this.detail_serno = detail_serno;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getPro_no() {
        return pro_no;
    }

    public void setPro_no(String pro_no) {
        this.pro_no = pro_no;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public BigDecimal getCrd_lmt() {
        return crd_lmt;
    }

    public void setCrd_lmt(BigDecimal crd_lmt) {
        this.crd_lmt = crd_lmt;
    }

    public BigDecimal getSum_balance() {
        return sum_balance;
    }

    public void setSum_balance(BigDecimal sum_balance) {
        this.sum_balance = sum_balance;
    }

    public BigDecimal getMargin_ratio() {
        return margin_ratio;
    }

    public void setMargin_ratio(BigDecimal margin_ratio) {
        this.margin_ratio = margin_ratio;
    }

    @Override
    public String toString() {
        return "LimitProductInfo{" +
                "detail_serno='" + detail_serno + '\'' +
                ", item_id='" + item_id + '\'' +
                ", pro_no='" + pro_no + '\'' +
                ", pro_name='" + pro_name + '\'' +
                ", crd_lmt=" + crd_lmt +
                ", sum_balance=" + sum_balance +
                ", margin_ratio=" + margin_ratio +
                '}';
    }
}