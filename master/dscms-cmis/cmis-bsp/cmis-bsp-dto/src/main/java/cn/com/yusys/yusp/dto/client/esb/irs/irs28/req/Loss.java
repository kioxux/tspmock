package cn.com.yusys.yusp.dto.client.esb.irs.irs28.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：财务信息同步
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 16:09
 * @since 2021/5/27 16:09
 */
@JsonPropertyOrder(alphabetic = true)
public class Loss implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "subjectno")
    private String subjectno;//科目编码
    @JsonProperty(value = "colvalue1")
    private BigDecimal colvalue1;//期初值
    @JsonProperty(value = "colvalue2")
    private BigDecimal colvalue2;//期末值

    public String getSubjectno() {
        return subjectno;
    }

    public void setSubjectno(String subjectno) {
        this.subjectno = subjectno;
    }

    public BigDecimal getColvalue1() {
        return colvalue1;
    }

    public void setColvalue1(BigDecimal colvalue1) {
        this.colvalue1 = colvalue1;
    }

    public BigDecimal getColvalue2() {
        return colvalue2;
    }

    public void setColvalue2(BigDecimal colvalue2) {
        this.colvalue2 = colvalue2;
    }

    @Override
    public String toString() {
        return "Loss{" +
                "subjectno='" + subjectno + '\'' +
                ", colvalue1=" + colvalue1 +
                ", colvalue2=" + colvalue2 +
                '}';
    }
}
