package cn.com.yusys.yusp.dto.server.biz.xddb0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarNo")
    private String guarNo;//押品编号

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "guarNo='" + guarNo + '\'' +
                '}';
    }
}
