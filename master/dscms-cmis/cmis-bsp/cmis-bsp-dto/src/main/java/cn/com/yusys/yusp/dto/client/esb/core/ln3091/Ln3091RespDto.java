package cn.com.yusys.yusp.dto.client.esb.core.ln3091;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：待付款指令查询
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3091RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lstdkzrfk")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3091.Lstdkzrfk> lstdkzrfk;//待付款指令登记簿[LIST]
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数

    public List<Lstdkzrfk> getLstdkzrfk() {
        return lstdkzrfk;
    }

    public void setLstdkzrfk(List<Lstdkzrfk> lstdkzrfk) {
        this.lstdkzrfk = lstdkzrfk;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    @Override
    public String toString() {
        return "Ln3091RespDto{" +
                "lstdkzrfk=" + lstdkzrfk +
                ", zongbish=" + zongbish +
                '}';
    }
}
