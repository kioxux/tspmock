package cn.com.yusys.yusp.dto.server.biz.xdtz0053.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号码
    @JsonProperty(value = "mainLoanManName")
    private String mainLoanManName;//主贷人姓名
    @JsonProperty(value = "mainLoanManCertNo")
    private String mainLoanManCertNo;//主贷人证件号码

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getMainLoanManName() {
        return mainLoanManName;
    }

    public void setMainLoanManName(String mainLoanManName) {
        this.mainLoanManName = mainLoanManName;
    }

    public String getMainLoanManCertNo() {
        return mainLoanManCertNo;
    }

    public void setMainLoanManCertNo(String mainLoanManCertNo) {
        this.mainLoanManCertNo = mainLoanManCertNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billNo='" + billNo + '\'' +
                ", mainLoanManName='" + mainLoanManName + '\'' +
                ", mainLoanManCertNo='" + mainLoanManCertNo + '\'' +
                '}';
    }
}
