package cn.com.yusys.yusp.dto.client.esb.core.ln3120.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：查询抵质押物录入信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3120RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "listdkjyzl")
    private List<Listdkjyzl> listdkjyzl;

    public List<Listdkjyzl> getListdkjyzl() {
        return listdkjyzl;
    }

    public void setListdkjyzl(List<Listdkjyzl> listdkjyzl) {
        this.listdkjyzl = listdkjyzl;
    }

    @Override
    public String toString() {
        return "Ln3120RespDto{" +
                "listdkjyzl=" + listdkjyzl +
                '}';
    }
}
