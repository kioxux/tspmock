package cn.com.yusys.yusp.dto.client.esb.core.dp2200;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：保证金账户部提
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2200RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "kehuzhlx")
    private String kehuzhlx;//客户账号类型
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh;//子账户序号
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//货币代号
    @JsonProperty(value = "chaohubz")
    private String chaohubz;//账户钞汇标志
    @JsonProperty(value = "zhhuzwmc")
    private String zhhuzwmc;//账户名称
    @JsonProperty(value = "jiaoyije")
    private BigDecimal jiaoyije;//交易金额
    @JsonProperty(value = "zhdaoqir")
    private String zhdaoqir;//账户到期日
    @JsonProperty(value = "guiylius")
    private String guiylius;//柜员流水号
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "zijinqux")
    private String zijinqux;//资金去向
    @JsonProperty(value = "xpizleix")
    private String xpizleix;//新凭证种类
    @JsonProperty(value = "xpnzxhao")
    private String xpnzxhao;//新凭证序号
    @JsonProperty(value = "skrkhuzh")
    private String skrkhuzh;//收款人客户账号
    @JsonProperty(value = "skrzhamc")
    private String skrzhamc;//收款人账户名称
    @JsonProperty(value = "xiaozxuh")
    private String xiaozxuh;//销账序号
    @JsonProperty(value = "lixilixi")
    private BigDecimal lixilixi;//利息
    @JsonProperty(value = "lixishui")
    private BigDecimal lixishui;//利息税
    @JsonProperty(value = "shijzqje")
    private BigDecimal shijzqje;//实际支取金额
    @JsonProperty(value = "chapbhao")
    private String chapbhao;//产品编号
    @JsonProperty(value = "chanpshm")
    private String chanpshm;//产品说明
    @JsonProperty(value = "zhhufenl")
    private String zhhufenl;//账户分类
    @JsonProperty(value = "zhshuxin")
    private String zhshuxin;//账户属性
    @JsonProperty(value = "cunqiiii")
    private String cunqiiii;//存期
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "shijlilv")
    private BigDecimal shijlilv;//实际利率
    @JsonProperty(value = "zhanghye")
    private BigDecimal zhanghye;//账户余额
    @JsonProperty(value = "daokjine")
    private BigDecimal daokjine;//倒扣金额
    @JsonProperty(value = "khhhming")
    private String khhhming;//单位开户行行名
    @JsonProperty(value = "cunkzlei")
    private String cunkzlei;//存款种类
    @JsonProperty(value = "zhcunfsh")
    private String zhcunfsh;//转存方式
    @JsonProperty(value = "chapqcmc")
    private String chapqcmc;//产品期次名称
    @JsonProperty(value = "chapqcbh")
    private String chapqcbh;//产品期次编号
    @JsonProperty(value = "kaihjine")
    private BigDecimal kaihjine;//开户金额
    @JsonProperty(value = "lixizjqx")
    private String lixizjqx;//利息资金去向


    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getZhdaoqir() {
        return zhdaoqir;
    }

    public void setZhdaoqir(String zhdaoqir) {
        this.zhdaoqir = zhdaoqir;
    }

    public String getGuiylius() {
        return guiylius;
    }

    public void setGuiylius(String guiylius) {
        this.guiylius = guiylius;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getZijinqux() {
        return zijinqux;
    }

    public void setZijinqux(String zijinqux) {
        this.zijinqux = zijinqux;
    }

    public String getXpizleix() {
        return xpizleix;
    }

    public void setXpizleix(String xpizleix) {
        this.xpizleix = xpizleix;
    }

    public String getXpnzxhao() {
        return xpnzxhao;
    }

    public void setXpnzxhao(String xpnzxhao) {
        this.xpnzxhao = xpnzxhao;
    }

    public String getSkrkhuzh() {
        return skrkhuzh;
    }

    public void setSkrkhuzh(String skrkhuzh) {
        this.skrkhuzh = skrkhuzh;
    }

    public String getSkrzhamc() {
        return skrzhamc;
    }

    public void setSkrzhamc(String skrzhamc) {
        this.skrzhamc = skrzhamc;
    }

    public String getXiaozxuh() {
        return xiaozxuh;
    }

    public void setXiaozxuh(String xiaozxuh) {
        this.xiaozxuh = xiaozxuh;
    }

    public BigDecimal getLixilixi() {
        return lixilixi;
    }

    public void setLixilixi(BigDecimal lixilixi) {
        this.lixilixi = lixilixi;
    }

    public BigDecimal getLixishui() {
        return lixishui;
    }

    public void setLixishui(BigDecimal lixishui) {
        this.lixishui = lixishui;
    }

    public BigDecimal getShijzqje() {
        return shijzqje;
    }

    public void setShijzqje(BigDecimal shijzqje) {
        this.shijzqje = shijzqje;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getChanpshm() {
        return chanpshm;
    }

    public void setChanpshm(String chanpshm) {
        this.chanpshm = chanpshm;
    }

    public String getZhhufenl() {
        return zhhufenl;
    }

    public void setZhhufenl(String zhhufenl) {
        this.zhhufenl = zhhufenl;
    }

    public String getZhshuxin() {
        return zhshuxin;
    }

    public void setZhshuxin(String zhshuxin) {
        this.zhshuxin = zhshuxin;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public BigDecimal getShijlilv() {
        return shijlilv;
    }

    public void setShijlilv(BigDecimal shijlilv) {
        this.shijlilv = shijlilv;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public BigDecimal getDaokjine() {
        return daokjine;
    }

    public void setDaokjine(BigDecimal daokjine) {
        this.daokjine = daokjine;
    }

    public String getKhhhming() {
        return khhhming;
    }

    public void setKhhhming(String khhhming) {
        this.khhhming = khhhming;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getZhcunfsh() {
        return zhcunfsh;
    }

    public void setZhcunfsh(String zhcunfsh) {
        this.zhcunfsh = zhcunfsh;
    }

    public String getChapqcmc() {
        return chapqcmc;
    }

    public void setChapqcmc(String chapqcmc) {
        this.chapqcmc = chapqcmc;
    }

    public String getChapqcbh() {
        return chapqcbh;
    }

    public void setChapqcbh(String chapqcbh) {
        this.chapqcbh = chapqcbh;
    }

    public BigDecimal getKaihjine() {
        return kaihjine;
    }

    public void setKaihjine(BigDecimal kaihjine) {
        this.kaihjine = kaihjine;
    }

    public String getLixizjqx() {
        return lixizjqx;
    }

    public void setLixizjqx(String lixizjqx) {
        this.lixizjqx = lixizjqx;
    }

    @Override
    public String toString() {
        return "Dp2200RespDto{" +
                "kehuzhao='" + kehuzhao + '\'' +
                "kehuzhlx='" + kehuzhlx + '\'' +
                "zhhaoxuh='" + zhhaoxuh + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                "chaohubz='" + chaohubz + '\'' +
                "zhhuzwmc='" + zhhuzwmc + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "zhdaoqir='" + zhdaoqir + '\'' +
                "guiylius='" + guiylius + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "zijinqux='" + zijinqux + '\'' +
                "xpizleix='" + xpizleix + '\'' +
                "xpnzxhao='" + xpnzxhao + '\'' +
                "skrkhuzh='" + skrkhuzh + '\'' +
                "skrzhamc='" + skrzhamc + '\'' +
                "xiaozxuh='" + xiaozxuh + '\'' +
                "lixilixi='" + lixilixi + '\'' +
                "lixishui='" + lixishui + '\'' +
                "shijzqje='" + shijzqje + '\'' +
                "chapbhao='" + chapbhao + '\'' +
                "chanpshm='" + chanpshm + '\'' +
                "zhhufenl='" + zhhufenl + '\'' +
                "zhshuxin='" + zhshuxin + '\'' +
                "cunqiiii='" + cunqiiii + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "shijlilv='" + shijlilv + '\'' +
                "zhanghye='" + zhanghye + '\'' +
                "daokjine='" + daokjine + '\'' +
                "khhhming='" + khhhming + '\'' +
                "cunkzlei='" + cunkzlei + '\'' +
                "zhcunfsh='" + zhcunfsh + '\'' +
                "chapqcmc='" + chapqcmc + '\'' +
                "chapqcbh='" + chapqcbh + '\'' +
                "kaihjine='" + kaihjine + '\'' +
                "lixizjqx='" + lixizjqx + '\'' +
                '}';
    }
}  
