package cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @类名 Xwd001ReqDto
 * @描述 请求Dto：贷款申请接口
 * @作者 黄勃
 * @时间 2021/10/12 16:48
 **/
@JsonPropertyOrder(alphabetic = true)
public class Xwd001ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "businessId")
    private String businessId;//行内申请流水号

    @JsonProperty(value = "applName")
    private String applName;//申请人姓名

    @JsonProperty(value = "applCertificateType")
    private String applCertificateType;//申请人证件类型

    @JsonProperty(value = "applCertificateNumber")
    private String applCertificateNumber;//申请人证件号码

    @JsonProperty(value = "appluserType")
    private String appluserType;//申请人类型

    @JsonProperty(value = "companyName")
    private String companyName;//企业名称

    @JsonProperty(value = "uscc")
    private String uscc;//统一社会信用代码

    @JsonProperty(value = "taxCode")
    private String taxCode;//纳税人识别号

    @JsonProperty(value = "tel")
    private String tel;//联系方式

    @JsonProperty(value = "applAmount")
    private String applAmount;//贷款申请金额

    @JsonProperty(value = "applTimeLimit")
    private String applTimeLimit;//贷款期限

    @JsonProperty(value = "loanPurposeCode")
    private String loanPurposeCode;//资金用途

    @JsonProperty(value = "productCode")
    private String productCode;//产品代码

    @JsonProperty(value = "marketingNumber")
    private String marketingNumber;//营销人员编号

    @JsonProperty(value = "marketingName")
    private String marketingName;//营销人员姓名

    @JsonProperty(value = "applArea")
    private String applArea;//客户区域

    @JsonProperty(value = "detailAddress")
    private String detailAddress;//详细地址

    @JsonProperty(value = "creditAuthCodeList")
    private String creditAuthCodeList;//贷款授权或声明信息

    @JsonProperty(value = "fraudGID")
    private String fraudGID;//百融全局设备标识

    @JsonProperty(value = "channelCode")
    private String channelCode;//渠道标记

    @JsonProperty(value = "existsHouse")
    private String existsHouse;//有无房产

    @JsonProperty(value = "houseValue")
    private String houseValue;//房屋估值

    @JsonProperty(value = "taxAuthorizationCode")
    private String taxAuthorizationCode;//税务授权码

    @JsonProperty(value = "marketPathCode")
    private String marketPathCode;//营销路径

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getApplName() {
        return applName;
    }

    public void setApplName(String applName) {
        this.applName = applName;
    }

    public String getApplCertificateType() {
        return applCertificateType;
    }

    public void setApplCertificateType(String applCertificateType) {
        this.applCertificateType = applCertificateType;
    }

    public String getApplCertificateNumber() {
        return applCertificateNumber;
    }

    public void setApplCertificateNumber(String applCertificateNumber) {
        this.applCertificateNumber = applCertificateNumber;
    }

    public String getAppluserType() {
        return appluserType;
    }

    public void setAppluserType(String appluserType) {
        this.appluserType = appluserType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUscc() {
        return uscc;
    }

    public void setUscc(String uscc) {
        this.uscc = uscc;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getApplAmount() {
        return applAmount;
    }

    public void setApplAmount(String applAmount) {
        this.applAmount = applAmount;
    }

    public String getApplTimeLimit() {
        return applTimeLimit;
    }

    public void setApplTimeLimit(String applTimeLimit) {
        this.applTimeLimit = applTimeLimit;
    }

    public String getLoanPurposeCode() {
        return loanPurposeCode;
    }

    public void setLoanPurposeCode(String loanPurposeCode) {
        this.loanPurposeCode = loanPurposeCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getMarketingNumber() {
        return marketingNumber;
    }

    public void setMarketingNumber(String marketingNumber) {
        this.marketingNumber = marketingNumber;
    }

    public String getMarketingName() {
        return marketingName;
    }

    public void setMarketingName(String marketingName) {
        this.marketingName = marketingName;
    }

    public String getApplArea() {
        return applArea;
    }

    public void setApplArea(String applArea) {
        this.applArea = applArea;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getCreditAuthCodeList() {
        return creditAuthCodeList;
    }

    public void setCreditAuthCodeList(String creditAuthCodeList) {
        this.creditAuthCodeList = creditAuthCodeList;
    }

    public String getFraudGID() {
        return fraudGID;
    }

    public void setFraudGID(String fraudGID) {
        this.fraudGID = fraudGID;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getExistsHouse() {
        return existsHouse;
    }

    public void setExistsHouse(String existsHouse) {
        this.existsHouse = existsHouse;
    }

    public String getHouseValue() {
        return houseValue;
    }

    public void setHouseValue(String houseValue) {
        this.houseValue = houseValue;
    }

    public String getTaxAuthorizationCode() {
        return taxAuthorizationCode;
    }

    public void setTaxAuthorizationCode(String taxAuthorizationCode) {
        this.taxAuthorizationCode = taxAuthorizationCode;
    }

    public String getMarketPathCode() {
        return marketPathCode;
    }

    public void setMarketPathCode(String marketPathCode) {
        this.marketPathCode = marketPathCode;
    }

    @Override
    public String toString() {
        return "Xwd001ReqDto{" +
                "businessId='" + businessId + '\'' +
                ", applName='" + applName + '\'' +
                ", applCertificateType='" + applCertificateType + '\'' +
                ", applCertificateNumber='" + applCertificateNumber + '\'' +
                ", appluserType='" + appluserType + '\'' +
                ", companyName='" + companyName + '\'' +
                ", uscc='" + uscc + '\'' +
                ", taxCode='" + taxCode + '\'' +
                ", tel='" + tel + '\'' +
                ", applAmount='" + applAmount + '\'' +
                ", applTimeLimit='" + applTimeLimit + '\'' +
                ", loanPurposeCode='" + loanPurposeCode + '\'' +
                ", productCode='" + productCode + '\'' +
                ", marketingNumber='" + marketingNumber + '\'' +
                ", marketingName='" + marketingName + '\'' +
                ", applArea='" + applArea + '\'' +
                ", detailAddress='" + detailAddress + '\'' +
                ", creditAuthCodeList='" + creditAuthCodeList + '\'' +
                ", fraudGID='" + fraudGID + '\'' +
                ", channelCode='" + channelCode + '\'' +
                ", existsHouse='" + existsHouse + '\'' +
                ", houseValue='" + houseValue + '\'' +
                ", taxAuthorizationCode='" + taxAuthorizationCode + '\'' +
                ", marketPathCode='" + marketPathCode + '\'' +
                '}';
    }
}
