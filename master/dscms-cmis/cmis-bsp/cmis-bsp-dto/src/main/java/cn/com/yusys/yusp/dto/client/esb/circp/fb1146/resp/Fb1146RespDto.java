package cn.com.yusys.yusp.dto.client.esb.circp.fb1146.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：受托信息审核
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1146RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Fb1146RespDto{" +
                '}';
    }
}  
