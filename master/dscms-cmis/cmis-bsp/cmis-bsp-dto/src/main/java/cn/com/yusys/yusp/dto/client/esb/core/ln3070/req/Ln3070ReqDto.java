package cn.com.yusys.yusp.dto.client.esb.core.ln3070.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：交易说明
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3070ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码
    @JsonProperty(value = "wjmingch")
    private String wjmingch;//文件名
    @JsonProperty(value = "jychlizt")
    private String jychlizt;//交易处理状态

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getWjmingch() {
        return wjmingch;
    }

    public void setWjmingch(String wjmingch) {
        this.wjmingch = wjmingch;
    }

    public String getJychlizt() {
        return jychlizt;
    }

    public void setJychlizt(String jychlizt) {
        this.jychlizt = jychlizt;
    }

    @Override
    public String toString() {
        return "Ln3070ReqDto{" +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                "wjmingch='" + wjmingch + '\'' +
                "jychlizt='" + jychlizt + '\'' +
                '}';
    }
}  
