package cn.com.yusys.yusp.dto.client.esb.doc.doc005.req;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：申请出库
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Doc005ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.util.List<cn.com.yusys.yusp.dto.client.esb.doc.doc005.req.IndexData> indexData;

    public List<IndexData> getIndexData() {
        return indexData;
    }

    public void setIndexData(List<IndexData> indexData) {
        this.indexData = indexData;
    }

    @Override
    public String toString() {
        return "Doc005ReqDto{" +
                "indexData=" + indexData +
                '}';
    }
}
