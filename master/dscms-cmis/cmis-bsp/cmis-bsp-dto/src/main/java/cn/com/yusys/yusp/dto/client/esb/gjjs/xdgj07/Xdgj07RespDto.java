package cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj07;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷发送台账信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdgj07RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
}
