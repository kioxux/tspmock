package cn.com.yusys.yusp.dto.client.esb.core.da3301.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：抵债资产入账
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3301RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "dbgdzcje")
    private BigDecimal dbgdzcje;//代保管抵债资产金额
    @JsonProperty(value = "dcldzcje")
    private BigDecimal dcldzcje;//待处理抵债资产金额
    @JsonProperty(value = "hfeijine")
    private BigDecimal hfeijine;//还费金额
    @JsonProperty(value = "huanbjee")
    private BigDecimal huanbjee;//还本金额
    @JsonProperty(value = "hxijinee")
    private BigDecimal hxijinee;//还息金额
    @JsonProperty(value = "dzzszhao")
    private String dzzszhao;//抵债暂收账号
    @JsonProperty(value = "dzzszzxh")
    private String dzzszzxh;//抵债暂收账号子序号
    @JsonProperty(value = "dbxdzcje")
    private BigDecimal dbxdzcje;//待变现抵债资产金额
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "dizruzfs")
    private String dizruzfs;//抵债入账方式
    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额
    @JsonProperty(value = "listnm1")
    private List<Listnm1> listnm1;//借据关联

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getDbgdzcje() {
        return dbgdzcje;
    }

    public void setDbgdzcje(BigDecimal dbgdzcje) {
        this.dbgdzcje = dbgdzcje;
    }

    public BigDecimal getDcldzcje() {
        return dcldzcje;
    }

    public void setDcldzcje(BigDecimal dcldzcje) {
        this.dcldzcje = dcldzcje;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public String getDzzszhao() {
        return dzzszhao;
    }

    public void setDzzszhao(String dzzszhao) {
        this.dzzszhao = dzzszhao;
    }

    public String getDzzszzxh() {
        return dzzszzxh;
    }

    public void setDzzszzxh(String dzzszzxh) {
        this.dzzszzxh = dzzszzxh;
    }

    public BigDecimal getDbxdzcje() {
        return dbxdzcje;
    }

    public void setDbxdzcje(BigDecimal dbxdzcje) {
        this.dbxdzcje = dbxdzcje;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getDizruzfs() {
        return dizruzfs;
    }

    public void setDizruzfs(String dizruzfs) {
        this.dizruzfs = dizruzfs;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public List<Listnm1> getListnm1() {
        return listnm1;
    }

    public void setListnm1(List<Listnm1> listnm1) {
        this.listnm1 = listnm1;
    }


    @Override
    public String toString() {
        return "Da3301RespDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "dbgdzcje='" + dbgdzcje + '\'' +
                "dcldzcje='" + dcldzcje + '\'' +
                "hfeijine='" + hfeijine + '\'' +
                "huanbjee='" + huanbjee + '\'' +
                "hxijinee='" + hxijinee + '\'' +
                "dzzszhao='" + dzzszhao + '\'' +
                "dzzszzxh='" + dzzszzxh + '\'' +
                "dbxdzcje='" + dbxdzcje + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "dizruzfs='" + dizruzfs + '\'' +
                "huankjee='" + huankjee + '\'' +
                "listnm1='" + listnm1 + '\'' +
                '}';
    }
}  
