package cn.com.yusys.yusp.dto.server.biz.xdcz0028.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 请求Dto：生成商贷账号数据文件
 * @Author zhangpeng
 * @Date 2021/4/27 21:52
 * @Version 1.0
 */
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "tranDate")
    private String tranDate;//交易日期
    @JsonProperty(value = "totlQnt")
    private String totlQnt;//总笔数
    @JsonProperty(value = "fileName")
    private String fileName;//文件名
    @JsonProperty(value = "seqNo")
    private String seqNo;//批次号
    public String  getCert_code() { return cert_code; }
    public void setCert_code(String cert_code ) { this.cert_code = cert_code;}
    public String  getCus_id() { return cus_id; }
    public void setCus_id(String cus_id ) { this.cus_id = cus_id;}
    public String  getCont_no() { return cont_no; }
    public void setCont_no(String cont_no ) { this.cont_no = cont_no;}
    public String  getTranDate() { return tranDate; }
    public void setTranDate(String tranDate ) { this.tranDate = tranDate;}
    public String  getTotlQnt() { return totlQnt; }
    public void setTotlQnt(String totlQnt ) { this.totlQnt = totlQnt;}
    public String  getFileName() { return fileName; }
    public void setFileName(String fileName ) { this.fileName = fileName;}
    public String  getSeqNo() { return seqNo; }
    public void setSeqNo(String seqNo ) { this.seqNo = seqNo;}
    @Override
    public String toString() {
        return "Xdcz0028ReqDto{" +
                "cert_code='" + cert_code+ '\'' +
                "cus_id='" + cus_id+ '\'' +
                "cont_no='" + cont_no+ '\'' +
                "tranDate='" + tranDate+ '\'' +
                "totlQnt='" + totlQnt+ '\'' +
                "fileName='" + fileName+ '\'' +
                "seqNo='" + seqNo+ '\'' +
                '}';
    }
}
