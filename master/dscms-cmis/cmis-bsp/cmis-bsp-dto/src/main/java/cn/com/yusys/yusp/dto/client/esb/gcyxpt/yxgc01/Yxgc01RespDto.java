package cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：贷后任务推送
 *
 * @author wrw
 * @version 1.0
 * @since 2021年8月14日28:09:30
 */
@JsonPropertyOrder(alphabetic = true)
public class Yxgc01RespDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;// 流水号

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    @Override
    public String toString() {
        return "Yxgc01RespDto{" +
                "serno='" + serno + '\'' +
                '}';
    }
}
