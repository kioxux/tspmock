package cn.com.yusys.yusp.dto.client.esb.core.ln3105;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款期供交易明细查询
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3105RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "lstqgmx")
    private List<Lstqgmx> lstqgmx;


    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List<Lstqgmx> getLstqgmx() {
        return lstqgmx;
    }

    public void setLstqgmx(List<Lstqgmx> lstqgmx) {
        this.lstqgmx = lstqgmx;
    }

    @Override
    public String toString() {
        return "Ln3105RespDto{" +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", zongbish=" + zongbish +
                ", lstqgmx=" + lstqgmx +
                '}';
    }
}
