package cn.com.yusys.yusp.dto.client.esb.wx.wxp003.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷将放款标识推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxp003RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Wxp003RespDto{" +
                '}';
    }
}  
