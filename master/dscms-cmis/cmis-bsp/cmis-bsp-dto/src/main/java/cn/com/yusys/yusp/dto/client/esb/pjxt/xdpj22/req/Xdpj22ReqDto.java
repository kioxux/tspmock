package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
/**
 * 请求Dto：根据批次号查询票号和票面金额
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj22ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sBatchNo")
    private String sBatchNo;//批次号

    public String getSBatchNo() {
        return sBatchNo;
    }

    public void setSBatchNo(String sBatchNo) {
        this.sBatchNo = sBatchNo;
    }

    @Override
    public String toString() {
        return "Xdpj22ReqDto{" +
                "sBatchNo='" + sBatchNo + '\'' +
                '}';
    }
}
