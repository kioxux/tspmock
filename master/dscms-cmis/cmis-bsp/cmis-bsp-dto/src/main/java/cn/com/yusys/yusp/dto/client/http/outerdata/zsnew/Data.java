package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 返回参数
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CODE")
    private String CODE;//		返回码
    @JsonProperty(value = "MSG")
    private String MSG;//			返回信息
    @JsonProperty(value = "ENT_INFO")
    private cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ENT_INFO ENT_INFO;
    @JsonIgnore
    public String getCODE() {
        return CODE;
    }
    @JsonIgnore
    public void setCODE(String CODE) {
        this.CODE = CODE;
    }
    @JsonIgnore
    public String getMSG() {
        return MSG;
    }
    @JsonIgnore
    public void setMSG(String MSG) {
        this.MSG = MSG;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ENT_INFO getENT_INFO() {
        return ENT_INFO;
    }
    @JsonIgnore
    public void setENT_INFO(cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ENT_INFO ENT_INFO) {
        this.ENT_INFO = ENT_INFO;
    }

    @Override
    public String toString() {
        return "Data{" +
                "CODE='" + CODE + '\'' +
                ", MSG='" + MSG + '\'' +
                ", ENT_INFO=" + ENT_INFO +
                '}';
    }
}
