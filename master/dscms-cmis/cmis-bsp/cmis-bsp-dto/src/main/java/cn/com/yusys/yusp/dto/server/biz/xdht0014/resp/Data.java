package cn.com.yusys.yusp.dto.server.biz.xdht0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：合同签订查询VX
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同号
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//到期日
    @JsonProperty(value = "exeRate")
    private BigDecimal exeRate;//执行利率
    @JsonProperty(value = "dailyInt")
    private BigDecimal dailyInt;//日利息
    @JsonProperty(value = "avlLmt")
    private BigDecimal avlLmt;//可用额度

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public BigDecimal getExeRate() {
        return exeRate;
    }

    public void setExeRate(BigDecimal exeRate) {
        this.exeRate = exeRate;
    }

    public BigDecimal getDailyInt() {
        return dailyInt;
    }

    public void setDailyInt(BigDecimal dailyInt) {
        this.dailyInt = dailyInt;
    }

    public BigDecimal getAvlLmt() {
        return avlLmt;
    }

    public void setAvlLmt(BigDecimal avlLmt) {
        this.avlLmt = avlLmt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contNo='" + contNo + '\'' +
                "cnContNo='" + cnContNo + '\'' +
                "curType='" + curType + '\'' +
                "contAmt='" + contAmt + '\'' +
                "contStartDate='" + contStartDate + '\'' +
                "contEndDate='" + contEndDate + '\'' +
                "exeRate='" + exeRate + '\'' +
                "dailyInt='" + dailyInt + '\'' +
                "avlLmt='" + avlLmt + '\'' +
                '}';
    }
}
