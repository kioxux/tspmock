package cn.com.yusys.yusp.dto.server.biz.xdht0029.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户非小微业务经营性贷款总金额查询
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号
    public String getCertNo() { return certNo; }
    public void setCertNo(String certNo) { this.certNo = certNo;}
    @Override
    public String toString() {
        return "Xdht0029ReqDto{" +
                "certNo='" + certNo+ '\'' +
                '}';
    }
}