package cn.com.yusys.yusp.dto.client.esb.core.ln3066.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：资产转让借据筛选
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3066ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yewufenl")
    private String yewufenl;//业务分类
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "kshchpdm")
    private String kshchpdm;//可售产品代码
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期
    @JsonProperty(value = "djqsriqi")
    private String djqsriqi;//冻结起始日期
    @JsonProperty(value = "djdqriqi")
    private String djdqriqi;//冻结到期日期
    @JsonProperty(value = "meiczxje")
    private BigDecimal meiczxje;//每次最小金额
    @JsonProperty(value = "meiczdje")
    private BigDecimal meiczdje;//每次最大金额
    @JsonProperty(value = "dkzhhzht")
    private String dkzhhzht;//贷款账户状态
    @JsonProperty(value = "daikxtai")
    private String daikxtai;//贷款形态
    @JsonProperty(value = "jzhdkbzh")
    private String jzhdkbzh;//减值贷款标志
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数

    public String getYewufenl() {
        return yewufenl;
    }

    public void setYewufenl(String yewufenl) {
        this.yewufenl = yewufenl;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public String getDjqsriqi() {
        return djqsriqi;
    }

    public void setDjqsriqi(String djqsriqi) {
        this.djqsriqi = djqsriqi;
    }

    public String getDjdqriqi() {
        return djdqriqi;
    }

    public void setDjdqriqi(String djdqriqi) {
        this.djdqriqi = djdqriqi;
    }

    public BigDecimal getMeiczxje() {
        return meiczxje;
    }

    public void setMeiczxje(BigDecimal meiczxje) {
        this.meiczxje = meiczxje;
    }

    public BigDecimal getMeiczdje() {
        return meiczdje;
    }

    public void setMeiczdje(BigDecimal meiczdje) {
        this.meiczdje = meiczdje;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getJzhdkbzh() {
        return jzhdkbzh;
    }

    public void setJzhdkbzh(String jzhdkbzh) {
        this.jzhdkbzh = jzhdkbzh;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Ln3066ReqDto{" +
                "yewufenl='" + yewufenl + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "kshchpdm='" + kshchpdm + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "zhzhriqi='" + zhzhriqi + '\'' +
                "djqsriqi='" + djqsriqi + '\'' +
                "djdqriqi='" + djdqriqi + '\'' +
                "meiczxje='" + meiczxje + '\'' +
                "meiczdje='" + meiczdje + '\'' +
                "dkzhhzht='" + dkzhhzht + '\'' +
                "daikxtai='" + daikxtai + '\'' +
                "jzhdkbzh='" + jzhdkbzh + '\'' +
                "qishibis='" + qishibis + '\'' +
                "chxunbis='" + chxunbis + '\'' +
                '}';
    }
}  
