package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi1;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：ESB信贷查询接口（处理码credi1）
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Credi1RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zxSerno")
    private String zxSerno;//	交易流水号	;	是	;	本次查询交易流水号，区分查询交易的唯一标识
    @JsonProperty(value = "id")
    private String id;//	业务明细ID	;	否	;	预留字段，未返回
    @JsonProperty(value = "servsq")
    private String servsq;//	渠道流水 	;	否	;	预留字段，未返回
    @JsonProperty(value = "docid")
    private String docid;//	授权书id	;	否	;	预留字段，未返回
    @JsonProperty(value = "repDate")
    private String repDate;//	报告生成日期	;	否	;	预留字段，未返回

    public String getZxSerno() {
        return zxSerno;
    }

    public void setZxSerno(String zxSerno) {
        this.zxSerno = zxSerno;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public String getRepDate() {
        return repDate;
    }

    public void setRepDate(String repDate) {
        this.repDate = repDate;
    }

    @Override
    public String toString() {
        return "Credi1RespDto{" +
                "zxSerno='" + zxSerno + '\'' +
                ", id='" + id + '\'' +
                ", servsq='" + servsq + '\'' +
                ", docid='" + docid + '\'' +
                ", repDate='" + repDate + '\'' +
                '}';
    }
}
