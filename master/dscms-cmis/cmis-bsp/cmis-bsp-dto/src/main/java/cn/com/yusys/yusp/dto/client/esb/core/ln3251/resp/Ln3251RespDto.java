package cn.com.yusys.yusp.dto.client.esb.core.ln3251.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：贷款指定日期利息试算
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3251RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "zongbish")
    private String zongbish;// 总笔数
    @JsonProperty(value = "lstkhzmx")
    private List<Lstkhzmx> lstkhzmx_ARRAY; // 贷款客户帐明细查询输出

    public String getZongbish() {
        return zongbish;
    }

    public void setZongbish(String zongbish) {
        this.zongbish = zongbish;
    }

    public List getLstkhzmx_ARRAY() {
        return lstkhzmx_ARRAY;
    }

    public void setLstkhzmx_ARRAY(List lstkhzmx_ARRAY) {
        this.lstkhzmx_ARRAY = lstkhzmx_ARRAY;
    }

    @Override
    public String toString() {
        return "Ln3251RespDto{" +
                "zongbish='" + zongbish + '\'' +
                ", lstkhzmx_ARRAY=" + lstkhzmx_ARRAY +
                '}';
    }
}
