package cn.com.yusys.yusp.dto.server.biz.hyy.babigc01;

import cn.com.yusys.yusp.dto.server.HyyRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：获取抵押信息详情
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Babigc01RespDto extends HyyRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.server.biz.hyy.babigc01.Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Babigc01RespDto{" +
                "data=" + data +
                '}';
    }
}
