package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 响应Dto：获取还款记录的借据一览信息，包括借据号、借据金额、借据余额
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bill_no")
    private String bill_no;//借据号
    @JsonProperty(value = "loan_amount")
    private BigDecimal loan_amount;//借据金额
    @JsonProperty(value = "loan_balance")
    private BigDecimal loan_balance;//借据余额

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public BigDecimal getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(BigDecimal loan_amount) {
        this.loan_amount = loan_amount;
    }

    public BigDecimal getLoan_balance() {
        return loan_balance;
    }

    public void setLoan_balance(BigDecimal loan_balance) {
        this.loan_balance = loan_balance;
    }

    @Override
    public String toString() {
        return "List{" +
                "bill_no='" + bill_no + '\'' +
                ", loan_amount=" + loan_amount +
                ", loan_balance=" + loan_balance +
                '}';
    }
}
