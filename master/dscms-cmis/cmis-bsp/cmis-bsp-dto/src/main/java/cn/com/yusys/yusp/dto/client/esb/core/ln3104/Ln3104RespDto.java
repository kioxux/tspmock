package cn.com.yusys.yusp.dto.client.esb.core.ln3104;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：客户账交易明细查询
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3104RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "lstkhzmx")
    private List<Lstkhzmx> lstkhzmx;//客户账交易明细


    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List<Lstkhzmx> getLstkhzmx() {
        return lstkhzmx;
    }

    public void setLstkhzmx(List<Lstkhzmx> lstkhzmx) {
        this.lstkhzmx = lstkhzmx;
    }

    @Override
    public String toString() {
        return "Ln3104RespDto{" +
                ", zongbish=" + zongbish +
                ", lstkhzmx=" + lstkhzmx +
                '}';
    }
}
