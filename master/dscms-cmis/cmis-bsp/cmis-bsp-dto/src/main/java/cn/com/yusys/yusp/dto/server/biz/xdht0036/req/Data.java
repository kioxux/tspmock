package cn.com.yusys.yusp.dto.server.biz.xdht0036.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款起始日
    @JsonProperty(value = "managerId")
    private String managerId;//管户客户经理编号
    @JsonProperty(value = "surveySerno")
    private String surveySerno;//调查流水



    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "queryType='" + queryType + '\'' +
                "cusId='" + cusId + '\'' +
                "loanStartDate='" + loanStartDate + '\'' +
                "managerId='" + managerId + '\'' +
                "surveySerno='" + surveySerno + '\'' +
                '}';
    }
}
