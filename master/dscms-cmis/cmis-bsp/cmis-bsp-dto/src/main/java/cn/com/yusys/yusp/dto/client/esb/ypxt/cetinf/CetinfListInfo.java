package cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 请求DTO：权证信息同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class CetinfListInfo {
    @JsonProperty(value = "yptybh")
    private String yptybh; // 押品统一编号


    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    @Override
    public String toString() {
        return "CetinfListInfo{" +
                "yptybh='" + yptybh + '\'' +
                '}';
    }
}
