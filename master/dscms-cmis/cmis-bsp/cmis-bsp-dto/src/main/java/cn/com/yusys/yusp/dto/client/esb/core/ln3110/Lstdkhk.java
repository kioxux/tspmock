package cn.com.yusys.yusp.dto.client.esb.core.ln3110;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：还款计划
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkhk implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "benqqish")
    private Integer benqqish;//本期期数
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "huankriq")
    private String huankriq;//还款日期
    @JsonProperty(value = "kxqdqirq")
    private String kxqdqirq;//宽限期到期日
    @JsonProperty(value = "jixibenj")
    private BigDecimal jixibenj;//计息本金
    @JsonProperty(value = "leijcslx")
    private BigDecimal leijcslx;//累计产生利息
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "zhanghye")
    private BigDecimal zhanghye;//账户余额

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getHuankriq() {
        return huankriq;
    }

    public void setHuankriq(String huankriq) {
        this.huankriq = huankriq;
    }

    public String getKxqdqirq() {
        return kxqdqirq;
    }

    public void setKxqdqirq(String kxqdqirq) {
        this.kxqdqirq = kxqdqirq;
    }

    public BigDecimal getJixibenj() {
        return jixibenj;
    }

    public void setJixibenj(BigDecimal jixibenj) {
        this.jixibenj = jixibenj;
    }

    public BigDecimal getLeijcslx() {
        return leijcslx;
    }

    public void setLeijcslx(BigDecimal leijcslx) {
        this.leijcslx = leijcslx;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    @Override
    public String toString() {
        return "Lstdkhk{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "benqqish='" + benqqish + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "huankriq='" + huankriq + '\'' +
                "kxqdqirq='" + kxqdqirq + '\'' +
                "jixibenj='" + jixibenj + '\'' +
                "leijcslx='" + leijcslx + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "zhanghye='" + zhanghye + '\'' +
                '}';
    }
}

