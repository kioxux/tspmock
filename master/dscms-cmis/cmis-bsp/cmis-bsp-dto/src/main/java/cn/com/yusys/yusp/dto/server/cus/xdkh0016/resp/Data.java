package cn.com.yusys.yusp.dto.server.cus.xdkh0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询是否我行关系人
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFlg")
    private String opFlg;//是否在我行关系人名单中
    @JsonProperty(value = "qnt")
    private Integer qnt;//数量

    public String getOpFlg() {
        return opFlg;
    }

    public void setOpFlg(String opFlg) {
        this.opFlg = opFlg;
    }

    public Integer getQnt() {
        return qnt;
    }

    public void setQnt(Integer qnt) {
        this.qnt = qnt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "opFlg='" + opFlg + '\'' +
                ", qnt='" + qnt + '\'' +
                '}';
    }
}