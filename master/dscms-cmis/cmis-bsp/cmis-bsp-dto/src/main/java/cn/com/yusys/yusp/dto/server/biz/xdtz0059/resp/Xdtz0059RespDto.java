package cn.com.yusys.yusp.dto.server.biz.xdtz0059.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0059.resp.Data;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：更新信贷台账信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0059RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.server.biz.xdtz0059.resp.Data data;

    public cn.com.yusys.yusp.dto.server.biz.xdtz0059.resp.Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdtz0059RespDto{" +
                "data=" + data +
                '}';
    }
}
