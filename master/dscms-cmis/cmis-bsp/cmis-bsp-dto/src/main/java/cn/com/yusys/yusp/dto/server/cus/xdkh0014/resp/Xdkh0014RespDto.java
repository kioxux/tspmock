package cn.com.yusys.yusp.dto.server.cus.xdkh0014.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：省心E付白名单信息维护
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0014RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdkh0014RespDto{" +
                "data=" + data +
                '}';
    }
}
