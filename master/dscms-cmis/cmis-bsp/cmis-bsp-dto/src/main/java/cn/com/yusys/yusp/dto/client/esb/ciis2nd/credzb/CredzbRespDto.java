package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：指标通用接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CredzbRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "ruleCode")
    private String ruleCode;//指标编码
    @JsonProperty(value = "discribe")
    private String discribe;//指标描述
    @JsonProperty(value = "result")
    private String result;//查询结果
    @JsonProperty(value = "r001")
    private R001 r001;//个人根据业务号获取指定报告
    @JsonProperty(value = "r002")
    private R002 r002;//企业根据业务号获取指定报告
    @JsonProperty(value = "r003")
    private R003 r003;//个人多少天内最新本地征信
    @JsonProperty(value = "r004")
    private R004 r004;//企业多少天内最新本地征信
    @JsonProperty(value = "r005")
    private R005 r005;//个人获取客户最新授权
    @JsonProperty(value = "r006")
    private R006 r006;//企业获取客户最新授权
    @JsonProperty(value = "r007")
    private R007 r007;//小微优企贷风险提示
    @JsonProperty(value = "r008")
    private R008 r008;//零售风险提示
    @JsonProperty(value = "r009")
    private R009 r009;//小微征信信息
    @JsonProperty(value = "r010")
    private R010 r010;//小微征信企业信息

    public R010 getR010() {
        return r010;
    }

    public void setR010(R010 r010) {
        this.r010 = r010;
    }

    public R009 getR009() {
        return r009;
    }

    public void setR009(R009 r009) {
        this.r009 = r009;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getDiscribe() {
        return discribe;
    }

    public void setDiscribe(String discribe) {
        this.discribe = discribe;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public R001 getR001() {
        return r001;
    }

    public void setR001(R001 r001) {
        this.r001 = r001;
    }

    public R002 getR002() {
        return r002;
    }

    public void setR002(R002 r002) {
        this.r002 = r002;
    }

    public R003 getR003() {
        return r003;
    }

    public void setR003(R003 r003) {
        this.r003 = r003;
    }

    public R004 getR004() {
        return r004;
    }

    public void setR004(R004 r004) {
        this.r004 = r004;
    }

    public R005 getR005() {
        return r005;
    }

    public void setR005(R005 r005) {
        this.r005 = r005;
    }

    public R006 getR006() {
        return r006;
    }

    public void setR006(R006 r006) {
        this.r006 = r006;
    }

    public R007 getR007() {
        return r007;
    }

    public void setR007(R007 r007) {
        this.r007 = r007;
    }

    public R008 getR008() {
        return r008;
    }

    public void setR008(R008 r008) {
        this.r008 = r008;
    }

    @Override
    public String toString() {
        return "CredzbRespDto{" +
                "servsq='" + servsq + '\'' +
                ", ruleCode='" + ruleCode + '\'' +
                ", discribe='" + discribe + '\'' +
                ", result='" + result + '\'' +
                ", r001=" + r001 +
                ", r002=" + r002 +
                ", r003=" + r003 +
                ", r004=" + r004 +
                ", r005=" + r005 +
                ", r006=" + r006 +
                ", r007=" + r007 +
                ", r008=" + r008 +
                ", r009=" + r009 +
                ", r010=" + r010 +
                '}';
    }
}
