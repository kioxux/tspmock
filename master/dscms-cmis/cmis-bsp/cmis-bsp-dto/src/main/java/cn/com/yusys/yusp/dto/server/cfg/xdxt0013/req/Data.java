package cn.com.yusys.yusp.dto.server.cfg.xdxt0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/25 10:06:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 10:06
 * @since 2021/5/25 10:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "optType")
    private String optType;//类型
    @JsonProperty(value = "enname")
    private String enname;//英文名
    @JsonProperty(value = "abvenname")
    private String abvenname;//上级字段名

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    public String getAbvenname() {
        return abvenname;
    }

    public void setAbvenname(String abvenname) {
        this.abvenname = abvenname;
    }

    @Override
    public String toString() {
        return "Xdxt0013ReqDto{" +
                "optType='" + optType + '\'' +
                "enname='" + enname + '\'' +
                "abvenname='" + abvenname + '\'' +
                '}';
    }
}
