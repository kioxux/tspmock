package cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.resp;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 响应Dto：新增批次报表数据
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cbocr1RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "success")
    private String success;//是否请求成功 1 成功
    @JsonProperty(value = "erorcd")
    private String erorcd;//错误码
    @JsonProperty(value = "erortx")
    private String erortx;//错误信息
    @JsonProperty(value = "taskId")
    private String taskId;//返回结果：任务id

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((erorcd == null) ? 0 : erorcd.hashCode());
		result = prime * result + ((erortx == null) ? 0 : erortx.hashCode());
		result = prime * result + ((success == null) ? 0 : success.hashCode());
		result = prime * result + ((taskId == null) ? 0 : taskId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cbocr1RespDto other = (Cbocr1RespDto) obj;
		if (erorcd == null) {
			if (other.erorcd != null)
				return false;
		} else if (!erorcd.equals(other.erorcd))
			return false;
		if (erortx == null) {
			if (other.erortx != null)
				return false;
		} else if (!erortx.equals(other.erortx))
			return false;
		if (success == null) {
			if (other.success != null)
				return false;
		} else if (!success.equals(other.success))
			return false;
		if (taskId == null) {
			if (other.taskId != null)
				return false;
		} else if (!taskId.equals(other.taskId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cbocr1RespDto [success=" + success + ", erorcd=" + erorcd
				+ ", erortx=" + erortx + ", taskId=" + taskId + "]";
	}
    
}
