package cn.com.yusys.yusp.dto.server.biz.xddb0021.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据客户名查询抵押物类型
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guaranty_id")
    private String guaranty_id;//押品编号

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    @Override
    public String toString() {
        return "Data{" +
                "guaranty_id='" + guaranty_id + '\'' +
                '}';
    }
}
