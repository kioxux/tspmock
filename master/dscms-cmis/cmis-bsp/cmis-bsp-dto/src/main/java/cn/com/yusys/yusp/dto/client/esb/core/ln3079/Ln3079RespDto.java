package cn.com.yusys.yusp.dto.client.esb.core.ln3079;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：贷款产品变更
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3079RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "xinchpdm")
    private String xinchpdm;//新产品代码
    @JsonProperty(value = "xinchpmc")
    private String xinchpmc;//新产品名称
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "kuaijilb")
    private String kuaijilb;//会计类别
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getXinchpdm() {
        return xinchpdm;
    }

    public void setXinchpdm(String xinchpdm) {
        this.xinchpdm = xinchpdm;
    }

    public String getXinchpmc() {
        return xinchpmc;
    }

    public void setXinchpmc(String xinchpmc) {
        this.xinchpmc = xinchpmc;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    @Override
    public String toString() {
        return "Ln3079RespDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "xinchpdm='" + xinchpdm + '\'' +
                "xinchpmc='" + xinchpmc + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "kuaijilb='" + kuaijilb + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                '}';
    }
}  
