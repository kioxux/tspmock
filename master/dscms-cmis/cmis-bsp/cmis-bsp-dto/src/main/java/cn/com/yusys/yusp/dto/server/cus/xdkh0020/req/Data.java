package cn.com.yusys.yusp.dto.server.cus.xdkh0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：小微平台请求信贷线上开户
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusStatus")
    private String cusStatus;//客户状态
    @JsonProperty(value = "cusType")
    private String cusType;//客户分类
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//身份证号
    @JsonProperty(value = "nation")
    private String nation;//国籍
    @JsonProperty(value = "phone")
    private String phone;//电话号码
    @JsonProperty(value = "sexcode")
    private String sexcode;//性别
    @JsonProperty(value = "borndate")
    private String borndate;//出生日期
    @JsonProperty(value = "indivMarSt")
    private String indivMarSt;//婚姻状况
    @JsonProperty(value = "indivEdt")
    private String indivEdt;//文化程度
    @JsonProperty(value = "postAddr")
    private String postAddr;//通讯地址
    @JsonProperty(value = "postCode")
    private String postCode;//邮政编码
    @JsonProperty(value = "indivOcc")
    private String indivOcc;//从事职业
    @JsonProperty(value = "indivComName")
    private String indivComName;//工作单位
    @JsonProperty(value = "indivComJobTtl")
    private String indivComJobTtl;//职务
    @JsonProperty(value = "indivRsdAddr")
    private String indivRsdAddr;//居住地址
    @JsonProperty(value = "indivZipCode")
    private String indivZipCode;//居住地址邮编
    @JsonProperty(value = "indivRsdSt")
    private String indivRsdSt;//居住状况
    @JsonProperty(value = "areaCode")
    private String areaCode;//区域编号
    @JsonProperty(value = "agriFlg")
    private String agriFlg;//是否农户
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//管护机构

    public String getCusStatus() {
        return cusStatus;
    }

    public void setCusStatus(String cusStatus) {
        this.cusStatus = cusStatus;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSexcode() {
        return sexcode;
    }

    public void setSexcode(String sexcode) {
        this.sexcode = sexcode;
    }

    public String getBorndate() {
        return borndate;
    }

    public void setBorndate(String borndate) {
        this.borndate = borndate;
    }

    public String getIndivMarSt() {
        return indivMarSt;
    }

    public void setIndivMarSt(String indivMarSt) {
        this.indivMarSt = indivMarSt;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public String getPostAddr() {
        return postAddr;
    }

    public void setPostAddr(String postAddr) {
        this.postAddr = postAddr;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getIndivOcc() {
        return indivOcc;
    }

    public void setIndivOcc(String indivOcc) {
        this.indivOcc = indivOcc;
    }

    public String getIndivComName() {
        return indivComName;
    }

    public void setIndivComName(String indivComName) {
        this.indivComName = indivComName;
    }

    public String getIndivComJobTtl() {
        return indivComJobTtl;
    }

    public void setIndivComJobTtl(String indivComJobTtl) {
        this.indivComJobTtl = indivComJobTtl;
    }

    public String getIndivRsdAddr() {
        return indivRsdAddr;
    }

    public void setIndivRsdAddr(String indivRsdAddr) {
        this.indivRsdAddr = indivRsdAddr;
    }

    public String getIndivZipCode() {
        return indivZipCode;
    }

    public void setIndivZipCode(String indivZipCode) {
        this.indivZipCode = indivZipCode;
    }

    public String getIndivRsdSt() {
        return indivRsdSt;
    }

    public void setIndivRsdSt(String indivRsdSt) {
        this.indivRsdSt = indivRsdSt;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAgriFlg() {
        return agriFlg;
    }

    public void setAgriFlg(String agriFlg) {
        this.agriFlg = agriFlg;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

}
