package cn.com.yusys.yusp.dto.client.esb.irs.xirs27.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：工作台提示条数
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xirs27ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "userid1")
    private String userid1;//管户人编号

    public String getUserid1() {
        return userid1;
    }

    public void setUserid1(String userid1) {
        this.userid1 = userid1;
    }

    @Override
    public String toString() {
        return "Xirs27ReqDto{" +
                "userid1='" + userid1 + '\'' +
                '}';
    }
}  
