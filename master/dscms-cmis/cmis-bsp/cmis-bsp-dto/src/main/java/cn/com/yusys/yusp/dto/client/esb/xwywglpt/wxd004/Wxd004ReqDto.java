package cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷系统获取征信报送监管信息接口
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxd004ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indcertid")
    private String indcertid;//申请人证件号码

    public String getIndcertid() {
        return indcertid;
    }

    public void setIndcertid(String indcertid) {
        this.indcertid = indcertid;
    }

    @Override
    public String toString() {
        return "Wxd004ReqDto{" +
                "indcertid='" + indcertid + '\'' +
                '}';
    }
}
