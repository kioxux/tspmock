package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：获取还款记录的借据一览信息，包括借据号、借据金额、借据余额
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd11ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "trade_code")
    private String trade_code;//交易码
    @JsonProperty(value = "brchno")
    private String brchno;
    @JsonProperty(value = "contract_no")
    private String contract_no;//利翃平台贷款合同号

    public String getTrade_code() {
        return trade_code;
    }

    public void setTrade_code(String trade_code) {
        this.trade_code = trade_code;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Fbxd11ReqDto{" +
                "trade_code='" + trade_code + '\'' +
                ", brchno='" + brchno + '\'' +
                ", contract_no='" + contract_no + '\'' +
                '}';
    }
}
