package cn.com.yusys.yusp.dto.server.biz.xdzc0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/9 9:31
 * @since 2021/6/9 9:31
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//出账流水号
    @JsonProperty(value = "yxSerno")
    private String yxSerno;//发票影像ID

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getYxSerno() {
        return yxSerno;
    }

    public void setYxSerno(String yxSerno) {
        this.yxSerno = yxSerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billNo='" + billNo + '\'' +
                ", yxSerno='" + yxSerno + '\'' +
                '}';
    }
}
