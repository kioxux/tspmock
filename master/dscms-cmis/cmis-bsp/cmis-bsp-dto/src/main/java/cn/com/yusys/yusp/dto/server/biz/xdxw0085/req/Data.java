package cn.com.yusys.yusp.dto.server.biz.xdxw0085.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：共借人声明文本生产PDF接口
 *
 * @author
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    @JsonProperty(value = "cusName")
    private String cusName;//借款人姓名
    @JsonProperty(value = "userName")
    private String userName;//声明人
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "signDate")
    private String signDate;//申请日期
    @JsonProperty(value = "type")
    private String type;//借款人与声明人关系：01单位负责人,02实际控制人,03法定代表人,04配偶,05其他
    @JsonProperty(value = "other")
    private String other;//当选05时，填写其他说明
    @JsonProperty(value = "accoNo")
    private String accoNo;//银行账号

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getAccoNo() {
        return accoNo;
    }

    public void setAccoNo(String accoNo) {
        this.accoNo = accoNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusName='" + cusName + '\'' +
                "userName='" + userName + '\'' +
                "contNo='" + contNo + '\'' +
                "signDate='" + signDate + '\'' +
                "type='" + type + '\'' +
                "other='" + other + '\'' +
                "accoNo='" + accoNo + '\'' +
                '}';
    }
}
