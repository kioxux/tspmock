package cn.com.yusys.yusp.dto.client.esb.core.ln3245.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：核销登记簿查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LstHxDkxx implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zhwujgmc")
    private String zhwujgmc;//账务机构名称
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "kuaijilb")
    private String kuaijilb;//会计类别
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限
    @JsonProperty(value = "daikxtai")
    private String daikxtai;//贷款形态
    @JsonProperty(value = "yjfyjzht")
    private String yjfyjzht;//应计非应计状态
    @JsonProperty(value = "dkzhhzht")
    private String dkzhhzht;//贷款账户状态
    @JsonProperty(value = "dbdkkksx")
    private Integer dbdkkksx;//多笔贷款扣款顺序
    @JsonProperty(value = "dkyqkksx")
    private Integer dkyqkksx;//多笔贷款逾期扣款顺序
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "fafangje")
    private BigDecimal fafangje;//已发放金额
    @JsonProperty(value = "djiekfje")
    private BigDecimal djiekfje;//冻结可放金额
    @JsonProperty(value = "kffangje")
    private BigDecimal kffangje;//可发放金额
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "yingjitx")
    private BigDecimal yingjitx;//应计贴息
    @JsonProperty(value = "yingshtx")
    private BigDecimal yingshtx;//应收贴息
    @JsonProperty(value = "tiexfuli")
    private BigDecimal tiexfuli;//贴息复利
    @JsonProperty(value = "zhcwjyrq")
    private String zhcwjyrq;//最后财务交易日
    @JsonProperty(value = "kshchpdm")
    private String kshchpdm;//可售产品代码
    @JsonProperty(value = "kshchpmc")
    private String kshchpmc;//可售产品名称
    @JsonProperty(value = "xtqudhao")
    private String xtqudhao;//系统渠道号
    @JsonProperty(value = "xiaohugy")
    private String xiaohugy;//销户柜员
    @JsonProperty(value = "xiaohurq")
    private String xiaohurq;//销户日期
    @JsonProperty(value = "kaihujig")
    private String kaihujig;//开户机构
    @JsonProperty(value = "kaihguiy")
    private String kaihguiy;//开户柜员
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "hexiaobj")
    private BigDecimal hexiaobj;//核销本金
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "ljghhxbj")
    private BigDecimal ljghhxbj;//累计归还核销本金
    @JsonProperty(value = "yihxbjlx")
    private BigDecimal yihxbjlx;//已核销本金利息
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//贷款核销日期
    @JsonProperty(value = "jychgbzh")
    private String jychgbzh;//上日逾期标志
    @JsonProperty(value = "hexiaols")
    private String hexiaols;//核销流水


    public String getZhwujgmc() {
        return zhwujgmc;
    }

    public void setZhwujgmc(String zhwujgmc) {
        this.zhwujgmc = zhwujgmc;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public Integer getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Integer dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public Integer getDkyqkksx() {
        return dkyqkksx;
    }

    public void setDkyqkksx(Integer dkyqkksx) {
        this.dkyqkksx = dkyqkksx;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getFafangje() {
        return fafangje;
    }

    public void setFafangje(BigDecimal fafangje) {
        this.fafangje = fafangje;
    }

    public BigDecimal getDjiekfje() {
        return djiekfje;
    }

    public void setDjiekfje(BigDecimal djiekfje) {
        this.djiekfje = djiekfje;
    }

    public BigDecimal getKffangje() {
        return kffangje;
    }

    public void setKffangje(BigDecimal kffangje) {
        this.kffangje = kffangje;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getTiexfuli() {
        return tiexfuli;
    }

    public void setTiexfuli(BigDecimal tiexfuli) {
        this.tiexfuli = tiexfuli;
    }

    public String getZhcwjyrq() {
        return zhcwjyrq;
    }

    public void setZhcwjyrq(String zhcwjyrq) {
        this.zhcwjyrq = zhcwjyrq;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getXtqudhao() {
        return xtqudhao;
    }

    public void setXtqudhao(String xtqudhao) {
        this.xtqudhao = xtqudhao;
    }

    public String getXiaohugy() {
        return xiaohugy;
    }

    public void setXiaohugy(String xiaohugy) {
        this.xiaohugy = xiaohugy;
    }

    public String getXiaohurq() {
        return xiaohurq;
    }

    public void setXiaohurq(String xiaohurq) {
        this.xiaohurq = xiaohurq;
    }

    public String getKaihujig() {
        return kaihujig;
    }

    public void setKaihujig(String kaihujig) {
        this.kaihujig = kaihujig;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getLjghhxbj() {
        return ljghhxbj;
    }

    public void setLjghhxbj(BigDecimal ljghhxbj) {
        this.ljghhxbj = ljghhxbj;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJychgbzh() {
        return jychgbzh;
    }

    public void setJychgbzh(String jychgbzh) {
        this.jychgbzh = jychgbzh;
    }

    public String getHexiaols() {
        return hexiaols;
    }

    public void setHexiaols(String hexiaols) {
        this.hexiaols = hexiaols;
    }

    @Override
    public String toString() {
        return "LstHxDkxx{" +
                "zhwujgmc='" + zhwujgmc + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "kuaijilb='" + kuaijilb + '\'' +
                "kaihriqi='" + kaihriqi + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "dkqixian='" + dkqixian + '\'' +
                "daikxtai='" + daikxtai + '\'' +
                "yjfyjzht='" + yjfyjzht + '\'' +
                "dkzhhzht='" + dkzhhzht + '\'' +
                "dbdkkksx='" + dbdkkksx + '\'' +
                "dkyqkksx='" + dkyqkksx + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "hetongje='" + hetongje + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "fafangje='" + fafangje + '\'' +
                "djiekfje='" + djiekfje + '\'' +
                "kffangje='" + kffangje + '\'' +
                "zhchbjin='" + zhchbjin + '\'' +
                "yuqibjin='" + yuqibjin + '\'' +
                "dzhibjin='" + dzhibjin + '\'' +
                "daizbjin='" + daizbjin + '\'' +
                "ysyjlixi='" + ysyjlixi + '\'' +
                "csyjlixi='" + csyjlixi + '\'' +
                "ysqianxi='" + ysqianxi + '\'' +
                "csqianxi='" + csqianxi + '\'' +
                "ysyjfaxi='" + ysyjfaxi + '\'' +
                "csyjfaxi='" + csyjfaxi + '\'' +
                "yshofaxi='" + yshofaxi + '\'' +
                "cshofaxi='" + cshofaxi + '\'' +
                "yingjifx='" + yingjifx + '\'' +
                "fuxiiiii='" + fuxiiiii + '\'' +
                "yingjitx='" + yingjitx + '\'' +
                "yingshtx='" + yingshtx + '\'' +
                "tiexfuli='" + tiexfuli + '\'' +
                "zhcwjyrq='" + zhcwjyrq + '\'' +
                "kshchpdm='" + kshchpdm + '\'' +
                "kshchpmc='" + kshchpmc + '\'' +
                "xtqudhao='" + xtqudhao + '\'' +
                "xiaohugy='" + xiaohugy + '\'' +
                "xiaohurq='" + xiaohurq + '\'' +
                "kaihujig='" + kaihujig + '\'' +
                "kaihguiy='" + kaihguiy + '\'' +
                "nyuelilv='" + nyuelilv + '\'' +
                "hexiaobj='" + hexiaobj + '\'' +
                "hexiaolx='" + hexiaolx + '\'' +
                "ljghhxbj='" + ljghhxbj + '\'' +
                "yihxbjlx='" + yihxbjlx + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jychgbzh='" + jychgbzh + '\'' +
                "hexiaols='" + hexiaols + '\'' +
                '}';
    }
}  
