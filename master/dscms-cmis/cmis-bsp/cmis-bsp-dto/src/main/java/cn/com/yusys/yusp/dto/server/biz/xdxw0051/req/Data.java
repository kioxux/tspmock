package cn.com.yusys.yusp.dto.server.biz.xdxw0051.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据业务唯一编号查询无还本续贷贷销售收入
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applySerno")
    private String applySerno;//业务唯一编号

    public String getApplySerno() {
        return applySerno;
    }

    public void setApplySerno(String applySerno) {
        this.applySerno = applySerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "applySerno='" + applySerno + '\'' +
                '}';
    }
}
