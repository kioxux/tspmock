package cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：企业微信
 *
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class QywxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "realtime")
    private String realtime;
    @JsonProperty(value = "text")
    private String text;
    @JsonProperty(value = "mobile")
    private String mobile;

    public String getRealtime() {
        return realtime;
    }

    public void setRealtime(String realtime) {
        this.realtime = realtime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "QywxReqDto{" +
                "realtime='" + realtime + '\'' +
                ", text='" + text + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
