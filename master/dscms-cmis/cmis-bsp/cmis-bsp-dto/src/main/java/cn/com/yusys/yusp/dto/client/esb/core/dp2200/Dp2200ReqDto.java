package cn.com.yusys.yusp.dto.client.esb.core.dp2200;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：保证金账户部提
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2200ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh;//子账户序号
    @JsonProperty(value = "kehuzhlx")
    private String kehuzhlx;//客户账号类型
    @JsonProperty(value = "zhhuzwmc")
    private String zhhuzwmc;//账户名称
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//货币代号
    @JsonProperty(value = "chaohubz")
    private String chaohubz;//账户钞汇标志
    @JsonProperty(value = "chapbhao")
    private String chapbhao;//产品编号
    @JsonProperty(value = "zhanghao")
    private String zhanghao;//负债账号
    @JsonProperty(value = "tonzbhao")
    private String tonzbhao;//通知编号
    @JsonProperty(value = "zhdaoqir")
    private String zhdaoqir;//账户到期日
    @JsonProperty(value = "jiaoyije")
    private BigDecimal jiaoyije;//交易金额
    @JsonProperty(value = "pngzzlei")
    private String pngzzlei;//凭证种类
    @JsonProperty(value = "pngzphao")
    private String pngzphao;//凭证批号
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao;//凭证序号
    @JsonProperty(value = "xpizleix")
    private String xpizleix;//新凭证种类
    @JsonProperty(value = "xpnzphao")
    private String xpnzphao;//新凭证批号
    @JsonProperty(value = "xpnzxhao")
    private String xpnzxhao;//新凭证序号
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn;//支付条件
    @JsonProperty(value = "jiaoymma")
    private String jiaoymma;//交易密码
    @JsonProperty(value = "zijinqux")
    private String zijinqux;//资金去向
    @JsonProperty(value = "skrkhuzh")
    private String skrkhuzh;//收款人客户账号
    @JsonProperty(value = "skrzhalx")
    private String skrzhalx;//收款人账号类型
    @JsonProperty(value = "skrzhamc")
    private String skrzhamc;//收款人账户名称
    @JsonProperty(value = "skzhxuho")
    private String skzhxuho;//收款人子账户序号
    @JsonProperty(value = "skhobidh")
    private String skhobidh;//收款人币种
    @JsonProperty(value = "skchhubz")
    private String skchhubz;//收款人钞汇
    @JsonProperty(value = "sfsfbzhi")
    private String sfsfbzhi;//是否收费标志
    @JsonProperty(value = "zhaiyodm")
    private String zhaiyodm;//摘要代码
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要描述
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注
    @JsonProperty(value = "xianzzbz")
    private String xianzzbz;//现转标志
    @JsonProperty(value = "mimazlei")
    private String mimazlei;//密码种类
    @JsonProperty(value = "dfzhhxuh")
    private String dfzhhxuh;//对方子账户序号
    @JsonProperty(value = "duifjgdm")
    private String duifjgdm;//对方金融机构代码
    @JsonProperty(value = "duifjgmc")
    private String duifjgmc;//对方金融机构名称
    @JsonProperty(value = "daokjine")
    private BigDecimal daokjine;//倒扣金额
    @JsonProperty(value = "lxzrkhzh")
    private String lxzrkhzh;//利息转入客户账号
    @JsonProperty(value = "zrzzhhxh")
    private String zrzzhhxh;//转入子账户序号
    @JsonProperty(value = "lixizjqx")
    private String lixizjqx;//利息资金去向


    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getTonzbhao() {
        return tonzbhao;
    }

    public void setTonzbhao(String tonzbhao) {
        this.tonzbhao = tonzbhao;
    }

    public String getZhdaoqir() {
        return zhdaoqir;
    }

    public void setZhdaoqir(String zhdaoqir) {
        this.zhdaoqir = zhdaoqir;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPngzphao() {
        return pngzphao;
    }

    public void setPngzphao(String pngzphao) {
        this.pngzphao = pngzphao;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getXpizleix() {
        return xpizleix;
    }

    public void setXpizleix(String xpizleix) {
        this.xpizleix = xpizleix;
    }

    public String getXpnzphao() {
        return xpnzphao;
    }

    public void setXpnzphao(String xpnzphao) {
        this.xpnzphao = xpnzphao;
    }

    public String getXpnzxhao() {
        return xpnzxhao;
    }

    public void setXpnzxhao(String xpnzxhao) {
        this.xpnzxhao = xpnzxhao;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getJiaoymma() {
        return jiaoymma;
    }

    public void setJiaoymma(String jiaoymma) {
        this.jiaoymma = jiaoymma;
    }

    public String getZijinqux() {
        return zijinqux;
    }

    public void setZijinqux(String zijinqux) {
        this.zijinqux = zijinqux;
    }

    public String getSkrkhuzh() {
        return skrkhuzh;
    }

    public void setSkrkhuzh(String skrkhuzh) {
        this.skrkhuzh = skrkhuzh;
    }

    public String getSkrzhalx() {
        return skrzhalx;
    }

    public void setSkrzhalx(String skrzhalx) {
        this.skrzhalx = skrzhalx;
    }

    public String getSkrzhamc() {
        return skrzhamc;
    }

    public void setSkrzhamc(String skrzhamc) {
        this.skrzhamc = skrzhamc;
    }

    public String getSkzhxuho() {
        return skzhxuho;
    }

    public void setSkzhxuho(String skzhxuho) {
        this.skzhxuho = skzhxuho;
    }

    public String getSkhobidh() {
        return skhobidh;
    }

    public void setSkhobidh(String skhobidh) {
        this.skhobidh = skhobidh;
    }

    public String getSkchhubz() {
        return skchhubz;
    }

    public void setSkchhubz(String skchhubz) {
        this.skchhubz = skchhubz;
    }

    public String getSfsfbzhi() {
        return sfsfbzhi;
    }

    public void setSfsfbzhi(String sfsfbzhi) {
        this.sfsfbzhi = sfsfbzhi;
    }

    public String getZhaiyodm() {
        return zhaiyodm;
    }

    public void setZhaiyodm(String zhaiyodm) {
        this.zhaiyodm = zhaiyodm;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getXianzzbz() {
        return xianzzbz;
    }

    public void setXianzzbz(String xianzzbz) {
        this.xianzzbz = xianzzbz;
    }

    public String getMimazlei() {
        return mimazlei;
    }

    public void setMimazlei(String mimazlei) {
        this.mimazlei = mimazlei;
    }

    public String getDfzhhxuh() {
        return dfzhhxuh;
    }

    public void setDfzhhxuh(String dfzhhxuh) {
        this.dfzhhxuh = dfzhhxuh;
    }

    public String getDuifjgdm() {
        return duifjgdm;
    }

    public void setDuifjgdm(String duifjgdm) {
        this.duifjgdm = duifjgdm;
    }

    public String getDuifjgmc() {
        return duifjgmc;
    }

    public void setDuifjgmc(String duifjgmc) {
        this.duifjgmc = duifjgmc;
    }

    public BigDecimal getDaokjine() {
        return daokjine;
    }

    public void setDaokjine(BigDecimal daokjine) {
        this.daokjine = daokjine;
    }

    public String getLxzrkhzh() {
        return lxzrkhzh;
    }

    public void setLxzrkhzh(String lxzrkhzh) {
        this.lxzrkhzh = lxzrkhzh;
    }

    public String getZrzzhhxh() {
        return zrzzhhxh;
    }

    public void setZrzzhhxh(String zrzzhhxh) {
        this.zrzzhhxh = zrzzhhxh;
    }

    public String getLixizjqx() {
        return lixizjqx;
    }

    public void setLixizjqx(String lixizjqx) {
        this.lixizjqx = lixizjqx;
    }

    @Override
    public String toString() {
        return "Dp2200ReqDto{" +
                "kehuzhao='" + kehuzhao + '\'' +
                "zhhaoxuh='" + zhhaoxuh + '\'' +
                "kehuzhlx='" + kehuzhlx + '\'' +
                "zhhuzwmc='" + zhhuzwmc + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                "chaohubz='" + chaohubz + '\'' +
                "chapbhao='" + chapbhao + '\'' +
                "zhanghao='" + zhanghao + '\'' +
                "tonzbhao='" + tonzbhao + '\'' +
                "zhdaoqir='" + zhdaoqir + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "pngzzlei='" + pngzzlei + '\'' +
                "pngzphao='" + pngzphao + '\'' +
                "pngzxhao='" + pngzxhao + '\'' +
                "xpizleix='" + xpizleix + '\'' +
                "xpnzphao='" + xpnzphao + '\'' +
                "xpnzxhao='" + xpnzxhao + '\'' +
                "zhfutojn='" + zhfutojn + '\'' +
                "jiaoymma='" + jiaoymma + '\'' +
                "zijinqux='" + zijinqux + '\'' +
                "skrkhuzh='" + skrkhuzh + '\'' +
                "skrzhalx='" + skrzhalx + '\'' +
                "skrzhamc='" + skrzhamc + '\'' +
                "skzhxuho='" + skzhxuho + '\'' +
                "skhobidh='" + skhobidh + '\'' +
                "skchhubz='" + skchhubz + '\'' +
                "sfsfbzhi='" + sfsfbzhi + '\'' +
                "zhaiyodm='" + zhaiyodm + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "xianzzbz='" + xianzzbz + '\'' +
                "mimazlei='" + mimazlei + '\'' +
                "dfzhhxuh='" + dfzhhxuh + '\'' +
                "duifjgdm='" + duifjgdm + '\'' +
                "duifjgmc='" + duifjgmc + '\'' +
                "daokjine='" + daokjine + '\'' +
                "lxzrkhzh='" + lxzrkhzh + '\'' +
                "zrzzhhxh='" + zrzzhhxh + '\'' +
                "lixizjqx='" + lixizjqx + '\'' +
                '}';
    }
}  
