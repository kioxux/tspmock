package cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷授信协议信息同步
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LmtinfRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
}  
