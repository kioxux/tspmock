package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：线下查询接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CredxxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "creditType")
    private String creditType;//报告类型
    @JsonProperty(value = "customName")
    private String customName;//客户名称
    @JsonProperty(value = "certificateType")
    private String certificateType;//证件类型
    @JsonProperty(value = "certificateNum")
    private String certificateNum;//证件号
    @JsonProperty(value = "queryReason")
    private String queryReason;//查询原因
    @JsonProperty(value = "reportType")
    private String reportType;//信用报告返回格式
    @JsonProperty(value = "queryType")
    private String queryType;//信用报告复用策略
    @JsonProperty(value = "businessLine")
    private String businessLine;//产品业务线
    @JsonProperty(value = "sysCode")
    private String sysCode;//系统标识
    @JsonProperty(value = "creditDocId")
    private String creditDocId;//影像编号
    @JsonProperty(value = "archiveCreateDate")
    private String archiveCreateDate;//授权书签订日期 时间格式：2016-01-04
    @JsonProperty(value = "archiveExpireDate")
    private String archiveExpireDate;//授权书到期日期
    @JsonProperty(value = "auditReason")
    private String auditReason;//授权条款
    @JsonProperty(value = "borrowPersonRelation")
    private String borrowPersonRelation;//与主借款人关系
    @JsonProperty(value = "borrowPersonRelationName")
    private String borrowPersonRelationName;//主借款人名称
    @JsonProperty(value = "borrowPersonRelationNumber")
    private String borrowPersonRelationNumber;//主借款人证件号
    @JsonProperty(value = "createUserId")
    private String createUserId;//客户经理工号
    @JsonProperty(value = "createUserName")
    private String createUserName;//客户经理名称
    @JsonProperty(value = "createUserIdCard")
    private String createUserIdCard;//客户经理身份证号
    @JsonProperty(value = "approvalName")
    private String approvalName;//审批人姓名
    @JsonProperty(value = "approvalIdCard")
    private String approvalIdCard;//审批人身份证号
    @JsonProperty(value = "brchno")
    private String brchno;//部门号


    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getCertificateNum() {
        return certificateNum;
    }

    public void setCertificateNum(String certificateNum) {
        this.certificateNum = certificateNum;
    }

    public String getQueryReason() {
        return queryReason;
    }

    public void setQueryReason(String queryReason) {
        this.queryReason = queryReason;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getBusinessLine() {
        return businessLine;
    }

    public void setBusinessLine(String businessLine) {
        this.businessLine = businessLine;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getCreditDocId() {
        return creditDocId;
    }

    public void setCreditDocId(String creditDocId) {
        this.creditDocId = creditDocId;
    }

    public String getArchiveCreateDate() {
        return archiveCreateDate;
    }

    public void setArchiveCreateDate(String archiveCreateDate) {
        this.archiveCreateDate = archiveCreateDate;
    }

    public String getArchiveExpireDate() {
        return archiveExpireDate;
    }

    public void setArchiveExpireDate(String archiveExpireDate) {
        this.archiveExpireDate = archiveExpireDate;
    }

    public String getAuditReason() {
        return auditReason;
    }

    public void setAuditReason(String auditReason) {
        this.auditReason = auditReason;
    }

    public String getBorrowPersonRelation() {
        return borrowPersonRelation;
    }

    public void setBorrowPersonRelation(String borrowPersonRelation) {
        this.borrowPersonRelation = borrowPersonRelation;
    }

    public String getBorrowPersonRelationName() {
        return borrowPersonRelationName;
    }

    public void setBorrowPersonRelationName(String borrowPersonRelationName) {
        this.borrowPersonRelationName = borrowPersonRelationName;
    }

    public String getBorrowPersonRelationNumber() {
        return borrowPersonRelationNumber;
    }

    public void setBorrowPersonRelationNumber(String borrowPersonRelationNumber) {
        this.borrowPersonRelationNumber = borrowPersonRelationNumber;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCreateUserIdCard() {
        return createUserIdCard;
    }

    public void setCreateUserIdCard(String createUserIdCard) {
        this.createUserIdCard = createUserIdCard;
    }

    public String getApprovalName() {
        return approvalName;
    }

    public void setApprovalName(String approvalName) {
        this.approvalName = approvalName;
    }

    public String getApprovalIdCard() {
        return approvalIdCard;
    }

    public void setApprovalIdCard(String approvalIdCard) {
        this.approvalIdCard = approvalIdCard;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    @Override
    public String toString() {
        return "CredxxReqDto{" +
                "creditType='" + creditType + '\'' +
                "customName='" + customName + '\'' +
                "certificateType='" + certificateType + '\'' +
                "certificateNum='" + certificateNum + '\'' +
                "queryReason='" + queryReason + '\'' +
                "reportType='" + reportType + '\'' +
                "queryType='" + queryType + '\'' +
                "businessLine='" + businessLine + '\'' +
                "sysCode='" + sysCode + '\'' +
                "creditDocId='" + creditDocId + '\'' +
                "archiveCreateDate='" + archiveCreateDate + '\'' +
                "archiveExpireDate='" + archiveExpireDate + '\'' +
                "auditReason='" + auditReason + '\'' +
                "borrowPersonRelation='" + borrowPersonRelation + '\'' +
                "borrowPersonRelationName='" + borrowPersonRelationName + '\'' +
                "borrowPersonRelationNumber='" + borrowPersonRelationNumber + '\'' +
                "createUserId='" + createUserId + '\'' +
                "createUserName='" + createUserName + '\'' +
                "createUserIdCard='" + createUserIdCard + '\'' +
                "approvalName='" + approvalName + '\'' +
                "approvalIdCard='" + approvalIdCard + '\'' +
                "brchno='" + brchno + '\'' +
                '}';
    }
}
