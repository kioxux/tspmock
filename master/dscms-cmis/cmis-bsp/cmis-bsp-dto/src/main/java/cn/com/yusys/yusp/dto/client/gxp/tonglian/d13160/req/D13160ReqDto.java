package cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：大额现金分期申请
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D13160ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cardno")
    private String cardno;//卡号
    @JsonProperty(value = "crcycd")
    private String crcycd;//币种
    @JsonProperty(value = "loannt")
    private BigDecimal loannt;//分期金额
    @JsonProperty(value = "loanterm")
    private String loanterm;//分期期数
    @JsonProperty(value = "lnfemd")
    private String lnfemd;//分期手续费收取方式
    @JsonProperty(value = "loanmethod")
    private String loanmethod;//分期本金分配方式
    @JsonProperty(value = "loanind")
    private String loanind;//分期自定义分期手续费率标识
    @JsonProperty(value = "lothod")
    private String lothod;//分期手续费计算方式
    @JsonProperty(value = "loanrate")
    private String loanrate;//分期手续费比例
    @JsonProperty(value = "loanamt")
    private String loanamt;//分期手续费固定金额
    @JsonProperty(value = "optcod")
    private String optcod;//操作类型
    @JsonProperty(value = "sendmode")
    private String sendmode;//分期放款方式
    @JsonProperty(value = "dbnknm")
    private String dbnknm;//分期放款银行名称
    @JsonProperty(value = "dbkbch")
    private String dbkbch;//分期放款开户行号
    @JsonProperty(value = "dbkact")
    private String dbkact;//分期放款账号
    @JsonProperty(value = "bkctnm")
    private String bkctnm;//分期放款账户姓名
    @JsonProperty(value = "seflag")
    private String seflag;//非本人账户放款标识
    @JsonProperty(value = "papose")
    private String papose;//资金用途
    @JsonProperty(value = "loanet")
    private String loanet;//分期放款账户对公/对私标识
    @JsonProperty(value = "salman")
    private String salman;//分期营销人员姓名
    @JsonProperty(value = "saleno")
    private String saleno;//分期营销人员编号
    @JsonProperty(value = "salech")
    private String salech;//分期营销人员所属分行
    @JsonProperty(value = "eappno")
    private String eappno;//外部申请编号
    @JsonProperty(value = "locode")
    private String locode;//分期计划代码

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public BigDecimal getLoannt() {
        return loannt;
    }

    public void setLoannt(BigDecimal loannt) {
        this.loannt = loannt;
    }

    public String getLoanterm() {
        return loanterm;
    }

    public void setLoanterm(String loanterm) {
        this.loanterm = loanterm;
    }

    public String getLnfemd() {
        return lnfemd;
    }

    public void setLnfemd(String lnfemd) {
        this.lnfemd = lnfemd;
    }

    public String getLoanmethod() {
        return loanmethod;
    }

    public void setLoanmethod(String loanmethod) {
        this.loanmethod = loanmethod;
    }

    public String getLoanind() {
        return loanind;
    }

    public void setLoanind(String loanind) {
        this.loanind = loanind;
    }

    public String getLothod() {
        return lothod;
    }

    public void setLothod(String lothod) {
        this.lothod = lothod;
    }

    public String getLoanrate() {
        return loanrate;
    }

    public void setLoanrate(String loanrate) {
        this.loanrate = loanrate;
    }

    public String getLoanamt() {
        return loanamt;
    }

    public void setLoanamt(String loanamt) {
        this.loanamt = loanamt;
    }

    public String getOptcod() {
        return optcod;
    }

    public void setOptcod(String optcod) {
        this.optcod = optcod;
    }

    public String getSendmode() {
        return sendmode;
    }

    public void setSendmode(String sendmode) {
        this.sendmode = sendmode;
    }

    public String getDbnknm() {
        return dbnknm;
    }

    public void setDbnknm(String dbnknm) {
        this.dbnknm = dbnknm;
    }

    public String getDbkbch() {
        return dbkbch;
    }

    public void setDbkbch(String dbkbch) {
        this.dbkbch = dbkbch;
    }

    public String getDbkact() {
        return dbkact;
    }

    public void setDbkact(String dbkact) {
        this.dbkact = dbkact;
    }

    public String getBkctnm() {
        return bkctnm;
    }

    public void setBkctnm(String bkctnm) {
        this.bkctnm = bkctnm;
    }

    public String getSeflag() {
        return seflag;
    }

    public void setSeflag(String seflag) {
        this.seflag = seflag;
    }

    public String getPapose() {
        return papose;
    }

    public void setPapose(String papose) {
        this.papose = papose;
    }

    public String getLoanet() {
        return loanet;
    }

    public void setLoanet(String loanet) {
        this.loanet = loanet;
    }

    public String getSalman() {
        return salman;
    }

    public void setSalman(String salman) {
        this.salman = salman;
    }

    public String getSaleno() {
        return saleno;
    }

    public void setSaleno(String saleno) {
        this.saleno = saleno;
    }

    public String getSalech() {
        return salech;
    }

    public void setSalech(String salech) {
        this.salech = salech;
    }

    public String getEappno() {
        return eappno;
    }

    public void setEappno(String eappno) {
        this.eappno = eappno;
    }

    public String getLocode() {
        return locode;
    }

    public void setLocode(String locode) {
        this.locode = locode;
    }

    @Override
    public String toString() {
        return "D13160ReqDto{" +
                "cardno='" + cardno + '\'' +
                "crcycd='" + crcycd + '\'' +
                "loannt='" + loannt + '\'' +
                "loanterm='" + loanterm + '\'' +
                "lnfemd='" + lnfemd + '\'' +
                "loanmethod='" + loanmethod + '\'' +
                "loanind='" + loanind + '\'' +
                "lothod='" + lothod + '\'' +
                "loanrate='" + loanrate + '\'' +
                "loanamt='" + loanamt + '\'' +
                "optcod='" + optcod + '\'' +
                "sendmode='" + sendmode + '\'' +
                "dbnknm='" + dbnknm + '\'' +
                "dbkbch='" + dbkbch + '\'' +
                "dbkact='" + dbkact + '\'' +
                "bkctnm='" + bkctnm + '\'' +
                "seflag='" + seflag + '\'' +
                "papose='" + papose + '\'' +
                "loanet='" + loanet + '\'' +
                "salman='" + salman + '\'' +
                "saleno='" + saleno + '\'' +
                "salech='" + salech + '\'' +
                "eappno='" + eappno + '\'' +
                "locode='" + locode + '\'' +
                '}';
    }
}  
