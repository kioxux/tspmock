package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 司法协助变更信息
 */
@JsonPropertyOrder(alphabetic = true)
public class JUDICIALAIDALTER implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "AEDATE")
    private String AEDATE;//	协助执行日期
    @JsonProperty(value = "ASSIGNEE")
    private String ASSIGNEE;//	受让人
    @JsonProperty(value = "ASSIGNEE_ITYPE")
    private String ASSIGNEE_ITYPE;//	受让人证件类型
    @JsonProperty(value = "ASSIGNEE_LICENCE")
    private String ASSIGNEE_LICENCE;//	受让人证照号码
    @JsonProperty(value = "ASSIGNEE_LTYPE")
    private String ASSIGNEE_LTYPE;//	受让人证照类型
    @JsonProperty(value = "ASSIGNEE_TYPE")
    private String ASSIGNEE_TYPE;//	受让人类型
    @JsonProperty(value = "COURTNAME")
    private String COURTNAME;//	执行法院
    @JsonProperty(value = "CUR")
    private String CUR;//	币种
    @JsonProperty(value = "EXECUTION")
    private String EXECUTION;//	执行事项
    @JsonProperty(value = "EX_NOTICE_NO")
    private String EX_NOTICE_NO;//	协助执行通知书文号
    @JsonProperty(value = "INAME")
    private String INAME;//	被执行人
    @JsonProperty(value = "INAME_ITYPE")
    private String INAME_ITYPE;//	被执行人证件类型
    @JsonProperty(value = "INAME_LICENCE")
    private String INAME_LICENCE;//	被执行人证照号码
    @JsonProperty(value = "INAME_LTYPE")
    private String INAME_LTYPE;//	被执行人证照类型
    @JsonProperty(value = "INAME_TYPE")
    private String INAME_TYPE;//	被执行人类型
    @JsonProperty(value = "JUDGMENT_NO")
    private String JUDGMENT_NO;//	O 执行裁定书文号
    @JsonProperty(value = "MODIFYID")
    private String MODIFYID;//	股权变更ID
    @JsonProperty(value = "SHAREAM")
    private String SHAREAM;//	股权数额

    @JsonIgnore
    public String getAEDATE() {
        return AEDATE;
    }

    @JsonIgnore
    public void setAEDATE(String AEDATE) {
        this.AEDATE = AEDATE;
    }

    @JsonIgnore
    public String getASSIGNEE() {
        return ASSIGNEE;
    }

    @JsonIgnore
    public void setASSIGNEE(String ASSIGNEE) {
        this.ASSIGNEE = ASSIGNEE;
    }

    @JsonIgnore
    public String getASSIGNEE_ITYPE() {
        return ASSIGNEE_ITYPE;
    }

    @JsonIgnore
    public void setASSIGNEE_ITYPE(String ASSIGNEE_ITYPE) {
        this.ASSIGNEE_ITYPE = ASSIGNEE_ITYPE;
    }

    @JsonIgnore
    public String getASSIGNEE_LICENCE() {
        return ASSIGNEE_LICENCE;
    }

    @JsonIgnore
    public void setASSIGNEE_LICENCE(String ASSIGNEE_LICENCE) {
        this.ASSIGNEE_LICENCE = ASSIGNEE_LICENCE;
    }

    @JsonIgnore
    public String getASSIGNEE_LTYPE() {
        return ASSIGNEE_LTYPE;
    }

    @JsonIgnore
    public void setASSIGNEE_LTYPE(String ASSIGNEE_LTYPE) {
        this.ASSIGNEE_LTYPE = ASSIGNEE_LTYPE;
    }

    @JsonIgnore
    public String getASSIGNEE_TYPE() {
        return ASSIGNEE_TYPE;
    }

    @JsonIgnore
    public void setASSIGNEE_TYPE(String ASSIGNEE_TYPE) {
        this.ASSIGNEE_TYPE = ASSIGNEE_TYPE;
    }

    @JsonIgnore
    public String getCOURTNAME() {
        return COURTNAME;
    }

    @JsonIgnore
    public void setCOURTNAME(String COURTNAME) {
        this.COURTNAME = COURTNAME;
    }

    @JsonIgnore
    public String getCUR() {
        return CUR;
    }

    @JsonIgnore
    public void setCUR(String CUR) {
        this.CUR = CUR;
    }

    @JsonIgnore
    public String getEXECUTION() {
        return EXECUTION;
    }

    @JsonIgnore
    public void setEXECUTION(String EXECUTION) {
        this.EXECUTION = EXECUTION;
    }

    @JsonIgnore
    public String getEX_NOTICE_NO() {
        return EX_NOTICE_NO;
    }

    @JsonIgnore
    public void setEX_NOTICE_NO(String EX_NOTICE_NO) {
        this.EX_NOTICE_NO = EX_NOTICE_NO;
    }

    @JsonIgnore
    public String getINAME() {
        return INAME;
    }

    @JsonIgnore
    public void setINAME(String INAME) {
        this.INAME = INAME;
    }

    @JsonIgnore
    public String getINAME_ITYPE() {
        return INAME_ITYPE;
    }

    @JsonIgnore
    public void setINAME_ITYPE(String INAME_ITYPE) {
        this.INAME_ITYPE = INAME_ITYPE;
    }

    @JsonIgnore
    public String getINAME_LICENCE() {
        return INAME_LICENCE;
    }

    @JsonIgnore
    public void setINAME_LICENCE(String INAME_LICENCE) {
        this.INAME_LICENCE = INAME_LICENCE;
    }

    @JsonIgnore
    public String getINAME_LTYPE() {
        return INAME_LTYPE;
    }

    @JsonIgnore
    public void setINAME_LTYPE(String INAME_LTYPE) {
        this.INAME_LTYPE = INAME_LTYPE;
    }

    @JsonIgnore
    public String getINAME_TYPE() {
        return INAME_TYPE;
    }

    @JsonIgnore
    public void setINAME_TYPE(String INAME_TYPE) {
        this.INAME_TYPE = INAME_TYPE;
    }

    @JsonIgnore
    public String getJUDGMENT_NO() {
        return JUDGMENT_NO;
    }

    @JsonIgnore
    public void setJUDGMENT_NO(String JUDGMENT_NO) {
        this.JUDGMENT_NO = JUDGMENT_NO;
    }

    @JsonIgnore
    public String getMODIFYID() {
        return MODIFYID;
    }

    @JsonIgnore
    public void setMODIFYID(String MODIFYID) {
        this.MODIFYID = MODIFYID;
    }

    @JsonIgnore
    public String getSHAREAM() {
        return SHAREAM;
    }

    @JsonIgnore
    public void setSHAREAM(String SHAREAM) {
        this.SHAREAM = SHAREAM;
    }

    @Override
    public String toString() {
        return "JUDICIALAIDALTER{" +
                "AEDATE='" + AEDATE + '\'' +
                ", ASSIGNEE='" + ASSIGNEE + '\'' +
                ", ASSIGNEE_ITYPE='" + ASSIGNEE_ITYPE + '\'' +
                ", ASSIGNEE_LICENCE='" + ASSIGNEE_LICENCE + '\'' +
                ", ASSIGNEE_LTYPE='" + ASSIGNEE_LTYPE + '\'' +
                ", ASSIGNEE_TYPE='" + ASSIGNEE_TYPE + '\'' +
                ", COURTNAME='" + COURTNAME + '\'' +
                ", CUR='" + CUR + '\'' +
                ", EXECUTION='" + EXECUTION + '\'' +
                ", EX_NOTICE_NO='" + EX_NOTICE_NO + '\'' +
                ", INAME='" + INAME + '\'' +
                ", INAME_ITYPE='" + INAME_ITYPE + '\'' +
                ", INAME_LICENCE='" + INAME_LICENCE + '\'' +
                ", INAME_LTYPE='" + INAME_LTYPE + '\'' +
                ", INAME_TYPE='" + INAME_TYPE + '\'' +
                ", JUDGMENT_NO='" + JUDGMENT_NO + '\'' +
                ", MODIFYID='" + MODIFYID + '\'' +
                ", SHAREAM='" + SHAREAM + '\'' +
                '}';
    }
}
