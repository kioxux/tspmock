package cn.com.yusys.yusp.dto.server.biz.xdht0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：合同详情查看
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certid")
    private String certid;//证件号
    @JsonProperty(value = "biz_type")
    private String biz_type;//贷款类型
    @JsonProperty(value = "cont_state")
    private String cont_state;//合同状态

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    @Override
    public String toString() {
        return "Data{" +
                "certid='" + certid + '\'' +
                ", biz_type='" + biz_type + '\'' +
                ", cont_state='" + cont_state + '\'' +
                '}';
    }
}