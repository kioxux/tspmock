package cn.com.yusys.yusp.dto.client.esb.circp.fb1167.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：企业征信查询通知
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1167ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "zx_serno")
    private String zx_serno;//征信流水号
    @JsonProperty(value = "zx_time")
    private String zx_time;//征信查询时间
    @JsonProperty(value = "zx_cus_id")
    private String zx_cus_id;//征信查询对象号
    @JsonProperty(value = "app_no")
    private String app_no;//申请流水号
    @JsonProperty(value = "ent_name")
    private String ent_name;//企业名称
    @JsonProperty(value = "ent_buz_lic_no")
    private String ent_buz_lic_no;//营业执照号
    @JsonProperty(value = "loancard_code")
    private String loancard_code;//中征码

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getZx_serno() {
        return zx_serno;
    }

    @JsonIgnore
    public void setZx_serno(String zx_serno) {
        this.zx_serno = zx_serno;
    }

    public String getZx_time() {
        return zx_time;
    }

    public void setZx_time(String zx_time) {
        this.zx_time = zx_time;
    }

    public String getZx_cus_id() {
        return zx_cus_id;
    }

    public void setZx_cus_id(String zx_cus_id) {
        this.zx_cus_id = zx_cus_id;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getEnt_name() {
        return ent_name;
    }

    public void setEnt_name(String ent_name) {
        this.ent_name = ent_name;
    }

    public String getEnt_buz_lic_no() {
        return ent_buz_lic_no;
    }

    public void setEnt_buz_lic_no(String ent_buz_lic_no) {
        this.ent_buz_lic_no = ent_buz_lic_no;
    }

    public String getLoancard_code() {
        return loancard_code;
    }

    public void setLoancard_code(String loancard_code) {
        this.loancard_code = loancard_code;
    }

    @Override
    public String toString() {
        return "Fb1167ReqDto{" +
                "CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", zx_serno='" + zx_serno + '\'' +
                ", zx_time='" + zx_time + '\'' +
                ", zx_cus_id='" + zx_cus_id + '\'' +
                ", app_no='" + app_no + '\'' +
                ", ent_name='" + ent_name + '\'' +
                ", ent_buz_lic_no='" + ent_buz_lic_no + '\'' +
                ", loancard_code='" + loancard_code + '\'' +
                '}';
    }
}
