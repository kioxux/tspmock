package cn.com.yusys.yusp.dto.client.esb.core.da3321.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：抵债资产模糊查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3321RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.da3321.resp.Listnm1> listnm1;

    public List<Listnm1> getListnm1() {
        return listnm1;
    }

    public void setListnm1(List<Listnm1> listnm1) {
        this.listnm1 = listnm1;
    }

    @Override
    public String toString() {
        return "Da3321RespDto{" +
                "listnm1=" + listnm1 +
                '}';
    }
}
