package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

//小微优企贷风险提示
@JsonPropertyOrder(alphabetic = true)
public class R007 implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "OVERDUEACCOUNTNUM")
    private BigDecimal OVERDUEACCOUNTNUM;// 当前逾期账户数
    @JsonProperty(value = "OVERDUEM3ACCOUNTNUM")
    private BigDecimal OVERDUEM3ACCOUNTNUM;// 逾期M3及以上账户数
    @JsonProperty(value = "GUARANTEEACCOUNTNUM")
    private BigDecimal GUARANTEEACCOUNTNUM;// 对外担保账户数
    @JsonProperty(value = "CREDITCARD")
    private BigDecimal CREDITCARD;// 近半年信用卡审批次数
    @JsonProperty(value = "LOAN")
    private BigDecimal LOAN;// 近半年贷款审批次数
    @JsonProperty(value = "BEFOREGUARANTEE")
    private BigDecimal BEFOREGUARANTEE;// 近半年保前审查次数
    @JsonProperty(value = "GUARANTEE")
    private BigDecimal GUARANTEE;// 近半年担保资格审查次数
    @JsonProperty(value = "ABNORMALACCOUNT")
    private BigDecimal ABNORMALACCOUNT;// 五级分类非正常贷款或信用卡账户数

    @JsonIgnore
    public BigDecimal getOVERDUEACCOUNTNUM() {
        return OVERDUEACCOUNTNUM;
    }

    @JsonIgnore
    public void setOVERDUEACCOUNTNUM(BigDecimal OVERDUEACCOUNTNUM) {
        this.OVERDUEACCOUNTNUM = OVERDUEACCOUNTNUM;
    }

    @JsonIgnore
    public BigDecimal getOVERDUEM3ACCOUNTNUM() {
        return OVERDUEM3ACCOUNTNUM;
    }

    @JsonIgnore
    public void setOVERDUEM3ACCOUNTNUM(BigDecimal OVERDUEM3ACCOUNTNUM) {
        this.OVERDUEM3ACCOUNTNUM = OVERDUEM3ACCOUNTNUM;
    }

    @JsonIgnore
    public BigDecimal getGUARANTEEACCOUNTNUM() {
        return GUARANTEEACCOUNTNUM;
    }

    @JsonIgnore
    public void setGUARANTEEACCOUNTNUM(BigDecimal GUARANTEEACCOUNTNUM) {
        this.GUARANTEEACCOUNTNUM = GUARANTEEACCOUNTNUM;
    }

    @JsonIgnore
    public BigDecimal getCREDITCARD() {
        return CREDITCARD;
    }

    @JsonIgnore
    public void setCREDITCARD(BigDecimal CREDITCARD) {
        this.CREDITCARD = CREDITCARD;
    }

    @JsonIgnore
    public BigDecimal getLOAN() {
        return LOAN;
    }

    @JsonIgnore
    public void setLOAN(BigDecimal LOAN) {
        this.LOAN = LOAN;
    }

    @JsonIgnore
    public BigDecimal getBEFOREGUARANTEE() {
        return BEFOREGUARANTEE;
    }

    @JsonIgnore
    public void setBEFOREGUARANTEE(BigDecimal BEFOREGUARANTEE) {
        this.BEFOREGUARANTEE = BEFOREGUARANTEE;
    }

    @JsonIgnore
    public BigDecimal getGUARANTEE() {
        return GUARANTEE;
    }

    @JsonIgnore
    public void setGUARANTEE(BigDecimal GUARANTEE) {
        this.GUARANTEE = GUARANTEE;
    }

    @JsonIgnore
    public BigDecimal getABNORMALACCOUNT() {
        return ABNORMALACCOUNT;
    }

    @JsonIgnore
    public void setABNORMALACCOUNT(BigDecimal ABNORMALACCOUNT) {
        this.ABNORMALACCOUNT = ABNORMALACCOUNT;
    }

    @Override
    public String toString() {
        return "R007{" +
                "OVERDUEACCOUNTNUM=" + OVERDUEACCOUNTNUM +
                ", OVERDUEM3ACCOUNTNUM=" + OVERDUEM3ACCOUNTNUM +
                ", GUARANTEEACCOUNTNUM=" + GUARANTEEACCOUNTNUM +
                ", CREDITCARD=" + CREDITCARD +
                ", LOAN=" + LOAN +
                ", BEFOREGUARANTEE=" + BEFOREGUARANTEE +
                ", GUARANTEE=" + GUARANTEE +
                ", ABNORMALACCOUNT=" + ABNORMALACCOUNT +
                '}';
    }
}
