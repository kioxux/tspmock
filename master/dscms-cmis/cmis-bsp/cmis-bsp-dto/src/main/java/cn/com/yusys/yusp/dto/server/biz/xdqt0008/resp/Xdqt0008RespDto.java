package cn.com.yusys.yusp.dto.server.biz.xdqt0008.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：乐悠金卡关联人查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdqt0008RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private cn.com.yusys.yusp.dto.server.biz.xdqt0008.resp.Data data;//data

	public cn.com.yusys.yusp.dto.server.biz.xdqt0008.resp.Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdqt0008RespDto{" +
				"data=" + data +
				'}';
	}
}  
