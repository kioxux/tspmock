package cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：失信被执行人查询接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SxbzxrRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "code")
    private String code;//返回码
    @JsonProperty(value = "get_type")
    private String get_type;//获取类型
    @JsonProperty(value = "msg")
    private String msg;//返回码说明
    @JsonProperty(value = "name")
    private String name;//未备注
    @JsonProperty(value = "message")
    private String message;//未备注
    @JsonProperty(value = "userid")
    private String userid;//未备注
    @JsonProperty(value = "data")
    private Data data;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGet_type() {
        return get_type;
    }

    public void setGet_type(String get_type) {
        this.get_type = get_type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SxbzxrRespDto{" +
                "code='" + code + '\'' +
                ", get_type='" + get_type + '\'' +
                ", msg='" + msg + '\'' +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                ", userid='" + userid + '\'' +
                ", data=" + data +
                '}';
    }
}
