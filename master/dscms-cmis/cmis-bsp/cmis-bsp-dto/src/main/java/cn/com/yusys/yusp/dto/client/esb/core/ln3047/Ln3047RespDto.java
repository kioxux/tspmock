package cn.com.yusys.yusp.dto.client.esb.core.ln3047;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：质押还贷
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3047RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "zdzyzhbz")
    private String zdzyzhbz;//指定质押账号标志
    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//质押账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//质押账号子序号
    @JsonProperty(value = "zhanghye")
    private BigDecimal zhanghye;//账户余额
    @JsonProperty(value = "zjzrzhao")
    private String zjzrzhao;//资金转入账号
    @JsonProperty(value = "zjzrzzxh")
    private String zjzrzzxh;//资金转入账号子序号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "ghysyjlx")
    private BigDecimal ghysyjlx;//归还应收应计利息
    @JsonProperty(value = "ghcsyjlx")
    private BigDecimal ghcsyjlx;//归还催收应计利息
    @JsonProperty(value = "ghynshqx")
    private BigDecimal ghynshqx;//归还应收欠息
    @JsonProperty(value = "ghcushqx")
    private BigDecimal ghcushqx;//归还催收欠息
    @JsonProperty(value = "ghysyjfx")
    private BigDecimal ghysyjfx;//归还应收应计罚息
    @JsonProperty(value = "ghcsyjfx")
    private BigDecimal ghcsyjfx;//归还催收应计罚息
    @JsonProperty(value = "ghynshfx")
    private BigDecimal ghynshfx;//归还应收罚息
    @JsonProperty(value = "ghcushfx")
    private BigDecimal ghcushfx;//归还催收罚息
    @JsonProperty(value = "ghyjfuxi")
    private BigDecimal ghyjfuxi;//归还应计复息
    @JsonProperty(value = "ghfxfuxi")
    private BigDecimal ghfxfuxi;//归还复息
    @JsonProperty(value = "lstZhzy")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3047.LstZhzy> lstZhzy;//质押列表[LIST]

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getZdzyzhbz() {
        return zdzyzhbz;
    }

    public void setZdzyzhbz(String zdzyzhbz) {
        this.zdzyzhbz = zdzyzhbz;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getGhysyjlx() {
        return ghysyjlx;
    }

    public void setGhysyjlx(BigDecimal ghysyjlx) {
        this.ghysyjlx = ghysyjlx;
    }

    public BigDecimal getGhcsyjlx() {
        return ghcsyjlx;
    }

    public void setGhcsyjlx(BigDecimal ghcsyjlx) {
        this.ghcsyjlx = ghcsyjlx;
    }

    public BigDecimal getGhynshqx() {
        return ghynshqx;
    }

    public void setGhynshqx(BigDecimal ghynshqx) {
        this.ghynshqx = ghynshqx;
    }

    public BigDecimal getGhcushqx() {
        return ghcushqx;
    }

    public void setGhcushqx(BigDecimal ghcushqx) {
        this.ghcushqx = ghcushqx;
    }

    public BigDecimal getGhysyjfx() {
        return ghysyjfx;
    }

    public void setGhysyjfx(BigDecimal ghysyjfx) {
        this.ghysyjfx = ghysyjfx;
    }

    public BigDecimal getGhcsyjfx() {
        return ghcsyjfx;
    }

    public void setGhcsyjfx(BigDecimal ghcsyjfx) {
        this.ghcsyjfx = ghcsyjfx;
    }

    public BigDecimal getGhynshfx() {
        return ghynshfx;
    }

    public void setGhynshfx(BigDecimal ghynshfx) {
        this.ghynshfx = ghynshfx;
    }

    public BigDecimal getGhcushfx() {
        return ghcushfx;
    }

    public void setGhcushfx(BigDecimal ghcushfx) {
        this.ghcushfx = ghcushfx;
    }

    public BigDecimal getGhyjfuxi() {
        return ghyjfuxi;
    }

    public void setGhyjfuxi(BigDecimal ghyjfuxi) {
        this.ghyjfuxi = ghyjfuxi;
    }

    public BigDecimal getGhfxfuxi() {
        return ghfxfuxi;
    }

    public void setGhfxfuxi(BigDecimal ghfxfuxi) {
        this.ghfxfuxi = ghfxfuxi;
    }

    public List<LstZhzy> getLstZhzy() {
        return lstZhzy;
    }

    public void setLstZhzy(List<LstZhzy> lstZhzy) {
        this.lstZhzy = lstZhzy;
    }

    @Override
    public String toString() {
        return "Ln3047RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", zdzyzhbz='" + zdzyzhbz + '\'' +
                ", huankjee=" + huankjee +
                ", huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", zhanghye=" + zhanghye +
                ", zjzrzhao='" + zjzrzhao + '\'' +
                ", zjzrzzxh='" + zjzrzzxh + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", zhchlilv=" + zhchlilv +
                ", yuqililv=" + yuqililv +
                ", dkrzhzhh='" + dkrzhzhh + '\'' +
                ", jiejuuje=" + jiejuuje +
                ", zhchbjin=" + zhchbjin +
                ", yuqibjin=" + yuqibjin +
                ", dzhibjin=" + dzhibjin +
                ", daizbjin=" + daizbjin +
                ", ghysyjlx=" + ghysyjlx +
                ", ghcsyjlx=" + ghcsyjlx +
                ", ghynshqx=" + ghynshqx +
                ", ghcushqx=" + ghcushqx +
                ", ghysyjfx=" + ghysyjfx +
                ", ghcsyjfx=" + ghcsyjfx +
                ", ghynshfx=" + ghynshfx +
                ", ghcushfx=" + ghcushfx +
                ", ghyjfuxi=" + ghyjfuxi +
                ", ghfxfuxi=" + ghfxfuxi +
                ", lstZhzy=" + lstZhzy +
                '}';
    }
}
