package cn.com.yusys.yusp.dto.server.biz.xdcz0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：企业网银查询影像补录批次
 *
 * @author xull
 * @version 1.0
 */
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "totalQnt")
    private String totalQnt;//总数
    @JsonProperty(value = "list")
    private java.util.List<java.util.List> list;

    public String getTotalQnt() {
        return totalQnt;
    }

    public void setTotalQnt(String totalQnt) {
        this.totalQnt = totalQnt;
    }

    public java.util.List<java.util.List> getList() {
        return list;
    }

    public void setList(List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "totalQnt='" + totalQnt + '\'' +
                ", list=" + list +
                '}';
    }
}
