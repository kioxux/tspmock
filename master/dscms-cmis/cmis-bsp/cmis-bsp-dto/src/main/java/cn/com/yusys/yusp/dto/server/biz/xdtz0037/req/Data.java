package cn.com.yusys.yusp.dto.server.biz.xdtz0037.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：无还本续贷额度状态更新
 * @Author zhangpeng
 * @Date 2021/4/27 13:56
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "surveySerno")
    private String surveySerno;//客户调查主键

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "surveySerno='" + surveySerno + '\'' +
                '}';
    }
}
