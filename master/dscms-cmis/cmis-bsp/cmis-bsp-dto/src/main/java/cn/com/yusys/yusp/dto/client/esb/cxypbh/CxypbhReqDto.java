package cn.com.yusys.yusp.dto.client.esb.cxypbh;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：通过票据号码查询押品编号
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CxypbhReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bill_no")
    private String bill_no;//票据号码

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    @Override
    public String toString() {
        return "CxypbhReqDto{" +
                "bill_no='" + bill_no + '\'' +
                '}';
    }
}
