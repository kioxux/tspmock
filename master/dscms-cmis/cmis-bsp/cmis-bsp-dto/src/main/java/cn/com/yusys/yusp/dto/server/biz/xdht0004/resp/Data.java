package cn.com.yusys.yusp.dto.server.biz.xdht0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：合同信息列表查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;
    /** 结算账户账号 **/
    @JsonProperty(value = "settlAcctNum")
    private String settlAcctNum;

    /** 账户名称 **/
    @JsonProperty(value = "acctName")
    private String acctName;

    /** 账户子序号 **/
    @JsonProperty(value = "acctSubNum")
    private String acctSubNum;

    /** 币种 **/
    @JsonProperty(value = "curType")
    private String curType;

    /** 开户机构 **/
    @JsonProperty(value = "openOrgId")
    private String openOrgId;

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    public String getSettlAcctNum() {
        return settlAcctNum;
    }

    public void setSettlAcctNum(String settlAcctNum) {
        this.settlAcctNum = settlAcctNum;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getAcctSubNum() {
        return acctSubNum;
    }

    public void setAcctSubNum(String acctSubNum) {
        this.acctSubNum = acctSubNum;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getOpenOrgId() {
        return openOrgId;
    }

    public void setOpenOrgId(String openOrgId) {
        this.openOrgId = openOrgId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "list=" + list +
                ", settlAcctNum='" + settlAcctNum + '\'' +
                ", acctName='" + acctName + '\'' +
                ", acctSubNum='" + acctSubNum + '\'' +
                ", curType='" + curType + '\'' +
                ", openOrgId='" + openOrgId + '\'' +
                '}';
    }
}