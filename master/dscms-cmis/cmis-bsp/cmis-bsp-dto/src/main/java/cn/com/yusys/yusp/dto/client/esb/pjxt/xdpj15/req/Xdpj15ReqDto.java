package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj15.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：发票补录信息推送（信贷调票据）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj15ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "status")
    private String status;//状态
    @JsonProperty(value = "batchNo")
    private String batchNo;//批次号
    @JsonProperty(value = "protocolno")
    private String protocolno;//协议号

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getProtocolno() {
        return protocolno;
    }

    public void setProtocolno(String protocolno) {
        this.protocolno = protocolno;
    }

    @Override
    public String toString() {
        return "Xdpj15ReqDto{" +
                "status='" + status + '\'' +
                "batchNo='" + batchNo + '\'' +
                "protocolno='" + protocolno + '\'' +
                '}';
    }
}  
