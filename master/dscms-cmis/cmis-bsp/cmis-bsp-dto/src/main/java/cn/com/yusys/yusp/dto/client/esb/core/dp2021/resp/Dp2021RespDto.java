package cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：客户账号-子账号互查
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2021RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "chaxfanw")
    private String chaxfanw;//查询范围
    @JsonProperty(value = "zongbshu")
    private Integer zongbshu;//总笔数
    @JsonProperty(value = "bblujing")
    private String bblujing;//报表路径
    @JsonProperty(value = "kzjedjbz")
    private String kzjedjbz;//客户账户金额冻结标志
    @JsonProperty(value = "kzfbdjbz")
    private String kzfbdjbz;//客户账户封闭冻结标志
    @JsonProperty(value = "kzzsbfbz")
    private String kzzsbfbz;//客户账户只收不付标志
    @JsonProperty(value = "kzzfbsbz")
    private String kzzfbsbz;//客户账户只付不收标志
    @JsonProperty(value = "kzhuztai")
    private String kzhuztai;//客户账户状态
    @JsonProperty(value = "zhhuyuee")
    private BigDecimal zhhuyuee;//当前账户余额
    @JsonProperty(value = "kehuleix")
    private String kehuleix;//客户类型
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "doqiriqi")
    private String doqiriqi;//客户号证件到期日期
    @JsonProperty(value = "kehuzhmc")
    private String kehuzhmc;//客户账户名称
    @JsonProperty(value = "lstacctinfo")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo> lstacctinfo;

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    public String getBblujing() {
        return bblujing;
    }

    public void setBblujing(String bblujing) {
        this.bblujing = bblujing;
    }

    public String getKzjedjbz() {
        return kzjedjbz;
    }

    public void setKzjedjbz(String kzjedjbz) {
        this.kzjedjbz = kzjedjbz;
    }

    public String getKzfbdjbz() {
        return kzfbdjbz;
    }

    public void setKzfbdjbz(String kzfbdjbz) {
        this.kzfbdjbz = kzfbdjbz;
    }

    public String getKzzsbfbz() {
        return kzzsbfbz;
    }

    public void setKzzsbfbz(String kzzsbfbz) {
        this.kzzsbfbz = kzzsbfbz;
    }

    public String getKzzfbsbz() {
        return kzzfbsbz;
    }

    public void setKzzfbsbz(String kzzfbsbz) {
        this.kzzfbsbz = kzzfbsbz;
    }

    public String getKzhuztai() {
        return kzhuztai;
    }

    public void setKzhuztai(String kzhuztai) {
        this.kzhuztai = kzhuztai;
    }

    public BigDecimal getZhhuyuee() {
        return zhhuyuee;
    }

    public void setZhhuyuee(BigDecimal zhhuyuee) {
        this.zhhuyuee = zhhuyuee;
    }

    public String getKehuleix() {
        return kehuleix;
    }

    public void setKehuleix(String kehuleix) {
        this.kehuleix = kehuleix;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public List<Lstacctinfo> getLstacctinfo() {
        return lstacctinfo;
    }

    public void setLstacctinfo(List<Lstacctinfo> lstacctinfo) {
        this.lstacctinfo = lstacctinfo;
    }

    @Override
    public String toString() {
        return "Dp2021RespDto{" +
                "chaxfanw='" + chaxfanw + '\'' +
                ", zongbshu=" + zongbshu +
                ", bblujing='" + bblujing + '\'' +
                ", kzjedjbz='" + kzjedjbz + '\'' +
                ", kzfbdjbz='" + kzfbdjbz + '\'' +
                ", kzzsbfbz='" + kzzsbfbz + '\'' +
                ", kzzfbsbz='" + kzzfbsbz + '\'' +
                ", kzhuztai='" + kzhuztai + '\'' +
                ", zhhuyuee=" + zhhuyuee +
                ", kehuleix='" + kehuleix + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", doqiriqi='" + doqiriqi + '\'' +
                ", kehuzhmc='" + kehuzhmc + '\'' +
                ", lstacctinfo=" + lstacctinfo +
                '}';
    }
}
