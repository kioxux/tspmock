package cn.com.yusys.yusp.dto.client.esb.core.ln3006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款产品组合查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3006ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "chanpzht")
    private String chanpzht;//产品状态
    @JsonProperty(value = "daikduix")
    private String daikduix;//贷款对象
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getChanpzht() {
        return chanpzht;
    }

    public void setChanpzht(String chanpzht) {
        this.chanpzht = chanpzht;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Ln3006ReqDto{" +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "chanpzht='" + chanpzht + '\'' +
                "daikduix='" + daikduix + '\'' +
                "qishibis='" + qishibis + '\'' +
                "chxunbis='" + chxunbis + '\'' +
                '}';
    }
}  
