package cn.com.yusys.yusp.dto.server.biz.xdxw0080.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：优惠利率申请结果通知
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "reqId")
    private String reqId;//优惠利率申请流水号
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批结果
    @JsonProperty(value = "approveRate")
    private String approveRate;//审批利率
    @JsonProperty(value = "refuseReason")
    private String refuseReason;//拒绝原因

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getApproveRate() {
        return approveRate;
    }

    public void setApproveRate(String approveRate) {
        this.approveRate = approveRate;
    }

    public String getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason;
    }

    @Override
    public String toString() {
        return "Data{" +
                "reqId='" + reqId + '\'' +
                "approveStatus='" + approveStatus + '\'' +
                "approveRate='" + approveRate + '\'' +
                "refuseReason='" + refuseReason + '\'' +
                '}';
    }
}
