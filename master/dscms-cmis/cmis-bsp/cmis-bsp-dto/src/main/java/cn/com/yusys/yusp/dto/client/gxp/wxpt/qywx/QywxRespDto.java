package cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：企业微信
 *
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class QywxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}
