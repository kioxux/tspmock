package cn.com.yusys.yusp.dto.client.esb.ypxt.contra;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：信贷查询地方征信接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class ContraRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String erorcd;//	响应码	否	char(4)	是	0000表示成功其它表示失败	erorcd
    private String erortx;//	响应信息	否	varchar(128)	否	失败详情	erortx


    public String getErorcd() {
        return erorcd;
    }


    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }


    @Override
    public String toString() {
        return "ContraRespDto{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                '}';
    }
}
