package cn.com.yusys.yusp.dto.server.biz.xddh0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 14:32:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 14:32
 * @since 2021/5/22 14:32
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Override
    public String toString() {
        return "Xddh0004ReqDto{" +
                "billNo='" + billNo + '\'' +
                '}';
    }
}
