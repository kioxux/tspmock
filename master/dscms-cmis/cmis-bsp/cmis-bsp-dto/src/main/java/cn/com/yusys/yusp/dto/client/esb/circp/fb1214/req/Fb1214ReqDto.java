package cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：房产信息修改同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1214ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "CUS_ID")
    private String CUS_ID;//客户号
    @JsonProperty(value = "CUS_NAME")
    private String CUS_NAME;//客户名称
    @JsonProperty(value = "CREDIT_TYPE")
    private String CREDIT_TYPE;//证件类型
    @JsonProperty(value = "CREDIT_ID")
    private String CREDIT_ID;//证件号码
    @JsonProperty(value = "CRD_ITEM_NUM")
    private String CRD_ITEM_NUM;//授信分项编号
    @JsonProperty(value = "PRE_CRD_AMT")
    private String PRE_CRD_AMT;//授信额度
    @JsonProperty(value = "HOUSE_LIST")
    private java.util.List<HOUSE_LIST> HOUSE_LIST;
    @JsonProperty(value = "ENT_LIST")
    private java.util.List<ENT_LIST> ENT_LIST;

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getCUS_ID() {
        return CUS_ID;
    }

    @JsonIgnore
    public void setCUS_ID(String CUS_ID) {
        this.CUS_ID = CUS_ID;
    }

    @JsonIgnore
    public String getCUS_NAME() {
        return CUS_NAME;
    }

    @JsonIgnore
    public void setCUS_NAME(String CUS_NAME) {
        this.CUS_NAME = CUS_NAME;
    }

    @JsonIgnore
    public String getCREDIT_TYPE() {
        return CREDIT_TYPE;
    }

    @JsonIgnore
    public void setCREDIT_TYPE(String CREDIT_TYPE) {
        this.CREDIT_TYPE = CREDIT_TYPE;
    }

    @JsonIgnore
    public String getCREDIT_ID() {
        return CREDIT_ID;
    }

    @JsonIgnore
    public void setCREDIT_ID(String CREDIT_ID) {
        this.CREDIT_ID = CREDIT_ID;
    }

    @JsonIgnore
    public String getCRD_ITEM_NUM() {
        return CRD_ITEM_NUM;
    }

    @JsonIgnore
    public void setCRD_ITEM_NUM(String CRD_ITEM_NUM) {
        this.CRD_ITEM_NUM = CRD_ITEM_NUM;
    }

    @JsonIgnore
    public String getPRE_CRD_AMT() {
        return PRE_CRD_AMT;
    }

    @JsonIgnore
    public void setPRE_CRD_AMT(String PRE_CRD_AMT) {
        this.PRE_CRD_AMT = PRE_CRD_AMT;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.HOUSE_LIST> getHOUSE_LIST() {
        return HOUSE_LIST;
    }

    @JsonIgnore
    public void setHOUSE_LIST(List<cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.HOUSE_LIST> HOUSE_LIST) {
        this.HOUSE_LIST = HOUSE_LIST;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.ENT_LIST> getENT_LIST() {
        return ENT_LIST;
    }

    @JsonIgnore
    public void setENT_LIST(List<cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.ENT_LIST> ENT_LIST) {
        this.ENT_LIST = ENT_LIST;
    }

    @Override
    public String toString() {
        return "Fb1214ReqDto{" +
                "PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", CUS_ID='" + CUS_ID + '\'' +
                ", CUS_NAME='" + CUS_NAME + '\'' +
                ", CREDIT_TYPE='" + CREDIT_TYPE + '\'' +
                ", CREDIT_ID='" + CREDIT_ID + '\'' +
                ", CRD_ITEM_NUM='" + CRD_ITEM_NUM + '\'' +
                ", PRE_CRD_AMT='" + PRE_CRD_AMT + '\'' +
                ", HOUSE_LIST=" + HOUSE_LIST +
                ", ENT_LIST=" + ENT_LIST +
                '}';
    }
}
