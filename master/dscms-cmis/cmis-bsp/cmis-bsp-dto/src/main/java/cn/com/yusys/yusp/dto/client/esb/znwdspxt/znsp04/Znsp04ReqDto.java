package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：自动审批调查报告
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp04ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "survey_serno")
    private String survey_serno;//业务流水号
    @JsonProperty(value = "apply_type")
    private String apply_type;//申请类型
    @JsonProperty(value = "prd_id")
    private String prd_id;//产品编号
    @JsonProperty(value = "prd_name")
    private String prd_name;//产品名称
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "loan_type")
    private String loan_type;//贷款类型
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cert_no")
    private String cert_no;//身份证号码
    @JsonProperty(value = "zb_manager_id")
    private String zb_manager_id;//主办客户经理工号
    @JsonProperty(value = "xb_manager_id")
    private String xb_manager_id;//协办客户经理
    @JsonProperty(value = "loan_amt")
    private BigDecimal loan_amt;//贷款金额
    @JsonProperty(value = "reality_ir_y")
    private BigDecimal reality_ir_y;//执行利率
    @JsonProperty(value = "loan_term")
    private BigDecimal loan_term;//贷款期限
    @JsonProperty(value = "guara_way")
    private String guara_way;//担保方式
    @JsonProperty(value = "is_sjsh")
    private String is_sjsh;//是否随机随还
    @JsonProperty(value = "repay_way")
    private String repay_way;//还款方式
    @JsonProperty(value = "cont_type")
    private String cont_type;//合同类型
    @JsonProperty(value = "term_loan_type")
    private String term_loan_type;//期限类型
    @JsonProperty(value = "is_trustee_pay")
    private String is_trustee_pay;//是否受托支付
    @JsonProperty(value = "entrust_type")
    private String entrust_type;//受托类型
    @JsonProperty(value = "is_credit_condition")
    private String is_credit_condition;//是否有放款条件
    @JsonProperty(value = "credit_condition")
    private String credit_condition;//放款条件
    @JsonProperty(value = "is_whbxd")
    private String is_whbxd;//是否无还本续贷
    @JsonProperty(value = "turnover_amt")
    private BigDecimal turnover_amt;//周转金额
    @JsonProperty(value = "add_amt")
    private BigDecimal add_amt;//新增金额
    @JsonProperty(value = "cus_phone")
    private String cus_phone;//客户手机号
    @JsonProperty(value = "loan_use")
    private String loan_use;//贷款用途


    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getApply_type() {
        return apply_type;
    }

    public void setApply_type(String apply_type) {
        this.apply_type = apply_type;
    }

    public String getPrd_id() {
        return prd_id;
    }

    public void setPrd_id(String prd_id) {
        this.prd_id = prd_id;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getLoan_type() {
        return loan_type;
    }

    public void setLoan_type(String loan_type) {
        this.loan_type = loan_type;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCert_no() {
        return cert_no;
    }

    public void setCert_no(String cert_no) {
        this.cert_no = cert_no;
    }

    public String getZb_manager_id() {
        return zb_manager_id;
    }

    public void setZb_manager_id(String zb_manager_id) {
        this.zb_manager_id = zb_manager_id;
    }

    public String getXb_manager_id() {
        return xb_manager_id;
    }

    public void setXb_manager_id(String xb_manager_id) {
        this.xb_manager_id = xb_manager_id;
    }

    public BigDecimal getLoan_amt() {
        return loan_amt;
    }

    public void setLoan_amt(BigDecimal loan_amt) {
        this.loan_amt = loan_amt;
    }

    public BigDecimal getReality_ir_y() {
        return reality_ir_y;
    }

    public void setReality_ir_y(BigDecimal reality_ir_y) {
        this.reality_ir_y = reality_ir_y;
    }

    public BigDecimal getLoan_term() {
        return loan_term;
    }

    public void setLoan_term(BigDecimal loan_term) {
        this.loan_term = loan_term;
    }

    public String getGuara_way() {
        return guara_way;
    }

    public void setGuara_way(String guara_way) {
        this.guara_way = guara_way;
    }

    public String getIs_sjsh() {
        return is_sjsh;
    }

    public void setIs_sjsh(String is_sjsh) {
        this.is_sjsh = is_sjsh;
    }

    public String getRepay_way() {
        return repay_way;
    }

    public void setRepay_way(String repay_way) {
        this.repay_way = repay_way;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getTerm_loan_type() {
        return term_loan_type;
    }

    public void setTerm_loan_type(String term_loan_type) {
        this.term_loan_type = term_loan_type;
    }

    public String getIs_trustee_pay() {
        return is_trustee_pay;
    }

    public void setIs_trustee_pay(String is_trustee_pay) {
        this.is_trustee_pay = is_trustee_pay;
    }

    public String getEntrust_type() {
        return entrust_type;
    }

    public void setEntrust_type(String entrust_type) {
        this.entrust_type = entrust_type;
    }

    public String getIs_credit_condition() {
        return is_credit_condition;
    }

    public void setIs_credit_condition(String is_credit_condition) {
        this.is_credit_condition = is_credit_condition;
    }

    public String getCredit_condition() {
        return credit_condition;
    }

    public void setCredit_condition(String credit_condition) {
        this.credit_condition = credit_condition;
    }

    public String getIs_whbxd() {
        return is_whbxd;
    }

    public void setIs_whbxd(String is_whbxd) {
        this.is_whbxd = is_whbxd;
    }

    public BigDecimal getTurnover_amt() {
        return turnover_amt;
    }

    public void setTurnover_amt(BigDecimal turnover_amt) {
        this.turnover_amt = turnover_amt;
    }

    public BigDecimal getAdd_amt() {
        return add_amt;
    }

    public void setAdd_amt(BigDecimal add_amt) {
        this.add_amt = add_amt;
    }

    public String getCus_phone() {
        return cus_phone;
    }

    public void setCus_phone(String cus_phone) {
        this.cus_phone = cus_phone;
    }

    public String getLoan_use() {
        return loan_use;
    }

    public void setLoan_use(String loan_use) {
        this.loan_use = loan_use;
    }

    @Override
    public String toString() {
        return "Znsp04ReqDto{" +
                "survey_serno='" + survey_serno + '\'' +
                "apply_type='" + apply_type + '\'' +
                "prd_id='" + prd_id + '\'' +
                "prd_name='" + prd_name + '\'' +
                "cus_name='" + cus_name + '\'' +
                "loan_type='" + loan_type + '\'' +
                "cus_id='" + cus_id + '\'' +
                "cert_no='" + cert_no + '\'' +
                "zb_manager_id='" + zb_manager_id + '\'' +
                "xb_manager_id='" + xb_manager_id + '\'' +
                "loan_amt='" + loan_amt + '\'' +
                "reality_ir_y='" + reality_ir_y + '\'' +
                "loan_term='" + loan_term + '\'' +
                "guara_way='" + guara_way + '\'' +
                "is_sjsh='" + is_sjsh + '\'' +
                "repay_way='" + repay_way + '\'' +
                "cont_type='" + cont_type + '\'' +
                "term_loan_type='" + term_loan_type + '\'' +
                "is_trustee_pay='" + is_trustee_pay + '\'' +
                "entrust_type='" + entrust_type + '\'' +
                "is_credit_condition='" + is_credit_condition + '\'' +
                "credit_condition='" + credit_condition + '\'' +
                "is_whbxd='" + is_whbxd + '\'' +
                "turnover_amt='" + turnover_amt + '\'' +
                "add_amt='" + add_amt + '\'' +
                "cus_phone='" + cus_phone + '\'' +
                "loan_use='" + loan_use + '\'' +
                '}';
    }
}
