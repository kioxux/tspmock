package cn.com.yusys.yusp.dto.client.esb.ecif.s10501;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：清单客户信息_ARRAY
 */
@JsonPropertyOrder(alphabetic = true)
public class ListArrayInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custno")
    private String custno;//客户编号
    @JsonProperty(value = "custna")
    private String custna;//客户名称
    @JsonProperty(value = "custst")
    private String custst;//客户状态
    @JsonProperty(value = "idtftp")
    private String idtftp;//证件类型
    @JsonProperty(value = "idtfno")
    private String idtfno;//证件号码
    @JsonProperty(value = "prpsex")
    private String prpsex;//性别
    @JsonProperty(value = "borndt")
    private String borndt;//出生日期
    @JsonProperty(value = "nation")
    private String nation;//国籍
    @JsonProperty(value = "creabr")
    private String creabr;//开户机构
    @JsonProperty(value = "userid")
    private String userid;//开户柜员
    @JsonProperty(value = "opendt")
    private String opendt;//开户时间
    @JsonProperty(value = "homead")
    private String homead;//地址
    @JsonProperty(value = "locpho")
    private String locpho;//电话

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getPrpsex() {
        return prpsex;
    }

    public void setPrpsex(String prpsex) {
        this.prpsex = prpsex;
    }

    public String getBorndt() {
        return borndt;
    }

    public void setBorndt(String borndt) {
        this.borndt = borndt;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getCreabr() {
        return creabr;
    }

    public void setCreabr(String creabr) {
        this.creabr = creabr;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOpendt() {
        return opendt;
    }

    public void setOpendt(String opendt) {
        this.opendt = opendt;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getLocpho() {
        return locpho;
    }

    public void setLocpho(String locpho) {
        this.locpho = locpho;
    }

    @Override
    public String toString() {
        return "ListArrayInfo{" +
                "custno='" + custno + '\'' +
                ", custna='" + custna + '\'' +
                ", custst='" + custst + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", prpsex='" + prpsex + '\'' +
                ", borndt='" + borndt + '\'' +
                ", nation='" + nation + '\'' +
                ", creabr='" + creabr + '\'' +
                ", userid='" + userid + '\'' +
                ", opendt='" + opendt + '\'' +
                ", homead='" + homead + '\'' +
                ", locpho='" + locpho + '\'' +
                '}';
    }
}
