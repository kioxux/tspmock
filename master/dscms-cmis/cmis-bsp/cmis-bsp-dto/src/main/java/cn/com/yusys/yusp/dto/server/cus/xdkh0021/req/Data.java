package cn.com.yusys.yusp.dto.server.cus.xdkh0021.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：在信贷系统中生成用户信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cus_id")
    private String cus_id;//用户ID
    @JsonProperty(value = "cus_name")
    private String cus_name;//用户名
    @JsonProperty(value = "cert_type")
    private String cert_type;//证件类型
    @JsonProperty(value = "cert_code")
    private String cert_code;//身份证号码
    @JsonProperty(value = "orgid")
    private String orgid;//机构号
    @JsonProperty(value = "currentuserid")
    private String currentuserid;//当前客户经理
    @JsonProperty(value = "agriFlg")
    private String agriFlg;//是否农户
    @JsonProperty(value = "yxdauthor")
    private String yxdauthor;//是否优享贷客户

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getCurrentuserid() {
        return currentuserid;
    }

    public void setCurrentuserid(String currentuserid) {
        this.currentuserid = currentuserid;
    }

    public String getAgriFlg() {
        return agriFlg;
    }

    public void setAgriFlg(String agriFlg) {
        this.agriFlg = agriFlg;
    }

    public String getYxdauthor() {
        return yxdauthor;
    }

    public void setYxdauthor(String yxdauthor) {
        this.yxdauthor = yxdauthor;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", orgid='" + orgid + '\'' +
                ", currentuserid='" + currentuserid + '\'' +
                ", agriFlg='" + agriFlg + '\'' +
                ", yxdauthor='" + yxdauthor + '\'' +
                '}';
    }
}
