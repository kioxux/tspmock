package cn.com.yusys.yusp.dto.server.biz.xdtz0021.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/21 13:54:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 13:54
 * @since 2021/5/21 13:54
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bizType")
    private String bizType;//业务类型
    @JsonProperty(value = "oprtype")
    private String oprtype;//操作类型
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户代码
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//贷款金额
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//贷款余额
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款起始日
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款到期日
    @JsonProperty(value = "rulingIr")
    private BigDecimal rulingIr;//基准利率年
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行利率年
    @JsonProperty(value = "rateFloat")
    private BigDecimal rateFloat;//利率浮动值
    @JsonProperty(value = "accStatus")
    private String accStatus;//台账状态
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//责任机构
    @JsonProperty(value = "cretQuotationExchgRate")
    private BigDecimal cretQuotationExchgRate;//信贷牌价汇率
    @JsonProperty(value = "subjectNo")
    private String subjectNo;//科目号

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getOprtype() {
        return oprtype;
    }

    public void setOprtype(String oprtype) {
        this.oprtype = oprtype;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public BigDecimal getRulingIr() {
        return rulingIr;
    }

    public void setRulingIr(BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public BigDecimal getRateFloat() {
        return rateFloat;
    }

    public void setRateFloat(BigDecimal rateFloat) {
        this.rateFloat = rateFloat;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public BigDecimal getCretQuotationExchgRate() {
        return cretQuotationExchgRate;
    }

    public void setCretQuotationExchgRate(BigDecimal cretQuotationExchgRate) {
        this.cretQuotationExchgRate = cretQuotationExchgRate;
    }

    public String getSubjectNo() {
        return subjectNo;
    }

    public void setSubjectNo(String subjectNo) {
        this.subjectNo = subjectNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "bizType='" + bizType + '\'' +
                ", oprtype='" + oprtype + '\'' +
                ", billNo='" + billNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", prdId='" + prdId + '\'' +
                ", contNo='" + contNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", loanAmt=" + loanAmt +
                ", curType='" + curType + '\'' +
                ", loanBalance=" + loanBalance +
                ", loanStartDate='" + loanStartDate + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", rulingIr=" + rulingIr +
                ", realityIrY=" + realityIrY +
                ", rateFloat=" + rateFloat +
                ", accStatus='" + accStatus + '\'' +
                ", inputId='" + inputId + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", cretQuotationExchgRate=" + cretQuotationExchgRate +
                ", subjectNo='" + subjectNo + '\'' +
                '}';
    }
}
