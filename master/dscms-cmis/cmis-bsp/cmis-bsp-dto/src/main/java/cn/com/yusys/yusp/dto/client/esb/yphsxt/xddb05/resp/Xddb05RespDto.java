package cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb05.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询基本信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb05RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_cus_name")
    private String guar_cus_name;//所有权人名称
    @JsonProperty(value = "ship_status")
    private String ship_status;//权属状况
    @JsonProperty(value = "eval_amt")
    private BigDecimal eval_amt;//我行确认价值
    @JsonProperty(value = "eval_date")
    private String eval_date;//我行确认日期
    @JsonProperty(value = "common_assets_ind")
    private String common_assets_ind;//是否共有财产
    @JsonProperty(value = "guar_type_cd")
    private String guar_type_cd;//担保分类代码
    @JsonProperty(value = "guar_sub_no")
    private String guar_sub_no;//押品详细编号
    @JsonProperty(value = "eval_in_amt")
    private BigDecimal eval_in_amt;//评估价值
    @JsonProperty(value = "guar_type_cd_cnname")
    private String guar_type_cd_cnname;//担保分类名称

    public String getGuar_cus_name() {
        return guar_cus_name;
    }

    public void setGuar_cus_name(String guar_cus_name) {
        this.guar_cus_name = guar_cus_name;
    }

    public String getShip_status() {
        return ship_status;
    }

    public void setShip_status(String ship_status) {
        this.ship_status = ship_status;
    }

    public BigDecimal getEval_amt() {
        return eval_amt;
    }

    public void setEval_amt(BigDecimal eval_amt) {
        this.eval_amt = eval_amt;
    }

    public String getEval_date() {
        return eval_date;
    }

    public void setEval_date(String eval_date) {
        this.eval_date = eval_date;
    }

    public String getCommon_assets_ind() {
        return common_assets_ind;
    }

    public void setCommon_assets_ind(String common_assets_ind) {
        this.common_assets_ind = common_assets_ind;
    }

    public String getGuar_type_cd() {
        return guar_type_cd;
    }

    public void setGuar_type_cd(String guar_type_cd) {
        this.guar_type_cd = guar_type_cd;
    }

    public String getGuar_sub_no() {
        return guar_sub_no;
    }

    public void setGuar_sub_no(String guar_sub_no) {
        this.guar_sub_no = guar_sub_no;
    }

    public BigDecimal getEval_in_amt() {
        return eval_in_amt;
    }

    public void setEval_in_amt(BigDecimal eval_in_amt) {
        this.eval_in_amt = eval_in_amt;
    }

    public String getGuar_type_cd_cnname() {
        return guar_type_cd_cnname;
    }

    public void setGuar_type_cd_cnname(String guar_type_cd_cnname) {
        this.guar_type_cd_cnname = guar_type_cd_cnname;
    }

    @Override
    public String toString() {
        return "Xddb05RespDto{" +
                "guar_cus_name='" + guar_cus_name + '\'' +
                "ship_status='" + ship_status + '\'' +
                "eval_amt='" + eval_amt + '\'' +
                "eval_date ='" + eval_date + '\'' +
                "common_assets_ind='" + common_assets_ind + '\'' +
                "guar_type_cd='" + guar_type_cd + '\'' +
                "guar_sub_no='" + guar_sub_no + '\'' +
                "eval_in_amt='" + eval_in_amt + '\'' +
                "guar_type_cd_cnname='" + guar_type_cd_cnname + '\'' +
                '}';
    }
}  
