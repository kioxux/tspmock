package cn.com.yusys.yusp.dto.server.biz.xdcz0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：电子保函注销
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data  implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFlag")
    private String opFlag;//操作成功标志位
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息
    public String  getOpFlag() { return opFlag; }
    public void setOpFlag(String opFlag ) { this.opFlag = opFlag;}
    public String  getOpMsg() { return opMsg; }
    public void setOpMsg(String opMsg ) { this.opMsg = opMsg;}
    @Override
    public String toString() {
        return "Xdcz0002RespDto{" +
                "opFlag='" + opFlag+ '\'' +
                "opMsg='" + opMsg+ '\'' +
                '}';
    }
}
