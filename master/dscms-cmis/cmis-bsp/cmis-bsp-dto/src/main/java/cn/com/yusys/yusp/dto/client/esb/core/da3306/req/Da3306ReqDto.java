package cn.com.yusys.yusp.dto.client.esb.core.da3306.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：抵债资产拨备计提
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3306ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "jitijine")
    private BigDecimal jitijine;//计提金额
    @JsonProperty(value = "dzzcrzjz")
    private BigDecimal dzzcrzjz;//抵债资产入账价值
    @JsonProperty(value = "pingjiaz")
    private BigDecimal pingjiaz;//评估价值
    @JsonProperty(value = "scbbjtrq")
    private String scbbjtrq;//上次拨备计提日期
    @JsonProperty(value = "scjzzbje")
    private BigDecimal scjzzbje;//上次减值准备金额

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getJitijine() {
        return jitijine;
    }

    public void setJitijine(BigDecimal jitijine) {
        this.jitijine = jitijine;
    }

    public BigDecimal getDzzcrzjz() {
        return dzzcrzjz;
    }

    public void setDzzcrzjz(BigDecimal dzzcrzjz) {
        this.dzzcrzjz = dzzcrzjz;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public String getScbbjtrq() {
        return scbbjtrq;
    }

    public void setScbbjtrq(String scbbjtrq) {
        this.scbbjtrq = scbbjtrq;
    }

    public BigDecimal getScjzzbje() {
        return scjzzbje;
    }

    public void setScjzzbje(BigDecimal scjzzbje) {
        this.scjzzbje = scjzzbje;
    }

    @Override
    public String toString() {
        return "Da3306ReqDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", jitijine=" + jitijine +
                ", dzzcrzjz=" + dzzcrzjz +
                ", pingjiaz=" + pingjiaz +
                ", scbbjtrq='" + scbbjtrq + '\'' +
                ", scjzzbje=" + scjzzbje +
                '}';
    }
}
