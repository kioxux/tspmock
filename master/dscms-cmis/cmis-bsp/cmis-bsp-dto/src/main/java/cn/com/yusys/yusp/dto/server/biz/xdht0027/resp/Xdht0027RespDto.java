package cn.com.yusys.yusp.dto.server.biz.xdht0027.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据客户调查表编号取得贷款合同主表的合同状态
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0027RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdht0027RespDto{" +
				"data=" + data +
				'}';
	}
}
