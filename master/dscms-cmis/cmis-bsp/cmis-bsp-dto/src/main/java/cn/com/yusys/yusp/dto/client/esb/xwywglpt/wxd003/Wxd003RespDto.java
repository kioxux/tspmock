package cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷系统请求V平台二级准入接口
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxd003RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "code")
    private String code;//返回码
    @JsonProperty(value = "message")
    private String message;//返回提示

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Wxd003RespDto{" +
                "code='" + code + '\'' +
                "message='" + message + '\'' +
                '}';
    }
}