package cn.com.yusys.yusp.dto.client.esb.core.ln3026;

import cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：银团贷款开户
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3026RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据放款金额
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//营业机构
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "bencfkje")
    private BigDecimal bencfkje;//本次放款金额
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "wtckzhao")
    private String wtckzhao;//委托存款账号
    @JsonProperty(value = "wtckzixh")
    private String wtckzixh;//委托存款账号子序号
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "dkrzhzxh")
    private String dkrzhzxh;//贷款入账账号子序号
    @JsonProperty(value = "lstytczfe")
    private List<Lstytczfe> lstytczfe;//银团出资份额定价计息复合类型
    @JsonProperty(value = "yintdkbz")
    private String yintdkbz;//银团贷款
    @JsonProperty(value = "dkdbfshi")
    private String dkdbfshi;//贷款担保方式
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "jfxibzhi")
    private String jfxibzhi;//计复息标志
    @JsonProperty(value = "flllcklx")
    private String flllcklx;//复利利率参考类型
    @JsonProperty(value = "zjlyzhao")
    private String zjlyzhao;//资金来源账号
    @JsonProperty(value = "zjlyzzxh")
    private String zjlyzzxh;//资金来源账号子序号
    @JsonProperty(value = "wujiflbz")
    private String wujiflbz;//五级分类标志
    @JsonProperty(value = "dkyongtu")
    private String dkyongtu;//贷款用途
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注
    @JsonProperty(value = "fuhejgou")
    private String fuhejgou;//复核机构
    @JsonProperty(value = "lstdkhbjh")
    private List<Lstdkhbjh> lstdkhbjh;//贷款还本计划
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "lstdzqgjh")
    private List<Lstdzqgjh> lstdzqgjh;//贷款定制期供计划表
    @JsonProperty(value = "llqxkdfs")
    private String llqxkdfs;//利率期限靠档方式
    @JsonProperty(value = "lilvqixx")
    private String lilvqixx;//利率期限
    @JsonProperty(value = "fkjzhfsh")
    private String fkjzhfsh;//放款记账方式
    @JsonProperty(value = "hetongll")
    private BigDecimal hetongll;//合同利率
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "dhkzhhbz")
    private String dhkzhhbz;//多还款账户标志
    @JsonProperty(value = "lstdkhkzh")
    private List<Lstdkhkzh> lstdkhkzh;//贷款多还款账户
    @JsonProperty(value = "lstdkstzf")
    private List<Lstdkstzf> lstdkstzf;//贷款受托支付


    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public BigDecimal getBencfkje() {
        return bencfkje;
    }

    public void setBencfkje(BigDecimal bencfkje) {
        this.bencfkje = bencfkje;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public List<Lstytczfe> getLstytczfe() {
        return lstytczfe;
    }

    public void setLstytczfe(List<Lstytczfe> lstytczfe) {
        this.lstytczfe = lstytczfe;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getWujiflbz() {
        return wujiflbz;
    }

    public void setWujiflbz(String wujiflbz) {
        this.wujiflbz = wujiflbz;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public List<Lstdkhbjh> getLstdkhbjh() {
        return lstdkhbjh;
    }

    public void setLstdkhbjh(List<Lstdkhbjh> lstdkhbjh) {
        this.lstdkhbjh = lstdkhbjh;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public List<Lstdzqgjh> getLstdzqgjh() {
        return lstdzqgjh;
    }

    public void setLstdzqgjh(List<Lstdzqgjh> lstdzqgjh) {
        this.lstdzqgjh = lstdzqgjh;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public List<Lstdkhkzh> getLstdkhkzh() {
        return lstdkhkzh;
    }

    public void setLstdkhkzh(List<Lstdkhkzh> lstdkhkzh) {
        this.lstdkhkzh = lstdkhkzh;
    }

    public List<Lstdkstzf> getLstdkstzf() {
        return lstdkstzf;
    }

    public void setLstdkstzf(List<Lstdkstzf> lstdkstzf) {
        this.lstdkstzf = lstdkstzf;
    }

    @Override
    public String toString() {
        return "Ln3026RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "dkqixian='" + dkqixian + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "bencfkje='" + bencfkje + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "wtckzhao='" + wtckzhao + '\'' +
                "wtckzixh='" + wtckzixh + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "lstytczfe='" + lstytczfe + '\'' +
                "yintdkbz='" + yintdkbz + '\'' +
                "dkdbfshi='" + dkdbfshi + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "jfxibzhi='" + jfxibzhi + '\'' +
                "flllcklx='" + flllcklx + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "wujiflbz='" + wujiflbz + '\'' +
                "dkyongtu='" + dkyongtu + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                "lstdkhbjh='" + lstdkhbjh + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "lstdzqgjh='" + lstdzqgjh + '\'' +
                "llqxkdfs='" + llqxkdfs + '\'' +
                "lilvqixx='" + lilvqixx + '\'' +
                "fkjzhfsh='" + fkjzhfsh + '\'' +
                "hetongll='" + hetongll + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "fulililv='" + fulililv + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "lstdkhkzh='" + lstdkhkzh + '\'' +
                "lstdkstzf='" + lstdkstzf + '\'' +
                '}';
    }
}  
