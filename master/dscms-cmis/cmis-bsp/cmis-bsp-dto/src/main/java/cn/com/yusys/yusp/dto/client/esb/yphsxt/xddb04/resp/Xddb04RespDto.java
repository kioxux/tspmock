package cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb04.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询质押物信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb04RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "name")
    private String name;//名称
    @JsonProperty(value = "amt")
    private BigDecimal amt;//价值
    @JsonProperty(value = "amount")
    private String amount;//数量
    @JsonProperty(value = "stock_no")
    private String stock_no;//代码/号码
    @JsonProperty(value = "end_date")
    private String end_date;//到期日
    @JsonProperty(value = "cur_type")
    private String cur_type;//币种

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStock_no() {
        return stock_no;
    }

    public void setStock_no(String stock_no) {
        this.stock_no = stock_no;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    @Override
    public String toString() {
        return "Xddb04RespDto{" +
                "name='" + name + '\'' +
                "amt='" + amt + '\'' +
                "amount='" + amount + '\'' +
                "stock_no='" + stock_no + '\'' +
                "end_date='" + end_date + '\'' +
                "cur_type='" + cur_type + '\'' +
                '}';
    }
}  
