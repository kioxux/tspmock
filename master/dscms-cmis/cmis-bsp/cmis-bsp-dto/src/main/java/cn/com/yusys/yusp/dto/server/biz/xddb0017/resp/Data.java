package cn.com.yusys.yusp.dto.server.biz.xddb0017.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 9:18
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "audioInfoLists")
    private java.util.List<AudioInfoList> audioInfoLists;

    public List<AudioInfoList> getAudioInfoLists() {
        return audioInfoLists;
    }

    public void setAudioInfoLists(List<AudioInfoList> audioInfoLists) {
        this.audioInfoLists = audioInfoLists;
    }

    @Override
    public String toString() {
        return "Data{" +
                "audioInfoLists=" + audioInfoLists +
                '}';
    }
}
