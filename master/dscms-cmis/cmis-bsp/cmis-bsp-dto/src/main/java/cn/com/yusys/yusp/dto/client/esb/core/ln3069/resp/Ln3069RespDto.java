package cn.com.yusys.yusp.dto.client.esb.core.ln3069.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：资产转让/证券化组合查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3069RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lstzczrxy")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3069.resp.Lstzczrxy> lstzczrxy;

    public List<Lstzczrxy> getLstzczrxy() {
        return lstzczrxy;
    }

    public void setLstzczrxy(List<Lstzczrxy> lstzczrxy) {
        this.lstzczrxy = lstzczrxy;
    }

    @Override
    public String toString() {
        return "Ln3069RespDto{" +
                "lstzczrxy=" + lstzczrxy +
                '}';
    }
}
