package cn.com.yusys.yusp.dto.client.esb.core.ln3102;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：贷款期供查询试算
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3102RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "zhxnlilv")
    private BigDecimal zhxnlilv;//执行年利率
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "zongqish")
    private Integer zongqish;//总期数
    @JsonProperty(value = "benqqish")
    private Integer benqqish;//本期期数
    @JsonProperty(value = "yuqiqish")
    private Integer yuqiqish;//逾期期数
    @JsonProperty(value = "shyuqish")
    private Integer shyuqish;//剩余期数
    @JsonProperty(value = "yihqishu")
    private Integer yihqishu;//已还期数
    @JsonProperty(value = "leijyhbj")
    private BigDecimal leijyhbj;//累计已还本金
    @JsonProperty(value = "leijyhlx")
    private BigDecimal leijyhlx;//累计已还利息
    @JsonProperty(value = "daikxtai")
    private String daikxtai;//贷款形态
    @JsonProperty(value = "dkzhhzht")
    private String dkzhhzht;//贷款账户状态
    @JsonProperty(value = "yjfyjzht")
    private String yjfyjzht;//应计非应计状态
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额
    @JsonProperty(value = "baoliuje")
    private BigDecimal baoliuje;//保留金额
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "schkriqi")
    private String schkriqi;//上次还款日
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "hexiaobj")
    private BigDecimal hexiaobj;//核销本金
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "benqizqs")
    private Integer benqizqs;//本期子期数
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "leijyqbj")
    private BigDecimal leijyqbj;//累计逾期本金
    @JsonProperty(value = "leijyqqx")
    private BigDecimal leijyqqx;//累计逾期欠息
    @JsonProperty(value = "leijcsbj")
    private BigDecimal leijcsbj;//累计产生本金
    @JsonProperty(value = "leijcslx")
    private BigDecimal leijcslx;//累计产生利息
    @JsonProperty(value = "yingjitx")
    private BigDecimal yingjitx;//应计贴息
    @JsonProperty(value = "tiexfuli")
    private BigDecimal tiexfuli;//贴息复利
    @JsonProperty(value = "yingshtx")
    private BigDecimal yingshtx;//应收贴息
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "wjmingch")
    private String wjmingch;//文件名
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "lstdkzqg")
    private List<Lstdkzqg> lstdkzqg;//贷款账户期供
    @JsonProperty(value = "lstdkhkzh")
    private List<Lstdkhkzh> lstdkhkzh;//贷款多还款账户


    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getZhxnlilv() {
        return zhxnlilv;
    }

    public void setZhxnlilv(BigDecimal zhxnlilv) {
        this.zhxnlilv = zhxnlilv;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public Integer getZongqish() {
        return zongqish;
    }

    public void setZongqish(Integer zongqish) {
        this.zongqish = zongqish;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public Integer getYuqiqish() {
        return yuqiqish;
    }

    public void setYuqiqish(Integer yuqiqish) {
        this.yuqiqish = yuqiqish;
    }

    public Integer getShyuqish() {
        return shyuqish;
    }

    public void setShyuqish(Integer shyuqish) {
        this.shyuqish = shyuqish;
    }

    public Integer getYihqishu() {
        return yihqishu;
    }

    public void setYihqishu(Integer yihqishu) {
        this.yihqishu = yihqishu;
    }

    public BigDecimal getLeijyhbj() {
        return leijyhbj;
    }

    public void setLeijyhbj(BigDecimal leijyhbj) {
        this.leijyhbj = leijyhbj;
    }

    public BigDecimal getLeijyhlx() {
        return leijyhlx;
    }

    public void setLeijyhlx(BigDecimal leijyhlx) {
        this.leijyhlx = leijyhlx;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getSchkriqi() {
        return schkriqi;
    }

    public void setSchkriqi(String schkriqi) {
        this.schkriqi = schkriqi;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public Integer getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(Integer benqizqs) {
        this.benqizqs = benqizqs;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public BigDecimal getLeijyqbj() {
        return leijyqbj;
    }

    public void setLeijyqbj(BigDecimal leijyqbj) {
        this.leijyqbj = leijyqbj;
    }

    public BigDecimal getLeijyqqx() {
        return leijyqqx;
    }

    public void setLeijyqqx(BigDecimal leijyqqx) {
        this.leijyqqx = leijyqqx;
    }

    public BigDecimal getLeijcsbj() {
        return leijcsbj;
    }

    public void setLeijcsbj(BigDecimal leijcsbj) {
        this.leijcsbj = leijcsbj;
    }

    public BigDecimal getLeijcslx() {
        return leijcslx;
    }

    public void setLeijcslx(BigDecimal leijcslx) {
        this.leijcslx = leijcslx;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getTiexfuli() {
        return tiexfuli;
    }

    public void setTiexfuli(BigDecimal tiexfuli) {
        this.tiexfuli = tiexfuli;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getWjmingch() {
        return wjmingch;
    }

    public void setWjmingch(String wjmingch) {
        this.wjmingch = wjmingch;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List<Lstdkzqg> getLstdkzqg() {
        return lstdkzqg;
    }

    public void setLstdkzqg(List<Lstdkzqg> lstdkzqg) {
        this.lstdkzqg = lstdkzqg;
    }

    public List<Lstdkhkzh> getLstdkhkzh() {
        return lstdkhkzh;
    }

    public void setLstdkhkzh(List<Lstdkhkzh> lstdkhkzh) {
        this.lstdkhkzh = lstdkhkzh;
    }

    @Override
    public String toString() {
        return "Ln3102RespDto{" +
                "dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", hetongje=" + hetongje +
                ", jiejuuje=" + jiejuuje +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", zhxnlilv=" + zhxnlilv +
                ", huankfsh='" + huankfsh + '\'' +
                ", dechligz='" + dechligz + '\'' +
                ", zongqish=" + zongqish +
                ", benqqish=" + benqqish +
                ", yuqiqish=" + yuqiqish +
                ", shyuqish=" + shyuqish +
                ", yihqishu=" + yihqishu +
                ", leijyhbj=" + leijyhbj +
                ", leijyhlx=" + leijyhlx +
                ", daikxtai='" + daikxtai + '\'' +
                ", dkzhhzht='" + dkzhhzht + '\'' +
                ", yjfyjzht='" + yjfyjzht + '\'' +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", baoliuje=" + baoliuje +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", schkriqi='" + schkriqi + '\'' +
                ", zhchbjin=" + zhchbjin +
                ", hexiaobj=" + hexiaobj +
                ", hexiaolx=" + hexiaolx +
                ", benqizqs=" + benqizqs +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", leijyqbj=" + leijyqbj +
                ", leijyqqx=" + leijyqqx +
                ", leijcsbj=" + leijcsbj +
                ", leijcslx=" + leijcslx +
                ", yingjitx=" + yingjitx +
                ", tiexfuli=" + tiexfuli +
                ", yingshtx=" + yingshtx +
                ", huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", wjmingch='" + wjmingch + '\'' +
                ", zongbish=" + zongbish +
                ", lstdkzqg=" + lstdkzqg +
                ", lstdkhkzh=" + lstdkhkzh +
                '}';
    }
}
