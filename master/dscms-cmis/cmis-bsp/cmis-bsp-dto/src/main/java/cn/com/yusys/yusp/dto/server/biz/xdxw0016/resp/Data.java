package cn.com.yusys.yusp.dto.server.biz.xdxw0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：根据客户号查询查询统一管控额度接口（总额度）
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "crdAmtTotal")
    private BigDecimal crdAmtTotal;//授信金额（总额度）
    @JsonProperty(value = "lmtValAmt")
    private BigDecimal lmtValAmt;//总授信可用额度
    @JsonProperty(value = "lmtList")
    private java.util.List<LmtList>  lmtList;

    public BigDecimal getCrdAmtTotal() {
        return crdAmtTotal;
    }

    public void setCrdAmtTotal(BigDecimal crdAmtTotal) {
        this.crdAmtTotal = crdAmtTotal;
    }

    public BigDecimal getLmtValAmt() {
        return lmtValAmt;
    }

    public void setLmtValAmt(BigDecimal lmtValAmt) {
        this.lmtValAmt = lmtValAmt;
    }

    public java.util.List<LmtList>  getLmtList() {
        return lmtList;
    }

    public void setLmtList(java.util.List<LmtList>  lmtList) {
        this.lmtList = lmtList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "crdAmtTotal=" + crdAmtTotal +
                "lmtList=" + lmtList +
                '}';
    }
}
