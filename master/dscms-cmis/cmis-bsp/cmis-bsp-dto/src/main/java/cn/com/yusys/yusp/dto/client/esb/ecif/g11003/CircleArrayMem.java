package cn.com.yusys.yusp.dto.client.esb.ecif.g11003;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Service：客户集团信息维护 (new)
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class CircleArrayMem implements Serializable {
    private static final long serialVersionUID = 1L;

    //	ARRAY		集团成员信息_ARRAY
    @JsonProperty(value = "custno")
    private String custno;//	varchar2  	30	集团成员客户号
    @JsonProperty(value = "cortype")
    private String cortype;//	varchar2  	5	集团关系类型
    @JsonProperty(value = "grpdesc")
    private String grpdesc;//	varchar2  	250	集团关系描述
    @JsonProperty(value = "iscust")
    private String iscust;//	varchar2  	5	是否集团主客户	"0：否1：是"
    @JsonProperty(value = "mainno")
    private String mainno;//	varchar2  	30	主管人
    @JsonProperty(value = "maiorg")
    private String maiorg;//	varchar2  	30	主管机构
    @JsonProperty(value = "socino")
    private String socino;//	varchar2  	18	统一社会信用代码
    @JsonProperty(value = "custst")
    private String custst;//	varchar2  	5	成员状态	"0：有效1：无效"

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCortype() {
        return cortype;
    }

    public void setCortype(String cortype) {
        this.cortype = cortype;
    }

    public String getGrpdesc() {
        return grpdesc;
    }

    public void setGrpdesc(String grpdesc) {
        this.grpdesc = grpdesc;
    }

    public String getIscust() {
        return iscust;
    }

    public void setIscust(String iscust) {
        this.iscust = iscust;
    }

    public String getMainno() {
        return mainno;
    }

    public void setMainno(String mainno) {
        this.mainno = mainno;
    }

    public String getMaiorg() {
        return maiorg;
    }

    public void setMaiorg(String maiorg) {
        this.maiorg = maiorg;
    }

    public String getSocino() {
        return socino;
    }

    public void setSocino(String socino) {
        this.socino = socino;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    @Override
    public String toString() {
        return "RelArrayInfo{" +
                "custno='" + custno + '\'' +
                ", cortype='" + cortype + '\'' +
                ", grpdesc='" + grpdesc + '\'' +
                ", iscust='" + iscust + '\'' +
                ", mainno='" + mainno + '\'' +
                ", maiorg='" + maiorg + '\'' +
                ", socino='" + socino + '\'' +
                ", custst='" + custst + '\'' +
                '}';
    }
}
