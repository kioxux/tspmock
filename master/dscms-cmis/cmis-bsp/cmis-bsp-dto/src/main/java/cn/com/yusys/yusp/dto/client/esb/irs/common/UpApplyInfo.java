package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 最高额授信协议信息(UpApplyInfo)
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
@JsonPropertyOrder(alphabetic = true)
public class UpApplyInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lmt_serno")
    private String lmt_serno; // 授信协议编号
    @JsonProperty(value = "item_id")
    private String item_id; // 授信台账编号
    @JsonProperty(value = "cus_id")
    private String cus_id; // 客户编号
    @JsonProperty(value = "cus_name")
    private String cus_name; // 客户名称
    @JsonProperty(value = "cur_type")
    private String cur_type; // 币种
    @JsonProperty(value = "amt")
    private BigDecimal amt; // 金额（元）
    @JsonProperty(value = "status")
    private String status; // 协议状态
    @JsonProperty(value = "start_date")
    private String start_date; // 协议起始日
    @JsonProperty(value = "end_date")
    private String end_date; // 协议到期日

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    @Override
    public String toString() {
        return "UpApplyInfo{" +
                "lmt_serno='" + lmt_serno + '\'' +
                ", item_id='" + item_id + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", amt=" + amt +
                ", status='" + status + '\'' +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                '}';
    }
}
