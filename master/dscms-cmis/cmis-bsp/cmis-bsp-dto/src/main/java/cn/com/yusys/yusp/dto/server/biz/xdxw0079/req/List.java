package cn.com.yusys.yusp.dto.server.biz.xdxw0079.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：小微续贷白名单查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "certCode")
    private String certCode;//申请人证件号
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "approveRs")
    private String approveRs;//准入结果

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getApproveRs() {
        return approveRs;
    }

    public void setApproveRs(String approveRs) {
        this.approveRs = approveRs;
    }

    @Override
    public String toString() {
        return "List{" +
                "certCode='" + certCode + '\'' +
                "serno='" + serno + '\'' +
                "approveRs='" + approveRs + '\'' +
                '}';
    }
}
