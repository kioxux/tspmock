package cn.com.yusys.yusp.dto.server.biz.xdzc0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/21 14:09
 * @since 2021/6/21 14:09
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "openType")
    private String openType;// 开关类型
    @JsonProperty(value = "cusId")
    private String cusId;// 客户编号

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "openType='" + openType + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
