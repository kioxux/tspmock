package cn.com.yusys.yusp.dto.client.esb.ecif.g10002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/6 13:56
 * @since 2021/7/6 13:56
 */
@JsonPropertyOrder(alphabetic = true)
public class GroupArrayMem implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "gpmbna")
    private String gpmbna;//群组成员客户号
    @JsonProperty(value = "membtp")
    private String membtp;//成员类型
    @JsonProperty(value = "membst")
    private String membst;//成员状态

    public String getGpmbna() {
        return gpmbna;
    }

    public void setGpmbna(String gpmbna) {
        this.gpmbna = gpmbna;
    }

    public String getMembtp() {
        return membtp;
    }

    public void setMembtp(String membtp) {
        this.membtp = membtp;
    }

    public String getMembst() {
        return membst;
    }

    public void setMembst(String membst) {
        this.membst = membst;
    }

    @Override
    public String toString() {
        return "GroupArrayMem{" +
                "gpmbna='" + gpmbna + '\'' +
                ", membtp='" + membtp + '\'' +
                ", membst='" + membst + '\'' +
                '}';
    }
}
