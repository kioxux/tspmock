package cn.com.yusys.yusp.dto.server.biz.xdtz0049.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "loanPrd")
    private String loanPrd;//贷款产品
    @JsonProperty(value = "belgTrade")
    private String belgTrade;//所属行业
    @JsonProperty(value = "cusType")
    private String cusType;//客户类型
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//贷款金额
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//贷款余额

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getLoanPrd() {
        return loanPrd;
    }

    public void setLoanPrd(String loanPrd) {
        this.loanPrd = loanPrd;
    }

    public String getBelgTrade() {
        return belgTrade;
    }

    public void setBelgTrade(String belgTrade) {
        this.belgTrade = belgTrade;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", loanPrd='" + loanPrd + '\'' +
                ", belgTrade='" + belgTrade + '\'' +
                ", cusType='" + cusType + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", repayType='" + repayType + '\'' +
                ", loanAmt=" + loanAmt +
                ", loanBalance=" + loanBalance +
                '}';
    }
}
