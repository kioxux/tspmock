package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：交易请求信息域:分项额度信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日15:10:55
 */
@JsonPropertyOrder(alphabetic = true)
public class LimitDetailsInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "detail_serno")
    private String detail_serno; // 授信分项流水号
    @JsonProperty(value = "item_id")
    private String item_id; // 额度台账编号
    @JsonProperty(value = "serno")
    private String serno; // 关联授信申请流水号
    @JsonProperty(value = "lmt_serno")
    private String lmt_serno; // 关联授信协议编号
    @JsonProperty(value = "cus_id")
    private String cus_id; // 客户编号
    @JsonProperty(value = "cus_name")
    private String cus_name; // 客户名称
    @JsonProperty(value = "assure_means_main")
    private String assure_means_main; // 担保方式
    @JsonProperty(value = "crd_lmt_type")
    private String crd_lmt_type; // 额度类型
    @JsonProperty(value = "cur_type")
    private String cur_type; // 币种
    @JsonProperty(value = "crd_lmt")
    private BigDecimal crd_lmt; // 授信额度（元）
    @JsonProperty(value = "start_date")
    private String start_date; // 起始日期
    @JsonProperty(value = "expi_date")
    private String expi_date; // 到期日
    @JsonProperty(value = "delay_months")
    private Integer delay_months; // 宽限月（月）

    public String getDetail_serno() {
        return detail_serno;
    }

    public void setDetail_serno(String detail_serno) {
        this.detail_serno = detail_serno;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getAssure_means_main() {
        return assure_means_main;
    }

    public void setAssure_means_main(String assure_means_main) {
        this.assure_means_main = assure_means_main;
    }

    public String getCrd_lmt_type() {
        return crd_lmt_type;
    }

    public void setCrd_lmt_type(String crd_lmt_type) {
        this.crd_lmt_type = crd_lmt_type;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getCrd_lmt() {
        return crd_lmt;
    }

    public void setCrd_lmt(BigDecimal crd_lmt) {
        this.crd_lmt = crd_lmt;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getExpi_date() {
        return expi_date;
    }

    public void setExpi_date(String expi_date) {
        this.expi_date = expi_date;
    }

    public Integer getDelay_months() {
        return delay_months;
    }

    public void setDelay_months(Integer delay_months) {
        this.delay_months = delay_months;
    }

    @Override
    public String toString() {
        return "LimitApplyInfo{" +
                "detail_serno='" + detail_serno + '\'' +
                ", item_id='" + item_id + '\'' +
                ", serno='" + serno + '\'' +
                ", lmt_serno='" + lmt_serno + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", assure_means_main='" + assure_means_main + '\'' +
                ", crd_lmt_type='" + crd_lmt_type + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", crd_lmt=" + crd_lmt +
                ", start_date='" + start_date + '\'' +
                ", expi_date='" + expi_date + '\'' +
                ", delay_months=" + delay_months +
                '}';
    }
}