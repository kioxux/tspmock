package cn.com.yusys.yusp.dto.client.esb.core.ln3196;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 账号子序号查询交易，支持查询内部户、负债账户
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/15 11:13
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3196RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//	客户账号		BaseType.U_KEHUZHAO	string	(35)		否
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//	客户号		BaseType.U_KEHUHAOO	string	(20)		否
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//	客户名称		BaseType.U_ZHONGWMC	string	(750)		否
    @JsonProperty(value = "lstioDpCusSysAccListInfo")
    private List<LstioDpCusSysAccListInfo> lstioDpCusSysAccListInfo;

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public List<LstioDpCusSysAccListInfo> getLstioDpCusSysAccListInfo() {
        return lstioDpCusSysAccListInfo;
    }

    public void setLstioDpCusSysAccListInfo(List<LstioDpCusSysAccListInfo> lstioDpCusSysAccListInfo) {
        this.lstioDpCusSysAccListInfo = lstioDpCusSysAccListInfo;
    }

    @Override
    public String toString() {
        return "Ln3196RespDto{" +
                "kehuzhao='" + kehuzhao + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", lstioDpCusSysAccListInfo=" + lstioDpCusSysAccListInfo +
                '}';
    }
}
