package cn.com.yusys.yusp.dto.server.biz.xdcz0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：出账记录详情查看
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isseDate")
    private String isseDate;//出票日期
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "drftTotlQnt")
    private BigDecimal drftTotlQnt;//票据总张数
    @JsonProperty(value = "isseAmt")
    private BigDecimal isseAmt;//出票金额
    @JsonProperty(value = "orgNo")
    private String orgNo;//机构号
    @JsonProperty(value = "chrgRate")
    private BigDecimal chrgRate;//手续费率
    @JsonProperty(value = "chrg")
    private BigDecimal chrg;//手续费
    @JsonProperty(value = "grtType")
    private String grtType;//担保方式
    @JsonProperty(value = "seq")
    private String seq;//批次号
    @JsonProperty(value = "bailAcctNo")
    private String bailAcctNo;//保证金账号
    @JsonProperty(value = "bailPerc")
    private String bailPerc;//保证金比例
    @JsonProperty(value = "bailAmt")
    private BigDecimal bailAmt;//保证金金额
    @JsonProperty(value = "bailAcct")
    private String bailAcct;//保证金账户

    public String getIsseDate() {
        return isseDate;
    }

    public void setIsseDate(String isseDate) {
        this.isseDate = isseDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getDrftTotlQnt() {
        return drftTotlQnt;
    }

    public void setDrftTotlQnt(BigDecimal drftTotlQnt) {
        this.drftTotlQnt = drftTotlQnt;
    }

    public BigDecimal getIsseAmt() {
        return isseAmt;
    }

    public void setIsseAmt(BigDecimal isseAmt) {
        this.isseAmt = isseAmt;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public BigDecimal getChrgRate() {
        return chrgRate;
    }

    public void setChrgRate(BigDecimal chrgRate) {
        this.chrgRate = chrgRate;
    }

    public BigDecimal getChrg() {
        return chrg;
    }

    public void setChrg(BigDecimal chrg) {
        this.chrg = chrg;
    }

    public String getGrtType() {
        return grtType;
    }

    public void setGrtType(String grtType) {
        this.grtType = grtType;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getBailAcctNo() {
        return bailAcctNo;
    }

    public void setBailAcctNo(String bailAcctNo) {
        this.bailAcctNo = bailAcctNo;
    }

    public String getBailPerc() {
        return bailPerc;
    }

    public void setBailPerc(String bailPerc) {
        this.bailPerc = bailPerc;
    }

    public BigDecimal getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(BigDecimal bailAmt) {
        this.bailAmt = bailAmt;
    }

    public String getBailAcct() {
        return bailAcct;
    }

    public void setBailAcct(String bailAcct) {
        this.bailAcct = bailAcct;
    }

    @Override
    public String toString() {
        return "XdczZ0006RespDto{" +
                "isseDate='" + isseDate + '\'' +
                "endDate='" + endDate + '\'' +
                "drftTotlQnt='" + drftTotlQnt + '\'' +
                "isseAmt='" + isseAmt + '\'' +
                "orgNo='" + orgNo + '\'' +
                "chrgRate='" + chrgRate + '\'' +
                "chrg='" + chrg + '\'' +
                "grtType='" + grtType + '\'' +
                "seq='" + seq + '\'' +
                "bailAcctNo='" + bailAcctNo + '\'' +
                "bailPerc='" + bailPerc + '\'' +
                "bailAmt='" + bailAmt + '\'' +
                "bailAcct='" + bailAcct + '\'' +
                '}';
    }
}

