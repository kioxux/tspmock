package cn.com.yusys.yusp.dto.client.esb.rircp.fbyd33;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：预授信申请提交
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbyd33RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "app_no")
    private String app_no;//授信申请流水号

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    @Override
    public String toString() {
        return "Fbyd33RespDto{" +
                "app_no='" + app_no + '\'' +
                '}';
    }
}  
