package cn.com.yusys.yusp.dto.client.esb.zjywxt.sqfyyz.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：行方验证房源信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SqfyyzRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "hlocation ")
    private String hlocation;//房屋坐落
    @JsonProperty(value = "spaccount ")
    private String spaccount;//监管账号

    public String getHlocation() {
        return hlocation;
    }

    public void setHlocation(String hlocation) {
        this.hlocation = hlocation;
    }

    public String getSpaccount() {
        return spaccount;
    }

    public void setSpaccount(String spaccount) {
        this.spaccount = spaccount;
    }

    @Override
    public String toString() {
        return "SqfyyzRespDto{" +
                "hlocation ='" + hlocation + '\'' +
                "spaccount ='" + spaccount + '\'' +
                '}';
    }
}  
