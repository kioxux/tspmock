package cn.com.yusys.yusp.dto.client.esb.ecif.g11004;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应DTO：客户集团信息查询（new）接口
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 21:24
 */
@JsonPropertyOrder(alphabetic = true)
public class G11004RespDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "grouno")
    private String grouno; // 集团编号
    @JsonProperty(value = "grouna")
    private String grouna; // 集团客户名称
    @JsonProperty(value = "custno")
    private String custno; // 客户编号
    @JsonProperty(value = "custln")
    private String custln; // 客户中证码
    @JsonProperty(value = "custna")
    private String custna; // 客户名称
    @JsonProperty(value = "idtfno")
    private String idtfno; // 证件号码
    @JsonProperty(value = "oadate")
    private String oadate; // 更新办公地址日期
    @JsonProperty(value = "homead")
    private String homead; // 地址
    @JsonProperty(value = "socino")
    private String socino; // 社会信用代码
    @JsonProperty(value = "busino")
    private String busino; // 工商登记注册号
    @JsonProperty(value = "ofaddr")
    private String ofaddr; // 办公地址行政区划
    @JsonProperty(value = "crpdeg")
    private String crpdeg; // 集团紧密程度
    @JsonProperty(value = "crpcno")
    private String crpcno; // 集团客户情况说明
    @JsonProperty(value = "ctigno")
    private String ctigno; // 管户客户经理
    @JsonProperty(value = "citorg")
    private String citorg; // 所属机构
    @JsonProperty(value = "grpsta")
    private String grpsta; // 集团客户状态
    @JsonProperty(value = "oprtye")
    private String oprtye; // 操作类型
    @JsonProperty(value = "inptno")
    private String inptno; // 登记人
    @JsonProperty(value = "inporg")
    private String inporg; // 登记机构
    @JsonProperty(value = "CIRCLE_ARRAY_MEM")
    private List<CircleArrayMem> circleArrayMem;// 集团成员信息_ARRAY

    public String getGrouno() {
        return grouno;
    }

    public void setGrouno(String grouno) {
        this.grouno = grouno;
    }

    public String getGrouna() {
        return grouna;
    }

    public void setGrouna(String grouna) {
        this.grouna = grouna;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustln() {
        return custln;
    }

    public void setCustln(String custln) {
        this.custln = custln;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getOadate() {
        return oadate;
    }

    public void setOadate(String oadate) {
        this.oadate = oadate;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getSocino() {
        return socino;
    }

    public void setSocino(String socino) {
        this.socino = socino;
    }

    public String getBusino() {
        return busino;
    }

    public void setBusino(String busino) {
        this.busino = busino;
    }

    public String getOfaddr() {
        return ofaddr;
    }

    public void setOfaddr(String ofaddr) {
        this.ofaddr = ofaddr;
    }

    public String getCrpdeg() {
        return crpdeg;
    }

    public void setCrpdeg(String crpdeg) {
        this.crpdeg = crpdeg;
    }

    public String getCrpcno() {
        return crpcno;
    }

    public void setCrpcno(String crpcno) {
        this.crpcno = crpcno;
    }

    public String getCtigno() {
        return ctigno;
    }

    public void setCtigno(String ctigno) {
        this.ctigno = ctigno;
    }

    public String getCitorg() {
        return citorg;
    }

    public void setCitorg(String citorg) {
        this.citorg = citorg;
    }

    public String getGrpsta() {
        return grpsta;
    }

    public void setGrpsta(String grpsta) {
        this.grpsta = grpsta;
    }

    public String getOprtye() {
        return oprtye;
    }

    public void setOprtye(String oprtye) {
        this.oprtye = oprtye;
    }

    public String getInptno() {
        return inptno;
    }

    public void setInptno(String inptno) {
        this.inptno = inptno;
    }

    public String getInporg() {
        return inporg;
    }

    public void setInporg(String inporg) {
        this.inporg = inporg;
    }

    public List<CircleArrayMem> getCircleArrayMem() {
        return circleArrayMem;
    }

    public void setCircleArrayMem(List<CircleArrayMem> circleArrayMem) {
        this.circleArrayMem = circleArrayMem;
    }

    @Override
    public String toString() {
        return "G11004RespDto{" +
                "grouno='" + grouno + '\'' +
                ", grouna='" + grouna + '\'' +
                ", custno='" + custno + '\'' +
                ", custln='" + custln + '\'' +
                ", custna='" + custna + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", oadate='" + oadate + '\'' +
                ", homead='" + homead + '\'' +
                ", socino='" + socino + '\'' +
                ", busino='" + busino + '\'' +
                ", ofaddr='" + ofaddr + '\'' +
                ", crpdeg='" + crpdeg + '\'' +
                ", crpcno='" + crpcno + '\'' +
                ", ctigno='" + ctigno + '\'' +
                ", citorg='" + citorg + '\'' +
                ", grpsta='" + grpsta + '\'' +
                ", oprtye='" + oprtye + '\'' +
                ", inptno='" + inptno + '\'' +
                ", inporg='" + inporg + '\'' +
                ", circleArrayMem=" + circleArrayMem +
                '}';
    }
}
