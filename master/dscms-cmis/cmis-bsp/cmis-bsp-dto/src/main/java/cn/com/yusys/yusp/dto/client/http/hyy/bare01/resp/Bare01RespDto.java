package cn.com.yusys.yusp.dto.client.http.hyy.bare01.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：押品状态查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Bare01RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.client.http.hyy.bare01.resp.Data data;
    @JsonProperty(value = "code")
    private String code;
    @JsonProperty(value = "message")
    private String message;
    @JsonProperty(value = "success")
    private String success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "Bare01RespDto{" +
                "data=" + data +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", success='" + success + '\'' +
                '}';
    }
}
