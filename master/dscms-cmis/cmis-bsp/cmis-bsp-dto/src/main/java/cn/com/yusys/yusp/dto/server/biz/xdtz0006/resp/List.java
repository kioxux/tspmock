package cn.com.yusys.yusp.dto.server.biz.xdtz0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：根据证件号查询借据信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "billBal")
    private BigDecimal billBal;//借据余额
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "cnAssureMeans")
    private String cnAssureMeans;//担保方式中文
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行利率（%）
    @JsonProperty(value = "startDate")
    private String startDate;//起始日期
    @JsonProperty(value = "endDate")
    private String endDate;//截至日期
    @JsonProperty(value = "billStatus")
    private String billStatus;//借据状态

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCnAssureMeans() {
        return cnAssureMeans;
    }

    public void setCnAssureMeans(String cnAssureMeans) {
        this.cnAssureMeans = cnAssureMeans;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    @Override
    public String toString() {
        return "List{" +
                "billNo='" + billNo + '\'' +
                ", billBal=" + billBal +
                ", prdName='" + prdName + '\'' +
                ", cnAssureMeans='" + cnAssureMeans + '\'' +
                ", realityIrY=" + realityIrY +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", billStatus='" + billStatus + '\'' +
                '}';
    }
}
