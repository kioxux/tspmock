package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：数据修改接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp07ReqDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "suvery_serno")
	private String suvery_serno;//调查流水号
	@JsonProperty(value = "loan_type")
	private String loan_type;//业务类型
	@JsonProperty(value = "cus_mgr_no")
	private String cus_mgr_no;//客户经理工号
	@JsonProperty(value = "spouse_name")
	private String spouse_name;//配偶名称
	@JsonProperty(value = "is_whbxd")
	private String is_whbxd;//是否无还本续贷
	@JsonProperty(value = "guar_list")
	private java.util.List<Guar_list> guar_list;
	@JsonProperty(value = "plage_list")
	private java.util.List<Plage_list> plage_list;

	public String getSuvery_serno() {
		return suvery_serno;
	}

	public void setSuvery_serno(String suvery_serno) {
		this.suvery_serno = suvery_serno;
	}

	public String getLoan_type() {
		return loan_type;
	}

	public void setLoan_type(String loan_type) {
		this.loan_type = loan_type;
	}

	public String getCus_mgr_no() {
		return cus_mgr_no;
	}

	public void setCus_mgr_no(String cus_mgr_no) {
		this.cus_mgr_no = cus_mgr_no;
	}

	public String getSpouse_name() {
		return spouse_name;
	}

	public void setSpouse_name(String spouse_name) {
		this.spouse_name = spouse_name;
	}

	public String getIs_whbxd() {
		return is_whbxd;
	}

	public void setIs_whbxd(String is_whbxd) {
		this.is_whbxd = is_whbxd;
	}

	public List<Guar_list> getGuar_list() {
		return guar_list;
	}

	public void setGuar_list(List<Guar_list> guar_list) {
		this.guar_list = guar_list;
	}

	public List<Plage_list> getPlage_list() {
		return plage_list;
	}

	public void setPlage_list(List<Plage_list> plage_list) {
		this.plage_list = plage_list;
	}

	@Override
	public String toString() {
		return "Znsp07ReqDto{" +
				"suvery_serno='" + suvery_serno + '\'' +
				", loan_type='" + loan_type + '\'' +
				", cus_mgr_no='" + cus_mgr_no + '\'' +
				", spouse_name='" + spouse_name + '\'' +
				", is_whbxd='" + is_whbxd + '\'' +
				", guar_list=" + guar_list +
				", plage_list=" + plage_list +
				'}';
	}
}
