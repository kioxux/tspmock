package cn.com.yusys.yusp.dto.server.lmt.xded0058.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class RecoverListDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;

    @JsonProperty(value = "recoverAmtCny")
    private BigDecimal recoverAmtCny;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public BigDecimal getRecoverAmtCny() {
        return recoverAmtCny;
    }

    public void setRecoverAmtCny(BigDecimal recoverAmtCny) {
        this.recoverAmtCny = recoverAmtCny;
    }

    @Override
    public String toString() {
        return "RecoverListDto{" +
                "cusId='" + cusId + '\'' +
                ", recoverAmtCny=" + recoverAmtCny +
                '}';
    }
}
