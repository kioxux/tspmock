package cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：押品处置信息同步
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class DealsyRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}  
