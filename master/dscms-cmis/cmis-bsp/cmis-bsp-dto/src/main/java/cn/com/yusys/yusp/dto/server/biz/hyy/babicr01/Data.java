package cn.com.yusys.yusp.dto.server.biz.hyy.babicr01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：获取抵押信息详情
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "items")
    private List<Boolean> items;

    public List<Boolean> getItems() {
        return items;
    }

    public void setItems(List<Boolean> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Data{" +
                "items=" + items +
                '}';
    }
}
