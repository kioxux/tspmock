package cn.com.yusys.yusp.dto.server.biz.xdtz0055.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/5/22 10:47:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 10:47
 * @since 2021/5/22 10:47
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "list")
    private java.util.List list;//列表

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "list=" + list +
                '}';
    }
}
