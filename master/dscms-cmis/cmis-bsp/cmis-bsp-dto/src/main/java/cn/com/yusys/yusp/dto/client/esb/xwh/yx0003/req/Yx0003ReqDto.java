package cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：市民贷优惠券核销锁定
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Yx0003ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cardSecret")
    private String cardSecret;//唯一码
    @JsonProperty(value = "phone")
    private String phone;//手机号码
    @JsonProperty(value = "loanContNo")
    private String loanContNo;//合同号
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "status")
    private String status;//状态1：锁定 2：核销（当签订合同时传1；当生成借据号时传2；）

    public String getCardSecret() {
        return cardSecret;
    }

    public void setCardSecret(String cardSecret) {
        this.cardSecret = cardSecret;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Yx0003ReqDto{" +
                "cardSecret='" + cardSecret + '\'' +
                ", phone='" + phone + '\'' +
                ", loanContNo='" + loanContNo + '\'' +
                ", billNo='" + billNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
