package cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 14:27
 * @since 2021/8/10 14:27
 */
@JsonPropertyOrder(alphabetic = true)
public class Dylist implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guaranty_id")
    private String guaranty_id;//抵押物编号
    @JsonProperty(value = "guaranty_type")
    private String guaranty_type;//抵押物类型
    @JsonProperty(value = "guaranty_name")
    private String guaranty_name;//抵押物名称
    @JsonProperty(value = "guaranty_amt")
    private String guaranty_amt;//评估金额
    @JsonProperty(value = "guaranty_state")
    private String guaranty_state;//出入库状态

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    public String getGuaranty_type() {
        return guaranty_type;
    }

    public void setGuaranty_type(String guaranty_type) {
        this.guaranty_type = guaranty_type;
    }

    public String getGuaranty_name() {
        return guaranty_name;
    }

    public void setGuaranty_name(String guaranty_name) {
        this.guaranty_name = guaranty_name;
    }

    public String getGuaranty_amt() {
        return guaranty_amt;
    }

    public void setGuaranty_amt(String guaranty_amt) {
        this.guaranty_amt = guaranty_amt;
    }

    public String getGuaranty_state() {
        return guaranty_state;
    }

    public void setGuaranty_state(String guaranty_state) {
        this.guaranty_state = guaranty_state;
    }

    @Override
    public String toString() {
        return "Dylist{" +
                "guaranty_id='" + guaranty_id + '\'' +
                ", guaranty_type='" + guaranty_type + '\'' +
                ", guaranty_name='" + guaranty_name + '\'' +
                ", guaranty_amt='" + guaranty_amt + '\'' +
                ", guaranty_state='" + guaranty_state + '\'' +
                '}';
    }
}
