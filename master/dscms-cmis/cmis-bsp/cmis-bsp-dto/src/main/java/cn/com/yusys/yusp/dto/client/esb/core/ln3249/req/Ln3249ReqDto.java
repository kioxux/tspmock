package cn.com.yusys.yusp.dto.client.esb.core.ln3249.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款指定日期利息试算
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3249ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "qishiriq")
    private String qishiriq;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getQishiriq() {
        return qishiriq;
    }

    public void setQishiriq(String qishiriq) {
        this.qishiriq = qishiriq;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    @Override
    public String toString() {
        return "Ln3249ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "qishiriq='" + qishiriq + '\'' +
                "zhzhriqi='" + zhzhriqi + '\'' +
                '}';
    }
}  
