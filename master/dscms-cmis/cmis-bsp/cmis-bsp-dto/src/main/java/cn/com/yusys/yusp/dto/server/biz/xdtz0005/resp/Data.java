package cn.com.yusys.yusp.dto.server.biz.xdtz0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据流水号查询是否放款标记
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "isDisb")
    private String isDisb;//是否放款

    public String getIsDisb() {
        return isDisb;
    }

    public void setIsDisb(String isDisb) {
        this.isDisb = isDisb;
    }

    @Override
    public String toString() {
        return "Data{" +
                "isDisb='" + isDisb + '\'' +
                '}';
    }
}
