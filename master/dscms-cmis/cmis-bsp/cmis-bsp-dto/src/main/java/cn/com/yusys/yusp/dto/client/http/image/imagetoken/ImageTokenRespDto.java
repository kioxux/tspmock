package cn.com.yusys.yusp.dto.client.http.image.imagetoken;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 响应DTO：通过用户名和密码获取token
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class ImageTokenRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "resultCode")
    private String resultCode;//返回码
    @JsonProperty(value = "resultMessage")
    private String resultMessage;//返回信息
    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.client.http.image.imagetoken.Data data;//返回数据

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ImageTokenRespDto{" +
                "resultCode='" + resultCode + '\'' +
                ", resultMessage='" + resultMessage + '\'' +
                ", data=" + data +
                '}';
    }
}
