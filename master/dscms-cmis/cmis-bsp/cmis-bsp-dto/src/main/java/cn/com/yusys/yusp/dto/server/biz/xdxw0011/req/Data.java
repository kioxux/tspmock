package cn.com.yusys.yusp.dto.server.biz.xdxw0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：提交勘验信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//业务编号
    @JsonProperty(value = "status")
    private String status;//状态
    @JsonProperty(value = "videoNo")
    private String videoNo;//视频编号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVideoNo() {
        return videoNo;
    }

    public void setVideoNo(String videoNo) {
        this.videoNo = videoNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "serno='" + serno + '\'' +
                ", status='" + status + '\'' +
                ", videoNo='" + videoNo + '\'' +
                '}';
    }
}
