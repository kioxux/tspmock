package cn.com.yusys.yusp.dto.client.esb.core.da3304.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：资产转让借据筛选
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3304RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yewubhao")
    private String yewubhao;//业务编号
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "zhchjgou")
    private String zhchjgou;//转出机构
    @JsonProperty(value = "zhrujgou")
    private String zhrujgou;//转入机构
    @JsonProperty(value = "jychgbzh")
    private String jychgbzh;//交易是否成功
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水

    public String getYewubhao() {
        return yewubhao;
    }

    public void setYewubhao(String yewubhao) {
        this.yewubhao = yewubhao;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getZhchjgou() {
        return zhchjgou;
    }

    public void setZhchjgou(String zhchjgou) {
        this.zhchjgou = zhchjgou;
    }

    public String getZhrujgou() {
        return zhrujgou;
    }

    public void setZhrujgou(String zhrujgou) {
        this.zhrujgou = zhrujgou;
    }

    public String getJychgbzh() {
        return jychgbzh;
    }

    public void setJychgbzh(String jychgbzh) {
        this.jychgbzh = jychgbzh;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    @Override
    public String toString() {
        return "Da3304RespDto{" +
                "yewubhao='" + yewubhao + '\'' +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "zhchjgou='" + zhchjgou + '\'' +
                "zhrujgou='" + zhrujgou + '\'' +
                "jychgbzh='" + jychgbzh + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                '}';
    }
}  
