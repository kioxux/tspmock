package cn.com.yusys.yusp.dto.server.oca.xdxt0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 16:12:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 16:12
 * @since 2021/5/24 16:12
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "actorName")
    private String actorName;//客户经理姓名
    @JsonProperty(value = "dutyNo")
    private String dutyNo;//岗位编号
    @JsonProperty(value = "actorNo")
    private String actorNo;//客户经理工号
    @JsonProperty(value = "dutyName")
    private String dutyName;//岗位名称

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getDutyNo() {
        return dutyNo;
    }

    public void setDutyNo(String dutyNo) {
        this.dutyNo = dutyNo;
    }

    public String getActorNo() {
        return actorNo;
    }

    public void setActorNo(String actorNo) {
        this.actorNo = actorNo;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    @Override
    public String toString() {
        return "List{" +
                "actorName='" + actorName + '\'' +
                ", dutyNo='" + dutyNo + '\'' +
                ", actorNo='" + actorNo + '\'' +
                ", dutyName='" + dutyName + '\'' +
                '}';
    }
}
