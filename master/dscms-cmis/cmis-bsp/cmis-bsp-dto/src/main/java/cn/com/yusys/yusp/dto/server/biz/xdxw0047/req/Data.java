package cn.com.yusys.yusp.dto.server.biz.xdxw0047.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：增享贷2.0风控模型A任务推送
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    @JsonProperty(value = "reqtype")
    private String reqtype;//请求类型
    @JsonProperty(value = "task_id")
    private String task_id;//办理流水号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "model_amount")
    private BigDecimal model_amount;//模型建议金额
    @JsonProperty(value = "model_rate")
    private BigDecimal model_rate;//模型建议利率

    public String getReqtype() {
        return reqtype;
    }

    public void setReqtype(String reqtype) {
        this.reqtype = reqtype;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public BigDecimal getModel_amount() {
        return model_amount;
    }

    public void setModel_amount(BigDecimal model_amount) {
        this.model_amount = model_amount;
    }

    public BigDecimal getModel_rate() {
        return model_rate;
    }

    public void setModel_rate(BigDecimal model_rate) {
        this.model_rate = model_rate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "reqtype='" + reqtype + '\'' +
                "task_id='" + task_id + '\'' +
                "cus_id='" + cus_id + '\'' +
                "model_amount='" + model_amount + '\'' +
                "model_rate='" + model_rate + '\'' +
                '}';
    }
}
