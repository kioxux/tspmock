package cn.com.yusys.yusp.dto.server.biz.xdxw0080.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：优惠利率申请结果通知
 * @author xll
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0080RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private cn.com.yusys.yusp.dto.server.biz.xdxw0080.resp.Data data;

	public cn.com.yusys.yusp.dto.server.biz.xdxw0080.resp.Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdxw0080RespDto{" +
				"data=" + data +
				'}';
	}
}  
