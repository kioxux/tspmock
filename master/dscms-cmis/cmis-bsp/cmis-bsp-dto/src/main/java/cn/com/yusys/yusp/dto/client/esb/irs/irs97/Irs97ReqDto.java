package cn.com.yusys.yusp.dto.client.esb.irs.irs97;

import cn.com.yusys.yusp.dto.client.esb.irs.common.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求DTO：新增授信时债项评级
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日 上午11:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs97ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "userid")
    private String userid;//请求发起人编号
    @JsonProperty(value = "username")
    private String username;//	请求发起人名称
    @JsonProperty(value = "brchno")
    private String brchno;//请求机构编号
    @JsonProperty(value = "brchnm")
    private String brchnm;//请求机构名称
    @JsonProperty(value = "LimitApplyInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo> LimitApplyInfo; // 综合授信申请信息
    @JsonProperty(value = "LimitDetailsInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo> LimitDetailsInfo; // 分项额度信息
    @JsonProperty(value = "LimitProductInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo> LimitProductInfo; // 产品额度信息
    @JsonProperty(value = "BusinessContractInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessContractInfo> BusinessContractInfo; // 合同信息
    @JsonProperty(value = "GuaranteeContrctInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteeContrctInfo> GuaranteeContrctInfo; // 担保合同信息 GuaranteeContrctInfo
    @JsonProperty(value = "AccLoanInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.AccLoanInfo> AccLoanInfo; // 担保合同信息
    @JsonProperty(value = "CustomerInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo> CustomerInfo; // 客户信息
    @JsonProperty(value = "LimitPleMortInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo> LimitPleMortInfo; //授信分项额度与抵质押、保证人关系信息
    @JsonProperty(value = "PledgeInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo> PledgeInfo; // 质押物信息
    @JsonProperty(value = "MortgageInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo> MortgageInfo; // 抵押物信息
    @JsonProperty(value = "AssurePersonInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo> AssurePersonInfo; // 保证人信息
    @JsonProperty(value = "CurInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo> CurInfo; // 汇率信息
    @JsonProperty(value = "AssureAccInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssureAccInfo> AssureAccInfo; // 保证金信息 AssureAccInfo
    @JsonProperty(value = "UpApplyInfoUpApplyInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.UpApplyInfo> UpApplyInfo; // 最高额授信协议信息
    @JsonProperty(value = "BusinessAssureInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessAssureInfo> BusinessAssureInfo; // 最高额授信协议信息
    @JsonProperty(value = "GuaranteePleMortInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteePleMortInfo> GuaranteePleMortInfo; // 担保合同与抵质押、保证人关联信息


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getBrchnm() {
        return brchnm;
    }

    public void setBrchnm(String brchnm) {
        this.brchnm = brchnm;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo> getLimitApplyInfo() {
        return LimitApplyInfo;
    }

    public void setLimitApplyInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo> limitApplyInfo) {
        LimitApplyInfo = limitApplyInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo> getLimitDetailsInfo() {
        return LimitDetailsInfo;
    }

    public void setLimitDetailsInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo> limitDetailsInfo) {
        LimitDetailsInfo = limitDetailsInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo> getLimitProductInfo() {
        return LimitProductInfo;
    }

    public void setLimitProductInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo> limitProductInfo) {
        LimitProductInfo = limitProductInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo> getCustomerInfo() {
        return CustomerInfo;
    }

    public void setCustomerInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo> customerInfo) {
        CustomerInfo = customerInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo> getLimitPleMortInfo() {
        return LimitPleMortInfo;
    }

    public void setLimitPleMortInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo> limitPleMortInfo) {
        LimitPleMortInfo = limitPleMortInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo> getPledgeInfo() {
        return PledgeInfo;
    }

    public void setPledgeInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo> pledgeInfo) {
        PledgeInfo = pledgeInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo> getMortgageInfo() {
        return MortgageInfo;
    }

    public void setMortgageInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo> mortgageInfo) {
        MortgageInfo = mortgageInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo> getAssurePersonInfo() {
        return AssurePersonInfo;
    }

    public void setAssurePersonInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo> assurePersonInfo) {
        AssurePersonInfo = assurePersonInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo> getCurInfo() {
        return CurInfo;
    }

    public void setCurInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo> curInfo) {
        CurInfo = curInfo;
    }


    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessContractInfo> getBusinessContractInfo() {
        return BusinessContractInfo;
    }

    public void setBusinessContractInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessContractInfo> businessContractInfo) {
        BusinessContractInfo = businessContractInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.AccLoanInfo> getAccLoanInfo() {
        return AccLoanInfo;
    }

    public void setAccLoanInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.AccLoanInfo> accLoanInfo) {
        AccLoanInfo = accLoanInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteeContrctInfo> getGuaranteeContrctInfo() {
        return GuaranteeContrctInfo;
    }

    public void setGuaranteeContrctInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteeContrctInfo> guaranteeContrctInfo) {
        GuaranteeContrctInfo = guaranteeContrctInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssureAccInfo> getAssureAccInfo() {
        return AssureAccInfo;
    }

    public void setAssureAccInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssureAccInfo> assureAccInfo) {
        AssureAccInfo = assureAccInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.UpApplyInfo> getUpApplyInfo() {
        return UpApplyInfo;
    }

    public void setUpApplyInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.UpApplyInfo> upApplyInfo) {
        UpApplyInfo = upApplyInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessAssureInfo> getBusinessAssureInfo() {
        return BusinessAssureInfo;
    }

    public void setBusinessAssureInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessAssureInfo> businessAssureInfo) {
        BusinessAssureInfo = businessAssureInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteePleMortInfo> getGuaranteePleMortInfo() {
        return GuaranteePleMortInfo;
    }

    public void setGuaranteePleMortInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteePleMortInfo> guaranteePleMortInfo) {
        GuaranteePleMortInfo = guaranteePleMortInfo;
    }

    @Override
    public String toString() {
        return "Irs97ReqDto{" +
                "userid='" + userid + '\'' +
                ", username='" + username + '\'' +
                ", brchno='" + brchno + '\'' +
                ", brchnm='" + brchnm + '\'' +
                ", LimitApplyInfo=" + LimitApplyInfo +
                ", LimitDetailsInfo=" + LimitDetailsInfo +
                ", LimitProductInfo=" + LimitProductInfo +
                ", CustomerInfo=" + CustomerInfo +
                ", LimitPleMortInfo=" + LimitPleMortInfo +
                ", PledgeInfo=" + PledgeInfo +
                ", MortgageInfo=" + MortgageInfo +
                ", AssurePersonInfo=" + AssurePersonInfo +
                ", CurInfo=" + CurInfo +
                '}';
    }
}
