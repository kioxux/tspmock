package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd09;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询日初（合约）信息历史表（利翃）总数据量
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd09RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lh_loan_init_num")
    private Integer lh_loan_init_num;//日初（合约）信息历史表（利翃）总数据量

    public Integer getLh_loan_init_num() {
        return lh_loan_init_num;
    }

    public void setLh_loan_init_num(Integer lh_loan_init_num) {
        this.lh_loan_init_num = lh_loan_init_num;
    }

    @Override
    public String toString() {
        return "Fbxd09RespDto{" +
                "lh_loan_init_num='" + lh_loan_init_num + '\'' +
                '}';
    }
}  
