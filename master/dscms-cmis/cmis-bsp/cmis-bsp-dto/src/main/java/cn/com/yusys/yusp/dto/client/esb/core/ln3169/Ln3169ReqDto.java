package cn.com.yusys.yusp.dto.client.esb.core.ln3169;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：资产证券化处理文件导入
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3169ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zichculi")
    private String zichculi;//资产处理类型
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "shuhuirq")
    private String shuhuirq;//赎回日期
    @JsonProperty(value = "fengbriq")
    private String fengbriq;//封包日期
    @JsonProperty(value = "huigriqi")
    private String huigriqi;//回购日期
    @JsonProperty(value = "jiaoyije")
    private BigDecimal jiaoyije;//支付金额
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//删除笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//新增笔数
    @JsonProperty(value = "plclmich")
    private String plclmich;//批量处理文件名

    public String getZichculi() {
        return zichculi;
    }

    public void setZichculi(String zichculi) {
        this.zichculi = zichculi;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getShuhuirq() {
        return shuhuirq;
    }

    public void setShuhuirq(String shuhuirq) {
        this.shuhuirq = shuhuirq;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getPlclmich() {
        return plclmich;
    }

    public void setPlclmich(String plclmich) {
        this.plclmich = plclmich;
    }

    @Override
    public String toString() {
        return "Ln3169ReqDto{" +
                "zichculi='" + zichculi + '\'' +
                "xieybhao='" + xieybhao + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "shuhuirq='" + shuhuirq + '\'' +
                "fengbriq='" + fengbriq + '\'' +
                "huigriqi='" + huigriqi + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "zongbish='" + zongbish + '\'' +
                "qishibis='" + qishibis + '\'' +
                "chxunbis='" + chxunbis + '\'' +
                "plclmich='" + plclmich + '\'' +
                '}';
    }
}  
