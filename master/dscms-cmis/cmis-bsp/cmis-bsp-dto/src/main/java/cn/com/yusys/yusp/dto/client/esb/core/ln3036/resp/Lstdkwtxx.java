package cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款多委托人账户
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkwtxx implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bhchzibz")
    private String bhchzibz;//本行出资标志
    @JsonProperty(value = "bhjiejuh")
    private String bhjiejuh;//本行借据号
    @JsonProperty(value = "bhzhaobz")
    private String bhzhaobz;//本行账号标志
    @JsonProperty(value = "zjingjbz")
    private String zjingjbz;//资金归集标志
    @JsonProperty(value = "hkzjhzfs")
    private String hkzjhzfs;//还款资金划转方式
    @JsonProperty(value = "zhkaihhh")
    private String zhkaihhh;//账户开户行行号
    @JsonProperty(value = "zhkaihhm")
    private String zhkaihhm;//账户开户行行名
    @JsonProperty(value = "wtrkehuh")
    private String wtrkehuh;//委托人客户号
    @JsonProperty(value = "wtrckuzh")
    private String wtrckuzh;//委托人存款账号
    @JsonProperty(value = "wtrckzxh")
    private String wtrckzxh;//委托人存款账号子序号
    @JsonProperty(value = "wtckzhao")
    private String wtckzhao;//委托存款账号
    @JsonProperty(value = "wtckzixh")
    private String wtckzixh;//委托存款账号子序号
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号
    @JsonProperty(value = "bjghrzxh")
    private String bjghrzxh;//本金归还入账账号子序号
    @JsonProperty(value = "lxghrzxh")
    private String lxghrzxh;//利息归还入账账号子序号
    @JsonProperty(value = "weituoje")
    private BigDecimal weituoje;//委托金额
    @JsonProperty(value = "wtrmingc")
    private String wtrmingc;//委托人名称

    public String getBhchzibz() {
        return bhchzibz;
    }

    public void setBhchzibz(String bhchzibz) {
        this.bhchzibz = bhchzibz;
    }

    public String getBhjiejuh() {
        return bhjiejuh;
    }

    public void setBhjiejuh(String bhjiejuh) {
        this.bhjiejuh = bhjiejuh;
    }

    public String getBhzhaobz() {
        return bhzhaobz;
    }

    public void setBhzhaobz(String bhzhaobz) {
        this.bhzhaobz = bhzhaobz;
    }

    public String getZjingjbz() {
        return zjingjbz;
    }

    public void setZjingjbz(String zjingjbz) {
        this.zjingjbz = zjingjbz;
    }

    public String getHkzjhzfs() {
        return hkzjhzfs;
    }

    public void setHkzjhzfs(String hkzjhzfs) {
        this.hkzjhzfs = hkzjhzfs;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public String getWtrkehuh() {
        return wtrkehuh;
    }

    public void setWtrkehuh(String wtrkehuh) {
        this.wtrkehuh = wtrkehuh;
    }

    public String getWtrckuzh() {
        return wtrckuzh;
    }

    public void setWtrckuzh(String wtrckuzh) {
        this.wtrckuzh = wtrckuzh;
    }

    public String getWtrckzxh() {
        return wtrckzxh;
    }

    public void setWtrckzxh(String wtrckzxh) {
        this.wtrckzxh = wtrckzxh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public BigDecimal getWeituoje() {
        return weituoje;
    }

    public void setWeituoje(BigDecimal weituoje) {
        this.weituoje = weituoje;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    @Override
    public String toString() {
        return "Lstdkwtxx{" +
                "bhchzibz='" + bhchzibz + '\'' +
                ", bhjiejuh='" + bhjiejuh + '\'' +
                ", bhzhaobz='" + bhzhaobz + '\'' +
                ", zjingjbz='" + zjingjbz + '\'' +
                ", hkzjhzfs='" + hkzjhzfs + '\'' +
                ", zhkaihhh='" + zhkaihhh + '\'' +
                ", zhkaihhm='" + zhkaihhm + '\'' +
                ", wtrkehuh='" + wtrkehuh + '\'' +
                ", wtrckuzh='" + wtrckuzh + '\'' +
                ", wtrckzxh='" + wtrckzxh + '\'' +
                ", wtckzhao='" + wtckzhao + '\'' +
                ", wtckzixh='" + wtckzixh + '\'' +
                ", bjghrzzh='" + bjghrzzh + '\'' +
                ", lxghrzzh='" + lxghrzzh + '\'' +
                ", bjghrzxh='" + bjghrzxh + '\'' +
                ", lxghrzxh='" + lxghrzxh + '\'' +
                ", weituoje=" + weituoje +
                ", wtrmingc='" + wtrmingc + '\'' +
                '}';
    }
}
