package cn.com.yusys.yusp.dto.client.esb.core.ln3078;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：贷款机构变更
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3078ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "yewubhao")
    private String yewubhao;//业务编号
    @JsonProperty(value = "zhrujgou")
    private String zhrujgou;//转入机构
    @JsonProperty(value = "zhchjgou")
    private String zhchjgou;//转出机构
    @JsonProperty(value = "dkgljgou")
    private String dkgljgou;//贷款管理机构
    @JsonProperty(value = "jgzhyibz")
    private String jgzhyibz;//机构转移标志
    @JsonProperty(value = "shifoudy")
    private String shifoudy;//是否打印
    @JsonProperty(value = "lstjiejxx")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3078.Lstjiejxx> lstjiejxx;//借据信息[LIST]

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getYewubhao() {
        return yewubhao;
    }

    public void setYewubhao(String yewubhao) {
        this.yewubhao = yewubhao;
    }

    public String getZhrujgou() {
        return zhrujgou;
    }

    public void setZhrujgou(String zhrujgou) {
        this.zhrujgou = zhrujgou;
    }

    public String getZhchjgou() {
        return zhchjgou;
    }

    public void setZhchjgou(String zhchjgou) {
        this.zhchjgou = zhchjgou;
    }

    public String getDkgljgou() {
        return dkgljgou;
    }

    public void setDkgljgou(String dkgljgou) {
        this.dkgljgou = dkgljgou;
    }

    public String getJgzhyibz() {
        return jgzhyibz;
    }

    public void setJgzhyibz(String jgzhyibz) {
        this.jgzhyibz = jgzhyibz;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    public List<Lstjiejxx> getLstjiejxx() {
        return lstjiejxx;
    }

    public void setLstjiejxx(List<Lstjiejxx> lstjiejxx) {
        this.lstjiejxx = lstjiejxx;
    }

    @Override
    public String toString() {
        return "Ln3078ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                ", yewubhao='" + yewubhao + '\'' +
                ", zhrujgou='" + zhrujgou + '\'' +
                ", zhchjgou='" + zhchjgou + '\'' +
                ", dkgljgou='" + dkgljgou + '\'' +
                ", jgzhyibz='" + jgzhyibz + '\'' +
                ", shifoudy='" + shifoudy + '\'' +
                ", lstjiejxx=" + lstjiejxx +
                '}';
    }
}
