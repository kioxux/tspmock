package cn.com.yusys.yusp.dto.server.biz.xdtz0009.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询客户经理不良率
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "badRate")
    private BigDecimal badRate;//不良率

    public BigDecimal getBadRate() {
        return badRate;
    }

    public void setBadRate(BigDecimal badRate) {
        this.badRate = badRate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "badRate=" + badRate +
                '}';
    }
}
