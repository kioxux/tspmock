package cn.com.yusys.yusp.dto.server.biz.xddh0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/22 14:33:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 14:33
 * @since 2021/5/22 14:33
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "onTheWayAmt")
    private BigDecimal onTheWayAmt;//在途金额
    @JsonProperty(value = "isOnTheWay")
    private String isOnTheWay;//是否在途
    @JsonProperty(value = "onTheWayContNo")
    private String onTheWayContNo;//在途合同号

    public BigDecimal getOnTheWayAmt() {
        return onTheWayAmt;
    }

    public void setOnTheWayAmt(BigDecimal onTheWayAmt) {
        this.onTheWayAmt = onTheWayAmt;
    }

    public String getIsOnTheWay() {
        return isOnTheWay;
    }

    public void setIsOnTheWay(String isOnTheWay) {
        this.isOnTheWay = isOnTheWay;
    }

    public String getOnTheWayContNo() {
        return onTheWayContNo;
    }

    public void setOnTheWayContNo(String onTheWayContNo) {
        this.onTheWayContNo = onTheWayContNo;
    }

    @Override
    public String toString() {
        return "Xddh0004RespDto{" +
                "onTheWayAmt='" + onTheWayAmt + '\'' +
                "isOnTheWay='" + isOnTheWay + '\'' +
                "onTheWayContNo='" + onTheWayContNo + '\'' +
                '}';
    }
}
