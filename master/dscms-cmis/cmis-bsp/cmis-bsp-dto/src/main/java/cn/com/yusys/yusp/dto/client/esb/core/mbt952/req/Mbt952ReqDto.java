package cn.com.yusys.yusp.dto.client.esb.core.mbt952.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：批量结果确认处理
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Mbt952ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "jysjzifu")
    private String jysjzifu;//交易时间字符串
    @JsonProperty(value = "picriqii")
    private String picriqii;//批次日期
    @JsonProperty(value = "picihaoo")
    private String picihaoo;//批次号
    @JsonProperty(value = "picileib")
    private String picileib;//批次类别
    @JsonProperty(value = "fuwudaim")
    private String fuwudaim;//服务代码
    @JsonProperty(value = "ypiciriq")
    private String ypiciriq;//原批次日期
    @JsonProperty(value = "ypicihao")
    private String ypicihao;//原批次号
    @JsonProperty(value = "weituoha")
    private String weituoha;//委托号
    @JsonProperty(value = "xieyihao")
    private String xieyihao;//合同号/协议号
    @JsonProperty(value = "qianyzhh")
    private String qianyzhh;//签约账号
    @JsonProperty(value = "zhixztai")
    private String zhixztai;//执行状态
    @JsonProperty(value = "qishiriq")
    private String qishiriq;//起始日期
    @JsonProperty(value = "zhngzhrq")
    private String zhngzhrq;//终止日期
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数

    public String getJysjzifu() {
        return jysjzifu;
    }

    public void setJysjzifu(String jysjzifu) {
        this.jysjzifu = jysjzifu;
    }

    public String getPicriqii() {
        return picriqii;
    }

    public void setPicriqii(String picriqii) {
        this.picriqii = picriqii;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public String getPicileib() {
        return picileib;
    }

    public void setPicileib(String picileib) {
        this.picileib = picileib;
    }

    public String getFuwudaim() {
        return fuwudaim;
    }

    public void setFuwudaim(String fuwudaim) {
        this.fuwudaim = fuwudaim;
    }

    public String getYpiciriq() {
        return ypiciriq;
    }

    public void setYpiciriq(String ypiciriq) {
        this.ypiciriq = ypiciriq;
    }

    public String getYpicihao() {
        return ypicihao;
    }

    public void setYpicihao(String ypicihao) {
        this.ypicihao = ypicihao;
    }

    public String getWeituoha() {
        return weituoha;
    }

    public void setWeituoha(String weituoha) {
        this.weituoha = weituoha;
    }

    public String getXieyihao() {
        return xieyihao;
    }

    public void setXieyihao(String xieyihao) {
        this.xieyihao = xieyihao;
    }

    public String getQianyzhh() {
        return qianyzhh;
    }

    public void setQianyzhh(String qianyzhh) {
        this.qianyzhh = qianyzhh;
    }

    public String getZhixztai() {
        return zhixztai;
    }

    public void setZhixztai(String zhixztai) {
        this.zhixztai = zhixztai;
    }

    public String getQishiriq() {
        return qishiriq;
    }

    public void setQishiriq(String qishiriq) {
        this.qishiriq = qishiriq;
    }

    public String getZhngzhrq() {
        return zhngzhrq;
    }

    public void setZhngzhrq(String zhngzhrq) {
        this.zhngzhrq = zhngzhrq;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Mbt952ReqDto{" +
                "jysjzifu='" + jysjzifu + '\'' +
                "picriqii='" + picriqii + '\'' +
                "picihaoo='" + picihaoo + '\'' +
                "picileib='" + picileib + '\'' +
                "fuwudaim='" + fuwudaim + '\'' +
                "ypiciriq='" + ypiciriq + '\'' +
                "ypicihao='" + ypicihao + '\'' +
                "weituoha='" + weituoha + '\'' +
                "xieyihao='" + xieyihao + '\'' +
                "qianyzhh='" + qianyzhh + '\'' +
                "zhixztai='" + zhixztai + '\'' +
                "qishiriq='" + qishiriq + '\'' +
                "zhngzhrq='" + zhngzhrq + '\'' +
                "qishibis='" + qishibis + '\'' +
                "chxunbis='" + chxunbis + '\'' +
                '}';
    }
}  
