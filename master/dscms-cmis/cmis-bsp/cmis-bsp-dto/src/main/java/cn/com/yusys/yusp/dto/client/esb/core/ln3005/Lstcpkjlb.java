package cn.com.yusys.yusp.dto.client.esb.core.ln3005;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：贷款产品会计类别对象
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstcpkjlb implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xuhaoooo")
    private Integer xuhaoooo;//序号
    @JsonProperty(value = "yewusx01")
    private String yewusx01;//业务属性1
    @JsonProperty(value = "ywsxms01")
    private String ywsxms01;//业务属性描述1
    @JsonProperty(value = "yewusx02")
    private String yewusx02;//业务属性2
    @JsonProperty(value = "ywsxms02")
    private String ywsxms02;//业务属性描述2
    @JsonProperty(value = "yewusx03")
    private String yewusx03;//业务属性3
    @JsonProperty(value = "ywsxms03")
    private String ywsxms03;//业务属性描述3
    @JsonProperty(value = "yewusx04")
    private String yewusx04;//业务属性4
    @JsonProperty(value = "ywsxms04")
    private String ywsxms04;//业务属性描述4
    @JsonProperty(value = "yewusx05")
    private String yewusx05;//业务属性5
    @JsonProperty(value = "ywsxms05")
    private String ywsxms05;//业务属性描述5
    @JsonProperty(value = "yewusx06")
    private String yewusx06;//业务属性6
    @JsonProperty(value = "ywsxms06")
    private String ywsxms06;//业务属性描述6
    @JsonProperty(value = "yewusx07")
    private String yewusx07;//业务属性7
    @JsonProperty(value = "ywsxms07")
    private String ywsxms07;//业务属性描述7
    @JsonProperty(value = "yewusx08")
    private String yewusx08;//业务属性8
    @JsonProperty(value = "ywsxms08")
    private String ywsxms08;//业务属性描述8
    @JsonProperty(value = "yewusx09")
    private String yewusx09;//业务属性9
    @JsonProperty(value = "ywsxms09")
    private String ywsxms09;//业务属性描述9
    @JsonProperty(value = "yewusx10")
    private String yewusx10;//业务属性10
    @JsonProperty(value = "ywsxms10")
    private String ywsxms10;//业务属性描述10

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getYwsxms01() {
        return ywsxms01;
    }

    public void setYwsxms01(String ywsxms01) {
        this.ywsxms01 = ywsxms01;
    }

    public String getYewusx02() {
        return yewusx02;
    }

    public void setYewusx02(String yewusx02) {
        this.yewusx02 = yewusx02;
    }

    public String getYwsxms02() {
        return ywsxms02;
    }

    public void setYwsxms02(String ywsxms02) {
        this.ywsxms02 = ywsxms02;
    }

    public String getYewusx03() {
        return yewusx03;
    }

    public void setYewusx03(String yewusx03) {
        this.yewusx03 = yewusx03;
    }

    public String getYwsxms03() {
        return ywsxms03;
    }

    public void setYwsxms03(String ywsxms03) {
        this.ywsxms03 = ywsxms03;
    }

    public String getYewusx04() {
        return yewusx04;
    }

    public void setYewusx04(String yewusx04) {
        this.yewusx04 = yewusx04;
    }

    public String getYwsxms04() {
        return ywsxms04;
    }

    public void setYwsxms04(String ywsxms04) {
        this.ywsxms04 = ywsxms04;
    }

    public String getYewusx05() {
        return yewusx05;
    }

    public void setYewusx05(String yewusx05) {
        this.yewusx05 = yewusx05;
    }

    public String getYwsxms05() {
        return ywsxms05;
    }

    public void setYwsxms05(String ywsxms05) {
        this.ywsxms05 = ywsxms05;
    }

    public String getYewusx06() {
        return yewusx06;
    }

    public void setYewusx06(String yewusx06) {
        this.yewusx06 = yewusx06;
    }

    public String getYwsxms06() {
        return ywsxms06;
    }

    public void setYwsxms06(String ywsxms06) {
        this.ywsxms06 = ywsxms06;
    }

    public String getYewusx07() {
        return yewusx07;
    }

    public void setYewusx07(String yewusx07) {
        this.yewusx07 = yewusx07;
    }

    public String getYwsxms07() {
        return ywsxms07;
    }

    public void setYwsxms07(String ywsxms07) {
        this.ywsxms07 = ywsxms07;
    }

    public String getYewusx08() {
        return yewusx08;
    }

    public void setYewusx08(String yewusx08) {
        this.yewusx08 = yewusx08;
    }

    public String getYwsxms08() {
        return ywsxms08;
    }

    public void setYwsxms08(String ywsxms08) {
        this.ywsxms08 = ywsxms08;
    }

    public String getYewusx09() {
        return yewusx09;
    }

    public void setYewusx09(String yewusx09) {
        this.yewusx09 = yewusx09;
    }

    public String getYwsxms09() {
        return ywsxms09;
    }

    public void setYwsxms09(String ywsxms09) {
        this.ywsxms09 = ywsxms09;
    }

    public String getYewusx10() {
        return yewusx10;
    }

    public void setYewusx10(String yewusx10) {
        this.yewusx10 = yewusx10;
    }

    public String getYwsxms10() {
        return ywsxms10;
    }

    public void setYwsxms10(String ywsxms10) {
        this.ywsxms10 = ywsxms10;
    }

    @Override
    public String toString() {
        return "Lstcpkjlb{" +
                "xuhaoooo='" + xuhaoooo + '\'' +
                "yewusx01='" + yewusx01 + '\'' +
                "ywsxms01='" + ywsxms01 + '\'' +
                "yewusx02='" + yewusx02 + '\'' +
                "ywsxms02='" + ywsxms02 + '\'' +
                "yewusx03='" + yewusx03 + '\'' +
                "ywsxms03='" + ywsxms03 + '\'' +
                "yewusx04='" + yewusx04 + '\'' +
                "ywsxms04='" + ywsxms04 + '\'' +
                "yewusx05='" + yewusx05 + '\'' +
                "ywsxms05='" + ywsxms05 + '\'' +
                "yewusx06='" + yewusx06 + '\'' +
                "ywsxms06='" + ywsxms06 + '\'' +
                "yewusx07='" + yewusx07 + '\'' +
                "ywsxms07='" + ywsxms07 + '\'' +
                "yewusx08='" + yewusx08 + '\'' +
                "ywsxms08='" + ywsxms08 + '\'' +
                "yewusx09='" + yewusx09 + '\'' +
                "ywsxms09='" + ywsxms09 + '\'' +
                "yewusx10='" + yewusx10 + '\'' +
                "ywsxms10='" + ywsxms10 + '\'' +
                '}';
    }
}  
