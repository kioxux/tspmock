package cn.com.yusys.yusp.dto.client.esb.core.ln3030;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：贷款资料维护
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3030ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkkhczbz")
    private String dkkhczbz;//开户操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "chuzhhao")
    private String chuzhhao;//出账号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "fkfangsh")
    private String fkfangsh;//放款金额方式
    @JsonProperty(value = "hxzdkkbz")
    private String hxzdkkbz;//核销自动扣款标志
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "dkrzhzxh")
    private String dkrzhzxh;//贷款入账账号子序号
    @JsonProperty(value = "fkzhouqi")
    private String fkzhouqi;//放款周期
    @JsonProperty(value = "dzhifkjh")
    private String dzhifkjh;//定制放款计划
    @JsonProperty(value = "kxqjjrgz")
    private String kxqjjrgz;//宽限期节假日规则
    @JsonProperty(value = "jixibzhi")
    private String jixibzhi;//计息标志
    @JsonProperty(value = "jfxibzhi")
    private String jfxibzhi;//计复息标志
    @JsonProperty(value = "fxjxbzhi")
    private String fxjxbzhi;//复息计息标志
    @JsonProperty(value = "jixiguiz")
    private String jixiguiz;//计息规则
    @JsonProperty(value = "kxqzyqgz")
    private String kxqzyqgz;//宽限期转逾期规则
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "llqxkdfs")
    private String llqxkdfs;//利率期限靠档方式
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "ltjixigz")
    private String ltjixigz;//零头计息规则
    @JsonProperty(value = "meictxfs")
    private String meictxfs;//每次摊销方式
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "yuqinyll")
    private String yuqinyll;//逾期年月利率
    @JsonProperty(value = "lilvfend")
    private String lilvfend;//利率分段
    @JsonProperty(value = "zaoqixbz")
    private String zaoqixbz;//早起息标志
    @JsonProperty(value = "wanqixbz")
    private String wanqixbz;//晚起息标志
    @JsonProperty(value = "tiexibzh")
    private String tiexibzh;//贴息标志
    @JsonProperty(value = "yushxfsh")
    private String yushxfsh;//预收息方式
    @JsonProperty(value = "meictxbl")
    private BigDecimal meictxbl;//每次摊销比例
    @JsonProperty(value = "kxqzdcsh")
    private Integer kxqzdcsh;//宽限期最大次数
    @JsonProperty(value = "zclilvbh")
    private String zclilvbh;//正常利率编号
    @JsonProperty(value = "yuqillbh")
    private String yuqillbh;//逾期利率编号
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "zhxnlilv")
    private BigDecimal zhxnlilv;//执行年利率
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "yushxize")
    private BigDecimal yushxize;//预收息总额
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lixitxzq")
    private String lixitxzq;//利息摊销周期
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "qigscfsh")
    private String qigscfsh;//期供生成方式
    @JsonProperty(value = "duophkbz")
    private String duophkbz;//多频率还款标志
    @JsonProperty(value = "dzhhkjih")
    private String dzhhkjih;//定制还款计划
    @JsonProperty(value = "zdkoukbz")
    private String zdkoukbz;//自动扣款标志
    @JsonProperty(value = "zdplkkbz")
    private String zdplkkbz;//指定文件批量扣款标识
    @JsonProperty(value = "zdjqdkbz")
    private String zdjqdkbz;//自动结清贷款标志
    @JsonProperty(value = "dhkzhhbz")
    private String dhkzhhbz;//多还款账户标志
    @JsonProperty(value = "qyxhdkbz")
    private String qyxhdkbz;//签约循环贷款标志
    @JsonProperty(value = "qxbgtzjh")
    private String qxbgtzjh;//期限变更调整计划
    @JsonProperty(value = "llbgtzjh")
    private String llbgtzjh;//利率变更调整计划
    @JsonProperty(value = "dcfktzjh")
    private String dcfktzjh;//多次放款调整计划
    @JsonProperty(value = "tqhktzjh")
    private String tqhktzjh;//提前还款调整计划
    @JsonProperty(value = "yunxtqhk")
    private String yunxtqhk;//允许提前还款
    @JsonProperty(value = "xycihkrq")
    private String xycihkrq;//下一次还款日
    @JsonProperty(value = "leijqjsh")
    private Integer leijqjsh;//累进区间期数
    @JsonProperty(value = "ljsxqish")
    private Integer ljsxqish;//累进首段期数
    @JsonProperty(value = "mqhbbili")
    private BigDecimal mqhbbili;//每期还本比例
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "xhdkqyzh")
    private String xhdkqyzh;//签约账号
    @JsonProperty(value = "hkqixian")
    private String hkqixian;//还款期限(月)
    @JsonProperty(value = "hkshxubh")
    private String hkshxubh;//还款顺序编号
    @JsonProperty(value = "leijinzh")
    private BigDecimal leijinzh;//累进值
    @JsonProperty(value = "baoliuje")
    private BigDecimal baoliuje;//保留金额
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "xhdkzhxh")
    private String xhdkzhxh;//签约账号子序号
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "huanbzhq")
    private String huanbzhq;//还本周期
    @JsonProperty(value = "yuqhkzhq")
    private String yuqhkzhq;//逾期还款周期
    @JsonProperty(value = "wtrkehuh")
    private String wtrkehuh;//委托人客户号
    @JsonProperty(value = "wtrckuzh")
    private String wtrckuzh;//委托人存款账号
    @JsonProperty(value = "wtckzhao")
    private String wtckzhao;//委托存款账号
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号
    @JsonProperty(value = "dailimsh")
    private String dailimsh;//代理描述
    @JsonProperty(value = "wtrckzxh")
    private String wtrckzxh;//委托人存款账号子序号
    @JsonProperty(value = "wtckzixh")
    private String wtckzixh;//委托存款账号子序号
    @JsonProperty(value = "bjghrzxh")
    private String bjghrzxh;//本金归还入账账号子序号
    @JsonProperty(value = "lxghrzxh")
    private String lxghrzxh;//利息归还入账账号子序号
    @JsonProperty(value = "baozjzxh")
    private String baozjzxh;//保证金账号子序号
    @JsonProperty(value = "duowtrbz")
    private String duowtrbz;//多委托人标志
    @JsonProperty(value = "zbjrsygz")
    private String zbjrsygz;//贷款到期假日规则
    @JsonProperty(value = "zhqxzekk")
    private String zhqxzekk;//展期需足额扣款
    @JsonProperty(value = "dbdkkksx")
    private Integer dbdkkksx;//多笔贷款扣款顺序
    @JsonProperty(value = "yunxdkzq")
    private String yunxdkzq;//允许贷款展期
    @JsonProperty(value = "zhqzdcsh")
    private Integer zhqzdcsh;//展期最大次数
    @JsonProperty(value = "zqgzbhao")
    private String zqgzbhao;//展期规则编号
    @JsonProperty(value = "hesuanfs")
    private String hesuanfs;//核算方式
    @JsonProperty(value = "lixizcgz")
    private String lixizcgz;//利息转出规则
    @JsonProperty(value = "lixizhgz")
    private String lixizhgz;//利息转回规则
    @JsonProperty(value = "yjfyjhes")
    private String yjfyjhes;//按应计非应计核算
    @JsonProperty(value = "yiyldhes")
    private String yiyldhes;//按一逾两呆核算
    @JsonProperty(value = "dkxtkmhs")
    private String dkxtkmhs;//形态分科目核算
    @JsonProperty(value = "zidxtzhy")
    private String zidxtzhy;//自动形态转移
    @JsonProperty(value = "shfleixi")
    private String shfleixi;//收费类型
    @JsonProperty(value = "tqhkfjbh")
    private String tqhkfjbh;//提前还款罚金编号
    @JsonProperty(value = "tqhkfjmc")
    private String tqhkfjmc;//提前还款罚金名称
    @JsonProperty(value = "tqhkfjfj")
    private BigDecimal tqhkfjfj;//提前还款附加罚金金额
    @JsonProperty(value = "zhchtqtz")
    private String zhchtqtz;//正常提前通知
    @JsonProperty(value = "yqcshtzh")
    private String yqcshtzh;//逾期催收通知
    @JsonProperty(value = "lilvbgtz")
    private String lilvbgtz;//利率变更通知
    @JsonProperty(value = "yuebgtzh")
    private String yuebgtzh;//余额变更通知
    @JsonProperty(value = "tzhtqtsh")
    private Integer tzhtqtsh;//通知提前天数
    @JsonProperty(value = "tzhjgtsh")
    private Integer tzhjgtsh;//通知间隔天数
    @JsonProperty(value = "pingzhzl")
    private String pingzhzl;//凭证种类
    @JsonProperty(value = "sydkcnuo")
    private String sydkcnuo;//使用贷款承诺
    @JsonProperty(value = "lmdkbzhi")
    private String lmdkbzhi;//联名贷款标志
    @JsonProperty(value = "bzhrdbbz")
    private String bzhrdbbz;//保证人担保标志
    @JsonProperty(value = "wujiflbz")
    private String wujiflbz;//五级分类标志
    @JsonProperty(value = "wujiflrq")
    private String wujiflrq;//五级分类日期
    @JsonProperty(value = "cndkjjho")
    private String cndkjjho;//承诺贷款借据号
    @JsonProperty(value = "dkgljgou")
    private String dkgljgou;//贷款管理机构
    @JsonProperty(value = "gljgleib")
    private String gljgleib;//管理机构类别
    @JsonProperty(value = "fuhejgou")
    private String fuhejgou;//复核机构
    @JsonProperty(value = "khjingli")
    private String khjingli;//客户经理
    @JsonProperty(value = "pingzhma")
    private String pingzhma;//凭证批号
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao;//凭证序号
    @JsonProperty(value = "yinhshlv")
    private BigDecimal yinhshlv;//印花税率
    @JsonProperty(value = "yinhshje")
    private BigDecimal yinhshje;//印花税金额
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息
    @JsonProperty(value = "hetongll")
    private BigDecimal hetongll;//合同利率
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "kuaijilb")
    private String kuaijilb;//会计类别
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限(月)
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "fafangje")
    private BigDecimal fafangje;//已发放金额
    @JsonProperty(value = "djiekfje")
    private BigDecimal djiekfje;//冻结可放金额
    @JsonProperty(value = "kffangje")
    private BigDecimal kffangje;//可发放金额
    @JsonProperty(value = "dkuanjij")
    private BigDecimal dkuanjij;//贷款基金
    @JsonProperty(value = "daikduix")
    private String daikduix;//贷款对象
    @JsonProperty(value = "yewufenl")
    private String yewufenl;//业务分类
    @JsonProperty(value = "ysywleix")
    private String ysywleix;//衍生业务类型
    @JsonProperty(value = "ysywbhao")
    private String ysywbhao;//衍生业务编号
    @JsonProperty(value = "zhqifkbz")
    private String zhqifkbz;//周期性放款标志
    @JsonProperty(value = "mcfkjebl")
    private BigDecimal mcfkjebl;//每次放款金额或比例
    @JsonProperty(value = "scjixirq")
    private String scjixirq;//上次计息日
    @JsonProperty(value = "dlxxzdgz")
    private String dlxxzdgz;//代理信息指定规则
    @JsonProperty(value = "dlxxqzgz")
    private String dlxxqzgz;//代理信息取值规则
    @JsonProperty(value = "wtckywbm")
    private String wtckywbm;//委托存款业务编码
    @JsonProperty(value = "dailixuh")
    private Integer dailixuh;//代理序号
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "edbizhgz")
    private String edbizhgz;//额度币种规则
    @JsonProperty(value = "edzdbizh")
    private String edzdbizh;//额度指定币种
    @JsonProperty(value = "yewusx01")
    private String yewusx01;//业务属性1
    @JsonProperty(value = "yewusx02")
    private String yewusx02;//业务属性2
    @JsonProperty(value = "yewusx03")
    private String yewusx03;//业务属性3
    @JsonProperty(value = "yewusx04")
    private String yewusx04;//业务属性4
    @JsonProperty(value = "yewusx05")
    private String yewusx05;//业务属性5
    @JsonProperty(value = "yewusx06")
    private String yewusx06;//业务属性6
    @JsonProperty(value = "yewusx07")
    private String yewusx07;//业务属性7
    @JsonProperty(value = "yewusx08")
    private String yewusx08;//业务属性8
    @JsonProperty(value = "yewusx09")
    private String yewusx09;//业务属性9
    @JsonProperty(value = "yewusx10")
    private String yewusx10;//业务属性10
    @JsonProperty(value = "ywsxms01")
    private String ywsxms01;//业务属性描述1
    @JsonProperty(value = "ywsxms02")
    private String ywsxms02;//业务属性描述2
    @JsonProperty(value = "ywsxms03")
    private String ywsxms03;//业务属性描述3
    @JsonProperty(value = "ywsxms04")
    private String ywsxms04;//业务属性描述4
    @JsonProperty(value = "ywsxms05")
    private String ywsxms05;//业务属性描述5
    @JsonProperty(value = "ywsxms06")
    private String ywsxms06;//业务属性描述6
    @JsonProperty(value = "ywsxms07")
    private String ywsxms07;//业务属性描述7
    @JsonProperty(value = "ywsxms08")
    private String ywsxms08;//业务属性描述8
    @JsonProperty(value = "ywsxms09")
    private String ywsxms09;//业务属性描述9
    @JsonProperty(value = "ywsxms10")
    private String ywsxms10;//业务属性描述10
    @JsonProperty(value = "lstdkfkjh")
    private List<Lstdkfkjh> lstdkfkjh;//贷款放款计划
    @JsonProperty(value = "lstdkfwdj")
    private List<Lstdkfwdj> lstdkfwdj;//贷款服务登记
    @JsonProperty(value = "lstdkhbjh")
    private List<Lstdkhbjh> lstdkhbjh;//贷款还本计划
    @JsonProperty(value = "benjinfd")
    private String benjinfd;//本金分段
    @JsonProperty(value = "lstdkbjfd")
    private List<Lstdkbjfd> lstdkbjfd;//本金分段登记
    @JsonProperty(value = "lstdkhkfs")
    private List<Lstdkhkfs> lstdkhkfs;//贷款组合还款方式
    @JsonProperty(value = "lstdkhkzh")
    private List<Lstdkhkzh> lstdkhkzh;//贷款多还款账户
    @JsonProperty(value = "lstdkllfd")
    private List<Lstdkllfd> lstdkllfd;//贷款利率分段
    @JsonProperty(value = "lstdksfsj")
    private List<Lstdksfsj> lstdksfsj;//贷款收费事件
    @JsonProperty(value = "lstdkstzf")
    private List<Lstdkstzf> lstdkstzf;//贷款受托支付
    @JsonProperty(value = "lstdktxzh")
    private List<Lstdktxzh> lstdktxzh;//贷款多贴息账户
    @JsonProperty(value = "lstdkwtxx")
    private List<Lstdkwtxx> lstdkwtxx;//贷款多委托人账户
    @JsonProperty(value = "lstdkzhbz")
    private List<Lstdkzhbz> lstdkzhbz;//贷款保证人信息
    @JsonProperty(value = "lstdkzhlm")
    private List<Lstdkzhlm> lstdkzhlm;//贷款账户联名
    @JsonProperty(value = "chanpmsh")
    private String chanpmsh;//产品描述
    @JsonProperty(value = "daikdxxf")
    private String daikdxxf;//贷款对象细分
    @JsonProperty(value = "xunhdaik")
    private String xunhdaik;//循环贷款
    @JsonProperty(value = "yansdaik")
    private String yansdaik;//衍生贷款
    @JsonProperty(value = "chendaik")
    private String chendaik;//承诺贷款
    @JsonProperty(value = "butidaik")
    private String butidaik;//补贴贷款
    @JsonProperty(value = "yintdkbz")
    private String yintdkbz;//银团贷款标志
    @JsonProperty(value = "yintdkfs")
    private String yintdkfs;//银团贷款方式
    @JsonProperty(value = "yintleib")
    private String yintleib;//银团类别
    @JsonProperty(value = "yintnbcy")
    private String yintnbcy;//内部银团成员类型
    @JsonProperty(value = "yintwbcy")
    private String yintwbcy;//外部银团成员类型
    @JsonProperty(value = "fangkulx")
    private String fangkulx;//放款类型
    @JsonProperty(value = "fkzjclfs")
    private String fkzjclfs;//放款资金处理方式
    @JsonProperty(value = "hntmifku")
    private String hntmifku;//允许对行内同名账户放款
    @JsonProperty(value = "hnftmfku")
    private String hnftmfku;//允许对行内非同名账户放款
    @JsonProperty(value = "neibufku")
    private String neibufku;//允许对内部账户放款
    @JsonProperty(value = "jixibjgz")
    private String jixibjgz;//计息本金规则
    @JsonProperty(value = "lixijsff")
    private String lixijsff;//利息计算方法
    @JsonProperty(value = "jixitwgz")
    private String jixitwgz;//计息头尾规则
    @JsonProperty(value = "jixizxje")
    private BigDecimal jixizxje;//计息最小金额
    @JsonProperty(value = "jixisrgz")
    private String jixisrgz;//计息舍入规则
    @JsonProperty(value = "srzxdanw")
    private String srzxdanw;//舍入最小单位
    @JsonProperty(value = "lilvqixx")
    private String lilvqixx;//利率期限
    @JsonProperty(value = "fdjixibz")
    private String fdjixibz;//分段计息标志
    @JsonProperty(value = "yuqitzfs")
    private String yuqitzfs;//逾期利率调整方式
    @JsonProperty(value = "yuqitzzq")
    private String yuqitzzq;//逾期利率调整周期
    @JsonProperty(value = "yqfxfdfs")
    private String yqfxfdfs;//逾期罚息浮动方式
    @JsonProperty(value = "yqfxfdzh")
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    @JsonProperty(value = "fulilvbh")
    private String fulilvbh;//复利利率编号
    @JsonProperty(value = "fulilvny")
    private String fulilvny;//复利利率年月标识
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "fulitzfs")
    private String fulitzfs;//复利利率调整方式
    @JsonProperty(value = "fulitzzq")
    private String fulitzzq;//复利利率调整周期
    @JsonProperty(value = "jitiguiz")
    private String jitiguiz;//计提规则
    @JsonProperty(value = "butijejs")
    private String butijejs;//补贴金额计算方式
    @JsonProperty(value = "tiaozhkf")
    private String tiaozhkf;//允许调整还款方式
    @JsonProperty(value = "scihkrbz")
    private String scihkrbz;//首次还款日模式
    @JsonProperty(value = "mqihkfsh")
    private String mqihkfsh;//末期还款方式
    @JsonProperty(value = "yunxsuoq")
    private String yunxsuoq;//允许缩期
    @JsonProperty(value = "bzuekkfs")
    private String bzuekkfs;//不足额扣款方式
    @JsonProperty(value = "yqbzkkfs")
    private String yqbzkkfs;//逾期不足额扣款方式
    @JsonProperty(value = "hntmihku")
    private String hntmihku;//允许行内同名账户还款
    @JsonProperty(value = "hnftmhku")
    private String hnftmhku;//允许行内非同名帐户还款
    @JsonProperty(value = "nbuzhhku")
    private String nbuzhhku;//允许内部账户还款
    @JsonProperty(value = "tiqhksdq")
    private Integer tiqhksdq;//提前还款锁定期
    @JsonProperty(value = "sfyxkuxq")
    private String sfyxkuxq;//是否有宽限期
    @JsonProperty(value = "kuanxqts")
    private Integer kuanxqts;//宽限期天数
    @JsonProperty(value = "kxqjixgz")
    private String kxqjixgz;//宽限期计息方式
    @JsonProperty(value = "kxqhjxgz")
    private String kxqhjxgz;//宽限期后计息规则
    @JsonProperty(value = "kxqshxgz")
    private String kxqshxgz;//宽限期收息规则
    @JsonProperty(value = "zhqizcqx")
    private String zhqizcqx;//展期最长期限(月)
    @JsonProperty(value = "fulifdfs")
    private String fulifdfs;//复利利率浮动方式
    @JsonProperty(value = "fulifdzh")
    private BigDecimal fulifdzh;//复利利率浮动值
    @JsonProperty(value = "bwchapbz")
    private String bwchapbz;//表外产品
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "flllcklx")
    private String flllcklx;//复利利率参考类型
    @JsonProperty(value = "kshchpdm")
    private String kshchpdm;//可售产品代码
    @JsonProperty(value = "kshchpmc")
    private String kshchpmc;//可售产品名称
    @JsonProperty(value = "dkczhzhh")
    private String dkczhzhh;//贷款出账号
    @JsonProperty(value = "dkdbfshi")
    private String dkdbfshi;//贷款担保方式
    @JsonProperty(value = "dkyongtu")
    private String dkyongtu;//贷款用途
    @JsonProperty(value = "fkjzhfsh")
    private String fkjzhfsh;//放款记账方式
    @JsonProperty(value = "shtzfhxm")
    private String shtzfhxm;//受托支付业务编码
    @JsonProperty(value = "bwhesdma")
    private String bwhesdma;//表外核算码
    @JsonProperty(value = "wtrmingc")
    private String wtrmingc;//委托人名称
    @JsonProperty(value = "ysxlyzhh")
    private String ysxlyzhh;//预收息扣息来源账号
    @JsonProperty(value = "ysxlyzxh")
    private String ysxlyzxh;//预收息扣息来源账号子序号


    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getChuzhhao() {
        return chuzhhao;
    }

    public void setChuzhhao(String chuzhhao) {
        this.chuzhhao = chuzhhao;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public String getFkfangsh() {
        return fkfangsh;
    }

    public void setFkfangsh(String fkfangsh) {
        this.fkfangsh = fkfangsh;
    }

    public String getHxzdkkbz() {
        return hxzdkkbz;
    }

    public void setHxzdkkbz(String hxzdkkbz) {
        this.hxzdkkbz = hxzdkkbz;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public String getFkzhouqi() {
        return fkzhouqi;
    }

    public void setFkzhouqi(String fkzhouqi) {
        this.fkzhouqi = fkzhouqi;
    }

    public String getDzhifkjh() {
        return dzhifkjh;
    }

    public void setDzhifkjh(String dzhifkjh) {
        this.dzhifkjh = dzhifkjh;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    public String getJixibzhi() {
        return jixibzhi;
    }

    public void setJixibzhi(String jixibzhi) {
        this.jixibzhi = jixibzhi;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFxjxbzhi() {
        return fxjxbzhi;
    }

    public void setFxjxbzhi(String fxjxbzhi) {
        this.fxjxbzhi = fxjxbzhi;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public String getKxqzyqgz() {
        return kxqzyqgz;
    }

    public void setKxqzyqgz(String kxqzyqgz) {
        this.kxqzyqgz = kxqzyqgz;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLtjixigz() {
        return ltjixigz;
    }

    public void setLtjixigz(String ltjixigz) {
        this.ltjixigz = ltjixigz;
    }

    public String getMeictxfs() {
        return meictxfs;
    }

    public void setMeictxfs(String meictxfs) {
        this.meictxfs = meictxfs;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public String getLilvfend() {
        return lilvfend;
    }

    public void setLilvfend(String lilvfend) {
        this.lilvfend = lilvfend;
    }

    public String getZaoqixbz() {
        return zaoqixbz;
    }

    public void setZaoqixbz(String zaoqixbz) {
        this.zaoqixbz = zaoqixbz;
    }

    public String getWanqixbz() {
        return wanqixbz;
    }

    public void setWanqixbz(String wanqixbz) {
        this.wanqixbz = wanqixbz;
    }

    public String getTiexibzh() {
        return tiexibzh;
    }

    public void setTiexibzh(String tiexibzh) {
        this.tiexibzh = tiexibzh;
    }

    public String getYushxfsh() {
        return yushxfsh;
    }

    public void setYushxfsh(String yushxfsh) {
        this.yushxfsh = yushxfsh;
    }

    public BigDecimal getMeictxbl() {
        return meictxbl;
    }

    public void setMeictxbl(BigDecimal meictxbl) {
        this.meictxbl = meictxbl;
    }

    public Integer getKxqzdcsh() {
        return kxqzdcsh;
    }

    public void setKxqzdcsh(Integer kxqzdcsh) {
        this.kxqzdcsh = kxqzdcsh;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getZhxnlilv() {
        return zhxnlilv;
    }

    public void setZhxnlilv(BigDecimal zhxnlilv) {
        this.zhxnlilv = zhxnlilv;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public BigDecimal getYushxize() {
        return yushxize;
    }

    public void setYushxize(BigDecimal yushxize) {
        this.yushxize = yushxize;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLixitxzq() {
        return lixitxzq;
    }

    public void setLixitxzq(String lixitxzq) {
        this.lixitxzq = lixitxzq;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public String getDuophkbz() {
        return duophkbz;
    }

    public void setDuophkbz(String duophkbz) {
        this.duophkbz = duophkbz;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getZdplkkbz() {
        return zdplkkbz;
    }

    public void setZdplkkbz(String zdplkkbz) {
        this.zdplkkbz = zdplkkbz;
    }

    public String getZdjqdkbz() {
        return zdjqdkbz;
    }

    public void setZdjqdkbz(String zdjqdkbz) {
        this.zdjqdkbz = zdjqdkbz;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public String getQyxhdkbz() {
        return qyxhdkbz;
    }

    public void setQyxhdkbz(String qyxhdkbz) {
        this.qyxhdkbz = qyxhdkbz;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    public String getXycihkrq() {
        return xycihkrq;
    }

    public void setXycihkrq(String xycihkrq) {
        this.xycihkrq = xycihkrq;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public Integer getLjsxqish() {
        return ljsxqish;
    }

    public void setLjsxqish(Integer ljsxqish) {
        this.ljsxqish = ljsxqish;
    }

    public BigDecimal getMqhbbili() {
        return mqhbbili;
    }

    public void setMqhbbili(BigDecimal mqhbbili) {
        this.mqhbbili = mqhbbili;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getXhdkqyzh() {
        return xhdkqyzh;
    }

    public void setXhdkqyzh(String xhdkqyzh) {
        this.xhdkqyzh = xhdkqyzh;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public String getHkshxubh() {
        return hkshxubh;
    }

    public void setHkshxubh(String hkshxubh) {
        this.hkshxubh = hkshxubh;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getXhdkzhxh() {
        return xhdkzhxh;
    }

    public void setXhdkzhxh(String xhdkzhxh) {
        this.xhdkzhxh = xhdkzhxh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    public String getWtrkehuh() {
        return wtrkehuh;
    }

    public void setWtrkehuh(String wtrkehuh) {
        this.wtrkehuh = wtrkehuh;
    }

    public String getWtrckuzh() {
        return wtrckuzh;
    }

    public void setWtrckuzh(String wtrckuzh) {
        this.wtrckuzh = wtrckuzh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getDailimsh() {
        return dailimsh;
    }

    public void setDailimsh(String dailimsh) {
        this.dailimsh = dailimsh;
    }

    public String getWtrckzxh() {
        return wtrckzxh;
    }

    public void setWtrckzxh(String wtrckzxh) {
        this.wtrckzxh = wtrckzxh;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public String getBaozjzxh() {
        return baozjzxh;
    }

    public void setBaozjzxh(String baozjzxh) {
        this.baozjzxh = baozjzxh;
    }

    public String getDuowtrbz() {
        return duowtrbz;
    }

    public void setDuowtrbz(String duowtrbz) {
        this.duowtrbz = duowtrbz;
    }

    public String getZbjrsygz() {
        return zbjrsygz;
    }

    public void setZbjrsygz(String zbjrsygz) {
        this.zbjrsygz = zbjrsygz;
    }

    public String getZhqxzekk() {
        return zhqxzekk;
    }

    public void setZhqxzekk(String zhqxzekk) {
        this.zhqxzekk = zhqxzekk;
    }

    public Integer getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Integer dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public String getYunxdkzq() {
        return yunxdkzq;
    }

    public void setYunxdkzq(String yunxdkzq) {
        this.yunxdkzq = yunxdkzq;
    }

    public Integer getZhqzdcsh() {
        return zhqzdcsh;
    }

    public void setZhqzdcsh(Integer zhqzdcsh) {
        this.zhqzdcsh = zhqzdcsh;
    }

    public String getZqgzbhao() {
        return zqgzbhao;
    }

    public void setZqgzbhao(String zqgzbhao) {
        this.zqgzbhao = zqgzbhao;
    }

    public String getHesuanfs() {
        return hesuanfs;
    }

    public void setHesuanfs(String hesuanfs) {
        this.hesuanfs = hesuanfs;
    }

    public String getLixizcgz() {
        return lixizcgz;
    }

    public void setLixizcgz(String lixizcgz) {
        this.lixizcgz = lixizcgz;
    }

    public String getLixizhgz() {
        return lixizhgz;
    }

    public void setLixizhgz(String lixizhgz) {
        this.lixizhgz = lixizhgz;
    }

    public String getYjfyjhes() {
        return yjfyjhes;
    }

    public void setYjfyjhes(String yjfyjhes) {
        this.yjfyjhes = yjfyjhes;
    }

    public String getYiyldhes() {
        return yiyldhes;
    }

    public void setYiyldhes(String yiyldhes) {
        this.yiyldhes = yiyldhes;
    }

    public String getDkxtkmhs() {
        return dkxtkmhs;
    }

    public void setDkxtkmhs(String dkxtkmhs) {
        this.dkxtkmhs = dkxtkmhs;
    }

    public String getZidxtzhy() {
        return zidxtzhy;
    }

    public void setZidxtzhy(String zidxtzhy) {
        this.zidxtzhy = zidxtzhy;
    }

    public String getShfleixi() {
        return shfleixi;
    }

    public void setShfleixi(String shfleixi) {
        this.shfleixi = shfleixi;
    }

    public String getTqhkfjbh() {
        return tqhkfjbh;
    }

    public void setTqhkfjbh(String tqhkfjbh) {
        this.tqhkfjbh = tqhkfjbh;
    }

    public String getTqhkfjmc() {
        return tqhkfjmc;
    }

    public void setTqhkfjmc(String tqhkfjmc) {
        this.tqhkfjmc = tqhkfjmc;
    }

    public BigDecimal getTqhkfjfj() {
        return tqhkfjfj;
    }

    public void setTqhkfjfj(BigDecimal tqhkfjfj) {
        this.tqhkfjfj = tqhkfjfj;
    }

    public String getZhchtqtz() {
        return zhchtqtz;
    }

    public void setZhchtqtz(String zhchtqtz) {
        this.zhchtqtz = zhchtqtz;
    }

    public String getYqcshtzh() {
        return yqcshtzh;
    }

    public void setYqcshtzh(String yqcshtzh) {
        this.yqcshtzh = yqcshtzh;
    }

    public String getLilvbgtz() {
        return lilvbgtz;
    }

    public void setLilvbgtz(String lilvbgtz) {
        this.lilvbgtz = lilvbgtz;
    }

    public String getYuebgtzh() {
        return yuebgtzh;
    }

    public void setYuebgtzh(String yuebgtzh) {
        this.yuebgtzh = yuebgtzh;
    }

    public Integer getTzhtqtsh() {
        return tzhtqtsh;
    }

    public void setTzhtqtsh(Integer tzhtqtsh) {
        this.tzhtqtsh = tzhtqtsh;
    }

    public Integer getTzhjgtsh() {
        return tzhjgtsh;
    }

    public void setTzhjgtsh(Integer tzhjgtsh) {
        this.tzhjgtsh = tzhjgtsh;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getSydkcnuo() {
        return sydkcnuo;
    }

    public void setSydkcnuo(String sydkcnuo) {
        this.sydkcnuo = sydkcnuo;
    }

    public String getLmdkbzhi() {
        return lmdkbzhi;
    }

    public void setLmdkbzhi(String lmdkbzhi) {
        this.lmdkbzhi = lmdkbzhi;
    }

    public String getBzhrdbbz() {
        return bzhrdbbz;
    }

    public void setBzhrdbbz(String bzhrdbbz) {
        this.bzhrdbbz = bzhrdbbz;
    }

    public String getWujiflbz() {
        return wujiflbz;
    }

    public void setWujiflbz(String wujiflbz) {
        this.wujiflbz = wujiflbz;
    }

    public String getWujiflrq() {
        return wujiflrq;
    }

    public void setWujiflrq(String wujiflrq) {
        this.wujiflrq = wujiflrq;
    }

    public String getCndkjjho() {
        return cndkjjho;
    }

    public void setCndkjjho(String cndkjjho) {
        this.cndkjjho = cndkjjho;
    }

    public String getDkgljgou() {
        return dkgljgou;
    }

    public void setDkgljgou(String dkgljgou) {
        this.dkgljgou = dkgljgou;
    }

    public String getGljgleib() {
        return gljgleib;
    }

    public void setGljgleib(String gljgleib) {
        this.gljgleib = gljgleib;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public String getKhjingli() {
        return khjingli;
    }

    public void setKhjingli(String khjingli) {
        this.khjingli = khjingli;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public BigDecimal getYinhshlv() {
        return yinhshlv;
    }

    public void setYinhshlv(BigDecimal yinhshlv) {
        this.yinhshlv = yinhshlv;
    }

    public BigDecimal getYinhshje() {
        return yinhshje;
    }

    public void setYinhshje(BigDecimal yinhshje) {
        this.yinhshje = yinhshje;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getFafangje() {
        return fafangje;
    }

    public void setFafangje(BigDecimal fafangje) {
        this.fafangje = fafangje;
    }

    public BigDecimal getDjiekfje() {
        return djiekfje;
    }

    public void setDjiekfje(BigDecimal djiekfje) {
        this.djiekfje = djiekfje;
    }

    public BigDecimal getKffangje() {
        return kffangje;
    }

    public void setKffangje(BigDecimal kffangje) {
        this.kffangje = kffangje;
    }

    public BigDecimal getDkuanjij() {
        return dkuanjij;
    }

    public void setDkuanjij(BigDecimal dkuanjij) {
        this.dkuanjij = dkuanjij;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public String getYewufenl() {
        return yewufenl;
    }

    public void setYewufenl(String yewufenl) {
        this.yewufenl = yewufenl;
    }

    public String getYsywleix() {
        return ysywleix;
    }

    public void setYsywleix(String ysywleix) {
        this.ysywleix = ysywleix;
    }

    public String getYsywbhao() {
        return ysywbhao;
    }

    public void setYsywbhao(String ysywbhao) {
        this.ysywbhao = ysywbhao;
    }

    public String getZhqifkbz() {
        return zhqifkbz;
    }

    public void setZhqifkbz(String zhqifkbz) {
        this.zhqifkbz = zhqifkbz;
    }

    public BigDecimal getMcfkjebl() {
        return mcfkjebl;
    }

    public void setMcfkjebl(BigDecimal mcfkjebl) {
        this.mcfkjebl = mcfkjebl;
    }

    public String getScjixirq() {
        return scjixirq;
    }

    public void setScjixirq(String scjixirq) {
        this.scjixirq = scjixirq;
    }

    public String getDlxxzdgz() {
        return dlxxzdgz;
    }

    public void setDlxxzdgz(String dlxxzdgz) {
        this.dlxxzdgz = dlxxzdgz;
    }

    public String getDlxxqzgz() {
        return dlxxqzgz;
    }

    public void setDlxxqzgz(String dlxxqzgz) {
        this.dlxxqzgz = dlxxqzgz;
    }

    public String getWtckywbm() {
        return wtckywbm;
    }

    public void setWtckywbm(String wtckywbm) {
        this.wtckywbm = wtckywbm;
    }

    public Integer getDailixuh() {
        return dailixuh;
    }

    public void setDailixuh(Integer dailixuh) {
        this.dailixuh = dailixuh;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getEdbizhgz() {
        return edbizhgz;
    }

    public void setEdbizhgz(String edbizhgz) {
        this.edbizhgz = edbizhgz;
    }

    public String getEdzdbizh() {
        return edzdbizh;
    }

    public void setEdzdbizh(String edzdbizh) {
        this.edzdbizh = edzdbizh;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getYewusx02() {
        return yewusx02;
    }

    public void setYewusx02(String yewusx02) {
        this.yewusx02 = yewusx02;
    }

    public String getYewusx03() {
        return yewusx03;
    }

    public void setYewusx03(String yewusx03) {
        this.yewusx03 = yewusx03;
    }

    public String getYewusx04() {
        return yewusx04;
    }

    public void setYewusx04(String yewusx04) {
        this.yewusx04 = yewusx04;
    }

    public String getYewusx05() {
        return yewusx05;
    }

    public void setYewusx05(String yewusx05) {
        this.yewusx05 = yewusx05;
    }

    public String getYewusx06() {
        return yewusx06;
    }

    public void setYewusx06(String yewusx06) {
        this.yewusx06 = yewusx06;
    }

    public String getYewusx07() {
        return yewusx07;
    }

    public void setYewusx07(String yewusx07) {
        this.yewusx07 = yewusx07;
    }

    public String getYewusx08() {
        return yewusx08;
    }

    public void setYewusx08(String yewusx08) {
        this.yewusx08 = yewusx08;
    }

    public String getYewusx09() {
        return yewusx09;
    }

    public void setYewusx09(String yewusx09) {
        this.yewusx09 = yewusx09;
    }

    public String getYewusx10() {
        return yewusx10;
    }

    public void setYewusx10(String yewusx10) {
        this.yewusx10 = yewusx10;
    }

    public String getYwsxms01() {
        return ywsxms01;
    }

    public void setYwsxms01(String ywsxms01) {
        this.ywsxms01 = ywsxms01;
    }

    public String getYwsxms02() {
        return ywsxms02;
    }

    public void setYwsxms02(String ywsxms02) {
        this.ywsxms02 = ywsxms02;
    }

    public String getYwsxms03() {
        return ywsxms03;
    }

    public void setYwsxms03(String ywsxms03) {
        this.ywsxms03 = ywsxms03;
    }

    public String getYwsxms04() {
        return ywsxms04;
    }

    public void setYwsxms04(String ywsxms04) {
        this.ywsxms04 = ywsxms04;
    }

    public String getYwsxms05() {
        return ywsxms05;
    }

    public void setYwsxms05(String ywsxms05) {
        this.ywsxms05 = ywsxms05;
    }

    public String getYwsxms06() {
        return ywsxms06;
    }

    public void setYwsxms06(String ywsxms06) {
        this.ywsxms06 = ywsxms06;
    }

    public String getYwsxms07() {
        return ywsxms07;
    }

    public void setYwsxms07(String ywsxms07) {
        this.ywsxms07 = ywsxms07;
    }

    public String getYwsxms08() {
        return ywsxms08;
    }

    public void setYwsxms08(String ywsxms08) {
        this.ywsxms08 = ywsxms08;
    }

    public String getYwsxms09() {
        return ywsxms09;
    }

    public void setYwsxms09(String ywsxms09) {
        this.ywsxms09 = ywsxms09;
    }

    public String getYwsxms10() {
        return ywsxms10;
    }

    public void setYwsxms10(String ywsxms10) {
        this.ywsxms10 = ywsxms10;
    }

    public List<Lstdkfkjh> getLstdkfkjh() {
        return lstdkfkjh;
    }

    public void setLstdkfkjh(List<Lstdkfkjh> lstdkfkjh) {
        this.lstdkfkjh = lstdkfkjh;
    }

    public List<Lstdkfwdj> getLstdkfwdj() {
        return lstdkfwdj;
    }

    public void setLstdkfwdj(List<Lstdkfwdj> lstdkfwdj) {
        this.lstdkfwdj = lstdkfwdj;
    }

    public List<Lstdkhbjh> getLstdkhbjh() {
        return lstdkhbjh;
    }

    public void setLstdkhbjh(List<Lstdkhbjh> lstdkhbjh) {
        this.lstdkhbjh = lstdkhbjh;
    }

    public String getBenjinfd() {
        return benjinfd;
    }

    public void setBenjinfd(String benjinfd) {
        this.benjinfd = benjinfd;
    }

    public List<Lstdkbjfd> getLstdkbjfd() {
        return lstdkbjfd;
    }

    public void setLstdkbjfd(List<Lstdkbjfd> lstdkbjfd) {
        this.lstdkbjfd = lstdkbjfd;
    }

    public List<Lstdkhkfs> getLstdkhkfs() {
        return lstdkhkfs;
    }

    public void setLstdkhkfs(List<Lstdkhkfs> lstdkhkfs) {
        this.lstdkhkfs = lstdkhkfs;
    }

    public List<Lstdkhkzh> getLstdkhkzh() {
        return lstdkhkzh;
    }

    public void setLstdkhkzh(List<Lstdkhkzh> lstdkhkzh) {
        this.lstdkhkzh = lstdkhkzh;
    }

    public List<Lstdkllfd> getLstdkllfd() {
        return lstdkllfd;
    }

    public void setLstdkllfd(List<Lstdkllfd> lstdkllfd) {
        this.lstdkllfd = lstdkllfd;
    }

    public List<Lstdksfsj> getLstdksfsj() {
        return lstdksfsj;
    }

    public void setLstdksfsj(List<Lstdksfsj> lstdksfsj) {
        this.lstdksfsj = lstdksfsj;
    }

    public List<Lstdkstzf> getLstdkstzf() {
        return lstdkstzf;
    }

    public void setLstdkstzf(List<Lstdkstzf> lstdkstzf) {
        this.lstdkstzf = lstdkstzf;
    }

    public List<Lstdktxzh> getLstdktxzh() {
        return lstdktxzh;
    }

    public void setLstdktxzh(List<Lstdktxzh> lstdktxzh) {
        this.lstdktxzh = lstdktxzh;
    }

    public List<Lstdkwtxx> getLstdkwtxx() {
        return lstdkwtxx;
    }

    public void setLstdkwtxx(List<Lstdkwtxx> lstdkwtxx) {
        this.lstdkwtxx = lstdkwtxx;
    }

    public List<Lstdkzhbz> getLstdkzhbz() {
        return lstdkzhbz;
    }

    public void setLstdkzhbz(List<Lstdkzhbz> lstdkzhbz) {
        this.lstdkzhbz = lstdkzhbz;
    }

    public List<Lstdkzhlm> getLstdkzhlm() {
        return lstdkzhlm;
    }

    public void setLstdkzhlm(List<Lstdkzhlm> lstdkzhlm) {
        this.lstdkzhlm = lstdkzhlm;
    }

    public String getChanpmsh() {
        return chanpmsh;
    }

    public void setChanpmsh(String chanpmsh) {
        this.chanpmsh = chanpmsh;
    }

    public String getDaikdxxf() {
        return daikdxxf;
    }

    public void setDaikdxxf(String daikdxxf) {
        this.daikdxxf = daikdxxf;
    }

    public String getXunhdaik() {
        return xunhdaik;
    }

    public void setXunhdaik(String xunhdaik) {
        this.xunhdaik = xunhdaik;
    }

    public String getYansdaik() {
        return yansdaik;
    }

    public void setYansdaik(String yansdaik) {
        this.yansdaik = yansdaik;
    }

    public String getChendaik() {
        return chendaik;
    }

    public void setChendaik(String chendaik) {
        this.chendaik = chendaik;
    }

    public String getButidaik() {
        return butidaik;
    }

    public void setButidaik(String butidaik) {
        this.butidaik = butidaik;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getYintdkfs() {
        return yintdkfs;
    }

    public void setYintdkfs(String yintdkfs) {
        this.yintdkfs = yintdkfs;
    }

    public String getYintleib() {
        return yintleib;
    }

    public void setYintleib(String yintleib) {
        this.yintleib = yintleib;
    }

    public String getYintnbcy() {
        return yintnbcy;
    }

    public void setYintnbcy(String yintnbcy) {
        this.yintnbcy = yintnbcy;
    }

    public String getYintwbcy() {
        return yintwbcy;
    }

    public void setYintwbcy(String yintwbcy) {
        this.yintwbcy = yintwbcy;
    }

    public String getFangkulx() {
        return fangkulx;
    }

    public void setFangkulx(String fangkulx) {
        this.fangkulx = fangkulx;
    }

    public String getFkzjclfs() {
        return fkzjclfs;
    }

    public void setFkzjclfs(String fkzjclfs) {
        this.fkzjclfs = fkzjclfs;
    }

    public String getHntmifku() {
        return hntmifku;
    }

    public void setHntmifku(String hntmifku) {
        this.hntmifku = hntmifku;
    }

    public String getHnftmfku() {
        return hnftmfku;
    }

    public void setHnftmfku(String hnftmfku) {
        this.hnftmfku = hnftmfku;
    }

    public String getNeibufku() {
        return neibufku;
    }

    public void setNeibufku(String neibufku) {
        this.neibufku = neibufku;
    }

    public String getJixibjgz() {
        return jixibjgz;
    }

    public void setJixibjgz(String jixibjgz) {
        this.jixibjgz = jixibjgz;
    }

    public String getLixijsff() {
        return lixijsff;
    }

    public void setLixijsff(String lixijsff) {
        this.lixijsff = lixijsff;
    }

    public String getJixitwgz() {
        return jixitwgz;
    }

    public void setJixitwgz(String jixitwgz) {
        this.jixitwgz = jixitwgz;
    }

    public BigDecimal getJixizxje() {
        return jixizxje;
    }

    public void setJixizxje(BigDecimal jixizxje) {
        this.jixizxje = jixizxje;
    }

    public String getJixisrgz() {
        return jixisrgz;
    }

    public void setJixisrgz(String jixisrgz) {
        this.jixisrgz = jixisrgz;
    }

    public String getSrzxdanw() {
        return srzxdanw;
    }

    public void setSrzxdanw(String srzxdanw) {
        this.srzxdanw = srzxdanw;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getFdjixibz() {
        return fdjixibz;
    }

    public void setFdjixibz(String fdjixibz) {
        this.fdjixibz = fdjixibz;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getJitiguiz() {
        return jitiguiz;
    }

    public void setJitiguiz(String jitiguiz) {
        this.jitiguiz = jitiguiz;
    }

    public String getButijejs() {
        return butijejs;
    }

    public void setButijejs(String butijejs) {
        this.butijejs = butijejs;
    }

    public String getTiaozhkf() {
        return tiaozhkf;
    }

    public void setTiaozhkf(String tiaozhkf) {
        this.tiaozhkf = tiaozhkf;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public String getYunxsuoq() {
        return yunxsuoq;
    }

    public void setYunxsuoq(String yunxsuoq) {
        this.yunxsuoq = yunxsuoq;
    }

    public String getBzuekkfs() {
        return bzuekkfs;
    }

    public void setBzuekkfs(String bzuekkfs) {
        this.bzuekkfs = bzuekkfs;
    }

    public String getYqbzkkfs() {
        return yqbzkkfs;
    }

    public void setYqbzkkfs(String yqbzkkfs) {
        this.yqbzkkfs = yqbzkkfs;
    }

    public String getHntmihku() {
        return hntmihku;
    }

    public void setHntmihku(String hntmihku) {
        this.hntmihku = hntmihku;
    }

    public String getHnftmhku() {
        return hnftmhku;
    }

    public void setHnftmhku(String hnftmhku) {
        this.hnftmhku = hnftmhku;
    }

    public String getNbuzhhku() {
        return nbuzhhku;
    }

    public void setNbuzhhku(String nbuzhhku) {
        this.nbuzhhku = nbuzhhku;
    }

    public Integer getTiqhksdq() {
        return tiqhksdq;
    }

    public void setTiqhksdq(Integer tiqhksdq) {
        this.tiqhksdq = tiqhksdq;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public String getKxqjixgz() {
        return kxqjixgz;
    }

    public void setKxqjixgz(String kxqjixgz) {
        this.kxqjixgz = kxqjixgz;
    }

    public String getKxqhjxgz() {
        return kxqhjxgz;
    }

    public void setKxqhjxgz(String kxqhjxgz) {
        this.kxqhjxgz = kxqhjxgz;
    }

    public String getKxqshxgz() {
        return kxqshxgz;
    }

    public void setKxqshxgz(String kxqshxgz) {
        this.kxqshxgz = kxqshxgz;
    }

    public String getZhqizcqx() {
        return zhqizcqx;
    }

    public void setZhqizcqx(String zhqizcqx) {
        this.zhqizcqx = zhqizcqx;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getBwchapbz() {
        return bwchapbz;
    }

    public void setBwchapbz(String bwchapbz) {
        this.bwchapbz = bwchapbz;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getDkczhzhh() {
        return dkczhzhh;
    }

    public void setDkczhzhh(String dkczhzhh) {
        this.dkczhzhh = dkczhzhh;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public String getShtzfhxm() {
        return shtzfhxm;
    }

    public void setShtzfhxm(String shtzfhxm) {
        this.shtzfhxm = shtzfhxm;
    }

    public String getBwhesdma() {
        return bwhesdma;
    }

    public void setBwhesdma(String bwhesdma) {
        this.bwhesdma = bwhesdma;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    public String getYsxlyzhh() {
        return ysxlyzhh;
    }

    public void setYsxlyzhh(String ysxlyzhh) {
        this.ysxlyzhh = ysxlyzhh;
    }

    public String getYsxlyzxh() {
        return ysxlyzxh;
    }

    public void setYsxlyzxh(String ysxlyzxh) {
        this.ysxlyzxh = ysxlyzxh;
    }

    @Override
    public String toString() {
        return "Ln3030ReqDto{" +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "chuzhhao='" + chuzhhao + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "hetongje='" + hetongje + '\'' +
                "fkfangsh='" + fkfangsh + '\'' +
                "hxzdkkbz='" + hxzdkkbz + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "fkzhouqi='" + fkzhouqi + '\'' +
                "dzhifkjh='" + dzhifkjh + '\'' +
                "kxqjjrgz='" + kxqjjrgz + '\'' +
                "jixibzhi='" + jixibzhi + '\'' +
                "jfxibzhi='" + jfxibzhi + '\'' +
                "fxjxbzhi='" + fxjxbzhi + '\'' +
                "jixiguiz='" + jixiguiz + '\'' +
                "kxqzyqgz='" + kxqzyqgz + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "llqxkdfs='" + llqxkdfs + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "ltjixigz='" + ltjixigz + '\'' +
                "meictxfs='" + meictxfs + '\'' +
                "nyuelilv='" + nyuelilv + '\'' +
                "yuqinyll='" + yuqinyll + '\'' +
                "lilvfend='" + lilvfend + '\'' +
                "zaoqixbz='" + zaoqixbz + '\'' +
                "wanqixbz='" + wanqixbz + '\'' +
                "tiexibzh='" + tiexibzh + '\'' +
                "yushxfsh='" + yushxfsh + '\'' +
                "meictxbl='" + meictxbl + '\'' +
                "kxqzdcsh='" + kxqzdcsh + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                "yuqillbh='" + yuqillbh + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "zhxnlilv='" + zhxnlilv + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "yushxize='" + yushxize + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lixitxzq='" + lixitxzq + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "dechligz='" + dechligz + '\'' +
                "qigscfsh='" + qigscfsh + '\'' +
                "duophkbz='" + duophkbz + '\'' +
                "dzhhkjih='" + dzhhkjih + '\'' +
                "zdkoukbz='" + zdkoukbz + '\'' +
                "zdplkkbz='" + zdplkkbz + '\'' +
                "zdjqdkbz='" + zdjqdkbz + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "qyxhdkbz='" + qyxhdkbz + '\'' +
                "qxbgtzjh='" + qxbgtzjh + '\'' +
                "llbgtzjh='" + llbgtzjh + '\'' +
                "dcfktzjh='" + dcfktzjh + '\'' +
                "tqhktzjh='" + tqhktzjh + '\'' +
                "yunxtqhk='" + yunxtqhk + '\'' +
                "xycihkrq='" + xycihkrq + '\'' +
                "leijqjsh='" + leijqjsh + '\'' +
                "ljsxqish='" + ljsxqish + '\'' +
                "mqhbbili='" + mqhbbili + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "meiqhbje='" + meiqhbje + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "xhdkqyzh='" + xhdkqyzh + '\'' +
                "hkqixian='" + hkqixian + '\'' +
                "hkshxubh='" + hkshxubh + '\'' +
                "leijinzh='" + leijinzh + '\'' +
                "baoliuje='" + baoliuje + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "xhdkzhxh='" + xhdkzhxh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "huanbzhq='" + huanbzhq + '\'' +
                "yuqhkzhq='" + yuqhkzhq + '\'' +
                "wtrkehuh='" + wtrkehuh + '\'' +
                "wtrckuzh='" + wtrckuzh + '\'' +
                "wtckzhao='" + wtckzhao + '\'' +
                "bjghrzzh='" + bjghrzzh + '\'' +
                "lxghrzzh='" + lxghrzzh + '\'' +
                "dailimsh='" + dailimsh + '\'' +
                "wtrckzxh='" + wtrckzxh + '\'' +
                "wtckzixh='" + wtckzixh + '\'' +
                "bjghrzxh='" + bjghrzxh + '\'' +
                "lxghrzxh='" + lxghrzxh + '\'' +
                "baozjzxh='" + baozjzxh + '\'' +
                "duowtrbz='" + duowtrbz + '\'' +
                "zbjrsygz='" + zbjrsygz + '\'' +
                "zhqxzekk='" + zhqxzekk + '\'' +
                "dbdkkksx='" + dbdkkksx + '\'' +
                "yunxdkzq='" + yunxdkzq + '\'' +
                "zhqzdcsh='" + zhqzdcsh + '\'' +
                "zqgzbhao='" + zqgzbhao + '\'' +
                "hesuanfs='" + hesuanfs + '\'' +
                "lixizcgz='" + lixizcgz + '\'' +
                "lixizhgz='" + lixizhgz + '\'' +
                "yjfyjhes='" + yjfyjhes + '\'' +
                "yiyldhes='" + yiyldhes + '\'' +
                "dkxtkmhs='" + dkxtkmhs + '\'' +
                "zidxtzhy='" + zidxtzhy + '\'' +
                "shfleixi='" + shfleixi + '\'' +
                "tqhkfjbh='" + tqhkfjbh + '\'' +
                "tqhkfjmc='" + tqhkfjmc + '\'' +
                "tqhkfjfj='" + tqhkfjfj + '\'' +
                "zhchtqtz='" + zhchtqtz + '\'' +
                "yqcshtzh='" + yqcshtzh + '\'' +
                "lilvbgtz='" + lilvbgtz + '\'' +
                "yuebgtzh='" + yuebgtzh + '\'' +
                "tzhtqtsh='" + tzhtqtsh + '\'' +
                "tzhjgtsh='" + tzhjgtsh + '\'' +
                "pingzhzl='" + pingzhzl + '\'' +
                "sydkcnuo='" + sydkcnuo + '\'' +
                "lmdkbzhi='" + lmdkbzhi + '\'' +
                "bzhrdbbz='" + bzhrdbbz + '\'' +
                "wujiflbz='" + wujiflbz + '\'' +
                "wujiflrq='" + wujiflrq + '\'' +
                "cndkjjho='" + cndkjjho + '\'' +
                "dkgljgou='" + dkgljgou + '\'' +
                "gljgleib='" + gljgleib + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                "khjingli='" + khjingli + '\'' +
                "pingzhma='" + pingzhma + '\'' +
                "pngzxhao='" + pngzxhao + '\'' +
                "yinhshlv='" + yinhshlv + '\'' +
                "yinhshje='" + yinhshje + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "hetongll='" + hetongll + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "kuaijilb='" + kuaijilb + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "dkqixian='" + dkqixian + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "fafangje='" + fafangje + '\'' +
                "djiekfje='" + djiekfje + '\'' +
                "kffangje='" + kffangje + '\'' +
                "dkuanjij='" + dkuanjij + '\'' +
                "daikduix='" + daikduix + '\'' +
                "yewufenl='" + yewufenl + '\'' +
                "ysywleix='" + ysywleix + '\'' +
                "ysywbhao='" + ysywbhao + '\'' +
                "zhqifkbz='" + zhqifkbz + '\'' +
                "mcfkjebl='" + mcfkjebl + '\'' +
                "scjixirq='" + scjixirq + '\'' +
                "dlxxzdgz='" + dlxxzdgz + '\'' +
                "dlxxqzgz='" + dlxxqzgz + '\'' +
                "wtckywbm='" + wtckywbm + '\'' +
                "dailixuh='" + dailixuh + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "edbizhgz='" + edbizhgz + '\'' +
                "edzdbizh='" + edzdbizh + '\'' +
                "yewusx01='" + yewusx01 + '\'' +
                "yewusx02='" + yewusx02 + '\'' +
                "yewusx03='" + yewusx03 + '\'' +
                "yewusx04='" + yewusx04 + '\'' +
                "yewusx05='" + yewusx05 + '\'' +
                "yewusx06='" + yewusx06 + '\'' +
                "yewusx07='" + yewusx07 + '\'' +
                "yewusx08='" + yewusx08 + '\'' +
                "yewusx09='" + yewusx09 + '\'' +
                "yewusx10='" + yewusx10 + '\'' +
                "ywsxms01='" + ywsxms01 + '\'' +
                "ywsxms02='" + ywsxms02 + '\'' +
                "ywsxms03='" + ywsxms03 + '\'' +
                "ywsxms04='" + ywsxms04 + '\'' +
                "ywsxms05='" + ywsxms05 + '\'' +
                "ywsxms06='" + ywsxms06 + '\'' +
                "ywsxms07='" + ywsxms07 + '\'' +
                "ywsxms08='" + ywsxms08 + '\'' +
                "ywsxms09='" + ywsxms09 + '\'' +
                "ywsxms10='" + ywsxms10 + '\'' +
                "lstdkfkjh='" + lstdkfkjh + '\'' +
                "lstdkfwdj='" + lstdkfwdj + '\'' +
                "lstdkhbjh='" + lstdkhbjh + '\'' +
                "benjinfd='" + benjinfd + '\'' +
                "lstdkbjfd='" + lstdkbjfd + '\'' +
                "lstdkhkfs='" + lstdkhkfs + '\'' +
                "lstdkhkzh='" + lstdkhkzh + '\'' +
                "lstdkllfd='" + lstdkllfd + '\'' +
                "lstdksfsj='" + lstdksfsj + '\'' +
                "lstdkstzf='" + lstdkstzf + '\'' +
                "lstdktxzh='" + lstdktxzh + '\'' +
                "lstdkwtxx='" + lstdkwtxx + '\'' +
                "lstdkzhbz='" + lstdkzhbz + '\'' +
                "lstdkzhlm='" + lstdkzhlm + '\'' +
                "chanpmsh='" + chanpmsh + '\'' +
                "daikdxxf='" + daikdxxf + '\'' +
                "xunhdaik='" + xunhdaik + '\'' +
                "yansdaik='" + yansdaik + '\'' +
                "chendaik='" + chendaik + '\'' +
                "butidaik='" + butidaik + '\'' +
                "yintdkbz='" + yintdkbz + '\'' +
                "yintdkfs='" + yintdkfs + '\'' +
                "yintleib='" + yintleib + '\'' +
                "yintnbcy='" + yintnbcy + '\'' +
                "yintwbcy='" + yintwbcy + '\'' +
                "fangkulx='" + fangkulx + '\'' +
                "fkzjclfs='" + fkzjclfs + '\'' +
                "hntmifku='" + hntmifku + '\'' +
                "hnftmfku='" + hnftmfku + '\'' +
                "neibufku='" + neibufku + '\'' +
                "jixibjgz='" + jixibjgz + '\'' +
                "lixijsff='" + lixijsff + '\'' +
                "jixitwgz='" + jixitwgz + '\'' +
                "jixizxje='" + jixizxje + '\'' +
                "jixisrgz='" + jixisrgz + '\'' +
                "srzxdanw='" + srzxdanw + '\'' +
                "lilvqixx='" + lilvqixx + '\'' +
                "fdjixibz='" + fdjixibz + '\'' +
                "yuqitzfs='" + yuqitzfs + '\'' +
                "yuqitzzq='" + yuqitzzq + '\'' +
                "yqfxfdfs='" + yqfxfdfs + '\'' +
                "yqfxfdzh='" + yqfxfdzh + '\'' +
                "fulilvbh='" + fulilvbh + '\'' +
                "fulilvny='" + fulilvny + '\'' +
                "fulililv='" + fulililv + '\'' +
                "fulitzfs='" + fulitzfs + '\'' +
                "fulitzzq='" + fulitzzq + '\'' +
                "jitiguiz='" + jitiguiz + '\'' +
                "butijejs='" + butijejs + '\'' +
                "tiaozhkf='" + tiaozhkf + '\'' +
                "scihkrbz='" + scihkrbz + '\'' +
                "mqihkfsh='" + mqihkfsh + '\'' +
                "yunxsuoq='" + yunxsuoq + '\'' +
                "bzuekkfs='" + bzuekkfs + '\'' +
                "yqbzkkfs='" + yqbzkkfs + '\'' +
                "hntmihku='" + hntmihku + '\'' +
                "hnftmhku='" + hnftmhku + '\'' +
                "nbuzhhku='" + nbuzhhku + '\'' +
                "tiqhksdq='" + tiqhksdq + '\'' +
                "sfyxkuxq='" + sfyxkuxq + '\'' +
                "kuanxqts='" + kuanxqts + '\'' +
                "kxqjixgz='" + kxqjixgz + '\'' +
                "kxqhjxgz='" + kxqhjxgz + '\'' +
                "kxqshxgz='" + kxqshxgz + '\'' +
                "zhqizcqx='" + zhqizcqx + '\'' +
                "fulifdfs='" + fulifdfs + '\'' +
                "fulifdzh='" + fulifdzh + '\'' +
                "bwchapbz='" + bwchapbz + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "flllcklx='" + flllcklx + '\'' +
                "kshchpdm='" + kshchpdm + '\'' +
                "kshchpmc='" + kshchpmc + '\'' +
                "dkczhzhh='" + dkczhzhh + '\'' +
                "dkdbfshi='" + dkdbfshi + '\'' +
                "dkyongtu='" + dkyongtu + '\'' +
                "fkjzhfsh='" + fkjzhfsh + '\'' +
                "shtzfhxm='" + shtzfhxm + '\'' +
                "bwhesdma='" + bwhesdma + '\'' +
                "wtrmingc='" + wtrmingc + '\'' +
                "ysxlyzhh='" + ysxlyzhh + '\'' +
                "ysxlyzxh='" + ysxlyzxh + '\'' +
                '}';
    }
}  
