package cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：影像图像路径查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class YxljcxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "docidd")
    private String docidd;//查询索引值
    @JsonProperty(value = "tpcode")
    private String tpcode;//根节点
    @JsonProperty(value = "sncode")
    private String sncode;//子节点
    @JsonProperty(value = "imname")
    private String imname;//图像名
    @JsonProperty(value = "servtp")
    private String servtp;//渠道
    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "prcscd")
    private String prcscd;//处理码
    @JsonProperty(value = "rqtem1")
    private String rqtem1;//请求备用字段1：是否最新影像 [1：最新文件（时间），2：最新文件（相同名称情况下）]
    @JsonProperty(value = "rqtem2")
    private String rqtem2;//请求备用字段2：是否显示文件名[1：显示文件名]
    @JsonProperty(value = "rqtem3")
    private String rqtem3;//请求备用字段3
    @JsonProperty(value = "rqtem4")
    private String rqtem4;//请求备用字段4

    public String getDocidd() {
        return docidd;
    }

    public void setDocidd(String docidd) {
        this.docidd = docidd;
    }

    public String getTpcode() {
        return tpcode;
    }

    public void setTpcode(String tpcode) {
        this.tpcode = tpcode;
    }

    public String getSncode() {
        return sncode;
    }

    public void setSncode(String sncode) {
        this.sncode = sncode;
    }

    public String getImname() {
        return imname;
    }

    public void setImname(String imname) {
        this.imname = imname;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getRqtem1() {
        return rqtem1;
    }

    public void setRqtem1(String rqtem1) {
        this.rqtem1 = rqtem1;
    }

    public String getRqtem2() {
        return rqtem2;
    }

    public void setRqtem2(String rqtem2) {
        this.rqtem2 = rqtem2;
    }

    public String getRqtem3() {
        return rqtem3;
    }

    public void setRqtem3(String rqtem3) {
        this.rqtem3 = rqtem3;
    }

    public String getRqtem4() {
        return rqtem4;
    }

    public void setRqtem4(String rqtem4) {
        this.rqtem4 = rqtem4;
    }

    @Override
    public String toString() {
        return "YxljcxReqDto{" +
                "docidd='" + docidd + '\'' +
                "tpcode='" + tpcode + '\'' +
                "sncode='" + sncode + '\'' +
                "imname='" + imname + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "prcscd='" + prcscd + '\'' +
                "rqtem1='" + rqtem1 + '\'' +
                "rqtem2='" + rqtem2 + '\'' +
                "rqtem3='" + rqtem3 + '\'' +
                "rqtem4='" + rqtem4 + '\'' +
                '}';
    }
}  
