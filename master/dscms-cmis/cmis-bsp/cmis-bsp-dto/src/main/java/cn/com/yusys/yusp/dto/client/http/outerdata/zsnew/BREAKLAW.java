package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 严重违法
 */
@JsonPropertyOrder(alphabetic = true)
public class BREAKLAW implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "INDATE")
    private String INDATE;//  列入日期
    @JsonProperty(value = "INREASON")
    private String INREASON;//  列入原因
    @JsonProperty(value = "INREGORG")
    private String INREGORG;//  列入作出决定机关
    @JsonProperty(value = "OUTDATE")
    private String OUTDATE;//  移出日期
    @JsonProperty(value = "OUTREASON")
    private String OUTREASON;//  移出原因
    @JsonProperty(value = "OUTREGORG")
    private String OUTREGORG;//  移出作出决定机关

    @JsonIgnore
    public String getINDATE() {
        return INDATE;
    }

    @JsonIgnore
    public void setINDATE(String INDATE) {
        this.INDATE = INDATE;
    }

    @JsonIgnore
    public String getINREASON() {
        return INREASON;
    }

    @JsonIgnore
    public void setINREASON(String INREASON) {
        this.INREASON = INREASON;
    }

    @JsonIgnore
    public String getINREGORG() {
        return INREGORG;
    }

    @JsonIgnore
    public void setINREGORG(String INREGORG) {
        this.INREGORG = INREGORG;
    }

    @JsonIgnore
    public String getOUTDATE() {
        return OUTDATE;
    }

    @JsonIgnore
    public void setOUTDATE(String OUTDATE) {
        this.OUTDATE = OUTDATE;
    }

    @JsonIgnore
    public String getOUTREASON() {
        return OUTREASON;
    }

    @JsonIgnore
    public void setOUTREASON(String OUTREASON) {
        this.OUTREASON = OUTREASON;
    }

    @JsonIgnore
    public String getOUTREGORG() {
        return OUTREGORG;
    }

    @JsonIgnore
    public void setOUTREGORG(String OUTREGORG) {
        this.OUTREGORG = OUTREGORG;
    }

    @Override
    public String toString() {
        return "BREAKLAW{" +
                "INDATE='" + INDATE + '\'' +
                ", INREASON='" + INREASON + '\'' +
                ", INREGORG='" + INREGORG + '\'' +
                ", OUTDATE='" + OUTDATE + '\'' +
                ", OUTREASON='" + OUTREASON + '\'' +
                ", OUTREGORG='" + OUTREGORG + '\'' +
                '}';
    }
}
