package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：交易请求信息域:担保合同信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 10:10
 */
@JsonPropertyOrder(alphabetic = true)
public class GuaranteeContrctInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guar_cont_no")
    private String guar_cont_no; // 担保合同流水号
    @JsonProperty(value = "item_id")
    private String item_id; // 授信台账编号
    @JsonProperty(value = "lmt_serno")
    private String lmt_serno; // 授信协议编号
    @JsonProperty(value = "guar_cont_type")
    private String guar_cont_type; // 担保合同类型
    @JsonProperty(value = "guar_way")
    private String guar_way; // 担保方式
    @JsonProperty(value = "cur_type")
    private String cur_type; // 币种
    @JsonProperty(value = "guar_amt")
    private BigDecimal guar_amt; // 担保金额（元）
    @JsonProperty(value = "guar_start_date")
    private String guar_start_date; // 担保起始日
    @JsonProperty(value = "guar_end_date")
    private String guar_end_date; // 担保到期日
    @JsonProperty(value = "guar_cont_state")
    private String guar_cont_state; // 担保合同状态

    public String getGuar_cont_no() {
        return guar_cont_no;
    }

    public void setGuar_cont_no(String guar_cont_no) {
        this.guar_cont_no = guar_cont_no;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getGuar_cont_type() {
        return guar_cont_type;
    }

    public void setGuar_cont_type(String guar_cont_type) {
        this.guar_cont_type = guar_cont_type;
    }

    public String getGuar_way() {
        return guar_way;
    }

    public void setGuar_way(String guar_way) {
        this.guar_way = guar_way;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getGuar_amt() {
        return guar_amt;
    }

    public void setGuar_amt(BigDecimal guar_amt) {
        this.guar_amt = guar_amt;
    }

    public String getGuar_start_date() {
        return guar_start_date;
    }

    public void setGuar_start_date(String guar_start_date) {
        this.guar_start_date = guar_start_date;
    }

    public String getGuar_end_date() {
        return guar_end_date;
    }

    public void setGuar_end_date(String guar_end_date) {
        this.guar_end_date = guar_end_date;
    }

    public String getGuar_cont_state() {
        return guar_cont_state;
    }

    public void setGuar_cont_state(String guar_cont_state) {
        this.guar_cont_state = guar_cont_state;
    }

    @Override
    public String toString() {
        return "GuaranteeContrctInfo{" +
                "guar_cont_no='" + guar_cont_no + '\'' +
                ", item_id='" + item_id + '\'' +
                ", lmt_serno='" + lmt_serno + '\'' +
                ", guar_cont_type='" + guar_cont_type + '\'' +
                ", guar_way='" + guar_way + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", guar_amt=" + guar_amt +
                ", guar_start_date='" + guar_start_date + '\'' +
                ", guar_end_date='" + guar_end_date + '\'' +
                ", guar_cont_state='" + guar_cont_state + '\'' +
                '}';
    }
}
