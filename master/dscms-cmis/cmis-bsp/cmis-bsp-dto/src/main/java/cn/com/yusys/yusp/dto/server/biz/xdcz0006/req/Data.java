package cn.com.yusys.yusp.dto.server.biz.xdcz0006.req;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
/**
 * 请求Dto：出账记录详情查看
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "acctNo")
    private String acctNo;//账号
    @JsonProperty(value = "seq")
    private String seq;//批次号
    public String  getAcctNo() { return acctNo; }
    public void setAcctNo(String acctNo ) { this.acctNo = acctNo;}

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    @Override
    public String toString() {
        return "Data{" +
                "acctNo='" + acctNo + '\'' +
                ", seq='" + seq + '\'' +
                '}';
    }
}
