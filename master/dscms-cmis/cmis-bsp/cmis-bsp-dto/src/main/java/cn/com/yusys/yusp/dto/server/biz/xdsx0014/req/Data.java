package cn.com.yusys.yusp.dto.server.biz.xdsx0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 16:03:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/19 16:03
 * @since 2021/5/19 16:03
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "fx_serno")
    private String fx_serno;//授信协议分项编号
    @JsonProperty(value = "fx_type")
    private String fx_type;//授信协议分项类型
    @JsonProperty(value = "app_no")
    private String app_no;//流水号
    @JsonProperty(value = "fx_amt")
    private String fx_amt;//授信协议分项金额

    public String getFx_serno() {
        return fx_serno;
    }

    public void setFx_serno(String fx_serno) {
        this.fx_serno = fx_serno;
    }

    public String getFx_type() {
        return fx_type;
    }

    public void setFx_type(String fx_type) {
        this.fx_type = fx_type;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getFx_amt() {
        return fx_amt;
    }

    public void setFx_amt(String fx_amt) {
        this.fx_amt = fx_amt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "fx_serno='" + fx_serno + '\'' +
                ", fx_type='" + fx_type + '\'' +
                ", app_no='" + app_no + '\'' +
                ", fx_amt='" + fx_amt + '\'' +
                '}';
    }
}
