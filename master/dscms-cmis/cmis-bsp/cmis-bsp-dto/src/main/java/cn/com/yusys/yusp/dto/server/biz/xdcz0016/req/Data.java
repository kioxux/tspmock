package cn.com.yusys.yusp.dto.server.biz.xdcz0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：支用列表查询(微信小程序)
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "qryType")
    private String qryType;//查询类型

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getQryType() {
        return qryType;
    }

    public void setQryType(String qryType) {
        this.qryType = qryType;
    }

    @Override
    public String toString() {
        return "Xdcz0010ReqDto{" +
                "cusId='" + cusId + '\'' +
                "qryType='" + qryType + '\'' +
                '}';
    }

}
