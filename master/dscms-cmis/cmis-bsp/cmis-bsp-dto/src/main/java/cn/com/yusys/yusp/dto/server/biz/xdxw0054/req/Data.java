package cn.com.yusys.yusp.dto.server.biz.xdxw0054.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询优抵贷损益表明细
 * @Author zhangpeng
 * @Date 2021/4/24 10:10
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//客户调查表编号
    @JsonProperty(value = "itemValue")
    private String itemValue;//项目值

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    @Override
    public String toString() {
        return "Data{" +
                "indgtSerno='" + indgtSerno + '\'' +
                ", itemValue='" + itemValue + '\'' +
                '}';
    }
}
