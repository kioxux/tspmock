package cn.com.yusys.yusp.dto.server.biz.xdxw0034.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据流水号查询无还本续贷基本信息
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusNo")
    private String cusNo;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//客户证件号
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "loanTerm")
    private String loanTerm;//贷款期限
    @JsonProperty(value = "apprId")
    private String apprId;//审批人工号
    @JsonProperty(value = "spouseName")
    private String spouseName;//配偶姓名
    @JsonProperty(value = "riskMsg")
    private String riskMsg;//风险提示
    @JsonProperty(value = "phone")
    private String phone;//手机号
    @JsonProperty(value = "spousePhone")
    private String spousePhone;//配偶手机号

    public String getCusNo() {
        return cusNo;
    }

    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getApprId() {
        return apprId;
    }

    public void setApprId(String apprId) {
        this.apprId = apprId;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getRiskMsg() {
        return riskMsg;
    }

    public void setRiskMsg(String riskMsg) {
        this.riskMsg = riskMsg;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpousePhone() {
        return spousePhone;
    }

    public void setSpousePhone(String spousePhone) {
        this.spousePhone = spousePhone;
    }

    @Override
    public String toString() {
        return "Xdxw0034RespDto{" +
                "cusNo='" + cusNo + '\'' +
                "cusName='" + cusName + '\'' +
                "certNo='" + certNo + '\'' +
                "assureMeans='" + assureMeans + '\'' +
                "repayType='" + repayType + '\'' +
                "loanTerm='" + loanTerm + '\'' +
                "apprId='" + apprId + '\'' +
                "spouseName='" + spouseName + '\'' +
                "riskMsg='" + riskMsg + '\'' +
                "phone='" + phone + '\'' +
                "spousePhone='" + spousePhone + '\'' +
                '}';
    }
}  
