package cn.com.yusys.yusp.dto.server.biz.xdwb0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：省联社金融服务平台借据信息查询接口
 *
 * @author zcrbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "flowno")
    private String flowno;//银行业务流水号
    @JsonProperty(value = "loanauditing")
    private String loanauditing;//贷审结果  0审核通过，1审核拒绝
    @JsonProperty(value = "fprproname")
    private String fprproname;//对接产品名称
    @JsonProperty(value = "fprproid")
    private String fprproid;//对接产品id
    @JsonProperty(value = "creditstt")
    private String creditstt;//授信结果  0审核通过，1审核拒绝
    @JsonProperty(value = "creditno")
    private String creditno;//银行授信合同号
    @JsonProperty(value = "creloanflag")
    private String creloanflag;//是否首贷  0 否，1是
    @JsonProperty(value = "cstartdate")
    private String cstartdate;//授信有效期起
    @JsonProperty(value = "cenddate")
    private String cenddate;//授信有效期止
    @JsonProperty(value = "guatype")
    private String guatype;//担保类型0,抵押 1,质押 2,信保基金 3,一般保证 4,信用 5, 实际控制人夫妇提供个人连带担保 6,其他担保
    @JsonProperty(value = "reditamt")
    private BigDecimal reditamt;//授信金额（元）
    @JsonProperty(value = "guabranchname")
    private String guabranchname;//担保公司/保险公司名称
    @JsonProperty(value = "fcppollicynum")
    private String fcppollicynum;//担保合同号/保单号
    @JsonProperty(value = "fcpbeginrate")
    private String fcpbeginrate;//担保费率起
    @JsonProperty(value = "fcpendrate")
    private String fcpendrate;//担保费率止
    @JsonProperty(value = "fcpguastatime")
    private String fcpguastatime;//担保开始日期
    @JsonProperty(value = "fcpguaendtime")
    private String fcpguaendtime;//担保结束日期
    @JsonProperty(value = "fcpguaamt")
    private BigDecimal fcpguaamt;//担保金额（元）
    @JsonProperty(value = "fcpmeasures")
    private String fcpmeasures;//反担保措施
    @JsonProperty(value = "receiptno")
    private String receiptno;//银行放款合同号
    @JsonProperty(value = "loanamt")
    private BigDecimal loanamt;//放款金额（元）
    @JsonProperty(value = "loanrate")
    private BigDecimal loanrate;//执行年利率
    @JsonProperty(value = "lstartdate")
    private String lstartdate;//贷款起始日
    @JsonProperty(value = "lenddate")
    private String lenddate;//贷款到期日
    @JsonProperty(value = "loanlimit")
    private String loanlimit;//放款期限（月）
    @JsonProperty(value = "repaystt")
    private String repaystt;//还款状态 0:正常还款、1:续贷、2:逾期、3:其他
    @JsonProperty(value = "repaylatestt")
    private String repaylatestt;//逾期等级分类0：正常:/1：关注/2：次级/3：可疑/4：损失


    public String getFlowno() {
        return flowno;
    }

    public void setFlowno(String flowno) {
        this.flowno = flowno;
    }

    public String getLoanauditing() {
        return loanauditing;
    }

    public void setLoanauditing(String loanauditing) {
        this.loanauditing = loanauditing;
    }

    public String getFprproname() {
        return fprproname;
    }

    public void setFprproname(String fprproname) {
        this.fprproname = fprproname;
    }

    public String getFprproid() {
        return fprproid;
    }

    public void setFprproid(String fprproid) {
        this.fprproid = fprproid;
    }

    public String getCreditstt() {
        return creditstt;
    }

    public void setCreditstt(String creditstt) {
        this.creditstt = creditstt;
    }

    public String getCreditno() {
        return creditno;
    }

    public void setCreditno(String creditno) {
        this.creditno = creditno;
    }

    public String getCreloanflag() {
        return creloanflag;
    }

    public void setCreloanflag(String creloanflag) {
        this.creloanflag = creloanflag;
    }

    public String getCstartdate() {
        return cstartdate;
    }

    public void setCstartdate(String cstartdate) {
        this.cstartdate = cstartdate;
    }

    public String getCenddate() {
        return cenddate;
    }

    public void setCenddate(String cenddate) {
        this.cenddate = cenddate;
    }

    public String getGuatype() {
        return guatype;
    }

    public void setGuatype(String guatype) {
        this.guatype = guatype;
    }

    public BigDecimal getReditamt() {
        return reditamt;
    }

    public void setReditamt(BigDecimal reditamt) {
        this.reditamt = reditamt;
    }

    public String getGuabranchname() {
        return guabranchname;
    }

    public void setGuabranchname(String guabranchname) {
        this.guabranchname = guabranchname;
    }

    public String getFcppollicynum() {
        return fcppollicynum;
    }

    public void setFcppollicynum(String fcppollicynum) {
        this.fcppollicynum = fcppollicynum;
    }

    public String getFcpbeginrate() {
        return fcpbeginrate;
    }

    public void setFcpbeginrate(String fcpbeginrate) {
        this.fcpbeginrate = fcpbeginrate;
    }

    public String getFcpendrate() {
        return fcpendrate;
    }

    public void setFcpendrate(String fcpendrate) {
        this.fcpendrate = fcpendrate;
    }

    public String getFcpguastatime() {
        return fcpguastatime;
    }

    public void setFcpguastatime(String fcpguastatime) {
        this.fcpguastatime = fcpguastatime;
    }

    public String getFcpguaendtime() {
        return fcpguaendtime;
    }

    public void setFcpguaendtime(String fcpguaendtime) {
        this.fcpguaendtime = fcpguaendtime;
    }

    public BigDecimal getFcpguaamt() {
        return fcpguaamt;
    }

    public void setFcpguaamt(BigDecimal fcpguaamt) {
        this.fcpguaamt = fcpguaamt;
    }

    public String getFcpmeasures() {
        return fcpmeasures;
    }

    public void setFcpmeasures(String fcpmeasures) {
        this.fcpmeasures = fcpmeasures;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public BigDecimal getLoanamt() {
        return loanamt;
    }

    public void setLoanamt(BigDecimal loanamt) {
        this.loanamt = loanamt;
    }

    public BigDecimal getLoanrate() {
        return loanrate;
    }

    public void setLoanrate(BigDecimal loanrate) {
        this.loanrate = loanrate;
    }

    public String getLstartdate() {
        return lstartdate;
    }

    public void setLstartdate(String lstartdate) {
        this.lstartdate = lstartdate;
    }

    public String getLenddate() {
        return lenddate;
    }

    public void setLenddate(String lenddate) {
        this.lenddate = lenddate;
    }

    public String getLoanlimit() {
        return loanlimit;
    }

    public void setLoanlimit(String loanlimit) {
        this.loanlimit = loanlimit;
    }

    public String getRepaystt() {
        return repaystt;
    }

    public void setRepaystt(String repaystt) {
        this.repaystt = repaystt;
    }

    public String getRepaylatestt() {
        return repaylatestt;
    }

    public void setRepaylatestt(String repaylatestt) {
        this.repaylatestt = repaylatestt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "flowno='" + flowno + '\'' +
                ", loanauditing='" + loanauditing + '\'' +
                ", fprproname='" + fprproname + '\'' +
                ", fprproid='" + fprproid + '\'' +
                ", creditstt='" + creditstt + '\'' +
                ", creditno='" + creditno + '\'' +
                ", creloanflag='" + creloanflag + '\'' +
                ", cstartdate='" + cstartdate + '\'' +
                ", cenddate='" + cenddate + '\'' +
                ", guatype='" + guatype + '\'' +
                ", reditamt='" + reditamt + '\'' +
                ", guabranchname='" + guabranchname + '\'' +
                ", fcppollicynum='" + fcppollicynum + '\'' +
                ", fcpbeginrate='" + fcpbeginrate + '\'' +
                ", fcpendrate='" + fcpendrate + '\'' +
                ", fcpguastatime='" + fcpguastatime + '\'' +
                ", fcpguaendtime='" + fcpguaendtime + '\'' +
                ", fcpguaamt=" + fcpguaamt +
                ", fcpmeasures='" + fcpmeasures + '\'' +
                ", receiptno='" + receiptno + '\'' +
                ", loanamt=" + loanamt +
                ", loanrate=" + loanrate +
                ", lstartdate='" + lstartdate + '\'' +
                ", lenddate='" + lenddate + '\'' +
                ", loanlimit=" + loanlimit +
                ", repaystt='" + repaystt + '\'' +
                ", repaylatestt='" + repaylatestt + '\'' +
                '}';
    }
}
