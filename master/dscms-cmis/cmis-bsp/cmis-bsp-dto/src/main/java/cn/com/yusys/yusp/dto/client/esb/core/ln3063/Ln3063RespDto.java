package cn.com.yusys.yusp.dto.client.esb.core.ln3063;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：资产转让处理
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3063RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "bjyskzhh")
    private String bjyskzhh;//其他应收款账号(本金)
    @JsonProperty(value = "qyskzhzh")
    private String qyskzhzh;//其他应收款账号子序号(本金)
    @JsonProperty(value = "benjinje")
    private BigDecimal benjinje;//本金金额
    @JsonProperty(value = "lxyskzhh")
    private String lxyskzhh;//其他应收款账号(利息)
    @JsonProperty(value = "lxyskzxh")
    private String lxyskzxh;//其他应收款账号子序号(利息)
    @JsonProperty(value = "lixijine")
    private BigDecimal lixijine;//利息金额
    @JsonProperty(value = "bjysfzhh")
    private String bjysfzhh;//其他应付款账号(本金)
    @JsonProperty(value = "bjysfzxh")
    private String bjysfzxh;//其他应付款账号子序号(本金)
    @JsonProperty(value = "yinghkbj")
    private BigDecimal yinghkbj;//应还本金
    @JsonProperty(value = "lxysfzhh")
    private String lxysfzhh;//其他应付款账号(利息)
    @JsonProperty(value = "qtyfzhzx")
    private String qtyfzhzx;//其他应付款账号子序号(利息)
    @JsonProperty(value = "yinghklx")
    private BigDecimal yinghklx;//应还利息
    @JsonProperty(value = "zongbish")
    private long zongbish;//总笔数
    @JsonProperty(value = "benjheji")
    private BigDecimal benjheji;//本金合计
    @JsonProperty(value = "lixiheji")
    private BigDecimal lixiheji;//利息合计
    @JsonProperty(value = "zjlyzhao")
    private String zjlyzhao;//资金来源账号
    @JsonProperty(value = "zjlyzzxh")
    private String zjlyzzxh;//资金来源账号子序号
    @JsonProperty(value = "zrfukzhh")
    private String zrfukzhh;//对外付款账号
    @JsonProperty(value = "zrfukzxh")
    private String zrfukzxh;//对外付款账号子序号
    @JsonProperty(value = "zjguijbz")
    private String zjguijbz;//自动归集标志
    @JsonProperty(value = "zrfkzoqi")
    private String zrfkzoqi;//付款周期
    @JsonProperty(value = "xcfkriqi")
    private String xcfkriqi;//下次付款日期
    @JsonProperty(value = "zrjjfshi")
    private String zrjjfshi;//转让计价方式

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getBjyskzhh() {
        return bjyskzhh;
    }

    public void setBjyskzhh(String bjyskzhh) {
        this.bjyskzhh = bjyskzhh;
    }

    public String getQyskzhzh() {
        return qyskzhzh;
    }

    public void setQyskzhzh(String qyskzhzh) {
        this.qyskzhzh = qyskzhzh;
    }

    public BigDecimal getBenjinje() {
        return benjinje;
    }

    public void setBenjinje(BigDecimal benjinje) {
        this.benjinje = benjinje;
    }

    public String getLxyskzhh() {
        return lxyskzhh;
    }

    public void setLxyskzhh(String lxyskzhh) {
        this.lxyskzhh = lxyskzhh;
    }

    public String getLxyskzxh() {
        return lxyskzxh;
    }

    public void setLxyskzxh(String lxyskzxh) {
        this.lxyskzxh = lxyskzxh;
    }

    public BigDecimal getLixijine() {
        return lixijine;
    }

    public void setLixijine(BigDecimal lixijine) {
        this.lixijine = lixijine;
    }

    public String getBjysfzhh() {
        return bjysfzhh;
    }

    public void setBjysfzhh(String bjysfzhh) {
        this.bjysfzhh = bjysfzhh;
    }

    public String getBjysfzxh() {
        return bjysfzxh;
    }

    public void setBjysfzxh(String bjysfzxh) {
        this.bjysfzxh = bjysfzxh;
    }

    public BigDecimal getYinghkbj() {
        return yinghkbj;
    }

    public void setYinghkbj(BigDecimal yinghkbj) {
        this.yinghkbj = yinghkbj;
    }

    public String getLxysfzhh() {
        return lxysfzhh;
    }

    public void setLxysfzhh(String lxysfzhh) {
        this.lxysfzhh = lxysfzhh;
    }

    public String getQtyfzhzx() {
        return qtyfzhzx;
    }

    public void setQtyfzhzx(String qtyfzhzx) {
        this.qtyfzhzx = qtyfzhzx;
    }

    public BigDecimal getYinghklx() {
        return yinghklx;
    }

    public void setYinghklx(BigDecimal yinghklx) {
        this.yinghklx = yinghklx;
    }

    public long getZongbish() {
        return zongbish;
    }

    public void setZongbish(long zongbish) {
        this.zongbish = zongbish;
    }

    public BigDecimal getBenjheji() {
        return benjheji;
    }

    public void setBenjheji(BigDecimal benjheji) {
        this.benjheji = benjheji;
    }

    public BigDecimal getLixiheji() {
        return lixiheji;
    }

    public void setLixiheji(BigDecimal lixiheji) {
        this.lixiheji = lixiheji;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    public String getZjguijbz() {
        return zjguijbz;
    }

    public void setZjguijbz(String zjguijbz) {
        this.zjguijbz = zjguijbz;
    }

    public String getZrfkzoqi() {
        return zrfkzoqi;
    }

    public void setZrfkzoqi(String zrfkzoqi) {
        this.zrfkzoqi = zrfkzoqi;
    }

    public String getXcfkriqi() {
        return xcfkriqi;
    }

    public void setXcfkriqi(String xcfkriqi) {
        this.xcfkriqi = xcfkriqi;
    }

    public String getZrjjfshi() {
        return zrjjfshi;
    }

    public void setZrjjfshi(String zrjjfshi) {
        this.zrjjfshi = zrjjfshi;
    }

    @Override
    public String toString() {
        return "Ln3063RespDto{" +
                "xieybhao='" + xieybhao + '\'' +
                "xieyimch='" + xieyimch + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "bjyskzhh='" + bjyskzhh + '\'' +
                "qyskzhzh='" + qyskzhzh + '\'' +
                "benjinje='" + benjinje + '\'' +
                "lxyskzhh='" + lxyskzhh + '\'' +
                "lxyskzxh='" + lxyskzxh + '\'' +
                "lixijine='" + lixijine + '\'' +
                "bjysfzhh='" + bjysfzhh + '\'' +
                "bjysfzxh='" + bjysfzxh + '\'' +
                "yinghkbj='" + yinghkbj + '\'' +
                "lxysfzhh='" + lxysfzhh + '\'' +
                "qtyfzhzx='" + qtyfzhzx + '\'' +
                "yinghklx='" + yinghklx + '\'' +
                "zongbish='" + zongbish + '\'' +
                "benjheji='" + benjheji + '\'' +
                "lixiheji='" + lixiheji + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "zrfukzhh='" + zrfukzhh + '\'' +
                "zrfukzxh='" + zrfukzxh + '\'' +
                "zjguijbz='" + zjguijbz + '\'' +
                "zrfkzoqi='" + zrfkzoqi + '\'' +
                "xcfkriqi='" + xcfkriqi + '\'' +
                "zrjjfshi='" + zrjjfshi + '\'' +
                '}';
    }
}  
