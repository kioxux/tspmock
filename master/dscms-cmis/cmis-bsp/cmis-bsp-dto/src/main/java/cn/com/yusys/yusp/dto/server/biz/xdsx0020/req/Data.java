package cn.com.yusys.yusp.dto.server.biz.xdsx0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contno")
    private String contno;//组织机构代码

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contno='" + contno + '\'' +
                '}';
    }
}
