package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

//抵押信息
@JsonPropertyOrder(alphabetic = true)
public class MortgageInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "mortgage_certificate_number")
    private String mortgage_certificate_number;//不动产登记证明号
    @JsonProperty(value = "contract_no")
    private String contract_no;//抵押合同编号
    @JsonProperty(value = "collateral_value")
    private BigDecimal collateral_value;//抵押物价值
    @JsonProperty(value = "debt_amount")
    private BigDecimal debt_amount;//被担保债权数额
    @JsonProperty(value = "mortgage_method")
    private MortgageMethod mortgage_method;//抵押方式
    @JsonProperty(value = "guarantee_scope")
    private String guarantee_scope;//担保范围
    @JsonProperty(value = "debt_start_time")
    private String debt_start_time;//债务开始时间
    @JsonProperty(value = "debt_end_time")
    private String debt_end_time;//债务结束时间
    @JsonProperty(value = "debtor")
    private Debtor debtor;//债务人
    @JsonProperty(value = "order")
    private String order;//抵押顺位
    @JsonProperty(value = "change_content")
    private String change_content;//抵押变更内容
    @JsonProperty(value = "has_garage")
    private Boolean has_garage;//是否包含车库
    @JsonProperty(value = "has_loft")
    private Boolean has_loft;//是否包含阁楼

    public String getMortgage_certificate_number() {
        return mortgage_certificate_number;
    }

    public void setMortgage_certificate_number(String mortgage_certificate_number) {
        this.mortgage_certificate_number = mortgage_certificate_number;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public BigDecimal getCollateral_value() {
        return collateral_value;
    }

    public void setCollateral_value(BigDecimal collateral_value) {
        this.collateral_value = collateral_value;
    }

    public BigDecimal getDebt_amount() {
        return debt_amount;
    }

    public void setDebt_amount(BigDecimal debt_amount) {
        this.debt_amount = debt_amount;
    }

    public MortgageMethod getMortgage_method() {
        return mortgage_method;
    }

    public void setMortgage_method(MortgageMethod mortgage_method) {
        this.mortgage_method = mortgage_method;
    }

    public String getGuarantee_scope() {
        return guarantee_scope;
    }

    public void setGuarantee_scope(String guarantee_scope) {
        this.guarantee_scope = guarantee_scope;
    }

    public String getDebt_start_time() {
        return debt_start_time;
    }

    public void setDebt_start_time(String debt_start_time) {
        this.debt_start_time = debt_start_time;
    }

    public String getDebt_end_time() {
        return debt_end_time;
    }

    public void setDebt_end_time(String debt_end_time) {
        this.debt_end_time = debt_end_time;
    }

    public Debtor getDebtor() {
        return debtor;
    }

    public void setDebtor(Debtor debtor) {
        this.debtor = debtor;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getChange_content() {
        return change_content;
    }

    public void setChange_content(String change_content) {
        this.change_content = change_content;
    }

    public Boolean getHas_garage() {
        return has_garage;
    }

    public void setHas_garage(Boolean has_garage) {
        this.has_garage = has_garage;
    }

    public Boolean getHas_loft() {
        return has_loft;
    }

    public void setHas_loft(Boolean has_loft) {
        this.has_loft = has_loft;
    }

    @Override
    public String toString() {
        return "MortgageInfo{" +
                "mortgage_certificate_number='" + mortgage_certificate_number + '\'' +
                ", contract_no='" + contract_no + '\'' +
                ", collateral_value=" + collateral_value +
                ", debt_amount=" + debt_amount +
                ", mortgage_method=" + mortgage_method +
                ", guarantee_scope='" + guarantee_scope + '\'' +
                ", debt_start_time='" + debt_start_time + '\'' +
                ", debt_end_time='" + debt_end_time + '\'' +
                ", debtor=" + debtor +
                ", order='" + order + '\'' +
                ", change_content='" + change_content + '\'' +
                ", has_garage=" + has_garage +
                ", has_loft=" + has_loft +
                '}';
    }
}
