package cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：买方资金存入
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class MfzjcrRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xybhsq ")
    private String xybhsq;//协议编号
    @JsonProperty(value = "acctto ")
    private String acctto;//托管账号
    @JsonProperty(value = "acctta ")
    private String acctta;//托管账户名称
    @JsonProperty(value = "jkcs")
    private String jkcs;//交款次数

    public String getXybhsq() {
        return xybhsq;
    }

    public void setXybhsq(String xybhsq) {
        this.xybhsq = xybhsq;
    }

    public String getAcctto() {
        return acctto;
    }

    public void setAcctto(String acctto) {
        this.acctto = acctto;
    }

    public String getAcctta() {
        return acctta;
    }

    public void setAcctta(String acctta) {
        this.acctta = acctta;
    }

    public String getJkcs() {
        return jkcs;
    }

    public void setJkcs(String jkcs) {
        this.jkcs = jkcs;
    }

    @Override
    public String toString() {
        return "MfzjcrRespDto{" +
                "xybhsq ='" + xybhsq + '\'' +
                "acctto ='" + acctto + '\'' +
                "acctta ='" + acctta + '\'' +
                "jkcs='" + jkcs + '\'' +
                '}';
    }
}  
