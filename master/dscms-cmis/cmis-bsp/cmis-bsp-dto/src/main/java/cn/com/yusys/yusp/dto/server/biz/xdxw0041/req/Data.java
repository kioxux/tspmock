package cn.com.yusys.yusp.dto.server.biz.xdxw0041.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：小企业无还本续贷审批结果维护
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "app_type")
	private String app_type;//申请类型
	@JsonProperty(value = "crd_type")
	private String crd_type;//续贷主体类型
	@JsonProperty(value = "crd_cont_no")
	private String crd_cont_no;//授信合同编号
	@JsonProperty(value = "bill_no")
	private String bill_no;//借据编号
	@JsonProperty(value = "cust_id")
	private String cust_id;//个人客户号
	@JsonProperty(value = "ent_cust_id")
	private String ent_cust_id;//企业核心客户号
	@JsonProperty(value = "manager_id")
	private String manager_id;//管户经理编号
	@JsonProperty(value = "approval_result")
	private String approval_result;//审批结果
	@JsonProperty(value = "rule01")
	private String rule01;//行内贷款无欠本欠息情况
	@JsonProperty(value = "rule02")
	private String rule02;//本行或他行经营性贷款无欠本欠息情况
	@JsonProperty(value = "rule03")
	private String rule03;//原周转贷款分类为正常类
	@JsonProperty(value = "rule04")
	private String rule04;//企业工商信息状态正常，且无工商处罚记录
	@JsonProperty(value = "rule05")
	private String rule05;//个人和企业无失信被执行情况
	@JsonProperty(value = "rule06")
	private String rule06;//授信评审部提供的信贷客户内部分类
	@JsonProperty(value = "rule07")
	private String rule07;//内部评级为BBB级以上
	public String  getApp_type() { return app_type; }
	public void setApp_type(String app_type ) { this.app_type = app_type;}
	public String  getCrd_type() { return crd_type; }
	public void setCrd_type(String crd_type ) { this.crd_type = crd_type;}
	public String  getCrd_cont_no() { return crd_cont_no; }
	public void setCrd_cont_no(String crd_cont_no ) { this.crd_cont_no = crd_cont_no;}
	public String  getBill_no() { return bill_no; }
	public void setBill_no(String bill_no ) { this.bill_no = bill_no;}
	public String  getCust_id() { return cust_id; }
	public void setCust_id(String cust_id ) { this.cust_id = cust_id;}
	public String  getEnt_cust_id() { return ent_cust_id; }
	public void setEnt_cust_id(String ent_cust_id ) { this.ent_cust_id = ent_cust_id;}
	public String  getManager_id() { return manager_id; }
	public void setManager_id(String manager_id ) { this.manager_id = manager_id;}
	public String  getApproval_result() { return approval_result; }
	public void setApproval_result(String approval_result ) { this.approval_result = approval_result;}
	public String  getRule01() { return rule01; }
	public void setRule01(String rule01 ) { this.rule01 = rule01;}
	public String  getRule02() { return rule02; }
	public void setRule02(String rule02 ) { this.rule02 = rule02;}
	public String  getRule03() { return rule03; }
	public void setRule03(String rule03 ) { this.rule03 = rule03;}
	public String  getRule04() { return rule04; }
	public void setRule04(String rule04 ) { this.rule04 = rule04;}
	public String  getRule05() { return rule05; }
	public void setRule05(String rule05 ) { this.rule05 = rule05;}
	public String  getRule06() { return rule06; }
	public void setRule06(String rule06 ) { this.rule06 = rule06;}
	public String  getRule07() { return rule07; }
	public void setRule07(String rule07 ) { this.rule07 = rule07;}
	@Override
	public String toString() {
		return "Data{" +
				"app_type='" + app_type+ '\'' +
				"crd_type='" + crd_type+ '\'' +
				"crd_cont_no='" + crd_cont_no+ '\'' +
				"bill_no='" + bill_no+ '\'' +
				"cust_id='" + cust_id+ '\'' +
				"ent_cust_id='" + ent_cust_id+ '\'' +
				"manager_id='" + manager_id+ '\'' +
				"approval_result='" + approval_result+ '\'' +
				"rule01='" + rule01+ '\'' +
				"rule02='" + rule02+ '\'' +
				"rule03='" + rule03+ '\'' +
				"rule04='" + rule04+ '\'' +
				"rule05='" + rule05+ '\'' +
				"rule06='" + rule06+ '\'' +
				"rule07='" + rule07+ '\'' +
				'}';
	}
}  
