package cn.com.yusys.yusp.dto.client.esb.ecif.g11004;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：客户集团信息查询（new）接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 21:24
 */
@JsonPropertyOrder(alphabetic = true)
public class G11004ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "resotp")
    private String resotp; // 识别方式
    @JsonProperty(value = "bginnm")
    private String bginnm; // 起始笔数
    @JsonProperty(value = "custno")
    private String custno; // 客户编号

    public String getResotp() {
        return resotp;
    }

    public void setResotp(String resotp) {
        this.resotp = resotp;
    }

    public String getBginnm() {
        return bginnm;
    }

    public void setBginnm(String bginnm) {
        this.bginnm = bginnm;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    @Override
    public String toString() {
        return "G11004ReqDto{" +
                "resotp='" + resotp + '\'' +
                ", bginnm='" + bginnm + '\'' +
                ", custno='" + custno + '\'' +
                '}';
    }
}
