package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询银票出账保证金账号信息（新信贷调票据）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj21RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "acctno")
    private String acctno;//账号
    @JsonProperty(value = "acctseq")
    private String acctseq;//保证金账号子序号
    @JsonProperty(value = "currency")
    private String currency;//币种
    @JsonProperty(value = "acctAmt")
    private BigDecimal acctAmt;//账户金额
    @JsonProperty(value = "interestMode")
    private String interestMode;//计息方式
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp.List> list;

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctseq() {
        return acctseq;
    }

    public void setAcctseq(String acctseq) {
        this.acctseq = acctseq;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAcctAmt() {
        return acctAmt;
    }

    public void setAcctAmt(BigDecimal acctAmt) {
        this.acctAmt = acctAmt;
    }

    public String getInterestMode() {
        return interestMode;
    }

    public void setInterestMode(String interestMode) {
        this.interestMode = interestMode;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdpj21RespDto{" +
                "acctno='" + acctno + '\'' +
                ", acctseq='" + acctseq + '\'' +
                ", currency='" + currency + '\'' +
                ", acctAmt=" + acctAmt +
                ", interestMode='" + interestMode + '\'' +
                ", list=" + list +
                '}';
    }
}
