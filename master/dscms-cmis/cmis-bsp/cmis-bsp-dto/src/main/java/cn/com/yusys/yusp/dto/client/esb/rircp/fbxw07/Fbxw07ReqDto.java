package cn.com.yusys.yusp.dto.client.esb.rircp.fbxw07;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：优惠利率申请接口接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/16下午4:46:23
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxw07ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "prd_name")
    private String prd_name; // 产品名称
    @JsonProperty(value = "cert_type")
    private String cert_type; // 证件类型
    @JsonProperty(value = "cert_code")
    private String cert_code; // 证件号码
    @JsonProperty(value = "cust_name")
    private String cust_name; // 客户姓名
    @JsonProperty(value = "loan_investment")
    private String loan_investment; // 贷款投向
    @JsonProperty(value = "guarantee_method")
    private String guarantee_method; // 主担保方式
    @JsonProperty(value = "collateral_pledge_type")
    private String collateral_pledge_type; // 抵押/质押类型
    @JsonProperty(value = "business_type")
    private String business_type; // 业务种类
    @JsonProperty(value = "rate")
    private String rate; // 优惠利率
    @JsonProperty(value = "survey_serno")
    private String survey_serno; // 调查表编号

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getLoan_investment() {
        return loan_investment;
    }

    public void setLoan_investment(String loan_investment) {
        this.loan_investment = loan_investment;
    }

    public String getGuarantee_method() {
        return guarantee_method;
    }

    public void setGuarantee_method(String guarantee_method) {
        this.guarantee_method = guarantee_method;
    }

    public String getCollateral_pledge_type() {
        return collateral_pledge_type;
    }

    public void setCollateral_pledge_type(String collateral_pledge_type) {
        this.collateral_pledge_type = collateral_pledge_type;
    }

    public String getBusiness_type() {
        return business_type;
    }

    public void setBusiness_type(String business_type) {
        this.business_type = business_type;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    @Override
    public String toString() {
        return "Fbxw07ReqDto{" +
                ", prd_name='" + prd_name + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", loan_investment='" + loan_investment + '\'' +
                ", guarantee_method='" + guarantee_method + '\'' +
                ", collateral_pledge_type='" + collateral_pledge_type + '\'' +
                ", business_type='" + business_type + '\'' +
                ", rate='" + rate + '\'' +
                ", survey_serno='" + survey_serno + '\'' +
                '}';
    }
}
