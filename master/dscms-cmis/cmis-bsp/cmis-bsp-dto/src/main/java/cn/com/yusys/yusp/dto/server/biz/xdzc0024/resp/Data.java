package cn.com.yusys.yusp.dto.server.biz.xdzc0024.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：票据池资料补全查询
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "count")
    private String count;//总记录条数
    @JsonProperty(value = "list")
    private java.util.List<List> list;//list

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdzc0024DataRespDto{" +
                "count='" + count + '\'' +
                ", list=" + list +
                '}';
    }
}
