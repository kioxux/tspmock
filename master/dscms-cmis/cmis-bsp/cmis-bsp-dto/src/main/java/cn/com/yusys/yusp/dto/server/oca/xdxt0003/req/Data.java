package cn.com.yusys.yusp.dto.server.oca.xdxt0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 15:15:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 15:15
 * @since 2021/5/24 15:15
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称
    @JsonProperty(value = "pageNum")
    private String pageNum;//页码
    @JsonProperty(value = "pageSize")
    private String pageSize;//一页查询数量

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Xdxt0003ReqDto{" +
                "managerId='" + managerId + '\'' +
                "managerName='" + managerName + '\'' +
                "pageNum='" + pageNum + '\'' +
                "pageSize='" + pageSize + '\'' +
                '}';
    }
}
