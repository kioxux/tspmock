package cn.com.yusys.yusp.dto.client.esb.circp.fb1146.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：受托信息审核
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1146ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "app_no")
    private String app_no;//申请流水号
    @JsonProperty(value = "vrify_task")
    private String vrify_task;//受托审核状态
    @JsonProperty(value = "exec_rate")
    private BigDecimal exec_rate;

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getVrify_task() {
        return vrify_task;
    }

    public void setVrify_task(String vrify_task) {
        this.vrify_task = vrify_task;
    }

    public BigDecimal getExec_rate() {
        return exec_rate;
    }

    public void setExec_rate(BigDecimal exec_rate) {
        this.exec_rate = exec_rate;
    }

    @Override
    public String toString() {
        return "Fb1146ReqDto{" +
                "app_no='" + app_no + '\'' +
                ", vrify_task='" + vrify_task + '\'' +
                ", exec_rate=" + exec_rate +
                '}';
    }
}
