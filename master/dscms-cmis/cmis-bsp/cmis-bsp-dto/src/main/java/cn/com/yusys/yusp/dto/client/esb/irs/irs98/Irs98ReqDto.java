package cn.com.yusys.yusp.dto.client.esb.irs.irs98;

import cn.com.yusys.yusp.dto.client.esb.irs.common.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;


/**
 * 请求DTO：授信申请债项评级
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月15日 上午11:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs98ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "userid")
    private String userid;//请求发起人编号
    @JsonProperty(value = "username")
    private String username;//	请求发起人名称
    @JsonProperty(value = "brchno")
    private String brchno;//请求机构编号
    @JsonProperty(value = "brchnm")
    private String brchnm;//请求机构名称
    @JsonProperty(value = "LimitApplyInfo")
    private List<LimitApplyInfo> LimitApplyInfo; // 综合授信申请信息 LimitApplyInfo
    @JsonProperty(value = "LimitDetailsInfo")
    private List<LimitDetailsInfo> LimitDetailsInfo; // 分项额度信息 LimitDetailsInfo
    @JsonProperty(value = "LimitProductInfo")
    private List<LimitProductInfo> LimitProductInfo; // 产品额度信息 LimitProductInfo
    @JsonProperty(value = "BusinessContractInfo")
    private List<BusinessContractInfo> BusinessContractInfo; // 合同信息 BusinessContractInfo
    @JsonProperty(value = "AccLoanInfo")
    private List<AccLoanInfo> AccLoanInfo; // 非垫款借据信息 AccLoanInfo
    @JsonProperty(value = "GuaranteeContrctInfo")
    private List<GuaranteeContrctInfo> GuaranteeContrctInfo; // 担保合同信息 GuaranteeContrctInfo
    @JsonProperty(value = "PledgeInfo")
    private List<PledgeInfo> PledgeInfo; // 质押物信息 PledgeInfo
    @JsonProperty(value = "MortgageInfo")
    private List<MortgageInfo> MortgageInfo; // 抵押物信息 MortgageInfo
    @JsonProperty(value = "AssurePersonInfo")
    private List<AssurePersonInfo> AssurePersonInfo; // 保证人信息 AssurePersonInfo
    @JsonProperty(value = "AssureAccInfo")
    private List<AssureAccInfo> AssureAccInfo; // 保证金信息 AssureAccInfo
    @JsonProperty(value = "CustomerInfo")
    private List<CustomerInfo> CustomerInfo; // 客户信息 CustomerInfo
    @JsonProperty(value = "DealCustomerInfo")
    private List<DealCustomerInfo> DealCustomerInfo; // 交易对手信息 DealCustomerInfo
    @JsonProperty(value = "LimitPleMortInfo")
    private List<LimitPleMortInfo> LimitPleMortInfo; // 授信分项额度与抵质押、保证人关联信息	LimitPleMortInfo
    @JsonProperty(value = "BusinessAssureInfo")
    private List<BusinessAssureInfo> BusinessAssureInfo; // 担保合同与合同关联信息 BusinessAssureInfo
    @JsonProperty(value = "GuaranteePleMortInfo")
    private List<GuaranteePleMortInfo> GuaranteePleMortInfo; // 担保合同与抵质押、保证人关联信息 GuaranteePleMortInfo
    @JsonProperty(value = "UpApplyInfoUpApplyInfo")
    private List<UpApplyInfo> UpApplyInfo; // 最高额授信协议信息 UpApplyInfo
    @JsonProperty(value = "CurInfo")
    private List<CurInfo> CurInfo; // 汇率信息 CurInfo

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getBrchnm() {
        return brchnm;
    }

    public void setBrchnm(String brchnm) {
        this.brchnm = brchnm;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo> getLimitApplyInfo() {
        return LimitApplyInfo;
    }

    public void setLimitApplyInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo> limitApplyInfo) {
        LimitApplyInfo = limitApplyInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo> getLimitDetailsInfo() {
        return LimitDetailsInfo;
    }

    public void setLimitDetailsInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo> limitDetailsInfo) {
        LimitDetailsInfo = limitDetailsInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo> getLimitProductInfo() {
        return LimitProductInfo;
    }

    public void setLimitProductInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo> limitProductInfo) {
        LimitProductInfo = limitProductInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessContractInfo> getBusinessContractInfo() {
        return BusinessContractInfo;
    }

    public void setBusinessContractInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessContractInfo> businessContractInfo) {
        BusinessContractInfo = businessContractInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.AccLoanInfo> getAccLoanInfo() {
        return AccLoanInfo;
    }

    public void setAccLoanInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.AccLoanInfo> accLoanInfo) {
        AccLoanInfo = accLoanInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteeContrctInfo> getGuaranteeContrctInfo() {
        return GuaranteeContrctInfo;
    }

    public void setGuaranteeContrctInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteeContrctInfo> guaranteeContrctInfo) {
        GuaranteeContrctInfo = guaranteeContrctInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo> getPledgeInfo() {
        return PledgeInfo;
    }

    public void setPledgeInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo> pledgeInfo) {
        PledgeInfo = pledgeInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo> getMortgageInfo() {
        return MortgageInfo;
    }

    public void setMortgageInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo> mortgageInfo) {
        MortgageInfo = mortgageInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo> getAssurePersonInfo() {
        return AssurePersonInfo;
    }

    public void setAssurePersonInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo> assurePersonInfo) {
        AssurePersonInfo = assurePersonInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssureAccInfo> getAssureAccInfo() {
        return AssureAccInfo;
    }

    public void setAssureAccInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssureAccInfo> assureAccInfo) {
        AssureAccInfo = assureAccInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo> getCustomerInfo() {
        return CustomerInfo;
    }

    public void setCustomerInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo> customerInfo) {
        CustomerInfo = customerInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.DealCustomerInfo> getDealCustomerInfo() {
        return DealCustomerInfo;
    }

    public void setDealCustomerInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.DealCustomerInfo> dealCustomerInfo) {
        DealCustomerInfo = dealCustomerInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo> getLimitPleMortInfo() {
        return LimitPleMortInfo;
    }

    public void setLimitPleMortInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo> limitPleMortInfo) {
        LimitPleMortInfo = limitPleMortInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessAssureInfo> getBusinessAssureInfo() {
        return BusinessAssureInfo;
    }

    public void setBusinessAssureInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessAssureInfo> businessAssureInfo) {
        BusinessAssureInfo = businessAssureInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteePleMortInfo> getGuaranteePleMortInfo() {
        return GuaranteePleMortInfo;
    }

    public void setGuaranteePleMortInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.GuaranteePleMortInfo> guaranteePleMortInfo) {
        GuaranteePleMortInfo = guaranteePleMortInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.UpApplyInfo> getUpApplyInfo() {
        return UpApplyInfo;
    }

    public void setUpApplyInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.UpApplyInfo> upApplyInfo) {
        UpApplyInfo = upApplyInfo;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo> getCurInfo() {
        return CurInfo;
    }

    public void setCurInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo> curInfo) {
        CurInfo = curInfo;
    }

    @Override
    public String toString() {
        return "Irs98ReqDto{" +
                "userid='" + userid + '\'' +
                ", username='" + username + '\'' +
                ", brchno='" + brchno + '\'' +
                ", brchnm='" + brchnm + '\'' +
                ", LimitApplyInfo=" + LimitApplyInfo +
                ", LimitDetailsInfo=" + LimitDetailsInfo +
                ", LimitProductInfo=" + LimitProductInfo +
                ", BusinessContractInfo=" + BusinessContractInfo +
                ", AccLoanInfo=" + AccLoanInfo +
                ", GuaranteeContrctInfo=" + GuaranteeContrctInfo +
                ", PledgeInfo=" + PledgeInfo +
                ", MortgageInfo=" + MortgageInfo +
                ", AssurePersonInfo=" + AssurePersonInfo +
                ", AssureAccInfo=" + AssureAccInfo +
                ", CustomerInfo=" + CustomerInfo +
                ", DealCustomerInfo=" + DealCustomerInfo +
                ", LimitPleMortInfo=" + LimitPleMortInfo +
                ", BusinessAssureInfo=" + BusinessAssureInfo +
                ", GuaranteePleMortInfo=" + GuaranteePleMortInfo +
                ", UpApplyInfo=" + UpApplyInfo +
                ", CurInfo=" + CurInfo +
                '}';
    }
}
