package cn.com.yusys.yusp.dto.client.esb.core.ln3246;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 贷款还款计划明细查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3246RespDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "zongbish")
    private Integer zongbish; // 总笔数 BaseType.U_CHANZXLX
    @JsonProperty(value = "lstLnDkhkmx")
    private List<LstLnDkhkmx> lstLnDkhkmx; // 还款明细列表

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List<LstLnDkhkmx> getLstLnDkhkmx() {
        return lstLnDkhkmx;
    }

    public void setLstLnDkhkmx(List<LstLnDkhkmx> lstLnDkhkmx) {
        this.lstLnDkhkmx = lstLnDkhkmx;
    }

    @Override
    public String toString() {
        return "Ln3246RespDto{" +
                "zongbish=" + zongbish +
                ", lstLnDkhkmx=" + lstLnDkhkmx +
                '}';
    }
}
