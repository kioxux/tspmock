package cn.com.yusys.yusp.dto.client.esb.core.mbt951.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：批量文件处理申请
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Mbt951RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "filename")
    private String filename;//报表文件名
    @JsonProperty(value = "picihaoo")
    private String picihaoo;//批次号
    @JsonProperty(value = "picriqii")
    private String picriqii;//批次日期
    @JsonProperty(value = "weituoha")
    private String weituoha;//委托号
    @JsonProperty(value = "zongbshu")
    private Integer zongbshu;//总笔数

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public String getPicriqii() {
        return picriqii;
    }

    public void setPicriqii(String picriqii) {
        this.picriqii = picriqii;
    }

    public String getWeituoha() {
        return weituoha;
    }

    public void setWeituoha(String weituoha) {
        this.weituoha = weituoha;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    @Override
    public String toString() {
        return "Mbt951RespDto{" +
                "filename='" + filename + '\'' +
                "picihaoo='" + picihaoo + '\'' +
                "picriqii='" + picriqii + '\'' +
                "weituoha='" + weituoha + '\'' +
                "zongbshu='" + zongbshu + '\'' +
                '}';
    }
}  
