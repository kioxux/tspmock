package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 主要管理人员
 */
@JsonPropertyOrder(alphabetic = true)
public class PERSON implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;//	企业名称
    @JsonProperty(value = "PERNAME")
    private String PERNAME;//	高管姓名
    @JsonProperty(value = "PERSONAMOUNT")
    private String PERSONAMOUNT;//	高管总数量
    @JsonProperty(value = "POSITION")
    private String POSITION;//	职位
    @JsonProperty(value = "POSITIONCODE")
    private String POSITIONCODE;//	职位代码

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getPERNAME() {
        return PERNAME;
    }

    @JsonIgnore
    public void setPERNAME(String PERNAME) {
        this.PERNAME = PERNAME;
    }

    @JsonIgnore
    public String getPERSONAMOUNT() {
        return PERSONAMOUNT;
    }

    @JsonIgnore
    public void setPERSONAMOUNT(String PERSONAMOUNT) {
        this.PERSONAMOUNT = PERSONAMOUNT;
    }

    @JsonIgnore
    public String getPOSITION() {
        return POSITION;
    }

    @JsonIgnore
    public void setPOSITION(String POSITION) {
        this.POSITION = POSITION;
    }

    @JsonIgnore
    public String getPOSITIONCODE() {
        return POSITIONCODE;
    }

    @JsonIgnore
    public void setPOSITIONCODE(String POSITIONCODE) {
        this.POSITIONCODE = POSITIONCODE;
    }

    @Override
    public String toString() {
        return "PERSON{" +
                "ENTNAME='" + ENTNAME + '\'' +
                ", PERNAME='" + PERNAME + '\'' +
                ", PERSONAMOUNT='" + PERSONAMOUNT + '\'' +
                ", POSITION='" + POSITION + '\'' +
                ", POSITIONCODE='" + POSITIONCODE + '\'' +
                '}';
    }
}
