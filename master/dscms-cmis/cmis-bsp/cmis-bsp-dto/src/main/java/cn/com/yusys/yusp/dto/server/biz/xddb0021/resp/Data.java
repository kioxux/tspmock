package cn.com.yusys.yusp.dto.server.biz.xddb0021.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据客户名查询抵押物类型
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "is_gyou")
    private String is_gyou;//是否共有
    @JsonProperty(value = "owner_type")
    private String owner_type;//共有类型
    @JsonProperty(value = "owner_title")
    private String owner_title;//共有份额
    @JsonProperty(value = "manager_id")
    private String manager_id;//客户经理号
    @JsonProperty(value = "manager_name")
    private String manager_name;//客户经理姓名
    @JsonProperty(value = "manager_ph")
    private String manager_ph;//客户经理号电话号码
    @JsonProperty(value = "manager_zjh")
    private String manager_zjh;//客户经理号身份证号码

    public String getIs_gyou() {
        return is_gyou;
    }

    public void setIs_gyou(String is_gyou) {
        this.is_gyou = is_gyou;
    }

    public String getOwner_type() {
        return owner_type;
    }

    public void setOwner_type(String owner_type) {
        this.owner_type = owner_type;
    }

    public String getOwner_title() {
        return owner_title;
    }

    public void setOwner_title(String owner_title) {
        this.owner_title = owner_title;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_ph() {
        return manager_ph;
    }

    public void setManager_ph(String manager_ph) {
        this.manager_ph = manager_ph;
    }

    public String getManager_zjh() {
        return manager_zjh;
    }

    public void setManager_zjh(String manager_zjh) {
        this.manager_zjh = manager_zjh;
    }

    @Override
    public String toString() {
        return "Data{" +
                "is_gyou='" + is_gyou + '\'' +
                ", owner_type='" + owner_type + '\'' +
                ", owner_title='" + owner_title + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", manager_ph='" + manager_ph + '\'' +
                ", manager_zjh='" + manager_zjh + '\'' +
                '}';
    }
}
