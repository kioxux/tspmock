package cn.com.yusys.yusp.dto.server.biz.xdxw0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 11:20:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 11:20
 * @since 2021/5/19 11:20
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "surveyNo")
    private String surveyNo;//调查编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码
    @JsonProperty(value = "cusLvl")
    private String cusLvl;//客户等级
    @JsonProperty(value = "grtMode")
    private String grtMode;//担保方式
    @JsonProperty(value = "contAmt")
    private String contAmt;//批复金额
    @JsonProperty(value = "termType")
    private String termType;//期限类型
    @JsonProperty(value = "applyTerm")
    private String applyTerm;//申请期限
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//批复起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//批复到期日
    @JsonProperty(value = "rate")
    private String rate;//执行年利率
    @JsonProperty(value = "repayMode")
    private String repayMode;//还款方式
    @JsonProperty(value = "limitType")
    private String limitType;//额度类型
    @JsonProperty(value = "curtLoanAmt")
    private String curtLoanAmt;//本次用信金额
    @JsonProperty(value = "truPayFlg")
    private String truPayFlg;//是否受托支付
    @JsonProperty(value = "loanCondFlg")
    private String loanCondFlg;//是否有用信条件
    @JsonProperty(value = "truPayType")
    private String truPayType;//受托类型
    @JsonProperty(value = "apprType")
    private String apprType;//审批类型
    @JsonProperty(value = "oprType")
    private String oprType;//操作类型
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//主管机构
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "updId")
    private String updId;//最后修改人
    @JsonProperty(value = "updBrId")
    private String updBrId;//最后修改机构
    @JsonProperty(value = "updDate")
    private String updDate;//最后修改日期
    @JsonProperty(value = "createTime")
    private String createTime;//创建时间
    @JsonProperty(value = "updateTime")
    private String updateTime;//修改时间
    @JsonProperty(value = "loanRenewFlg")
    private String iswxbxd;//是否无还本续贷
    @JsonProperty(value = "oldContNo")
    private String xdOrigiContNo;//续贷原合同编号
    @JsonProperty(value = "oldBillNo")
    private String xdOrigiBillNo;//续贷原借据号
    @JsonProperty(value = "loanModal")
    private String loanModal;// 贷款形式
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "certType")
    private String certType;//证件类型

    public String getXdOrigiBillNo() {
        return xdOrigiBillNo;
    }

    public void setXdOrigiBillNo(String xdOrigiBillNo) {
        this.xdOrigiBillNo = xdOrigiBillNo;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getSurveyNo() {
        return surveyNo;
    }

    public void setSurveyNo(String surveyNo) {
        this.surveyNo = surveyNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCusLvl() {
        return cusLvl;
    }

    public void setCusLvl(String cusLvl) {
        this.cusLvl = cusLvl;
    }

    public String getGrtMode() {
        return grtMode;
    }

    public void setGrtMode(String grtMode) {
        this.grtMode = grtMode;
    }

    public String getContAmt() {
        return contAmt;
    }

    public void setContAmt(String contAmt) {
        this.contAmt = contAmt;
    }

    public String getTermType() {
        return termType;
    }

    public void setTermType(String termType) {
        this.termType = termType;
    }

    public String getApplyTerm() {
        return applyTerm;
    }

    public void setApplyTerm(String applyTerm) {
        this.applyTerm = applyTerm;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public String getCurtLoanAmt() {
        return curtLoanAmt;
    }

    public void setCurtLoanAmt(String curtLoanAmt) {
        this.curtLoanAmt = curtLoanAmt;
    }

    public String getTruPayFlg() {
        return truPayFlg;
    }

    public void setTruPayFlg(String truPayFlg) {
        this.truPayFlg = truPayFlg;
    }

    public String getLoanCondFlg() {
        return loanCondFlg;
    }

    public void setLoanCondFlg(String loanCondFlg) {
        this.loanCondFlg = loanCondFlg;
    }

    public String getTruPayType() {
        return truPayType;
    }

    public void setTruPayType(String truPayType) {
        this.truPayType = truPayType;
    }

    public String getApprType() {
        return apprType;
    }

    public void setApprType(String apprType) {
        this.apprType = apprType;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setIswxbxd(String iswxbxd) {
        this.iswxbxd = iswxbxd;
    }

    public String getIswxbxd() {
        return this.iswxbxd;
    }

    public void setXdOrigiContNo(String xdOrigiContNo) {
        this.xdOrigiContNo = xdOrigiContNo;
    }

    public String getXdOrigiContNo() {
        return this.xdOrigiContNo;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "surveyNo='" + surveyNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certCode='" + certCode + '\'' +
                ", cusLvl='" + cusLvl + '\'' +
                ", grtMode='" + grtMode + '\'' +
                ", contAmt='" + contAmt + '\'' +
                ", termType='" + termType + '\'' +
                ", applyTerm='" + applyTerm + '\'' +
                ", contStartDate='" + contStartDate + '\'' +
                ", contEndDate='" + contEndDate + '\'' +
                ", rate='" + rate + '\'' +
                ", repayMode='" + repayMode + '\'' +
                ", limitType='" + limitType + '\'' +
                ", curtLoanAmt='" + curtLoanAmt + '\'' +
                ", truPayFlg='" + truPayFlg + '\'' +
                ", loanCondFlg='" + loanCondFlg + '\'' +
                ", truPayType='" + truPayType + '\'' +
                ", apprType='" + apprType + '\'' +
                ", oprType='" + oprType + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", iswxbxd='" + iswxbxd + '\'' +
                ", xdOrigiContNo='" + xdOrigiContNo + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", certType='" + certType + '\'' +
                '}';
    }
}
