package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：交易请求信息域:质押物信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 10:10
 */
@JsonPropertyOrder(alphabetic = true)
public class PledgeInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guaranty_id")
    private String guaranty_id; // 担保ID
    @JsonProperty(value = "gage_type")
    private String gage_type; // 质押物类型
    @JsonProperty(value = "guide_type")
    private String guide_type; // 担保品类型细分
    @JsonProperty(value = "gage_name")
    private String gage_name; // 质押物名称
    @JsonProperty(value = "currency")
    private String currency; // 币种
    @JsonProperty(value = "eval_amt")
    private BigDecimal eval_amt; // 评估价值（元）
    @JsonProperty(value = "book_amt")
    private BigDecimal book_amt; // 权利价值（元）
    @JsonProperty(value = "status")
    private String status; // 抵质押物状态
    @JsonProperty(value = "bond_type")
    private String bond_type; // 债券类型
    @JsonProperty(value = "outrate_org")
    private String outrate_org; // 外部评级机构
    @JsonProperty(value = "outrate_grade")
    private String outrate_grade; // 外部评级等级
    @JsonProperty(value = "inrate_grade")
    private String inrate_grade; // 内部评级等级
    @JsonProperty(value = "easy_nature")
    private String easy_nature; // 查封便利性
    @JsonProperty(value = "law_validity")
    private String law_validity; // 法律有效性
    @JsonProperty(value = "ple_cust_type")
    private String ple_cust_type; // 抵质押物与借款人相关性
    @JsonProperty(value = "ple_curr")
    private String ple_curr; // 抵质押品通用性
    @JsonProperty(value = "ple_cash")
    private String ple_cash; // 抵质押品变现能力
    @JsonProperty(value = "value_wave")
    private String value_wave; // 价格波动性

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    public String getGage_type() {
        return gage_type;
    }

    public void setGage_type(String gage_type) {
        this.gage_type = gage_type;
    }

    public String getGuide_type() {
        return guide_type;
    }

    public void setGuide_type(String guide_type) {
        this.guide_type = guide_type;
    }

    public String getGage_name() {
        return gage_name;
    }

    public void setGage_name(String gage_name) {
        this.gage_name = gage_name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getEval_amt() {
        return eval_amt;
    }

    public void setEval_amt(BigDecimal eval_amt) {
        this.eval_amt = eval_amt;
    }

    public BigDecimal getBook_amt() {
        return book_amt;
    }

    public void setBook_amt(BigDecimal book_amt) {
        this.book_amt = book_amt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBond_type() {
        return bond_type;
    }

    public void setBond_type(String bond_type) {
        this.bond_type = bond_type;
    }

    public String getOutrate_org() {
        return outrate_org;
    }

    public void setOutrate_org(String outrate_org) {
        this.outrate_org = outrate_org;
    }

    public String getOutrate_grade() {
        return outrate_grade;
    }

    public void setOutrate_grade(String outrate_grade) {
        this.outrate_grade = outrate_grade;
    }

    public String getInrate_grade() {
        return inrate_grade;
    }

    public void setInrate_grade(String inrate_grade) {
        this.inrate_grade = inrate_grade;
    }

    public String getEasy_nature() {
        return easy_nature;
    }

    public void setEasy_nature(String easy_nature) {
        this.easy_nature = easy_nature;
    }

    public String getLaw_validity() {
        return law_validity;
    }

    public void setLaw_validity(String law_validity) {
        this.law_validity = law_validity;
    }

    public String getPle_cust_type() {
        return ple_cust_type;
    }

    public void setPle_cust_type(String ple_cust_type) {
        this.ple_cust_type = ple_cust_type;
    }

    public String getPle_curr() {
        return ple_curr;
    }

    public void setPle_curr(String ple_curr) {
        this.ple_curr = ple_curr;
    }

    public String getPle_cash() {
        return ple_cash;
    }

    public void setPle_cash(String ple_cash) {
        this.ple_cash = ple_cash;
    }

    public String getValue_wave() {
        return value_wave;
    }

    public void setValue_wave(String value_wave) {
        this.value_wave = value_wave;
    }

    @Override
    public String toString() {
        return "PledgeInfo{" +
                "guaranty_id='" + guaranty_id + '\'' +
                ", gage_type='" + gage_type + '\'' +
                ", guide_type='" + guide_type + '\'' +
                ", gage_name='" + gage_name + '\'' +
                ", currency='" + currency + '\'' +
                ", eval_amt=" + eval_amt +
                ", book_amt=" + book_amt +
                ", status='" + status + '\'' +
                ", bond_type='" + bond_type + '\'' +
                ", outrate_org='" + outrate_org + '\'' +
                ", outrate_grade='" + outrate_grade + '\'' +
                ", inrate_grade='" + inrate_grade + '\'' +
                ", easy_nature='" + easy_nature + '\'' +
                ", law_validity='" + law_validity + '\'' +
                ", ple_cust_type='" + ple_cust_type + '\'' +
                ", ple_curr='" + ple_curr + '\'' +
                ", ple_cash='" + ple_cash + '\'' +
                ", value_wave='" + value_wave + '\'' +
                '}';
    }
}
