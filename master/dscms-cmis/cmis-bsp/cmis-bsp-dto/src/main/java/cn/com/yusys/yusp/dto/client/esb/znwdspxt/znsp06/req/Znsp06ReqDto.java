package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp06.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：被拒绝的线上产品推送接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp06ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cert_no")
    private String cert_no;//证件号
    @JsonProperty(value = "cus_mgr_no")
    private String cus_mgr_no;//客户经理工号
    @JsonProperty(value = "cus_mgr_name")
    private String cus_mgr_name;//客户经理名称
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//申请金额
    @JsonProperty(value = "guar_way_code")
    private String guar_way_code;//担保方式编码
    @JsonProperty(value = "guar_way_name")
    private String guar_way_name;//担保方式名称
    @JsonProperty(value = "bus_type")
    private String bus_type;//业务类型
    @JsonProperty(value = "prd_id")
    private String prd_id;//产品代码
    @JsonProperty(value = "prd_name")
    private String prd_name;//产品名称
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//调查流水号
    @JsonProperty(value = "reason")
    private String reason;//拒绝原因

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCert_no() {
        return cert_no;
    }

    public void setCert_no(String cert_no) {
        this.cert_no = cert_no;
    }

    public String getCus_mgr_no() {
        return cus_mgr_no;
    }

    public void setCus_mgr_no(String cus_mgr_no) {
        this.cus_mgr_no = cus_mgr_no;
    }

    public String getCus_mgr_name() {
        return cus_mgr_name;
    }

    public void setCus_mgr_name(String cus_mgr_name) {
        this.cus_mgr_name = cus_mgr_name;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getGuar_way_code() {
        return guar_way_code;
    }

    public void setGuar_way_code(String guar_way_code) {
        this.guar_way_code = guar_way_code;
    }

    public String getGuar_way_name() {
        return guar_way_name;
    }

    public void setGuar_way_name(String guar_way_name) {
        this.guar_way_name = guar_way_name;
    }

    public String getBus_type() {
        return bus_type;
    }

    public void setBus_type(String bus_type) {
        this.bus_type = bus_type;
    }

    public String getPrd_id() {
        return prd_id;
    }

    public void setPrd_id(String prd_id) {
        this.prd_id = prd_id;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Znsp06ReqDto{" +
                "cus_name='" + cus_name + '\'' +
                "cert_no='" + cert_no + '\'' +
                "cus_mgr_no='" + cus_mgr_no + '\'' +
                "cus_mgr_name='" + cus_mgr_name + '\'' +
                "apply_amount='" + apply_amount + '\'' +
                "guar_way_code='" + guar_way_code + '\'' +
                "guar_way_name='" + guar_way_name + '\'' +
                "bus_type='" + bus_type + '\'' +
                "prd_id='" + prd_id + '\'' +
                "prd_name='" + prd_name + '\'' +
                "survey_serno='" + survey_serno + '\'' +
                "reason='" + reason + '\'' +
                '}';
    }
}  
