package cn.com.yusys.yusp.dto.client.esb.ypxt.ypztbg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：押品状态变更
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日15:13:49
 */
@JsonPropertyOrder(alphabetic = true)
public class YpztbgRespDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarno")
    private String guarno;// 押品编号

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    @Override
    public String toString() {
        return "YpztbgRespDto{" +
                "guarno='" + guarno + '\'' +
                '}';
    }
}
