package cn.com.yusys.yusp.dto.server.biz.xdtz0045.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：商贷分户实时查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "loanType")
    private String loanType;//贷款类别
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "disbDate")
    private String disbDate;//放款日期
    @JsonProperty(value = "loanTerm")
    private String loanTerm;//贷款期限
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//贷款金额
    @JsonProperty(value = "loanBal")
    private BigDecimal loanBal;//贷款余额
    @JsonProperty(value = "repayAcctNo")
    private String repayAcctNo;//还款账号
    @JsonProperty(value = "repayFinishDate")
    private String repayFinishDate;//还清日期
    @JsonProperty(value = "housePlace")
    private String housePlace;//房屋坐落
    @JsonProperty(value = "propertyRegCode")
    private String propertyRegCode;//不动产权登记号或房产证编号
    @JsonProperty(value = "houseSqu")
    private String houseSqu;//房屋面积
    @JsonProperty(value = "houseTotlPrice")
    private BigDecimal houseTotlPrice;//房屋总价
    @JsonProperty(value = "regArea")
    private String regArea;//行政区域
    @JsonProperty(value = "isFirstHouseFlag")
    private String isFirstHouseFlag;//首套房标志
    @JsonProperty(value = "netSignAgrNo")
    private String netSignAgrNo;//网签协议号
    @JsonProperty(value = "reqPageNum")
    private String reqPageNum;//请求页码
    @JsonProperty(value = "totlRecordQnt")
    private String totlRecordQnt;//总记录数
    @JsonProperty(value = "totlPageNum")
    private String totlPageNum;//总页数
    @JsonProperty(value = "repayAcctName")
    private String repayAcctName;//还款账号户名

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getDisbDate() {
        return disbDate;
    }

    public void setDisbDate(String disbDate) {
        this.disbDate = disbDate;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    public String getRepayAcctNo() {
        return repayAcctNo;
    }

    public void setRepayAcctNo(String repayAcctNo) {
        this.repayAcctNo = repayAcctNo;
    }

    public String getRepayFinishDate() {
        return repayFinishDate;
    }

    public void setRepayFinishDate(String repayFinishDate) {
        this.repayFinishDate = repayFinishDate;
    }

    public String getHousePlace() {
        return housePlace;
    }

    public void setHousePlace(String housePlace) {
        this.housePlace = housePlace;
    }

    public String getPropertyRegCode() {
        return propertyRegCode;
    }

    public void setPropertyRegCode(String propertyRegCode) {
        this.propertyRegCode = propertyRegCode;
    }

    public String getHouseSqu() {
        return houseSqu;
    }

    public void setHouseSqu(String houseSqu) {
        this.houseSqu = houseSqu;
    }

    public BigDecimal getHouseTotlPrice() {
        return houseTotlPrice;
    }

    public void setHouseTotlPrice(BigDecimal houseTotlPrice) {
        this.houseTotlPrice = houseTotlPrice;
    }

    public String getRegArea() {
        return regArea;
    }

    public void setRegArea(String regArea) {
        this.regArea = regArea;
    }

    public String getIsFirstHouseFlag() {
        return isFirstHouseFlag;
    }

    public void setIsFirstHouseFlag(String isFirstHouseFlag) {
        this.isFirstHouseFlag = isFirstHouseFlag;
    }

    public String getNetSignAgrNo() {
        return netSignAgrNo;
    }

    public void setNetSignAgrNo(String netSignAgrNo) {
        this.netSignAgrNo = netSignAgrNo;
    }

    public String getReqPageNum() {
        return reqPageNum;
    }

    public void setReqPageNum(String reqPageNum) {
        this.reqPageNum = reqPageNum;
    }

    public String getTotlRecordQnt() {
        return totlRecordQnt;
    }

    public void setTotlRecordQnt(String totlRecordQnt) {
        this.totlRecordQnt = totlRecordQnt;
    }

    public String getTotlPageNum() {
        return totlPageNum;
    }

    public void setTotlPageNum(String totlPageNum) {
        this.totlPageNum = totlPageNum;
    }

    public String getRepayAcctName() {
        return repayAcctName;
    }

    public void setRepayAcctName(String repayAcctName) {
        this.repayAcctName = repayAcctName;
    }

    @Override
    public String toString() {
        return "List{" +
                "loanType='" + loanType + '\'' +
                ", billNo='" + billNo + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", disbDate='" + disbDate + '\'' +
                ", loanTerm='" + loanTerm + '\'' +
                ", loanAmt=" + loanAmt +
                ", loanBal=" + loanBal +
                ", repayAcctNo='" + repayAcctNo + '\'' +
                ", repayFinishDate='" + repayFinishDate + '\'' +
                ", housePlace='" + housePlace + '\'' +
                ", propertyRegCode='" + propertyRegCode + '\'' +
                ", houseSqu='" + houseSqu + '\'' +
                ", houseTotlPrice=" + houseTotlPrice +
                ", regArea='" + regArea + '\'' +
                ", isFirstHouseFlag='" + isFirstHouseFlag + '\'' +
                ", netSignAgrNo='" + netSignAgrNo + '\'' +
                ", reqPageNum='" + reqPageNum + '\'' +
                ", totlRecordQnt='" + totlRecordQnt + '\'' +
                ", totlPageNum='" + totlPageNum + '\'' +
                ", repayAcctName='" + repayAcctName + '\'' +
                '}';
    }
}
