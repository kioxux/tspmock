package cn.com.yusys.yusp.dto.server.biz.xdca0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：信用卡调额申请
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;


    public String getRiskResult() {
        return riskResult;
    }

    public void setRiskResult(String riskResult) {
        this.riskResult = riskResult;
    }

    public BigDecimal getAccLmt() {
        return accLmt;
    }

    public void setAccLmt(BigDecimal accLmt) {
        this.accLmt = accLmt;
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    @JsonProperty(value = "riskResult")
    private String riskResult;//审批结果
    @JsonProperty(value = "accLmt")
    private BigDecimal accLmt;//核准额度
    @JsonProperty(value = "remark3")
    private String remark3;//备注


    @Override
    public String toString() {
        return "Xdca0006DataRespDto{" +
                "RISK_RESULT='" + riskResult + '\'' +
                "ACC_LMT='" + accLmt + '\'' +
                "REMARK3='" + remark3 + '\'' +
                '}';
    }
}  
