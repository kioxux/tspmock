package cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：贷后任务推送
 *
 * @author wrw
 * @version 1.0
 * @since 2021年8月14日28:09:30
 */
@JsonPropertyOrder(alphabetic = true)
public class Yxgc01ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;// 任务编号
    @JsonProperty(value = "cus_id")
    private String cus_id;// 客户号
    @JsonProperty(value = "cus_name")
    private String cus_name;// 客户名称
    @JsonProperty(value = "phone")
    private String phone;// 手机号码
    @JsonProperty(value = "address")
    private String address;// 地址
    @JsonProperty(value = "cus_mng_id")
    private String cus_mng_id;// 客户经理号
    @JsonProperty(value = "cus_mng_name")
    private String cus_mng_name;// 客户经理名称
    @JsonProperty(value = "mng_br_id")
    private String mng_br_id;// 机构号
    @JsonProperty(value = "mng_br_name")
    private String mng_br_name;// 机构名
    @JsonProperty(value = "crt_date")
    private String crt_date;// 任务生成日期
    @JsonProperty(value = "rqr_fin_date")
    private String rqr_fin_date;// 要求完成日期
    @JsonProperty(value = "psp_state")
    private String psp_state;// 贷后任务操作标记
    @JsonProperty(value = "is_production")
    private String is_production;// 是否正常生产
    @JsonProperty(value = "is_coop")
    private String is_coop;// 客户配合度

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCus_mng_id() {
        return cus_mng_id;
    }

    public void setCus_mng_id(String cus_mng_id) {
        this.cus_mng_id = cus_mng_id;
    }

    public String getCus_mng_name() {
        return cus_mng_name;
    }

    public void setCus_mng_name(String cus_mng_name) {
        this.cus_mng_name = cus_mng_name;
    }

    public String getMng_br_id() {
        return mng_br_id;
    }

    public void setMng_br_id(String mng_br_id) {
        this.mng_br_id = mng_br_id;
    }

    public String getMng_br_name() {
        return mng_br_name;
    }

    public void setMng_br_name(String mng_br_name) {
        this.mng_br_name = mng_br_name;
    }

    public String getCrt_date() {
        return crt_date;
    }

    public void setCrt_date(String crt_date) {
        this.crt_date = crt_date;
    }

    public String getRqr_fin_date() {
        return rqr_fin_date;
    }

    public void setRqr_fin_date(String rqr_fin_date) {
        this.rqr_fin_date = rqr_fin_date;
    }

    public String getPsp_state() {
        return psp_state;
    }

    public void setPsp_state(String psp_state) {
        this.psp_state = psp_state;
    }

    public String getIs_production() {
        return is_production;
    }

    public void setIs_production(String is_production) {
        this.is_production = is_production;
    }

    public String getIs_coop() {
        return is_coop;
    }

    public void setIs_coop(String is_coop) {
        this.is_coop = is_coop;
    }

    @Override
    public String toString() {
        return "Yxgc01ReqDto{" +
                "serno='" + serno + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", cus_mngId='" + cus_mng_id + '\'' +
                ", cus_mngName='" + cus_mng_name + '\'' +
                ", mng_br_id='" + mng_br_id + '\'' +
                ", mng_br_name='" + mng_br_name + '\'' +
                ", crt_date='" + crt_date + '\'' +
                ", rqr_fin_date='" + rqr_fin_date + '\'' +
                ", psp_state='" + psp_state + '\'' +
                ", is_production='" + is_production + '\'' +
                ", is_coop='" + is_coop + '\'' +
                '}';
    }
}
