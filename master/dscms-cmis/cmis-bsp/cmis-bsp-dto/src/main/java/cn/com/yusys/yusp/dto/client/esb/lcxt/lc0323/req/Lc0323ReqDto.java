package cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询理财是否冻结
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lc0323ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "FunctionId")
    private String FunctionId;//交易代码
    @JsonProperty(value = "ExSerial")
    private String ExSerial;//发起方流水号
    @JsonProperty(value = "BankNo")
    private String BankNo;//银行编号
    @JsonProperty(value = "BranchNo")
    private String BranchNo;//交易机构
    @JsonProperty(value = "Channel")
    private String Channel;//交易渠道
    @JsonProperty(value = "TermNo")
    private String TermNo;//终端代码
    @JsonProperty(value = "OperNo")
    private String OperNo;//交易柜员
    @JsonProperty(value = "AuthOper")
    private String AuthOper;//授权柜员
    @JsonProperty(value = "AuthPwd")
    private String AuthPwd;//授权密码
    @JsonProperty(value = "TransDate")
    private String TransDate;//交易日期
    @JsonProperty(value = "TransTime")
    private String TransTime;//交易时间
    @JsonProperty(value = "PrdType")
    private String PrdType;//产品类别
    @JsonProperty(value = "Reserve")
    private String Reserve;//交易附加信息
    @JsonProperty(value = "Reserve1")
    private String Reserve1;//附加信息1

    @JsonProperty(value = "AccType")
    private String AccType;//客户标识类型
    @JsonProperty(value = "Account")
    private String Account;//客户标识
    @JsonProperty(value = "IdType")
    private String IdType;//证件类型
    @JsonProperty(value = "StartDate")
    private String StartDate;//开始日期
    @JsonProperty(value = "EndDate")
    private String EndDate;//结束日期
    @JsonProperty(value = "SerialNo")
    private String SerialNo;//冻结流水号
    @JsonProperty(value = "OffSet")
    private String OffSet;//定位串
    @JsonProperty(value = "QueryNum")
    private String QueryNum;//查询行数
    @JsonProperty(value = "ClientType")
    private String ClientType;//客户类型
    @JsonProperty(value = "TransCode")
    private String TransCode;//冻结来源
    @JsonProperty(value = "OriBranchNo")
    private String OriBranchNo;//原交易机构编号
    @JsonProperty(value = "FrozenCause")
    private String FrozenCause;//冻结原因
    @JsonProperty(value = "CardType")
    private String CardType;//卡种
    @JsonProperty(value = "BankAcc")
    private String BankAcc;//银行账号
    @JsonProperty(value = "TemplateCode")
    private String TemplateCode;//模板代码
    @JsonProperty(value = "CurrType")
    private String CurrType;//币种
    @JsonProperty(value = "CashFlag")
    private String CashFlag;//钞汇

    @JsonIgnore
    public String getFunctionId() {
        return FunctionId;
    }

    @JsonIgnore
    public void setFunctionId(String functionId) {
        FunctionId = functionId;
    }

    @JsonIgnore
    public String getExSerial() {
        return ExSerial;
    }

    @JsonIgnore
    public void setExSerial(String exSerial) {
        ExSerial = exSerial;
    }

    @JsonIgnore
    public String getBankNo() {
        return BankNo;
    }

    @JsonIgnore
    public void setBankNo(String bankNo) {
        BankNo = bankNo;
    }

    @JsonIgnore
    public String getBranchNo() {
        return BranchNo;
    }

    @JsonIgnore
    public void setBranchNo(String branchNo) {
        BranchNo = branchNo;
    }

    @JsonIgnore
    public String getChannel() {
        return Channel;
    }

    @JsonIgnore
    public void setChannel(String channel) {
        Channel = channel;
    }

    @JsonIgnore
    public String getTermNo() {
        return TermNo;
    }

    @JsonIgnore
    public void setTermNo(String termNo) {
        TermNo = termNo;
    }

    @JsonIgnore
    public String getOperNo() {
        return OperNo;
    }

    @JsonIgnore
    public void setOperNo(String operNo) {
        OperNo = operNo;
    }

    @JsonIgnore
    public String getAuthOper() {
        return AuthOper;
    }

    @JsonIgnore
    public void setAuthOper(String authOper) {
        AuthOper = authOper;
    }

    @JsonIgnore
    public String getAuthPwd() {
        return AuthPwd;
    }

    @JsonIgnore
    public void setAuthPwd(String authPwd) {
        AuthPwd = authPwd;
    }

    @JsonIgnore
    public String getTransDate() {
        return TransDate;
    }

    @JsonIgnore
    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    @JsonIgnore
    public String getTransTime() {
        return TransTime;
    }

    @JsonIgnore
    public void setTransTime(String transTime) {
        TransTime = transTime;
    }

    @JsonIgnore
    public String getPrdType() {
        return PrdType;
    }

    @JsonIgnore
    public void setPrdType(String prdType) {
        PrdType = prdType;
    }

    @JsonIgnore
    public String getReserve() {
        return Reserve;
    }

    @JsonIgnore
    public void setReserve(String reserve) {
        Reserve = reserve;
    }

    @JsonIgnore
    public String getReserve1() {
        return Reserve1;
    }

    @JsonIgnore
    public void setReserve1(String reserve1) {
        Reserve1 = reserve1;
    }

    @JsonIgnore
    public String getAccType() {
        return AccType;
    }

    @JsonIgnore
    public void setAccType(String accType) {
        AccType = accType;
    }

    @JsonIgnore
    public String getAccount() {
        return Account;
    }

    @JsonIgnore
    public void setAccount(String account) {
        Account = account;
    }

    @JsonIgnore
    public String getIdType() {
        return IdType;
    }

    @JsonIgnore
    public void setIdType(String idType) {
        IdType = idType;
    }

    @JsonIgnore
    public String getStartDate() {
        return StartDate;
    }

    @JsonIgnore
    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    @JsonIgnore
    public String getEndDate() {
        return EndDate;
    }

    @JsonIgnore
    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    @JsonIgnore
    public String getSerialNo() {
        return SerialNo;
    }

    @JsonIgnore
    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    @JsonIgnore
    public String getOffSet() {
        return OffSet;
    }

    @JsonIgnore
    public void setOffSet(String offSet) {
        OffSet = offSet;
    }

    @JsonIgnore
    public String getQueryNum() {
        return QueryNum;
    }

    @JsonIgnore
    public void setQueryNum(String queryNum) {
        QueryNum = queryNum;
    }

    @JsonIgnore
    public String getClientType() {
        return ClientType;
    }

    @JsonIgnore
    public void setClientType(String clientType) {
        ClientType = clientType;
    }

    @JsonIgnore
    public String getTransCode() {
        return TransCode;
    }

    @JsonIgnore
    public void setTransCode(String transCode) {
        TransCode = transCode;
    }

    @JsonIgnore
    public String getOriBranchNo() {
        return OriBranchNo;
    }

    @JsonIgnore
    public void setOriBranchNo(String oriBranchNo) {
        OriBranchNo = oriBranchNo;
    }

    @JsonIgnore
    public String getFrozenCause() {
        return FrozenCause;
    }

    @JsonIgnore
    public void setFrozenCause(String frozenCause) {
        FrozenCause = frozenCause;
    }

    @JsonIgnore
    public String getCardType() {
        return CardType;
    }

    @JsonIgnore
    public void setCardType(String cardType) {
        CardType = cardType;
    }

    @JsonIgnore
    public String getBankAcc() {
        return BankAcc;
    }

    @JsonIgnore
    public void setBankAcc(String bankAcc) {
        BankAcc = bankAcc;
    }

    @JsonIgnore
    public String getTemplateCode() {
        return TemplateCode;
    }

    @JsonIgnore
    public void setTemplateCode(String templateCode) {
        TemplateCode = templateCode;
    }

    @JsonIgnore
    public String getCurrType() {
        return CurrType;
    }

    @JsonIgnore
    public void setCurrType(String currType) {
        CurrType = currType;
    }

    @JsonIgnore
    public String getCashFlag() {
        return CashFlag;
    }

    @JsonIgnore
    public void setCashFlag(String cashFlag) {
        CashFlag = cashFlag;
    }

    @Override
    public String toString() {
        return "Lc0323ReqDto{" +
                "FunctionId='" + FunctionId + '\'' +
                ", ExSerial='" + ExSerial + '\'' +
                ", BankNo='" + BankNo + '\'' +
                ", BranchNo='" + BranchNo + '\'' +
                ", Channel='" + Channel + '\'' +
                ", TermNo='" + TermNo + '\'' +
                ", OperNo='" + OperNo + '\'' +
                ", AuthOper='" + AuthOper + '\'' +
                ", AuthPwd='" + AuthPwd + '\'' +
                ", TransDate='" + TransDate + '\'' +
                ", TransTime='" + TransTime + '\'' +
                ", PrdType='" + PrdType + '\'' +
                ", Reserve='" + Reserve + '\'' +
                ", Reserve1='" + Reserve1 + '\'' +
                ", AccType='" + AccType + '\'' +
                ", Account='" + Account + '\'' +
                ", IdType='" + IdType + '\'' +
                ", StartDate='" + StartDate + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", OffSet='" + OffSet + '\'' +
                ", QueryNum='" + QueryNum + '\'' +
                ", ClientType='" + ClientType + '\'' +
                ", TransCode='" + TransCode + '\'' +
                ", OriBranchNo='" + OriBranchNo + '\'' +
                ", FrozenCause='" + FrozenCause + '\'' +
                ", CardType='" + CardType + '\'' +
                ", BankAcc='" + BankAcc + '\'' +
                ", TemplateCode='" + TemplateCode + '\'' +
                ", CurrType='" + CurrType + '\'' +
                ", CashFlag='" + CashFlag + '\'' +
                '}';
    }
}
