package cn.com.yusys.yusp.dto.client.esb.irs.xirs10;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：公司客户信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xirs10RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}  
