package cn.com.yusys.yusp.dto.server.biz.xdht0017.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 15:58
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号
    @JsonProperty(value = "grtContNo")
    private String grtContNo;//担保合同号

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getGrtContNo() {
        return grtContNo;
    }

    public void setGrtContNo(String grtContNo) {
        this.grtContNo = grtContNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "loanContNo='" + loanContNo + '\'' +
                ", grtContNo='" + grtContNo + '\'' +
                '}';
    }
}
