package cn.com.yusys.yusp.dto.server.biz.xdtz0055.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 10:45:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 10:45
 * @since 2021/5/22 10:45
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusAcctNo")
    private String cusAcctNo;//客户账号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusAcctNo() {
        return cusAcctNo;
    }

    public void setCusAcctNo(String cusAcctNo) {
        this.cusAcctNo = cusAcctNo;
    }

    @Override
    public String toString() {
        return "Xdtz0055ReqDto{" +
                "cusId='" + cusId + '\'' +
                "cusAcctNo='" + cusAcctNo + '\'' +
                '}';
    }
}
