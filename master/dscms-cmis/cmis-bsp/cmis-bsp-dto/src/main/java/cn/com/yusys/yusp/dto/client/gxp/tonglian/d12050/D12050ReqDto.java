package cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：账单列表查询
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D12050ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cardno")
    private String cardno;//卡号
    @JsonProperty(value = "crcycd")
    private String crcycd;//币种
    @JsonProperty(value = "stsart")
    private String stsart;//账单起始年月
    @JsonProperty(value = "enddat")
    private String enddat;//账单截止日期
    @JsonProperty(value = "smendt")
    private String smendt;//发送日期
    @JsonProperty(value = "frtrow")
    private String frtrow;//开始位置
    @JsonProperty(value = "lstrow")
    private String lstrow;//结束位置

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getStsart() {
        return stsart;
    }

    public void setStsart(String stsart) {
        this.stsart = stsart;
    }

    public String getEnddat() {
        return enddat;
    }

    public void setEnddat(String enddat) {
        this.enddat = enddat;
    }

    public String getSmendt() {
        return smendt;
    }

    public void setSmendt(String smendt) {
        this.smendt = smendt;
    }

    public String getFrtrow() {
        return frtrow;
    }

    public void setFrtrow(String frtrow) {
        this.frtrow = frtrow;
    }

    public String getLstrow() {
        return lstrow;
    }

    public void setLstrow(String lstrow) {
        this.lstrow = lstrow;
    }

    @Override
    public String toString() {
        return "D12050ReqDto{" +
                "cardno='" + cardno + '\'' +
                "crcycd='" + crcycd + '\'' +
                "stsart='" + stsart + '\'' +
                "enddat='" + enddat + '\'' +
                "smendt='" + smendt + '\'' +
                "frtrow='" + frtrow + '\'' +
                "lstrow='" + lstrow + '\'' +
                '}';
    }
}
