package cn.com.yusys.yusp.dto.server.biz.xdxw0034.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据流水号查询无还本续贷基本信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0034RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdxw0034RespDto{" +
				"data=" + data +
				'}';
	}
}
