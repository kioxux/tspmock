package cn.com.yusys.yusp.dto.client.esb.core.ln3244;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款客户经理批量移交
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LstHetongbh implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    @Override
    public String toString() {
        return "LstHetongbh{" +
                "hetongbh='" + hetongbh + '\'' +
                '}';
    }
}
