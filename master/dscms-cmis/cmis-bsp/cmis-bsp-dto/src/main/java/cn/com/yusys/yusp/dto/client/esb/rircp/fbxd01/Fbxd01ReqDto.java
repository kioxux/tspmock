package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：惠享贷授信申请件数取得
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd01ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    @Override
    public String toString() {
        return "Fbxd01ReqDto{" +
                "certCode='" + certCode + '\'' +
                '}';
    }
}  
