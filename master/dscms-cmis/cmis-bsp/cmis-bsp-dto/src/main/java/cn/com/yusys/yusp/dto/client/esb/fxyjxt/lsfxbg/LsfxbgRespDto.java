package cn.com.yusys.yusp.dto.client.esb.fxyjxt.lsfxbg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询客户项下历史风险预警报告
 */
@JsonPropertyOrder(alphabetic = true)
public class LsfxbgRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "LsfxbgRespDto{" +
                "list=" + list +
                '}';
    }
}
