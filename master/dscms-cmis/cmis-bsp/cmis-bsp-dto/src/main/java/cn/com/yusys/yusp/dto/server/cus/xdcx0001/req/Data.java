package cn.com.yusys.yusp.dto.server.cus.xdcx0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/27 10:05
 * @since 2021/8/27 10:05
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bankNo")
    private String bankNo;//行号
    @JsonProperty(value = "bicCode")
    private String bicCode;//BIC CODE（分支行）

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getBicCode() {
        return bicCode;
    }

    public void setBicCode(String bicCode) {
        this.bicCode = bicCode;
    }

    @Override
    public String toString() {
        return "Xdcx0001ReqDto{" +
                "bankNo ='" + bankNo + '\'' +
                "bicCode='" + bicCode + '\'' +
                '}';
    }
}
