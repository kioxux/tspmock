package cn.com.yusys.yusp.dto.client.esb.irs.irs28.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：财务信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs28ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custid")
    private String custid;//客户编号
    @JsonProperty(value = "reportdate")
    private String reportdate;//报表日期
    @JsonProperty(value = "reportsope")
    private String reportsope;//报表口径
    @JsonProperty(value = "reportcurrency")
    private String reportcurrency;//报表币种
    @JsonProperty(value = "reportunit")
    private String reportunit;//报表单位
    @JsonProperty(value = "reportaudit")
    private String reportaudit;//审计标志
    @JsonProperty(value = "auditunit")
    private String auditunit;//审计单位
    @JsonProperty(value = "auditopinion")
    private String auditopinion;//审计意见
    @JsonProperty(value = "modelclass")
    private String modelclass;//财务报表类别
    @JsonProperty(value = "flag")
    private String flag;//报表标识
    @JsonProperty(value = "assetDebt")
    private java.util.List<AssetDebt> assetDebt;
    @JsonProperty(value = "loss")
    private java.util.List<Loss> loss;

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getReportdate() {
        return reportdate;
    }

    public void setReportdate(String reportdate) {
        this.reportdate = reportdate;
    }

    public String getReportsope() {
        return reportsope;
    }

    public void setReportsope(String reportsope) {
        this.reportsope = reportsope;
    }

    public String getReportcurrency() {
        return reportcurrency;
    }

    public void setReportcurrency(String reportcurrency) {
        this.reportcurrency = reportcurrency;
    }

    public String getReportunit() {
        return reportunit;
    }

    public void setReportunit(String reportunit) {
        this.reportunit = reportunit;
    }

    public String getReportaudit() {
        return reportaudit;
    }

    public void setReportaudit(String reportaudit) {
        this.reportaudit = reportaudit;
    }

    public String getAuditunit() {
        return auditunit;
    }

    public void setAuditunit(String auditunit) {
        this.auditunit = auditunit;
    }

    public String getAuditopinion() {
        return auditopinion;
    }

    public void setAuditopinion(String auditopinion) {
        this.auditopinion = auditopinion;
    }

    public String getModelclass() {
        return modelclass;
    }

    public void setModelclass(String modelclass) {
        this.modelclass = modelclass;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public List<AssetDebt> getAssetDebt() {
        return assetDebt;
    }

    public void setAssetDebt(List<AssetDebt> assetDebt) {
        this.assetDebt = assetDebt;
    }

    public List<Loss> getLoss() {
        return loss;
    }

    public void setLoss(List<Loss> loss) {
        this.loss = loss;
    }

    @Override
    public String toString() {
        return "Irs28ReqDto{" +
                "custid='" + custid + '\'' +
                ", reportdate='" + reportdate + '\'' +
                ", reportsope='" + reportsope + '\'' +
                ", reportcurrency='" + reportcurrency + '\'' +
                ", reportunit='" + reportunit + '\'' +
                ", reportaudit='" + reportaudit + '\'' +
                ", auditunit='" + auditunit + '\'' +
                ", auditopinion='" + auditopinion + '\'' +
                ", modelclass='" + modelclass + '\'' +
                ", flag='" + flag + '\'' +
                ", assetDebt=" + assetDebt +
                ", loss=" + loss +
                '}';
    }
}
