package cn.com.yusys.yusp.dto.server.biz.xdzc0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：资产池台账列表查询
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty("loanType")
    private String loanType;// 贷款种类
    @JsonProperty("billNo")
    private String billNo;// 借据编号
    @JsonProperty("execRateYear")
    private String execRateYear;// 借据利率(执行利率年)
    @JsonProperty("rateAdjMode")
    private String rateAdjMode;// 利率调整方式
    @JsonProperty("loanTerm")
    private String loanTerm;// 借款期限
    @JsonProperty("loanAmt")
    private BigDecimal loanAmt;// 贷款金额
    @JsonProperty("startDate")
    private String startDate;// 贷款起始日
    @JsonProperty("endDate")
    private String endDate;// 贷款到期日
    @JsonProperty("accStatus")
    private String accStatus;// 贷款状态

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(String execRateYear) {
        this.execRateYear = execRateYear;
    }

    public String getRateAdjMode() {
        return rateAdjMode;
    }

    public void setRateAdjMode(String rateAdjMode) {
        this.rateAdjMode = rateAdjMode;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    @Override
    public String toString() {
        return "List{" +
                "loanType='" + loanType + '\'' +
                ", billNo='" + billNo + '\'' +
                ", execRateYear='" + execRateYear + '\'' +
                ", rateAdjMode='" + rateAdjMode + '\'' +
                ", loanTerm='" + loanTerm + '\'' +
                ", loanAmt=" + loanAmt +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", accStatus='" + accStatus + '\'' +
                '}';
    }
}