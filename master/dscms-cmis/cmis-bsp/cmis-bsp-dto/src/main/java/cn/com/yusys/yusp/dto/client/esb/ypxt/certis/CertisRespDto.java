package cn.com.yusys.yusp.dto.client.esb.ypxt.certis;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：权证状态同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class CertisRespDto implements Serializable {
    // 此接口不返回业务字段
}
