package cn.com.yusys.yusp.dto.client.esb.core.ln3108;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款组合查询
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3108RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "lstdkzhzb")
    private List<Lstdkzhzb> lstdkzhzb;//贷款账户主表


    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List<Lstdkzhzb> getLstdkzhzb() {
        return lstdkzhzb;
    }

    public void setLstdkzhzb(List<Lstdkzhzb> lstdkzhzb) {
        this.lstdkzhzb = lstdkzhzb;
    }

    @Override
    public String toString() {
        return "Ln3108RespDto{" +
                ", zongbish=" + zongbish +
                ", lstdkzhzb=" + lstdkzhzb +
                '}';
    }
}
