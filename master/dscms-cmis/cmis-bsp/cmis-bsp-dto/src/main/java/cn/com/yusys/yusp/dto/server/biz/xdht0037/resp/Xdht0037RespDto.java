package cn.com.yusys.yusp.dto.server.biz.xdht0037.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：大家e贷、乐悠金根据客户号查询房贷首付款比例
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0037RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdht0037RespDto{" +
				"data=" + data +
				'}';
	}
}
