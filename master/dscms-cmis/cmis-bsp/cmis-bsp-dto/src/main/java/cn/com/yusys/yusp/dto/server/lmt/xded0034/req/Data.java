package cn.com.yusys.yusp.dto.server.lmt.xded0034.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 国结票据出账额度占用
 * add by dumd 20210623
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sysNo")
    private String sysNo;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "bizNo")
    private String bizNo;//合同编号
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期


    /**
     * 台账明细
     */
    @JsonProperty(value = "dealBizList")
    private List<DealBizList> dealBizList;

    /**
     * 占用额度列表(同业客户额度)
     */
    @JsonProperty(value = "occRelList")
    private List<OccRelList> occRelList;

    public List<DealBizList> getDealBizList() {
        return dealBizList;
    }

    public void setDealBizList(List<DealBizList> dealBizList) {
        this.dealBizList = dealBizList;
    }

    public List<OccRelList> getOccRelList() {
        return occRelList;
    }

    public void setOccRelList(List<OccRelList> occRelList) {
        this.occRelList = occRelList;
    }

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", serno='" + serno + '\'' +
                ", bizNo='" + bizNo + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", dealBizList=" + dealBizList +
                ", occRelList=" + occRelList +
                '}';
    }
}
