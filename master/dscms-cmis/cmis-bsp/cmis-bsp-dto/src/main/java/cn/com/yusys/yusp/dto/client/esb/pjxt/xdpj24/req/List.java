package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：票据承兑签发审批请求
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "batchNo")
    private String batchNo;//批次号

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }
    @Override
    public String toString() {
        return "List{" +
                "batchNo='" + batchNo + '\'' +
                '}';
    }
}