package cn.com.yusys.yusp.dto.client.esb.wx.wxp001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷将审批结果推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxp001RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}  
