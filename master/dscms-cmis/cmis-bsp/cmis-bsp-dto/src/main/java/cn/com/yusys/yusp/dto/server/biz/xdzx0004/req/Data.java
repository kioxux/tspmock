package cn.com.yusys.yusp.dto.server.biz.xdzx0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：授权结果反馈接口
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "crqlSerno")
    private String crqlSerno;//征信查询流水号

    public String getCrqlSerno() {
        return crqlSerno;
    }

    public void setCrqlSerno(String crqlSerno) {
        this.crqlSerno = crqlSerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "crqlSerno='" + crqlSerno + '\'' +
                '}';
    }
}  
