package cn.com.yusys.yusp.dto.server.biz.xdcz0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：查询敞口额度及保证金校验
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bizType")
    private String bizType;//业务类型
    @JsonProperty(value = "oprtype")
    private String oprtype;//操作类型
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "busiClass")
    private String busiClass;//业务细分
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//放款金额
    @JsonProperty(value = "loanAmtExchgRat")
    private BigDecimal loanAmtExchgRat;//放款金额汇率
    @JsonProperty(value = "list")
    private java.util.List<List> list;//放款金额汇率

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    public String getOprtype() {
        return oprtype;
    }

    public void setOprtype(String oprtype) {
        this.oprtype = oprtype;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBusiClass() {
        return busiClass;
    }

    public void setBusiClass(String busiClass) {
        this.busiClass = busiClass;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanAmtExchgRat() {
        return loanAmtExchgRat;
    }

    public void setLoanAmtExchgRat(BigDecimal loanAmtExchgRat) {
        this.loanAmtExchgRat = loanAmtExchgRat;
    }

    @Override
    public String toString() {
        return "Data{" +
                "bizType='" + bizType + '\'' +
                ", oprtype='" + oprtype + '\'' +
                ", contNo='" + contNo + '\'' +
                ", busiClass='" + busiClass + '\'' +
                ", loanAmt=" + loanAmt +
                ", loanAmtExchgRat=" + loanAmtExchgRat +
                ", list=" + list +
                '}';
    }
}
