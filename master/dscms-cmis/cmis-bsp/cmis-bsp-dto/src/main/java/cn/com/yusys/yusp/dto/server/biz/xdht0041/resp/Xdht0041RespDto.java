package cn.com.yusys.yusp.dto.server.biz.xdht0041.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询受托记录状态
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0041RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdht0041RespDto{" +
                "data=" + data +
                '}';
    }
}