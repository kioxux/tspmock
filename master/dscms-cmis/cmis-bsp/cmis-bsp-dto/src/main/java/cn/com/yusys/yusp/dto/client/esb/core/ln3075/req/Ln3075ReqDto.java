package cn.com.yusys.yusp.dto.client.esb.core.ln3075.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：逾期贷款展期
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3075ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zhanqixh")
    private BigDecimal zhanqixh;//展期序号
    @JsonProperty(value = "zhanqirq")
    private String zhanqirq;//展期日期
    @JsonProperty(value = "zhanqije")
    private BigDecimal zhanqije;//展期金额
    @JsonProperty(value = "zhanqhth")
    private String zhanqhth;//展期合同号
    @JsonProperty(value = "zhanqdqr")
    private String zhanqdqr;//展期到期日
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率

    public BigDecimal getZhanqixh() {
        return zhanqixh;
    }

    public void setZhanqixh(BigDecimal zhanqixh) {
        this.zhanqixh = zhanqixh;
    }

    public String getZhanqirq() {
        return zhanqirq;
    }

    public void setZhanqirq(String zhanqirq) {
        this.zhanqirq = zhanqirq;
    }

    public BigDecimal getZhanqije() {
        return zhanqije;
    }

    public void setZhanqije(BigDecimal zhanqije) {
        this.zhanqije = zhanqije;
    }

    public String getZhanqhth() {
        return zhanqhth;
    }

    public void setZhanqhth(String zhanqhth) {
        this.zhanqhth = zhanqhth;
    }

    public String getZhanqdqr() {
        return zhanqdqr;
    }

    public void setZhanqdqr(String zhanqdqr) {
        this.zhanqdqr = zhanqdqr;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    @Override
    public String toString() {
        return "Ln3075ReqDto{" +
                "zhanqixh=" + zhanqixh +
                ", zhanqirq='" + zhanqirq + '\'' +
                ", zhanqije=" + zhanqije +
                ", zhanqhth='" + zhanqhth + '\'' +
                ", zhanqdqr='" + zhanqdqr + '\'' +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvleix='" + lilvleix + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", lilvfdfs='" + lilvfdfs + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", zhchlilv=" + zhchlilv +
                '}';
    }
}
