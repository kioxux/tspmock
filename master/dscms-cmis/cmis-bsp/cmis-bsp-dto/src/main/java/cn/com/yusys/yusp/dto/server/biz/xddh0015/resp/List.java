package cn.com.yusys.yusp.dto.server.biz.xddh0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 16:46:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 16:46
 * @since 2021/5/22 16:46
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "commonDebitCertNo")
    private String commonDebitCertNo;//共同借款人证件号
    @JsonProperty(value = "commonDebitName")
    private String commonDebitName;//共同借款人名称
    @JsonProperty(value = "signFlag")
    private String signFlag;//是否签约
    @JsonProperty(value = "subRela")
    private String subRela;//主副关系

    public String getCommonDebitCertNo() {
        return commonDebitCertNo;
    }

    public void setCommonDebitCertNo(String commonDebitCertNo) {
        this.commonDebitCertNo = commonDebitCertNo;
    }

    public String getCommonDebitName() {
        return commonDebitName;
    }

    public void setCommonDebitName(String commonDebitName) {
        this.commonDebitName = commonDebitName;
    }

    public String getSignFlag() {
        return signFlag;
    }

    public void setSignFlag(String signFlag) {
        this.signFlag = signFlag;
    }

    public String getSubRela() {
        return subRela;
    }

    public void setSubRela(String subRela) {
        this.subRela = subRela;
    }

    @Override
    public String toString() {
        return "List{" +
                "commonDebitCertNo='" + commonDebitCertNo + '\'' +
                ", commonDebitName='" + commonDebitName + '\'' +
                ", signFlag='" + signFlag + '\'' +
                ", subRela='" + subRela + '\'' +
                '}';
    }
}
