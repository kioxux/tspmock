package cn.com.yusys.yusp.dto.client.esb.core.ln3160;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class LstKlnb_dkzrjj {
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "picihaoo")
    private String picihaoo;//批次号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "jiejuhao")
    private String jiejuhao;//借据号
    @JsonProperty(value = "zcrfshii")
    private String zcrfshii;//资产融通方式
    @JsonProperty(value = "zcrtbili")
    private BigDecimal zcrtbili;//资产融通比例
    @JsonProperty(value = "zczrjine")
    private BigDecimal zczrjine;//资产转让金额
    @JsonProperty(value = "fbsdlixi")
    private BigDecimal fbsdlixi;//封包时点利息
    @JsonProperty(value = "yhfbsdlx")
    private BigDecimal yhfbsdlx;//已还封包时点利息
    @JsonProperty(value = "zczrcpdm")
    private String zczrcpdm;//资产转让产品代码
    @JsonProperty(value = "zczrcpmc")
    private String zczrcpmc;//资产转让产品名称
    @JsonProperty(value = "zrcpdmzy")
    private String zrcpdmzy;//部分转让产品代码(自营)
    @JsonProperty(value = "zrcpmczy")
    private String zrcpmczy;//部分转让产品名称(自营)
    @JsonProperty(value = "zrcpdmtg")
    private String zrcpdmtg;//部分转让产品代码(托管)
    @JsonProperty(value = "zrcpmctg")
    private String zrcpmctg;//部分转让产品名称(托管)
    @JsonProperty(value = "fbhrcqhb")
    private BigDecimal fbhrcqhb;//封包后入池前还本金额
    @JsonProperty(value = "fbhrcqhx")
    private BigDecimal fbhrcqhx;//封包后入池前还息金额
    @JsonProperty(value = "fbhrcqfy")
    private BigDecimal fbhrcqfy;//封包后入池前归还费用金额
    @JsonProperty(value = "fbhrcqfj")
    private BigDecimal fbhrcqfj;//封包后入池前归还罚金金额
    @JsonProperty(value = "zjjjclzt")
    private String zjjjclzt;//资产借据处理状态
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "xieyilil")
    private BigDecimal xieyilil;//协议利率
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "jjzrlilv")
    private BigDecimal jjzrlilv;//借据转让利率
    @JsonProperty(value = "llqxkdfs")
    private String llqxkdfs;//利率期限靠档方式
    @JsonProperty(value = "lilvqixx")
    private String lilvqixx;//利率期限
    @JsonProperty(value = "lilvbhao")
    private String lilvbhao;//利率编号
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "fbsdfeiy")
    private BigDecimal fbsdfeiy;//封包时点费用
    @JsonProperty(value = "yhfbsdfy")
    private BigDecimal yhfbsdfy;//已还封包时点费用
}
