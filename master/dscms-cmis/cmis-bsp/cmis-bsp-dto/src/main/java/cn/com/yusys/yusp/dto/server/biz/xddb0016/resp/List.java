package cn.com.yusys.yusp.dto.server.biz.xddb0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：押品信息查询
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "collid")
    private String collid;//抵押唯一ID
    @JsonProperty(value = "collno")
    private String collno;//抵押物编号
    @JsonProperty(value = "mortna")
    private String mortna;//抵押人名称
    @JsonProperty(value = "moclco")
    private String moclco;//抵押人客户码
    @JsonProperty(value = "modonu")
    private String modonu;//抵押人证件号码
    @JsonProperty(value = "collva")
    private BigDecimal collva;//抵押物价值
    @JsonProperty(value = "collty")
    private String collty;//抵押物类型

    public String getCollid() {
        return collid;
    }

    public void setCollid(String collid) {
        this.collid = collid;
    }

    public String getCollno() {
        return collno;
    }

    public void setCollno(String collno) {
        this.collno = collno;
    }

    public String getMortna() {
        return mortna;
    }

    public void setMortna(String mortna) {
        this.mortna = mortna;
    }

    public String getMoclco() {
        return moclco;
    }

    public void setMoclco(String moclco) {
        this.moclco = moclco;
    }

    public String getModonu() {
        return modonu;
    }

    public void setModonu(String modonu) {
        this.modonu = modonu;
    }

    public BigDecimal getCollva() {
        return collva;
    }

    public void setCollva(BigDecimal collva) {
        this.collva = collva;
    }

    public String getCollty() {
        return collty;
    }

    public void setCollty(String collty) {
        this.collty = collty;
    }

    @Override
    public String toString() {
        return "List{" +
                "collid='" + collid + '\'' +
                ", collno='" + collno + '\'' +
                ", mortna='" + mortna + '\'' +
                ", moclco='" + moclco + '\'' +
                ", modonu='" + modonu + '\'' +
                ", collva='" + collva + '\'' +
                ", collty='" + collty + '\'' +
                '}';
    }
}
