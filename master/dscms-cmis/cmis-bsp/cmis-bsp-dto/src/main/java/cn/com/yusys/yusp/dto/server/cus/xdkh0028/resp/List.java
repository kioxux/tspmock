package cn.com.yusys.yusp.dto.server.cus.xdkh0028.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：优农贷黑名单查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List  implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//主键
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cus_cert_code")
    private String cus_cert_code;//客户证件号
    @JsonProperty(value = "tel_phone")
    private String tel_phone;//手机号
    @JsonProperty(value = "sexsex")
    private String sexsex;//性别
    @JsonProperty(value = "edu_stateedu_state")
    private String edu_stateedu_state;//学历
    @JsonProperty(value = "chilren_state")
    private String chilren_state;//有无子女
    @JsonProperty(value = "marr_statemarr_state")
    private String marr_statemarr_state;//婚姻状况
    @JsonProperty(value = "spouse_name")
    private String spouse_name;//配偶名称
    @JsonProperty(value = "spouse_cert_code")
    private String spouse_cert_code;//配偶证件号
    @JsonProperty(value = "business_years")
    private String business_years;//经营年限
    @JsonProperty(value = "business_size")
    private String business_size;//经营规模
    @JsonProperty(value = "annual_income")
    private BigDecimal annual_income;//年销售收入
    @JsonProperty(value = "annual_profits")
    private BigDecimal annual_profits;//年利润
    @JsonProperty(value = "recommend_org")
    private String recommend_org;//推荐机构名称
    @JsonProperty(value = "list_state")
    private String list_state;//名单状态
    @JsonProperty(value = "create_date")
    private String create_date;//入库日期
    @JsonProperty(value = "modify_date")
    private String modify_date;//最近一次变更日期
    @JsonProperty(value = "cus_mgr_id")
    private String cus_mgr_id;//管户经理编号
    @JsonProperty(value = "buessine_addr")
    private String buessine_addr;//经营地址
    @JsonProperty(value = "industry_code")
    private String industry_code;//一级行业代码
    @JsonProperty(value = "industry_name")
    private String industry_name;//一级行业名称
    @JsonProperty(value = "industry_detail_code")
    private String industry_detail_code;//细分行业代码
    @JsonProperty(value = "industry_detail_name")
    private String industry_detail_name;//细分行业名称

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_cert_code() {
        return cus_cert_code;
    }

    public void setCus_cert_code(String cus_cert_code) {
        this.cus_cert_code = cus_cert_code;
    }

    public String getTel_phone() {
        return tel_phone;
    }

    public void setTel_phone(String tel_phone) {
        this.tel_phone = tel_phone;
    }

    public String getSexsex() {
        return sexsex;
    }

    public void setSexsex(String sexsex) {
        this.sexsex = sexsex;
    }

    public String getEdu_stateedu_state() {
        return edu_stateedu_state;
    }

    public void setEdu_stateedu_state(String edu_stateedu_state) {
        this.edu_stateedu_state = edu_stateedu_state;
    }

    public String getChilren_state() {
        return chilren_state;
    }

    public void setChilren_state(String chilren_state) {
        this.chilren_state = chilren_state;
    }

    public String getMarr_statemarr_state() {
        return marr_statemarr_state;
    }

    public void setMarr_statemarr_state(String marr_statemarr_state) {
        this.marr_statemarr_state = marr_statemarr_state;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_cert_code() {
        return spouse_cert_code;
    }

    public void setSpouse_cert_code(String spouse_cert_code) {
        this.spouse_cert_code = spouse_cert_code;
    }

    public String getBusiness_years() {
        return business_years;
    }

    public void setBusiness_years(String business_years) {
        this.business_years = business_years;
    }

    public String getBusiness_size() {
        return business_size;
    }

    public void setBusiness_size(String business_size) {
        this.business_size = business_size;
    }

    public BigDecimal getAnnual_income() {
        return annual_income;
    }

    public void setAnnual_income(BigDecimal annual_income) {
        this.annual_income = annual_income;
    }

    public BigDecimal getAnnual_profits() {
        return annual_profits;
    }

    public void setAnnual_profits(BigDecimal annual_profits) {
        this.annual_profits = annual_profits;
    }

    public String getRecommend_org() {
        return recommend_org;
    }

    public void setRecommend_org(String recommend_org) {
        this.recommend_org = recommend_org;
    }

    public String getList_state() {
        return list_state;
    }

    public void setList_state(String list_state) {
        this.list_state = list_state;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getModify_date() {
        return modify_date;
    }

    public void setModify_date(String modify_date) {
        this.modify_date = modify_date;
    }

    public String getCus_mgr_id() {
        return cus_mgr_id;
    }

    public void setCus_mgr_id(String cus_mgr_id) {
        this.cus_mgr_id = cus_mgr_id;
    }

    public String getBuessine_addr() {
        return buessine_addr;
    }

    public void setBuessine_addr(String buessine_addr) {
        this.buessine_addr = buessine_addr;
    }

    public String getIndustry_code() {
        return industry_code;
    }

    public void setIndustry_code(String industry_code) {
        this.industry_code = industry_code;
    }

    public String getIndustry_name() {
        return industry_name;
    }

    public void setIndustry_name(String industry_name) {
        this.industry_name = industry_name;
    }

    public String getIndustry_detail_code() {
        return industry_detail_code;
    }

    public void setIndustry_detail_code(String industry_detail_code) {
        this.industry_detail_code = industry_detail_code;
    }

    public String getIndustry_detail_name() {
        return industry_detail_name;
    }

    public void setIndustry_detail_name(String industry_detail_name) {
        this.industry_detail_name = industry_detail_name;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cus_cert_code='" + cus_cert_code + '\'' +
                ", tel_phone='" + tel_phone + '\'' +
                ", sexsex='" + sexsex + '\'' +
                ", edu_stateedu_state='" + edu_stateedu_state + '\'' +
                ", chilren_state='" + chilren_state + '\'' +
                ", marr_statemarr_state='" + marr_statemarr_state + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", spouse_cert_code='" + spouse_cert_code + '\'' +
                ", business_years=" + business_years +
                ", business_size='" + business_size + '\'' +
                ", annual_income=" + annual_income +
                ", annual_profits=" + annual_profits +
                ", recommend_org='" + recommend_org + '\'' +
                ", list_state='" + list_state + '\'' +
                ", create_date='" + create_date + '\'' +
                ", modify_date='" + modify_date + '\'' +
                ", cus_mgr_id='" + cus_mgr_id + '\'' +
                ", buessine_addr='" + buessine_addr + '\'' +
                ", industry_code='" + industry_code + '\'' +
                ", industry_name='" + industry_name + '\'' +
                ", industry_detail_code='" + industry_detail_code + '\'' +
                ", industry_detail_name='" + industry_detail_name + '\'' +
                '}';
    }
}