package cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class Lstdzqgjh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "benqqish")
    private Integer benqqish;//本期期数
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "hxijinee")
    private BigDecimal hxijinee;//还息金额
    @JsonProperty(value = "zwhkriqi")
    private String zwhkriqi;//最晚还款日

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public String getZwhkriqi() {
        return zwhkriqi;
    }

    public void setZwhkriqi(String zwhkriqi) {
        this.zwhkriqi = zwhkriqi;
    }

    @Override
    public String toString() {
        return "Lstdzqgjh{" +
                "benqqish=" + benqqish +
                ", qishriqi='" + qishriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", hxijinee=" + hxijinee +
                ", zwhkriqi='" + zwhkriqi + '\'' +
                '}';
    }
}
