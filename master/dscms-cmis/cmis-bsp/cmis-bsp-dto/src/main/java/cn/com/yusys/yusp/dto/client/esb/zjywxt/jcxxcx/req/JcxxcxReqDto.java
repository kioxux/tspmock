package cn.com.yusys.yusp.dto.client.esb.zjywxt.jcxxcx.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：缴存信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class JcxxcxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ipaddr")
    private String ipaddr;//请求方IP
    @JsonProperty(value = "mac")
    private String mac;//请求方MAC
    @JsonProperty(value = "xybhsq")
    private String xybhsq;//协议编号

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getXybhsq() {
        return xybhsq;
    }

    public void setXybhsq(String xybhsq) {
        this.xybhsq = xybhsq;
    }

    @Override
    public String toString() {
        return "JcxxcxReqDto{" +
                "ipaddr='" + ipaddr + '\'' +
                "mac='" + mac + '\'' +
                "xybhsq='" + xybhsq + '\'' +
                '}';
    }
}  
