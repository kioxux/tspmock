package cn.com.yusys.yusp.dto.client.esb.core.da3302.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：抵债资产处置
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3302RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "dzzcbxkx")
    private String dzzcbxkx;//抵债资产变现款项账号
    @JsonProperty(value = "dzzcbzxh")
    private String dzzcbzxh;//抵债资产变现账号子序号
    @JsonProperty(value = "dzzcbxje")
    private BigDecimal dzzcbxje;//抵债资产变现款金额
    @JsonProperty(value = "daizdzzc")
    private BigDecimal daizdzzc;//待转抵债资产
    @JsonProperty(value = "dcldzzic")
    private BigDecimal dcldzzic;//待处理抵债资产
    @JsonProperty(value = "dbxdzzic")
    private BigDecimal dbxdzzic;//待变现抵债资产
    @JsonProperty(value = "bxssjine")
    private BigDecimal bxssjine;//变现损失金额
    @JsonProperty(value = "yicldzzc")
    private BigDecimal yicldzzc;//已处置抵债资产
    @JsonProperty(value = "keyongje")
    private BigDecimal keyongje;//可用金额
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getDzzcbxkx() {
        return dzzcbxkx;
    }

    public void setDzzcbxkx(String dzzcbxkx) {
        this.dzzcbxkx = dzzcbxkx;
    }

    public String getDzzcbzxh() {
        return dzzcbzxh;
    }

    public void setDzzcbzxh(String dzzcbzxh) {
        this.dzzcbzxh = dzzcbzxh;
    }

    public BigDecimal getDzzcbxje() {
        return dzzcbxje;
    }

    public void setDzzcbxje(BigDecimal dzzcbxje) {
        this.dzzcbxje = dzzcbxje;
    }

    public BigDecimal getDaizdzzc() {
        return daizdzzc;
    }

    public void setDaizdzzc(BigDecimal daizdzzc) {
        this.daizdzzc = daizdzzc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getBxssjine() {
        return bxssjine;
    }

    public void setBxssjine(BigDecimal bxssjine) {
        this.bxssjine = bxssjine;
    }

    public BigDecimal getYicldzzc() {
        return yicldzzc;
    }

    public void setYicldzzc(BigDecimal yicldzzc) {
        this.yicldzzc = yicldzzc;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    @Override
    public String toString() {
        return "Da3302RespDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehuzwmc='" + kehuzwmc + '\'' +
                "dzzcbxkx='" + dzzcbxkx + '\'' +
                "dzzcbzxh='" + dzzcbzxh + '\'' +
                "dzzcbxje='" + dzzcbxje + '\'' +
                "daizdzzc='" + daizdzzc + '\'' +
                "dcldzzic='" + dcldzzic + '\'' +
                "dbxdzzic='" + dbxdzzic + '\'' +
                "bxssjine='" + bxssjine + '\'' +
                "yicldzzc='" + yicldzzc + '\'' +
                "keyongje='" + keyongje + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                '}';
    }
}  
