package cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "kehuzhlx")
    private String kehuzhlx;//客户账号类型
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "zhanghao")
    private String zhanghao;//负债账号
    @JsonProperty(value = "chanpshm")
    private String chanpshm;//产品说明
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh;//子账户序号
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//货币代号
    @JsonProperty(value = "chaohubz")
    private String chaohubz;//账户钞汇标志
    @JsonProperty(value = "zhanghye")
    private BigDecimal zhanghye;//账户余额
    @JsonProperty(value = "keyongye")
    private BigDecimal keyongye;//可用余额
    @JsonProperty(value = "zhhuztai")
    private String zhhuztai;//账户状态
    @JsonProperty(value = "cunqiiii")
    private String cunqiiii;//存期
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "doqiriqi")
    private String doqiriqi;//到期日期
    @JsonProperty(value = "kaihjigo")
    private String kaihjigo;//账户开户机构
    @JsonProperty(value = "zhxililv")
    private BigDecimal zhxililv;//当前执行利率
    @JsonProperty(value = "yegxriqi")
    private String yegxriqi;//余额最近更新日期
    @JsonProperty(value = "chapbhao")
    private String chapbhao;//产品编号
    @JsonProperty(value = "cunkzlei")
    private String cunkzlei;//存款种类
    @JsonProperty(value = "glpinzbz")
    private String glpinzbz;//关联凭证标志
    @JsonProperty(value = "kaihguiy")
    private String kaihguiy;//账户开户柜员
    @JsonProperty(value = "zhhuzwmc")
    private String zhhuzwmc;//账户名称
    @JsonProperty(value = "yezztbbz")
    private String yezztbbz;//余额与总账同步标志
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "keyonedu")
    private BigDecimal keyonedu;//可用额度
    @JsonProperty(value = "beiyzd01")
    private String beiyzd01;//备用字段01
    @JsonProperty(value = "beiyzd02")
    private String beiyzd02;//备用字段02
    @JsonProperty(value = "beiyzd03")
    private String beiyzd03;//备用字段03
    @JsonProperty(value = "beiyye01")
    private BigDecimal beiyye01;//备用余额01
    @JsonProperty(value = "beiyye02")
    private BigDecimal beiyye02;//备用余额02
    @JsonProperty(value = "beiyye03")
    private BigDecimal beiyye03;//备用余额03
    @JsonProperty(value = "zhhufenl")
    private String zhhufenl;//账户分类
    @JsonProperty(value = "zhshuxin")
    private String zhshuxin;//账户属性
    @JsonProperty(value = "cenjleix")
    private String cenjleix;//层级类型

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getChanpshm() {
        return chanpshm;
    }

    public void setChanpshm(String chanpshm) {
        this.chanpshm = chanpshm;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public BigDecimal getKeyongye() {
        return keyongye;
    }

    public void setKeyongye(BigDecimal keyongye) {
        this.keyongye = keyongye;
    }

    public String getZhhuztai() {
        return zhhuztai;
    }

    public void setZhhuztai(String zhhuztai) {
        this.zhhuztai = zhhuztai;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getKaihjigo() {
        return kaihjigo;
    }

    public void setKaihjigo(String kaihjigo) {
        this.kaihjigo = kaihjigo;
    }

    public BigDecimal getZhxililv() {
        return zhxililv;
    }

    public void setZhxililv(BigDecimal zhxililv) {
        this.zhxililv = zhxililv;
    }

    public String getYegxriqi() {
        return yegxriqi;
    }

    public void setYegxriqi(String yegxriqi) {
        this.yegxriqi = yegxriqi;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getGlpinzbz() {
        return glpinzbz;
    }

    public void setGlpinzbz(String glpinzbz) {
        this.glpinzbz = glpinzbz;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getYezztbbz() {
        return yezztbbz;
    }

    public void setYezztbbz(String yezztbbz) {
        this.yezztbbz = yezztbbz;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public BigDecimal getKeyonedu() {
        return keyonedu;
    }

    public void setKeyonedu(BigDecimal keyonedu) {
        this.keyonedu = keyonedu;
    }

    public String getBeiyzd01() {
        return beiyzd01;
    }

    public void setBeiyzd01(String beiyzd01) {
        this.beiyzd01 = beiyzd01;
    }

    public String getBeiyzd02() {
        return beiyzd02;
    }

    public void setBeiyzd02(String beiyzd02) {
        this.beiyzd02 = beiyzd02;
    }

    public String getBeiyzd03() {
        return beiyzd03;
    }

    public void setBeiyzd03(String beiyzd03) {
        this.beiyzd03 = beiyzd03;
    }

    public BigDecimal getBeiyye01() {
        return beiyye01;
    }

    public void setBeiyye01(BigDecimal beiyye01) {
        this.beiyye01 = beiyye01;
    }

    public BigDecimal getBeiyye02() {
        return beiyye02;
    }

    public void setBeiyye02(BigDecimal beiyye02) {
        this.beiyye02 = beiyye02;
    }

    public BigDecimal getBeiyye03() {
        return beiyye03;
    }

    public void setBeiyye03(BigDecimal beiyye03) {
        this.beiyye03 = beiyye03;
    }

    public String getZhhufenl() {
        return zhhufenl;
    }

    public void setZhhufenl(String zhhufenl) {
        this.zhhufenl = zhhufenl;
    }

    public String getZhshuxin() {
        return zhshuxin;
    }

    public void setZhshuxin(String zhshuxin) {
        this.zhshuxin = zhshuxin;
    }

    public String getCenjleix() {
        return cenjleix;
    }

    public void setCenjleix(String cenjleix) {
        this.cenjleix = cenjleix;
    }

    @Override
    public String toString() {
        return "List{" +
                "kehuzhlx='" + kehuzhlx + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", zhanghao='" + zhanghao + '\'' +
                ", chanpshm='" + chanpshm + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", huobdaih='" + huobdaih + '\'' +
                ", chaohubz='" + chaohubz + '\'' +
                ", zhanghye=" + zhanghye +
                ", keyongye=" + keyongye +
                ", zhhuztai='" + zhhuztai + '\'' +
                ", cunqiiii='" + cunqiiii + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", doqiriqi='" + doqiriqi + '\'' +
                ", kaihjigo='" + kaihjigo + '\'' +
                ", zhxililv=" + zhxililv +
                ", yegxriqi='" + yegxriqi + '\'' +
                ", chapbhao='" + chapbhao + '\'' +
                ", cunkzlei='" + cunkzlei + '\'' +
                ", glpinzbz='" + glpinzbz + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", zhhuzwmc='" + zhhuzwmc + '\'' +
                ", yezztbbz='" + yezztbbz + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", keyonedu=" + keyonedu +
                ", beiyzd01='" + beiyzd01 + '\'' +
                ", beiyzd02='" + beiyzd02 + '\'' +
                ", beiyzd03='" + beiyzd03 + '\'' +
                ", beiyye01=" + beiyye01 +
                ", beiyye02=" + beiyye02 +
                ", beiyye03=" + beiyye03 +
                ", zhhufenl='" + zhhufenl + '\'' +
                ", zhshuxin='" + zhshuxin + '\'' +
                ", cenjleix='" + cenjleix + '\'' +
                '}';
    }
}
