package cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷系统请求小V平台贷后预警回传定期检查任务结果
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dhxd02RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//返回码
    @JsonProperty(value = "erortx")
    private String erortx;//返回提示

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Dhxd02RespDto{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                '}';
    }
}  
