package cn.com.yusys.yusp.dto.server.biz.xdht0034.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto: 根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applyAmt")
    private String applyAmt;//申请金额

    public String getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(String applyAmt) {
        this.applyAmt = applyAmt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "applyAmt='" + applyAmt + '\'' +
                '}';
    }
}