package cn.com.yusys.yusp.dto.server.biz.xdtz0032.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：询客户所担保的行内当前贷款逾期件数
 * @Author zhangpeng
 * @Date 2021/4/27 14:34
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "curtOverdueLoanCnt")
    private String curtOverdueLoanCnt;//所担保的行内当前贷款逾期件数

    public String getCurtOverdueLoanCnt() {
        return curtOverdueLoanCnt;
    }

    public void setCurtOverdueLoanCnt(String curtOverdueLoanCnt) {
        this.curtOverdueLoanCnt = curtOverdueLoanCnt;
    }
}
