package cn.com.yusys.yusp.dto.client.esb.core.ln3163;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：资产证券化处理
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3163RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "benjheji")
    private BigDecimal benjheji;//本金合计
    @JsonProperty(value = "lixiheji")
    private BigDecimal lixiheji;//利息合计
    @JsonProperty(value = "bjyskzhh")
    private String bjyskzhh;//其他应收款账号(本金)
    @JsonProperty(value = "qyskzhzh")
    private String qyskzhzh;//其他应收款账号子序号(本金)
    @JsonProperty(value = "benjinje")
    private BigDecimal benjinje;//本金金额
    @JsonProperty(value = "lxyskzhh")
    private String lxyskzhh;//其他应收款账号(利息)
    @JsonProperty(value = "lxyskzxh")
    private String lxyskzxh;//其他应收款账号子序号(利息)
    @JsonProperty(value = "lixijine")
    private BigDecimal lixijine;//利息金额
    @JsonProperty(value = "bjysfzhh")
    private String bjysfzhh;//其他应付款账号(本金)
    @JsonProperty(value = "bjysfzxh")
    private String bjysfzxh;//其他应付款账号子序号(本金)
    @JsonProperty(value = "yinghkbj")
    private BigDecimal yinghkbj;//应还本金
    @JsonProperty(value = "lxysfzhh")
    private String lxysfzhh;//其他应付款账号(利息)
    @JsonProperty(value = "qtyfzhzx")
    private String qtyfzhzx;//其他应付款账号子序号(利息)
    @JsonProperty(value = "yinghklx")
    private BigDecimal yinghklx;//应还利息
    @JsonProperty(value = "zchzhtai")
    private String zchzhtai;//资产处理状态
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "lstzrjj")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3163.Lstzrjj> lstzrjj;//借据列表[LIST]


    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public BigDecimal getBenjheji() {
        return benjheji;
    }

    public void setBenjheji(BigDecimal benjheji) {
        this.benjheji = benjheji;
    }

    public BigDecimal getLixiheji() {
        return lixiheji;
    }

    public void setLixiheji(BigDecimal lixiheji) {
        this.lixiheji = lixiheji;
    }

    public String getBjyskzhh() {
        return bjyskzhh;
    }

    public void setBjyskzhh(String bjyskzhh) {
        this.bjyskzhh = bjyskzhh;
    }

    public String getQyskzhzh() {
        return qyskzhzh;
    }

    public void setQyskzhzh(String qyskzhzh) {
        this.qyskzhzh = qyskzhzh;
    }

    public BigDecimal getBenjinje() {
        return benjinje;
    }

    public void setBenjinje(BigDecimal benjinje) {
        this.benjinje = benjinje;
    }

    public String getLxyskzhh() {
        return lxyskzhh;
    }

    public void setLxyskzhh(String lxyskzhh) {
        this.lxyskzhh = lxyskzhh;
    }

    public String getLxyskzxh() {
        return lxyskzxh;
    }

    public void setLxyskzxh(String lxyskzxh) {
        this.lxyskzxh = lxyskzxh;
    }

    public BigDecimal getLixijine() {
        return lixijine;
    }

    public void setLixijine(BigDecimal lixijine) {
        this.lixijine = lixijine;
    }

    public String getBjysfzhh() {
        return bjysfzhh;
    }

    public void setBjysfzhh(String bjysfzhh) {
        this.bjysfzhh = bjysfzhh;
    }

    public String getBjysfzxh() {
        return bjysfzxh;
    }

    public void setBjysfzxh(String bjysfzxh) {
        this.bjysfzxh = bjysfzxh;
    }

    public BigDecimal getYinghkbj() {
        return yinghkbj;
    }

    public void setYinghkbj(BigDecimal yinghkbj) {
        this.yinghkbj = yinghkbj;
    }

    public String getLxysfzhh() {
        return lxysfzhh;
    }

    public void setLxysfzhh(String lxysfzhh) {
        this.lxysfzhh = lxysfzhh;
    }

    public String getQtyfzhzx() {
        return qtyfzhzx;
    }

    public void setQtyfzhzx(String qtyfzhzx) {
        this.qtyfzhzx = qtyfzhzx;
    }

    public BigDecimal getYinghklx() {
        return yinghklx;
    }

    public void setYinghklx(BigDecimal yinghklx) {
        this.yinghklx = yinghklx;
    }

    public String getZchzhtai() {
        return zchzhtai;
    }

    public void setZchzhtai(String zchzhtai) {
        this.zchzhtai = zchzhtai;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

	public List<Lstzrjj> getLstzrjj() {
		return lstzrjj;
	}

	public void setLstzrjj(List<Lstzrjj> lstzrjj) {
		this.lstzrjj = lstzrjj;
	}

	@Override
	public String toString() {
		return "Ln3163RespDto{" +
				"xieybhao='" + xieybhao + '\'' +
				", xieyimch='" + xieyimch + '\'' +
				", zongbish=" + zongbish +
				", benjheji=" + benjheji +
				", lixiheji=" + lixiheji +
				", bjyskzhh='" + bjyskzhh + '\'' +
				", qyskzhzh='" + qyskzhzh + '\'' +
				", benjinje=" + benjinje +
				", lxyskzhh='" + lxyskzhh + '\'' +
				", lxyskzxh='" + lxyskzxh + '\'' +
				", lixijine=" + lixijine +
				", bjysfzhh='" + bjysfzhh + '\'' +
				", bjysfzxh='" + bjysfzxh + '\'' +
				", yinghkbj=" + yinghkbj +
				", lxysfzhh='" + lxysfzhh + '\'' +
				", qtyfzhzx='" + qtyfzhzx + '\'' +
				", yinghklx=" + yinghklx +
				", zchzhtai='" + zchzhtai + '\'' +
				", jiaoyirq='" + jiaoyirq + '\'' +
				", jiaoyigy='" + jiaoyigy + '\'' +
				", yngyjigo='" + yngyjigo + '\'' +
				", jiaoyils='" + jiaoyils + '\'' +
				", lstzrjj='" + lstzrjj + '\'' +
				'}';
	}
}
