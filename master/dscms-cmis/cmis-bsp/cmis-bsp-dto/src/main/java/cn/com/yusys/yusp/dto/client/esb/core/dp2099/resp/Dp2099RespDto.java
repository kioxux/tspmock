package cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：保证金账户查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2099RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzhmc")
    private String kehuzhmc;//客户账户名称
    @JsonProperty(value = "lstacctinfo")
    private String lstacctinfo;//查询结果列表展示
    @JsonProperty(value = "chaxfanw")
    private String chaxfanw;//查询范围
    @JsonProperty(value = "zongbshu")
    private Integer zongbshu;//总笔数
    @JsonProperty(value = "bblujing")
    private String bblujing;//报表路径
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getLstacctinfo() {
        return lstacctinfo;
    }

    public void setLstacctinfo(String lstacctinfo) {
        this.lstacctinfo = lstacctinfo;
    }

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    public String getBblujing() {
        return bblujing;
    }

    public void setBblujing(String bblujing) {
        this.bblujing = bblujing;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Dp2099RespDto{" +
                "kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzhmc='" + kehuzhmc + '\'' +
                ", lstacctinfo='" + lstacctinfo + '\'' +
                ", chaxfanw='" + chaxfanw + '\'' +
                ", zongbshu=" + zongbshu +
                ", bblujing='" + bblujing + '\'' +
                ", list=" + list +
                '}';
    }
}
