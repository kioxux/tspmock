package cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：押品权证出库接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Cwm004RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("erorcd")
    private String erorcd;//	响应码	否	Char(4)	是
    @JsonProperty("erortx")
    private String erortx;//	响应信息	否	Char(120)	是
    @JsonProperty("returnCode")
    private String returnCode; // 处理结果
    @JsonProperty("returnInfo")
    private String returnInfo; // 处理信息


    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnInfo() {
        return returnInfo;
    }

    public void setReturnInfo(String returnInfo) {
        this.returnInfo = returnInfo;
    }

    @Override
    public String toString() {
        return "Cwm004RespDto{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", returnCode='" + returnCode + '\'' +
                ", returnInfo='" + returnInfo + '\'' +
                '}';
    }
}
