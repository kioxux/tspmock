package cn.com.yusys.yusp.dto.server.biz.xdsx0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：审批人资本占用率参数表同步
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dutyno")
    private String dutyno;//岗位编号
    @JsonProperty(value = "lowerval")
    private String lowerval;//最小值
    @JsonProperty(value = "upval")
    private String upval;//最大值

    public String getDutyno() {
        return dutyno;
    }

    public void setDutyno(String dutyno) {
        this.dutyno = dutyno;
    }

    public String getLowerval() {
        return lowerval;
    }

    public void setLowerval(String lowerval) {
        this.lowerval = lowerval;
    }

    public String getUpval() {
        return upval;
    }

    public void setUpval(String upval) {
        this.upval = upval;
    }

    @Override
    public String toString() {
        return "List{" +
                "dutyno='" + dutyno + '\'' +
                "lowerval='" + lowerval + '\'' +
                "upval='" + upval + '\'' +
                '}';
    }
}
