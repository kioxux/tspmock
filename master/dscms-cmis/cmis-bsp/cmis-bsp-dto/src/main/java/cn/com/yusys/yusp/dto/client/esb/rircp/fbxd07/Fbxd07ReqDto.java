package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd07;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：获取指定数据日期存在还款记录的借据一览信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd07ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data_date")
    private String data_date;//数据日期

    public String getData_date() {
        return data_date;
    }

    public void setData_date(String data_date) {
        this.data_date = data_date;
    }

    @Override
    public String toString() {
        return "Fbxd07ReqDto{" +
                "data_date='" + data_date + '\'' +
                '}';
    }
}  
