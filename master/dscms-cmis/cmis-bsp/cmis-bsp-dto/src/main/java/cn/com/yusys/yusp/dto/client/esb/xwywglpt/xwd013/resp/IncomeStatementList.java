package cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class IncomeStatementList implements Serializable {
    private static final long serialVersionUID = 1L;


    @JsonProperty(value = "totalsalFullYear")
    private String totalsalFullYear;//销售收入-总额 - 本期
    @JsonProperty(value = "variableMatYear")
    private String variableMatYear;//可变成本-总额 - 本期
    @JsonProperty(value = "grossYearRecently")
    private String grossYearRecently;//毛利润 - 本期
    @JsonProperty(value = "fixedManagecostFullYear")
    private String fixedManagecostFullYear;//固定支出-总额 - 本期
    @JsonProperty(value = "recentlyNetProfit")
    private String recentlyNetProfit;//净利润 - 本期


    public String getTotalsalFullYear() {
        return totalsalFullYear;
    }

    public void setTotalsalFullYear(String totalsalFullYear) {
        this.totalsalFullYear = totalsalFullYear;
    }

    public String getVariableMatYear() {
        return variableMatYear;
    }

    public void setVariableMatYear(String variableMatYear) {
        this.variableMatYear = variableMatYear;
    }

    public String getGrossYearRecently() {
        return grossYearRecently;
    }

    public void setGrossYearRecently(String grossYearRecently) {
        this.grossYearRecently = grossYearRecently;
    }

    public String getFixedManagecostFullYear() {
        return fixedManagecostFullYear;
    }

    public void setFixedManagecostFullYear(String fixedManagecostFullYear) {
        this.fixedManagecostFullYear = fixedManagecostFullYear;
    }

    public String getRecentlyNetProfit() {
        return recentlyNetProfit;
    }

    public void setRecentlyNetProfit(String recentlyNetProfit) {
        this.recentlyNetProfit = recentlyNetProfit;
    }

    @Override
    public String toString() {
        return "IncomeStatementList{" +
                "totalsalFullYear='" + totalsalFullYear + '\'' +
                ", variableMatYear='" + variableMatYear + '\'' +
                ", grossYearRecently='" + grossYearRecently + '\'' +
                ", fixedManagecostFullYear='" + fixedManagecostFullYear + '\'' +
                ", recentlyNetProfit='" + recentlyNetProfit + '\'' +
                '}';
    }
}
