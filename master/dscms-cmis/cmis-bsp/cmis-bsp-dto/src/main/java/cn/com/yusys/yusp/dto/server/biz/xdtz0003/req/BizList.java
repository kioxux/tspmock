package cn.com.yusys.yusp.dto.server.biz.xdtz0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/5/7 9:53
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class BizList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "bizType")
    private String bizType;//业务类型

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    @Override
    public String toString() {
        return "BizList{" +
                "bizType='" + bizType + '\'' +
                '}';
    }
}
