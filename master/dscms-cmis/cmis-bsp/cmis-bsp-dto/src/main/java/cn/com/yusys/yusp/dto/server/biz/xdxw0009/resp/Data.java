package cn.com.yusys.yusp.dto.server.biz.xdxw0009.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：担保人人数查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//调查流水号
    @JsonProperty(value = "gageId")
    private String gageId;//押品编号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称
    @JsonProperty(value = "orgNo")
    private String orgNo;//机构号
    @JsonProperty(value = "orgName")
    private String orgName;//机构名称
    @JsonProperty(value = "contSealCode")
    private String contSealCode;//合同用章编码
    @JsonProperty(value = "pldSealCode")
    private String pldSealCode;//抵押用章编码

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getGageId() {
        return gageId;
    }

    public void setGageId(String gageId) {
        this.gageId = gageId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getContSealCode() {
        return contSealCode;
    }

    public void setContSealCode(String contSealCode) {
        this.contSealCode = contSealCode;
    }

    public String getPldSealCode() {
        return pldSealCode;
    }

    public void setPldSealCode(String pldSealCode) {
        this.pldSealCode = pldSealCode;
    }

    @Override
    public String toString() {
        return "Data{" +
                "indgtSerno='" + indgtSerno + '\'' +
                ", gageId='" + gageId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", orgName='" + orgName + '\'' +
                ", contSealCode='" + contSealCode + '\'' +
                ", pldSealCode='" + pldSealCode + '\'' +
                '}';
    }
}
