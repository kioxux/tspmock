package cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lsnp01RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "apply_seq")
    private String apply_seq;//申请流水号
    @JsonProperty(value = "trstatus")
    private String trstatus;
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.List> list;

    public String getApply_seq() {
        return apply_seq;
    }

    public void setApply_seq(String apply_seq) {
        this.apply_seq = apply_seq;
    }


    public String getTrstatus() {
        return trstatus;
    }

    public void setTrstatus(String trstatus) {
        this.trstatus = trstatus;
    }

    public List<cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.List> getList() {
        return list;
    }

    public void setList(List<cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Lsnp01RespDto{" +
                "apply_seq='" + apply_seq + '\'' +
                ", trstatus='" + trstatus + '\'' +
                ", list=" + list +
                '}';
    }
}
