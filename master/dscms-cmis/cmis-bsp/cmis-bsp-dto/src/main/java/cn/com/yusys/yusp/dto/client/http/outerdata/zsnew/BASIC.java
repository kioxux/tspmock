package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 企业照面信息
 */
@JsonPropertyOrder(alphabetic = true)
public class BASIC implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ABUITEM")
    private String ABUITEM;  // 许可经营项目
    @JsonProperty(value = "ANCHEYEAR")
    private String ANCHEYEAR;  // 最后年检年度
    @JsonProperty(value = "APPRDATE")
    private String APPRDATE;  // 核准日期
    @JsonProperty(value = "CANDATE")
    private String CANDATE;  // 注销日期
    @JsonProperty(value = "CREDITCODE")
    private String CREDITCODE;  // 统一信用代码
    @JsonProperty(value = "DOM")
    private String DOM;  // 住址
    @JsonProperty(value = "EMAIL")
    private String EMAIL;  // 邮箱
    @JsonProperty(value = "ENTITYTYPE")
    private String ENTITYTYPE;  // 实体类型
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;  // 企业名称
    @JsonProperty(value = "ENTNAME_OLD")
    private String ENTNAME_OLD;  // 曾用名
    @JsonProperty(value = "ENTSTATUS")
    private String ENTSTATUS;  // 经营状态
    @JsonProperty(value = "ENTSTATUSCODE")
    private String ENTSTATUSCODE;  // 经营状态编码
    @JsonProperty(value = "ENTTYPE")
    private String ENTTYPE;  // 企业类型
    @JsonProperty(value = "ENTTYPECODE")
    private String ENTTYPECODE;  // 企业(机构)类型编码
    @JsonProperty(value = "ESDATE")
    private String ESDATE;  // 成立日期
    @JsonProperty(value = "FRNAME")
    private String FRNAME;  // 法定代表人/负责人/执行事务合伙人
    @JsonProperty(value = "ID")
    private String ID;  // 企业ID
    @JsonProperty(value = "INDUSTRYPHYNAME")
    private String INDUSTRYPHYNAME;  // 行业门类名称
    @JsonProperty(value = "OPFROM")
    private String OPFROM;  // 经营期限自
    @JsonProperty(value = "OPTO")
    private String OPTO;  // 经营期限至
    @JsonProperty(value = "ORGCODES")
    private String ORGCODES;  // 组织机构代码
    @JsonProperty(value = "POSTALCODE")
    private String POSTALCODE;  // 邮编
    @JsonProperty(value = "REGCAP")
    private String REGCAP;  // 注册资本（企业:万元）
    @JsonProperty(value = "REGCAPCUR")
    private String REGCAPCUR;  // 注册资本币种
    @JsonProperty(value = "REGCAPCURCODE")
    private String REGCAPCURCODE;  // 注册资本币种代码
    @JsonProperty(value = "REGCITY")
    private String REGCITY;  // 所在城市编码
    @JsonProperty(value = "REGNO")
    private String REGNO;  // 注册号
    @JsonProperty(value = "REGORG")
    private String REGORG;  // 登记机关
    @JsonProperty(value = "REGORGCITY")
    private String REGORGCITY;  // 所在城市
    @JsonProperty(value = "REGORGCODE")
    private String REGORGCODE;  // 注册地址行政编号
    @JsonProperty(value = "REGORGDISTRICT")
    private String REGORGDISTRICT;  // 所在区/县
    @JsonProperty(value = "REGORGPROVINCE")
    private String REGORGPROVINCE;  // 所在省份
    @JsonProperty(value = "REVDATE")
    private String REVDATE;  // 吊销日期
    @JsonProperty(value = "S_EXT_NODENUM")
    private String S_EXT_NODENUM;  // 所在省份编码
    @JsonProperty(value = "ZSOPSCOPE")
    private String ZSOPSCOPE;  // 经营业务范围

    @JsonIgnore
    public String getABUITEM() {
        return ABUITEM;
    }

    @JsonIgnore
    public void setABUITEM(String ABUITEM) {
        this.ABUITEM = ABUITEM;
    }

    @JsonIgnore
    public String getANCHEYEAR() {
        return ANCHEYEAR;
    }

    @JsonIgnore
    public void setANCHEYEAR(String ANCHEYEAR) {
        this.ANCHEYEAR = ANCHEYEAR;
    }

    @JsonIgnore
    public String getAPPRDATE() {
        return APPRDATE;
    }

    @JsonIgnore
    public void setAPPRDATE(String APPRDATE) {
        this.APPRDATE = APPRDATE;
    }

    @JsonIgnore
    public String getCANDATE() {
        return CANDATE;
    }

    @JsonIgnore
    public void setCANDATE(String CANDATE) {
        this.CANDATE = CANDATE;
    }

    @JsonIgnore
    public String getCREDITCODE() {
        return CREDITCODE;
    }

    @JsonIgnore
    public void setCREDITCODE(String CREDITCODE) {
        this.CREDITCODE = CREDITCODE;
    }

    @JsonIgnore
    public String getDOM() {
        return DOM;
    }

    @JsonIgnore
    public void setDOM(String DOM) {
        this.DOM = DOM;
    }

    @JsonIgnore
    public String getEMAIL() {
        return EMAIL;
    }

    @JsonIgnore
    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    @JsonIgnore
    public String getENTITYTYPE() {
        return ENTITYTYPE;
    }

    @JsonIgnore
    public void setENTITYTYPE(String ENTITYTYPE) {
        this.ENTITYTYPE = ENTITYTYPE;
    }

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getENTNAME_OLD() {
        return ENTNAME_OLD;
    }

    @JsonIgnore
    public void setENTNAME_OLD(String ENTNAME_OLD) {
        this.ENTNAME_OLD = ENTNAME_OLD;
    }

    @JsonIgnore
    public String getENTSTATUS() {
        return ENTSTATUS;
    }

    @JsonIgnore
    public void setENTSTATUS(String ENTSTATUS) {
        this.ENTSTATUS = ENTSTATUS;
    }

    @JsonIgnore
    public String getENTSTATUSCODE() {
        return ENTSTATUSCODE;
    }

    @JsonIgnore
    public void setENTSTATUSCODE(String ENTSTATUSCODE) {
        this.ENTSTATUSCODE = ENTSTATUSCODE;
    }

    @JsonIgnore
    public String getENTTYPE() {
        return ENTTYPE;
    }

    @JsonIgnore
    public void setENTTYPE(String ENTTYPE) {
        this.ENTTYPE = ENTTYPE;
    }

    @JsonIgnore
    public String getENTTYPECODE() {
        return ENTTYPECODE;
    }

    @JsonIgnore
    public void setENTTYPECODE(String ENTTYPECODE) {
        this.ENTTYPECODE = ENTTYPECODE;
    }

    @JsonIgnore
    public String getESDATE() {
        return ESDATE;
    }

    @JsonIgnore
    public void setESDATE(String ESDATE) {
        this.ESDATE = ESDATE;
    }

    @JsonIgnore
    public String getFRNAME() {
        return FRNAME;
    }

    @JsonIgnore
    public void setFRNAME(String FRNAME) {
        this.FRNAME = FRNAME;
    }

    @JsonIgnore
    public String getID() {
        return ID;
    }

    @JsonIgnore
    public void setID(String ID) {
        this.ID = ID;
    }

    @JsonIgnore
    public String getINDUSTRYPHYNAME() {
        return INDUSTRYPHYNAME;
    }

    @JsonIgnore
    public void setINDUSTRYPHYNAME(String INDUSTRYPHYNAME) {
        this.INDUSTRYPHYNAME = INDUSTRYPHYNAME;
    }

    @JsonIgnore
    public String getOPFROM() {
        return OPFROM;
    }

    @JsonIgnore
    public void setOPFROM(String OPFROM) {
        this.OPFROM = OPFROM;
    }

    @JsonIgnore
    public String getOPTO() {
        return OPTO;
    }

    @JsonIgnore
    public void setOPTO(String OPTO) {
        this.OPTO = OPTO;
    }

    @JsonIgnore
    public String getORGCODES() {
        return ORGCODES;
    }

    @JsonIgnore
    public void setORGCODES(String ORGCODES) {
        this.ORGCODES = ORGCODES;
    }

    @JsonIgnore
    public String getPOSTALCODE() {
        return POSTALCODE;
    }

    @JsonIgnore
    public void setPOSTALCODE(String POSTALCODE) {
        this.POSTALCODE = POSTALCODE;
    }

    @JsonIgnore
    public String getREGCAP() {
        return REGCAP;
    }

    @JsonIgnore
    public void setREGCAP(String REGCAP) {
        this.REGCAP = REGCAP;
    }

    @JsonIgnore
    public String getREGCAPCUR() {
        return REGCAPCUR;
    }

    @JsonIgnore
    public void setREGCAPCUR(String REGCAPCUR) {
        this.REGCAPCUR = REGCAPCUR;
    }

    @JsonIgnore
    public String getREGCAPCURCODE() {
        return REGCAPCURCODE;
    }

    @JsonIgnore
    public void setREGCAPCURCODE(String REGCAPCURCODE) {
        this.REGCAPCURCODE = REGCAPCURCODE;
    }

    @JsonIgnore
    public String getREGCITY() {
        return REGCITY;
    }

    @JsonIgnore
    public void setREGCITY(String REGCITY) {
        this.REGCITY = REGCITY;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @JsonIgnore
    public String getREGORG() {
        return REGORG;
    }

    @JsonIgnore
    public void setREGORG(String REGORG) {
        this.REGORG = REGORG;
    }

    @JsonIgnore
    public String getREGORGCITY() {
        return REGORGCITY;
    }

    @JsonIgnore
    public void setREGORGCITY(String REGORGCITY) {
        this.REGORGCITY = REGORGCITY;
    }

    @JsonIgnore
    public String getREGORGCODE() {
        return REGORGCODE;
    }

    @JsonIgnore
    public void setREGORGCODE(String REGORGCODE) {
        this.REGORGCODE = REGORGCODE;
    }

    @JsonIgnore
    public String getREGORGDISTRICT() {
        return REGORGDISTRICT;
    }

    @JsonIgnore
    public void setREGORGDISTRICT(String REGORGDISTRICT) {
        this.REGORGDISTRICT = REGORGDISTRICT;
    }

    @JsonIgnore
    public String getREGORGPROVINCE() {
        return REGORGPROVINCE;
    }

    @JsonIgnore
    public void setREGORGPROVINCE(String REGORGPROVINCE) {
        this.REGORGPROVINCE = REGORGPROVINCE;
    }

    @JsonIgnore
    public String getREVDATE() {
        return REVDATE;
    }

    @JsonIgnore
    public void setREVDATE(String REVDATE) {
        this.REVDATE = REVDATE;
    }

    @JsonIgnore
    public String getS_EXT_NODENUM() {
        return S_EXT_NODENUM;
    }

    @JsonIgnore
    public void setS_EXT_NODENUM(String s_EXT_NODENUM) {
        S_EXT_NODENUM = s_EXT_NODENUM;
    }

    @JsonIgnore
    public String getZSOPSCOPE() {
        return ZSOPSCOPE;
    }

    @JsonIgnore
    public void setZSOPSCOPE(String ZSOPSCOPE) {
        this.ZSOPSCOPE = ZSOPSCOPE;
    }

    @Override
    public String toString() {
        return "BASIC{" +
                "ABUITEM='" + ABUITEM + '\'' +
                ", ANCHEYEAR='" + ANCHEYEAR + '\'' +
                ", APPRDATE='" + APPRDATE + '\'' +
                ", CANDATE='" + CANDATE + '\'' +
                ", CREDITCODE='" + CREDITCODE + '\'' +
                ", DOM='" + DOM + '\'' +
                ", EMAIL='" + EMAIL + '\'' +
                ", ENTITYTYPE='" + ENTITYTYPE + '\'' +
                ", ENTNAME='" + ENTNAME + '\'' +
                ", ENTNAME_OLD='" + ENTNAME_OLD + '\'' +
                ", ENTSTATUS='" + ENTSTATUS + '\'' +
                ", ENTSTATUSCODE='" + ENTSTATUSCODE + '\'' +
                ", ENTTYPE='" + ENTTYPE + '\'' +
                ", ENTTYPECODE='" + ENTTYPECODE + '\'' +
                ", ESDATE='" + ESDATE + '\'' +
                ", FRNAME='" + FRNAME + '\'' +
                ", ID='" + ID + '\'' +
                ", INDUSTRYPHYNAME='" + INDUSTRYPHYNAME + '\'' +
                ", OPFROM='" + OPFROM + '\'' +
                ", OPTO='" + OPTO + '\'' +
                ", ORGCODES='" + ORGCODES + '\'' +
                ", POSTALCODE='" + POSTALCODE + '\'' +
                ", REGCAP='" + REGCAP + '\'' +
                ", REGCAPCUR='" + REGCAPCUR + '\'' +
                ", REGCAPCURCODE='" + REGCAPCURCODE + '\'' +
                ", REGCITY='" + REGCITY + '\'' +
                ", REGNO='" + REGNO + '\'' +
                ", REGORG='" + REGORG + '\'' +
                ", REGORGCITY='" + REGORGCITY + '\'' +
                ", REGORGCODE='" + REGORGCODE + '\'' +
                ", REGORGDISTRICT='" + REGORGDISTRICT + '\'' +
                ", REGORGPROVINCE='" + REGORGPROVINCE + '\'' +
                ", REVDATE='" + REVDATE + '\'' +
                ", S_EXT_NODENUM='" + S_EXT_NODENUM + '\'' +
                ", ZSOPSCOPE='" + ZSOPSCOPE + '\'' +
                '}';
    }
}
