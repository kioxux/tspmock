package cn.com.yusys.yusp.dto.server.biz.xdxw0065.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 响应Dto：调查报告审批结果信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//主键
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//调查报告主表主键
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户代码
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cont_type")
    private String cont_type;//合同类型
    @JsonProperty(value = "guar_ways")
    private String guar_ways;//担保方式
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//合同金额
    @JsonProperty(value = "term_time_type")
    private String term_time_type;//期限类型
    @JsonProperty(value = "apply_term")
    private String apply_term;//申请期限
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//合同起始日
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//合同到期日
    @JsonProperty(value = "reality_ir_y")
    private BigDecimal reality_ir_y;//利率
    @JsonProperty(value = "repay_type")
    private String repay_type;//还款方式
    @JsonProperty(value = "limit_type")
    private String limit_type;//额度类型
    @JsonProperty(value = "insert_time")
    private String insert_time;//插入时间
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "cur_use_amt")
    private BigDecimal cur_use_amt;//本次用信金额
    @JsonProperty(value = "is_trustee_pay")
    private String is_trustee_pay;//是否受托支付
    @JsonProperty(value = "is_credit_condition")
    private String is_credit_condition;//是否有用信条件

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getGuar_ways() {
        return guar_ways;
    }

    public void setGuar_ways(String guar_ways) {
        this.guar_ways = guar_ways;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getTerm_time_type() {
        return term_time_type;
    }

    public void setTerm_time_type(String term_time_type) {
        this.term_time_type = term_time_type;
    }

    public String getApply_term() {
        return apply_term;
    }

    public void setApply_term(String apply_term) {
        this.apply_term = apply_term;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public BigDecimal getReality_ir_y() {
        return reality_ir_y;
    }

    public void setReality_ir_y(BigDecimal reality_ir_y) {
        this.reality_ir_y = reality_ir_y;
    }

    public String getRepay_type() {
        return repay_type;
    }

    public void setRepay_type(String repay_type) {
        this.repay_type = repay_type;
    }

    public String getLimit_type() {
        return limit_type;
    }

    public void setLimit_type(String limit_type) {
        this.limit_type = limit_type;
    }

    public String getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(String insert_time) {
        this.insert_time = insert_time;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public BigDecimal getCur_use_amt() {
        return cur_use_amt;
    }

    public void setCur_use_amt(BigDecimal cur_use_amt) {
        this.cur_use_amt = cur_use_amt;
    }

    public String getIs_trustee_pay() {
        return is_trustee_pay;
    }

    public void setIs_trustee_pay(String is_trustee_pay) {
        this.is_trustee_pay = is_trustee_pay;
    }

    public String getIs_credit_condition() {
        return is_credit_condition;
    }

    public void setIs_credit_condition(String is_credit_condition) {
        this.is_credit_condition = is_credit_condition;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                ", survey_serno='" + survey_serno + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cont_type='" + cont_type + '\'' +
                ", guar_ways='" + guar_ways + '\'' +
                ", apply_amount=" + apply_amount +
                ", term_time_type='" + term_time_type + '\'' +
                ", apply_term='" + apply_term + '\'' +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", reality_ir_y=" + reality_ir_y +
                ", repay_type='" + repay_type + '\'' +
                ", limit_type='" + limit_type + '\'' +
                ", insert_time='" + insert_time + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cur_use_amt=" + cur_use_amt +
                ", is_trustee_pay='" + is_trustee_pay + '\'' +
                ", is_credit_condition='" + is_credit_condition + '\'' +
                '}';
    }
}
