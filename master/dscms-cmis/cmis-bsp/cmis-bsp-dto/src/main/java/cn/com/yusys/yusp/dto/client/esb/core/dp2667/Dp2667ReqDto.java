package cn.com.yusys.yusp.dto.client.esb.core.dp2667;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：组合账户特殊账户查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2667ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "tszhzhlx")
    private String tszhzhlx;//特殊组合账户类型
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//货币代号


    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getTszhzhlx() {
        return tszhzhlx;
    }

    public void setTszhzhlx(String tszhzhlx) {
        this.tszhzhlx = tszhzhlx;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    @Override
    public String toString() {
        return "Dp2667ReqDto{" +
                "kehuzhao='" + kehuzhao + '\'' +
                "tszhzhlx='" + tszhzhlx + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                '}';
    }
}  
