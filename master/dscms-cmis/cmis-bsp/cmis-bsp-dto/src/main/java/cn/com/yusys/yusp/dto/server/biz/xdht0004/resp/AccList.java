package cn.com.yusys.yusp.dto.server.biz.xdht0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：保证金母户
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class AccList implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 结算账户账号 **/
    @JsonProperty(value = "settlAcctNum")
    private String settlAcctNum;

    /** 账户名称 **/
    @JsonProperty(value = "acctName")
    private String acctName;

    /** 账户子序号 **/
    @JsonProperty(value = "acctSubNum")
    private String acctSubNum;

    /** 币种 **/
    @JsonProperty(value = "curType")
    private String curType;

    /** 开户机构 **/
    @JsonProperty(value = "openOrgId")
    private String openOrgId;

    public String getSettlAcctNum() {
        return settlAcctNum;
    }

    public void setSettlAcctNum(String settlAcctNum) {
        this.settlAcctNum = settlAcctNum;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getAcctSubNum() {
        return acctSubNum;
    }

    public void setAcctSubNum(String acctSubNum) {
        this.acctSubNum = acctSubNum;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getOpenOrgId() {
        return openOrgId;
    }

    public void setOpenOrgId(String openOrgId) {
        this.openOrgId = openOrgId;
    }

    @Override
    public String toString() {
        return "AccList{" +
                "settlAcctNum='" + settlAcctNum + '\'' +
                ", acctName='" + acctName + '\'' +
                ", acctSubNum='" + acctSubNum + '\'' +
                ", curType='" + curType + '\'' +
                ", openOrgId='" + openOrgId + '\'' +
                '}';
    }
}
