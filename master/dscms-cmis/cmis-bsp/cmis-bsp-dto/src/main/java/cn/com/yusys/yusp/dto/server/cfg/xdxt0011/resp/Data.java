package cn.com.yusys.yusp.dto.server.cfg.xdxt0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/25 9:35:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/25 9:35
 * @since 2021/5/25 9:35
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称
    @JsonProperty(value = "telnum")
    private String telnum;//客户经理联系方式
    @JsonProperty(value = "orgName")
    private String orgName;//客户经理所在机构名称
    @JsonProperty(value = "status")
    private String status;//客户经理状态
    @JsonProperty(value = "roleNo")
    private String roleNo;//客户经理角色码
    @JsonProperty(value = "roleName")
    private String roleName;//客户经理角色名称
    @JsonProperty(value = "orgid")
    private String orgid;//组织号
    @JsonProperty(value = "xfOrgid")
    private String xfOrgid;//小贷机构
    @JsonProperty(value = "xfOrgName")
    private String xfOrgName;//小贷机构名称
    @JsonProperty(value = "surveyModel")
    private String surveyModel;//调查模式
    @JsonProperty(value = "sgOrgid")
    private String sgOrgid;//寿光小贷机构号
    @JsonProperty(value = "loanBalance")
    private String loanBalance;//贷款余额

    public String getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(String loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getTelnum() {
        return telnum;
    }

    public void setTelnum(String telnum) {
        this.telnum = telnum;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRoleNo() {
        return roleNo;
    }

    public void setRoleNo(String roleNo) {
        this.roleNo = roleNo;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getXfOrgid() {
        return xfOrgid;
    }

    public void setXfOrgid(String xfOrgid) {
        this.xfOrgid = xfOrgid;
    }

    public String getXfOrgName() {
        return xfOrgName;
    }

    public void setXfOrgName(String xfOrgName) {
        this.xfOrgName = xfOrgName;
    }

    public String getSurveyModel() {
        return surveyModel;
    }

    public void setSurveyModel(String surveyModel) {
        this.surveyModel = surveyModel;
    }

    public String getSgOrgid() {
        return sgOrgid;
    }

    public void setSgOrgid(String sgOrgid) {
        this.sgOrgid = sgOrgid;
    }

    @Override
    public String toString() {
        return "Data{" +
                "managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", telnum='" + telnum + '\'' +
                ", orgName='" + orgName + '\'' +
                ", status='" + status + '\'' +
                ", roleNo='" + roleNo + '\'' +
                ", roleName='" + roleName + '\'' +
                ", orgid='" + orgid + '\'' +
                ", xfOrgid='" + xfOrgid + '\'' +
                ", xfOrgName='" + xfOrgName + '\'' +
                ", surveyModel='" + surveyModel + '\'' +
                ", sgOrgid='" + sgOrgid + '\'' +
                ", loanBalance='" + loanBalance + '\'' +
                '}';
    }
}
