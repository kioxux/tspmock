package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd15;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd15ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contract_no")
    private String contract_no;//利翃平台贷款合同号

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Fbxd15ReqDto{" +
                "contract_no='" + contract_no + '\'' +
                '}';
    }
}  
