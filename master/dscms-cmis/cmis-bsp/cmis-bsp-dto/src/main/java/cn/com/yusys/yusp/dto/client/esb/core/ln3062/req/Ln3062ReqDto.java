package cn.com.yusys.yusp.dto.client.esb.core.ln3062.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：资产转让借据维护
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3062ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "caozfshi")
    private String caozfshi;//操作方式
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "zcrfshii")
    private String zcrfshii;//资产融通方式
    @JsonProperty(value = "zcrtbili")
    private BigDecimal zcrtbili;//资产融通比例
    @JsonProperty(value = "xieyilil")
    private BigDecimal xieyilil;//协议利率
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注
    @JsonProperty(value = "lstKlnb_dkzrjj")
    private java.util.List<LstKlnb_dkzrjj> lstKlnb_dkzrjj;

    public String getCaozfshi() {
        return caozfshi;
    }

    public void setCaozfshi(String caozfshi) {
        this.caozfshi = caozfshi;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getZcrfshii() {
        return zcrfshii;
    }

    public void setZcrfshii(String zcrfshii) {
        this.zcrfshii = zcrfshii;
    }

    public BigDecimal getZcrtbili() {
        return zcrtbili;
    }

    public void setZcrtbili(BigDecimal zcrtbili) {
        this.zcrtbili = zcrtbili;
    }

    public BigDecimal getXieyilil() {
        return xieyilil;
    }

    public void setXieyilil(BigDecimal xieyilil) {
        this.xieyilil = xieyilil;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public List<LstKlnb_dkzrjj> getLstKlnb_dkzrjj() {
        return lstKlnb_dkzrjj;
    }

    public void setLstKlnb_dkzrjj(List<LstKlnb_dkzrjj> lstKlnb_dkzrjj) {
        this.lstKlnb_dkzrjj = lstKlnb_dkzrjj;
    }

    @Override
    public String toString() {
        return "Ln3062ReqDto{" +
                "caozfshi='" + caozfshi + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", zcrfshii='" + zcrfshii + '\'' +
                ", zcrtbili=" + zcrtbili +
                ", xieyilil=" + xieyilil +
                ", beizhuxx='" + beizhuxx + '\'' +
                ", lstKlnb_dkzrjj=" + lstKlnb_dkzrjj +
                '}';
    }
}
