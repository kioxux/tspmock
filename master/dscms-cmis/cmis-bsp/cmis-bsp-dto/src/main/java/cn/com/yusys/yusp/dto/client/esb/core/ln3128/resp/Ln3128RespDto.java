package cn.com.yusys.yusp.dto.client.esb.core.ln3128.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款机构变更查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3128RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lstdkjgzy")
    private List<Lstdkjgzy> lstdkjgzy;//贷款账户机构转移信息

	public List<Lstdkjgzy> getLstdkjgzy() {
		return lstdkjgzy;
	}

	public void setLstdkjgzy(List<Lstdkjgzy> lstdkjgzy) {
		this.lstdkjgzy = lstdkjgzy;
	}

	@Override
	public String toString() {
		return "Ln3128RespDto{" +
				"lstdkjgzy=" + lstdkjgzy +
				'}';
	}
}
