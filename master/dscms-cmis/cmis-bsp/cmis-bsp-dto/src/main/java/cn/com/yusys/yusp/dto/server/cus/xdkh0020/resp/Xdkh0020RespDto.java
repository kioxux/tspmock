package cn.com.yusys.yusp.dto.server.cus.xdkh0020.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：小微平台请求信贷线上开户
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0020RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdkh0020RespDto{" +
				"data=" + data +
				'}';
	}
}
