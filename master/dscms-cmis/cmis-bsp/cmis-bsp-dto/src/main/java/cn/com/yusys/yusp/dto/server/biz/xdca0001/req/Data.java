package cn.com.yusys.yusp.dto.server.biz.xdca0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：信用卡调额申请
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "adjustmentChnl")
    private String adjustmentChnl;//提额渠道
    @JsonProperty(value = "cardNo")
    private String cardNo;//卡号
    @JsonProperty(value = "cardPrd")
    private String cardPrd;//卡产品
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "holdCardName")
    private String holdCardName;//持卡人姓名
    @JsonProperty(value = "cdtAmt")
    private BigDecimal cdtAmt;//信用额度
    @JsonProperty(value = "newAmt")
    private BigDecimal newAmt;//新额度
    @JsonProperty(value = "isProvidCdtProve")
    private String isProvidCdtProve;//是否提供增信证明
    @JsonProperty(value = "memo")
    private String memo;//备注

    public String getAdjustmentChnl() {
        return adjustmentChnl;
    }

    public void setAdjustmentChnl(String adjustmentChnl) {
        this.adjustmentChnl = adjustmentChnl;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardPrd() {
        return cardPrd;
    }

    public void setCardPrd(String cardPrd) {
        this.cardPrd = cardPrd;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getHoldCardName() {
        return holdCardName;
    }

    public void setHoldCardName(String holdCardName) {
        this.holdCardName = holdCardName;
    }

    public BigDecimal getCdtAmt() {
        return cdtAmt;
    }

    public void setCdtAmt(BigDecimal cdtAmt) {
        this.cdtAmt = cdtAmt;
    }

    public BigDecimal getNewAmt() {
        return newAmt;
    }

    public void setNewAmt(BigDecimal newAmt) {
        this.newAmt = newAmt;
    }

    public String getIsProvidCdtProve() {
        return isProvidCdtProve;
    }

    public void setIsProvidCdtProve(String isProvidCdtProve) {
        this.isProvidCdtProve = isProvidCdtProve;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "adjustmentChnl='" + adjustmentChnl + '\'' +
                "cardNo='" + cardNo + '\'' +
                "cardPrd='" + cardPrd + '\'' +
                "certType='" + certType + '\'' +
                "certNo='" + certNo + '\'' +
                "holdCardName='" + holdCardName + '\'' +
                "cdtAmt='" + cdtAmt + '\'' +
                "newAmt='" + newAmt + '\'' +
                "isProvidCdtProve='" + isProvidCdtProve + '\'' +
                "memo='" + memo + '\'' +
                '}';
    }
}
