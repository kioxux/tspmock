package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：获取还款记录的借据一览信息，包括借据号、借据金额、借据余额
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd11RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11.List> list;//start

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Fbxd11RespDto{" +
                "list=" + list +
                '}';
    }
}
