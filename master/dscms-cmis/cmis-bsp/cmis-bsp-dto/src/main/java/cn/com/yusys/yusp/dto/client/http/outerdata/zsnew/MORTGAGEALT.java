package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 动产抵押-变更信息
 */
@JsonPropertyOrder(alphabetic = true)
public class MORTGAGEALT implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "MAB_ALT_DATE")
    private String MAB_ALT_DATE;//	变更日期
    @JsonProperty(value = "MAB_ALT_DETAILS")
    private String MAB_ALT_DETAILS;//	变更内容
    @JsonProperty(value = "MAB_REGNO")
    private String MAB_REGNO;//	登记编号

    @JsonIgnore
    public String getMAB_ALT_DATE() {
        return MAB_ALT_DATE;
    }

    @JsonIgnore
    public void setMAB_ALT_DATE(String MAB_ALT_DATE) {
        this.MAB_ALT_DATE = MAB_ALT_DATE;
    }

    @JsonIgnore
    public String getMAB_ALT_DETAILS() {
        return MAB_ALT_DETAILS;
    }

    @JsonIgnore
    public void setMAB_ALT_DETAILS(String MAB_ALT_DETAILS) {
        this.MAB_ALT_DETAILS = MAB_ALT_DETAILS;
    }

    @JsonIgnore
    public String getMAB_REGNO() {
        return MAB_REGNO;
    }

    @JsonIgnore
    public void setMAB_REGNO(String MAB_REGNO) {
        this.MAB_REGNO = MAB_REGNO;
    }

    @Override
    public String toString() {
        return "MORTGAGEALT{" +
                "MAB_ALT_DATE='" + MAB_ALT_DATE + '\'' +
                ", MAB_ALT_DETAILS='" + MAB_ALT_DETAILS + '\'' +
                ", MAB_REGNO='" + MAB_REGNO + '\'' +
                '}';
    }
}
