package cn.com.yusys.yusp.dto.client.esb.core.dp2280;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：子账户序号查询接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/16上午9:54:28
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2280ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao; // 客户账号
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh; // 子账户序号
    @JsonProperty(value = "yxzhuhbz")
    private String yxzhuhbz; // 允许账户序号为空标志
    @JsonProperty(value = "sfcxxhbz")
    private String sfcxxhbz; // 是否查询虚账户标志
    @JsonProperty(value = "qishibis")
    private BigDecimal qishibis; // 起始笔数
    @JsonProperty(value = "chxunbis")
    private BigDecimal chxunbis; // 查询笔数
    @JsonProperty(value = "sfcxxhzh")
    private String sfcxxhzh; // 是否查询销户账户标志
    @JsonProperty(value = "sfcxglzh")
    private String sfcxglzh; // 是否查询关联账户

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getYxzhuhbz() {
        return yxzhuhbz;
    }

    public void setYxzhuhbz(String yxzhuhbz) {
        this.yxzhuhbz = yxzhuhbz;
    }

    public String getSfcxxhbz() {
        return sfcxxhbz;
    }

    public void setSfcxxhbz(String sfcxxhbz) {
        this.sfcxxhbz = sfcxxhbz;
    }

    public BigDecimal getQishibis() {
        return qishibis;
    }

    public void setQishibis(BigDecimal qishibis) {
        this.qishibis = qishibis;
    }

    public BigDecimal getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(BigDecimal chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getSfcxxhzh() {
        return sfcxxhzh;
    }

    public void setSfcxxhzh(String sfcxxhzh) {
        this.sfcxxhzh = sfcxxhzh;
    }

    public String getSfcxglzh() {
        return sfcxglzh;
    }

    public void setSfcxglzh(String sfcxglzh) {
        this.sfcxglzh = sfcxglzh;
    }

    @Override
    public String toString() {
        return "Dp2280ReqDto{" +
                "kehuzhao='" + kehuzhao + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", yxzhuhbz='" + yxzhuhbz + '\'' +
                ", sfcxxhbz='" + sfcxxhbz + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", sfcxxhzh='" + sfcxxhzh + '\'' +
                ", sfcxglzh='" + sfcxglzh + '\'' +
                '}';
    }
}