package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 交易对手信息( DealCustomerInfo)
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
@JsonPropertyOrder(alphabetic = true)
public class DealCustomerInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bill_no")
    private String bill_no; // 借据编号
    @JsonProperty(value = "accept_org")
    private String accept_org; // 承兑行名称
    @JsonProperty(value = "org_type")
    private String org_type; // 承兑行同业机构（行）类型
    @JsonProperty(value = "cus_id")
    private String cus_id; // 客户号
    @JsonProperty(value = "cus_name")
    private String cus_name; // 客户名称
    @JsonProperty(value = "cus_type")
    private String cus_type; // 客户类型
    @JsonProperty(value = "post_no")
    private String post_no; // 汇票编号
    @JsonProperty(value = "cur_type")
    private String cur_type; // 汇票币种
    @JsonProperty(value = "post_balance")
    private BigDecimal post_balance; // 汇票金额
    @JsonProperty(value = "bus_owner")
    private String bus_owner; // 企业所有制
    @JsonProperty(value = "reg_area_code")
    private String reg_area_code; // 注册地行政区划代码
    @JsonProperty(value = "reg_area_name")
    private String reg_area_name; // 注册地行政区划名称
    @JsonProperty(value = "assets")
    private BigDecimal assets; // 资产负债率
    @JsonProperty(value = "bas_acc_flg")
    private String bas_acc_flg; // 基本户是否在本行
    @JsonProperty(value = "inrate_grade")
    private String inrate_grade; // 内部评级等级
    @JsonProperty(value = "outrate_org")
    private String outrate_org; // 外部评级机构
    @JsonProperty(value = "outrate_grade")
    private String outrate_grade; // 外部评级等级
    @JsonProperty(value = "pledge_relevance")
    private String pledge_relevance; // 借款人与承兑企业关系

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getAccept_org() {
        return accept_org;
    }

    public void setAccept_org(String accept_org) {
        this.accept_org = accept_org;
    }

    public String getOrg_type() {
        return org_type;
    }

    public void setOrg_type(String org_type) {
        this.org_type = org_type;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }

    public String getPost_no() {
        return post_no;
    }

    public void setPost_no(String post_no) {
        this.post_no = post_no;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getPost_balance() {
        return post_balance;
    }

    public void setPost_balance(BigDecimal post_balance) {
        this.post_balance = post_balance;
    }

    public String getBus_owner() {
        return bus_owner;
    }

    public void setBus_owner(String bus_owner) {
        this.bus_owner = bus_owner;
    }

    public String getReg_area_code() {
        return reg_area_code;
    }

    public void setReg_area_code(String reg_area_code) {
        this.reg_area_code = reg_area_code;
    }

    public String getReg_area_name() {
        return reg_area_name;
    }

    public void setReg_area_name(String reg_area_name) {
        this.reg_area_name = reg_area_name;
    }

    public BigDecimal getAssets() {
        return assets;
    }

    public void setAssets(BigDecimal assets) {
        this.assets = assets;
    }

    public String getBas_acc_flg() {
        return bas_acc_flg;
    }

    public void setBas_acc_flg(String bas_acc_flg) {
        this.bas_acc_flg = bas_acc_flg;
    }

    public String getInrate_grade() {
        return inrate_grade;
    }

    public void setInrate_grade(String inrate_grade) {
        this.inrate_grade = inrate_grade;
    }

    public String getOutrate_org() {
        return outrate_org;
    }

    public void setOutrate_org(String outrate_org) {
        this.outrate_org = outrate_org;
    }

    public String getOutrate_grade() {
        return outrate_grade;
    }

    public void setOutrate_grade(String outrate_grade) {
        this.outrate_grade = outrate_grade;
    }

    public String getPledge_relevance() {
        return pledge_relevance;
    }

    public void setPledge_relevance(String pledge_relevance) {
        this.pledge_relevance = pledge_relevance;
    }

    @Override
    public String toString() {
        return "DealCustomerInfo{" +
                "bill_no='" + bill_no + '\'' +
                ", accept_org='" + accept_org + '\'' +
                ", org_type='" + org_type + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cus_type='" + cus_type + '\'' +
                ", post_no='" + post_no + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", post_balance=" + post_balance +
                ", bus_owner='" + bus_owner + '\'' +
                ", reg_area_code='" + reg_area_code + '\'' +
                ", reg_area_name='" + reg_area_name + '\'' +
                ", assets=" + assets +
                ", bas_acc_flg='" + bas_acc_flg + '\'' +
                ", inrate_grade='" + inrate_grade + '\'' +
                ", outrate_org='" + outrate_org + '\'' +
                ", outrate_grade='" + outrate_grade + '\'' +
                ", pledge_relevance='" + pledge_relevance + '\'' +
                '}';
    }
}
