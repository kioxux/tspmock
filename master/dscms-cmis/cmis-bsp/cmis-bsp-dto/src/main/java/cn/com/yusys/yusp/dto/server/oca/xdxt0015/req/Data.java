package cn.com.yusys.yusp.dto.server.oca.xdxt0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 20:11:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 20:11
 * @since 2021/5/24 20:11
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "actorNo")
    private String actorNo;//客户经理号
    @JsonProperty(value = "actorName")
    private String actorName;//客户经理名
    @JsonProperty(value = "orgId")
    private String orgId;//机构号

    public String getActorNo() {
        return actorNo;
    }

    public void setActorNo(String actorNo) {
        this.actorNo = actorNo;
    }

    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "actorNo='" + actorNo + '\'' +
                ", actorName='" + actorName + '\'' +
                ", orgId='" + orgId + '\'' +
                '}';
    }
}
