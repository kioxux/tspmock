package cn.com.yusys.yusp.dto.server.cus.xdkh0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询客户是否本行员工及直属亲属
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isBankStaff")
    private String isBankStaff;//是否本行员工
    @JsonProperty(value = "isBankStaffFamily")
    private String isBankStaffFamily;//是否本行员工直属亲属（父母、子女、配偶）

    public String getIsBankStaff() {
        return isBankStaff;
    }

    public void setIsBankStaff(String isBankStaff) {
        this.isBankStaff = isBankStaff;
    }

    public String getIsBankStaffFamily() {
        return isBankStaffFamily;
    }

    public void setIsBankStaffFamily(String isBankStaffFamily) {
        this.isBankStaffFamily = isBankStaffFamily;
    }

    @Override
    public String toString() {
        return "Data{" +
                "isBankStaff='" + isBankStaff + '\'' +
                ", isBankStaffFamily='" + isBankStaffFamily + '\'' +
                '}';
    }
}
