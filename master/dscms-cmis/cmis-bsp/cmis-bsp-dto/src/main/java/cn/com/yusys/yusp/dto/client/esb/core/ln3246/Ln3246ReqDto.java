package cn.com.yusys.yusp.dto.client.esb.core.ln3246;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 贷款还款计划明细查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3246ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh; //贷款借据号 BaseType.U_JIEJUHAO (60)
    @JsonProperty(value = "qishiqsh")
    private Integer qishiqsh; // 起始期数 BaseType.U_CHANZXLX
    @JsonProperty(value = "zhzhiqsh")
    private Integer zhzhiqsh; // 终止期数 BaseType.U_CHANZXLX
    @JsonProperty(value = "qishriqi")
    private String qishriqi; // 起始日期 BaseType.U_BZRIQILX
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi; // 终止日期 BaseType.U_BZRIQILX
    @JsonProperty(value = "qishibis")
    private Integer qishibis; // 起始笔数 BaseType.U_CHANZXLX
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis; // 查询笔数 BaseType.U_CHANZXLX

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getQishiqsh() {
        return qishiqsh;
    }

    public void setQishiqsh(Integer qishiqsh) {
        this.qishiqsh = qishiqsh;
    }

    public Integer getZhzhiqsh() {
        return zhzhiqsh;
    }

    public void setZhzhiqsh(Integer zhzhiqsh) {
        this.zhzhiqsh = zhzhiqsh;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Ln3246ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", qishiqsh=" + qishiqsh +
                ", zhzhiqsh=" + zhzhiqsh +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                '}';
    }
}
