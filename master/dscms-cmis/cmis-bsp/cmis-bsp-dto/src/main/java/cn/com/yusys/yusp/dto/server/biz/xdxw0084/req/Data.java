package cn.com.yusys.yusp.dto.server.biz.xdxw0084.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：共借人送达地址确认书文本生成PDF接口
 *
 * @author
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusName")
    private String cusName;//借款人姓名
    @JsonProperty(value = "signDate")
    private String signDate;//签订日期
    @JsonProperty(value = "address")
    private String address;//签订地址
    @JsonProperty(value = "phone")
    private String phone;//电话号码

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusName='" + cusName + '\'' +
                "signDate='" + signDate + '\'' +
                "address='" + address + '\'' +
                "phone='" + phone + '\'' +
                '}';
    }
}
