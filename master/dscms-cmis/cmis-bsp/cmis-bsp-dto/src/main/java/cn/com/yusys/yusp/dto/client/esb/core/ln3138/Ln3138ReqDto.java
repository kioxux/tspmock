package cn.com.yusys.yusp.dto.client.esb.core.ln3138;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：等额等本贷款推算
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3138ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikjine")
    private BigDecimal daikjine;//贷款金额
    @JsonProperty(value = "zongqish")
    private Integer zongqish;//总期数
    @JsonProperty(value = "sjnylilv")
    private String sjnylilv;//实际年月利率
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额

    public BigDecimal getDaikjine() {
        return daikjine;
    }

    public void setDaikjine(BigDecimal daikjine) {
        this.daikjine = daikjine;
    }

    public Integer getZongqish() {
        return zongqish;
    }

    public void setZongqish(Integer zongqish) {
        this.zongqish = zongqish;
    }

    public String getSjnylilv() {
        return sjnylilv;
    }

    public void setSjnylilv(String sjnylilv) {
        this.sjnylilv = sjnylilv;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    @Override
    public String toString() {
        return "Ln3138ReqDto{" +
                "daikjine='" + daikjine + '\'' +
                "zongqish='" + zongqish + '\'' +
                "sjnylilv='" + sjnylilv + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "meiqhbje='" + meiqhbje + '\'' +
                '}';
    }
}  
