package cn.com.yusys.yusp.dto.server.biz.xdzc0004.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：客户资产清单查询接口
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0004RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;//data

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdzc0004RespDto{" +
                "data=" + data +
                '}';
    }
}
