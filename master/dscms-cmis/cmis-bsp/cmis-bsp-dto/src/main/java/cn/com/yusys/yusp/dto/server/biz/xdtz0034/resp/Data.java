package cn.com.yusys.yusp.dto.server.biz.xdtz0034.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/21 16:07:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 16:07
 * @since 2021/5/21 16:07
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHavingCdtRecord")
    private String isHavingCdtRecord;//是否有行内信用记录

    public String getIsHavingCdtRecord() {
        return isHavingCdtRecord;
    }

    public void setIsHavingCdtRecord(String isHavingCdtRecord) {
        this.isHavingCdtRecord = isHavingCdtRecord;
    }

    @Override
    public String toString() {
        return "Xdtz0034RespDto{" +
                "isHavingCdtRecord='" + isHavingCdtRecord + '\'' +
                '}';
    }


}
