package cn.com.yusys.yusp.dto.client.esb.core.ln3055.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：贷款利率调整
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3055ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限(月)
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "zclilvbh")
    private String zclilvbh;//正常利率编号
    @JsonProperty(value = "llqxkdfs")
    private String llqxkdfs;//利率期限靠档方式
    @JsonProperty(value = "lilvqixx")
    private String lilvqixx;//利率期限
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "lilvfend")
    private String lilvfend;//利率分段
    @JsonProperty(value = "yuqillbh")
    private String yuqillbh;//逾期利率编号
    @JsonProperty(value = "yuqinyll")
    private String yuqinyll;//逾期年月利率
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "yuqitzfs")
    private String yuqitzfs;//逾期利率调整方式
    @JsonProperty(value = "yuqitzzq")
    private String yuqitzzq;//逾期利率调整周期
    @JsonProperty(value = "yqfxfdfs")
    private String yqfxfdfs;//逾期罚息浮动方式
    @JsonProperty(value = "yqfxfdzh")
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    @JsonProperty(value = "fulilvbh")
    private String fulilvbh;//复利利率编号
    @JsonProperty(value = "fulilvny")
    private String fulilvny;//复利利率年月标识
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "fulitzfs")
    private String fulitzfs;//复利利率调整方式
    @JsonProperty(value = "fulitzzq")
    private String fulitzzq;//复利利率调整周期
    @JsonProperty(value = "fulifdfs")
    private String fulifdfs;//复利利率浮动方式
    @JsonProperty(value = "fulifdzh")
    private BigDecimal fulifdzh;//复利利率浮动值
    @JsonProperty(value = "flllcklx")
    private String flllcklx;//复利利率参考类型
	@JsonProperty(value = "lstdkllfds")
	private java.util.List<Lstdkllfd> lstdkllfds;

	public String getDkjiejuh() {
		return dkjiejuh;
	}

	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}

	public String getDkzhangh() {
		return dkzhangh;
	}

	public void setDkzhangh(String dkzhangh) {
		this.dkzhangh = dkzhangh;
	}

	public String getKehuhaoo() {
		return kehuhaoo;
	}

	public void setKehuhaoo(String kehuhaoo) {
		this.kehuhaoo = kehuhaoo;
	}

	public String getKehmingc() {
		return kehmingc;
	}

	public void setKehmingc(String kehmingc) {
		this.kehmingc = kehmingc;
	}

	public String getQixiriqi() {
		return qixiriqi;
	}

	public void setQixiriqi(String qixiriqi) {
		this.qixiriqi = qixiriqi;
	}

	public String getDaoqriqi() {
		return daoqriqi;
	}

	public void setDaoqriqi(String daoqriqi) {
		this.daoqriqi = daoqriqi;
	}

	public String getDkqixian() {
		return dkqixian;
	}

	public void setDkqixian(String dkqixian) {
		this.dkqixian = dkqixian;
	}

	public String getHuobdhao() {
		return huobdhao;
	}

	public void setHuobdhao(String huobdhao) {
		this.huobdhao = huobdhao;
	}

	public BigDecimal getHetongje() {
		return hetongje;
	}

	public void setHetongje(BigDecimal hetongje) {
		this.hetongje = hetongje;
	}

	public BigDecimal getJiejuuje() {
		return jiejuuje;
	}

	public void setJiejuuje(BigDecimal jiejuuje) {
		this.jiejuuje = jiejuuje;
	}

	public String getLilvleix() {
		return lilvleix;
	}

	public void setLilvleix(String lilvleix) {
		this.lilvleix = lilvleix;
	}

	public String getNyuelilv() {
		return nyuelilv;
	}

	public void setNyuelilv(String nyuelilv) {
		this.nyuelilv = nyuelilv;
	}

	public BigDecimal getZhchlilv() {
		return zhchlilv;
	}

	public void setZhchlilv(BigDecimal zhchlilv) {
		this.zhchlilv = zhchlilv;
	}

	public String getLilvtzfs() {
		return lilvtzfs;
	}

	public void setLilvtzfs(String lilvtzfs) {
		this.lilvtzfs = lilvtzfs;
	}

	public String getLilvtzzq() {
		return lilvtzzq;
	}

	public void setLilvtzzq(String lilvtzzq) {
		this.lilvtzzq = lilvtzzq;
	}

	public String getLilvfdfs() {
		return lilvfdfs;
	}

	public void setLilvfdfs(String lilvfdfs) {
		this.lilvfdfs = lilvfdfs;
	}

	public BigDecimal getLilvfdzh() {
		return lilvfdzh;
	}

	public void setLilvfdzh(BigDecimal lilvfdzh) {
		this.lilvfdzh = lilvfdzh;
	}

	public String getZclilvbh() {
		return zclilvbh;
	}

	public void setZclilvbh(String zclilvbh) {
		this.zclilvbh = zclilvbh;
	}

	public String getLlqxkdfs() {
		return llqxkdfs;
	}

	public void setLlqxkdfs(String llqxkdfs) {
		this.llqxkdfs = llqxkdfs;
	}

	public String getLilvqixx() {
		return lilvqixx;
	}

	public void setLilvqixx(String lilvqixx) {
		this.lilvqixx = lilvqixx;
	}

	public String getYqllcklx() {
		return yqllcklx;
	}

	public void setYqllcklx(String yqllcklx) {
		this.yqllcklx = yqllcklx;
	}

	public String getLilvfend() {
		return lilvfend;
	}

	public void setLilvfend(String lilvfend) {
		this.lilvfend = lilvfend;
	}

	public String getYuqillbh() {
		return yuqillbh;
	}

	public void setYuqillbh(String yuqillbh) {
		this.yuqillbh = yuqillbh;
	}

	public String getYuqinyll() {
		return yuqinyll;
	}

	public void setYuqinyll(String yuqinyll) {
		this.yuqinyll = yuqinyll;
	}

	public BigDecimal getYuqililv() {
		return yuqililv;
	}

	public void setYuqililv(BigDecimal yuqililv) {
		this.yuqililv = yuqililv;
	}

	public String getYuqitzfs() {
		return yuqitzfs;
	}

	public void setYuqitzfs(String yuqitzfs) {
		this.yuqitzfs = yuqitzfs;
	}

	public String getYuqitzzq() {
		return yuqitzzq;
	}

	public void setYuqitzzq(String yuqitzzq) {
		this.yuqitzzq = yuqitzzq;
	}

	public String getYqfxfdfs() {
		return yqfxfdfs;
	}

	public void setYqfxfdfs(String yqfxfdfs) {
		this.yqfxfdfs = yqfxfdfs;
	}

	public BigDecimal getYqfxfdzh() {
		return yqfxfdzh;
	}

	public void setYqfxfdzh(BigDecimal yqfxfdzh) {
		this.yqfxfdzh = yqfxfdzh;
	}

	public String getFulilvbh() {
		return fulilvbh;
	}

	public void setFulilvbh(String fulilvbh) {
		this.fulilvbh = fulilvbh;
	}

	public String getFulilvny() {
		return fulilvny;
	}

	public void setFulilvny(String fulilvny) {
		this.fulilvny = fulilvny;
	}

	public BigDecimal getFulililv() {
		return fulililv;
	}

	public void setFulililv(BigDecimal fulililv) {
		this.fulililv = fulililv;
	}

	public String getFulitzfs() {
		return fulitzfs;
	}

	public void setFulitzfs(String fulitzfs) {
		this.fulitzfs = fulitzfs;
	}

	public String getFulitzzq() {
		return fulitzzq;
	}

	public void setFulitzzq(String fulitzzq) {
		this.fulitzzq = fulitzzq;
	}

	public String getFulifdfs() {
		return fulifdfs;
	}

	public void setFulifdfs(String fulifdfs) {
		this.fulifdfs = fulifdfs;
	}

	public BigDecimal getFulifdzh() {
		return fulifdzh;
	}

	public void setFulifdzh(BigDecimal fulifdzh) {
		this.fulifdzh = fulifdzh;
	}

	public String getFlllcklx() {
		return flllcklx;
	}

	public void setFlllcklx(String flllcklx) {
		this.flllcklx = flllcklx;
	}

	public List<Lstdkllfd> getLstdkllfds() {
		return lstdkllfds;
	}

	public void setLstdkllfds(List<Lstdkllfd> lstdkllfds) {
		this.lstdkllfds = lstdkllfds;
	}
}
