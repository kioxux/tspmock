package cn.com.yusys.yusp.dto.server.biz.xddb0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 20:56
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarid")
    private String guarid;//押品编号
    @JsonProperty(value = "UpdateCollateralElectronicCertificateForm")
    private UpdateCollateralElectronicCertificateForm UpdateCollateralElectronicCertificateForm;//证件类型

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public cn.com.yusys.yusp.dto.server.biz.xddb0013.req.UpdateCollateralElectronicCertificateForm getUpdateCollateralElectronicCertificateForm() {
        return UpdateCollateralElectronicCertificateForm;
    }

    public void setUpdateCollateralElectronicCertificateForm(cn.com.yusys.yusp.dto.server.biz.xddb0013.req.UpdateCollateralElectronicCertificateForm updateCollateralElectronicCertificateForm) {
        UpdateCollateralElectronicCertificateForm = updateCollateralElectronicCertificateForm;
    }

    @Override
    public String toString() {
        return "Data{" +
                "guarid='" + guarid + '\'' +
                ", UpdateCollateralElectronicCertificateForm=" + UpdateCollateralElectronicCertificateForm +
                '}';
    }
}
