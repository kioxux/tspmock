package cn.com.yusys.yusp.dto.client.esb.ypxt.certis;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求DTO：权证状态同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class CertisReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "list")
    private List<CertisListInfo> list;

    public List<CertisListInfo> getList() {
        return list;
    }

    public void setList(List<CertisListInfo> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "CertisReqDto{" +
                "list=" + list +
                '}';
    }
}
