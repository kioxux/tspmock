package cn.com.yusys.yusp.dto.client.esb.core.ln3064.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：资产转让内部借据信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3064RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lstNbjjInfo")
    private java.util.List<LstNbjjInfo> lstNbjjInfo;

    public List<LstNbjjInfo> getLstNbjjInfo() {
        return lstNbjjInfo;
    }

    public void setLstNbjjInfo(List<LstNbjjInfo> lstNbjjInfo) {
        this.lstNbjjInfo = lstNbjjInfo;
    }

    @Override
    public String toString() {
        return "Ln3064RespDto{" +
                "lstNbjjInfo=" + lstNbjjInfo +
                '}';
    }
}
