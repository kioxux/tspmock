package cn.com.yusys.yusp.dto.server.biz.xddb0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 15:26
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list_grt")
    private java.util.List<ListGrt> listGrt;

    public List<ListGrt> getListGrt() {
        return listGrt;
    }

    public void setListGrt(List<ListGrt> listGrt) {
        this.listGrt = listGrt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "listGrt=" + listGrt +
                '}';
    }
}
