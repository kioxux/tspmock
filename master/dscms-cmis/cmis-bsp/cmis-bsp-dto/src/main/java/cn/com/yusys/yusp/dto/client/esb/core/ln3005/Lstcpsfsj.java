package cn.com.yusys.yusp.dto.client.esb.core.ln3005;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款产品收费事件定义属性对象
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstcpsfsj implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "shoufshj")
    private String shoufshj;//收费事件
    @JsonProperty(value = "shfshjmc")
    private String shfshjmc;//收费事件名称
    @JsonProperty(value = "shoufzhl")
    private String shoufzhl;//收费种类
    @JsonProperty(value = "shoufdma")
    private String shoufdma;//收费代码
    @JsonProperty(value = "shfdmamc")
    private String shfdmamc;//收费代码名称
    @JsonProperty(value = "shoufjee")
    private BigDecimal shoufjee;//收费金额/比例

    public String getShoufshj() {
        return shoufshj;
    }

    public void setShoufshj(String shoufshj) {
        this.shoufshj = shoufshj;
    }

    public String getShfshjmc() {
        return shfshjmc;
    }

    public void setShfshjmc(String shfshjmc) {
        this.shfshjmc = shfshjmc;
    }

    public String getShoufzhl() {
        return shoufzhl;
    }

    public void setShoufzhl(String shoufzhl) {
        this.shoufzhl = shoufzhl;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    @Override
    public String toString() {
        return "Lstcpsfsj{" +
                "shoufshj='" + shoufshj + '\'' +
                "shfshjmc='" + shfshjmc + '\'' +
                "shoufzhl='" + shoufzhl + '\'' +
                "shoufdma='" + shoufdma + '\'' +
                "shfdmamc='" + shfdmamc + '\'' +
                "shoufjee='" + shoufjee + '\'' +
                '}';
    }
}  
