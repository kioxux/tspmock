package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款多还款账户
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkhkzh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "youxianj")
    private Integer youxianj;//优先级
    @JsonProperty(value = "hkzhhmch")
    private String hkzhhmch;//还款账户名称
    @JsonProperty(value = "hkzhhzhl")
    private String hkzhhzhl;//还款账户种类
    @JsonProperty(value = "hkzhhgze")
    private String hkzhhgze;//还款账户规则
    @JsonProperty(value = "hkjshzhl")
    private String hkjshzhl;//还款基数种类
    @JsonProperty(value = "huankbli")
    private BigDecimal huankbli;//还款比例

    public Integer getYouxianj() {
        return youxianj;
    }

    public void setYouxianj(Integer youxianj) {
        this.youxianj = youxianj;
    }

    public String getHkzhhmch() {
        return hkzhhmch;
    }

    public void setHkzhhmch(String hkzhhmch) {
        this.hkzhhmch = hkzhhmch;
    }

    public String getHkzhhzhl() {
        return hkzhhzhl;
    }

    public void setHkzhhzhl(String hkzhhzhl) {
        this.hkzhhzhl = hkzhhzhl;
    }

    public String getHkzhhgze() {
        return hkzhhgze;
    }

    public void setHkzhhgze(String hkzhhgze) {
        this.hkzhhgze = hkzhhgze;
    }

    public String getHkjshzhl() {
        return hkjshzhl;
    }

    public void setHkjshzhl(String hkjshzhl) {
        this.hkjshzhl = hkjshzhl;
    }

    public BigDecimal getHuankbli() {
        return huankbli;
    }

    public void setHuankbli(BigDecimal huankbli) {
        this.huankbli = huankbli;
    }

    @Override
    public String toString() {
        return "LstdkhkzhRespDto{" +
                "youxianj='" + youxianj + '\'' +
                "hkzhhmch='" + hkzhhmch + '\'' +
                "hkzhhzhl='" + hkzhhzhl + '\'' +
                "hkzhhgze='" + hkzhhgze + '\'' +
                "hkjshzhl='" + hkjshzhl + '\'' +
                "huankbli='" + huankbli + '\'' +
                '}';
    }
}  
