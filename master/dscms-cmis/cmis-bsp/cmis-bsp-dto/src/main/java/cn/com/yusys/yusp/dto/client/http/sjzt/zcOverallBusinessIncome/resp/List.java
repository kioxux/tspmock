package cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/22 10:34
 * @since 2021/7/22 10:34
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "asplType")
    private String asplType;//资产池类型
    @JsonProperty(value = "contNo")
    private String contNo;//资产池协议编号
    @JsonProperty(value = "zcrcjz")
    private BigDecimal zcrcjz;//资产入池价值
    @JsonProperty(value = "rczczjdkhgxd")
    private BigDecimal rczczjdkhgxd;//入池资产增加的客户贡献度
    @JsonProperty(value = "rczqzcyssyjz")
    private BigDecimal rczqzcyssyjz;//入池前资产原始收益基准
    @JsonProperty(value = "rchzcsr")
    private BigDecimal rchzcsr;//入池后资产收入
    @JsonProperty(value = "zcrchsyzzqk")
    private BigDecimal zcrchsyzzqk;//资产入池后收益增长情况
    @JsonProperty(value = "rczczyhkpjbxjfyhj")
    private BigDecimal rczczyhkpjbxjfyhj;//入池资产质押换开票据本息及费用合计
    @JsonProperty(value = "ycrczczbtxjehj")
    private BigDecimal ycrczczbtxjehj;//预测入池资产逐笔贴现金额合计
    @JsonProperty(value = "rzcbjyje")
    private BigDecimal rzcbjyje;//融资成本节约金额
    @JsonProperty(value = "rzcbjjybl")
    private BigDecimal rzcbjjybl;//融资成本节约比例

    public String getAsplType() {
        return asplType;
    }

    public void setAsplType(String asplType) {
        this.asplType = asplType;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public BigDecimal getZcrcjz() {
        return zcrcjz;
    }

    public void setZcrcjz(BigDecimal zcrcjz) {
        this.zcrcjz = zcrcjz;
    }

    public BigDecimal getRczczjdkhgxd() {
        return rczczjdkhgxd;
    }

    public void setRczczjdkhgxd(BigDecimal rczczjdkhgxd) {
        this.rczczjdkhgxd = rczczjdkhgxd;
    }

    public BigDecimal getRczqzcyssyjz() {
        return rczqzcyssyjz;
    }

    public void setRczqzcyssyjz(BigDecimal rczqzcyssyjz) {
        this.rczqzcyssyjz = rczqzcyssyjz;
    }

    public BigDecimal getRchzcsr() {
        return rchzcsr;
    }

    public void setRchzcsr(BigDecimal rchzcsr) {
        this.rchzcsr = rchzcsr;
    }

    public BigDecimal getZcrchsyzzqk() {
        return zcrchsyzzqk;
    }

    public void setZcrchsyzzqk(BigDecimal zcrchsyzzqk) {
        this.zcrchsyzzqk = zcrchsyzzqk;
    }

    public BigDecimal getRczczyhkpjbxjfyhj() {
        return rczczyhkpjbxjfyhj;
    }

    public void setRczczyhkpjbxjfyhj(BigDecimal rczczyhkpjbxjfyhj) {
        this.rczczyhkpjbxjfyhj = rczczyhkpjbxjfyhj;
    }

    public BigDecimal getYcrczczbtxjehj() {
        return ycrczczbtxjehj;
    }

    public void setYcrczczbtxjehj(BigDecimal ycrczczbtxjehj) {
        this.ycrczczbtxjehj = ycrczczbtxjehj;
    }

    public BigDecimal getRzcbjyje() {
        return rzcbjyje;
    }

    public void setRzcbjyje(BigDecimal rzcbjyje) {
        this.rzcbjyje = rzcbjyje;
    }

    public BigDecimal getRzcbjjybl() {
        return rzcbjjybl;
    }

    public void setRzcbjjybl(BigDecimal rzcbjjybl) {
        this.rzcbjjybl = rzcbjjybl;
    }

    @Override
    public String toString() {
        return "List{" +
                "asplType='" + asplType + '\'' +
                ", contNo='" + contNo + '\'' +
                ", zcrcjz=" + zcrcjz +
                ", rczczjdkhgxd=" + rczczjdkhgxd +
                ", rczqzcyssyjz=" + rczqzcyssyjz +
                ", rchzcsr=" + rchzcsr +
                ", zcrchsyzzqk=" + zcrchsyzzqk +
                ", rczczyhkpjbxjfyhj=" + rczczyhkpjbxjfyhj +
                ", ycrczczbtxjehj=" + ycrczczbtxjehj +
                ", rzcbjyje=" + rzcbjyje +
                ", rzcbjjybl=" + rzcbjjybl +
                '}';
    }
}
