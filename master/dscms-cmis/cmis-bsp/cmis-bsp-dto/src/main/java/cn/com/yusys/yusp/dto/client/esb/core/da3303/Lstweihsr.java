package cn.com.yusys.yusp.dto.client.esb.core.da3303;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：抵债资产信息维护
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstweihsr implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    @Override
    public String toString() {
        return "Lstweihsr{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", huankjee=" + huankjee +
                '}';
    }
}
