package cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

// 贷款账户质押信息
public class Lstdkzhzy {
    @JsonProperty(value = "shfbhzhh")
    private String shfbhzhh;//是否本行账号
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "khzhhzxh")
    private String khzhhzxh;//客户账号子序号
    @JsonProperty(value = "zhjzhyfs")
    private String zhjzhyfs;//质押方式
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "zhiyajee")
    private BigDecimal zhiyajee;//质押金额
    @JsonProperty(value = "zyzhleix")
    private String zyzhleix;//质押账户类型
    @JsonProperty(value = "zhydaoqr")
    private String zhydaoqr;//质押到期日
    @JsonProperty(value = "dzywbhao")
    private String dzywbhao;//抵质押物编号

    public String getShfbhzhh() {
        return shfbhzhh;
    }

    public void setShfbhzhh(String shfbhzhh) {
        this.shfbhzhh = shfbhzhh;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    public String getZhjzhyfs() {
        return zhjzhyfs;
    }

    public void setZhjzhyfs(String zhjzhyfs) {
        this.zhjzhyfs = zhjzhyfs;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getZhiyajee() {
        return zhiyajee;
    }

    public void setZhiyajee(BigDecimal zhiyajee) {
        this.zhiyajee = zhiyajee;
    }

    public String getZyzhleix() {
        return zyzhleix;
    }

    public void setZyzhleix(String zyzhleix) {
        this.zyzhleix = zyzhleix;
    }

    public String getZhydaoqr() {
        return zhydaoqr;
    }

    public void setZhydaoqr(String zhydaoqr) {
        this.zhydaoqr = zhydaoqr;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    @Override
    public String toString() {
        return "Lstdkzhzy{" +
                "shfbhzhh='" + shfbhzhh + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", khzhhzxh='" + khzhhzxh + '\'' +
                ", zhjzhyfs='" + zhjzhyfs + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", zhiyajee=" + zhiyajee +
                ", zyzhleix='" + zyzhleix + '\'' +
                ", zhydaoqr='" + zhydaoqr + '\'' +
                ", dzywbhao='" + dzywbhao + '\'' +
                '}';
    }
}
