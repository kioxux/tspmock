package cn.com.yusys.yusp.dto.server.biz.xdht0032.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto: 根据核心客户号查询我行信用类合同金额汇总
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cdtContTotlAmt")
    private String cdtContTotlAmt;//信用类合同总金额
    @JsonProperty(value = "latestInureYqdContAmt")
    private String latestInureYqdContAmt;//最近生效优企贷合同金额

    public String getCdtContTotlAmt() {
        return cdtContTotlAmt;
    }

    public void setCdtContTotlAmt(String cdtContTotlAmt) {
        this.cdtContTotlAmt = cdtContTotlAmt;
    }

    public String getLatestInureYqdContAmt() {
        return latestInureYqdContAmt;
    }

    public void setLatestInureYqdContAmt(String latestInureYqdContAmt) {
        this.latestInureYqdContAmt = latestInureYqdContAmt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cdtContTotlAmt='" + cdtContTotlAmt + '\'' +
                ", latestInureYqdContAmt='" + latestInureYqdContAmt + '\'' +
                '}';
    }
}