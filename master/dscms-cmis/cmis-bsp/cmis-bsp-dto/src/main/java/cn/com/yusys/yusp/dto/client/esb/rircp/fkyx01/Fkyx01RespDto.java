package cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：优享贷客户经理分配通知接口
 */
@JsonPropertyOrder(alphabetic = true)
public class Fkyx01RespDto implements Serializable {
    private static final long serialVersionUID = 1L;


}
