package cn.com.yusys.yusp.dto.server.biz.xddh0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/22 15:40:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 15:40
 * @since 2021/5/22 15:40
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "modelResult")
    private String modelResult;//模型结果
    @JsonProperty(value = "warnDate")
    private String warnDate;//预警日期
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//逾期本金
    @JsonProperty(value = "loanReality")
    private BigDecimal loanReality;//逾期利息

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getModelResult() {
        return modelResult;
    }

    public void setModelResult(String modelResult) {
        this.modelResult = modelResult;
    }

    public String getWarnDate() {
        return warnDate;
    }

    public void setWarnDate(String warnDate) {
        this.warnDate = warnDate;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getLoanReality() {
        return loanReality;
    }

    public void setLoanReality(BigDecimal loanReality) {
        this.loanReality = loanReality;
    }

    @Override
    public String toString() {
        return "Xddh0010ReqDto{" +
                "billNo='" + billNo + '\'' +
                "modelResult='" + modelResult + '\'' +
                "warnDate='" + warnDate + '\'' +
                "loanBalance='" + loanBalance + '\'' +
                "loanReality='" + loanReality + '\'' +
                '}';
    }
}
