package cn.com.yusys.yusp.dto.client.esb.circp.fb1174.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：房抵e点贷尽调结果通知
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1174ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "is_admint")
    private String is_admint;//是否予以准入
    @JsonProperty(value = "app_no")
    private String app_no;//业务流水号
    @JsonProperty(value = "cust_core_id")
    private String cust_core_id;//核心客户号
    @JsonProperty(value = "cust_name")
    private String cust_name;//个人客户名
    @JsonProperty(value = "cert_code")
    private String cert_code;//身份证号

    public String getIs_admint() {
        return is_admint;
    }

    public void setIs_admint(String is_admint) {
        this.is_admint = is_admint;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getCust_core_id() {
        return cust_core_id;
    }

    public void setCust_core_id(String cust_core_id) {
        this.cust_core_id = cust_core_id;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    @Override
    public String toString() {
        return "Fb1174ReqDto{" +
                "is_admint='" + is_admint + '\'' +
                ", app_no='" + app_no + '\'' +
                ", cust_core_id='" + cust_core_id + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", cert_code='" + cert_code + '\'' +
                '}';
    }
}
