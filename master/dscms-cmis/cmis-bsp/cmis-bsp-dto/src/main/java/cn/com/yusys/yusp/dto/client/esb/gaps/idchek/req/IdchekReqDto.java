package cn.com.yusys.yusp.dto.client.esb.gaps.idchek.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：身份证核查
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class IdchekReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sfhm")
    private String sfhm;//身份证号码
    @JsonProperty(value = "sfmc")
    private String sfmc;//姓名
    @JsonProperty(value = "yhhh")
    private String yhhh;//发起行行号
    @JsonProperty(value = "yhhm")
    private String yhhm;//发起行名称
    @JsonProperty(value = "flcmc")
    private String flcmc;//分理处名称
    @JsonProperty(value = "flcdz")
    private String flcdz;//分理处地址
    @JsonProperty(value = "flcdh")
    private String flcdh;//分理处电话
    @JsonProperty(value = "ywlx")
    private String ywlx;//业务类型

    public String getSfhm() {
        return sfhm;
    }

    public void setSfhm(String sfhm) {
        this.sfhm = sfhm;
    }

    public String getSfmc() {
        return sfmc;
    }

    public void setSfmc(String sfmc) {
        this.sfmc = sfmc;
    }

    public String getYhhh() {
        return yhhh;
    }

    public void setYhhh(String yhhh) {
        this.yhhh = yhhh;
    }

    public String getYhhm() {
        return yhhm;
    }

    public void setYhhm(String yhhm) {
        this.yhhm = yhhm;
    }

    public String getFlcmc() {
        return flcmc;
    }

    public void setFlcmc(String flcmc) {
        this.flcmc = flcmc;
    }

    public String getFlcdz() {
        return flcdz;
    }

    public void setFlcdz(String flcdz) {
        this.flcdz = flcdz;
    }

    public String getFlcdh() {
        return flcdh;
    }

    public void setFlcdh(String flcdh) {
        this.flcdh = flcdh;
    }

    public String getYwlx() {
        return ywlx;
    }

    public void setYwlx(String ywlx) {
        this.ywlx = ywlx;
    }
}
