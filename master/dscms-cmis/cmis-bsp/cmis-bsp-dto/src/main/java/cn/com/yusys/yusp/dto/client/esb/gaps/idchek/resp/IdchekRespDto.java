package cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：身份证核查
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class IdchekRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "idcard")
    private String idcard;//身份证号
    @JsonProperty(value = "idname")
    private String idname;//姓名
    @JsonProperty(value = "result")
    private String result;//核查结果
    @JsonProperty(value = "office")
    private String office;//签发机关
    @JsonProperty(value = "infotx")
    private String infotx;//图片返回

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getIdname() {
        return idname;
    }

    public void setIdname(String idname) {
        this.idname = idname;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getInfotx() {
        return infotx;
    }

    public void setInfotx(String infotx) {
        this.infotx = infotx;
    }

    @Override
    public String toString() {
        return "IdchekRespDto{" +
                "idcard='" + idcard + '\'' +
                ", idname='" + idname + '\'' +
                ", result='" + result + '\'' +
                ", office='" + office + '\'' +
                ", infotx='" + infotx + '\'' +
                '}';
    }
}
