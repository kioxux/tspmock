package cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：现金（大额）放款接口
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D13087RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lentus")
    private String lentus;//放款状态
    @JsonProperty(value = "b039")
    private String b039;//行内放款 39 域响应码

    public String getLentus() {
        return lentus;
    }

    public void setLentus(String lentus) {
        this.lentus = lentus;
    }

    public String getB039() {
        return b039;
    }

    public void setB039(String b039) {
        this.b039 = b039;
    }

    @Override
    public String toString() {
        return "D13087RespDto{" +
                "lentus='" + lentus + '\'' +
                "b039='" + b039 + '\'' +
                '}';
    }
}  
