package cn.com.yusys.yusp.dto.server.biz.xdxw0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：联系人信息维护
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "surveySerno")
    private String surveySerno;//批复号
    @JsonProperty(value = "lxrList")
    private java.util.List<List> list;

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "surveySerno='" + surveySerno + '\'' +
                ", lxrList=" + list +
                '}';
    }
}
