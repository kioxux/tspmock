package cn.com.yusys.yusp.dto.client.esb.core.ln3235;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：还款方式转换
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3235ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "tshkfshi")
    private String tshkfshi;//特殊还款方式
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "huanbzhq")
    private String huanbzhq;//还本周期
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getTshkfshi() {
        return tshkfshi;
    }

    public void setTshkfshi(String tshkfshi) {
        this.tshkfshi = tshkfshi;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    @Override
    public String toString() {
        return "Ln3235ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "tshkfshi='" + tshkfshi + '\'' +
                "zhchbjin='" + zhchbjin + '\'' +
                "kaihriqi='" + kaihriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "huanbzhq='" + huanbzhq + '\'' +
                "dechligz='" + dechligz + '\'' +
                '}';
    }
}  
