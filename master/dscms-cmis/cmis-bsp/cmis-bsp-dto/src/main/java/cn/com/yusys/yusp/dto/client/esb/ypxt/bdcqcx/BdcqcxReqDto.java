package cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 请求DTO：不动产权证查询接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日15:12:29
 */
@JsonPropertyOrder(alphabetic = true)
public class BdcqcxReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarid")
    private String guarid;// 押品编号 否
    @JsonProperty(value = "grttyp")
    private String grttyp;// 押品细分类 否

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public String getGrttyp() {
        return grttyp;
    }

    public void setGrttyp(String grttyp) {
        this.grttyp = grttyp;
    }

    @Override
    public String toString() {
        return "BdcqcxReqDto{" +
                "guarid='" + guarid + '\'' +
                ", grttyp='" + grttyp + '\'' +
                '}';
    }
}
