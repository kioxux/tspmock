package cn.com.yusys.yusp.dto.client.esb.core.ln3038.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 14:22
 * @since 2021/5/28 14:22
 */
public class Lstdkbgmx {

    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "xuhaoooo")
    private Integer xuhaoooo;//序号
    @JsonProperty(value = "zhhujizd")
    private String zhhujizd;//账户级字段
    @JsonProperty(value = "zhhjzdms")
    private String zhhjzdms;//账户级字段描述
    @JsonProperty(value = "yshjuzhi")
    private String yshjuzhi;//原数据值
    @JsonProperty(value = "xshjuzhi")
    private String xshjuzhi;//新数据值
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getZhhujizd() {
        return zhhujizd;
    }

    public void setZhhujizd(String zhhujizd) {
        this.zhhujizd = zhhujizd;
    }

    public String getZhhjzdms() {
        return zhhjzdms;
    }

    public void setZhhjzdms(String zhhjzdms) {
        this.zhhjzdms = zhhjzdms;
    }

    public String getYshjuzhi() {
        return yshjuzhi;
    }

    public void setYshjuzhi(String yshjuzhi) {
        this.yshjuzhi = yshjuzhi;
    }

    public String getXshjuzhi() {
        return xshjuzhi;
    }

    public void setXshjuzhi(String xshjuzhi) {
        this.xshjuzhi = xshjuzhi;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    @Override
    public String toString() {
        return "Lstdkbgmx{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", xuhaoooo=" + xuhaoooo +
                ", zhhujizd='" + zhhujizd + '\'' +
                ", zhhjzdms='" + zhhjzdms + '\'' +
                ", yshjuzhi='" + yshjuzhi + '\'' +
                ", xshjuzhi='" + xshjuzhi + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", jiaoyima='" + jiaoyima + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                '}';
    }
}
