package cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：现金（大额）放款接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D13087ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "rgstid")
    private String rgstid;//分期申请号
    @JsonProperty(value = "cardno")
    private String cardno;//卡号
    @JsonProperty(value = "remark")
    private String remark;//备注信息

    public String getRgstid() {
        return rgstid;
    }

    public void setRgstid(String rgstid) {
        this.rgstid = rgstid;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "D13087ReqDto{" +
                "rgstid='" + rgstid + '\'' +
                "cardno='" + cardno + '\'' +
                "remark='" + remark + '\'' +
                '}';
    }
}  
