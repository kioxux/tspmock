package cn.com.yusys.yusp.dto.client.esb.core.co3225.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：抵质押物明细查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Co3225RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lstdzywmx")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.co3225.resp.Lstdzywmx> lstdzywmx;

    public List<Lstdzywmx> getLstdzywmx() {
        return lstdzywmx;
    }

    public void setLstdzywmx(List<Lstdzywmx> lstdzywmx) {
        this.lstdzywmx = lstdzywmx;
    }

    @Override
    public String toString() {
        return "Co3225RespDto{" +
                "lstdzywmx=" + lstdzywmx +
                '}';
    }
}
