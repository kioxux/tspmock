package cn.com.yusys.yusp.dto.client.esb.core.ln3026;

import cn.com.yusys.yusp.dto.client.esb.core.ln3026.req.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：银团贷款开户
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3026ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkkhczbz")
    private String dkkhczbz;//开户操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "zidfkbzh")
    private String zidfkbzh;//自动放款标志
    @JsonProperty(value = "zdkoukbz")
    private String zdkoukbz;//自动扣款标志
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "dkczhzhh")
    private String dkczhzhh;//贷款出账号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "bencfkje")
    private BigDecimal bencfkje;//本次放款金额
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "hetongll")
    private BigDecimal hetongll;//合同利率
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "zclilvbh")
    private String zclilvbh;//正常利率编号
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "yuqillbh")
    private String yuqillbh;//逾期利率编号
    @JsonProperty(value = "yuqinyll")
    private String yuqinyll;//逾期年月利率
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "yuqitzfs")
    private String yuqitzfs;//逾期利率调整方式
    @JsonProperty(value = "yuqitzzq")
    private String yuqitzzq;//逾期利率调整周期
    @JsonProperty(value = "yqfxfdfs")
    private String yqfxfdfs;//逾期罚息浮动方式
    @JsonProperty(value = "yqfxfdzh")
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    @JsonProperty(value = "fulilvbh")
    private String fulilvbh;//复利利率编号
    @JsonProperty(value = "fulilvny")
    private String fulilvny;//复利利率年月标识
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "fulitzfs")
    private String fulitzfs;//复利利率调整方式
    @JsonProperty(value = "fulitzzq")
    private String fulitzzq;//复利利率调整周期
    @JsonProperty(value = "fulifdfs")
    private String fulifdfs;//复利利率浮动方式
    @JsonProperty(value = "fulifdzh")
    private BigDecimal fulifdzh;//复利利率浮动值
    @JsonProperty(value = "zyjjcpdm")
    private String zyjjcpdm;//自营借据产品代码
    @JsonProperty(value = "zyjjcpmc")
    private String zyjjcpmc;//自营借据产品名称
    @JsonProperty(value = "fzyjcpdm")
    private String fzyjcpdm;//非自营借据产品代码
    @JsonProperty(value = "fzyjcpmc")
    private String fzyjcpmc;//非自营借据产品名称
    @JsonProperty(value = "wtckywbm")
    private String wtckywbm;//内部专户核算码
    @JsonProperty(value = "kshchpdm")
    private String kshchpdm;//可售产品代码
    @JsonProperty(value = "kshchpmc")
    private String kshchpmc;//可售产品名称
    @JsonProperty(value = "kuaijilb")
    private String kuaijilb;//会计类别
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "dkrzhzxh")
    private String dkrzhzxh;//贷款入账账号子序号
    @JsonProperty(value = "lstytczfe")
    private List<Lstytczfe> lstytczfe;//银团出资份额定价计息复合类型
    @JsonProperty(value = "yintdkbz")
    private String yintdkbz;//银团贷款
    @JsonProperty(value = "dkdbfshi")
    private String dkdbfshi;//贷款担保方式
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "jixiguiz")
    private String jixiguiz;//计息规则
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "jfxibzhi")
    private String jfxibzhi;//计复息标志
    @JsonProperty(value = "flllcklx")
    private String flllcklx;//复利利率参考类型
    @JsonProperty(value = "wujiflbz")
    private String wujiflbz;//五级分类标志
    @JsonProperty(value = "dkyongtu")
    private String dkyongtu;//贷款用途
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注
    @JsonProperty(value = "fuhejgou")
    private String fuhejgou;//复核机构
    @JsonProperty(value = "lstdkhbjh")
    private List<Lstdkhbjh> lstdkhbjh;//贷款还本计划
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "lstdzqgjh")
    private List<Lstdzqgjh> lstdzqgjh;//贷款定制期供计划表
    @JsonProperty(value = "llqxkdfs")
    private String llqxkdfs;//利率期限靠档方式
    @JsonProperty(value = "lilvqixx")
    private String lilvqixx;//利率期限
    @JsonProperty(value = "fkjzhfsh")
    private String fkjzhfsh;//放款记账方式
    @JsonProperty(value = "fkzjclfs")
    private String fkzjclfs;//放款资金处理方式
    @JsonProperty(value = "zjlyzhao")
    private String zjlyzhao;//资金来源账号
    @JsonProperty(value = "zjlyzzxh")
    private String zjlyzzxh;//资金来源账号子序号
    @JsonProperty(value = "dhkzhhbz")
    private String dhkzhhbz;//多还款账户标志
    @JsonProperty(value = "lstdkhkzh")
    private List<Lstdkhkzh> lstdkhkzh;//贷款多还款账户
    @JsonProperty(value = "sfxmyint")
    private String sfxmyint;//是否项目类银团标志
    @JsonProperty(value = "lstdkstzf")
    private List<Lstdkstzf> lstdkstzf;//贷款受托支付


    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getZidfkbzh() {
        return zidfkbzh;
    }

    public void setZidfkbzh(String zidfkbzh) {
        this.zidfkbzh = zidfkbzh;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getDkczhzhh() {
        return dkczhzhh;
    }

    public void setDkczhzhh(String dkczhzhh) {
        this.dkczhzhh = dkczhzhh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getBencfkje() {
        return bencfkje;
    }

    public void setBencfkje(BigDecimal bencfkje) {
        this.bencfkje = bencfkje;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getZyjjcpdm() {
        return zyjjcpdm;
    }

    public void setZyjjcpdm(String zyjjcpdm) {
        this.zyjjcpdm = zyjjcpdm;
    }

    public String getZyjjcpmc() {
        return zyjjcpmc;
    }

    public void setZyjjcpmc(String zyjjcpmc) {
        this.zyjjcpmc = zyjjcpmc;
    }

    public String getFzyjcpdm() {
        return fzyjcpdm;
    }

    public void setFzyjcpdm(String fzyjcpdm) {
        this.fzyjcpdm = fzyjcpdm;
    }

    public String getFzyjcpmc() {
        return fzyjcpmc;
    }

    public void setFzyjcpmc(String fzyjcpmc) {
        this.fzyjcpmc = fzyjcpmc;
    }

    public String getWtckywbm() {
        return wtckywbm;
    }

    public void setWtckywbm(String wtckywbm) {
        this.wtckywbm = wtckywbm;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public List<Lstytczfe> getLstytczfe() {
        return lstytczfe;
    }

    public void setLstytczfe(List<Lstytczfe> lstytczfe) {
        this.lstytczfe = lstytczfe;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getWujiflbz() {
        return wujiflbz;
    }

    public void setWujiflbz(String wujiflbz) {
        this.wujiflbz = wujiflbz;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public List<Lstdkhbjh> getLstdkhbjh() {
        return lstdkhbjh;
    }

    public void setLstdkhbjh(List<Lstdkhbjh> lstdkhbjh) {
        this.lstdkhbjh = lstdkhbjh;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public List<Lstdzqgjh> getLstdzqgjh() {
        return lstdzqgjh;
    }

    public void setLstdzqgjh(List<Lstdzqgjh> lstdzqgjh) {
        this.lstdzqgjh = lstdzqgjh;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public String getFkzjclfs() {
        return fkzjclfs;
    }

    public void setFkzjclfs(String fkzjclfs) {
        this.fkzjclfs = fkzjclfs;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public List<Lstdkhkzh> getLstdkhkzh() {
        return lstdkhkzh;
    }

    public void setLstdkhkzh(List<Lstdkhkzh> lstdkhkzh) {
        this.lstdkhkzh = lstdkhkzh;
    }

    public String getSfxmyint() {
        return sfxmyint;
    }

    public void setSfxmyint(String sfxmyint) {
        this.sfxmyint = sfxmyint;
    }

    public List<Lstdkstzf> getLstdkstzf() {
        return lstdkstzf;
    }

    public void setLstdkstzf(List<Lstdkstzf> lstdkstzf) {
        this.lstdkstzf = lstdkstzf;
    }

    @Override
    public String toString() {
        return "Ln3026ReqDto{" +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "zidfkbzh='" + zidfkbzh + '\'' +
                "zdkoukbz='" + zdkoukbz + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "dkczhzhh='" + dkczhzhh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "dkqixian='" + dkqixian + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "hetongje='" + hetongje + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "bencfkje='" + bencfkje + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "hetongll='" + hetongll + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                "nyuelilv='" + nyuelilv + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "yuqillbh='" + yuqillbh + '\'' +
                "yuqinyll='" + yuqinyll + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "yuqitzfs='" + yuqitzfs + '\'' +
                "yuqitzzq='" + yuqitzzq + '\'' +
                "yqfxfdfs='" + yqfxfdfs + '\'' +
                "yqfxfdzh='" + yqfxfdzh + '\'' +
                "fulilvbh='" + fulilvbh + '\'' +
                "fulilvny='" + fulilvny + '\'' +
                "fulililv='" + fulililv + '\'' +
                "fulitzfs='" + fulitzfs + '\'' +
                "fulitzzq='" + fulitzzq + '\'' +
                "fulifdfs='" + fulifdfs + '\'' +
                "fulifdzh='" + fulifdzh + '\'' +
                "zyjjcpdm='" + zyjjcpdm + '\'' +
                "zyjjcpmc='" + zyjjcpmc + '\'' +
                "fzyjcpdm='" + fzyjcpdm + '\'' +
                "fzyjcpmc='" + fzyjcpmc + '\'' +
                "wtckywbm='" + wtckywbm + '\'' +
                "kshchpdm='" + kshchpdm + '\'' +
                "kshchpmc='" + kshchpmc + '\'' +
                "kuaijilb='" + kuaijilb + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "lstytczfe='" + lstytczfe + '\'' +
                "yintdkbz='" + yintdkbz + '\'' +
                "dkdbfshi='" + dkdbfshi + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "jixiguiz='" + jixiguiz + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "jfxibzhi='" + jfxibzhi + '\'' +
                "flllcklx='" + flllcklx + '\'' +
                "wujiflbz='" + wujiflbz + '\'' +
                "dkyongtu='" + dkyongtu + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                "lstdkhbjh='" + lstdkhbjh + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "lstdzqgjh='" + lstdzqgjh + '\'' +
                "llqxkdfs='" + llqxkdfs + '\'' +
                "lilvqixx='" + lilvqixx + '\'' +
                "fkjzhfsh='" + fkjzhfsh + '\'' +
                "fkzjclfs='" + fkzjclfs + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "lstdkhkzh='" + lstdkhkzh + '\'' +
                "sfxmyint='" + sfxmyint + '\'' +
                "lstdkstzf='" + lstdkstzf + '\'' +
                '}';
    }
}  
