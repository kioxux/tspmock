package cn.com.yusys.yusp.dto.client.esb.doc.doc000.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/17 18:29
 * @since 2021/6/17 18:29
 */
public class List_index implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contid")
    private String contid;
    @JsonProperty(value = "contdate")
    private String contdate;
    @JsonProperty(value = "custname")
    private String custname;
    @JsonProperty(value = "businessid")
    private String businessid;
    @JsonProperty(value = "outsystem_code")
    private String outsystem_code;
    @JsonProperty(value = "custid")
    private String custid;
    @JsonProperty(value = "billno")
    private String billno;
    @JsonProperty(value = "custconduct")
    private String custconduct;
    @JsonProperty(value = "custtype")
    private String custtype;

    public String getContid() {
        return contid;
    }

    public void setContid(String contid) {
        this.contid = contid;
    }

    public String getContdate() {
        return contdate;
    }

    public void setContdate(String contdate) {
        this.contdate = contdate;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getOutsystem_code() {
        return outsystem_code;
    }

    public void setOutsystem_code(String outsystem_code) {
        this.outsystem_code = outsystem_code;
    }

    public String getBusinessid() {
        return businessid;
    }

    public void setBusinessid(String businessid) {
        this.businessid = businessid;
    }
    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getCustconduct() {
        return custconduct;
    }

    public void setCustconduct(String custconduct) {
        this.custconduct = custconduct;
    }

    public String getCusttype() {
        return custtype;
    }

    public void setCusttype(String custtype) {
        this.custtype = custtype;
    }

    @Override
    public String toString() {
        return "List_index{" +
                "contid='" + contid + '\'' +
                ", contdate='" + contdate + '\'' +
                ", custname='" + custname + '\'' +
                ", businessid='" + businessid + '\'' +
                ", outsystem_code='" + outsystem_code + '\'' +
                ", custid='" + custid + '\'' +
                ", billno='" + billno + '\'' +
                ", custconduct='" + custconduct + '\'' +
                ", custtype='" + custtype + '\'' +
                '}';
    }
}
