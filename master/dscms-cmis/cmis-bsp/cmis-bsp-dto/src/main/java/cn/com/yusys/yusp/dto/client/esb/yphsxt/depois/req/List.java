package cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/5 15:08
 * @since 2021/6/5 15:08
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guar_cus_id")
    private String guar_cus_id;//所有权人编号
    @JsonProperty(value = "guar_cus_name")
    private String guar_cus_name;//所有权人名称
    @JsonProperty(value = "guar_cert_type")
    private String guar_cert_type;//押品所有人证件类型
    @JsonProperty(value = "guar_cert_code")
    private String guar_cert_code;//押品所有人证件号码
    @JsonProperty(value = "guar_name")
    private String guar_name;//抵质押品名称
    @JsonProperty(value = "guar_type_cd")
    private String guar_type_cd;//担保分类代码
    @JsonProperty(value = "create_sys")
    private String create_sys;//创建系统
    @JsonProperty(value = "account_manager")
    private String account_manager;//管户人
    @JsonProperty(value = "guar_lastupdate_date")
    private String guar_lastupdate_date;//最后更新时间
    @JsonProperty(value = "lastmodify_userid")
    private String lastmodify_userid;//最后修改人
    @JsonProperty(value = "lastmodify_orgid")
    private String lastmodify_orgid;//最后修改人机构
    @JsonProperty(value = "guar_cus_type")
    private String guar_cus_type;//押品所有人类型
    @JsonProperty(value = "common_assets_ind")
    private String common_assets_ind;//是否共有财产
    @JsonProperty(value = "is_ownership_clear")
    private String is_ownership_clear;//是否权属清晰
    @JsonProperty(value = "insurance_ind")
    private String insurance_ind;//是否需要办理保险
    @JsonProperty(value = "relation_int")
    private String relation_int;//是否实质正相关
    @JsonProperty(value = "legal_pri_payment")
    private BigDecimal legal_pri_payment;//法定优先受偿款
    @JsonProperty(value = "def_effect_type")
    private String def_effect_type;//担保权生效方式
    @JsonProperty(value = "contract_justice_ind")
    private String contract_justice_ind;//是否需要抵质押合同公证
    @JsonProperty(value = "other_back_guar_ind")
    private String other_back_guar_ind;//他行是否已设定担保权
    @JsonProperty(value = "if_deal")
    private String if_deal;//是否抵债资产
    @JsonProperty(value = "guar_borrower_rela")
    private String guar_borrower_rela;//抵质押物与借款人相关性
    @JsonProperty(value = "shut_down_conv")
    private String shut_down_conv;//查封便利性
    @JsonProperty(value = "legal_validity")
    private String legal_validity;//法律有效性
    @JsonProperty(value = "guar_universality")
    private String guar_universality;//抵质押品通用性
    @JsonProperty(value = "sale_state")
    private String sale_state;//抵质押品变现能力
    @JsonProperty(value = "price_volatility")
    private String price_volatility;//价格波动性
    @JsonProperty(value = "receipt_no")
    private String receipt_no;//存单凭证号
    @JsonProperty(value = "account_no")
    private String account_no;//账户号码
    @JsonProperty(value = "freeze_amt")
    private BigDecimal freeze_amt;//存单冻结/止付金额
    @JsonProperty(value = "start_date")
    private String start_date;//起始日
    @JsonProperty(value = "store_term")
    private BigDecimal store_term;//存期(月)
    @JsonProperty(value = "depo_cert_no")
    private String depo_cert_no;//单位存款开户证书编号
    @JsonProperty(value = "stop_payment_date")
    private String stop_payment_date;//止付起始日期
    @JsonProperty(value = "oth_bank_name")
    private String oth_bank_name;//银行名称
    @JsonProperty(value = "interest_acc")
    private String interest_acc;//存单利息账号
    @JsonProperty(value = "account_num")
    private String account_num;//子账户序号
    @JsonProperty(value = "origin_amt")
    private BigDecimal origin_amt;//存单原始金额
    @JsonProperty(value = "cur_type")
    private String cur_type;//币种
    @JsonProperty(value = "end_date")
    private String end_date;//到期日
    @JsonProperty(value = "rate")
    private BigDecimal rate;//利率
    @JsonProperty(value = "stop_payment_no")
    private String stop_payment_no;//止付通知书编号
    @JsonProperty(value = "stop_payment_end_date")
    private String stop_payment_end_date;//止付到期日期
    @JsonProperty(value = "remark")
    private String remark;//备注
    @JsonProperty(value = "assetNo")
    private String assetNo;

    public String getGuar_cus_id() {
        return guar_cus_id;
    }

    public void setGuar_cus_id(String guar_cus_id) {
        this.guar_cus_id = guar_cus_id;
    }

    public String getGuar_cus_name() {
        return guar_cus_name;
    }

    public void setGuar_cus_name(String guar_cus_name) {
        this.guar_cus_name = guar_cus_name;
    }

    public String getGuar_cert_type() {
        return guar_cert_type;
    }

    public void setGuar_cert_type(String guar_cert_type) {
        this.guar_cert_type = guar_cert_type;
    }

    public String getGuar_cert_code() {
        return guar_cert_code;
    }

    public void setGuar_cert_code(String guar_cert_code) {
        this.guar_cert_code = guar_cert_code;
    }

    public String getGuar_name() {
        return guar_name;
    }

    public void setGuar_name(String guar_name) {
        this.guar_name = guar_name;
    }

    public String getGuar_type_cd() {
        return guar_type_cd;
    }

    public void setGuar_type_cd(String guar_type_cd) {
        this.guar_type_cd = guar_type_cd;
    }

    public String getCreate_sys() {
        return create_sys;
    }

    public void setCreate_sys(String create_sys) {
        this.create_sys = create_sys;
    }

    public String getAccount_manager() {
        return account_manager;
    }

    public void setAccount_manager(String account_manager) {
        this.account_manager = account_manager;
    }

    public String getGuar_lastupdate_date() {
        return guar_lastupdate_date;
    }

    public void setGuar_lastupdate_date(String guar_lastupdate_date) {
        this.guar_lastupdate_date = guar_lastupdate_date;
    }

    public String getLastmodify_userid() {
        return lastmodify_userid;
    }

    public void setLastmodify_userid(String lastmodify_userid) {
        this.lastmodify_userid = lastmodify_userid;
    }

    public String getLastmodify_orgid() {
        return lastmodify_orgid;
    }

    public void setLastmodify_orgid(String lastmodify_orgid) {
        this.lastmodify_orgid = lastmodify_orgid;
    }

    public String getGuar_cus_type() {
        return guar_cus_type;
    }

    public void setGuar_cus_type(String guar_cus_type) {
        this.guar_cus_type = guar_cus_type;
    }

    public String getCommon_assets_ind() {
        return common_assets_ind;
    }

    public void setCommon_assets_ind(String common_assets_ind) {
        this.common_assets_ind = common_assets_ind;
    }

    public String getIs_ownership_clear() {
        return is_ownership_clear;
    }

    public void setIs_ownership_clear(String is_ownership_clear) {
        this.is_ownership_clear = is_ownership_clear;
    }

    public String getInsurance_ind() {
        return insurance_ind;
    }

    public void setInsurance_ind(String insurance_ind) {
        this.insurance_ind = insurance_ind;
    }

    public String getRelation_int() {
        return relation_int;
    }

    public void setRelation_int(String relation_int) {
        this.relation_int = relation_int;
    }

    public BigDecimal getLegal_pri_payment() {
        return legal_pri_payment;
    }

    public void setLegal_pri_payment(BigDecimal legal_pri_payment) {
        this.legal_pri_payment = legal_pri_payment;
    }

    public String getDef_effect_type() {
        return def_effect_type;
    }

    public void setDef_effect_type(String def_effect_type) {
        this.def_effect_type = def_effect_type;
    }

    public String getContract_justice_ind() {
        return contract_justice_ind;
    }

    public void setContract_justice_ind(String contract_justice_ind) {
        this.contract_justice_ind = contract_justice_ind;
    }

    public String getOther_back_guar_ind() {
        return other_back_guar_ind;
    }

    public void setOther_back_guar_ind(String other_back_guar_ind) {
        this.other_back_guar_ind = other_back_guar_ind;
    }

    public String getIf_deal() {
        return if_deal;
    }

    public void setIf_deal(String if_deal) {
        this.if_deal = if_deal;
    }

    public String getGuar_borrower_rela() {
        return guar_borrower_rela;
    }

    public void setGuar_borrower_rela(String guar_borrower_rela) {
        this.guar_borrower_rela = guar_borrower_rela;
    }

    public String getShut_down_conv() {
        return shut_down_conv;
    }

    public void setShut_down_conv(String shut_down_conv) {
        this.shut_down_conv = shut_down_conv;
    }

    public String getLegal_validity() {
        return legal_validity;
    }

    public void setLegal_validity(String legal_validity) {
        this.legal_validity = legal_validity;
    }

    public String getGuar_universality() {
        return guar_universality;
    }

    public void setGuar_universality(String guar_universality) {
        this.guar_universality = guar_universality;
    }

    public String getSale_state() {
        return sale_state;
    }

    public void setSale_state(String sale_state) {
        this.sale_state = sale_state;
    }

    public String getPrice_volatility() {
        return price_volatility;
    }

    public void setPrice_volatility(String price_volatility) {
        this.price_volatility = price_volatility;
    }

    public String getReceipt_no() {
        return receipt_no;
    }

    public void setReceipt_no(String receipt_no) {
        this.receipt_no = receipt_no;
    }

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public BigDecimal getFreeze_amt() {
        return freeze_amt;
    }

    public void setFreeze_amt(BigDecimal freeze_amt) {
        this.freeze_amt = freeze_amt;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public BigDecimal getStore_term() {
        return store_term;
    }

    public void setStore_term(BigDecimal store_term) {
        this.store_term = store_term;
    }

    public String getDepo_cert_no() {
        return depo_cert_no;
    }

    public void setDepo_cert_no(String depo_cert_no) {
        this.depo_cert_no = depo_cert_no;
    }

    public String getStop_payment_date() {
        return stop_payment_date;
    }

    public void setStop_payment_date(String stop_payment_date) {
        this.stop_payment_date = stop_payment_date;
    }

    public String getOth_bank_name() {
        return oth_bank_name;
    }

    public void setOth_bank_name(String oth_bank_name) {
        this.oth_bank_name = oth_bank_name;
    }

    public String getInterest_acc() {
        return interest_acc;
    }

    public void setInterest_acc(String interest_acc) {
        this.interest_acc = interest_acc;
    }

    public String getAccount_num() {
        return account_num;
    }

    public void setAccount_num(String account_num) {
        this.account_num = account_num;
    }

    public BigDecimal getOrigin_amt() {
        return origin_amt;
    }

    public void setOrigin_amt(BigDecimal origin_amt) {
        this.origin_amt = origin_amt;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getStop_payment_no() {
        return stop_payment_no;
    }

    public void setStop_payment_no(String stop_payment_no) {
        this.stop_payment_no = stop_payment_no;
    }

    public String getStop_payment_end_date() {
        return stop_payment_end_date;
    }

    public void setStop_payment_end_date(String stop_payment_end_date) {
        this.stop_payment_end_date = stop_payment_end_date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    @Override
    public String toString() {
        return "List{" +
                "guar_cus_id='" + guar_cus_id + '\'' +
                ", guar_cus_name='" + guar_cus_name + '\'' +
                ", guar_cert_type='" + guar_cert_type + '\'' +
                ", guar_cert_code='" + guar_cert_code + '\'' +
                ", guar_name='" + guar_name + '\'' +
                ", guar_type_cd='" + guar_type_cd + '\'' +
                ", create_sys='" + create_sys + '\'' +
                ", account_manager='" + account_manager + '\'' +
                ", guar_lastupdate_date='" + guar_lastupdate_date + '\'' +
                ", lastmodify_userid='" + lastmodify_userid + '\'' +
                ", lastmodify_orgid='" + lastmodify_orgid + '\'' +
                ", guar_cus_type='" + guar_cus_type + '\'' +
                ", common_assets_ind='" + common_assets_ind + '\'' +
                ", is_ownership_clear='" + is_ownership_clear + '\'' +
                ", insurance_ind='" + insurance_ind + '\'' +
                ", relation_int='" + relation_int + '\'' +
                ", legal_pri_payment=" + legal_pri_payment +
                ", def_effect_type='" + def_effect_type + '\'' +
                ", contract_justice_ind='" + contract_justice_ind + '\'' +
                ", other_back_guar_ind='" + other_back_guar_ind + '\'' +
                ", if_deal='" + if_deal + '\'' +
                ", guar_borrower_rela='" + guar_borrower_rela + '\'' +
                ", shut_down_conv='" + shut_down_conv + '\'' +
                ", legal_validity='" + legal_validity + '\'' +
                ", guar_universality='" + guar_universality + '\'' +
                ", sale_state='" + sale_state + '\'' +
                ", price_volatility='" + price_volatility + '\'' +
                ", receipt_no='" + receipt_no + '\'' +
                ", account_no='" + account_no + '\'' +
                ", freeze_amt=" + freeze_amt +
                ", start_date='" + start_date + '\'' +
                ", store_term=" + store_term +
                ", depo_cert_no='" + depo_cert_no + '\'' +
                ", stop_payment_date='" + stop_payment_date + '\'' +
                ", oth_bank_name='" + oth_bank_name + '\'' +
                ", interest_acc='" + interest_acc + '\'' +
                ", account_num='" + account_num + '\'' +
                ", origin_amt=" + origin_amt +
                ", cur_type='" + cur_type + '\'' +
                ", end_date='" + end_date + '\'' +
                ", rate=" + rate +
                ", stop_payment_no='" + stop_payment_no + '\'' +
                ", stop_payment_end_date='" + stop_payment_end_date + '\'' +
                ", remark='" + remark + '\'' +
                ", assetNo='" + assetNo + '\'' +
                '}';
    }
}
