package cn.com.yusys.yusp.dto.server.biz.xdxw0029.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "finTotalAmt")
    private BigDecimal finTotalAmt;//融资总额
    @JsonProperty(value = "totalBal")
    private BigDecimal totalBal;//总余额
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式，多个

    public BigDecimal getFinTotalAmt() {
        return finTotalAmt;
    }

    public void setFinTotalAmt(BigDecimal finTotalAmt) {
        this.finTotalAmt = finTotalAmt;
    }

    public BigDecimal getTotalBal() {
        return totalBal;
    }

    public void setTotalBal(BigDecimal totalBal) {
        this.totalBal = totalBal;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    @Override
    public String toString() {
        return "List{" +
                "finTotalAmt=" + finTotalAmt +
                ", totalBal=" + totalBal +
                ", assureMeans='" + assureMeans + '\'' +
                '}';
    }
}
