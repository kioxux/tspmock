package cn.com.yusys.yusp.dto.client.esb.core.ln3244;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：贷款客户经理批量移交
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3244ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lstKehuhaoo")
    private java.util.List<LstKehuhaoo> lstKehuhaoo;//移交客户列表[LIST]

    @JsonProperty(value = "lstHetongbh")
    private java.util.List<LstHetongbh> lstHetongbh;//移交合同列表[LIST]

    @JsonProperty(value = "khjingli")
    private String khjingli;//客户经理

    public List<LstKehuhaoo> getLstKehuhaoo() {
        return lstKehuhaoo;
    }

    public void setLstKehuhaoo(List<LstKehuhaoo> lstKehuhaoo) {
        this.lstKehuhaoo = lstKehuhaoo;
    }

    public List<LstHetongbh> getLstHetongbh() {
        return lstHetongbh;
    }

    public void setLstHetongbh(List<LstHetongbh> lstHetongbh) {
        this.lstHetongbh = lstHetongbh;
    }

    public String getKhjingli() {
        return khjingli;
    }

    public void setKhjingli(String khjingli) {
        this.khjingli = khjingli;
    }

    @Override
    public String toString() {
        return "Ln3244ReqDto{" +
                "lstKehuhaoo=" + lstKehuhaoo +
                ", lstHetongbh=" + lstHetongbh +
                ", khjingli='" + khjingli + '\'' +
                '}';
    }
}
