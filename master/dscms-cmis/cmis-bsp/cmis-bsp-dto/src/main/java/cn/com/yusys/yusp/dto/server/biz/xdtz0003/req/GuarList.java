package cn.com.yusys.yusp.dto.server.biz.xdtz0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/5/7 9:53
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class GuarList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    @Override
    public String toString() {
        return "GuarList{" +
                "assureMeans='" + assureMeans + '\'' +
                '}';
    }
}
