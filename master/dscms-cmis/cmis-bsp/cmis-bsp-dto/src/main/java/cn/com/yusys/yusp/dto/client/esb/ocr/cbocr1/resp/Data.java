package cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 10:15
 * @since 2021/6/4 10:15
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "taskId")
    private String taskId;//返回结果：任务id

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "taskId='" + taskId + '\'' +
                '}';
    }
}
