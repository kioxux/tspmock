package cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：押品信息同步及引入
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdypxxtbyrList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "bus_no")
    private String bus_no;//业务流水号
    @JsonProperty(value = "guar_type_cd")
    private String guar_type_cd;//抵押物类型
    @JsonProperty(value = "guar_type_name")
    private String guar_type_name;//抵押物类型名称
    @JsonProperty(value = "guar_no")
    private String quanzheng_no;//权证编号及其他编号
    @JsonProperty(value = "is_common")
    private String is_common;//是否共有财产
    @JsonProperty(value = "is_ownership_clear")
    private String is_ownership_clear;//是否权属清晰
    @JsonProperty(value = "is_insurance")
    private String is_insurance;//是否需要办理保险
    @JsonProperty(value = "is_relation")
    private String is_relation;//是否实质正相关
    @JsonProperty(value = "legal_pri_payment")
    private BigDecimal legal_pri_payment;//法定优先受偿款（元）
    @JsonProperty(value = "effect_type")
    private String effect_type;//担保生效方式
    @JsonProperty(value = "is_other_back_guar")
    private String is_other_back_guar;//他行是否已设定担保权
    @JsonProperty(value = "is_deal")
    private String is_deal;//是否抵债资产
    @JsonProperty(value = "guar_borrower_rela")
    private String guar_borrower_rela;//抵质押物与借款人相关性
    @JsonProperty(value = "shut_down_conv")
    private String shut_down_conv;//查封便利性
    @JsonProperty(value = "legal_validity")
    private String legal_validity;//法律有效性
    @JsonProperty(value = "universality")
    private String universality;//抵质押品通用性
    @JsonProperty(value = "ability")
    private String ability;//抵质押品变现能力
    @JsonProperty(value = "price_volatility")
    private String price_volatility;//价格波动性
    @JsonProperty(value = "house_ownership")
    private String house_ownership;//抵押住房是否权属人唯一住所
    @JsonProperty(value = "house_pr_desc")
    private BigDecimal house_pr_desc;//房屋产权期限信息（年）
    @JsonProperty(value = "community_name")
    private String community_name;//楼盘（社区）名称
    @JsonProperty(value = "area_location")
    private String area_location;//所属地段
    @JsonProperty(value = "arr_env")
    private String arr_env;//周边环境
    @JsonProperty(value = "f_sty")
    private String f_sty;//套型
    @JsonProperty(value = "house_sta")
    private String house_sta;//房屋状态
    @JsonProperty(value = "house_structure")
    private String house_structure;//房屋结构
    @JsonProperty(value = "ground_structure")
    private String ground_structure;//地面构造
    @JsonProperty(value = "roof_structure")
    private String roof_structure;//屋顶构造
    @JsonProperty(value = "public_facilities")
    private String public_facilities;//公共配套
    @JsonProperty(value = "decoration")
    private String decoration;//装修状况
    @JsonProperty(value = "plane_layout")
    private String plane_layout;//平面布局
    @JsonProperty(value = "orientations")
    private String orientations;//朝向
    @JsonProperty(value = "ventilation_and_lighting")
    private String ventilation_and_lighting;//通风采光
    @JsonProperty(value = "street_situation")
    private String street_situation;//临街状况
    @JsonProperty(value = "street")
    private String street;//街道/村镇/路名
    @JsonProperty(value = "property")
    private String property;//物业情况
    @JsonProperty(value = "usage")
    private String usage;//使用情况
    @JsonProperty(value = "carport_type")
    private String carport_type;//车库/车位类型
    @JsonProperty(value = "is_house_all_pledge")
    private String is_house_all_pledge;//该产权证所属房产是否全部抵押
    @JsonProperty(value = "industry_decelop_model")
    private String industry_decelop_model;//工业房地产开发模式
    @JsonProperty(value = "end_date")
    private String end_date;//竣工日期
    @JsonProperty(value = "located_position")
    private String located_position;//所属地理位置
    @JsonProperty(value = "is_house_land_pledge")
    private String is_house_land_pledge;//房、地是否均已抵押我行
    @JsonProperty(value = "wall_structure")
    private String wall_structure;//墙壁结构
    @JsonProperty(value = "is_full_land")
    private String is_full_land;//是否包含土地
    @JsonProperty(value = "province_cd")
    private String province_cd;//所在/注册省
    @JsonProperty(value = "city_cd")
    private String city_cd;//所在/注册市
    @JsonProperty(value = "county_cd")
    private String county_cd;//所在县（区）
    @JsonProperty(value = "house_type")
    private String house_type;//房产类型
    @JsonProperty(value = "land_explain")
    private String land_explain;//土地说明
    @JsonProperty(value = "land_notinuse_type")
    private String land_notinuse_type;//闲置土地类型
    @JsonProperty(value = "land_p_info")
    private String land_p_info;//土地所在地段情况
    @JsonProperty(value = "parcel_no")
    private String parcel_no;//宗地号
    @JsonProperty(value = "is_land_up")
    private String is_land_up;//是否有土地定着物
    @JsonProperty(value = "land_up_type")
    private String land_up_type;//定着物种类
    @JsonProperty(value = "land_build_amount")
    private BigDecimal land_build_amount;//地上建筑物项数
    @JsonProperty(value = "land_up_all_area")
    private BigDecimal land_up_all_area;//地上定着物总面积（m2)
    @JsonProperty(value = "land_up_ownership_name")
    private String land_up_ownership_name;//定着物所有权人名称
    @JsonProperty(value = "land_up_ownership_scope")
    private String land_up_ownership_scope;//定着物所有权人范围
    @JsonProperty(value = "land_up_explain")
    private String land_up_explain;//地上定着物说明
    @JsonProperty(value = "business_no")
    private String business_no;//买卖合同编号
    @JsonProperty(value = "foeest_right")
    private String foeest_right;//林地名称
    @JsonProperty(value = "foeest_type")
    private String foeest_type;//林种
    @JsonProperty(value = "main_tree_type")
    private String main_tree_type;//主要树种
    @JsonProperty(value = "get_type")
    private String get_type;//取得方式
    @JsonProperty(value = "is_used")
    private String is_used;//一手/二手标识
    @JsonProperty(value = "invoice_no")
    private String invoice_no;//发票编号
    @JsonProperty(value = "invoice_date")
    private String invoice_date;//发票日期
    @JsonProperty(value = "equip_no")
    private String equip_no;//设备铭牌编号
    @JsonProperty(value = "machine_type")
    private String machine_type;//设备类型
    @JsonProperty(value = "machine_code")
    private String machine_code;//设备分类
    @JsonProperty(value = "spec_model")
    private String spec_model;//型号/规格
    @JsonProperty(value = "vehicle_brand")
    private String vehicle_brand;//品牌/厂家/产地
    @JsonProperty(value = "mach_num")
    private BigDecimal mach_num;//设备数量
    @JsonProperty(value = "is_eligible_cerit")
    private String is_eligible_cerit;//是否有产品合格证
    @JsonProperty(value = "factory_date")
    private String factory_date;//出厂日期或报关日期
    @JsonProperty(value = "mature_date")
    private String mature_date;//设计使用到期日期
    @JsonProperty(value = "buy_price")
    private BigDecimal buy_price;//发票金额（元）
    @JsonProperty(value = "land_purp")
    private String land_purp;//土地用途
    @JsonProperty(value = "is_arrearage")
    private String is_arrearage;//是否欠工程款
    @JsonProperty(value = "arrearage_amt")
    private BigDecimal arrearage_amt;//欠工程款金额
    @JsonProperty(value = "address")
    private String address;//地址
    @JsonProperty(value = "gyl_val")
    private BigDecimal gyl_val;//供应链质押价值
    @JsonProperty(value = "is_has_supervision")
    private String is_has_supervision;//是否有监管公司
    @JsonProperty(value = "supervision_company_name")
    private String supervision_company_name;//监管公司名称
    @JsonProperty(value = "supervision_org_code")
    private String supervision_org_code;//监管公司组织机构代码
    @JsonProperty(value = "agreement_begin_date")
    private String agreement_begin_date;//协议生效日
    @JsonProperty(value = "agreement_end_date")
    private String agreement_end_date;//协议到期日
    @JsonProperty(value = "cargo_amt")
    private BigDecimal cargo_amt;//金额
    @JsonProperty(value = "keep_user")
    private String keep_user;//保管人
    @JsonProperty(value = "cargo_class")
    private String cargo_class;//货物详细类型
    @JsonProperty(value = "cargo_name")
    private String cargo_name;//货物名称
    @JsonProperty(value = "cargo_amount")
    private BigDecimal cargo_amount;//货物数量
    @JsonProperty(value = "latest_approved_price")
    private BigDecimal latest_approved_price;//最新核定单价
    @JsonProperty(value = "cargo_measure_unit")
    private String cargo_measure_unit;//货物计量单位
    @JsonProperty(value = "cargo_type")
    private String cargo_type;//货物型号

    public String getBus_no() {
        return bus_no;
    }

    public void setBus_no(String bus_no) {
        this.bus_no = bus_no;
    }

    public String getGuar_type_cd() {
        return guar_type_cd;
    }

    public void setGuar_type_cd(String guar_type_cd) {
        this.guar_type_cd = guar_type_cd;
    }

    public String getGuar_type_name() {
        return guar_type_name;
    }

    public void setGuar_type_name(String guar_type_name) {
        this.guar_type_name = guar_type_name;
    }

    public String getQuanzheng_no() {
        return quanzheng_no;
    }

    public void setQuanzheng_no(String quanzheng_no) {
        this.quanzheng_no = quanzheng_no;
    }

    public String getIs_common() {
        return is_common;
    }

    public void setIs_common(String is_common) {
        this.is_common = is_common;
    }

    public String getIs_ownership_clear() {
        return is_ownership_clear;
    }

    public void setIs_ownership_clear(String is_ownership_clear) {
        this.is_ownership_clear = is_ownership_clear;
    }

    public String getIs_insurance() {
        return is_insurance;
    }

    public void setIs_insurance(String is_insurance) {
        this.is_insurance = is_insurance;
    }

    public String getIs_relation() {
        return is_relation;
    }

    public void setIs_relation(String is_relation) {
        this.is_relation = is_relation;
    }

    public BigDecimal getLegal_pri_payment() {
        return legal_pri_payment;
    }

    public void setLegal_pri_payment(BigDecimal legal_pri_payment) {
        this.legal_pri_payment = legal_pri_payment;
    }

    public String getEffect_type() {
        return effect_type;
    }

    public void setEffect_type(String effect_type) {
        this.effect_type = effect_type;
    }

    public String getIs_other_back_guar() {
        return is_other_back_guar;
    }

    public void setIs_other_back_guar(String is_other_back_guar) {
        this.is_other_back_guar = is_other_back_guar;
    }

    public String getIs_deal() {
        return is_deal;
    }

    public void setIs_deal(String is_deal) {
        this.is_deal = is_deal;
    }

    public String getGuar_borrower_rela() {
        return guar_borrower_rela;
    }

    public void setGuar_borrower_rela(String guar_borrower_rela) {
        this.guar_borrower_rela = guar_borrower_rela;
    }

    public String getShut_down_conv() {
        return shut_down_conv;
    }

    public void setShut_down_conv(String shut_down_conv) {
        this.shut_down_conv = shut_down_conv;
    }

    public String getLegal_validity() {
        return legal_validity;
    }

    public void setLegal_validity(String legal_validity) {
        this.legal_validity = legal_validity;
    }

    public String getUniversality() {
        return universality;
    }

    public void setUniversality(String universality) {
        this.universality = universality;
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public String getPrice_volatility() {
        return price_volatility;
    }

    public void setPrice_volatility(String price_volatility) {
        this.price_volatility = price_volatility;
    }

    public String getHouse_ownership() {
        return house_ownership;
    }

    public void setHouse_ownership(String house_ownership) {
        this.house_ownership = house_ownership;
    }

    public BigDecimal getHouse_pr_desc() {
        return house_pr_desc;
    }

    public void setHouse_pr_desc(BigDecimal house_pr_desc) {
        this.house_pr_desc = house_pr_desc;
    }

    public String getCommunity_name() {
        return community_name;
    }

    public void setCommunity_name(String community_name) {
        this.community_name = community_name;
    }

    public String getArea_location() {
        return area_location;
    }

    public void setArea_location(String area_location) {
        this.area_location = area_location;
    }

    public String getArr_env() {
        return arr_env;
    }

    public void setArr_env(String arr_env) {
        this.arr_env = arr_env;
    }

    public String getF_sty() {
        return f_sty;
    }

    public void setF_sty(String f_sty) {
        this.f_sty = f_sty;
    }

    public String getHouse_sta() {
        return house_sta;
    }

    public void setHouse_sta(String house_sta) {
        this.house_sta = house_sta;
    }

    public String getHouse_structure() {
        return house_structure;
    }

    public void setHouse_structure(String house_structure) {
        this.house_structure = house_structure;
    }

    public String getGround_structure() {
        return ground_structure;
    }

    public void setGround_structure(String ground_structure) {
        this.ground_structure = ground_structure;
    }

    public String getRoof_structure() {
        return roof_structure;
    }

    public void setRoof_structure(String roof_structure) {
        this.roof_structure = roof_structure;
    }

    public String getPublic_facilities() {
        return public_facilities;
    }

    public void setPublic_facilities(String public_facilities) {
        this.public_facilities = public_facilities;
    }

    public String getDecoration() {
        return decoration;
    }

    public void setDecoration(String decoration) {
        this.decoration = decoration;
    }

    public String getPlane_layout() {
        return plane_layout;
    }

    public void setPlane_layout(String plane_layout) {
        this.plane_layout = plane_layout;
    }

    public String getOrientations() {
        return orientations;
    }

    public void setOrientations(String orientations) {
        this.orientations = orientations;
    }

    public String getVentilation_and_lighting() {
        return ventilation_and_lighting;
    }

    public void setVentilation_and_lighting(String ventilation_and_lighting) {
        this.ventilation_and_lighting = ventilation_and_lighting;
    }

    public String getStreet_situation() {
        return street_situation;
    }

    public void setStreet_situation(String street_situation) {
        this.street_situation = street_situation;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getCarport_type() {
        return carport_type;
    }

    public void setCarport_type(String carport_type) {
        this.carport_type = carport_type;
    }

    public String getIs_house_all_pledge() {
        return is_house_all_pledge;
    }

    public void setIs_house_all_pledge(String is_house_all_pledge) {
        this.is_house_all_pledge = is_house_all_pledge;
    }

    public String getIndustry_decelop_model() {
        return industry_decelop_model;
    }

    public void setIndustry_decelop_model(String industry_decelop_model) {
        this.industry_decelop_model = industry_decelop_model;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getLocated_position() {
        return located_position;
    }

    public void setLocated_position(String located_position) {
        this.located_position = located_position;
    }

    public String getIs_house_land_pledge() {
        return is_house_land_pledge;
    }

    public void setIs_house_land_pledge(String is_house_land_pledge) {
        this.is_house_land_pledge = is_house_land_pledge;
    }

    public String getWall_structure() {
        return wall_structure;
    }

    public void setWall_structure(String wall_structure) {
        this.wall_structure = wall_structure;
    }

    public String getIs_full_land() {
        return is_full_land;
    }

    public void setIs_full_land(String is_full_land) {
        this.is_full_land = is_full_land;
    }

    public String getProvince_cd() {
        return province_cd;
    }

    public void setProvince_cd(String province_cd) {
        this.province_cd = province_cd;
    }

    public String getCity_cd() {
        return city_cd;
    }

    public void setCity_cd(String city_cd) {
        this.city_cd = city_cd;
    }

    public String getCounty_cd() {
        return county_cd;
    }

    public void setCounty_cd(String county_cd) {
        this.county_cd = county_cd;
    }

    public String getHouse_type() {
        return house_type;
    }

    public void setHouse_type(String house_type) {
        this.house_type = house_type;
    }

    public String getLand_explain() {
        return land_explain;
    }

    public void setLand_explain(String land_explain) {
        this.land_explain = land_explain;
    }

    public String getLand_notinuse_type() {
        return land_notinuse_type;
    }

    public void setLand_notinuse_type(String land_notinuse_type) {
        this.land_notinuse_type = land_notinuse_type;
    }

    public String getLand_p_info() {
        return land_p_info;
    }

    public void setLand_p_info(String land_p_info) {
        this.land_p_info = land_p_info;
    }

    public String getParcel_no() {
        return parcel_no;
    }

    public void setParcel_no(String parcel_no) {
        this.parcel_no = parcel_no;
    }

    public String getIs_land_up() {
        return is_land_up;
    }

    public void setIs_land_up(String is_land_up) {
        this.is_land_up = is_land_up;
    }

    public String getLand_up_type() {
        return land_up_type;
    }

    public void setLand_up_type(String land_up_type) {
        this.land_up_type = land_up_type;
    }

    public BigDecimal getLand_build_amount() {
        return land_build_amount;
    }

    public void setLand_build_amount(BigDecimal land_build_amount) {
        this.land_build_amount = land_build_amount;
    }

    public BigDecimal getLand_up_all_area() {
        return land_up_all_area;
    }

    public void setLand_up_all_area(BigDecimal land_up_all_area) {
        this.land_up_all_area = land_up_all_area;
    }

    public String getLand_up_ownership_name() {
        return land_up_ownership_name;
    }

    public void setLand_up_ownership_name(String land_up_ownership_name) {
        this.land_up_ownership_name = land_up_ownership_name;
    }

    public String getLand_up_ownership_scope() {
        return land_up_ownership_scope;
    }

    public void setLand_up_ownership_scope(String land_up_ownership_scope) {
        this.land_up_ownership_scope = land_up_ownership_scope;
    }

    public String getLand_up_explain() {
        return land_up_explain;
    }

    public void setLand_up_explain(String land_up_explain) {
        this.land_up_explain = land_up_explain;
    }

    public String getBusiness_no() {
        return business_no;
    }

    public void setBusiness_no(String business_no) {
        this.business_no = business_no;
    }

    public String getFoeest_right() {
        return foeest_right;
    }

    public void setFoeest_right(String foeest_right) {
        this.foeest_right = foeest_right;
    }

    public String getFoeest_type() {
        return foeest_type;
    }

    public void setFoeest_type(String foeest_type) {
        this.foeest_type = foeest_type;
    }

    public String getMain_tree_type() {
        return main_tree_type;
    }

    public void setMain_tree_type(String main_tree_type) {
        this.main_tree_type = main_tree_type;
    }

    public String getGet_type() {
        return get_type;
    }

    public void setGet_type(String get_type) {
        this.get_type = get_type;
    }

    public String getIs_used() {
        return is_used;
    }

    public void setIs_used(String is_used) {
        this.is_used = is_used;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getInvoice_date() {
        return invoice_date;
    }

    public void setInvoice_date(String invoice_date) {
        this.invoice_date = invoice_date;
    }

    public String getEquip_no() {
        return equip_no;
    }

    public void setEquip_no(String equip_no) {
        this.equip_no = equip_no;
    }

    public String getMachine_type() {
        return machine_type;
    }

    public void setMachine_type(String machine_type) {
        this.machine_type = machine_type;
    }

    public String getMachine_code() {
        return machine_code;
    }

    public void setMachine_code(String machine_code) {
        this.machine_code = machine_code;
    }

    public String getSpec_model() {
        return spec_model;
    }

    public void setSpec_model(String spec_model) {
        this.spec_model = spec_model;
    }

    public String getVehicle_brand() {
        return vehicle_brand;
    }

    public void setVehicle_brand(String vehicle_brand) {
        this.vehicle_brand = vehicle_brand;
    }

    public BigDecimal getMach_num() {
        return mach_num;
    }

    public void setMach_num(BigDecimal mach_num) {
        this.mach_num = mach_num;
    }

    public String getIs_eligible_cerit() {
        return is_eligible_cerit;
    }

    public void setIs_eligible_cerit(String is_eligible_cerit) {
        this.is_eligible_cerit = is_eligible_cerit;
    }

    public String getFactory_date() {
        return factory_date;
    }

    public void setFactory_date(String factory_date) {
        this.factory_date = factory_date;
    }

    public String getMature_date() {
        return mature_date;
    }

    public void setMature_date(String mature_date) {
        this.mature_date = mature_date;
    }

    public BigDecimal getBuy_price() {
        return buy_price;
    }

    public void setBuy_price(BigDecimal buy_price) {
        this.buy_price = buy_price;
    }

    public String getLand_purp() {
        return land_purp;
    }

    public void setLand_purp(String land_purp) {
        this.land_purp = land_purp;
    }

    public String getIs_arrearage() {
        return is_arrearage;
    }

    public void setIs_arrearage(String is_arrearage) {
        this.is_arrearage = is_arrearage;
    }

    public BigDecimal getArrearage_amt() {
        return arrearage_amt;
    }

    public void setArrearage_amt(BigDecimal arrearage_amt) {
        this.arrearage_amt = arrearage_amt;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getGyl_val() {
        return gyl_val;
    }

    public void setGyl_val(BigDecimal gyl_val) {
        this.gyl_val = gyl_val;
    }

    public String getIs_has_supervision() {
        return is_has_supervision;
    }

    public void setIs_has_supervision(String is_has_supervision) {
        this.is_has_supervision = is_has_supervision;
    }

    public String getSupervision_company_name() {
        return supervision_company_name;
    }

    public void setSupervision_company_name(String supervision_company_name) {
        this.supervision_company_name = supervision_company_name;
    }

    public String getSupervision_org_code() {
        return supervision_org_code;
    }

    public void setSupervision_org_code(String supervision_org_code) {
        this.supervision_org_code = supervision_org_code;
    }

    public String getAgreement_begin_date() {
        return agreement_begin_date;
    }

    public void setAgreement_begin_date(String agreement_begin_date) {
        this.agreement_begin_date = agreement_begin_date;
    }

    public String getAgreement_end_date() {
        return agreement_end_date;
    }

    public void setAgreement_end_date(String agreement_end_date) {
        this.agreement_end_date = agreement_end_date;
    }

    public BigDecimal getCargo_amt() {
        return cargo_amt;
    }

    public void setCargo_amt(BigDecimal cargo_amt) {
        this.cargo_amt = cargo_amt;
    }

    public String getKeep_user() {
        return keep_user;
    }

    public void setKeep_user(String keep_user) {
        this.keep_user = keep_user;
    }

    public String getCargo_class() {
        return cargo_class;
    }

    public void setCargo_class(String cargo_class) {
        this.cargo_class = cargo_class;
    }

    public String getCargo_name() {
        return cargo_name;
    }

    public void setCargo_name(String cargo_name) {
        this.cargo_name = cargo_name;
    }

    public BigDecimal getCargo_amount() {
        return cargo_amount;
    }

    public void setCargo_amount(BigDecimal cargo_amount) {
        this.cargo_amount = cargo_amount;
    }

    public BigDecimal getLatest_approved_price() {
        return latest_approved_price;
    }

    public void setLatest_approved_price(BigDecimal latest_approved_price) {
        this.latest_approved_price = latest_approved_price;
    }

    public String getCargo_measure_unit() {
        return cargo_measure_unit;
    }

    public void setCargo_measure_unit(String cargo_measure_unit) {
        this.cargo_measure_unit = cargo_measure_unit;
    }

    public String getCargo_type() {
        return cargo_type;
    }

    public void setCargo_type(String cargo_type) {
        this.cargo_type = cargo_type;
    }

    @Override
    public String toString() {
        return "XdypxxtbyrList{" +
                "bus_no='" + bus_no + '\'' +
                ", guar_type_cd='" + guar_type_cd + '\'' +
                ", guar_type_name='" + guar_type_name + '\'' +
                ", quanzheng_no='" + quanzheng_no + '\'' +
                ", is_common='" + is_common + '\'' +
                ", is_ownership_clear='" + is_ownership_clear + '\'' +
                ", is_insurance='" + is_insurance + '\'' +
                ", is_relation='" + is_relation + '\'' +
                ", legal_pri_payment=" + legal_pri_payment +
                ", effect_type='" + effect_type + '\'' +
                ", is_other_back_guar='" + is_other_back_guar + '\'' +
                ", is_deal='" + is_deal + '\'' +
                ", guar_borrower_rela='" + guar_borrower_rela + '\'' +
                ", shut_down_conv='" + shut_down_conv + '\'' +
                ", legal_validity='" + legal_validity + '\'' +
                ", universality='" + universality + '\'' +
                ", ability='" + ability + '\'' +
                ", price_volatility='" + price_volatility + '\'' +
                ", house_ownership='" + house_ownership + '\'' +
                ", house_pr_desc=" + house_pr_desc +
                ", community_name='" + community_name + '\'' +
                ", area_location='" + area_location + '\'' +
                ", arr_env='" + arr_env + '\'' +
                ", f_sty='" + f_sty + '\'' +
                ", house_sta='" + house_sta + '\'' +
                ", house_structure='" + house_structure + '\'' +
                ", ground_structure='" + ground_structure + '\'' +
                ", roof_structure='" + roof_structure + '\'' +
                ", public_facilities='" + public_facilities + '\'' +
                ", decoration='" + decoration + '\'' +
                ", plane_layout='" + plane_layout + '\'' +
                ", orientations='" + orientations + '\'' +
                ", ventilation_and_lighting='" + ventilation_and_lighting + '\'' +
                ", street_situation='" + street_situation + '\'' +
                ", street='" + street + '\'' +
                ", property='" + property + '\'' +
                ", usage='" + usage + '\'' +
                ", carport_type='" + carport_type + '\'' +
                ", is_house_all_pledge='" + is_house_all_pledge + '\'' +
                ", industry_decelop_model='" + industry_decelop_model + '\'' +
                ", end_date='" + end_date + '\'' +
                ", located_position='" + located_position + '\'' +
                ", is_house_land_pledge='" + is_house_land_pledge + '\'' +
                ", wall_structure='" + wall_structure + '\'' +
                ", is_full_land='" + is_full_land + '\'' +
                ", province_cd='" + province_cd + '\'' +
                ", city_cd='" + city_cd + '\'' +
                ", county_cd='" + county_cd + '\'' +
                ", house_type='" + house_type + '\'' +
                ", land_explain='" + land_explain + '\'' +
                ", land_notinuse_type='" + land_notinuse_type + '\'' +
                ", land_p_info='" + land_p_info + '\'' +
                ", parcel_no='" + parcel_no + '\'' +
                ", is_land_up='" + is_land_up + '\'' +
                ", land_up_type='" + land_up_type + '\'' +
                ", land_build_amount=" + land_build_amount +
                ", land_up_all_area=" + land_up_all_area +
                ", land_up_ownership_name='" + land_up_ownership_name + '\'' +
                ", land_up_ownership_scope='" + land_up_ownership_scope + '\'' +
                ", land_up_explain='" + land_up_explain + '\'' +
                ", business_no='" + business_no + '\'' +
                ", foeest_right='" + foeest_right + '\'' +
                ", foeest_type='" + foeest_type + '\'' +
                ", main_tree_type='" + main_tree_type + '\'' +
                ", get_type='" + get_type + '\'' +
                ", is_used='" + is_used + '\'' +
                ", invoice_no='" + invoice_no + '\'' +
                ", invoice_date='" + invoice_date + '\'' +
                ", equip_no='" + equip_no + '\'' +
                ", machine_type='" + machine_type + '\'' +
                ", machine_code='" + machine_code + '\'' +
                ", spec_model='" + spec_model + '\'' +
                ", vehicle_brand='" + vehicle_brand + '\'' +
                ", mach_num=" + mach_num +
                ", is_eligible_cerit='" + is_eligible_cerit + '\'' +
                ", factory_date='" + factory_date + '\'' +
                ", mature_date='" + mature_date + '\'' +
                ", buy_price=" + buy_price +
                ", land_purp='" + land_purp + '\'' +
                ", is_arrearage='" + is_arrearage + '\'' +
                ", arrearage_amt=" + arrearage_amt +
                ", address='" + address + '\'' +
                ", gyl_val=" + gyl_val +
                ", is_has_supervision='" + is_has_supervision + '\'' +
                ", supervision_company_name='" + supervision_company_name + '\'' +
                ", supervision_org_code='" + supervision_org_code + '\'' +
                ", agreement_begin_date='" + agreement_begin_date + '\'' +
                ", agreement_end_date='" + agreement_end_date + '\'' +
                ", cargo_amt=" + cargo_amt +
                ", keep_user='" + keep_user + '\'' +
                ", cargo_class='" + cargo_class + '\'' +
                ", cargo_name='" + cargo_name + '\'' +
                ", cargo_amount=" + cargo_amount +
                ", latest_approved_price=" + latest_approved_price +
                ", cargo_measure_unit='" + cargo_measure_unit + '\'' +
                ", cargo_type='" + cargo_type + '\'' +
                '}';
    }
}
