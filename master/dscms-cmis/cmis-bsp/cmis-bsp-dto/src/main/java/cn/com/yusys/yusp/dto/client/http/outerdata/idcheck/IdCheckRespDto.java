package cn.com.yusys.yusp.dto.client.http.outerdata.idcheck;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 响应DTO：个人身份核查
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class IdCheckRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "code")
    private String code;//返回码              ,00： 查询成功，其他：查询失败 失败原因参考erortx
    @JsonProperty(value = "resultCode")
    private String resultCode;//结果码              ,0000： 查询成功，其他：查询失败 失败原因参考msg
    @JsonProperty(value = "reason")
    private String reason;//结果码说明            ,
    @JsonProperty(value = "checkresult")
    private String checkresult;//身份核验结果           ,身份核验结果 00 一致 01 不一致
    @JsonProperty(value = "verify_authorityresult")
    private String verify_authorityresult;//发证机关核验结果         ,0 一致 1 不一致 3 无记录
    @JsonProperty(value = "verify_expirybeginresult")
    private String verify_expirybeginresult;//有效期始核验结果         ,0 一致 1 不一致 3 无记录
    @JsonProperty(value = "verify_expiryendresult")
    private String verify_expiryendresult;//有效期止核验结果

    @JsonProperty(value = "get_type")
    private String get_type;
    @JsonProperty(value = "responsetime")
    private String responsetime;
    @JsonProperty(value = "sessionid")
    private String sessionid;
    @JsonProperty(value = "responseid")
    private String responseid;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCheckresult() {
        return checkresult;
    }

    public void setCheckresult(String checkresult) {
        this.checkresult = checkresult;
    }

    public String getVerify_authorityresult() {
        return verify_authorityresult;
    }

    public void setVerify_authorityresult(String verify_authorityresult) {
        this.verify_authorityresult = verify_authorityresult;
    }

    public String getVerify_expirybeginresult() {
        return verify_expirybeginresult;
    }

    public void setVerify_expirybeginresult(String verify_expirybeginresult) {
        this.verify_expirybeginresult = verify_expirybeginresult;
    }

    public String getVerify_expiryendresult() {
        return verify_expiryendresult;
    }

    public void setVerify_expiryendresult(String verify_expiryendresult) {
        this.verify_expiryendresult = verify_expiryendresult;
    }

    public String getGet_type() {
        return get_type;
    }

    public void setGet_type(String get_type) {
        this.get_type = get_type;
    }

    public String getResponsetime() {
        return responsetime;
    }

    public void setResponsetime(String responsetime) {
        this.responsetime = responsetime;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getResponseid() {
        return responseid;
    }

    public void setResponseid(String responseid) {
        this.responseid = responseid;
    }

    @Override
    public String toString() {
        return "IdCheckRespDto{" +
                "code='" + code + '\'' +
                ", resultCode='" + resultCode + '\'' +
                ", reason='" + reason + '\'' +
                ", checkresult='" + checkresult + '\'' +
                ", verify_authorityresult='" + verify_authorityresult + '\'' +
                ", verify_expirybeginresult='" + verify_expirybeginresult + '\'' +
                ", verify_expiryendresult='" + verify_expiryendresult + '\'' +
                ", get_type='" + get_type + '\'' +
                ", responsetime='" + responsetime + '\'' +
                ", sessionid='" + sessionid + '\'' +
                ", responseid='" + responseid + '\'' +
                '}';
    }
}
