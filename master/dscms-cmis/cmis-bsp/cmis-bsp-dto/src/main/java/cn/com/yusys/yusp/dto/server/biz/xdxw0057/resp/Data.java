package cn.com.yusys.yusp.dto.server.biz.xdxw0057.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：根据核心客户号查询经营性贷款批复额度
 * @Author zhangpeng
 * @Date 2021/4/24 17:08
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contAmt=" + contAmt +
                '}';
    }
}
