package cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷客户核心业绩统计查询列表
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdhxQueryTotalListReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    @JsonProperty(value = "loginId")
    private String loginId;//登录用户
    @JsonProperty(value = "loginBrId")
    private String loginBrId;//登录机构

    @JsonProperty(value = "size")
    private Integer size;
    @JsonProperty(value = "from")
    private Integer from;

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginBrId() {
        return loginBrId;
    }

    public void setLoginBrId(String loginBrId) {
        this.loginBrId = loginBrId;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "XdhxQueryTotalListReqDto{" +
                "queryType='" + queryType + '\'' +
                ", loginId='" + loginId + '\'' +
                ", loginBrId='" + loginBrId + '\'' +
                ", size=" + size +
                ", from=" + from +
                '}';
    }
}
