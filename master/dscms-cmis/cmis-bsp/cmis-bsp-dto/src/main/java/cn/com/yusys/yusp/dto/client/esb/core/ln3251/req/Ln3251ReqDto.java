package cn.com.yusys.yusp.dto.client.esb.core.ln3251.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款指定日期利息试算
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3251ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;// 贷款借据号
    @JsonProperty(value = "qishibis")
    private String qishibis;// 起始笔数
    @JsonProperty(value = "chxunbis")
    private String chxunbis;// 查询笔数

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getQishibis() {
        return qishibis;
    }

    public void setQishibis(String qishibis) {
        this.qishibis = qishibis;
    }

    public String getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(String chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Ln3251ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", qishibis='" + qishibis + '\'' +
                ", chxunbis='" + chxunbis + '\'' +
                '}';
    }
}
