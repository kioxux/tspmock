package cn.com.yusys.yusp.dto.server.biz.xdxw0062.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "businessId")
    private String businessId;//业务申请编号
    @JsonProperty(value = "indname")
    private String indname;//申请人姓名
    @JsonProperty(value = "indcertid")
    private String indcertid;//申请人证件号
    @JsonProperty(value = "entname")
    private String entname;//企业名称
    @JsonProperty(value = "indphoneid")
    private String indphoneid;//申请人手机号
    @JsonProperty(value = "address")
    private String address;//生产经营地址
    @JsonProperty(value = "hydm")
    private String hydm;//行业代码
    @JsonProperty(value = "sfnh")
    private String sfnh;//是否农户
    @JsonProperty(value = "ent_cert_type")
    private String ent_cert_type;//企业证件类型
    @JsonProperty(value = "ent_cert_id")
    private String ent_cert_id;//企业证件号
    @JsonProperty(value = "cus_mgr_id")
    private String cus_mgr_id;//营销客户经理编号
    @JsonProperty(value = "is_scene_check")
    private String is_scene_check;//是否现场核验
    @JsonProperty(value = "check_reason")
    private String check_reason;//核验原因
    @JsonProperty(value = "live_addr")
    private String live_addr;//居住地址
    @JsonProperty(value = "final_lmt")
    private BigDecimal final_lmt;//终审额度
    @JsonProperty(value = "interest_rate")
    private BigDecimal interest_rate;//利率
    @JsonProperty(value = "indcredit005")
    private BigDecimal indcredit005;//累计逾期次数
    @JsonProperty(value = "mdzrcb_icr0007")
    private BigDecimal mdzrcb_icr0007;//未结清贷款笔数
    @JsonProperty(value = "mdzrcb_icr0010")
    private BigDecimal mdzrcb_icr0010;//未销户信用卡授信总额
    @JsonProperty(value = "mdzrcb_icr0011")
    private BigDecimal mdzrcb_icr0011;//未结清贷款余额
    @JsonProperty(value = "mdzrcb_icr0015")
    private BigDecimal mdzrcb_icr0015;//近6个月信用卡平均支用比例
    @JsonProperty(value = "mdzrcb_icr0017")
    private BigDecimal mdzrcb_icr0017;//最近2年贷款审批查询机构数
    @JsonProperty(value = "mdzrcb_icr0019")
    private BigDecimal mdzrcb_icr0019;//未结清消费金融公司贷款笔数
    @JsonProperty(value = "mdzrcb_icr0020")
    private BigDecimal mdzrcb_icr0020;//未结清对外贷款担保笔数
    @JsonProperty(value = "mdzrcb_ecr0005")
    private BigDecimal mdzrcb_ecr0005;//企业未结清业务机构数
    @JsonProperty(value = "mdzrcb_ecr0004")
    private BigDecimal mdzrcb_ecr0004;//企业未结清信贷余额
    @JsonProperty(value = "tax003")
    private String tax003;//完整纳税月份数
    @JsonProperty(value = "mdzrcb_tax0008")
    private String mdzrcb_tax0008;//当前税务信用评级
    @JsonProperty(value = "mdzrcb_tax0005")
    private BigDecimal mdzrcb_tax0005;//近1年销售收入
    @JsonProperty(value = "mdzrcb_tax0007")
    private BigDecimal mdzrcb_tax0007;//近1年综合应纳税额
    @JsonProperty(value = "continuedloan")
    private String continuedloan;//是否续贷
    @JsonProperty(value = "model_preli_result")
    private String model_preli_result;//模型初步结果
    @JsonProperty(value = "applyterm")
    private BigDecimal applyterm;//申请期限
    @JsonProperty(value = "is_tqsd")
    private String is_tqsd;// 是否提前申贷
    @JsonProperty(value = "cn_whb")
    private String cn_whb;// 是否可无还本续贷
    @JsonProperty(value = "whbmodelrat")
    private BigDecimal whbmodelrat;// 无还本续贷模型金额
    @JsonProperty(value = "whbmodelamt")
    private BigDecimal whbmodelamt;// 无还本续贷模型利率
    @JsonProperty(value = "listSerno")
    private String listSerno;// 白名单流水号

    public String getIs_tqsd() {
        return is_tqsd;
    }

    public void setIs_tqsd(String is_tqsd) {
        this.is_tqsd = is_tqsd;
    }

    public String getCn_whb() {
        return cn_whb;
    }

    public void setCn_whb(String cn_whb) {
        this.cn_whb = cn_whb;
    }

    public BigDecimal getWhbmodelrat() {
        return whbmodelrat;
    }

    public void setWhbmodelrat(BigDecimal whbmodelrat) {
        this.whbmodelrat = whbmodelrat;
    }

    public BigDecimal getWhbmodelamt() {
        return whbmodelamt;
    }

    public void setWhbmodelamt(BigDecimal whbmodelamt) {
        this.whbmodelamt = whbmodelamt;
    }

    public String getListSerno() {
        return listSerno;
    }

    public void setListSerno(String listSerno) {
        this.listSerno = listSerno;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getIndname() {
        return indname;
    }

    public void setIndname(String indname) {
        this.indname = indname;
    }

    public String getIndcertid() {
        return indcertid;
    }

    public void setIndcertid(String indcertid) {
        this.indcertid = indcertid;
    }

    public String getEntname() {
        return entname;
    }

    public void setEntname(String entname) {
        this.entname = entname;
    }

    public String getIndphoneid() {
        return indphoneid;
    }

    public void setIndphoneid(String indphoneid) {
        this.indphoneid = indphoneid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHydm() {
        return hydm;
    }

    public void setHydm(String hydm) {
        this.hydm = hydm;
    }

    public String getSfnh() {
        return sfnh;
    }

    public void setSfnh(String sfnh) {
        this.sfnh = sfnh;
    }

    public String getEnt_cert_type() {
        return ent_cert_type;
    }

    public void setEnt_cert_type(String ent_cert_type) {
        this.ent_cert_type = ent_cert_type;
    }

    public String getEnt_cert_id() {
        return ent_cert_id;
    }

    public void setEnt_cert_id(String ent_cert_id) {
        this.ent_cert_id = ent_cert_id;
    }

    public String getCus_mgr_id() {
        return cus_mgr_id;
    }

    public void setCus_mgr_id(String cus_mgr_id) {
        this.cus_mgr_id = cus_mgr_id;
    }

    public String getIs_scene_check() {
        return is_scene_check;
    }

    public void setIs_scene_check(String is_scene_check) {
        this.is_scene_check = is_scene_check;
    }

    public String getCheck_reason() {
        return check_reason;
    }

    public void setCheck_reason(String check_reason) {
        this.check_reason = check_reason;
    }

    public String getLive_addr() {
        return live_addr;
    }

    public void setLive_addr(String live_addr) {
        this.live_addr = live_addr;
    }

    public BigDecimal getFinal_lmt() {
        return final_lmt;
    }

    public void setFinal_lmt(BigDecimal final_lmt) {
        this.final_lmt = final_lmt;
    }

    public BigDecimal getInterest_rate() {
        return interest_rate;
    }

    public void setInterest_rate(BigDecimal interest_rate) {
        this.interest_rate = interest_rate;
    }

    public BigDecimal getIndcredit005() {
        return indcredit005;
    }

    public void setIndcredit005(BigDecimal indcredit005) {
        this.indcredit005 = indcredit005;
    }

    public BigDecimal getMdzrcb_icr0007() {
        return mdzrcb_icr0007;
    }

    public void setMdzrcb_icr0007(BigDecimal mdzrcb_icr0007) {
        this.mdzrcb_icr0007 = mdzrcb_icr0007;
    }

    public BigDecimal getMdzrcb_icr0010() {
        return mdzrcb_icr0010;
    }

    public void setMdzrcb_icr0010(BigDecimal mdzrcb_icr0010) {
        this.mdzrcb_icr0010 = mdzrcb_icr0010;
    }

    public BigDecimal getMdzrcb_icr0011() {
        return mdzrcb_icr0011;
    }

    public void setMdzrcb_icr0011(BigDecimal mdzrcb_icr0011) {
        this.mdzrcb_icr0011 = mdzrcb_icr0011;
    }

    public BigDecimal getMdzrcb_icr0015() {
        return mdzrcb_icr0015;
    }

    public void setMdzrcb_icr0015(BigDecimal mdzrcb_icr0015) {
        this.mdzrcb_icr0015 = mdzrcb_icr0015;
    }

    public BigDecimal getMdzrcb_icr0017() {
        return mdzrcb_icr0017;
    }

    public void setMdzrcb_icr0017(BigDecimal mdzrcb_icr0017) {
        this.mdzrcb_icr0017 = mdzrcb_icr0017;
    }

    public BigDecimal getMdzrcb_icr0019() {
        return mdzrcb_icr0019;
    }

    public void setMdzrcb_icr0019(BigDecimal mdzrcb_icr0019) {
        this.mdzrcb_icr0019 = mdzrcb_icr0019;
    }

    public BigDecimal getMdzrcb_icr0020() {
        return mdzrcb_icr0020;
    }

    public void setMdzrcb_icr0020(BigDecimal mdzrcb_icr0020) {
        this.mdzrcb_icr0020 = mdzrcb_icr0020;
    }

    public BigDecimal getMdzrcb_ecr0005() {
        return mdzrcb_ecr0005;
    }

    public void setMdzrcb_ecr0005(BigDecimal mdzrcb_ecr0005) {
        this.mdzrcb_ecr0005 = mdzrcb_ecr0005;
    }

    public BigDecimal getMdzrcb_ecr0004() {
        return mdzrcb_ecr0004;
    }

    public void setMdzrcb_ecr0004(BigDecimal mdzrcb_ecr0004) {
        this.mdzrcb_ecr0004 = mdzrcb_ecr0004;
    }

    public String getTax003() {
        return tax003;
    }

    public void setTax003(String tax003) {
        this.tax003 = tax003;
    }

    public String getMdzrcb_tax0008() {
        return mdzrcb_tax0008;
    }

    public void setMdzrcb_tax0008(String mdzrcb_tax0008) {
        this.mdzrcb_tax0008 = mdzrcb_tax0008;
    }

    public BigDecimal getMdzrcb_tax0005() {
        return mdzrcb_tax0005;
    }

    public void setMdzrcb_tax0005(BigDecimal mdzrcb_tax0005) {
        this.mdzrcb_tax0005 = mdzrcb_tax0005;
    }

    public BigDecimal getMdzrcb_tax0007() {
        return mdzrcb_tax0007;
    }

    public void setMdzrcb_tax0007(BigDecimal mdzrcb_tax0007) {
        this.mdzrcb_tax0007 = mdzrcb_tax0007;
    }

    public String getContinuedloan() {
        return continuedloan;
    }

    public void setContinuedloan(String continuedloan) {
        this.continuedloan = continuedloan;
    }

    public String getModel_preli_result() {
        return model_preli_result;
    }

    public void setModel_preli_result(String model_preli_result) {
        this.model_preli_result = model_preli_result;
    }

    public BigDecimal getApplyterm() {
        return applyterm;
    }

    public void setApplyterm(BigDecimal applyterm) {
        this.applyterm = applyterm;
    }

    @Override
    public String toString() {
        return "Data{" +
                "businessId='" + businessId + '\'' +
                "indname='" + indname + '\'' +
                "indcertid='" + indcertid + '\'' +
                "entname='" + entname + '\'' +
                "indphoneid='" + indphoneid + '\'' +
                "address='" + address + '\'' +
                "hydm='" + hydm + '\'' +
                "sfnh='" + sfnh + '\'' +
                "ent_cert_type='" + ent_cert_type + '\'' +
                "ent_cert_id='" + ent_cert_id + '\'' +
                "cus_mgr_id='" + cus_mgr_id + '\'' +
                "is_scene_check='" + is_scene_check + '\'' +
                "check_reason='" + check_reason + '\'' +
                "live_addr='" + live_addr + '\'' +
                "final_lmt='" + final_lmt + '\'' +
                "interest_rate='" + interest_rate + '\'' +
                "indcredit005='" + indcredit005 + '\'' +
                "mdzrcb_icr0007='" + mdzrcb_icr0007 + '\'' +
                "mdzrcb_icr0010='" + mdzrcb_icr0010 + '\'' +
                "mdzrcb_icr0011='" + mdzrcb_icr0011 + '\'' +
                "mdzrcb_icr0015='" + mdzrcb_icr0015 + '\'' +
                "mdzrcb_icr0017='" + mdzrcb_icr0017 + '\'' +
                "mdzrcb_icr0019='" + mdzrcb_icr0019 + '\'' +
                "mdzrcb_icr0020='" + mdzrcb_icr0020 + '\'' +
                "mdzrcb_ecr0005='" + mdzrcb_ecr0005 + '\'' +
                "mdzrcb_ecr0004='" + mdzrcb_ecr0004 + '\'' +
                "tax003='" + tax003 + '\'' +
                "mdzrcb_tax0008='" + mdzrcb_tax0008 + '\'' +
                "mdzrcb_tax0005='" + mdzrcb_tax0005 + '\'' +
                "mdzrcb_tax0007='" + mdzrcb_tax0007 + '\'' +
                "continuedloan='" + continuedloan + '\'' +
                "model_preli_result='" + model_preli_result + '\'' +
                "applyterm='" + applyterm + '\'' +
                '}';
    }
}
