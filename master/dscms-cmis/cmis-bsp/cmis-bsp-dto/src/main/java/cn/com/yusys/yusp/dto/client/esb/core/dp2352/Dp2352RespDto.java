package cn.com.yusys.yusp.dto.client.esb.core.dp2352;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：组合账户子账户开立
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2352RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh;//子账户序号
    @JsonProperty(value = "kehuzhmc")
    private String kehuzhmc;//客户账户名称
    @JsonProperty(value = "zhhuzwmc")
    private String zhhuzwmc;//账户名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpshm")
    private String chanpshm;//产品说明
    @JsonProperty(value = "zhhufenl")
    private String zhhufenl;//账户分类
    @JsonProperty(value = "zhshuxin")
    private String zhshuxin;//账户属性
    @JsonProperty(value = "guiylius")
    private String guiylius;//柜员流水号
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//货币代号
    @JsonProperty(value = "chaohubz")
    private String chaohubz;//账户钞汇标志
    @JsonProperty(value = "keschpmc")
    private String keschpmc;//可售产品名称
    @JsonProperty(value = "jifeibzh")
    private String jifeibzh;//计费标志
    @JsonProperty(value = "shijlilv")
    private BigDecimal shijlilv;//实际利率
    @JsonProperty(value = "tduifwei")
    private String tduifwei;//通兑范围
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn;//支付条件
    @JsonProperty(value = "lyzhipbz")
    private String lyzhipbz;//领用支票标志
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源
    @JsonProperty(value = "duifkhzh")
    private String duifkhzh;//对方客户账号
    @JsonProperty(value = "zhhaoxh1")
    private String zhhaoxh1;//子账户序号
    @JsonProperty(value = "lyzhumcc")
    private String lyzhumcc;//来源账户名称
    @JsonProperty(value = "cunqiiii")
    private String cunqiiii;//存期
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "doqiriqi")
    private String doqiriqi;//到期日期
    @JsonProperty(value = "jineeeee")
    private BigDecimal jineeeee;//金额
    @JsonProperty(value = "zhcunfsh")
    private String zhcunfsh;//转存方式
    @JsonProperty(value = "zcuncqii")
    private String zcuncqii;//转存存期
    @JsonProperty(value = "pngzzlei")
    private String pngzzlei;//凭证种类
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao;//凭证序号
    @JsonProperty(value = "mobmingc")
    private String mobmingc;//组合账户模板名称
    @JsonProperty(value = "aiostype")
    private String aiostype;//组合账户形态
    @JsonProperty(value = "sfkldqsh")
    private String sfkldqsh;//是否开立待清算户


    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpshm() {
        return chanpshm;
    }

    public void setChanpshm(String chanpshm) {
        this.chanpshm = chanpshm;
    }

    public String getZhhufenl() {
        return zhhufenl;
    }

    public void setZhhufenl(String zhhufenl) {
        this.zhhufenl = zhhufenl;
    }

    public String getZhshuxin() {
        return zhshuxin;
    }

    public void setZhshuxin(String zhshuxin) {
        this.zhshuxin = zhshuxin;
    }

    public String getGuiylius() {
        return guiylius;
    }

    public void setGuiylius(String guiylius) {
        this.guiylius = guiylius;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getKeschpmc() {
        return keschpmc;
    }

    public void setKeschpmc(String keschpmc) {
        this.keschpmc = keschpmc;
    }

    public String getJifeibzh() {
        return jifeibzh;
    }

    public void setJifeibzh(String jifeibzh) {
        this.jifeibzh = jifeibzh;
    }

    public BigDecimal getShijlilv() {
        return shijlilv;
    }

    public void setShijlilv(BigDecimal shijlilv) {
        this.shijlilv = shijlilv;
    }

    public String getTduifwei() {
        return tduifwei;
    }

    public void setTduifwei(String tduifwei) {
        this.tduifwei = tduifwei;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getLyzhipbz() {
        return lyzhipbz;
    }

    public void setLyzhipbz(String lyzhipbz) {
        this.lyzhipbz = lyzhipbz;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public String getDuifkhzh() {
        return duifkhzh;
    }

    public void setDuifkhzh(String duifkhzh) {
        this.duifkhzh = duifkhzh;
    }

    public String getZhhaoxh1() {
        return zhhaoxh1;
    }

    public void setZhhaoxh1(String zhhaoxh1) {
        this.zhhaoxh1 = zhhaoxh1;
    }

    public String getLyzhumcc() {
        return lyzhumcc;
    }

    public void setLyzhumcc(String lyzhumcc) {
        this.lyzhumcc = lyzhumcc;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public BigDecimal getJineeeee() {
        return jineeeee;
    }

    public void setJineeeee(BigDecimal jineeeee) {
        this.jineeeee = jineeeee;
    }

    public String getZhcunfsh() {
        return zhcunfsh;
    }

    public void setZhcunfsh(String zhcunfsh) {
        this.zhcunfsh = zhcunfsh;
    }

    public String getZcuncqii() {
        return zcuncqii;
    }

    public void setZcuncqii(String zcuncqii) {
        this.zcuncqii = zcuncqii;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getMobmingc() {
        return mobmingc;
    }

    public void setMobmingc(String mobmingc) {
        this.mobmingc = mobmingc;
    }

    public String getAiostype() {
        return aiostype;
    }

    public void setAiostype(String aiostype) {
        this.aiostype = aiostype;
    }

    public String getSfkldqsh() {
        return sfkldqsh;
    }

    public void setSfkldqsh(String sfkldqsh) {
        this.sfkldqsh = sfkldqsh;
    }

    @Override
    public String toString() {
        return "Dp2352RespDto{" +
                "kehuzhao='" + kehuzhao + '\'' +
                "zhhaoxuh='" + zhhaoxuh + '\'' +
                "kehuzhmc='" + kehuzhmc + '\'' +
                "zhhuzwmc='" + zhhuzwmc + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpshm='" + chanpshm + '\'' +
                "zhhufenl='" + zhhufenl + '\'' +
                "zhshuxin='" + zhshuxin + '\'' +
                "guiylius='" + guiylius + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                "chaohubz='" + chaohubz + '\'' +
                "keschpmc='" + keschpmc + '\'' +
                "jifeibzh='" + jifeibzh + '\'' +
                "shijlilv='" + shijlilv + '\'' +
                "tduifwei='" + tduifwei + '\'' +
                "zhfutojn='" + zhfutojn + '\'' +
                "lyzhipbz='" + lyzhipbz + '\'' +
                "zijnlaiy='" + zijnlaiy + '\'' +
                "duifkhzh='" + duifkhzh + '\'' +
                "zhhaoxh1='" + zhhaoxh1 + '\'' +
                "lyzhumcc='" + lyzhumcc + '\'' +
                "cunqiiii='" + cunqiiii + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "doqiriqi='" + doqiriqi + '\'' +
                "jineeeee='" + jineeeee + '\'' +
                "zhcunfsh='" + zhcunfsh + '\'' +
                "zcuncqii='" + zcuncqii + '\'' +
                "pngzzlei='" + pngzzlei + '\'' +
                "pngzxhao='" + pngzxhao + '\'' +
                "mobmingc='" + mobmingc + '\'' +
                "aiostype='" + aiostype + '\'' +
                "sfkldqsh='" + sfkldqsh + '\'' +
                '}';
    }
}  
