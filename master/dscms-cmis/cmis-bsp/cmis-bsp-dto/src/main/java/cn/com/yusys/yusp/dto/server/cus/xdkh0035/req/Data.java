package cn.com.yusys.yusp.dto.server.cus.xdkh0035.req;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：违约认定、重生认定评级信息同步
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "bankCreditLvl")
    private String bankCreditLvl;//本行即期信用等级
    @JsonProperty(value = "avgBreachProblyPD")
    private BigDecimal avgBreachProblyPD;//平均违约概率PD
    @JsonProperty(value = "evalStartDate")
    private String evalStartDate;//评级生效日
    @JsonProperty(value = "evalEndDate")
    private String evalEndDate;//评级到期日
    @JsonProperty(value = "dtghFlag")
    private String dtghFlag;//区分标识

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getBankCreditLvl() {
        return bankCreditLvl;
    }

    public void setBankCreditLvl(String bankCreditLvl) {
        this.bankCreditLvl = bankCreditLvl;
    }

    public BigDecimal getAvgBreachProblyPD() {
        return avgBreachProblyPD;
    }

    public void setAvgBreachProblyPD(BigDecimal avgBreachProblyPD) {
        this.avgBreachProblyPD = avgBreachProblyPD;
    }

    public String getEvalStartDate() {
        return evalStartDate;
    }

    public void setEvalStartDate(String evalStartDate) {
        this.evalStartDate = evalStartDate;
    }

    public String getEvalEndDate() {
        return evalEndDate;
    }

    public void setEvalEndDate(String evalEndDate) {
        this.evalEndDate = evalEndDate;
    }

    public String getDtghFlag() {
        return dtghFlag;
    }

    public void setDtghFlag(String dtghFlag) {
        this.dtghFlag = dtghFlag;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", bankCreditLvl='" + bankCreditLvl + '\'' +
                ", avgBreachProblyPD=" + avgBreachProblyPD +
                ", evalStartDate='" + evalStartDate + '\'' +
                ", evalEndDate='" + evalEndDate + '\'' +
                ", dtghFlag='" + dtghFlag + '\'' +
                '}';
    }
}
