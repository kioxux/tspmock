package cn.com.yusys.yusp.dto.server.biz.xddb0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 16:55
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ypLists")
    private java.util.List<YpList> ypLists;

    public List<YpList> getYpLists() {
        return ypLists;
    }

    public void setYpLists(List<YpList> ypLists) {
        this.ypLists = ypLists;
    }

    @Override
    public String toString() {
        return "Data{" +
                "ypLists=" + ypLists +
                '}';
    }
}
