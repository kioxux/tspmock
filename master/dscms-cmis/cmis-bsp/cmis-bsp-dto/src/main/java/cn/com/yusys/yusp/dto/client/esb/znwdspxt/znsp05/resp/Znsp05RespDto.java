package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp05.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：风险拦截截接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp05RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "result")
    private String result;//校验结果

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Znsp05RespDto{" +
                "result='" + result + '\'' +
                '}';
    }
}  
