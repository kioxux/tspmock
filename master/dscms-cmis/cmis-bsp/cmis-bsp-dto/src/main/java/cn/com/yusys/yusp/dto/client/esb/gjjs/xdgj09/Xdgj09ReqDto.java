package cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷查询牌价信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdgj09ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ccy")
    private String ccy;//买入币种
    @JsonProperty(value = "baseccy")
    private String baseccy;//卖出币种

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getBaseccy() {
        return baseccy;
    }

    public void setBaseccy(String baseccy) {
        this.baseccy = baseccy;
    }

    @Override
    public String toString() {
        return "Xdgj09ReqDto{" +
                "ccy='" + ccy + '\'' +
                "baseccy='" + baseccy + '\'' +
                '}';
    }
}  
