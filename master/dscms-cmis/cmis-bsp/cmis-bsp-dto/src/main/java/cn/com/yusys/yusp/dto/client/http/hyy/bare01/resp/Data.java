package cn.com.yusys.yusp.dto.client.http.hyy.bare01.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/24 13:39
 * @since 2021/8/24 13:39
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "items")
    private java.util.List<cn.com.yusys.yusp.dto.client.http.hyy.bare01.resp.Items> items;

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Data{" +
                "items=" + items +
                '}';
    }
}
