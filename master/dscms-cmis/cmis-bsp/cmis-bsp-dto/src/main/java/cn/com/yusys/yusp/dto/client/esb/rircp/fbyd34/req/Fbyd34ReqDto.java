package cn.com.yusys.yusp.dto.client.esb.rircp.fbyd34.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：支用模型校验
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbyd34ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "channel_type")
    private String channel_type;//渠道来源
    @JsonProperty(value = "co_platform")
    private String co_platform;//合作平台
    @JsonProperty(value = "prd_type")
    private String prd_type;//产品类别
    @JsonProperty(value = "prd_code")
    private String prd_code;//产品代码（零售智能风控内部代码）
    @JsonProperty(value = "cust_name")
    private String cust_name;//客户姓名
    @JsonProperty(value = "cert_code")
    private String cert_code;//客户证件号码
    @JsonProperty(value = "app_no")
    private String app_no;//授信申请业务流水号

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    @Override
    public String toString() {
        return "Fbyd34ReqDto{" +
                "channel_type='" + channel_type + '\'' +
                "co_platform='" + co_platform + '\'' +
                "prd_type='" + prd_type + '\'' +
                "prd_code='" + prd_code + '\'' +
                "cust_name='" + cust_name + '\'' +
                "cert_code='" + cert_code + '\'' +
                "app_no='" + app_no + '\'' +
                '}';
    }
}  
