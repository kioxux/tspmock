package cn.com.yusys.yusp.dto.server.cfg.xdqt0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据产品号查询产品名称
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prdId")
    private String prdId;//产品代码


    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "prdId='" + prdId + '\'' +
                '}';
    }
}
