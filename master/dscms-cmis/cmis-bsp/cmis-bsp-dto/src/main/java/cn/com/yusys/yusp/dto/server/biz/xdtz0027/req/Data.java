package cn.com.yusys.yusp.dto.server.biz.xdtz0027.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;
    @JsonProperty(value = "sftqzz")
    private String sftqzz;//是否提前周转

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getSftqzz() {
        return sftqzz;
    }

    public void setSftqzz(String sftqzz) {
        this.sftqzz = sftqzz;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                "sftqzz='" + sftqzz + '\'' +
                '}';
    }
}
