package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 20:33
 * @since 2021/5/28 20:33
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "currency")
    private String currency;//币种
    @JsonProperty(value = "acctname")
    private String acctname;//账户名称
    @JsonProperty(value = "acctseq")
    private String acctseq;//账户序号
    @JsonProperty(value = "acctno")
    private String acctno;//账号
    @JsonProperty(value = "custname")
    private String custname;//客户名称
    @JsonProperty(value = "custno")
    private String custno;//客户号

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAcctname() {
        return acctname;
    }

    public void setAcctname(String acctname) {
        this.acctname = acctname;
    }

    public String getAcctseq() {
        return acctseq;
    }

    public void setAcctseq(String acctseq) {
        this.acctseq = acctseq;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    @Override
    public String toString() {
        return "Xdpj12ReqDto{" +
                "currency='" + currency + '\'' +
                "acctname='" + acctname + '\'' +
                "acctseq='" + acctseq + '\'' +
                "acctno='" + acctno + '\'' +
                "custname='" + custname + '\'' +
                "custno='" + custno + '\'' +
                '}';
    }
}
