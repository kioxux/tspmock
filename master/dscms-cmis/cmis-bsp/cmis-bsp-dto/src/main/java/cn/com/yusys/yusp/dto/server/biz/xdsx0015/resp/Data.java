package cn.com.yusys.yusp.dto.server.biz.xdsx0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 15:12:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 15:12
 * @since 2021/5/19 15:12
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "db_cus_name")
    private String db_cus_name;//担保合同客户名

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getDb_cus_name() {
        return db_cus_name;
    }

    public void setDb_cus_name(String db_cus_name) {
        this.db_cus_name = db_cus_name;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cus_id='" + cus_id + '\'' +
                ", db_cus_name='" + db_cus_name + '\'' +
                '}';
    }
}
