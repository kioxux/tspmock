package cn.com.yusys.yusp.dto.server.biz.xdxw0077.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/11 10:56
 * @since 2021/6/11 10:56
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//业务流水号
    @JsonProperty(value = "prdNo")
    private String prdNo;//产品编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "serno='" + serno + '\'' +
                ", prdNo='" + prdNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}
