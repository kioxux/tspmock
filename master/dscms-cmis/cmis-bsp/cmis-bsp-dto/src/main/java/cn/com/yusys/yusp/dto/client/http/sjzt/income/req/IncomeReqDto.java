package cn.com.yusys.yusp.dto.client.http.sjzt.income.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/20 18:32
 * @since 2021/8/20 18:32
 */
@JsonPropertyOrder(alphabetic = true)
public class IncomeReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "startDate")
    private String startDate;//收益产生日开始时间
    @JsonProperty(value = "endDate")
    private String endDate;//收益产生日截止时间
    @JsonProperty(value = "size")
    private Integer size;//请求条数
    @JsonProperty(value = "from")
    private Integer from;//页码


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "IncomeReqDto{" +
                "cusId='" + cusId + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", size=" + size +
                ", from=" + from +
                '}';
    }
}
