package cn.com.yusys.yusp.dto.client.esb.core.ln3074.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：用于贷款预约展期
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3074RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "zhanqixh")
    private Integer zhanqixh;//展期序号
    @JsonProperty(value = "zhanqhth")
    private String zhanqhth;//展期合同号
    @JsonProperty(value = "zhanqirq")
    private String zhanqirq;//展期日期
    @JsonProperty(value = "zhanqdqr")
    private String zhanqdqr;//展期到期日
    @JsonProperty(value = "zhanqije")
    private BigDecimal zhanqije;//展期金额
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "kaihguiy")
    private String kaihguiy;//开户柜员
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getZhanqixh() {
        return zhanqixh;
    }

    public void setZhanqixh(Integer zhanqixh) {
        this.zhanqixh = zhanqixh;
    }

    public String getZhanqhth() {
        return zhanqhth;
    }

    public void setZhanqhth(String zhanqhth) {
        this.zhanqhth = zhanqhth;
    }

    public String getZhanqirq() {
        return zhanqirq;
    }

    public void setZhanqirq(String zhanqirq) {
        this.zhanqirq = zhanqirq;
    }

    public String getZhanqdqr() {
        return zhanqdqr;
    }

    public void setZhanqdqr(String zhanqdqr) {
        this.zhanqdqr = zhanqdqr;
    }

    public BigDecimal getZhanqije() {
        return zhanqije;
    }

    public void setZhanqije(BigDecimal zhanqije) {
        this.zhanqije = zhanqije;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    @Override
    public String toString() {
        return "Ln3074RespDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "zhanqixh='" + zhanqixh + '\'' +
                "zhanqhth='" + zhanqhth + '\'' +
                "zhanqirq='" + zhanqirq + '\'' +
                "zhanqdqr='" + zhanqdqr + '\'' +
                "zhanqije='" + zhanqije + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "kaihriqi='" + kaihriqi + '\'' +
                "kaihguiy='" + kaihguiy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                '}';
    }
}  
