package cn.com.yusys.yusp.dto.client.esb.irs.xirs27.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：工作台提示条数
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xirs27RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "count")
    private String count;//提示条数

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Xirs27RespDto{" +
                "count='" + count + '\'' +
                '}';
    }
}  
