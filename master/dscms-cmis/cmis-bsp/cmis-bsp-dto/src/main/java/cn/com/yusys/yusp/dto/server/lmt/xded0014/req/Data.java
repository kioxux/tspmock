package cn.com.yusys.yusp.dto.server.lmt.xded0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：台账恢复接口
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    //系统编号
    @JsonProperty(value = "sysId")
    private String sysId;

    //金融机构代码
    @JsonProperty(value = "instuCde")
    private String instuCde;

    //交易流水号
    @JsonProperty(value = "serno")
    private String serno;

    //登记人
    @JsonProperty(value = "inputId")
    private String inputId;

    //登记机构
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    //登记日期
    @JsonProperty(value = "inputDate")
    private String inputDate;

    @JsonProperty(value = "dealBizList")
    private List<DealBizList> dealBizList ;

    public List<DealBizList> getDealBizList() {
        return dealBizList;
    }

    public void setDealBizList(List<DealBizList> dealBizList) {
        this.dealBizList = dealBizList;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "sysId='" + sysId + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", serno='" + serno + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", dealBizList=" + dealBizList +
                '}';
    }
}
