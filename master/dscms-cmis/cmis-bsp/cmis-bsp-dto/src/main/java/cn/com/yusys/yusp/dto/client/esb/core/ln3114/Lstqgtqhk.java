package cn.com.yusys.yusp.dto.client.esb.core.ln3114;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：期供提前还款
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstqgtqhk implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer benqqish;//本期期数
    @JsonProperty(value = "huankriq")
    private String huankriq;//还款日期
    @JsonProperty(value = "chushibj")
    private BigDecimal chushibj;//初始本金
    @JsonProperty(value = "chushilx")
    private BigDecimal chushilx;//初始利息
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "bjdshyje")
    private BigDecimal bjdshyje;//本阶段剩余本金

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getHuankriq() {
        return huankriq;
    }

    public void setHuankriq(String huankriq) {
        this.huankriq = huankriq;
    }

    public BigDecimal getChushibj() {
        return chushibj;
    }

    public void setChushibj(BigDecimal chushibj) {
        this.chushibj = chushibj;
    }

    public BigDecimal getChushilx() {
        return chushilx;
    }

    public void setChushilx(BigDecimal chushilx) {
        this.chushilx = chushilx;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getBjdshyje() {
        return bjdshyje;
    }

    public void setBjdshyje(BigDecimal bjdshyje) {
        this.bjdshyje = bjdshyje;
    }

    @Override
    public String toString() {
        return "Lstqgtqhk{" +
                "benqqish='" + benqqish + '\'' +
                "huankriq='" + huankriq + '\'' +
                "chushibj='" + chushibj + '\'' +
                "chushilx='" + chushilx + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "bjdshyje='" + bjdshyje + '\'' +
                '}';
    }
}  
