package cn.com.yusys.yusp.dto.server.biz.xdsx0024.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：省心快贷plus授信，风控自动审批结果推送信贷
 * @author xll
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0024RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdsx0024RespDto{" +
				"data=" + data +
				'}';
	}
}  
