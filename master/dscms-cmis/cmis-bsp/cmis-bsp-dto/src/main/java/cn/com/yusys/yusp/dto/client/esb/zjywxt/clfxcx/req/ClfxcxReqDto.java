package cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：协议信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ClfxcxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xybhsq")
    private String xybhsq;//协议号
    @JsonProperty(value = "fkzjh")
    private String fkzjh;//买方证件号
    @JsonProperty(value = "fkmc")
    private String fkmc;//买方名称

    public String getXybhsq() {
        return xybhsq;
    }

    public void setXybhsq(String xybhsq) {
        this.xybhsq = xybhsq;
    }

    public String getFkzjh() {
        return fkzjh;
    }

    public void setFkzjh(String fkzjh) {
        this.fkzjh = fkzjh;
    }

    public String getFkmc() {
        return fkmc;
    }

    public void setFkmc(String fkmc) {
        this.fkmc = fkmc;
    }

    @Override
    public String toString() {
        return "ClfxcxReqDto{" +
                "xybhsq='" + xybhsq + '\'' +
                "fkzjh='" + fkzjh + '\'' +
                "fkmc='" + fkmc + '\'' +
                '}';
    }
}  
