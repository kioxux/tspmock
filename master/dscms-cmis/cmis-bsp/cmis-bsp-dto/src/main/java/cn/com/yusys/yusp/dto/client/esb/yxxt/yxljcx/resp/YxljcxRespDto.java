package cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：影像图像路径查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class YxljcxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "pathss")
    private String pathss;//图像路径（多个使用;分隔）（若有文件名则通过#imageName#分隔）
    @JsonProperty(value = "rptem1")
    private String rptem1;//应答备用字段1
    @JsonProperty(value = "rptem2")
    private String rptem2;//应答备用字段2
    @JsonProperty(value = "rptem3")
    private String rptem3;//应答备用字段3
    @JsonProperty(value = "rptem4")
    private String rptem4;//应答备用字段4

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getPathss() {
        return pathss;
    }

    public void setPathss(String pathss) {
        this.pathss = pathss;
    }

    public String getRptem1() {
        return rptem1;
    }

    public void setRptem1(String rptem1) {
        this.rptem1 = rptem1;
    }

    public String getRptem2() {
        return rptem2;
    }

    public void setRptem2(String rptem2) {
        this.rptem2 = rptem2;
    }

    public String getRptem3() {
        return rptem3;
    }

    public void setRptem3(String rptem3) {
        this.rptem3 = rptem3;
    }

    public String getRptem4() {
        return rptem4;
    }

    public void setRptem4(String rptem4) {
        this.rptem4 = rptem4;
    }

    @Override
    public String toString() {
        return "YxljcxRespDto{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "pathss='" + pathss + '\'' +
                "rptem1='" + rptem1 + '\'' +
                "rptem2='" + rptem2 + '\'' +
                "rptem3='" + rptem3 + '\'' +
                "rptem4='" + rptem4 + '\'' +
                '}';
    }
}  
