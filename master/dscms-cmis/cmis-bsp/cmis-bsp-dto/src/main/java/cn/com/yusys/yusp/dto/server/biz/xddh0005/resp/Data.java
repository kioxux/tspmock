package cn.com.yusys.yusp.dto.server.biz.xddh0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/22 14:53:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 14:53
 * @since 2021/5/22 14:53
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "accotTranSerno")
    private String accotTranSerno;//核心交易流水
    @JsonProperty(value = "repayInt")
    private BigDecimal repayInt;//还款利息
    @JsonProperty(value = "nextRepayDate")
    private String nextRepayDate;//下次还款日
    @JsonProperty(value = "repayCap")
    private BigDecimal repayCap;//还款本金

    public String getAccotTranSerno() {
        return accotTranSerno;
    }

    public void setAccotTranSerno(String accotTranSerno) {
        this.accotTranSerno = accotTranSerno;
    }

    public BigDecimal getRepayInt() {
        return repayInt;
    }

    public void setRepayInt(BigDecimal repayInt) {
        this.repayInt = repayInt;
    }

    public String getNextRepayDate() {
        return nextRepayDate;
    }

    public void setNextRepayDate(String nextRepayDate) {
        this.nextRepayDate = nextRepayDate;
    }

    public BigDecimal getRepayCap() {
        return repayCap;
    }

    public void setRepayCap(BigDecimal repayCap) {
        this.repayCap = repayCap;
    }

    @Override
    public String toString() {
        return "Xddh0005RespDto{" +
                "accotTranSerno='" + accotTranSerno + '\'' +
                "repayInt='" + repayInt + '\'' +
                "nextRepayDate='" + nextRepayDate + '\'' +
                "repayCap='" + repayCap + '\'' +
                '}';
    }
}
