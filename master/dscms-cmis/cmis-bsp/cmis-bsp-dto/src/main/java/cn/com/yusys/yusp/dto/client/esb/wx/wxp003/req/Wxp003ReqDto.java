package cn.com.yusys.yusp.dto.client.esb.wx.wxp003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷将放款标识推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxp003ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applid")
    private String applid;//对接记录ID
    @JsonProperty(value = "entnam")
    private String entnam;//企业名称
    @JsonProperty(value = "entpid")
    private String entpid;//企业代码
    @JsonProperty(value = "postim")
    private String postim;//放款时间
    @JsonProperty(value = "connum")
    private String connum;//贷款合同号
    @JsonProperty(value = "posamt")
    private String posamt;//放款金额
    @JsonProperty(value = "posrat")
    private String posrat;//贷款利率
    @JsonProperty(value = "loanno")
    private String loanno;//贷款借据号
    @JsonProperty(value = "guartp")
    private String guartp;//担保方式
    @JsonProperty(value = "fivecg")
    private String fivecg;//五级分类
    @JsonProperty(value = "posbeg")
    private String posbeg;//贷款开始时间
    @JsonProperty(value = "posend")
    private String posend;//贷款结束时间
    @JsonProperty(value = "posper")
    private String posper;//放款期限
    @JsonProperty(value = "posrea")
    private String posrea;//放款原因

    public String getApplid() {
        return applid;
    }

    public void setApplid(String applid) {
        this.applid = applid;
    }

    public String getEntnam() {
        return entnam;
    }

    public void setEntnam(String entnam) {
        this.entnam = entnam;
    }

    public String getEntpid() {
        return entpid;
    }

    public void setEntpid(String entpid) {
        this.entpid = entpid;
    }

    public String getPostim() {
        return postim;
    }

    public void setPostim(String postim) {
        this.postim = postim;
    }

    public String getConnum() {
        return connum;
    }

    public void setConnum(String connum) {
        this.connum = connum;
    }

    public String getPosamt() {
        return posamt;
    }

    public void setPosamt(String posamt) {
        this.posamt = posamt;
    }

    public String getPosrat() {
        return posrat;
    }

    public void setPosrat(String posrat) {
        this.posrat = posrat;
    }

    public String getLoanno() {
        return loanno;
    }

    public void setLoanno(String loanno) {
        this.loanno = loanno;
    }

    public String getGuartp() {
        return guartp;
    }

    public void setGuartp(String guartp) {
        this.guartp = guartp;
    }

    public String getFivecg() {
        return fivecg;
    }

    public void setFivecg(String fivecg) {
        this.fivecg = fivecg;
    }

    public String getPosbeg() {
        return posbeg;
    }

    public void setPosbeg(String posbeg) {
        this.posbeg = posbeg;
    }

    public String getPosend() {
        return posend;
    }

    public void setPosend(String posend) {
        this.posend = posend;
    }

    public String getPosper() {
        return posper;
    }

    public void setPosper(String posper) {
        this.posper = posper;
    }

    public String getPosrea() {
        return posrea;
    }

    public void setPosrea(String posrea) {
        this.posrea = posrea;
    }

    @Override
    public String toString() {
        return "Wxp003ReqDto{" +
                "applid='" + applid + '\'' +
                "entnam='" + entnam + '\'' +
                "entpid='" + entpid + '\'' +
                "postim='" + postim + '\'' +
                "connum='" + connum + '\'' +
                "posamt='" + posamt + '\'' +
                "posrat='" + posrat + '\'' +
                "loanno='" + loanno + '\'' +
                "guartp='" + guartp + '\'' +
                "fivecg='" + fivecg + '\'' +
                "posbeg='" + posbeg + '\'' +
                "posend='" + posend + '\'' +
                "posper='" + posper + '\'' +
                "posrea='" + posrea + '\'' +
                '}';
    }
}  
