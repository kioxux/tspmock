package cn.com.yusys.yusp.dto.client.http.ocr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 20:26
 * @since 2021/6/4 20:26
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "statisItem")
    private List<cn.com.yusys.yusp.dto.client.http.ocr.resp.StatisItem> statisItem;//科目对应的统计项列表
    @JsonProperty(value = "leftSubjectList")
    private List<cn.com.yusys.yusp.dto.client.http.ocr.resp.LeftSubject> leftSubjectList;
    @JsonProperty(value = "rightSubjectList")
    private List<cn.com.yusys.yusp.dto.client.http.ocr.resp.RightSubject> rightSubjectList;

    public List<StatisItem> getStatisItem() {
        return statisItem;
    }

    public void setStatisItem(List<StatisItem> statisItem) {
        this.statisItem = statisItem;
    }

    public List<LeftSubject> getLeftSubjectList() {
        return leftSubjectList;
    }

    public void setLeftSubjectList(List<LeftSubject> leftSubjectList) {
        this.leftSubjectList = leftSubjectList;
    }

    public List<RightSubject> getRightSubjectList() {
        return rightSubjectList;
    }

    public void setRightSubjectList(List<RightSubject> rightSubjectList) {
        this.rightSubjectList = rightSubjectList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "statisItem=" + statisItem +
                ", leftSubjectList=" + leftSubjectList +
                ", rightSubjectList=" + rightSubjectList +
                '}';
    }
}
