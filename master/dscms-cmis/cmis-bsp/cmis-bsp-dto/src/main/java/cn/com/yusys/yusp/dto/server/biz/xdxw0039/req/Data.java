package cn.com.yusys.yusp.dto.server.biz.xdxw0039.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：提交决议
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//调查报告主键
    @JsonProperty(value = "approve_status")
    private String approve_status;//审批状态 WF_STATUS
    @JsonProperty(value = "option_flag")
    private String option_flag;//01-提交
	@JsonProperty(value = "CusSurveyList")
	private List<CusSurveyList> cusSurveyList;//01-提交

	public List<CusSurveyList> getCusSurveyList() {
		return cusSurveyList;
	}

	public void setCusSurveyList(List<CusSurveyList> cusSurveyList) {
		this.cusSurveyList = cusSurveyList;
	}

	public String getSurvey_serno() {
		return survey_serno;
	}

	public void setSurvey_serno(String survey_serno) {
		this.survey_serno = survey_serno;
	}

	public String getApprove_status() {
		return approve_status;
	}

	public void setApprove_status(String approve_status) {
		this.approve_status = approve_status;
	}

	public String getOption_flag() {
		return option_flag;
	}

	public void setOption_flag(String option_flag) {
		this.option_flag = option_flag;
	}

	@Override
	public String toString() {
		return "Data{" +
				"survey_serno='" + survey_serno + '\'' +
				", approve_status='" + approve_status + '\'' +
				", option_flag='" + option_flag + '\'' +
				", CusSurveyList=" + cusSurveyList +
				'}';
	}
}
