package cn.com.yusys.yusp.dto.client.esb.core.ln3243;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：停息贷款利息试算
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3243RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "daikuyue")
    private BigDecimal daikuyue;//贷款余额
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期
    @JsonProperty(value = "qianxiii")
    private BigDecimal qianxiii;//欠息
    @JsonProperty(value = "faxiiiii")
    private BigDecimal faxiiiii;//罚息
    @JsonProperty(value = "whqianxi")
    private BigDecimal whqianxi;//未偿还欠息总额

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public BigDecimal getDaikuyue() {
        return daikuyue;
    }

    public void setDaikuyue(BigDecimal daikuyue) {
        this.daikuyue = daikuyue;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public BigDecimal getQianxiii() {
        return qianxiii;
    }

    public void setQianxiii(BigDecimal qianxiii) {
        this.qianxiii = qianxiii;
    }

    public BigDecimal getFaxiiiii() {
        return faxiiiii;
    }

    public void setFaxiiiii(BigDecimal faxiiiii) {
        this.faxiiiii = faxiiiii;
    }

    public BigDecimal getWhqianxi() {
        return whqianxi;
    }

    public void setWhqianxi(BigDecimal whqianxi) {
        this.whqianxi = whqianxi;
    }

    @Override
    public String toString() {
        return "Ln3243RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "daikuyue='" + daikuyue + '\'' +
                "zhchbjin='" + zhchbjin + '\'' +
                "yuqibjin='" + yuqibjin + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "zhzhriqi='" + zhzhriqi + '\'' +
                "qianxiii='" + qianxiii + '\'' +
                "faxiiiii='" + faxiiiii + '\'' +
                "whqianxi='" + whqianxi + '\'' +
                '}';
    }
}  
