package cn.com.yusys.yusp.dto.server.oca.xdxt0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 16:30:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 16:30
 * @since 2021/5/24 16:30
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "orgNo")
    private String orgNo;//账户机构号

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    @Override
    public String toString() {
        return "Xdxt0008RespDto{" +
                "orgNo='" + orgNo + '\'' +
                '}';
    }
}
