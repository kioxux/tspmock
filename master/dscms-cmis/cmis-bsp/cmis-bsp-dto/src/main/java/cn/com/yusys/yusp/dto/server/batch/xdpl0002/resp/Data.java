package cn.com.yusys.yusp.dto.server.batch.xdpl0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：提供日终调度平台查询批量任务状态
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "priFlag")
    private String priFlag;//任务级别
    @JsonProperty(value = "priTaskStatus")
    private String priTaskStatus;//该任务级别总状态
    @JsonProperty(value = "priTaskNum")
    private Integer priTaskNum;// 该任务级别总任务数
    @JsonProperty(value = "priTask100Num")
    private Integer priTask100Num;// 该任务级别中执行成功的任务数
    @JsonProperty(value = "priTask010Num")
    private Integer priTask010Num;// 该任务级别中执行中的任务数
    @JsonProperty(value = "priErrorTaskNos")
    private String priErrorTaskNos;//该任务级别的错误任务编号
    @JsonProperty(value = "priTaskErrorCodes")
    private String priTaskErrorCodes;//该任务级别的错误码
    @JsonProperty(value = "priTaskErrorInfos")
    private String priTaskErrorInfos;//该任务级别的错误信息

    @JsonProperty(value = "list")
    private java.util.List<BatTaskRunDto> list;//任务列表

    public String getPriFlag() {
        return priFlag;
    }

    public void setPriFlag(String priFlag) {
        this.priFlag = priFlag;
    }

    public String getPriTaskStatus() {
        return priTaskStatus;
    }

    public void setPriTaskStatus(String priTaskStatus) {
        this.priTaskStatus = priTaskStatus;
    }

    public Integer getPriTaskNum() {
        return priTaskNum;
    }

    public void setPriTaskNum(Integer priTaskNum) {
        this.priTaskNum = priTaskNum;
    }

    public Integer getPriTask100Num() {
        return priTask100Num;
    }

    public void setPriTask100Num(Integer priTask100Num) {
        this.priTask100Num = priTask100Num;
    }

    public Integer getPriTask010Num() {
        return priTask010Num;
    }

    public void setPriTask010Num(Integer priTask010Num) {
        this.priTask010Num = priTask010Num;
    }

    public String getPriErrorTaskNos() {
        return priErrorTaskNos;
    }

    public void setPriErrorTaskNos(String priErrorTaskNos) {
        this.priErrorTaskNos = priErrorTaskNos;
    }

    public String getPriTaskErrorCodes() {
        return priTaskErrorCodes;
    }

    public void setPriTaskErrorCodes(String priTaskErrorCodes) {
        this.priTaskErrorCodes = priTaskErrorCodes;
    }

    public String getPriTaskErrorInfos() {
        return priTaskErrorInfos;
    }

    public void setPriTaskErrorInfos(String priTaskErrorInfos) {
        this.priTaskErrorInfos = priTaskErrorInfos;
    }

    public List<BatTaskRunDto> getList() {
        return list;
    }

    public void setList(List<BatTaskRunDto> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "priFlag='" + priFlag + '\'' +
                ", priTaskStatus='" + priTaskStatus + '\'' +
                ", priTaskNum=" + priTaskNum +
                ", priTask100Num=" + priTask100Num +
                ", priTask010Num=" + priTask010Num +
                ", priErrorTaskNos='" + priErrorTaskNos + '\'' +
                ", priTaskErrorCodes='" + priTaskErrorCodes + '\'' +
                ", priTaskErrorInfos='" + priTaskErrorInfos + '\'' +
                ", list=" + list +
                '}';
    }
}
