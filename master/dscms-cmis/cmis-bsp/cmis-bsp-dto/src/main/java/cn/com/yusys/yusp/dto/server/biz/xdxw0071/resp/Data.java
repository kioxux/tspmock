package cn.com.yusys.yusp.dto.server.biz.xdxw0071.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询优抵贷抵质押品信息
 * @Author zhangpeng
 * @Date 2021/4/26 22:01
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.server.biz.xdxw0071.resp.List> list;

    public java.util.List<cn.com.yusys.yusp.dto.server.biz.xdxw0071.resp.List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "list=" + list +
                '}';
    }
}
