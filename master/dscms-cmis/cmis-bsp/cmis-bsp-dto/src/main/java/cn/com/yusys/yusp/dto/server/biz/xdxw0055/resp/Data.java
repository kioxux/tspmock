package cn.com.yusys.yusp.dto.server.biz.xdxw0055.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：经营地址查询
 * @Author zhangpeng
 * @Date 2021/4/24 15:02
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "operAddr")
    private String operAddr;//经营地址

    public String getOperAddr() {
        return operAddr;
    }

    public void setOperAddr(String operAddr) {
        this.operAddr = operAddr;
    }

    @Override
    public String toString() {
        return "Data{" +
                "operAddr='" + operAddr + '\'' +
                '}';
    }
}
