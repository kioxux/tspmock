package cn.com.yusys.yusp.dto.server.biz.xdxw0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/18 13:36:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/18 13:36
 * @since 2021/5/18 13:36
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//身份证件号码
    @JsonProperty(value = "certType")
    private String certType;//身份证件类型
    @JsonProperty(value = "applyDate")
    private String applyDate;//申请日期
    @JsonProperty(value = "lmtLmtType")
    private String lmtLmtType;//授信文件类型
    @JsonProperty(value = "cus_type")
    private String cus_type;//查询类型 01企业；02个人
    @JsonProperty(value = "cusname_type")
    private String cusname_type;//授权人类型:01主借款人，02共同借款人，03其他关系人，04担保人，05法人代表、出资人及关联人等，06关系人
    @JsonProperty(value = "prdType")
    private String  prdType;//产品类型 ：优享贷 YXD   优抵贷YDD  优企贷YQD  优农贷 YND
    @JsonProperty(value = "company")
    private String  company;//企业客户

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getLmtLmtType() {
        return lmtLmtType;
    }

    public void setLmtLmtType(String lmtLmtType) {
        this.lmtLmtType = lmtLmtType;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }

    public String getCusname_type() {
        return cusname_type;
    }

    public void setCusname_type(String cusname_type) {
        this.cusname_type = cusname_type;
    }

    public String getPrdType() {
        return prdType;
    }

    public void setPrdType(String prdType) {
        this.prdType = prdType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusName='" + cusName + '\'' +
                ", certNo='" + certNo + '\'' +
                ", certType='" + certType + '\'' +
                ", applyDate='" + applyDate + '\'' +
                ", lmtLmtType='" + lmtLmtType + '\'' +
                ", cus_type='" + cus_type + '\'' +
                ", cusname_type='" + cusname_type + '\'' +
                ", prdType='" + prdType + '\'' +
                ", company='" + company + '\'' +
                '}';
    }
}
