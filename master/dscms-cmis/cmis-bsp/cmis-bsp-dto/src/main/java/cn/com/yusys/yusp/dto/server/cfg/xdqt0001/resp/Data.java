package cn.com.yusys.yusp.dto.server.cfg.xdqt0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据产品号查询产品名称
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    @Override
    public String toString() {
        return "Data{" +
                "prdName='" + prdName + '\'' +
                '}';
    }
}  
