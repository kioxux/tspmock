package cn.com.yusys.yusp.dto.server.biz.xdcz0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：推送在线保函预约信息
 *
 * @Author xs
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//任务编号
    @JsonProperty(value = "creditcode")
    private String creditcode;//统一社会信用代码
    @JsonProperty(value = "cusname")
    private String cusname;//客户名称
    @JsonProperty(value = "contacts")
    private String contacts;//联系人
    @JsonProperty(value = "phone")
    private String phone;//联系电话
    @JsonProperty(value = "input_time")
    private String input_time;//登记时间
    @JsonProperty(value = "main_br_id")
    private String main_br_id;//管户机构

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCreditcode() {
        return creditcode;
    }

    public void setCreditcode(String creditcode) {
        this.creditcode = creditcode;
    }

    public String getCusname() {
        return cusname;
    }

    public void setCusname(String cusname) {
        this.cusname = cusname;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInput_time() {
        return input_time;
    }

    public void setInput_time(String input_time) {
        this.input_time = input_time;
    }

    public String getMain_br_id() {
        return main_br_id;
    }

    public void setMain_br_id(String main_br_id) {
        this.main_br_id = main_br_id;
    }

    @Override
    public String toString() {
        return "Xdcz0024ReqDto{" +
                "serno='" + serno + '\'' +
                "creditcode='" + creditcode + '\'' +
                "cusname='" + cusname + '\'' +
                "contacts='" + contacts + '\'' +
                "phone='" + phone + '\'' +
                "input_time='" + input_time + '\'' +
                "main_br_id='" + main_br_id + '\'' +
                '}';
    }
}
