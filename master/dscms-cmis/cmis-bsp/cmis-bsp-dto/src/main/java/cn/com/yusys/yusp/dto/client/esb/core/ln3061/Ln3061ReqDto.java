package cn.com.yusys.yusp.dto.client.esb.core.ln3061;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：资产转让协议登记
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3061ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "caozfshi")
    private String caozfshi;//操作方式
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "ruzjigou")
    private String ruzjigou;//入账机构
    @JsonProperty(value = "xyzuigxe")
    private BigDecimal xyzuigxe;//协议最高限额
    @JsonProperty(value = "xieyilil")
    private BigDecimal xieyilil;//协议利率
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "xieyilix")
    private BigDecimal xieyilix;//协议利息
    @JsonProperty(value = "llqxkdfs")
    private String llqxkdfs;//利率期限靠档方式
    @JsonProperty(value = "lilvqixx")
    private String lilvqixx;//利率期限
    @JsonProperty(value = "lilvbhao")
    private String lilvbhao;//利率编号
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "yuqillbh")
    private String yuqillbh;//逾期利率编号
    @JsonProperty(value = "yuqinyll")
    private String yuqinyll;//逾期年月利率
    @JsonProperty(value = "yuqitzfs")
    private String yuqitzfs;//逾期利率调整方式
    @JsonProperty(value = "yuqitzzq")
    private String yuqitzzq;//逾期利率调整周期
    @JsonProperty(value = "yqfxfdfs")
    private String yqfxfdfs;//逾期罚息浮动方式
    @JsonProperty(value = "yqfxfdzh")
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "flllcklx")
    private String flllcklx;//复利利率参考类型
    @JsonProperty(value = "fulilvbh")
    private String fulilvbh;//复利利率编号
    @JsonProperty(value = "fulilvny")
    private String fulilvny;//复利利率年月标识
    @JsonProperty(value = "fulitzfs")
    private String fulitzfs;//复利利率调整方式
    @JsonProperty(value = "fulitzzq")
    private String fulitzzq;//复利利率调整周期
    @JsonProperty(value = "fulifdfs")
    private String fulifdfs;//复利利率浮动方式
    @JsonProperty(value = "fulifdzh")
    private BigDecimal fulifdzh;//复利利率浮动值
    @JsonProperty(value = "zcrfshii")
    private String zcrfshii;//资产融通方式
    @JsonProperty(value = "zcrtbili")
    private BigDecimal zcrtbili;//资产融通比例
    @JsonProperty(value = "zjlyzhao")
    private String zjlyzhao;//资金来源账号
    @JsonProperty(value = "zjlyzzxh")
    private String zjlyzzxh;//资金来源账号子序号
    @JsonProperty(value = "zrfukzhh")
    private String zrfukzhh;//对外付款账号
    @JsonProperty(value = "zrfukzxh")
    private String zrfukzxh;//对外付款账号子序号
    @JsonProperty(value = "zjguijbz")
    private String zjguijbz;//自动归集标志
    @JsonProperty(value = "zrfkzoqi")
    private String zrfkzoqi;//付款周期
    @JsonProperty(value = "zrjjfshi")
    private String zrjjfshi;//转让计价方式
    @JsonProperty(value = "qiandriq")
    private String qiandriq;//签订日期
    @JsonProperty(value = "fengbriq")
    private String fengbriq;//封包日期
    @JsonProperty(value = "huigriqi")
    private String huigriqi;//回购日期
    @JsonProperty(value = "bzesfzrr")
    private String bzesfzrr;//资金来源账号不足额是否转让
    @JsonProperty(value = "jydsleix")
    private String jydsleix;//交易对手类型
    @JsonProperty(value = "jydshmch")
    private String jydshmch;//交易对手名称
    @JsonProperty(value = "jydszhao")
    private String jydszhao;//交易对手账号
    @JsonProperty(value = "jydszhzh")
    private String jydszhzh;//交易对手账号子序号
    @JsonProperty(value = "jydszhmc")
    private String jydszhmc;//交易对手账户名称
    @JsonProperty(value = "zhkaihhh")
    private String zhkaihhh;//账户开户行行号
    @JsonProperty(value = "zhkaihhm")
    private String zhkaihhm;//账户开户行行名
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要
    @JsonProperty(value = "lstDkzrjj")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3061.LstDkzrjj> lstDkzrjj;//资产转让借据信息[LIST]

    public String getCaozfshi() {
        return caozfshi;
    }

    public void setCaozfshi(String caozfshi) {
        this.caozfshi = caozfshi;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public BigDecimal getXyzuigxe() {
        return xyzuigxe;
    }

    public void setXyzuigxe(BigDecimal xyzuigxe) {
        this.xyzuigxe = xyzuigxe;
    }

    public BigDecimal getXieyilil() {
        return xieyilil;
    }

    public void setXieyilil(BigDecimal xieyilil) {
        this.xieyilil = xieyilil;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getXieyilix() {
        return xieyilix;
    }

    public void setXieyilix(BigDecimal xieyilix) {
        this.xieyilix = xieyilix;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getLilvbhao() {
        return lilvbhao;
    }

    public void setLilvbhao(String lilvbhao) {
        this.lilvbhao = lilvbhao;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getZcrfshii() {
        return zcrfshii;
    }

    public void setZcrfshii(String zcrfshii) {
        this.zcrfshii = zcrfshii;
    }

    public BigDecimal getZcrtbili() {
        return zcrtbili;
    }

    public void setZcrtbili(BigDecimal zcrtbili) {
        this.zcrtbili = zcrtbili;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    public String getZjguijbz() {
        return zjguijbz;
    }

    public void setZjguijbz(String zjguijbz) {
        this.zjguijbz = zjguijbz;
    }

    public String getZrfkzoqi() {
        return zrfkzoqi;
    }

    public void setZrfkzoqi(String zrfkzoqi) {
        this.zrfkzoqi = zrfkzoqi;
    }

    public String getZrjjfshi() {
        return zrjjfshi;
    }

    public void setZrjjfshi(String zrjjfshi) {
        this.zrjjfshi = zrjjfshi;
    }

    public String getQiandriq() {
        return qiandriq;
    }

    public void setQiandriq(String qiandriq) {
        this.qiandriq = qiandriq;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public String getBzesfzrr() {
        return bzesfzrr;
    }

    public void setBzesfzrr(String bzesfzrr) {
        this.bzesfzrr = bzesfzrr;
    }

    public String getJydsleix() {
        return jydsleix;
    }

    public void setJydsleix(String jydsleix) {
        this.jydsleix = jydsleix;
    }

    public String getJydshmch() {
        return jydshmch;
    }

    public void setJydshmch(String jydshmch) {
        this.jydshmch = jydshmch;
    }

    public String getJydszhao() {
        return jydszhao;
    }

    public void setJydszhao(String jydszhao) {
        this.jydszhao = jydszhao;
    }

    public String getJydszhzh() {
        return jydszhzh;
    }

    public void setJydszhzh(String jydszhzh) {
        this.jydszhzh = jydszhzh;
    }

    public String getJydszhmc() {
        return jydszhmc;
    }

    public void setJydszhmc(String jydszhmc) {
        this.jydszhmc = jydszhmc;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public List<LstDkzrjj> getLstDkzrjj() {
        return lstDkzrjj;
    }

    public void setLstDkzrjj(List<LstDkzrjj> lstDkzrjj) {
        this.lstDkzrjj = lstDkzrjj;
    }

    @Override
    public String toString() {
        return "Ln3061ReqDto{" +
                "caozfshi='" + caozfshi + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", ruzjigou='" + ruzjigou + '\'' +
                ", xyzuigxe=" + xyzuigxe +
                ", xieyilil=" + xieyilil +
                ", lilvleix='" + lilvleix + '\'' +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", xieyilix=" + xieyilix +
                ", llqxkdfs='" + llqxkdfs + '\'' +
                ", lilvqixx='" + lilvqixx + '\'' +
                ", lilvbhao='" + lilvbhao + '\'' +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", lilvfdfs='" + lilvfdfs + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", yuqililv=" + yuqililv +
                ", yqllcklx='" + yqllcklx + '\'' +
                ", yuqillbh='" + yuqillbh + '\'' +
                ", yuqinyll='" + yuqinyll + '\'' +
                ", yuqitzfs='" + yuqitzfs + '\'' +
                ", yuqitzzq='" + yuqitzzq + '\'' +
                ", yqfxfdfs='" + yqfxfdfs + '\'' +
                ", yqfxfdzh=" + yqfxfdzh +
                ", fulililv=" + fulililv +
                ", flllcklx='" + flllcklx + '\'' +
                ", fulilvbh='" + fulilvbh + '\'' +
                ", fulilvny='" + fulilvny + '\'' +
                ", fulitzfs='" + fulitzfs + '\'' +
                ", fulitzzq='" + fulitzzq + '\'' +
                ", fulifdfs='" + fulifdfs + '\'' +
                ", fulifdzh=" + fulifdzh +
                ", zcrfshii='" + zcrfshii + '\'' +
                ", zcrtbili=" + zcrtbili +
                ", zjlyzhao='" + zjlyzhao + '\'' +
                ", zjlyzzxh='" + zjlyzzxh + '\'' +
                ", zrfukzhh='" + zrfukzhh + '\'' +
                ", zrfukzxh='" + zrfukzxh + '\'' +
                ", zjguijbz='" + zjguijbz + '\'' +
                ", zrfkzoqi='" + zrfkzoqi + '\'' +
                ", zrjjfshi='" + zrjjfshi + '\'' +
                ", qiandriq='" + qiandriq + '\'' +
                ", fengbriq='" + fengbriq + '\'' +
                ", huigriqi='" + huigriqi + '\'' +
                ", bzesfzrr='" + bzesfzrr + '\'' +
                ", jydsleix='" + jydsleix + '\'' +
                ", jydshmch='" + jydshmch + '\'' +
                ", jydszhao='" + jydszhao + '\'' +
                ", jydszhzh='" + jydszhzh + '\'' +
                ", jydszhmc='" + jydszhmc + '\'' +
                ", zhkaihhh='" + zhkaihhh + '\'' +
                ", zhkaihhm='" + zhkaihhm + '\'' +
                ", beizhuxx='" + beizhuxx + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                ", lstDkzrjj=" + lstDkzrjj +
                '}';
    }
}
