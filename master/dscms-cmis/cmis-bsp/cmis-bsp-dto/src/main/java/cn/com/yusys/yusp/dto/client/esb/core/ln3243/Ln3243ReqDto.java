package cn.com.yusys.yusp.dto.client.esb.core.ln3243;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：停息贷款利息试算
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3243ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    @Override
    public String toString() {
        return "Ln3243ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                '}';
    }
}  
