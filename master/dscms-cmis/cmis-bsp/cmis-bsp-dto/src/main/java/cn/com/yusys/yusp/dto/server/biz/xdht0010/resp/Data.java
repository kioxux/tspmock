package cn.com.yusys.yusp.dto.server.biz.xdht0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhangpeng
 * @Date 2021/5/6 13:45
 * @Version 1.0
 */
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contList")
    private List<ContList> contList;

    public List<ContList> getContList() {
        return contList;
    }

    public void setContList(List<ContList> contList) {
        this.contList = contList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contList=" + contList +
                '}';
    }
}
