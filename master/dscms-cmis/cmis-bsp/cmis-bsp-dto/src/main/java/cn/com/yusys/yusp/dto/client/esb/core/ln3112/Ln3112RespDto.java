package cn.com.yusys.yusp.dto.client.esb.core.ln3112;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：贷款展期查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3112RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "datasq")
    private String datasq;//全局流水
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "daikxtai")
    private String daikxtai;//贷款形态
    @JsonProperty(value = "dkzhhzht")
    private String dkzhhzht;//贷款账户状态
    @JsonProperty(value = "lstdkzqdj")
    private List<Lstdkzqdj> lstdkzqdj;//贷款展期登记簿

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public List<Lstdkzqdj> getLstdkzqdj() {
        return lstdkzqdj;
    }

    public void setLstdkzqdj(List<Lstdkzqdj> lstdkzqdj) {
        this.lstdkzqdj = lstdkzqdj;
    }

    @Override
    public String toString() {
        return "Ln3112RespDto{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", zhchbjin=" + zhchbjin +
                ", daikxtai='" + daikxtai + '\'' +
                ", dkzhhzht='" + dkzhhzht + '\'' +
                ", lstdkzqdj=" + lstdkzqdj +
                '}';
    }
}
