package cn.com.yusys.yusp.dto.client.esb.ecif.g11004;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 21:19
 */
@JsonPropertyOrder(alphabetic = true)
public class CircleArrayMem implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "custno")
    private String custno; // 集团成员客户号
    @JsonProperty(value = "cortype")
    private String cortype; // 集团关系类型
    @JsonProperty(value = "grpdesc")
    private String grpdesc; // 集团关系描述
    @JsonProperty(value = "iscust")
    private String iscust; // 是否集团主客户
    @JsonProperty(value = "mainno")
    private String mainno; // 主管人
    @JsonProperty(value = "maiorg")
    private String maiorg; // 主管机构
    @JsonProperty(value = "socino")
    private String socino; // 统一社会信用代码
    @JsonProperty(value = "custst")
    private String custst; // 成员状态

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCortype() {
        return cortype;
    }

    public void setCortype(String cortype) {
        this.cortype = cortype;
    }

    public String getGrpdesc() {
        return grpdesc;
    }

    public void setGrpdesc(String grpdesc) {
        this.grpdesc = grpdesc;
    }

    public String getIscust() {
        return iscust;
    }

    public void setIscust(String iscust) {
        this.iscust = iscust;
    }

    public String getMainno() {
        return mainno;
    }

    public void setMainno(String mainno) {
        this.mainno = mainno;
    }

    public String getMaiorg() {
        return maiorg;
    }

    public void setMaiorg(String maiorg) {
        this.maiorg = maiorg;
    }

    public String getSocino() {
        return socino;
    }

    public void setSocino(String socino) {
        this.socino = socino;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    @Override
    public String toString() {
        return "CircleArrayMem{" +
                "custno='" + custno + '\'' +
                ", cortype='" + cortype + '\'' +
                ", grpdesc='" + grpdesc + '\'' +
                ", iscust='" + iscust + '\'' +
                ", mainno='" + mainno + '\'' +
                ", maiorg='" + maiorg + '\'' +
                ", socino='" + socino + '\'' +
                ", custst='" + custst + '\'' +
                '}';
    }
}
