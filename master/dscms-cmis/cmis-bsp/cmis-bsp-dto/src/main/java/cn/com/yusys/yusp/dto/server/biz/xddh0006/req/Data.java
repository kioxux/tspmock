package cn.com.yusys.yusp.dto.server.biz.xddh0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/22 15:07:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 15:07
 * @since 2021/5/22 15:07
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "repayType")
    private String repayType;//还款模式
    @JsonProperty(value = "advRepaymentAmt")
    private BigDecimal advRepaymentAmt;//提前还本金额

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public BigDecimal getAdvRepaymentAmt() {
        return advRepaymentAmt;
    }

    public void setAdvRepaymentAmt(BigDecimal advRepaymentAmt) {
        this.advRepaymentAmt = advRepaymentAmt;
    }

    @Override
    public String toString() {
        return "Xddh0006ReqDto{" +
                "billNo='" + billNo + '\'' +
                "repayType='" + repayType + '\'' +
                "advRepaymentAmt='" + advRepaymentAmt + '\'' +
                '}';
    }
}
