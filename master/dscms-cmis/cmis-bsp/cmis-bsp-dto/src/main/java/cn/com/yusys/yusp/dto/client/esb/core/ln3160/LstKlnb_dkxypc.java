package cn.com.yusys.yusp.dto.client.esb.core.ln3160;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LstKlnb_dkxypc {
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "picihaoo")
    private String picihaoo;//批次号
    @JsonProperty(value = "qiandriq")
    private String qiandriq;//签订日期
    @JsonProperty(value = "fengbriq")
    private String fengbriq;//封包日期
    @JsonProperty(value = "jiebriqi")
    private String jiebriqi;//解包日期
    @JsonProperty(value = "ruchiriq")
    private String ruchiriq;//入池日期
    @JsonProperty(value = "huigriqi")
    private String huigriqi;//回购日期
    @JsonProperty(value = "zchzhtai")
    private String zchzhtai;//资产处理状态
    @JsonProperty(value = "fuheztai")
    private String fuheztai;//复核状态
    @JsonProperty(value = "byxinxi1")
    private String byxinxi1;//备用信息1
    @JsonProperty(value = "byxinxi2")
    private String byxinxi2;//备用信息2
    @JsonProperty(value = "byxinxi3")
    private String byxinxi3;//备用信息3
    @JsonProperty(value = "ssfenbbz")
    private String ssfenbbz;//实时封包标志
}
