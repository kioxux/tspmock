package cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信用证信息同步
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_no")
    private String guar_no;//押品统一编号

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    @Override
    public String toString() {
        return "List{" +
                "guar_no='" + guar_no + '\'' +
                '}';
    }
}