package cn.com.yusys.yusp.dto.server.biz.xdcz0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：查询敞口额度及保证金校验
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "bail")
    private BigDecimal bail;//保证金
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "bailExchgRate")
    private BigDecimal bailExchgRate;//保证金汇率

    public BigDecimal getBail() {
        return bail;
    }

    public void setBail(BigDecimal bail) {
        this.bail = bail;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getBailExchgRate() {
        return bailExchgRate;
    }

    public void setBailExchgRate(BigDecimal bailExchgRate) {
        this.bailExchgRate = bailExchgRate;
    }

    @Override
    public String toString() {
        return "List{" +
                "bail=" + bail +
                ", curType='" + curType + '\'' +
                ", bailExchgRate=" + bailExchgRate +
                '}';
    }
}
