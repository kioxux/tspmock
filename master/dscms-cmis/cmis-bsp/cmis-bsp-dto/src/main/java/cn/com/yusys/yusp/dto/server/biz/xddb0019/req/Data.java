package cn.com.yusys.yusp.dto.server.biz.xddb0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
/**
 * 请求Dto：解质押押品校验
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "drftNo")
    private String drftNo;//票号

    public String getDrftNo() {
        return drftNo;
    }

    public void setDrftNo(String drftNo) {
        this.drftNo = drftNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "drftNo='" + drftNo + '\'' +
                '}';
    }
}
