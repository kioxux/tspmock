package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：惠享贷授信申请件数取得
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd01RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "crd_app_count")
    private String crd_app_count;//授信申请件数

    public String getCrd_app_count() {
        return crd_app_count;
    }

    public void setCrd_app_count(String crd_app_count) {
        this.crd_app_count = crd_app_count;
    }

    @Override
    public String toString() {
        return "Fbxd01RespDto{" +
                "crd_app_count='" + crd_app_count + '\'' +
                '}';
    }
}  
