package cn.com.yusys.yusp.dto.server.biz.xddh0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：还款记录列表查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalQnt")
    private Integer totalQnt;//总数
    @JsonProperty(value = "list")
    private java.util.List<List> list;//start

	public Integer getTotalQnt() {
		return totalQnt;
	}

	public void setTotalQnt(Integer totalQnt) {
		this.totalQnt = totalQnt;
	}

	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Data{" +
				"totalQnt=" + totalQnt +
				", list=" + list +
				'}';
	}
}
