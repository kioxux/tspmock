package cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：账单交易明细查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D12011ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cardno")
    private String cardno;//卡号
    @JsonProperty(value = "crcycd")
    private String crcycd;//币种
    @JsonProperty(value = "sttntp")
    private String sttntp;//交易类型
    @JsonProperty(value = "stmtdt")
    private String stmtdt;//账单年月
    @JsonProperty(value = "frtrow")
    private String frtrow;//开始位置
    @JsonProperty(value = "lstrow")
    private String lstrow;//结束位置

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getSttntp() {
        return sttntp;
    }

    public void setSttntp(String sttntp) {
        this.sttntp = sttntp;
    }

    public String getStmtdt() {
        return stmtdt;
    }

    public void setStmtdt(String stmtdt) {
        this.stmtdt = stmtdt;
    }

    public String getFrtrow() {
        return frtrow;
    }

    public void setFrtrow(String frtrow) {
        this.frtrow = frtrow;
    }

    public String getLstrow() {
        return lstrow;
    }

    public void setLstrow(String lstrow) {
        this.lstrow = lstrow;
    }

    @Override
    public String toString() {
        return "D12011ReqDto{" +
                "cardno='" + cardno + '\'' +
                "crcycd='" + crcycd + '\'' +
                "sttntp='" + sttntp + '\'' +
                "stmtdt='" + stmtdt + '\'' +
                "frtrow='" + frtrow + '\'' +
                "lstrow='" + lstrow + '\'' +
                '}';
    }
}  
