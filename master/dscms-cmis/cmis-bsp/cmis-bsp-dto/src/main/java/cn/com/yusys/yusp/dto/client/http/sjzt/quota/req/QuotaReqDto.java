package cn.com.yusys.yusp.dto.client.http.sjzt.quota.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/8/30 21:11:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/8/30 21:11
 * @since 2021/8/30 21:11
 */
@JsonPropertyOrder(alphabetic = true)
public class QuotaReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    @Override
    public String toString() {
        return "QuotaReqDto{" +
                "cus_id='" + cus_id + '\'' +
                '}';
    }
}
