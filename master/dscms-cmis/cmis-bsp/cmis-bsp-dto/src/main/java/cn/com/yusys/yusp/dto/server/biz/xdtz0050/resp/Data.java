package cn.com.yusys.yusp.dto.server.biz.xdtz0050.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/21 20:53:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/21 20:53
 * @since 2021/5/21 20:53
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "deletableFlag")
    private String deletableFlag;//可删除标志
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getDeletableFlag() {
        return deletableFlag;
    }

    public void setDeletableFlag(String deletableFlag) {
        this.deletableFlag = deletableFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xdtz0050RespDto{" +
                "deletableFlag='" + deletableFlag + '\'' +
                "opMsg='" + opMsg + '\'' +
                '}';
    }
}
