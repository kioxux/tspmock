package cn.com.yusys.yusp.dto.server.biz.xdtz0060.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询我行有未结清贷款
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isOverdueOrBadDebts")
    private String isOverdueOrBadDebts;//结果标识 Y有 N无

    public String getIsOverdueOrBadDebts() {
        return isOverdueOrBadDebts;
    }

    public void setIsOverdueOrBadDebts(String isOverdueOrBadDebts) {
        this.isOverdueOrBadDebts = isOverdueOrBadDebts;
    }

    @Override
    public String toString() {
        return "Data{" +
                "isOverdueOrBadDebts='" + isOverdueOrBadDebts + '\'' +
                '}';
    }
}
