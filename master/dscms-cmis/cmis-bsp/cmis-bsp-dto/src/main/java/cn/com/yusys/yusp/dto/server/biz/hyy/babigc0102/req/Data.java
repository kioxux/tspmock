package cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：获取抵押信息详情
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "mortgagor_name")
    private String mortgagor_name;//抵押人名称
    @JsonProperty(value = "page")
    private Integer page;//页码
    @JsonProperty(value = "size")
    private Integer size;//每页记录数

    public String getMortgagor_name() {
        return mortgagor_name;
    }

    public void setMortgagor_name(String mortgagor_name) {
        this.mortgagor_name = mortgagor_name;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Data{" +
                "mortgagor_name=" + mortgagor_name +
                "page=" + page +
                "size=" + size +
                '}';
    }
}
