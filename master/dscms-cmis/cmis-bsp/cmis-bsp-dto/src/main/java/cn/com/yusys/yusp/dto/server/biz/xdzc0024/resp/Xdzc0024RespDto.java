package cn.com.yusys.yusp.dto.server.biz.xdzc0024.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：发票补录
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0024RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdzc0024RespDto{" +
                "data=" + data +
                '}';
    }
}
