package cn.com.yusys.yusp.dto.client.esb.core.ln3169;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：资产证券化处理文件导入
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3169RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "zichculi")
    private String zichculi;//资产处理类型
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "lstzrjj")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3169.Lstzrjj> lstzrjj;//借据列表

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getZichculi() {
        return zichculi;
    }

    public void setZichculi(String zichculi) {
        this.zichculi = zichculi;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public List<Lstzrjj> getLstzrjj() {
        return lstzrjj;
    }

    public void setLstzrjj(List<Lstzrjj> lstzrjj) {
        this.lstzrjj = lstzrjj;
    }

    @Override
    public String toString() {
        return "Ln3169RespDto{" +
                "jiaoyirq='" + jiaoyirq + '\'' +
                ", zichculi='" + zichculi + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", lstzrjj=" + lstzrjj +
                '}';
    }
}
