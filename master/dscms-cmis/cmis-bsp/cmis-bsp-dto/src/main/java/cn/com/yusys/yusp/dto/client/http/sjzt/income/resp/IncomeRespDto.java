package cn.com.yusys.yusp.dto.client.http.sjzt.income.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/20 18:34
 * @since 2021/8/20 18:34
 */
public class IncomeRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "detail")
    private Detail detail;//明细信息
    @JsonProperty(value = "aggregate")
    private java.util.List<Aggregate> aggregate;//汇总信息

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public List<Aggregate> getAggregate() {
        return aggregate;
    }

    public void setAggregate(List<Aggregate> aggregate) {
        this.aggregate = aggregate;
    }

    @Override
    public String toString() {
        return "IncomeRespDto{" +
                "detail=" + detail +
                ", aggregate=" + aggregate +
                '}';
    }
}
