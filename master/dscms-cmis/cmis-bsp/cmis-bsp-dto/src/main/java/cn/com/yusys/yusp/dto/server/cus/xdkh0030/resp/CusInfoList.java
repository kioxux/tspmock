package cn.com.yusys.yusp.dto.server.cus.xdkh0030.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：公司客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CusInfoList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "cusType")
    private String cusType;//客户类型
    @JsonProperty(value = "indusCode")
    private String indusCode;//所属国标行业
    @JsonProperty(value = "conBuildDate")
    private String conBuildDate;//企业成立日期
    @JsonProperty(value = "isSteelTrade")
    private String isSteelTrade;//是否钢贸行业
    @JsonProperty(value = "regStateCode")
    private String regStateCode;//注册地行政区划代码
    @JsonProperty(value = "finrepType")
    private String finrepType;//财务报表类型
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理编号
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//主管客户经理所属机构编号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getIndusCode() {
        return indusCode;
    }

    public void setIndusCode(String indusCode) {
        this.indusCode = indusCode;
    }

    public String getConBuildDate() {
        return conBuildDate;
    }

    public void setConBuildDate(String conBuildDate) {
        this.conBuildDate = conBuildDate;
    }

    public String getIsSteelTrade() {
        return isSteelTrade;
    }

    public void setIsSteelTrade(String isSteelTrade) {
        this.isSteelTrade = isSteelTrade;
    }

    public String getRegStateCode() {
        return regStateCode;
    }

    public void setRegStateCode(String regStateCode) {
        this.regStateCode = regStateCode;
    }

    public String getFinrepType() {
        return finrepType;
    }

    public void setFinrepType(String finrepType) {
        this.finrepType = finrepType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    @Override
    public String toString() {
        return "CusInfoList{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", cusType='" + cusType + '\'' +
                ", indusCode='" + indusCode + '\'' +
                ", conBuildDate='" + conBuildDate + '\'' +
                ", isSteelTrade='" + isSteelTrade + '\'' +
                ", regStateCode='" + regStateCode + '\'' +
                ", finrepType='" + finrepType + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                '}';
    }
}