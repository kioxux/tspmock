package cn.com.yusys.yusp.dto.client.esb.core.ln3030;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款账户联名
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkzhlm implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehugxlx")
    private String kehugxlx;//客户关系类型
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称

    public String getKehugxlx() {
        return kehugxlx;
    }

    public void setKehugxlx(String kehugxlx) {
        this.kehugxlx = kehugxlx;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    @Override
    public String toString() {
        return "Lstdkzhlm{" +
                "kehugxlx='" + kehugxlx + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                '}';
    }
}  
