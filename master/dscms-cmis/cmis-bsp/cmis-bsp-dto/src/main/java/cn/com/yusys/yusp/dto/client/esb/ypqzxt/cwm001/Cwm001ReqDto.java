package cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * 请求DTO：本异地借阅出库接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Cwm001ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "userName")
    private String userName; // 柜员名称

    @JsonProperty(value = "orgcode")
    private String orgcode; // 操作员机构号

    @JsonProperty(value = "orgname")
    private String orgname; // 操作员机构名称

    @JsonProperty(value = "isyd")
    private String isyd; // 是否异地支行

    @JsonProperty(value = "backtime")
    private String backtime; // 归还时间

    @JsonProperty(value = "list")
    private List<ListArrayInfo> list; // 循环节点

    public String getIsyd() {
        return isyd;
    }

    public void setIsyd(String isyd) {
        this.isyd = isyd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getBacktime() {
        return backtime;
    }

    public void setBacktime(String backtime) {
        this.backtime = backtime;
    }

    public List<ListArrayInfo> getList() {
        return list;
    }

    public void setList(List<ListArrayInfo> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Cwm001ReqDto{" +
                "userName='" + userName + '\'' +
                ", orgcode='" + orgcode + '\'' +
                ", orgname='" + orgname + '\'' +
                ", isyd='" + isyd + '\'' +
                ", backtime='" + backtime + '\'' +
                ", list=" + list +
                '}';
    }
}
