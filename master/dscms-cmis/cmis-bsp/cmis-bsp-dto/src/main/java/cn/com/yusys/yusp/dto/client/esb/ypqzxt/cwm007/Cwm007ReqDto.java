package cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：押品状态查询接口
 *
 * @author dumd
 * @version 1.0
 * @since 2021年10月18日 下午4:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Cwm007ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty("userName")
    private String userName; // 柜员姓名
    @JsonProperty("gageId")
    private String gageId; // 核心担保品编号

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGageId() {
        return gageId;
    }

    public void setGageId(String gageId) {
        this.gageId = gageId;
    }

    @Override
    public String toString() {
        return "Cwm007ReqDto{" +
                "userName='" + userName + '\'' +
                ", gageId='" + gageId + '\'' +
                '}';
    }
}