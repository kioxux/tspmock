package cn.com.yusys.yusp.dto.server.biz.xdtz0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/21 14:51:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 14:51
 * @since 2021/5/21 14:51
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bailAcct")
    private String bailAcct;//保证金账户
    @JsonProperty(value = "securityAmt")
    private BigDecimal securityAmt;//保证金金额
    @JsonProperty(value = "intMode")
    private String intMode;//计息方式
    @JsonProperty(value = "bailIntDepAcctNo")
    private String bailIntDepAcctNo;//保证金利息存入账号
    @JsonProperty(value = "bailExchgRate")
    private BigDecimal bailExchgRate;//保证金汇率
    @JsonProperty(value = "cusSettlAcct")
    private String cusSettlAcct;//客户结算账户
    @JsonProperty(value = "cretQuotationExchgRate")
    private BigDecimal cretQuotationExchgRate;//信贷牌价汇率

    public String getBailAcct() {
        return bailAcct;
    }

    public void setBailAcct(String bailAcct) {
        this.bailAcct = bailAcct;
    }

    public BigDecimal getSecurityAmt() {
        return securityAmt;
    }

    public void setSecurityAmt(BigDecimal securityAmt) {
        this.securityAmt = securityAmt;
    }

    public String getIntMode() {
        return intMode;
    }

    public void setIntMode(String intMode) {
        this.intMode = intMode;
    }

    public String getBailIntDepAcctNo() {
        return bailIntDepAcctNo;
    }

    public void setBailIntDepAcctNo(String bailIntDepAcctNo) {
        this.bailIntDepAcctNo = bailIntDepAcctNo;
    }

    public BigDecimal getBailExchgRate() {
        return bailExchgRate;
    }

    public void setBailExchgRate(BigDecimal bailExchgRate) {
        this.bailExchgRate = bailExchgRate;
    }

    public String getCusSettlAcct() {
        return cusSettlAcct;
    }

    public void setCusSettlAcct(String cusSettlAcct) {
        this.cusSettlAcct = cusSettlAcct;
    }

    public BigDecimal getCretQuotationExchgRate() {
        return cretQuotationExchgRate;
    }

    public void setCretQuotationExchgRate(BigDecimal cretQuotationExchgRate) {
        this.cretQuotationExchgRate = cretQuotationExchgRate;
    }

    @Override
    public String toString() {
        return "List{" +
                "bailAcct='" + bailAcct + '\'' +
                ", securityAmt=" + securityAmt +
                ", intMode='" + intMode + '\'' +
                ", bailIntDepAcctNo='" + bailIntDepAcctNo + '\'' +
                ", bailExchgRate=" + bailExchgRate +
                ", cusSettlAcct='" + cusSettlAcct + '\'' +
                ", cretQuotationExchgRate=" + cretQuotationExchgRate +
                '}';
    }
}
