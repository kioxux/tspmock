package cn.com.yusys.yusp.dto.server.biz.xdtz0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：在查询经营性贷款借据信息
 *
 * @author chenyong
 * @version 1.0
 */

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billList")
    private java.util.List<BillList> billList;

    public java.util.List<BillList> getBillList() {
        return billList;
    }

    public void setBillList(java.util.List<BillList> billList) {
        this.billList = billList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billList=" + billList +
                '}';
    }
}
