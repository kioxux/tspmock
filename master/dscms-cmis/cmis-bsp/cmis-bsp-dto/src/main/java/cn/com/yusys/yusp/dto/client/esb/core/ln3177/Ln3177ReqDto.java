package cn.com.yusys.yusp.dto.client.esb.core.ln3177;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：资产转让付款状态维护
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3177ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "jiejuhao")
    private String jiejuhao;//借据号
    @JsonProperty(value = "xuhaoooo")
    private Integer xuhaoooo;//序号
    @JsonProperty(value = "fukuanrq")
    private String fukuanrq;//付款日期
    @JsonProperty(value = "zrfukzht")
    private String zrfukzht;//转让付款状态

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getFukuanrq() {
        return fukuanrq;
    }

    public void setFukuanrq(String fukuanrq) {
        this.fukuanrq = fukuanrq;
    }

    public String getZrfukzht() {
        return zrfukzht;
    }

    public void setZrfukzht(String zrfukzht) {
        this.zrfukzht = zrfukzht;
    }

    @Override
    public String toString() {
        return "Ln3177ReqDto{" +
                "xieybhao='" + xieybhao + '\'' +
                "jiejuhao='" + jiejuhao + '\'' +
                "xuhaoooo='" + xuhaoooo + '\'' +
                "fukuanrq='" + fukuanrq + '\'' +
                "zrfukzht='" + zrfukzht + '\'' +
                '}';
    }
}  
