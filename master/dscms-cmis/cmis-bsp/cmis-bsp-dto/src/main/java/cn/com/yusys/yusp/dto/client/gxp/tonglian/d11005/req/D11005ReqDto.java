package cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：d11005
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D11005ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "idtfno")
    private String idtfno;//证件号码
    @JsonProperty(value = "idtftp")
    private String idtftp;//证件类型
    @JsonProperty(value = "cardfc")
    private String cardfc;//卡面代码
    @JsonProperty(value = "cardtn")
    private String cardtn;//卡片寄送地址标志
    @JsonProperty(value = "apptyp")
    private String apptyp;//申请类型
    @JsonProperty(value = "prodtp")
    private String prodtp;//卡产品代码
    @JsonProperty(value = "applmt")
    private String applmt;//申请额度
    @JsonProperty(value = "bkbrch")
    private String bkbrch;//受理网点\发卡网点
    @JsonProperty(value = "custtp")
    private String custtp;//客户类型
    @JsonProperty(value = "spretp")
    private String spretp;//推广渠道
    @JsonProperty(value = "ctmgna")
    private String ctmgna;//推广人姓名
    @JsonProperty(value = "ctmgno")
    private String ctmgno;//推广人编号
    @JsonProperty(value = "ctmgtl")
    private String ctmgtl;//推广人手机号
    @JsonProperty(value = "source")
    private String source;//申请渠道
    @JsonProperty(value = "taskip")
    private String taskip;//影像批次号
    @JsonProperty(value = "idtfdt")
    private String idtfdt;//证件到期日
    @JsonProperty(value = "enname")
    private String enname;//凸印姓-姓名拼音
    @JsonProperty(value = "mtstat")
    private String mtstat;//婚姻状况
    @JsonProperty(value = "forver")
    private String forver;//是否永久居住
    @JsonProperty(value = "qlftin")
    private String qlftin;//教育状况
    @JsonProperty(value = "brtday")
    private String brtday;//生日
    @JsonProperty(value = "corpnm")
    private String corpnm;//公司名称
    @JsonProperty(value = "emplvr")
    private String emplvr;//职务
    @JsonProperty(value = "gender")
    private String gender;//性别
    @JsonProperty(value = "custnm")
    private String custnm;//姓名
    @JsonProperty(value = "email")
    private String email;//电子邮箱
    @JsonProperty(value = "ntnaty")
    private String ntnaty;//国籍
    @JsonProperty(value = "ocpatn")
    private String ocpatn;//职业
    @JsonProperty(value = "hmphon")
    private String hmphon;//家庭电话
    @JsonProperty(value = "hourst")
    private String hourst;//住宅状况\住宅持有类型
    @JsonProperty(value = "income")
    private String income;//年收入
    @JsonProperty(value = "teleno")
    private String teleno;//移动电话
    @JsonProperty(value = "hmprov")
    private String hmprov;//家庭所在省
    @JsonProperty(value = "hmcity")
    private String hmcity;//家庭所在市
    @JsonProperty(value = "hmqury")
    private String hmqury;//家庭所在区县
    @JsonProperty(value = "hmaddr")
    private String hmaddr;//家庭地址
    @JsonProperty(value = "hmpost")
    private String hmpost;//家庭住宅邮编
    @JsonProperty(value = "titlal")
    private String titlal;//职称
    @JsonProperty(value = "empstr")
    private String empstr;//公司性质
    @JsonProperty(value = "emptpy")
    private String emptpy;//公司行业类别
    @JsonProperty(value = "coprov")
    private String coprov;//公司所在省
    @JsonProperty(value = "cocity")
    private String cocity;//公司所在市
    @JsonProperty(value = "coqury")
    private String coqury;//公司所在区/县
    @JsonProperty(value = "coaddr")
    private String coaddr;//公司地址
    @JsonProperty(value = "copost")
    private String copost;//公司邮编
    @JsonProperty(value = "cophon")
    private String cophon;//公司电话
    @JsonProperty(value = "cmname")
    private String cmname;//联系人中文姓名
    @JsonProperty(value = "cmrela")
    private String cmrela;//联系人与申请人关系
    @JsonProperty(value = "cmmobi")
    private String cmmobi;//联系人移动电话
    @JsonProperty(value = "otname")
    private String otname;//其他联系人中文姓名
    @JsonProperty(value = "otrela")
    private String otrela;//其他联系人与申请人关系
    @JsonProperty(value = "otmobi")
    private String otmobi;//其他联系人移动电话
    @JsonProperty(value = "answer")
    private String answer;//预留问题
    @JsonProperty(value = "questi")
    private String questi;//预留答案
    @JsonProperty(value = "islong")
    private String islong;//证件是否永久有效
    @JsonProperty(value = "fkname")
    private String fkname;//附卡持卡人姓名
    @JsonProperty(value = "fkidtp")
    private String fkidtp;//附卡证件类型
    @JsonProperty(value = "fkidno")
    private String fkidno;//附卡证件号码
    @JsonProperty(value = "fktobs")
    private String fktobs;//与主卡持卡人关系
    @JsonProperty(value = "fkbrdt")
    private String fkbrdt;//附卡生日
    @JsonProperty(value = "fksext")
    private String fksext;//附卡性别
    @JsonProperty(value = "fkiddt")
    private String fkiddt;//附卡证件到期日
    @JsonProperty(value = "fknaty")
    private String fknaty;//附卡国籍
    @JsonProperty(value = "fktele")
    private String fktele;//附卡家庭电话
    @JsonProperty(value = "fkmobi")
    private String fkmobi;//附卡移动电话
    @JsonProperty(value = "fkprov")
    private String fkprov;//附卡家庭所在省
    @JsonProperty(value = "fkcity")
    private String fkcity;//附卡家庭所在市
    @JsonProperty(value = "fkqury")
    private String fkqury;//附卡家庭所在区县
    @JsonProperty(value = "fkaddr")
    private String fkaddr;//附卡家庭地址
    @JsonProperty(value = "fkpost")
    private String fkpost;//附卡家庭住宅邮编
    @JsonProperty(value = "foprov")
    private String foprov;//附卡公司所在省
    @JsonProperty(value = "focity")
    private String focity;//附卡公司所在市
    @JsonProperty(value = "foqury")
    private String foqury;//附卡公司所在区/县
    @JsonProperty(value = "foaddr")
    private String foaddr;//附卡公司地址
    @JsonProperty(value = "fopost")
    private String fopost;//附卡公司邮编
    @JsonProperty(value = "folong")
    private String folong;//附卡证件是否长期有效
    @JsonProperty(value = "ddind")
    private String ddind;//约定还款类型
    @JsonProperty(value = "isjstj")
    private String isjstj;//是否同意卡片自动降级
    @JsonProperty(value = "carsst")
    private String carsst;//车辆状况
    @JsonProperty(value = "carval")
    private BigDecimal carval;//车辆价值
    @JsonProperty(value = "qssext")
    private String qssext;//联系人性别
    @JsonProperty(value = "qsphon")
    private String qsphon;//联系人联系电话
    @JsonProperty(value = "qtsext")
    private String qtsext;//其他联系人性别
    @JsonProperty(value = "qtphon")
    private String qtphon;//其他联系人联系电话
    @JsonProperty(value = "fkcdno")
    private String fkcdno;//申请人主卡卡号
    @JsonProperty(value = "dbacna")
    private String dbacna;//无备注
    @JsonProperty(value = "dbkact")
    private String dbkact;//约定还款扣款账号
    @JsonProperty(value = "taskid")
    private String taskid;//影像批次号
    @JsonProperty(value = "fkcatn")
    private String fkcatn;//附卡卡片寄送地址标志
    @JsonProperty(value = "submit")
    private String submit;//提交操作
    @JsonProperty(value = "idress")
    private String idress;//发证机关所在地址
    @JsonProperty(value = "fkennm")
    private String fkennm;
    @JsonProperty(value = "fachno")
    private String fachno;//附卡编号
    @JsonProperty(value = "fxdage")
    private String fxdage;//附卡年龄
    @JsonProperty(value = "maritu")
    private String maritu;//附卡婚姻状况
    @JsonProperty(value = "fcntry")
    private String fcntry;//附卡是否永久居住
    @JsonProperty(value = "fccode")
    private String fccode;//附卡永久居住地国家代码
    @JsonProperty(value = "fqtion")
    private String fqtion;//附卡教育状况
    @JsonProperty(value = "fegree")
    private String fegree;//附卡学位
    @JsonProperty(value = "fdress")
    private String fdress;//附卡发证机关所在地址
    @JsonProperty(value = "ficome")
    private String ficome;//附卡年收入
    @JsonProperty(value = "fmlogo")
    private String fmlogo;//附卡母亲姓氏
    @JsonProperty(value = "femail")
    private String femail;//附卡电子邮箱
    @JsonProperty(value = "fhouse")
    private String fhouse;//附卡住宅状况
    @JsonProperty(value = "fshome")
    private String fshome;//附卡现住址居住起始年月
    @JsonProperty(value = "fccity")
    private String fccity;//附卡家庭国家代码
    @JsonProperty(value = "fhpedn")
    private String fhpedn;//附卡家庭电话区号
    @JsonProperty(value = "fvenue")
    private String fvenue;//附卡家庭年收入
    @JsonProperty(value = "fbflag")
    private String fbflag;//附卡是否行内员工
    @JsonProperty(value = "fbanno")
    private String fbanno;//附卡本行员工号
    @JsonProperty(value = "fcname")
    private String fcname;//附卡公司名称
    @JsonProperty(value = "factry")
    private String factry;//附卡公司国家代码
    @JsonProperty(value = "fcture")
    private String fcture;//附卡公司性质
    @JsonProperty(value = "fetype")
    private String fetype;//附卡公司行业类别
    @JsonProperty(value = "fphone")
    private String fphone;//附卡公司电话
    @JsonProperty(value = "fcofax")
    private String fcofax;//附卡公司传真
    @JsonProperty(value = "fefrom")
    private String fefrom;//附卡现单位工作起始年月
    @JsonProperty(value = "fpment")
    private String fpment;//附卡任职部门
    @JsonProperty(value = "fation")
    private String fation;//附卡职业
    @JsonProperty(value = "fnical")
    private String fnical;//附卡职称
    @JsonProperty(value = "femppo")
    private String femppo;//附卡职务
    @JsonProperty(value = "fpjuse")
    private String fpjuse;//附卡是否彩照卡
    @JsonProperty(value = "fverif")
    private String fverif;//附卡是否消费凭密
    @JsonProperty(value = "fstatu")
    private String fstatu;//附卡是否在职
    @JsonProperty(value = "fstaby")
    private String fstaby;//附卡工作稳定性
    @JsonProperty(value = "fotask")
    private String fotask;//附卡预留问题
    @JsonProperty(value = "fanswe")
    private String fanswe;//附卡预留答案
    @JsonProperty(value = "fappno")
    private String fappno;//附卡申请编号_外部送入
    @JsonProperty(value = "fcusno")
    private String fcusno;//附卡行内客户号
    @JsonProperty(value = "fsmamt")
    private String fsmamt;//附卡小额免密
    @JsonProperty(value = "fcatch")
    private String fcatch;//附卡领卡方式
    @JsonProperty(value = "ffanch")
    private String ffanch;//附卡领卡网点
    @JsonProperty(value = "ficity")
    private String ficity;//发卡城市
    @JsonProperty(value = "fyears")
    private String fyears;//居住年限
    @JsonProperty(value = "fksddt")
    private String fksddt;//附卡证件起始日
    @JsonProperty(value = "iddate")
    private String iddate;//证件起始日
    @JsonProperty(value = "cardno")
    private String cardno;//自选卡号
    @JsonProperty(value = "secard")
    private String secard;//是否自选卡号
    @JsonProperty(value = "fscano")
    private String fscano;//附卡自选卡号
    @JsonProperty(value = "ficard")
    private String ficard;//附卡是否自选卡号
    @JsonProperty(value = "nation")
    private String nation;
    @JsonProperty(value = "appnal")
    private String appnal;//外围编号
    @JsonProperty(value = "appnaf")
    private String appnaf;//附卡外围编号
    @JsonProperty(value = "zjgcre")
    private String zjgcre;
    @JsonProperty(value = "fntion")//附卡名族
    private String fntion;
    public String getFntion() {
        return fntion;
    }

    public void setFntion(String fntion) {
        this.fntion = fntion;
    }
    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    @JsonProperty(value = "bankid")//行内客户号
    private String bankid;

    public String getQuestr() {
        return questr;
    }

    public void setQuestr(String questr) {
        this.questr = questr;
    }

    @JsonProperty(value = "questr")
    private String questr;

    public String getDarate() {
        return darate;
    }

    public void setDarate(String darate) {
        this.darate = darate;
    }

    @JsonProperty(value = "darate")
    private String darate;

    public String getFscano() {
        return fscano;
    }

    public void setFscano(String fscano) {
        this.fscano = fscano;
    }

    public String getFicard() {
        return ficard;
    }

    public void setFicard(String ficard) {
        this.ficard = ficard;
    }


    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getSecard() {
        return secard;
    }

    public void setSecard(String secard) {
        this.secard = secard;
    }


    public String getIddate() {
        return iddate;
    }

    public void setIddate(String iddate) {
        this.iddate = iddate;
    }


    public String getFksddt() {
        return fksddt;
    }

    public void setFksddt(String fksddt) {
        this.fksddt = fksddt;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getCardfc() {
        return cardfc;
    }

    public void setCardfc(String cardfc) {
        this.cardfc = cardfc;
    }

    public String getCardtn() {
        return cardtn;
    }

    public void setCardtn(String cardtn) {
        this.cardtn = cardtn;
    }

    public String getApptyp() {
        return apptyp;
    }

    public void setApptyp(String apptyp) {
        this.apptyp = apptyp;
    }

    public String getProdtp() {
        return prodtp;
    }

    public void setProdtp(String prodtp) {
        this.prodtp = prodtp;
    }

    public String getApplmt() {
        return applmt;
    }

    public void setApplmt(String applmt) {
        this.applmt = applmt;
    }

    public String getBkbrch() {
        return bkbrch;
    }

    public void setBkbrch(String bkbrch) {
        this.bkbrch = bkbrch;
    }

    public String getCusttp() {
        return custtp;
    }

    public void setCusttp(String custtp) {
        this.custtp = custtp;
    }

    public String getSpretp() {
        return spretp;
    }

    public void setSpretp(String spretp) {
        this.spretp = spretp;
    }

    public String getCtmgna() {
        return ctmgna;
    }

    public void setCtmgna(String ctmgna) {
        this.ctmgna = ctmgna;
    }

    public String getCtmgno() {
        return ctmgno;
    }

    public void setCtmgno(String ctmgno) {
        this.ctmgno = ctmgno;
    }

    public String getCtmgtl() {
        return ctmgtl;
    }

    public void setCtmgtl(String ctmgtl) {
        this.ctmgtl = ctmgtl;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTaskip() {
        return taskip;
    }

    public void setTaskip(String taskip) {
        this.taskip = taskip;
    }

    public String getIdtfdt() {
        return idtfdt;
    }

    public void setIdtfdt(String idtfdt) {
        this.idtfdt = idtfdt;
    }

    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    public String getMtstat() {
        return mtstat;
    }

    public void setMtstat(String mtstat) {
        this.mtstat = mtstat;
    }

    public String getForver() {
        return forver;
    }

    public void setForver(String forver) {
        this.forver = forver;
    }

    public String getQlftin() {
        return qlftin;
    }

    public void setQlftin(String qlftin) {
        this.qlftin = qlftin;
    }

    public String getBrtday() {
        return brtday;
    }

    public void setBrtday(String brtday) {
        this.brtday = brtday;
    }

    public String getCorpnm() {
        return corpnm;
    }

    public void setCorpnm(String corpnm) {
        this.corpnm = corpnm;
    }

    public String getEmplvr() {
        return emplvr;
    }

    public void setEmplvr(String emplvr) {
        this.emplvr = emplvr;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNtnaty() {
        return ntnaty;
    }

    public void setNtnaty(String ntnaty) {
        this.ntnaty = ntnaty;
    }

    public String getOcpatn() {
        return ocpatn;
    }

    public void setOcpatn(String ocpatn) {
        this.ocpatn = ocpatn;
    }

    public String getHmphon() {
        return hmphon;
    }

    public void setHmphon(String hmphon) {
        this.hmphon = hmphon;
    }

    public String getHourst() {
        return hourst;
    }

    public void setHourst(String hourst) {
        this.hourst = hourst;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getTeleno() {
        return teleno;
    }

    public void setTeleno(String teleno) {
        this.teleno = teleno;
    }

    public String getHmprov() {
        return hmprov;
    }

    public void setHmprov(String hmprov) {
        this.hmprov = hmprov;
    }

    public String getHmcity() {
        return hmcity;
    }

    public void setHmcity(String hmcity) {
        this.hmcity = hmcity;
    }

    public String getHmqury() {
        return hmqury;
    }

    public void setHmqury(String hmqury) {
        this.hmqury = hmqury;
    }

    public String getHmaddr() {
        return hmaddr;
    }

    public void setHmaddr(String hmaddr) {
        this.hmaddr = hmaddr;
    }

    public String getHmpost() {
        return hmpost;
    }

    public void setHmpost(String hmpost) {
        this.hmpost = hmpost;
    }

    public String getTitlal() {
        return titlal;
    }

    public void setTitlal(String titlal) {
        this.titlal = titlal;
    }

    public String getEmpstr() {
        return empstr;
    }

    public void setEmpstr(String empstr) {
        this.empstr = empstr;
    }

    public String getEmptpy() {
        return emptpy;
    }

    public void setEmptpy(String emptpy) {
        this.emptpy = emptpy;
    }

    public String getCoprov() {
        return coprov;
    }

    public void setCoprov(String coprov) {
        this.coprov = coprov;
    }

    public String getCocity() {
        return cocity;
    }

    public void setCocity(String cocity) {
        this.cocity = cocity;
    }

    public String getCoqury() {
        return coqury;
    }

    public void setCoqury(String coqury) {
        this.coqury = coqury;
    }

    public String getCoaddr() {
        return coaddr;
    }

    public void setCoaddr(String coaddr) {
        this.coaddr = coaddr;
    }

    public String getCopost() {
        return copost;
    }

    public void setCopost(String copost) {
        this.copost = copost;
    }

    public String getCophon() {
        return cophon;
    }

    public void setCophon(String cophon) {
        this.cophon = cophon;
    }

    public String getCmname() {
        return cmname;
    }

    public void setCmname(String cmname) {
        this.cmname = cmname;
    }

    public String getCmrela() {
        return cmrela;
    }

    public void setCmrela(String cmrela) {
        this.cmrela = cmrela;
    }

    public String getCmmobi() {
        return cmmobi;
    }

    public void setCmmobi(String cmmobi) {
        this.cmmobi = cmmobi;
    }

    public String getOtname() {
        return otname;
    }

    public void setOtname(String otname) {
        this.otname = otname;
    }

    public String getOtrela() {
        return otrela;
    }

    public void setOtrela(String otrela) {
        this.otrela = otrela;
    }

    public String getOtmobi() {
        return otmobi;
    }

    public void setOtmobi(String otmobi) {
        this.otmobi = otmobi;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getQuesti() {
        return questi;
    }

    public void setQuesti(String questi) {
        this.questi = questi;
    }

    public String getIslong() {
        return islong;
    }

    public void setIslong(String islong) {
        this.islong = islong;
    }

    public String getFkname() {
        return fkname;
    }

    public void setFkname(String fkname) {
        this.fkname = fkname;
    }

    public String getFkidtp() {
        return fkidtp;
    }

    public void setFkidtp(String fkidtp) {
        this.fkidtp = fkidtp;
    }

    public String getFkidno() {
        return fkidno;
    }

    public void setFkidno(String fkidno) {
        this.fkidno = fkidno;
    }

    public String getFktobs() {
        return fktobs;
    }

    public void setFktobs(String fktobs) {
        this.fktobs = fktobs;
    }

    public String getFkbrdt() {
        return fkbrdt;
    }

    public void setFkbrdt(String fkbrdt) {
        this.fkbrdt = fkbrdt;
    }

    public String getFksext() {
        return fksext;
    }

    public void setFksext(String fksext) {
        this.fksext = fksext;
    }

    public String getFkiddt() {
        return fkiddt;
    }

    public void setFkiddt(String fkiddt) {
        this.fkiddt = fkiddt;
    }

    public String getFknaty() {
        return fknaty;
    }

    public void setFknaty(String fknaty) {
        this.fknaty = fknaty;
    }

    public String getFktele() {
        return fktele;
    }

    public void setFktele(String fktele) {
        this.fktele = fktele;
    }

    public String getFkmobi() {
        return fkmobi;
    }

    public void setFkmobi(String fkmobi) {
        this.fkmobi = fkmobi;
    }

    public String getFkprov() {
        return fkprov;
    }

    public void setFkprov(String fkprov) {
        this.fkprov = fkprov;
    }

    public String getFkcity() {
        return fkcity;
    }

    public void setFkcity(String fkcity) {
        this.fkcity = fkcity;
    }

    public String getFkqury() {
        return fkqury;
    }

    public void setFkqury(String fkqury) {
        this.fkqury = fkqury;
    }

    public String getFkaddr() {
        return fkaddr;
    }

    public void setFkaddr(String fkaddr) {
        this.fkaddr = fkaddr;
    }

    public String getFkpost() {
        return fkpost;
    }

    public void setFkpost(String fkpost) {
        this.fkpost = fkpost;
    }

    public String getFoprov() {
        return foprov;
    }

    public void setFoprov(String foprov) {
        this.foprov = foprov;
    }

    public String getFocity() {
        return focity;
    }

    public void setFocity(String focity) {
        this.focity = focity;
    }

    public String getFoqury() {
        return foqury;
    }

    public void setFoqury(String foqury) {
        this.foqury = foqury;
    }

    public String getFoaddr() {
        return foaddr;
    }

    public void setFoaddr(String foaddr) {
        this.foaddr = foaddr;
    }

    public String getFopost() {
        return fopost;
    }

    public void setFopost(String fopost) {
        this.fopost = fopost;
    }

    public String getFolong() {
        return folong;
    }

    public void setFolong(String folong) {
        this.folong = folong;
    }

    public String getDdind() {
        return ddind;
    }

    public void setDdind(String ddind) {
        this.ddind = ddind;
    }

    public String getIsjstj() {
        return isjstj;
    }

    public void setIsjstj(String isjstj) {
        this.isjstj = isjstj;
    }

    public String getCarsst() {
        return carsst;
    }

    public void setCarsst(String carsst) {
        this.carsst = carsst;
    }

    public BigDecimal getCarval() {
        return carval;
    }

    public void setCarval(BigDecimal carval) {
        this.carval = carval;
    }

    public String getQssext() {
        return qssext;
    }

    public void setQssext(String qssext) {
        this.qssext = qssext;
    }

    public String getQsphon() {
        return qsphon;
    }

    public void setQsphon(String qsphon) {
        this.qsphon = qsphon;
    }

    public String getQtsext() {
        return qtsext;
    }

    public void setQtsext(String qtsext) {
        this.qtsext = qtsext;
    }

    public String getQtphon() {
        return qtphon;
    }

    public void setQtphon(String qtphon) {
        this.qtphon = qtphon;
    }

    public String getFkcdno() {
        return fkcdno;
    }

    public void setFkcdno(String fkcdno) {
        this.fkcdno = fkcdno;
    }

    public String getDbacna() {
        return dbacna;
    }

    public void setDbacna(String dbacna) {
        this.dbacna = dbacna;
    }

    public String getDbkact() {
        return dbkact;
    }

    public void setDbkact(String dbkact) {
        this.dbkact = dbkact;
    }

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid;
    }

    public String getFkcatn() {
        return fkcatn;
    }

    public void setFkcatn(String fkcatn) {
        this.fkcatn = fkcatn;
    }

    public String getSubmit() {
        return submit;
    }

    public void setSubmit(String submit) {
        this.submit = submit;
    }

    public String getIdress() {
        return idress;
    }

    public void setIdress(String idress) {
        this.idress = idress;
    }

    public String getFkennm() {
        return fkennm;
    }

    public void setFkennm(String fkennm) {
        this.fkennm = fkennm;
    }

    public String getFachno() {
        return fachno;
    }

    public void setFachno(String fachno) {
        this.fachno = fachno;
    }

    public String getFxdage() {
        return fxdage;
    }

    public void setFxdage(String fxdage) {
        this.fxdage = fxdage;
    }

    public String getMaritu() {
        return maritu;
    }

    public void setMaritu(String maritu) {
        this.maritu = maritu;
    }

    public String getFcntry() {
        return fcntry;
    }

    public void setFcntry(String fcntry) {
        this.fcntry = fcntry;
    }

    public String getFccode() {
        return fccode;
    }

    public void setFccode(String fccode) {
        this.fccode = fccode;
    }

    public String getFqtion() {
        return fqtion;
    }

    public void setFqtion(String fqtion) {
        this.fqtion = fqtion;
    }

    public String getFegree() {
        return fegree;
    }

    public void setFegree(String fegree) {
        this.fegree = fegree;
    }

    public String getFdress() {
        return fdress;
    }

    public void setFdress(String fdress) {
        this.fdress = fdress;
    }

    public String getFicome() {
        return ficome;
    }

    public void setFicome(String ficome) {
        this.ficome = ficome;
    }

    public String getFmlogo() {
        return fmlogo;
    }

    public void setFmlogo(String fmlogo) {
        this.fmlogo = fmlogo;
    }

    public String getFemail() {
        return femail;
    }

    public void setFemail(String femail) {
        this.femail = femail;
    }

    public String getFhouse() {
        return fhouse;
    }

    public void setFhouse(String fhouse) {
        this.fhouse = fhouse;
    }

    public String getFshome() {
        return fshome;
    }

    public void setFshome(String fshome) {
        this.fshome = fshome;
    }

    public String getFccity() {
        return fccity;
    }

    public void setFccity(String fccity) {
        this.fccity = fccity;
    }

    public String getFhpedn() {
        return fhpedn;
    }

    public void setFhpedn(String fhpedn) {
        this.fhpedn = fhpedn;
    }

    public String getFvenue() {
        return fvenue;
    }

    public void setFvenue(String fvenue) {
        this.fvenue = fvenue;
    }

    public String getFbflag() {
        return fbflag;
    }

    public void setFbflag(String fbflag) {
        this.fbflag = fbflag;
    }

    public String getFbanno() {
        return fbanno;
    }

    public void setFbanno(String fbanno) {
        this.fbanno = fbanno;
    }

    public String getFcname() {
        return fcname;
    }

    public void setFcname(String fcname) {
        this.fcname = fcname;
    }

    public String getFactry() {
        return factry;
    }

    public void setFactry(String factry) {
        this.factry = factry;
    }

    public String getFcture() {
        return fcture;
    }

    public void setFcture(String fcture) {
        this.fcture = fcture;
    }

    public String getFetype() {
        return fetype;
    }

    public void setFetype(String fetype) {
        this.fetype = fetype;
    }

    public String getFphone() {
        return fphone;
    }

    public void setFphone(String fphone) {
        this.fphone = fphone;
    }

    public String getFcofax() {
        return fcofax;
    }

    public void setFcofax(String fcofax) {
        this.fcofax = fcofax;
    }

    public String getFefrom() {
        return fefrom;
    }

    public void setFefrom(String fefrom) {
        this.fefrom = fefrom;
    }

    public String getFpment() {
        return fpment;
    }

    public void setFpment(String fpment) {
        this.fpment = fpment;
    }

    public String getFation() {
        return fation;
    }

    public void setFation(String fation) {
        this.fation = fation;
    }

    public String getFnical() {
        return fnical;
    }

    public void setFnical(String fnical) {
        this.fnical = fnical;
    }

    public String getFemppo() {
        return femppo;
    }

    public void setFemppo(String femppo) {
        this.femppo = femppo;
    }

    public String getFpjuse() {
        return fpjuse;
    }

    public void setFpjuse(String fpjuse) {
        this.fpjuse = fpjuse;
    }

    public String getFverif() {
        return fverif;
    }

    public void setFverif(String fverif) {
        this.fverif = fverif;
    }

    public String getFstatu() {
        return fstatu;
    }

    public void setFstatu(String fstatu) {
        this.fstatu = fstatu;
    }

    public String getFstaby() {
        return fstaby;
    }

    public void setFstaby(String fstaby) {
        this.fstaby = fstaby;
    }

    public String getFotask() {
        return fotask;
    }

    public void setFotask(String fotask) {
        this.fotask = fotask;
    }

    public String getFanswe() {
        return fanswe;
    }

    public void setFanswe(String fanswe) {
        this.fanswe = fanswe;
    }

    public String getFappno() {
        return fappno;
    }

    public void setFappno(String fappno) {
        this.fappno = fappno;
    }

    public String getFcusno() {
        return fcusno;
    }

    public void setFcusno(String fcusno) {
        this.fcusno = fcusno;
    }

    public String getFsmamt() {
        return fsmamt;
    }

    public void setFsmamt(String fsmamt) {
        this.fsmamt = fsmamt;
    }

    public String getFcatch() {
        return fcatch;
    }

    public void setFcatch(String fcatch) {
        this.fcatch = fcatch;
    }

    public String getFfanch() {
        return ffanch;
    }

    public void setFfanch(String ffanch) {
        this.ffanch = ffanch;
    }

    public String getFicity() {
        return ficity;
    }

    public void setFicity(String ficity) {
        this.ficity = ficity;
    }

    public String getFyears() {
        return fyears;
    }

    public void setFyears(String fyears) {
        this.fyears = fyears;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getAppnal() {
        return appnal;
    }

    public void setAppnal(String appnal) {
        this.appnal = appnal;
    }

    public String getAppnaf() {
        return appnaf;
    }

    public void setAppnaf(String appnaf) {
        this.appnaf = appnaf;
    }

    public String getZjgcre() {
        return zjgcre;
    }

    public void setZjgcre(String zjgcre) {
        this.zjgcre = zjgcre;
    }


    @Override
    public String toString() {
        return "D11005ReqDto{" +
                "idtfno='" + idtfno + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", cardfc='" + cardfc + '\'' +
                ", cardtn='" + cardtn + '\'' +
                ", apptyp='" + apptyp + '\'' +
                ", prodtp='" + prodtp + '\'' +
                ", applmt='" + applmt + '\'' +
                ", bkbrch='" + bkbrch + '\'' +
                ", custtp='" + custtp + '\'' +
                ", spretp='" + spretp + '\'' +
                ", ctmgna='" + ctmgna + '\'' +
                ", ctmgno='" + ctmgno + '\'' +
                ", ctmgtl='" + ctmgtl + '\'' +
                ", source='" + source + '\'' +
                ", taskip='" + taskip + '\'' +
                ", idtfdt='" + idtfdt + '\'' +
                ", enname='" + enname + '\'' +
                ", mtstat='" + mtstat + '\'' +
                ", forver='" + forver + '\'' +
                ", qlftin='" + qlftin + '\'' +
                ", brtday='" + brtday + '\'' +
                ", corpnm='" + corpnm + '\'' +
                ", emplvr='" + emplvr + '\'' +
                ", gender='" + gender + '\'' +
                ", custnm='" + custnm + '\'' +
                ", email='" + email + '\'' +
                ", ntnaty='" + ntnaty + '\'' +
                ", ocpatn='" + ocpatn + '\'' +
                ", hmphon='" + hmphon + '\'' +
                ", hourst='" + hourst + '\'' +
                ", income='" + income + '\'' +
                ", teleno='" + teleno + '\'' +
                ", hmprov='" + hmprov + '\'' +
                ", hmcity='" + hmcity + '\'' +
                ", hmqury='" + hmqury + '\'' +
                ", hmaddr='" + hmaddr + '\'' +
                ", hmpost='" + hmpost + '\'' +
                ", titlal='" + titlal + '\'' +
                ", empstr='" + empstr + '\'' +
                ", emptpy='" + emptpy + '\'' +
                ", coprov='" + coprov + '\'' +
                ", cocity='" + cocity + '\'' +
                ", coqury='" + coqury + '\'' +
                ", coaddr='" + coaddr + '\'' +
                ", copost='" + copost + '\'' +
                ", cophon='" + cophon + '\'' +
                ", cmname='" + cmname + '\'' +
                ", cmrela='" + cmrela + '\'' +
                ", cmmobi='" + cmmobi + '\'' +
                ", otname='" + otname + '\'' +
                ", otrela='" + otrela + '\'' +
                ", otmobi='" + otmobi + '\'' +
                ", answer='" + answer + '\'' +
                ", questi='" + questi + '\'' +
                ", islong='" + islong + '\'' +
                ", fkname='" + fkname + '\'' +
                ", fkidtp='" + fkidtp + '\'' +
                ", fkidno='" + fkidno + '\'' +
                ", fktobs='" + fktobs + '\'' +
                ", fkbrdt='" + fkbrdt + '\'' +
                ", fksext='" + fksext + '\'' +
                ", fkiddt='" + fkiddt + '\'' +
                ", fknaty='" + fknaty + '\'' +
                ", fktele='" + fktele + '\'' +
                ", fkmobi='" + fkmobi + '\'' +
                ", fkprov='" + fkprov + '\'' +
                ", fkcity='" + fkcity + '\'' +
                ", fkqury='" + fkqury + '\'' +
                ", fkaddr='" + fkaddr + '\'' +
                ", fkpost='" + fkpost + '\'' +
                ", foprov='" + foprov + '\'' +
                ", focity='" + focity + '\'' +
                ", foqury='" + foqury + '\'' +
                ", foaddr='" + foaddr + '\'' +
                ", fopost='" + fopost + '\'' +
                ", folong='" + folong + '\'' +
                ", ddind='" + ddind + '\'' +
                ", isjstj='" + isjstj + '\'' +
                ", carsst='" + carsst + '\'' +
                ", carval=" + carval +
                ", qssext='" + qssext + '\'' +
                ", qsphon='" + qsphon + '\'' +
                ", qtsext='" + qtsext + '\'' +
                ", qtphon='" + qtphon + '\'' +
                ", fkcdno='" + fkcdno + '\'' +
                ", dbacna='" + dbacna + '\'' +
                ", dbkact='" + dbkact + '\'' +
                ", taskid='" + taskid + '\'' +
                ", fkcatn='" + fkcatn + '\'' +
                ", submit='" + submit + '\'' +
                ", idress='" + idress + '\'' +
                ", fkennm='" + fkennm + '\'' +
                ", fachno='" + fachno + '\'' +
                ", fxdage='" + fxdage + '\'' +
                ", maritu='" + maritu + '\'' +
                ", fcntry='" + fcntry + '\'' +
                ", fccode='" + fccode + '\'' +
                ", fqtion='" + fqtion + '\'' +
                ", fegree='" + fegree + '\'' +
                ", fdress='" + fdress + '\'' +
                ", ficome='" + ficome + '\'' +
                ", fmlogo='" + fmlogo + '\'' +
                ", femail='" + femail + '\'' +
                ", fhouse='" + fhouse + '\'' +
                ", fshome='" + fshome + '\'' +
                ", fccity='" + fccity + '\'' +
                ", fhpedn='" + fhpedn + '\'' +
                ", fvenue='" + fvenue + '\'' +
                ", fbflag='" + fbflag + '\'' +
                ", fbanno='" + fbanno + '\'' +
                ", fcname='" + fcname + '\'' +
                ", factry='" + factry + '\'' +
                ", fcture='" + fcture + '\'' +
                ", fetype='" + fetype + '\'' +
                ", fphone='" + fphone + '\'' +
                ", fcofax='" + fcofax + '\'' +
                ", fefrom='" + fefrom + '\'' +
                ", fpment='" + fpment + '\'' +
                ", fation='" + fation + '\'' +
                ", fnical='" + fnical + '\'' +
                ", femppo='" + femppo + '\'' +
                ", fpjuse='" + fpjuse + '\'' +
                ", fverif='" + fverif + '\'' +
                ", fstatu='" + fstatu + '\'' +
                ", fstaby='" + fstaby + '\'' +
                ", fotask='" + fotask + '\'' +
                ", fanswe='" + fanswe + '\'' +
                ", fappno='" + fappno + '\'' +
                ", fcusno='" + fcusno + '\'' +
                ", fsmamt='" + fsmamt + '\'' +
                ", fcatch='" + fcatch + '\'' +
                ", ffanch='" + ffanch + '\'' +
                ", ficity='" + ficity + '\'' +
                ", fyears='" + fyears + '\'' +
                ", fksddt='" + fksddt + '\'' +
                ", iddate='" + iddate + '\'' +
                ", cardno='" + cardno + '\'' +
                ", secard='" + secard + '\'' +
                ", fscano='" + fscano + '\'' +
                ", ficard='" + ficard + '\'' +
                ", nation='" + nation + '\'' +
                ", appnal='" + appnal + '\'' +
                ", appnaf='" + appnaf + '\'' +
                ", zjgcre='" + zjgcre + '\'' +
                ", darate='" + darate + '\'' +
                ", questr='" + questr + '\'' +
                ", bankid='" + bankid + '\'' +
                ", fntion='" + fntion + '\'' +
                '}';
    }
}
