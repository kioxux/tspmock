package cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：分期审核
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D13162ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "rgstid")
    private String rgstid;//分期申请号
    @JsonProperty(value = "cardno")
    private String cardno;//卡号
    @JsonProperty(value = "remark")
    private String remark;//备注信息
    @JsonProperty(value = "loanit")
    private BigDecimal loanit;//分期总本金
    @JsonProperty(value = "opt")
    private String opt;//操作码
    @JsonProperty(value = "sendnd")
    private String sendnd;//是否直接放款

    public String getRgstid() {
        return rgstid;
    }

    public void setRgstid(String rgstid) {
        this.rgstid = rgstid;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getLoanit() {
        return loanit;
    }

    public void setLoanit(BigDecimal loanit) {
        this.loanit = loanit;
    }

    public String getOpt() {
        return opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public String getSendnd() {
        return sendnd;
    }

    public void setSendnd(String sendnd) {
        this.sendnd = sendnd;
    }

    @Override
    public String toString() {
        return "D13162ReqDto{" +
                "rgstid='" + rgstid + '\'' +
                "cardno='" + cardno + '\'' +
                "remark='" + remark + '\'' +
                "loanit='" + loanit + '\'' +
                "opt='" + opt + '\'' +
                "sendnd='" + sendnd + '\'' +
                '}';
    }
}  
