package cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询共有人信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdypgyrcxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "list")
    private java.util.List<List> list;//list

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "XdypgyrcxRespDto{" +
                "list='" + list + '\'' +
                '}';
    }
}
