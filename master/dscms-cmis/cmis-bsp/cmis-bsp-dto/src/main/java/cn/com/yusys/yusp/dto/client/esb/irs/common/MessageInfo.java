package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应DTO：响应信息域元素:产品层债项评级结果
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
@JsonPropertyOrder(alphabetic = true)
public class MessageInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "detail_serno")
    private String detail_serno; // 授信分项流水号
    @JsonProperty(value = "pro_no")
    private String pro_no; // 业务品种编号
    @JsonProperty(value = "pro_name")
    private String pro_name; // 业务品种名称
    @JsonProperty(value = "guaranteeGrade")
    private String guaranteeGrade; // 债项等级
    @JsonProperty(value = "ead")
    private BigDecimal ead; // 违约风险暴露
    @JsonProperty(value = "lgd")
    private BigDecimal lgd; // 违约损失率

    public String getDetail_serno() {
        return detail_serno;
    }

    public void setDetail_serno(String detail_serno) {
        this.detail_serno = detail_serno;
    }

    public String getPro_no() {
        return pro_no;
    }

    public void setPro_no(String pro_no) {
        this.pro_no = pro_no;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getGuaranteeGrade() {
        return guaranteeGrade;
    }

    public void setGuaranteeGrade(String guaranteeGrade) {
        this.guaranteeGrade = guaranteeGrade;
    }

    public BigDecimal getEad() {
        return ead;
    }

    public void setEad(BigDecimal ead) {
        this.ead = ead;
    }

    public BigDecimal getLgd() {
        return lgd;
    }

    public void setLgd(BigDecimal lgd) {
        this.lgd = lgd;
    }

    @Override
    public String toString() {
        return "MessageInfo{" +
                "detail_serno='" + detail_serno + '\'' +
                ", pro_no='" + pro_no + '\'' +
                ", pro_name='" + pro_name + '\'' +
                ", guaranteeGrade='" + guaranteeGrade + '\'' +
                ", ead=" + ead +
                ", lgd=" + lgd +
                '}';
    }
}
