package cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：新入职人员信息登记
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XxdentRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "total")
    private String total;//总数量
    @JsonProperty(value = "xxdentList")
    private java.util.List<XxdentList> xxdentList;//xxdentList

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<XxdentList> getXxdentList() {
        return xxdentList;
    }

    public void setXxdentList(List<XxdentList> xxdentList) {
        this.xxdentList = xxdentList;
    }

    @Override
    public String toString() {
        return "XxdentRespDto{" +
                "total='" + total + '\'' +
                ", xxdentList=" + xxdentList +
                '}';
    }
}
