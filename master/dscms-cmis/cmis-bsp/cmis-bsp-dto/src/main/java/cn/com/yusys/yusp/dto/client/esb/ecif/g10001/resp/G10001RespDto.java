package cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：客户群组信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class G10001RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bginnm")
    private String bginnm;//起始笔数
    @JsonProperty(value = "qurynm")
    private String qurynm;//查询笔数
    @JsonProperty(value = "totlnm")
    private String totlnm;//总笔数
    @JsonProperty(value = "grouno")
    private String grouno;//群组编号
    @JsonProperty(value = "grouna")
    private String grouna;//群组名称
    @JsonProperty(value = "xdjtbh")
    private String xdjtbh;//信贷集团编号
    @JsonProperty(value = "xdjtmc")
    private String xdjtmc;//信贷集团名称
    @JsonProperty(value = "tyjtbh")
    private String tyjtbh;//同业集团编号
    @JsonProperty(value = "tyjtmc")
    private String tyjtmc;//同业集团名称
    @JsonProperty(value = "grouds")
    private String grouds;//群组描述
    @JsonProperty(value = "acctbr")
    private String acctbr;//管户机构
    @JsonProperty(value = "acmang")
    private String acmang;//管户客户经理
    @JsonProperty(value = "mxmang")
    private String mxmang;//主办客户经理
    @JsonProperty(value = "inptdt")
    private String inptdt;//主办日期
    @JsonProperty(value = "inptbr")
    private String inptbr;//主办机构
    @JsonProperty(value = "jinmlv")
    private String jinmlv;//紧密程度
    @JsonProperty(value = "remark")
    private String remark;//备注
    @JsonProperty(value = "listnm")
    private Integer listnm;//循环列表记录数
    @JsonProperty(value = "List")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp.GroupArrayMem> List;

    public String getBginnm() {
        return bginnm;
    }

    public void setBginnm(String bginnm) {
        this.bginnm = bginnm;
    }

    public String getQurynm() {
        return qurynm;
    }

    public void setQurynm(String qurynm) {
        this.qurynm = qurynm;
    }

    public String getTotlnm() {
        return totlnm;
    }

    public void setTotlnm(String totlnm) {
        this.totlnm = totlnm;
    }

    public String getGrouno() {
        return grouno;
    }

    public void setGrouno(String grouno) {
        this.grouno = grouno;
    }

    public String getGrouna() {
        return grouna;
    }

    public void setGrouna(String grouna) {
        this.grouna = grouna;
    }

    public String getXdjtbh() {
        return xdjtbh;
    }

    public void setXdjtbh(String xdjtbh) {
        this.xdjtbh = xdjtbh;
    }

    public String getXdjtmc() {
        return xdjtmc;
    }

    public void setXdjtmc(String xdjtmc) {
        this.xdjtmc = xdjtmc;
    }

    public String getTyjtbh() {
        return tyjtbh;
    }

    public void setTyjtbh(String tyjtbh) {
        this.tyjtbh = tyjtbh;
    }

    public String getTyjtmc() {
        return tyjtmc;
    }

    public void setTyjtmc(String tyjtmc) {
        this.tyjtmc = tyjtmc;
    }

    public String getGrouds() {
        return grouds;
    }

    public void setGrouds(String grouds) {
        this.grouds = grouds;
    }

    public String getAcctbr() {
        return acctbr;
    }

    public void setAcctbr(String acctbr) {
        this.acctbr = acctbr;
    }

    public String getAcmang() {
        return acmang;
    }

    public void setAcmang(String acmang) {
        this.acmang = acmang;
    }

    public String getMxmang() {
        return mxmang;
    }

    public void setMxmang(String mxmang) {
        this.mxmang = mxmang;
    }

    public String getInptdt() {
        return inptdt;
    }

    public void setInptdt(String inptdt) {
        this.inptdt = inptdt;
    }

    public String getInptbr() {
        return inptbr;
    }

    public void setInptbr(String inptbr) {
        this.inptbr = inptbr;
    }

    public String getJinmlv() {
        return jinmlv;
    }

    public void setJinmlv(String jinmlv) {
        this.jinmlv = jinmlv;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getListnm() {
        return listnm;
    }

    public void setListnm(Integer listnm) {
        this.listnm = listnm;
    }

    public java.util.List<GroupArrayMem> getList() {
        return List;
    }

    public void setList(java.util.List<GroupArrayMem> list) {
        List = list;
    }

    @Override
    public String toString() {
        return "G10001RespDto{" +
                "bginnm='" + bginnm + '\'' +
                ", qurynm='" + qurynm + '\'' +
                ", totlnm='" + totlnm + '\'' +
                ", grouno='" + grouno + '\'' +
                ", grouna='" + grouna + '\'' +
                ", xdjtbh='" + xdjtbh + '\'' +
                ", xdjtmc='" + xdjtmc + '\'' +
                ", tyjtbh='" + tyjtbh + '\'' +
                ", tyjtmc='" + tyjtmc + '\'' +
                ", grouds='" + grouds + '\'' +
                ", acctbr='" + acctbr + '\'' +
                ", acmang='" + acmang + '\'' +
                ", mxmang='" + mxmang + '\'' +
                ", inptdt='" + inptdt + '\'' +
                ", inptbr='" + inptbr + '\'' +
                ", jinmlv='" + jinmlv + '\'' +
                ", remark='" + remark + '\'' +
                ", listnm=" + listnm +
                ", List=" + List +
                '}';
    }
}
