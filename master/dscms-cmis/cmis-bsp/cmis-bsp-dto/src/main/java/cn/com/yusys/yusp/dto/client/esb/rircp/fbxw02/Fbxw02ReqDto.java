package cn.com.yusys.yusp.dto.client.esb.rircp.fbxw02;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;


/**
 * 请求DTO：终审结果同步
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午7:31:23
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxw02ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd; // 处理码
    @JsonProperty(value = "servtp")
    private String servtp; // 渠道
    @JsonProperty(value = "servsq")
    private String servsq; // 渠道流水
    @JsonProperty(value = "userid")
    private String userid; // 柜员号
    @JsonProperty(value = "brchno")
    private String brchno; // 部门号
    @JsonProperty(value = "channel_type")
    private String channel_type; // 渠道来源
    @JsonProperty(value = "co_platform")
    private String co_platform; // 合作平台
    @JsonProperty(value = "prd_type")
    private String prd_type; // 产品类别
    @JsonProperty(value = "prd_code")
    private String prd_code; // 产品代码（零售智能风控内部代码）
    @JsonProperty(value = "apply_no")
    private String apply_no; // 业务唯一编号
    @JsonProperty(value = "app_status")
    private String app_status; // 批复状态
    @JsonProperty(value = "appr_amt")
    private String appr_amt; // 终审额度
    @JsonProperty(value = "crd_start_date")
    private String crd_start_date; // 授信起始日期
    @JsonProperty(value = "crd_end_date")
    private String crd_end_date; // 授信结束日期
    @JsonProperty(value = "crd_term")
    private String crd_term; // 授信期限(月)
    @JsonProperty(value = "appr_rate")
    private String appr_rate; // 审批利率
    @JsonProperty(value = "refusal_reason")
    private String refusal_reason; // 拒绝原因

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getApply_no() {
        return apply_no;
    }

    public void setApply_no(String apply_no) {
        this.apply_no = apply_no;
    }

    public String getApp_status() {
        return app_status;
    }

    public void setApp_status(String app_status) {
        this.app_status = app_status;
    }

    public String getAppr_amt() {
        return appr_amt;
    }

    public void setAppr_amt(String appr_amt) {
        this.appr_amt = appr_amt;
    }

    public String getCrd_start_date() {
        return crd_start_date;
    }

    public void setCrd_start_date(String crd_start_date) {
        this.crd_start_date = crd_start_date;
    }

    public String getCrd_end_date() {
        return crd_end_date;
    }

    public void setCrd_end_date(String crd_end_date) {
        this.crd_end_date = crd_end_date;
    }

    public String getCrd_term() {
        return crd_term;
    }

    public void setCrd_term(String crd_term) {
        this.crd_term = crd_term;
    }

    public String getAppr_rate() {
        return appr_rate;
    }

    public void setAppr_rate(String appr_rate) {
        this.appr_rate = appr_rate;
    }

    public String getRefusal_reason() {
        return refusal_reason;
    }

    public void setRefusal_reason(String refusal_reason) {
        this.refusal_reason = refusal_reason;
    }

    @Override
    public String toString() {
        return "Fbxw02ReqDto{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", channel_type='" + channel_type + '\'' +
                ", co_platform='" + co_platform + '\'' +
                ", prd_type='" + prd_type + '\'' +
                ", prd_code='" + prd_code + '\'' +
                ", apply_no='" + apply_no + '\'' +
                ", app_status='" + app_status + '\'' +
                ", appr_amt='" + appr_amt + '\'' +
                ", crd_start_date='" + crd_start_date + '\'' +
                ", crd_end_date='" + crd_end_date + '\'' +
                ", crd_term='" + crd_term + '\'' +
                ", appr_rate='" + appr_rate + '\'' +
                ", refusal_reason='" + refusal_reason + '\'' +
                '}';
    }
}