package cn.com.yusys.yusp.dto.server.biz.xdzc0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/3 11:02
 * @since 2021/6/3 11:02
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "assetNo")
    private String assetNo;//资产编号

    @JsonProperty(value = "assetType")
    private String assetType;//资产类型

    @JsonProperty(value = "assetValue")
    private BigDecimal assetValue;//资产价值

    @JsonProperty(value = "assetEndDate")
    private String assetEndDate;//资产到期日

    @JsonProperty(value = "assetSour")
    private String assetSour;//资产来源

    @JsonProperty(value = "assetStatus")
    private String assetStatus;//资产状态

    @JsonProperty(value = "isPledge")
    private String isPledge;//是否入池质押

    @JsonProperty(value = "acptBankId")
    private String acptBankId;//行号

    @JsonProperty(value = "guarCusId")
    private String guarCusId;// 所有权人编号

    @JsonProperty(value = "guarCusName")
    private String guarCusName;// 所有权人名称

    @JsonProperty(value = "guarCertType")
    private String guarCertType;// 押品所有人证件类型

    @JsonProperty(value = "guarCertCode")
    private String guarCertCode;// 押品所有人证件号码

    @JsonProperty(value = "guarName")
    private String guarName;// 抵质押品名称

    @JsonProperty(value = "guarTypeCd")
    private String guarTypeCd;// 担保分类代码

    @JsonProperty(value = "accountManager")
    private String accountManager;// 管户人

    @JsonProperty(value = "guarLastupdateDate")
    private String guarLastupdateDate;// 最后更新时间

    @JsonProperty(value = "lastmodifyUserid")
    private String lastmodifyUserid;// 最后修改人

    @JsonProperty(value = "lastmodifyOrgid")
    private String lastmodifyOrgid;// 最后修改人机构

    @JsonProperty(value = "curType")
    private String curType;// 币种

    @JsonProperty(value = "receiptNo")
    private String receiptNo;// 发票编号(BP号)

    @JsonProperty(value = "guarNo")
    private String guarNo;// 发票编号(BP号)

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public BigDecimal getAssetValue() {
        return assetValue;
    }

    public void setAssetValue(BigDecimal assetValue) {
        this.assetValue = assetValue;
    }

    public String getAssetEndDate() {
        return assetEndDate;
    }

    public void setAssetEndDate(String assetEndDate) {
        this.assetEndDate = assetEndDate;
    }

    public String getAssetSour() {
        return assetSour;
    }

    public void setAssetSour(String assetSour) {
        this.assetSour = assetSour;
    }

    public String getAssetStatus() {
        return assetStatus;
    }

    public void setAssetStatus(String assetStatus) {
        this.assetStatus = assetStatus;
    }

    public String getIsPledge() {
        return isPledge;
    }

    public void setIsPledge(String isPledge) {
        this.isPledge = isPledge;
    }

    public String getAcptBankId() {
        return acptBankId;
    }

    public void setAcptBankId(String acptBankId) {
        this.acptBankId = acptBankId;
    }

    public String getGuarCusId() {
        return guarCusId;
    }

    public void setGuarCusId(String guarCusId) {
        this.guarCusId = guarCusId;
    }

    public String getGuarCusName() {
        return guarCusName;
    }

    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName;
    }

    public String getGuarCertType() {
        return guarCertType;
    }

    public void setGuarCertType(String guarCertType) {
        this.guarCertType = guarCertType;
    }

    public String getGuarCertCode() {
        return guarCertCode;
    }

    public void setGuarCertCode(String guarCertCode) {
        this.guarCertCode = guarCertCode;
    }

    public String getGuarName() {
        return guarName;
    }

    public void setGuarName(String guarName) {
        this.guarName = guarName;
    }

    public String getGuarTypeCd() {
        return guarTypeCd;
    }

    public void setGuarTypeCd(String guarTypeCd) {
        this.guarTypeCd = guarTypeCd;
    }

    public String getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(String accountManager) {
        this.accountManager = accountManager;
    }

    public String getGuarLastupdateDate() {
        return guarLastupdateDate;
    }

    public void setGuarLastupdateDate(String guarLastupdateDate) {
        this.guarLastupdateDate = guarLastupdateDate;
    }

    public String getLastmodifyUserid() {
        return lastmodifyUserid;
    }

    public void setLastmodifyUserid(String lastmodifyUserid) {
        this.lastmodifyUserid = lastmodifyUserid;
    }

    public String getLastmodifyOrgid() {
        return lastmodifyOrgid;
    }

    public void setLastmodifyOrgid(String lastmodifyOrgid) {
        this.lastmodifyOrgid = lastmodifyOrgid;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    @Override
    public String toString() {
        return "List{" +
                "assetNo='" + assetNo + '\'' +
                ", assetType='" + assetType + '\'' +
                ", assetValue=" + assetValue +
                ", assetEndDate='" + assetEndDate + '\'' +
                ", assetSour='" + assetSour + '\'' +
                ", assetStatus='" + assetStatus + '\'' +
                ", isPledge='" + isPledge + '\'' +
                ", acptBankId='" + acptBankId + '\'' +
                ", guarCusId='" + guarCusId + '\'' +
                ", guarCusName='" + guarCusName + '\'' +
                ", guarCertType='" + guarCertType + '\'' +
                ", guarCertCode='" + guarCertCode + '\'' +
                ", guarName='" + guarName + '\'' +
                ", guarTypeCd='" + guarTypeCd + '\'' +
                ", accountManager='" + accountManager + '\'' +
                ", guarLastupdateDate='" + guarLastupdateDate + '\'' +
                ", lastmodifyUserid='" + lastmodifyUserid + '\'' +
                ", lastmodifyOrgid='" + lastmodifyOrgid + '\'' +
                ", curType='" + curType + '\'' +
                ", receiptNo='" + receiptNo + '\'' +
                ", guarNo='" + guarNo + '\'' +
                '}';
    }
}
