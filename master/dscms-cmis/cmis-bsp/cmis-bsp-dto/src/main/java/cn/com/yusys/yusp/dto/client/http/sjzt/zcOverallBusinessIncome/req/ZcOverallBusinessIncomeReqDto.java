package cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：资产池业务总体收益情况详情
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ZcOverallBusinessIncomeReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "agrNo")
    private String agrNo;//协议编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "size")
    private Integer size;
    @JsonProperty(value = "from")
    private Integer from;

    public String getAgrNo() {
        return agrNo;
    }

    public void setAgrNo(String agrNo) {
        this.agrNo = agrNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "ZcOverallBusinessIncomeReqDto{" +
                "agrNo='" + agrNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", size=" + size +
                ", from=" + from +
                '}';
    }
}
