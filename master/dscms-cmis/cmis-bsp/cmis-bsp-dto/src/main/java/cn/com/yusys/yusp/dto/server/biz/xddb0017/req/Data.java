package cn.com.yusys.yusp.dto.server.biz.xddb0017.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 9:18
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarno")
    private String guarno;//抵押物编号

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "guarno='" + guarno + '\'' +
                '}';
    }
}
