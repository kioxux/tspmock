package cn.com.yusys.yusp.dto.server.biz.xdzc0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author xs
 * @version 0.1
 * @date 2021/6/8 20:53
 * @since 2021/6/8 20:53
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "assetType")
    private String assetType;//资产类型
    @JsonProperty(value = "contNo")
    private String contNo;//资产池协议编号
    @JsonProperty(value = "queryType")
    private String queryType;//当前历史
    @JsonProperty(value = "assetStartDate")
    private String assetStartDate;//资产到期日起（yyyy-MM-dd）
    @JsonProperty(value = "assetEndDate")
    private String assetEndDate;//资产到期日止
    @JsonProperty(value = "assetCeiling")
    private String assetCeiling;//资产价值上限
    @JsonProperty(value = "assetFloor")
    private String assetFloor;//资产价值下限
    @JsonProperty(value = "opStartDate")
    private String opStartDate;// 操作日开始
    @JsonProperty(value = "opEndDate")
    private String opEndDate;// 操作日截至
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "pageNum")
    private String pageNum;//查询起始页
    @JsonProperty(value = "pageSize")
    private String pageSize;//请求条数

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getAssetStartDate() {
        return assetStartDate;
    }

    public void setAssetStartDate(String assetStartDate) {
        this.assetStartDate = assetStartDate;
    }

    public String getAssetEndDate() {
        return assetEndDate;
    }

    public void setAssetEndDate(String assetEndDate) {
        this.assetEndDate = assetEndDate;
    }

    public String getAssetCeiling() {
        return assetCeiling;
    }

    public void setAssetCeiling(String assetCeiling) {
        this.assetCeiling = assetCeiling;
    }

    public String getAssetFloor() {
        return assetFloor;
    }

    public void setAssetFloor(String assetFloor) {
        this.assetFloor = assetFloor;
    }

    public String getOpStartDate() {
        return opStartDate;
    }

    public void setOpStartDate(String opStartDate) {
        this.opStartDate = opStartDate;
    }

    public String getOpEndDate() {
        return opEndDate;
    }

    public void setOpEndDate(String opEndDate) {
        this.opEndDate = opEndDate;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Xdzc0019DataReqDto{" +
                "assetType='" + assetType + '\'' +
                ", contNo='" + contNo + '\'' +
                ", queryType='" + queryType + '\'' +
                ", assetStartDate='" + assetStartDate + '\'' +
                ", assetEndDate='" + assetEndDate + '\'' +
                ", assetCeiling='" + assetCeiling + '\'' +
                ", assetFloor='" + assetFloor + '\'' +
                ", opStartDate='" + opStartDate + '\'' +
                ", opEndDate='" + opEndDate + '\'' +
                ", cusId='" + cusId + '\'' +
                ", pageNum='" + pageNum + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
