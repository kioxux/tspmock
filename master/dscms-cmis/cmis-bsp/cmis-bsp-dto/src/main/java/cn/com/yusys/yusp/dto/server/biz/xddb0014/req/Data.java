package cn.com.yusys.yusp.dto.server.biz.xddb0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 9:06
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "retype")
    private String retype;//登记类型
    @JsonProperty(value = "guarid")
    private String guarid;//押品编号

    public String getRetype() {
        return retype;
    }

    public void setRetype(String retype) {
        this.retype = retype;
    }

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    @Override
    public String toString() {
        return "Data{" +
                "retype='" + retype + '\'' +
                ", guarid='" + guarid + '\'' +
                '}';
    }
}
