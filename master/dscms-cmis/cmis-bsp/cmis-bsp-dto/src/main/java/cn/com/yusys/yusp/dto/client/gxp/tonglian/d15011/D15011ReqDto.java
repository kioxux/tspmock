package cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：永久额度调整
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class D15011ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cardno")
    private String cardno;//  卡号
    @JsonProperty(value = "crcycd")
    private String crcycd;//  币种
    @JsonProperty(value = "opt")
    private String opt;//  功能码
    @JsonProperty(value = "cdtlmt")
    private BigDecimal cdtlmt;//  信用额度

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getOpt() {
        return opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public BigDecimal getCdtlmt() {
        return cdtlmt;
    }

    public void setCdtlmt(BigDecimal cdtlmt) {
        this.cdtlmt = cdtlmt;
    }

    @Override
    public String toString() {
        return "D15011ReqDto{" +
                "cardno='" + cardno + '\'' +
                ", crcycd='" + crcycd + '\'' +
                ", opt='" + opt + '\'' +
                ", cdtlmt=" + cdtlmt +
                '}';
    }
}
