package cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：未备注
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ClzjcxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "ipaddr")
    private String ipaddr;//ip地址
    @JsonProperty(value = "mac")
    private String mac;//mac地址
    @JsonProperty(value = "savvou")
    private String savvou;//监管协议号（缴款凭证编号）

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSavvou() {
        return savvou;
    }

    public void setSavvou(String savvou) {
        this.savvou = savvou;
    }

    @Override
    public String toString() {
        return "ClzjcxReqDto{" +
                "ipaddr='" + ipaddr + '\'' +
                ", mac='" + mac + '\'' +
                ", savvou='" + savvou + '\'' +
                '}';
    }
}
