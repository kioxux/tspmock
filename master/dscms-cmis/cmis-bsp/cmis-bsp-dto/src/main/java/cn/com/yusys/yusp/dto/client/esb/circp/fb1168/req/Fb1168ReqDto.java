package cn.com.yusys.yusp.dto.client.esb.circp.fb1168.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：抵押查封结果推送
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1168ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "dy_no")
    private String dy_no;//抵押品编号
    @JsonProperty(value = "bdc_no")
    private String bdc_no;//不动产证号
    @JsonProperty(value = "jk_cus_name")
    private String jk_cus_name;//借款人名称
    @JsonProperty(value = "yp_manager_id")
    private String yp_manager_id;//押品所有人名称
    @JsonProperty(value = "yp_book_serno")
    private String yp_book_serno;//押品权证编号
    @JsonProperty(value = "cx_cf_result")
    private String cx_cf_result;//查询查封结果
    @JsonProperty(value = "cx_cf_time")
    private String cx_cf_time;//查询查封时间
    @JsonProperty(value = "pre_app_no")
    private String pre_app_no;//预授信流水号

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    public String getDy_no() {
        return dy_no;
    }

    public void setDy_no(String dy_no) {
        this.dy_no = dy_no;
    }

    public String getBdc_no() {
        return bdc_no;
    }

    public void setBdc_no(String bdc_no) {
        this.bdc_no = bdc_no;
    }

    public String getJk_cus_name() {
        return jk_cus_name;
    }

    public void setJk_cus_name(String jk_cus_name) {
        this.jk_cus_name = jk_cus_name;
    }

    public String getYp_manager_id() {
        return yp_manager_id;
    }

    public void setYp_manager_id(String yp_manager_id) {
        this.yp_manager_id = yp_manager_id;
    }

    public String getYp_book_serno() {
        return yp_book_serno;
    }

    public void setYp_book_serno(String yp_book_serno) {
        this.yp_book_serno = yp_book_serno;
    }

    public String getCx_cf_result() {
        return cx_cf_result;
    }

    public void setCx_cf_result(String cx_cf_result) {
        this.cx_cf_result = cx_cf_result;
    }

    public String getCx_cf_time() {
        return cx_cf_time;
    }

    public void setCx_cf_time(String cx_cf_time) {
        this.cx_cf_time = cx_cf_time;
    }

    public String getPre_app_no() {
        return pre_app_no;
    }

    public void setPre_app_no(String pre_app_no) {
        this.pre_app_no = pre_app_no;
    }

    @Override
    public String toString() {
        return "Fb1168ReqDto{" +
                "CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", dy_no='" + dy_no + '\'' +
                ", bdc_no='" + bdc_no + '\'' +
                ", jk_cus_name='" + jk_cus_name + '\'' +
                ", yp_manager_id='" + yp_manager_id + '\'' +
                ", yp_book_serno='" + yp_book_serno + '\'' +
                ", cx_cf_result='" + cx_cf_result + '\'' +
                ", cx_cf_time='" + cx_cf_time + '\'' +
                ", pre_app_no='" + pre_app_no + '\'' +
                '}';
    }
}
