package cn.com.yusys.yusp.dto.client.esb.ecif.g10001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户群组信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class G10001ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "grouno")
    private String grouno;//群组编号
    @JsonProperty(value = "grouna")
    private String grouna;//群组名称
    @JsonProperty(value = "gpcsst")
    private String gpcsst;//群组客户状态
    @JsonProperty(value = "custno")
    private String custno;//成员客户号
    @JsonProperty(value = "bginnm")
    private String bginnm;//起始笔数
    @JsonProperty(value = "qurynm")
    private String qurynm;//查询笔数

    public String getGrouno() {
        return grouno;
    }

    public void setGrouno(String grouno) {
        this.grouno = grouno;
    }

    public String getGrouna() {
        return grouna;
    }

    public void setGrouna(String grouna) {
        this.grouna = grouna;
    }

    public String getGpcsst() {
        return gpcsst;
    }

    public void setGpcsst(String gpcsst) {
        this.gpcsst = gpcsst;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getBginnm() {
        return bginnm;
    }

    public void setBginnm(String bginnm) {
        this.bginnm = bginnm;
    }

    public String getQurynm() {
        return qurynm;
    }

    public void setQurynm(String qurynm) {
        this.qurynm = qurynm;
    }

    @Override
    public String toString() {
        return "G10001ReqDto{" +
                "grouno='" + grouno + '\'' +
                "grouna='" + grouna + '\'' +
                "gpcsst='" + gpcsst + '\'' +
                "custno='" + custno + '\'' +
                "bginnm='" + bginnm + '\'' +
                "qurynm='" + qurynm + '\'' +
                '}';
    }
}  
