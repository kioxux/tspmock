package cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷系统请求V平台二级准入接口
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxd003ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "businessid")
    private String businessid;//业务申请编号 (银行业务单号)
    @JsonProperty(value = "grantdate")
    private String grantdate;//授权日期
    @JsonProperty(value = "scenecheckresult")
    private String scenecheckresult;//现场核验结果是否通过
    @JsonProperty(value = "scenechecksuggest")
    private String scenechecksuggest;//客户经理勘验意见
    @JsonProperty(value = "latestamount")
    private String latestamount;//调整后最新额度
    @JsonProperty(value = "latestinterest")
    private String latestinterest;//调整后最新利率
    @JsonProperty(value = "repaytype")
    private String repaytype;//还款方式
    @JsonProperty(value = "applyterm")
    private String applyterm;//申请期限
    @JsonProperty(value = "turnover")
    private String turnover;//周转方式

    public String getBusinessid() {
        return businessid;
    }

    public void setBusinessid(String businessid) {
        this.businessid = businessid;
    }

    public String getGrantdate() {
        return grantdate;
    }

    public void setGrantdate(String grantdate) {
        this.grantdate = grantdate;
    }

    public String getScenecheckresult() {
        return scenecheckresult;
    }

    public void setScenecheckresult(String scenecheckresult) {
        this.scenecheckresult = scenecheckresult;
    }

    public String getScenechecksuggest() {
        return scenechecksuggest;
    }

    public void setScenechecksuggest(String scenechecksuggest) {
        this.scenechecksuggest = scenechecksuggest;
    }

    public String getLatestamount() {
        return latestamount;
    }

    public void setLatestamount(String latestamount) {
        this.latestamount = latestamount;
    }

    public String getLatestinterest() {
        return latestinterest;
    }

    public void setLatestinterest(String latestinterest) {
        this.latestinterest = latestinterest;
    }

    public String getRepaytype() {
        return repaytype;
    }

    public void setRepaytype(String repaytype) {
        this.repaytype = repaytype;
    }

    public String getApplyterm() {
        return applyterm;
    }

    public void setApplyterm(String applyterm) {
        this.applyterm = applyterm;
    }

    public String getTurnover() {
        return turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    @Override
    public String toString() {
        return "Wxd003ReqDto{" +
                "businessid='" + businessid + '\'' +
                ", grantdate='" + grantdate + '\'' +
                ", scenecheckresult='" + scenecheckresult + '\'' +
                ", scenechecksuggest='" + scenechecksuggest + '\'' +
                ", latestamount='" + latestamount + '\'' +
                ", latestinterest='" + latestinterest + '\'' +
                ", repaytype='" + repaytype + '\'' +
                ", applyterm='" + applyterm + '\'' +
                ", turnover='" + turnover + '\'' +
                '}';
    }
}
