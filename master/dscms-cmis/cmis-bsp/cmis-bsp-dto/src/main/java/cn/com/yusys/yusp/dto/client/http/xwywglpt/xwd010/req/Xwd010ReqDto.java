package cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：利率撤销接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwd010ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "reqId")
    private String reqId;//申请业务流水号

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    @Override
    public String toString() {
        return "Xwd010ReqDto{" +
                "reqId='" + reqId + '\'' +
                '}';
    }
}  
