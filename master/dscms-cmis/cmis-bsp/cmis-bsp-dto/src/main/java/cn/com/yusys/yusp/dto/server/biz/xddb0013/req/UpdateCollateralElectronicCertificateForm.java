package cn.com.yusys.yusp.dto.server.biz.xddb0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 表单
 */
@JsonPropertyOrder(alphabetic = true)
public class UpdateCollateralElectronicCertificateForm implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certificate")
    private String certificate;//押品登记证明
    @JsonProperty(value = "certificate_info")
    private CertificateInfo certificate_info;//登记证明信息
    @JsonProperty(value = "order")
    private String order;//押品登记证明-抵押顺位

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public CertificateInfo getCertificate_info() {
        return certificate_info;
    }

    public void setCertificate_info(CertificateInfo certificate_info) {
        this.certificate_info = certificate_info;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "UpdateCollateralElectronicCertificateForm{" +
                "certificate='" + certificate + '\'' +
                ", certificate_info=" + certificate_info +
                ", order='" + order + '\'' +
                '}';
    }
}
