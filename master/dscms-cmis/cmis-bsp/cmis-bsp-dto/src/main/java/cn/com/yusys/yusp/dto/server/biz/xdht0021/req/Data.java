package cn.com.yusys.yusp.dto.server.biz.xdht0021.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：合同签订
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "dealType")
    private String dealType;//操作类型
    @JsonProperty(value = "creditImageNo")
    private String creditImageNo;//个人征信影像编号
    @JsonProperty(value = "bigDataCreditNo")
    private String bigDataCreditNo;//大数据信用影像编号
    @JsonProperty(value = "loanDirectionNo")
    private String loanDirectionNo;//贷款用途影像编号
    @JsonProperty(value = "contImageNo")
    private String contImageNo;//电子合同影像编号
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "oldContNo")
    private String oldContNo;// 原合同号
    @JsonProperty(value = "turnover")
    private String turnover;// 周转方式
    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getDealType() {
        return dealType;
    }

    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    public String getCreditImageNo() {
        return creditImageNo;
    }

    public void setCreditImageNo(String creditImageNo) {
        this.creditImageNo = creditImageNo;
    }

    public String getBigDataCreditNo() {
        return bigDataCreditNo;
    }

    public void setBigDataCreditNo(String bigDataCreditNo) {
        this.bigDataCreditNo = bigDataCreditNo;
    }

    public String getLoanDirectionNo() {
        return loanDirectionNo;
    }

    public void setLoanDirectionNo(String loanDirectionNo) {
        this.loanDirectionNo = loanDirectionNo;
    }

    public String getContImageNo() {
        return contImageNo;
    }

    public void setContImageNo(String contImageNo) {
        this.contImageNo = contImageNo;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getOldContNo() {
        return oldContNo;
    }

    public void setOldContNo(String oldContNo) {
        this.oldContNo = oldContNo;
    }

    public String getTurnover() {
        return turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contNo='" + contNo + '\'' +
                ", dealType='" + dealType + '\'' +
                ", creditImageNo='" + creditImageNo + '\'' +
                ", bigDataCreditNo='" + bigDataCreditNo + '\'' +
                ", loanDirectionNo='" + loanDirectionNo + '\'' +
                ", contImageNo='" + contImageNo + '\'' +
                ", repayType='" + repayType + '\'' +
                ", oldContNo='" + oldContNo + '\'' +
                ", turnover='" + turnover + '\'' +
                '}';
    }
}
