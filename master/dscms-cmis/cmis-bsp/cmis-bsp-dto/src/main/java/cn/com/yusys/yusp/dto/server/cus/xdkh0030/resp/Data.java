package cn.com.yusys.yusp.dto.server.cus.xdkh0030.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：公司客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusInfoList")
    private java.util.List<CusInfoList> cusInfoList;
    @JsonProperty(value = "mainPersonInfoList")
    private java.util.List<MainPersonInfoList> mainPersonInfoList;

    public List<CusInfoList> getCusInfoList() {
        return cusInfoList;
    }

    public void setCusInfoList(List<CusInfoList> cusInfoList) {
        this.cusInfoList = cusInfoList;
    }

    public List<MainPersonInfoList> getMainPersonInfoList() {
        return mainPersonInfoList;
    }

    public void setMainPersonInfoList(List<MainPersonInfoList> mainPersonInfoList) {
        this.mainPersonInfoList = mainPersonInfoList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusInfoList=" + cusInfoList +
                ", mainPersonInfoList=" + mainPersonInfoList +
                '}';
    }
}