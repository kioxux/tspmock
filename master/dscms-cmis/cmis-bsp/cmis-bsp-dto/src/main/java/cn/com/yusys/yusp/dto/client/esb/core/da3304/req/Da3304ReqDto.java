package cn.com.yusys.yusp.dto.client.esb.core.da3304.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：资产转让借据筛选
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3304ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "yewubhao")
    private String yewubhao;//业务编号
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "zhchjgou")
    private String zhchjgou;//转出机构
    @JsonProperty(value = "zhrujgou")
    private String zhrujgou;//转入机构
    @JsonProperty(value = "shifoudy")
    private String shifoudy;//是否打印

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getYewubhao() {
        return yewubhao;
    }

    public void setYewubhao(String yewubhao) {
        this.yewubhao = yewubhao;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getZhchjgou() {
        return zhchjgou;
    }

    public void setZhchjgou(String zhchjgou) {
        this.zhchjgou = zhchjgou;
    }

    public String getZhrujgou() {
        return zhrujgou;
    }

    public void setZhrujgou(String zhrujgou) {
        this.zhrujgou = zhrujgou;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    @Override
    public String toString() {
        return "Da3304ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "yewubhao='" + yewubhao + '\'' +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "zhchjgou='" + zhchjgou + '\'' +
                "zhrujgou='" + zhrujgou + '\'' +
                "shifoudy='" + shifoudy + '\'' +
                '}';
    }
}  
