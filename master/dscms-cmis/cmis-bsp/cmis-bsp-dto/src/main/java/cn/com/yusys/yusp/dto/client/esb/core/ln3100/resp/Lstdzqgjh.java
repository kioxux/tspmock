package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款定制期供计划表
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdzqgjh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "hxijinee")
    private BigDecimal hxijinee;//还息金额
    @JsonProperty(value = "zwhkriqi")
    private String zwhkriqi;//最晚还款日
    @JsonProperty(value = "absbkulx")
    private String absbkulx;//贸融ABS备款类型
    @JsonProperty(value = "absgbfsh")
    private String absgbfsh;//贸融ABS关闭方式

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public String getZwhkriqi() {
        return zwhkriqi;
    }

    public void setZwhkriqi(String zwhkriqi) {
        this.zwhkriqi = zwhkriqi;
    }

    public String getAbsbkulx() {
        return absbkulx;
    }

    public void setAbsbkulx(String absbkulx) {
        this.absbkulx = absbkulx;
    }

    public String getAbsgbfsh() {
        return absgbfsh;
    }

    public void setAbsgbfsh(String absgbfsh) {
        this.absgbfsh = absgbfsh;
    }

    @Override
    public String toString() {
        return "LstdzqgjhRespDto{" +
                "hxijinee='" + hxijinee + '\'' +
                "zwhkriqi='" + zwhkriqi + '\'' +
                "absbkulx='" + absbkulx + '\'' +
                "absgbfsh='" + absgbfsh + '\'' +
                '}';
    }
}  
