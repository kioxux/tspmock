package cn.com.yusys.yusp.dto.client.esb.core.mbt999;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：V5通用记账
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Mbt999RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "jioylius")
    private String jioylius;//交易流水
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "qianzhls")
    private String qianzhls;//前置流水
    @JsonProperty(value = "qianzhrq")
    private String qianzhrq;//前置日期
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码
    @JsonProperty(value = "qiatyima")
    private String qiatyima;//前台交易码
    @JsonProperty(value = "jiaoyisj")
    private Integer jiaoyisj;//交易时间
    @JsonProperty(value = "jioyguiy")
    private String jioyguiy;//交易柜员
    @JsonProperty(value = "shoqguiy")
    private String shoqguiy;//授权柜员
    @JsonProperty(value = "guazhnxh")
    private String guazhnxh;//挂账序号
    @JsonProperty(value = "ibmingxOut")
    private List<IbmingxOut> ibmingxOut;//v5通用记账列表


    public String getJioylius() {
        return jioylius;
    }

    public void setJioylius(String jioylius) {
        this.jioylius = jioylius;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getQianzhls() {
        return qianzhls;
    }

    public void setQianzhls(String qianzhls) {
        this.qianzhls = qianzhls;
    }

    public String getQianzhrq() {
        return qianzhrq;
    }

    public void setQianzhrq(String qianzhrq) {
        this.qianzhrq = qianzhrq;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getQiatyima() {
        return qiatyima;
    }

    public void setQiatyima(String qiatyima) {
        this.qiatyima = qiatyima;
    }

    public Integer getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(Integer jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getJioyguiy() {
        return jioyguiy;
    }

    public void setJioyguiy(String jioyguiy) {
        this.jioyguiy = jioyguiy;
    }

    public String getShoqguiy() {
        return shoqguiy;
    }

    public void setShoqguiy(String shoqguiy) {
        this.shoqguiy = shoqguiy;
    }

    public String getGuazhnxh() {
        return guazhnxh;
    }

    public void setGuazhnxh(String guazhnxh) {
        this.guazhnxh = guazhnxh;
    }

    public List<IbmingxOut> getIbmingxOut() {
        return ibmingxOut;
    }

    public void setIbmingxOut(List<IbmingxOut> ibmingxOut) {
        this.ibmingxOut = ibmingxOut;
    }

    @Override
    public String toString() {
        return "Mbt999RespDto{" +
                "jioylius='" + jioylius + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "qianzhls='" + qianzhls + '\'' +
                "qianzhrq='" + qianzhrq + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                "qiatyima='" + qiatyima + '\'' +
                "jiaoyisj='" + jiaoyisj + '\'' +
                "jioyguiy='" + jioyguiy + '\'' +
                "shoqguiy='" + shoqguiy + '\'' +
                "guazhnxh='" + guazhnxh + '\'' +
                "ibmingxOut='" + ibmingxOut + '\'' +
                '}';
    }
}  
