package cn.com.yusys.yusp.dto.server.biz.xdzc0012.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/6/8 15:45:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 15:45
 * @since 2021/6/8 15:45
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFlag")
    private String opFlag;//返回码
    @JsonProperty(value = "opMsg")
    private String opMsg;//返回信息

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xdzc0012RespDto{" +
                "opFlag='" + opFlag + '\'' +
                "opMsg='" + opMsg + '\'' +
                '}';
    }
}
