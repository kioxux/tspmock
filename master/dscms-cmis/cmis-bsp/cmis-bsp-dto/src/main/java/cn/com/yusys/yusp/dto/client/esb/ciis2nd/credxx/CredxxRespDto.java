package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：线下查询接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CredxxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "reqId")
    private String reqId;//请求业务号
    @JsonProperty(value = "reportId")
    private String reportId;//报告编号
    @JsonProperty(value = "htmlStr")
    private String htmlStr;//html报告
    @JsonProperty(value = "xmlStr")
    private String xmlStr;//xml报告
    @JsonProperty(value = "jsonStr")
    private String jsonStr;//json报告
    @JsonProperty(value = "reportSource")
    private String reportSource;//报告来源
    @JsonProperty(value = "usetime")
    private String usetime;//查询报告耗时


    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getHtmlStr() {
        return htmlStr;
    }

    public void setHtmlStr(String htmlStr) {
        this.htmlStr = htmlStr;
    }

    public String getXmlStr() {
        return xmlStr;
    }

    public void setXmlStr(String xmlStr) {
        this.xmlStr = xmlStr;
    }

    public String getJsonStr() {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr) {
        this.jsonStr = jsonStr;
    }

    public String getReportSource() {
        return reportSource;
    }

    public void setReportSource(String reportSource) {
        this.reportSource = reportSource;
    }

    public String getUsetime() {
        return usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    @Override
    public String toString() {
        return "CredxxRespDto{" +
                "reqId='" + reqId + '\'' +
                "reportId='" + reportId + '\'' +
                "htmlStr='" + htmlStr + '\'' +
                "xmlStr='" + xmlStr + '\'' +
                "jsonStr='" + jsonStr + '\'' +
                "reportSource='" + reportSource + '\'' +
                "usetime='" + usetime + '\'' +
                '}';
    }
}  
