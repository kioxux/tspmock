package cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：押品信息同步及引入
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/5 15:52
 * @since 2021/6/5 15:52
 */
@JsonPropertyOrder(alphabetic = true)
public class XdjzzyReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "XdjzzyReqDto{" +
                "list=" + list +
                '}';
    }
}
