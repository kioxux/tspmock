package cn.com.yusys.yusp.dto.server.cfg.xdsx0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：授信业务授权同步、资本占用率参数表同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dutyId")
    private String dutyId;//岗位ID
    @JsonProperty(value = "empoinfoList")
    private java.util.List<EmpoinfoList> empoinfoList;
    @JsonProperty(value = "syncDate")
    private String syncDate;//同步日期
    @JsonProperty(value = "dtghFlag")
    private String dtghFlag;//区分标识
    @JsonProperty(value = "cusType")
    private String cusType;//客户类型

    public void setEmpoinfoList(List<EmpoinfoList> empoinfoList) {
        this.empoinfoList = empoinfoList;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getDutyId() {
        return dutyId;
    }

    public void setDutyId(String dutyId) {
        this.dutyId = dutyId;
    }

    public java.util.List<EmpoinfoList> getEmpoinfoList() {
        return empoinfoList;
    }


    public String getSyncDate() {
        return syncDate;
    }

    public void setSyncDate(String syncDate) {
        this.syncDate = syncDate;
    }

    public String getDtghFlag() {
        return dtghFlag;
    }

    public void setDtghFlag(String dtghFlag) {
        this.dtghFlag = dtghFlag;
    }

    @Override
    public String toString() {
        return "Data{" +
                "dutyId='" + dutyId + '\'' +
                ", empoinfoList=" + empoinfoList +
                ", syncDate='" + syncDate + '\'' +
                ", dtghFlag='" + dtghFlag + '\'' +
                ", cusType='" + cusType + '\'' +
                '}';
    }
}