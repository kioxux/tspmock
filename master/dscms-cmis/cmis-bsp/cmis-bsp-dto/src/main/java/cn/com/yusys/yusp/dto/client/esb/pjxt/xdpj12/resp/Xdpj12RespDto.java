package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：票据池推送保证金账号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj12RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Xdpj12RespDto{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                '}';
    }
}  
