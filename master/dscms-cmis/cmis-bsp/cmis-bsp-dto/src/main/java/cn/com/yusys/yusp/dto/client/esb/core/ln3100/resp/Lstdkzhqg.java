package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款账户期供
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkzhqg implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "benqqish")
    private Integer benqqish;//本期期数
    @JsonProperty(value = "benqizqs")
    private Integer benqizqs;//本期子期数
    @JsonProperty(value = "kxqdqirq")
    private String kxqdqirq;//宽限期到期日
    @JsonProperty(value = "schkriqi")
    private String schkriqi;//上次还款日
    @JsonProperty(value = "xchkriqi")
    private String xchkriqi;//下次还款日
    @JsonProperty(value = "chushibj")
    private BigDecimal chushibj;//初始本金
    @JsonProperty(value = "chushilx")
    private BigDecimal chushilx;//初始利息
    @JsonProperty(value = "benjinnn")
    private BigDecimal benjinnn;//本金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "sjlllxsr")
    private BigDecimal sjlllxsr;//实际利率利息收入
    @JsonProperty(value = "lxtiaozh")
    private BigDecimal lxtiaozh;//利息调整
    @JsonProperty(value = "sjyjlixi")
    private BigDecimal sjyjlixi;//实际应计利息
    @JsonProperty(value = "sjyjfaxi")
    private BigDecimal sjyjfaxi;//实际应计罚息
    @JsonProperty(value = "sjyjfuxi")
    private BigDecimal sjyjfuxi;//实际应计复息
    @JsonProperty(value = "sjllsjsr")
    private BigDecimal sjllsjsr;//实际利率实际利息收入
    @JsonProperty(value = "yingjitx")
    private BigDecimal yingjitx;//应计贴息
    @JsonProperty(value = "yingshtx")
    private BigDecimal yingshtx;//应收贴息
    @JsonProperty(value = "yingshfy")
    private BigDecimal yingshfy;//应收费用
    @JsonProperty(value = "yingshfj")
    private BigDecimal yingshfj;//应收罚金
    @JsonProperty(value = "benqizht")
    private String benqizht;//本期状态
    @JsonProperty(value = "yjfyjzht")
    private String yjfyjzht;//应计非应计状态
    @JsonProperty(value = "qigengzl")
    private String qigengzl;//期供种类
    @JsonProperty(value = "jzhqshrq")
    private String jzhqshrq;//基准起始日
    @JsonProperty(value = "jzhdqirq")
    private String jzhdqirq;//基准到期日
    @JsonProperty(value = "mingxixh")
    private Integer mingxixh;//明细序号
    @JsonProperty(value = "qgyqtnsh")
    private Integer qgyqtnsh;//期供逾期天数

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public Integer getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(Integer benqizqs) {
        this.benqizqs = benqizqs;
    }

    public String getKxqdqirq() {
        return kxqdqirq;
    }

    public void setKxqdqirq(String kxqdqirq) {
        this.kxqdqirq = kxqdqirq;
    }

    public String getSchkriqi() {
        return schkriqi;
    }

    public void setSchkriqi(String schkriqi) {
        this.schkriqi = schkriqi;
    }

    public String getXchkriqi() {
        return xchkriqi;
    }

    public void setXchkriqi(String xchkriqi) {
        this.xchkriqi = xchkriqi;
    }

    public BigDecimal getChushibj() {
        return chushibj;
    }

    public void setChushibj(BigDecimal chushibj) {
        this.chushibj = chushibj;
    }

    public BigDecimal getChushilx() {
        return chushilx;
    }

    public void setChushilx(BigDecimal chushilx) {
        this.chushilx = chushilx;
    }

    public BigDecimal getBenjinnn() {
        return benjinnn;
    }

    public void setBenjinnn(BigDecimal benjinnn) {
        this.benjinnn = benjinnn;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getSjlllxsr() {
        return sjlllxsr;
    }

    public void setSjlllxsr(BigDecimal sjlllxsr) {
        this.sjlllxsr = sjlllxsr;
    }

    public BigDecimal getLxtiaozh() {
        return lxtiaozh;
    }

    public void setLxtiaozh(BigDecimal lxtiaozh) {
        this.lxtiaozh = lxtiaozh;
    }

    public BigDecimal getSjyjlixi() {
        return sjyjlixi;
    }

    public void setSjyjlixi(BigDecimal sjyjlixi) {
        this.sjyjlixi = sjyjlixi;
    }

    public BigDecimal getSjyjfaxi() {
        return sjyjfaxi;
    }

    public void setSjyjfaxi(BigDecimal sjyjfaxi) {
        this.sjyjfaxi = sjyjfaxi;
    }

    public BigDecimal getSjyjfuxi() {
        return sjyjfuxi;
    }

    public void setSjyjfuxi(BigDecimal sjyjfuxi) {
        this.sjyjfuxi = sjyjfuxi;
    }

    public BigDecimal getSjllsjsr() {
        return sjllsjsr;
    }

    public void setSjllsjsr(BigDecimal sjllsjsr) {
        this.sjllsjsr = sjllsjsr;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public String getBenqizht() {
        return benqizht;
    }

    public void setBenqizht(String benqizht) {
        this.benqizht = benqizht;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public String getQigengzl() {
        return qigengzl;
    }

    public void setQigengzl(String qigengzl) {
        this.qigengzl = qigengzl;
    }

    public String getJzhqshrq() {
        return jzhqshrq;
    }

    public void setJzhqshrq(String jzhqshrq) {
        this.jzhqshrq = jzhqshrq;
    }

    public String getJzhdqirq() {
        return jzhdqirq;
    }

    public void setJzhdqirq(String jzhdqirq) {
        this.jzhdqirq = jzhdqirq;
    }

    public Integer getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Integer mingxixh) {
        this.mingxixh = mingxixh;
    }

    public Integer getQgyqtnsh() {
        return qgyqtnsh;
    }

    public void setQgyqtnsh(Integer qgyqtnsh) {
        this.qgyqtnsh = qgyqtnsh;
    }

    @Override
    public String toString() {
        return "LstdkzhqgRespDto{" +
                "dkzhangh='" + dkzhangh + '\'' +
                "benqqish='" + benqqish + '\'' +
                "benqizqs='" + benqizqs + '\'' +
                "kxqdqirq='" + kxqdqirq + '\'' +
                "schkriqi='" + schkriqi + '\'' +
                "xchkriqi='" + xchkriqi + '\'' +
                "chushibj='" + chushibj + '\'' +
                "chushilx='" + chushilx + '\'' +
                "benjinnn='" + benjinnn + '\'' +
                "ysyjlixi='" + ysyjlixi + '\'' +
                "csyjlixi='" + csyjlixi + '\'' +
                "ysqianxi='" + ysqianxi + '\'' +
                "csqianxi='" + csqianxi + '\'' +
                "ysyjfaxi='" + ysyjfaxi + '\'' +
                "csyjfaxi='" + csyjfaxi + '\'' +
                "yshofaxi='" + yshofaxi + '\'' +
                "cshofaxi='" + cshofaxi + '\'' +
                "yingjifx='" + yingjifx + '\'' +
                "fuxiiiii='" + fuxiiiii + '\'' +
                "hexiaolx='" + hexiaolx + '\'' +
                "sjlllxsr='" + sjlllxsr + '\'' +
                "lxtiaozh='" + lxtiaozh + '\'' +
                "sjyjlixi='" + sjyjlixi + '\'' +
                "sjyjfaxi='" + sjyjfaxi + '\'' +
                "sjyjfuxi='" + sjyjfuxi + '\'' +
                "sjllsjsr='" + sjllsjsr + '\'' +
                "yingjitx='" + yingjitx + '\'' +
                "yingshtx='" + yingshtx + '\'' +
                "yingshfy='" + yingshfy + '\'' +
                "yingshfj='" + yingshfj + '\'' +
                "benqizht='" + benqizht + '\'' +
                "yjfyjzht='" + yjfyjzht + '\'' +
                "qigengzl='" + qigengzl + '\'' +
                "jzhqshrq='" + jzhqshrq + '\'' +
                "jzhdqirq='" + jzhdqirq + '\'' +
                "mingxixh='" + mingxixh + '\'' +
                "qgyqtnsh='" + qgyqtnsh + '\'' +
                '}';
    }
}  
