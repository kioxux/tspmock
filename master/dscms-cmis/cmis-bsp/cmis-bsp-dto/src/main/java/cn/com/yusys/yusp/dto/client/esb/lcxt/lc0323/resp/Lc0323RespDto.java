package cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：查询理财是否冻结
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lc0323RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "FunctionId")
    private String FunctionId;//交易代码
    @JsonProperty(value = "ExSerial")
    private String ExSerial;//发起方流水号
    @JsonProperty(value = "SysDate")
    private String SysDate;//交易发生时的系统日期
    @JsonProperty(value = "SysTime")
    private String SysTime;//交易发生时的系统时间

    @JsonProperty(value = "TotNum")
    private String TotNum;//总行数
    @JsonProperty(value = "RetNum")
    private String RetNum;//本次返回行数
    @JsonProperty(value = "OffSet")
    private String OffSet;//定位串
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.resp.Lc0323RespListDto> list;

    @JsonIgnore
    public String getFunctionId() {
        return FunctionId;
    }

    @JsonIgnore
    public void setFunctionId(String functionId) {
        FunctionId = functionId;
    }

    @JsonIgnore
    public String getExSerial() {
        return ExSerial;
    }

    @JsonIgnore
    public void setExSerial(String exSerial) {
        ExSerial = exSerial;
    }

    @JsonIgnore
    public String getSysDate() {
        return SysDate;
    }

    @JsonIgnore
    public void setSysDate(String sysDate) {
        SysDate = sysDate;
    }

    @JsonIgnore
    public String getSysTime() {
        return SysTime;
    }

    @JsonIgnore
    public void setSysTime(String sysTime) {
        SysTime = sysTime;
    }

    @JsonIgnore
    public String getTotNum() {
        return TotNum;
    }

    @JsonIgnore
    public void setTotNum(String totNum) {
        TotNum = totNum;
    }

    @JsonIgnore
    public String getRetNum() {
        return RetNum;
    }

    @JsonIgnore
    public void setRetNum(String retNum) {
        RetNum = retNum;
    }

    @JsonIgnore
    public String getOffSet() {
        return OffSet;
    }

    @JsonIgnore
    public void setOffSet(String offSet) {
        OffSet = offSet;
    }

    public List<Lc0323RespListDto> getList() {
        return list;
    }

    public void setList(List<Lc0323RespListDto> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Lc0323RespDto{" +
                "FunctionId='" + FunctionId + '\'' +
                ", ExSerial='" + ExSerial + '\'' +
                ", SysDate='" + SysDate + '\'' +
                ", SysTime='" + SysTime + '\'' +
                ", TotNum='" + TotNum + '\'' +
                ", RetNum='" + RetNum + '\'' +
                ", OffSet='" + OffSet + '\'' +
                ", list=" + list +
                '}';
    }
}
