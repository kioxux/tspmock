package cn.com.yusys.yusp.dto.client.esb.circp.fb1149.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：无还本续贷借据更新
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1149ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "BILL_NO")
    private String BILL_NO;//新借据号
    @JsonProperty(value = "FK_STATUS")
    private String FK_STATUS;//放款状态


    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getBILL_NO() {
        return BILL_NO;
    }

    @JsonIgnore
    public void setBILL_NO(String BILL_NO) {
        this.BILL_NO = BILL_NO;
    }

    @JsonIgnore
    public String getFK_STATUS() {
        return FK_STATUS;
    }

    @JsonIgnore
    public void setFK_STATUS(String FK_STATUS) {
        this.FK_STATUS = FK_STATUS;
    }

    @Override
    public String toString() {
        return "Fb1149ReqDto{" +
                "CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", BILL_NO='" + BILL_NO + '\'' +
                ", FK_STATUS='" + FK_STATUS + '\'' +
                '}';
    }
}
