package cn.com.yusys.yusp.dto.server.biz.xdxw0052.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//客户调查表编号
    @JsonProperty(value = "applySerno")
    private String applySerno;//业务唯一编号
    @JsonProperty(value = "rqstrName")
    private String rqstrName;//申请人
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号
    @JsonProperty(value = "mobile")
    private String mobile;//手机号
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "assureMeansMain")
    private String assureMeansMain;//主担保方式
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "tpGuarWays")
    private String tpGuarWays;//追加担保方式数值
    @JsonProperty(value = "loanType")
    private String loanType;//贷款类型
    @JsonProperty(value = "managerId")
    private String managerId;//主办客户经理号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理中文名
    @JsonProperty(value = "onlineDate")
    private String onlineDate;//上线日期
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批状态
    @JsonProperty(value = "bizSour")
    private String bizSour;//业务来源
    @JsonProperty(value = "recomId")
    private String recomId;//推荐人
    @JsonProperty(value = "bizType")
    private String bizType;//业务类型
    @JsonProperty(value = "multiplexSerno")
    private String multiplexSerno;//复用流水
    @JsonProperty(value = "lastApprTime")
    private String lastApprTime;//最后一次审批时间
    @JsonProperty(value = "payType")
    private String payType;//支付方式
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "thisCrdAmt")
    private BigDecimal thisCrdAmt;//本次授信金额
    @JsonProperty(value = "thisLoanAmt")
    private BigDecimal thisLoanAmt;//本次用信金额
    @JsonProperty(value = "isLoanAndReturn")
    private String isLoanAndReturn;//是否随借随还
    @JsonProperty(value = "cancelFlag")
    private String cancelFlag;//撤销标志
    @JsonProperty(value = "orgNo")
    private String orgNo;//所属机构
    @JsonProperty(value = "multiplexedSerno")
    private String multiplexedSerno;//被复用流水号
    @JsonProperty(value = "oldRate")
    private BigDecimal oldRate;//原利率
    @JsonProperty(value = "rate")
    private BigDecimal rate;//利率
    @JsonProperty(value = "loanReason")
    private String loanReason;//贷款原因
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//贷款用途
    @JsonProperty(value = "billBal")
    private String billBal;//借据余额
    @JsonProperty(value = "loanAmt")
    private String loanAmt;//贷款金额
    @JsonProperty(value = "loanTerm")
    private BigDecimal loanTerm;//贷款期限
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "isOperNormal")
    private String isOperNormal;//借款人经营是否正常
    @JsonProperty(value = "result1")
    private String result1;//借款人经营是否正常
    @JsonProperty(value = "isOperOwnershipChange")
    private String isOperOwnershipChange;//借款人经营所有权是否发生重大变化
    @JsonProperty(value = "result2")
    private String result2;//借款人经营所有权是否发生重大变化原因
    @JsonProperty(value = "isDebtAdd50")
    private String isDebtAdd50;//借款人负债较上期增加是否超50
    @JsonProperty(value = "result3")
    private String result3;//借款人负债较上期增加是否超50%原因
    @JsonProperty(value = "isMoreAssure")
    private String isMoreAssure;//借款人对外是否提供过多担保或大量资产被抵押
    @JsonProperty(value = "result4")
    private String result4;//过多担保或大量资产被抵押原因
    @JsonProperty(value = "isFamilyAdt")
    private String isFamilyAdt;//借款人及其家庭是否发生意外（包括借款人死亡、被拘留、家庭破裂、涉及黄赌毒等）
    @JsonProperty(value = "result5")
    private String result5;//发生意外原因
    @JsonProperty(value = "IsPldimnMoreDvl")
    private String IsPldimnMoreDvl;//抵/质押物是否损毁或大幅贬值，变现能力与审批贷款时有无较大差异
    @JsonProperty(value = "result6")
    private String result6;//有较大差异原因
    @JsonProperty(value = "rateApplyReason")
    private String rateApplyReason;//利率申请原因
    @JsonProperty(value = "isWxbxd")
    private String isWxbxd;//是否优转续贷
    @JsonProperty(value = "cusLevel")
    private String cusLevel;//客户分层
    @JsonProperty(value = "conName")
    private String conName;//企业名称
    @JsonProperty(value = "reprName")
    private String reprName;//法人代表
    @JsonProperty(value = "operAddr")
    private String operAddr;//经营地址
    @JsonProperty(value = "mainOperBusi")
    private String mainOperBusi;//主营业务
    @JsonProperty(value = "bsinsLicYear")
    private String bsinsLicYear;//营业执照年限
    @JsonProperty(value = "trade")
    private String trade;//行业
    @JsonProperty(value = "indivCredit")
    private String indivCredit;//个人征信
    @JsonProperty(value = "indivCreditMemo")
    private String indivCreditMemo;//个人征信备注
    @JsonProperty(value = "conCredit")
    private String conCredit;//企业征信
    @JsonProperty(value = "conCreditMemo")
    private String conCreditMemo;//企业征信备注
    @JsonProperty(value = "indivMarSt")
    private String indivMarSt;//婚姻状况
    @JsonProperty(value = "spouseName")
    private String spouseName;//配偶姓名
    @JsonProperty(value = "spouseCertNo")
    private String spouseCertNo;//配偶身份证
    @JsonProperty(value = "spousePhone")
    private String spousePhone;//配偶电话
    @JsonProperty(value = "oldBillAmt")
    private String oldBillAmt;//原借据金额
    @JsonProperty(value = "apprId")
    private String apprId;//审核人工号
    @JsonProperty(value = "checkDate")
    private String checkDate;//检查日期
    @JsonProperty(value = "copyFrom")
    private String copyFrom;//从哪复制
    @JsonProperty(value = "annualRepayAmt")
    private BigDecimal annualRepayAmt;//每年归还本金（元）
    @JsonProperty(value = "isNewEmployee")
    private String isNewEmployee;//是否新员工
    @JsonProperty(value = "newEmployeeName")
    private String newEmployeeName;//新员工名称
    @JsonProperty(value = "employeePhone")
    private String employeePhone;//新员工电话
    @JsonProperty(value = "apprApplyStatus")
    private String apprApplyStatus;//审批申请状态
    @JsonProperty(value = "data")
    private Data data;

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getApplySerno() {
        return applySerno;
    }

    public void setApplySerno(String applySerno) {
        this.applySerno = applySerno;
    }

    public String getRqstrName() {
        return rqstrName;
    }

    public void setRqstrName(String rqstrName) {
        this.rqstrName = rqstrName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getAssureMeansMain() {
        return assureMeansMain;
    }

    public void setAssureMeansMain(String assureMeansMain) {
        this.assureMeansMain = assureMeansMain;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getTpGuarWays() {
        return tpGuarWays;
    }

    public void setTpGuarWays(String tpGuarWays) {
        this.tpGuarWays = tpGuarWays;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getOnlineDate() {
        return onlineDate;
    }

    public void setOnlineDate(String onlineDate) {
        this.onlineDate = onlineDate;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getBizSour() {
        return bizSour;
    }

    public void setBizSour(String bizSour) {
        this.bizSour = bizSour;
    }

    public String getRecomId() {
        return recomId;
    }

    public void setRecomId(String recomId) {
        this.recomId = recomId;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getMultiplexSerno() {
        return multiplexSerno;
    }

    public void setMultiplexSerno(String multiplexSerno) {
        this.multiplexSerno = multiplexSerno;
    }

    public String getLastApprTime() {
        return lastApprTime;
    }

    public void setLastApprTime(String lastApprTime) {
        this.lastApprTime = lastApprTime;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public BigDecimal getThisCrdAmt() {
        return thisCrdAmt;
    }

    public void setThisCrdAmt(BigDecimal thisCrdAmt) {
        this.thisCrdAmt = thisCrdAmt;
    }

    public BigDecimal getThisLoanAmt() {
        return thisLoanAmt;
    }

    public void setThisLoanAmt(BigDecimal thisLoanAmt) {
        this.thisLoanAmt = thisLoanAmt;
    }

    public String getIsLoanAndReturn() {
        return isLoanAndReturn;
    }

    public void setIsLoanAndReturn(String isLoanAndReturn) {
        this.isLoanAndReturn = isLoanAndReturn;
    }

    public String getCancelFlag() {
        return cancelFlag;
    }

    public void setCancelFlag(String cancelFlag) {
        this.cancelFlag = cancelFlag;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getMultiplexedSerno() {
        return multiplexedSerno;
    }

    public void setMultiplexedSerno(String multiplexedSerno) {
        this.multiplexedSerno = multiplexedSerno;
    }

    public BigDecimal getOldRate() {
        return oldRate;
    }

    public void setOldRate(BigDecimal oldRate) {
        this.oldRate = oldRate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getLoanReason() {
        return loanReason;
    }

    public void setLoanReason(String loanReason) {
        this.loanReason = loanReason;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getBillBal() {
        return billBal;
    }

    public void setBillBal(String billBal) {
        this.billBal = billBal;
    }

    public String getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(String loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(BigDecimal loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsOperNormal() {
        return isOperNormal;
    }

    public void setIsOperNormal(String isOperNormal) {
        this.isOperNormal = isOperNormal;
    }

    public String getResult1() {
        return result1;
    }

    public void setResult1(String result1) {
        this.result1 = result1;
    }

    public String getIsOperOwnershipChange() {
        return isOperOwnershipChange;
    }

    public void setIsOperOwnershipChange(String isOperOwnershipChange) {
        this.isOperOwnershipChange = isOperOwnershipChange;
    }

    public String getResult2() {
        return result2;
    }

    public void setResult2(String result2) {
        this.result2 = result2;
    }

    public String getIsDebtAdd50() {
        return isDebtAdd50;
    }

    public void setIsDebtAdd50(String isDebtAdd50) {
        this.isDebtAdd50 = isDebtAdd50;
    }

    public String getResult3() {
        return result3;
    }

    public void setResult3(String result3) {
        this.result3 = result3;
    }

    public String getIsMoreAssure() {
        return isMoreAssure;
    }

    public void setIsMoreAssure(String isMoreAssure) {
        this.isMoreAssure = isMoreAssure;
    }

    public String getResult4() {
        return result4;
    }

    public void setResult4(String result4) {
        this.result4 = result4;
    }

    public String getIsFamilyAdt() {
        return isFamilyAdt;
    }

    public void setIsFamilyAdt(String isFamilyAdt) {
        this.isFamilyAdt = isFamilyAdt;
    }

    public String getResult5() {
        return result5;
    }

    public void setResult5(String result5) {
        this.result5 = result5;
    }

    public String getIsPldimnMoreDvl() {
        return IsPldimnMoreDvl;
    }

    public void setIsPldimnMoreDvl(String isPldimnMoreDvl) {
        IsPldimnMoreDvl = isPldimnMoreDvl;
    }

    public String getResult6() {
        return result6;
    }

    public void setResult6(String result6) {
        this.result6 = result6;
    }

    public String getRateApplyReason() {
        return rateApplyReason;
    }

    public void setRateApplyReason(String rateApplyReason) {
        this.rateApplyReason = rateApplyReason;
    }

    public String getIsWxbxd() {
        return isWxbxd;
    }

    public void setIsWxbxd(String isWxbxd) {
        this.isWxbxd = isWxbxd;
    }

    public String getCusLevel() {
        return cusLevel;
    }

    public void setCusLevel(String cusLevel) {
        this.cusLevel = cusLevel;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getReprName() {
        return reprName;
    }

    public void setReprName(String reprName) {
        this.reprName = reprName;
    }

    public String getOperAddr() {
        return operAddr;
    }

    public void setOperAddr(String operAddr) {
        this.operAddr = operAddr;
    }

    public String getMainOperBusi() {
        return mainOperBusi;
    }

    public void setMainOperBusi(String mainOperBusi) {
        this.mainOperBusi = mainOperBusi;
    }

    public String getBsinsLicYear() {
        return bsinsLicYear;
    }

    public void setBsinsLicYear(String bsinsLicYear) {
        this.bsinsLicYear = bsinsLicYear;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getIndivCredit() {
        return indivCredit;
    }

    public void setIndivCredit(String indivCredit) {
        this.indivCredit = indivCredit;
    }

    public String getIndivCreditMemo() {
        return indivCreditMemo;
    }

    public void setIndivCreditMemo(String indivCreditMemo) {
        this.indivCreditMemo = indivCreditMemo;
    }

    public String getConCredit() {
        return conCredit;
    }

    public void setConCredit(String conCredit) {
        this.conCredit = conCredit;
    }

    public String getConCreditMemo() {
        return conCreditMemo;
    }

    public void setConCreditMemo(String conCreditMemo) {
        this.conCreditMemo = conCreditMemo;
    }

    public String getIndivMarSt() {
        return indivMarSt;
    }

    public void setIndivMarSt(String indivMarSt) {
        this.indivMarSt = indivMarSt;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getSpouseCertNo() {
        return spouseCertNo;
    }

    public void setSpouseCertNo(String spouseCertNo) {
        this.spouseCertNo = spouseCertNo;
    }

    public String getSpousePhone() {
        return spousePhone;
    }

    public void setSpousePhone(String spousePhone) {
        this.spousePhone = spousePhone;
    }

    public String getOldBillAmt() {
        return oldBillAmt;
    }

    public void setOldBillAmt(String oldBillAmt) {
        this.oldBillAmt = oldBillAmt;
    }

    public String getApprId() {
        return apprId;
    }

    public void setApprId(String apprId) {
        this.apprId = apprId;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getCopyFrom() {
        return copyFrom;
    }

    public void setCopyFrom(String copyFrom) {
        this.copyFrom = copyFrom;
    }

    public BigDecimal getAnnualRepayAmt() {
        return annualRepayAmt;
    }

    public void setAnnualRepayAmt(BigDecimal annualRepayAmt) {
        this.annualRepayAmt = annualRepayAmt;
    }

    public String getIsNewEmployee() {
        return isNewEmployee;
    }

    public void setIsNewEmployee(String isNewEmployee) {
        this.isNewEmployee = isNewEmployee;
    }

    public String getNewEmployeeName() {
        return newEmployeeName;
    }

    public void setNewEmployeeName(String newEmployeeName) {
        this.newEmployeeName = newEmployeeName;
    }

    public String getEmployeePhone() {
        return employeePhone;
    }

    public void setEmployeePhone(String employeePhone) {
        this.employeePhone = employeePhone;
    }

    public String getApprApplyStatus() {
        return apprApplyStatus;
    }

    public void setApprApplyStatus(String apprApplyStatus) {
        this.apprApplyStatus = apprApplyStatus;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Data{" +
                "indgtSerno='" + indgtSerno + '\'' +
                ", applySerno='" + applySerno + '\'' +
                ", rqstrName='" + rqstrName + '\'' +
                ", certNo='" + certNo + '\'' +
                ", mobile='" + mobile + '\'' +
                ", billNo='" + billNo + '\'' +
                ", assureMeansMain='" + assureMeansMain + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", tpGuarWays='" + tpGuarWays + '\'' +
                ", loanType='" + loanType + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", onlineDate='" + onlineDate + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", bizSour='" + bizSour + '\'' +
                ", recomId='" + recomId + '\'' +
                ", bizType='" + bizType + '\'' +
                ", multiplexSerno='" + multiplexSerno + '\'' +
                ", lastApprTime='" + lastApprTime + '\'' +
                ", payType='" + payType + '\'' +
                ", contType='" + contType + '\'' +
                ", thisCrdAmt=" + thisCrdAmt +
                ", thisLoanAmt=" + thisLoanAmt +
                ", isLoanAndReturn='" + isLoanAndReturn + '\'' +
                ", cancelFlag='" + cancelFlag + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", multiplexedSerno='" + multiplexedSerno + '\'' +
                ", oldRate=" + oldRate +
                ", rate=" + rate +
                ", loanReason='" + loanReason + '\'' +
                ", loanUseType='" + loanUseType + '\'' +
                ", billBal='" + billBal + '\'' +
                ", loanAmt='" + loanAmt + '\'' +
                ", loanTerm=" + loanTerm +
                ", repayType='" + repayType + '\'' +
                ", cusId='" + cusId + '\'' +
                ", isOperNormal='" + isOperNormal + '\'' +
                ", result1='" + result1 + '\'' +
                ", isOperOwnershipChange='" + isOperOwnershipChange + '\'' +
                ", result2='" + result2 + '\'' +
                ", isDebtAdd50='" + isDebtAdd50 + '\'' +
                ", result3='" + result3 + '\'' +
                ", isMoreAssure='" + isMoreAssure + '\'' +
                ", result4='" + result4 + '\'' +
                ", isFamilyAdt='" + isFamilyAdt + '\'' +
                ", result5='" + result5 + '\'' +
                ", IsPldimnMoreDvl='" + IsPldimnMoreDvl + '\'' +
                ", result6='" + result6 + '\'' +
                ", rateApplyReason='" + rateApplyReason + '\'' +
                ", isWxbxd='" + isWxbxd + '\'' +
                ", cusLevel='" + cusLevel + '\'' +
                ", conName='" + conName + '\'' +
                ", reprName='" + reprName + '\'' +
                ", operAddr='" + operAddr + '\'' +
                ", mainOperBusi='" + mainOperBusi + '\'' +
                ", bsinsLicYear='" + bsinsLicYear + '\'' +
                ", trade='" + trade + '\'' +
                ", indivCredit='" + indivCredit + '\'' +
                ", indivCreditMemo='" + indivCreditMemo + '\'' +
                ", conCredit='" + conCredit + '\'' +
                ", conCreditMemo='" + conCreditMemo + '\'' +
                ", indivMarSt='" + indivMarSt + '\'' +
                ", spouseName='" + spouseName + '\'' +
                ", spouseCertNo='" + spouseCertNo + '\'' +
                ", spousePhone='" + spousePhone + '\'' +
                ", oldBillAmt='" + oldBillAmt + '\'' +
                ", apprId='" + apprId + '\'' +
                ", checkDate='" + checkDate + '\'' +
                ", copyFrom='" + copyFrom + '\'' +
                ", annualRepayAmt=" + annualRepayAmt +
                ", isNewEmployee='" + isNewEmployee + '\'' +
                ", newEmployeeName='" + newEmployeeName + '\'' +
                ", employeePhone='" + employeePhone + '\'' +
                ", apprApplyStatus='" + apprApplyStatus + '\'' +
                ", data=" + data +
                '}';
    }
}
