package cn.com.yusys.yusp.dto.server.biz.xdht0010.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询符合条件的省心快贷合同
 * @author xll
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdht0010RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdht0010RespDto{" +
                "data=" + data +
                '}';
    }
}  
