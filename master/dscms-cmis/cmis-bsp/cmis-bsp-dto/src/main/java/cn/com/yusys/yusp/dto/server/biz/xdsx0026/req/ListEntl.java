package cn.com.yusys.yusp.dto.server.biz.xdsx0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/19 14:02:<br>
 * 企业经营信息列表
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 14:02
 * @since 2021/5/19 14:02
 */
@JsonPropertyOrder(alphabetic = true)
public class ListEntl implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "app_no")
    private String app_no;//申请流水号
    @JsonProperty(value = "ent_name")
    private String ent_name;//企业名称
    @JsonProperty(value = "ent_region")
    private String ent_region;//企业所在区域
    @JsonProperty(value = "ent_buz_lic_no")
    private String ent_buz_lic_no;//营业执照号
    @JsonProperty(value = "ent_buz_scope")
    private String ent_buz_scope;//经营范围
    @JsonProperty(value = "ent_annual_sales")
    private BigDecimal ent_annual_sales;//年销售收入

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getEnt_name() {
        return ent_name;
    }

    public void setEnt_name(String ent_name) {
        this.ent_name = ent_name;
    }

    public String getEnt_region() {
        return ent_region;
    }

    public void setEnt_region(String ent_region) {
        this.ent_region = ent_region;
    }

    public String getEnt_buz_lic_no() {
        return ent_buz_lic_no;
    }

    public void setEnt_buz_lic_no(String ent_buz_lic_no) {
        this.ent_buz_lic_no = ent_buz_lic_no;
    }

    public String getEnt_buz_scope() {
        return ent_buz_scope;
    }

    public void setEnt_buz_scope(String ent_buz_scope) {
        this.ent_buz_scope = ent_buz_scope;
    }

    public BigDecimal getEnt_annual_sales() {
        return ent_annual_sales;
    }

    public void setEnt_annual_sales(BigDecimal ent_annual_sales) {
        this.ent_annual_sales = ent_annual_sales;
    }

    @Override
    public String toString() {
        return "ListEntl{" +
                "app_no='" + app_no + '\'' +
                ", ent_name='" + ent_name + '\'' +
                ", ent_region='" + ent_region + '\'' +
                ", ent_buz_lic_no='" + ent_buz_lic_no + '\'' +
                ", ent_buz_scope='" + ent_buz_scope + '\'' +
                ", ent_annual_sales=" + ent_annual_sales +
                '}';
    }
}
