package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

// 年报-股权变更信息
@JsonPropertyOrder(alphabetic = true)
public class YEARREPORTALTERSTOCK implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ALTDATE")
    private String ALTDATE;//	股权变更日期
    @JsonProperty(value = "ANCHEID")
    private String ANCHEID;//	年报ID
    @JsonProperty(value = "INV")
    private String INV;//	股东名称
    @JsonProperty(value = "TRANSAMAFT")
    private String TRANSAMAFT;//	转让后股权比例
    @JsonProperty(value = "TRANSAMPR")
    private String TRANSAMPR;//	转让前股权比例

    @JsonIgnore
    public String getALTDATE() {
        return ALTDATE;
    }

    @JsonIgnore
    public void setALTDATE(String ALTDATE) {
        this.ALTDATE = ALTDATE;
    }

    @JsonIgnore
    public String getANCHEID() {
        return ANCHEID;
    }

    @JsonIgnore
    public void setANCHEID(String ANCHEID) {
        this.ANCHEID = ANCHEID;
    }

    @JsonIgnore
    public String getINV() {
        return INV;
    }

    @JsonIgnore
    public void setINV(String INV) {
        this.INV = INV;
    }

    @JsonIgnore
    public String getTRANSAMAFT() {
        return TRANSAMAFT;
    }

    @JsonIgnore
    public void setTRANSAMAFT(String TRANSAMAFT) {
        this.TRANSAMAFT = TRANSAMAFT;
    }

    @JsonIgnore
    public String getTRANSAMPR() {
        return TRANSAMPR;
    }

    @JsonIgnore
    public void setTRANSAMPR(String TRANSAMPR) {
        this.TRANSAMPR = TRANSAMPR;
    }

    @Override
    public String toString() {
        return "YEARREPORTALTERSTOCK{" +
                "ALTDATE='" + ALTDATE + '\'' +
                ", ANCHEID='" + ANCHEID + '\'' +
                ", INV='" + INV + '\'' +
                ", TRANSAMAFT='" + TRANSAMAFT + '\'' +
                ", TRANSAMPR='" + TRANSAMPR + '\'' +
                '}';
    }
}
