package cn.com.yusys.yusp.dto.server.biz.xdxw0022.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "loanType")
    private String loanType;//贷款类型
    @JsonProperty(value = "bizType")
    private String bizType;//业务类型
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式名称
    @JsonProperty(value = "applyDate")
    private String applyDate;//申请日期
    @JsonProperty(value = "loanStatus")
    private String loanStatus;//贷款状态
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称
    @JsonProperty(value = "orgName")
    private String orgName;//机构名称

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                ", loanType='" + loanType + '\'' +
                ", bizType='" + bizType + '\'' +
                ", cusName='" + cusName + '\'' +
                ", applyAmt=" + applyAmt +
                ", assureMeans='" + assureMeans + '\'' +
                ", applyDate='" + applyDate + '\'' +
                ", loanStatus='" + loanStatus + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", orgName='" + orgName + '\'' +
                '}';
    }
}
