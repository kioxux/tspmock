package cn.com.yusys.yusp.dto.server.biz.xdqt0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：微业贷信贷文件处理通知
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    @JsonProperty(value = "opflag")
    private String opflag;//是否可处理标志 01：可处理 02：不可处理
    @JsonProperty(value = "buzdate")
    private String buzdate;//业务日期 yyyy-MM-dd

    public String getOpflag() {
        return opflag;
    }

    public void setOpflag(String opflag) {
        this.opflag = opflag;
    }

    public String getBuzdate() {
        return buzdate;
    }

    public void setBuzdate(String buzdate) {
        this.buzdate = buzdate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "opflag='" + opflag + '\'' +
                ", buzdate='" + buzdate + '\'' +
                '}';
    }
}
