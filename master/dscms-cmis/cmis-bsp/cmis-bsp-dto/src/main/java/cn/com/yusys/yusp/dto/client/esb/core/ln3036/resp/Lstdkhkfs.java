package cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：贷款组合还款方式
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkhkfs implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "shengxrq")
    private String shengxrq;//生效日期
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额
    @JsonProperty(value = "huanbzhq")
    private String huanbzhq;//还本周期
    @JsonProperty(value = "yuqhkzhq")
    private String yuqhkzhq;//逾期还款周期

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    @Override
    public String toString() {
        return "Lstdkhkfs{" +
                "huankfsh='" + huankfsh + '\'' +
                ", shengxrq='" + shengxrq + '\'' +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", huanbzhq='" + huanbzhq + '\'' +
                ", yuqhkzhq='" + yuqhkzhq + '\'' +
                '}';
    }
}
