package cn.com.yusys.yusp.dto.client.esb.core.ln3174;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：资产证券化欠款借据查询
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3174RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "lstZhzb")
    private java.util.List<LstZhzb> lstZhzb;//主表信息[LIST]

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List<LstZhzb> getLstZhzb() {
        return lstZhzb;
    }

    public void setLstZhzb(List<LstZhzb> lstZhzb) {
        this.lstZhzb = lstZhzb;
    }

    @Override
    public String toString() {
        return "Ln3174RespDto{" +
                "zongbish=" + zongbish +
                ", lstZhzb=" + lstZhzb +
                '}';
    }
}
