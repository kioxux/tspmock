package cn.com.yusys.yusp.dto.server.lmt.xded0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：个人额度查询（新微贷）
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    //客户编号
    private String cusId;
    @JsonProperty(value = "lmtSubNo")
    //额度品种编号
    private String lmtSubNo;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLmtSubNo() {
        return lmtSubNo;
    }

    public void setLmtSubNo(String lmtSubNo) {
        this.lmtSubNo = lmtSubNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", lmtSubNo='" + lmtSubNo + '\'' +
                '}';
    }
}