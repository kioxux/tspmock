package cn.com.yusys.yusp.dto.server.biz.xdtz0045.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "tranDate")
    private String tranDate;//交易日期
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "loanAccount")
    private String loanAccount;//贷款账号
    @JsonProperty(value = "reqPageNum")
    private String reqPageNum;//请求页码
    @JsonProperty(value = "totlRecordQnt")
    private String totlRecordQnt;//总记录数
    @JsonProperty(value = "totlPageNum")
    private String totlPageNum;//总页数

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getLoanAccount() {
        return loanAccount;
    }

    public void setLoanAccount(String loanAccount) {
        this.loanAccount = loanAccount;
    }

    public String getReqPageNum() {
        return reqPageNum;
    }

    public void setReqPageNum(String reqPageNum) {
        this.reqPageNum = reqPageNum;
    }

    public String getTotlRecordQnt() {
        return totlRecordQnt;
    }

    public void setTotlRecordQnt(String totlRecordQnt) {
        this.totlRecordQnt = totlRecordQnt;
    }

    public String getTotlPageNum() {
        return totlPageNum;
    }

    public void setTotlPageNum(String totlPageNum) {
        this.totlPageNum = totlPageNum;
    }

    @Override
    public String toString() {
        return "Data{" +
                "tranDate='" + tranDate + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certNo='" + certNo + '\'' +
                ", loanAccount='" + loanAccount + '\'' +
                ", reqPageNum='" + reqPageNum + '\'' +
                ", totlRecordQnt='" + totlRecordQnt + '\'' +
                ", totlPageNum='" + totlPageNum + '\'' +
                '}';
    }
}
