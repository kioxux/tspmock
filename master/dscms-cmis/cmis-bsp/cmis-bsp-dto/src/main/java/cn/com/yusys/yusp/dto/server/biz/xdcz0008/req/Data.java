package cn.com.yusys.yusp.dto.server.biz.xdcz0008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：代开保函
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "amt")
    private BigDecimal amt;//开证金额
    @JsonProperty("valuestartDate")
    private String startDate;//开证日期
    @JsonProperty(value = "endDate")
    private String endDate;//信用证到期日
    @JsonProperty(value = "huser")
    private String huser;//经办人
    @JsonProperty(value = "bankNo")
    private String bankNo;//他行开证行号
    @JsonProperty(value = "bankName")
    private String bankName;//他行开证名称
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户中文名称
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "guarantNo")
    private String guarantNo;//信用证号码
    @JsonProperty(value = "oprType")
    private String oprType;//操作类型
    @JsonProperty(value = "limitType")
    private String limitType;//同业间额度类型

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getHuser() {
        return huser;
    }

    public void setHuser(String huser) {
        this.huser = huser;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getGuarantNo() {
        return guarantNo;
    }

    public void setGuarantNo(String guarantNo) {
        this.guarantNo = guarantNo;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    @Override
    public String toString() {
        return "Xdcz0009DataReqDto{" +
                "amt='" + amt + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                "huser='" + huser + '\'' +
                "bankNo='" + bankNo + '\'' +
                "bankName='" + bankName + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "serno='" + serno + '\'' +
                "guarantNo='" + guarantNo + '\'' +
                "oprType='" + oprType + '\'' +
                "limitType='" + limitType + '\'' +
                '}';
    }
}
