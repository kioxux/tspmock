package cn.com.yusys.yusp.dto.server.biz.xdzc0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：客户资产清单查询接口
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "assetNo")
    private String assetNo;//资产编号
    @JsonProperty(value = "guarNo")
    private String guarNo;//统一押品编号
    @JsonProperty(value = "assetType")
    private String assetType;//资产类型
    @JsonProperty(value = "assetValue")
    private BigDecimal assetValue;//资产价值
    @JsonProperty(value = "assetEndDate")
    private String assetEndDate;//资产到期日
    @JsonProperty(value = "assetStatus")
    private String assetStatus;//资产状态
    @JsonProperty(value = "isPool")
    private String isPool;//是否入池
    @JsonProperty(value = "isPledge")
    private String isPledge;//是否入池质押
    @JsonProperty(value = "inpTime")
    private String inpTime;//入池时间
    @JsonProperty(value = "outpTime")
    private String outpTime;//出池时间

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public BigDecimal getAssetValue() {
        return assetValue;
    }

    public void setAssetValue(BigDecimal assetValue) {
        this.assetValue = assetValue;
    }

    public String getAssetEndDate() {
        return assetEndDate;
    }

    public void setAssetEndDate(String assetEndDate) {
        this.assetEndDate = assetEndDate;
    }

    public String getAssetStatus() {
        return assetStatus;
    }

    public void setAssetStatus(String assetStatus) {
        this.assetStatus = assetStatus;
    }

    public String getIsPool() {
        return isPool;
    }

    public void setIsPool(String isPool) {
        this.isPool = isPool;
    }

    public String getIsPledge() {
        return isPledge;
    }

    public void setIsPledge(String isPledge) {
        this.isPledge = isPledge;
    }

    public String getInpTime() {
        return inpTime;
    }

    public void setInpTime(String inpTime) {
        this.inpTime = inpTime;
    }

    public String getOutpTime() {
        return outpTime;
    }

    public void setOutpTime(String outpTime) {
        this.outpTime = outpTime;
    }

    @Override
    public String toString() {
        return "List{" +
                "assetNo='" + assetNo + '\'' +
                ", guarNo='" + guarNo + '\'' +
                ", assetType='" + assetType + '\'' +
                ", assetValue=" + assetValue +
                ", assetEndDate='" + assetEndDate + '\'' +
                ", assetStatus='" + assetStatus + '\'' +
                ", isPool='" + isPool + '\'' +
                ", isPledge='" + isPledge + '\'' +
                ", inpTime='" + inpTime + '\'' +
                ", outpTime='" + outpTime + '\'' +
                '}';
    }
}
