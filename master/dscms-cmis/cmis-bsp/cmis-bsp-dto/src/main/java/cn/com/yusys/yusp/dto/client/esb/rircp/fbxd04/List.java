package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查找指定数据日期的放款合约明细记录历史表（利翃）一览
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cust_id_core")
    private String cust_id_core;//核心客户号
    @JsonProperty(value = "cust_name")
    private String cust_name;//客户姓名
    @JsonProperty(value = "contract_no")
    private String contract_no;//融资平台贷款合同号
    @JsonProperty(value = "encash_amt")
    private String encash_amt;//放款金额
    @JsonProperty(value = "day_rate")
    private String day_rate;//日利率
    @JsonProperty(value = "start_date")
    private String start_date;//贷款起息日
    @JsonProperty(value = "end_date")
    private String end_date;//贷款到期日
    @JsonProperty(value = "apply_date")
    private String apply_date;//申请支用日期
    @JsonProperty(value = "encash_date")
    private String encash_date;//放款日期
    @JsonProperty(value = "repay_mode ")
    private String repay_mode;//还款方式

    public String getCust_id_core() {
        return cust_id_core;
    }

    public void setCust_id_core(String cust_id_core) {
        this.cust_id_core = cust_id_core;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public String getEncash_amt() {
        return encash_amt;
    }

    public void setEncash_amt(String encash_amt) {
        this.encash_amt = encash_amt;
    }

    public String getDay_rate() {
        return day_rate;
    }

    public void setDay_rate(String day_rate) {
        this.day_rate = day_rate;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getApply_date() {
        return apply_date;
    }

    public void setApply_date(String apply_date) {
        this.apply_date = apply_date;
    }

    public String getEncash_date() {
        return encash_date;
    }

    public void setEncash_date(String encash_date) {
        this.encash_date = encash_date;
    }

    public String getRepay_mode() {
        return repay_mode;
    }

    public void setRepay_mode(String repay_mode) {
        this.repay_mode = repay_mode;
    }

    @Override
    public String toString() {
        return "List{" +
                "cust_id_core='" + cust_id_core + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", contract_no='" + contract_no + '\'' +
                ", encash_amt='" + encash_amt + '\'' +
                ", day_rate='" + day_rate + '\'' +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", apply_date='" + apply_date + '\'' +
                ", encash_date='" + encash_date + '\'' +
                ", repay_mode='" + repay_mode + '\'' +
                '}';
    }
}
