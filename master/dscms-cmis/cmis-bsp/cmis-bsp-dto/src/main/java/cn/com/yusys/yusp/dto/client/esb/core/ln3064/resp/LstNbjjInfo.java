package cn.com.yusys.yusp.dto.client.esb.core.ln3064.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 14:15
 * @since 2021/6/7 14:15
 */
@JsonPropertyOrder(alphabetic = true)
public class LstNbjjInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "nbjiejuh")
    private String nbjiejuh;//内部借据号
    @JsonProperty(value = "nbjjxzhi")
    private String nbjjxzhi;//内部借据性质
    @JsonProperty(value = "weituoje")
    private BigDecimal weituoje;//委托金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "bjghrzxh")
    private String bjghrzxh;//本金归还入账账号子序号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号
    @JsonProperty(value = "lxghrzxh")
    private String lxghrzxh;//利息归还入账账号子序号
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注
    @JsonProperty(value = "byxinxi1")
    private String byxinxi1;//备用信息1
    @JsonProperty(value = "byxinxi2")
    private String byxinxi2;//备用信息2
    @JsonProperty(value = "byxinxi3")
    private String byxinxi3;//备用信息3
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "tiaozhje")
    private BigDecimal tiaozhje;//调整金额

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getNbjiejuh() {
        return nbjiejuh;
    }

    public void setNbjiejuh(String nbjiejuh) {
        this.nbjiejuh = nbjiejuh;
    }

    public String getNbjjxzhi() {
        return nbjjxzhi;
    }

    public void setNbjjxzhi(String nbjjxzhi) {
        this.nbjjxzhi = nbjjxzhi;
    }

    public BigDecimal getWeituoje() {
        return weituoje;
    }

    public void setWeituoje(BigDecimal weituoje) {
        this.weituoje = weituoje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getByxinxi1() {
        return byxinxi1;
    }

    public void setByxinxi1(String byxinxi1) {
        this.byxinxi1 = byxinxi1;
    }

    public String getByxinxi2() {
        return byxinxi2;
    }

    public void setByxinxi2(String byxinxi2) {
        this.byxinxi2 = byxinxi2;
    }

    public String getByxinxi3() {
        return byxinxi3;
    }

    public void setByxinxi3(String byxinxi3) {
        this.byxinxi3 = byxinxi3;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getTiaozhje() {
        return tiaozhje;
    }

    public void setTiaozhje(BigDecimal tiaozhje) {
        this.tiaozhje = tiaozhje;
    }

    @Override
    public String toString() {
        return "Ln3064RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "nbjiejuh='" + nbjiejuh + '\'' +
                "nbjjxzhi='" + nbjjxzhi + '\'' +
                "weituoje='" + weituoje + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "zhchbjin='" + zhchbjin + '\'' +
                "yuqibjin='" + yuqibjin + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "fulililv='" + fulililv + '\'' +
                "ysyjlixi='" + ysyjlixi + '\'' +
                "ysqianxi='" + ysqianxi + '\'' +
                "ysyjfaxi='" + ysyjfaxi + '\'' +
                "yshofaxi='" + yshofaxi + '\'' +
                "yingjifx='" + yingjifx + '\'' +
                "fuxiiiii='" + fuxiiiii + '\'' +
                "bjghrzzh='" + bjghrzzh + '\'' +
                "bjghrzxh='" + bjghrzxh + '\'' +
                "lxghrzzh='" + lxghrzzh + '\'' +
                "lxghrzxh='" + lxghrzxh + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "byxinxi1='" + byxinxi1 + '\'' +
                "byxinxi2='" + byxinxi2 + '\'' +
                "byxinxi3='" + byxinxi3 + '\'' +
                "dzhibjin='" + dzhibjin + '\'' +
                "daizbjin='" + daizbjin + '\'' +
                "tiaozhje='" + tiaozhje + '\'' +
                '}';
    }
}
