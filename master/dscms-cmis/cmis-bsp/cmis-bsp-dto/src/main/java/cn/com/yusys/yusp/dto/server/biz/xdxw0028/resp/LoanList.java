package cn.com.yusys.yusp.dto.server.biz.xdxw0028.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：根据客户号查询我行现有融资情况
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LoanList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusNo")
    private String cusNo;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "billBal")
    private BigDecimal billBal;//借据余额
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "cnAssureMeans")
    private String cnAssureMeans;//担保方式中文
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行利率（%）
    @JsonProperty(value = "startDate")
    private String startDate;//起始日期
    @JsonProperty(value = "endDate")
    private String endDate;//截至日期
    @JsonProperty(value = "billStatus")
    private String billStatus;//借据状态
    @JsonProperty(value = "creditLoanAmt")
    private BigDecimal creditLoanAmt;//信用经营性贷款金额
    @JsonProperty(value = "creditConsumeAmt")
    private BigDecimal creditConsumeAmt;//信用经营性消费金额

    public String getCusNo() {
        return cusNo;
    }

    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCnAssureMeans() {
        return cnAssureMeans;
    }

    public void setCnAssureMeans(String cnAssureMeans) {
        this.cnAssureMeans = cnAssureMeans;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public BigDecimal getCreditLoanAmt() {
        return creditLoanAmt;
    }

    public void setCreditLoanAmt(BigDecimal creditLoanAmt) {
        this.creditLoanAmt = creditLoanAmt;
    }

    public BigDecimal getCreditConsumeAmt() {
        return creditConsumeAmt;
    }

    public void setCreditConsumeAmt(BigDecimal creditConsumeAmt) {
        this.creditConsumeAmt = creditConsumeAmt;
    }

    @Override
    public String toString() {
        return "LoanList{" +
                "cusNo='" + cusNo + '\'' +
                ", cusName='" + cusName + '\'' +
                ", billBal=" + billBal +
                ", prdName='" + prdName + '\'' +
                ", cnAssureMeans='" + cnAssureMeans + '\'' +
                ", realityIrY=" + realityIrY +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", billStatus='" + billStatus + '\'' +
                ", creditLoanAmt=" + creditLoanAmt +
                ", creditConsumeAmt=" + creditConsumeAmt +
                '}';
    }
}
