package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 企业基本信息列表
 */
@JsonPropertyOrder(alphabetic = true)
public class BASICLIST implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "APPRDATE")
    private String APPRDATE;//核准日期
    @JsonProperty(value = "CANDATE")
    private String CANDATE;//注销日期
    @JsonProperty(value = "CREDITCODE")
    private String CREDITCODE;//统一信用代码
    @JsonProperty(value = "ENTITYTYPE")
    private String ENTITYTYPE;//实体类型
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;//企业名称
    @JsonProperty(value = "ENTSTATUS")
    private String ENTSTATUS;//经营状态
    @JsonProperty(value = "ENTSTATUSCODE")
    private String ENTSTATUSCODE;//经营状态编码
    @JsonProperty(value = "ENTTYPE")
    private String ENTTYPE;//企业类型
    @JsonProperty(value = "ESDATE")
    private String ESDATE;//成立日期
    @JsonProperty(value = "FRNAME")
    private String FRNAME;//法定代表人/负责人/执行事务合伙人
    @JsonProperty(value = "ID")
    private String ID;//企业ID
    @JsonProperty(value = "ORGCODES")
    private String ORGCODES;//组织机构代码
    @JsonProperty(value = "REGCAP")
    private String REGCAP;//注册资本（企业:万元）
    @JsonProperty(value = "REGNO")
    private String REGNO;//注册号
    @JsonProperty(value = "REGORGPROVINCE")
    private String REGORGPROVINCE;//所在省份
    @JsonProperty(value = "REVDATE")
    private String REVDATE;//吊销日期

    @JsonIgnore
    public String getAPPRDATE() {
        return APPRDATE;
    }

    @JsonIgnore
    public void setAPPRDATE(String APPRDATE) {
        this.APPRDATE = APPRDATE;
    }

    @JsonIgnore
    public String getCANDATE() {
        return CANDATE;
    }

    @JsonIgnore
    public void setCANDATE(String CANDATE) {
        this.CANDATE = CANDATE;
    }

    @JsonIgnore
    public String getCREDITCODE() {
        return CREDITCODE;
    }

    @JsonIgnore
    public void setCREDITCODE(String CREDITCODE) {
        this.CREDITCODE = CREDITCODE;
    }

    @JsonIgnore
    public String getENTITYTYPE() {
        return ENTITYTYPE;
    }

    @JsonIgnore
    public void setENTITYTYPE(String ENTITYTYPE) {
        this.ENTITYTYPE = ENTITYTYPE;
    }

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getENTSTATUS() {
        return ENTSTATUS;
    }

    @JsonIgnore
    public void setENTSTATUS(String ENTSTATUS) {
        this.ENTSTATUS = ENTSTATUS;
    }

    @JsonIgnore
    public String getENTSTATUSCODE() {
        return ENTSTATUSCODE;
    }

    @JsonIgnore
    public void setENTSTATUSCODE(String ENTSTATUSCODE) {
        this.ENTSTATUSCODE = ENTSTATUSCODE;
    }

    @JsonIgnore
    public String getENTTYPE() {
        return ENTTYPE;
    }

    @JsonIgnore
    public void setENTTYPE(String ENTTYPE) {
        this.ENTTYPE = ENTTYPE;
    }

    @JsonIgnore
    public String getESDATE() {
        return ESDATE;
    }

    @JsonIgnore
    public void setESDATE(String ESDATE) {
        this.ESDATE = ESDATE;
    }

    @JsonIgnore
    public String getFRNAME() {
        return FRNAME;
    }

    @JsonIgnore
    public void setFRNAME(String FRNAME) {
        this.FRNAME = FRNAME;
    }

    @JsonIgnore
    public String getID() {
        return ID;
    }

    @JsonIgnore
    public void setID(String ID) {
        this.ID = ID;
    }

    @JsonIgnore
    public String getORGCODES() {
        return ORGCODES;
    }

    @JsonIgnore
    public void setORGCODES(String ORGCODES) {
        this.ORGCODES = ORGCODES;
    }

    @JsonIgnore
    public String getREGCAP() {
        return REGCAP;
    }

    @JsonIgnore
    public void setREGCAP(String REGCAP) {
        this.REGCAP = REGCAP;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @JsonIgnore
    public String getREGORGPROVINCE() {
        return REGORGPROVINCE;
    }

    @JsonIgnore
    public void setREGORGPROVINCE(String REGORGPROVINCE) {
        this.REGORGPROVINCE = REGORGPROVINCE;
    }

    @JsonIgnore
    public String getREVDATE() {
        return REVDATE;
    }

    @JsonIgnore
    public void setREVDATE(String REVDATE) {
        this.REVDATE = REVDATE;
    }

    @Override
    public String toString() {
        return "BASICLIST{" +
                "APPRDATE='" + APPRDATE + '\'' +
                ", CANDATE='" + CANDATE + '\'' +
                ", CREDITCODE='" + CREDITCODE + '\'' +
                ", ENTITYTYPE='" + ENTITYTYPE + '\'' +
                ", ENTNAME='" + ENTNAME + '\'' +
                ", ENTSTATUS='" + ENTSTATUS + '\'' +
                ", ENTSTATUSCODE='" + ENTSTATUSCODE + '\'' +
                ", ENTTYPE='" + ENTTYPE + '\'' +
                ", ESDATE='" + ESDATE + '\'' +
                ", FRNAME='" + FRNAME + '\'' +
                ", ID='" + ID + '\'' +
                ", ORGCODES='" + ORGCODES + '\'' +
                ", REGCAP='" + REGCAP + '\'' +
                ", REGNO='" + REGNO + '\'' +
                ", REGORGPROVINCE='" + REGORGPROVINCE + '\'' +
                ", REVDATE='" + REVDATE + '\'' +
                '}';
    }
}
