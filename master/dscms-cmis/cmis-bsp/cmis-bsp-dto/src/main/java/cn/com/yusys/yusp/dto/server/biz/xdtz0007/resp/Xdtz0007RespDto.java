package cn.com.yusys.yusp.dto.server.biz.xdtz0007.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据客户号获取非信用方式发放贷款的最长到期日
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0007RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdtz0007RespDto{" +
				"data=" + data +
				'}';
	}
}
