package cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/29 16:15
 * @since 2021/5/29 16:15
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "ywbhyp")
    private String ywbhyp;//业务编号
    @JsonProperty(value = "ywlxyp")
    private String ywlxyp;//业务类型
    @JsonProperty(value = "yptybh")
    private String yptybh;//押品统一编号
    @JsonProperty(value = "isflag")
    private String isflag;//是否有效

    public String getYwbhyp() {
        return ywbhyp;
    }

    public void setYwbhyp(String ywbhyp) {
        this.ywbhyp = ywbhyp;
    }

    public String getYwlxyp() {
        return ywlxyp;
    }

    public void setYwlxyp(String ywlxyp) {
        this.ywlxyp = ywlxyp;
    }

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getIsflag() {
        return isflag;
    }

    public void setIsflag(String isflag) {
        this.isflag = isflag;
    }

    @Override
    public String toString() {
        return "List{" +
                "ywbhyp='" + ywbhyp + '\'' +
                ", ywlxyp='" + ywlxyp + '\'' +
                ", yptybh='" + yptybh + '\'' +
                ", isflag='" + isflag + '\'' +
                '}';
    }
}
