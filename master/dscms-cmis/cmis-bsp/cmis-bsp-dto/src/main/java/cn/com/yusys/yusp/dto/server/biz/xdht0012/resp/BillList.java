package cn.com.yusys.yusp.dto.server.biz.xdht0012.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：借款担保合同签订/支用
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class BillList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户代码
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "bizType")
    private String bizType;//业务品种
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//贷款金额(元)
    @JsonProperty(value = "loanBal")
    private BigDecimal loanBal;//贷款余额(元)
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款起始日
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款终止日
    @JsonProperty(value = "term")
    private String term;//期限
    @JsonProperty(value = "loanAcctNo")
    private String loanAcctNo;//贷款发放账户
    @JsonProperty(value = "loanAcctName")
    private String loanAcctName;//贷款账户名称
    @JsonProperty(value = "loanSubAcctNo")
    private String loanSubAcctNo;//贷款发放账户子账户序号
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行年利率
    @JsonProperty(value = "accStatus")
    private String accStatus;//台帐状态
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getLoanAcctNo() {
        return loanAcctNo;
    }

    public void setLoanAcctNo(String loanAcctNo) {
        this.loanAcctNo = loanAcctNo;
    }

    public String getLoanAcctName() {
        return loanAcctName;
    }

    public void setLoanAcctName(String loanAcctName) {
        this.loanAcctName = loanAcctName;
    }

    public String getLoanSubAcctNo() {
        return loanSubAcctNo;
    }

    public void setLoanSubAcctNo(String loanSubAcctNo) {
        this.loanSubAcctNo = loanSubAcctNo;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    @Override
    public String toString() {
        return "BillList{" +
                "billNo='" + billNo + '\'' +
                "contNo='" + contNo + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "bizType='" + bizType + '\'' +
                "assureMeans='" + assureMeans + '\'' +
                "loanAmt='" + loanAmt + '\'' +
                "loanBal='" + loanBal + '\'' +
                "loanStartDate='" + loanStartDate + '\'' +
                "loanEndDate='" + loanEndDate + '\'' +
                "term='" + term + '\'' +
                "loanAcctNo='" + loanAcctNo + '\'' +
                "loanAcctName='" + loanAcctName + '\'' +
                "loanSubAcctNo='" + loanSubAcctNo + '\'' +
                "realityIrY='" + realityIrY + '\'' +
                "accStatus='" + accStatus + '\'' +
                "managerId='" + managerId + '\'' +
                "managerName='" + managerName + '\'' +
                '}';
    }

}
