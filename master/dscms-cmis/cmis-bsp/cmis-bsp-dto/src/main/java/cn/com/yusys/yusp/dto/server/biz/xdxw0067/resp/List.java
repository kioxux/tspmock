package cn.com.yusys.yusp.dto.server.biz.xdxw0067.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：调查基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//外键
    @JsonProperty(value = "industry_code")
    private String industry_code;//一级行业代码
    @JsonProperty(value = "industry_name")
    private String industry_name;//一级行业名称
    @JsonProperty(value = "industry_detail_code")
    private String industry_detail_code;//细分行业代码
    @JsonProperty(value = "industry_detail_name")
    private String industry_detail_name;//细分行业名称
    @JsonProperty(value = "scale_operation")
    private String scale_operation;//经营规模
    @JsonProperty(value = "years_operation")
    private String years_operation;//经营年限
    @JsonProperty(value = "prop_rights_opera")
    private String prop_rights_opera;//经营场所产权
    @JsonProperty(value = "reamrks")
    private String reamrks;//备注
    @JsonProperty(value = "serno")
    private String serno;//主键
    @JsonProperty(value = "annual_sales_revenue")
    private BigDecimal annual_sales_revenue;//年销售收入
    @JsonProperty(value = "annual_gross_profit")
    private BigDecimal annual_gross_profit;//年利润
    @JsonProperty(value = "cus_list_serno")
    private String cus_list_serno;//名单表流水号


    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getIndustry_code() {
        return industry_code;
    }

    public void setIndustry_code(String industry_code) {
        this.industry_code = industry_code;
    }

    public String getIndustry_name() {
        return industry_name;
    }

    public void setIndustry_name(String industry_name) {
        this.industry_name = industry_name;
    }

    public String getIndustry_detail_code() {
        return industry_detail_code;
    }

    public void setIndustry_detail_code(String industry_detail_code) {
        this.industry_detail_code = industry_detail_code;
    }

    public String getIndustry_detail_name() {
        return industry_detail_name;
    }

    public void setIndustry_detail_name(String industry_detail_name) {
        this.industry_detail_name = industry_detail_name;
    }

    public String getScale_operation() {
        return scale_operation;
    }

    public void setScale_operation(String scale_operation) {
        this.scale_operation = scale_operation;
    }

    public String getYears_operation() {
        return years_operation;
    }

    public void setYears_operation(String years_operation) {
        this.years_operation = years_operation;
    }

    public String getProp_rights_opera() {
        return prop_rights_opera;
    }

    public void setProp_rights_opera(String prop_rights_opera) {
        this.prop_rights_opera = prop_rights_opera;
    }

    public String getReamrks() {
        return reamrks;
    }

    public void setReamrks(String reamrks) {
        this.reamrks = reamrks;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public BigDecimal getAnnual_sales_revenue() {
        return annual_sales_revenue;
    }

    public void setAnnual_sales_revenue(BigDecimal annual_sales_revenue) {
        this.annual_sales_revenue = annual_sales_revenue;
    }

    public BigDecimal getAnnual_gross_profit() {
        return annual_gross_profit;
    }

    public void setAnnual_gross_profit(BigDecimal annual_gross_profit) {
        this.annual_gross_profit = annual_gross_profit;
    }

    public String getCus_list_serno() {
        return cus_list_serno;
    }

    public void setCus_list_serno(String cus_list_serno) {
        this.cus_list_serno = cus_list_serno;
    }

    @Override
    public String toString() {
        return "List{" +
                "survey_serno='" + survey_serno + '\'' +
                ", industry_code='" + industry_code + '\'' +
                ", industry_name='" + industry_name + '\'' +
                ", industry_detail_code='" + industry_detail_code + '\'' +
                ", industry_detail_name='" + industry_detail_name + '\'' +
                ", scale_operation='" + scale_operation + '\'' +
                ", years_operation='" + years_operation + '\'' +
                ", prop_rights_opera='" + prop_rights_opera + '\'' +
                ", reamrks='" + reamrks + '\'' +
                ", serno='" + serno + '\'' +
                ", annual_sales_revenue=" + annual_sales_revenue +
                ", annual_gross_profit=" + annual_gross_profit +
                ", cus_list_serno='" + cus_list_serno + '\'' +
                '}';
    }
}
