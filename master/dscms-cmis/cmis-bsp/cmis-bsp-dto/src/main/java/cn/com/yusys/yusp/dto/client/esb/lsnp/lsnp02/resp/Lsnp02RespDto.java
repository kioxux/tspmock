package cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：信用卡业务零售评级
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lsnp02RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "businum")
    private String businum;//业务受理编号
    @JsonProperty(value = "trstatus")
    private String trstatus;//交易状态
    @JsonProperty(value = "txsts")
    private String txsts;//业务状态
    @JsonProperty(value = "sxf")
    private BigDecimal sxf;//手续费
    @JsonProperty(value = "apply_seq")
    private String apply_seq;//申请流水号
    @JsonProperty(value = "inpret_val")
    private BigDecimal inpret_val;//数字解读值
    @JsonProperty(value = "inpret_val_risk_lvl")
    private String inpret_val_risk_lvl;//数字解读值风险等级
    @JsonProperty(value = "apply_score")
    private BigDecimal apply_score;//申请评分
    @JsonProperty(value = "apply_score_risk_lvl")
    private String apply_score_risk_lvl;//申请评分风险等级
    @JsonProperty(value = "lmt_advice")
    private BigDecimal lmt_advice;//额度建议
    @JsonProperty(value = "price_advice")
    private BigDecimal price_advice;//定价建议
    @JsonProperty(value = "rule_risk_lvl")
    private String rule_risk_lvl;//规则风险等级
    @JsonProperty(value = "fund_deposit_base")
    private String fund_deposit_base;//住房公积金缴存基数
    @JsonProperty(value = "aum")
    private String aum;//AUM值
    @JsonProperty(value = "day_rate")
    private String day_rate;//乐悠金日利率
    @JsonProperty(value = "limit_flag")
    private String limit_flag;//自动化审批标志
    @JsonProperty(value = "complex_risk_lvl")
    private String complex_risk_lvl;//综合风险等级
    @JsonProperty(value = "cust_lvl")
    private String cust_lvl;//客户等级

    public String getBusinum() {
        return businum;
    }

    public void setBusinum(String businum) {
        this.businum = businum;
    }

    public String getTrstatus() {
        return trstatus;
    }

    public void setTrstatus(String trstatus) {
        this.trstatus = trstatus;
    }

    public String getTxsts() {
        return txsts;
    }

    public void setTxsts(String txsts) {
        this.txsts = txsts;
    }

    public BigDecimal getSxf() {
        return sxf;
    }

    public void setSxf(BigDecimal sxf) {
        this.sxf = sxf;
    }

    public String getApply_seq() {
        return apply_seq;
    }

    public void setApply_seq(String apply_seq) {
        this.apply_seq = apply_seq;
    }

    public BigDecimal getInpret_val() {
        return inpret_val;
    }

    public void setInpret_val(BigDecimal inpret_val) {
        this.inpret_val = inpret_val;
    }

    public String getInpret_val_risk_lvl() {
        return inpret_val_risk_lvl;
    }

    public void setInpret_val_risk_lvl(String inpret_val_risk_lvl) {
        this.inpret_val_risk_lvl = inpret_val_risk_lvl;
    }

    public BigDecimal getApply_score() {
        return apply_score;
    }

    public void setApply_score(BigDecimal apply_score) {
        this.apply_score = apply_score;
    }

    public String getApply_score_risk_lvl() {
        return apply_score_risk_lvl;
    }

    public void setApply_score_risk_lvl(String apply_score_risk_lvl) {
        this.apply_score_risk_lvl = apply_score_risk_lvl;
    }

    public BigDecimal getLmt_advice() {
        return lmt_advice;
    }

    public void setLmt_advice(BigDecimal lmt_advice) {
        this.lmt_advice = lmt_advice;
    }

    public BigDecimal getPrice_advice() {
        return price_advice;
    }

    public void setPrice_advice(BigDecimal price_advice) {
        this.price_advice = price_advice;
    }

    public String getRule_risk_lvl() {
        return rule_risk_lvl;
    }

    public void setRule_risk_lvl(String rule_risk_lvl) {
        this.rule_risk_lvl = rule_risk_lvl;
    }

    public String getFund_deposit_base() {
        return fund_deposit_base;
    }

    public void setFund_deposit_base(String fund_deposit_base) {
        this.fund_deposit_base = fund_deposit_base;
    }

    public String getAum() {
        return aum;
    }

    public void setAum(String aum) {
        this.aum = aum;
    }

    public String getDay_rate() {
        return day_rate;
    }

    public void setDay_rate(String day_rate) {
        this.day_rate = day_rate;
    }

    public String getLimit_flag() {
        return limit_flag;
    }

    public void setLimit_flag(String limit_flag) {
        this.limit_flag = limit_flag;
    }

    public String getComplex_risk_lvl() {
        return complex_risk_lvl;
    }

    public void setComplex_risk_lvl(String complex_risk_lvl) {
        this.complex_risk_lvl = complex_risk_lvl;
    }

    public String getCust_lvl() {
        return cust_lvl;
    }

    public void setCust_lvl(String cust_lvl) {
        this.cust_lvl = cust_lvl;
    }

    @Override
    public String toString() {
        return "Lsnp02RespDto{" +
                "businum='" + businum + '\'' +
                "trstatus='" + trstatus + '\'' +
                "txsts='" + txsts + '\'' +
                "sxf='" + sxf + '\'' +
                "apply_seq='" + apply_seq + '\'' +
                "inpret_val='" + inpret_val + '\'' +
                "inpret_val_risk_lvl='" + inpret_val_risk_lvl + '\'' +
                "apply_score='" + apply_score + '\'' +
                "apply_score_risk_lvl='" + apply_score_risk_lvl + '\'' +
                "lmt_advice='" + lmt_advice + '\'' +
                "price_advice='" + price_advice + '\'' +
                "rule_risk_lvl='" + rule_risk_lvl + '\'' +
                "fund_deposit_base='" + fund_deposit_base + '\'' +
                "aum='" + aum + '\'' +
                "day_rate='" + day_rate + '\'' +
                "limit_flag='" + limit_flag + '\'' +
                "complex_risk_lvl='" + complex_risk_lvl + '\'' +
                "cust_lvl='" + cust_lvl + '\'' +
                '}';
    }
}  
