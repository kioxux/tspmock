package cn.com.yusys.yusp.dto.server.biz.xdsx0018.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：房抵e点贷无还本续贷审核
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0018RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdsx0018RespDto{" +
                "data=" + data +
                '}';
    }
}
