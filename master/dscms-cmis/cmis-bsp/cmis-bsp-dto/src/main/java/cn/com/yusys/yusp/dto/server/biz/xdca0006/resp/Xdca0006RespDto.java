package cn.com.yusys.yusp.dto.server.biz.xdca0006.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：信用卡审批结果核准接口
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdca0006RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@JsonProperty(value = "data")
	private cn.com.yusys.yusp.dto.server.biz.xdca0006.resp.Data data;//data



	@Override
	public String toString() {
		return "Xdca0006RespDto{" +
				"data=" + data +
				'}';
	}

}
