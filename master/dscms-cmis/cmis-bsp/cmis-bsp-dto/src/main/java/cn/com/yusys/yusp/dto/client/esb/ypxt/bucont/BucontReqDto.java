package cn.com.yusys.yusp.dto.client.esb.ypxt.bucont;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求DTO：业务与担保合同关系接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class BucontReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "bucontListInfo")
    private List<BucontListInfo> bucontListInfo;

    public List<BucontListInfo> getBucontListInfo() {
        return bucontListInfo;
    }

    public void setBucontListInfo(List<BucontListInfo> bucontListInfo) {
        this.bucontListInfo = bucontListInfo;
    }

    @Override
    public String toString() {
        return "BucontReqDto{" +
                "bucontListInfo=" + bucontListInfo +
                '}';
    }
}
