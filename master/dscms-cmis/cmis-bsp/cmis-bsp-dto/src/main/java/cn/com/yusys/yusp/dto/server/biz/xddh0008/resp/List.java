package cn.com.yusys.yusp.dto.server.biz.xddh0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：还款计划列表查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dateno")
    private Integer dateno;//期号
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "billDate")
    private String billDate;//账单日
    @JsonProperty(value = "instmAmt")
    private BigDecimal instmAmt;//期供金额
    @JsonProperty(value = "cap")
    private BigDecimal cap;//本金
    @JsonProperty(value = "interest")
    private BigDecimal interest;//利息
    @JsonProperty(value = "odint")
    private BigDecimal odint;//罚息
    @JsonProperty(value = "ci")
    private BigDecimal ci;//复利
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//剩余本金
    @JsonProperty(value = "hasbcCap")
    private BigDecimal hasbcCap;//已还本金
    @JsonProperty(value = "hasbcInt")
    private BigDecimal hasbcInt;//已还利息
    @JsonProperty(value = "hasbcOdint")
    private BigDecimal hasbcOdint;//已还罚息
    @JsonProperty(value = "hasbcCi")
    private BigDecimal hasbcCi;//已还复利
    @JsonProperty(value = "isSettlFlag")
    private String isSettlFlag;//结清标记


    public Integer getDateno() {
        return dateno;
    }

    public void setDateno(Integer dateno) {
        this.dateno = dateno;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public BigDecimal getInstmAmt() {
        return instmAmt;
    }

    public void setInstmAmt(BigDecimal instmAmt) {
        this.instmAmt = instmAmt;
    }

    public BigDecimal getCap() {
        return cap;
    }

    public void setCap(BigDecimal cap) {
        this.cap = cap;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public BigDecimal getOdint() {
        return odint;
    }

    public void setOdint(BigDecimal odint) {
        this.odint = odint;
    }

    public BigDecimal getCi() {
        return ci;
    }

    public void setCi(BigDecimal ci) {
        this.ci = ci;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getHasbcCap() {
        return hasbcCap;
    }

    public void setHasbcCap(BigDecimal hasbcCap) {
        this.hasbcCap = hasbcCap;
    }

    public BigDecimal getHasbcInt() {
        return hasbcInt;
    }

    public void setHasbcInt(BigDecimal hasbcInt) {
        this.hasbcInt = hasbcInt;
    }

    public BigDecimal getHasbcOdint() {
        return hasbcOdint;
    }

    public void setHasbcOdint(BigDecimal hasbcOdint) {
        this.hasbcOdint = hasbcOdint;
    }

    public BigDecimal getHasbcCi() {
        return hasbcCi;
    }

    public void setHasbcCi(BigDecimal hasbcCi) {
        this.hasbcCi = hasbcCi;
    }

    public String getIsSettlFlag() {
        return isSettlFlag;
    }

    public void setIsSettlFlag(String isSettlFlag) {
        this.isSettlFlag = isSettlFlag;
    }

    @Override
    public String toString() {
        return "List{" +
                "dateno='" + dateno + '\'' +
                "endDate='" + endDate + '\'' +
                "billDate='" + billDate + '\'' +
                "instmAmt='" + instmAmt + '\'' +
                "cap='" + cap + '\'' +
                "interest='" + interest + '\'' +
                "odint='" + odint + '\'' +
                "ci='" + ci + '\'' +
                "loanBalance='" + loanBalance + '\'' +
                "hasbcCap='" + hasbcCap + '\'' +
                "hasbcInt='" + hasbcInt + '\'' +
                "hasbcOdint='" + hasbcOdint + '\'' +
                "hasbcCi='" + hasbcCi + '\'' +
                "isSettlFlag='" + isSettlFlag + '\'' +
                '}';
    }
}  
