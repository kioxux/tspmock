package cn.com.yusys.yusp.dto.server.psp.xddh0017.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：贷后任务完成接收接口
 *
 * @author wrw
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0017RespDto extends TradeServerRespDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;//data

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    @Override
    public String toString() {
        return "Xddh0017RespDto{" +
                "data=" + data +
                '}';
    }
}
