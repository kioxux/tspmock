package cn.com.yusys.yusp.dto.client.esb.doc.doc003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：出库
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Doc003ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "rwtype")
    private String rwtype;//操作类型: [2：已出库，26：归还在途]
    @JsonProperty(value = "rqtem1")
    private String rqtem1;//请求备用字段1
    @JsonProperty(value = "rqtem2")
    private String rqtem2;//请求备用字段2
    @JsonProperty(value = "rqtem3")
    private BigDecimal rqtem3;//请求备用字段3
    @JsonProperty(value = "rqtem4")
    private String rqtem4;//请求备用字段4

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRwtype() {
        return rwtype;
    }

    public void setRwtype(String rwtype) {
        this.rwtype = rwtype;
    }

    public String getRqtem1() {
        return rqtem1;
    }

    public void setRqtem1(String rqtem1) {
        this.rqtem1 = rqtem1;
    }

    public String getRqtem2() {
        return rqtem2;
    }

    public void setRqtem2(String rqtem2) {
        this.rqtem2 = rqtem2;
    }

    public BigDecimal getRqtem3() {
        return rqtem3;
    }

    public void setRqtem3(BigDecimal rqtem3) {
        this.rqtem3 = rqtem3;
    }

    public String getRqtem4() {
        return rqtem4;
    }

    public void setRqtem4(String rqtem4) {
        this.rqtem4 = rqtem4;
    }

    @Override
    public String toString() {
        return "Doc003ReqDto{" +
                "serno='" + serno + '\'' +
                "rwtype='" + rwtype + '\'' +
                "rqtem1='" + rqtem1 + '\'' +
                "rqtem2='" + rqtem2 + '\'' +
                "rqtem3='" + rqtem3 + '\'' +
                "rqtem4='" + rqtem4 + '\'' +
                '}';
    }
}  
