package cn.com.yusys.yusp.dto.client.esb.pjxt.fkpj35.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：承兑签发审批结果综合服务接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fkpj35ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sBatchNo")
    private String sBatchNo;// 出入池批次号
    @JsonProperty(value = "billno")
    private String billno;// 票据编号
    @JsonProperty(value = "cusid")
    private String cusid;// 客户编号

    public String getsBatchNo() {
        return sBatchNo;
    }

    public void setsBatchNo(String sBatchNo) {
        this.sBatchNo = sBatchNo;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getCusid() {
        return cusid;
    }

    public void setCusid(String cusid) {
        this.cusid = cusid;
    }

    @Override
    public String toString() {
        return "Fkpj35ReqDto{" +
                "sBatchNo='" + sBatchNo + '\'' +
                ", billno='" + billno + '\'' +
                ", cusid='" + cusid + '\'' +
                '}';
    }
}
