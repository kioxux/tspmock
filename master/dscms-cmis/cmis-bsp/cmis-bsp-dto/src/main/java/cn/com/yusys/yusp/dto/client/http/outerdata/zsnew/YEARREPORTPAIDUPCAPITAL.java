package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	年报-企业实缴出资信息
@JsonPropertyOrder(alphabetic = true)
public class YEARREPORTPAIDUPCAPITAL implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ANCHEID")
    private String ANCHEID;//	年报ID
    @JsonProperty(value = "CONDATE")
    private String CONDATE;//	实缴出资日期
    @JsonProperty(value = "CONFORM")
    private String CONFORM;//	出资方式
    @JsonProperty(value = "INV")
    private String INV;//	股东/发起人
    @JsonProperty(value = "LIACCONAM")
    private String LIACCONAM;//	累计实缴额（万元）

    @JsonIgnore
    public String getANCHEID() {
        return ANCHEID;
    }

    @JsonIgnore
    public void setANCHEID(String ANCHEID) {
        this.ANCHEID = ANCHEID;
    }

    @JsonIgnore
    public String getCONDATE() {
        return CONDATE;
    }

    @JsonIgnore
    public void setCONDATE(String CONDATE) {
        this.CONDATE = CONDATE;
    }

    @JsonIgnore
    public String getCONFORM() {
        return CONFORM;
    }

    @JsonIgnore
    public void setCONFORM(String CONFORM) {
        this.CONFORM = CONFORM;
    }

    @JsonIgnore
    public String getINV() {
        return INV;
    }

    @JsonIgnore
    public void setINV(String INV) {
        this.INV = INV;
    }

    @JsonIgnore
    public String getLIACCONAM() {
        return LIACCONAM;
    }

    @JsonIgnore
    public void setLIACCONAM(String LIACCONAM) {
        this.LIACCONAM = LIACCONAM;
    }

    @Override
    public String toString() {
        return "YEARREPORTPAIDUPCAPITAL{" +
                "ANCHEID='" + ANCHEID + '\'' +
                ", CONDATE='" + CONDATE + '\'' +
                ", CONFORM='" + CONFORM + '\'' +
                ", INV='" + INV + '\'' +
                ", LIACCONAM='" + LIACCONAM + '\'' +
                '}';
    }
}
