package cn.com.yusys.yusp.dto.server.cfg.xdsx0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：客户准入级别同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "mapsetList")
    private java.util.List<MapsetList> mapsetList;

    public List<MapsetList> getMapsetList() {
        return mapsetList;
    }

    public void setMapsetList(List<MapsetList> mapsetList) {
        this.mapsetList = mapsetList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "mapsetList=" + mapsetList +
                '}';
    }
}