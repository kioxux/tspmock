package cn.com.yusys.yusp.dto.server.biz.xdtz0025.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/21 15:34:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 15:34
 * @since 2021/5/21 15:34
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "loanAmtTotal")
    private BigDecimal loanAmtTotal;//贷款余额合计
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款开始日
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款结束日

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public BigDecimal getLoanAmtTotal() {
        return loanAmtTotal;
    }

    public void setLoanAmtTotal(BigDecimal loanAmtTotal) {
        this.loanAmtTotal = loanAmtTotal;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    @Override
    public String toString() {
        return "List{" +
                "cusId='" + cusId + '\'' +
                ", loanAmtTotal=" + loanAmtTotal +
                ", loanStartDate='" + loanStartDate + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                '}';
    }
}
