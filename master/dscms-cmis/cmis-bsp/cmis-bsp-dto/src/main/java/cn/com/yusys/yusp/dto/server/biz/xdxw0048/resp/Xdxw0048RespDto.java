package cn.com.yusys.yusp.dto.server.biz.xdxw0048.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：增享贷2.0风控模型B生成批复
 * @author xll
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0048RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdxw0048RespDto{" +
				"data=" + data +
				'}';
	}
}  
