package cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：分期审核
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D13162RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cardno")
    private String cardno;//卡号
    @JsonProperty(value = "rgstid")
    private String rgstid;//分期申请号
    @JsonProperty(value = "loanit")
    private String loanit;//分期总本金
    @JsonProperty(value = "lnrgst")
    private String lnrgst;//分期注册状态

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getRgstid() {
        return rgstid;
    }

    public void setRgstid(String rgstid) {
        this.rgstid = rgstid;
    }

    public String getLoanit() {
        return loanit;
    }

    public void setLoanit(String loanit) {
        this.loanit = loanit;
    }

    public String getLnrgst() {
        return lnrgst;
    }

    public void setLnrgst(String lnrgst) {
        this.lnrgst = lnrgst;
    }

    @Override
    public String toString() {
        return "D13162RespDto{" +
                "cardno='" + cardno + '\'' +
                "rgstid='" + rgstid + '\'' +
                "loanit='" + loanit + '\'' +
                "lnrgst='" + lnrgst + '\'' +
                '}';
    }
}  
