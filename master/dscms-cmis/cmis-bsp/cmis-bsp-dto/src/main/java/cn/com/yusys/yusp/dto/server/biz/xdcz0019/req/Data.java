package cn.com.yusys.yusp.dto.server.biz.xdcz0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：小贷额度支用申请书生成pdf
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//放款金额
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//借款起始日期
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//借款到期日期
    @JsonProperty(value = "loanType")
    private String loanType;//借款种类
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//借款用途
    @JsonProperty(value = "rateExeMode")
    private String rateExeMode;//利率执行方式
    @JsonProperty(value = "lprInterzone")
    private String lprInterzone;//lpr区间
    @JsonProperty(value = "lprBasePoint")
    private BigDecimal lprBasePoint;//lpr基点
    @JsonProperty(value = "yearRate")
    private BigDecimal yearRate;//年利率
    @JsonProperty(value = "floatRateAdjMode")
    private String floatRateAdjMode;//浮动利率调整方式
    @JsonProperty(value = "drawIntervalDate")
    private String drawIntervalDate;//提款间隔日
    @JsonProperty(value = "eiMode")
    private String eiMode;//结息方式
    @JsonProperty(value = "acctNo")
    private String acctNo;//账号
    @JsonProperty(value = "acctsvcr")
    private String acctsvcr;//开户行
    @JsonProperty(value = "payType")
    private String payType;//支付方式
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "applyDate")
    private String applyDate;//申请日期
    @JsonProperty(value = "isdzqy")
    private String isdzqy;//是否电子签约:1-是 0-否

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getRateExeMode() {
        return rateExeMode;
    }

    public void setRateExeMode(String rateExeMode) {
        this.rateExeMode = rateExeMode;
    }

    public String getLprInterzone() {
        return lprInterzone;
    }

    public void setLprInterzone(String lprInterzone) {
        this.lprInterzone = lprInterzone;
    }

    public BigDecimal getLprBasePoint() {
        return lprBasePoint;
    }

    public void setLprBasePoint(BigDecimal lprBasePoint) {
        this.lprBasePoint = lprBasePoint;
    }

    public BigDecimal getYearRate() {
        return yearRate;
    }

    public void setYearRate(BigDecimal yearRate) {
        this.yearRate = yearRate;
    }

    public String getFloatRateAdjMode() {
        return floatRateAdjMode;
    }

    public void setFloatRateAdjMode(String floatRateAdjMode) {
        this.floatRateAdjMode = floatRateAdjMode;
    }

    public String getDrawIntervalDate() {
        return drawIntervalDate;
    }

    public void setDrawIntervalDate(String drawIntervalDate) {
        this.drawIntervalDate = drawIntervalDate;
    }

    public String getEiMode() {
        return eiMode;
    }

    public void setEiMode(String eiMode) {
        this.eiMode = eiMode;
    }

    public String getAcctNo() {
        return acctNo;
    }

    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }

    public String getAcctsvcr() {
        return acctsvcr;
    }

    public void setAcctsvcr(String acctsvcr) {
        this.acctsvcr = acctsvcr;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getIsdzqy() {
        return isdzqy;
    }

    public void setIsdzqy(String isdzqy) {
        this.isdzqy = isdzqy;
    }

    @Override
    public String toString() {
        return "Xdcz0019ReqDto{" +
                "cusName='" + cusName + '\'' +
                "contNo='" + contNo + '\'' +
                "cnContNo='" + cnContNo + '\'' +
                "loanAmt='" + loanAmt + '\'' +
                "curType='" + curType + '\'' +
                "loanStartDate='" + loanStartDate + '\'' +
                "loanEndDate='" + loanEndDate + '\'' +
                "loanType='" + loanType + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "rateExeMode='" + rateExeMode + '\'' +
                "lprInterzone='" + lprInterzone + '\'' +
                "lprBasePoint  ='" + lprBasePoint + '\'' +
                "yearRate='" + yearRate + '\'' +
                "floatRateAdjMode='" + floatRateAdjMode + '\'' +
                "drawIntervalDate='" + drawIntervalDate + '\'' +
                "eiMode='" + eiMode + '\'' +
                "acctNo='" + acctNo + '\'' +
                "acctsvcr='" + acctsvcr + '\'' +
                "payType='" + payType + '\'' +
                "repayType='" + repayType + '\'' +
                "applyDate='" + applyDate + '\'' +
                "isdzqy='" + isdzqy + '\'' +
                '}';
    }
}
