package cn.com.yusys.yusp.dto.client.esb.ecif.g11003;

import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.RelArrayInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Service：客户集团信息维护 (new)
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class G11003RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value="grouno")
    private String	grouno	;	// VARCHAR2	30	集团编号

    public String getGrouno() {
        return grouno;
    }

    public void setGrouno(String grouno) {
        this.grouno = grouno;
    }

    @Override
    public String toString() {
        return "G11003RespDto{" +
                "grouno='" + grouno + '\'' +
                '}';
    }
}
