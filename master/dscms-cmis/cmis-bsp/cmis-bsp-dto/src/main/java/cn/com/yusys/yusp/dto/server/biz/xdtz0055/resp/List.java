package cn.com.yusys.yusp.dto.server.biz.xdtz0055.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询退回受托信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "toppAcctNo")
    private String toppAcctNo;//交易对手账号
    @JsonProperty(value = "toppName")
    private String toppName;//交易对手名称
    @JsonProperty(value = "oldToppAcctNo")
    private String oldToppAcctNo;//原交易对手账号
    @JsonProperty(value = "oldToppName")
    private String oldToppName;//原交易对手名称
    @JsonProperty(value = "toppAmt")
    private BigDecimal toppAmt;//交易对手金额
    @JsonProperty(value = "oldToppAmt")
    private BigDecimal oldToppAmt;//原交易对手金额
    @JsonProperty(value = "acctsvcrAcctNo")
    private String acctsvcrAcctNo;//开户行号
    @JsonProperty(value = "acctsvcrName")
    private String acctsvcrName;//开户行名称
    @JsonProperty(value = "oldAcctsvcrAcctNo")
    private String oldAcctsvcrAcctNo;//原开户行号
    @JsonProperty(value = "oldAacctsvcrName")
    private String oldAacctsvcrName;//原开户行名称
    @JsonProperty(value = "isOnline")
    private String isOnline;//是否线上
    @JsonProperty(value = "oldIsOnline")
    private String oldIsOnline;//原是否线上

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getToppAcctNo() {
        return toppAcctNo;
    }

    public void setToppAcctNo(String toppAcctNo) {
        this.toppAcctNo = toppAcctNo;
    }

    public String getToppName() {
        return toppName;
    }

    public void setToppName(String toppName) {
        this.toppName = toppName;
    }

    public String getOldToppAcctNo() {
        return oldToppAcctNo;
    }

    public void setOldToppAcctNo(String oldToppAcctNo) {
        this.oldToppAcctNo = oldToppAcctNo;
    }

    public String getOldToppName() {
        return oldToppName;
    }

    public void setOldToppName(String oldToppName) {
        this.oldToppName = oldToppName;
    }

    public BigDecimal getToppAmt() {
        return toppAmt;
    }

    public void setToppAmt(BigDecimal toppAmt) {
        this.toppAmt = toppAmt;
    }

    public BigDecimal getOldToppAmt() {
        return oldToppAmt;
    }

    public void setOldToppAmt(BigDecimal oldToppAmt) {
        this.oldToppAmt = oldToppAmt;
    }

    public String getAcctsvcrAcctNo() {
        return acctsvcrAcctNo;
    }

    public void setAcctsvcrAcctNo(String acctsvcrAcctNo) {
        this.acctsvcrAcctNo = acctsvcrAcctNo;
    }

    public String getAcctsvcrName() {
        return acctsvcrName;
    }

    public void setAcctsvcrName(String acctsvcrName) {
        this.acctsvcrName = acctsvcrName;
    }

    public String getOldAcctsvcrAcctNo() {
        return oldAcctsvcrAcctNo;
    }

    public void setOldAcctsvcrAcctNo(String oldAcctsvcrAcctNo) {
        this.oldAcctsvcrAcctNo = oldAcctsvcrAcctNo;
    }

    public String getOldAacctsvcrName() {
        return oldAacctsvcrName;
    }

    public void setOldAacctsvcrName(String oldAacctsvcrName) {
        this.oldAacctsvcrName = oldAacctsvcrName;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getOldIsOnline() {
        return oldIsOnline;
    }

    public void setOldIsOnline(String oldIsOnline) {
        this.oldIsOnline = oldIsOnline;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", toppAcctNo='" + toppAcctNo + '\'' +
                ", toppName='" + toppName + '\'' +
                ", oldToppAcctNo='" + oldToppAcctNo + '\'' +
                ", oldToppName='" + oldToppName + '\'' +
                ", toppAmt=" + toppAmt +
                ", oldToppAmt=" + oldToppAmt +
                ", acctsvcrAcctNo='" + acctsvcrAcctNo + '\'' +
                ", acctsvcrName='" + acctsvcrName + '\'' +
                ", oldAcctsvcrAcctNo='" + oldAcctsvcrAcctNo + '\'' +
                ", oldAacctsvcrName='" + oldAacctsvcrName + '\'' +
                ", isOnline='" + isOnline + '\'' +
                ", oldIsOnline='" + oldIsOnline + '\'' +
                '}';
    }
}
