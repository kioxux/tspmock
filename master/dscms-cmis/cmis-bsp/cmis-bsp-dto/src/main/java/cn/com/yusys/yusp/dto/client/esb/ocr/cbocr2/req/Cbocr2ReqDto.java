package cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：识别任务的模板匹配结果列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cbocr2ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "taskId")
    private String taskId;//接口交易码区分交易

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "Cbocr2ReqDto{" +
                "taskId='" + taskId + '\'' +
                '}';
    }
}  
