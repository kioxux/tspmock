package cn.com.yusys.yusp.dto.server.biz.xdxw0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/3 21:36
 * @since 2021/6/3 21:36
 */
@JsonPropertyOrder(alphabetic = true)
public class ListCont implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//业务流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "loanType")
    private String loanType;//贷款类型
    @JsonProperty(value = "isMicroDept")
    private String isMicroDept;//是否属于小微部门标志
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "teamType")
    private String teamType;//直营团队类型
    @JsonProperty(value = "managerId")
    private String managerId;//管理客户经理工号
    @JsonProperty(value = "managerName")
    private String managerName;//管户客户经理工号
    @JsonProperty(value = "orgNo")
    private String orgNo;//客户经理所在机构编号
    @JsonProperty(value = "orgName")
    private String orgName;//客户经理所在机构名称

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getIsMicroDept() {
        return isMicroDept;
    }

    public void setIsMicroDept(String isMicroDept) {
        this.isMicroDept = isMicroDept;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    @Override
    public String toString() {
        return "ListCont{" +
                "serno='" + serno + '\'' +
                ", cusId='" + cusId + '\'' +
                ", loanType='" + loanType + '\'' +
                ", isMicroDept='" + isMicroDept + '\'' +
                ", contAmt=" + contAmt +
                ", teamType='" + teamType + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", orgName='" + orgName + '\'' +
                '}';
    }
}
