package cn.com.yusys.yusp.dto.client.esb.core.ln3006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款产品组合查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3006RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "listnm")
    private List<Listnm> listnm;//产品组合查询输出

	public Integer getZongbish() {
		return zongbish;
	}

	public void setZongbish(Integer zongbish) {
		this.zongbish = zongbish;
	}

	public List<Listnm> getListnm() {
		return listnm;
	}

	public void setListnm(List<Listnm> listnm) {
		this.listnm = listnm;
	}

	@Override
	public String toString() {
		return "Ln3006RespDto{" +
				"zongbish=" + zongbish +
				", listnm=" + listnm +
				'}';
	}
}
