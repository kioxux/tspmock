package cn.com.yusys.yusp.dto.client.esb.circp.fb1149.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：无还本续贷借据更新
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1149RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "OLS_TRAN_NO")
    private String OLS_TRAN_NO;
    @JsonProperty(value = "OLS_DATE")
    private String OLS_DATE;

    @JsonIgnore
    public String getOLS_TRAN_NO() {
        return OLS_TRAN_NO;
    }

    @JsonIgnore
    public void setOLS_TRAN_NO(String OLS_TRAN_NO) {
        this.OLS_TRAN_NO = OLS_TRAN_NO;
    }

    @JsonIgnore
    public String getOLS_DATE() {
        return OLS_DATE;
    }

    @JsonIgnore
    public void setOLS_DATE(String OLS_DATE) {
        this.OLS_DATE = OLS_DATE;
    }

    @Override
    public String toString() {
        return "Fb1149RespDto{" +
                "OLS_TRAN_NO='" + OLS_TRAN_NO + '\'' +
                ", OLS_DATE='" + OLS_DATE + '\'' +
                '}';
    }
}
