package cn.com.yusys.yusp.dto.server.biz.xddb0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据押品编号查询核心及信贷系统有无押品数据
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "book_serno")
    private String book_serno;//核心押品编号

    public String getBook_serno() {
        return book_serno;
    }

    public void setBook_serno(String book_serno) {
        this.book_serno = book_serno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "book_serno='" + book_serno + '\'' +
                '}';
    }
}
