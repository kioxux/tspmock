package cn.com.yusys.yusp.dto.server.biz.xdtz0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/21 14:51:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 14:51
 * @since 2021/5/21 14:51
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bizType")
    private String bizType;//业务类型
    @JsonProperty(value = "oprtype")
    private String oprtype;//操作类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "finaDept")
    private String finaDept;//财务部门
    @JsonProperty(value = "endDate")
    private String endDate;//到期日期
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getOprtype() {
        return oprtype;
    }

    public void setOprtype(String oprtype) {
        this.oprtype = oprtype;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getFinaDept() {
        return finaDept;
    }

    public void setFinaDept(String finaDept) {
        this.finaDept = finaDept;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "bizType='" + bizType + '\'' +
                ", oprtype='" + oprtype + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", finaDept='" + finaDept + '\'' +
                ", endDate='" + endDate + '\'' +
                ", billNo='" + billNo + '\'' +
                ", list=" + list +
                '}';
    }
}
