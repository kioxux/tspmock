package cn.com.yusys.yusp.dto.client.esb.doc.doc005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/23 14:48
 * @since 2021/6/23 14:48
 */
@JsonPropertyOrder(alphabetic = true)
public class IndexData implements Serializable {

    private static final long serialVersionUID = 1L;

    // 调阅流水信息主键唯一
    @JsonProperty(value = "pkBappId")
    private String pkBappId;
    // 调阅单位
    @JsonProperty(value = "bappOrg")
    private String bappOrg;
    // 调阅原因
    @JsonProperty(value = "bappReason")
    private String bappReason;
    // 调阅人
    @JsonProperty(value = "bappReader")
    private String bappReader;
    // 身份证号
    @JsonProperty(value = "bappIdCard")
    private String bappIdCard;
    // 批准人
    @JsonProperty(value = "bappApprove")
    private String bappApprove;
    // 操作机构
    @JsonProperty(value = "bappApproveOrg")
    private String bappApproveOrg;
    // 用户code
    @JsonProperty(value = "userCode")
    private String userCode;
    // 用户机构
    @JsonProperty(value = "userOrg")
    private String userOrg;
    // 调阅证明
    @JsonProperty(value = "bappProve")
    private String bappProve;
    // 授权人
    @JsonProperty(value = "authCode")
    private String authCode;
    // 调阅类型 1 内部调阅 2 外部调阅 3 纸质调阅
    @JsonProperty(value = "bappType")
    private String bappType;
    // 是否打印 0 未打印 1 已打印
    @JsonProperty(value = "print")
    private String print;
    // 公共用户
    @JsonProperty(value = "publicCode")
    private String publicCode;
    // 联系电话
    @JsonProperty(value = "bappPhoneNumber")
    private String bappPhoneNumber;
    // 调阅状态：0-待审批；1-审批中；2-审批通过；3-审批不通过  4-未审批
    @JsonProperty(value = "approveStatus")
    private String approveStatus;
    // 出库状态 ：0-未出库  1-待出库   2-已出库   3-已归还,24：待归还，25:出库在途，26：归还在途，27:退回在途
    @JsonProperty(value = "outRoomStatus")
    private String outRoomStatus;
    // 出库时间
    @JsonProperty(value = "outRoomTime")
    private String outRoomTime;
    // 入库时间
    @JsonProperty(value = "inRoomTime")
    private String inRoomTime;
    // 交易日期
    @JsonProperty(value = "tradeDate")
    private String tradeDate;
    // 交易机构号
    @JsonProperty(value = "tradeOrgCode")
    private String tradeOrgCode;
    // 交易机构号
    @JsonProperty(value = "tradeUserCode")
    private String tradeUserCode;
    // 交易机构名称
    @JsonProperty(value = "tradeOrgCodeName")
    private String tradeOrgCodeName;
    // 出库内容
    @JsonProperty(value = "outRoomContent")
    private String outRoomContent;
    // 档案类型编号
    @JsonProperty(value = "fileTypeNum")
    private String fileTypeNum;
    // 档案位置信息
    @JsonProperty(value = "storePosition")
    private String storePosition;
    // 附件名称
    @JsonProperty(value = "attachName")
    private String attachName;
    // 出库机构
    @JsonProperty(value = "outbranch")
    private String outbranch;
    // 逾期标示 0-已经逾期 1-已经出库但是还未逾期 2-归还途中 3-未出库 4-其他
    @JsonProperty(value = "status")
    private String status;
    // 审批流程ID
    @JsonProperty(value = "caseId")
    private String caseId;
    // 证件类型
    @JsonProperty(value = "bappIdType")
    private String bappIdType;
    // 0-未出库  1-待出库   2-已出库   3-已归还,24：待归还，25:出库在途，26：归还在途
    @JsonProperty(value = "outRoomStatusName")
    private String outRoomStatusName;
    // 调阅记录id
    @JsonProperty(value = "fkBappId")
    private String fkBappId;
    // 档案记录id
    @JsonProperty(value = "fkFileId")
    private String fkFileId;
    // 档案编号
    @JsonProperty(value = "fileNum")
    private String fileNum;
    // 档案名称
    @JsonProperty(value = "accFileName")
    private String accFileName;
    // 创建时间
    @JsonProperty(value = "createTime")
    private String createTime;
    // 出库时间
    @JsonProperty(value = "outtime")
    private String outtime;
    // 出库状态  0:出库登记 1:待出库 2:已出库  3：已归还  4：未出库
    @JsonProperty(value = "accFileStatus")
    private String accFileStatus;
    // 借阅开始日期
    @JsonProperty(value = "bappStartDate")
    private String bappStartDate;
    // 借阅借阅日期
    @JsonProperty(value = "bappEndDate")
    private String bappEndDate;
    // 索引信息(业务编号、合同号等信息):{\"K_BILLNO\":\"016000170816166\"}
    @JsonProperty(value = "indexMap")
    private String indexMap;
    @JsonProperty(value = "bappSeqNo")
    private String bappSeqNo;//调阅流水号

    public String getPkBappId() {
        return pkBappId;
    }

    public void setPkBappId(String pkBappId) {
        this.pkBappId = pkBappId;
    }

    public String getBappOrg() {
        return bappOrg;
    }

    public void setBappOrg(String bappOrg) {
        this.bappOrg = bappOrg;
    }

    public String getBappReason() {
        return bappReason;
    }

    public void setBappReason(String bappReason) {
        this.bappReason = bappReason;
    }

    public String getBappReader() {
        return bappReader;
    }

    public void setBappReader(String bappReader) {
        this.bappReader = bappReader;
    }

    public String getBappIdCard() {
        return bappIdCard;
    }

    public void setBappIdCard(String bappIdCard) {
        this.bappIdCard = bappIdCard;
    }

    public String getBappApprove() {
        return bappApprove;
    }

    public void setBappApprove(String bappApprove) {
        this.bappApprove = bappApprove;
    }

    public String getBappApproveOrg() {
        return bappApproveOrg;
    }

    public void setBappApproveOrg(String bappApproveOrg) {
        this.bappApproveOrg = bappApproveOrg;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserOrg() {
        return userOrg;
    }

    public void setUserOrg(String userOrg) {
        this.userOrg = userOrg;
    }

    public String getBappProve() {
        return bappProve;
    }

    public void setBappProve(String bappProve) {
        this.bappProve = bappProve;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getBappType() {
        return bappType;
    }

    public void setBappType(String bappType) {
        this.bappType = bappType;
    }

    public String getPrint() {
        return print;
    }

    public void setPrint(String print) {
        this.print = print;
    }

    public String getPublicCode() {
        return publicCode;
    }

    public void setPublicCode(String publicCode) {
        this.publicCode = publicCode;
    }

    public String getBappPhoneNumber() {
        return bappPhoneNumber;
    }

    public void setBappPhoneNumber(String bappPhoneNumber) {
        this.bappPhoneNumber = bappPhoneNumber;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getOutRoomStatus() {
        return outRoomStatus;
    }

    public void setOutRoomStatus(String outRoomStatus) {
        this.outRoomStatus = outRoomStatus;
    }

    public String getOutRoomTime() {
        return outRoomTime;
    }

    public void setOutRoomTime(String outRoomTime) {
        this.outRoomTime = outRoomTime;
    }

    public String getInRoomTime() {
        return inRoomTime;
    }

    public void setInRoomTime(String inRoomTime) {
        this.inRoomTime = inRoomTime;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getTradeOrgCode() {
        return tradeOrgCode;
    }

    public void setTradeOrgCode(String tradeOrgCode) {
        this.tradeOrgCode = tradeOrgCode;
    }

    public String getTradeUserCode() {
        return tradeUserCode;
    }

    public void setTradeUserCode(String tradeUserCode) {
        this.tradeUserCode = tradeUserCode;
    }

    public String getTradeOrgCodeName() {
        return tradeOrgCodeName;
    }

    public void setTradeOrgCodeName(String tradeOrgCodeName) {
        this.tradeOrgCodeName = tradeOrgCodeName;
    }

    public String getOutRoomContent() {
        return outRoomContent;
    }

    public void setOutRoomContent(String outRoomContent) {
        this.outRoomContent = outRoomContent;
    }

    public String getFileTypeNum() {
        return fileTypeNum;
    }

    public void setFileTypeNum(String fileTypeNum) {
        this.fileTypeNum = fileTypeNum;
    }

    public String getStorePosition() {
        return storePosition;
    }

    public void setStorePosition(String storePosition) {
        this.storePosition = storePosition;
    }

    public String getAttachName() {
        return attachName;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public String getOutbranch() {
        return outbranch;
    }

    public void setOutbranch(String outbranch) {
        this.outbranch = outbranch;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getBappIdType() {
        return bappIdType;
    }

    public void setBappIdType(String bappIdType) {
        this.bappIdType = bappIdType;
    }

    public String getOutRoomStatusName() {
        return outRoomStatusName;
    }

    public void setOutRoomStatusName(String outRoomStatusName) {
        this.outRoomStatusName = outRoomStatusName;
    }

    public String getFkBappId() {
        return fkBappId;
    }

    public void setFkBappId(String fkBappId) {
        this.fkBappId = fkBappId;
    }

    public String getFkFileId() {
        return fkFileId;
    }

    public void setFkFileId(String fkFileId) {
        this.fkFileId = fkFileId;
    }

    public String getFileNum() {
        return fileNum;
    }

    public void setFileNum(String fileNum) {
        this.fileNum = fileNum;
    }

    public String getAccFileName() {
        return accFileName;
    }

    public void setAccFileName(String accFileName) {
        this.accFileName = accFileName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getOuttime() {
        return outtime;
    }

    public void setOuttime(String outtime) {
        this.outtime = outtime;
    }

    public String getAccFileStatus() {
        return accFileStatus;
    }

    public void setAccFileStatus(String accFileStatus) {
        this.accFileStatus = accFileStatus;
    }

    public String getIndexMap() {
        return indexMap;
    }

    public void setIndexMap(String indexMap) {
        this.indexMap = indexMap;
    }

    public String getBappStartDate() {
        return bappStartDate;
    }

    public void setBappStartDate(String bappStartDate) {
        this.bappStartDate = bappStartDate;
    }

    public String getBappEndDate() {
        return bappEndDate;
    }

    public void setBappEndDate(String bappEndDate) {
        this.bappEndDate = bappEndDate;
    }

    public String getBappSeqNo() {
        return bappSeqNo;
    }

    public void setBappSeqNo(String bappSeqNo) {
        this.bappSeqNo = bappSeqNo;
    }

    @Override
    public String toString() {
        return "IndexData{" +
                "pkBappId='" + pkBappId + '\'' +
                ", bappOrg='" + bappOrg + '\'' +
                ", bappReason='" + bappReason + '\'' +
                ", bappReader='" + bappReader + '\'' +
                ", bappIdCard='" + bappIdCard + '\'' +
                ", bappApprove='" + bappApprove + '\'' +
                ", bappApproveOrg='" + bappApproveOrg + '\'' +
                ", userCode='" + userCode + '\'' +
                ", userOrg='" + userOrg + '\'' +
                ", bappProve='" + bappProve + '\'' +
                ", authCode='" + authCode + '\'' +
                ", bappType='" + bappType + '\'' +
                ", print='" + print + '\'' +
                ", publicCode='" + publicCode + '\'' +
                ", bappPhoneNumber='" + bappPhoneNumber + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", outRoomStatus='" + outRoomStatus + '\'' +
                ", outRoomTime='" + outRoomTime + '\'' +
                ", inRoomTime='" + inRoomTime + '\'' +
                ", tradeDate='" + tradeDate + '\'' +
                ", tradeOrgCode='" + tradeOrgCode + '\'' +
                ", tradeUserCode='" + tradeUserCode + '\'' +
                ", tradeOrgCodeName='" + tradeOrgCodeName + '\'' +
                ", outRoomContent='" + outRoomContent + '\'' +
                ", fileTypeNum='" + fileTypeNum + '\'' +
                ", storePosition='" + storePosition + '\'' +
                ", attachName='" + attachName + '\'' +
                ", outbranch='" + outbranch + '\'' +
                ", status='" + status + '\'' +
                ", caseId='" + caseId + '\'' +
                ", bappIdType='" + bappIdType + '\'' +
                ", outRoomStatusName='" + outRoomStatusName + '\'' +
                ", fkBappId='" + fkBappId + '\'' +
                ", fkFileId='" + fkFileId + '\'' +
                ", fileNum='" + fileNum + '\'' +
                ", accFileName='" + accFileName + '\'' +
                ", createTime='" + createTime + '\'' +
                ", outtime='" + outtime + '\'' +
                ", accFileStatus='" + accFileStatus + '\'' +
                ", bappStartDate='" + bappStartDate + '\'' +
                ", bappEndDate='" + bappEndDate + '\'' +
                ", indexMap='" + indexMap + '\'' +
                ", bappSeqNo='" + bappSeqNo + '\'' +
                '}';
    }
}
