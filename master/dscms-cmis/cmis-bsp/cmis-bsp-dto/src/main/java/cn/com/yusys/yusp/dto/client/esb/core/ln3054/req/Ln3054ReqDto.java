package cn.com.yusys.yusp.dto.client.esb.core.ln3054.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：调整贷款还款方式
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3054ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限(月)
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "leijinzh")
    private BigDecimal leijinzh;//累进值
    @JsonProperty(value = "leijqjsh")
    private Integer leijqjsh;//累进区间期数
    @JsonProperty(value = "duophkbz")
    private String duophkbz;//多频率还款标志
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "huanbzhq")
    private String huanbzhq;//还本周期
    @JsonProperty(value = "yuqhkzhq")
    private String yuqhkzhq;//逾期还款周期
    @JsonProperty(value = "hkqixian")
    private String hkqixian;//还款期限(月)
    @JsonProperty(value = "baoliuje")
    private BigDecimal baoliuje;//保留金额
    @JsonProperty(value = "dhkzhhbz")
    private String dhkzhhbz;//多还款账户标志
    @JsonProperty(value = "dbdkkksx")
    private Integer dbdkkksx;//多笔贷款扣款顺序
    @JsonProperty(value = "lstdkhkzh")
    private java.util.List<Lstdkhkzh> lstdkhkzh;


    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public String getDuophkbz() {
        return duophkbz;
    }

    public void setDuophkbz(String duophkbz) {
        this.duophkbz = duophkbz;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public Integer getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Integer dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public List<Lstdkhkzh> getLstdkhkzh() {
        return lstdkhkzh;
    }

    public void setLstdkhkzh(List<Lstdkhkzh> lstdkhkzh) {
        this.lstdkhkzh = lstdkhkzh;
    }

    @Override
    public String toString() {
        return "Ln3054ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "dkqixian='" + dkqixian + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "hetongje='" + hetongje + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "dechligz='" + dechligz + '\'' +
                "leijinzh='" + leijinzh + '\'' +
                "leijqjsh='" + leijqjsh + '\'' +
                "duophkbz='" + duophkbz + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "huanbzhq='" + huanbzhq + '\'' +
                "yuqhkzhq='" + yuqhkzhq + '\'' +
                "hkqixian='" + hkqixian + '\'' +
                "baoliuje='" + baoliuje + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "dbdkkksx='" + dbdkkksx + '\'' +
                '}';
    }
}  
