package cn.com.yusys.yusp.dto.server.cfg.xdxt0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/25 10:07:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/25 10:07
 * @since 2021/5/25 10:07
 */
@JsonPropertyOrder(alphabetic = true)
public class OptList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pk1")
    private String pk1;//主键
    @JsonProperty(value = "cnname")
    private String cnname;//字段名
    @JsonProperty(value = "enname")
    private String enname;//中文名
    @JsonProperty(value = "flag")
    private String flag;//标识
    @JsonProperty(value = "levels")
    private String levels;//等级
    @JsonProperty(value = "abvenname")
    private String abvenname;//上级字段名
    @JsonProperty(value = "locate")
    private String locate;//位置
    @JsonProperty(value = "memo")
    private String memo;//说明
    @JsonProperty(value = "opttype")
    private String opttype;//字典项类型
    @JsonProperty(value = "orderid")
    private String orderid;//排序编号

    public String getPk1() {
        return pk1;
    }

    public void setPk1(String pk1) {
        this.pk1 = pk1;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getLevels() {
        return levels;
    }

    public void setLevels(String levels) {
        this.levels = levels;
    }

    public String getAbvenname() {
        return abvenname;
    }

    public void setAbvenname(String abvenname) {
        this.abvenname = abvenname;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getOpttype() {
        return opttype;
    }

    public void setOpttype(String opttype) {
        this.opttype = opttype;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    @Override
    public String toString() {
        return "Xdxt0013RespDto{" +
                "pk1='" + pk1 + '\'' +
                "cnname='" + cnname + '\'' +
                "enname='" + enname + '\'' +
                "flag='" + flag + '\'' +
                "levels='" + levels + '\'' +
                "abvenname='" + abvenname + '\'' +
                "locate='" + locate + '\'' +
                "memo='" + memo + '\'' +
                "opttype='" + opttype + '\'' +
                "orderid='" + orderid + '\'' +
                '}';
    }
}
