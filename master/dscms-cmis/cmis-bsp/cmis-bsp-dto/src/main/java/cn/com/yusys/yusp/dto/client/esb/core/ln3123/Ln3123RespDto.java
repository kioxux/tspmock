package cn.com.yusys.yusp.dto.client.esb.core.ln3123;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应DTO：受托支付查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/16上午10:34:31
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3123RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lstdkstzf")
    private List<Lstdkstzf> lstdkstzf;//贷款受托支付


    public List<Lstdkstzf> getLstdkstzf() {
        return lstdkstzf;
    }

    public void setLstdkstzf(List<Lstdkstzf> lstdkstzf) {
        this.lstdkstzf = lstdkstzf;
    }

    @Override
    public String toString() {
        return "Ln3123RespDto{" +
                "lstdkstzf=" + lstdkstzf +
                '}';
    }
}
