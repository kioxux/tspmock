package cn.com.yusys.yusp.dto.client.esb.irs.irs19.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：专业贷款基本信息同步
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs19ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sxserialno")
    private String sxserialno;//授信申请流水号
    @JsonProperty(value = "custid")
    private String custid;//客户编号
    @JsonProperty(value = "custname")
    private String custname;//客户名称
    @JsonProperty(value = "certtype")
    private String certtype;//证件类型
    @JsonProperty(value = "certid")
    private String certid;//证件号码
    @JsonProperty(value = "cantoncode")
    private String cantoncode;//注册地行政区划代码
    @JsonProperty(value = "creaditsum")
    private BigDecimal creaditsum;//授信申请金额
    @JsonProperty(value = "creadittype")
    private String creadittype;//专业贷款类型
    @JsonProperty(value = "sxbegdate")
    private String sxbegdate;//授信起始日
    @JsonProperty(value = "sxenddate")
    private String sxenddate;//授信到期日
    @JsonProperty(value = "userid1")
    private String userid1;//管户人编号
    @JsonProperty(value = "username")
    private String username;//管户人名称
    @JsonProperty(value = "orgid")
    private String orgid;//管户机构编号
    @JsonProperty(value = "orgname")
    private String orgname;//管户机构名称

    public String getSxserialno() {
        return sxserialno;
    }

    public void setSxserialno(String sxserialno) {
        this.sxserialno = sxserialno;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getCerttype() {
        return certtype;
    }

    public void setCerttype(String certtype) {
        this.certtype = certtype;
    }

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getCantoncode() {
        return cantoncode;
    }

    public void setCantoncode(String cantoncode) {
        this.cantoncode = cantoncode;
    }

    public BigDecimal getCreaditsum() {
        return creaditsum;
    }

    public void setCreaditsum(BigDecimal creaditsum) {
        this.creaditsum = creaditsum;
    }

    public String getCreadittype() {
        return creadittype;
    }

    public void setCreadittype(String creadittype) {
        this.creadittype = creadittype;
    }

    public String getSxbegdate() {
        return sxbegdate;
    }

    public void setSxbegdate(String sxbegdate) {
        this.sxbegdate = sxbegdate;
    }

    public String getSxenddate() {
        return sxenddate;
    }

    public void setSxenddate(String sxenddate) {
        this.sxenddate = sxenddate;
    }

    public String getUserid1() {
        return userid1;
    }

    public void setUserid1(String userid1) {
        this.userid1 = userid1;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    @Override
    public String toString() {
        return "Irs19ReqDto{" +
                "sxserialno='" + sxserialno + '\'' +
                "custid='" + custid + '\'' +
                "custname='" + custname + '\'' +
                "certtype='" + certtype + '\'' +
                "certid='" + certid + '\'' +
                "cantoncode='" + cantoncode + '\'' +
                "creaditsum='" + creaditsum + '\'' +
                "creadittype='" + creadittype + '\'' +
                "sxbegdate='" + sxbegdate + '\'' +
                "sxenddate='" + sxenddate + '\'' +
                "userid1='" + userid1 + '\'' +
                "username='" + username + '\'' +
                "orgid='" + orgid + '\'' +
                "orgname='" + orgname + '\'' +
                '}';
    }
}  
