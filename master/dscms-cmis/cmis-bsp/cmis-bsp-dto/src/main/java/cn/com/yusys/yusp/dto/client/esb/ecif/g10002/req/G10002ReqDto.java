package cn.com.yusys.yusp.dto.client.esb.ecif.g10002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户群组信息维护
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class G10002ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "grouno")
    private String grouno;//群组编号
    @JsonProperty(value = "grouna")
    private String grouna;//群组名称
    @JsonProperty(value = "grouds")
    private String grouds;//群组描述
    @JsonProperty(value = "acctbr")
    private String acctbr;//管户机构
    @JsonProperty(value = "acmang")
    private String acmang;//管户客户经理
    @JsonProperty(value = "mxmang")
    private String mxmang;//主办客户经理
    @JsonProperty(value = "inptdt")
    private String inptdt;//主办日期
    @JsonProperty(value = "inptbr")
    private String inptbr;//主办机构
    @JsonProperty(value = "jinmlv")
    private String jinmlv;//紧密程度
    @JsonProperty(value = "remark")
    private String remark;//备注
    @JsonProperty(value = "gpcsst")
    private String gpcsst;//群组客户状态
    @JsonProperty(value = "List")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.ecif.g10002.req.GroupArrayMem> List;

    public String getGrouno() {
        return grouno;
    }

    public void setGrouno(String grouno) {
        this.grouno = grouno;
    }

    public String getGrouna() {
        return grouna;
    }

    public void setGrouna(String grouna) {
        this.grouna = grouna;
    }

    public String getGrouds() {
        return grouds;
    }

    public void setGrouds(String grouds) {
        this.grouds = grouds;
    }

    public String getAcctbr() {
        return acctbr;
    }

    public void setAcctbr(String acctbr) {
        this.acctbr = acctbr;
    }

    public String getAcmang() {
        return acmang;
    }

    public void setAcmang(String acmang) {
        this.acmang = acmang;
    }

    public String getMxmang() {
        return mxmang;
    }

    public void setMxmang(String mxmang) {
        this.mxmang = mxmang;
    }

    public String getInptdt() {
        return inptdt;
    }

    public void setInptdt(String inptdt) {
        this.inptdt = inptdt;
    }

    public String getInptbr() {
        return inptbr;
    }

    public void setInptbr(String inptbr) {
        this.inptbr = inptbr;
    }

    public String getJinmlv() {
        return jinmlv;
    }

    public void setJinmlv(String jinmlv) {
        this.jinmlv = jinmlv;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getGpcsst() {
        return gpcsst;
    }

    public void setGpcsst(String gpcsst) {
        this.gpcsst = gpcsst;
    }

    public java.util.List<GroupArrayMem> getList() {
        return List;
    }

    public void setList(java.util.List<GroupArrayMem> list) {
        List = list;
    }

    @Override
    public String toString() {
        return "G10002ReqDto{" +
                "grouno='" + grouno + '\'' +
                ", grouna='" + grouna + '\'' +
                ", grouds='" + grouds + '\'' +
                ", acctbr='" + acctbr + '\'' +
                ", acmang='" + acmang + '\'' +
                ", mxmang='" + mxmang + '\'' +
                ", inptdt='" + inptdt + '\'' +
                ", inptbr='" + inptbr + '\'' +
                ", jinmlv='" + jinmlv + '\'' +
                ", remark='" + remark + '\'' +
                ", gpcsst='" + gpcsst + '\'' +
                ", List=" + List +
                '}';
    }
}
