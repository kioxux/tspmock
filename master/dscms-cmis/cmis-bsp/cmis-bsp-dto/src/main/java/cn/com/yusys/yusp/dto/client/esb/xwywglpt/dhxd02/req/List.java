package cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷系统请求小V平台贷后预警回传定期检查任务结果
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 16:12
 * @since 2021/5/28 16:12
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "id")
    private String id;//主键id
    @JsonProperty(value = "warnsignal")
    private String warnsignal;//预警指标信号
    @JsonProperty(value = "isrelievesignal")
    private String isrelievesignal;//是否解除信号
    @JsonProperty(value = "receivetime")
    private String receivetime;//接收时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWarnsignal() {
        return warnsignal;
    }

    public void setWarnsignal(String warnsignal) {
        this.warnsignal = warnsignal;
    }

    public String getIsrelievesignal() {
        return isrelievesignal;
    }

    public void setIsrelievesignal(String isrelievesignal) {
        this.isrelievesignal = isrelievesignal;
    }

    public String getReceivetime() {
        return receivetime;
    }

    public void setReceivetime(String receivetime) {
        this.receivetime = receivetime;
    }

    @Override
    public String toString() {
        return "Dhxd02ReqDto{" +
                "id='" + id + '\'' +
                "warnsignal='" + warnsignal + '\'' +
                "isrelievesignal='" + isrelievesignal + '\'' +
                "receivetime='" + receivetime + '\'' +
                '}';
    }
}
