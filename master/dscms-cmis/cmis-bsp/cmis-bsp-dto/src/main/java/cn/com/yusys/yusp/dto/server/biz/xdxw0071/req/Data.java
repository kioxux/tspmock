package cn.com.yusys.yusp.dto.server.biz.xdxw0071.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询优抵贷抵质押品信息
 * @Author zhangpeng
 * @Date 2021/4/26 22:01
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//客户调查表编号

    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

}
