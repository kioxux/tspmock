package cn.com.yusys.yusp.dto.server.biz.xdtz0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：借据明细查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "loanDate")
    private String loanDate;//贷款日期
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//贷款金额
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//剩余本金
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "repaySpaceUnit")
    private String repaySpaceUnit;//还款间隔单位
    @JsonProperty(value = "repaySpace")
    private BigDecimal repaySpace;//还款间隔
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "billStatus")
    private String billStatus;//借据状态
    @JsonProperty(value = "isWcwl")
    private String isWcwl;//是否白领贷

    public String getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(String loanDate) {
        this.loanDate = loanDate;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRepaySpaceUnit() {
        return repaySpaceUnit;
    }

    public void setRepaySpaceUnit(String repaySpaceUnit) {
        this.repaySpaceUnit = repaySpaceUnit;
    }

    public BigDecimal getRepaySpace() {
        return repaySpace;
    }

    public void setRepaySpace(BigDecimal repaySpace) {
        this.repaySpace = repaySpace;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getIsWcwl() {
        return isWcwl;
    }

    public void setIsWcwl(String isWcwl) {
        this.isWcwl = isWcwl;
    }
}
