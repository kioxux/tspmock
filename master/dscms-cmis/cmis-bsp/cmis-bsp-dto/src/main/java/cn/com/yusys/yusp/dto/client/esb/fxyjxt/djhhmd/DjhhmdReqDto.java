package cn.com.yusys.yusp.dto.client.esb.fxyjxt.djhhmd;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询客户风险预警等级与是否黑灰名单
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class DjhhmdReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "custid")
    private String custid;//客户号

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    @Override
    public String toString() {
        return "DjhhmdReqDto{" +
                "custid='" + custid + '\'' +
                '}';
    }
}
