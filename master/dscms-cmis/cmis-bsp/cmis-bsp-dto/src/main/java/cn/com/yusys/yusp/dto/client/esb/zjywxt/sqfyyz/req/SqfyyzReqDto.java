package cn.com.yusys.yusp.dto.client.esb.zjywxt.sqfyyz.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：行方验证房源信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SqfyyzReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "houseno")
    private String houseno;//房屋唯一标识

    public String getHouseno() {
        return houseno;
    }

    public void setHouseno(String houseno) {
        this.houseno = houseno;
    }

    @Override
    public String toString() {
        return "SqfyyzReqDto{" +
                "houseno='" + houseno + '\'' +
                '}';
    }
}
