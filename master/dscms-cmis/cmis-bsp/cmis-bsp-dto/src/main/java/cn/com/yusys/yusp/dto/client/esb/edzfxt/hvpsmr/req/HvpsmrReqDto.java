package cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：大额往帐一体化
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class HvpsmrReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "hvflag")
    private String hvflag;//大小额交易标志
    @JsonProperty(value = "functp")
    private String functp;//业务类型
    @JsonProperty(value = "csbxno")
    private String csbxno;//钱箱号
    @JsonProperty(value = "subprc")
    private String subprc;//前台交易码
    @JsonProperty(value = "operdt")
    private String operdt;//交易日期
    @JsonProperty(value = "srflag")
    private String srflag;//往来账标志
    @JsonProperty(value = "opersq")
    private String opersq;//业务受理编号
    @JsonProperty(value = "termid")
    private String termid;//终端号
    @JsonProperty(value = "systdt")
    private String systdt;//系统日期
    @JsonProperty(value = "bankid")
    private String bankid;//行号
    @JsonProperty(value = "mesgtp")
    private String mesgtp;//跨境标志
    @JsonProperty(value = "flagtp")
    private String flagtp;//疑似重账标志
    @JsonProperty(value = "tlmotp")
    private String tlmotp;//电汇凭证类型
    @JsonProperty(value = "tlmono")
    private String tlmono;//电汇凭证号码
    @JsonProperty(value = "opertp")
    private String opertp;//业务类型
    @JsonProperty(value = "opertr")
    private String opertr;//业务种类
    @JsonProperty(value = "crcycd")
    private String crcycd;//币种
    @JsonProperty(value = "tranam")
    private BigDecimal tranam;//交易金额
    @JsonProperty(value = "rdcode")
    private String rdcode;//接收行行号
    @JsonProperty(value = "rdname")
    private String rdname;//接收行行名
    @JsonProperty(value = "pyeeac")
    private String pyeeac;//实际收款账号
    @JsonProperty(value = "pyeena")
    private String pyeena;//实际收款人名称
    @JsonProperty(value = "pyeead")
    private String pyeead;//实际收款人地址
    @JsonProperty(value = "pscrtx")
    private String pscrtx;//附言
    @JsonProperty(value = "isfykh")
    private String isfykh;//是否法院查控
    @JsonProperty(value = "priotg")
    private String priotg;//业务优先级
    @JsonProperty(value = "billtp")
    private String billtp;//票据种类
    @JsonProperty(value = "pyerac")
    private String pyerac;//账户账号
    @JsonProperty(value = "pyerna")
    private String pyerna;//账户户名
    @JsonProperty(value = "pyerad")
    private String pyerad;//客户地址
    @JsonProperty(value = "trantp")
    private String trantp;//转账类型
    @JsonProperty(value = "drawtp")
    private String drawtp;//小额贷记手续费扣款标志
    @JsonProperty(value = "handch")
    private BigDecimal handch;//手续费
    @JsonProperty(value = "postch")
    private BigDecimal postch;//邮电费
    @JsonProperty(value = "tldctp")
    private String tldctp;//凭证种类
    @JsonProperty(value = "tldcno")
    private String tldcno;//凭证号码
    @JsonProperty(value = "tldcdt")
    private String tldcdt;//凭证日期
    @JsonProperty(value = "compam")
    private BigDecimal compam;//赔偿金额
    @JsonProperty(value = "repuam")
    private BigDecimal repuam;//拒付金额
    @JsonProperty(value = "sctram")
    private BigDecimal sctram;//原托金额
    @JsonProperty(value = "pytram")
    private BigDecimal pytram;//支付金额
    @JsonProperty(value = "adtram")
    private BigDecimal adtram;//多付金额
    @JsonProperty(value = "bklnrt")
    private BigDecimal bklnrt;//拆借利率
    @JsonProperty(value = "bklntm")
    private Integer bklntm;//拆借天数
    @JsonProperty(value = "stzftg")
    private String stzftg;//受托支付标志
    @JsonProperty(value = "bdwybs")
    private String bdwybs;//标段号
    @JsonProperty(value = "billsq")
    private String billsq;//账单流水
    @JsonProperty(value = "trandt2")
    private String trandt2;//招标日期
    @JsonProperty(value = "certid")
    private String certid;//用户证书
    @JsonProperty(value = "adcode")
    private String adcode;//行政区划
    @JsonProperty(value = "styear")
    private String styear;//业务年度
    @JsonProperty(value = "vtcode")
    private String vtcode;//凭证类型
    @JsonProperty(value = "vochno")
    private String vochno;//凭证号码
    @JsonProperty(value = "agcode")
    private String agcode;//基层预算单位编码
    @JsonProperty(value = "agname")
    private String agname;//基层预算单位名称
    @JsonProperty(value = "efcode")
    private String efcode;//支出功能分类科目编码
    @JsonProperty(value = "efname")
    private String efname;//支出功能分类科目名称
    @JsonProperty(value = "mocode")
    private String mocode;//结算方式编码（现金or转账）
    @JsonProperty(value = "moname")
    private String moname;//结算方式名称
    @JsonProperty(value = "pbcode")
    private String pbcode;//代理银行编码
    @JsonProperty(value = "pbname")
    private String pbname;//代理银行名称
    @JsonProperty(value = "paycno")
    private String paycno;//付款人账号
    @JsonProperty(value = "paycna")
    private String paycna;//付款人名称
    @JsonProperty(value = "pabkna")
    private String pabkna;//付款行行号
    @JsonProperty(value = "pasuna")
    private String pasuna;//用途名称
    @JsonProperty(value = "chekno")
    private String chekno;//票据号码
    @JsonProperty(value = "xpbkna")
    private String xpbkna;//收款人银行
    @JsonProperty(value = "secret")
    private String secret;//支票密码
    @JsonProperty(value = "asstno")
    private String asstno;//关联业务参考号
    @JsonProperty(value = "xpbkno")
    private String xpbkno;//
    @JsonProperty(value = "xpctno")
    private String xpctno;//收款人账号
    @JsonProperty(value = "xpctna")
    private String xpctna;//收款人全称
    @JsonProperty(value = "asstdt")
    private String asstdt;//关联委托日期
    @JsonProperty(value = "rksf")
    private String rksf;//入库身份
    @JsonProperty(value = "chrgcd")
    private String chrgcd;//费用编码
    @JsonProperty(value = "fundtp")
    private String fundtp;//预算类型
    @JsonProperty(value = "billdt")
    private String billdt;//支票签发日期
    @JsonProperty(value = "rmkinf")
    private String rmkinf;//跨境业务附言
    @JsonProperty(value = "billno")
    private String billno;//支票号码
    @JsonProperty(value = "sndmam")
    private String sndmam;//发报行的收费
    @JsonProperty(value = "billna")
    private String billna;//出票人名称
    @JsonProperty(value = "billam")
    private String billam;//支票金额
    @JsonProperty(value = "billpr")
    private String billpr;//牌价
    @JsonProperty(value = "excsendflg")
    private String excsendflg;//大额汇兑发送方式
    @JsonProperty(value = "billcn")
    private String billcn;//票据张数
    @JsonProperty(value = "oriinstgpty")
    private String oriinstgpty;//原发起参与机构
    @JsonProperty(value = "rexchngrslt")
    private String rexchngrslt;//退汇原因
    @JsonProperty(value = "orimsgtp")
    private String orimsgtp;//原报文类型


    @JsonProperty(value = "billac")
    private String billac;//支票签发日期

    @JsonProperty(value = "dftaam                                                                                                        ")
    private String dftaam;//支票签发日期
    @JsonProperty(value = "dftram")
    private String dftram;//支票签发日期
    @JsonProperty(value = "rcvmam")
    private String rcvmam;//支票签发日期
    @JsonProperty(value = "billmy")
    private String billmy;//支票签发日期
    @JsonProperty(value = "dfcode")
    private String dfcode;//支票签发日期
    @JsonProperty(value = "dftrtp")
    private String dftrtp;//支票签发日期
    @JsonProperty(value = "seqnno")
    private String seqnno;//支票签发日期
    @JsonProperty(value = "onofbz")
    private String onofbz;//支票签发日期
    @JsonProperty(value = "stzfbill")
    private String stzfbill;//支票签发日期
    @JsonProperty(value = "seqnum")
    private String seqnum;//支票签发日期
    @JsonProperty(value = "wkdate")
    private String wkdate;//支票签发日期
    @JsonProperty(value = "dzpzkflg")
    private String dzpzkflg;//支票签发日期
    @JsonProperty(value = "djbh")
    private String djbh;//支票签发日期
    @JsonProperty(value = "jzzym")
    private String jzzym;//记账摘要码
    @JsonProperty(value = "hxkhh")
    private String hxkhh;//核心客户号
    @JsonProperty(value = "dcmtgp")
    private String dcmtgp;//凭证组合
    @JsonProperty(value = "dlzjzlei1")
    private String dlzjzlei1;//代理人证件类型
    @JsonProperty(value = "dailrzjh1")
    private String dailrzjh1;//代理人证件号码
    @JsonProperty(value = "dailremc1")
    private String dailremc1;//代理人名称
    // 以下字段2021.9.9 add
    @JsonProperty(value = "waibjymc")
    private String waibjymc;
    @JsonProperty(value = "servti")
    private String servti;//服务时间
    @JsonProperty(value = "servdt")
    private String servdt;//服务日期
    @JsonProperty(value = "waibclma")
    private String waibclma;

    @JsonProperty(value = "brchno")
    private String brchno;//    部门号,取账务机构号


    public String getHvflag() {
        return hvflag;
    }

    public void setHvflag(String hvflag) {
        this.hvflag = hvflag;
    }

    public String getFunctp() {
        return functp;
    }

    public void setFunctp(String functp) {
        this.functp = functp;
    }

    public String getCsbxno() {
        return csbxno;
    }

    public void setCsbxno(String csbxno) {
        this.csbxno = csbxno;
    }

    public String getSubprc() {
        return subprc;
    }

    public void setSubprc(String subprc) {
        this.subprc = subprc;
    }

    public String getOperdt() {
        return operdt;
    }

    public void setOperdt(String operdt) {
        this.operdt = operdt;
    }

    public String getSrflag() {
        return srflag;
    }

    public void setSrflag(String srflag) {
        this.srflag = srflag;
    }

    public String getOpersq() {
        return opersq;
    }

    public void setOpersq(String opersq) {
        this.opersq = opersq;
    }

    public String getTermid() {
        return termid;
    }

    public void setTermid(String termid) {
        this.termid = termid;
    }

    public String getSystdt() {
        return systdt;
    }

    public void setSystdt(String systdt) {
        this.systdt = systdt;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getMesgtp() {
        return mesgtp;
    }

    public void setMesgtp(String mesgtp) {
        this.mesgtp = mesgtp;
    }

    public String getFlagtp() {
        return flagtp;
    }

    public void setFlagtp(String flagtp) {
        this.flagtp = flagtp;
    }

    public String getTlmotp() {
        return tlmotp;
    }

    public void setTlmotp(String tlmotp) {
        this.tlmotp = tlmotp;
    }

    public String getTlmono() {
        return tlmono;
    }

    public void setTlmono(String tlmono) {
        this.tlmono = tlmono;
    }

    public String getOpertp() {
        return opertp;
    }

    public void setOpertp(String opertp) {
        this.opertp = opertp;
    }

    public String getOpertr() {
        return opertr;
    }

    public void setOpertr(String opertr) {
        this.opertr = opertr;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getRdcode() {
        return rdcode;
    }

    public void setRdcode(String rdcode) {
        this.rdcode = rdcode;
    }

    public String getRdname() {
        return rdname;
    }

    public void setRdname(String rdname) {
        this.rdname = rdname;
    }

    public String getPyeeac() {
        return pyeeac;
    }

    public void setPyeeac(String pyeeac) {
        this.pyeeac = pyeeac;
    }

    public String getPyeena() {
        return pyeena;
    }

    public void setPyeena(String pyeena) {
        this.pyeena = pyeena;
    }

    public String getPyeead() {
        return pyeead;
    }

    public void setPyeead(String pyeead) {
        this.pyeead = pyeead;
    }

    public String getPscrtx() {
        return pscrtx;
    }

    public void setPscrtx(String pscrtx) {
        this.pscrtx = pscrtx;
    }

    public String getIsfykh() {
        return isfykh;
    }

    public void setIsfykh(String isfykh) {
        this.isfykh = isfykh;
    }

    public String getPriotg() {
        return priotg;
    }

    public void setPriotg(String priotg) {
        this.priotg = priotg;
    }

    public String getBilltp() {
        return billtp;
    }

    public void setBilltp(String billtp) {
        this.billtp = billtp;
    }

    public String getPyerac() {
        return pyerac;
    }

    public void setPyerac(String pyerac) {
        this.pyerac = pyerac;
    }

    public String getPyerna() {
        return pyerna;
    }

    public void setPyerna(String pyerna) {
        this.pyerna = pyerna;
    }

    public String getPyerad() {
        return pyerad;
    }

    public void setPyerad(String pyerad) {
        this.pyerad = pyerad;
    }

    public String getTrantp() {
        return trantp;
    }

    public void setTrantp(String trantp) {
        this.trantp = trantp;
    }

    public String getDrawtp() {
        return drawtp;
    }

    public void setDrawtp(String drawtp) {
        this.drawtp = drawtp;
    }

    public BigDecimal getHandch() {
        return handch;
    }

    public void setHandch(BigDecimal handch) {
        this.handch = handch;
    }

    public BigDecimal getPostch() {
        return postch;
    }

    public void setPostch(BigDecimal postch) {
        this.postch = postch;
    }

    public String getTldctp() {
        return tldctp;
    }

    public void setTldctp(String tldctp) {
        this.tldctp = tldctp;
    }

    public String getTldcno() {
        return tldcno;
    }

    public void setTldcno(String tldcno) {
        this.tldcno = tldcno;
    }

    public String getTldcdt() {
        return tldcdt;
    }

    public void setTldcdt(String tldcdt) {
        this.tldcdt = tldcdt;
    }

    public BigDecimal getCompam() {
        return compam;
    }

    public void setCompam(BigDecimal compam) {
        this.compam = compam;
    }

    public BigDecimal getRepuam() {
        return repuam;
    }

    public void setRepuam(BigDecimal repuam) {
        this.repuam = repuam;
    }

    public BigDecimal getSctram() {
        return sctram;
    }

    public void setSctram(BigDecimal sctram) {
        this.sctram = sctram;
    }

    public BigDecimal getPytram() {
        return pytram;
    }

    public void setPytram(BigDecimal pytram) {
        this.pytram = pytram;
    }

    public BigDecimal getAdtram() {
        return adtram;
    }

    public void setAdtram(BigDecimal adtram) {
        this.adtram = adtram;
    }

    public BigDecimal getBklnrt() {
        return bklnrt;
    }

    public void setBklnrt(BigDecimal bklnrt) {
        this.bklnrt = bklnrt;
    }

    public Integer getBklntm() {
        return bklntm;
    }

    public void setBklntm(Integer bklntm) {
        this.bklntm = bklntm;
    }

    public String getStzftg() {
        return stzftg;
    }

    public void setStzftg(String stzftg) {
        this.stzftg = stzftg;
    }

    public String getBdwybs() {
        return bdwybs;
    }

    public void setBdwybs(String bdwybs) {
        this.bdwybs = bdwybs;
    }

    public String getBillsq() {
        return billsq;
    }

    public void setBillsq(String billsq) {
        this.billsq = billsq;
    }

    public String getTrandt2() {
        return trandt2;
    }

    public void setTrandt2(String trandt2) {
        this.trandt2 = trandt2;
    }

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getAdcode() {
        return adcode;
    }

    public void setAdcode(String adcode) {
        this.adcode = adcode;
    }

    public String getStyear() {
        return styear;
    }

    public void setStyear(String styear) {
        this.styear = styear;
    }

    public String getVtcode() {
        return vtcode;
    }

    public void setVtcode(String vtcode) {
        this.vtcode = vtcode;
    }

    public String getVochno() {
        return vochno;
    }

    public void setVochno(String vochno) {
        this.vochno = vochno;
    }

    public String getAgcode() {
        return agcode;
    }

    public void setAgcode(String agcode) {
        this.agcode = agcode;
    }

    public String getAgname() {
        return agname;
    }

    public void setAgname(String agname) {
        this.agname = agname;
    }

    public String getEfcode() {
        return efcode;
    }

    public void setEfcode(String efcode) {
        this.efcode = efcode;
    }

    public String getEfname() {
        return efname;
    }

    public void setEfname(String efname) {
        this.efname = efname;
    }

    public String getMocode() {
        return mocode;
    }

    public void setMocode(String mocode) {
        this.mocode = mocode;
    }

    public String getMoname() {
        return moname;
    }

    public void setMoname(String moname) {
        this.moname = moname;
    }

    public String getPbcode() {
        return pbcode;
    }

    public void setPbcode(String pbcode) {
        this.pbcode = pbcode;
    }

    public String getPbname() {
        return pbname;
    }

    public void setPbname(String pbname) {
        this.pbname = pbname;
    }

    public String getPaycno() {
        return paycno;
    }

    public void setPaycno(String paycno) {
        this.paycno = paycno;
    }

    public String getPaycna() {
        return paycna;
    }

    public void setPaycna(String paycna) {
        this.paycna = paycna;
    }

    public String getPabkna() {
        return pabkna;
    }

    public void setPabkna(String pabkna) {
        this.pabkna = pabkna;
    }

    public String getPasuna() {
        return pasuna;
    }

    public void setPasuna(String pasuna) {
        this.pasuna = pasuna;
    }

    public String getChekno() {
        return chekno;
    }

    public void setChekno(String chekno) {
        this.chekno = chekno;
    }

    public String getXpbkna() {
        return xpbkna;
    }

    public void setXpbkna(String xpbkna) {
        this.xpbkna = xpbkna;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getAsstno() {
        return asstno;
    }

    public void setAsstno(String asstno) {
        this.asstno = asstno;
    }

    public String getXpbkno() {
        return xpbkno;
    }

    public void setXpbkno(String xpbkno) {
        this.xpbkno = xpbkno;
    }

    public String getXpctno() {
        return xpctno;
    }

    public void setXpctno(String xpctno) {
        this.xpctno = xpctno;
    }

    public String getXpctna() {
        return xpctna;
    }

    public void setXpctna(String xpctna) {
        this.xpctna = xpctna;
    }

    public String getAsstdt() {
        return asstdt;
    }

    public void setAsstdt(String asstdt) {
        this.asstdt = asstdt;
    }

    public String getRksf() {
        return rksf;
    }

    public void setRksf(String rksf) {
        this.rksf = rksf;
    }

    public String getChrgcd() {
        return chrgcd;
    }

    public void setChrgcd(String chrgcd) {
        this.chrgcd = chrgcd;
    }

    public String getFundtp() {
        return fundtp;
    }

    public void setFundtp(String fundtp) {
        this.fundtp = fundtp;
    }

    public String getBilldt() {
        return billdt;
    }

    public void setBilldt(String billdt) {
        this.billdt = billdt;
    }

    public String getRmkinf() {
        return rmkinf;
    }

    public void setRmkinf(String rmkinf) {
        this.rmkinf = rmkinf;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getSndmam() {
        return sndmam;
    }

    public void setSndmam(String sndmam) {
        this.sndmam = sndmam;
    }

    public String getBillna() {
        return billna;
    }

    public void setBillna(String billna) {
        this.billna = billna;
    }

    public String getBillam() {
        return billam;
    }

    public void setBillam(String billam) {
        this.billam = billam;
    }

    public String getBillpr() {
        return billpr;
    }

    public void setBillpr(String billpr) {
        this.billpr = billpr;
    }

    public String getExcsendflg() {
        return excsendflg;
    }

    public void setExcsendflg(String excsendflg) {
        this.excsendflg = excsendflg;
    }

    public String getBillcn() {
        return billcn;
    }

    public void setBillcn(String billcn) {
        this.billcn = billcn;
    }

    public String getOriinstgpty() {
        return oriinstgpty;
    }

    public void setOriinstgpty(String oriinstgpty) {
        this.oriinstgpty = oriinstgpty;
    }

    public String getRexchngrslt() {
        return rexchngrslt;
    }

    public void setRexchngrslt(String rexchngrslt) {
        this.rexchngrslt = rexchngrslt;
    }

    public String getOrimsgtp() {
        return orimsgtp;
    }

    public void setOrimsgtp(String orimsgtp) {
        this.orimsgtp = orimsgtp;
    }

    public String getBillac() {
        return billac;
    }

    public void setBillac(String billac) {
        this.billac = billac;
    }

    public String getDftaam() {
        return dftaam;
    }

    public void setDftaam(String dftaam) {
        this.dftaam = dftaam;
    }

    public String getDftram() {
        return dftram;
    }

    public void setDftram(String dftram) {
        this.dftram = dftram;
    }

    public String getRcvmam() {
        return rcvmam;
    }

    public void setRcvmam(String rcvmam) {
        this.rcvmam = rcvmam;
    }

    public String getBillmy() {
        return billmy;
    }

    public void setBillmy(String billmy) {
        this.billmy = billmy;
    }

    public String getDfcode() {
        return dfcode;
    }

    public void setDfcode(String dfcode) {
        this.dfcode = dfcode;
    }

    public String getDftrtp() {
        return dftrtp;
    }

    public void setDftrtp(String dftrtp) {
        this.dftrtp = dftrtp;
    }

    public String getSeqnno() {
        return seqnno;
    }

    public void setSeqnno(String seqnno) {
        this.seqnno = seqnno;
    }

    public String getOnofbz() {
        return onofbz;
    }

    public void setOnofbz(String onofbz) {
        this.onofbz = onofbz;
    }

    public String getStzfbill() {
        return stzfbill;
    }

    public void setStzfbill(String stzfbill) {
        this.stzfbill = stzfbill;
    }

    public String getSeqnum() {
        return seqnum;
    }

    public void setSeqnum(String seqnum) {
        this.seqnum = seqnum;
    }

    public String getWkdate() {
        return wkdate;
    }

    public void setWkdate(String wkdate) {
        this.wkdate = wkdate;
    }

    public String getDzpzkflg() {
        return dzpzkflg;
    }

    public void setDzpzkflg(String dzpzkflg) {
        this.dzpzkflg = dzpzkflg;
    }

    public String getDjbh() {
        return djbh;
    }

    public void setDjbh(String djbh) {
        this.djbh = djbh;
    }

    public String getJzzym() {
        return jzzym;
    }

    public void setJzzym(String jzzym) {
        this.jzzym = jzzym;
    }

    public String getHxkhh() {
        return hxkhh;
    }

    public void setHxkhh(String hxkhh) {
        this.hxkhh = hxkhh;
    }

    public String getDcmtgp() {
        return dcmtgp;
    }

    public void setDcmtgp(String dcmtgp) {
        this.dcmtgp = dcmtgp;
    }

    public String getDlzjzlei1() {
        return dlzjzlei1;
    }

    public void setDlzjzlei1(String dlzjzlei1) {
        this.dlzjzlei1 = dlzjzlei1;
    }

    public String getDailrzjh1() {
        return dailrzjh1;
    }

    public void setDailrzjh1(String dailrzjh1) {
        this.dailrzjh1 = dailrzjh1;
    }

    public String getDailremc1() {
        return dailremc1;
    }

    public void setDailremc1(String dailremc1) {
        this.dailremc1 = dailremc1;
    }

    public String getWaibjymc() {
        return waibjymc;
    }

    public void setWaibjymc(String waibjymc) {
        this.waibjymc = waibjymc;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getWaibclma() {
        return waibclma;
    }

    public void setWaibclma(String waibclma) {
        this.waibclma = waibclma;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    @Override
    public String toString() {
        return "HvpsmrReqDto{" +
                "hvflag='" + hvflag + '\'' +
                ", functp='" + functp + '\'' +
                ", csbxno='" + csbxno + '\'' +
                ", subprc='" + subprc + '\'' +
                ", operdt='" + operdt + '\'' +
                ", srflag='" + srflag + '\'' +
                ", opersq='" + opersq + '\'' +
                ", termid='" + termid + '\'' +
                ", systdt='" + systdt + '\'' +
                ", bankid='" + bankid + '\'' +
                ", mesgtp='" + mesgtp + '\'' +
                ", flagtp='" + flagtp + '\'' +
                ", tlmotp='" + tlmotp + '\'' +
                ", tlmono='" + tlmono + '\'' +
                ", opertp='" + opertp + '\'' +
                ", opertr='" + opertr + '\'' +
                ", crcycd='" + crcycd + '\'' +
                ", tranam=" + tranam +
                ", rdcode='" + rdcode + '\'' +
                ", rdname='" + rdname + '\'' +
                ", pyeeac='" + pyeeac + '\'' +
                ", pyeena='" + pyeena + '\'' +
                ", pyeead='" + pyeead + '\'' +
                ", pscrtx='" + pscrtx + '\'' +
                ", isfykh='" + isfykh + '\'' +
                ", priotg='" + priotg + '\'' +
                ", billtp='" + billtp + '\'' +
                ", pyerac='" + pyerac + '\'' +
                ", pyerna='" + pyerna + '\'' +
                ", pyerad='" + pyerad + '\'' +
                ", trantp='" + trantp + '\'' +
                ", drawtp='" + drawtp + '\'' +
                ", handch=" + handch +
                ", postch=" + postch +
                ", tldctp='" + tldctp + '\'' +
                ", tldcno='" + tldcno + '\'' +
                ", tldcdt='" + tldcdt + '\'' +
                ", compam=" + compam +
                ", repuam=" + repuam +
                ", sctram=" + sctram +
                ", pytram=" + pytram +
                ", adtram=" + adtram +
                ", bklnrt=" + bklnrt +
                ", bklntm=" + bklntm +
                ", stzftg='" + stzftg + '\'' +
                ", bdwybs='" + bdwybs + '\'' +
                ", billsq='" + billsq + '\'' +
                ", trandt2='" + trandt2 + '\'' +
                ", certid='" + certid + '\'' +
                ", adcode='" + adcode + '\'' +
                ", styear='" + styear + '\'' +
                ", vtcode='" + vtcode + '\'' +
                ", vochno='" + vochno + '\'' +
                ", agcode='" + agcode + '\'' +
                ", agname='" + agname + '\'' +
                ", efcode='" + efcode + '\'' +
                ", efname='" + efname + '\'' +
                ", mocode='" + mocode + '\'' +
                ", moname='" + moname + '\'' +
                ", pbcode='" + pbcode + '\'' +
                ", pbname='" + pbname + '\'' +
                ", paycno='" + paycno + '\'' +
                ", paycna='" + paycna + '\'' +
                ", pabkna='" + pabkna + '\'' +
                ", pasuna='" + pasuna + '\'' +
                ", chekno='" + chekno + '\'' +
                ", xpbkna='" + xpbkna + '\'' +
                ", secret='" + secret + '\'' +
                ", asstno='" + asstno + '\'' +
                ", xpbkno='" + xpbkno + '\'' +
                ", xpctno='" + xpctno + '\'' +
                ", xpctna='" + xpctna + '\'' +
                ", asstdt='" + asstdt + '\'' +
                ", rksf='" + rksf + '\'' +
                ", chrgcd='" + chrgcd + '\'' +
                ", fundtp='" + fundtp + '\'' +
                ", billdt='" + billdt + '\'' +
                ", rmkinf='" + rmkinf + '\'' +
                ", billno='" + billno + '\'' +
                ", sndmam='" + sndmam + '\'' +
                ", billna='" + billna + '\'' +
                ", billam='" + billam + '\'' +
                ", billpr='" + billpr + '\'' +
                ", excsendflg='" + excsendflg + '\'' +
                ", billcn='" + billcn + '\'' +
                ", oriinstgpty='" + oriinstgpty + '\'' +
                ", rexchngrslt='" + rexchngrslt + '\'' +
                ", orimsgtp='" + orimsgtp + '\'' +
                ", billac='" + billac + '\'' +
                ", dftaam='" + dftaam + '\'' +
                ", dftram='" + dftram + '\'' +
                ", rcvmam='" + rcvmam + '\'' +
                ", billmy='" + billmy + '\'' +
                ", dfcode='" + dfcode + '\'' +
                ", dftrtp='" + dftrtp + '\'' +
                ", seqnno='" + seqnno + '\'' +
                ", onofbz='" + onofbz + '\'' +
                ", stzfbill='" + stzfbill + '\'' +
                ", seqnum='" + seqnum + '\'' +
                ", wkdate='" + wkdate + '\'' +
                ", dzpzkflg='" + dzpzkflg + '\'' +
                ", djbh='" + djbh + '\'' +
                ", jzzym='" + jzzym + '\'' +
                ", hxkhh='" + hxkhh + '\'' +
                ", dcmtgp='" + dcmtgp + '\'' +
                ", dlzjzlei1='" + dlzjzlei1 + '\'' +
                ", dailrzjh1='" + dailrzjh1 + '\'' +
                ", dailremc1='" + dailremc1 + '\'' +
                ", waibjymc='" + waibjymc + '\'' +
                ", servti='" + servti + '\'' +
                ", servdt='" + servdt + '\'' +
                ", waibclma='" + waibclma + '\'' +
                ", brchno='" + brchno + '\'' +
                '}';
    }
}
