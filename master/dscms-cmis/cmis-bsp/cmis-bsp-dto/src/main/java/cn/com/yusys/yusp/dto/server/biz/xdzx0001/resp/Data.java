package cn.com.yusys.yusp.dto.server.biz.xdzx0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
/**
 * 响应Dto：查询征信业务流水号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zxSerno")
    private String zxSerno;//征信业务流水号

    public String getZxSerno() {
        return zxSerno;
    }

    public void setZxSerno(String zxSerno) {
        this.zxSerno = zxSerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "zxSerno='" + zxSerno + '\'' +
                '}';
    }
}
