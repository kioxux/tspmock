package cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/5 11:34
 * @since 2021/6/5 11:34
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "common_owner_name")
    private String common_owner_name;//共有人客户名称
    @JsonProperty(value = "common_cert_code")
    private String common_cert_code;//共有人证件号码
    @JsonProperty(value = "common_owner_id")
    private String common_owner_id;//共有人客户编号
    @JsonProperty(value = "common_addr")
    private String common_addr;//共有人地址
    @JsonProperty(value = "common_mail")
    private String common_mail;//共有人电子邮箱
    @JsonProperty(value = "common_qq")
    private String common_qq;//共有人QQ号码
    @JsonProperty(value = "common_wxin")
    private String common_wxin;//共有人微信号码
    @JsonProperty(value = "common_tel")
    private String common_tel;//共有人电话
    @JsonProperty(value = "house_no")
    private String house_no;//不动产（房产证号）
    @JsonProperty(value = "share_owner_type")
    private String share_owner_type;//共有人客户类型
    @JsonProperty(value = "common_hold_portio")
    private BigDecimal common_hold_portio;//共有人所占份额
    @JsonProperty(value = "common_cert_type")
    private String common_cert_type;//共有人证件类型

    public String getCommon_owner_name() {
        return common_owner_name;
    }

    public void setCommon_owner_name(String common_owner_name) {
        this.common_owner_name = common_owner_name;
    }

    public String getCommon_cert_code() {
        return common_cert_code;
    }

    public void setCommon_cert_code(String common_cert_code) {
        this.common_cert_code = common_cert_code;
    }

    public String getCommon_owner_id() {
        return common_owner_id;
    }

    public void setCommon_owner_id(String common_owner_id) {
        this.common_owner_id = common_owner_id;
    }

    public String getCommon_addr() {
        return common_addr;
    }

    public void setCommon_addr(String common_addr) {
        this.common_addr = common_addr;
    }

    public String getCommon_mail() {
        return common_mail;
    }

    public void setCommon_mail(String common_mail) {
        this.common_mail = common_mail;
    }

    public String getCommon_qq() {
        return common_qq;
    }

    public void setCommon_qq(String common_qq) {
        this.common_qq = common_qq;
    }

    public String getCommon_wxin() {
        return common_wxin;
    }

    public void setCommon_wxin(String common_wxin) {
        this.common_wxin = common_wxin;
    }

    public String getCommon_tel() {
        return common_tel;
    }

    public void setCommon_tel(String common_tel) {
        this.common_tel = common_tel;
    }

    public String getHouse_no() {
        return house_no;
    }

    public void setHouse_no(String house_no) {
        this.house_no = house_no;
    }

    public String getShare_owner_type() {
        return share_owner_type;
    }

    public void setShare_owner_type(String share_owner_type) {
        this.share_owner_type = share_owner_type;
    }

    public BigDecimal getCommon_hold_portio() {
        return common_hold_portio;
    }

    public void setCommon_hold_portio(BigDecimal common_hold_portio) {
        this.common_hold_portio = common_hold_portio;
    }

    public String getCommon_cert_type() {
        return common_cert_type;
    }

    public void setCommon_cert_type(String common_cert_type) {
        this.common_cert_type = common_cert_type;
    }

    @Override
    public String toString() {
        return "List{" +
                "common_owner_name='" + common_owner_name + '\'' +
                ", common_cert_code='" + common_cert_code + '\'' +
                ", common_owner_id='" + common_owner_id + '\'' +
                ", common_addr='" + common_addr + '\'' +
                ", common_mail='" + common_mail + '\'' +
                ", common_qq='" + common_qq + '\'' +
                ", common_wxin='" + common_wxin + '\'' +
                ", common_tel='" + common_tel + '\'' +
                ", house_no='" + house_no + '\'' +
                ", share_owner_type='" + share_owner_type + '\'' +
                ", common_hold_portio=" + common_hold_portio +
                ", common_cert_type='" + common_cert_type + '\'' +
                '}';
    }
}
