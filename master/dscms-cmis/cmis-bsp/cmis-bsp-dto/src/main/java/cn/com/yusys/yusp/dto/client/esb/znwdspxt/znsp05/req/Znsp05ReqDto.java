package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp05.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：风险拦截截接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp05ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cert_no")
    private String cert_no;//证件号
    @JsonProperty(value = "cus_mgr_no")
    private String cus_mgr_no;//客户经理工号
    @JsonProperty(value = "check_type")
    private String check_type;//校验类型
    @JsonProperty(value = "cus_mgr_name")
    private String cus_mgr_name;//客户经理名称

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCert_no() {
        return cert_no;
    }

    public void setCert_no(String cert_no) {
        this.cert_no = cert_no;
    }

    public String getCus_mgr_no() {
        return cus_mgr_no;
    }

    public void setCus_mgr_no(String cus_mgr_no) {
        this.cus_mgr_no = cus_mgr_no;
    }

    public String getCheck_type() {
        return check_type;
    }

    public void setCheck_type(String check_type) {
        this.check_type = check_type;
    }

    public String getCus_mgr_name() {
        return cus_mgr_name;
    }

    public void setCus_mgr_name(String cus_mgr_name) {
        this.cus_mgr_name = cus_mgr_name;
    }

    @Override
    public String toString() {
        return "Znsp05ReqDto{" +
                "cus_name='" + cus_name + '\'' +
                "cert_no='" + cert_no + '\'' +
                "cus_mgr_no='" + cus_mgr_no + '\'' +
                "check_type='" + check_type + '\'' +
                "cus_mgr_name='" + cus_mgr_name + '\'' +
                '}';
    }
}  
