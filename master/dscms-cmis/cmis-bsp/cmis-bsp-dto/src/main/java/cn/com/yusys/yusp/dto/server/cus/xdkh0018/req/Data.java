package cn.com.yusys.yusp.dto.server.cus.xdkh0018.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：临时客户信息维护
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dgdslx")
    private String dgdslx;//对公对私类型
    @JsonProperty(value = "zjtype")
    private String zjtype;//证件类型
    @JsonProperty(value = "zjcode")
    private String zjcode;//证件号码
    @JsonProperty(value = "kehumc")
    private String kehumc;//客户名称
    @JsonProperty(value = "contry")
    private String contry;//国别
    @JsonProperty(value = "kehulx")
    private String kehulx;//客户类型
    @JsonProperty(value = "whgudg")
    private String whgudg;//是否我行股东
    @JsonProperty(value = "xigbie")
    private String xigbie;//性别（对私必输）
    @JsonProperty(value = "csriqi")
    private String csriqi;//出生日期（对私必输）
    @JsonProperty(value = "zuigxl")
    private String zuigxl;//最高学历（对私必输）
    @JsonProperty(value = "zuigxw")
    private String zuigxw;//最高学位（对私必输）
    @JsonProperty(value = "xdzhiy")
    private String xdzhiy;//职业（对私必输）
    @JsonProperty(value = "xdduty")
    private String xdduty;//职务（对私必输）
    @JsonProperty(value = "xdzhic")
    private String xdzhic;//职称（对私必输）
    @JsonProperty(value = "txdizh")
    private String txdizh;//通讯地址（对私必输）
    @JsonProperty(value = "zzdiah")
    private String zzdiah;//住宅电话（对私必输）
    @JsonProperty(value = "sfdanb")
    private String sfdanb;//是否为担保公司（对公必输）
    @JsonProperty(value = "tzzhut")
    private String tzzhut;//投资主体
    @JsonProperty(value = "kgtype")
    private String kgtype;//控股类型
    @JsonProperty(value = "qyguim")
    private String qyguim;//企业规模
    @JsonProperty(value = "hyfenl")
    private String hyfenl;//行业分类
    @JsonProperty(value = "cldate")
    private String cldate;//成立日期
    @JsonProperty(value = "xinzqh")
    private String xinzqh;//注册地行政区划
    @JsonProperty(value = "inptid")
    private String inptid;//登记人
    @JsonProperty(value = "mangid")
    private String mangid;//责任人
    @JsonProperty(value = "inbrid")
    private String inbrid;//登记人机构
    @JsonProperty(value = "mabrid")
    private String mabrid;//责任人机构
    @JsonProperty(value = "indate")
    private String indate;//登记日期

    public String getDgdslx() {
        return dgdslx;
    }

    public void setDgdslx(String dgdslx) {
        this.dgdslx = dgdslx;
    }

    public String getZjtype() {
        return zjtype;
    }

    public void setZjtype(String zjtype) {
        this.zjtype = zjtype;
    }

    public String getZjcode() {
        return zjcode;
    }

    public void setZjcode(String zjcode) {
        this.zjcode = zjcode;
    }

    public String getKehumc() {
        return kehumc;
    }

    public void setKehumc(String kehumc) {
        this.kehumc = kehumc;
    }

    public String getContry() {
        return contry;
    }

    public void setContry(String contry) {
        this.contry = contry;
    }

    public String getKehulx() {
        return kehulx;
    }

    public void setKehulx(String kehulx) {
        this.kehulx = kehulx;
    }

    public String getWhgudg() {
        return whgudg;
    }

    public void setWhgudg(String whgudg) {
        this.whgudg = whgudg;
    }

    public String getXigbie() {
        return xigbie;
    }

    public void setXigbie(String xigbie) {
        this.xigbie = xigbie;
    }

    public String getCsriqi() {
        return csriqi;
    }

    public void setCsriqi(String csriqi) {
        this.csriqi = csriqi;
    }

    public String getZuigxl() {
        return zuigxl;
    }

    public void setZuigxl(String zuigxl) {
        this.zuigxl = zuigxl;
    }

    public String getZuigxw() {
        return zuigxw;
    }

    public void setZuigxw(String zuigxw) {
        this.zuigxw = zuigxw;
    }

    public String getXdzhiy() {
        return xdzhiy;
    }

    public void setXdzhiy(String xdzhiy) {
        this.xdzhiy = xdzhiy;
    }

    public String getXdduty() {
        return xdduty;
    }

    public void setXdduty(String xdduty) {
        this.xdduty = xdduty;
    }

    public String getXdzhic() {
        return xdzhic;
    }

    public void setXdzhic(String xdzhic) {
        this.xdzhic = xdzhic;
    }

    public String getTxdizh() {
        return txdizh;
    }

    public void setTxdizh(String txdizh) {
        this.txdizh = txdizh;
    }

    public String getZzdiah() {
        return zzdiah;
    }

    public void setZzdiah(String zzdiah) {
        this.zzdiah = zzdiah;
    }

    public String getSfdanb() {
        return sfdanb;
    }

    public void setSfdanb(String sfdanb) {
        this.sfdanb = sfdanb;
    }

    public String getTzzhut() {
        return tzzhut;
    }

    public void setTzzhut(String tzzhut) {
        this.tzzhut = tzzhut;
    }

    public String getKgtype() {
        return kgtype;
    }

    public void setKgtype(String kgtype) {
        this.kgtype = kgtype;
    }

    public String getQyguim() {
        return qyguim;
    }

    public void setQyguim(String qyguim) {
        this.qyguim = qyguim;
    }

    public String getHyfenl() {
        return hyfenl;
    }

    public void setHyfenl(String hyfenl) {
        this.hyfenl = hyfenl;
    }

    public String getCldate() {
        return cldate;
    }

    public void setCldate(String cldate) {
        this.cldate = cldate;
    }

    public String getXinzqh() {
        return xinzqh;
    }

    public void setXinzqh(String xinzqh) {
        this.xinzqh = xinzqh;
    }

    public String getInptid() {
        return inptid;
    }

    public void setInptid(String inptid) {
        this.inptid = inptid;
    }

    public String getMangid() {
        return mangid;
    }

    public void setMangid(String mangid) {
        this.mangid = mangid;
    }

    public String getInbrid() {
        return inbrid;
    }

    public void setInbrid(String inbrid) {
        this.inbrid = inbrid;
    }

    public String getMabrid() {
        return mabrid;
    }

    public void setMabrid(String mabrid) {
        this.mabrid = mabrid;
    }

    public String getIndate() {
        return indate;
    }

    public void setIndate(String indate) {
        this.indate = indate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "dgdslx='" + dgdslx + '\'' +
                ", zjtype='" + zjtype + '\'' +
                ", zjcode='" + zjcode + '\'' +
                ", kehumc='" + kehumc + '\'' +
                ", contry='" + contry + '\'' +
                ", kehulx='" + kehulx + '\'' +
                ", whgudg='" + whgudg + '\'' +
                ", xigbie='" + xigbie + '\'' +
                ", csriqi='" + csriqi + '\'' +
                ", zuigxl='" + zuigxl + '\'' +
                ", zuigxw='" + zuigxw + '\'' +
                ", xdzhiy='" + xdzhiy + '\'' +
                ", xdduty='" + xdduty + '\'' +
                ", xdzhic='" + xdzhic + '\'' +
                ", txdizh='" + txdizh + '\'' +
                ", zzdiah='" + zzdiah + '\'' +
                ", sfdanb='" + sfdanb + '\'' +
                ", tzzhut='" + tzzhut + '\'' +
                ", kgtype='" + kgtype + '\'' +
                ", qyguim='" + qyguim + '\'' +
                ", hyfenl='" + hyfenl + '\'' +
                ", cldate='" + cldate + '\'' +
                ", xinzqh='" + xinzqh + '\'' +
                ", inptid='" + inptid + '\'' +
                ", mangid='" + mangid + '\'' +
                ", inbrid='" + inbrid + '\'' +
                ", mabrid='" + mabrid + '\'' +
                ", indate='" + indate + '\'' +
                '}';
    }
}
