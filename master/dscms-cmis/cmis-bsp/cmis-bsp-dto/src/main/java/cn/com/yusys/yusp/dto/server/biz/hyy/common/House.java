package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

//房屋
@JsonPropertyOrder(alphabetic = true)
public class House implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "real_estate_unit_number")
    private String real_estate_unit_number;//不动产单元号
    @JsonProperty(value = "certificate_number")
    private String certificate_number;//产权证书号
    @JsonProperty(value = "certificate_short_number")
    private String certificate_short_number;//产权证书号简称
    @JsonProperty(value = "area")
    private BigDecimal area;//产权面积
    @JsonProperty(value = "usage")
    private Usage usage;//用途

    public String getReal_estate_unit_number() {
        return real_estate_unit_number;
    }

    public void setReal_estate_unit_number(String real_estate_unit_number) {
        this.real_estate_unit_number = real_estate_unit_number;
    }

    public String getCertificate_number() {
        return certificate_number;
    }

    public void setCertificate_number(String certificate_number) {
        this.certificate_number = certificate_number;
    }

    public String getCertificate_short_number() {
        return certificate_short_number;
    }

    public void setCertificate_short_number(String certificate_short_number) {
        this.certificate_short_number = certificate_short_number;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    @Override
    public String toString() {
        return "House{" +
                "real_estate_unit_number='" + real_estate_unit_number + '\'' +
                ", certificate_number='" + certificate_number + '\'' +
                ", certificate_short_number='" + certificate_short_number + '\'' +
                ", area='" + area + '\'' +
                ", usage=" + usage +
                '}';
    }
}
