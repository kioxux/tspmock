package cn.com.yusys.yusp.dto.server.biz.xdht0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：信贷贴现合同号查询
 *
 * @author xuchao
 * @version 1.0
 */
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "discAgrNo")
    private String discAgrNo;//贴现协议编号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "buyType")
    private String buyType;//买入类型
    @JsonProperty(value = "discCurType")
    private String discCurType;//贴现币种
    @JsonProperty(value = "discTotlAmt")
    private BigDecimal discTotlAmt;//贴现总金额
    @JsonProperty(value = "agrManagerId")
    private String agrManagerId;//协议责任人
    @JsonProperty(value = "managerIdOrg")
    private String managerIdOrg;//责任人机构
    @JsonProperty(value = "agrAmt")
    private BigDecimal agrAmt;//协议金额
    @JsonProperty(value = "pvpAmt")
    private BigDecimal pvpAmt;//出账金额
    @JsonProperty(value = "highContSurplusLmt")
    private BigDecimal highContSurplusLmt;//最高额合同剩余额度

    public String getDiscAgrNo() {
        return discAgrNo;
    }

    public void setDiscAgrNo(String discAgrNo) {
        this.discAgrNo = discAgrNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBuyType() {
        return buyType;
    }

    public void setBuyType(String buyType) {
        this.buyType = buyType;
    }

    public String getDiscCurType() {
        return discCurType;
    }

    public void setDiscCurType(String discCurType) {
        this.discCurType = discCurType;
    }

    public BigDecimal getDiscTotlAmt() {
        return discTotlAmt;
    }

    public void setDiscTotlAmt(BigDecimal discTotlAmt) {
        this.discTotlAmt = discTotlAmt;
    }

    public String getAgrManagerId() {
        return agrManagerId;
    }

    public void setAgrManagerId(String agrManagerId) {
        this.agrManagerId = agrManagerId;
    }

    public String getManagerIdOrg() {
        return managerIdOrg;
    }

    public void setManagerIdOrg(String managerIdOrg) {
        this.managerIdOrg = managerIdOrg;
    }

    public BigDecimal getAgrAmt() {
        return agrAmt;
    }

    public void setAgrAmt(BigDecimal agrAmt) {
        this.agrAmt = agrAmt;
    }

    public BigDecimal getPvpAmt() {
        return pvpAmt;
    }

    public void setPvpAmt(BigDecimal pvpAmt) {
        this.pvpAmt = pvpAmt;
    }

    public BigDecimal getHighContSurplusLmt() {
        return highContSurplusLmt;
    }

    public void setHighContSurplusLmt(BigDecimal highContSurplusLmt) {
        this.highContSurplusLmt = highContSurplusLmt;
    }

    @Override
    public String toString() {
        return "List{" +
                "discAgrNo='" + discAgrNo + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", cusName='" + cusName + '\'' +
                ", buyType='" + buyType + '\'' +
                ", discCurType='" + discCurType + '\'' +
                ", discTotlAmt=" + discTotlAmt +
                ", agrManagerId='" + agrManagerId + '\'' +
                ", managerIdOrg='" + managerIdOrg + '\'' +
                ", agrAmt=" + agrAmt +
                ", pvpAmt=" + pvpAmt +
                ", highContSurplusLmt=" + highContSurplusLmt +
                '}';
    }
}
