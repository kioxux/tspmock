package cn.com.yusys.yusp.dto.server.biz.xdtz0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/5/7 9:51
 * @Version 1.0
 */
public class Data {

    @JsonProperty(value = "loanBal")
    private BigDecimal loanBal;//贷款余额

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    @Override
    public String toString() {
        return "Xdtz0003RespDto{" +
                "loanBal='" + loanBal + '\'' +
                '}';
    }
}
