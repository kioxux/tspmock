package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 终本案件信息
 */
@JsonPropertyOrder(alphabetic = true)
public class FINALCASE implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CARDNUM")
    private String CARDNUM;//	身份证号码/组织机构代码
    @JsonProperty(value = "CASECODE")
    private String CASECODE;//	案号
    @JsonProperty(value = "COURTNAME")
    private String COURTNAME;//	执行法院
    @JsonProperty(value = "EXECMONEY")
    private String EXECMONEY;//	执行标的
    @JsonProperty(value = "FINALDATE")
    private String FINALDATE;//	终本日期
    @JsonProperty(value = "INAME")
    private String INAME;//	被执行人姓名/名称
    @JsonProperty(value = "REGDATE")
    private String REGDATE;//	立案时间
    @JsonProperty(value = "UNPERFMONEY")
    private String UNPERFMONEY;//	未履行金额

    @JsonIgnore
    public String getCARDNUM() {
        return CARDNUM;
    }

    @JsonIgnore
    public void setCARDNUM(String CARDNUM) {
        this.CARDNUM = CARDNUM;
    }

    @JsonIgnore
    public String getCASECODE() {
        return CASECODE;
    }

    @JsonIgnore
    public void setCASECODE(String CASECODE) {
        this.CASECODE = CASECODE;
    }

    @JsonIgnore
    public String getCOURTNAME() {
        return COURTNAME;
    }

    @JsonIgnore
    public void setCOURTNAME(String COURTNAME) {
        this.COURTNAME = COURTNAME;
    }

    @JsonIgnore
    public String getEXECMONEY() {
        return EXECMONEY;
    }

    @JsonIgnore
    public void setEXECMONEY(String EXECMONEY) {
        this.EXECMONEY = EXECMONEY;
    }

    @JsonIgnore
    public String getFINALDATE() {
        return FINALDATE;
    }

    @JsonIgnore
    public void setFINALDATE(String FINALDATE) {
        this.FINALDATE = FINALDATE;
    }

    @JsonIgnore
    public String getINAME() {
        return INAME;
    }

    @JsonIgnore
    public void setINAME(String INAME) {
        this.INAME = INAME;
    }

    @JsonIgnore
    public String getREGDATE() {
        return REGDATE;
    }

    @JsonIgnore
    public void setREGDATE(String REGDATE) {
        this.REGDATE = REGDATE;
    }

    @JsonIgnore
    public String getUNPERFMONEY() {
        return UNPERFMONEY;
    }

    @JsonIgnore
    public void setUNPERFMONEY(String UNPERFMONEY) {
        this.UNPERFMONEY = UNPERFMONEY;
    }

    @Override
    public String toString() {
        return "FINALCASE{" +
                "CARDNUM='" + CARDNUM + '\'' +
                ", CASECODE='" + CASECODE + '\'' +
                ", COURTNAME='" + COURTNAME + '\'' +
                ", EXECMONEY='" + EXECMONEY + '\'' +
                ", FINALDATE='" + FINALDATE + '\'' +
                ", INAME='" + INAME + '\'' +
                ", REGDATE='" + REGDATE + '\'' +
                ", UNPERFMONEY='" + UNPERFMONEY + '\'' +
                '}';
    }
}
