package cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：批量结果确认处理
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Mbt952RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cplWjplxxOut")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.CplWjplxxOut> cplWjplxxOut;

    public List<CplWjplxxOut> getCplWjplxxOut() {
        return cplWjplxxOut;
    }

    public void setCplWjplxxOut(List<CplWjplxxOut> cplWjplxxOut) {
        this.cplWjplxxOut = cplWjplxxOut;
    }

    @Override
    public String toString() {
        return "Mbt952RespDto{" +
                "cplWjplxxOut=" + cplWjplxxOut +
                '}';
    }
}
