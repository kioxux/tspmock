package cn.com.yusys.yusp.dto.server.biz.xdsx0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 10:19:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 10:19
 * @since 2021/5/19 10:19
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "credit_no")
    private String credit_no;//证件号

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCredit_no() {
        return credit_no;
    }

    public void setCredit_no(String credit_no) {
        this.credit_no = credit_no;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cus_id='" + cus_id + '\'' +
                ", credit_no='" + credit_no + '\'' +
                '}';
    }
}
