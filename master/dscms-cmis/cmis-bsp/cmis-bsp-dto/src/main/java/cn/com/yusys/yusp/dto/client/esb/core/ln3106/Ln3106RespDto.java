package cn.com.yusys.yusp.dto.client.esb.core.ln3106;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款利率变更明细查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3106RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "listllbg")
    private List<Listllbg> listllbg;//贷款利率变更明细查询


    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List<Listllbg> getListllbg() {
        return listllbg;
    }

    public void setListllbg(List<Listllbg> listllbg) {
        this.listllbg = listllbg;
    }

    @Override
    public String toString() {
        return "Ln3106RespDto{" +
                ", zongbish=" + zongbish +
                ", listllbg=" + listllbg +
                '}';
    }
}
