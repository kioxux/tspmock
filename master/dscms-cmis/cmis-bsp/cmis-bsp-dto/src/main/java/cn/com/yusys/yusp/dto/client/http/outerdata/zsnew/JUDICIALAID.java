package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 司法协助基本信息
 */
@JsonPropertyOrder(alphabetic = true)
public class JUDICIALAID implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "COURTNAME")
    private String COURTNAME;//	执行法院
    @JsonProperty(value = "COURTNO")
    private String COURTNO;//	协助公示通知书文号
    @JsonProperty(value = "CUR")
    private String CUR;//	币种
    @JsonProperty(value = "INAME")
    private String INAME;//	被执行人
    @JsonProperty(value = "MODIFYID")
    private String MODIFYID;//	股权变更ID
    @JsonProperty(value = "PARENT_ID")
    private String PARENT_ID;//	被执行人ID
    @JsonProperty(value = "SHAREAM")
    private String SHAREAM;//	股权数额
    @JsonProperty(value = "STATUS")
    private String STATUS;//	股权冻结状态

    @JsonIgnore
    public String getCOURTNAME() {
        return COURTNAME;
    }

    @JsonIgnore
    public void setCOURTNAME(String COURTNAME) {
        this.COURTNAME = COURTNAME;
    }

    @JsonIgnore
    public String getCOURTNO() {
        return COURTNO;
    }

    @JsonIgnore
    public void setCOURTNO(String COURTNO) {
        this.COURTNO = COURTNO;
    }

    @JsonIgnore
    public String getCUR() {
        return CUR;
    }

    @JsonIgnore
    public void setCUR(String CUR) {
        this.CUR = CUR;
    }

    @JsonIgnore
    public String getINAME() {
        return INAME;
    }

    @JsonIgnore
    public void setINAME(String INAME) {
        this.INAME = INAME;
    }

    @JsonIgnore
    public String getMODIFYID() {
        return MODIFYID;
    }

    @JsonIgnore
    public void setMODIFYID(String MODIFYID) {
        this.MODIFYID = MODIFYID;
    }

    @JsonIgnore
    public String getPARENT_ID() {
        return PARENT_ID;
    }

    @JsonIgnore
    public void setPARENT_ID(String PARENT_ID) {
        this.PARENT_ID = PARENT_ID;
    }

    @JsonIgnore
    public String getSHAREAM() {
        return SHAREAM;
    }

    @JsonIgnore
    public void setSHAREAM(String SHAREAM) {
        this.SHAREAM = SHAREAM;
    }

    @JsonIgnore
    public String getSTATUS() {
        return STATUS;
    }

    @JsonIgnore
    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    @Override
    public String toString() {
        return "JUDICIALAID{" +
                "COURTNAME='" + COURTNAME + '\'' +
                ", COURTNO='" + COURTNO + '\'' +
                ", CUR='" + CUR + '\'' +
                ", INAME='" + INAME + '\'' +
                ", MODIFYID='" + MODIFYID + '\'' +
                ", PARENT_ID='" + PARENT_ID + '\'' +
                ", SHAREAM='" + SHAREAM + '\'' +
                ", STATUS='" + STATUS + '\'' +
                '}';
    }
}
