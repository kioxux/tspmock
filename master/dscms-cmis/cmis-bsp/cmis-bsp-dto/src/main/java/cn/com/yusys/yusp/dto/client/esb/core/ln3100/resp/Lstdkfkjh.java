package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款账户放款计划
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkfkjh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "xuhaoooo")
    private Integer xuhaoooo;//序号
    @JsonProperty(value = "fangkzhl")
    private String fangkzhl;//放款种类
    @JsonProperty(value = "shchriqi")
    private String shchriqi;//生成日期
    @JsonProperty(value = "fkriqiii")
    private String fkriqiii;//放款日期
    @JsonProperty(value = "fkjineee")
    private BigDecimal fkjineee;//放款金额
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "dkrzhzxh")
    private String dkrzhzxh;//贷款入账账号子序号
    @JsonProperty(value = "chulizht")
    private String chulizht;//处理状态

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getFangkzhl() {
        return fangkzhl;
    }

    public void setFangkzhl(String fangkzhl) {
        this.fangkzhl = fangkzhl;
    }

    public String getShchriqi() {
        return shchriqi;
    }

    public void setShchriqi(String shchriqi) {
        this.shchriqi = shchriqi;
    }

    public String getFkriqiii() {
        return fkriqiii;
    }

    public void setFkriqiii(String fkriqiii) {
        this.fkriqiii = fkriqiii;
    }

    public BigDecimal getFkjineee() {
        return fkjineee;
    }

    public void setFkjineee(BigDecimal fkjineee) {
        this.fkjineee = fkjineee;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public String getChulizht() {
        return chulizht;
    }

    public void setChulizht(String chulizht) {
        this.chulizht = chulizht;
    }

    @Override
    public String toString() {
        return "LstdkfkjhRespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "xuhaoooo='" + xuhaoooo + '\'' +
                "fangkzhl='" + fangkzhl + '\'' +
                "shchriqi='" + shchriqi + '\'' +
                "fkriqiii='" + fkriqiii + '\'' +
                "fkjineee='" + fkjineee + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "chulizht='" + chulizht + '\'' +
                '}';
    }
}  
