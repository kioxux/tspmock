package cn.com.yusys.yusp.dto.server.cfg.xdsx0009.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：客户准入级别同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0009RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdsx0009RespDto{" +
                "data=" + data +
                '}';
    }
}