package cn.com.yusys.yusp.dto.server.biz.xdxw0016.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：根据客户号查询查询统一管控额度接口（总额度）
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LmtList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "apprSubSerno")
    private String apprSubSerno;//额度分项流水号
    @JsonProperty(value = "lmtBizType")
    private String lmtBizType;//授信品种编号
    @JsonProperty(value = "lmtBizTypeName")
    private String lmtBizTypeName;//授信品种名称
    @JsonProperty(value = "spacAmt")
    private BigDecimal spacAmt;//授信敞口金额
    @JsonProperty(value = "spacOutstandAmt")
    private BigDecimal spacOutstandAmt;//授信敞口已用
    @JsonProperty(value = "guarMode")
    private String guarMode;//担保方式
    @JsonProperty(value = "avlAmt")
    private BigDecimal avlAmt;//授信总额
    @JsonProperty(value = "outstandAmt")
    private BigDecimal outstandAmt;//授信总额已用
    @JsonProperty(value = "isRevolv")
    private String isRevolv;//是否循环
    @JsonProperty(value = "startDate")
    private String startDate;//起始日期
    @JsonProperty(value = "endDate")
    private String endDate;//到期日期

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getApprSubSerno() {
        return apprSubSerno;
    }

    public void setApprSubSerno(String apprSubSerno) {
        this.apprSubSerno = apprSubSerno;
    }

    public String getLmtBizType() {
        return lmtBizType;
    }

    public void setLmtBizType(String lmtBizType) {
        this.lmtBizType = lmtBizType;
    }

    public String getLmtBizTypeName() {
        return lmtBizTypeName;
    }

    public void setLmtBizTypeName(String lmtBizTypeName) {
        this.lmtBizTypeName = lmtBizTypeName;
    }

    public BigDecimal getSpacAmt() {
        return spacAmt;
    }

    public void setSpacAmt(BigDecimal spacAmt) {
        this.spacAmt = spacAmt;
    }

    public BigDecimal getSpacOutstandAmt() {
        return spacOutstandAmt;
    }

    public void setSpacOutstandAmt(BigDecimal spacOutstandAmt) {
        this.spacOutstandAmt = spacOutstandAmt;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public BigDecimal getAvlAmt() {
        return avlAmt;
    }

    public void setAvlAmt(BigDecimal avlAmt) {
        this.avlAmt = avlAmt;
    }

    public BigDecimal getOutstandAmt() {
        return outstandAmt;
    }

    public void setOutstandAmt(BigDecimal outstandAmt) {
        this.outstandAmt = outstandAmt;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "LmtList{" +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "apprSubSerno='" + apprSubSerno + '\'' +
                "lmtBizType='" + lmtBizType + '\'' +
                "lmtBizTypeName='" + lmtBizTypeName + '\'' +
                "spacAmt='" + spacAmt + '\'' +
                "spacOutstandAmt='" + spacOutstandAmt + '\'' +
                "guarMode='" + guarMode + '\'' +
                "avlAmt='" + avlAmt + '\'' +
                "outstandAmt='" + outstandAmt + '\'' +
                "isRevolv='" + isRevolv + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                '}';
    }
}
