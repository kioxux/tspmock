package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 客户信息(CustomerInfo)
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
@JsonPropertyOrder(alphabetic = true)
public class CustomerInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id; // 客户号
    @JsonProperty(value = "cus_name")
    private String cus_name; // 客户名称
    @JsonProperty(value = "cus_type")
    private String cus_type; // 客户类型
    @JsonProperty(value = "cert_type")
    private String cert_type; // 证件类型
    @JsonProperty(value = "cert_code")
    private String cert_code; // 证件号码
    @JsonProperty(value = "bus_owner")
    private String bus_owner; // 企业所有制
    @JsonProperty(value = "new_industry_type")
    private String new_industry_type; // 所属国标行业
    @JsonProperty(value = "bas_acc_flg")
    private String bas_acc_flg; // 基本户是否在本行
    @JsonProperty(value = "assets")
    private String assets; // 资产负债率
    @JsonProperty(value = "grade")
    private String grade; // 本行即期信用等级
    @JsonProperty(value = "reg_area_code")
    private String reg_area_code; // 注册地行政区划代码
    @JsonProperty(value = "reg_area_name")
    private String reg_area_name; // 注册地行政区划名称
    @JsonProperty(value = "cust_mgr")
    private String cust_mgr; // 主管客户经理
    @JsonProperty(value = "main_br_id")
    private String main_br_id; // 主管机构

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getBus_owner() {
        return bus_owner;
    }

    public void setBus_owner(String bus_owner) {
        this.bus_owner = bus_owner;
    }

    public String getNew_industry_type() {
        return new_industry_type;
    }

    public void setNew_industry_type(String new_industry_type) {
        this.new_industry_type = new_industry_type;
    }

    public String getBas_acc_flg() {
        return bas_acc_flg;
    }

    public void setBas_acc_flg(String bas_acc_flg) {
        this.bas_acc_flg = bas_acc_flg;
    }

    public String getAssets() {
        return assets;
    }

    public void setAssets(String assets) {
        this.assets = assets;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getReg_area_code() {
        return reg_area_code;
    }

    public void setReg_area_code(String reg_area_code) {
        this.reg_area_code = reg_area_code;
    }

    public String getReg_area_name() {
        return reg_area_name;
    }

    public void setReg_area_name(String reg_area_name) {
        this.reg_area_name = reg_area_name;
    }

    public String getCust_mgr() {
        return cust_mgr;
    }

    public void setCust_mgr(String cust_mgr) {
        this.cust_mgr = cust_mgr;
    }

    public String getMain_br_id() {
        return main_br_id;
    }

    public void setMain_br_id(String main_br_id) {
        this.main_br_id = main_br_id;
    }

    @Override
    public String toString() {
        return "CustomerInfo{" +
                "cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cus_type='" + cus_type + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", bus_owner='" + bus_owner + '\'' +
                ", new_industry_type='" + new_industry_type + '\'' +
                ", bas_acc_flg='" + bas_acc_flg + '\'' +
                ", assets=" + assets +
                ", grade='" + grade + '\'' +
                ", reg_area_code='" + reg_area_code + '\'' +
                ", reg_area_name='" + reg_area_name + '\'' +
                ", cust_mgr='" + cust_mgr + '\'' +
                ", main_br_id='" + main_br_id + '\'' +
                '}';
    }
}
