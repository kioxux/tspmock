package cn.com.yusys.yusp.dto.server.oca.xdxt0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 17:23:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 17:23
 * @since 2021/5/24 17:23
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "deptChiefId")
    private String deptChiefId;//分中心负责人工号
    @JsonProperty(value = "startPageNum")
    private Integer startPageNum;//开始页数
    @JsonProperty(value = "pageSize")
    private Integer pageSize;//条数

    public String getDeptChiefId() {
        return deptChiefId;
    }

    public void setDeptChiefId(String deptChiefId) {
        this.deptChiefId = deptChiefId;
    }

    public Integer getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(Integer startPageNum) {
        this.startPageNum = startPageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Xdxt0010ReqDto{" +
                "deptChiefId='" + deptChiefId + '\'' +
                "startPageNum='" + startPageNum + '\'' +
                "pageSize='" + pageSize + '\'' +
                '}';
    }
}
