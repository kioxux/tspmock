package cn.com.yusys.yusp.dto.client.esb.ypxt.coninf;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求DTO：信贷担保合同信息同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class ConinfReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "coninfListInfo")
    private List<ConinfListInfo> coninfListInfo;

    public List<ConinfListInfo> getConinfListInfo() {
        return coninfListInfo;
    }

    public void setConinfListInfo(List<ConinfListInfo> coninfListInfo) {
        this.coninfListInfo = coninfListInfo;
    }

    @Override
    public String toString() {
        return "ConinfReqDto{" +
                "coninfListInfo=" + coninfListInfo +
                '}';
    }
}
