package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：承兑签发审批结果综合服务接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj004RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erortx")
    private String erortx;//交易返回信息
    @JsonProperty(value = "retcod")
    private String retcod;//交易返回代码

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getRetcod() {
        return retcod;
    }

    public void setRetcod(String retcod) {
        this.retcod = retcod;
    }

    @Override
    public String toString() {
        return "Xdpj004RespDto{" +
                "erortx='" + erortx + '\'' +
                "retcod='" + retcod + '\'' +
                '}';
    }
}  
