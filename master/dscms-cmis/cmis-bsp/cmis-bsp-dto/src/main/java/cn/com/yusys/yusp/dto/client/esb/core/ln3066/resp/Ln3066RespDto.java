package cn.com.yusys.yusp.dto.client.esb.core.ln3066.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：资产转让借据筛选
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3066RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "results")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3066.resp.Results> results;

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "Ln3066RespDto{" +
                "results=" + results +
                '}';
    }
}
