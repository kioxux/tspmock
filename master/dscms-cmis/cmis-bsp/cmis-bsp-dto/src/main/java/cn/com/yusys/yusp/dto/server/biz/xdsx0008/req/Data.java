package cn.com.yusys.yusp.dto.server.biz.xdsx0008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：单一客户人工限额同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "singleCusQuota")
    private BigDecimal singleCusQuota;//单一客户人工限额
    @JsonProperty(value = "dtghFlag")
    private Integer dtghFlag;//区分标识

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public BigDecimal getSingleCusQuota() {
        return singleCusQuota;
    }

    public void setSingleCusQuota(BigDecimal singleCusQuota) {
        this.singleCusQuota = singleCusQuota;
    }

    public Integer getDtghFlag() {
        return dtghFlag;
    }

    public void setDtghFlag(Integer dtghFlag) {
        this.dtghFlag = dtghFlag;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                "singleCusQuota='" + singleCusQuota + '\'' +
                "dtghFlag='" + dtghFlag + '\'' +
                '}';
    }
}
