package cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：获取抵押信息详情
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "items")
    private java.util.List<Items> items;

    public java.util.List<Items> getItems() {
        return items;
    }

    public void setItems(java.util.List<Items> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Data{" +
                "items=" + items +
                '}';
    }
}
