package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//个人多少天内最新本地征信
@JsonPropertyOrder(alphabetic = true)
public class R010 implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty("REPORTID")
    private String REPORTID;// 报告编号
    @JsonProperty("REQID")
    private String REQID;// 请求业务号
    @JsonProperty("QUERYTIME")
    private String QUERYTIME;// 查询时间
    @JsonProperty("SOURCE")
    private String SOURCE;// 报告来源
    @JsonProperty("ARCHIVECREATEDATE")
    private String ARCHIVECREATEDATE; // 授权日期
    @JsonProperty("CREDITDOCID")
    private String CREDITDOCID; // 影像编号
    @JsonIgnore
    public String getARCHIVECREATEDATE() {
        return ARCHIVECREATEDATE;
    }
    @JsonIgnore
    public void setARCHIVECREATEDATE(String ARCHIVECREATEDATE) {
        this.ARCHIVECREATEDATE = ARCHIVECREATEDATE;
    }
    @JsonIgnore
    public String getCREDITDOCID() {
        return CREDITDOCID;
    }
    @JsonIgnore
    public void setCREDITDOCID(String CREDITDOCID) {
        this.CREDITDOCID = CREDITDOCID;
    }


    @JsonIgnore
    public String getREPORTID() {
        return REPORTID;
    }

    @JsonIgnore
    public void setREPORTID(String REPORTID) {
        this.REPORTID = REPORTID;
    }

    @JsonIgnore
    public String getREQID() {
        return REQID;
    }

    @JsonIgnore
    public void setREQID(String REQID) {
        this.REQID = REQID;
    }

    @JsonIgnore
    public String getQUERYTIME() {
        return QUERYTIME;
    }

    @JsonIgnore
    public void setQUERYTIME(String QUERYTIME) {
        this.QUERYTIME = QUERYTIME;
    }

    @JsonIgnore
    public String getSOURCE() {
        return SOURCE;
    }

    @JsonIgnore
    public void setSOURCE(String SOURCE) {
        this.SOURCE = SOURCE;
    }

    @Override
    public String toString() {
        return "R009{" +
                "REPORTID='" + REPORTID + '\'' +
                ", REQID='" + REQID + '\'' +
                ", QUERYTIME='" + QUERYTIME + '\'' +
                ", SOURCE='" + SOURCE + '\'' +
                ", ARCHIVECREATEDATE='" + ARCHIVECREATEDATE + '\'' +
                ", CREDITDOCID='" + CREDITDOCID + '\'' +
                '}';
    }
}
