package cn.com.yusys.yusp.dto.client.esb.doc.doc006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：申请出库
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Doc006ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pkbappid")
    private String pkbappid;//借阅申请id
    @JsonProperty(value = "inroomtime")
    private String inroomtime;//归还时间
    @JsonProperty(value = "rqtem1")
    private String rqtem1;//请求备用字段1
    @JsonProperty(value = "rqtem2")
    private String rqtem2;//请求备用字段2
    @JsonProperty(value = "rqtem3")
    private String rqtem3;//请求备用字段3
    @JsonProperty(value = "rqtem4")
    private String rqtem4;//请求备用字段4

    public String getPkbappid() {
        return pkbappid;
    }

    public void setPkbappid(String pkbappid) {
        this.pkbappid = pkbappid;
    }

    public String getInroomtime() {
        return inroomtime;
    }

    public void setInroomtime(String inroomtime) {
        this.inroomtime = inroomtime;
    }

    public String getRqtem1() {
        return rqtem1;
    }

    public void setRqtem1(String rqtem1) {
        this.rqtem1 = rqtem1;
    }

    public String getRqtem2() {
        return rqtem2;
    }

    public void setRqtem2(String rqtem2) {
        this.rqtem2 = rqtem2;
    }

    public String getRqtem3() {
        return rqtem3;
    }

    public void setRqtem3(String rqtem3) {
        this.rqtem3 = rqtem3;
    }

    public String getRqtem4() {
        return rqtem4;
    }

    public void setRqtem4(String rqtem4) {
        this.rqtem4 = rqtem4;
    }

    @Override
    public String toString() {
        return "Doc006ReqDto{" +
                "pkbappid='" + pkbappid + '\'' +
                "inroomtime='" + inroomtime + '\'' +
                "rqtem1='" + rqtem1 + '\'' +
                "rqtem2='" + rqtem2 + '\'' +
                "rqtem3='" + rqtem3 + '\'' +
                "rqtem4='" + rqtem4 + '\'' +
                '}';
    }
}  
