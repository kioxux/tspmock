package cn.com.yusys.yusp.dto.server.biz.xddb0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 10:06
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class RegisterOwnerList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ownnam")
    private String ownnam;//登记簿权利人信息-权利人姓名
    @JsonProperty(value = "owdonu")
    private String owdonu;//登记簿权利人信息-权利人证件号码
    @JsonProperty(value = "owdoty")
    private String owdoty;//登记簿权利人信息-权利人证件类型
    @JsonProperty(value = "owcenu")
    private String owcenu;//登记簿权利人信息-不动产权证号
    @JsonProperty(value = "ooshmo")
    private String ooshmo;//登记簿权利人信息-共有方式
    @JsonProperty(value = "ooshin")
    private String ooshin;//登记簿权利人信息-共有情况
    @JsonProperty(value = "ooshpr")
    private String ooshpr;//登记簿权利人信息-权利比例
    @JsonProperty(value = "ohcemo")
    private String ohcemo;//登记簿权利人信息-持证方式

    public String getOwnnam() {
        return ownnam;
    }

    public void setOwnnam(String ownnam) {
        this.ownnam = ownnam;
    }

    public String getOwdonu() {
        return owdonu;
    }

    public void setOwdonu(String owdonu) {
        this.owdonu = owdonu;
    }

    public String getOwdoty() {
        return owdoty;
    }

    public void setOwdoty(String owdoty) {
        this.owdoty = owdoty;
    }

    public String getOwcenu() {
        return owcenu;
    }

    public void setOwcenu(String owcenu) {
        this.owcenu = owcenu;
    }

    public String getOoshmo() {
        return ooshmo;
    }

    public void setOoshmo(String ooshmo) {
        this.ooshmo = ooshmo;
    }

    public String getOoshin() {
        return ooshin;
    }

    public void setOoshin(String ooshin) {
        this.ooshin = ooshin;
    }

    public String getOoshpr() {
        return ooshpr;
    }

    public void setOoshpr(String ooshpr) {
        this.ooshpr = ooshpr;
    }

    public String getOhcemo() {
        return ohcemo;
    }

    public void setOhcemo(String ohcemo) {
        this.ohcemo = ohcemo;
    }

    @Override
    public String toString() {
        return "RegisterOwnerList{" +
                "ownnam='" + ownnam + '\'' +
                ", owdonu='" + owdonu + '\'' +
                ", owdoty='" + owdoty + '\'' +
                ", owcenu='" + owcenu + '\'' +
                ", ooshmo='" + ooshmo + '\'' +
                ", ooshin='" + ooshin + '\'' +
                ", ooshpr='" + ooshpr + '\'' +
                ", ohcemo='" + ohcemo + '\'' +
                '}';
    }
}
