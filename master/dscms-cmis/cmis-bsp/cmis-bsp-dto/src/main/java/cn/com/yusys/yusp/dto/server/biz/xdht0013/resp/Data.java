package cn.com.yusys.yusp.dto.server.biz.xdht0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 15:08
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "bailAcct")
    private String bailAcct;//保证金账户
    @JsonProperty(value = "contCnNo")
    private String contCnNo;//合同中文号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "bizName")
    private String bizName;//业务品种分类
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//借款金额
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//借款余额
    @JsonProperty(value = "bailRate")
    private String bailRate;//保证金比例
    @JsonProperty(value = "guarTypeName")
    private String guarTypeName;//担保方式名称
    @JsonProperty(value = "exchangeRate")
    private String exchangeRate;//汇率
    @JsonProperty(value = "billStartDate")
    private String billStartDate;//借据开始日期
    @JsonProperty(value = "billEndDate")
    private String billEndDate;//借据结束日期
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "entruPayAcctName")
    private String entruPayAcctName;//受托支付账户名
    @JsonProperty(value = "entruPayNo")
    private String entruPayNo;//受托支付号码
    @JsonProperty(value = "entruPayBank")
    private String entruPayBank;//受托银行
    @JsonProperty(value = "orgName")
    private String orgName;//机构名
    @JsonProperty(value = "rate")
    private BigDecimal rate;//利率
    @JsonProperty(value = "term")
    private String term;//期限
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//贷款用途
    @JsonProperty(value = "huser")
    private String huser;//经办人

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBailAcct() {
        return bailAcct;
    }

    public void setBailAcct(String bailAcct) {
        this.bailAcct = bailAcct;
    }

    public String getContCnNo() {
        return contCnNo;
    }

    public void setContCnNo(String contCnNo) {
        this.contCnNo = contCnNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getBizName() {
        return bizName;
    }

    public void setBizName(String bizName) {
        this.bizName = bizName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getBailRate() {
        return bailRate;
    }

    public void setBailRate(String bailRate) {
        this.bailRate = bailRate;
    }

    public String getGuarTypeName() {
        return guarTypeName;
    }

    public void setGuarTypeName(String guarTypeName) {
        this.guarTypeName = guarTypeName;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getBillStartDate() {
        return billStartDate;
    }

    public void setBillStartDate(String billStartDate) {
        this.billStartDate = billStartDate;
    }

    public String getBillEndDate() {
        return billEndDate;
    }

    public void setBillEndDate(String billEndDate) {
        this.billEndDate = billEndDate;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getEntruPayAcctName() {
        return entruPayAcctName;
    }

    public void setEntruPayAcctName(String entruPayAcctName) {
        this.entruPayAcctName = entruPayAcctName;
    }

    public String getEntruPayNo() {
        return entruPayNo;
    }

    public void setEntruPayNo(String entruPayNo) {
        this.entruPayNo = entruPayNo;
    }

    public String getEntruPayBank() {
        return entruPayBank;
    }

    public void setEntruPayBank(String entruPayBank) {
        this.entruPayBank = entruPayBank;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getHuser() {
        return huser;
    }

    public void setHuser(String huser) {
        this.huser = huser;
    }

    @Override
    public String toString() {
        return "Xdht0013RespDto{" +
                "contNo='" + contNo + '\'' +
                "serno='" + serno + '\'' +
                "bailAcct='" + bailAcct + '\'' +
                "contCnNo='" + contCnNo + '\'' +
                "contType='" + contType + '\'' +
                "bizName='" + bizName + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "curType='" + curType + '\'' +
                "loanAmt='" + loanAmt + '\'' +
                "loanBalance='" + loanBalance + '\'' +
                "bailRate='" + bailRate + '\'' +
                "guarTypeName='" + guarTypeName + '\'' +
                "exchangeRate='" + exchangeRate + '\'' +
                "billStartDate='" + billStartDate + '\'' +
                "billEndDate='" + billEndDate + '\'' +
                "inputId='" + inputId + '\'' +
                "managerId='" + managerId + '\'' +
                "entruPayAcctName='" + entruPayAcctName + '\'' +
                "entruPayNo='" + entruPayNo + '\'' +
                "entruPayBank='" + entruPayBank + '\'' +
                "orgName='" + orgName + '\'' +
                "rate='" + rate + '\'' +
                "term='" + term + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "huser='" + huser + '\'' +
                '}';
    }
}
