package cn.com.yusys.yusp.dto.client.http.hyy.bare01.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：押品状态查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Bare01ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "district_code")
    private String district_code;//区县代码
    @JsonProperty(value = "certificate_number")
    private String certificate_number;//不动产权证书号
    @JsonProperty(value = "mortgage_ certificate_number")
    private String mortgage_certificate_number;//不动产登记证明号

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getDistrict_code() {
        return district_code;
    }

    public void setDistrict_code(String district_code) {
        this.district_code = district_code;
    }

    public String getCertificate_number() {
        return certificate_number;
    }

    public void setCertificate_number(String certificate_number) {
        this.certificate_number = certificate_number;
    }

    public String getMortgage_certificate_number() {
        return mortgage_certificate_number;
    }

    public void setMortgage_certificate_number(String mortgage_certificate_number) {
        this.mortgage_certificate_number = mortgage_certificate_number;
    }

    @Override
    public String toString() {
        return "Bare01ReqDto{" +
                "district_code='" + district_code + '\'' +
                ", certificate_number='" + certificate_number + '\'' +
                ", mortgage_certificate_number='" + mortgage_certificate_number + '\'' +
                '}';
    }
}
