package cn.com.yusys.yusp.dto.client.esb.core.co3225.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/31 16:15
 * @since 2021/5/31 16:15
 */
public class Lstdzywmx {
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "dzywbhao")
    private String dzywbhao;//抵质押物编号
    @JsonProperty(value = "dzywminc")
    private String dzywminc;//抵质押物名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "dizyfshi")
    private String dizyfshi;//抵质押方式
    @JsonProperty(value = "dzywzlei")
    private String dzywzlei;//抵质押物种类
    @JsonProperty(value = "syrkhhao")
    private String syrkhhao;//受益人客户号
    @JsonProperty(value = "syrkhmin")
    private String syrkhmin;//受益人客户名
    @JsonProperty(value = "syqrkehh")
    private String syqrkehh;//所有权人客户号
    @JsonProperty(value = "syqrkehm")
    private String syqrkehm;//所有权人客户名
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "minyjiaz")
    private BigDecimal minyjiaz;//名义价值
    @JsonProperty(value = "shijjiaz")
    private BigDecimal shijjiaz;//实际价值
    @JsonProperty(value = "dizybilv")
    private BigDecimal dizybilv;//抵质押比率
    @JsonProperty(value = "keyongje")
    private BigDecimal keyongje;//可用金额
    @JsonProperty(value = "yiyongje")
    private BigDecimal yiyongje;//已用金额
    @JsonProperty(value = "shengxrq")
    private String shengxrq;//生效日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dzywztai")
    private String dzywztai;//抵质押物状态
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "mingxixh")
    private Integer mingxixh;//明细序号
    @JsonProperty(value = "jiaoyisj")
    private String jiaoyisj;//交易事件
    @JsonProperty(value = "shijshum")
    private String shijshum;//交易事件说明
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要
    @JsonProperty(value = "weihguiy")
    private String weihguiy;//维护柜员
    @JsonProperty(value = "weihjigo")
    private String weihjigo;//维护机构
    @JsonProperty(value = "weihriqi")
    private String weihriqi;//维护日期
    @JsonProperty(value = "shijchuo")
    private String shijchuo;//时间戳
    @JsonProperty(value = "jiluztai")
    private String jiluztai;//记录状态
    @JsonProperty(value = "glywbhao")
    private String glywbhao;//关联业务编号
    @JsonProperty(value = "huowuhth")
    private String huowuhth;//货物合同号
    @JsonProperty(value = "yewusx01")
    private String yewusx01;//记账余额属性
    @JsonProperty(value = "yewmingc")
    private String yewmingc;//业务名称

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDzywminc() {
        return dzywminc;
    }

    public void setDzywminc(String dzywminc) {
        this.dzywminc = dzywminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getDizyfshi() {
        return dizyfshi;
    }

    public void setDizyfshi(String dizyfshi) {
        this.dizyfshi = dizyfshi;
    }

    public String getDzywzlei() {
        return dzywzlei;
    }

    public void setDzywzlei(String dzywzlei) {
        this.dzywzlei = dzywzlei;
    }

    public String getSyrkhhao() {
        return syrkhhao;
    }

    public void setSyrkhhao(String syrkhhao) {
        this.syrkhhao = syrkhhao;
    }

    public String getSyrkhmin() {
        return syrkhmin;
    }

    public void setSyrkhmin(String syrkhmin) {
        this.syrkhmin = syrkhmin;
    }

    public String getSyqrkehh() {
        return syqrkehh;
    }

    public void setSyqrkehh(String syqrkehh) {
        this.syqrkehh = syqrkehh;
    }

    public String getSyqrkehm() {
        return syqrkehm;
    }

    public void setSyqrkehm(String syqrkehm) {
        this.syqrkehm = syqrkehm;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getMinyjiaz() {
        return minyjiaz;
    }

    public void setMinyjiaz(BigDecimal minyjiaz) {
        this.minyjiaz = minyjiaz;
    }

    public BigDecimal getShijjiaz() {
        return shijjiaz;
    }

    public void setShijjiaz(BigDecimal shijjiaz) {
        this.shijjiaz = shijjiaz;
    }

    public BigDecimal getDizybilv() {
        return dizybilv;
    }

    public void setDizybilv(BigDecimal dizybilv) {
        this.dizybilv = dizybilv;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public BigDecimal getYiyongje() {
        return yiyongje;
    }

    public void setYiyongje(BigDecimal yiyongje) {
        this.yiyongje = yiyongje;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDzywztai() {
        return dzywztai;
    }

    public void setDzywztai(String dzywztai) {
        this.dzywztai = dzywztai;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public Integer getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Integer mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(String jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getShijshum() {
        return shijshum;
    }

    public void setShijshum(String shijshum) {
        this.shijshum = shijshum;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public String getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(String shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    public String getGlywbhao() {
        return glywbhao;
    }

    public void setGlywbhao(String glywbhao) {
        this.glywbhao = glywbhao;
    }

    public String getHuowuhth() {
        return huowuhth;
    }

    public void setHuowuhth(String huowuhth) {
        this.huowuhth = huowuhth;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getYewmingc() {
        return yewmingc;
    }

    public void setYewmingc(String yewmingc) {
        this.yewmingc = yewmingc;
    }

    @Override
    public String toString() {
        return "Co3225RespDto{" +
                "zongbish='" + zongbish + '\'' +
                "dzywbhao='" + dzywbhao + '\'' +
                "dzywminc='" + dzywminc + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "dizyfshi='" + dizyfshi + '\'' +
                "dzywzlei='" + dzywzlei + '\'' +
                "syrkhhao='" + syrkhhao + '\'' +
                "syrkhmin='" + syrkhmin + '\'' +
                "syqrkehh='" + syqrkehh + '\'' +
                "syqrkehm='" + syqrkehm + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "minyjiaz='" + minyjiaz + '\'' +
                "shijjiaz='" + shijjiaz + '\'' +
                "dizybilv='" + dizybilv + '\'' +
                "keyongje='" + keyongje + '\'' +
                "yiyongje='" + yiyongje + '\'' +
                "shengxrq='" + shengxrq + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "dzywztai='" + dzywztai + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "mingxixh='" + mingxixh + '\'' +
                "jiaoyisj='" + jiaoyisj + '\'' +
                "shijshum='" + shijshum + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "weihguiy='" + weihguiy + '\'' +
                "weihjigo='" + weihjigo + '\'' +
                "weihriqi='" + weihriqi + '\'' +
                "shijchuo='" + shijchuo + '\'' +
                "jiluztai='" + jiluztai + '\'' +
                "glywbhao='" + glywbhao + '\'' +
                "huowuhth='" + huowuhth + '\'' +
                "yewusx01='" + yewusx01 + '\'' +
                "yewmingc='" + yewmingc + '\'' +
                '}';
    }
}
