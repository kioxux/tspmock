package cn.com.yusys.yusp.dto.server.biz.xdtz0047.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/18 14:24:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/18 14:24
 * @since 2021/5/18 14:24
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "biz_type")
    private String biz_type;//产品编号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户编号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同编号
    @JsonProperty(value = "prd_name")
    private String prd_name;//产品名称
    @JsonProperty(value = "account_class")
    private String account_class;//科目号
    @JsonProperty(value = "bill_no")
    private String bill_no;//借据编号
    @JsonProperty(value = "account_status")
    private String account_status;//台帐状态
    @JsonProperty(value = "loan_amount")
    private BigDecimal loan_amount;//借据金额
    @JsonProperty(value = "loan_balance")
    private BigDecimal loan_balance;//借据余额
    @JsonProperty(value = "cla")
    private String cla;//五级分类标志
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//贷款起始日
    @JsonProperty(value = "first_disb_date")
    private String first_disb_date;//首次放款日期
    @JsonProperty(value = "ps_due_dt")
    private String ps_due_dt;//还款日期
    @JsonProperty(value = "cap_overdue_days")
    private String cap_overdue_days;//本金逾期天数
    @JsonProperty(value = "cap_overdue_date")
    private String cap_overdue_date;//本金逾期起始日期
    @JsonProperty(value = "overdue_balance")
    private BigDecimal overdue_balance;//逾期贷款余额(元)
    @JsonProperty(value = "int_overdue_date")
    private String int_overdue_date;//利息逾期起始日期
    @JsonProperty(value = "off_int_cumu")
    private BigDecimal off_int_cumu;//表外欠息
    @JsonProperty(value = "inner_int_cumu")
    private BigDecimal inner_int_cumu;//表内欠息
    @JsonProperty(value = "manager_br_id")
    private String manager_br_id;//管理机构
    @JsonProperty(value = "settl_date")
    private String settl_date;//结清日期
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//贷款到期日
    @JsonProperty(value = "fina_br_id")
    private String fina_br_id;//帐务机构

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getAccount_class() {
        return account_class;
    }

    public void setAccount_class(String account_class) {
        this.account_class = account_class;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public BigDecimal getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(BigDecimal loan_amount) {
        this.loan_amount = loan_amount;
    }

    public BigDecimal getLoan_balance() {
        return loan_balance;
    }

    public void setLoan_balance(BigDecimal loan_balance) {
        this.loan_balance = loan_balance;
    }

    public String getCla() {
        return cla;
    }

    public void setCla(String cla) {
        this.cla = cla;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getFirst_disb_date() {
        return first_disb_date;
    }

    public void setFirst_disb_date(String first_disb_date) {
        this.first_disb_date = first_disb_date;
    }

    public String getPs_due_dt() {
        return ps_due_dt;
    }

    public void setPs_due_dt(String ps_due_dt) {
        this.ps_due_dt = ps_due_dt;
    }

    public String getCap_overdue_days() {
        return cap_overdue_days;
    }

    public void setCap_overdue_days(String cap_overdue_days) {
        this.cap_overdue_days = cap_overdue_days;
    }

    public String getCap_overdue_date() {
        return cap_overdue_date;
    }

    public void setCap_overdue_date(String cap_overdue_date) {
        this.cap_overdue_date = cap_overdue_date;
    }

    public BigDecimal getOverdue_balance() {
        return overdue_balance;
    }

    public void setOverdue_balance(BigDecimal overdue_balance) {
        this.overdue_balance = overdue_balance;
    }

    public String getInt_overdue_date() {
        return int_overdue_date;
    }

    public void setInt_overdue_date(String int_overdue_date) {
        this.int_overdue_date = int_overdue_date;
    }

    public BigDecimal getOff_int_cumu() {
        return off_int_cumu;
    }

    public void setOff_int_cumu(BigDecimal off_int_cumu) {
        this.off_int_cumu = off_int_cumu;
    }

    public BigDecimal getInner_int_cumu() {
        return inner_int_cumu;
    }

    public void setInner_int_cumu(BigDecimal inner_int_cumu) {
        this.inner_int_cumu = inner_int_cumu;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getSettl_date() {
        return settl_date;
    }

    public void setSettl_date(String settl_date) {
        this.settl_date = settl_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getFina_br_id() {
        return fina_br_id;
    }

    public void setFina_br_id(String fina_br_id) {
        this.fina_br_id = fina_br_id;
    }

    @Override
    public String toString() {
        return "List{" +
                "biz_type='" + biz_type + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", account_class='" + account_class + '\'' +
                ", bill_no='" + bill_no + '\'' +
                ", account_status='" + account_status + '\'' +
                ", loan_amount=" + loan_amount +
                ", loan_balance=" + loan_balance +
                ", cla='" + cla + '\'' +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", first_disb_date='" + first_disb_date + '\'' +
                ", ps_due_dt='" + ps_due_dt + '\'' +
                ", cap_overdue_days='" + cap_overdue_days + '\'' +
                ", cap_overdue_date='" + cap_overdue_date + '\'' +
                ", overdue_balance=" + overdue_balance +
                ", int_overdue_date='" + int_overdue_date + '\'' +
                ", off_int_cumu=" + off_int_cumu +
                ", inner_int_cumu=" + inner_int_cumu +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", settl_date='" + settl_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", fina_br_id='" + fina_br_id + '\'' +
                '}';
    }
}
