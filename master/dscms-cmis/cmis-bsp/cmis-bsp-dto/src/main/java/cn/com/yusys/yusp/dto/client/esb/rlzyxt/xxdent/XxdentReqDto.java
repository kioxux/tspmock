package cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：新入职人员信息登记
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XxdentReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pagenm")
    private String pagenm;//第几页
    @JsonProperty(value = "pagecd")
    private String pagecd;//每页多少条
    @JsonProperty(value = "qydate")
    private String qydate;//查询日期

    public String getPagenm() {
        return pagenm;
    }

    public void setPagenm(String pagenm) {
        this.pagenm = pagenm;
    }

    public String getPagecd() {
        return pagecd;
    }

    public void setPagecd(String pagecd) {
        this.pagecd = pagecd;
    }

    public String getQydate() {
        return qydate;
    }

    public void setQydate(String qydate) {
        this.qydate = qydate;
    }

    @Override
    public String toString() {
        return "XxdentReqDto{" +
                "pagenm='" + pagenm + '\'' +
                "pagecd='" + pagecd + '\'' +
                "qydate='" + qydate + '\'' +
                '}';
    }
}  
