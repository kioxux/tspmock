package cn.com.yusys.yusp.dto.server.biz.xddb0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 20:38
 * @Version 1.0
 */
public class ReqList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "gagtyp")
    private String gagtyp;//押品分类
    @JsonProperty(value = "guarid")
    private String guarid;//押品统一编号
    @JsonProperty(value = "mangid")
    private String mangid;//客户经理编号
    @JsonProperty(value = "orgno")
    private String orgno;//所属机构
    @JsonProperty(value = "status")
    private String status;//任务状态
    @JsonProperty(value = "stime")
    private String stime;//生成任务时间
    @JsonProperty(value = "mark")
    private String mark;//任务描述

    public String getGagtyp() {
        return gagtyp;
    }

    public void setGagtyp(String gagtyp) {
        this.gagtyp = gagtyp;
    }

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public String getMangid() {
        return mangid;
    }

    public void setMangid(String mangid) {
        this.mangid = mangid;
    }

    public String getOrgno() {
        return orgno;
    }

    public void setOrgno(String orgno) {
        this.orgno = orgno;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "ReqList{" +
                "gagtyp='" + gagtyp + '\'' +
                ", guarid='" + guarid + '\'' +
                ", mangid='" + mangid + '\'' +
                ", orgno='" + orgno + '\'' +
                ", status='" + status + '\'' +
                ", stime='" + stime + '\'' +
                ", mark='" + mark + '\'' +
                '}';
    }
}
