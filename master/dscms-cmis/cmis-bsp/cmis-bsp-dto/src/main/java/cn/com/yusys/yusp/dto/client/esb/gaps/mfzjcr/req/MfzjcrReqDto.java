package cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：买方资金存入
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class MfzjcrReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ipaddr")
    private String ipaddr;//请求方IP
    @JsonProperty(value = "mac")
    private String mac;//请求方MAC
    @JsonProperty(value = "xybhsq")
    private String xybhsq;//协议编号
    @JsonProperty(value = "qydm")
    private String qydm;//区域代码
    @JsonProperty(value = "tranam")
    private BigDecimal tranam;//存款金额
    @JsonProperty(value = "zjxz")
    private String zjxz;//资金性质
    @JsonProperty(value = "ckrq")
    private String ckrq;//存款日期
    @JsonProperty(value = "acctno")
    private String acctno;//买方账号
    @JsonProperty(value = "acctsa")
    private String acctsa;//买方户名
    @JsonProperty(value = "acctba")
    private String acctba;//资金托管账号
    @JsonProperty(value = "dataid")
    private String dataid;//资金托管账户名
    @JsonProperty(value = "sfxz")
    private String sfxz;//是否销账标志

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getXybhsq() {
        return xybhsq;
    }

    public void setXybhsq(String xybhsq) {
        this.xybhsq = xybhsq;
    }

    public String getQydm() {
        return qydm;
    }

    public void setQydm(String qydm) {
        this.qydm = qydm;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getZjxz() {
        return zjxz;
    }

    public void setZjxz(String zjxz) {
        this.zjxz = zjxz;
    }

    public String getCkrq() {
        return ckrq;
    }

    public void setCkrq(String ckrq) {
        this.ckrq = ckrq;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctsa() {
        return acctsa;
    }

    public void setAcctsa(String acctsa) {
        this.acctsa = acctsa;
    }

    public String getAcctba() {
        return acctba;
    }

    public void setAcctba(String acctba) {
        this.acctba = acctba;
    }

    public String getDataid() {
        return dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    public String getSfxz() {
        return sfxz;
    }

    public void setSfxz(String sfxz) {
        this.sfxz = sfxz;
    }

    @Override
    public String toString() {
        return "MfzjcrReqDto{" +
                "ipaddr='" + ipaddr + '\'' +
                "mac='" + mac + '\'' +
                "xybhsq='" + xybhsq + '\'' +
                "qydm='" + qydm + '\'' +
                "tranam='" + tranam + '\'' +
                "zjxz='" + zjxz + '\'' +
                "ckrq='" + ckrq + '\'' +
                "acctno='" + acctno + '\'' +
                "acctsa='" + acctsa + '\'' +
                "acctba='" + acctba + '\'' +
                "dataid='" + dataid + '\'' +
                "sfxz='" + sfxz + '\'' +
                '}';
    }
}  
