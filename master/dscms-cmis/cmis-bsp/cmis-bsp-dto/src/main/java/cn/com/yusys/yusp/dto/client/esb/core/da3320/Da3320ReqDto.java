package cn.com.yusys.yusp.dto.client.esb.core.da3320;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3320ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "shifoudy")
    private String shifoudy;//是否打印

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    @Override
    public String toString() {
        return "Da3320ReqDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "shifoudy='" + shifoudy + '\'' +
                '}';
    }
}  
