package cn.com.yusys.yusp.dto.server.biz.xdtz0012.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/5/7 10:53
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHavingSpouse")
    private String isHavingSpouse;//是否有配偶
    @JsonProperty(value = "isHavingOperBillBal")
    private String isHavingOperBillBal;//经营性贷款是否存在余额
    @JsonProperty(value = "managerId")
    private String managerId;//归属客户经理工号
    @JsonProperty(value = "managerName")
    private String managerName;//归属客户经理姓名
    @JsonProperty(value = "orgNo")
    private String orgNo;//所在机构编号
    @JsonProperty(value = "orgName")
    private String orgName;//所在机构名称
    @JsonProperty(value = "billBal")
    private BigDecimal billBal;//经营性贷款余额
    @JsonProperty(value = "isCrdApprStatusPending")
    private String isCrdApprStatusPending;//授信审批状态是否“审批中”
    @JsonProperty(value = "isApprVaild")
    private String isApprVaild;//是否存在有效批复
    @JsonProperty(value = "isContVaild")
    private String isContVaild;//是否存在有效合同

    public String getIsCrdApprStatusPending() {
        return isCrdApprStatusPending;
    }

    public void setIsCrdApprStatusPending(String isCrdApprStatusPending) {
        this.isCrdApprStatusPending = isCrdApprStatusPending;
    }

    public String getIsApprVaild() {
        return isApprVaild;
    }

    public void setIsApprVaild(String isApprVaild) {
        this.isApprVaild = isApprVaild;
    }

    public String getIsContVaild() {
        return isContVaild;
    }

    public void setIsContVaild(String isContVaild) {
        this.isContVaild = isContVaild;
    }

    public String getIsHavingSpouse() {
        return isHavingSpouse;
    }

    public void setIsHavingSpouse(String isHavingSpouse) {
        this.isHavingSpouse = isHavingSpouse;
    }

    public String getIsHavingOperBillBal() {
        return isHavingOperBillBal;
    }

    public void setIsHavingOperBillBal(String isHavingOperBillBal) {
        this.isHavingOperBillBal = isHavingOperBillBal;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    @Override
    public String toString() {
        return "Data{" +
                "isHavingSpouse='" + isHavingSpouse + '\'' +
                ", isHavingOperBillBal='" + isHavingOperBillBal + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", orgName='" + orgName + '\'' +
                ", billBal=" + billBal +
                ", isCrdApprStatusPending='" + isCrdApprStatusPending + '\'' +
                ", isApprVaild='" + isApprVaild + '\'' +
                ", isContVaild='" + isContVaild + '\'' +
                '}';
    }
}
