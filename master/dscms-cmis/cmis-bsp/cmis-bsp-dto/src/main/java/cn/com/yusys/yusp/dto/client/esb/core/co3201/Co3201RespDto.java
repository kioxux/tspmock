package cn.com.yusys.yusp.dto.client.esb.core.co3201;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：抵质押物信息修改
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Co3201RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzywbhao")
    private String dzywbhao;//抵质押物编号
    @JsonProperty(value = "dzywminc")
    private String dzywminc;//抵质押物名称
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "syrkhhao")
    private String syrkhhao;//受益人客户号
    @JsonProperty(value = "syrkhmin")
    private String syrkhmin;//受益人客户名
    @JsonProperty(value = "syqrkehh")
    private String syqrkehh;//所有权人客户号
    @JsonProperty(value = "syqrkehm")
    private String syqrkehm;//所有权人客户名
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "minyjiaz")
    private BigDecimal minyjiaz;//名义价值
    @JsonProperty(value = "pingjiaz")
    private BigDecimal pingjiaz;//评估价值
    @JsonProperty(value = "shijjiaz")
    private BigDecimal shijjiaz;//实际价值
    @JsonProperty(value = "dizybilv")
    private BigDecimal dizybilv;//抵质押比率
    @JsonProperty(value = "keyongje")
    private BigDecimal keyongje;//可用金额
    @JsonProperty(value = "shengxrq")
    private String shengxrq;//生效日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "dzywztai")
    private String dzywztai;//抵质押物状态
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要
    @JsonProperty(value = "kaihguiy")
    private String kaihguiy;//开户柜员
    @JsonProperty(value = "kaihriqi")
    private String kaihriqi;//开户日期
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "dizyfshi")
    private String dizyfshi;//抵质押方式

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDzywminc() {
        return dzywminc;
    }

    public void setDzywminc(String dzywminc) {
        this.dzywminc = dzywminc;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getSyrkhhao() {
        return syrkhhao;
    }

    public void setSyrkhhao(String syrkhhao) {
        this.syrkhhao = syrkhhao;
    }

    public String getSyrkhmin() {
        return syrkhmin;
    }

    public void setSyrkhmin(String syrkhmin) {
        this.syrkhmin = syrkhmin;
    }

    public String getSyqrkehh() {
        return syqrkehh;
    }

    public void setSyqrkehh(String syqrkehh) {
        this.syqrkehh = syqrkehh;
    }

    public String getSyqrkehm() {
        return syqrkehm;
    }

    public void setSyqrkehm(String syqrkehm) {
        this.syqrkehm = syqrkehm;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getMinyjiaz() {
        return minyjiaz;
    }

    public void setMinyjiaz(BigDecimal minyjiaz) {
        this.minyjiaz = minyjiaz;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getShijjiaz() {
        return shijjiaz;
    }

    public void setShijjiaz(BigDecimal shijjiaz) {
        this.shijjiaz = shijjiaz;
    }

    public BigDecimal getDizybilv() {
        return dizybilv;
    }

    public void setDizybilv(BigDecimal dizybilv) {
        this.dizybilv = dizybilv;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDzywztai() {
        return dzywztai;
    }

    public void setDzywztai(String dzywztai) {
        this.dzywztai = dzywztai;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getDizyfshi() {
        return dizyfshi;
    }

    public void setDizyfshi(String dizyfshi) {
        this.dizyfshi = dizyfshi;
    }

    @Override
    public String toString() {
        return "Co3201RespDto{" +
                "dzywbhao='" + dzywbhao + '\'' +
                "dzywminc='" + dzywminc + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "syrkhhao='" + syrkhhao + '\'' +
                "syrkhmin='" + syrkhmin + '\'' +
                "syqrkehh='" + syqrkehh + '\'' +
                "syqrkehm='" + syqrkehm + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "minyjiaz='" + minyjiaz + '\'' +
                "pingjiaz='" + pingjiaz + '\'' +
                "shijjiaz='" + shijjiaz + '\'' +
                "dizybilv='" + dizybilv + '\'' +
                "keyongje='" + keyongje + '\'' +
                "shengxrq='" + shengxrq + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "dzywztai='" + dzywztai + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "kaihguiy='" + kaihguiy + '\'' +
                "kaihriqi='" + kaihriqi + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "dizyfshi='" + dizyfshi + '\'' +
                '}';
    }
}  
