package cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：针对录入、复核模式的交易，用于查询信贷录入信息的查询，复核时使用
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3036RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "dbdkkksx")
    private Integer dbdkkksx;//多笔贷款扣款顺序
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kuaijilb")
    private String kuaijilb;//会计类别
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限(月)
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "yingjitx")
    private BigDecimal yingjitx;//应计贴息
    @JsonProperty(value = "yingshtx")
    private BigDecimal yingshtx;//应收贴息
    @JsonProperty(value = "yingshfy")
    private BigDecimal yingshfy;//应收费用
    @JsonProperty(value = "yingshfj")
    private BigDecimal yingshfj;//应收罚金
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "fkfangsh")
    private String fkfangsh;//放款金额方式
    @JsonProperty(value = "jxhjdkkz")
    private String jxhjdkkz;//借新还旧控制
    @JsonProperty(value = "jxhjhkkz")
    private String jxhjhkkz;//借新还旧还款控制
    @JsonProperty(value = "hjiuleix")
    private String hjiuleix;//还旧类型

    private java.util.List<Lstydkjjh> lstydkjjh;//借新还旧列表
    @JsonProperty(value = "zidfkbzh")
    private String zidfkbzh;//自动放款标志
    @JsonProperty(value = "zhqifkbz")
    private String zhqifkbz;//周期性放款标志
    @JsonProperty(value = "dzhifkjh")
    private String dzhifkjh;//定制放款计划
    @JsonProperty(value = "yxtsfkbz")
    private String yxtsfkbz;//允许特殊放款标志
    @JsonProperty(value = "zdfkjjms")
    private String zdfkjjms;//放款借据管理模式
    @JsonProperty(value = "mcfkjebl")
    private BigDecimal mcfkjebl;//每次放款金额或比例
    @JsonProperty(value = "bencfkje")
    private BigDecimal bencfkje;//本次放款金额
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "dkrzhzxh")
    private String dkrzhzxh;//贷款入账账号子序号
    @JsonProperty(value = "zjzrzhmc")
    private String zjzrzhmc;//资金转入账号名称
    @JsonProperty(value = "hkzhhmch")
    private String hkzhhmch;//还款账户名称
    @JsonProperty(value = "fkzhouqi")
    private String fkzhouqi;//放款周期
    @JsonProperty(value = "kxqjjrgz")
    private String kxqjjrgz;//宽限期节假日规则
    @JsonProperty(value = "jixibzhi")
    private String jixibzhi;//计息标志
    @JsonProperty(value = "jfxibzhi")
    private String jfxibzhi;//计复息标志
    @JsonProperty(value = "fxjxbzhi")
    private String fxjxbzhi;//复息计息标志
    @JsonProperty(value = "jixiguiz")
    private String jixiguiz;//计息规则
    @JsonProperty(value = "kxqshxgz")
    private String kxqshxgz;//宽限期收息规则
    @JsonProperty(value = "kxqzyqgz")
    private String kxqzyqgz;//宽限期转逾期规则
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "yqfxfdfs")
    private String yqfxfdfs;//逾期罚息浮动方式
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "llqxkdfs")
    private String llqxkdfs;//利率期限靠档方式
    @JsonProperty(value = "ltjixigz")
    private String ltjixigz;//零头计息规则
    @JsonProperty(value = "meictxfs")
    private String meictxfs;//每次摊销方式
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "yuqinyll")
    private String yuqinyll;//逾期年月利率
    @JsonProperty(value = "lilvfend")
    private String lilvfend;//利率分段
    @JsonProperty(value = "zaoqixbz")
    private String zaoqixbz;//早起息标志
    @JsonProperty(value = "wanqixbz")
    private String wanqixbz;//晚起息标志
    @JsonProperty(value = "tiexibzh")
    private String tiexibzh;//贴息标志
    @JsonProperty(value = "whlilvzl")
    private String whlilvzl;//外汇利率种类
    @JsonProperty(value = "whllqxzl")
    private String whllqxzl;//外汇利率期限种类
    @JsonProperty(value = "yushxfsh")
    private String yushxfsh;//预收息方式
    @JsonProperty(value = "meictxbl")
    private BigDecimal meictxbl;//每次摊销比例
    @JsonProperty(value = "kxqzdcsh")
    private Integer kxqzdcsh;//宽限期最大次数
    @JsonProperty(value = "zclilvbh")
    private String zclilvbh;//正常利率编号
    @JsonProperty(value = "yuqillbh")
    private String yuqillbh;//逾期利率编号
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "zhxnlilv")
    private BigDecimal zhxnlilv;//执行年利率
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "yqfxfdzh")
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "lilvqixx")
    private String lilvqixx;//利率期限
    @JsonProperty(value = "yushxize")
    private BigDecimal yushxize;//预收息总额
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lixitxzq")
    private String lixitxzq;//利息摊销周期
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "qigscfsh")
    private String qigscfsh;//期供生成方式
    @JsonProperty(value = "duophkbz")
    private String duophkbz;//多频率还款标志
    @JsonProperty(value = "dzhhkjih")
    private String dzhhkjih;//定制还款计划
    @JsonProperty(value = "zdkoukbz")
    private String zdkoukbz;//自动扣款标志
    @JsonProperty(value = "zdplkkbz")
    private String zdplkkbz;//指定文件批量扣款标识
    @JsonProperty(value = "zdjqdkbz")
    private String zdjqdkbz;//自动结清贷款标志
    @JsonProperty(value = "dhkzhhbz")
    private String dhkzhhbz;//多还款账户标志
    @JsonProperty(value = "qyxhdkbz")
    private String qyxhdkbz;//签约循环贷款标志
    @JsonProperty(value = "qxbgtzjh")
    private String qxbgtzjh;//期限变更调整计划
    @JsonProperty(value = "llbgtzjh")
    private String llbgtzjh;//利率变更调整计划
    @JsonProperty(value = "dcfktzjh")
    private String dcfktzjh;//多次放款调整计划
    @JsonProperty(value = "tqhktzjh")
    private String tqhktzjh;//提前还款调整计划
    @JsonProperty(value = "yunxtqhk")
    private String yunxtqhk;//允许提前还款
    @JsonProperty(value = "hezuofbh")
    private String hezuofbh;//合作方编号
    @JsonProperty(value = "hezuofmc")
    private String hezuofmc;//合作方名称
    @JsonProperty(value = "xycihkrq")
    private String xycihkrq;//下一次还款日
    @JsonProperty(value = "leijqjsh")
    private Integer leijqjsh;//累进区间期数
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "xhdkqyzh")
    private String xhdkqyzh;//签约账号
    @JsonProperty(value = "hkqixian")
    private String hkqixian;//还款期限(月)
    @JsonProperty(value = "hkshxubh")
    private String hkshxubh;//还款顺序编号
    @JsonProperty(value = "leijinzh")
    private BigDecimal leijinzh;//累进值
    @JsonProperty(value = "baoliuje")
    private BigDecimal baoliuje;//保留金额
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "xhdkzhxh")
    private String xhdkzhxh;//签约账号子序号
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "huanbzhq")
    private String huanbzhq;//还本周期
    @JsonProperty(value = "yuqhkzhq")
    private String yuqhkzhq;//逾期还款周期
    @JsonProperty(value = "zbjrsygz")
    private String zbjrsygz;//贷款到期假日规则
    @JsonProperty(value = "zhqxzekk")
    private String zhqxzekk;//展期需足额扣款
    @JsonProperty(value = "yunxdkzq")
    private String yunxdkzq;//允许贷款展期
    @JsonProperty(value = "zhqzdcsh")
    private Integer zhqzdcsh;//展期最大次数
    @JsonProperty(value = "zqgzbhao")
    private String zqgzbhao;//展期规则编号
    @JsonProperty(value = "dlhesfsh")
    private String dlhesfsh;//代理核算方式
    @JsonProperty(value = "dlxxqzgz")
    private String dlxxqzgz;//代理信息取值规则
    @JsonProperty(value = "dlxxzdgz")
    private String dlxxzdgz;//代理信息指定规则
    @JsonProperty(value = "wtckywbm")
    private String wtckywbm;//委托存款业务编码
    @JsonProperty(value = "duowtrbz")
    private String duowtrbz;//多委托人标志
    private java.util.List<Lstdkwtxx> lstdkwtxx;// 贷款多委托人账户
    @JsonProperty(value = "dailixuh")
    private Integer dailixuh;//代理序号
    @JsonProperty(value = "wtrkehuh")
    private String wtrkehuh;//委托人客户号
    @JsonProperty(value = "wtrckuzh")
    private String wtrckuzh;//委托人存款账号
    @JsonProperty(value = "wtckzhao")
    private String wtckzhao;//委托存款账号
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号
    @JsonProperty(value = "dailimsh")
    private String dailimsh;//代理描述
    @JsonProperty(value = "wtrckzxh")
    private String wtrckzxh;//委托人存款账号子序号
    @JsonProperty(value = "wtckzixh")
    private String wtckzixh;//委托存款账号子序号
    @JsonProperty(value = "bjghrzxh")
    private String bjghrzxh;//本金归还入账账号子序号
    @JsonProperty(value = "lxghrzxh")
    private String lxghrzxh;//利息归还入账账号子序号
    @JsonProperty(value = "baozjzxh")
    private String baozjzxh;//保证金账号子序号
    @JsonProperty(value = "daikduix")
    private String daikduix;//贷款对象
    @JsonProperty(value = "yewufenl")
    private String yewufenl;//业务分类
    @JsonProperty(value = "ysywleix")
    private String ysywleix;//衍生业务类型
    @JsonProperty(value = "ysywbhao")
    private String ysywbhao;//衍生业务编号
    @JsonProperty(value = "hesuanfs")
    private String hesuanfs;//核算方式
    @JsonProperty(value = "lixizcgz")
    private String lixizcgz;//利息转出规则
    @JsonProperty(value = "lixizhgz")
    private String lixizhgz;//利息转回规则
    @JsonProperty(value = "yjfyjhes")
    private String yjfyjhes;//按应计非应计核算
    @JsonProperty(value = "yiyldhes")
    private String yiyldhes;//按一逾两呆核算
    @JsonProperty(value = "dkxtkmhs")
    private String dkxtkmhs;//形态分科目核算
    @JsonProperty(value = "zidxtzhy")
    private String zidxtzhy;//自动形态转移
    @JsonProperty(value = "zhchtqtz")
    private String zhchtqtz;//正常提前通知
    @JsonProperty(value = "yqcshtzh")
    private String yqcshtzh;//逾期催收通知
    @JsonProperty(value = "lilvbgtz")
    private String lilvbgtz;//利率变更通知
    @JsonProperty(value = "yuebgtzh")
    private String yuebgtzh;//余额变更通知
    @JsonProperty(value = "tzhtqtsh")
    private Integer tzhtqtsh;//通知提前天数
    @JsonProperty(value = "tzhjgtsh")
    private Integer tzhjgtsh;//通知间隔天数
    @JsonProperty(value = "shfleixi")
    private String shfleixi;//收费类型
    @JsonProperty(value = "tqhkfjbh")
    private String tqhkfjbh;//提前还款罚金编号
    @JsonProperty(value = "tqhkfjmc")
    private String tqhkfjmc;//提前还款罚金名称
    @JsonProperty(value = "tqhkfjfj")
    private BigDecimal tqhkfjfj;//提前还款附加罚金金额
    @JsonProperty(value = "pingzhzl")
    private String pingzhzl;//凭证种类
    @JsonProperty(value = "sydkcnuo")
    private String sydkcnuo;//使用贷款承诺
    @JsonProperty(value = "lmdkbzhi")
    private String lmdkbzhi;//联名贷款标志
    @JsonProperty(value = "bzhrdbbz")
    private String bzhrdbbz;//保证人担保标志
    @JsonProperty(value = "lstdkzhbz")
    private java.util.List<Lstdkzhbz> lstdkzhbz;// 贷款保证人信息
    @JsonProperty(value = "wujiflbz")
    private String wujiflbz;//五级分类标志
    @JsonProperty(value = "wujiflrq")
    private String wujiflrq;//五级分类日期
    @JsonProperty(value = "cndkjjho")
    private String cndkjjho;//承诺贷款借据号
    @JsonProperty(value = "dkgljgou")
    private String dkgljgou;//贷款管理机构
    @JsonProperty(value = "gljgleib")
    private String gljgleib;//管理机构类别
    @JsonProperty(value = "fuhejgou")
    private String fuhejgou;//复核机构
    @JsonProperty(value = "khjingli")
    private String khjingli;//客户经理
    @JsonProperty(value = "pingzhma")
    private String pingzhma;//凭证批号
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao;//凭证序号
    @JsonProperty(value = "sfjnyhsh")
    private String sfjnyhsh;//是否缴纳印花税标志
    @JsonProperty(value = "yinhshlv")
    private BigDecimal yinhshlv;//印花税率
    @JsonProperty(value = "yinhshje")
    private BigDecimal yinhshje;//印花税金额
    @JsonProperty(value = "edbiahao")
    private String edbiahao;//额度编号
    @JsonProperty(value = "shynedbz")
    private String shynedbz;//使用额度标志
    @JsonProperty(value = "edbizhgz")
    private String edbizhgz;//额度币种规则
    @JsonProperty(value = "edzdbizh")
    private String edzdbizh;//额度指定币种
    @JsonProperty(value = "edshyngz")
    private String edshyngz;//额度使用规则
    @JsonProperty(value = "hetongll")
    private BigDecimal hetongll;//合同利率
    @JsonProperty(value = "qixitshu")
    private Integer qixitshu;//起息天数
    @JsonProperty(value = "chanpzdm")
    private String chanpzdm;//产品组代码
    @JsonProperty(value = "chanpzmc")
    private String chanpzmc;//产品组名称
    @JsonProperty(value = "lstdkfkjh")
    private java.util.List<Lstdkfkjh> lstdkfkjh;// 贷款放款计划
    @JsonProperty(value = "lstdkfwdj")
    private java.util.List<Lstdkfwdj> lstdkfwdj;// 贷款服务登记
    @JsonProperty(value = "lstdkhbjh")
    private java.util.List<Lstdkhbjh> lstdkhbjh;// 贷款还本计划
    @JsonProperty(value = "benjinfd")
    private String benjinfd;//本金分段
    @JsonProperty(value = "lstdkbjfd")
    private java.util.List<Lstdkbjfd> lstdkbjfd;// 本金分段登记
    @JsonProperty(value = "lstdkhkfs")
    private java.util.List<Lstdkhkfs> lstdkhkfs;// 贷款组合还款方式
    @JsonProperty(value = "lstdkhkzh")
    private java.util.List<Lstdkhkzh> lstdkhkzh;// 贷款多还款账户
    @JsonProperty(value = "lstdkllfd")
    private java.util.List<Lstdkllfd> lstdkllfd;// 贷款利率分段
    @JsonProperty(value = "lstdksfsj")
    private java.util.List<Lstdksfsj> lstdksfsj;// 贷款收费事件
    @JsonProperty(value = "lstdkstzf")
    private java.util.List<Lstdkstzf> lstdkstzf;// 贷款受托支付
    @JsonProperty(value = "lstdktxzh")
    private java.util.List<Lstdktxzh> lstdktxzh;// 贷款多贴息账户
    @JsonProperty(value = "lstdkzhlm")
    private java.util.List<Lstdkzhlm> lstdkzhlm;// 贷款账户联名
    @JsonProperty(value = "fafangje")
    private BigDecimal fafangje;//已发放金额
    @JsonProperty(value = "djiekfje")
    private BigDecimal djiekfje;//冻结可放金额
    @JsonProperty(value = "kffangje")
    private BigDecimal kffangje;//可发放金额
    @JsonProperty(value = "yewusx01")
    private String yewusx01;//业务属性1
    @JsonProperty(value = "yewusx02")
    private String yewusx02;//业务属性2
    @JsonProperty(value = "yewusx03")
    private String yewusx03;//业务属性3
    @JsonProperty(value = "yewusx04")
    private String yewusx04;//业务属性4
    @JsonProperty(value = "yewusx05")
    private String yewusx05;//业务属性5
    @JsonProperty(value = "yewusx06")
    private String yewusx06;//业务属性6
    @JsonProperty(value = "yewusx07")
    private String yewusx07;//业务属性7
    @JsonProperty(value = "yewusx08")
    private String yewusx08;//业务属性8
    @JsonProperty(value = "yewusx09")
    private String yewusx09;//业务属性9
    @JsonProperty(value = "yewusx10")
    private String yewusx10;//业务属性10
    @JsonProperty(value = "ywsxms01")
    private String ywsxms01;//业务属性描述1
    @JsonProperty(value = "ywsxms02")
    private String ywsxms02;//业务属性描述2
    @JsonProperty(value = "ywsxms03")
    private String ywsxms03;//业务属性描述3
    @JsonProperty(value = "ywsxms04")
    private String ywsxms04;//业务属性描述4
    @JsonProperty(value = "ywsxms05")
    private String ywsxms05;//业务属性描述5
    @JsonProperty(value = "ywsxms06")
    private String ywsxms06;//业务属性描述6
    @JsonProperty(value = "ywsxms07")
    private String ywsxms07;//业务属性描述7
    @JsonProperty(value = "ywsxms08")
    private String ywsxms08;//业务属性描述8
    @JsonProperty(value = "ywsxms09")
    private String ywsxms09;//业务属性描述9
    @JsonProperty(value = "ywsxms10")
    private String ywsxms10;//业务属性描述10
    @JsonProperty(value = "daikdxxf")
    private String daikdxxf;//贷款对象细分
    @JsonProperty(value = "bwchapbz")
    private String bwchapbz;//表外产品
    @JsonProperty(value = "xunhdaik")
    private String xunhdaik;//循环贷款
    @JsonProperty(value = "yansdaik")
    private String yansdaik;//衍生贷款
    @JsonProperty(value = "chendaik")
    private String chendaik;//承诺贷款
    @JsonProperty(value = "cnkxhbzh")
    private String cnkxhbzh;//承诺可循环标志
    @JsonProperty(value = "butidaik")
    private String butidaik;//补贴贷款
    @JsonProperty(value = "yintdkbz")
    private String yintdkbz;//银团贷款标志
    @JsonProperty(value = "yintdkfs")
    private String yintdkfs;//银团贷款方式
    @JsonProperty(value = "yintleib")
    private String yintleib;//银团类别
    @JsonProperty(value = "yintnbcy")
    private String yintnbcy;//内部银团成员类型
    @JsonProperty(value = "yintwbcy")
    private String yintwbcy;//外部银团成员类型
    @JsonProperty(value = "jiangulx")
    private String jiangulx;//产品适用监管类型
    @JsonProperty(value = "zhqizcqx")
    private String zhqizcqx;//展期最长期限(月)
    @JsonProperty(value = "fangkulx")
    private String fangkulx;//放款类型
    @JsonProperty(value = "fkzjclfs")
    private String fkzjclfs;//放款资金处理方式
    @JsonProperty(value = "hntmifku")
    private String hntmifku;//允许对行内同名账户放款
    @JsonProperty(value = "hnftmfku")
    private String hnftmfku;//允许对行内非同名账户放款
    @JsonProperty(value = "neibufku")
    private String neibufku;//允许对内部账户放款
    @JsonProperty(value = "jixibjgz")
    private String jixibjgz;//计息本金规则
    @JsonProperty(value = "lixijsff")
    private String lixijsff;//利息计算方法
    @JsonProperty(value = "jixitwgz")
    private String jixitwgz;//计息头尾规则
    @JsonProperty(value = "jixizxje")
    private BigDecimal jixizxje;//计息最小金额
    @JsonProperty(value = "jixisrgz")
    private String jixisrgz;//计息舍入规则
    @JsonProperty(value = "srzxdanw")
    private String srzxdanw;//舍入最小单位
    @JsonProperty(value = "fdjixibz")
    private String fdjixibz;//分段计息标志
    @JsonProperty(value = "yuqitzfs")
    private String yuqitzfs;//逾期利率调整方式
    @JsonProperty(value = "yuqitzzq")
    private String yuqitzzq;//逾期利率调整周期
    @JsonProperty(value = "fulilvbh")
    private String fulilvbh;//复利利率编号
    @JsonProperty(value = "fulilvny")
    private String fulilvny;//复利利率年月标识
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "fulitzfs")
    private String fulitzfs;//复利利率调整方式
    @JsonProperty(value = "fulitzzq")
    private String fulitzzq;//复利利率调整周期
    @JsonProperty(value = "fulifdfs")
    private String fulifdfs;//复利利率浮动方式
    @JsonProperty(value = "fulifdzh")
    private BigDecimal fulifdzh;//复利利率浮动值
    @JsonProperty(value = "jitiguiz")
    private String jitiguiz;//计提规则
    @JsonProperty(value = "zqxizdds")
    private Integer zqxizdds;//早起息最大天数
    @JsonProperty(value = "wqxizdds")
    private Integer wqxizdds;//晚起息最大天数
    @JsonProperty(value = "butijejs")
    private String butijejs;//补贴金额计算方式
    @JsonProperty(value = "tiaozhkf")
    private String tiaozhkf;//允许调整还款方式
    @JsonProperty(value = "scihkrbz")
    private String scihkrbz;//首次还款日模式
    @JsonProperty(value = "mqihkfsh")
    private String mqihkfsh;//末期还款方式
    @JsonProperty(value = "yunxsuoq")
    private String yunxsuoq;//允许缩期
    @JsonProperty(value = "suoqcish")
    private Integer suoqcish;//缩期次数
    @JsonProperty(value = "bzuekkfs")
    private String bzuekkfs;//不足额扣款方式
    @JsonProperty(value = "yqbzkkfs")
    private String yqbzkkfs;//逾期不足额扣款方式
    @JsonProperty(value = "hntmihku")
    private String hntmihku;//允许行内同名账户还款
    @JsonProperty(value = "hnftmhku")
    private String hnftmhku;//允许行内非同名帐户还款
    @JsonProperty(value = "nbuzhhku")
    private String nbuzhhku;//允许内部账户还款
    @JsonProperty(value = "tiqhksdq")
    private Integer tiqhksdq;//提前还款锁定期
    @JsonProperty(value = "sfyxkuxq")
    private String sfyxkuxq;//是否有宽限期
    @JsonProperty(value = "kuanxqts")
    private Integer kuanxqts;//宽限期天数
    @JsonProperty(value = "kxqjixgz")
    private String kxqjixgz;//宽限期计息方式
    @JsonProperty(value = "kxqhjxgz")
    private String kxqhjxgz;//宽限期后计息规则
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "flllcklx")
    private String flllcklx;//复利利率参考类型
    @JsonProperty(value = "lstdzqgjh")
    private java.util.List<Lstdzqgjh> lstdzqgjh;// 贷款定制期供计划表
    @JsonProperty(value = "qglxleix")
    private String qglxleix;//期供利息类型
    @JsonProperty(value = "kshchpdm")
    private String kshchpdm;//可售产品代码
    @JsonProperty(value = "kshchpmc")
    private String kshchpmc;//可售产品名称
    @JsonProperty(value = "dkczhzhh")
    private String dkczhzhh;//贷款出账号
    @JsonProperty(value = "dkdbfshi")
    private String dkdbfshi;//贷款担保方式
    @JsonProperty(value = "dkyongtu")
    private String dkyongtu;//贷款用途
    @JsonProperty(value = "shtzfhxm")
    private String shtzfhxm;//受托支付业务编码
    @JsonProperty(value = "fkjzhfsh")
    private String fkjzhfsh;//放款记账方式
    @JsonProperty(value = "bwhesdma")
    private String bwhesdma;//表外核算码
    @JsonProperty(value = "wtrmingc")
    private String wtrmingc;//委托人名称
    @JsonProperty(value = "ysxlyzhh")
    private String ysxlyzhh;//预收息扣息来源账号
    @JsonProperty(value = "ysxlyzxh")
    private String ysxlyzxh;//预收息扣息来源账号子序号
    @JsonProperty(value = "lstytczfe")
    private java.util.List<Lstytczfe> lstytczfe;// 银团出资份额
    @JsonProperty(value = "zjlyzhao")
    private String zjlyzhao;//资金来源账号
    @JsonProperty(value = "zjlyzzxh")
    private String zjlyzzxh;//资金来源账号子序号
    @JsonProperty(value = "zyjjcpdm")
    private String zyjjcpdm;//自营借据产品代码
    @JsonProperty(value = "zyjjcpmc")
    private String zyjjcpmc;//自营借据产品名称
    @JsonProperty(value = "fzyjcpdm")
    private String fzyjcpdm;//非自营借据产品代码
    @JsonProperty(value = "fzyjcpmc")
    private String fzyjcpmc;//非自营借据产品名称
    @JsonProperty(value = "dktiansh")
    private Integer dktiansh;//贷款天数
    @JsonProperty(value = "sfpinlvv")
    private String sfpinlvv;//收费频率
    @JsonProperty(value = "sfzhouqi")
    private String sfzhouqi;//收费周期
    @JsonProperty(value = "shoufzhl")
    private String shoufzhl;//收费种类
    @JsonProperty(value = "txzhouqi")
    private String txzhouqi;//摊销周期
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期
    @JsonProperty(value = "shoufdma")
    private String shoufdma;//收费代码
    @JsonProperty(value = "shfdmamc")
    private String shfdmamc;//收费代码名称
    @JsonProperty(value = "shoufjee")
    private BigDecimal shoufjee;//收费金额/比例
    @JsonProperty(value = "fufeizhh")
    private String fufeizhh;//付费账号
    @JsonProperty(value = "ffzhhzxh")
    private String ffzhhzxh;//付费账号子序号
    @JsonProperty(value = "ffzhhmch")
    private String ffzhhmch;//付费账号名称
    @JsonProperty(value = "sfrzhzhh")
    private String sfrzhzhh;//收费入账账号
    @JsonProperty(value = "sfrzhzxh")
    private String sfrzhzxh;//收费入账账号子序号
    @JsonProperty(value = "mqjqsfbz")
    private String mqjqsfbz;//末期已结清是否收费
    @JsonProperty(value = "fybkdqbz")
    private String fybkdqbz;//费用首期是否包括当前期
    @JsonProperty(value = "sfljsfei")
    private String sfljsfei;//是否立即收费
    private java.util.List<Lstdklhmx> lstdklhmx;// 联合贷款
    @JsonProperty(value = "jrlxsybz")
    private String jrlxsybz;//节假日利息顺延标志
    @JsonProperty(value = "zdkuanje")
    private BigDecimal zdkuanje;//转垫款金额
    @JsonProperty(value = "shifyyqx")
    private String shifyyqx;//是否取消预约
    @JsonProperty(value = "zhanqixh")
    private Integer zhanqixh;//展期序号
    @JsonProperty(value = "zhanqhth")
    private String zhanqhth;//展期合同号
    @JsonProperty(value = "zhanqirq")
    private String zhanqirq;//展期日期
    @JsonProperty(value = "zhanqdqr")
    private String zhanqdqr;//展期到期日
    @JsonProperty(value = "zhanqije")
    private BigDecimal zhanqije;//展期金额
    @JsonProperty(value = "chuzhhao")
    private String chuzhhao;//出账号
    @JsonProperty(value = "benqqish")
    private Integer benqqish;//本期期数
    @JsonProperty(value = "lxirenmc")
    private String lxirenmc;//联系人名称
    @JsonProperty(value = "lxirendh")
    private String lxirendh;//联系人电话
    @JsonProperty(value = "absdkulx")
    private String absdkulx;//贸融ABS贷款类型
    @JsonProperty(value = "lstdkzhzy")
    private java.util.List<Lstdkzhzy> lstdkzhzy;// 贷款账户质押信息
    @JsonProperty(value = "sfglzhzy")
    private String sfglzhzy;//是否账户质押标志

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public Integer getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Integer dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getFkfangsh() {
        return fkfangsh;
    }

    public void setFkfangsh(String fkfangsh) {
        this.fkfangsh = fkfangsh;
    }

    public String getJxhjdkkz() {
        return jxhjdkkz;
    }

    public void setJxhjdkkz(String jxhjdkkz) {
        this.jxhjdkkz = jxhjdkkz;
    }

    public String getJxhjhkkz() {
        return jxhjhkkz;
    }

    public void setJxhjhkkz(String jxhjhkkz) {
        this.jxhjhkkz = jxhjhkkz;
    }

    public String getHjiuleix() {
        return hjiuleix;
    }

    public void setHjiuleix(String hjiuleix) {
        this.hjiuleix = hjiuleix;
    }

    public List<Lstydkjjh> getLstydkjjh() {
        return lstydkjjh;
    }

    public void setLstydkjjh(List<Lstydkjjh> lstydkjjh) {
        this.lstydkjjh = lstydkjjh;
    }

    public String getZidfkbzh() {
        return zidfkbzh;
    }

    public void setZidfkbzh(String zidfkbzh) {
        this.zidfkbzh = zidfkbzh;
    }

    public String getZhqifkbz() {
        return zhqifkbz;
    }

    public void setZhqifkbz(String zhqifkbz) {
        this.zhqifkbz = zhqifkbz;
    }

    public String getDzhifkjh() {
        return dzhifkjh;
    }

    public void setDzhifkjh(String dzhifkjh) {
        this.dzhifkjh = dzhifkjh;
    }

    public String getYxtsfkbz() {
        return yxtsfkbz;
    }

    public void setYxtsfkbz(String yxtsfkbz) {
        this.yxtsfkbz = yxtsfkbz;
    }

    public String getZdfkjjms() {
        return zdfkjjms;
    }

    public void setZdfkjjms(String zdfkjjms) {
        this.zdfkjjms = zdfkjjms;
    }

    public BigDecimal getMcfkjebl() {
        return mcfkjebl;
    }

    public void setMcfkjebl(BigDecimal mcfkjebl) {
        this.mcfkjebl = mcfkjebl;
    }

    public BigDecimal getBencfkje() {
        return bencfkje;
    }

    public void setBencfkje(BigDecimal bencfkje) {
        this.bencfkje = bencfkje;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public String getZjzrzhmc() {
        return zjzrzhmc;
    }

    public void setZjzrzhmc(String zjzrzhmc) {
        this.zjzrzhmc = zjzrzhmc;
    }

    public String getHkzhhmch() {
        return hkzhhmch;
    }

    public void setHkzhhmch(String hkzhhmch) {
        this.hkzhhmch = hkzhhmch;
    }

    public String getFkzhouqi() {
        return fkzhouqi;
    }

    public void setFkzhouqi(String fkzhouqi) {
        this.fkzhouqi = fkzhouqi;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    public String getJixibzhi() {
        return jixibzhi;
    }

    public void setJixibzhi(String jixibzhi) {
        this.jixibzhi = jixibzhi;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFxjxbzhi() {
        return fxjxbzhi;
    }

    public void setFxjxbzhi(String fxjxbzhi) {
        this.fxjxbzhi = fxjxbzhi;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public String getKxqshxgz() {
        return kxqshxgz;
    }

    public void setKxqshxgz(String kxqshxgz) {
        this.kxqshxgz = kxqshxgz;
    }

    public String getKxqzyqgz() {
        return kxqzyqgz;
    }

    public void setKxqzyqgz(String kxqzyqgz) {
        this.kxqzyqgz = kxqzyqgz;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLtjixigz() {
        return ltjixigz;
    }

    public void setLtjixigz(String ltjixigz) {
        this.ltjixigz = ltjixigz;
    }

    public String getMeictxfs() {
        return meictxfs;
    }

    public void setMeictxfs(String meictxfs) {
        this.meictxfs = meictxfs;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public String getLilvfend() {
        return lilvfend;
    }

    public void setLilvfend(String lilvfend) {
        this.lilvfend = lilvfend;
    }

    public String getZaoqixbz() {
        return zaoqixbz;
    }

    public void setZaoqixbz(String zaoqixbz) {
        this.zaoqixbz = zaoqixbz;
    }

    public String getWanqixbz() {
        return wanqixbz;
    }

    public void setWanqixbz(String wanqixbz) {
        this.wanqixbz = wanqixbz;
    }

    public String getTiexibzh() {
        return tiexibzh;
    }

    public void setTiexibzh(String tiexibzh) {
        this.tiexibzh = tiexibzh;
    }

    public String getWhlilvzl() {
        return whlilvzl;
    }

    public void setWhlilvzl(String whlilvzl) {
        this.whlilvzl = whlilvzl;
    }

    public String getWhllqxzl() {
        return whllqxzl;
    }

    public void setWhllqxzl(String whllqxzl) {
        this.whllqxzl = whllqxzl;
    }

    public String getYushxfsh() {
        return yushxfsh;
    }

    public void setYushxfsh(String yushxfsh) {
        this.yushxfsh = yushxfsh;
    }

    public BigDecimal getMeictxbl() {
        return meictxbl;
    }

    public void setMeictxbl(BigDecimal meictxbl) {
        this.meictxbl = meictxbl;
    }

    public Integer getKxqzdcsh() {
        return kxqzdcsh;
    }

    public void setKxqzdcsh(Integer kxqzdcsh) {
        this.kxqzdcsh = kxqzdcsh;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getZhxnlilv() {
        return zhxnlilv;
    }

    public void setZhxnlilv(BigDecimal zhxnlilv) {
        this.zhxnlilv = zhxnlilv;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public BigDecimal getYushxize() {
        return yushxize;
    }

    public void setYushxize(BigDecimal yushxize) {
        this.yushxize = yushxize;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLixitxzq() {
        return lixitxzq;
    }

    public void setLixitxzq(String lixitxzq) {
        this.lixitxzq = lixitxzq;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public String getDuophkbz() {
        return duophkbz;
    }

    public void setDuophkbz(String duophkbz) {
        this.duophkbz = duophkbz;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getZdplkkbz() {
        return zdplkkbz;
    }

    public void setZdplkkbz(String zdplkkbz) {
        this.zdplkkbz = zdplkkbz;
    }

    public String getZdjqdkbz() {
        return zdjqdkbz;
    }

    public void setZdjqdkbz(String zdjqdkbz) {
        this.zdjqdkbz = zdjqdkbz;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public String getQyxhdkbz() {
        return qyxhdkbz;
    }

    public void setQyxhdkbz(String qyxhdkbz) {
        this.qyxhdkbz = qyxhdkbz;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    public String getHezuofbh() {
        return hezuofbh;
    }

    public void setHezuofbh(String hezuofbh) {
        this.hezuofbh = hezuofbh;
    }

    public String getHezuofmc() {
        return hezuofmc;
    }

    public void setHezuofmc(String hezuofmc) {
        this.hezuofmc = hezuofmc;
    }

    public String getXycihkrq() {
        return xycihkrq;
    }

    public void setXycihkrq(String xycihkrq) {
        this.xycihkrq = xycihkrq;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getXhdkqyzh() {
        return xhdkqyzh;
    }

    public void setXhdkqyzh(String xhdkqyzh) {
        this.xhdkqyzh = xhdkqyzh;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public String getHkshxubh() {
        return hkshxubh;
    }

    public void setHkshxubh(String hkshxubh) {
        this.hkshxubh = hkshxubh;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getXhdkzhxh() {
        return xhdkzhxh;
    }

    public void setXhdkzhxh(String xhdkzhxh) {
        this.xhdkzhxh = xhdkzhxh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    public String getZbjrsygz() {
        return zbjrsygz;
    }

    public void setZbjrsygz(String zbjrsygz) {
        this.zbjrsygz = zbjrsygz;
    }

    public String getZhqxzekk() {
        return zhqxzekk;
    }

    public void setZhqxzekk(String zhqxzekk) {
        this.zhqxzekk = zhqxzekk;
    }

    public String getYunxdkzq() {
        return yunxdkzq;
    }

    public void setYunxdkzq(String yunxdkzq) {
        this.yunxdkzq = yunxdkzq;
    }

    public Integer getZhqzdcsh() {
        return zhqzdcsh;
    }

    public void setZhqzdcsh(Integer zhqzdcsh) {
        this.zhqzdcsh = zhqzdcsh;
    }

    public String getZqgzbhao() {
        return zqgzbhao;
    }

    public void setZqgzbhao(String zqgzbhao) {
        this.zqgzbhao = zqgzbhao;
    }

    public String getDlhesfsh() {
        return dlhesfsh;
    }

    public void setDlhesfsh(String dlhesfsh) {
        this.dlhesfsh = dlhesfsh;
    }

    public String getDlxxqzgz() {
        return dlxxqzgz;
    }

    public void setDlxxqzgz(String dlxxqzgz) {
        this.dlxxqzgz = dlxxqzgz;
    }

    public String getDlxxzdgz() {
        return dlxxzdgz;
    }

    public void setDlxxzdgz(String dlxxzdgz) {
        this.dlxxzdgz = dlxxzdgz;
    }

    public String getWtckywbm() {
        return wtckywbm;
    }

    public void setWtckywbm(String wtckywbm) {
        this.wtckywbm = wtckywbm;
    }

    public String getDuowtrbz() {
        return duowtrbz;
    }

    public void setDuowtrbz(String duowtrbz) {
        this.duowtrbz = duowtrbz;
    }

    public List<Lstdkwtxx> getLstdkwtxx() {
        return lstdkwtxx;
    }

    public void setLstdkwtxx(List<Lstdkwtxx> lstdkwtxx) {
        this.lstdkwtxx = lstdkwtxx;
    }

    public Integer getDailixuh() {
        return dailixuh;
    }

    public void setDailixuh(Integer dailixuh) {
        this.dailixuh = dailixuh;
    }

    public String getWtrkehuh() {
        return wtrkehuh;
    }

    public void setWtrkehuh(String wtrkehuh) {
        this.wtrkehuh = wtrkehuh;
    }

    public String getWtrckuzh() {
        return wtrckuzh;
    }

    public void setWtrckuzh(String wtrckuzh) {
        this.wtrckuzh = wtrckuzh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getDailimsh() {
        return dailimsh;
    }

    public void setDailimsh(String dailimsh) {
        this.dailimsh = dailimsh;
    }

    public String getWtrckzxh() {
        return wtrckzxh;
    }

    public void setWtrckzxh(String wtrckzxh) {
        this.wtrckzxh = wtrckzxh;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public String getBaozjzxh() {
        return baozjzxh;
    }

    public void setBaozjzxh(String baozjzxh) {
        this.baozjzxh = baozjzxh;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public String getYewufenl() {
        return yewufenl;
    }

    public void setYewufenl(String yewufenl) {
        this.yewufenl = yewufenl;
    }

    public String getYsywleix() {
        return ysywleix;
    }

    public void setYsywleix(String ysywleix) {
        this.ysywleix = ysywleix;
    }

    public String getYsywbhao() {
        return ysywbhao;
    }

    public void setYsywbhao(String ysywbhao) {
        this.ysywbhao = ysywbhao;
    }

    public String getHesuanfs() {
        return hesuanfs;
    }

    public void setHesuanfs(String hesuanfs) {
        this.hesuanfs = hesuanfs;
    }

    public String getLixizcgz() {
        return lixizcgz;
    }

    public void setLixizcgz(String lixizcgz) {
        this.lixizcgz = lixizcgz;
    }

    public String getLixizhgz() {
        return lixizhgz;
    }

    public void setLixizhgz(String lixizhgz) {
        this.lixizhgz = lixizhgz;
    }

    public String getYjfyjhes() {
        return yjfyjhes;
    }

    public void setYjfyjhes(String yjfyjhes) {
        this.yjfyjhes = yjfyjhes;
    }

    public String getYiyldhes() {
        return yiyldhes;
    }

    public void setYiyldhes(String yiyldhes) {
        this.yiyldhes = yiyldhes;
    }

    public String getDkxtkmhs() {
        return dkxtkmhs;
    }

    public void setDkxtkmhs(String dkxtkmhs) {
        this.dkxtkmhs = dkxtkmhs;
    }

    public String getZidxtzhy() {
        return zidxtzhy;
    }

    public void setZidxtzhy(String zidxtzhy) {
        this.zidxtzhy = zidxtzhy;
    }

    public String getZhchtqtz() {
        return zhchtqtz;
    }

    public void setZhchtqtz(String zhchtqtz) {
        this.zhchtqtz = zhchtqtz;
    }

    public String getYqcshtzh() {
        return yqcshtzh;
    }

    public void setYqcshtzh(String yqcshtzh) {
        this.yqcshtzh = yqcshtzh;
    }

    public String getLilvbgtz() {
        return lilvbgtz;
    }

    public void setLilvbgtz(String lilvbgtz) {
        this.lilvbgtz = lilvbgtz;
    }

    public String getYuebgtzh() {
        return yuebgtzh;
    }

    public void setYuebgtzh(String yuebgtzh) {
        this.yuebgtzh = yuebgtzh;
    }

    public Integer getTzhtqtsh() {
        return tzhtqtsh;
    }

    public void setTzhtqtsh(Integer tzhtqtsh) {
        this.tzhtqtsh = tzhtqtsh;
    }

    public Integer getTzhjgtsh() {
        return tzhjgtsh;
    }

    public void setTzhjgtsh(Integer tzhjgtsh) {
        this.tzhjgtsh = tzhjgtsh;
    }

    public String getShfleixi() {
        return shfleixi;
    }

    public void setShfleixi(String shfleixi) {
        this.shfleixi = shfleixi;
    }

    public String getTqhkfjbh() {
        return tqhkfjbh;
    }

    public void setTqhkfjbh(String tqhkfjbh) {
        this.tqhkfjbh = tqhkfjbh;
    }

    public String getTqhkfjmc() {
        return tqhkfjmc;
    }

    public void setTqhkfjmc(String tqhkfjmc) {
        this.tqhkfjmc = tqhkfjmc;
    }

    public BigDecimal getTqhkfjfj() {
        return tqhkfjfj;
    }

    public void setTqhkfjfj(BigDecimal tqhkfjfj) {
        this.tqhkfjfj = tqhkfjfj;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getSydkcnuo() {
        return sydkcnuo;
    }

    public void setSydkcnuo(String sydkcnuo) {
        this.sydkcnuo = sydkcnuo;
    }

    public String getLmdkbzhi() {
        return lmdkbzhi;
    }

    public void setLmdkbzhi(String lmdkbzhi) {
        this.lmdkbzhi = lmdkbzhi;
    }

    public String getBzhrdbbz() {
        return bzhrdbbz;
    }

    public void setBzhrdbbz(String bzhrdbbz) {
        this.bzhrdbbz = bzhrdbbz;
    }

    public List<Lstdkzhbz> getLstdkzhbz() {
        return lstdkzhbz;
    }

    public void setLstdkzhbz(List<Lstdkzhbz> lstdkzhbz) {
        this.lstdkzhbz = lstdkzhbz;
    }

    public String getWujiflbz() {
        return wujiflbz;
    }

    public void setWujiflbz(String wujiflbz) {
        this.wujiflbz = wujiflbz;
    }

    public String getWujiflrq() {
        return wujiflrq;
    }

    public void setWujiflrq(String wujiflrq) {
        this.wujiflrq = wujiflrq;
    }

    public String getCndkjjho() {
        return cndkjjho;
    }

    public void setCndkjjho(String cndkjjho) {
        this.cndkjjho = cndkjjho;
    }

    public String getDkgljgou() {
        return dkgljgou;
    }

    public void setDkgljgou(String dkgljgou) {
        this.dkgljgou = dkgljgou;
    }

    public String getGljgleib() {
        return gljgleib;
    }

    public void setGljgleib(String gljgleib) {
        this.gljgleib = gljgleib;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public String getKhjingli() {
        return khjingli;
    }

    public void setKhjingli(String khjingli) {
        this.khjingli = khjingli;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getSfjnyhsh() {
        return sfjnyhsh;
    }

    public void setSfjnyhsh(String sfjnyhsh) {
        this.sfjnyhsh = sfjnyhsh;
    }

    public BigDecimal getYinhshlv() {
        return yinhshlv;
    }

    public void setYinhshlv(BigDecimal yinhshlv) {
        this.yinhshlv = yinhshlv;
    }

    public BigDecimal getYinhshje() {
        return yinhshje;
    }

    public void setYinhshje(BigDecimal yinhshje) {
        this.yinhshje = yinhshje;
    }

    public String getEdbiahao() {
        return edbiahao;
    }

    public void setEdbiahao(String edbiahao) {
        this.edbiahao = edbiahao;
    }

    public String getShynedbz() {
        return shynedbz;
    }

    public void setShynedbz(String shynedbz) {
        this.shynedbz = shynedbz;
    }

    public String getEdbizhgz() {
        return edbizhgz;
    }

    public void setEdbizhgz(String edbizhgz) {
        this.edbizhgz = edbizhgz;
    }

    public String getEdzdbizh() {
        return edzdbizh;
    }

    public void setEdzdbizh(String edzdbizh) {
        this.edzdbizh = edzdbizh;
    }

    public String getEdshyngz() {
        return edshyngz;
    }

    public void setEdshyngz(String edshyngz) {
        this.edshyngz = edshyngz;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public Integer getQixitshu() {
        return qixitshu;
    }

    public void setQixitshu(Integer qixitshu) {
        this.qixitshu = qixitshu;
    }

    public String getChanpzdm() {
        return chanpzdm;
    }

    public void setChanpzdm(String chanpzdm) {
        this.chanpzdm = chanpzdm;
    }

    public String getChanpzmc() {
        return chanpzmc;
    }

    public void setChanpzmc(String chanpzmc) {
        this.chanpzmc = chanpzmc;
    }

    public List<Lstdkfkjh> getLstdkfkjh() {
        return lstdkfkjh;
    }

    public void setLstdkfkjh(List<Lstdkfkjh> lstdkfkjh) {
        this.lstdkfkjh = lstdkfkjh;
    }

    public List<Lstdkfwdj> getLstdkfwdj() {
        return lstdkfwdj;
    }

    public void setLstdkfwdj(List<Lstdkfwdj> lstdkfwdj) {
        this.lstdkfwdj = lstdkfwdj;
    }

    public List<Lstdkhbjh> getLstdkhbjh() {
        return lstdkhbjh;
    }

    public void setLstdkhbjh(List<Lstdkhbjh> lstdkhbjh) {
        this.lstdkhbjh = lstdkhbjh;
    }

    public String getBenjinfd() {
        return benjinfd;
    }

    public void setBenjinfd(String benjinfd) {
        this.benjinfd = benjinfd;
    }

    public List<Lstdkbjfd> getLstdkbjfd() {
        return lstdkbjfd;
    }

    public void setLstdkbjfd(List<Lstdkbjfd> lstdkbjfd) {
        this.lstdkbjfd = lstdkbjfd;
    }

    public List<Lstdkhkfs> getLstdkhkfs() {
        return lstdkhkfs;
    }

    public void setLstdkhkfs(List<Lstdkhkfs> lstdkhkfs) {
        this.lstdkhkfs = lstdkhkfs;
    }

    public List<Lstdkhkzh> getLstdkhkzh() {
        return lstdkhkzh;
    }

    public void setLstdkhkzh(List<Lstdkhkzh> lstdkhkzh) {
        this.lstdkhkzh = lstdkhkzh;
    }

    public List<Lstdkllfd> getLstdkllfd() {
        return lstdkllfd;
    }

    public void setLstdkllfd(List<Lstdkllfd> lstdkllfd) {
        this.lstdkllfd = lstdkllfd;
    }

    public List<Lstdksfsj> getLstdksfsj() {
        return lstdksfsj;
    }

    public void setLstdksfsj(List<Lstdksfsj> lstdksfsj) {
        this.lstdksfsj = lstdksfsj;
    }

    public List<Lstdkstzf> getLstdkstzf() {
        return lstdkstzf;
    }

    public void setLstdkstzf(List<Lstdkstzf> lstdkstzf) {
        this.lstdkstzf = lstdkstzf;
    }

    public List<Lstdktxzh> getLstdktxzh() {
        return lstdktxzh;
    }

    public void setLstdktxzh(List<Lstdktxzh> lstdktxzh) {
        this.lstdktxzh = lstdktxzh;
    }

    public List<Lstdkzhlm> getLstdkzhlm() {
        return lstdkzhlm;
    }

    public void setLstdkzhlm(List<Lstdkzhlm> lstdkzhlm) {
        this.lstdkzhlm = lstdkzhlm;
    }

    public BigDecimal getFafangje() {
        return fafangje;
    }

    public void setFafangje(BigDecimal fafangje) {
        this.fafangje = fafangje;
    }

    public BigDecimal getDjiekfje() {
        return djiekfje;
    }

    public void setDjiekfje(BigDecimal djiekfje) {
        this.djiekfje = djiekfje;
    }

    public BigDecimal getKffangje() {
        return kffangje;
    }

    public void setKffangje(BigDecimal kffangje) {
        this.kffangje = kffangje;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getYewusx02() {
        return yewusx02;
    }

    public void setYewusx02(String yewusx02) {
        this.yewusx02 = yewusx02;
    }

    public String getYewusx03() {
        return yewusx03;
    }

    public void setYewusx03(String yewusx03) {
        this.yewusx03 = yewusx03;
    }

    public String getYewusx04() {
        return yewusx04;
    }

    public void setYewusx04(String yewusx04) {
        this.yewusx04 = yewusx04;
    }

    public String getYewusx05() {
        return yewusx05;
    }

    public void setYewusx05(String yewusx05) {
        this.yewusx05 = yewusx05;
    }

    public String getYewusx06() {
        return yewusx06;
    }

    public void setYewusx06(String yewusx06) {
        this.yewusx06 = yewusx06;
    }

    public String getYewusx07() {
        return yewusx07;
    }

    public void setYewusx07(String yewusx07) {
        this.yewusx07 = yewusx07;
    }

    public String getYewusx08() {
        return yewusx08;
    }

    public void setYewusx08(String yewusx08) {
        this.yewusx08 = yewusx08;
    }

    public String getYewusx09() {
        return yewusx09;
    }

    public void setYewusx09(String yewusx09) {
        this.yewusx09 = yewusx09;
    }

    public String getYewusx10() {
        return yewusx10;
    }

    public void setYewusx10(String yewusx10) {
        this.yewusx10 = yewusx10;
    }

    public String getYwsxms01() {
        return ywsxms01;
    }

    public void setYwsxms01(String ywsxms01) {
        this.ywsxms01 = ywsxms01;
    }

    public String getYwsxms02() {
        return ywsxms02;
    }

    public void setYwsxms02(String ywsxms02) {
        this.ywsxms02 = ywsxms02;
    }

    public String getYwsxms03() {
        return ywsxms03;
    }

    public void setYwsxms03(String ywsxms03) {
        this.ywsxms03 = ywsxms03;
    }

    public String getYwsxms04() {
        return ywsxms04;
    }

    public void setYwsxms04(String ywsxms04) {
        this.ywsxms04 = ywsxms04;
    }

    public String getYwsxms05() {
        return ywsxms05;
    }

    public void setYwsxms05(String ywsxms05) {
        this.ywsxms05 = ywsxms05;
    }

    public String getYwsxms06() {
        return ywsxms06;
    }

    public void setYwsxms06(String ywsxms06) {
        this.ywsxms06 = ywsxms06;
    }

    public String getYwsxms07() {
        return ywsxms07;
    }

    public void setYwsxms07(String ywsxms07) {
        this.ywsxms07 = ywsxms07;
    }

    public String getYwsxms08() {
        return ywsxms08;
    }

    public void setYwsxms08(String ywsxms08) {
        this.ywsxms08 = ywsxms08;
    }

    public String getYwsxms09() {
        return ywsxms09;
    }

    public void setYwsxms09(String ywsxms09) {
        this.ywsxms09 = ywsxms09;
    }

    public String getYwsxms10() {
        return ywsxms10;
    }

    public void setYwsxms10(String ywsxms10) {
        this.ywsxms10 = ywsxms10;
    }

    public String getDaikdxxf() {
        return daikdxxf;
    }

    public void setDaikdxxf(String daikdxxf) {
        this.daikdxxf = daikdxxf;
    }

    public String getBwchapbz() {
        return bwchapbz;
    }

    public void setBwchapbz(String bwchapbz) {
        this.bwchapbz = bwchapbz;
    }

    public String getXunhdaik() {
        return xunhdaik;
    }

    public void setXunhdaik(String xunhdaik) {
        this.xunhdaik = xunhdaik;
    }

    public String getYansdaik() {
        return yansdaik;
    }

    public void setYansdaik(String yansdaik) {
        this.yansdaik = yansdaik;
    }

    public String getChendaik() {
        return chendaik;
    }

    public void setChendaik(String chendaik) {
        this.chendaik = chendaik;
    }

    public String getCnkxhbzh() {
        return cnkxhbzh;
    }

    public void setCnkxhbzh(String cnkxhbzh) {
        this.cnkxhbzh = cnkxhbzh;
    }

    public String getButidaik() {
        return butidaik;
    }

    public void setButidaik(String butidaik) {
        this.butidaik = butidaik;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getYintdkfs() {
        return yintdkfs;
    }

    public void setYintdkfs(String yintdkfs) {
        this.yintdkfs = yintdkfs;
    }

    public String getYintleib() {
        return yintleib;
    }

    public void setYintleib(String yintleib) {
        this.yintleib = yintleib;
    }

    public String getYintnbcy() {
        return yintnbcy;
    }

    public void setYintnbcy(String yintnbcy) {
        this.yintnbcy = yintnbcy;
    }

    public String getYintwbcy() {
        return yintwbcy;
    }

    public void setYintwbcy(String yintwbcy) {
        this.yintwbcy = yintwbcy;
    }

    public String getJiangulx() {
        return jiangulx;
    }

    public void setJiangulx(String jiangulx) {
        this.jiangulx = jiangulx;
    }

    public String getZhqizcqx() {
        return zhqizcqx;
    }

    public void setZhqizcqx(String zhqizcqx) {
        this.zhqizcqx = zhqizcqx;
    }

    public String getFangkulx() {
        return fangkulx;
    }

    public void setFangkulx(String fangkulx) {
        this.fangkulx = fangkulx;
    }

    public String getFkzjclfs() {
        return fkzjclfs;
    }

    public void setFkzjclfs(String fkzjclfs) {
        this.fkzjclfs = fkzjclfs;
    }

    public String getHntmifku() {
        return hntmifku;
    }

    public void setHntmifku(String hntmifku) {
        this.hntmifku = hntmifku;
    }

    public String getHnftmfku() {
        return hnftmfku;
    }

    public void setHnftmfku(String hnftmfku) {
        this.hnftmfku = hnftmfku;
    }

    public String getNeibufku() {
        return neibufku;
    }

    public void setNeibufku(String neibufku) {
        this.neibufku = neibufku;
    }

    public String getJixibjgz() {
        return jixibjgz;
    }

    public void setJixibjgz(String jixibjgz) {
        this.jixibjgz = jixibjgz;
    }

    public String getLixijsff() {
        return lixijsff;
    }

    public void setLixijsff(String lixijsff) {
        this.lixijsff = lixijsff;
    }

    public String getJixitwgz() {
        return jixitwgz;
    }

    public void setJixitwgz(String jixitwgz) {
        this.jixitwgz = jixitwgz;
    }

    public BigDecimal getJixizxje() {
        return jixizxje;
    }

    public void setJixizxje(BigDecimal jixizxje) {
        this.jixizxje = jixizxje;
    }

    public String getJixisrgz() {
        return jixisrgz;
    }

    public void setJixisrgz(String jixisrgz) {
        this.jixisrgz = jixisrgz;
    }

    public String getSrzxdanw() {
        return srzxdanw;
    }

    public void setSrzxdanw(String srzxdanw) {
        this.srzxdanw = srzxdanw;
    }

    public String getFdjixibz() {
        return fdjixibz;
    }

    public void setFdjixibz(String fdjixibz) {
        this.fdjixibz = fdjixibz;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getJitiguiz() {
        return jitiguiz;
    }

    public void setJitiguiz(String jitiguiz) {
        this.jitiguiz = jitiguiz;
    }

    public Integer getZqxizdds() {
        return zqxizdds;
    }

    public void setZqxizdds(Integer zqxizdds) {
        this.zqxizdds = zqxizdds;
    }

    public Integer getWqxizdds() {
        return wqxizdds;
    }

    public void setWqxizdds(Integer wqxizdds) {
        this.wqxizdds = wqxizdds;
    }

    public String getButijejs() {
        return butijejs;
    }

    public void setButijejs(String butijejs) {
        this.butijejs = butijejs;
    }

    public String getTiaozhkf() {
        return tiaozhkf;
    }

    public void setTiaozhkf(String tiaozhkf) {
        this.tiaozhkf = tiaozhkf;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public String getYunxsuoq() {
        return yunxsuoq;
    }

    public void setYunxsuoq(String yunxsuoq) {
        this.yunxsuoq = yunxsuoq;
    }

    public Integer getSuoqcish() {
        return suoqcish;
    }

    public void setSuoqcish(Integer suoqcish) {
        this.suoqcish = suoqcish;
    }

    public String getBzuekkfs() {
        return bzuekkfs;
    }

    public void setBzuekkfs(String bzuekkfs) {
        this.bzuekkfs = bzuekkfs;
    }

    public String getYqbzkkfs() {
        return yqbzkkfs;
    }

    public void setYqbzkkfs(String yqbzkkfs) {
        this.yqbzkkfs = yqbzkkfs;
    }

    public String getHntmihku() {
        return hntmihku;
    }

    public void setHntmihku(String hntmihku) {
        this.hntmihku = hntmihku;
    }

    public String getHnftmhku() {
        return hnftmhku;
    }

    public void setHnftmhku(String hnftmhku) {
        this.hnftmhku = hnftmhku;
    }

    public String getNbuzhhku() {
        return nbuzhhku;
    }

    public void setNbuzhhku(String nbuzhhku) {
        this.nbuzhhku = nbuzhhku;
    }

    public Integer getTiqhksdq() {
        return tiqhksdq;
    }

    public void setTiqhksdq(Integer tiqhksdq) {
        this.tiqhksdq = tiqhksdq;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public String getKxqjixgz() {
        return kxqjixgz;
    }

    public void setKxqjixgz(String kxqjixgz) {
        this.kxqjixgz = kxqjixgz;
    }

    public String getKxqhjxgz() {
        return kxqhjxgz;
    }

    public void setKxqhjxgz(String kxqhjxgz) {
        this.kxqhjxgz = kxqhjxgz;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public List<Lstdzqgjh> getLstdzqgjh() {
        return lstdzqgjh;
    }

    public void setLstdzqgjh(List<Lstdzqgjh> lstdzqgjh) {
        this.lstdzqgjh = lstdzqgjh;
    }

    public String getQglxleix() {
        return qglxleix;
    }

    public void setQglxleix(String qglxleix) {
        this.qglxleix = qglxleix;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getDkczhzhh() {
        return dkczhzhh;
    }

    public void setDkczhzhh(String dkczhzhh) {
        this.dkczhzhh = dkczhzhh;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getShtzfhxm() {
        return shtzfhxm;
    }

    public void setShtzfhxm(String shtzfhxm) {
        this.shtzfhxm = shtzfhxm;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public String getBwhesdma() {
        return bwhesdma;
    }

    public void setBwhesdma(String bwhesdma) {
        this.bwhesdma = bwhesdma;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    public String getYsxlyzhh() {
        return ysxlyzhh;
    }

    public void setYsxlyzhh(String ysxlyzhh) {
        this.ysxlyzhh = ysxlyzhh;
    }

    public String getYsxlyzxh() {
        return ysxlyzxh;
    }

    public void setYsxlyzxh(String ysxlyzxh) {
        this.ysxlyzxh = ysxlyzxh;
    }

    public List<Lstytczfe> getLstytczfe() {
        return lstytczfe;
    }

    public void setLstytczfe(List<Lstytczfe> lstytczfe) {
        this.lstytczfe = lstytczfe;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZyjjcpdm() {
        return zyjjcpdm;
    }

    public void setZyjjcpdm(String zyjjcpdm) {
        this.zyjjcpdm = zyjjcpdm;
    }

    public String getZyjjcpmc() {
        return zyjjcpmc;
    }

    public void setZyjjcpmc(String zyjjcpmc) {
        this.zyjjcpmc = zyjjcpmc;
    }

    public String getFzyjcpdm() {
        return fzyjcpdm;
    }

    public void setFzyjcpdm(String fzyjcpdm) {
        this.fzyjcpdm = fzyjcpdm;
    }

    public String getFzyjcpmc() {
        return fzyjcpmc;
    }

    public void setFzyjcpmc(String fzyjcpmc) {
        this.fzyjcpmc = fzyjcpmc;
    }

    public Integer getDktiansh() {
        return dktiansh;
    }

    public void setDktiansh(Integer dktiansh) {
        this.dktiansh = dktiansh;
    }

    public String getSfpinlvv() {
        return sfpinlvv;
    }

    public void setSfpinlvv(String sfpinlvv) {
        this.sfpinlvv = sfpinlvv;
    }

    public String getSfzhouqi() {
        return sfzhouqi;
    }

    public void setSfzhouqi(String sfzhouqi) {
        this.sfzhouqi = sfzhouqi;
    }

    public String getShoufzhl() {
        return shoufzhl;
    }

    public void setShoufzhl(String shoufzhl) {
        this.shoufzhl = shoufzhl;
    }

    public String getTxzhouqi() {
        return txzhouqi;
    }

    public void setTxzhouqi(String txzhouqi) {
        this.txzhouqi = txzhouqi;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    public String getFufeizhh() {
        return fufeizhh;
    }

    public void setFufeizhh(String fufeizhh) {
        this.fufeizhh = fufeizhh;
    }

    public String getFfzhhzxh() {
        return ffzhhzxh;
    }

    public void setFfzhhzxh(String ffzhhzxh) {
        this.ffzhhzxh = ffzhhzxh;
    }

    public String getFfzhhmch() {
        return ffzhhmch;
    }

    public void setFfzhhmch(String ffzhhmch) {
        this.ffzhhmch = ffzhhmch;
    }

    public String getSfrzhzhh() {
        return sfrzhzhh;
    }

    public void setSfrzhzhh(String sfrzhzhh) {
        this.sfrzhzhh = sfrzhzhh;
    }

    public String getSfrzhzxh() {
        return sfrzhzxh;
    }

    public void setSfrzhzxh(String sfrzhzxh) {
        this.sfrzhzxh = sfrzhzxh;
    }

    public String getMqjqsfbz() {
        return mqjqsfbz;
    }

    public void setMqjqsfbz(String mqjqsfbz) {
        this.mqjqsfbz = mqjqsfbz;
    }

    public String getFybkdqbz() {
        return fybkdqbz;
    }

    public void setFybkdqbz(String fybkdqbz) {
        this.fybkdqbz = fybkdqbz;
    }

    public String getSfljsfei() {
        return sfljsfei;
    }

    public void setSfljsfei(String sfljsfei) {
        this.sfljsfei = sfljsfei;
    }

    public List<Lstdklhmx> getLstdklhmx() {
        return lstdklhmx;
    }

    public void setLstdklhmx(List<Lstdklhmx> lstdklhmx) {
        this.lstdklhmx = lstdklhmx;
    }

    public String getJrlxsybz() {
        return jrlxsybz;
    }

    public void setJrlxsybz(String jrlxsybz) {
        this.jrlxsybz = jrlxsybz;
    }

    public BigDecimal getZdkuanje() {
        return zdkuanje;
    }

    public void setZdkuanje(BigDecimal zdkuanje) {
        this.zdkuanje = zdkuanje;
    }

    public String getShifyyqx() {
        return shifyyqx;
    }

    public void setShifyyqx(String shifyyqx) {
        this.shifyyqx = shifyyqx;
    }

    public Integer getZhanqixh() {
        return zhanqixh;
    }

    public void setZhanqixh(Integer zhanqixh) {
        this.zhanqixh = zhanqixh;
    }

    public String getZhanqhth() {
        return zhanqhth;
    }

    public void setZhanqhth(String zhanqhth) {
        this.zhanqhth = zhanqhth;
    }

    public String getZhanqirq() {
        return zhanqirq;
    }

    public void setZhanqirq(String zhanqirq) {
        this.zhanqirq = zhanqirq;
    }

    public String getZhanqdqr() {
        return zhanqdqr;
    }

    public void setZhanqdqr(String zhanqdqr) {
        this.zhanqdqr = zhanqdqr;
    }

    public BigDecimal getZhanqije() {
        return zhanqije;
    }

    public void setZhanqije(BigDecimal zhanqije) {
        this.zhanqije = zhanqije;
    }

    public String getChuzhhao() {
        return chuzhhao;
    }

    public void setChuzhhao(String chuzhhao) {
        this.chuzhhao = chuzhhao;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getLxirenmc() {
        return lxirenmc;
    }

    public void setLxirenmc(String lxirenmc) {
        this.lxirenmc = lxirenmc;
    }

    public String getLxirendh() {
        return lxirendh;
    }

    public void setLxirendh(String lxirendh) {
        this.lxirendh = lxirendh;
    }

    public String getAbsdkulx() {
        return absdkulx;
    }

    public void setAbsdkulx(String absdkulx) {
        this.absdkulx = absdkulx;
    }

    public List<Lstdkzhzy> getLstdkzhzy() {
        return lstdkzhzy;
    }

    public void setLstdkzhzy(List<Lstdkzhzy> lstdkzhzy) {
        this.lstdkzhzy = lstdkzhzy;
    }

    public String getSfglzhzy() {
        return sfglzhzy;
    }

    public void setSfglzhzy(String sfglzhzy) {
        this.sfglzhzy = sfglzhzy;
    }

    @Override
    public String toString() {
        return "Ln3036RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", dbdkkksx=" + dbdkkksx +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kuaijilb='" + kuaijilb + '\'' +
                ", dkqixian='" + dkqixian + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", hetongje=" + hetongje +
                ", jiejuuje=" + jiejuuje +
                ", zhchbjin=" + zhchbjin +
                ", yuqibjin=" + yuqibjin +
                ", dzhibjin=" + dzhibjin +
                ", daizbjin=" + daizbjin +
                ", ysyjlixi=" + ysyjlixi +
                ", csyjlixi=" + csyjlixi +
                ", ysqianxi=" + ysqianxi +
                ", csqianxi=" + csqianxi +
                ", ysyjfaxi=" + ysyjfaxi +
                ", csyjfaxi=" + csyjfaxi +
                ", yshofaxi=" + yshofaxi +
                ", cshofaxi=" + cshofaxi +
                ", yingjifx=" + yingjifx +
                ", fuxiiiii=" + fuxiiiii +
                ", yingjitx=" + yingjitx +
                ", yingshtx=" + yingshtx +
                ", yingshfy=" + yingshfy +
                ", yingshfj=" + yingshfj +
                ", kehmingc='" + kehmingc + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", fkfangsh='" + fkfangsh + '\'' +
                ", jxhjdkkz='" + jxhjdkkz + '\'' +
                ", jxhjhkkz='" + jxhjhkkz + '\'' +
                ", hjiuleix='" + hjiuleix + '\'' +
                ", lstydkjjh=" + lstydkjjh +
                ", zidfkbzh='" + zidfkbzh + '\'' +
                ", zhqifkbz='" + zhqifkbz + '\'' +
                ", dzhifkjh='" + dzhifkjh + '\'' +
                ", yxtsfkbz='" + yxtsfkbz + '\'' +
                ", zdfkjjms='" + zdfkjjms + '\'' +
                ", mcfkjebl=" + mcfkjebl +
                ", bencfkje=" + bencfkje +
                ", dkrzhzhh='" + dkrzhzhh + '\'' +
                ", dkrzhzxh='" + dkrzhzxh + '\'' +
                ", zjzrzhmc='" + zjzrzhmc + '\'' +
                ", hkzhhmch='" + hkzhhmch + '\'' +
                ", fkzhouqi='" + fkzhouqi + '\'' +
                ", kxqjjrgz='" + kxqjjrgz + '\'' +
                ", jixibzhi='" + jixibzhi + '\'' +
                ", jfxibzhi='" + jfxibzhi + '\'' +
                ", fxjxbzhi='" + fxjxbzhi + '\'' +
                ", jixiguiz='" + jixiguiz + '\'' +
                ", kxqshxgz='" + kxqshxgz + '\'' +
                ", kxqzyqgz='" + kxqzyqgz + '\'' +
                ", lilvfdfs='" + lilvfdfs + '\'' +
                ", yqfxfdfs='" + yqfxfdfs + '\'' +
                ", lilvleix='" + lilvleix + '\'' +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", llqxkdfs='" + llqxkdfs + '\'' +
                ", ltjixigz='" + ltjixigz + '\'' +
                ", meictxfs='" + meictxfs + '\'' +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", yuqinyll='" + yuqinyll + '\'' +
                ", lilvfend='" + lilvfend + '\'' +
                ", zaoqixbz='" + zaoqixbz + '\'' +
                ", wanqixbz='" + wanqixbz + '\'' +
                ", tiexibzh='" + tiexibzh + '\'' +
                ", whlilvzl='" + whlilvzl + '\'' +
                ", whllqxzl='" + whllqxzl + '\'' +
                ", yushxfsh='" + yushxfsh + '\'' +
                ", meictxbl=" + meictxbl +
                ", kxqzdcsh=" + kxqzdcsh +
                ", zclilvbh='" + zclilvbh + '\'' +
                ", yuqillbh='" + yuqillbh + '\'' +
                ", zhchlilv=" + zhchlilv +
                ", zhxnlilv=" + zhxnlilv +
                ", lilvfdzh=" + lilvfdzh +
                ", yqfxfdzh=" + yqfxfdzh +
                ", yuqililv=" + yuqililv +
                ", lilvqixx='" + lilvqixx + '\'' +
                ", yushxize=" + yushxize +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", lixitxzq='" + lixitxzq + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", dechligz='" + dechligz + '\'' +
                ", qigscfsh='" + qigscfsh + '\'' +
                ", duophkbz='" + duophkbz + '\'' +
                ", dzhhkjih='" + dzhhkjih + '\'' +
                ", zdkoukbz='" + zdkoukbz + '\'' +
                ", zdplkkbz='" + zdplkkbz + '\'' +
                ", zdjqdkbz='" + zdjqdkbz + '\'' +
                ", dhkzhhbz='" + dhkzhhbz + '\'' +
                ", qyxhdkbz='" + qyxhdkbz + '\'' +
                ", qxbgtzjh='" + qxbgtzjh + '\'' +
                ", llbgtzjh='" + llbgtzjh + '\'' +
                ", dcfktzjh='" + dcfktzjh + '\'' +
                ", tqhktzjh='" + tqhktzjh + '\'' +
                ", yunxtqhk='" + yunxtqhk + '\'' +
                ", hezuofbh='" + hezuofbh + '\'' +
                ", hezuofmc='" + hezuofmc + '\'' +
                ", xycihkrq='" + xycihkrq + '\'' +
                ", leijqjsh=" + leijqjsh +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", huankzhh='" + huankzhh + '\'' +
                ", xhdkqyzh='" + xhdkqyzh + '\'' +
                ", hkqixian='" + hkqixian + '\'' +
                ", hkshxubh='" + hkshxubh + '\'' +
                ", leijinzh=" + leijinzh +
                ", baoliuje=" + baoliuje +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", xhdkzhxh='" + xhdkzhxh + '\'' +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", huanbzhq='" + huanbzhq + '\'' +
                ", yuqhkzhq='" + yuqhkzhq + '\'' +
                ", zbjrsygz='" + zbjrsygz + '\'' +
                ", zhqxzekk='" + zhqxzekk + '\'' +
                ", yunxdkzq='" + yunxdkzq + '\'' +
                ", zhqzdcsh=" + zhqzdcsh +
                ", zqgzbhao='" + zqgzbhao + '\'' +
                ", dlhesfsh='" + dlhesfsh + '\'' +
                ", dlxxqzgz='" + dlxxqzgz + '\'' +
                ", dlxxzdgz='" + dlxxzdgz + '\'' +
                ", wtckywbm='" + wtckywbm + '\'' +
                ", duowtrbz='" + duowtrbz + '\'' +
                ", lstdkwtxx=" + lstdkwtxx +
                ", dailixuh=" + dailixuh +
                ", wtrkehuh='" + wtrkehuh + '\'' +
                ", wtrckuzh='" + wtrckuzh + '\'' +
                ", wtckzhao='" + wtckzhao + '\'' +
                ", bjghrzzh='" + bjghrzzh + '\'' +
                ", lxghrzzh='" + lxghrzzh + '\'' +
                ", dailimsh='" + dailimsh + '\'' +
                ", wtrckzxh='" + wtrckzxh + '\'' +
                ", wtckzixh='" + wtckzixh + '\'' +
                ", bjghrzxh='" + bjghrzxh + '\'' +
                ", lxghrzxh='" + lxghrzxh + '\'' +
                ", baozjzxh='" + baozjzxh + '\'' +
                ", daikduix='" + daikduix + '\'' +
                ", yewufenl='" + yewufenl + '\'' +
                ", ysywleix='" + ysywleix + '\'' +
                ", ysywbhao='" + ysywbhao + '\'' +
                ", hesuanfs='" + hesuanfs + '\'' +
                ", lixizcgz='" + lixizcgz + '\'' +
                ", lixizhgz='" + lixizhgz + '\'' +
                ", yjfyjhes='" + yjfyjhes + '\'' +
                ", yiyldhes='" + yiyldhes + '\'' +
                ", dkxtkmhs='" + dkxtkmhs + '\'' +
                ", zidxtzhy='" + zidxtzhy + '\'' +
                ", zhchtqtz='" + zhchtqtz + '\'' +
                ", yqcshtzh='" + yqcshtzh + '\'' +
                ", lilvbgtz='" + lilvbgtz + '\'' +
                ", yuebgtzh='" + yuebgtzh + '\'' +
                ", tzhtqtsh=" + tzhtqtsh +
                ", tzhjgtsh=" + tzhjgtsh +
                ", shfleixi='" + shfleixi + '\'' +
                ", tqhkfjbh='" + tqhkfjbh + '\'' +
                ", tqhkfjmc='" + tqhkfjmc + '\'' +
                ", tqhkfjfj=" + tqhkfjfj +
                ", pingzhzl='" + pingzhzl + '\'' +
                ", sydkcnuo='" + sydkcnuo + '\'' +
                ", lmdkbzhi='" + lmdkbzhi + '\'' +
                ", bzhrdbbz='" + bzhrdbbz + '\'' +
                ", lstdkzhbz=" + lstdkzhbz +
                ", wujiflbz='" + wujiflbz + '\'' +
                ", wujiflrq='" + wujiflrq + '\'' +
                ", cndkjjho='" + cndkjjho + '\'' +
                ", dkgljgou='" + dkgljgou + '\'' +
                ", gljgleib='" + gljgleib + '\'' +
                ", fuhejgou='" + fuhejgou + '\'' +
                ", khjingli='" + khjingli + '\'' +
                ", pingzhma='" + pingzhma + '\'' +
                ", pngzxhao='" + pngzxhao + '\'' +
                ", sfjnyhsh='" + sfjnyhsh + '\'' +
                ", yinhshlv=" + yinhshlv +
                ", yinhshje=" + yinhshje +
                ", edbiahao='" + edbiahao + '\'' +
                ", shynedbz='" + shynedbz + '\'' +
                ", edbizhgz='" + edbizhgz + '\'' +
                ", edzdbizh='" + edzdbizh + '\'' +
                ", edshyngz='" + edshyngz + '\'' +
                ", hetongll=" + hetongll +
                ", qixitshu=" + qixitshu +
                ", chanpzdm='" + chanpzdm + '\'' +
                ", chanpzmc='" + chanpzmc + '\'' +
                ", lstdkfkjh=" + lstdkfkjh +
                ", lstdkfwdj=" + lstdkfwdj +
                ", lstdkhbjh=" + lstdkhbjh +
                ", benjinfd='" + benjinfd + '\'' +
                ", lstdkbjfd=" + lstdkbjfd +
                ", lstdkhkfs=" + lstdkhkfs +
                ", lstdkhkzh=" + lstdkhkzh +
                ", lstdkllfd=" + lstdkllfd +
                ", lstdksfsj=" + lstdksfsj +
                ", lstdkstzf=" + lstdkstzf +
                ", lstdktxzh=" + lstdktxzh +
                ", lstdkzhlm=" + lstdkzhlm +
                ", fafangje=" + fafangje +
                ", djiekfje=" + djiekfje +
                ", kffangje=" + kffangje +
                ", yewusx01='" + yewusx01 + '\'' +
                ", yewusx02='" + yewusx02 + '\'' +
                ", yewusx03='" + yewusx03 + '\'' +
                ", yewusx04='" + yewusx04 + '\'' +
                ", yewusx05='" + yewusx05 + '\'' +
                ", yewusx06='" + yewusx06 + '\'' +
                ", yewusx07='" + yewusx07 + '\'' +
                ", yewusx08='" + yewusx08 + '\'' +
                ", yewusx09='" + yewusx09 + '\'' +
                ", yewusx10='" + yewusx10 + '\'' +
                ", ywsxms01='" + ywsxms01 + '\'' +
                ", ywsxms02='" + ywsxms02 + '\'' +
                ", ywsxms03='" + ywsxms03 + '\'' +
                ", ywsxms04='" + ywsxms04 + '\'' +
                ", ywsxms05='" + ywsxms05 + '\'' +
                ", ywsxms06='" + ywsxms06 + '\'' +
                ", ywsxms07='" + ywsxms07 + '\'' +
                ", ywsxms08='" + ywsxms08 + '\'' +
                ", ywsxms09='" + ywsxms09 + '\'' +
                ", ywsxms10='" + ywsxms10 + '\'' +
                ", daikdxxf='" + daikdxxf + '\'' +
                ", bwchapbz='" + bwchapbz + '\'' +
                ", xunhdaik='" + xunhdaik + '\'' +
                ", yansdaik='" + yansdaik + '\'' +
                ", chendaik='" + chendaik + '\'' +
                ", cnkxhbzh='" + cnkxhbzh + '\'' +
                ", butidaik='" + butidaik + '\'' +
                ", yintdkbz='" + yintdkbz + '\'' +
                ", yintdkfs='" + yintdkfs + '\'' +
                ", yintleib='" + yintleib + '\'' +
                ", yintnbcy='" + yintnbcy + '\'' +
                ", yintwbcy='" + yintwbcy + '\'' +
                ", jiangulx='" + jiangulx + '\'' +
                ", zhqizcqx='" + zhqizcqx + '\'' +
                ", fangkulx='" + fangkulx + '\'' +
                ", fkzjclfs='" + fkzjclfs + '\'' +
                ", hntmifku='" + hntmifku + '\'' +
                ", hnftmfku='" + hnftmfku + '\'' +
                ", neibufku='" + neibufku + '\'' +
                ", jixibjgz='" + jixibjgz + '\'' +
                ", lixijsff='" + lixijsff + '\'' +
                ", jixitwgz='" + jixitwgz + '\'' +
                ", jixizxje=" + jixizxje +
                ", jixisrgz='" + jixisrgz + '\'' +
                ", srzxdanw='" + srzxdanw + '\'' +
                ", fdjixibz='" + fdjixibz + '\'' +
                ", yuqitzfs='" + yuqitzfs + '\'' +
                ", yuqitzzq='" + yuqitzzq + '\'' +
                ", fulilvbh='" + fulilvbh + '\'' +
                ", fulilvny='" + fulilvny + '\'' +
                ", fulililv=" + fulililv +
                ", fulitzfs='" + fulitzfs + '\'' +
                ", fulitzzq='" + fulitzzq + '\'' +
                ", fulifdfs='" + fulifdfs + '\'' +
                ", fulifdzh=" + fulifdzh +
                ", jitiguiz='" + jitiguiz + '\'' +
                ", zqxizdds=" + zqxizdds +
                ", wqxizdds=" + wqxizdds +
                ", butijejs='" + butijejs + '\'' +
                ", tiaozhkf='" + tiaozhkf + '\'' +
                ", scihkrbz='" + scihkrbz + '\'' +
                ", mqihkfsh='" + mqihkfsh + '\'' +
                ", yunxsuoq='" + yunxsuoq + '\'' +
                ", suoqcish=" + suoqcish +
                ", bzuekkfs='" + bzuekkfs + '\'' +
                ", yqbzkkfs='" + yqbzkkfs + '\'' +
                ", hntmihku='" + hntmihku + '\'' +
                ", hnftmhku='" + hnftmhku + '\'' +
                ", nbuzhhku='" + nbuzhhku + '\'' +
                ", tiqhksdq=" + tiqhksdq +
                ", sfyxkuxq='" + sfyxkuxq + '\'' +
                ", kuanxqts=" + kuanxqts +
                ", kxqjixgz='" + kxqjixgz + '\'' +
                ", kxqhjxgz='" + kxqhjxgz + '\'' +
                ", yqllcklx='" + yqllcklx + '\'' +
                ", flllcklx='" + flllcklx + '\'' +
                ", lstdzqgjh=" + lstdzqgjh +
                ", qglxleix='" + qglxleix + '\'' +
                ", kshchpdm='" + kshchpdm + '\'' +
                ", kshchpmc='" + kshchpmc + '\'' +
                ", dkczhzhh='" + dkczhzhh + '\'' +
                ", dkdbfshi='" + dkdbfshi + '\'' +
                ", dkyongtu='" + dkyongtu + '\'' +
                ", shtzfhxm='" + shtzfhxm + '\'' +
                ", fkjzhfsh='" + fkjzhfsh + '\'' +
                ", bwhesdma='" + bwhesdma + '\'' +
                ", wtrmingc='" + wtrmingc + '\'' +
                ", ysxlyzhh='" + ysxlyzhh + '\'' +
                ", ysxlyzxh='" + ysxlyzxh + '\'' +
                ", lstytczfe=" + lstytczfe +
                ", zjlyzhao='" + zjlyzhao + '\'' +
                ", zjlyzzxh='" + zjlyzzxh + '\'' +
                ", zyjjcpdm='" + zyjjcpdm + '\'' +
                ", zyjjcpmc='" + zyjjcpmc + '\'' +
                ", fzyjcpdm='" + fzyjcpdm + '\'' +
                ", fzyjcpmc='" + fzyjcpmc + '\'' +
                ", dktiansh=" + dktiansh +
                ", sfpinlvv='" + sfpinlvv + '\'' +
                ", sfzhouqi='" + sfzhouqi + '\'' +
                ", shoufzhl='" + shoufzhl + '\'' +
                ", txzhouqi='" + txzhouqi + '\'' +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", shoufdma='" + shoufdma + '\'' +
                ", shfdmamc='" + shfdmamc + '\'' +
                ", shoufjee=" + shoufjee +
                ", fufeizhh='" + fufeizhh + '\'' +
                ", ffzhhzxh='" + ffzhhzxh + '\'' +
                ", ffzhhmch='" + ffzhhmch + '\'' +
                ", sfrzhzhh='" + sfrzhzhh + '\'' +
                ", sfrzhzxh='" + sfrzhzxh + '\'' +
                ", mqjqsfbz='" + mqjqsfbz + '\'' +
                ", fybkdqbz='" + fybkdqbz + '\'' +
                ", sfljsfei='" + sfljsfei + '\'' +
                ", lstdklhmx=" + lstdklhmx +
                ", jrlxsybz='" + jrlxsybz + '\'' +
                ", zdkuanje=" + zdkuanje +
                ", shifyyqx='" + shifyyqx + '\'' +
                ", zhanqixh=" + zhanqixh +
                ", zhanqhth='" + zhanqhth + '\'' +
                ", zhanqirq='" + zhanqirq + '\'' +
                ", zhanqdqr='" + zhanqdqr + '\'' +
                ", zhanqije=" + zhanqije +
                ", chuzhhao='" + chuzhhao + '\'' +
                ", benqqish=" + benqqish +
                ", lxirenmc='" + lxirenmc + '\'' +
                ", lxirendh='" + lxirendh + '\'' +
                ", absdkulx='" + absdkulx + '\'' +
                ", lstdkzhzy=" + lstdkzhzy +
                ", sfglzhzy='" + sfglzhzy + '\'' +
                '}';
    }
}
