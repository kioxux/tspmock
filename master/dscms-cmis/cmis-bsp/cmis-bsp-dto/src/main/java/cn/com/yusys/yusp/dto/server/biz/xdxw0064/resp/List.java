package cn.com.yusys.yusp.dto.server.biz.xdxw0064.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：优企贷、优农贷授信调查信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "survey_serno")
    private String survey_serno;//主键
    @JsonProperty(value = "loan_type")
    private String loan_type;//1综合消费贷2汽车消费贷3经营性小贷4经营性微贷
    @JsonProperty(value = "zb_manager_id")
    private String zb_manager_id;//主办客户经理号
    @JsonProperty(value = "xb_manager_id")
    private String xb_manager_id;//现在存的是协办客户经理中文名
    @JsonProperty(value = "input_date")
    private String input_date;//检查日期
    @JsonProperty(value = "approve_status")
    private String approve_status;//审批状态
    @JsonProperty(value = "biz_from")
    private String biz_from;//业务来源XD_BIZ_FROM
    @JsonProperty(value = "tjr")
    private String tjr;//推荐人
    @JsonProperty(value = "biz_type")
    private String biz_type;//业务类型
    @JsonProperty(value = "copy_from")
    private String copy_from;//从哪复制
    @JsonProperty(value = "last_update_time")
    private String last_update_time;//最后一次审批时间
    @JsonProperty(value = "zffs")
    private String zffs;//支付方式
    @JsonProperty(value = "cont_type")
    private String cont_type;//合同类型S
    @JsonProperty(value = "bcsx_amt")
    private String bcsx_amt;//本次授信金额
    @JsonProperty(value = "bcyx_amt")
    private String bcyx_amt;//本次用信金额
    @JsonProperty(value = "is_sjsh")
    private String is_sjsh;//是否随借随还
    @JsonProperty(value = "iflag")
    private String iflag;//撤销标志
    @JsonProperty(value = "xf_org")
    private String xf_org;//取的s_user表中的xf_orgid

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getLoan_type() {
        return loan_type;
    }

    public void setLoan_type(String loan_type) {
        this.loan_type = loan_type;
    }

    public String getZb_manager_id() {
        return zb_manager_id;
    }

    public void setZb_manager_id(String zb_manager_id) {
        this.zb_manager_id = zb_manager_id;
    }

    public String getXb_manager_id() {
        return xb_manager_id;
    }

    public void setXb_manager_id(String xb_manager_id) {
        this.xb_manager_id = xb_manager_id;
    }

    public String getInput_date() {
        return input_date;
    }

    public void setInput_date(String input_date) {
        this.input_date = input_date;
    }

    public String getApprove_status() {
        return approve_status;
    }

    public void setApprove_status(String approve_status) {
        this.approve_status = approve_status;
    }

    public String getBiz_from() {
        return biz_from;
    }

    public void setBiz_from(String biz_from) {
        this.biz_from = biz_from;
    }

    public String getTjr() {
        return tjr;
    }

    public void setTjr(String tjr) {
        this.tjr = tjr;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getCopy_from() {
        return copy_from;
    }

    public void setCopy_from(String copy_from) {
        this.copy_from = copy_from;
    }

    public String getLast_update_time() {
        return last_update_time;
    }

    public void setLast_update_time(String last_update_time) {
        this.last_update_time = last_update_time;
    }

    public String getZffs() {
        return zffs;
    }

    public void setZffs(String zffs) {
        this.zffs = zffs;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getBcsx_amt() {
        return bcsx_amt;
    }

    public void setBcsx_amt(String bcsx_amt) {
        this.bcsx_amt = bcsx_amt;
    }

    public String getBcyx_amt() {
        return bcyx_amt;
    }

    public void setBcyx_amt(String bcyx_amt) {
        this.bcyx_amt = bcyx_amt;
    }

    public String getIs_sjsh() {
        return is_sjsh;
    }

    public void setIs_sjsh(String is_sjsh) {
        this.is_sjsh = is_sjsh;
    }

    public String getIflag() {
        return iflag;
    }

    public void setIflag(String iflag) {
        this.iflag = iflag;
    }

    public String getXf_org() {
        return xf_org;
    }

    public void setXf_org(String xf_org) {
        this.xf_org = xf_org;
    }

    @Override
    public String toString() {
        return "List{" +
                "survey_serno='" + survey_serno + '\'' +
                ", loan_type='" + loan_type + '\'' +
                ", zb_manager_id='" + zb_manager_id + '\'' +
                ", xb_manager_id='" + xb_manager_id + '\'' +
                ", input_date='" + input_date + '\'' +
                ", approve_status='" + approve_status + '\'' +
                ", biz_from='" + biz_from + '\'' +
                ", tjr='" + tjr + '\'' +
                ", biz_type='" + biz_type + '\'' +
                ", copy_from='" + copy_from + '\'' +
                ", last_update_time='" + last_update_time + '\'' +
                ", zffs='" + zffs + '\'' +
                ", cont_type='" + cont_type + '\'' +
                ", bcsx_amt='" + bcsx_amt + '\'' +
                ", bcyx_amt='" + bcyx_amt + '\'' +
                ", is_sjsh='" + is_sjsh + '\'' +
                ", iflag='" + iflag + '\'' +
                ", xf_org='" + xf_org + '\'' +
                '}';
    }
}
