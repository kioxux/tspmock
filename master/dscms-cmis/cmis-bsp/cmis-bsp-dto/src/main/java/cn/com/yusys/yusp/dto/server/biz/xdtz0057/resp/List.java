package cn.com.yusys.yusp.dto.server.biz.xdtz0057.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 响应Dto：根据流水号查询客户调查的放款信息（在途需求）
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//借据余额
    @JsonProperty(value = "accMgr")
    private String accMgr;//业务负责人
    @JsonProperty(value = "cusMgr")
    private String cusMgr;//管户经理

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getAccMgr() {
        return accMgr;
    }

    public void setAccMgr(String accMgr) {
        this.accMgr = accMgr;
    }

    public String getCusMgr() {
        return cusMgr;
    }

    public void setCusMgr(String cusMgr) {
        this.cusMgr = cusMgr;
    }

    @Override
    public String toString() {
        return "Xdtz0057RespDto{" +
                "billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", loanBalance=" + loanBalance +
                ", accMgr='" + accMgr + '\'' +
                ", cusMgr='" + cusMgr + '\'' +
                '}';
    }
}
