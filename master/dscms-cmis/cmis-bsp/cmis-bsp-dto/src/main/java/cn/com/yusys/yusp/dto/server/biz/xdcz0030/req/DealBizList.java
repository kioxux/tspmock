package cn.com.yusys.yusp.dto.server.biz.xdcz0030.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/16 17:05
 * @since 2021/7/16 17:05
 */
@JsonPropertyOrder(alphabetic = true)
public class DealBizList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;//台账编号
    @JsonProperty(value = "isFollowBiz")
    private String isFollowBiz;//是否无缝衔接
    @JsonProperty(value = "origiDealBizNo")
    private String origiDealBizNo;//原台账编号
    @JsonProperty(value = "origiRecoverType")
    private String origiRecoverType;//原交易业务恢复类型
    @JsonProperty(value = "prdNo")
    private String prdNo;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "dealBizAmtCny")
    private BigDecimal dealBizAmtCny;//台账总额
    @JsonProperty(value = "dealBizSpacAmtCny")
    private BigDecimal dealBizSpacAmtCny;//台账敞口
    @JsonProperty(value = "startDate")
    private String startDate;//合同起始日
    @JsonProperty(value = "endDate")
    private String endDate;//合同到期日

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getIsFollowBiz() {
        return isFollowBiz;
    }

    public void setIsFollowBiz(String isFollowBiz) {
        this.isFollowBiz = isFollowBiz;
    }

    public String getOrigiDealBizNo() {
        return origiDealBizNo;
    }

    public void setOrigiDealBizNo(String origiDealBizNo) {
        this.origiDealBizNo = origiDealBizNo;
    }

    public String getOrigiRecoverType() {
        return origiRecoverType;
    }

    public void setOrigiRecoverType(String origiRecoverType) {
        this.origiRecoverType = origiRecoverType;
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public BigDecimal getDealBizAmtCny() {
        return dealBizAmtCny;
    }

    public void setDealBizAmtCny(BigDecimal dealBizAmtCny) {
        this.dealBizAmtCny = dealBizAmtCny;
    }

    public BigDecimal getDealBizSpacAmtCny() {
        return dealBizSpacAmtCny;
    }

    public void setDealBizSpacAmtCny(BigDecimal dealBizSpacAmtCny) {
        this.dealBizSpacAmtCny = dealBizSpacAmtCny;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "DealBizList{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", isFollowBiz='" + isFollowBiz + '\'' +
                ", origiDealBizNo='" + origiDealBizNo + '\'' +
                ", origiRecoverType='" + origiRecoverType + '\'' +
                ", prdNo='" + prdNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", dealBizAmtCny=" + dealBizAmtCny +
                ", dealBizSpacAmtCny=" + dealBizSpacAmtCny +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
