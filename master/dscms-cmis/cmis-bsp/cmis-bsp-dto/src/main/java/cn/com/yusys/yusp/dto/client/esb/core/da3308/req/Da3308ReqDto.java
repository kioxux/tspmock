package cn.com.yusys.yusp.dto.client.esb.core.da3308.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：抵债资产费用管理
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3308ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "shoufjee")
    private BigDecimal shoufjee;//收费金额
    @JsonProperty(value = "sfrzhzhh")
    private String sfrzhzhh;//收费入账账号
    @JsonProperty(value = "sfrzhzxh")
    private String sfrzhzxh;//收费入账账号子序号
    @JsonProperty(value = "fufeizhh")
    private String fufeizhh;//付费账号
    @JsonProperty(value = "ffzhhzxh")
    private String ffzhhzxh;//付费账号子序号
    @JsonProperty(value = "shifoubz")
    private String shifoubz;//费用挂账标志
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注
    @JsonProperty(value = "dzzcfylx")
    private String dzzcfylx;//抵债资产费用类型
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    public String getSfrzhzhh() {
        return sfrzhzhh;
    }

    public void setSfrzhzhh(String sfrzhzhh) {
        this.sfrzhzhh = sfrzhzhh;
    }

    public String getSfrzhzxh() {
        return sfrzhzxh;
    }

    public void setSfrzhzxh(String sfrzhzxh) {
        this.sfrzhzxh = sfrzhzxh;
    }

    public String getFufeizhh() {
        return fufeizhh;
    }

    public void setFufeizhh(String fufeizhh) {
        this.fufeizhh = fufeizhh;
    }

    public String getFfzhhzxh() {
        return ffzhhzxh;
    }

    public void setFfzhhzxh(String ffzhhzxh) {
        this.ffzhhzxh = ffzhhzxh;
    }

    public String getShifoubz() {
        return shifoubz;
    }

    public void setShifoubz(String shifoubz) {
        this.shifoubz = shifoubz;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getDzzcfylx() {
        return dzzcfylx;
    }

    public void setDzzcfylx(String dzzcfylx) {
        this.dzzcfylx = dzzcfylx;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    @Override
    public String toString() {
        return "Da3308ReqDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "shoufjee='" + shoufjee + '\'' +
                "sfrzhzhh='" + sfrzhzhh + '\'' +
                "sfrzhzxh='" + sfrzhzxh + '\'' +
                "fufeizhh='" + fufeizhh + '\'' +
                "ffzhhzxh='" + ffzhhzxh + '\'' +
                "shifoubz='" + shifoubz + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "dzzcfylx='" + dzzcfylx + '\'' +
                "zijnlaiy='" + zijnlaiy + '\'' +
                '}';
    }
}  
