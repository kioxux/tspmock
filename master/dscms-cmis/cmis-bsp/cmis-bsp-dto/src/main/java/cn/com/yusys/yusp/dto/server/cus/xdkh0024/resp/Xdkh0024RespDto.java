package cn.com.yusys.yusp.dto.server.cus.xdkh0024.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：优企贷、优农贷客户基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0024RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdkh0024RespDto{" +
                "data=" + data +
                '}';
    }
}