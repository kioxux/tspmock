package cn.com.yusys.yusp.dto.client.esb.core.da3303;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：抵债资产信息维护
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3303ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "dzwzleib")
    private String dzwzleib;//抵债物资类别
    @JsonProperty(value = "cqzmzlei")
    private String cqzmzlei;//产权证明种类
    @JsonProperty(value = "daizdzzc")
    private BigDecimal daizdzzc;//待转抵债资产
    @JsonProperty(value = "dcldzzic")
    private BigDecimal dcldzzic;//待处理抵债资产
    @JsonProperty(value = "dbxdzzic")
    private BigDecimal dbxdzzic;//待变现抵债资产
    @JsonProperty(value = "hfeijine")
    private BigDecimal hfeijine;//还费金额
    @JsonProperty(value = "huanbjee")
    private BigDecimal huanbjee;//还本金额
    @JsonProperty(value = "hxijinee")
    private BigDecimal hxijinee;//还息金额
    @JsonProperty(value = "hqitjine")
    private BigDecimal hqitjine;//还其它金额
    @JsonProperty(value = "lstweihsr")
    private java.util.List<Lstweihsr> lstweihsr;//抵债资产维护输入


    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getDzwzleib() {
        return dzwzleib;
    }

    public void setDzwzleib(String dzwzleib) {
        this.dzwzleib = dzwzleib;
    }

    public String getCqzmzlei() {
        return cqzmzlei;
    }

    public void setCqzmzlei(String cqzmzlei) {
        this.cqzmzlei = cqzmzlei;
    }

    public BigDecimal getDaizdzzc() {
        return daizdzzc;
    }

    public void setDaizdzzc(BigDecimal daizdzzc) {
        this.daizdzzc = daizdzzc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public BigDecimal getHqitjine() {
        return hqitjine;
    }

    public void setHqitjine(BigDecimal hqitjine) {
        this.hqitjine = hqitjine;
    }

    public List<Lstweihsr> getLstweihsr() {
        return lstweihsr;
    }

    public void setLstweihsr(List<Lstweihsr> lstweihsr) {
        this.lstweihsr = lstweihsr;
    }

    @Override
    public String toString() {
        return "Da3303ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", dzwzleib='" + dzwzleib + '\'' +
                ", cqzmzlei='" + cqzmzlei + '\'' +
                ", daizdzzc=" + daizdzzc +
                ", dcldzzic=" + dcldzzic +
                ", dbxdzzic=" + dbxdzzic +
                ", hfeijine=" + hfeijine +
                ", huanbjee=" + huanbjee +
                ", hxijinee=" + hxijinee +
                ", hqitjine=" + hqitjine +
                ", lstweihsr=" + lstweihsr +
                '}';
    }
}
