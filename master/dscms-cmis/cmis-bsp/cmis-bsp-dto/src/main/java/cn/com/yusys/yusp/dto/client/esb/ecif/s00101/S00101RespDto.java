package cn.com.yusys.yusp.dto.client.esb.ecif.s00101;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：对私客户综合信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class S00101RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custno")
    private String custno;//客户编号
    @JsonProperty(value = "custna")
    private String custna;//客户名称
    @JsonProperty(value = "idtftp")
    private String idtftp;//证件类型
    @JsonProperty(value = "idtfno")
    private String idtfno;//证件号码
    @JsonProperty(value = "efctdt")
    private String efctdt;//证件生效日期
    @JsonProperty(value = "inefdt")
    private String inefdt;//证件失效日期
    @JsonProperty(value = "auxidtftp")
    private String auxidtftp;//辅证件类型
    @JsonProperty(value = "auxidtfno")
    private String auxidtfno;//辅证件号码
    @JsonProperty(value = "auxinefdt")
    private String auxinefdt;//辅证件到期日
    @JsonProperty(value = "areaad")
    private String areaad;//发证机关国家或地区
    @JsonProperty(value = "depart")
    private String depart;//发证机关
    @JsonProperty(value = "custst")
    private String custst;//客户状态
    @JsonProperty(value = "csspna")
    private String csspna;//拼音名称
    @JsonProperty(value = "csenna")
    private String csenna;//客户英文名
    @JsonProperty(value = "prpsex")
    private String prpsex;//性别
    @JsonProperty(value = "natvpc")
    private String natvpc;//籍贯
    @JsonProperty(value = "ethnic")
    private String ethnic;//民族
    @JsonProperty(value = "nation")
    private String nation;//国籍
    @JsonProperty(value = "borndt")
    private String borndt;//出生日期
    @JsonProperty(value = "polist")
    private String polist;//政治面貌
    @JsonProperty(value = "marist")
    private String marist;//婚姻状态
    @JsonProperty(value = "havetg")
    private String havetg;//是否有子女
    @JsonProperty(value = "brinnm")
    private BigDecimal brinnm;//抚养人数
    @JsonProperty(value = "educlv")
    private String educlv;//教育水平（学历）
    @JsonProperty(value = "indidp")
    private String indidp;//学位
    @JsonProperty(value = "gradna")
    private String gradna;//毕业院校
    @JsonProperty(value = "graddt")
    private String graddt;//毕业日期
    @JsonProperty(value = "projob")
    private String projob;//职业
    @JsonProperty(value = "poston")
    private String poston;//职务
    @JsonProperty(value = "posttl")
    private String posttl;//职称
    @JsonProperty(value = "offibr")
    private String offibr;//任职部门
    @JsonProperty(value = "wkutna")
    private String wkutna;//工作单位
    @JsonProperty(value = "unquty")
    private String unquty;//单位性质
    @JsonProperty(value = "compad")
    private String compad;//单位地址
    @JsonProperty(value = "unitnm")
    private Integer unitnm;//工作单位雇员人数
    @JsonProperty(value = "workdt")
    private String workdt;//参加工作日期
    @JsonProperty(value = "idwkdt")
    private String idwkdt;//参加本行业工作日期
    @JsonProperty(value = "utwkdt")
    private String utwkdt;//参加本单位工作日期
    @JsonProperty(value = "corppr")
    private String corppr;//单位所属行业
    @JsonProperty(value = "expryr")
    private BigDecimal expryr;//行业经验年限
    @JsonProperty(value = "utwkyr")
    private BigDecimal utwkyr;//现单位工作年限
    @JsonProperty(value = "ctcktg")
    private String ctcktg;//是否联网核查
    @JsonProperty(value = "result")
    private String result;//联网核查结果
    @JsonProperty(value = "creabr")
    private String creabr;//创建机构
    @JsonProperty(value = "usrsid")
    private String usrsid;//创建柜员
    @JsonProperty(value = "opendt")
    private String opendt;//创建日期
    @JsonProperty(value = "livest")
    private String livest;//居住状况
    @JsonProperty(value = "lvbgdt")
    private String lvbgdt;//本城市居住起始日期
    @JsonProperty(value = "famisz")
    private Integer famisz;//家庭人口
    @JsonProperty(value = "mainic")
    private String mainic;//主要收入来源
    @JsonProperty(value = "currcy")
    private String currcy;//收入币种
    @JsonProperty(value = "prmnic")
    private BigDecimal prmnic;//个人月收入
    @JsonProperty(value = "pryric")
    private BigDecimal pryric;//个人年收入
    @JsonProperty(value = "fmmnic")
    private BigDecimal fmmnic;//家庭月收入
    @JsonProperty(value = "fmyric")
    private BigDecimal fmyric;//家庭年收入
    @JsonProperty(value = "empltg")
    private String empltg;//本行员工标志
    @JsonProperty(value = "ctmgno")
    private String ctmgno;//本行员工号
    @JsonProperty(value = "farmtg")
    private String farmtg;//是否农户
    @JsonProperty(value = "sthdfg")
    private String sthdfg;//是否我行股东
    @JsonProperty(value = "relgon")
    private String relgon;//宗教信仰
    @JsonProperty(value = "proptg")
    private String proptg;//居民标志
    @JsonProperty(value = "onacnm")
    private BigDecimal onacnm;//一类户数量
    @JsonProperty(value = "totlnm")
    private BigDecimal totlnm;//总户数
    @JsonProperty(value = "amlrat")
    private String amlrat;//反洗钱评级
    @JsonProperty(value = "resiyr")
    private BigDecimal resiyr;//居住年限
    @JsonProperty(value = "healst")
    private String healst;//健康状况
    @JsonProperty(value = "socist")
    private String socist;//社会保障情况
    @JsonProperty(value = "hobyds")
    private String hobyds;//爱好
    @JsonProperty(value = "psrttp")
    private String psrttp;//个人评级类型
    @JsonProperty(value = "richtg")
    private String richtg;//是否强村富民贷款
    @JsonProperty(value = "vipptg")
    private String vipptg;//是否VIP客户
    @JsonProperty(value = "credlv")
    private String credlv;//信用等级
    @JsonProperty(value = "fscddt")
    private String fscddt;//首次建立信贷关系年月
    @JsonProperty(value = "cnbgdt")
    private String cnbgdt;//往来起始日期
    @JsonProperty(value = "cneddt")
    private String cneddt;//往来终止日期
    @JsonProperty(value = "imagno")
    private String imagno;//影像编号
    @JsonProperty(value = "ctigno")
    private String ctigno;//客户经理号
    @JsonProperty(value = "ctigna")
    private String ctigna;//客户经理姓名
    @JsonProperty(value = "ctigph")
    private String ctigph;//客户经理手机号
    @JsonProperty(value = "txpstp")
    private String txpstp;//税收居民类型
    @JsonProperty(value = "addttr")
    private String addttr;//税收现居英文地址
    @JsonProperty(value = "brctry")
    private String brctry;//税收出生国家或地区
    @JsonProperty(value = "txnion")
    private String txnion;//税收居民国（地区）
    @JsonProperty(value = "taxnum")
    private String taxnum;//纳税人识别号
    @JsonProperty(value = "notxrs")
    private String notxrs;//不能提供纳税人识别号原因
    @JsonProperty(value = "mobitl")
    private String mobitl;//移动电话
    @JsonProperty(value = "offctl")
    private String offctl;//固定电话
    @JsonProperty(value = "cncktg")
    private String cncktg;//联系方式验证标识
    @JsonProperty(value = "postcd")
    private String postcd;//联系地址邮编
    @JsonProperty(value = "cncuty")
    private String cncuty;//联系地址所在国家
    @JsonProperty(value = "cnprov")
    private String cnprov;//联系地址所在省
    @JsonProperty(value = "cncity")
    private String cncity;//联系地址所在市
    @JsonProperty(value = "cnarea")
    private String cnarea;//联系地址所在区
    @JsonProperty(value = "homead")
    private String homead;//联系地址信息
    @JsonProperty(value = "qqnumb")
    private String qqnumb;//QQ
    @JsonProperty(value = "wechat")
    private String wechat;//微信
    @JsonProperty(value = "emailx")
    private String emailx;//邮箱
    @JsonProperty(value = "listnm")
    private BigDecimal listnm;//循环列表记录数
    @JsonProperty(value = "chflag")
    private String chflag;//境内标识
    @JsonProperty(value = "comadd")
    private String comadd;//常用居住地址
    @JsonProperty(value = "fullfg")
    private String fullfg;//信息完整性标识
    @JsonProperty(value = "chsdsc")
    private String chsdsc;//校验结果登记说明
    @JsonProperty(value = "prelttp")
    private String prelttp;//电话关系人
    @JsonProperty(value = "passno")
    private String passno;//通行证号码
    @JsonProperty(value = "uphone")
    private String uphone;//单位电话
    @JsonProperty(value = "cusstp")
    private String cusstp;//对私客户类型
    @JsonProperty(value = "resare")
    private String resare;//居住区域编号
    @JsonProperty(value = "resvue")
    private String resvue;//居住区域名称
    @JsonProperty(value = "resadd")
    private String resadd;//居住地址
    @JsonProperty(value = "relArrayInfo")
    private java.util.List<RelArrayInfo> relArrayInfo;

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getEfctdt() {
        return efctdt;
    }

    public void setEfctdt(String efctdt) {
        this.efctdt = efctdt;
    }

    public String getInefdt() {
        return inefdt;
    }

    public void setInefdt(String inefdt) {
        this.inefdt = inefdt;
    }

    public String getAuxidtftp() {
        return auxidtftp;
    }

    public void setAuxidtftp(String auxidtftp) {
        this.auxidtftp = auxidtftp;
    }

    public String getAuxidtfno() {
        return auxidtfno;
    }

    public void setAuxidtfno(String auxidtfno) {
        this.auxidtfno = auxidtfno;
    }

    public String getAuxinefdt() {
        return auxinefdt;
    }

    public void setAuxinefdt(String auxinefdt) {
        this.auxinefdt = auxinefdt;
    }

    public String getAreaad() {
        return areaad;
    }

    public void setAreaad(String areaad) {
        this.areaad = areaad;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    public String getCsspna() {
        return csspna;
    }

    public void setCsspna(String csspna) {
        this.csspna = csspna;
    }

    public String getCsenna() {
        return csenna;
    }

    public void setCsenna(String csenna) {
        this.csenna = csenna;
    }

    public String getPrpsex() {
        return prpsex;
    }

    public void setPrpsex(String prpsex) {
        this.prpsex = prpsex;
    }

    public String getNatvpc() {
        return natvpc;
    }

    public void setNatvpc(String natvpc) {
        this.natvpc = natvpc;
    }

    public String getEthnic() {
        return ethnic;
    }

    public void setEthnic(String ethnic) {
        this.ethnic = ethnic;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getBorndt() {
        return borndt;
    }

    public void setBorndt(String borndt) {
        this.borndt = borndt;
    }

    public String getPolist() {
        return polist;
    }

    public void setPolist(String polist) {
        this.polist = polist;
    }

    public String getMarist() {
        return marist;
    }

    public void setMarist(String marist) {
        this.marist = marist;
    }

    public String getHavetg() {
        return havetg;
    }

    public void setHavetg(String havetg) {
        this.havetg = havetg;
    }

    public BigDecimal getBrinnm() {
        return brinnm;
    }

    public void setBrinnm(BigDecimal brinnm) {
        this.brinnm = brinnm;
    }

    public String getEduclv() {
        return educlv;
    }

    public void setEduclv(String educlv) {
        this.educlv = educlv;
    }

    public String getIndidp() {
        return indidp;
    }

    public void setIndidp(String indidp) {
        this.indidp = indidp;
    }

    public String getGradna() {
        return gradna;
    }

    public void setGradna(String gradna) {
        this.gradna = gradna;
    }

    public String getGraddt() {
        return graddt;
    }

    public void setGraddt(String graddt) {
        this.graddt = graddt;
    }

    public String getProjob() {
        return projob;
    }

    public void setProjob(String projob) {
        this.projob = projob;
    }

    public String getPoston() {
        return poston;
    }

    public void setPoston(String poston) {
        this.poston = poston;
    }

    public String getPosttl() {
        return posttl;
    }

    public void setPosttl(String posttl) {
        this.posttl = posttl;
    }

    public String getOffibr() {
        return offibr;
    }

    public void setOffibr(String offibr) {
        this.offibr = offibr;
    }

    public String getWkutna() {
        return wkutna;
    }

    public void setWkutna(String wkutna) {
        this.wkutna = wkutna;
    }

    public String getUnquty() {
        return unquty;
    }

    public void setUnquty(String unquty) {
        this.unquty = unquty;
    }

    public String getCompad() {
        return compad;
    }

    public void setCompad(String compad) {
        this.compad = compad;
    }

    public Integer getUnitnm() {
        return unitnm;
    }

    public void setUnitnm(Integer unitnm) {
        this.unitnm = unitnm;
    }

    public String getWorkdt() {
        return workdt;
    }

    public void setWorkdt(String workdt) {
        this.workdt = workdt;
    }

    public String getIdwkdt() {
        return idwkdt;
    }

    public void setIdwkdt(String idwkdt) {
        this.idwkdt = idwkdt;
    }

    public String getUtwkdt() {
        return utwkdt;
    }

    public void setUtwkdt(String utwkdt) {
        this.utwkdt = utwkdt;
    }

    public String getCorppr() {
        return corppr;
    }

    public void setCorppr(String corppr) {
        this.corppr = corppr;
    }

    public BigDecimal getExpryr() {
        return expryr;
    }

    public void setExpryr(BigDecimal expryr) {
        this.expryr = expryr;
    }

    public BigDecimal getUtwkyr() {
        return utwkyr;
    }

    public void setUtwkyr(BigDecimal utwkyr) {
        this.utwkyr = utwkyr;
    }

    public String getCtcktg() {
        return ctcktg;
    }

    public void setCtcktg(String ctcktg) {
        this.ctcktg = ctcktg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCreabr() {
        return creabr;
    }

    public void setCreabr(String creabr) {
        this.creabr = creabr;
    }

    public String getUsrsid() {
        return usrsid;
    }

    public void setUsrsid(String usrsid) {
        this.usrsid = usrsid;
    }

    public String getOpendt() {
        return opendt;
    }

    public void setOpendt(String opendt) {
        this.opendt = opendt;
    }

    public String getLivest() {
        return livest;
    }

    public void setLivest(String livest) {
        this.livest = livest;
    }

    public String getLvbgdt() {
        return lvbgdt;
    }

    public void setLvbgdt(String lvbgdt) {
        this.lvbgdt = lvbgdt;
    }

    public Integer getFamisz() {
        return famisz;
    }

    public void setFamisz(Integer famisz) {
        this.famisz = famisz;
    }

    public String getMainic() {
        return mainic;
    }

    public void setMainic(String mainic) {
        this.mainic = mainic;
    }

    public String getCurrcy() {
        return currcy;
    }

    public void setCurrcy(String currcy) {
        this.currcy = currcy;
    }

    public BigDecimal getPrmnic() {
        return prmnic;
    }

    public void setPrmnic(BigDecimal prmnic) {
        this.prmnic = prmnic;
    }

    public BigDecimal getPryric() {
        return pryric;
    }

    public void setPryric(BigDecimal pryric) {
        this.pryric = pryric;
    }

    public BigDecimal getFmmnic() {
        return fmmnic;
    }

    public void setFmmnic(BigDecimal fmmnic) {
        this.fmmnic = fmmnic;
    }

    public BigDecimal getFmyric() {
        return fmyric;
    }

    public void setFmyric(BigDecimal fmyric) {
        this.fmyric = fmyric;
    }

    public String getEmpltg() {
        return empltg;
    }

    public void setEmpltg(String empltg) {
        this.empltg = empltg;
    }

    public String getCtmgno() {
        return ctmgno;
    }

    public void setCtmgno(String ctmgno) {
        this.ctmgno = ctmgno;
    }

    public String getFarmtg() {
        return farmtg;
    }

    public void setFarmtg(String farmtg) {
        this.farmtg = farmtg;
    }

    public String getSthdfg() {
        return sthdfg;
    }

    public void setSthdfg(String sthdfg) {
        this.sthdfg = sthdfg;
    }

    public String getRelgon() {
        return relgon;
    }

    public void setRelgon(String relgon) {
        this.relgon = relgon;
    }

    public String getProptg() {
        return proptg;
    }

    public void setProptg(String proptg) {
        this.proptg = proptg;
    }

    public BigDecimal getOnacnm() {
        return onacnm;
    }

    public void setOnacnm(BigDecimal onacnm) {
        this.onacnm = onacnm;
    }

    public BigDecimal getTotlnm() {
        return totlnm;
    }

    public void setTotlnm(BigDecimal totlnm) {
        this.totlnm = totlnm;
    }

    public String getAmlrat() {
        return amlrat;
    }

    public void setAmlrat(String amlrat) {
        this.amlrat = amlrat;
    }

    public BigDecimal getResiyr() {
        return resiyr;
    }

    public void setResiyr(BigDecimal resiyr) {
        this.resiyr = resiyr;
    }

    public String getHealst() {
        return healst;
    }

    public void setHealst(String healst) {
        this.healst = healst;
    }

    public String getSocist() {
        return socist;
    }

    public void setSocist(String socist) {
        this.socist = socist;
    }

    public String getHobyds() {
        return hobyds;
    }

    public void setHobyds(String hobyds) {
        this.hobyds = hobyds;
    }

    public String getPsrttp() {
        return psrttp;
    }

    public void setPsrttp(String psrttp) {
        this.psrttp = psrttp;
    }

    public String getRichtg() {
        return richtg;
    }

    public void setRichtg(String richtg) {
        this.richtg = richtg;
    }

    public String getVipptg() {
        return vipptg;
    }

    public void setVipptg(String vipptg) {
        this.vipptg = vipptg;
    }

    public String getCredlv() {
        return credlv;
    }

    public void setCredlv(String credlv) {
        this.credlv = credlv;
    }

    public String getFscddt() {
        return fscddt;
    }

    public void setFscddt(String fscddt) {
        this.fscddt = fscddt;
    }

    public String getCnbgdt() {
        return cnbgdt;
    }

    public void setCnbgdt(String cnbgdt) {
        this.cnbgdt = cnbgdt;
    }

    public String getCneddt() {
        return cneddt;
    }

    public void setCneddt(String cneddt) {
        this.cneddt = cneddt;
    }

    public String getImagno() {
        return imagno;
    }

    public void setImagno(String imagno) {
        this.imagno = imagno;
    }

    public String getCtigno() {
        return ctigno;
    }

    public void setCtigno(String ctigno) {
        this.ctigno = ctigno;
    }

    public String getCtigna() {
        return ctigna;
    }

    public void setCtigna(String ctigna) {
        this.ctigna = ctigna;
    }

    public String getCtigph() {
        return ctigph;
    }

    public void setCtigph(String ctigph) {
        this.ctigph = ctigph;
    }

    public String getTxpstp() {
        return txpstp;
    }

    public void setTxpstp(String txpstp) {
        this.txpstp = txpstp;
    }

    public String getAddttr() {
        return addttr;
    }

    public void setAddttr(String addttr) {
        this.addttr = addttr;
    }

    public String getBrctry() {
        return brctry;
    }

    public void setBrctry(String brctry) {
        this.brctry = brctry;
    }

    public String getTxnion() {
        return txnion;
    }

    public void setTxnion(String txnion) {
        this.txnion = txnion;
    }

    public String getTaxnum() {
        return taxnum;
    }

    public void setTaxnum(String taxnum) {
        this.taxnum = taxnum;
    }

    public String getNotxrs() {
        return notxrs;
    }

    public void setNotxrs(String notxrs) {
        this.notxrs = notxrs;
    }

    public String getMobitl() {
        return mobitl;
    }

    public void setMobitl(String mobitl) {
        this.mobitl = mobitl;
    }

    public String getOffctl() {
        return offctl;
    }

    public void setOffctl(String offctl) {
        this.offctl = offctl;
    }

    public String getCncktg() {
        return cncktg;
    }

    public void setCncktg(String cncktg) {
        this.cncktg = cncktg;
    }

    public String getPostcd() {
        return postcd;
    }

    public void setPostcd(String postcd) {
        this.postcd = postcd;
    }

    public String getCncuty() {
        return cncuty;
    }

    public void setCncuty(String cncuty) {
        this.cncuty = cncuty;
    }

    public String getCnprov() {
        return cnprov;
    }

    public void setCnprov(String cnprov) {
        this.cnprov = cnprov;
    }

    public String getCncity() {
        return cncity;
    }

    public void setCncity(String cncity) {
        this.cncity = cncity;
    }

    public String getCnarea() {
        return cnarea;
    }

    public void setCnarea(String cnarea) {
        this.cnarea = cnarea;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getQqnumb() {
        return qqnumb;
    }

    public void setQqnumb(String qqnumb) {
        this.qqnumb = qqnumb;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getEmailx() {
        return emailx;
    }

    public void setEmailx(String emailx) {
        this.emailx = emailx;
    }

    public BigDecimal getListnm() {
        return listnm;
    }

    public void setListnm(BigDecimal listnm) {
        this.listnm = listnm;
    }

    public String getChflag() {
        return chflag;
    }

    public void setChflag(String chflag) {
        this.chflag = chflag;
    }

    public String getComadd() {
        return comadd;
    }

    public void setComadd(String comadd) {
        this.comadd = comadd;
    }

    public String getFullfg() {
        return fullfg;
    }

    public void setFullfg(String fullfg) {
        this.fullfg = fullfg;
    }

    public String getChsdsc() {
        return chsdsc;
    }

    public void setChsdsc(String chsdsc) {
        this.chsdsc = chsdsc;
    }

    public String getPrelttp() {
        return prelttp;
    }

    public void setPrelttp(String prelttp) {
        this.prelttp = prelttp;
    }

    public String getPassno() {
        return passno;
    }

    public void setPassno(String passno) {
        this.passno = passno;
    }

    public String getUphone() {
        return uphone;
    }

    public void setUphone(String uphone) {
        this.uphone = uphone;
    }

    public String getCusstp() {
        return cusstp;
    }

    public void setCusstp(String cusstp) {
        this.cusstp = cusstp;
    }

    public String getResare() {
        return resare;
    }

    public void setResare(String resare) {
        this.resare = resare;
    }

    public String getResvue() {
        return resvue;
    }

    public void setResvue(String resvue) {
        this.resvue = resvue;
    }

    public String getResadd() {
        return resadd;
    }

    public void setResadd(String resadd) {
        this.resadd = resadd;
    }

    public List<RelArrayInfo> getRelArrayInfo() {
        return relArrayInfo;
    }

    public void setRelArrayInfo(List<RelArrayInfo> relArrayInfo) {
        this.relArrayInfo = relArrayInfo;
    }

    @Override
    public String toString() {
        return "S00101RespDto{" +
                "custno='" + custno + '\'' +
                ", custna='" + custna + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", efctdt='" + efctdt + '\'' +
                ", inefdt='" + inefdt + '\'' +
                ", auxidtftp='" + auxidtftp + '\'' +
                ", auxidtfno='" + auxidtfno + '\'' +
                ", auxinefdt='" + auxinefdt + '\'' +
                ", areaad='" + areaad + '\'' +
                ", depart='" + depart + '\'' +
                ", custst='" + custst + '\'' +
                ", csspna='" + csspna + '\'' +
                ", csenna='" + csenna + '\'' +
                ", prpsex='" + prpsex + '\'' +
                ", natvpc='" + natvpc + '\'' +
                ", ethnic='" + ethnic + '\'' +
                ", nation='" + nation + '\'' +
                ", borndt='" + borndt + '\'' +
                ", polist='" + polist + '\'' +
                ", marist='" + marist + '\'' +
                ", havetg='" + havetg + '\'' +
                ", brinnm=" + brinnm +
                ", educlv='" + educlv + '\'' +
                ", indidp='" + indidp + '\'' +
                ", gradna='" + gradna + '\'' +
                ", graddt='" + graddt + '\'' +
                ", projob='" + projob + '\'' +
                ", poston='" + poston + '\'' +
                ", posttl='" + posttl + '\'' +
                ", offibr='" + offibr + '\'' +
                ", wkutna='" + wkutna + '\'' +
                ", unquty='" + unquty + '\'' +
                ", compad='" + compad + '\'' +
                ", unitnm=" + unitnm +
                ", workdt='" + workdt + '\'' +
                ", idwkdt='" + idwkdt + '\'' +
                ", utwkdt='" + utwkdt + '\'' +
                ", corppr='" + corppr + '\'' +
                ", expryr=" + expryr +
                ", utwkyr=" + utwkyr +
                ", ctcktg='" + ctcktg + '\'' +
                ", result='" + result + '\'' +
                ", creabr='" + creabr + '\'' +
                ", usrsid='" + usrsid + '\'' +
                ", opendt='" + opendt + '\'' +
                ", livest='" + livest + '\'' +
                ", lvbgdt='" + lvbgdt + '\'' +
                ", famisz=" + famisz +
                ", mainic='" + mainic + '\'' +
                ", currcy='" + currcy + '\'' +
                ", prmnic=" + prmnic +
                ", pryric=" + pryric +
                ", fmmnic=" + fmmnic +
                ", fmyric=" + fmyric +
                ", empltg='" + empltg + '\'' +
                ", ctmgno='" + ctmgno + '\'' +
                ", farmtg='" + farmtg + '\'' +
                ", sthdfg='" + sthdfg + '\'' +
                ", relgon='" + relgon + '\'' +
                ", proptg='" + proptg + '\'' +
                ", onacnm=" + onacnm +
                ", totlnm=" + totlnm +
                ", amlrat='" + amlrat + '\'' +
                ", resiyr=" + resiyr +
                ", healst='" + healst + '\'' +
                ", socist='" + socist + '\'' +
                ", hobyds='" + hobyds + '\'' +
                ", psrttp='" + psrttp + '\'' +
                ", richtg='" + richtg + '\'' +
                ", vipptg='" + vipptg + '\'' +
                ", credlv='" + credlv + '\'' +
                ", fscddt='" + fscddt + '\'' +
                ", cnbgdt='" + cnbgdt + '\'' +
                ", cneddt='" + cneddt + '\'' +
                ", imagno='" + imagno + '\'' +
                ", ctigno='" + ctigno + '\'' +
                ", ctigna='" + ctigna + '\'' +
                ", ctigph='" + ctigph + '\'' +
                ", txpstp='" + txpstp + '\'' +
                ", addttr='" + addttr + '\'' +
                ", brctry='" + brctry + '\'' +
                ", txnion='" + txnion + '\'' +
                ", taxnum='" + taxnum + '\'' +
                ", notxrs='" + notxrs + '\'' +
                ", mobitl='" + mobitl + '\'' +
                ", offctl='" + offctl + '\'' +
                ", cncktg='" + cncktg + '\'' +
                ", postcd='" + postcd + '\'' +
                ", cncuty='" + cncuty + '\'' +
                ", cnprov='" + cnprov + '\'' +
                ", cncity='" + cncity + '\'' +
                ", cnarea='" + cnarea + '\'' +
                ", homead='" + homead + '\'' +
                ", qqnumb='" + qqnumb + '\'' +
                ", wechat='" + wechat + '\'' +
                ", emailx='" + emailx + '\'' +
                ", listnm=" + listnm +
                ", chflag='" + chflag + '\'' +
                ", comadd='" + comadd + '\'' +
                ", fullfg='" + fullfg + '\'' +
                ", chsdsc='" + chsdsc + '\'' +
                ", prelttp='" + prelttp + '\'' +
                ", passno='" + passno + '\'' +
                ", uphone='" + uphone + '\'' +
                ", cusstp='" + cusstp + '\'' +
                ", resare='" + resare + '\'' +
                ", resvue='" + resvue + '\'' +
                ", resadd='" + resadd + '\'' +
                ", relArrayInfo=" + relArrayInfo +
                '}';
    }
}