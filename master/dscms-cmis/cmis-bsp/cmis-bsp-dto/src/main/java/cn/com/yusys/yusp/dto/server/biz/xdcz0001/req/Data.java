package cn.com.yusys.yusp.dto.server.biz.xdcz0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：电子保函开立
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contno")
    private String contno;//合作协议编号
    @JsonProperty(value = "custid")
    private String custid;//客户代码
    @JsonProperty(value = "custna")
    private String custna;//客户名称
    @JsonProperty(value = "prodno")
    private String prodno;//产品编号
    @JsonProperty(value = "bhtype")
    private String bhtype;//保函类型
    @JsonProperty(value = "curren")
    private String curren;//币种
    @JsonProperty(value = "bhhamt")
    private BigDecimal bhhamt;//申请金额
    @JsonProperty(value = "bhstda")
    private String bhstda;//生效日期
    @JsonProperty(value = "bhedda")
    private String bhedda;//到期日期
    @JsonProperty(value = "jsacct")
    private String jsacct;//结算账号
    @JsonProperty(value = "jsacna")
    private String jsacna;//结算账号户名
    @JsonProperty(value = "sxfamt")
    private BigDecimal sxfamt;//手续费金额
    @JsonProperty(value = "sxflll")
    private BigDecimal sxflll;//手续费率
    @JsonProperty(value = "xmname")
    private String xmname;//项目名称
    @JsonProperty(value = "xmmamt")
    private BigDecimal xmmamt;//项目金额
    @JsonProperty(value = "coreno")
    private String coreno;//合同协议或标书编号
    @JsonProperty(value = "xghtje")
    private BigDecimal xghtje;//相关贸易合同金额
    @JsonProperty(value = "syrnam")
    private String syrnam;//受益人名称
    @JsonProperty(value = "syradd")
    private String syradd;//受益人地址
    @JsonProperty(value = "syrkhh")
    private String syrkhh;//受益人开户行
    @JsonProperty(value = "syracc")
    private String syracc;//受益人账号
    @JsonProperty(value = "bhfkfs")
    private String bhfkfs;//保函付款方式
    @JsonProperty(value = "cftjsm")
    private String cftjsm;//承付条件说明
    @JsonProperty(value = "zwogid")
    private String zwogid;//账务机构
    @JsonProperty(value = "inptid")
    private String inptid;//登记人
    @JsonProperty(value = "mangid")
    private String mangid;//责任人
    @JsonProperty(value = "inbrid")
    private String inbrid;//登记人机构
    @JsonProperty(value = "mabrid")
    private String mabrid;//责任人机构
    @JsonProperty(value = "indate")
    private String indate;//登记日期
    @JsonProperty(value = "tbcuid")
    private String tbcuid;//投标人客户号
    @JsonProperty(value = "billno")
    private String billno;//借据号
    @JsonProperty(value = "bhhtno")
    private String bhhtno;//保函协议编号
    @JsonProperty(value = "issece")
    private String issece;//是否密文
    public String  getContno() { return contno; }
    public void setContno(String contno ) { this.contno = contno;}
    public String  getCustid() { return custid; }
    public void setCustid(String custid ) { this.custid = custid;}
    public String  getCustna() { return custna; }
    public void setCustna(String custna ) { this.custna = custna;}
    public String  getProdno() { return prodno; }
    public void setProdno(String prodno ) { this.prodno = prodno;}
    public String  getBhtype() { return bhtype; }
    public void setBhtype(String bhtype ) { this.bhtype = bhtype;}
    public String  getCurren() { return curren; }
    public void setCurren(String curren ) { this.curren = curren;}
    public BigDecimal getBhhamt() { return bhhamt; }
    public void setBhhamt(BigDecimal bhhamt ) { this.bhhamt = bhhamt;}
    public String  getBhstda() { return bhstda; }
    public void setBhstda(String bhstda ) { this.bhstda = bhstda;}
    public String  getBhedda() { return bhedda; }
    public void setBhedda(String bhedda ) { this.bhedda = bhedda;}
    public String  getJsacct() { return jsacct; }
    public void setJsacct(String jsacct ) { this.jsacct = jsacct;}
    public String  getJsacna() { return jsacna; }
    public void setJsacna(String jsacna ) { this.jsacna = jsacna;}
    public BigDecimal  getSxfamt() { return sxfamt; }
    public void setSxfamt(BigDecimal sxfamt ) { this.sxfamt = sxfamt;}
    public BigDecimal  getSxflll() { return sxflll; }
    public void setSxflll(BigDecimal sxflll ) { this.sxflll = sxflll;}
    public String  getXmname() { return xmname; }
    public void setXmname(String xmname ) { this.xmname = xmname;}
    public BigDecimal  getXmmamt() { return xmmamt; }
    public void setXmmamt(BigDecimal xmmamt ) { this.xmmamt = xmmamt;}
    public String  getCoreno() { return coreno; }
    public void setCoreno(String coreno ) { this.coreno = coreno;}
    public BigDecimal  getXghtje() { return xghtje; }
    public void setXghtje(BigDecimal xghtje ) { this.xghtje = xghtje;}
    public String  getSyrnam() { return syrnam; }
    public void setSyrnam(String syrnam ) { this.syrnam = syrnam;}
    public String  getSyradd() { return syradd; }
    public void setSyradd(String syradd ) { this.syradd = syradd;}
    public String  getSyrkhh() { return syrkhh; }
    public void setSyrkhh(String syrkhh ) { this.syrkhh = syrkhh;}
    public String  getSyracc() { return syracc; }
    public void setSyracc(String syracc ) { this.syracc = syracc;}
    public String  getBhfkfs() { return bhfkfs; }
    public void setBhfkfs(String bhfkfs ) { this.bhfkfs = bhfkfs;}
    public String  getCftjsm() { return cftjsm; }
    public void setCftjsm(String cftjsm ) { this.cftjsm = cftjsm;}
    public String  getZwogid() { return zwogid; }
    public void setZwogid(String zwogid ) { this.zwogid = zwogid;}
    public String  getInptid() { return inptid; }
    public void setInptid(String inptid ) { this.inptid = inptid;}
    public String  getMangid() { return mangid; }
    public void setMangid(String mangid ) { this.mangid = mangid;}
    public String  getInbrid() { return inbrid; }
    public void setInbrid(String inbrid ) { this.inbrid = inbrid;}
    public String  getMabrid() { return mabrid; }
    public void setMabrid(String mabrid ) { this.mabrid = mabrid;}
    public String  getIndate() { return indate; }
    public void setIndate(String indate ) { this.indate = indate;}
    public String  getTbcuid() { return tbcuid; }
    public void setTbcuid(String tbcuid ) { this.tbcuid = tbcuid;}
    public String  getBillno() { return billno; }
    public void setBillno(String billno ) { this.billno = billno;}
    public String  getBhhtno() { return bhhtno; }
    public void setBhhtno(String bhhtno ) { this.bhhtno = bhhtno;}
    public String  getIssece() { return issece; }
    public void setIssece(String issece ) { this.issece = issece;}
    @Override
    public String toString() {
        return "Xdcz0001ReqDto{" +
                "custid='" + custid+ '\'' +
                "custna='" + custna+ '\'' +
                "contno='" + contno+ '\'' +
                "prodno='" + prodno+ '\'' +
                "bhtype='" + bhtype+ '\'' +
                "curren='" + curren+ '\'' +
                "bhhamt='" + bhhamt+ '\'' +
                "bhstda='" + bhstda+ '\'' +
                "bhedda='" + bhedda+ '\'' +
                "jsacct='" + jsacct+ '\'' +
                "jsacna='" + jsacna+ '\'' +
                "sxfamt='" + sxfamt+ '\'' +
                "sxflll='" + sxflll+ '\'' +
                "xmname='" + xmname+ '\'' +
                "xmmamt='" + xmmamt+ '\'' +
                "coreno='" + coreno+ '\'' +
                "xghtje='" + xghtje+ '\'' +
                "syrnam='" + syrnam+ '\'' +
                "syradd='" + syradd+ '\'' +
                "syrkhh='" + syrkhh+ '\'' +
                "syracc='" + syracc+ '\'' +
                "bhfkfs='" + bhfkfs+ '\'' +
                "cftjsm='" + cftjsm+ '\'' +
                "zwogid='" + zwogid+ '\'' +
                "inptid='" + inptid+ '\'' +
                "mangid='" + mangid+ '\'' +
                "inbrid='" + inbrid+ '\'' +
                "mabrid='" + mabrid+ '\'' +
                "indate='" + indate+ '\'' +
                "tbcuid='" + tbcuid+ '\'' +
                "billno='" + billno+ '\'' +
                "bhhtno='" + bhhtno+ '\'' +
                "issece='" + issece+ '\'' +
                '}';
    }
}

