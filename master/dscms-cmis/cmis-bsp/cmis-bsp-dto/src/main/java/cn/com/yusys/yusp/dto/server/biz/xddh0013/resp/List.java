package cn.com.yusys.yusp.dto.server.biz.xddh0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询优抵贷抵质押品信息
 * @Author zhangpeng
 * @Date 2021/4/25 14:22
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "setlMode")
    private String setlMode;//还款模式
    @JsonProperty(value = "setlTyp")
    private String setlTyp;//还款类型
    @JsonProperty(value = "setlRecvAmt")
    private BigDecimal setlRecvAmt;//收到金额
    @JsonProperty(value = "setlValDt")
    private String setlValDt;//记账日期
    @JsonProperty(value = "setlOdPrcpAmt")
    private BigDecimal setlOdPrcpAmt;//归还本金
    @JsonProperty(value = "setlOdIntAmt")
    private BigDecimal setlOdIntAmt;//归还逾期利息
    @JsonProperty(value = "setlOdCommInt")
    private BigDecimal setlOdCommInt;//归还复息
    @JsonProperty(value = "setlRemPrcpPaym")
    private BigDecimal setlRemPrcpPaym;//归还后剩余本金
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批状态
    @JsonProperty(value = "setlIntAmt")
    private BigDecimal setlIntAmt;//归还利息
    @JsonProperty(value = "bussDataSour")
    private String bussDataSour;//业务数据来源
    @JsonProperty(value = "paymPerdCnt")
    private BigDecimal paymPerdCnt;//还款后期数
    @JsonProperty(value = "paymAcctNo")
    private String paymAcctNo;//还款账号
    @JsonProperty(value = "paymAcctNam")
    private String paymAcctNam;//还款账号名称
    @JsonProperty(value = "isClearOwe")
    private String isClearOwe;//是否清偿还 Y清偿 N不清偿
    @JsonProperty(value = "trueRecvAmt")
    private BigDecimal trueRecvAmt;//在清偿模式下的实际还款金额
    @JsonProperty(value = "instmAmt")
    private BigDecimal instmAmt;//还款后的期供金额
    @JsonProperty(value = "isShort")
    private String isShort;//是否缩期
    @JsonProperty(value = "curtype")
    private String curtype;//币种
    @JsonProperty(value = "billBal")
    private BigDecimal billBal;//借据余额
    @JsonProperty(value = "hppDate")
    private String hppDate;//发生日期
    @JsonProperty(value = "isInstm")
    private String isInstm;//是否期供贷款
    @JsonProperty(value = "repayModeType")
    private String repayModeType;//还款方式类型
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "allOdPrcpAmt")
    private BigDecimal allOdPrcpAmt;//拖欠本金
    @JsonProperty(value = "allOdNormIntAmt")
    private BigDecimal allOdNormIntAmt;//拖欠利息
    @JsonProperty(value = "allOdIntAmt")
    private BigDecimal allOdIntAmt;//拖欠罚息
    @JsonProperty(value = "allOdTotalAmt")
    private BigDecimal allOdTotalAmt;//合计
    @JsonProperty(value = "setlOdNormInt")
    private BigDecimal setlOdNormInt;//归还逾期利息
    @JsonProperty(value = "feeAmt")
    private BigDecimal feeAmt;//费用金额
    @JsonProperty(value = "totalAmt")
    private BigDecimal totalAmt;//总计金额
    @JsonProperty(value = "accField")
    private BigDecimal accField;//核算字段
    @JsonProperty(value = "setlRpymOrd")
    private String setlRpymOrd;//还款顺序
    @JsonProperty(value = "nextDueDate")
    private String nextDueDate;//下期还款日期
    @JsonProperty(value = "setlPsIncTaken")
    private BigDecimal setlPsIncTaken;//当期利息
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "repayState")
    private String repayState;//还款状态
    @JsonProperty(value = "inputId")
    private String inputId;//登记号
    @JsonProperty(value = "managerId")
    private String managerId;//管户号
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构
    @JsonProperty(value = "loanRecType")
    private String loanRecType;//贷款收回方式
    @JsonProperty(value = "accStatus")
    private String accStatus;//台账状态
    @JsonProperty(value = "debtCollNo")
    private String debtCollNo;//抵债资产入账编号
    @JsonProperty(value = "isReturnFiscal")
    private String isReturnFiscal;//是否归还财政资金

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getSetlMode() {
        return setlMode;
    }

    public void setSetlMode(String setlMode) {
        this.setlMode = setlMode;
    }

    public String getSetlTyp() {
        return setlTyp;
    }

    public void setSetlTyp(String setlTyp) {
        this.setlTyp = setlTyp;
    }

    public BigDecimal getSetlRecvAmt() {
        return setlRecvAmt;
    }

    public void setSetlRecvAmt(BigDecimal setlRecvAmt) {
        this.setlRecvAmt = setlRecvAmt;
    }

    public String getSetlValDt() {
        return setlValDt;
    }

    public void setSetlValDt(String setlValDt) {
        this.setlValDt = setlValDt;
    }

    public BigDecimal getSetlOdPrcpAmt() {
        return setlOdPrcpAmt;
    }

    public void setSetlOdPrcpAmt(BigDecimal setlOdPrcpAmt) {
        this.setlOdPrcpAmt = setlOdPrcpAmt;
    }

    public BigDecimal getSetlOdIntAmt() {
        return setlOdIntAmt;
    }

    public void setSetlOdIntAmt(BigDecimal setlOdIntAmt) {
        this.setlOdIntAmt = setlOdIntAmt;
    }

    public BigDecimal getSetlOdCommInt() {
        return setlOdCommInt;
    }

    public void setSetlOdCommInt(BigDecimal setlOdCommInt) {
        this.setlOdCommInt = setlOdCommInt;
    }

    public BigDecimal getSetlRemPrcpPaym() {
        return setlRemPrcpPaym;
    }

    public void setSetlRemPrcpPaym(BigDecimal setlRemPrcpPaym) {
        this.setlRemPrcpPaym = setlRemPrcpPaym;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public BigDecimal getSetlIntAmt() {
        return setlIntAmt;
    }

    public void setSetlIntAmt(BigDecimal setlIntAmt) {
        this.setlIntAmt = setlIntAmt;
    }

    public String getBussDataSour() {
        return bussDataSour;
    }

    public void setBussDataSour(String bussDataSour) {
        this.bussDataSour = bussDataSour;
    }

    public BigDecimal getPaymPerdCnt() {
        return paymPerdCnt;
    }

    public void setPaymPerdCnt(BigDecimal paymPerdCnt) {
        this.paymPerdCnt = paymPerdCnt;
    }

    public String getPaymAcctNo() {
        return paymAcctNo;
    }

    public void setPaymAcctNo(String paymAcctNo) {
        this.paymAcctNo = paymAcctNo;
    }

    public String getPaymAcctNam() {
        return paymAcctNam;
    }

    public void setPaymAcctNam(String paymAcctNam) {
        this.paymAcctNam = paymAcctNam;
    }

    public String getIsClearOwe() {
        return isClearOwe;
    }

    public void setIsClearOwe(String isClearOwe) {
        this.isClearOwe = isClearOwe;
    }

    public BigDecimal getTrueRecvAmt() {
        return trueRecvAmt;
    }

    public void setTrueRecvAmt(BigDecimal trueRecvAmt) {
        this.trueRecvAmt = trueRecvAmt;
    }

    public BigDecimal getInstmAmt() {
        return instmAmt;
    }

    public void setInstmAmt(BigDecimal instmAmt) {
        this.instmAmt = instmAmt;
    }

    public String getIsShort() {
        return isShort;
    }

    public void setIsShort(String isShort) {
        this.isShort = isShort;
    }

    public String getCurtype() {
        return curtype;
    }

    public void setCurtype(String curtype) {
        this.curtype = curtype;
    }

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    public String getHppDate() {
        return hppDate;
    }

    public void setHppDate(String hppDate) {
        this.hppDate = hppDate;
    }


    public String getRepayModeType() {
        return repayModeType;
    }

    public void setRepayModeType(String repayModeType) {
        this.repayModeType = repayModeType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public BigDecimal getAllOdPrcpAmt() {
        return allOdPrcpAmt;
    }

    public void setAllOdPrcpAmt(BigDecimal allOdPrcpAmt) {
        this.allOdPrcpAmt = allOdPrcpAmt;
    }

    public BigDecimal getAllOdNormIntAmt() {
        return allOdNormIntAmt;
    }

    public void setAllOdNormIntAmt(BigDecimal allOdNormIntAmt) {
        this.allOdNormIntAmt = allOdNormIntAmt;
    }

    public BigDecimal getAllOdIntAmt() {
        return allOdIntAmt;
    }

    public void setAllOdIntAmt(BigDecimal allOdIntAmt) {
        this.allOdIntAmt = allOdIntAmt;
    }

    public BigDecimal getAllOdTotalAmt() {
        return allOdTotalAmt;
    }

    public void setAllOdTotalAmt(BigDecimal allOdTotalAmt) {
        this.allOdTotalAmt = allOdTotalAmt;
    }

    public BigDecimal getSetlOdNormInt() {
        return setlOdNormInt;
    }

    public void setSetlOdNormInt(BigDecimal setlOdNormInt) {
        this.setlOdNormInt = setlOdNormInt;
    }

    public BigDecimal getFeeAmt() {
        return feeAmt;
    }

    public void setFeeAmt(BigDecimal feeAmt) {
        this.feeAmt = feeAmt;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public BigDecimal getAccField() {
        return accField;
    }

    public void setAccField(BigDecimal accField) {
        this.accField = accField;
    }

    public String getSetlRpymOrd() {
        return setlRpymOrd;
    }

    public void setSetlRpymOrd(String setlRpymOrd) {
        this.setlRpymOrd = setlRpymOrd;
    }

    public String getNextDueDate() {
        return nextDueDate;
    }

    public void setNextDueDate(String nextDueDate) {
        this.nextDueDate = nextDueDate;
    }

    public BigDecimal getSetlPsIncTaken() {
        return setlPsIncTaken;
    }

    public void setSetlPsIncTaken(BigDecimal setlPsIncTaken) {
        this.setlPsIncTaken = setlPsIncTaken;
    }

    public String getIsInstm() {
        return isInstm;
    }

    public void setIsInstm(String isInstm) {
        this.isInstm = isInstm;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getRepayState() {
        return repayState;
    }

    public void setRepayState(String repayState) {
        this.repayState = repayState;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getLoanRecType() {
        return loanRecType;
    }

    public void setLoanRecType(String loanRecType) {
        this.loanRecType = loanRecType;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getDebtCollNo() {
        return debtCollNo;
    }

    public void setDebtCollNo(String debtCollNo) {
        this.debtCollNo = debtCollNo;
    }

    public String getIsReturnFiscal() {
        return isReturnFiscal;
    }

    public void setIsReturnFiscal(String isReturnFiscal) {
        this.isReturnFiscal = isReturnFiscal;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                ", billNo='" + billNo + '\'' +
                ", setlMode='" + setlMode + '\'' +
                ", setlTyp='" + setlTyp + '\'' +
                ", setlRecvAmt=" + setlRecvAmt +
                ", setlValDt='" + setlValDt + '\'' +
                ", setlOdPrcpAmt=" + setlOdPrcpAmt +
                ", setlOdIntAmt=" + setlOdIntAmt +
                ", setlOdCommInt=" + setlOdCommInt +
                ", setlRemPrcpPaym=" + setlRemPrcpPaym +
                ", approveStatus='" + approveStatus + '\'' +
                ", setlIntAmt=" + setlIntAmt +
                ", bussDataSour='" + bussDataSour + '\'' +
                ", paymPerdCnt=" + paymPerdCnt +
                ", paymAcctNo='" + paymAcctNo + '\'' +
                ", paymAcctNam='" + paymAcctNam + '\'' +
                ", isClearOwe='" + isClearOwe + '\'' +
                ", trueRecvAmt=" + trueRecvAmt +
                ", instmAmt=" + instmAmt +
                ", isShort='" + isShort + '\'' +
                ", curtype='" + curtype + '\'' +
                ", billBal=" + billBal +
                ", hppDate='" + hppDate + '\'' +
                ", isInstm='" + isInstm + '\'' +
                ", repayModeType='" + repayModeType + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", allOdPrcpAmt=" + allOdPrcpAmt +
                ", allOdNormIntAmt=" + allOdNormIntAmt +
                ", allOdIntAmt=" + allOdIntAmt +
                ", allOdTotalAmt=" + allOdTotalAmt +
                ", setlOdNormInt=" + setlOdNormInt +
                ", feeAmt=" + feeAmt +
                ", totalAmt=" + totalAmt +
                ", accField=" + accField +
                ", setlRpymOrd='" + setlRpymOrd + '\'' +
                ", nextDueDate='" + nextDueDate + '\'' +
                ", setlPsIncTaken=" + setlPsIncTaken +
                ", contNo='" + contNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", repayState='" + repayState + '\'' +
                ", inputId='" + inputId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", loanRecType='" + loanRecType + '\'' +
                ", accStatus='" + accStatus + '\'' +
                ", debtCollNo='" + debtCollNo + '\'' +
                ", isReturnFiscal='" + isReturnFiscal + '\'' +
                '}';
    }
}
