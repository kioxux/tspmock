package cn.com.yusys.yusp.dto.client.esb.core.dp2099.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：保证金账户查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2099ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "chaxleix")
    private String chaxleix;//查询类型
    @JsonProperty(value = "zhhufenl")
    private String zhhufenl;//账户分类
    @JsonProperty(value = "zhshuxin")
    private String zhshuxin;//账户属性
    @JsonProperty(value = "chaxfanw")
    private String chaxfanw;//查询范围
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "zhanghao")
    private String zhanghao;//负债账号
    @JsonProperty(value = "dinhuobz")
    private String dinhuobz;//产品定活标志
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh;//子账户序号
    @JsonProperty(value = "cunkzlei")
    private String cunkzlei;//存款种类
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//币种
    @JsonProperty(value = "chaohubz")
    private String chaohubz;//账户钞汇标志
    @JsonProperty(value = "zhhuztai")
    private String zhhuztai;//账户状态
    @JsonProperty(value = "chaxmima")
    private String chaxmima;//查询密码
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数
    @JsonProperty(value = "shifoudy")
    private String shifoudy;//是否打印
    @JsonProperty(value = "sfcxglzh")
    private String sfcxglzh;//是否查询关联账户
    @JsonProperty(value = "zhjnzlei")
    private String zhjnzlei;//证件种类
    @JsonProperty(value = "zhjhaoma")
    private String zhjhaoma;//证件号码
    @JsonProperty(value = "kehuzhmc")
    private String kehuzhmc;//客户账户名称
    @JsonProperty(value = "xgywbhao")
    private String xgywbhao;//相关业务编号

    public String getChaxleix() {
        return chaxleix;
    }

    public void setChaxleix(String chaxleix) {
        this.chaxleix = chaxleix;
    }

    public String getZhhufenl() {
        return zhhufenl;
    }

    public void setZhhufenl(String zhhufenl) {
        this.zhhufenl = zhhufenl;
    }

    public String getZhshuxin() {
        return zhshuxin;
    }

    public void setZhshuxin(String zhshuxin) {
        this.zhshuxin = zhshuxin;
    }

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getDinhuobz() {
        return dinhuobz;
    }

    public void setDinhuobz(String dinhuobz) {
        this.dinhuobz = dinhuobz;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getZhhuztai() {
        return zhhuztai;
    }

    public void setZhhuztai(String zhhuztai) {
        this.zhhuztai = zhhuztai;
    }

    public String getChaxmima() {
        return chaxmima;
    }

    public void setChaxmima(String chaxmima) {
        this.chaxmima = chaxmima;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    public String getSfcxglzh() {
        return sfcxglzh;
    }

    public void setSfcxglzh(String sfcxglzh) {
        this.sfcxglzh = sfcxglzh;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getXgywbhao() {
        return xgywbhao;
    }

    public void setXgywbhao(String xgywbhao) {
        this.xgywbhao = xgywbhao;
    }

    @Override
    public String toString() {
        return "Dp2099ReqDto{" +
                "chaxleix='" + chaxleix + '\'' +
                "zhhufenl='" + zhhufenl + '\'' +
                "zhshuxin='" + zhshuxin + '\'' +
                "chaxfanw='" + chaxfanw + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehuzhao='" + kehuzhao + '\'' +
                "zhanghao='" + zhanghao + '\'' +
                "dinhuobz='" + dinhuobz + '\'' +
                "zhhaoxuh='" + zhhaoxuh + '\'' +
                "cunkzlei='" + cunkzlei + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                "chaohubz='" + chaohubz + '\'' +
                "zhhuztai='" + zhhuztai + '\'' +
                "chaxmima='" + chaxmima + '\'' +
                "qishibis='" + qishibis + '\'' +
                "chxunbis='" + chxunbis + '\'' +
                "shifoudy='" + shifoudy + '\'' +
                "sfcxglzh='" + sfcxglzh + '\'' +
                "zhjnzlei='" + zhjnzlei + '\'' +
                "zhjhaoma='" + zhjhaoma + '\'' +
                "kehuzhmc='" + kehuzhmc + '\'' +
                "xgywbhao='" + xgywbhao + '\'' +
                '}';
    }
}  
