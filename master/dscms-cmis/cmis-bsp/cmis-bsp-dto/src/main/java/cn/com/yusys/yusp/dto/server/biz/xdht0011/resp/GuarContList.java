package cn.com.yusys.yusp.dto.server.biz.xdht0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class GuarContList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarContNo")
    private String guarContNo;//担保合同编号
    @JsonProperty(value = "cnGuarContNo")
    private String cnGuarContNo;//中文合同编号
    @JsonProperty(value = "guarContType")
    private String guarContType;//担保合同类型
    @JsonProperty(value = "lmtGrtFlag")
    private String lmtGrtFlag;//是否授信项下
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "guarContAmt")
    private String guarContAmt;//担保合同金额
    @JsonProperty(value = "debitCusId")
    private String debitCusId;//借款人客户号
    @JsonProperty(value = "debitName")
    private String debitName;//借款人名称
    @JsonProperty(value = "debitCertNo")
    private String debitCertNo;//借款人证件号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "guarStartDate")
    private String guarStartDate;//担保起始日
    @JsonProperty(value = "guarEndDate")
    private String guarEndDate;//担保终止日
    @JsonProperty(value = "guarContState")
    private String guarContState;//担保合同状态
    @JsonProperty(value = "guarContSignFlag")
    private String guarContSignFlag;//担保合同是否签约
    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号编号
    @JsonProperty(value = "loanContSignFlag")
    private String loanContSignFlag;//借款合同是否签约
    @JsonProperty(value = "signChnl")
    private String signChnl;//签约渠道
    @JsonProperty(value = "guarCusNo")
    private String guarCusNo;//担保人客户号
    @JsonProperty(value = "guarCusName")
    private String guarCusName;//担保人名称
    @JsonProperty(value = "guarCertNo")
    private String guarCertNo;//担保人证件号

    public String getGuarContNo() {
        return guarContNo;
    }

    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    public String getCnGuarContNo() {
        return cnGuarContNo;
    }

    public void setCnGuarContNo(String cnGuarContNo) {
        this.cnGuarContNo = cnGuarContNo;
    }

    public String getGuarContType() {
        return guarContType;
    }

    public void setGuarContType(String guarContType) {
        this.guarContType = guarContType;
    }

    public String getLmtGrtFlag() {
        return lmtGrtFlag;
    }

    public void setLmtGrtFlag(String lmtGrtFlag) {
        this.lmtGrtFlag = lmtGrtFlag;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getGuarContAmt() {
        return guarContAmt;
    }

    public void setGuarContAmt(String guarContAmt) {
        this.guarContAmt = guarContAmt;
    }

    public String getDebitCusId() {
        return debitCusId;
    }

    public void setDebitCusId(String debitCusId) {
        this.debitCusId = debitCusId;
    }

    public String getDebitName() {
        return debitName;
    }

    public void setDebitName(String debitName) {
        this.debitName = debitName;
    }

    public String getDebitCertNo() {
        return debitCertNo;
    }

    public void setDebitCertNo(String debitCertNo) {
        this.debitCertNo = debitCertNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getGuarStartDate() {
        return guarStartDate;
    }

    public void setGuarStartDate(String guarStartDate) {
        this.guarStartDate = guarStartDate;
    }

    public String getGuarEndDate() {
        return guarEndDate;
    }

    public void setGuarEndDate(String guarEndDate) {
        this.guarEndDate = guarEndDate;
    }

    public String getGuarContState() {
        return guarContState;
    }

    public void setGuarContState(String guarContState) {
        this.guarContState = guarContState;
    }

    public String getGuarContSignFlag() {
        return guarContSignFlag;
    }

    public void setGuarContSignFlag(String guarContSignFlag) {
        this.guarContSignFlag = guarContSignFlag;
    }

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getLoanContSignFlag() {
        return loanContSignFlag;
    }

    public void setLoanContSignFlag(String loanContSignFlag) {
        this.loanContSignFlag = loanContSignFlag;
    }

    public String getSignChnl() {
        return signChnl;
    }

    public void setSignChnl(String signChnl) {
        this.signChnl = signChnl;
    }

    public String getGuarCusNo() {
        return guarCusNo;
    }

    public void setGuarCusNo(String guarCusNo) {
        this.guarCusNo = guarCusNo;
    }

    public String getGuarCusName() {
        return guarCusName;
    }

    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName;
    }

    public String getGuarCertNo() {
        return guarCertNo;
    }

    public void setGuarCertNo(String guarCertNo) {
        this.guarCertNo = guarCertNo;
    }

    @Override
    public String toString() {
        return "GuarContList{" +
                "guarContNo='" + guarContNo + '\'' +
                "cnGuarContNo='" + cnGuarContNo + '\'' +
                "guarContType='" + guarContType + '\'' +
                "lmtGrtFlag='" + lmtGrtFlag + '\'' +
                "assureMeans='" + assureMeans + '\'' +
                "guarContAmt='" + guarContAmt + '\'' +
                "debitCusId='" + debitCusId + '\'' +
                "debitName='" + debitName + '\'' +
                "debitCertNo='" + debitCertNo + '\'' +
                "prdName='" + prdName + '\'' +
                "guarStartDate='" + guarStartDate + '\'' +
                "guarEndDate='" + guarEndDate + '\'' +
                "guarContState='" + guarContState + '\'' +
                "guarContSignFlag='" + guarContSignFlag + '\'' +
                "loanContNo='" + loanContNo + '\'' +
                "loanContSignFlag='" + loanContSignFlag + '\'' +
                "signChnl='" + signChnl + '\'' +
                "guarCusNo='" + guarCusNo + '\'' +
                "guarCusName='" + guarCusName + '\'' +
                "guarCertNo='" + guarCertNo + '\'' +
                '}';
    }

}
