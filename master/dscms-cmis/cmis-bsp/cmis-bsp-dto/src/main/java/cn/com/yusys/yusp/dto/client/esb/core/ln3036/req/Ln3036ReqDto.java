package cn.com.yusys.yusp.dto.client.esb.core.ln3036.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：针对录入、复核模式的交易，用于查询信贷录入信息的查询，复核时使用
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3036ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    @Override
    public String toString() {
        return "Ln3036ReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                '}';
    }
}  
