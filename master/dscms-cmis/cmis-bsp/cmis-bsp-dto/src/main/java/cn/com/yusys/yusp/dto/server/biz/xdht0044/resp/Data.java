package cn.com.yusys.yusp.dto.server.biz.xdht0044.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：房群客户查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHouseCus")
    private String isHouseCus;//是否是房群客户

    public String getIsHouseCus() {
        return isHouseCus;
    }

    public void setIsHouseCus(String isHouseCus) {
        this.isHouseCus = isHouseCus;
    }

    @Override
    public String toString() {
        return "Data{" +
                "isHouseCus='" + isHouseCus + '\'' +
                '}';
    }
}