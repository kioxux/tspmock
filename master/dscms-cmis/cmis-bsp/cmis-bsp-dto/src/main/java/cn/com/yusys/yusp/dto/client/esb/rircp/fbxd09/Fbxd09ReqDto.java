package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd09;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询日初（合约）信息历史表（利翃）总数据量
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd09ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contract_no")
    private String contract_no;//利翃平台贷款合同号

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Fbxd09ReqDto{" +
                "contract_no='" + contract_no + '\'' +
                '}';
    }
}  
