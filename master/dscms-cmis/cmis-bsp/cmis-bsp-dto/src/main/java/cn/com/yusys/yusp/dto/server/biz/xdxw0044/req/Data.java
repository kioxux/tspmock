package cn.com.yusys.yusp.dto.server.biz.xdxw0044.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：优抵贷待办事项接口
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "apply_serno")
    private String apply_serno;//业务流水
    @JsonProperty(value = "cus_name")
    private String cus_name;//申请人名称
    @JsonProperty(value = "cert_no")
    private String cert_no;//证件号码
    @JsonProperty(value = "telphone")
    private String telphone;//手机
    @JsonProperty(value = "company_name")
    private String company_name;//企业名称
    @JsonProperty(value = "legal_repre")
    private String legal_repre;//法人代表
    @JsonProperty(value = "judical_audi")
    private String judical_audi;//司法审核
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//申请金额
    @JsonProperty(value = "apply_rate")
    private BigDecimal apply_rate;//申请利率
    @JsonProperty(value = "apply_limit")
    private Integer apply_limit;//申请期限
    @JsonProperty(value = "cus_type")
    private String cus_type;//客户类型
    @JsonProperty(value = "cus_mgr_id")
    private String cus_mgr_id;//营销客户经理编号
    @JsonProperty(value = "ent_cert_type")
    private String ent_cert_type;//企业证件类型
    @JsonProperty(value = "ent_cert_id")
    private String ent_cert_id;//企业证件号
    @JsonProperty(value = "report_type")
    private String report_type;//调查表类型
    @JsonProperty(value = "com_tax_level")
    private String com_tax_level;//企业当前税务信用评级
    @JsonProperty(value = "com_profit_margin")
    private BigDecimal com_profit_margin;//企业近1年税前利润率
    @JsonProperty(value = "com_ratal")
    private BigDecimal com_ratal;//企业近1年综合应纳税额
    @JsonProperty(value = "debt_income_ratio")
    private BigDecimal debt_income_ratio;//负债收入比
    @JsonProperty(value = "com_sales_income")
    private BigDecimal com_sales_income;//企业近1年销售收入
    @JsonProperty(value = "tax_grade_result")
    private String tax_grade_result;//税务模型评级结果
    @JsonProperty(value = "tax_score")
    private String tax_score;//税务模型评分
    @JsonProperty(value = "qy_type")
    private String qy_type;//企业类型

    public String getApply_serno() {
        return apply_serno;
    }

    public void setApply_serno(String apply_serno) {
        this.apply_serno = apply_serno;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCert_no() {
        return cert_no;
    }

    public void setCert_no(String cert_no) {
        this.cert_no = cert_no;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getLegal_repre() {
        return legal_repre;
    }

    public void setLegal_repre(String legal_repre) {
        this.legal_repre = legal_repre;
    }

    public String getJudical_audi() {
        return judical_audi;
    }

    public void setJudical_audi(String judical_audi) {
        this.judical_audi = judical_audi;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public BigDecimal getApply_rate() {
        return apply_rate;
    }

    public void setApply_rate(BigDecimal apply_rate) {
        this.apply_rate = apply_rate;
    }

    public Integer getApply_limit() {
        return apply_limit;
    }

    public void setApply_limit(Integer apply_limit) {
        this.apply_limit = apply_limit;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }

    public String getCus_mgr_id() {
        return cus_mgr_id;
    }

    public void setCus_mgr_id(String cus_mgr_id) {
        this.cus_mgr_id = cus_mgr_id;
    }

    public String getEnt_cert_type() {
        return ent_cert_type;
    }

    public void setEnt_cert_type(String ent_cert_type) {
        this.ent_cert_type = ent_cert_type;
    }

    public String getEnt_cert_id() {
        return ent_cert_id;
    }

    public void setEnt_cert_id(String ent_cert_id) {
        this.ent_cert_id = ent_cert_id;
    }

    public String getReport_type() {
        return report_type;
    }

    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }

    public String getCom_tax_level() {
        return com_tax_level;
    }

    public void setCom_tax_level(String com_tax_level) {
        this.com_tax_level = com_tax_level;
    }

    public BigDecimal getCom_profit_margin() {
        return com_profit_margin;
    }

    public void setCom_profit_margin(BigDecimal com_profit_margin) {
        this.com_profit_margin = com_profit_margin;
    }

    public BigDecimal getCom_ratal() {
        return com_ratal;
    }

    public void setCom_ratal(BigDecimal com_ratal) {
        this.com_ratal = com_ratal;
    }

    public BigDecimal getDebt_income_ratio() {
        return debt_income_ratio;
    }

    public void setDebt_income_ratio(BigDecimal debt_income_ratio) {
        this.debt_income_ratio = debt_income_ratio;
    }

    public BigDecimal getCom_sales_income() {
        return com_sales_income;
    }

    public void setCom_sales_income(BigDecimal com_sales_income) {
        this.com_sales_income = com_sales_income;
    }

    public String getTax_grade_result() {
        return tax_grade_result;
    }

    public void setTax_grade_result(String tax_grade_result) {
        this.tax_grade_result = tax_grade_result;
    }

    public String getTax_score() {
        return tax_score;
    }

    public void setTax_score(String tax_score) {
        this.tax_score = tax_score;
    }

    public String getQy_type() {
        return qy_type;
    }

    public void setQy_type(String qy_type) {
        this.qy_type = qy_type;
    }

    @Override
    public String toString() {
        return "Data{" +
                "apply_serno='" + apply_serno + '\'' +
                "cus_name='" + cus_name + '\'' +
                "cert_no='" + cert_no + '\'' +
                "telphone='" + telphone + '\'' +
                "company_name='" + company_name + '\'' +
                "legal_repre='" + legal_repre + '\'' +
                "judical_audi='" + judical_audi + '\'' +
                "apply_amount='" + apply_amount + '\'' +
                "apply_rate='" + apply_rate + '\'' +
                "apply_limit='" + apply_limit + '\'' +
                "cus_type='" + cus_type + '\'' +
                "cus_mgr_id='" + cus_mgr_id + '\'' +
                "ent_cert_type='" + ent_cert_type + '\'' +
                "ent_cert_id='" + ent_cert_id + '\'' +
                "report_type='" + report_type + '\'' +
                "com_tax_level='" + com_tax_level + '\'' +
                "com_profit_margin='" + com_profit_margin + '\'' +
                "com_ratal='" + com_ratal + '\'' +
                "debt_income_ratio='" + debt_income_ratio + '\'' +
                "com_sales_income='" + com_sales_income + '\'' +
                "tax_grade_result='" + tax_grade_result + '\'' +
                "tax_score='" + tax_score + '\'' +
                "qy_type='" + qy_type + '\'' +
                '}';
    }
}  
