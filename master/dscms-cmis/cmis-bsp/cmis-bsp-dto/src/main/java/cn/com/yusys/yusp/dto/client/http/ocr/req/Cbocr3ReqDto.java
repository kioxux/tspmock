package cn.com.yusys.yusp.dto.client.http.ocr.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：报表核对统计总览列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cbocr3ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "subTaskId")
    private String subTaskId;//子任务id
    @JsonProperty(value = "templateId")
    private String templateId;//子任务对应的模板id

    public String getSubTaskId() {
        return subTaskId;
    }

    public void setSubTaskId(String subTaskId) {
        this.subTaskId = subTaskId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    @Override
    public String toString() {
        return "Cbocr3ReqDto{" +
                "subTaskId='" + subTaskId + '\'' +
                "templateId='" + templateId + '\'' +
                '}';
    }
}  
