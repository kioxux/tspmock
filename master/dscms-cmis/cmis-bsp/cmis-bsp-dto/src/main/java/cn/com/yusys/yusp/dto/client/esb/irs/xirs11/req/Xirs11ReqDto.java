package cn.com.yusys.yusp.dto.client.esb.irs.xirs11.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：同业客户信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xirs11ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custid")
    private String custid;//客户编号
    @JsonProperty(value = "same_org_no")
    private String same_org_no;//同业机构（行）号
    @JsonProperty(value = "same_org_name")
    private String same_org_name;//同业机构（行）号名称
    @JsonProperty(value = "same_org_type")
    private String same_org_type;//同业机构（行）类型
    @JsonProperty(value = "cantoncode")
    private String cantoncode;//注册地行政区划代码
    @JsonProperty(value = "inputuserid")
    private String inputuserid;//主管客户经理编号
    @JsonProperty(value = "inputorgid")
    private String inputorgid;//主管客户经理所属机构编号

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getSame_org_no() {
        return same_org_no;
    }

    public void setSame_org_no(String same_org_no) {
        this.same_org_no = same_org_no;
    }

    public String getSame_org_name() {
        return same_org_name;
    }

    public void setSame_org_name(String same_org_name) {
        this.same_org_name = same_org_name;
    }

    public String getSame_org_type() {
        return same_org_type;
    }

    public void setSame_org_type(String same_org_type) {
        this.same_org_type = same_org_type;
    }

    public String getCantoncode() {
        return cantoncode;
    }

    public void setCantoncode(String cantoncode) {
        this.cantoncode = cantoncode;
    }

    public String getInputuserid() {
        return inputuserid;
    }

    public void setInputuserid(String inputuserid) {
        this.inputuserid = inputuserid;
    }

    public String getInputorgid() {
        return inputorgid;
    }

    public void setInputorgid(String inputorgid) {
        this.inputorgid = inputorgid;
    }

    @Override
    public String toString() {
        return "Xirs11ReqDto{" +
                "custid='" + custid + '\'' +
                "same_org_no='" + same_org_no + '\'' +
                "same_org_name='" + same_org_name + '\'' +
                "same_org_type='" + same_org_type + '\'' +
                "cantoncode='" + cantoncode + '\'' +
                "inputuserid='" + inputuserid + '\'' +
                "inputorgid='" + inputorgid + '\'' +
                '}';
    }
}  
