package cn.com.yusys.yusp.dto.server.biz.xdsx0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/5/6 15:04
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dy_no")
    private String dy_no;//抵押品编号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "pre_app_no")
    private String pre_app_no;//预授信流水
    @JsonProperty(value = "bdc_no")
    private String bdc_no;//不动产证号
    @JsonProperty(value = "jk_cus_name")
    private String jk_cus_name;//借款人名称
    @JsonProperty(value = "manager_id")
    private String manager_id;//客户经理号

    public String getDy_no() {
        return dy_no;
    }

    public void setDy_no(String dy_no) {
        this.dy_no = dy_no;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getPre_app_no() {
        return pre_app_no;
    }

    public void setPre_app_no(String pre_app_no) {
        this.pre_app_no = pre_app_no;
    }

    public String getBdc_no() {
        return bdc_no;
    }

    public void setBdc_no(String bdc_no) {
        this.bdc_no = bdc_no;
    }

    public String getJk_cus_name() {
        return jk_cus_name;
    }

    public void setJk_cus_name(String jk_cus_name) {
        this.jk_cus_name = jk_cus_name;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    @Override
    public String toString() {
        return "Data{" +
                "dy_no='" + dy_no + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", pre_app_no='" + pre_app_no + '\'' +
                ", bdc_no='" + bdc_no + '\'' +
                ", jk_cus_name='" + jk_cus_name + '\'' +
                ", manager_id='" + manager_id + '\'' +
                '}';
    }
}
