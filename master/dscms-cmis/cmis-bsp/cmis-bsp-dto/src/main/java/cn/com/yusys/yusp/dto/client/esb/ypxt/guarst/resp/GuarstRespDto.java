package cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：押品状态变更推送
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class GuarstRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}  
