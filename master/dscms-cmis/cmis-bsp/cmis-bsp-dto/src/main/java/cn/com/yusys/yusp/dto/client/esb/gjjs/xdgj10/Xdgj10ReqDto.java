package cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj10;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷获取国结信用证信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdgj10ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lc_no")
    private String lc_no;//信用证编号

    public String getLc_no() {
        return lc_no;
    }

    public void setLc_no(String lc_no) {
        this.lc_no = lc_no;
    }

    @Override
    public String toString() {
        return "Xdgj10ReqDto{" +
                "lc_no='" + lc_no + '\'' +
                '}';
    }
}  
