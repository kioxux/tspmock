package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 法定代表人其他公司任职
 */
@JsonPropertyOrder(alphabetic = true)
public class FRPOSITION implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CANDATE")
    private String CANDATE;//	注销日期
    @JsonProperty(value = "CREDITCODE")
    private String CREDITCODE;//	统一社会信用代码
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;//	企业机构名称
    @JsonProperty(value = "ENTSTATUS")
    private String ENTSTATUS;//	企业状态
    @JsonProperty(value = "ENTTYPE")
    private String ENTTYPE;//	企业（机构）类型
    @JsonProperty(value = "ESDATE")
    private String ESDATE;//	成立日期
    @JsonProperty(value = "LEREPSIGN")
    private String LEREPSIGN;//	是否法定代表人
    @JsonProperty(value = "NAME")
    private String NAME;//	法定代表人姓名
    @JsonProperty(value = "POSITION")
    private String POSITION;//	职务
    @JsonProperty(value = "PPVAMOUNT")
    private String PPVAMOUNT;//	企业总数量
    @JsonProperty(value = "REGCAP")
    private String REGCAP;//	注册资本（企业:万元）
    @JsonProperty(value = "REGCAPCUR")
    private String REGCAPCUR;//	注册资本币种
    @JsonProperty(value = "REGNO")
    private String REGNO;//	注册号
    @JsonProperty(value = "REGORG")
    private String REGORG;//	登记机关
    @JsonProperty(value = "REGORGCODE")
    private String REGORGCODE;//	注册地址行政区编号
    @JsonProperty(value = "REVDATE")
    private String REVDATE;//	吊销日期

    @JsonIgnore
    public String getCANDATE() {
        return CANDATE;
    }

    @JsonIgnore
    public void setCANDATE(String CANDATE) {
        this.CANDATE = CANDATE;
    }

    @JsonIgnore
    public String getCREDITCODE() {
        return CREDITCODE;
    }

    @JsonIgnore
    public void setCREDITCODE(String CREDITCODE) {
        this.CREDITCODE = CREDITCODE;
    }

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getENTSTATUS() {
        return ENTSTATUS;
    }

    @JsonIgnore
    public void setENTSTATUS(String ENTSTATUS) {
        this.ENTSTATUS = ENTSTATUS;
    }

    @JsonIgnore
    public String getENTTYPE() {
        return ENTTYPE;
    }

    @JsonIgnore
    public void setENTTYPE(String ENTTYPE) {
        this.ENTTYPE = ENTTYPE;
    }

    @JsonIgnore
    public String getESDATE() {
        return ESDATE;
    }

    @JsonIgnore
    public void setESDATE(String ESDATE) {
        this.ESDATE = ESDATE;
    }

    @JsonIgnore
    public String getLEREPSIGN() {
        return LEREPSIGN;
    }

    @JsonIgnore
    public void setLEREPSIGN(String LEREPSIGN) {
        this.LEREPSIGN = LEREPSIGN;
    }

    @JsonIgnore
    public String getNAME() {
        return NAME;
    }

    @JsonIgnore
    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    @JsonIgnore
    public String getPOSITION() {
        return POSITION;
    }

    @JsonIgnore
    public void setPOSITION(String POSITION) {
        this.POSITION = POSITION;
    }

    @JsonIgnore
    public String getPPVAMOUNT() {
        return PPVAMOUNT;
    }

    @JsonIgnore
    public void setPPVAMOUNT(String PPVAMOUNT) {
        this.PPVAMOUNT = PPVAMOUNT;
    }

    @JsonIgnore
    public String getREGCAP() {
        return REGCAP;
    }

    @JsonIgnore
    public void setREGCAP(String REGCAP) {
        this.REGCAP = REGCAP;
    }

    @JsonIgnore
    public String getREGCAPCUR() {
        return REGCAPCUR;
    }

    @JsonIgnore
    public void setREGCAPCUR(String REGCAPCUR) {
        this.REGCAPCUR = REGCAPCUR;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @JsonIgnore
    public String getREGORG() {
        return REGORG;
    }

    @JsonIgnore
    public void setREGORG(String REGORG) {
        this.REGORG = REGORG;
    }

    @JsonIgnore
    public String getREGORGCODE() {
        return REGORGCODE;
    }

    @JsonIgnore
    public void setREGORGCODE(String REGORGCODE) {
        this.REGORGCODE = REGORGCODE;
    }

    @JsonIgnore
    public String getREVDATE() {
        return REVDATE;
    }

    @JsonIgnore
    public void setREVDATE(String REVDATE) {
        this.REVDATE = REVDATE;
    }

    @Override
    public String toString() {
        return "FRPOSITION{" +
                "CANDATE='" + CANDATE + '\'' +
                ", CREDITCODE='" + CREDITCODE + '\'' +
                ", ENTNAME='" + ENTNAME + '\'' +
                ", ENTSTATUS='" + ENTSTATUS + '\'' +
                ", ENTTYPE='" + ENTTYPE + '\'' +
                ", ESDATE='" + ESDATE + '\'' +
                ", LEREPSIGN='" + LEREPSIGN + '\'' +
                ", NAME='" + NAME + '\'' +
                ", POSITION='" + POSITION + '\'' +
                ", PPVAMOUNT='" + PPVAMOUNT + '\'' +
                ", REGCAP='" + REGCAP + '\'' +
                ", REGCAPCUR='" + REGCAPCUR + '\'' +
                ", REGNO='" + REGNO + '\'' +
                ", REGORG='" + REGORG + '\'' +
                ", REGORGCODE='" + REGORGCODE + '\'' +
                ", REVDATE='" + REVDATE + '\'' +
                '}';
    }
}
