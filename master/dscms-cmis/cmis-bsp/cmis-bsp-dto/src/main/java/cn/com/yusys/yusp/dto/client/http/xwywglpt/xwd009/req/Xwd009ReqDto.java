package cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：利率申请接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwd009ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "businessId")
    private String businessId;//行内业务流水号
    @JsonProperty(value = "identNo")
    private String identNo;//申请人身份证号码
    @JsonProperty(value = "applName")
    private String applName;//申请人名称
    @JsonProperty(value = "applLimit")
    private String applLimit;//贷款期限
    @JsonProperty(value = "industry")
    private String industry;//行业
    @JsonProperty(value = "grade")
    private String grade;//客户评级
    @JsonProperty(value = "businessSource")
    private String businessSource;//业务来源
    @JsonProperty(value = "applInterest")
    private String applInterest;//申请利率
    @JsonProperty(value = "customerManagerOpinion")
    private String customerManagerOpinion;//客户经理意见
    @JsonProperty(value = "applReason")
    private String applReason;//申请原因
    @JsonProperty(value = "guaranteeType")
    private String guaranteeType;//担保类型
    @JsonProperty(value = "creator")
    private String creator;//申请人工号
    @JsonProperty(value = "collateralType")
    private String collateralType;//抵押物类型
    @JsonProperty(value = "rateBranch")
    private String rateBranch;//分部
    @JsonProperty(value = "newCustomers")
    private String newCustomers;//新老客户
    @JsonProperty(value = "creditAuthCodeList")
    private String creditAuthCodeList;//贷款授权码
    @JsonProperty(value = "repayWay")
    private String repayWay;


    public String getCreditAuthCodeList() {
        return creditAuthCodeList;
    }

    public void setCreditAuthCodeList(String creditAuthCodeList) {
        this.creditAuthCodeList = creditAuthCodeList;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getIdentNo() {
        return identNo;
    }

    public void setIdentNo(String identNo) {
        this.identNo = identNo;
    }

    public String getApplName() {
        return applName;
    }

    public void setApplName(String applName) {
        this.applName = applName;
    }

    public String getApplLimit() {
        return applLimit;
    }

    public void setApplLimit(String applLimit) {
        this.applLimit = applLimit;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getBusinessSource() {
        return businessSource;
    }

    public void setBusinessSource(String businessSource) {
        this.businessSource = businessSource;
    }

    public String getApplInterest() {
        return applInterest;
    }

    public void setApplInterest(String applInterest) {
        this.applInterest = applInterest;
    }

    public String getCustomerManagerOpinion() {
        return customerManagerOpinion;
    }

    public void setCustomerManagerOpinion(String customerManagerOpinion) {
        this.customerManagerOpinion = customerManagerOpinion;
    }

    public String getApplReason() {
        return applReason;
    }

    public void setApplReason(String applReason) {
        this.applReason = applReason;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCollateralType() {
        return collateralType;
    }

    public void setCollateralType(String collateralType) {
        this.collateralType = collateralType;
    }

    public String getRateBranch() {
        return rateBranch;
    }

    public void setRateBranch(String rateBranch) {
        this.rateBranch = rateBranch;
    }

    public String getNewCustomers() {
        return newCustomers;
    }

    public void setNewCustomers(String newCustomers) {
        this.newCustomers = newCustomers;
    }

    public String getRepayWay() {
        return repayWay;
    }

    public void setRepayWay(String repayWay) {
        this.repayWay = repayWay;
    }

    @Override
    public String toString() {
        return "Xwd009ReqDto{" +
                "businessId='" + businessId + '\'' +
                ", identNo='" + identNo + '\'' +
                ", applName='" + applName + '\'' +
                ", applLimit='" + applLimit + '\'' +
                ", industry='" + industry + '\'' +
                ", grade='" + grade + '\'' +
                ", businessSource='" + businessSource + '\'' +
                ", applInterest='" + applInterest + '\'' +
                ", customerManagerOpinion='" + customerManagerOpinion + '\'' +
                ", applReason='" + applReason + '\'' +
                ", guaranteeType='" + guaranteeType + '\'' +
                ", creator='" + creator + '\'' +
                ", collateralType='" + collateralType + '\'' +
                ", rateBranch='" + rateBranch + '\'' +
                ", newCustomers='" + newCustomers + '\'' +
                ", creditAuthCodeList='" + creditAuthCodeList + '\'' +
                ", repayWay='" + repayWay + '\'' +
                '}';
    }
}  
