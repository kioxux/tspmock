package cn.com.yusys.yusp.dto.server.biz.xdht0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 15:08
 * @Version 1.0
 */
public class Data {
    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billNo='" + billNo + '\'' +
                '}';
    }
}
