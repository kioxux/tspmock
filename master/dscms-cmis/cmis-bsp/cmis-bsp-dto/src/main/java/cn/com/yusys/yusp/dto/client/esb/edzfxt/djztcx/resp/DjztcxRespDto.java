package cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：贷记入账状态查询申请往帐
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class DjztcxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ttfnlst")
    private String ttfnlst;//入账状态
    @JsonProperty(value = "rxcinfo")
    private String rxcinfo;//退汇原因码
    @JsonProperty(value = "rexchngrslt")
    private String rexchngrslt;//退汇原因
    @JsonProperty(value = "npcprcsts")
    private String npcprcsts;//NPC处理状态
    @JsonProperty(value = "npcprccd")
    private String npcprccd;//NPC处理码
    @JsonProperty(value = "npcrjctinf")
    private String npcrjctinf;//处理信息

    public String getTtfnlst() {
        return ttfnlst;
    }

    public void setTtfnlst(String ttfnlst) {
        this.ttfnlst = ttfnlst;
    }

    public String getRxcinfo() {
        return rxcinfo;
    }

    public void setRxcinfo(String rxcinfo) {
        this.rxcinfo = rxcinfo;
    }

    public String getRexchngrslt() {
        return rexchngrslt;
    }

    public void setRexchngrslt(String rexchngrslt) {
        this.rexchngrslt = rexchngrslt;
    }

    public String getNpcprcsts() {
        return npcprcsts;
    }

    public void setNpcprcsts(String npcprcsts) {
        this.npcprcsts = npcprcsts;
    }

    public String getNpcprccd() {
        return npcprccd;
    }

    public void setNpcprccd(String npcprccd) {
        this.npcprccd = npcprccd;
    }

    public String getNpcrjctinf() {
        return npcrjctinf;
    }

    public void setNpcrjctinf(String npcrjctinf) {
        this.npcrjctinf = npcrjctinf;
    }

    @Override
    public String toString() {
        return "DjztcxRespDto{" +
                "ttfnlst='" + ttfnlst + '\'' +
                "rxcinfo='" + rxcinfo + '\'' +
                "rexchngrslt='" + rexchngrslt + '\'' +
                "npcprcsts='" + npcprcsts + '\'' +
                "npcprccd='" + npcprccd + '\'' +
                "npcrjctinf='" + npcrjctinf + '\'' +
                '}';
    }
}  
