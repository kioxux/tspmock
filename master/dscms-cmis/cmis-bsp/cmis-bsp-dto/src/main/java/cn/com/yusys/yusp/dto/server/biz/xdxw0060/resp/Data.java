package cn.com.yusys.yusp.dto.server.biz.xdxw0060.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：客户及配偶信用类小微业务贷款授信金额
 * @Author zhangpeng
 * @Date 2021/4/25 9:56
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totlApplyAmount")
    private BigDecimal totlApplyAmount;//授信总金额

    public BigDecimal getTotlApplyAmount() {
        return totlApplyAmount;
    }

    public void setTotlApplyAmount(BigDecimal totlApplyAmount) {
        this.totlApplyAmount = totlApplyAmount;
    }

    @Override
    public String toString() {
        return "Data{" +
                "totlApplyAmount=" + totlApplyAmount +
                '}';
    }
}
