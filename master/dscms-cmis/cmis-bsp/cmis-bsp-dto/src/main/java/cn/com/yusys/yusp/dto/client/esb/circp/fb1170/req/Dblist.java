package cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/8/10 14:26:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/8/10 14:26
 * @since 2021/8/10 14:26
 */
@JsonPropertyOrder(alphabetic = true)
public class Dblist implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "db_cont_no")
    private String db_cont_no;//担保合同编号
    @JsonProperty(value = "dh_cont_no_cn")
    private String dh_cont_no_cn;//担保合同中文编号
    @JsonProperty(value = "guar_cont_type")
    private String guar_cont_type;//担保合同类型
    @JsonProperty(value = "guar_way")
    private String guar_way;//担保方式
    @JsonProperty(value = "db_cur_type")
    private String db_cur_type;//担保币种
    @JsonProperty(value = "guar_amt")
    private String guar_amt;//担保合同金额
    @JsonProperty(value = "guar_start_date")
    private String guar_start_date;//担保起始日
    @JsonProperty(value = "guar_end_date")
    private String guar_end_date;//担保终止日
    @JsonProperty(value = "sign_date")
    private String sign_date;//担保合同签订日期
    @JsonProperty(value = "guar_cont_state")
    private String guar_cont_state;//担保合同状态

    public String getDb_cont_no() {
        return db_cont_no;
    }

    public void setDb_cont_no(String db_cont_no) {
        this.db_cont_no = db_cont_no;
    }

    public String getDh_cont_no_cn() {
        return dh_cont_no_cn;
    }

    public void setDh_cont_no_cn(String dh_cont_no_cn) {
        this.dh_cont_no_cn = dh_cont_no_cn;
    }

    public String getGuar_cont_type() {
        return guar_cont_type;
    }

    public void setGuar_cont_type(String guar_cont_type) {
        this.guar_cont_type = guar_cont_type;
    }

    public String getGuar_way() {
        return guar_way;
    }

    public void setGuar_way(String guar_way) {
        this.guar_way = guar_way;
    }

    public String getDb_cur_type() {
        return db_cur_type;
    }

    public void setDb_cur_type(String db_cur_type) {
        this.db_cur_type = db_cur_type;
    }

    public String getGuar_amt() {
        return guar_amt;
    }

    public void setGuar_amt(String guar_amt) {
        this.guar_amt = guar_amt;
    }

    public String getGuar_start_date() {
        return guar_start_date;
    }

    public void setGuar_start_date(String guar_start_date) {
        this.guar_start_date = guar_start_date;
    }

    public String getGuar_end_date() {
        return guar_end_date;
    }

    public void setGuar_end_date(String guar_end_date) {
        this.guar_end_date = guar_end_date;
    }

    public String getSign_date() {
        return sign_date;
    }

    public void setSign_date(String sign_date) {
        this.sign_date = sign_date;
    }

    public String getGuar_cont_state() {
        return guar_cont_state;
    }

    public void setGuar_cont_state(String guar_cont_state) {
        this.guar_cont_state = guar_cont_state;
    }

    @Override
    public String toString() {
        return "Dblist{" +
                "db_cont_no='" + db_cont_no + '\'' +
                ", dh_cont_no_cn='" + dh_cont_no_cn + '\'' +
                ", guar_cont_type='" + guar_cont_type + '\'' +
                ", guar_way='" + guar_way + '\'' +
                ", db_cur_type='" + db_cur_type + '\'' +
                ", guar_amt='" + guar_amt + '\'' +
                ", guar_start_date='" + guar_start_date + '\'' +
                ", guar_end_date='" + guar_end_date + '\'' +
                ", sign_date='" + sign_date + '\'' +
                ", guar_cont_state='" + guar_cont_state + '\'' +
                '}';
    }
}
