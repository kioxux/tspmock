package cn.com.yusys.yusp.dto.server.biz.xdtz0010.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据身份证号获取借据信息
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0010RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.server.biz.xdtz0010.resp.Data data;

    public cn.com.yusys.yusp.dto.server.biz.xdtz0010.resp.Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdtz0010RespDto{" +
                "data=" + data +
                '}';
    }
}