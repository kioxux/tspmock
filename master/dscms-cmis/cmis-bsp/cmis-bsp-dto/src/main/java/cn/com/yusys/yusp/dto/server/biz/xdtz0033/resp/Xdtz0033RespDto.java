package cn.com.yusys.yusp.dto.server.biz.xdtz0033.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询客户所担保的行内贷款五级分类非正常状态件数
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0033RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdtz0033RespDto{" +
                "data=" + data +
                '}';
    }
}
