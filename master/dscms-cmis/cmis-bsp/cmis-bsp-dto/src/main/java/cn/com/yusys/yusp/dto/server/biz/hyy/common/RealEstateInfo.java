package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	不动产信息
@JsonPropertyOrder(alphabetic = true)
public class RealEstateInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "district")
    private District district;//所属区县
    @JsonProperty(value = "location")
    private String location;//坐落
    @JsonProperty(value = "house")
    private House house;//房屋
    @JsonProperty(value = "land")
    private Land land;//土地

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public Land getLand() {
        return land;
    }

    public void setLand(Land land) {
        this.land = land;
    }

    @Override
    public String toString() {
        return "RealEstateInfo{" +
                "district=" + district +
                ", location='" + location + '\'' +
                ", house=" + house +
                ", land=" + land +
                '}';
    }
}
