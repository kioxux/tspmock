package cn.com.yusys.yusp.dto.client.esb.core.ib1253;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：根据账号查询帐户信息接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16上午10:30:52
 */
@JsonPropertyOrder(alphabetic = true)
public class Ib1253ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao; // 客户账号
    @JsonProperty(value = "zhhaoxuh")
    private String zhhaoxuh; // 子账户序号
    @JsonProperty(value = "yanmbzhi")
    private String yanmbzhi; // 密码校验方式
    @JsonProperty(value = "mimammmm")
    private String mimammmm; // 密码
    @JsonProperty(value = "kehzhao2")
    private String kehzhao2; // 客户账号2
    @JsonProperty(value = "shifoubz")
    private String shifoubz; // 是否标志
    @JsonProperty(value = "zhufldm1")
    private String zhufldm1; // 账户分类代码1
    @JsonProperty(value = "zhufldm2")
    private String zhufldm2; // 账户分类代码2

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getYanmbzhi() {
        return yanmbzhi;
    }

    public void setYanmbzhi(String yanmbzhi) {
        this.yanmbzhi = yanmbzhi;
    }

    public String getMimammmm() {
        return mimammmm;
    }

    public void setMimammmm(String mimammmm) {
        this.mimammmm = mimammmm;
    }

    public String getKehzhao2() {
        return kehzhao2;
    }

    public void setKehzhao2(String kehzhao2) {
        this.kehzhao2 = kehzhao2;
    }

    public String getShifoubz() {
        return shifoubz;
    }

    public void setShifoubz(String shifoubz) {
        this.shifoubz = shifoubz;
    }

    public String getZhufldm1() {
        return zhufldm1;
    }

    public void setZhufldm1(String zhufldm1) {
        this.zhufldm1 = zhufldm1;
    }

    public String getZhufldm2() {
        return zhufldm2;
    }

    public void setZhufldm2(String zhufldm2) {
        this.zhufldm2 = zhufldm2;
    }

    @Override
    public String toString() {
        return "Ib1253ReqDto{" +
                "kehuzhao='" + kehuzhao + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", yanmbzhi='" + yanmbzhi + '\'' +
                ", mimammmm='" + mimammmm + '\'' +
                ", kehzhao2='" + kehzhao2 + '\'' +
                ", shifoubz='" + shifoubz + '\'' +
                ", zhufldm1='" + zhufldm1 + '\'' +
                ", zhufldm2='" + zhufldm2 + '\'' +
                '}';
    }
}