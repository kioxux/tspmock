package cn.com.yusys.yusp.dto.server.biz.xdtz0031.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据合同号获取借据信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "account_status")
    private String account_status;//借据状态

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cert_code='" + cert_code + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", account_status='" + account_status + '\'' +
                '}';
    }
}