package cn.com.yusys.yusp.dto.server.biz.xdtz0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/5/7 10:53
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "certType")
    private String certType;//借款人证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//借款人证件号码

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                '}';
    }
}
