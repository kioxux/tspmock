package cn.com.yusys.yusp.dto.server.biz.xdht0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 11:27
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusCertNo")
    private String cusCertNo;//客户证件号
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//贷款用途
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同开始日期
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同结束日期
    @JsonProperty(value = "guarContNo")
    private String guarContNo;//担保合同号
    @JsonProperty(value = "exeRate")
    private BigDecimal exeRate;//执行利率
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "contState")
    private String contState;//合同状态
    @JsonProperty(value = "isWhb")
    private String isWhb;//是否无还本续贷

    public String getCusCertNo() {
        return cusCertNo;
    }

    public void setCusCertNo(String cusCertNo) {
        this.cusCertNo = cusCertNo;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getGuarContNo() {
        return guarContNo;
    }

    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    public BigDecimal getExeRate() {
        return exeRate;
    }

    public void setExeRate(BigDecimal exeRate) {
        this.exeRate = exeRate;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getContState() {
        return contState;
    }

    public void setContState(String contState) {
        this.contState = contState;
    }

    public String getIsWhb() {
        return isWhb;
    }

    public void setIsWhb(String isWhb) {
        this.isWhb = isWhb;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusCertNo='" + cusCertNo + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "applyAmt='" + applyAmt + '\'' +
                "contStartDate='" + contStartDate + '\'' +
                "contEndDate='" + contEndDate + '\'' +
                "guarContNo='" + guarContNo + '\'' +
                "exeRate='" + exeRate + '\'' +
                "repayType='" + repayType + '\'' +
                ", contState='" + contState + '\'' +
                ", isWhb='" + isWhb + '\'' +
                '}';
    }
}
