package cn.com.yusys.yusp.dto.server.biz.xdcz0027.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：审批失败支用申请( 登记支用失败)
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0027RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdcz0022RespDto{" +
                ", data=" + data +
                '}';
    }
}
