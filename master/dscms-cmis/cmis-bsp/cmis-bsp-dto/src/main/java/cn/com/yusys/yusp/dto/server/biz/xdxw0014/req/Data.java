package cn.com.yusys.yusp.dto.server.biz.xdxw0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：优企贷共借人签订
 *
 * @author sunzhen
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "commonCertNo")
    private String commonCertNo;//共借人证件号
    @JsonProperty(value = "taskSerno")
    private String taskSerno;//优企贷待办流水
    @JsonProperty(value = "imageSerno1")
    private String imageSerno1;//影像流水号1
    @JsonProperty(value = "imageSerno2")
    private String imageSerno2;//影像流水号2

    public String getCommonCertNo() {
        return commonCertNo;
    }

    public void setCommonCertNo(String commonCertNo) {
        this.commonCertNo = commonCertNo;
    }

    public String getTaskSerno() {
        return taskSerno;
    }

    public void setTaskSerno(String taskSerno) {
        this.taskSerno = taskSerno;
    }

    public String getImageSerno1() {
        return imageSerno1;
    }

    public void setImageSerno1(String imageSerno1) {
        this.imageSerno1 = imageSerno1;
    }

    public String getImageSerno2() {
        return imageSerno2;
    }

    public void setImageSerno2(String imageSerno2) {
        this.imageSerno2 = imageSerno2;
    }

    @Override
    public String toString() {
        return "Data{" +
                "commonCertNo='" + commonCertNo + '\'' +
                ", taskSerno='" + taskSerno + '\'' +
                ", imageSerno1='" + imageSerno1 + '\'' +
                ", imageSerno2='" + imageSerno2 + '\'' +
                '}';
    }
}
