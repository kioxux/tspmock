package cn.com.yusys.yusp.dto.server.biz.xdxw0082.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询上一笔抵押贷款的余额接口（增享贷）
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;


    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cus_id='" + cus_id + '\'' +
                '}';
    }

}
