package cn.com.yusys.yusp.dto.server.biz.xdsx0018.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/5/6 15:37
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同编号
    @JsonProperty(value = "cn_cont_no")
    private String cn_cont_no;//中文合同编号
    @JsonProperty(value = "manager_id")
    private String manager_id;//客户经理号
    @JsonProperty(value = "manager_name")
    private String manager_name;//客户经理名称
    @JsonProperty(value = "old_bill_no")
    private String old_bill_no;//旧借据编号
    @JsonProperty(value = "new_bill_no")
    private String new_bill_no;//新借据编号
    @JsonProperty(value = "new_loan_amount")
    private String new_loan_amount;//新借据金额
    @JsonProperty(value = "new_bill_time")
    private BigDecimal new_bill_time;//新借据期限
    @JsonProperty(value = "new_start_date")
    private String new_start_date;//新借据起始日
    @JsonProperty(value = "new_end_date")
    private String new_end_date;//新借据到期日
    @JsonProperty(value = "new_bill_chack_rate")
    private BigDecimal new_bill_chack_rate;//新借据审批利率
    @JsonProperty(value = "new_bill_card_num")
    private String new_bill_card_num;//新借据放款/还款卡号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getCn_cont_no() {
        return cn_cont_no;
    }

    public void setCn_cont_no(String cn_cont_no) {
        this.cn_cont_no = cn_cont_no;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getOld_bill_no() {
        return old_bill_no;
    }

    public void setOld_bill_no(String old_bill_no) {
        this.old_bill_no = old_bill_no;
    }

    public String getNew_bill_no() {
        return new_bill_no;
    }

    public void setNew_bill_no(String new_bill_no) {
        this.new_bill_no = new_bill_no;
    }

    public String getNew_loan_amount() {
        return new_loan_amount;
    }

    public void setNew_loan_amount(String new_loan_amount) {
        this.new_loan_amount = new_loan_amount;
    }

    public BigDecimal getNew_bill_time() {
        return new_bill_time;
    }

    public void setNew_bill_time(BigDecimal new_bill_time) {
        this.new_bill_time = new_bill_time;
    }

    public String getNew_start_date() {
        return new_start_date;
    }

    public void setNew_start_date(String new_start_date) {
        this.new_start_date = new_start_date;
    }

    public String getNew_end_date() {
        return new_end_date;
    }

    public void setNew_end_date(String new_end_date) {
        this.new_end_date = new_end_date;
    }

    public BigDecimal getNew_bill_chack_rate() {
        return new_bill_chack_rate;
    }

    public void setNew_bill_chack_rate(BigDecimal new_bill_chack_rate) {
        this.new_bill_chack_rate = new_bill_chack_rate;
    }

    public String getNew_bill_card_num() {
        return new_bill_card_num;
    }

    public void setNew_bill_card_num(String new_bill_card_num) {
        this.new_bill_card_num = new_bill_card_num;
    }

    @Override
    public String toString() {
        return "Data{" +
                "serno='" + serno + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", cn_cont_no='" + cn_cont_no + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", old_bill_no='" + old_bill_no + '\'' +
                ", new_bill_no='" + new_bill_no + '\'' +
                ", new_loan_amount='" + new_loan_amount + '\'' +
                ", new_bill_time=" + new_bill_time +
                ", new_start_date='" + new_start_date + '\'' +
                ", new_end_date='" + new_end_date + '\'' +
                ", new_bill_chack_rate=" + new_bill_chack_rate +
                ", new_bill_card_num='" + new_bill_card_num + '\'' +
                '}';
    }
}
