package cn.com.yusys.yusp.dto.server.biz.hyy.babiur01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：押品状态变更推送
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Babiur01ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "district_code")
    private String district_code;//区县代码
    @JsonProperty(value = "certificate_number")
    private String certificate_number;//不动产权证书号
    @JsonProperty(value = "status")
    private String status;//押品状态

    public String getDistrict_code() {
        return district_code;
    }

    public void setDistrict_code(String district_code) {
        this.district_code = district_code;
    }

    public String getCertificate_number() {
        return certificate_number;
    }

    public void setCertificate_number(String certificate_number) {
        this.certificate_number = certificate_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BabiurReqDto{" +
                "district_code='" + district_code + '\'' +
                ", certificate_number='" + certificate_number + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
