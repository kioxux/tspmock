package cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：待清算账户查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2098RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "zongbshu")
    private Integer zongbshu;//总笔数
    @JsonProperty(value = "lstacctinfo")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Lstacctinfo> lstacctinfo;


    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    public List<Lstacctinfo> getLstacctinfo() {
        return lstacctinfo;
    }

    public void setLstacctinfo(List<Lstacctinfo> lstacctinfo) {
        this.lstacctinfo = lstacctinfo;
    }

    @Override
    public String toString() {
        return "Dp2098RespDto{" +
                "kehuhaoo='" + kehuhaoo + '\'' +
                ", zongbshu=" + zongbshu +
                ", lstacctinfo=" + lstacctinfo +
                '}';
    }
}
