package cn.com.yusys.yusp.dto.client.esb.ypxt.billyp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：票据信息同步接口
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class BillypReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custno")
    private String custno;//押品所有人编号
    @JsonProperty(value = "custnm")
    private String custnm;//押品所有人名称
    @JsonProperty(value = "custlx")
    private String custlx;//押品所有人类型
    @JsonProperty(value = "zjlxyp")
    private String zjlxyp;//押品所有人证件类型
    @JsonProperty(value = "zjhmyp")
    private String zjhmyp;//押品所有人证件号码
    @JsonProperty(value = "dzypnm")
    private String dzypnm;//质押品名称
    @JsonProperty(value = "cjxtyp")
    private String cjxtyp;//创建系统
    @JsonProperty(value = "ghridy")
    private String ghridy;//管户人
    @JsonProperty(value = "zhxgry")
    private String zhxgry;//最后修改人
    @JsonProperty(value = "zhxgsj")
    private String zhxgsj;//最后更新时间
    @JsonProperty(value = "zhxgjg")
    private String zhxgjg;//最后修改人机构
    @JsonProperty(value = "pjhmyp")
    private String pjhmyp;//票据号码
    @JsonProperty(value = "cprorg")
    private String cprorg;//出票人组织机构代码
    @JsonProperty(value = "cprnmy")
    private String cprnmy;//出票人名称
    @JsonProperty(value = "cprlxy")
    private String cprlxy;//出票人类型
    @JsonProperty(value = "cprhnm")
    private String cprhnm;//出票人开户行名称
    @JsonProperty(value = "cprhno")
    private String cprhno;//出票人开户行行号
    @JsonProperty(value = "cprzhy")
    private String cprzhy;//出票人账号
    @JsonProperty(value = "cdrorg")
    private String cdrorg;//承兑人组织机构代码
    @JsonProperty(value = "cdrhno")
    private String cdrhno;//承兑行行号
    @JsonProperty(value = "cdrhnm")
    private String cdrhnm;//承兑人名称
    @JsonProperty(value = "cdrlxy")
    private String cdrlxy;//承兑人类型
    @JsonProperty(value = "skrorg")
    private String skrorg;//收款人组织机构代码
    @JsonProperty(value = "skrhnm")
    private String skrhnm;//收款人开户行行名
    @JsonProperty(value = "skrhno")
    private String skrhno;//收款人开户行行号
    @JsonProperty(value = "skrzhy")
    private String skrzhy;//收款人账号
    @JsonProperty(value = "skrnmy")
    private String skrnmy;//收款人名称
    @JsonProperty(value = "skrlxy")
    private String skrlxy;//收款人类型
    @JsonProperty(value = "ispjqs")
    private String ispjqs;//是否有票据前手
    @JsonProperty(value = "pjqsjg")
    private String pjqsjg;//票据前手组织机构代码
    @JsonProperty(value = "pjqsnm")
    private String pjqsnm;//票据前手名称
    @JsonProperty(value = "pjqslx")
    private String pjqslx;//票据前手类型
    @JsonProperty(value = "pmjeyp")
    private BigDecimal pmjeyp;//票面金额
    @JsonProperty(value = "bzypxt")
    private String bzypxt;//币种
    @JsonProperty(value = "llypxt")
    private BigDecimal llypxt;//利率
    @JsonProperty(value = "cprqyp")
    private String cprqyp;//出票日期
    @JsonProperty(value = "pjdqrq")
    private String pjdqrq;//票据到期日期
    @JsonProperty(value = "cxcfqk")
    private String cxcfqk;//查询查复情况
    @JsonProperty(value = "cxcfrq")
    private String cxcfrq;//查询查复日期
    @JsonProperty(value = "assetNo")
    private String assetNo;

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getCustlx() {
        return custlx;
    }

    public void setCustlx(String custlx) {
        this.custlx = custlx;
    }

    public String getZjlxyp() {
        return zjlxyp;
    }

    public void setZjlxyp(String zjlxyp) {
        this.zjlxyp = zjlxyp;
    }

    public String getZjhmyp() {
        return zjhmyp;
    }

    public void setZjhmyp(String zjhmyp) {
        this.zjhmyp = zjhmyp;
    }

    public String getDzypnm() {
        return dzypnm;
    }

    public void setDzypnm(String dzypnm) {
        this.dzypnm = dzypnm;
    }

    public String getCjxtyp() {
        return cjxtyp;
    }

    public void setCjxtyp(String cjxtyp) {
        this.cjxtyp = cjxtyp;
    }

    public String getGhridy() {
        return ghridy;
    }

    public void setGhridy(String ghridy) {
        this.ghridy = ghridy;
    }

    public String getZhxgry() {
        return zhxgry;
    }

    public void setZhxgry(String zhxgry) {
        this.zhxgry = zhxgry;
    }

    public String getZhxgsj() {
        return zhxgsj;
    }

    public void setZhxgsj(String zhxgsj) {
        this.zhxgsj = zhxgsj;
    }

    public String getZhxgjg() {
        return zhxgjg;
    }

    public void setZhxgjg(String zhxgjg) {
        this.zhxgjg = zhxgjg;
    }

    public String getPjhmyp() {
        return pjhmyp;
    }

    public void setPjhmyp(String pjhmyp) {
        this.pjhmyp = pjhmyp;
    }

    public String getCprorg() {
        return cprorg;
    }

    public void setCprorg(String cprorg) {
        this.cprorg = cprorg;
    }

    public String getCprnmy() {
        return cprnmy;
    }

    public void setCprnmy(String cprnmy) {
        this.cprnmy = cprnmy;
    }

    public String getCprlxy() {
        return cprlxy;
    }

    public void setCprlxy(String cprlxy) {
        this.cprlxy = cprlxy;
    }

    public String getCprhnm() {
        return cprhnm;
    }

    public void setCprhnm(String cprhnm) {
        this.cprhnm = cprhnm;
    }

    public String getCprhno() {
        return cprhno;
    }

    public void setCprhno(String cprhno) {
        this.cprhno = cprhno;
    }

    public String getCprzhy() {
        return cprzhy;
    }

    public void setCprzhy(String cprzhy) {
        this.cprzhy = cprzhy;
    }

    public String getCdrorg() {
        return cdrorg;
    }

    public void setCdrorg(String cdrorg) {
        this.cdrorg = cdrorg;
    }

    public String getCdrhno() {
        return cdrhno;
    }

    public void setCdrhno(String cdrhno) {
        this.cdrhno = cdrhno;
    }

    public String getCdrhnm() {
        return cdrhnm;
    }

    public void setCdrhnm(String cdrhnm) {
        this.cdrhnm = cdrhnm;
    }

    public String getCdrlxy() {
        return cdrlxy;
    }

    public void setCdrlxy(String cdrlxy) {
        this.cdrlxy = cdrlxy;
    }

    public String getSkrorg() {
        return skrorg;
    }

    public void setSkrorg(String skrorg) {
        this.skrorg = skrorg;
    }

    public String getSkrhnm() {
        return skrhnm;
    }

    public void setSkrhnm(String skrhnm) {
        this.skrhnm = skrhnm;
    }

    public String getSkrhno() {
        return skrhno;
    }

    public void setSkrhno(String skrhno) {
        this.skrhno = skrhno;
    }

    public String getSkrzhy() {
        return skrzhy;
    }

    public void setSkrzhy(String skrzhy) {
        this.skrzhy = skrzhy;
    }

    public String getSkrnmy() {
        return skrnmy;
    }

    public void setSkrnmy(String skrnmy) {
        this.skrnmy = skrnmy;
    }

    public String getSkrlxy() {
        return skrlxy;
    }

    public void setSkrlxy(String skrlxy) {
        this.skrlxy = skrlxy;
    }

    public String getIspjqs() {
        return ispjqs;
    }

    public void setIspjqs(String ispjqs) {
        this.ispjqs = ispjqs;
    }

    public String getPjqsjg() {
        return pjqsjg;
    }

    public void setPjqsjg(String pjqsjg) {
        this.pjqsjg = pjqsjg;
    }

    public String getPjqsnm() {
        return pjqsnm;
    }

    public void setPjqsnm(String pjqsnm) {
        this.pjqsnm = pjqsnm;
    }

    public String getPjqslx() {
        return pjqslx;
    }

    public void setPjqslx(String pjqslx) {
        this.pjqslx = pjqslx;
    }

    public BigDecimal getPmjeyp() {
        return pmjeyp;
    }

    public void setPmjeyp(BigDecimal pmjeyp) {
        this.pmjeyp = pmjeyp;
    }

    public String getBzypxt() {
        return bzypxt;
    }

    public void setBzypxt(String bzypxt) {
        this.bzypxt = bzypxt;
    }

    public BigDecimal getLlypxt() {
        return llypxt;
    }

    public void setLlypxt(BigDecimal llypxt) {
        this.llypxt = llypxt;
    }

    public String getCprqyp() {
        return cprqyp;
    }

    public void setCprqyp(String cprqyp) {
        this.cprqyp = cprqyp;
    }

    public String getPjdqrq() {
        return pjdqrq;
    }

    public void setPjdqrq(String pjdqrq) {
        this.pjdqrq = pjdqrq;
    }

    public String getCxcfqk() {
        return cxcfqk;
    }

    public void setCxcfqk(String cxcfqk) {
        this.cxcfqk = cxcfqk;
    }

    public String getCxcfrq() {
        return cxcfrq;
    }

    public void setCxcfrq(String cxcfrq) {
        this.cxcfrq = cxcfrq;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    @Override
    public String toString() {
        return "BillypReqDto{" +
                "custno='" + custno + '\'' +
                ", custnm='" + custnm + '\'' +
                ", custlx='" + custlx + '\'' +
                ", zjlxyp='" + zjlxyp + '\'' +
                ", zjhmyp='" + zjhmyp + '\'' +
                ", dzypnm='" + dzypnm + '\'' +
                ", cjxtyp='" + cjxtyp + '\'' +
                ", ghridy='" + ghridy + '\'' +
                ", zhxgry='" + zhxgry + '\'' +
                ", zhxgsj='" + zhxgsj + '\'' +
                ", zhxgjg='" + zhxgjg + '\'' +
                ", pjhmyp='" + pjhmyp + '\'' +
                ", cprorg='" + cprorg + '\'' +
                ", cprnmy='" + cprnmy + '\'' +
                ", cprlxy='" + cprlxy + '\'' +
                ", cprhnm='" + cprhnm + '\'' +
                ", cprhno='" + cprhno + '\'' +
                ", cprzhy='" + cprzhy + '\'' +
                ", cdrorg='" + cdrorg + '\'' +
                ", cdrhno='" + cdrhno + '\'' +
                ", cdrhnm='" + cdrhnm + '\'' +
                ", cdrlxy='" + cdrlxy + '\'' +
                ", skrorg='" + skrorg + '\'' +
                ", skrhnm='" + skrhnm + '\'' +
                ", skrhno='" + skrhno + '\'' +
                ", skrzhy='" + skrzhy + '\'' +
                ", skrnmy='" + skrnmy + '\'' +
                ", skrlxy='" + skrlxy + '\'' +
                ", ispjqs='" + ispjqs + '\'' +
                ", pjqsjg='" + pjqsjg + '\'' +
                ", pjqsnm='" + pjqsnm + '\'' +
                ", pjqslx='" + pjqslx + '\'' +
                ", pmjeyp=" + pmjeyp +
                ", bzypxt='" + bzypxt + '\'' +
                ", llypxt=" + llypxt +
                ", cprqyp='" + cprqyp + '\'' +
                ", pjdqrq='" + pjdqrq + '\'' +
                ", cxcfqk='" + cxcfqk + '\'' +
                ", cxcfrq='" + cxcfrq + '\'' +
                ", assetNo='" + assetNo + '\'' +
                '}';
    }
}
