package cn.com.yusys.yusp.dto.client.esb.circp.fb1213.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：押品出库通知
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1213ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "CUST_ID")
    private String CUST_ID;//客户号
    @JsonProperty(value = "CUST_NAME")
    private String CUST_NAME;//客户名称
    @JsonProperty(value = "CERT_TYPE")
    private String CERT_TYPE;//证件类型
    @JsonProperty(value = "CERT_NO")
    private String CERT_NO;//证件号码
    @JsonProperty(value = "GUAR_LIST")
    private java.util.List<GUAR_LIST> GUAR_LIST;

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getCUST_ID() {
        return CUST_ID;
    }

    @JsonIgnore
    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    @JsonIgnore
    public String getCUST_NAME() {
        return CUST_NAME;
    }

    @JsonIgnore
    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    @JsonIgnore
    public String getCERT_TYPE() {
        return CERT_TYPE;
    }

    @JsonIgnore
    public void setCERT_TYPE(String CERT_TYPE) {
        this.CERT_TYPE = CERT_TYPE;
    }

    @JsonIgnore
    public String getCERT_NO() {
        return CERT_NO;
    }

    @JsonIgnore
    public void setCERT_NO(String CERT_NO) {
        this.CERT_NO = CERT_NO;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.circp.fb1213.req.GUAR_LIST> getGUAR_LIST() {
        return GUAR_LIST;
    }

    @JsonIgnore
    public void setGUAR_LIST(List<cn.com.yusys.yusp.dto.client.esb.circp.fb1213.req.GUAR_LIST> GUAR_LIST) {
        this.GUAR_LIST = GUAR_LIST;
    }

    @Override
    public String toString() {
        return "Fb1213ReqDto{" +
                "CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", CUST_ID='" + CUST_ID + '\'' +
                ", CUST_NAME='" + CUST_NAME + '\'' +
                ", CERT_TYPE='" + CERT_TYPE + '\'' +
                ", CERT_NO='" + CERT_NO + '\'' +
                ", GUAR_LIST=" + GUAR_LIST +
                '}';
    }
}
