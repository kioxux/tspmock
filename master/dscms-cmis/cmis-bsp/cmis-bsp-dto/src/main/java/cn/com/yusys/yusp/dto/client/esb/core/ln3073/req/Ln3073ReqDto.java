package cn.com.yusys.yusp.dto.client.esb.core.ln3073.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：用于贷款定制期供计划的修改调整
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3073ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
	@JsonProperty(value = "lstdzqgjh")
	private java.util.List <Lstdzqgjh> lstdzqgjh;

	public String getDkjiejuh() {
		return dkjiejuh;
	}

	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}

	public List<Lstdzqgjh> getLstdzqgjh() {
		return lstdzqgjh;
	}

	public void setLstdzqgjh(List<Lstdzqgjh> lstdzqgjh) {
		this.lstdzqgjh = lstdzqgjh;
	}

	@Override
	public String toString() {
		return "Ln3073ReqDto{" +
				"dkjiejuh='" + dkjiejuh + '\'' +
				", lstdzqgjh=" + lstdzqgjh +
				'}';
	}
}
