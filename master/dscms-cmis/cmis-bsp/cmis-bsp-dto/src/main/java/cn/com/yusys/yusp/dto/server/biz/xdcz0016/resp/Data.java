package cn.com.yusys.yusp.dto.server.biz.xdcz0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：支用列表查询(微信小程序)
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isManagerId")
    private String isManagerId;//是否为小贷客户经理
    @JsonProperty(value = "managerIdNo")
    private String managerIdNo;//小贷客户经理工号
    @JsonProperty(value = "responseRecordQnt")
    private String responseRecordQnt;//响应记录数
    @JsonProperty(value = "responseList")
    private java.util.List<List> responseList;


    public String getIsManagerId() {
        return isManagerId;
    }

    public void setIsManagerId(String isManagerId) {
        this.isManagerId = isManagerId;
    }

    public String getManagerIdNo() {
        return managerIdNo;
    }

    public void setManagerIdNo(String managerIdNo) {
        this.managerIdNo = managerIdNo;
    }

    public String getResponseRecordQnt() {
        return responseRecordQnt;
    }

    public void setResponseRecordQnt(String responseRecordQnt) {
        this.responseRecordQnt = responseRecordQnt;
    }

    public java.util.List<List> getResponseList() {
        return responseList;
    }

    public void setResponseList(java.util.List<List> responseList) {
        this.responseList = responseList;
    }
    @Override
    public String toString() {
        return "Data{" +
                "isManagerId='" + isManagerId + '\'' +
                ", managerIdNo='" + managerIdNo + '\'' +
                ", responseRecordQnt='" + responseRecordQnt + '\'' +
                ", responseList=" + responseList +
                '}';
    }
}
