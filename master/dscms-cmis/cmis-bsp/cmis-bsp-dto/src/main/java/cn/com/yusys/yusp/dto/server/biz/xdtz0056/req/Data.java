package cn.com.yusys.yusp.dto.server.biz.xdtz0056.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 11:02:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 11:02
 * @since 2021/5/22 11:02
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billno")
    private String billno;//借据号
    @JsonProperty(value = "cusid")
    private String cusid;//客户号

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getCusid() {
        return cusid;
    }

    public void setCusid(String cusid) {
        this.cusid = cusid;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billno='" + billno + '\'' +
                ", cusid='" + cusid + '\'' +
                '}';
    }
}
