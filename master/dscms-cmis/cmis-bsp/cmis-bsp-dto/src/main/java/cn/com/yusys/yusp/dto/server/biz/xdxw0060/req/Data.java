package cn.com.yusys.yusp.dto.server.biz.xdxw0060.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户及配偶信用类小微业务贷款授信金额
 * @Author zhangpeng
 * @Date 2021/4/25 9:56
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "certNo='" + certNo + '\'' +
                '}';
    }
}
