package cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：押品我行确认价值同步接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class EvalypRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "evalypListInfoResp")
    private java.util.List<EvalypListInfoResp> evalypListInfoResp;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public List<EvalypListInfoResp> getEvalypListInfoResp() {
        return evalypListInfoResp;
    }

    public void setEvalypListInfoResp(List<EvalypListInfoResp> evalypListInfoResp) {
        this.evalypListInfoResp = evalypListInfoResp;
    }

    @Override
    public String toString() {
        return "EvalypRespDto{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", evalypListInfoResp=" + evalypListInfoResp +
                '}';
    }
}
