package cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：新入职人员信息登记
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XxdentList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "psncde")
    private String psncde;//工号
    @JsonProperty(value = "psnnam")
    private String psnnam;//姓名
    @JsonProperty(value = "hrdpcd")
    private String hrdpcd;//人力编制部门编号
    @JsonProperty(value = "hrdpna")
    private String hrdpna;//人力编制部门名称
    @JsonProperty(value = "hrwdcd")
    private String hrwdcd;//人力工作部门编号
    @JsonProperty(value = "hrwdna")
    private String hrwdna;//人力工作部门名称
    @JsonProperty(value = "sydpcd")
    private String sydpcd;//核心编制部门编号
    @JsonProperty(value = "sydpna")
    private String sydpna;//核心编制部门名称
    @JsonProperty(value = "sywdcd")
    private String sywdcd;//核心工作部门编号
    @JsonProperty(value = "sywdna")
    private String sywdna;//核心工作部门名称
    @JsonProperty(value = "jobcde")
    private String jobcde;//岗位编号
    @JsonProperty(value = "jobnam")
    private String jobnam;//岗位名称
    @JsonProperty(value = "dtrkcd")
    private String dtrkcd;//职务编号
    @JsonProperty(value = "dtrkna")
    private String dtrkna;//职务名称
    @JsonProperty(value = "psnste")
    private String psnste;//员工状态编号
    @JsonProperty(value = "wrkste")
    private String wrkste;//工作状态编号
    @JsonProperty(value = "psncls")
    private String psncls;//员工性质编号
    @JsonProperty(value = "idtftp")
    private String idtftp;//证件类型
    @JsonProperty(value = "idtfno")
    private String idtfno;//营业执照号码
    @JsonProperty(value = "mobile")
    private String mobile;//手机号
    @JsonProperty(value = "wechat")
    private String wechat;//微信号
    @JsonProperty(value = "emailx")
    private String emailx;//邮箱
    @JsonProperty(value = "psnsex")
    private String psnsex;//性别编号
    @JsonProperty(value = "borndt")
    private String borndt;//出生年月
    @JsonProperty(value = "psnage")
    private String psnage;//年龄
    @JsonProperty(value = "polity")
    private String polity;//政治面貌编号
    @JsonProperty(value = "psnedu")
    private String psnedu;//学历编号
    @JsonProperty(value = "psnttl")
    private String psnttl;//职称编号
    @JsonProperty(value = "workdt")
    private String workdt;//参加工作时间
    @JsonProperty(value = "bankdt")
    private String bankdt;//人行时间
    @JsonProperty(value = "workag")
    private String workag;//工龄
    @JsonProperty(value = "bankag")
    private String bankag;//行龄
    @JsonProperty(value = "cntste")
    private String cntste;//上柜权限编号
    @JsonProperty(value = "saledt")
    private String saledt;//营销证书到期日
    @JsonProperty(value = "antidt")
    private String antidt;//反假证书到期日

    public String getPsncde() {
        return psncde;
    }

    public void setPsncde(String psncde) {
        this.psncde = psncde;
    }

    public String getPsnnam() {
        return psnnam;
    }

    public void setPsnnam(String psnnam) {
        this.psnnam = psnnam;
    }

    public String getHrdpcd() {
        return hrdpcd;
    }

    public void setHrdpcd(String hrdpcd) {
        this.hrdpcd = hrdpcd;
    }

    public String getHrdpna() {
        return hrdpna;
    }

    public void setHrdpna(String hrdpna) {
        this.hrdpna = hrdpna;
    }

    public String getHrwdcd() {
        return hrwdcd;
    }

    public void setHrwdcd(String hrwdcd) {
        this.hrwdcd = hrwdcd;
    }

    public String getHrwdna() {
        return hrwdna;
    }

    public void setHrwdna(String hrwdna) {
        this.hrwdna = hrwdna;
    }

    public String getSydpcd() {
        return sydpcd;
    }

    public void setSydpcd(String sydpcd) {
        this.sydpcd = sydpcd;
    }

    public String getSydpna() {
        return sydpna;
    }

    public void setSydpna(String sydpna) {
        this.sydpna = sydpna;
    }

    public String getSywdcd() {
        return sywdcd;
    }

    public void setSywdcd(String sywdcd) {
        this.sywdcd = sywdcd;
    }

    public String getSywdna() {
        return sywdna;
    }

    public void setSywdna(String sywdna) {
        this.sywdna = sywdna;
    }

    public String getJobcde() {
        return jobcde;
    }

    public void setJobcde(String jobcde) {
        this.jobcde = jobcde;
    }

    public String getJobnam() {
        return jobnam;
    }

    public void setJobnam(String jobnam) {
        this.jobnam = jobnam;
    }

    public String getDtrkcd() {
        return dtrkcd;
    }

    public void setDtrkcd(String dtrkcd) {
        this.dtrkcd = dtrkcd;
    }

    public String getDtrkna() {
        return dtrkna;
    }

    public void setDtrkna(String dtrkna) {
        this.dtrkna = dtrkna;
    }

    public String getPsnste() {
        return psnste;
    }

    public void setPsnste(String psnste) {
        this.psnste = psnste;
    }

    public String getWrkste() {
        return wrkste;
    }

    public void setWrkste(String wrkste) {
        this.wrkste = wrkste;
    }

    public String getPsncls() {
        return psncls;
    }

    public void setPsncls(String psncls) {
        this.psncls = psncls;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getEmailx() {
        return emailx;
    }

    public void setEmailx(String emailx) {
        this.emailx = emailx;
    }

    public String getPsnsex() {
        return psnsex;
    }

    public void setPsnsex(String psnsex) {
        this.psnsex = psnsex;
    }

    public String getBorndt() {
        return borndt;
    }

    public void setBorndt(String borndt) {
        this.borndt = borndt;
    }

    public String getPsnage() {
        return psnage;
    }

    public void setPsnage(String psnage) {
        this.psnage = psnage;
    }

    public String getPolity() {
        return polity;
    }

    public void setPolity(String polity) {
        this.polity = polity;
    }

    public String getPsnedu() {
        return psnedu;
    }

    public void setPsnedu(String psnedu) {
        this.psnedu = psnedu;
    }

    public String getPsnttl() {
        return psnttl;
    }

    public void setPsnttl(String psnttl) {
        this.psnttl = psnttl;
    }

    public String getWorkdt() {
        return workdt;
    }

    public void setWorkdt(String workdt) {
        this.workdt = workdt;
    }

    public String getBankdt() {
        return bankdt;
    }

    public void setBankdt(String bankdt) {
        this.bankdt = bankdt;
    }

    public String getWorkag() {
        return workag;
    }

    public void setWorkag(String workag) {
        this.workag = workag;
    }

    public String getBankag() {
        return bankag;
    }

    public void setBankag(String bankag) {
        this.bankag = bankag;
    }

    public String getCntste() {
        return cntste;
    }

    public void setCntste(String cntste) {
        this.cntste = cntste;
    }

    public String getSaledt() {
        return saledt;
    }

    public void setSaledt(String saledt) {
        this.saledt = saledt;
    }

    public String getAntidt() {
        return antidt;
    }

    public void setAntidt(String antidt) {
        this.antidt = antidt;
    }

    @Override
    public String toString() {
        return "XxdentList{" +
                "psncde='" + psncde + '\'' +
                ", psnnam='" + psnnam + '\'' +
                ", hrdpcd='" + hrdpcd + '\'' +
                ", hrdpna='" + hrdpna + '\'' +
                ", hrwdcd='" + hrwdcd + '\'' +
                ", hrwdna='" + hrwdna + '\'' +
                ", sydpcd='" + sydpcd + '\'' +
                ", sydpna='" + sydpna + '\'' +
                ", sywdcd='" + sywdcd + '\'' +
                ", sywdna='" + sywdna + '\'' +
                ", jobcde='" + jobcde + '\'' +
                ", jobnam='" + jobnam + '\'' +
                ", dtrkcd='" + dtrkcd + '\'' +
                ", dtrkna='" + dtrkna + '\'' +
                ", psnste='" + psnste + '\'' +
                ", wrkste='" + wrkste + '\'' +
                ", psncls='" + psncls + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", mobile='" + mobile + '\'' +
                ", wechat='" + wechat + '\'' +
                ", emailx='" + emailx + '\'' +
                ", psnsex='" + psnsex + '\'' +
                ", borndt='" + borndt + '\'' +
                ", psnage='" + psnage + '\'' +
                ", polity='" + polity + '\'' +
                ", psnedu='" + psnedu + '\'' +
                ", psnttl='" + psnttl + '\'' +
                ", workdt='" + workdt + '\'' +
                ", bankdt='" + bankdt + '\'' +
                ", workag='" + workag + '\'' +
                ", bankag='" + bankag + '\'' +
                ", cntste='" + cntste + '\'' +
                ", saledt='" + saledt + '\'' +
                ", antidt='" + antidt + '\'' +
                '}';
    }
}
