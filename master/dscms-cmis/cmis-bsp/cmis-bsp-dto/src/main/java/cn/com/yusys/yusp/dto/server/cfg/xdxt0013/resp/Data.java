package cn.com.yusys.yusp.dto.server.cfg.xdxt0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/5/25 10:07:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 10:07
 * @since 2021/5/25 10:07
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "optList")
    private List<OptList> optList;

    public List<OptList> getOptList() {
        return optList;
    }

    public void setOptList(List<OptList> optList) {
        this.optList = optList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "optList=" + optList +
                '}';
    }
}
