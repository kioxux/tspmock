package cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：账单列表查询
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D12050RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pgeflg")
    private String pgeflg;//是否有下一页标志
    @JsonProperty(value = "frtrow")
    private String frtrow;//开始位置
    @JsonProperty(value = "lstrow")
    private String lstrow;//结束位置
    @JsonProperty(value = "cardno")
    private String cardno;//卡号
    @JsonProperty(value = "totrow")
    private Integer totrow;//总条数
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.List> list;//list

    public String getPgeflg() {
        return pgeflg;
    }

    public void setPgeflg(String pgeflg) {
        this.pgeflg = pgeflg;
    }

    public String getFrtrow() {
        return frtrow;
    }

    public void setFrtrow(String frtrow) {
        this.frtrow = frtrow;
    }

    public String getLstrow() {
        return lstrow;
    }

    public void setLstrow(String lstrow) {
        this.lstrow = lstrow;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public Integer getTotrow() {
        return totrow;
    }

    public void setTotrow(Integer totrow) {
        this.totrow = totrow;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "D12050RespDto{" +
                "pgeflg='" + pgeflg + '\'' +
                ", frtrow='" + frtrow + '\'' +
                ", lstrow='" + lstrow + '\'' +
                ", cardno='" + cardno + '\'' +
                ", totrow=" + totrow +
                ", list=" + list +
                '}';
    }
}
