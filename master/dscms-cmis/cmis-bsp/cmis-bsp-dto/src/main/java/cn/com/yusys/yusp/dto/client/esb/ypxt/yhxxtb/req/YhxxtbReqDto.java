package cn.com.yusys.yusp.dto.client.esb.ypxt.yhxxtb.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：用户信息同步接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class YhxxtbReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//处理码
    @JsonProperty(value = "servtp")
    private String servtp;//渠道
    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "userid")
    private String userid;//柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//部门号
    @JsonProperty(value = "usrnid")
    private String usrnid;//用户编号
    @JsonProperty(value = "usrnam")
    private String usrnam;//用户名称
    @JsonProperty(value = "urcert")
    private String urcert;//证件类型
    @JsonProperty(value = "urcern")
    private String urcern;//证件号码
    @JsonProperty(value = "uorgid")
    private String uorgid;//所属机构码
    @JsonProperty(value = "sursta")
    private String sursta;//用户状态

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getUsrnid() {
        return usrnid;
    }

    public void setUsrnid(String usrnid) {
        this.usrnid = usrnid;
    }

    public String getUsrnam() {
        return usrnam;
    }

    public void setUsrnam(String usrnam) {
        this.usrnam = usrnam;
    }

    public String getUrcert() {
        return urcert;
    }

    public void setUrcert(String urcert) {
        this.urcert = urcert;
    }

    public String getUrcern() {
        return urcern;
    }

    public void setUrcern(String urcern) {
        this.urcern = urcern;
    }

    public String getUorgid() {
        return uorgid;
    }

    public void setUorgid(String uorgid) {
        this.uorgid = uorgid;
    }

    public String getSursta() {
        return sursta;
    }

    public void setSursta(String sursta) {
        this.sursta = sursta;
    }

    @Override
    public String toString() {
        return "YhxxtbReqDto{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "usrnid='" + usrnid + '\'' +
                "usrnam='" + usrnam + '\'' +
                "urcert='" + urcert + '\'' +
                "urcern='" + urcern + '\'' +
                "uorgid='" + uorgid + '\'' +
                "sursta='" + sursta + '\'' +
                '}';
    }
}  
