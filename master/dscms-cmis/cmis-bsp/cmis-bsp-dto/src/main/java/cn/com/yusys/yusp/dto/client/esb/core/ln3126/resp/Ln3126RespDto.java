package cn.com.yusys.yusp.dto.client.esb.core.ln3126.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：贷款费用交易明细查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3126RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3126.resp.Listdkfycx> listdkfycx;

	public List<Listdkfycx> getListdkfycx() {
		return listdkfycx;
	}

	public void setListdkfycx(List<Listdkfycx> listdkfycx) {
		this.listdkfycx = listdkfycx;
	}

	@Override
	public String toString() {
		return "Ln3126RespDto{" +
				"listdkfycx=" + listdkfycx +
				'}';
	}
}
