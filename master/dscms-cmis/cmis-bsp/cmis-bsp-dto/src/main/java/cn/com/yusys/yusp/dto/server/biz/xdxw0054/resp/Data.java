package cn.com.yusys.yusp.dto.server.biz.xdxw0054.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询优抵贷损益表明细
 * @Author zhangpeng
 * @Date 2021/4/24 10:21
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "nearlyYear")
    private BigDecimal nearlyYear;//最近一个完整年度

    public BigDecimal getNearlyYear() {
        return nearlyYear;
    }

    public void setNearlyYear(BigDecimal nearlyYear) {
        this.nearlyYear = nearlyYear;
    }

    @Override
    public String toString() {
        return "Data{" +
                "nearlyYear='" + nearlyYear + '\'' +
                '}';
    }
}
