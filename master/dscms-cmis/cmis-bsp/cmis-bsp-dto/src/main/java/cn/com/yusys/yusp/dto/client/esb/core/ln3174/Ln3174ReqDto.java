package cn.com.yusys.yusp.dto.client.esb.core.ln3174;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：资产证券化欠款借据查询
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3174ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "chaxunzl")
    private String chaxunzl;//查询种类
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getChaxunzl() {
        return chaxunzl;
    }

    public void setChaxunzl(String chaxunzl) {
        this.chaxunzl = chaxunzl;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Ln3174ReqDto{" +
                "xieybhao='" + xieybhao + '\'' +
                "chaxunzl='" + chaxunzl + '\'' +
                "qishibis='" + qishibis + '\'' +
                "chxunbis='" + chxunbis + '\'' +
                '}';
    }
}  
