package cn.com.yusys.yusp.dto.client.esb.ypxt.businf.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷业务合同信息同步
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class BusinfRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
}
