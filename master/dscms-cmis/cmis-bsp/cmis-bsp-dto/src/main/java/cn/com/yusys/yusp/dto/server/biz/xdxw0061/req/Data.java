package cn.com.yusys.yusp.dto.server.biz.xdxw0061.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：通过无还本续贷调查表编号查询配偶核心客户号
 * @Author zhangpeng
 * @Date 2021/4/25 10:29
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//调查流水号

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "indgtSerno='" + indgtSerno + '\'' +
                '}';
    }
}
