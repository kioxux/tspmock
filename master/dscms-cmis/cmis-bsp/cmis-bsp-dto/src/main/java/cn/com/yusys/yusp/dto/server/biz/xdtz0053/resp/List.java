package cn.com.yusys.yusp.dto.server.biz.xdtz0053.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "name")
    private String name;//姓名
    @JsonProperty(value = "mainLoanManRela")
    private String mainLoanManRela;//与主贷人关系

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMainLoanManRela() {
        return mainLoanManRela;
    }

    public void setMainLoanManRela(String mainLoanManRela) {
        this.mainLoanManRela = mainLoanManRela;
    }

    @Override
    public String toString() {
        return "List{" +
                "certNo='" + certNo + '\'' +
                ", name='" + name + '\'' +
                ", mainLoanManRela='" + mainLoanManRela + '\'' +
                '}';
    }
}
