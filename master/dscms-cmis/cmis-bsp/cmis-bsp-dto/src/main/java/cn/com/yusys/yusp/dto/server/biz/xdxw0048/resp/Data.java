package cn.com.yusys.yusp.dto.server.biz.xdxw0048.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：增享贷2.0风控模型B生成批复
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "end_date")
    private String end_date;//批复有效结束日期
    @JsonProperty(value = "serno")
    private String serno;//批复号

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "end_date='" + end_date + '\'' +
                "serno='" + serno + '\'' +
                '}';
    }
}
