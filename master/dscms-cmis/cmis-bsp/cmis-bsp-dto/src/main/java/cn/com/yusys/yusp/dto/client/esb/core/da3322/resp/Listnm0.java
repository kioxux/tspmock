package cn.com.yusys.yusp.dto.client.esb.core.da3322.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 19:21
 * @since 2021/6/7 19:21
 */
@JsonPropertyOrder(alphabetic = true)
public class Listnm0 implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "dzwzleib")
    private String dzwzleib;//抵债物资类别
    @JsonProperty(value = "cqzmzlei")
    private String cqzmzlei;//产权证明种类
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "dbgdzcje")
    private BigDecimal dbgdzcje;//代保管抵债资产金额
    @JsonProperty(value = "dcldzcje")
    private BigDecimal dcldzcje;//待处理抵债资产金额
    @JsonProperty(value = "dbxdzcje")
    private BigDecimal dbxdzcje;//待变现抵债资产金额
    @JsonProperty(value = "hfeijine")
    private BigDecimal hfeijine;//还费金额
    @JsonProperty(value = "huanbjee")
    private BigDecimal huanbjee;//还本金额
    @JsonProperty(value = "hxijinee")
    private BigDecimal hxijinee;//还息金额
    @JsonProperty(value = "hqitjine")
    private BigDecimal hqitjine;//还其它金额
    @JsonProperty(value = "dzzszhao")
    private String dzzszhao;//抵债暂收账号
    @JsonProperty(value = "dzzszzxh")
    private String dzzszzxh;//抵债暂收账号子序号
    @JsonProperty(value = "rzywbhao")
    private String rzywbhao;//入账业务编号
    @JsonProperty(value = "yicldzzc")
    private BigDecimal yicldzzc;//已处置抵债资产
    @JsonProperty(value = "yihxdzzc")
    private BigDecimal yihxdzzc;//已核销抵债资产
    @JsonProperty(value = "czywbhao")
    private String czywbhao;//处置业务编号
    @JsonProperty(value = "yixjdzzc")
    private BigDecimal yixjdzzc;//已消减抵债资产
    @JsonProperty(value = "dzzcztai")
    private String dzzcztai;//抵债资产状态
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "mingxixh")
    private Integer mingxixh;//明细序号
    @JsonProperty(value = "jiaoyisj")
    private String jiaoyisj;//交易事件
    @JsonProperty(value = "shijshum")
    private String shijshum;//交易事件说明
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getDzwzleib() {
        return dzwzleib;
    }

    public void setDzwzleib(String dzwzleib) {
        this.dzwzleib = dzwzleib;
    }

    public String getCqzmzlei() {
        return cqzmzlei;
    }

    public void setCqzmzlei(String cqzmzlei) {
        this.cqzmzlei = cqzmzlei;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getDbgdzcje() {
        return dbgdzcje;
    }

    public void setDbgdzcje(BigDecimal dbgdzcje) {
        this.dbgdzcje = dbgdzcje;
    }

    public BigDecimal getDcldzcje() {
        return dcldzcje;
    }

    public void setDcldzcje(BigDecimal dcldzcje) {
        this.dcldzcje = dcldzcje;
    }

    public BigDecimal getDbxdzcje() {
        return dbxdzcje;
    }

    public void setDbxdzcje(BigDecimal dbxdzcje) {
        this.dbxdzcje = dbxdzcje;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public BigDecimal getHqitjine() {
        return hqitjine;
    }

    public void setHqitjine(BigDecimal hqitjine) {
        this.hqitjine = hqitjine;
    }

    public String getDzzszhao() {
        return dzzszhao;
    }

    public void setDzzszhao(String dzzszhao) {
        this.dzzszhao = dzzszhao;
    }

    public String getDzzszzxh() {
        return dzzszzxh;
    }

    public void setDzzszzxh(String dzzszzxh) {
        this.dzzszzxh = dzzszzxh;
    }

    public String getRzywbhao() {
        return rzywbhao;
    }

    public void setRzywbhao(String rzywbhao) {
        this.rzywbhao = rzywbhao;
    }

    public BigDecimal getYicldzzc() {
        return yicldzzc;
    }

    public void setYicldzzc(BigDecimal yicldzzc) {
        this.yicldzzc = yicldzzc;
    }

    public BigDecimal getYihxdzzc() {
        return yihxdzzc;
    }

    public void setYihxdzzc(BigDecimal yihxdzzc) {
        this.yihxdzzc = yihxdzzc;
    }

    public String getCzywbhao() {
        return czywbhao;
    }

    public void setCzywbhao(String czywbhao) {
        this.czywbhao = czywbhao;
    }

    public BigDecimal getYixjdzzc() {
        return yixjdzzc;
    }

    public void setYixjdzzc(BigDecimal yixjdzzc) {
        this.yixjdzzc = yixjdzzc;
    }

    public String getDzzcztai() {
        return dzzcztai;
    }

    public void setDzzcztai(String dzzcztai) {
        this.dzzcztai = dzzcztai;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public Integer getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Integer mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(String jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getShijshum() {
        return shijshum;
    }

    public void setShijshum(String shijshum) {
        this.shijshum = shijshum;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    @Override
    public String toString() {
        return "Listnm0{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", dzwzleib='" + dzwzleib + '\'' +
                ", cqzmzlei='" + cqzmzlei + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", dbgdzcje=" + dbgdzcje +
                ", dcldzcje=" + dcldzcje +
                ", dbxdzcje=" + dbxdzcje +
                ", hfeijine=" + hfeijine +
                ", huanbjee=" + huanbjee +
                ", hxijinee=" + hxijinee +
                ", hqitjine=" + hqitjine +
                ", dzzszhao='" + dzzszhao + '\'' +
                ", dzzszzxh='" + dzzszzxh + '\'' +
                ", rzywbhao='" + rzywbhao + '\'' +
                ", yicldzzc=" + yicldzzc +
                ", yihxdzzc=" + yihxdzzc +
                ", czywbhao='" + czywbhao + '\'' +
                ", yixjdzzc=" + yixjdzzc +
                ", dzzcztai='" + dzzcztai + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", mingxixh=" + mingxixh +
                ", jiaoyisj='" + jiaoyisj + '\'' +
                ", shijshum='" + shijshum + '\'' +
                ", jiaoyima='" + jiaoyima + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                '}';
    }
}
