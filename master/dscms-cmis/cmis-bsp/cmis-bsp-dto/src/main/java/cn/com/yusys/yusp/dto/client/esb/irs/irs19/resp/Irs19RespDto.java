package cn.com.yusys.yusp.dto.client.esb.irs.irs19.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：专业贷款基本信息同步
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs19RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}  
