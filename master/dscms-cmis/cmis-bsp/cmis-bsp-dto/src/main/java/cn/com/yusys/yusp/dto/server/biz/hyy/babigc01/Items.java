package cn.com.yusys.yusp.dto.server.biz.hyy.babigc01;

import cn.com.yusys.yusp.dto.server.biz.hyy.common.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：获取抵押信息详情
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Items implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "collateral_id")
    private String collateral_id;//抵押唯一标识
    @JsonProperty(value = "mortgagee")
    private Mortgagee mortgagee;//抵押权人
    @JsonProperty(value = "mortgagee_agent")
    private MortgageeAgent mortgagee_agent;//	抵押权人代理人
    @JsonProperty(value = "mortgagors")
    private java.util.List<Mortgagors> mortgagors;//	抵押人
    @JsonProperty(value = "real_estate_info")
    private MortgagorAgent mortgagor_agent;//	抵押人代理人
    @JsonProperty(value = "mortgagor_agent")
    private RealEstateInfo real_estate_info;//	不动产信息
    @JsonProperty(value = "mortgage_info")
    private MortgageInfo mortgage_info;//抵押信息
    @JsonProperty(value = "deal_info")
    private DealInfo deal_info;//交易信息


    public String getCollateral_id() {
        return collateral_id;
    }

    public void setCollateral_id(String collateral_id) {
        this.collateral_id = collateral_id;
    }

    public Mortgagee getMortgagee() {
        return mortgagee;
    }

    public void setMortgagee(Mortgagee mortgagee) {
        this.mortgagee = mortgagee;
    }

    public MortgageeAgent getMortgagee_agent() {
        return mortgagee_agent;
    }

    public void setMortgagee_agent(MortgageeAgent mortgagee_agent) {
        this.mortgagee_agent = mortgagee_agent;
    }

    public List<Mortgagors> getMortgagors() {
        return mortgagors;
    }

    public void setMortgagors(List<Mortgagors> mortgagors) {
        this.mortgagors = mortgagors;
    }

    public MortgagorAgent getMortgagor_agent() {
        return mortgagor_agent;
    }

    public void setMortgagor_agent(MortgagorAgent mortgagor_agent) {
        this.mortgagor_agent = mortgagor_agent;
    }

    public RealEstateInfo getReal_estate_info() {
        return real_estate_info;
    }

    public void setReal_estate_info(RealEstateInfo real_estate_info) {
        this.real_estate_info = real_estate_info;
    }

    public MortgageInfo getMortgage_info() {
        return mortgage_info;
    }

    public void setMortgage_info(MortgageInfo mortgage_info) {
        this.mortgage_info = mortgage_info;
    }

    public DealInfo getDeal_info() {
        return deal_info;
    }

    public void setDeal_info(DealInfo deal_info) {
        this.deal_info = deal_info;
    }

    @Override
    public String toString() {
        return "Items{" +
                "collateral_id='" + collateral_id + '\'' +
                ", mortgagee=" + mortgagee +
                ", mortgagee_agent=" + mortgagee_agent +
                ", mortgagors=" + mortgagors +
                ", mortgagor_agent=" + mortgagor_agent +
                ", real_estate_info=" + real_estate_info +
                ", mortgage_info=" + mortgage_info +
                ", deal_info=" + deal_info +
                '}';
    }
}
