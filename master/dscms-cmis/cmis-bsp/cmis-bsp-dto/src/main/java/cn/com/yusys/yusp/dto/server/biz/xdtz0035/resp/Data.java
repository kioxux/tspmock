package cn.com.yusys.yusp.dto.server.biz.xdtz0035.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/21 16:30:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 16:30
 * @since 2021/5/21 16:30
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHavingOverdueRecord24")
    private String isHavingOverdueRecord24;//征信业务流水号

    public String getIsHavingOverdueRecord24() {
        return isHavingOverdueRecord24;
    }

    public void setIsHavingOverdueRecord24(String isHavingOverdueRecord24) {
        this.isHavingOverdueRecord24 = isHavingOverdueRecord24;
    }

    @Override
    public String toString() {
        return "Xdtz0035RespDto{" +
                "isHavingOverdueRecord24='" + isHavingOverdueRecord24 + '\'' +
                '}';
    }
}
