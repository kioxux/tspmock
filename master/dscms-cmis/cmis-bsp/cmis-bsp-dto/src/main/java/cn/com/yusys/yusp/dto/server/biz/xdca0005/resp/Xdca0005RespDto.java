package cn.com.yusys.yusp.dto.server.biz.xdca0005.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：大额分期合同签订接口
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdca0005RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

	@JsonProperty(value = "data")
	private Data data;//data

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdca0005RespDto{" +
				"data=" + data +
				'}';
	}

}  
