package cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

// 贷款利率分段
@JsonPropertyOrder(alphabetic = true)
public class Lstdkllfd implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "zclilvbh")
    private String zclilvbh;//正常利率编号

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    @Override
    public String toString() {
        return "Lstdkllfd{" +
                "lilvfdfs='" + lilvfdfs + '\'' +
                ", lilvleix='" + lilvleix + '\'' +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", zhchlilv=" + zhchlilv +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", zclilvbh='" + zclilvbh + '\'' +
                '}';
    }
}
