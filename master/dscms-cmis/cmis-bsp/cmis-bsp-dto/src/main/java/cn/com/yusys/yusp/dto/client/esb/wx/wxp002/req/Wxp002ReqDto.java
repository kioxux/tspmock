package cn.com.yusys.yusp.dto.client.esb.wx.wxp002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷将授信额度推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxp002ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applid")
    private String applid;//对接记录ID
    @JsonProperty(value = "entnam")
    private String entnam;//企业名称
    @JsonProperty(value = "entpid")
    private String entpid;//企业代码
    @JsonProperty(value = "connum")
    private String connum;//授信合同编号
    @JsonProperty(value = "crerst")
    private String crerst;//授信结果
    @JsonProperty(value = "crerea")
    private String crerea;//授信原因
    @JsonProperty(value = "adlmt")
    private String adlmt;//授信金额
    @JsonProperty(value = "crerat")
    private String crerat;//授信利率
    @JsonProperty(value = "crebeg")
    private String crebeg;//授信开始时间
    @JsonProperty(value = "creend")
    private String creend;//授信结束时间
    @JsonProperty(value = "iffist")
    private String iffist;//是否首次贷款

    public String getApplid() {
        return applid;
    }

    public void setApplid(String applid) {
        this.applid = applid;
    }

    public String getEntnam() {
        return entnam;
    }

    public void setEntnam(String entnam) {
        this.entnam = entnam;
    }

    public String getEntpid() {
        return entpid;
    }

    public void setEntpid(String entpid) {
        this.entpid = entpid;
    }

    public String getConnum() {
        return connum;
    }

    public void setConnum(String connum) {
        this.connum = connum;
    }

    public String getCrerst() {
        return crerst;
    }

    public void setCrerst(String crerst) {
        this.crerst = crerst;
    }

    public String getCrerea() {
        return crerea;
    }

    public void setCrerea(String crerea) {
        this.crerea = crerea;
    }

    public String getAdlmt() {
        return adlmt;
    }

    public void setAdlmt(String adlmt) {
        this.adlmt = adlmt;
    }

    public String getCrerat() {
        return crerat;
    }

    public void setCrerat(String crerat) {
        this.crerat = crerat;
    }

    public String getCrebeg() {
        return crebeg;
    }

    public void setCrebeg(String crebeg) {
        this.crebeg = crebeg;
    }

    public String getCreend() {
        return creend;
    }

    public void setCreend(String creend) {
        this.creend = creend;
    }

    public String getIffist() {
        return iffist;
    }

    public void setIffist(String iffist) {
        this.iffist = iffist;
    }

    @Override
    public String toString() {
        return "Wxp002ReqDto{" +
                "applid='" + applid + '\'' +
                "entnam='" + entnam + '\'' +
                "entpid='" + entpid + '\'' +
                "connum='" + connum + '\'' +
                "crerst='" + crerst + '\'' +
                "crerea='" + crerea + '\'' +
                "adlmt='" + adlmt + '\'' +
                "crerat='" + crerat + '\'' +
                "crebeg='" + crebeg + '\'' +
                "creend='" + creend + '\'' +
                "iffist='" + iffist + '\'' +
                '}';
    }
}  
