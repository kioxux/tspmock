package cn.com.yusys.yusp.dto.client.esb.core.ln3054.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：多还款账户
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkhkzh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "xuhaoooo")
    private Integer xuhaoooo;//序号
    @JsonProperty(value = "youxianj")
    private Integer youxianj;//优先级
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "hkzhhmch")
    private String hkzhhmch;//还款账户名称
    @JsonProperty(value = "hkzhhzhl")
    private String hkzhhzhl;//还款账户种类
    @JsonProperty(value = "hkzhhgze")
    private String hkzhhgze;//还款账户规则
    @JsonProperty(value = "hkjshzhl")
    private String hkjshzhl;//还款基数种类
    @JsonProperty(value = "huankbli")
    private BigDecimal huankbli;//还款比例
    @JsonProperty(value = "shengxrq")
    private String shengxrq;//生效日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public Integer getYouxianj() {
        return youxianj;
    }

    public void setYouxianj(Integer youxianj) {
        this.youxianj = youxianj;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHkzhhmch() {
        return hkzhhmch;
    }

    public void setHkzhhmch(String hkzhhmch) {
        this.hkzhhmch = hkzhhmch;
    }

    public String getHkzhhzhl() {
        return hkzhhzhl;
    }

    public void setHkzhhzhl(String hkzhhzhl) {
        this.hkzhhzhl = hkzhhzhl;
    }

    public String getHkzhhgze() {
        return hkzhhgze;
    }

    public void setHkzhhgze(String hkzhhgze) {
        this.hkzhhgze = hkzhhgze;
    }

    public String getHkjshzhl() {
        return hkjshzhl;
    }

    public void setHkjshzhl(String hkjshzhl) {
        this.hkjshzhl = hkjshzhl;
    }

    public BigDecimal getHuankbli() {
        return huankbli;
    }

    public void setHuankbli(BigDecimal huankbli) {
        this.huankbli = huankbli;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    @Override
    public String toString() {
        return "LstdkhkzhReqDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "xuhaoooo='" + xuhaoooo + '\'' +
                "youxianj='" + youxianj + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "hkzhhmch='" + hkzhhmch + '\'' +
                "hkzhhzhl='" + hkzhhzhl + '\'' +
                "hkzhhgze='" + hkzhhgze + '\'' +
                "hkjshzhl='" + hkjshzhl + '\'' +
                "huankbli='" + huankbli + '\'' +
                "shengxrq='" + shengxrq + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                '}';
    }
}  
