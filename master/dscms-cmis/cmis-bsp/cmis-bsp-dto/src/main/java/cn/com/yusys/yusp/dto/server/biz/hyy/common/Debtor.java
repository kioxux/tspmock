package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//债务人
@JsonPropertyOrder(alphabetic = true)
public class Debtor implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "name")
    private String name;//姓名/名称
    @JsonProperty(value = "document_type")
    private DocumentType document_type;//证件类型
    @JsonProperty(value = "document_number")
    private String document_number;//证件号

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DocumentType getDocument_type() {
        return document_type;
    }

    public void setDocument_type(DocumentType document_type) {
        this.document_type = document_type;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }

    @Override
    public String toString() {
        return "Debtor{" +
                "name='" + name + '\'' +
                ", document_type=" + document_type +
                ", document_number='" + document_number + '\'' +
                '}';
    }
}
