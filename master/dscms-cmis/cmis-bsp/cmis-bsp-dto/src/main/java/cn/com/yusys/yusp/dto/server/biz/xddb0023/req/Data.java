package cn.com.yusys.yusp.dto.server.biz.xddb0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：更新抵质押品状态
 *
 * @author zdl
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ypseno")
    private String ypseno;//核心担保编号
    @JsonProperty(value = "status")
    private String status;//出入库状态

    public String getYpseno() {
        return ypseno;
    }

    public void setYpseno(String ypseno) {
        this.ypseno = ypseno;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Data{" +
                "ypseno='" + ypseno + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
