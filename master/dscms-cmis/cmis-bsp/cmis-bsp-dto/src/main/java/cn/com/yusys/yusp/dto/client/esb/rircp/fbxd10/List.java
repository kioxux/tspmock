package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd10;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询日初（合约）信息历史表（利翃）的合约状态和结清日期
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "status")
    private String status;//合约状态
    @JsonProperty(value = "clear_date")
    private String clear_date;//结清日期

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClear_date() {
        return clear_date;
    }

    public void setClear_date(String clear_date) {
        this.clear_date = clear_date;
    }

    @Override
    public String toString() {
        return "List{" +
                "status='" + status + '\'' +
                ", clear_date='" + clear_date + '\'' +
                '}';
    }
}
