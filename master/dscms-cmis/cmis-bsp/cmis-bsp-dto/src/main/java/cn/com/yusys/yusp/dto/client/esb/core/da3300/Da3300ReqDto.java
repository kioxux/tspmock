package cn.com.yusys.yusp.dto.client.esb.core.da3300;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：交易用于抵债资产信息登记（增、删、改）至核心，核心做相应的表外账务处理
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3300ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkkhczbz")
    private String dkkhczbz;//开户操作标志
    @JsonProperty(value = "caozfshi")
    private String caozfshi;//操作方式
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "ruzjigou")
    private String ruzjigou;//入账机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "dzwzleib")
    private String dzwzleib;//抵债物资类别
    @JsonProperty(value = "cqzmzlei")
    private String cqzmzlei;//产权证明种类
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "dbgdzcje")
    private BigDecimal dbgdzcje;//代保管抵债资产金额
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要
    @JsonProperty(value = "dzzcdanw")
    private String dzzcdanw;//抵债资产单位
    @JsonProperty(value = "dzzcshul")
    private long dzzcshul;//抵债资产数量
    @JsonProperty(value = "pingjiaz")
    private BigDecimal pingjiaz;//评估价值
    @JsonProperty(value = "dzzcrzjz")
    private BigDecimal dzzcrzjz;//抵债资产入账价值
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getCaozfshi() {
        return caozfshi;
    }

    public void setCaozfshi(String caozfshi) {
        this.caozfshi = caozfshi;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getDzwzleib() {
        return dzwzleib;
    }

    public void setDzwzleib(String dzwzleib) {
        this.dzwzleib = dzwzleib;
    }

    public String getCqzmzlei() {
        return cqzmzlei;
    }

    public void setCqzmzlei(String cqzmzlei) {
        this.cqzmzlei = cqzmzlei;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getDbgdzcje() {
        return dbgdzcje;
    }

    public void setDbgdzcje(BigDecimal dbgdzcje) {
        this.dbgdzcje = dbgdzcje;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getDzzcdanw() {
        return dzzcdanw;
    }

    public void setDzzcdanw(String dzzcdanw) {
        this.dzzcdanw = dzzcdanw;
    }

    public long getDzzcshul() {
        return dzzcshul;
    }

    public void setDzzcshul(long dzzcshul) {
        this.dzzcshul = dzzcshul;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getDzzcrzjz() {
        return dzzcrzjz;
    }

    public void setDzzcrzjz(BigDecimal dzzcrzjz) {
        this.dzzcrzjz = dzzcrzjz;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    @Override
    public String toString() {
        return "Da3300ReqDto{" +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "caozfshi='" + caozfshi + '\'' +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "ruzjigou='" + ruzjigou + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "dzwzleib='" + dzwzleib + '\'' +
                "cqzmzlei='" + cqzmzlei + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "dbgdzcje='" + dbgdzcje + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "dzzcdanw='" + dzzcdanw + '\'' +
                "dzzcshul='" + dzzcshul + '\'' +
                "pingjiaz='" + pingjiaz + '\'' +
                "dzzcrzjz='" + dzzcrzjz + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                '}';
    }
}  
