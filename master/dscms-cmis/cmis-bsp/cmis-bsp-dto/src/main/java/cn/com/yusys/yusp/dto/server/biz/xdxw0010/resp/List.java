package cn.com.yusys.yusp.dto.server.biz.xdxw0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：勘验列表信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    private String serno;//业务编号
    @JsonProperty(value = "checkMan")
    private String checkMan;//勘验人（客户）
    @JsonProperty(value = "certNo")
    private String certNo;//勘验人身份证号（客户）
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名
    @JsonProperty(value = "guarantyId")
    private String guarantyId;//抵押物编号
    @JsonProperty(value = "estateName")
    private String estateName;//小区名称
    @JsonProperty(value = "addr")
    private String addr;//地址
    @JsonProperty(value = "building")
    private String building;//楼栋
    @JsonProperty(value = "squ")
    private String squ;//面积
    @JsonProperty(value = "owner")
    private String owner;//所有权人
    @JsonProperty(value = "status")
    private String status;//状态
    @JsonProperty(value = "videoSerno")
    private String videoSerno;//状态视频流水号
    @JsonProperty(value = "surveySerno")
    private String surveySerno;//调查流水号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCheckMan() {
        return checkMan;
    }

    public void setCheckMan(String checkMan) {
        this.checkMan = checkMan;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getGuarantyId() {
        return guarantyId;
    }

    public void setGuarantyId(String guarantyId) {
        this.guarantyId = guarantyId;
    }

    public String getEstateName() {
        return estateName;
    }

    public void setEstateName(String estateName) {
        this.estateName = estateName;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getSqu() {
        return squ;
    }

    public void setSqu(String squ) {
        this.squ = squ;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVideoSerno() {
        return videoSerno;
    }

    public void setVideoSerno(String videoSerno) {
        this.videoSerno = videoSerno;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                ", checkMan='" + checkMan + '\'' +
                ", certNo='" + certNo + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", guarantyId='" + guarantyId + '\'' +
                ", estateName='" + estateName + '\'' +
                ", addr='" + addr + '\'' +
                ", building='" + building + '\'' +
                ", squ='" + squ + '\'' +
                ", owner='" + owner + '\'' +
                ", status='" + status + '\'' +
                ", videoSerno='" + videoSerno + '\'' +
                ", surveySerno='" + surveySerno + '\'' +
                '}';
    }
}
