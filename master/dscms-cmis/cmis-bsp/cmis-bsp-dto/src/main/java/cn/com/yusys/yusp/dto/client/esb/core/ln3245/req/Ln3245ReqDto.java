package cn.com.yusys.yusp.dto.client.esb.core.ln3245.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：核销登记簿查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3245ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    @Override
    public String toString() {
        return "Ln3245ReqDto{" +
                "kehmingc='" + kehmingc + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "zhzhriqi='" + zhzhriqi + '\'' +
                "qishibis='" + qishibis + '\'' +
                "chxunbis='" + chxunbis + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                '}';
    }
}  
