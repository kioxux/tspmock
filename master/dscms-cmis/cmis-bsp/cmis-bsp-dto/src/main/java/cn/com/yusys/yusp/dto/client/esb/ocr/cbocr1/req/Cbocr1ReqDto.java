package cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：新增批次报表数据
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cbocr1ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "flowId")
    private String flowId;//业务流水号
    @JsonProperty(value = "organization")
    private String organization;//编制单位
    @JsonProperty(value = "clientNumber")
    private String clientNumber;//客户号
    @JsonProperty(value = "reportingPeriod")
    private String reportingPeriod;//报表期间
    @JsonProperty(value = "templateClassCode")
    private String templateClassCode;//报表类型字典编码
    @JsonProperty(value = "templateClassName")
    private String templateClassName;//报表类型名称
    @JsonProperty(value = "remark")
    private String remark;//备注
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.List> list;

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getReportingPeriod() {
        return reportingPeriod;
    }

    public void setReportingPeriod(String reportingPeriod) {
        this.reportingPeriod = reportingPeriod;
    }

    public String getTemplateClassCode() {
        return templateClassCode;
    }

    public void setTemplateClassCode(String templateClassCode) {
        this.templateClassCode = templateClassCode;
    }

    public String getTemplateClassName() {
        return templateClassName;
    }

    public void setTemplateClassName(String templateClassName) {
        this.templateClassName = templateClassName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Cbocr1ReqDto{" +
                "flowId='" + flowId + '\'' +
                ", organization='" + organization + '\'' +
                ", clientNumber='" + clientNumber + '\'' +
                ", reportingPeriod='" + reportingPeriod + '\'' +
                ", templateClassCode='" + templateClassCode + '\'' +
                ", templateClassName='" + templateClassName + '\'' +
                ", remark='" + remark + '\'' +
                ", list=" + list +
                '}';
    }
}
