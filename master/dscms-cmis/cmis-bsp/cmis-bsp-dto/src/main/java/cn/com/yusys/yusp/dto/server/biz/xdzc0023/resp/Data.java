package cn.com.yusys.yusp.dto.server.biz.xdzc0023.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/9 9:33
 * @since 2021/6/9 9:33
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFlag")
    private String opFlag;//返回码

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    @Override
    public String toString() {
        return "Data{" +
                "opFlag='" + opFlag + '\'' +
                '}';
    }
}
