package cn.com.yusys.yusp.dto.client.esb.circp.fb1203.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：质押物金额覆盖校验
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1203ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "GUAR_COMT_NO")
    private String GUAR_COMT_NO;//担保合同编号
    @JsonProperty(value = "CONT_NO")
    private String CONT_NO;//合同编号
    @JsonProperty(value = "LOAN_AMT")
    private BigDecimal LOAN_AMT;//放款金额
    @JsonProperty(value = "OLS_TRAN_NO")
    private String OLS_TRAN_NO;//系统交易流水
    @JsonProperty(value = "OLS_DATE")
    private String OLS_DATE;//系统交易日期
    @JsonProperty(value = "PASS_FLAG")
    private String PASS_FLAG;//校验是否通过

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getGUAR_COMT_NO() {
        return GUAR_COMT_NO;
    }

    @JsonIgnore
    public void setGUAR_COMT_NO(String GUAR_COMT_NO) {
        this.GUAR_COMT_NO = GUAR_COMT_NO;
    }

    @JsonIgnore
    public String getCONT_NO() {
        return CONT_NO;
    }

    @JsonIgnore
    public void setCONT_NO(String CONT_NO) {
        this.CONT_NO = CONT_NO;
    }

    @JsonIgnore
    public BigDecimal getLOAN_AMT() {
        return LOAN_AMT;
    }

    @JsonIgnore
    public void setLOAN_AMT(BigDecimal LOAN_AMT) {
        this.LOAN_AMT = LOAN_AMT;
    }

    @JsonIgnore
    public String getOLS_TRAN_NO() {
        return OLS_TRAN_NO;
    }

    @JsonIgnore
    public void setOLS_TRAN_NO(String OLS_TRAN_NO) {
        this.OLS_TRAN_NO = OLS_TRAN_NO;
    }

    @JsonIgnore
    public String getOLS_DATE() {
        return OLS_DATE;
    }

    @JsonIgnore
    public void setOLS_DATE(String OLS_DATE) {
        this.OLS_DATE = OLS_DATE;
    }

    @JsonIgnore
    public String getPASS_FLAG() {
        return PASS_FLAG;
    }

    @JsonIgnore
    public void setPASS_FLAG(String PASS_FLAG) {
        this.PASS_FLAG = PASS_FLAG;
    }

    @Override
    public String toString() {
        return "Fb1203ReqDto{" +
                "CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", GUAR_COMT_NO='" + GUAR_COMT_NO + '\'' +
                ", CONT_NO='" + CONT_NO + '\'' +
                ", LOAN_AMT=" + LOAN_AMT +
                ", OLS_TRAN_NO='" + OLS_TRAN_NO + '\'' +
                ", OLS_DATE='" + OLS_DATE + '\'' +
                ", PASS_FLAG='" + PASS_FLAG + '\'' +
                '}';
    }
}
