package cn.com.yusys.yusp.dto.server.oca.xdxt0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 15:17:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 15:17
 * @since 2021/5/24 15:17
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "num")
    private String num;//总数
    @JsonProperty(value = "managerList")
    private java.util.List<List> list;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "num='" + num + '\'' +
                ", list=" + list +
                '}';
    }
}
