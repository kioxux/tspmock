package cn.com.yusys.yusp.dto.server.biz.xdxw0056.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据流水号取得优企贷信息的客户经理ID和客户经理名
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "serno='" + serno + '\'' +
                '}';
    }
}
