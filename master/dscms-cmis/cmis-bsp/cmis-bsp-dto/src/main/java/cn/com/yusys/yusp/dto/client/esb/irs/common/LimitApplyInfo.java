package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：交易请求信息域:综合授信申请信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日15:10:55
 */
@JsonPropertyOrder(alphabetic = true)
public class  LimitApplyInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno; // 授信申请流水号
    @JsonProperty(value = "lmt_serno")
    private String lmt_serno; // 授信协议编号
    @JsonProperty(value = "flag")
    private String flag; // 新增变更标志
    @JsonProperty(value = "cus_id")
    private String cus_id; // 客户编号
    @JsonProperty(value = "cus_name")
    private String cus_name; // 客户名称
    @JsonProperty(value = "cus_type")
    private String cus_type; // 客户类型
    @JsonProperty(value = "cur_type")
    private String cur_type; // 币种
    @JsonProperty(value = "app_crd_totl_amt")
    private BigDecimal app_crd_totl_amt; // 循环授信敞口额度（元）
    @JsonProperty(value = "app_temp_crd_totl_amt")
    private BigDecimal app_temp_crd_totl_amt; // 临时授信敞口额度（元）
    @JsonProperty(value = "app_crd_guar_amt")
    private BigDecimal app_crd_guar_amt; // 低风险额度（元）
    @JsonProperty(value = "crd_totl_sum_amt")
    private BigDecimal crd_totl_sum_amt; // 授信总额（元）
    @JsonProperty(value = "start_date")
    private String start_date; // 授信申请日
    @JsonProperty(value = "end_date")
    private String end_date; // 授信到期日
    @JsonProperty(value = "delay_months")
    private Integer delay_months; // 宽限月（月）
    @JsonProperty(value = "input_id")
    private String input_id; // 登记人
    @JsonProperty(value = "input_date")
    private String input_date; // 登记日期
    @JsonProperty(value = "input_br_id")
    private String input_br_id; // 登记机构
    @JsonProperty(value = "manager_id")
    private String manager_id; // 责任人
    @JsonProperty(value = "manager_br_id")
    private String manager_br_id; // 责任机构

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getApp_crd_totl_amt() {
        return app_crd_totl_amt;
    }

    public void setApp_crd_totl_amt(BigDecimal app_crd_totl_amt) {
        this.app_crd_totl_amt = app_crd_totl_amt;
    }

    public BigDecimal getApp_temp_crd_totl_amt() {
        return app_temp_crd_totl_amt;
    }

    public void setApp_temp_crd_totl_amt(BigDecimal app_temp_crd_totl_amt) {
        this.app_temp_crd_totl_amt = app_temp_crd_totl_amt;
    }

    public BigDecimal getApp_crd_guar_amt() {
        return app_crd_guar_amt;
    }

    public void setApp_crd_guar_amt(BigDecimal app_crd_guar_amt) {
        this.app_crd_guar_amt = app_crd_guar_amt;
    }

    public BigDecimal getCrd_totl_sum_amt() {
        return crd_totl_sum_amt;
    }

    public void setCrd_totl_sum_amt(BigDecimal crd_totl_sum_amt) {
        this.crd_totl_sum_amt = crd_totl_sum_amt;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Integer getDelay_months() {
        return delay_months;
    }

    public void setDelay_months(Integer delay_months) {
        this.delay_months = delay_months;
    }

    public String getInput_id() {
        return input_id;
    }

    public void setInput_id(String input_id) {
        this.input_id = input_id;
    }

    public String getInput_date() {
        return input_date;
    }

    public void setInput_date(String input_date) {
        this.input_date = input_date;
    }

    public String getInput_br_id() {
        return input_br_id;
    }

    public void setInput_br_id(String input_br_id) {
        this.input_br_id = input_br_id;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    @Override
    public String toString() {
        return "LimitApplyInfo{" +
                "serno='" + serno + '\'' +
                ", lmt_serno='" + lmt_serno + '\'' +
                ", flag='" + flag + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cus_type='" + cus_type + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", app_crd_totl_amt=" + app_crd_totl_amt +
                ", app_temp_crd_totl_amt=" + app_temp_crd_totl_amt +
                ", app_crd_guar_amt=" + app_crd_guar_amt +
                ", crd_totl_sum_amt=" + crd_totl_sum_amt +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", delay_months=" + delay_months +
                ", input_id='" + input_id + '\'' +
                ", input_date='" + input_date + '\'' +
                ", input_br_id='" + input_br_id + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                '}';
    }
}
