package cn.com.yusys.yusp.dto.server.biz.xddb0010.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷押品列表查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0010RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xddb0010RespDto{" +
                "data=" + data +
                '}';
    }
}
