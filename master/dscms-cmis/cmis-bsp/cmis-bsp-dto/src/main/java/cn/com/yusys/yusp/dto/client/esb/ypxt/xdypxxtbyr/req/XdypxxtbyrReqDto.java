package cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：押品信息同步及引入
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdypxxtbyrReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xdypxxtbyrList")
    private java.util.List<XdypxxtbyrList> xdypxxtbyrList;

    public List<XdypxxtbyrList> getXdypxxtbyrList() {
        return xdypxxtbyrList;
    }

    public void setXdypxxtbyrList(List<XdypxxtbyrList> xdypxxtbyrList) {
        this.xdypxxtbyrList = xdypxxtbyrList;
    }

    @Override
    public String toString() {
        return "XdypxxtbyrReqDto{" +
                "xdypxxtbyrList=" + xdypxxtbyrList +
                '}';
    }
}

