package cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @类名 Xwd001RespDto
 * @描述 响应Dto：贷款申请接口
 * @作者 黄勃
 * @时间 2021/10/12 16:48
 **/
@JsonPropertyOrder(alphabetic = true)
public class Xwd001RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "code")
    private String code;//业务状态码

    @JsonProperty(value = "data")
    private String data;//新微贷流水号

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xwd001RespDto{" +
                "code='" + code + '\'' +
                ", data=" + data +
                '}';
    }
}
