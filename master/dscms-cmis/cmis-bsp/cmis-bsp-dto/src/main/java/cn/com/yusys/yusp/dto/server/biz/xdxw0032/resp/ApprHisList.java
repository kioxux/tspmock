package cn.com.yusys.yusp.dto.server.biz.xdxw0032.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询信贷系统的审批历史信息
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ApprHisList  implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "apprNode")
    private String apprNode;//审批节点
    @JsonProperty(value = "apprRes")
    private String apprRes;//审批结果
    @JsonProperty(value = "apprAdvice")
    private String apprAdvice;//审批意见
    @JsonProperty(value = "apprId")
    private String apprId;//审批人工号
    @JsonProperty(value = "apprName")
    private String apprName;//审批人名称
    @JsonProperty(value = "apprDuty")
    private String apprDuty;//审批岗位
    @JsonProperty(value = "apprOrg")
    private String apprOrg;//审批机构
    @JsonProperty(value = "apprTime")
    private String apprTime;//审批时间

    public String getApprNode() {
        return apprNode;
    }

    public void setApprNode(String apprNode) {
        this.apprNode = apprNode;
    }

    public String getApprRes() {
        return apprRes;
    }

    public void setApprRes(String apprRes) {
        this.apprRes = apprRes;
    }

    public String getApprAdvice() {
        return apprAdvice;
    }

    public void setApprAdvice(String apprAdvice) {
        this.apprAdvice = apprAdvice;
    }

    public String getApprId() {
        return apprId;
    }

    public void setApprId(String apprId) {
        this.apprId = apprId;
    }

    public String getApprName() {
        return apprName;
    }

    public void setApprName(String apprName) {
        this.apprName = apprName;
    }

    public String getApprDuty() {
        return apprDuty;
    }

    public void setApprDuty(String apprDuty) {
        this.apprDuty = apprDuty;
    }

    public String getApprOrg() {
        return apprOrg;
    }

    public void setApprOrg(String apprOrg) {
        this.apprOrg = apprOrg;
    }

    public String getApprTime() {
        return apprTime;
    }

    public void setApprTime(String apprTime) {
        this.apprTime = apprTime;
    }

    @Override
    public String toString() {
        return "ApprHisList{" +
                "apprNode='" + apprNode + '\'' +
                ", apprRes='" + apprRes + '\'' +
                ", apprAdvice='" + apprAdvice + '\'' +
                ", apprId='" + apprId + '\'' +
                ", apprName='" + apprName + '\'' +
                ", apprDuty='" + apprDuty + '\'' +
                ", apprOrg='" + apprOrg + '\'' +
                ", apprTime='" + apprTime + '\'' +
                '}';
    }
}
