package cn.com.yusys.yusp.dto.server.biz.xddb0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 15:33
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ListGrt implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "ddbh")
    private String ddbh;//订单编号
    @JsonProperty(value = "ypbh")
    private String ypbh;//押品编号
    @JsonProperty(value = "dbhth")
    private String dbhth;//担保合同号
    @JsonProperty(value = "khth")
    private String khth;//放款合同号
    @JsonProperty(value = "qchgzh")
    private String qchgzh;//汽车合格证号
    @JsonProperty(value = "cjh")
    private String cjh;//车架号

    public String getDdbh() {
        return ddbh;
    }

    public void setDdbh(String ddbh) {
        this.ddbh = ddbh;
    }

    public String getYpbh() {
        return ypbh;
    }

    public void setYpbh(String ypbh) {
        this.ypbh = ypbh;
    }

    public String getDbhth() {
        return dbhth;
    }

    public void setDbhth(String dbhth) {
        this.dbhth = dbhth;
    }

    public String getKhth() {
        return khth;
    }

    public void setKhth(String khth) {
        this.khth = khth;
    }

    public String getQchgzh() {
        return qchgzh;
    }

    public void setQchgzh(String qchgzh) {
        this.qchgzh = qchgzh;
    }

    public String getCjh() {
        return cjh;
    }

    public void setCjh(String cjh) {
        this.cjh = cjh;
    }

    @Override
    public String toString() {
        return "ListGrt{" +
                "ddbh='" + ddbh + '\'' +
                ", ypbh='" + ypbh + '\'' +
                ", dbhth='" + dbhth + '\'' +
                ", khth='" + khth + '\'' +
                ", qchgzh='" + qchgzh + '\'' +
                ", cjh='" + cjh + '\'' +
                '}';
    }
}
