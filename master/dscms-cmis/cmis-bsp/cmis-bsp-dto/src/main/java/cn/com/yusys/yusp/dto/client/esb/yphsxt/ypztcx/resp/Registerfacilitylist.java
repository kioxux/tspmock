package cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 13:55
 * @since 2021/7/9 13:55
 */
@JsonPropertyOrder(alphabetic = true)
public class Registerfacilitylist implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "affaar")
    private String affaar;//登记簿附属设施-建筑面积
    @JsonProperty(value = "afinar")
    private String afinar;//登记簿附属设施-套内建筑面积
    @JsonProperty(value = "afshar")
    private String afshar;//登记簿附属设施-分摊建筑面积
    @JsonProperty(value = "affuse")
    private String affuse;//登记簿附属设施-用途
    @JsonProperty(value = "affloc")
    private String affloc;//登记簿附属设施-坐落
    @JsonProperty(value = "aflrsd")
    private String aflrsd;//登记簿附属设施-土地使用权开始时间
    @JsonProperty(value = "aflred")
    private String aflred;//登记簿附属设施-土地使用权结束时间

    public String getAffaar() {
        return affaar;
    }

    public void setAffaar(String affaar) {
        this.affaar = affaar;
    }

    public String getAfinar() {
        return afinar;
    }

    public void setAfinar(String afinar) {
        this.afinar = afinar;
    }

    public String getAfshar() {
        return afshar;
    }

    public void setAfshar(String afshar) {
        this.afshar = afshar;
    }

    public String getAffuse() {
        return affuse;
    }

    public void setAffuse(String affuse) {
        this.affuse = affuse;
    }

    public String getAffloc() {
        return affloc;
    }

    public void setAffloc(String affloc) {
        this.affloc = affloc;
    }

    public String getAflrsd() {
        return aflrsd;
    }

    public void setAflrsd(String aflrsd) {
        this.aflrsd = aflrsd;
    }

    public String getAflred() {
        return aflred;
    }

    public void setAflred(String aflred) {
        this.aflred = aflred;
    }

    @Override
    public String toString() {
        return "Registerfacilitylist{" +
                "affaar='" + affaar + '\'' +
                ", afinar='" + afinar + '\'' +
                ", afshar='" + afshar + '\'' +
                ", affuse='" + affuse + '\'' +
                ", affloc='" + affloc + '\'' +
                ", aflrsd='" + aflrsd + '\'' +
                ", aflred='" + aflred + '\'' +
                '}';
    }
}
