package cn.com.yusys.yusp.dto.server.biz.xdcz0018.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：家速贷、极速快贷贷款申请
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0018RespDto  extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdcz0015RespDto{" +
                "data='" + data + '\'' +
                '}';
    }
}  
