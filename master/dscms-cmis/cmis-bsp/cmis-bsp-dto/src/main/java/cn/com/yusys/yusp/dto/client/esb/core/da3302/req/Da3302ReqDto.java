package cn.com.yusys.yusp.dto.client.esb.core.da3302.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：抵债资产处置
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3302ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "dbxdzzic")
    private BigDecimal dbxdzzic;//待变现抵债资产
    @JsonProperty(value = "feiyjine")
    private BigDecimal feiyjine;//费用金额
    @JsonProperty(value = "dcldzzic")
    private BigDecimal dcldzzic;//待处理抵债资产
    @JsonProperty(value = "dzzcbxkx")
    private String dzzcbxkx;//抵债资产变现款项账号
    @JsonProperty(value = "dzzcbzxh")
    private String dzzcbzxh;//抵债资产变现账号子序号
    @JsonProperty(value = "fygzzhao")
    private String fygzzhao;//费用挂账账号
    @JsonProperty(value = "fygzzzxh")
    private String fygzzzxh;//费用挂账账号子序号
    @JsonProperty(value = "dzzcztai")
    private String dzzcztai;//抵债资产状态
    @JsonProperty(value = "rzczfshi")
    private String rzczfshi;//入账操作方式
    @JsonProperty(value = "buchjine")
    private BigDecimal buchjine;//补偿金额
    @JsonProperty(value = "buczhhao")
    private String buczhhao;//补偿账号
    @JsonProperty(value = "buczhzxh")
    private String buczhzxh;//补偿账号子序号
    @JsonProperty(value = "dzzcczzl")
    private String dzzcczzl;//抵债资产处置种类
    @JsonProperty(value = "dzzcczwb")
    private String dzzcczwb;//抵债资产是否处置完毕标志

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getFeiyjine() {
        return feiyjine;
    }

    public void setFeiyjine(BigDecimal feiyjine) {
        this.feiyjine = feiyjine;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public String getDzzcbxkx() {
        return dzzcbxkx;
    }

    public void setDzzcbxkx(String dzzcbxkx) {
        this.dzzcbxkx = dzzcbxkx;
    }

    public String getDzzcbzxh() {
        return dzzcbzxh;
    }

    public void setDzzcbzxh(String dzzcbzxh) {
        this.dzzcbzxh = dzzcbzxh;
    }

    public String getFygzzhao() {
        return fygzzhao;
    }

    public void setFygzzhao(String fygzzhao) {
        this.fygzzhao = fygzzhao;
    }

    public String getFygzzzxh() {
        return fygzzzxh;
    }

    public void setFygzzzxh(String fygzzzxh) {
        this.fygzzzxh = fygzzzxh;
    }

    public String getDzzcztai() {
        return dzzcztai;
    }

    public void setDzzcztai(String dzzcztai) {
        this.dzzcztai = dzzcztai;
    }

    public String getRzczfshi() {
        return rzczfshi;
    }

    public void setRzczfshi(String rzczfshi) {
        this.rzczfshi = rzczfshi;
    }

    public BigDecimal getBuchjine() {
        return buchjine;
    }

    public void setBuchjine(BigDecimal buchjine) {
        this.buchjine = buchjine;
    }

    public String getBuczhhao() {
        return buczhhao;
    }

    public void setBuczhhao(String buczhhao) {
        this.buczhhao = buczhhao;
    }

    public String getBuczhzxh() {
        return buczhzxh;
    }

    public void setBuczhzxh(String buczhzxh) {
        this.buczhzxh = buczhzxh;
    }

    public String getDzzcczzl() {
        return dzzcczzl;
    }

    public void setDzzcczzl(String dzzcczzl) {
        this.dzzcczzl = dzzcczzl;
    }

    public String getDzzcczwb() {
        return dzzcczwb;
    }

    public void setDzzcczwb(String dzzcczwb) {
        this.dzzcczwb = dzzcczwb;
    }

    @Override
    public String toString() {
        return "Da3302ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehuzwmc='" + kehuzwmc + '\'' +
                "dbxdzzic='" + dbxdzzic + '\'' +
                "feiyjine='" + feiyjine + '\'' +
                "dcldzzic='" + dcldzzic + '\'' +
                "dzzcbxkx='" + dzzcbxkx + '\'' +
                "dzzcbzxh='" + dzzcbzxh + '\'' +
                "fygzzhao='" + fygzzhao + '\'' +
                "fygzzzxh='" + fygzzzxh + '\'' +
                "dzzcztai='" + dzzcztai + '\'' +
                "rzczfshi='" + rzczfshi + '\'' +
                "buchjine='" + buchjine + '\'' +
                "buczhhao='" + buczhhao + '\'' +
                "buczhzxh='" + buczhzxh + '\'' +
                "dzzcczzl='" + dzzcczzl + '\'' +
                "dzzcczwb='" + dzzcczwb + '\'' +
                '}';
    }
}  
