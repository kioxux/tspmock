package cn.com.yusys.yusp.dto.server.biz.xdls0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 13:51
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "isHavingCrdHis")
    private String isHavingCrdHis;//信贷是否有授信历史

    public String getIsHavingCrdHis() {
        return isHavingCrdHis;
    }

    public void setIsHavingCrdHis(String isHavingCrdHis) {
        this.isHavingCrdHis = isHavingCrdHis;
    }

    @Override
    public String toString() {
        return "Xdls0004RespDto{" +
                "isHavingCrdHis='" + isHavingCrdHis + '\'' +
                '}';
    }
}
