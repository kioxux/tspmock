package cn.com.yusys.yusp.dto.server.biz.xdtz0036.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/21 16:53:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 16:53
 * @since 2021/5/21 16:53
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "maxOverdueDays")
    private String maxOverdueDays;//最大逾期天数
    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getMaxOverdueDays() {
        return maxOverdueDays;
    }

    public void setMaxOverdueDays(String maxOverdueDays) {
        this.maxOverdueDays = maxOverdueDays;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }


    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", maxOverdueDays='" + maxOverdueDays + '\'' +
                ", billNo='" + billNo + '\'' +
                '}';
    }
}
