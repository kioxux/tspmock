package cn.com.yusys.yusp.dto.server.biz.xddh0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 14:06:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 14:06
 * @since 2021/5/22 14:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Xddh0003ReqDto{" +
                "billNo='" + billNo + '\'' +
                "managerId='" + managerId + '\'' +
                '}';
    }
}
