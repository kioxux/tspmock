package cn.com.yusys.yusp.dto.server.cus.xdkh0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：对公客户基本信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certCode")
    private String certCode;//开户证件号码
    @JsonProperty(value = "loanCardId")
    private String loanCardId;//贷款卡编号（中证码）
    @JsonProperty(value = "cmonNo")
    private String cmonNo;//组织机构代码


    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getLoanCardId() {
        return loanCardId;
    }

    public void setLoanCardId(String loanCardId) {
        this.loanCardId = loanCardId;
    }

    public String getCmonNo() {
        return cmonNo;
    }

    public void setCmonNo(String cmonNo) {
        this.cmonNo = cmonNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                ", queryType='" + queryType + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certCode='" + certCode + '\'' +
                ", loanCardId='" + loanCardId + '\'' +
                ", cmonNo='" + cmonNo + '\'' +
                '}';
    }
}
