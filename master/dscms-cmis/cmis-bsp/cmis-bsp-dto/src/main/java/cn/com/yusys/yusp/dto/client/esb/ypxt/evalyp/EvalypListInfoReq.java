package cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：押品我行确认价值同步接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class EvalypListInfoReq implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yptybh")
    private String yptybh;//押品统一编号
    @JsonProperty(value = "whrdjz")
    private BigDecimal whrdjz;//我行认定价值
    @JsonProperty(value = "rdbzyp")
    private String rdbzyp;//认定币种
    @JsonProperty(value = "rdrqyp")
    private String rdrqyp;//认定日期

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public BigDecimal getWhrdjz() {
        return whrdjz;
    }

    public void setWhrdjz(BigDecimal whrdjz) {
        this.whrdjz = whrdjz;
    }

    public String getRdbzyp() {
        return rdbzyp;
    }

    public void setRdbzyp(String rdbzyp) {
        this.rdbzyp = rdbzyp;
    }

    public String getRdrqyp() {
        return rdrqyp;
    }

    public void setRdrqyp(String rdrqyp) {
        this.rdrqyp = rdrqyp;
    }

    @Override
    public String toString() {
        return "EvalypListInfoReqDto{" +
                "yptybh='" + yptybh + '\'' +
                "whrdjz='" + whrdjz + '\'' +
                "rdbzyp='" + rdbzyp + '\'' +
                "rdrqyp='" + rdrqyp + '\'' +
                '}';
    }
}  
