package cn.com.yusys.yusp.dto.client.esb.core.ib1241;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：外围自动冲正(出库撤销)
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ib1241ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yqzhriqi")
    private String yqzhriqi;//原前置日期
    @JsonProperty(value = "yqzhlshu")
    private String yqzhlshu;//原前置流水
    @JsonProperty(value = "yjiaoyrq")
    private String yjiaoyrq;//原交易日期
    @JsonProperty(value = "ygyliush")
    private String ygyliush;//原柜员流水
    @JsonProperty(value = "chanpnma")
    private String chanpnma;//产品码
    @JsonProperty(value = "qianzhrq")
    private String qianzhrq;//前置日期
    @JsonProperty(value = "qianzhls")
    private String qianzhls;//前置流水
    @JsonProperty(value = "brchno")
    private String brchno;// 部门号	否	char(5)	是		brchno

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getYqzhriqi() {
        return yqzhriqi;
    }

    public void setYqzhriqi(String yqzhriqi) {
        this.yqzhriqi = yqzhriqi;
    }

    public String getYqzhlshu() {
        return yqzhlshu;
    }

    public void setYqzhlshu(String yqzhlshu) {
        this.yqzhlshu = yqzhlshu;
    }

    public String getYjiaoyrq() {
        return yjiaoyrq;
    }

    public void setYjiaoyrq(String yjiaoyrq) {
        this.yjiaoyrq = yjiaoyrq;
    }

    public String getYgyliush() {
        return ygyliush;
    }

    public void setYgyliush(String ygyliush) {
        this.ygyliush = ygyliush;
    }

    public String getChanpnma() {
        return chanpnma;
    }

    public void setChanpnma(String chanpnma) {
        this.chanpnma = chanpnma;
    }

    public String getQianzhrq() {
        return qianzhrq;
    }

    public void setQianzhrq(String qianzhrq) {
        this.qianzhrq = qianzhrq;
    }

    public String getQianzhls() {
        return qianzhls;
    }

    public void setQianzhls(String qianzhls) {
        this.qianzhls = qianzhls;
    }


    @Override
    public String toString() {
        return "Ib1241ReqDto{" +
                "yqzhriqi='" + yqzhriqi + '\'' +
                ", yqzhlshu='" + yqzhlshu + '\'' +
                ", yjiaoyrq='" + yjiaoyrq + '\'' +
                ", ygyliush='" + ygyliush + '\'' +
                ", chanpnma='" + chanpnma + '\'' +
                ", qianzhrq='" + qianzhrq + '\'' +
                ", qianzhls='" + qianzhls + '\'' +
                ", brchno='" + brchno + '\'' +
                '}';
    }
}
