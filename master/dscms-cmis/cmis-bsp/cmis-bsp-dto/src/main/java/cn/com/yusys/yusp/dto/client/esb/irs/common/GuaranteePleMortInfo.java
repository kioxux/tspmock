package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：交易请求信息域:担保合同与抵质押、保证人关联信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
@JsonPropertyOrder(alphabetic = true)
public class GuaranteePleMortInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_cont_no")
    private String guar_cont_no; // 担保合同流水号
    @JsonProperty(value = "guaranty_id")
    private String guaranty_id; // 押品编号

    public String getGuar_cont_no() {
        return guar_cont_no;
    }

    public void setGuar_cont_no(String guar_cont_no) {
        this.guar_cont_no = guar_cont_no;
    }

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    @Override
    public String toString() {
        return "GuaranteePleMortInfo{" +
                "guar_cont_no='" + guar_cont_no + '\'' +
                ", guaranty_id='" + guaranty_id + '\'' +
                '}';
    }
}
