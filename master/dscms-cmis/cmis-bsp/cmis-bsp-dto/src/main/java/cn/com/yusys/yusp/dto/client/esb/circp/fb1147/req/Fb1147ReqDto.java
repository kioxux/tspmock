package cn.com.yusys.yusp.dto.client.esb.circp.fb1147.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：无还本续贷申请
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1147ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "APP_TYPE")
    private String APP_TYPE;//申请类型
    @JsonProperty(value = "XD_TYPE")
    private String XD_TYPE;//续贷主体类型
    @JsonProperty(value = "CRD_CONT_NO")
    private String CRD_CONT_NO;//授信合同编号
    @JsonProperty(value = "BILL_NO")
    private String BILL_NO;//借据编号
    @JsonProperty(value = "ENT_NAME")
    private String ENT_NAME;//企业名称
    @JsonProperty(value = "ENT_CUST_ID")
    private String ENT_CUST_ID;//企业核心客户号
    @JsonProperty(value = "ORG_CODE")
    private String ORG_CODE;//企业统一社会信用代码
    @JsonProperty(value = "ENT_CUST_NAME")
    private String ENT_CUST_NAME;//企业法人
    @JsonProperty(value = "ENT_CUST_NO")
    private String ENT_CUST_NO;//企业法人身份证
    @JsonProperty(value = "ENT_CUST_NAME_ID")
    private String ENT_CUST_NAME_ID;//企业法人客户号
    @JsonProperty(value = "CUST_NAME")
    private String CUST_NAME;//个人客户名称
    @JsonProperty(value = "CUST_NO")
    private String CUST_NO;//个人身份证号码
    @JsonProperty(value = "CUST_ID")
    private String CUST_ID;//个人核心客户号
    @JsonProperty(value = "GRANT_NO")
    private String GRANT_NO;//个人征信授权书编号
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "MANAGER_ID")
    private String MANAGER_ID;//管户经理ID
    @JsonProperty(value = "MANAGER_NAME")
    private String MANAGER_NAME;//管户经理名称
    @JsonProperty(value = "MANAGER_ORG_ID")
    private String MANAGER_ORG_ID;//管户经理所属机构ID
    @JsonProperty(value = "MANAGER_ORG_NAME")
    private String MANAGER_ORG_NAME;//管户经理所属机构名称
    @JsonProperty(value = "APP_AMT")
    private String APP_AMT;//续贷金额
    @JsonProperty(value = "LOAN_CARD_NO")
    private String LOAN_CARD_NO;//放款卡号
    @JsonProperty(value = "LOAN_START_DATE")
    private String LOAN_START_DATE;//新借据起始日
    @JsonProperty(value = "LOAN_TERM")
    private String LOAN_TERM;//新借据贷款期限
    @JsonProperty(value = "LOAN_END_DATE")
    private String LOAN_END_DATE;//新借据到日期
    @JsonProperty(value = "OLS_TRAN_NO")
    private String OLS_TRAN_NO;//系统交易流水
    @JsonProperty(value = "OLS_DATE")
    private String OLS_DATE;//系统交易日期

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getAPP_TYPE() {
        return APP_TYPE;
    }

    @JsonIgnore
    public void setAPP_TYPE(String APP_TYPE) {
        this.APP_TYPE = APP_TYPE;
    }

    @JsonIgnore
    public String getXD_TYPE() {
        return XD_TYPE;
    }

    @JsonIgnore
    public void setXD_TYPE(String XD_TYPE) {
        this.XD_TYPE = XD_TYPE;
    }

    @JsonIgnore
    public String getCRD_CONT_NO() {
        return CRD_CONT_NO;
    }

    @JsonIgnore
    public void setCRD_CONT_NO(String CRD_CONT_NO) {
        this.CRD_CONT_NO = CRD_CONT_NO;
    }

    @JsonIgnore
    public String getBILL_NO() {
        return BILL_NO;
    }

    @JsonIgnore
    public void setBILL_NO(String BILL_NO) {
        this.BILL_NO = BILL_NO;
    }

    @JsonIgnore
    public String getENT_NAME() {
        return ENT_NAME;
    }

    @JsonIgnore
    public void setENT_NAME(String ENT_NAME) {
        this.ENT_NAME = ENT_NAME;
    }

    @JsonIgnore
    public String getENT_CUST_ID() {
        return ENT_CUST_ID;
    }

    @JsonIgnore
    public void setENT_CUST_ID(String ENT_CUST_ID) {
        this.ENT_CUST_ID = ENT_CUST_ID;
    }

    @JsonIgnore
    public String getORG_CODE() {
        return ORG_CODE;
    }

    @JsonIgnore
    public void setORG_CODE(String ORG_CODE) {
        this.ORG_CODE = ORG_CODE;
    }

    @JsonIgnore
    public String getENT_CUST_NAME() {
        return ENT_CUST_NAME;
    }

    @JsonIgnore
    public void setENT_CUST_NAME(String ENT_CUST_NAME) {
        this.ENT_CUST_NAME = ENT_CUST_NAME;
    }

    @JsonIgnore
    public String getENT_CUST_NO() {
        return ENT_CUST_NO;
    }

    @JsonIgnore
    public void setENT_CUST_NO(String ENT_CUST_NO) {
        this.ENT_CUST_NO = ENT_CUST_NO;
    }

    @JsonIgnore
    public String getENT_CUST_NAME_ID() {
        return ENT_CUST_NAME_ID;
    }

    @JsonIgnore
    public void setENT_CUST_NAME_ID(String ENT_CUST_NAME_ID) {
        this.ENT_CUST_NAME_ID = ENT_CUST_NAME_ID;
    }

    @JsonIgnore
    public String getCUST_NAME() {
        return CUST_NAME;
    }

    @JsonIgnore
    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    @JsonIgnore
    public String getCUST_NO() {
        return CUST_NO;
    }

    @JsonIgnore
    public void setCUST_NO(String CUST_NO) {
        this.CUST_NO = CUST_NO;
    }

    @JsonIgnore
    public String getCUST_ID() {
        return CUST_ID;
    }

    @JsonIgnore
    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    @JsonIgnore
    public String getGRANT_NO() {
        return GRANT_NO;
    }

    @JsonIgnore
    public void setGRANT_NO(String GRANT_NO) {
        this.GRANT_NO = GRANT_NO;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getMANAGER_ID() {
        return MANAGER_ID;
    }

    @JsonIgnore
    public void setMANAGER_ID(String MANAGER_ID) {
        this.MANAGER_ID = MANAGER_ID;
    }

    @JsonIgnore
    public String getMANAGER_NAME() {
        return MANAGER_NAME;
    }

    @JsonIgnore
    public void setMANAGER_NAME(String MANAGER_NAME) {
        this.MANAGER_NAME = MANAGER_NAME;
    }

    @JsonIgnore
    public String getMANAGER_ORG_ID() {
        return MANAGER_ORG_ID;
    }

    @JsonIgnore
    public void setMANAGER_ORG_ID(String MANAGER_ORG_ID) {
        this.MANAGER_ORG_ID = MANAGER_ORG_ID;
    }

    @JsonIgnore
    public String getMANAGER_ORG_NAME() {
        return MANAGER_ORG_NAME;
    }

    @JsonIgnore
    public void setMANAGER_ORG_NAME(String MANAGER_ORG_NAME) {
        this.MANAGER_ORG_NAME = MANAGER_ORG_NAME;
    }

    @JsonIgnore
    public String getAPP_AMT() {
        return APP_AMT;
    }

    @JsonIgnore
    public void setAPP_AMT(String APP_AMT) {
        this.APP_AMT = APP_AMT;
    }

    @JsonIgnore
    public String getLOAN_CARD_NO() {
        return LOAN_CARD_NO;
    }

    @JsonIgnore
    public void setLOAN_CARD_NO(String LOAN_CARD_NO) {
        this.LOAN_CARD_NO = LOAN_CARD_NO;
    }

    @JsonIgnore
    public String getLOAN_START_DATE() {
        return LOAN_START_DATE;
    }

    @JsonIgnore
    public void setLOAN_START_DATE(String LOAN_START_DATE) {
        this.LOAN_START_DATE = LOAN_START_DATE;
    }

    @JsonIgnore
    public String getLOAN_TERM() {
        return LOAN_TERM;
    }

    @JsonIgnore
    public void setLOAN_TERM(String LOAN_TERM) {
        this.LOAN_TERM = LOAN_TERM;
    }

    @JsonIgnore
    public String getLOAN_END_DATE() {
        return LOAN_END_DATE;
    }

    @JsonIgnore
    public void setLOAN_END_DATE(String LOAN_END_DATE) {
        this.LOAN_END_DATE = LOAN_END_DATE;
    }

    @JsonIgnore
    public String getOLS_TRAN_NO() {
        return OLS_TRAN_NO;
    }

    @JsonIgnore
    public void setOLS_TRAN_NO(String OLS_TRAN_NO) {
        this.OLS_TRAN_NO = OLS_TRAN_NO;
    }

    @JsonIgnore
    public String getOLS_DATE() {
        return OLS_DATE;
    }

    @JsonIgnore
    public void setOLS_DATE(String OLS_DATE) {
        this.OLS_DATE = OLS_DATE;
    }

    @Override
    public String toString() {
        return "Fb1147ReqDto{" +
                "CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", APP_TYPE='" + APP_TYPE + '\'' +
                ", XD_TYPE='" + XD_TYPE + '\'' +
                ", CRD_CONT_NO='" + CRD_CONT_NO + '\'' +
                ", BILL_NO='" + BILL_NO + '\'' +
                ", ENT_NAME='" + ENT_NAME + '\'' +
                ", ENT_CUST_ID='" + ENT_CUST_ID + '\'' +
                ", ORG_CODE='" + ORG_CODE + '\'' +
                ", ENT_CUST_NAME='" + ENT_CUST_NAME + '\'' +
                ", ENT_CUST_NO='" + ENT_CUST_NO + '\'' +
                ", ENT_CUST_NAME_ID='" + ENT_CUST_NAME_ID + '\'' +
                ", CUST_NAME='" + CUST_NAME + '\'' +
                ", CUST_NO='" + CUST_NO + '\'' +
                ", CUST_ID='" + CUST_ID + '\'' +
                ", GRANT_NO='" + GRANT_NO + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", MANAGER_ID='" + MANAGER_ID + '\'' +
                ", MANAGER_NAME='" + MANAGER_NAME + '\'' +
                ", MANAGER_ORG_ID='" + MANAGER_ORG_ID + '\'' +
                ", MANAGER_ORG_NAME='" + MANAGER_ORG_NAME + '\'' +
                ", APP_AMT='" + APP_AMT + '\'' +
                ", LOAN_CARD_NO='" + LOAN_CARD_NO + '\'' +
                ", LOAN_START_DATE='" + LOAN_START_DATE + '\'' +
                ", LOAN_TERM='" + LOAN_TERM + '\'' +
                ", LOAN_END_DATE='" + LOAN_END_DATE + '\'' +
                ", OLS_TRAN_NO='" + OLS_TRAN_NO + '\'' +
                ", OLS_DATE='" + OLS_DATE + '\'' +
                '}';
    }
}
