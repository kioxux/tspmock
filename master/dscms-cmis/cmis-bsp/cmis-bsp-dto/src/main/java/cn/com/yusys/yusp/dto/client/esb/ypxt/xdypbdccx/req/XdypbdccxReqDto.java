package cn.com.yusys.yusp.dto.client.esb.ypxt.xdypbdccx.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询不动产信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdypbdccxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guaranty_id")
    private String guaranty_id;//押品编号
    @JsonProperty(value = "type")
    private String type;//类型

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "XdypbdccxReqDto{" +
                "guaranty_id='" + guaranty_id + '\'' +
                "type='" + type + '\'' +
                '}';
    }
}

