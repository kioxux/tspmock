package cn.com.yusys.yusp.dto.client.esb.irs.xirs11.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：同业客户信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xirs11RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Xirs11RespDto{" +
                '}';
    }
}  
