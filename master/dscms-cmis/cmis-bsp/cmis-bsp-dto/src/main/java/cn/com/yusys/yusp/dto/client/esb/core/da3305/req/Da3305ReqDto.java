package cn.com.yusys.yusp.dto.client.esb.core.da3305.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：待变现抵债资产销账
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3305ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "dbxdzzic")
    private BigDecimal dbxdzzic;//待变现抵债资产
    @JsonProperty(value = "fygzzhao")
    private String fygzzhao;//费用挂账账号
    @JsonProperty(value = "feiyfase")
    private BigDecimal feiyfase;//费用发生额
    @JsonProperty(value = "bnxyijje")
    private BigDecimal bnxyijje;//变现溢价金额
    @JsonProperty(value = "fygzzzxh")
    private String fygzzzxh;//费用挂账账号子序号

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public String getFygzzhao() {
        return fygzzhao;
    }

    public void setFygzzhao(String fygzzhao) {
        this.fygzzhao = fygzzhao;
    }

    public BigDecimal getFeiyfase() {
        return feiyfase;
    }

    public void setFeiyfase(BigDecimal feiyfase) {
        this.feiyfase = feiyfase;
    }

    public BigDecimal getBnxyijje() {
        return bnxyijje;
    }

    public void setBnxyijje(BigDecimal bnxyijje) {
        this.bnxyijje = bnxyijje;
    }

    public String getFygzzzxh() {
        return fygzzzxh;
    }

    public void setFygzzzxh(String fygzzzxh) {
        this.fygzzzxh = fygzzzxh;
    }

    @Override
    public String toString() {
        return "Da3305ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "dbxdzzic='" + dbxdzzic + '\'' +
                "fygzzhao='" + fygzzhao + '\'' +
                "feiyfase='" + feiyfase + '\'' +
                "bnxyijje='" + bnxyijje + '\'' +
                "fygzzzxh='" + fygzzzxh + '\'' +
                '}';
    }
}  
