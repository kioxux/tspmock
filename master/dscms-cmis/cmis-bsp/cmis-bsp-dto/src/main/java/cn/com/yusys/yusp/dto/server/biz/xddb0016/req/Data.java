package cn.com.yusys.yusp.dto.server.biz.xddb0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarna")
    private String guarna;//抵押人名称
    @JsonProperty(value = "yppage")
    private String yppage;//页码
    @JsonProperty(value = "ypsize")
    private String ypsize;//每页条数

    public String getGuarna() {
        return guarna;
    }

    public void setGuarna(String guarna) {
        this.guarna = guarna;
    }

    public String getyppage() {
        return yppage;
    }

    public void setYppage(String yppage) {
        this.yppage = yppage;
    }

    public String getYpsize() {
        return ypsize;
    }

    public void setYpsize(String ypsize) {
        this.ypsize = ypsize;
    }

    @Override
    public String toString() {
        return "Data{" +
                "guarna='" + guarna + '\'' +
                "yppage='" + yppage + '\'' +
                "ypsize='" + ypsize + '\'' +
                '}';
    }
}
