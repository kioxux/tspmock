package cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：连云港存量房列表
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CljctzReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ipaddr")
    private String ipaddr;//ip地址
    @JsonProperty(value = "mac")
    private String mac;//mac地址
    @JsonProperty(value = "savvou")
    private String savvou;//监管协议号（缴款凭证编号）
    @JsonProperty(value = "acctno")
    private String acctno;//监管账号
    @JsonProperty(value = "acctna")
    private String acctna;//监管账号名称
    @JsonProperty(value = "tranam")
    private BigDecimal tranam;//本次缴款金额
    @JsonProperty(value = "trantp")
    private String trantp;//缴款类型
    @JsonProperty(value = "savsrc")
    private String savsrc;//缴存资金来源
    @JsonProperty(value = "svbkna")
    private String svbkna;//缴款银行名称
    @JsonProperty(value = "svbkno")
    private String svbkno;//缴款银行行号
    @JsonProperty(value = "sernum")
    private String sernum;//流水号
    @JsonProperty(value = "savtim")
    private String savtim;//缴款时间
    @JsonProperty(value = "savdec")
    private String savdec;//缴款说明
    @JsonProperty(value = "savimg")
    private String savimg;//缴款凭证图片
    @JsonProperty(value = "pyerac")
    private String pyerac;//付款账号
    @JsonProperty(value = "pyerna")
    private String pyerna;//付款账号名称
    @JsonProperty(value = "byidna")
    private String byidna;//买方名称
    @JsonProperty(value = "byidno")
    private String byidno;//买方证件号
    @JsonProperty(value = "contno")
    private String contno;//合同编号

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSavvou() {
        return savvou;
    }

    public void setSavvou(String savvou) {
        this.savvou = savvou;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctna() {
        return acctna;
    }

    public void setAcctna(String acctna) {
        this.acctna = acctna;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getTrantp() {
        return trantp;
    }

    public void setTrantp(String trantp) {
        this.trantp = trantp;
    }

    public String getSavsrc() {
        return savsrc;
    }

    public void setSavsrc(String savsrc) {
        this.savsrc = savsrc;
    }

    public String getSvbkna() {
        return svbkna;
    }

    public void setSvbkna(String svbkna) {
        this.svbkna = svbkna;
    }

    public String getSvbkno() {
        return svbkno;
    }

    public void setSvbkno(String svbkno) {
        this.svbkno = svbkno;
    }

    public String getSernum() {
        return sernum;
    }

    public void setSernum(String sernum) {
        this.sernum = sernum;
    }

    public String getSavtim() {
        return savtim;
    }

    public void setSavtim(String savtim) {
        this.savtim = savtim;
    }

    public String getSavdec() {
        return savdec;
    }

    public void setSavdec(String savdec) {
        this.savdec = savdec;
    }

    public String getSavimg() {
        return savimg;
    }

    public void setSavimg(String savimg) {
        this.savimg = savimg;
    }

    public String getPyerac() {
        return pyerac;
    }

    public void setPyerac(String pyerac) {
        this.pyerac = pyerac;
    }

    public String getPyerna() {
        return pyerna;
    }

    public void setPyerna(String pyerna) {
        this.pyerna = pyerna;
    }

    public String getByidna() {
        return byidna;
    }

    public void setByidna(String byidna) {
        this.byidna = byidna;
    }

    public String getByidno() {
        return byidno;
    }

    public void setByidno(String byidno) {
        this.byidno = byidno;
    }

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    @Override
    public String toString() {
        return "CljctzReqDto{" +
                "ipaddr='" + ipaddr + '\'' +
                "mac='" + mac + '\'' +
                "savvou='" + savvou + '\'' +
                "acctno='" + acctno + '\'' +
                "acctna='" + acctna + '\'' +
                "tranam='" + tranam + '\'' +
                "trantp='" + trantp + '\'' +
                "savsrc='" + savsrc + '\'' +
                "svbkna='" + svbkna + '\'' +
                "svbkno='" + svbkno + '\'' +
                "sernum='" + sernum + '\'' +
                "savtim='" + savtim + '\'' +
                "savdec='" + savdec + '\'' +
                "savimg='" + savimg + '\'' +
                "pyerac='" + pyerac + '\'' +
                "pyerna='" + pyerna + '\'' +
                "byidna='" + byidna + '\'' +
                "byidno='" + byidno + '\'' +
                "contno='" + contno + '\'' +
                '}';
    }
}  
