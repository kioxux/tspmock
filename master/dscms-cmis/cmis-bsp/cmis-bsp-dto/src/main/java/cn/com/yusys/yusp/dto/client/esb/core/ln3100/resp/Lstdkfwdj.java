package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：贷款服务登记
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkfwdj implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "fuwuzidu")
    private String fuwuzidu;//服务字段
    @JsonProperty(value = "fuwudaim")
    private String fuwudaim;//服务代码
    @JsonProperty(value = "fuwuzdmc")
    private String fuwuzdmc;//服务字段名称
    @JsonProperty(value = "fuwudmmc")
    private String fuwudmmc;//服务代码名称

    public String getFuwuzidu() {
        return fuwuzidu;
    }

    public void setFuwuzidu(String fuwuzidu) {
        this.fuwuzidu = fuwuzidu;
    }

    public String getFuwudaim() {
        return fuwudaim;
    }

    public void setFuwudaim(String fuwudaim) {
        this.fuwudaim = fuwudaim;
    }

    public String getFuwuzdmc() {
        return fuwuzdmc;
    }

    public void setFuwuzdmc(String fuwuzdmc) {
        this.fuwuzdmc = fuwuzdmc;
    }

    public String getFuwudmmc() {
        return fuwudmmc;
    }

    public void setFuwudmmc(String fuwudmmc) {
        this.fuwudmmc = fuwudmmc;
    }

    @Override
    public String toString() {
        return "LstdkfwdjRespDto{" +
                "fuwuzidu='" + fuwuzidu + '\'' +
                "fuwudaim='" + fuwudaim + '\'' +
                "fuwuzdmc='" + fuwuzdmc + '\'' +
                "fuwudmmc='" + fuwudmmc + '\'' +
                '}';
    }
}  
