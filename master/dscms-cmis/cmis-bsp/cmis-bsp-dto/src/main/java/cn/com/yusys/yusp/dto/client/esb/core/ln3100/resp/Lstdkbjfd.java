package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：本金分段登记
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkbjfd implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bjdhkfsh")
    private String bjdhkfsh;//本阶段还款方式
    @JsonProperty(value = "bjdhkqsh")
    private Integer bjdhkqsh;//本阶段还款期数
    @JsonProperty(value = "bjdhbjee")
    private BigDecimal bjdhbjee;//本阶段还本金额
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "leijinzh")
    private BigDecimal leijinzh;//累进值
    @JsonProperty(value = "leijqjsh")
    private Integer leijqjsh;//累进区间期数
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "jixiguiz")
    private String jixiguiz;//计息规则

    public String getBjdhkfsh() {
        return bjdhkfsh;
    }

    public void setBjdhkfsh(String bjdhkfsh) {
        this.bjdhkfsh = bjdhkfsh;
    }

    public Integer getBjdhkqsh() {
        return bjdhkqsh;
    }

    public void setBjdhkqsh(Integer bjdhkqsh) {
        this.bjdhkqsh = bjdhkqsh;
    }

    public BigDecimal getBjdhbjee() {
        return bjdhbjee;
    }

    public void setBjdhbjee(BigDecimal bjdhbjee) {
        this.bjdhbjee = bjdhbjee;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    @Override
    public String toString() {
        return "LstdkbjfdRespDto{" +
                "bjdhkfsh='" + bjdhkfsh + '\'' +
                "bjdhkqsh='" + bjdhkqsh + '\'' +
                "bjdhbjee='" + bjdhbjee + '\'' +
                "dechligz='" + dechligz + '\'' +
                "leijinzh='" + leijinzh + '\'' +
                "leijqjsh='" + leijqjsh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "jixiguiz='" + jixiguiz + '\'' +
                '}';
    }
}  
