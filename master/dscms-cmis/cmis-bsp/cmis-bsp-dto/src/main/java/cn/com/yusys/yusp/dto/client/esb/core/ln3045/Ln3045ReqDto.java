package cn.com.yusys.yusp.dto.client.esb.core.ln3045;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：贷款核销处理
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3045ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "hexiaobj")
    private BigDecimal hexiaobj;//核销本金
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "hexiaozh")
    private String hexiaozh;//核销来源账号
    @JsonProperty(value = "hexiaozx")
    private String hexiaozx;//核销来源账号子序号
    @JsonProperty(value = "daikghfs")
    private String daikghfs;//贷款归还方式
    @JsonProperty(value = "hxzdkkbz")
    private String hxzdkkbz;//核销自动扣款标志
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//核销备注
    @JsonProperty(value = "jixibzhi")
    private String jixibzhi;//计息标志

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public String getHexiaozh() {
        return hexiaozh;
    }

    public void setHexiaozh(String hexiaozh) {
        this.hexiaozh = hexiaozh;
    }

    public String getHexiaozx() {
        return hexiaozx;
    }

    public void setHexiaozx(String hexiaozx) {
        this.hexiaozx = hexiaozx;
    }

    public String getDaikghfs() {
        return daikghfs;
    }

    public void setDaikghfs(String daikghfs) {
        this.daikghfs = daikghfs;
    }

    public String getHxzdkkbz() {
        return hxzdkkbz;
    }

    public void setHxzdkkbz(String hxzdkkbz) {
        this.hxzdkkbz = hxzdkkbz;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getJixibzhi() {
        return jixibzhi;
    }

    public void setJixibzhi(String jixibzhi) {
        this.jixibzhi = jixibzhi;
    }

    @Override
    public String toString() {
        return "Ln3045ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "hexiaobj='" + hexiaobj + '\'' +
                "hexiaolx='" + hexiaolx + '\'' +
                "hexiaozh='" + hexiaozh + '\'' +
                "hexiaozx='" + hexiaozx + '\'' +
                "daikghfs='" + daikghfs + '\'' +
                "hxzdkkbz='" + hxzdkkbz + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "jixibzhi='" + jixibzhi + '\'' +
                '}';
    }
}  
