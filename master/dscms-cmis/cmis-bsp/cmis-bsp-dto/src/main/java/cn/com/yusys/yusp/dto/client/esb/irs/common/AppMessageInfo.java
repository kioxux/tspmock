package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应DTO：响应信息域元素:授信层债项评级结果
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
@JsonPropertyOrder(alphabetic = true)
public class AppMessageInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno; // 授信申请流水号
    @JsonProperty(value = "guaranteeGrade")
    private String guaranteeGrade; // 债项等级
    @JsonProperty(value = "ead")
    private BigDecimal ead; // 违约风险暴露
    @JsonProperty(value = "lgd")
    private BigDecimal lgd; // 违约损失率

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getGuaranteeGrade() {
        return guaranteeGrade;
    }

    public void setGuaranteeGrade(String guaranteeGrade) {
        this.guaranteeGrade = guaranteeGrade;
    }

    public BigDecimal getEad() {
        return ead;
    }

    public void setEad(BigDecimal ead) {
        this.ead = ead;
    }

    public BigDecimal getLgd() {
        return lgd;
    }

    public void setLgd(BigDecimal lgd) {
        this.lgd = lgd;
    }

    @Override
    public String toString() {
        return "AppMessageInfo{" +
                "serno='" + serno + '\'' +
                ", guaranteeGrade='" + guaranteeGrade + '\'' +
                ", ead=" + ead +
                ", lgd=" + lgd +
                '}';
    }
}
