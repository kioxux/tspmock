package cn.com.yusys.yusp.dto.client.esb.wx.wxp001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：信贷将审批结果推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxp001ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applid")
    private String applid;//对接记录ID
    @JsonProperty(value = "entnam")
    private String entnam;//企业名称
    @JsonProperty(value = "entpid")
    private String entpid;//企业代码
    @JsonProperty(value = "admres")
    private String admres;//是否准入
    @JsonProperty(value = "reason")
    private String reason;//原因
    @JsonProperty(value = "preamt")
    private BigDecimal preamt;//预授信额度

    public String getApplid() {
        return applid;
    }

    public void setApplid(String applid) {
        this.applid = applid;
    }

    public String getEntnam() {
        return entnam;
    }

    public void setEntnam(String entnam) {
        this.entnam = entnam;
    }

    public String getEntpid() {
        return entpid;
    }

    public void setEntpid(String entpid) {
        this.entpid = entpid;
    }

    public String getAdmres() {
        return admres;
    }

    public void setAdmres(String admres) {
        this.admres = admres;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public BigDecimal getPreamt() {
        return preamt;
    }

    public void setPreamt(BigDecimal preamt) {
        this.preamt = preamt;
    }

    @Override
    public String toString() {
        return "Wxp001ReqDto{" +
                "applid='" + applid + '\'' +
                "entnam='" + entnam + '\'' +
                "entpid='" + entpid + '\'' +
                "admres='" + admres + '\'' +
                "reason='" + reason + '\'' +
                "preamt='" + preamt + '\'' +
                '}';
    }
}  
