package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd02;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：为正式客户更新信贷客户信息手机号码
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd02RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ols_tran_no")
    private String ols_tran_no; // 交易流水号
    @JsonProperty(value = "ols_date")
    private String ols_date; // 交易日期
    @JsonProperty(value = "ifmobl")
    private String ifmobl;//手机号

    public String getOls_tran_no() {
        return ols_tran_no;
    }

    public void setOls_tran_no(String ols_tran_no) {
        this.ols_tran_no = ols_tran_no;
    }

    public String getOls_date() {
        return ols_date;
    }

    public void setOls_date(String ols_date) {
        this.ols_date = ols_date;
    }

    public String getIfmobl() {
        return ifmobl;
    }

    public void setIfmobl(String ifmobl) {
        this.ifmobl = ifmobl;
    }

    @Override
    public String toString() {
        return "Fbxd02RespDto{" +
                "ols_tran_no='" + ols_tran_no + '\'' +
                ", ols_date='" + ols_date + '\'' +
                ", ifmobl='" + ifmobl + '\'' +
                '}';
    }
}
