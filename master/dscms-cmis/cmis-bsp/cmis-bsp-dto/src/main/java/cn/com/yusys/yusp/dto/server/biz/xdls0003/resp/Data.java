package cn.com.yusys.yusp.dto.server.biz.xdls0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 10:54
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "crdLmtTotal")
    private String crdLmtTotal;//授信协议合同金额合计

    public String getCrdLmtTotal() {
        return crdLmtTotal;
    }

    public void setCrdLmtTotal(String crdLmtTotal) {
        this.crdLmtTotal = crdLmtTotal;
    }

    @Override
    public String toString() {
        return "Xdls0003RespDto{" +
                "crdLmtTotal='" + crdLmtTotal + '\'' +
                '}';
    }
}
