package cn.com.yusys.yusp.dto.client.esb.core.ib1243;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：通用电子记帐交易（多借多贷-多币种）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ib1243RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "guiylius")
    private String guiylius;//柜员流水号
    @JsonProperty(value = "lstAcctOut")
    private List<LstAcctOut> lstAcctOut;//交易账户信息输出


    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getGuiylius() {
        return guiylius;
    }

    public void setGuiylius(String guiylius) {
        this.guiylius = guiylius;
    }

    public List<LstAcctOut> getLstAcctOut() {
        return lstAcctOut;
    }

    public void setLstAcctOut(List<LstAcctOut> lstAcctOut) {
        this.lstAcctOut = lstAcctOut;
    }

    @Override
    public String toString() {
        return "Ib1243RespDto{" +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "guiylius='" + guiylius + '\'' +
                "lstAcctOut='" + lstAcctOut + '\'' +
                '}';
    }
}  
