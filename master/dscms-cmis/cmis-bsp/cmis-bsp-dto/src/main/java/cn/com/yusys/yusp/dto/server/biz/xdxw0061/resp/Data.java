package cn.com.yusys.yusp.dto.server.biz.xdxw0061.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：通过无还本续贷调查表编号查询配偶核心客户号
 * @Author zhangpeng
 * @Date 2021/4/25 10:29
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusNo")
    private String cusNo;//客户代码

    public String getCusNo() {
        return cusNo;
    }

    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusNo='" + cusNo + '\'' +
                '}';
    }
}
