package cn.com.yusys.yusp.dto.client.esb.core.ln3176;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：本交易用户贷款多个还款账户进行还款
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3176RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "guihzcbj")
    private BigDecimal guihzcbj;//归还正常本金
    @JsonProperty(value = "guihyqbj")
    private BigDecimal guihyqbj;//归还逾期本金
    @JsonProperty(value = "guihdzbj")
    private BigDecimal guihdzbj;//归还呆滞本金
    @JsonProperty(value = "ghdzhabj")
    private BigDecimal ghdzhabj;//归还呆账本金
    @JsonProperty(value = "ghysyjlx")
    private BigDecimal ghysyjlx;//归还应收应计利息
    @JsonProperty(value = "ghcsyjlx")
    private BigDecimal ghcsyjlx;//归还催收应计利息
    @JsonProperty(value = "ghynshqx")
    private BigDecimal ghynshqx;//归还应收欠息
    @JsonProperty(value = "ghcushqx")
    private BigDecimal ghcushqx;//归还催收欠息
    @JsonProperty(value = "ghysyjfx")
    private BigDecimal ghysyjfx;//归还应收应计罚息
    @JsonProperty(value = "ghcsyjfx")
    private BigDecimal ghcsyjfx;//归还催收应计罚息
    @JsonProperty(value = "ghynshfx")
    private BigDecimal ghynshfx;//归还应收罚息
    @JsonProperty(value = "ghcushfx")
    private BigDecimal ghcushfx;//归还催收罚息
    @JsonProperty(value = "ghyjfuxi")
    private BigDecimal ghyjfuxi;//归还应计复息
    @JsonProperty(value = "ghfxfuxi")
    private BigDecimal ghfxfuxi;//归还复息
    @JsonProperty(value = "ghfeiyin")
    private BigDecimal ghfeiyin;//归还费用
    @JsonProperty(value = "ghfajinn")
    private BigDecimal ghfajinn;//归还罚金
    @JsonProperty(value = "guihzone")
    private BigDecimal guihzone;//归还总额
    @JsonProperty(value = "benjheji")
    private BigDecimal benjheji;//本金合计
    @JsonProperty(value = "xcsdxzxh")
    private String xcsdxzxh;//新产生待销账序号
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "wtrmingc")
    private String wtrmingc;//委托人名称
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public BigDecimal getGuihzcbj() {
        return guihzcbj;
    }

    public void setGuihzcbj(BigDecimal guihzcbj) {
        this.guihzcbj = guihzcbj;
    }

    public BigDecimal getGuihyqbj() {
        return guihyqbj;
    }

    public void setGuihyqbj(BigDecimal guihyqbj) {
        this.guihyqbj = guihyqbj;
    }

    public BigDecimal getGuihdzbj() {
        return guihdzbj;
    }

    public void setGuihdzbj(BigDecimal guihdzbj) {
        this.guihdzbj = guihdzbj;
    }

    public BigDecimal getGhdzhabj() {
        return ghdzhabj;
    }

    public void setGhdzhabj(BigDecimal ghdzhabj) {
        this.ghdzhabj = ghdzhabj;
    }

    public BigDecimal getGhysyjlx() {
        return ghysyjlx;
    }

    public void setGhysyjlx(BigDecimal ghysyjlx) {
        this.ghysyjlx = ghysyjlx;
    }

    public BigDecimal getGhcsyjlx() {
        return ghcsyjlx;
    }

    public void setGhcsyjlx(BigDecimal ghcsyjlx) {
        this.ghcsyjlx = ghcsyjlx;
    }

    public BigDecimal getGhynshqx() {
        return ghynshqx;
    }

    public void setGhynshqx(BigDecimal ghynshqx) {
        this.ghynshqx = ghynshqx;
    }

    public BigDecimal getGhcushqx() {
        return ghcushqx;
    }

    public void setGhcushqx(BigDecimal ghcushqx) {
        this.ghcushqx = ghcushqx;
    }

    public BigDecimal getGhysyjfx() {
        return ghysyjfx;
    }

    public void setGhysyjfx(BigDecimal ghysyjfx) {
        this.ghysyjfx = ghysyjfx;
    }

    public BigDecimal getGhcsyjfx() {
        return ghcsyjfx;
    }

    public void setGhcsyjfx(BigDecimal ghcsyjfx) {
        this.ghcsyjfx = ghcsyjfx;
    }

    public BigDecimal getGhynshfx() {
        return ghynshfx;
    }

    public void setGhynshfx(BigDecimal ghynshfx) {
        this.ghynshfx = ghynshfx;
    }

    public BigDecimal getGhcushfx() {
        return ghcushfx;
    }

    public void setGhcushfx(BigDecimal ghcushfx) {
        this.ghcushfx = ghcushfx;
    }

    public BigDecimal getGhyjfuxi() {
        return ghyjfuxi;
    }

    public void setGhyjfuxi(BigDecimal ghyjfuxi) {
        this.ghyjfuxi = ghyjfuxi;
    }

    public BigDecimal getGhfxfuxi() {
        return ghfxfuxi;
    }

    public void setGhfxfuxi(BigDecimal ghfxfuxi) {
        this.ghfxfuxi = ghfxfuxi;
    }

    public BigDecimal getGhfeiyin() {
        return ghfeiyin;
    }

    public void setGhfeiyin(BigDecimal ghfeiyin) {
        this.ghfeiyin = ghfeiyin;
    }

    public BigDecimal getGhfajinn() {
        return ghfajinn;
    }

    public void setGhfajinn(BigDecimal ghfajinn) {
        this.ghfajinn = ghfajinn;
    }

    public BigDecimal getGuihzone() {
        return guihzone;
    }

    public void setGuihzone(BigDecimal guihzone) {
        this.guihzone = guihzone;
    }

    public BigDecimal getBenjheji() {
        return benjheji;
    }

    public void setBenjheji(BigDecimal benjheji) {
        this.benjheji = benjheji;
    }

    public String getXcsdxzxh() {
        return xcsdxzxh;
    }

    public void setXcsdxzxh(String xcsdxzxh) {
        this.xcsdxzxh = xcsdxzxh;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    @Override
    public String toString() {
        return "Ln3176RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehuzwmc='" + kehuzwmc + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "guihzcbj='" + guihzcbj + '\'' +
                "guihyqbj='" + guihyqbj + '\'' +
                "guihdzbj='" + guihdzbj + '\'' +
                "ghdzhabj='" + ghdzhabj + '\'' +
                "ghysyjlx='" + ghysyjlx + '\'' +
                "ghcsyjlx='" + ghcsyjlx + '\'' +
                "ghynshqx='" + ghynshqx + '\'' +
                "ghcushqx='" + ghcushqx + '\'' +
                "ghysyjfx='" + ghysyjfx + '\'' +
                "ghcsyjfx='" + ghcsyjfx + '\'' +
                "ghynshfx='" + ghynshfx + '\'' +
                "ghcushfx='" + ghcushfx + '\'' +
                "ghyjfuxi='" + ghyjfuxi + '\'' +
                "ghfxfuxi='" + ghfxfuxi + '\'' +
                "ghfeiyin='" + ghfeiyin + '\'' +
                "ghfajinn='" + ghfajinn + '\'' +
                "guihzone='" + guihzone + '\'' +
                "benjheji='" + benjheji + '\'' +
                "xcsdxzxh='" + xcsdxzxh + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "wtrmingc='" + wtrmingc + '\'' +
                "bjghrzzh='" + bjghrzzh + '\'' +
                "lxghrzzh='" + lxghrzzh + '\'' +
                '}';
    }
}  
