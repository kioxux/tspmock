package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询银票出账保证金账号信息（新信贷调票据）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj21ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sBatchNo")
    private String sBatchNo;//批次号

    public String getSBatchNo() {
        return sBatchNo;
    }

    public void setSBatchNo(String sBatchNo) {
        this.sBatchNo = sBatchNo;
    }

    @Override
    public String toString() {
        return "Xdpj21ReqDto{" +
                "sBatchNo='" + sBatchNo + '\'' +
                '}';
    }
}  
