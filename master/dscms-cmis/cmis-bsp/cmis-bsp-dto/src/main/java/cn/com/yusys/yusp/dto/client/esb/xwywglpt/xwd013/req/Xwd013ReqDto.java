package cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：新信贷获取调查信息接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwd013ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "reqId")
    private String reqId;//调查编号
    @JsonProperty(value = "reqType")
    private String reqType;//财报种类

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    @Override
    public String toString() {
        return "Xwd013ReqDto{" +
                "reqId='" + reqId + '\'' +
                "reqType='" + reqType + '\'' +
                '}';
    }
}  
