package cn.com.yusys.yusp.dto.server.biz.xdht0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：借款合同/担保合同列表信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusName")
    private String cusName;//客户名称（查询条件）
    @JsonProperty(value = "signVideo")
    private String signVideo;//签约视频（查询条件）
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "pageNum")
    private Integer pageNum;//页数
    @JsonProperty(value = "pageSize")
    private Integer pageSize;//条数

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSignVideo() {
        return signVideo;
    }

    public void setSignVideo(String signVideo) {
        this.signVideo = signVideo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusName='" + cusName + '\'' +
                ", signVideo='" + signVideo + '\'' +
                ", managerId='" + managerId + '\'' +
                ", contNo='" + contNo + '\'' +
                ", contType='" + contType + '\'' +
                ", pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                '}';
    }
}
