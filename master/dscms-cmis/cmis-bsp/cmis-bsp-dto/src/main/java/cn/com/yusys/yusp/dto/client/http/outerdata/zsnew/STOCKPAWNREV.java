package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//股权出质信息（新）-注销信息
@JsonPropertyOrder(alphabetic = true)
public class STOCKPAWNREV implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "STK_PAWN_DATE")
    private String STK_PAWN_DATE;//	注销日期
    @JsonProperty(value = "STK_PAWN_RES")
    private String STK_PAWN_RES;//	注销原因
    @JsonProperty(value = "URL")
    private String URL;//	关联内容

    @JsonIgnore
    public String getSTK_PAWN_DATE() {
        return STK_PAWN_DATE;
    }

    @JsonIgnore
    public void setSTK_PAWN_DATE(String STK_PAWN_DATE) {
        this.STK_PAWN_DATE = STK_PAWN_DATE;
    }

    @JsonIgnore
    public String getSTK_PAWN_RES() {
        return STK_PAWN_RES;
    }

    @JsonIgnore
    public void setSTK_PAWN_RES(String STK_PAWN_RES) {
        this.STK_PAWN_RES = STK_PAWN_RES;
    }

    @JsonIgnore
    public String getURL() {
        return URL;
    }

    @JsonIgnore
    public void setURL(String URL) {
        this.URL = URL;
    }

    @Override
    public String toString() {
        return "STOCKPAWNREV{" +
                "STK_PAWN_DATE='" + STK_PAWN_DATE + '\'' +
                ", STK_PAWN_RES='" + STK_PAWN_RES + '\'' +
                ", URL='" + URL + '\'' +
                '}';
    }
}
