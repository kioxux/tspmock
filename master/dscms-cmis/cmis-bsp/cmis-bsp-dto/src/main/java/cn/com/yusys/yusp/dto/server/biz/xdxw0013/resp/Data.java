package cn.com.yusys.yusp.dto.server.biz.xdxw0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：优企贷共借人、合同信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contQnt")
    private Integer contQnt;//合同数量
    @JsonProperty(value = "list")
    private java.util.List<List> list;//list

    public Integer getContQnt() {
        return contQnt;
    }

    public void setContQnt(Integer contQnt) {
        this.contQnt = contQnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contQnt=" + contQnt +
                ", list=" + list +
                '}';
    }
}
