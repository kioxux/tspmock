package cn.com.yusys.yusp.dto.client.esb.irs.irs21;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：单一客户限额测算信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs21RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sxserialno")
    private String sxserialno;//授信申请流水号
    @JsonProperty(value = "custid")
    private String custid;//	客户编号
    @JsonProperty(value = "custame")
    private String custame;//	客户名称
    @JsonProperty(value = "ratingyear")
    private String ratingyear;//评级年度
    @JsonProperty(value = "ratinglevel")
    private String ratinglevel;//	评级结果
    @JsonProperty(value = "modelid")
    private String modelid;//	评级模型
    @JsonProperty(value = "calvalue")
    private String calvalue;//	单一客户测算限额
    @JsonProperty(value = "caldate")
    private String caldate;//	单一客户限额测算日期

    public String getSxserialno() {
        return sxserialno;
    }

    public void setSxserialno(String sxserialno) {
        this.sxserialno = sxserialno;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustame() {
        return custame;
    }

    public void setCustame(String custame) {
        this.custame = custame;
    }

    public String getRatingyear() {
        return ratingyear;
    }

    public void setRatingyear(String ratingyear) {
        this.ratingyear = ratingyear;
    }

    public String getRatinglevel() {
        return ratinglevel;
    }

    public void setRatinglevel(String ratinglevel) {
        this.ratinglevel = ratinglevel;
    }

    public String getModelid() {
        return modelid;
    }

    public void setModelid(String modelid) {
        this.modelid = modelid;
    }

    public String getCalvalue() {
        return calvalue;
    }

    public void setCalvalue(String calvalue) {
        this.calvalue = calvalue;
    }

    public String getCaldate() {
        return caldate;
    }

    public void setCaldate(String caldate) {
        this.caldate = caldate;
    }

    @Override
    public String toString() {
        return "Irs21RespDto{" +
                "sxserialno='" + sxserialno + '\'' +
                ", custid='" + custid + '\'' +
                ", custame='" + custame + '\'' +
                ", ratingyear='" + ratingyear + '\'' +
                ", ratinglevel='" + ratinglevel + '\'' +
                ", modelid='" + modelid + '\'' +
                ", calvalue='" + calvalue + '\'' +
                ", caldate='" + caldate + '\'' +
                '}';
    }
}
