package cn.com.yusys.yusp.dto.client.esb.ypxt.contra;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：信贷查询地方征信接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class ContraListInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dbhtbh")
    private String dbhtbh;//	担保合同编号	是	varchar(50)	是		dbhtbh
    @JsonProperty(value = "yptybh")
    private String yptybh;//	押品统一编号	是	varchar(32)	是		yptybh
    @JsonProperty(value = "sernbz")
    private String sernbz;//	保证人流水号	是	varchar(40)	否		sernbz
    @JsonProperty(value = "bzrzbh")
    private String bzrzbh;//	保证人组编号	是	varchar(40)	否		bzrzbh
    @JsonProperty(value = "isflag")
    private String isflag;//	是否有效	是	varchar(5)	是	1-有效；0-无效	isflag
    @JsonProperty(value = "dbjkrb")
    private String dbjkrb;


    public String getDbhtbh() {
        return dbhtbh;
    }

    public void setDbhtbh(String dbhtbh) {
        this.dbhtbh = dbhtbh;
    }

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getSernbz() {
        return sernbz;
    }

    public void setSernbz(String sernbz) {
        this.sernbz = sernbz;
    }

    public String getBzrzbh() {
        return bzrzbh;
    }

    public void setBzrzbh(String bzrzbh) {
        this.bzrzbh = bzrzbh;
    }

    public String getIsflag() {
        return isflag;
    }

    public void setIsflag(String isflag) {
        this.isflag = isflag;
    }

    public String getDbjkrb() {
        return dbjkrb;
    }

    public void setDbjkrb(String dbjkrb) {
        this.dbjkrb = dbjkrb;
    }

    @Override
    public String toString() {
        return "ArrayListInfo{" +
                "dbhtbh='" + dbhtbh + '\'' +
                ", yptybh='" + yptybh + '\'' +
                ", sernbz='" + sernbz + '\'' +
                ", bzrzbh='" + bzrzbh + '\'' +
                ", isflag='" + isflag + '\'' +
                ", dbjkrb='" + dbjkrb + '\'' +
                '}';
    }
}
