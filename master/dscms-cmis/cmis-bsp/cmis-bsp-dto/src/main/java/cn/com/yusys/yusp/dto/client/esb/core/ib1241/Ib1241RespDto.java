package cn.com.yusys.yusp.dto.client.esb.core.ib1241;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：外围自动冲正(出库撤销)
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ib1241RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yqzhriqi")
    private String yqzhriqi;//原前置日期
    @JsonProperty(value = "yqzhlshu")
    private String yqzhlshu;//原前置流水
    @JsonProperty(value = "chzhriqi")
    private String chzhriqi;//冲账日期
    @JsonProperty(value = "chzhlshu")
    private String chzhlshu;//冲账流水

    public String getYqzhriqi() {
        return yqzhriqi;
    }

    public void setYqzhriqi(String yqzhriqi) {
        this.yqzhriqi = yqzhriqi;
    }

    public String getYqzhlshu() {
        return yqzhlshu;
    }

    public void setYqzhlshu(String yqzhlshu) {
        this.yqzhlshu = yqzhlshu;
    }

    public String getChzhriqi() {
        return chzhriqi;
    }

    public void setChzhriqi(String chzhriqi) {
        this.chzhriqi = chzhriqi;
    }

    public String getChzhlshu() {
        return chzhlshu;
    }

    public void setChzhlshu(String chzhlshu) {
        this.chzhlshu = chzhlshu;
    }

    @Override
    public String toString() {
        return "Ib1241RespDto{" +
                "yqzhriqi='" + yqzhriqi + '\'' +
                "yqzhlshu='" + yqzhlshu + '\'' +
                "chzhriqi='" + chzhriqi + '\'' +
                "chzhlshu='" + chzhlshu + '\'' +
                '}';
    }
}  
