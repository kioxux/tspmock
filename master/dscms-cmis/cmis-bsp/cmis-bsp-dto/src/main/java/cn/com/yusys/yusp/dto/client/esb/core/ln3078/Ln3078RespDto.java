package cn.com.yusys.yusp.dto.client.esb.core.ln3078;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款机构变更
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3078RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yewubhao")
    private String yewubhao;//业务编号
    @JsonProperty(value = "jgzhyibz")
    private String jgzhyibz;//机构转移标志
    @JsonProperty(value = "zhchjgou")
    private String zhchjgou;//转出机构
    @JsonProperty(value = "zhrujgou")
    private String zhrujgou;//转入机构
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "outjiejxx")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3078.Outjiejxx> outjiejxx;//借据输出信息[LIST]

    public String getYewubhao() {
        return yewubhao;
    }

    public void setYewubhao(String yewubhao) {
        this.yewubhao = yewubhao;
    }

    public String getJgzhyibz() {
        return jgzhyibz;
    }

    public void setJgzhyibz(String jgzhyibz) {
        this.jgzhyibz = jgzhyibz;
    }

    public String getZhchjgou() {
        return zhchjgou;
    }

    public void setZhchjgou(String zhchjgou) {
        this.zhchjgou = zhchjgou;
    }

    public String getZhrujgou() {
        return zhrujgou;
    }

    public void setZhrujgou(String zhrujgou) {
        this.zhrujgou = zhrujgou;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public List<Outjiejxx> getOutjiejxx() {
        return outjiejxx;
    }

    public void setOutjiejxx(List<Outjiejxx> outjiejxx) {
        this.outjiejxx = outjiejxx;
    }

    @Override
    public String toString() {
        return "Ln3078RespDto{" +
                "yewubhao='" + yewubhao + '\'' +
                ", jgzhyibz='" + jgzhyibz + '\'' +
                ", zhchjgou='" + zhchjgou + '\'' +
                ", zhrujgou='" + zhrujgou + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", outjiejxx=" + outjiejxx +
                '}';
    }
}
