package cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：借据台账信息接收
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwh001ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "idCard")
    private String idCard;//客户证件号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "loanAmount")
    private BigDecimal loanAmount;//放款金额
    @JsonProperty(value = "month")
    private Integer month;//期限
    @JsonProperty(value = "startDate")
    private String startDate;//借据开始日
    @JsonProperty(value = "endDate")
    private String endDate;//借据到期日
    @JsonProperty(value = "recommendId")
    private String recommendId;//推荐客户经理号
    @JsonProperty(value = "productName")
    private String productName;//产品名称
    @JsonProperty(value = "phone")
    private String phone;//手机号

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRecommendId() {
        return recommendId;
    }

    public void setRecommendId(String recommendId) {
        this.recommendId = recommendId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Xwh001ReqDto{" +
                "idCard='" + idCard + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "billNo='" + billNo + '\'' +
                "loanAmount='" + loanAmount + '\'' +
                "month='" + month + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                "recommendId='" + recommendId + '\'' +
                "productName='" + productName + '\'' +
                "phone='" + phone + '\'' +
                '}';
    }
}  
