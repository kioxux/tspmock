package cn.com.yusys.yusp.dto.client.esb.yk.yky001.req;

import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.LoanList;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.MortList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：用印列表
 *
 * @author xuwh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "tradeCode")
    private String tradeCode;//印章编号
    @JsonProperty(value = "tradeCodeName")
    private String tradeCodeName;//印章名称
    @JsonProperty(value = "applyNum")
    private String applyNum;//印章数量

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    public String getTradeCodeName() {
        return tradeCodeName;
    }

    public void setTradeCodeName(String tradeCodeName) {
        this.tradeCodeName = tradeCodeName;
    }

    public String getApplyNum() {
        return applyNum;
    }

    public void setApplyNum(String applyNum) {
        this.applyNum = applyNum;
    }
}
