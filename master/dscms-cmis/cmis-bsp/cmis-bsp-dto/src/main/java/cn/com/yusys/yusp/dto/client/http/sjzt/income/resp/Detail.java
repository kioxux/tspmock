package cn.com.yusys.yusp.dto.client.http.sjzt.income.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/20 18:37
 * @since 2021/8/20 18:37
 */
public class Detail implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "total")
    private String total;//总条数
    @JsonProperty(value = "data")
    private java.util.List<cn.com.yusys.yusp.dto.client.http.sjzt.income.resp.Data> data;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Detail{" +
                "total='" + total + '\'' +
                ", data=" + data +
                '}';
    }
}
