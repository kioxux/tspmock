package cn.com.yusys.yusp.dto.client.esb.core.mbt951.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：批量文件处理申请
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Mbt951ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "banlwand")
    private String banlwand;//办理网点
    @JsonProperty(value = "beiyzd01")
    private String beiyzd01;//备用字段01
    @JsonProperty(value = "beiyzd02")
    private String beiyzd02;//备用字段2
    @JsonProperty(value = "bizhongg")
    private String bizhongg;//币种
    @JsonProperty(value = "chaohubz")
    private String chaohubz;//钞汇标志
    @JsonProperty(value = "chapbhao")
    private String chapbhao;//产品编号
    @JsonProperty(value = "cunqiiii")
    private String cunqiiii;//存期
    @JsonProperty(value = "daifzhje")
    private BigDecimal daifzhje;//贷方总金额
    @JsonProperty(value = "dailxinm")
    private String dailxinm;//代理人姓名
    @JsonProperty(value = "dailzjho")
    private String dailzjho;//代理人证件号码
    @JsonProperty(value = "daoqxcfs")
    private String daoqxcfs;//到期续存方式
    @JsonProperty(value = "dfzhangh")
    private String dfzhangh;//对方账号
    @JsonProperty(value = "dlywhaoo")
    private String dlywhaoo;//代理业务号
    @JsonProperty(value = "dlzjzlei")
    private String dlzjzlei;//代理人证件种类
    @JsonProperty(value = "filename")
    private String filename;//报表文件名
    @JsonProperty(value = "fuwudaim")
    private String fuwudaim;//服务代码
    @JsonProperty(value = "jiefzhje")
    private BigDecimal jiefzhje;//借方总金额
    @JsonProperty(value = "jizhunll")
    private BigDecimal jizhunll;//基准利率
    @JsonProperty(value = "jysjzifu")
    private String jysjzifu;//交易时间字符串
    @JsonProperty(value = "koukleix")
    private String koukleix;//扣款类型
    @JsonProperty(value = "koukzhao")
    private String koukzhao;//扣款主账号
    @JsonProperty(value = "koukzhmc")
    private String koukzhmc;//扣款账户名称
    @JsonProperty(value = "lilvfdbl")
    private BigDecimal lilvfdbl;//利率浮动比例
    @JsonProperty(value = "lilvfdsz")
    private BigDecimal lilvfdsz;//利率浮动值
    @JsonProperty(value = "llfdonbz")
    private String llfdonbz;//利率浮动标志1
    @JsonProperty(value = "luruguiy")
    private String luruguiy;//录入柜员号
    @JsonProperty(value = "lururiqi")
    private String lururiqi;//录入日期
    @JsonProperty(value = "nfshoqfs")
    private String nfshoqfs;//年费收取方式
    @JsonProperty(value = "piciczlx")
    private String piciczlx;//批次操作类型
    @JsonProperty(value = "picihaoo")
    private String picihaoo;//批次号
    @JsonProperty(value = "picileib")
    private String picileib;//批次类别
    @JsonProperty(value = "picriqii")
    private String picriqii;//批次日期
    @JsonProperty(value = "pingzhzl")
    private String pingzhzl;//凭证类型
    @JsonProperty(value = "qixifans")
    private String qixifans;//起息方式
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "sbzjclfs")
    private String sbzjclfs;//失败资金处理方式
    @JsonProperty(value = "shijlilv")
    private BigDecimal shijlilv;//实际利率
    @JsonProperty(value = "tduifwei")
    private String tduifwei;//通兑范围
    @JsonProperty(value = "tjrgonh1")
    private String tjrgonh1;//推荐人工号1
    @JsonProperty(value = "tjrminc1")
    private String tjrminc1;//推荐人名称1
    @JsonProperty(value = "weixdhao")
    private String weixdhao;//尾箱号
    @JsonProperty(value = "yngxguiy")
    private String yngxguiy;//营销人
    @JsonProperty(value = "youhuibz")
    private String youhuibz;//优惠标志
    @JsonProperty(value = "youhuisz")
    private BigDecimal youhuisz;//优惠值
    @JsonProperty(value = "zcuncqii")
    private String zcuncqii;//转存存期
    @JsonProperty(value = "zhaiyodm")
    private String zhaiyodm;//摘要代码
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn;//支付条件
    @JsonProperty(value = "zhhmjcbz")
    private String zhhmjcbz;//账户户名相符检查标志
    @JsonProperty(value = "zhufldm1")
    private String zhufldm1;//账户分类代码1
    @JsonProperty(value = "zhufldm2")
    private String zhufldm2;//账户分类代码2
    @JsonProperty(value = "zongbis")
    private Integer zongbis;//总笔数
    @JsonProperty(value = "zongjine")
    private BigDecimal zongjine;//总金额
    @JsonProperty(value = "zskhbzhi")
    private String zskhbzhi;//赠送客户标志

    public String getBanlwand() {
        return banlwand;
    }

    public void setBanlwand(String banlwand) {
        this.banlwand = banlwand;
    }

    public String getBeiyzd01() {
        return beiyzd01;
    }

    public void setBeiyzd01(String beiyzd01) {
        this.beiyzd01 = beiyzd01;
    }

    public String getBeiyzd02() {
        return beiyzd02;
    }

    public void setBeiyzd02(String beiyzd02) {
        this.beiyzd02 = beiyzd02;
    }

    public String getBizhongg() {
        return bizhongg;
    }

    public void setBizhongg(String bizhongg) {
        this.bizhongg = bizhongg;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public BigDecimal getDaifzhje() {
        return daifzhje;
    }

    public void setDaifzhje(BigDecimal daifzhje) {
        this.daifzhje = daifzhje;
    }

    public String getDailxinm() {
        return dailxinm;
    }

    public void setDailxinm(String dailxinm) {
        this.dailxinm = dailxinm;
    }

    public String getDailzjho() {
        return dailzjho;
    }

    public void setDailzjho(String dailzjho) {
        this.dailzjho = dailzjho;
    }

    public String getDaoqxcfs() {
        return daoqxcfs;
    }

    public void setDaoqxcfs(String daoqxcfs) {
        this.daoqxcfs = daoqxcfs;
    }

    public String getDfzhangh() {
        return dfzhangh;
    }

    public void setDfzhangh(String dfzhangh) {
        this.dfzhangh = dfzhangh;
    }

    public String getDlywhaoo() {
        return dlywhaoo;
    }

    public void setDlywhaoo(String dlywhaoo) {
        this.dlywhaoo = dlywhaoo;
    }

    public String getDlzjzlei() {
        return dlzjzlei;
    }

    public void setDlzjzlei(String dlzjzlei) {
        this.dlzjzlei = dlzjzlei;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFuwudaim() {
        return fuwudaim;
    }

    public void setFuwudaim(String fuwudaim) {
        this.fuwudaim = fuwudaim;
    }

    public BigDecimal getJiefzhje() {
        return jiefzhje;
    }

    public void setJiefzhje(BigDecimal jiefzhje) {
        this.jiefzhje = jiefzhje;
    }

    public BigDecimal getJizhunll() {
        return jizhunll;
    }

    public void setJizhunll(BigDecimal jizhunll) {
        this.jizhunll = jizhunll;
    }

    public String getJysjzifu() {
        return jysjzifu;
    }

    public void setJysjzifu(String jysjzifu) {
        this.jysjzifu = jysjzifu;
    }

    public String getKoukleix() {
        return koukleix;
    }

    public void setKoukleix(String koukleix) {
        this.koukleix = koukleix;
    }

    public String getKoukzhao() {
        return koukzhao;
    }

    public void setKoukzhao(String koukzhao) {
        this.koukzhao = koukzhao;
    }

    public String getKoukzhmc() {
        return koukzhmc;
    }

    public void setKoukzhmc(String koukzhmc) {
        this.koukzhmc = koukzhmc;
    }

    public BigDecimal getLilvfdbl() {
        return lilvfdbl;
    }

    public void setLilvfdbl(BigDecimal lilvfdbl) {
        this.lilvfdbl = lilvfdbl;
    }

    public BigDecimal getLilvfdsz() {
        return lilvfdsz;
    }

    public void setLilvfdsz(BigDecimal lilvfdsz) {
        this.lilvfdsz = lilvfdsz;
    }

    public String getLlfdonbz() {
        return llfdonbz;
    }

    public void setLlfdonbz(String llfdonbz) {
        this.llfdonbz = llfdonbz;
    }

    public String getLuruguiy() {
        return luruguiy;
    }

    public void setLuruguiy(String luruguiy) {
        this.luruguiy = luruguiy;
    }

    public String getLururiqi() {
        return lururiqi;
    }

    public void setLururiqi(String lururiqi) {
        this.lururiqi = lururiqi;
    }

    public String getNfshoqfs() {
        return nfshoqfs;
    }

    public void setNfshoqfs(String nfshoqfs) {
        this.nfshoqfs = nfshoqfs;
    }

    public String getPiciczlx() {
        return piciczlx;
    }

    public void setPiciczlx(String piciczlx) {
        this.piciczlx = piciczlx;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public String getPicileib() {
        return picileib;
    }

    public void setPicileib(String picileib) {
        this.picileib = picileib;
    }

    public String getPicriqii() {
        return picriqii;
    }

    public void setPicriqii(String picriqii) {
        this.picriqii = picriqii;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getQixifans() {
        return qixifans;
    }

    public void setQixifans(String qixifans) {
        this.qixifans = qixifans;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getSbzjclfs() {
        return sbzjclfs;
    }

    public void setSbzjclfs(String sbzjclfs) {
        this.sbzjclfs = sbzjclfs;
    }

    public BigDecimal getShijlilv() {
        return shijlilv;
    }

    public void setShijlilv(BigDecimal shijlilv) {
        this.shijlilv = shijlilv;
    }

    public String getTduifwei() {
        return tduifwei;
    }

    public void setTduifwei(String tduifwei) {
        this.tduifwei = tduifwei;
    }

    public String getTjrgonh1() {
        return tjrgonh1;
    }

    public void setTjrgonh1(String tjrgonh1) {
        this.tjrgonh1 = tjrgonh1;
    }

    public String getTjrminc1() {
        return tjrminc1;
    }

    public void setTjrminc1(String tjrminc1) {
        this.tjrminc1 = tjrminc1;
    }

    public String getWeixdhao() {
        return weixdhao;
    }

    public void setWeixdhao(String weixdhao) {
        this.weixdhao = weixdhao;
    }

    public String getYngxguiy() {
        return yngxguiy;
    }

    public void setYngxguiy(String yngxguiy) {
        this.yngxguiy = yngxguiy;
    }

    public String getYouhuibz() {
        return youhuibz;
    }

    public void setYouhuibz(String youhuibz) {
        this.youhuibz = youhuibz;
    }

    public BigDecimal getYouhuisz() {
        return youhuisz;
    }

    public void setYouhuisz(BigDecimal youhuisz) {
        this.youhuisz = youhuisz;
    }

    public String getZcuncqii() {
        return zcuncqii;
    }

    public void setZcuncqii(String zcuncqii) {
        this.zcuncqii = zcuncqii;
    }

    public String getZhaiyodm() {
        return zhaiyodm;
    }

    public void setZhaiyodm(String zhaiyodm) {
        this.zhaiyodm = zhaiyodm;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getZhhmjcbz() {
        return zhhmjcbz;
    }

    public void setZhhmjcbz(String zhhmjcbz) {
        this.zhhmjcbz = zhhmjcbz;
    }

    public String getZhufldm1() {
        return zhufldm1;
    }

    public void setZhufldm1(String zhufldm1) {
        this.zhufldm1 = zhufldm1;
    }

    public String getZhufldm2() {
        return zhufldm2;
    }

    public void setZhufldm2(String zhufldm2) {
        this.zhufldm2 = zhufldm2;
    }

    public Integer getZongbis() {
        return zongbis;
    }

    public void setZongbis(Integer zongbis) {
        this.zongbis = zongbis;
    }

    public BigDecimal getZongjine() {
        return zongjine;
    }

    public void setZongjine(BigDecimal zongjine) {
        this.zongjine = zongjine;
    }

    public String getZskhbzhi() {
        return zskhbzhi;
    }

    public void setZskhbzhi(String zskhbzhi) {
        this.zskhbzhi = zskhbzhi;
    }

    @Override
    public String toString() {
        return "Mbt951ReqDto{" +
                "banlwand='" + banlwand + '\'' +
                "beiyzd01='" + beiyzd01 + '\'' +
                "beiyzd02='" + beiyzd02 + '\'' +
                "bizhongg='" + bizhongg + '\'' +
                "chaohubz='" + chaohubz + '\'' +
                "chapbhao='" + chapbhao + '\'' +
                "cunqiiii='" + cunqiiii + '\'' +
                "daifzhje='" + daifzhje + '\'' +
                "dailxinm='" + dailxinm + '\'' +
                "dailzjho='" + dailzjho + '\'' +
                "daoqxcfs='" + daoqxcfs + '\'' +
                "dfzhangh='" + dfzhangh + '\'' +
                "dlywhaoo='" + dlywhaoo + '\'' +
                "dlzjzlei='" + dlzjzlei + '\'' +
                "filename='" + filename + '\'' +
                "fuwudaim='" + fuwudaim + '\'' +
                "jiefzhje='" + jiefzhje + '\'' +
                "jizhunll='" + jizhunll + '\'' +
                "jysjzifu='" + jysjzifu + '\'' +
                "koukleix='" + koukleix + '\'' +
                "koukzhao='" + koukzhao + '\'' +
                "koukzhmc='" + koukzhmc + '\'' +
                "lilvfdbl='" + lilvfdbl + '\'' +
                "lilvfdsz='" + lilvfdsz + '\'' +
                "llfdonbz='" + llfdonbz + '\'' +
                "luruguiy='" + luruguiy + '\'' +
                "lururiqi='" + lururiqi + '\'' +
                "nfshoqfs='" + nfshoqfs + '\'' +
                "piciczlx='" + piciczlx + '\'' +
                "picihaoo='" + picihaoo + '\'' +
                "picileib='" + picileib + '\'' +
                "picriqii='" + picriqii + '\'' +
                "pingzhzl='" + pingzhzl + '\'' +
                "qixifans='" + qixifans + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "sbzjclfs='" + sbzjclfs + '\'' +
                "shijlilv='" + shijlilv + '\'' +
                "tduifwei='" + tduifwei + '\'' +
                "tjrgonh1='" + tjrgonh1 + '\'' +
                "tjrminc1='" + tjrminc1 + '\'' +
                "weixdhao='" + weixdhao + '\'' +
                "yngxguiy='" + yngxguiy + '\'' +
                "youhuibz='" + youhuibz + '\'' +
                "youhuisz='" + youhuisz + '\'' +
                "zcuncqii='" + zcuncqii + '\'' +
                "zhaiyodm='" + zhaiyodm + '\'' +
                "zhfutojn='" + zhfutojn + '\'' +
                "zhhmjcbz='" + zhhmjcbz + '\'' +
                "zhufldm1='" + zhufldm1 + '\'' +
                "zhufldm2='" + zhufldm2 + '\'' +
                "zongbis='" + zongbis + '\'' +
                "zongjine='" + zongjine + '\'' +
                "zskhbzhi='" + zskhbzhi + '\'' +
                '}';
    }
}  
