package cn.com.yusys.yusp.dto.client.esb.ypxt.billyp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：票据信息同步接口
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class BillypRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yptybh")
    private String yptybh;//押品统一编号
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "assetNo")
    private String assetNo;

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    @Override
    public String toString() {
        return "BillypRespDto{" +
                "yptybh='" + yptybh + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", assetNo='" + assetNo + '\'' +
                '}';
    }
}

