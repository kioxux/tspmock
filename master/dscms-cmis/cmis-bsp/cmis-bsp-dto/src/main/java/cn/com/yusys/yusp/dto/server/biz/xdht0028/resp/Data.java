package cn.com.yusys.yusp.dto.server.biz.xdht0028.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 10:34
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "manager_phone")
    private String manager_phone;//客户经理电话号码
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号

    public String getManager_phone() {
        return manager_phone;
    }

    public void setManager_phone(String manager_phone) {
        this.manager_phone = manager_phone;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Xdht0028RespDto{" +
                "manager_phone='" + manager_phone + '\'' +
                "managerId='" + managerId + '\'' +
                '}';
    }
}
