package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：交易请求信息域:汇率信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
@JsonPropertyOrder(alphabetic = true)
public class CurInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "curtype")
    private String curtype; // 币种代码
    @JsonProperty(value = "curvalue")
    private BigDecimal curvalue; // 汇率

    public String getCurtype() {
        return curtype;
    }

    public void setCurtype(String curtype) {
        this.curtype = curtype;
    }

    public BigDecimal getCurvalue() {
        return curvalue;
    }

    public void setCurvalue(BigDecimal curvalue) {
        this.curvalue = curvalue;
    }

    @Override
    public String toString() {
        return "CurInfo{" +
                "curtype='" + curtype + '\'' +
                ", curvalue=" + curvalue +
                '}';
    }
}
