package cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 响应DTO：本异地借阅归还接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Cwm002RespDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "returnCode")
    private String returnCode; // 处理结果

    @JsonProperty(value = "returnInfo")
    private String returnInfo; // 处理信息

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnInfo() {
        return returnInfo;
    }

    public void setReturnInfo(String returnInfo) {
        this.returnInfo = returnInfo;
    }

    @Override
    public String toString() {
        return "Cwm002RespDto{" +
                "returnCode='" + returnCode + '\'' +
                ", returnInfo='" + returnInfo + '\'' +
                '}';
    }
}
