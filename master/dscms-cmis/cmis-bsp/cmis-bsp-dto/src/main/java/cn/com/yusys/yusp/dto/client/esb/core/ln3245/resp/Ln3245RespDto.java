package cn.com.yusys.yusp.dto.client.esb.core.ln3245.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：核销登记簿查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3245RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zongbish")
    private Integer zongbish;//总笔数
    @JsonProperty(value = "lstHxDkxx")
    private List<LstHxDkxx> lstHxDkxx;//核销登记信息列表

	public Integer getZongbish() {
		return zongbish;
	}

	public void setZongbish(Integer zongbish) {
		this.zongbish = zongbish;
	}

	public List<LstHxDkxx> getLstHxDkxx() {
		return lstHxDkxx;
	}

	public void setLstHxDkxx(List<LstHxDkxx> lstHxDkxx) {
		this.lstHxDkxx = lstHxDkxx;
	}

	@Override
	public String toString() {
		return "Ln3245RespDto{" +
				"zongbish=" + zongbish +
				", lstHxDkxx=" + lstHxDkxx +
				'}';
	}
}
