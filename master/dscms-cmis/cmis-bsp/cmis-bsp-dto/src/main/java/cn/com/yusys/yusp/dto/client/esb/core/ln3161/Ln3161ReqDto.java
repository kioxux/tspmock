package cn.com.yusys.yusp.dto.client.esb.core.ln3161;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：资产证券化协议登记
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3161ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "caozfshi")
    private String caozfshi;//操作方式
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "ruzjigou")
    private String ruzjigou;//入账机构
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "zcrfshii")
    private String zcrfshii;//资产融通方式
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "qiandriq")
    private String qiandriq;//签订日期
    @JsonProperty(value = "fengbriq")
    private String fengbriq;//封包日期
    @JsonProperty(value = "ruchiriq")
    private String ruchiriq;//入池日期
    @JsonProperty(value = "huigriqi")
    private String huigriqi;//回购日期
    @JsonProperty(value = "shxiaorq")
    private String shxiaorq;//失效日期
    @JsonProperty(value = "picihaoo")
    private String picihaoo;//批次号
    @JsonProperty(value = "xyzuigxe")
    private BigDecimal xyzuigxe;//协议最高限额
    @JsonProperty(value = "xieyilil")
    private BigDecimal xieyilil;//协议利率
    @JsonProperty(value = "xieyilix")
    private BigDecimal xieyilix;//协议利息
    @JsonProperty(value = "zcrtbili")
    private BigDecimal zcrtbili;//资产融通比例
    @JsonProperty(value = "fysfzhqh")
    private String fysfzhqh;//费用是否证券化
    @JsonProperty(value = "fjsfzhqh")
    private String fjsfzhqh;//罚金是否证券化
    @JsonProperty(value = "jydsleix")
    private String jydsleix;//交易对手类型
    @JsonProperty(value = "jydshmch")
    private String jydshmch;//交易对手名称
    @JsonProperty(value = "jydszhao")
    private String jydszhao;//交易对手账号
    @JsonProperty(value = "jydszhzh")
    private String jydszhzh;//交易对手账号子序号
    @JsonProperty(value = "jydszhmc")
    private String jydszhmc;//交易对手账户名称
    @JsonProperty(value = "zhkaihhh")
    private String zhkaihhh;//账户开户行行号
    @JsonProperty(value = "zhkaihhm")
    private String zhkaihhm;//账户开户行行名
    @JsonProperty(value = "xunhchbz")
    private String xunhchbz;//循环池标志
    @JsonProperty(value = "ssfenbbz")
    private String ssfenbbz;//实时封包标志
    @JsonProperty(value = "zjlyzhao")
    private String zjlyzhao;//资金来源账号
    @JsonProperty(value = "zjlyzzxh")
    private String zjlyzzxh;//资金来源账号子序号
    @JsonProperty(value = "zrfukzhh")
    private String zrfukzhh;//对外付款账号
    @JsonProperty(value = "zrfukzxh")
    private String zrfukzxh;//对外付款账号子序号

    public String getCaozfshi() {
        return caozfshi;
    }

    public void setCaozfshi(String caozfshi) {
        this.caozfshi = caozfshi;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getZcrfshii() {
        return zcrfshii;
    }

    public void setZcrfshii(String zcrfshii) {
        this.zcrfshii = zcrfshii;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getQiandriq() {
        return qiandriq;
    }

    public void setQiandriq(String qiandriq) {
        this.qiandriq = qiandriq;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getRuchiriq() {
        return ruchiriq;
    }

    public void setRuchiriq(String ruchiriq) {
        this.ruchiriq = ruchiriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public String getShxiaorq() {
        return shxiaorq;
    }

    public void setShxiaorq(String shxiaorq) {
        this.shxiaorq = shxiaorq;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public BigDecimal getXyzuigxe() {
        return xyzuigxe;
    }

    public void setXyzuigxe(BigDecimal xyzuigxe) {
        this.xyzuigxe = xyzuigxe;
    }

    public BigDecimal getXieyilil() {
        return xieyilil;
    }

    public void setXieyilil(BigDecimal xieyilil) {
        this.xieyilil = xieyilil;
    }

    public BigDecimal getXieyilix() {
        return xieyilix;
    }

    public void setXieyilix(BigDecimal xieyilix) {
        this.xieyilix = xieyilix;
    }

    public BigDecimal getZcrtbili() {
        return zcrtbili;
    }

    public void setZcrtbili(BigDecimal zcrtbili) {
        this.zcrtbili = zcrtbili;
    }

    public String getFysfzhqh() {
        return fysfzhqh;
    }

    public void setFysfzhqh(String fysfzhqh) {
        this.fysfzhqh = fysfzhqh;
    }

    public String getFjsfzhqh() {
        return fjsfzhqh;
    }

    public void setFjsfzhqh(String fjsfzhqh) {
        this.fjsfzhqh = fjsfzhqh;
    }

    public String getJydsleix() {
        return jydsleix;
    }

    public void setJydsleix(String jydsleix) {
        this.jydsleix = jydsleix;
    }

    public String getJydshmch() {
        return jydshmch;
    }

    public void setJydshmch(String jydshmch) {
        this.jydshmch = jydshmch;
    }

    public String getJydszhao() {
        return jydszhao;
    }

    public void setJydszhao(String jydszhao) {
        this.jydszhao = jydszhao;
    }

    public String getJydszhzh() {
        return jydszhzh;
    }

    public void setJydszhzh(String jydszhzh) {
        this.jydszhzh = jydszhzh;
    }

    public String getJydszhmc() {
        return jydszhmc;
    }

    public void setJydszhmc(String jydszhmc) {
        this.jydszhmc = jydszhmc;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public String getXunhchbz() {
        return xunhchbz;
    }

    public void setXunhchbz(String xunhchbz) {
        this.xunhchbz = xunhchbz;
    }

    public String getSsfenbbz() {
        return ssfenbbz;
    }

    public void setSsfenbbz(String ssfenbbz) {
        this.ssfenbbz = ssfenbbz;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    @Override
    public String toString() {
        return "Ln3161ReqDto{" +
                "caozfshi='" + caozfshi + '\'' +
                "xieybhao='" + xieybhao + '\'' +
                "xieyimch='" + xieyimch + '\'' +
                "ruzjigou='" + ruzjigou + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "zcrfshii='" + zcrfshii + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "qiandriq='" + qiandriq + '\'' +
                "fengbriq='" + fengbriq + '\'' +
                "ruchiriq='" + ruchiriq + '\'' +
                "huigriqi='" + huigriqi + '\'' +
                "shxiaorq='" + shxiaorq + '\'' +
                "picihaoo='" + picihaoo + '\'' +
                "xyzuigxe='" + xyzuigxe + '\'' +
                "xieyilil='" + xieyilil + '\'' +
                "xieyilix='" + xieyilix + '\'' +
                "zcrtbili='" + zcrtbili + '\'' +
                "fysfzhqh='" + fysfzhqh + '\'' +
                "fjsfzhqh='" + fjsfzhqh + '\'' +
                "jydsleix='" + jydsleix + '\'' +
                "jydshmch='" + jydshmch + '\'' +
                "jydszhao='" + jydszhao + '\'' +
                "jydszhzh='" + jydszhzh + '\'' +
                "jydszhmc='" + jydszhmc + '\'' +
                "zhkaihhh='" + zhkaihhh + '\'' +
                "zhkaihhm='" + zhkaihhm + '\'' +
                "xunhchbz='" + xunhchbz + '\'' +
                "ssfenbbz='" + ssfenbbz + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "zrfukzhh='" + zrfukzhh + '\'' +
                "zrfukzxh='" + zrfukzxh + '\'' +
                '}';
    }
}  
