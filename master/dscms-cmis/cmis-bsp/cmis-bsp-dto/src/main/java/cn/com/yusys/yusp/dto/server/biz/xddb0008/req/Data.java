package cn.com.yusys.yusp.dto.server.biz.xddb0008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 15:00
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pldManName")
    private String pldManName;//抵押人名称
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "startPageNum")
    private String startPageNum;//开始页码
    @JsonProperty(value = "pageSize")
    private String pageSize;//每页记录数

    public String getPldManName() {
        return pldManName;
    }

    public void setPldManName(String pldManName) {
        this.pldManName = pldManName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Data{" +
                "pldManName='" + pldManName + '\'' +
                ", managerId='" + managerId + '\'' +
                ", startPageNum='" + startPageNum + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
