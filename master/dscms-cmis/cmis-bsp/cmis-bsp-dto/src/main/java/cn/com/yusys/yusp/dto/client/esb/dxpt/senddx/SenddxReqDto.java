package cn.com.yusys.yusp.dto.client.esb.dxpt.senddx;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：短信/微信发送批量接口
 *
 * @author hjk
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SenddxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "infopt")
    private String infopt;//信息平台,dx:短信 wx1:微信 wx2:寿光微信 wxN：其它微信（此字段可扩展，结合微信签约时登记） sgdx:寿光短信 dhdx:东海短信
    @JsonProperty(value = "senddxReqList")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList> senddxReqList;

    public String getInfopt() {
        return infopt;
    }

    public void setInfopt(String infopt) {
        this.infopt = infopt;
    }

    public List<SenddxReqList> getSenddxReqList() {
        return senddxReqList;
    }

    public void setSenddxReqList(List<SenddxReqList> senddxReqList) {
        this.senddxReqList = senddxReqList;
    }

    @Override
    public String toString() {
        return "SenddxReqDto{" +
                "infopt='" + infopt + '\'' +
                ", senddxReqList=" + senddxReqList +
                '}';
    }
}
