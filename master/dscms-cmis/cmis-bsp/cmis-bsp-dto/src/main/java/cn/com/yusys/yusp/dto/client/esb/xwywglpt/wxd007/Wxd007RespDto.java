package cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd007;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：请求小V平台综合决策管理列表查询
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxd007RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "code")
    private String code;//返回码
    @JsonProperty(value = "message")
    private String message;//返回信息
    @JsonProperty(value = "productCode")
    private String productCode;//产品编码
    @JsonProperty(value = "productType")
    private String productType;//产品阶段
    @JsonProperty(value = "indName")
    private String indName;//借款人姓名
    @JsonProperty(value = "indCertID")
    private String indCertID;//身份证号
    @JsonProperty(value = "creditAmount")
    private BigDecimal creditAmount;//审批额度
    @JsonProperty(value = "createTime")
    private String createTime;//申请时间
    @JsonProperty(value = "statusDetail")
    private String statusDetail;//审批状态


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getIndName() {
        return indName;
    }

    public void setIndName(String indName) {
        this.indName = indName;
    }

    public String getIndCertID() {
        return indCertID;
    }

    public void setIndCertID(String indCertID) {
        this.indCertID = indCertID;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    @Override
    public String toString() {
        return "Wxd007RespDto{" +
                "code='" + code + '\'' +
                "message='" + message + '\'' +
                "productCode='" + productCode + '\'' +
                "productType='" + productType + '\'' +
                "indName='" + indName + '\'' +
                "indCertID='" + indCertID + '\'' +
                "creditAmount='" + creditAmount + '\'' +
                "createTime='" + createTime + '\'' +
                "statusDetail='" + statusDetail + '\'' +
                '}';
    }
}
