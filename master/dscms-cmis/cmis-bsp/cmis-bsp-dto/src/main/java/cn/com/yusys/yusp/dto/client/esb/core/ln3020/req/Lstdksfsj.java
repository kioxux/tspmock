package cn.com.yusys.yusp.dto.client.esb.core.ln3020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：贷款收费事件
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdksfsj implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "shoufzhl")
    private String shoufzhl;//收费种类
    @JsonProperty(value = "shoufjee")
    private BigDecimal shoufjee;//收费金额/比例
    @JsonProperty(value = "fufeizhh")
    private String fufeizhh;//付费账号
    @JsonProperty(value = "shoufshj")
    private String shoufshj;//收费事件
    @JsonProperty(value = "shoufdma")
    private String shoufdma;//收费代码
    @JsonProperty(value = "shfshjmc")
    private String shfshjmc;//收费事件名称
    @JsonProperty(value = "shfdmamc")
    private String shfdmamc;//收费代码名称
    @JsonProperty(value = "ffzhhzxh")
    private String ffzhhzxh;//付费账号子序号
    @JsonProperty(value = "sfrzhzhh")
    private String sfrzhzhh;//收费入账账号
    @JsonProperty(value = "sfrzhzxh")
    private String sfrzhzxh;//收费入账账号子序号

    public String getShoufzhl() {
        return shoufzhl;
    }

    public void setShoufzhl(String shoufzhl) {
        this.shoufzhl = shoufzhl;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    public String getFufeizhh() {
        return fufeizhh;
    }

    public void setFufeizhh(String fufeizhh) {
        this.fufeizhh = fufeizhh;
    }

    public String getShoufshj() {
        return shoufshj;
    }

    public void setShoufshj(String shoufshj) {
        this.shoufshj = shoufshj;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfshjmc() {
        return shfshjmc;
    }

    public void setShfshjmc(String shfshjmc) {
        this.shfshjmc = shfshjmc;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public String getFfzhhzxh() {
        return ffzhhzxh;
    }

    public void setFfzhhzxh(String ffzhhzxh) {
        this.ffzhhzxh = ffzhhzxh;
    }

    public String getSfrzhzhh() {
        return sfrzhzhh;
    }

    public void setSfrzhzhh(String sfrzhzhh) {
        this.sfrzhzhh = sfrzhzhh;
    }

    public String getSfrzhzxh() {
        return sfrzhzxh;
    }

    public void setSfrzhzxh(String sfrzhzxh) {
        this.sfrzhzxh = sfrzhzxh;
    }

    @Override
    public String toString() {
        return "Lstdksfsj{" +
                "shoufzhl='" + shoufzhl + '\'' +
                ", shoufjee=" + shoufjee +
                ", fufeizhh='" + fufeizhh + '\'' +
                ", shoufshj='" + shoufshj + '\'' +
                ", shoufdma='" + shoufdma + '\'' +
                ", shfshjmc='" + shfshjmc + '\'' +
                ", shfdmamc='" + shfdmamc + '\'' +
                ", ffzhhzxh='" + ffzhhzxh + '\'' +
                ", sfrzhzhh='" + sfrzhzhh + '\'' +
                ", sfrzhzxh='" + sfrzhzxh + '\'' +
                '}';
    }
}