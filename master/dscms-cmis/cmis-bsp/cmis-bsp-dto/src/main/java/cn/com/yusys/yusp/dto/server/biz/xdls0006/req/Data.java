package cn.com.yusys.yusp.dto.server.biz.xdls0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/18 11:05:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/18 11:05
 * @since 2021/5/18 11:05
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "prdType")
    private String prdType;//产品类型

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getPrdType() {
        return prdType;
    }

    public void setPrdType(String prdType) {
        this.prdType = prdType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "certNo='" + certNo + '\'' +
                ", prdType='" + prdType + '\'' +
                '}';
    }
}
