package cn.com.yusys.yusp.dto.server.biz.xddh0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 11:21:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 11:21
 * @since 2021/5/22 11:21
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//任务编号
    @JsonProperty(value = "iotspotTime")
    private String iotspotTime;//现场检查时间
    @JsonProperty(value = "chekRsn")
    private String chekRsn;//检查原因s
    @JsonProperty(value = "isNormalProduce")
    private String isNormalProduce;//是否正常生产
    @JsonProperty(value = "isCoop")
    private String isCoop;//客户配合度
    @JsonProperty(value = "status")
    private String status;//状态

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getIotspotTime() {
        return iotspotTime;
    }

    public void setIotspotTime(String iotspotTime) {
        this.iotspotTime = iotspotTime;
    }

    public String getChekRsn() {
        return chekRsn;
    }

    public void setChekRsn(String chekRsn) {
        this.chekRsn = chekRsn;
    }

    public String getIsNormalProduce() {
        return isNormalProduce;
    }

    public void setIsNormalProduce(String isNormalProduce) {
        this.isNormalProduce = isNormalProduce;
    }

    public String getIsCoop() {
        return isCoop;
    }

    public void setIsCoop(String isCoop) {
        this.isCoop = isCoop;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Xddh0001ReqDto{" +
                "serno='" + serno + '\'' +
                "iotspotTime='" + iotspotTime + '\'' +
                "chekRsn='" + chekRsn + '\'' +
                "isNormalProduce='" + isNormalProduce + '\'' +
                "isCoop='" + isCoop + '\'' +
                "status='" + status + '\'' +
                '}';
    }
}
