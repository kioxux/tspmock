package cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb02.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询不动产信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb02RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_no")
    private String addr;//地址
    @JsonProperty(value = "reg_no")
    private String reg_no;//数量
    @JsonProperty(value = "num")
    private String num;//编号

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Xddb02RespDto{" +
                "addr='" + addr + '\'' +
                ", reg_no='" + reg_no + '\'' +
                ", num='" + num + '\'' +
                '}';
    }
}
