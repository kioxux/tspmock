package cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：核销锁定接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwh003RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
   
}  
