package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：交易请求信息域:业务信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 10:09
 */
@JsonPropertyOrder(alphabetic = true)
public class BusinessInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//	业务申请流水号
    @JsonProperty(value = "item_id")
    private String item_id;//	授信台账编号
    @JsonProperty(value = "lmt_serno")
    private String lmt_serno;//	授信协议编号
    @JsonProperty(value = "cus_id")
    private String cus_id;//	客户号
    @JsonProperty(value = "cus_name")
    private String cus_name;//	客户名称
    @JsonProperty(value = "loan_form")
    private String loan_form;//	贷款形式
    @JsonProperty(value = "pro_no")
    private String pro_no;//	业务品种编号
    @JsonProperty(value = "prd_name")
    private String prd_name;//	业务品种名称
    @JsonProperty(value = "loan_direction")
    private String loan_direction;//	贷款投向
    @JsonProperty(value = "cont_type")
    private String cont_type;//	合同类型
    @JsonProperty(value = "assure_means_main")
    private String assure_means_main;//	担保方式
    @JsonProperty(value = "cur_type")
    private String cur_type;//	币种
    @JsonProperty(value = "acc_amount")
    private BigDecimal acc_amount;//	合同金额（元）
    @JsonProperty(value = "start_date")
    private String start_date;//	合同起始日期
    @JsonProperty(value = "expi_date")
    private String expi_date;//	合同到期日期
    @JsonProperty(value = "cont_state")
    private String cont_state;//	合同状态
    @JsonProperty(value = "input_id")
    private String input_id;//	登记人
    @JsonProperty(value = "manager_id")
    private String manager_id;//	客户经理
    @JsonProperty(value = "manager_br_id")
    private String manager_br_id;//	管理机构
    @JsonProperty(value = "fina_br_id")
    private String fina_br_id;//	账务机构
    @JsonProperty(value = "guarantee_no")
    private String guarantee_no;//	保函种类
    @JsonProperty(value = "guarantee_name")
    private String guarantee_name;//	保函名称
    @JsonProperty(value = "loancard_due")
    private BigDecimal loancard_due;//	信用证付款期限
    @JsonProperty(value = "forward_days")
    private BigDecimal forward_days;//	远期天数
    @JsonProperty(value = "pro_details")
    private String pro_details;//	具体产品
    @JsonProperty(value = "isin_revocation")
    private String isin_revocation;//	是否可无条件撤销
    @JsonProperty(value = "loan_due")
    private BigDecimal loan_due;//	贷款期限

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getLoan_form() {
        return loan_form;
    }

    public void setLoan_form(String loan_form) {
        this.loan_form = loan_form;
    }

    public String getPro_no() {
        return pro_no;
    }

    public void setPro_no(String pro_no) {
        this.pro_no = pro_no;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getLoan_direction() {
        return loan_direction;
    }

    public void setLoan_direction(String loan_direction) {
        this.loan_direction = loan_direction;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getAssure_means_main() {
        return assure_means_main;
    }

    public void setAssure_means_main(String assure_means_main) {
        this.assure_means_main = assure_means_main;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getAcc_amount() {
        return acc_amount;
    }

    public void setAcc_amount(BigDecimal acc_amount) {
        this.acc_amount = acc_amount;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getExpi_date() {
        return expi_date;
    }

    public void setExpi_date(String expi_date) {
        this.expi_date = expi_date;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    public String getInput_id() {
        return input_id;
    }

    public void setInput_id(String input_id) {
        this.input_id = input_id;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getFina_br_id() {
        return fina_br_id;
    }

    public void setFina_br_id(String fina_br_id) {
        this.fina_br_id = fina_br_id;
    }

    public String getGuarantee_no() {
        return guarantee_no;
    }

    public void setGuarantee_no(String guarantee_no) {
        this.guarantee_no = guarantee_no;
    }

    public String getGuarantee_name() {
        return guarantee_name;
    }

    public void setGuarantee_name(String guarantee_name) {
        this.guarantee_name = guarantee_name;
    }

    public BigDecimal getLoancard_due() {
        return loancard_due;
    }

    public void setLoancard_due(BigDecimal loancard_due) {
        this.loancard_due = loancard_due;
    }

    public BigDecimal getForward_days() {
        return forward_days;
    }

    public void setForward_days(BigDecimal forward_days) {
        this.forward_days = forward_days;
    }

    public String getPro_details() {
        return pro_details;
    }

    public void setPro_details(String pro_details) {
        this.pro_details = pro_details;
    }

    public String getIsin_revocation() {
        return isin_revocation;
    }

    public void setIsin_revocation(String isin_revocation) {
        this.isin_revocation = isin_revocation;
    }

    public BigDecimal getLoan_due() {
        return loan_due;
    }

    public void setLoan_due(BigDecimal loan_due) {
        this.loan_due = loan_due;
    }

    @Override
    public String toString() {
        return "BusinessInfo{" +
                "serno='" + serno + '\'' +
                ", item_id='" + item_id + '\'' +
                ", lmt_serno='" + lmt_serno + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", loan_form='" + loan_form + '\'' +
                ", pro_no='" + pro_no + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", loan_direction='" + loan_direction + '\'' +
                ", cont_type='" + cont_type + '\'' +
                ", assure_means_main='" + assure_means_main + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", acc_amount=" + acc_amount +
                ", start_date='" + start_date + '\'' +
                ", expi_date='" + expi_date + '\'' +
                ", cont_state='" + cont_state + '\'' +
                ", input_id='" + input_id + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", fina_br_id='" + fina_br_id + '\'' +
                ", guarantee_no='" + guarantee_no + '\'' +
                ", guarantee_name='" + guarantee_name + '\'' +
                ", loancard_due=" + loancard_due +
                ", forward_days=" + forward_days +
                ", pro_details='" + pro_details + '\'' +
                ", isin_revocation='" + isin_revocation + '\'' +
                ", loan_due=" + loan_due +
                '}';
    }
}
