package cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷客户核心业绩统计查询列表
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdhxQueryTotalListRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "total")
    private Integer total;//查询数量
    @JsonProperty(value = "page")
    private Integer page;//查询页数
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.List> list;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "XdhxQueryTotalListRespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", total=" + total +
                ", page=" + page +
                ", list=" + list +
                '}';
    }
}
