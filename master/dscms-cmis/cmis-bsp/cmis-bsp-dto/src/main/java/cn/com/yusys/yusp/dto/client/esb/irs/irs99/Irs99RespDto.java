package cn.com.yusys.yusp.dto.client.esb.irs.irs99;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应DTO：债项评级
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月15日 上午11:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs99RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "BizMessageInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.BizMessageInfo> BizMessageInfo;

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.BizMessageInfo> getBizMessageInfo() {
        return BizMessageInfo;
    }

    @JsonIgnore
    public void setBizMessageInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.BizMessageInfo> bizMessageInfo) {
        BizMessageInfo = bizMessageInfo;
    }

    @Override
    public String toString() {
        return "Irs99RespDto{" +
                "BizMessageInfo=" + BizMessageInfo +
                '}';
    }
}
