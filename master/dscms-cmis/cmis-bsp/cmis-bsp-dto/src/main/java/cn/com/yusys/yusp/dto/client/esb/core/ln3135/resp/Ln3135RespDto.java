package cn.com.yusys.yusp.dto.client.esb.core.ln3135.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：通过贷款借据号和贷款账号查询贷款期供计划
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3135RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "listnm")
    private java.util.List<Listnm> listnm;//贷款定制期供计划表

    public List<Listnm> getListnm() {
        return listnm;
    }

    public void setListnm(List<Listnm> listnm) {
        this.listnm = listnm;
    }

    @Override
    public String toString() {
        return "Ln3135RespDto{" +
                "listnm=" + listnm +
                '}';
    }
}
