package cn.com.yusys.yusp.dto.server.biz.xdtz0063.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：他行退汇冻结编号更新
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "hstrsq")
    private String hstrsq;//受理编号
    @JsonProperty(value = "xdjbh")
    private String xdjbh;//新冻结编号

    public String getHstrsq() {
        return hstrsq;
    }

    public void setHstrsq(String hstrsq) {
        this.hstrsq = hstrsq;
    }

    public String getXdjbh() {
        return xdjbh;
    }

    public void setXdjbh(String xdjbh) {
        this.xdjbh = xdjbh;
    }

    @Override
    public String toString() {
        return "Data{" +
                "hstrsq='" + hstrsq + '\'' +
                ", xdjbh='" + xdjbh + '\'' +
                '}';
    }
}
