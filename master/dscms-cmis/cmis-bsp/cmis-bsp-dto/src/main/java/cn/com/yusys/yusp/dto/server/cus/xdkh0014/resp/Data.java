package cn.com.yusys.yusp.dto.server.cus.xdkh0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：省心E付白名单信息维护
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalQnt")
    private String totalQnt;//总数
    @JsonProperty(value = "list")
    private java.util.List<List> list;


    public String getTotalQnt() {
        return totalQnt;
    }

    public void setTotalQnt(String totalQnt) {
        this.totalQnt = totalQnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "totalQnt='" + totalQnt + '\'' +
                ", list=" + list +
                '}';
    }
}
