package cn.com.yusys.yusp.dto.server.biz.xdtz0056.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 11:04:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 11:04
 * @since 2021/5/22 11:04
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "biztype")
    private String biztype;//

    public String getBiztype() {
        return biztype;
    }

    public void setBiztype(String biztype) {
        this.biztype = biztype;
    }

    @Override
    public String toString() {
        return "Xdtz0056RespDto{" +
                "biztype='" + biztype + '\'' +
                '}';
    }
}
