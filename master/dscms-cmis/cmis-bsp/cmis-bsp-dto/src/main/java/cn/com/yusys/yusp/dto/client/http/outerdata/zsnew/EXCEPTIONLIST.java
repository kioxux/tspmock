package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 企业经营异常名录
 */
@JsonPropertyOrder(alphabetic = true)
public class EXCEPTIONLIST implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;//  企业名称
    @JsonProperty(value = "INDATE")
    private String INDATE;//  列入日期
    @JsonProperty(value = "INREASON")
    private String INREASON;//  列入原因
    @JsonProperty(value = "OUTDATE")
    private String OUTDATE;//  移出日期
    @JsonProperty(value = "OUTREASON")
    private String OUTREASON;//  退出异常名录原因
    @JsonProperty(value = "REGNO")
    private String REGNO;//  注册号
    @JsonProperty(value = "SHXYDM")
    private String SHXYDM;//  统一社会信用代码
    @JsonProperty(value = "YC_REGORG")
    private String YC_REGORG;//  移出机关名称
    @JsonProperty(value = "YR_REGORG")
    private String YR_REGORG;//  列入机关名称

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getINDATE() {
        return INDATE;
    }

    @JsonIgnore
    public void setINDATE(String INDATE) {
        this.INDATE = INDATE;
    }

    @JsonIgnore
    public String getINREASON() {
        return INREASON;
    }

    @JsonIgnore
    public void setINREASON(String INREASON) {
        this.INREASON = INREASON;
    }

    @JsonIgnore
    public String getOUTDATE() {
        return OUTDATE;
    }

    @JsonIgnore
    public void setOUTDATE(String OUTDATE) {
        this.OUTDATE = OUTDATE;
    }

    @JsonIgnore
    public String getOUTREASON() {
        return OUTREASON;
    }

    @JsonIgnore
    public void setOUTREASON(String OUTREASON) {
        this.OUTREASON = OUTREASON;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @JsonIgnore
    public String getSHXYDM() {
        return SHXYDM;
    }

    @JsonIgnore
    public void setSHXYDM(String SHXYDM) {
        this.SHXYDM = SHXYDM;
    }

    @JsonIgnore
    public String getYC_REGORG() {
        return YC_REGORG;
    }

    @JsonIgnore
    public void setYC_REGORG(String YC_REGORG) {
        this.YC_REGORG = YC_REGORG;
    }

    @JsonIgnore
    public String getYR_REGORG() {
        return YR_REGORG;
    }

    @JsonIgnore
    public void setYR_REGORG(String YR_REGORG) {
        this.YR_REGORG = YR_REGORG;
    }

    @Override
    public String toString() {
        return "EXCEPTIONLIST{" +
                "ENTNAME='" + ENTNAME + '\'' +
                ", INDATE='" + INDATE + '\'' +
                ", INREASON='" + INREASON + '\'' +
                ", OUTDATE='" + OUTDATE + '\'' +
                ", OUTREASON='" + OUTREASON + '\'' +
                ", REGNO='" + REGNO + '\'' +
                ", SHXYDM='" + SHXYDM + '\'' +
                ", YC_REGORG='" + YC_REGORG + '\'' +
                ", YR_REGORG='" + YR_REGORG + '\'' +
                '}';
    }
}
