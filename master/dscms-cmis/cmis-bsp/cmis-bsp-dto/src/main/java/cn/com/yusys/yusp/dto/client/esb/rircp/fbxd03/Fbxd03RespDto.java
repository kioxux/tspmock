package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd03;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：从风控获取客户详细信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd03RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ols_tran_no")
    private String ols_tran_no; // 交易流水号
    @JsonProperty(value = "ols_date")
    private String ols_date; // 交易日期
    @JsonProperty(value = "sex")
    private String sex;//性別
    @JsonProperty(value = "birthday")
    private String birthday;//出生日期
    @JsonProperty(value = "ifwedd")
    private String ifwedd;//婚姻状况
    @JsonProperty(value = "ifeduc")
    private String ifeduc;//学历
    @JsonProperty(value = "addr")
    private String addr;//联系地址
    @JsonProperty(value = "zpcd")
    private String zpcd;//联系地址邮政编码
    @JsonProperty(value = "indiv_occ")
    private String indiv_occ;//职业
    @JsonProperty(value = "indiv_com_name")
    private String indiv_com_name;//工作单位
    @JsonProperty(value = "indiv_com_job_ttl")
    private String indiv_com_job_ttl;//职务
    @JsonProperty(value = "siaddr")
    private String siaddr;//现居住地
    @JsonProperty(value = "sizpcd")
    private String sizpcd;//现居住地邮政编码
    @JsonProperty(value = "live_status")
    private String live_status;//居住状态
    @JsonProperty(value = "agri_flg")
    private String agri_flg;//是否农户
    @JsonProperty(value = "ifmobl")
    private String ifmobl;//手机号
    @JsonProperty(value = "top_degree")
    private String top_degree;//最高学位
    @JsonProperty(value = "cust_type")
    private String cust_type;//客户类型
    @JsonProperty(value = "id_due_date")
    private String id_due_date;//证件有效期
    @JsonProperty(value = "std_zx_employmet")
    private String std_zx_employmet;//行业类别
    @JsonProperty(value = "indiv_ann_incm")
    private String indiv_ann_incm;//个人年收入
    @JsonProperty(value = "indiv_com_fld_name")
    private String indiv_com_fld_name;//单位所属行业名称
    @JsonProperty(value = "indiv_com_addr")
    private String indiv_com_addr;//单位地址
    @JsonProperty(value = "indiv_com_phn")
    private String indiv_com_phn;//单位电话

    public String getOls_tran_no() {
        return ols_tran_no;
    }

    public void setOls_tran_no(String ols_tran_no) {
        this.ols_tran_no = ols_tran_no;
    }

    public String getOls_date() {
        return ols_date;
    }

    public void setOls_date(String ols_date) {
        this.ols_date = ols_date;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getIfwedd() {
        return ifwedd;
    }

    public void setIfwedd(String ifwedd) {
        this.ifwedd = ifwedd;
    }

    public String getIfeduc() {
        return ifeduc;
    }

    public void setIfeduc(String ifeduc) {
        this.ifeduc = ifeduc;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getZpcd() {
        return zpcd;
    }

    public void setZpcd(String zpcd) {
        this.zpcd = zpcd;
    }

    public String getIndiv_occ() {
        return indiv_occ;
    }

    public void setIndiv_occ(String indiv_occ) {
        this.indiv_occ = indiv_occ;
    }

    public String getIndiv_com_name() {
        return indiv_com_name;
    }

    public void setIndiv_com_name(String indiv_com_name) {
        this.indiv_com_name = indiv_com_name;
    }

    public String getIndiv_com_job_ttl() {
        return indiv_com_job_ttl;
    }

    public void setIndiv_com_job_ttl(String indiv_com_job_ttl) {
        this.indiv_com_job_ttl = indiv_com_job_ttl;
    }

    public String getSiaddr() {
        return siaddr;
    }

    public void setSiaddr(String siaddr) {
        this.siaddr = siaddr;
    }

    public String getSizpcd() {
        return sizpcd;
    }

    public void setSizpcd(String sizpcd) {
        this.sizpcd = sizpcd;
    }

    public String getLive_status() {
        return live_status;
    }

    public void setLive_status(String live_status) {
        this.live_status = live_status;
    }

    public String getAgri_flg() {
        return agri_flg;
    }

    public void setAgri_flg(String agri_flg) {
        this.agri_flg = agri_flg;
    }

    public String getIfmobl() {
        return ifmobl;
    }

    public void setIfmobl(String ifmobl) {
        this.ifmobl = ifmobl;
    }

    public String getTop_degree() {
        return top_degree;
    }

    public void setTop_degree(String top_degree) {
        this.top_degree = top_degree;
    }

    public String getCust_type() {
        return cust_type;
    }

    public void setCust_type(String cust_type) {
        this.cust_type = cust_type;
    }

    public String getId_due_date() {
        return id_due_date;
    }

    public void setId_due_date(String id_due_date) {
        this.id_due_date = id_due_date;
    }

    public String getStd_zx_employmet() {
        return std_zx_employmet;
    }

    public void setStd_zx_employmet(String std_zx_employmet) {
        this.std_zx_employmet = std_zx_employmet;
    }

    public String getIndiv_ann_incm() {
        return indiv_ann_incm;
    }

    public void setIndiv_ann_incm(String indiv_ann_incm) {
        this.indiv_ann_incm = indiv_ann_incm;
    }

    public String getIndiv_com_fld_name() {
        return indiv_com_fld_name;
    }

    public void setIndiv_com_fld_name(String indiv_com_fld_name) {
        this.indiv_com_fld_name = indiv_com_fld_name;
    }

    public String getIndiv_com_addr() {
        return indiv_com_addr;
    }

    public void setIndiv_com_addr(String indiv_com_addr) {
        this.indiv_com_addr = indiv_com_addr;
    }

    public String getIndiv_com_phn() {
        return indiv_com_phn;
    }

    public void setIndiv_com_phn(String indiv_com_phn) {
        this.indiv_com_phn = indiv_com_phn;
    }

    @Override
    public String toString() {
        return "Fbxd03RespDto{" +
                "ols_tran_no='" + ols_tran_no + '\'' +
                ", ols_date='" + ols_date + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", ifwedd='" + ifwedd + '\'' +
                ", ifeduc='" + ifeduc + '\'' +
                ", addr='" + addr + '\'' +
                ", zpcd='" + zpcd + '\'' +
                ", indiv_occ='" + indiv_occ + '\'' +
                ", indiv_com_name='" + indiv_com_name + '\'' +
                ", indiv_com_job_ttl='" + indiv_com_job_ttl + '\'' +
                ", siaddr='" + siaddr + '\'' +
                ", sizpcd='" + sizpcd + '\'' +
                ", live_status='" + live_status + '\'' +
                ", agri_flg='" + agri_flg + '\'' +
                ", ifmobl='" + ifmobl + '\'' +
                ", top_degree='" + top_degree + '\'' +
                ", cust_type='" + cust_type + '\'' +
                ", id_due_date='" + id_due_date + '\'' +
                ", std_zx_employmet='" + std_zx_employmet + '\'' +
                ", indiv_ann_incm='" + indiv_ann_incm + '\'' +
                ", indiv_com_fld_name='" + indiv_com_fld_name + '\'' +
                ", indiv_com_addr='" + indiv_com_addr + '\'' +
                ", indiv_com_phn='" + indiv_com_phn + '\'' +
                '}';
    }
}