package cn.com.yusys.yusp.dto.client.esb.core.ln3020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：账户质押信息
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkzhzy implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "xuhaoooo")
    private Integer xuhaoooo;//序号
    @JsonProperty(value = "shfbhzhh")
    private String shfbhzhh;//是否本行账号
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "khzhhzxh")
    private String khzhhzxh;//客户账号子序号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "xitongzh")
    private String xitongzh;//系统账号
    @JsonProperty(value = "zhjzhyfs")
    private String zhjzhyfs;//直接质押方式
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "zhiyajee")
    private String zhiyajee;//质押金额
    @JsonProperty(value = "djiebhao")
    private String djiebhao;//冻结编号
    @JsonProperty(value = "guanlzht")
    private String guanlzht;//关联状态
    @JsonProperty(value = "zyzhleix")
    private String zyzhleix;//质押账户类型
    @JsonProperty(value = "dzywbhao")
    private String dzywbhao;//抵质押物编号
    @JsonProperty(value = "zhydaoqr")
    private String zhydaoqr;//质押到期日
    @JsonProperty(value = "zjzrzhao")
    private String zjzrzhao;//资金转入账号
    @JsonProperty(value = "zjzrzzxh")
    private String zjzrzzxh;//资金转入账号子序号

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getShfbhzhh() {
        return shfbhzhh;
    }

    public void setShfbhzhh(String shfbhzhh) {
        this.shfbhzhh = shfbhzhh;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getXitongzh() {
        return xitongzh;
    }

    public void setXitongzh(String xitongzh) {
        this.xitongzh = xitongzh;
    }

    public String getZhjzhyfs() {
        return zhjzhyfs;
    }

    public void setZhjzhyfs(String zhjzhyfs) {
        this.zhjzhyfs = zhjzhyfs;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getZhiyajee() {
        return zhiyajee;
    }

    public void setZhiyajee(String zhiyajee) {
        this.zhiyajee = zhiyajee;
    }

    public String getDjiebhao() {
        return djiebhao;
    }

    public void setDjiebhao(String djiebhao) {
        this.djiebhao = djiebhao;
    }

    public String getGuanlzht() {
        return guanlzht;
    }

    public void setGuanlzht(String guanlzht) {
        this.guanlzht = guanlzht;
    }

    public String getZyzhleix() {
        return zyzhleix;
    }

    public void setZyzhleix(String zyzhleix) {
        this.zyzhleix = zyzhleix;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getZhydaoqr() {
        return zhydaoqr;
    }

    public void setZhydaoqr(String zhydaoqr) {
        this.zhydaoqr = zhydaoqr;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    @Override
    public String toString() {
        return "Lstdkzhzy{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", xuhaoooo='" + xuhaoooo + '\'' +
                ", shfbhzhh='" + shfbhzhh + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", khzhhzxh='" + khzhhzxh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", xitongzh='" + xitongzh + '\'' +
                ", zhjzhyfs='" + zhjzhyfs + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", zhiyajee='" + zhiyajee + '\'' +
                ", djiebhao='" + djiebhao + '\'' +
                ", guanlzht='" + guanlzht + '\'' +
                ", zyzhleix='" + zyzhleix + '\'' +
                ", dzywbhao='" + dzywbhao + '\'' +
                ", zhydaoqr='" + zhydaoqr + '\'' +
                ", zjzrzhao='" + zjzrzhao + '\'' +
                ", zjzrzzxh='" + zjzrzzxh + '\'' +
                '}';
    }
}