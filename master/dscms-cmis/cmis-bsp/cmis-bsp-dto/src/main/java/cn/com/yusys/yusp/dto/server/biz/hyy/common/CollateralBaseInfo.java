package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
@JsonPropertyOrder(alphabetic = true)
public class CollateralBaseInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "collateral_id")
    private String collateral_id;//抵押唯一ID
    @JsonProperty(value = "collateral_no")
    private String collateral_no;//抵押物编号
    @JsonProperty(value = "mortgagor_name")
    private String mortgagor_name;// 抵押人名称
    @JsonProperty(value = "mortgagor_client_code")
    private String mortgagor_client_code;// 抵押人客户码
    @JsonProperty(value = "mortgagor_document_number")
    private String mortgagor_document_number;// 抵押人证件号码
    @JsonProperty(value = "collateral_value")
    private String collateral_value;// 抵押物价值
    @JsonProperty(value = "collateral_type")
    private String collateral_type;//抵押物类型
    @JsonProperty(value = "collateral_location")
    private String collateral_location;//抵押物坐落
    @JsonProperty(value = "collateral_area")
    private String collateral_area;//抵押物面积


    public String getCollateral_id() {
        return collateral_id;
    }

    public void setCollateral_id(String collateral_id) {
        this.collateral_id = collateral_id;
    }

    public String getCollateral_no() {
        return collateral_no;
    }

    public void setCollateral_no(String collateral_no) {
        this.collateral_no = collateral_no;
    }

    public String getMortgagor_name() {
        return mortgagor_name;
    }

    public void setMortgagor_name(String mortgagor_name) {
        this.mortgagor_name = mortgagor_name;
    }

    public String getMortgagor_client_code() {
        return mortgagor_client_code;
    }

    public void setMortgagor_client_code(String mortgagor_client_code) {
        this.mortgagor_client_code = mortgagor_client_code;
    }

    public String getMortgagor_document_number() {
        return mortgagor_document_number;
    }

    public void setMortgagor_document_number(String mortgagor_document_number) {
        this.mortgagor_document_number = mortgagor_document_number;
    }

    public String getCollateral_value() {
        return collateral_value;
    }

    public void setCollateral_value(String collateral_value) {
        this.collateral_value = collateral_value;
    }

    public String getCollateral_type() {
        return collateral_type;
    }

    public void setCollateral_type(String collateral_type) {
        this.collateral_type = collateral_type;
    }

    public String getCollateral_location() {
        return collateral_location;
    }

    public void setCollateral_location(String collateral_location) {
        this.collateral_location = collateral_location;
    }

    public String getCollateral_area() {
        return collateral_area;
    }

    public void setCollateral_area(String collateral_area) {
        this.collateral_area = collateral_area;
    }

    @Override
    public String toString() {
        return "CollateralBaseInfo{" +
                "collateral_id='" + collateral_id + '\'' +
                ", collateral_no='" + collateral_no + '\'' +
                ", mortgagor_name='" + mortgagor_name + '\'' +
                ", mortgagor_client_code='" + mortgagor_client_code + '\'' +
                ", mortgagor_document_number='" + mortgagor_document_number + '\'' +
                ", collateral_value='" + collateral_value + '\'' +
                ", collateral_type='" + collateral_type + '\'' +
                ", collateral_location='" + collateral_location + '\'' +
                ", collateral_area='" + collateral_area + '\'' +
                '}';
    }
}
