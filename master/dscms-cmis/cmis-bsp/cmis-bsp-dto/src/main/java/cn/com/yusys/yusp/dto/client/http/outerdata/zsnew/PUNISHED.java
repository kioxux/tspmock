package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	被执行人
@JsonPropertyOrder(alphabetic = true)
public class PUNISHED implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "AGECLEAN")
    private String AGECLEAN;//	年龄
    @JsonProperty(value = "AREANAMECLEAN")
    private String AREANAMECLEAN;//	省份
    @JsonProperty(value = "CARDNUMCLEAN")
    private String CARDNUMCLEAN;//	身份证号码/企业注册号
    @JsonProperty(value = "CASECODE")
    private String CASECODE;//	案号
    @JsonProperty(value = "CASESTATE")
    private String CASESTATE;//	案件状态
    @JsonProperty(value = "COURTNAME")
    private String COURTNAME;//	执行法院
    @JsonProperty(value = "EXECMONEY")
    private String EXECMONEY;//	执行标的（元）
    @JsonProperty(value = "INAMECLEAN")
    private String INAMECLEAN;//	被执行人姓名/名称
    @JsonProperty(value = "REGDATECLEAN")
    private String REGDATECLEAN;//	立案时间
    @JsonProperty(value = "SEXYCLEAN")
    private String SEXYCLEAN;//	性别
    @JsonProperty(value = "TYPE")
    private String TYPE;//	自然人、法人或其他组织
    @JsonProperty(value = "YSFZD")
    private String YSFZD;//	身份证原始发证地

    @JsonIgnore
    public String getAGECLEAN() {
        return AGECLEAN;
    }

    @JsonIgnore
    public void setAGECLEAN(String AGECLEAN) {
        this.AGECLEAN = AGECLEAN;
    }

    @JsonIgnore
    public String getAREANAMECLEAN() {
        return AREANAMECLEAN;
    }

    @JsonIgnore
    public void setAREANAMECLEAN(String AREANAMECLEAN) {
        this.AREANAMECLEAN = AREANAMECLEAN;
    }

    @JsonIgnore
    public String getCARDNUMCLEAN() {
        return CARDNUMCLEAN;
    }

    @JsonIgnore
    public void setCARDNUMCLEAN(String CARDNUMCLEAN) {
        this.CARDNUMCLEAN = CARDNUMCLEAN;
    }

    @JsonIgnore
    public String getCASECODE() {
        return CASECODE;
    }

    @JsonIgnore
    public void setCASECODE(String CASECODE) {
        this.CASECODE = CASECODE;
    }

    @JsonIgnore
    public String getCASESTATE() {
        return CASESTATE;
    }

    @JsonIgnore
    public void setCASESTATE(String CASESTATE) {
        this.CASESTATE = CASESTATE;
    }

    @JsonIgnore
    public String getCOURTNAME() {
        return COURTNAME;
    }

    @JsonIgnore
    public void setCOURTNAME(String COURTNAME) {
        this.COURTNAME = COURTNAME;
    }

    @JsonIgnore
    public String getEXECMONEY() {
        return EXECMONEY;
    }

    @JsonIgnore
    public void setEXECMONEY(String EXECMONEY) {
        this.EXECMONEY = EXECMONEY;
    }

    @JsonIgnore
    public String getINAMECLEAN() {
        return INAMECLEAN;
    }

    @JsonIgnore
    public void setINAMECLEAN(String INAMECLEAN) {
        this.INAMECLEAN = INAMECLEAN;
    }

    @JsonIgnore
    public String getREGDATECLEAN() {
        return REGDATECLEAN;
    }

    @JsonIgnore
    public void setREGDATECLEAN(String REGDATECLEAN) {
        this.REGDATECLEAN = REGDATECLEAN;
    }

    @JsonIgnore
    public String getSEXYCLEAN() {
        return SEXYCLEAN;
    }

    @JsonIgnore
    public void setSEXYCLEAN(String SEXYCLEAN) {
        this.SEXYCLEAN = SEXYCLEAN;
    }

    @JsonIgnore
    public String getTYPE() {
        return TYPE;
    }

    @JsonIgnore
    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    @JsonIgnore
    public String getYSFZD() {
        return YSFZD;
    }

    @JsonIgnore
    public void setYSFZD(String YSFZD) {
        this.YSFZD = YSFZD;
    }

    @Override
    public String toString() {
        return "PUNISHED{" +
                "AGECLEAN='" + AGECLEAN + '\'' +
                ", AREANAMECLEAN='" + AREANAMECLEAN + '\'' +
                ", CARDNUMCLEAN='" + CARDNUMCLEAN + '\'' +
                ", CASECODE='" + CASECODE + '\'' +
                ", CASESTATE='" + CASESTATE + '\'' +
                ", COURTNAME='" + COURTNAME + '\'' +
                ", EXECMONEY='" + EXECMONEY + '\'' +
                ", INAMECLEAN='" + INAMECLEAN + '\'' +
                ", REGDATECLEAN='" + REGDATECLEAN + '\'' +
                ", SEXYCLEAN='" + SEXYCLEAN + '\'' +
                ", TYPE='" + TYPE + '\'' +
                ", YSFZD='" + YSFZD + '\'' +
                '}';
    }
}
