package cn.com.yusys.yusp.dto.server.cfg.xdxt0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/25 9:15:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 9:15
 * @since 2021/5/25 9:15
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "branchNo")
    private String branchNo;//分部编号

    public String getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(String branchNo) {
        this.branchNo = branchNo;
    }

    @Override
    public String toString() {
        return "Xdxt0005RespDto{" +
                "branchNo='" + branchNo + '\'' +
                '}';
    }
}
