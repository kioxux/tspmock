package cn.com.yusys.yusp.dto.server.biz.xddb0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 19:09
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarNo")
    private String guarNo;//抵押物编号
    @JsonProperty(value = "imagePk1SerNo")
    private String imagePk1SerNo;//影像主键流水号
    @JsonProperty(value = "tranType")
    private String tranType;//交易类型

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getImagePk1SerNo() {
        return imagePk1SerNo;
    }

    public void setImagePk1SerNo(String imagePk1SerNo) {
        this.imagePk1SerNo = imagePk1SerNo;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "guarNo='" + guarNo + '\'' +
                ", imagePk1SerNo='" + imagePk1SerNo + '\'' +
                ", tranType='" + tranType + '\'' +
                '}';
    }
}
