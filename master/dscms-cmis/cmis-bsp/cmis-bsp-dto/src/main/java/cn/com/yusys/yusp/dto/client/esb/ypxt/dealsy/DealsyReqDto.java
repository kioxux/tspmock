package cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：押品处置信息同步
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class DealsyReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "yptybh")
    private String yptybh;//押品统一编号
    @JsonProperty(value = "ypczfs")
    private String ypczfs;//押品处置方式
    @JsonProperty(value = "xjhslx")
    private String xjhslx;//现金回收来源类型
    @JsonProperty(value = "xjhsbh")
    private String xjhsbh;//现金回收来源编号
    @JsonProperty(value = "dzzcbh")
    private String dzzcbh;//抵债资产编号
    @JsonProperty(value = "dzzclx")
    private String dzzclx;//抵债资产类型
    @JsonProperty(value = "yzdzje")
    private BigDecimal yzdzje;//以资抵债金额
    @JsonProperty(value = "czhsze")
    private BigDecimal czhsze;//处置回收金额总额
    @JsonProperty(value = "czhslx")
    private String czhslx;//处置回收金额类型
    @JsonProperty(value = "czhsje")
    private BigDecimal czhsje;//处置回收金额
    @JsonProperty(value = "isflag")
    private String isflag;//是否处置完毕
    @JsonProperty(value = "zjfyze")
    private BigDecimal zjfyze;//处置直接费用总额
    @JsonProperty(value = "zjfylx")
    private String zjfylx;//处置直接费用类型
    @JsonProperty(value = "zjfyje")
    private BigDecimal zjfyje;//处置直接费用金额
    @JsonProperty(value = "czrqyp")
    private String czrqyp;//处置日期
    @JsonProperty(value = "remark")
    private String remark;//备注
    @JsonProperty(value = "jbrbhy")
    private String jbrbhy;//经办人
    @JsonProperty(value = "jbjgyp")
    private String jbjgyp;//经办机构
    @JsonProperty(value = "jbrqyp")
    private String jbrqyp;//经办日期
    @JsonProperty(value = "operat")
    private String operat;//操作
    @JsonProperty(value = "sernum")
    private String sernum;

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getYpczfs() {
        return ypczfs;
    }

    public void setYpczfs(String ypczfs) {
        this.ypczfs = ypczfs;
    }

    public String getXjhslx() {
        return xjhslx;
    }

    public void setXjhslx(String xjhslx) {
        this.xjhslx = xjhslx;
    }

    public String getXjhsbh() {
        return xjhsbh;
    }

    public void setXjhsbh(String xjhsbh) {
        this.xjhsbh = xjhsbh;
    }

    public String getDzzcbh() {
        return dzzcbh;
    }

    public void setDzzcbh(String dzzcbh) {
        this.dzzcbh = dzzcbh;
    }

    public String getDzzclx() {
        return dzzclx;
    }

    public void setDzzclx(String dzzclx) {
        this.dzzclx = dzzclx;
    }

    public BigDecimal getYzdzje() {
        return yzdzje;
    }

    public void setYzdzje(BigDecimal yzdzje) {
        this.yzdzje = yzdzje;
    }

    public BigDecimal getCzhsze() {
        return czhsze;
    }

    public void setCzhsze(BigDecimal czhsze) {
        this.czhsze = czhsze;
    }

    public String getCzhslx() {
        return czhslx;
    }

    public void setCzhslx(String czhslx) {
        this.czhslx = czhslx;
    }

    public BigDecimal getCzhsje() {
        return czhsje;
    }

    public void setCzhsje(BigDecimal czhsje) {
        this.czhsje = czhsje;
    }

    public String getIsflag() {
        return isflag;
    }

    public void setIsflag(String isflag) {
        this.isflag = isflag;
    }

    public BigDecimal getZjfyze() {
        return zjfyze;
    }

    public void setZjfyze(BigDecimal zjfyze) {
        this.zjfyze = zjfyze;
    }

    public String getZjfylx() {
        return zjfylx;
    }

    public void setZjfylx(String zjfylx) {
        this.zjfylx = zjfylx;
    }

    public BigDecimal getZjfyje() {
        return zjfyje;
    }

    public void setZjfyje(BigDecimal zjfyje) {
        this.zjfyje = zjfyje;
    }

    public String getCzrqyp() {
        return czrqyp;
    }

    public void setCzrqyp(String czrqyp) {
        this.czrqyp = czrqyp;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getJbrbhy() {
        return jbrbhy;
    }

    public void setJbrbhy(String jbrbhy) {
        this.jbrbhy = jbrbhy;
    }

    public String getJbjgyp() {
        return jbjgyp;
    }

    public void setJbjgyp(String jbjgyp) {
        this.jbjgyp = jbjgyp;
    }

    public String getJbrqyp() {
        return jbrqyp;
    }

    public void setJbrqyp(String jbrqyp) {
        this.jbrqyp = jbrqyp;
    }

    public String getOperat() {
        return operat;
    }

    public void setOperat(String operat) {
        this.operat = operat;
    }

    public String getSernum() {
        return sernum;
    }

    public void setSernum(String sernum) {
        this.sernum = sernum;
    }

    @Override
    public String toString() {
        return "DealsyReqDto{" +
                "yptybh='" + yptybh + '\'' +
                ", ypczfs='" + ypczfs + '\'' +
                ", xjhslx='" + xjhslx + '\'' +
                ", xjhsbh='" + xjhsbh + '\'' +
                ", dzzcbh='" + dzzcbh + '\'' +
                ", dzzclx='" + dzzclx + '\'' +
                ", yzdzje=" + yzdzje +
                ", czhsze=" + czhsze +
                ", czhslx='" + czhslx + '\'' +
                ", czhsje=" + czhsje +
                ", isflag='" + isflag + '\'' +
                ", zjfyze=" + zjfyze +
                ", zjfylx='" + zjfylx + '\'' +
                ", zjfyje=" + zjfyje +
                ", czrqyp='" + czrqyp + '\'' +
                ", remark='" + remark + '\'' +
                ", jbrbhy='" + jbrbhy + '\'' +
                ", jbjgyp='" + jbjgyp + '\'' +
                ", jbrqyp='" + jbrqyp + '\'' +
                ", operat='" + operat + '\'' +
                ", sernum='" + sernum + '\'' +
                '}';
    }
}
