package cn.com.yusys.yusp.dto.client.esb.irs.irs21;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：单一客户限额测算信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs21ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sxserialno")
    private String sxserialno;//授信申请流水号
    @JsonProperty(value = "custid")
    private String custid;//    客户编号
    @JsonProperty(value = "custname")
    private String custname;//	客户名称
    @JsonProperty(value = "extdebt")
    private String extdebt;//	客户对外负债
    @JsonProperty(value = "otherdebt")
    private String otherdebt;//	现有他行负债
    @JsonProperty(value = "guarsum")
    private String guarsum;//	对外抵押金额
    @JsonProperty(value = "impsum")
    private String impsum;//	对外质押金额
    @JsonProperty(value = "cashsum")
    private String cashsum;//	对外保证金额

    public String getSxserialno() {
        return sxserialno;
    }

    public void setSxserialno(String sxserialno) {
        this.sxserialno = sxserialno;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getExtdebt() {
        return extdebt;
    }

    public void setExtdebt(String extdebt) {
        this.extdebt = extdebt;
    }

    public String getOtherdebt() {
        return otherdebt;
    }

    public void setOtherdebt(String otherdebt) {
        this.otherdebt = otherdebt;
    }

    public String getGuarsum() {
        return guarsum;
    }

    public void setGuarsum(String guarsum) {
        this.guarsum = guarsum;
    }

    public String getImpsum() {
        return impsum;
    }

    public void setImpsum(String impsum) {
        this.impsum = impsum;
    }

    public String getCashsum() {
        return cashsum;
    }

    public void setCashsum(String cashsum) {
        this.cashsum = cashsum;
    }

    @Override
    public String toString() {
        return "Irs21ReqDto{" +
                "sxserialno='" + sxserialno + '\'' +
                ", custid='" + custid + '\'' +
                ", custname='" + custname + '\'' +
                ", extdebt='" + extdebt + '\'' +
                ", otherdebt='" + otherdebt + '\'' +
                ", guarsum='" + guarsum + '\'' +
                ", impsum='" + impsum + '\'' +
                ", cashsum='" + cashsum + '\'' +
                '}';
    }
}
