package cn.com.yusys.yusp.dto.client.esb.core.ln3235;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：还款方式转换
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3235RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lsdkhbjh")
    private java.util.List<Lsdkhbjh> lsdkhbjh;//贷款还本计划[LIST]
    @JsonProperty(value = "lsdkbjfd")
    private java.util.List<Lsdkbjfd> lsdkbjfd;//本金分段登记[LIST]
    @JsonProperty(value = "qxbgtzjh")
    private String qxbgtzjh;//期限变更调整计划
    @JsonProperty(value = "benjinfd")
    private String benjinfd;//本金分段
    @JsonProperty(value = "dzhhkjih")
    private String dzhhkjih;//定制还款计划
    @JsonProperty(value = "llbgtzjh")
    private String llbgtzjh;//利率变更调整计划
    @JsonProperty(value = "dcfktzjh")
    private String dcfktzjh;//多次放款调整计划
    @JsonProperty(value = "tqhktzjh")
    private String tqhktzjh;//提前还款调整计划
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "scihkrbz")
    private String scihkrbz;//首次还款日模式
    @JsonProperty(value = "yunxtqhk")
    private String yunxtqhk;//允许提前还款

    public List<Lsdkhbjh> getLsdkhbjh() {
        return lsdkhbjh;
    }

    public void setLsdkhbjh(List<Lsdkhbjh> lsdkhbjh) {
        this.lsdkhbjh = lsdkhbjh;
    }

    public List<Lsdkbjfd> getLsdkbjfd() {
        return lsdkbjfd;
    }

    public void setLsdkbjfd(List<Lsdkbjfd> lsdkbjfd) {
        this.lsdkbjfd = lsdkbjfd;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getBenjinfd() {
        return benjinfd;
    }

    public void setBenjinfd(String benjinfd) {
        this.benjinfd = benjinfd;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    @Override
    public String toString() {
        return "Ln3235RespDto{" +
                "lsdkhbjh=" + lsdkhbjh +
                ", lsdkbjfd=" + lsdkbjfd +
                ", qxbgtzjh='" + qxbgtzjh + '\'' +
                ", benjinfd='" + benjinfd + '\'' +
                ", dzhhkjih='" + dzhhkjih + '\'' +
                ", llbgtzjh='" + llbgtzjh + '\'' +
                ", dcfktzjh='" + dcfktzjh + '\'' +
                ", tqhktzjh='" + tqhktzjh + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", scihkrbz='" + scihkrbz + '\'' +
                ", yunxtqhk='" + yunxtqhk + '\'' +
                '}';
    }
}
