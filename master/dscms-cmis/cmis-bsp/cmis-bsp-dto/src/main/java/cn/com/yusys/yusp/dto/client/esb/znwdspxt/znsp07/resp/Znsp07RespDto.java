package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：数据修改接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp07RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Znsp07RespDto{" +
                '}';
    }
}  
