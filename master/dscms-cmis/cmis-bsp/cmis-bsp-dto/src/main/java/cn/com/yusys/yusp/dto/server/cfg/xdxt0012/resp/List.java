package cn.com.yusys.yusp.dto.server.cfg.xdxt0012.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/25 9:50:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 9:50
 * @since 2021/5/25 9:50
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "enname")
    private String enname;//国标地区编码字典英文名
    @JsonProperty(value = "cnname")
    private String cnname;//国标地区编码字典中文名

    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    public String getCnname() {
        return cnname;
    }

    public void setCnname(String cnname) {
        this.cnname = cnname;
    }

    @Override
    public String toString() {
        return "Xdxt0012RespDto{" +
                "enname='" + enname + '\'' +
                "cnname='" + cnname + '\'' +
                '}';
    }
}
