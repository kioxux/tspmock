package cn.com.yusys.yusp.dto.server.biz.xdxw0032.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询信贷系统的审批历史信息
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pkValue")
    private String pkValue;//客户号

    public String getPkValue() {
        return pkValue;
    }

    public void setPkValue(String pkValue) {
        this.pkValue = pkValue;
    }

    @Override
    public String toString() {
        return "Data{" +
                "pkValue='" + pkValue + '\'' +
                '}';
    }
}  
