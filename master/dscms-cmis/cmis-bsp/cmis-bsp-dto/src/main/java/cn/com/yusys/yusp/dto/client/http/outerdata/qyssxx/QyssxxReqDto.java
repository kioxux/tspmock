package cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：涉诉信息查询接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class QyssxxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "type")
    private String type;//查询类型 0：查询自然人， 1：查询组织机构
    @JsonProperty(value = "name")
    private String name;//姓名/企业名称
    @JsonProperty(value = "qyid")
    private String qyid;//身份证号/组织机构代码


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQyid() {
        return qyid;
    }

    public void setQyid(String qyid) {
        this.qyid = qyid;
    }

    @Override
    public String toString() {
        return "QyssxxReqDto{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", qyid='" + qyid + '\'' +
                '}';
    }
}
