package cn.com.yusys.yusp.dto.server.lmt.cmislmt0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CmisLmt0001DataLmtSubListReqDto {

    /**
     * 授信分项编号
     */
    @JsonProperty(value = "accSubNo")
    private String accSubNo;

    /**
     * 原授信分项编号
     */
    @JsonProperty(value = "origiAccSubNo")
    private String origiAccSubNo;

    /**
     * 父节点
     */
    @JsonProperty(value = "parentId")
    private String parentId;

    /**
     * 授信品种编号
     */
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;

    /**
     * 授信品种名称
     */
    @JsonProperty(value = "limitSubName")
    private String limitSubName;

    /**
     * 授信金额
     */
    @JsonProperty(value = "avlamt")
    private BigDecimal avlamt;

    /**
     * 币种
     */
    @JsonProperty(value = "curType")
    private String curType;

    /**
     * 授信期限
     */
    @JsonProperty(value = "term")
    private Integer term;

    /**
     * 额度起始日
     */
    @JsonProperty(value = "startDate")
    private String startDate;

    /**
     * 额度到期日
     */
    @JsonProperty(value = "endDate")
    private String endDate;

    /**
     * 批复保证金比例
     */
    @JsonProperty(value = "bailPreRate")
    private BigDecimal bailPreRate;

    /**
     * 年利率
     */
    @JsonProperty(value = "rateYear")
    private BigDecimal rateYear;

    /**
     * 担保方式
     */
    @JsonProperty(value = "suitGuarWay")
    private String suitGuarWay;

    /**
     * 宽限期
     */
    @JsonProperty(value = "lmtGraper")
    private Integer lmtGraper;

    /**
     * 是否可循环
     */
    @JsonProperty(value = "isRevolv")
    private String isRevolv;

    /**
     * 是否预授信
     */
    @JsonProperty(value = "isPreCrd")
    private String isPreCrd;

    /**
     * 是否低风险
     */
    @JsonProperty(value = "isLriskLmt")
    private String isLriskLmt;

    /**
     * 批复分项状态
     */
    @JsonProperty(value = "accSubStatus")
    private String accSubStatus;

    /**
     * 操作类型
     */
    @JsonProperty(value = "oprType")
    private String oprType;

    /**
     * 操作类型
     */
    @JsonProperty(value = "lmtSubType")
    private String lmtSubType ;


    public String getAccSubNo() {
        return accSubNo;
    }

    public void setAccSubNo(String accSubNo) {
        this.accSubNo = accSubNo;
    }

    public String getOrigiAccSubNo() {
        return origiAccSubNo;
    }

    public void setOrigiAccSubNo(String origiAccSubNo) {
        this.origiAccSubNo = origiAccSubNo;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public BigDecimal getAvlamt() {
        return avlamt;
    }

    public void setAvlamt(BigDecimal avlamt) {
        this.avlamt = avlamt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getBailPreRate() {
        return bailPreRate;
    }

    public void setBailPreRate(BigDecimal bailPreRate) {
        this.bailPreRate = bailPreRate;
    }

    public BigDecimal getRateYear() {
        return rateYear;
    }

    public void setRateYear(BigDecimal rateYear) {
        this.rateYear = rateYear;
    }

    public String getSuitGuarWay() {
        return suitGuarWay;
    }

    public void setSuitGuarWay(String suitGuarWay) {
        this.suitGuarWay = suitGuarWay;
    }

    public Integer getLmtGraper() {
        return lmtGraper;
    }

    public void setLmtGraper(Integer lmtGraper) {
        this.lmtGraper = lmtGraper;
    }

    public String getIsRevolv() {
        return isRevolv;
    }

    public void setIsRevolv(String isRevolv) {
        this.isRevolv = isRevolv;
    }

    public String getIsPreCrd() {
        return isPreCrd;
    }

    public void setIsPreCrd(String isPreCrd) {
        this.isPreCrd = isPreCrd;
    }

    public String getIsLriskLmt() {
        return isLriskLmt;
    }

    public void setIsLriskLmt(String isLriskLmt) {
        this.isLriskLmt = isLriskLmt;
    }

    public String getAccSubStatus() {
        return accSubStatus;
    }

    public void setAccSubStatus(String accSubStatus) {
        this.accSubStatus = accSubStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getLmtSubType() {
        return lmtSubType;
    }

    public void setLmtSubType(String lmtSubType) {
        this.lmtSubType = lmtSubType;
    }

    @Override
    public String toString() {
        return "CmisLmt0001ReqBodyList{" +
                "accSubNo='" + accSubNo + '\'' +
                ", parentId='" + parentId + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", lmtSubType='" + lmtSubType + '\'' +
                ", avlamt='" + avlamt + '\'' +
                ", curType='" + curType + '\'' +
                ", term='" + term + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", bailPreRate='" + bailPreRate + '\'' +
                ", rateYear='" + rateYear + '\'' +
                ", suitGuarWay='" + suitGuarWay + '\'' +
                ", lmtGraper='" + lmtGraper + '\'' +
                ", isRevolv='" + isRevolv + '\'' +
                ", isPreCrd='" + isPreCrd + '\'' +
                ", isLriskLmt='" + isLriskLmt + '\'' +
                ", accSubStatus='" + accSubStatus + '\'' +
                '}';
    }
}
