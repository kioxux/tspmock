package cn.com.yusys.yusp.dto.server.biz.xdht0038.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "firstpayPerc")
    private BigDecimal firstpayPerc;//首付款比例

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getFirstpayPerc() {
        return firstpayPerc;
    }

    public void setFirstpayPerc(BigDecimal firstpayPerc) {
        this.firstpayPerc = firstpayPerc;
    }

    @Override
    public String toString() {
        return "List{" +
                "billNo='" + billNo + '\'' +
                ", firstpayPerc=" + firstpayPerc +
                '}';
    }
}
