package cn.com.yusys.yusp.dto.client.esb.core.ln3030;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：贷款放款计划
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkfkjh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "fangkzhl")
    private String fangkzhl;//放款种类
    @JsonProperty(value = "fkriqiii")
    private String fkriqiii;//放款日期
    @JsonProperty(value = "fkjineee")
    private BigDecimal fkjineee;//放款金额
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "dkrzhzxh")
    private String dkrzhzxh;//贷款入账账号子序号

    public String getFangkzhl() {
        return fangkzhl;
    }

    public void setFangkzhl(String fangkzhl) {
        this.fangkzhl = fangkzhl;
    }

    public String getFkriqiii() {
        return fkriqiii;
    }

    public void setFkriqiii(String fkriqiii) {
        this.fkriqiii = fkriqiii;
    }

    public BigDecimal getFkjineee() {
        return fkjineee;
    }

    public void setFkjineee(BigDecimal fkjineee) {
        this.fkjineee = fkjineee;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    @Override
    public String toString() {
        return "Lstdkfkjh{" +
                "fangkzhl='" + fangkzhl + '\'' +
                "fkriqiii='" + fkriqiii + '\'' +
                "fkjineee='" + fkjineee + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                '}';
    }
}  
