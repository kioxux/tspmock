package cn.com.yusys.yusp.dto.client.esb.core.ln3103;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款账户交易明细查询
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3103ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数
    @JsonProperty(value = "jymxpxfs")
    private String jymxpxfs;//交易明细排序方式
    @JsonProperty(value = "qishishj")
    private String qishishj;//起始时间
    @JsonProperty(value = "jieshshj")
    private String jieshshj;//结束时间
    @JsonProperty(value = "chaxfanw")
    private String chaxfanw;//查询范围标志

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getJymxpxfs() {
        return jymxpxfs;
    }

    public void setJymxpxfs(String jymxpxfs) {
        this.jymxpxfs = jymxpxfs;
    }

    public String getQishishj() {
        return qishishj;
    }

    public void setQishishj(String qishishj) {
        this.qishishj = qishishj;
    }

    public String getJieshshj() {
        return jieshshj;
    }

    public void setJieshshj(String jieshshj) {
        this.jieshshj = jieshshj;
    }

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    @Override
    public String toString() {
        return "Ln3103ReqDto{" +
                "dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", jiaoyima='" + jiaoyima + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", jymxpxfs='" + jymxpxfs + '\'' +
                ", qishishj='" + qishishj + '\'' +
                ", jieshshj='" + jieshshj + '\'' +
                ", chaxfanw='" + chaxfanw + '\'' +
                '}';
    }
}
