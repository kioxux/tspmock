package cn.com.yusys.yusp.dto.client.esb.rircp.fbyd33;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：预授信申请提交
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbyd33ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "channel_type")
    private String channel_type;//渠道来源
    @JsonProperty(value = "co_platform")
    private String co_platform;//合作平台
    @JsonProperty(value = "prd_type")
    private String prd_type;//产品类别
    @JsonProperty(value = "prd_code")
    private String prd_code;//产品代码（零售智能风控内部代码）
    @JsonProperty(value = "cust_name")
    private String cust_name;//客户姓名
    @JsonProperty(value = "cert_code")
    private String cert_code;//客户证件号码
    @JsonProperty(value = "cust_id")
    private String cust_id;//核心客户号
    @JsonProperty(value = "spouse_name")
    private String spouse_name;//配偶姓名
    @JsonProperty(value = "spouse_cert_code")
    private String spouse_cert_code;//配偶证件号码
    @JsonProperty(value = "has_tax")
    private String has_tax;//是否纳税
    @JsonProperty(value = "com_type")
    private String com_type;//企业类型
    @JsonProperty(value = "nsrsbh")
    private String nsrsbh;//纳税人识别号
    @JsonProperty(value = "nsrsmc")
    private String nsrsmc;//纳税人名称（企业名称）
    @JsonProperty(value = "uscc")
    private String uscc;//企业统一社会信用代码
    @JsonProperty(value = "grant_no")
    private String grant_no;//个人征信查询授权书编号
    @JsonProperty(value = "qy_grant_no")
    private String qy_grant_no;//企业征信查询授权书编号
    @JsonProperty(value = "phone")
    private String phone;//申请人手机号
    @JsonProperty(value = "referee_name")
    private String referee_name;//推荐人姓名
    @JsonProperty(value = "referee_no")
    private String referee_no;//推荐人工号
    @JsonProperty(value = "app_amt")
    private BigDecimal app_amt;//授信申请金额
    @JsonProperty(value = "cert_valid_end_date")
    private String cert_valid_end_date;//证件有效期
    @JsonProperty(value = "grxycxsqs_no")
    private String grxycxsqs_no;//个人信用信息服务协议电子合同编号
    @JsonProperty(value = "dkytsms_no")
    private String dkytsms_no;//贷款用途声明书电子合同编号
    @JsonProperty(value = "ssxxcxsqs_no")
    private String ssxxcxsqs_no;//涉税保密信息查询委托授权书电子合同编号
    @JsonProperty(value = "total_price")
    private BigDecimal total_price;//房产评估价
    @JsonProperty(value = "khssqy")
    private String khssqy;//地区（渠道端使用）


    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_cert_code() {
        return spouse_cert_code;
    }

    public void setSpouse_cert_code(String spouse_cert_code) {
        this.spouse_cert_code = spouse_cert_code;
    }

    public String getHas_tax() {
        return has_tax;
    }

    public void setHas_tax(String has_tax) {
        this.has_tax = has_tax;
    }

    public String getCom_type() {
        return com_type;
    }

    public void setCom_type(String com_type) {
        this.com_type = com_type;
    }

    public String getNsrsbh() {
        return nsrsbh;
    }

    public void setNsrsbh(String nsrsbh) {
        this.nsrsbh = nsrsbh;
    }

    public String getNsrsmc() {
        return nsrsmc;
    }

    public void setNsrsmc(String nsrsmc) {
        this.nsrsmc = nsrsmc;
    }

    public String getUscc() {
        return uscc;
    }

    public void setUscc(String uscc) {
        this.uscc = uscc;
    }

    public String getGrant_no() {
        return grant_no;
    }

    public void setGrant_no(String grant_no) {
        this.grant_no = grant_no;
    }

    public String getQy_grant_no() {
        return qy_grant_no;
    }

    public void setQy_grant_no(String qy_grant_no) {
        this.qy_grant_no = qy_grant_no;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReferee_name() {
        return referee_name;
    }

    public void setReferee_name(String referee_name) {
        this.referee_name = referee_name;
    }

    public String getReferee_no() {
        return referee_no;
    }

    public void setReferee_no(String referee_no) {
        this.referee_no = referee_no;
    }

    public BigDecimal getApp_amt() {
        return app_amt;
    }

    public void setApp_amt(BigDecimal app_amt) {
        this.app_amt = app_amt;
    }

    public String getCert_valid_end_date() {
        return cert_valid_end_date;
    }

    public void setCert_valid_end_date(String cert_valid_end_date) {
        this.cert_valid_end_date = cert_valid_end_date;
    }

    public String getGrxycxsqs_no() {
        return grxycxsqs_no;
    }

    public void setGrxycxsqs_no(String grxycxsqs_no) {
        this.grxycxsqs_no = grxycxsqs_no;
    }

    public String getDkytsms_no() {
        return dkytsms_no;
    }

    public void setDkytsms_no(String dkytsms_no) {
        this.dkytsms_no = dkytsms_no;
    }

    public String getSsxxcxsqs_no() {
        return ssxxcxsqs_no;
    }

    public void setSsxxcxsqs_no(String ssxxcxsqs_no) {
        this.ssxxcxsqs_no = ssxxcxsqs_no;
    }

    public BigDecimal getTotal_price() {
        return total_price;
    }

    public void setTotal_price(BigDecimal total_price) {
        this.total_price = total_price;
    }

    public String getKhssqy() {
        return khssqy;
    }

    public void setKhssqy(String khssqy) {
        this.khssqy = khssqy;
    }

    @Override
    public String toString() {
        return "Fbyd33ReqDto{" +
                "channel_type='" + channel_type + '\'' +
                ", co_platform='" + co_platform + '\'' +
                ", prd_type='" + prd_type + '\'' +
                ", prd_code='" + prd_code + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cust_id='" + cust_id + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", spouse_cert_code='" + spouse_cert_code + '\'' +
                ", has_tax='" + has_tax + '\'' +
                ", com_type='" + com_type + '\'' +
                ", nsrsbh='" + nsrsbh + '\'' +
                ", nsrsmc='" + nsrsmc + '\'' +
                ", uscc='" + uscc + '\'' +
                ", grant_no='" + grant_no + '\'' +
                ", qy_grant_no='" + qy_grant_no + '\'' +
                ", phone='" + phone + '\'' +
                ", referee_name='" + referee_name + '\'' +
                ", referee_no='" + referee_no + '\'' +
                ", app_amt=" + app_amt +
                ", cert_valid_end_date='" + cert_valid_end_date + '\'' +
                ", grxycxsqs_no='" + grxycxsqs_no + '\'' +
                ", dkytsms_no='" + dkytsms_no + '\'' +
                ", ssxxcxsqs_no='" + ssxxcxsqs_no + '\'' +
                ", total_price=" + total_price +
                ", khssqy='" + khssqy + '\'' +
                '}';
    }
}  
