package cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：连云港存量房列表
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CljctzRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "gapssq")
    private String gapssq;//gaps流水号
    @JsonProperty(value = "hstrsq ")
    private String hstrsq;//核心流水号（记账流水号）

    public String getGapssq() {
        return gapssq;
    }

    public void setGapssq(String gapssq) {
        this.gapssq = gapssq;
    }

    public String getHstrsq() {
        return hstrsq;
    }

    public void setHstrsq(String hstrsq) {
        this.hstrsq = hstrsq;
    }

    @Override
    public String toString() {
        return "CljctzRespDto{" +
                "gapssq='" + gapssq + '\'' +
                "hstrsq ='" + hstrsq + '\'' +
                '}';
    }
}  
