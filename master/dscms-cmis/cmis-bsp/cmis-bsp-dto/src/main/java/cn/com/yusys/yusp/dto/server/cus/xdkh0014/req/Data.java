package cn.com.yusys.yusp.dto.server.cus.xdkh0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：省心E付白名单信息维护
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "flag")
    private String flag;//标识
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "acctNo")
    private String acctNo;//账号
    @JsonProperty(value = "acctName")
    private String acctName;//户名
    @JsonProperty(value = "acctsvcr")
    private String acctsvcr;//开户行
    @JsonProperty(value = "acctsvcrName")
    private String acctsvcrName;//开户行名
    @JsonProperty(value = "isBankFlag")
    private String isBankFlag;//行内外标识
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//贷款用途
    @JsonProperty(value = "fileUpload")
    private String fileUpload;//附件
    @JsonProperty(value = "startPageNum")
    private String startPageNum;//起始页数
    @JsonProperty(value = "pageSize")
    private String pageSize;//分页大小


    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getAcctNo() {
        return acctNo;
    }

    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getAcctsvcr() {
        return acctsvcr;
    }

    public void setAcctsvcr(String acctsvcr) {
        this.acctsvcr = acctsvcr;
    }

    public String getAcctsvcrName() {
        return acctsvcrName;
    }

    public void setAcctsvcrName(String acctsvcrName) {
        this.acctsvcrName = acctsvcrName;
    }

    public String getIsBankFlag() {
        return isBankFlag;
    }

    public void setIsBankFlag(String isBankFlag) {
        this.isBankFlag = isBankFlag;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(String fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Data{" +
                "flag='" + flag + '\'' +
                "serno='" + serno + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "acctNo='" + acctNo + '\'' +
                "acctName='" + acctName + '\'' +
                "acctsvcr='" + acctsvcr + '\'' +
                "acctsvcrName='" + acctsvcrName + '\'' +
                "isBankFlag='" + isBankFlag + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "fileUpload='" + fileUpload + '\'' +
                "startPageNum='" + startPageNum + '\'' +
                "pageSize='" + pageSize + '\'' +
                '}';
    }
}  
