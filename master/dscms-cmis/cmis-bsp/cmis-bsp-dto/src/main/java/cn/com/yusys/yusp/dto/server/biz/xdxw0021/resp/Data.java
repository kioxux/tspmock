package cn.com.yusys.yusp.dto.server.biz.xdxw0021.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/18 14:50:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/18 14:50
 * @since 2021/5/18 14:50
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "disbStatus")
    private String disbStatus;//放款状态
    @JsonProperty(value = "billStatus")
    private String billStatus;//借据状态

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getDisbStatus() {
        return disbStatus;
    }

    public void setDisbStatus(String disbStatus) {
        this.disbStatus = disbStatus;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contStatus='" + contStatus + '\'' +
                ", disbStatus='" + disbStatus + '\'' +
                ", billStatus='" + billStatus + '\'' +
                '}';
    }
}
