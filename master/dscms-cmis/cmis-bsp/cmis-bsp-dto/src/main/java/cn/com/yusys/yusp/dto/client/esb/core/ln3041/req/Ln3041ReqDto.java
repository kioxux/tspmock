package cn.com.yusys.yusp.dto.client.esb.core.ln3041.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还；
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3041ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "dkqixian")
    private String dkqixian;//贷款期限(月)
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "benjheji")
    private BigDecimal benjheji;//本金合计
    @JsonProperty(value = "lixiheji")
    private BigDecimal lixiheji;//利息合计
    @JsonProperty(value = "qiankzee")
    private BigDecimal qiankzee;//欠款总额
    @JsonProperty(value = "huankzle")
    private String huankzle;//还款种类
    @JsonProperty(value = "dktqhkzl")
    private String dktqhkzl;//提前还款种类
    @JsonProperty(value = "tqhktysx")
    private BigDecimal tqhktysx;//退客户预收息金额
    @JsonProperty(value = "yshxhbje")
    private BigDecimal yshxhbje;//预收息还本金额
    @JsonProperty(value = "tqhktzfs")
    private String tqhktzfs;//提前还款调整计划方式
    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "sfqzjieq")
    private String sfqzjieq;//强制结清标志
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "tqhkfjje")
    private BigDecimal tqhkfjje;//提前还款罚金金额
    @JsonProperty(value = "daikghfs")
    private String daikghfs;//贷款归还方式
    @JsonProperty(value = "hkbeizhu")
    private String hkbeizhu;//还款备注
    @JsonProperty(value = "fajiftbl")
    private BigDecimal fajiftbl;//罚金分摊比例
    @JsonProperty(value = "fajirzzh")
    private String fajirzzh;//罚金入账账号
    @JsonProperty(value = "fajirzxh")
    private String fajirzxh;//罚金入账子序号
    @JsonProperty(value = "tqhkhxfs")
    private String tqhkhxfs;//提前还款还息方式
    @JsonProperty(value = "pingzhzl")
    private String pingzhzl;//凭证种类
    @JsonProperty(value = "pingzhma")
    private String pingzhma;//凭证批号
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao;//凭证序号
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn;//支付条件
    @JsonProperty(value = "jiaoymma")
    private String jiaoymma;//交易密码
    @JsonProperty(value = "mimazlei")
    private String mimazlei;//密码种类
    @JsonProperty(value = "zhjnzlei")
    private String zhjnzlei;//证件种类
    @JsonProperty(value = "zhjhaoma")
    private String zhjhaoma;//证件号码
    @JsonProperty(value = "kehuzhlx")
    private String kehuzhlx;//还款账号类型
    @JsonProperty(value = "yanmjine")
    private BigDecimal yanmjine;//验密金额
    @JsonProperty(value = "huandzms")
    private String huandzms;//还贷证明书
    @JsonProperty(value = "brchno")
    private String brchno;//    部门号,取账务机构号

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getBenjheji() {
        return benjheji;
    }

    public void setBenjheji(BigDecimal benjheji) {
        this.benjheji = benjheji;
    }

    public BigDecimal getLixiheji() {
        return lixiheji;
    }

    public void setLixiheji(BigDecimal lixiheji) {
        this.lixiheji = lixiheji;
    }

    public BigDecimal getQiankzee() {
        return qiankzee;
    }

    public void setQiankzee(BigDecimal qiankzee) {
        this.qiankzee = qiankzee;
    }

    public String getHuankzle() {
        return huankzle;
    }

    public void setHuankzle(String huankzle) {
        this.huankzle = huankzle;
    }

    public String getDktqhkzl() {
        return dktqhkzl;
    }

    public void setDktqhkzl(String dktqhkzl) {
        this.dktqhkzl = dktqhkzl;
    }

    public BigDecimal getTqhktysx() {
        return tqhktysx;
    }

    public void setTqhktysx(BigDecimal tqhktysx) {
        this.tqhktysx = tqhktysx;
    }

    public BigDecimal getYshxhbje() {
        return yshxhbje;
    }

    public void setYshxhbje(BigDecimal yshxhbje) {
        this.yshxhbje = yshxhbje;
    }

    public String getTqhktzfs() {
        return tqhktzfs;
    }

    public void setTqhktzfs(String tqhktzfs) {
        this.tqhktzfs = tqhktzfs;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getSfqzjieq() {
        return sfqzjieq;
    }

    public void setSfqzjieq(String sfqzjieq) {
        this.sfqzjieq = sfqzjieq;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public BigDecimal getTqhkfjje() {
        return tqhkfjje;
    }

    public void setTqhkfjje(BigDecimal tqhkfjje) {
        this.tqhkfjje = tqhkfjje;
    }

    public String getDaikghfs() {
        return daikghfs;
    }

    public void setDaikghfs(String daikghfs) {
        this.daikghfs = daikghfs;
    }

    public String getHkbeizhu() {
        return hkbeizhu;
    }

    public void setHkbeizhu(String hkbeizhu) {
        this.hkbeizhu = hkbeizhu;
    }

    public BigDecimal getFajiftbl() {
        return fajiftbl;
    }

    public void setFajiftbl(BigDecimal fajiftbl) {
        this.fajiftbl = fajiftbl;
    }

    public String getFajirzzh() {
        return fajirzzh;
    }

    public void setFajirzzh(String fajirzzh) {
        this.fajirzzh = fajirzzh;
    }

    public String getFajirzxh() {
        return fajirzxh;
    }

    public void setFajirzxh(String fajirzxh) {
        this.fajirzxh = fajirzxh;
    }

    public String getTqhkhxfs() {
        return tqhkhxfs;
    }

    public void setTqhkhxfs(String tqhkhxfs) {
        this.tqhkhxfs = tqhkhxfs;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getJiaoymma() {
        return jiaoymma;
    }

    public void setJiaoymma(String jiaoymma) {
        this.jiaoymma = jiaoymma;
    }

    public String getMimazlei() {
        return mimazlei;
    }

    public void setMimazlei(String mimazlei) {
        this.mimazlei = mimazlei;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public BigDecimal getYanmjine() {
        return yanmjine;
    }

    public void setYanmjine(BigDecimal yanmjine) {
        this.yanmjine = yanmjine;
    }

    public String getHuandzms() {
        return huandzms;
    }

    public void setHuandzms(String huandzms) {
        this.huandzms = huandzms;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    @Override
    public String toString() {
        return "Ln3041ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", dkqixian='" + dkqixian + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", zhchbjin=" + zhchbjin +
                ", yuqibjin=" + yuqibjin +
                ", dzhibjin=" + dzhibjin +
                ", daizbjin=" + daizbjin +
                ", ysyjlixi=" + ysyjlixi +
                ", csyjlixi=" + csyjlixi +
                ", ysqianxi=" + ysqianxi +
                ", csqianxi=" + csqianxi +
                ", ysyjfaxi=" + ysyjfaxi +
                ", csyjfaxi=" + csyjfaxi +
                ", yshofaxi=" + yshofaxi +
                ", cshofaxi=" + cshofaxi +
                ", yingjifx=" + yingjifx +
                ", fuxiiiii=" + fuxiiiii +
                ", benjheji=" + benjheji +
                ", lixiheji=" + lixiheji +
                ", qiankzee=" + qiankzee +
                ", huankzle='" + huankzle + '\'' +
                ", dktqhkzl='" + dktqhkzl + '\'' +
                ", tqhktysx=" + tqhktysx +
                ", yshxhbje=" + yshxhbje +
                ", tqhktzfs='" + tqhktzfs + '\'' +
                ", huankjee=" + huankjee +
                ", zijnlaiy='" + zijnlaiy + '\'' +
                ", huankzhh='" + huankzhh + '\'' +
                ", sfqzjieq='" + sfqzjieq + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", tqhkfjje=" + tqhkfjje +
                ", daikghfs='" + daikghfs + '\'' +
                ", hkbeizhu='" + hkbeizhu + '\'' +
                ", fajiftbl=" + fajiftbl +
                ", fajirzzh='" + fajirzzh + '\'' +
                ", fajirzxh='" + fajirzxh + '\'' +
                ", tqhkhxfs='" + tqhkhxfs + '\'' +
                ", pingzhzl='" + pingzhzl + '\'' +
                ", pingzhma='" + pingzhma + '\'' +
                ", pngzxhao='" + pngzxhao + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                ", jiaoymma='" + jiaoymma + '\'' +
                ", mimazlei='" + mimazlei + '\'' +
                ", zhjnzlei='" + zhjnzlei + '\'' +
                ", zhjhaoma='" + zhjhaoma + '\'' +
                ", kehuzhlx='" + kehuzhlx + '\'' +
                ", yanmjine=" + yanmjine +
                ", huandzms='" + huandzms + '\'' +
                ", brchno='" + brchno + '\'' +
                '}';
    }
}
