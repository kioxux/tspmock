package cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：财务信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xirs28ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custid")
    private String custid;//客户编号
    @JsonProperty(value = "reportdate")
    private String reportdate;//报表日期
    @JsonProperty(value = "reportsope")
    private String reportsope;//报表口径
    @JsonProperty(value = "reportcurrency")
    private String reportcurrency;//报表币种
    @JsonProperty(value = "reportunit")
    private String reportunit;//报表单位
    @JsonProperty(value = "reportaudit")
    private String reportaudit;//审计标志
    @JsonProperty(value = "auditunit")
    private String auditunit;//审计单位
    @JsonProperty(value = "auditopinion")
    private String auditopinion;//审计意见
    @JsonProperty(value = "modelclass")
    private String modelclass;//财务报表类别
    @JsonProperty(value = "flag")
    private String flag;//报表标识
    @JsonProperty(value = "Loss")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Loss> Loss;
    @JsonProperty(value = "AssetDebt")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.AssetDebt> AssetDebt;

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getReportdate() {
        return reportdate;
    }

    public void setReportdate(String reportdate) {
        this.reportdate = reportdate;
    }

    public String getReportsope() {
        return reportsope;
    }

    public void setReportsope(String reportsope) {
        this.reportsope = reportsope;
    }

    public String getReportcurrency() {
        return reportcurrency;
    }

    public void setReportcurrency(String reportcurrency) {
        this.reportcurrency = reportcurrency;
    }

    public String getReportunit() {
        return reportunit;
    }

    public void setReportunit(String reportunit) {
        this.reportunit = reportunit;
    }

    public String getReportaudit() {
        return reportaudit;
    }

    public void setReportaudit(String reportaudit) {
        this.reportaudit = reportaudit;
    }

    public String getAuditunit() {
        return auditunit;
    }

    public void setAuditunit(String auditunit) {
        this.auditunit = auditunit;
    }

    public String getAuditopinion() {
        return auditopinion;
    }

    public void setAuditopinion(String auditopinion) {
        this.auditopinion = auditopinion;
    }

    public String getModelclass() {
        return modelclass;
    }

    public void setModelclass(String modelclass) {
        this.modelclass = modelclass;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Loss> getLoss() {
        return Loss;
    }

    @JsonIgnore
    public void setLoss(List<cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Loss> loss) {
        Loss = loss;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.AssetDebt> getAssetDebt() {
        return AssetDebt;
    }

    @JsonIgnore
    public void setAssetDebt(List<cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.AssetDebt> assetDebt) {
        AssetDebt = assetDebt;
    }

    @Override
    public String toString() {
        return "Xirs28ReqDto{" +
                "custid='" + custid + '\'' +
                ", reportdate='" + reportdate + '\'' +
                ", reportsope='" + reportsope + '\'' +
                ", reportcurrency='" + reportcurrency + '\'' +
                ", reportunit='" + reportunit + '\'' +
                ", reportaudit='" + reportaudit + '\'' +
                ", auditunit='" + auditunit + '\'' +
                ", auditopinion='" + auditopinion + '\'' +
                ", modelclass='" + modelclass + '\'' +
                ", flag='" + flag + '\'' +
                ", Loss=" + Loss +
                ", AssetDebt=" + AssetDebt +
                '}';
    }
}
