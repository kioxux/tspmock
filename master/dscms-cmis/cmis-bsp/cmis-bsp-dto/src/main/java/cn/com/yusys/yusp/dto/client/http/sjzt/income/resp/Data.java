package cn.com.yusys.yusp.dto.client.http.sjzt.income.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/20 18:38
 * @since 2021/8/20 18:38
 */
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "incomeType")
    private String incomeType;//收益类型
    @JsonProperty(value = "incomeDate")
    private String incomeDate;//收益产生日
    @JsonProperty(value = "remark")
    private String remark;//备注
    @JsonProperty(value = "incomeAmt")
    private BigDecimal incomeAmt;//收益金额


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(String incomeType) {
        this.incomeType = incomeType;
    }

    public String getIncomeDate() {
        return incomeDate;
    }

    public void setIncomeDate(String incomeDate) {
        this.incomeDate = incomeDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getIncomeAmt() {
        return incomeAmt;
    }

    public void setIncomeAmt(BigDecimal incomeAmt) {
        this.incomeAmt = incomeAmt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", incomeType='" + incomeType + '\'' +
                ", incomeDate='" + incomeDate + '\'' +
                ", remark='" + remark + '\'' +
                ", incomeAmt=" + incomeAmt +
                '}';
    }
}
