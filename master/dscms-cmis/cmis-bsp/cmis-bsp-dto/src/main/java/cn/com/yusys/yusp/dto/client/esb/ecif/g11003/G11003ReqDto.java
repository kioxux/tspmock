package cn.com.yusys.yusp.dto.client.esb.ecif.g11003;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Service：客户集团信息维护 (new)
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class G11003ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "grouna")
    private String grouna;//	varchar2  	30	集团客户名称
    @JsonProperty(value = "custno")
    private String custno;//	VARCHAR2	30	客户编号			Y
    @JsonProperty(value = "custln")
    private String custln;//	varchar2  	50	客户中证码
    @JsonProperty(value = "custna")
    private String custna;//	VARCHAR2	240	客户名称
    @JsonProperty(value = "idtfno")
    private String idtfno;//	VARCHAR2	40	证件号码
    @JsonProperty(value = "oadate")
    private String oadate;//	varchar2  	10	更新办公地址日期					yyyymmdd
    @JsonProperty(value = "homead")
    private String homead;//	VARCHAR2  	100	地址
    @JsonProperty(value = "socino")
    private String socino;//	varchar2  	30	社会信用代码
    @JsonProperty(value = "busino")
    private String busino;//	varchar2  	50	工商登记注册号
    @JsonProperty(value = "ofaddr")
    private String ofaddr;//	varchar2  	255	办公地址行政区划
    @JsonProperty(value = "crpdeg")
    private String crpdeg;//	varchar2  	40	集团紧密程度
    @JsonProperty(value = "crpcno")
    private String crpcno;//	varchar2  	250	集团客户情况说明
    @JsonProperty(value = "ctigno")
    private String ctigno;//	varchar2  	20	管户客户经理
    @JsonProperty(value = "citorg")
    private String citorg;//	varchar2  	20	所属机构
    @JsonProperty(value = "grpsta")
    private String grpsta;//	varchar2  	5	集团客户状态	"0：无效1：有效"
    @JsonProperty(value = "oprtye")
    private String oprtye;//	varchar2  	5	操作类型
    @JsonProperty(value = "inptno")
    private String inptno;//	varchar2  	30	登记人
    @JsonProperty(value = "inporg")
    private String inporg;//	varchar2  	30	登记机构
    @JsonProperty(value = "CIRCLE_ARRAY_MEM")
    private List<CircleArrayMem> circleArrayMem;// 集团成员信息_ARRAY

    public List<CircleArrayMem> getCircleArrayMem() {
        return circleArrayMem;
    }

    public void setCircleArrayMem(List<CircleArrayMem> circleArrayMem) {
        this.circleArrayMem = circleArrayMem;
    }

    public String getGrouna() {
        return grouna;
    }

    public void setGrouna(String grouna) {
        this.grouna = grouna;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustln() {
        return custln;
    }

    public void setCustln(String custln) {
        this.custln = custln;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getOadate() {
        return oadate;
    }

    public void setOadate(String oadate) {
        this.oadate = oadate;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getSocino() {
        return socino;
    }

    public void setSocino(String socino) {
        this.socino = socino;
    }

    public String getBusino() {
        return busino;
    }

    public void setBusino(String busino) {
        this.busino = busino;
    }

    public String getOfaddr() {
        return ofaddr;
    }

    public void setOfaddr(String ofaddr) {
        this.ofaddr = ofaddr;
    }

    public String getCrpdeg() {
        return crpdeg;
    }

    public void setCrpdeg(String crpdeg) {
        this.crpdeg = crpdeg;
    }

    public String getCrpcno() {
        return crpcno;
    }

    public void setCrpcno(String crpcno) {
        this.crpcno = crpcno;
    }

    public String getCtigno() {
        return ctigno;
    }

    public void setCtigno(String ctigno) {
        this.ctigno = ctigno;
    }

    public String getCitorg() {
        return citorg;
    }

    public void setCitorg(String citorg) {
        this.citorg = citorg;
    }

    public String getGrpsta() {
        return grpsta;
    }

    public void setGrpsta(String grpsta) {
        this.grpsta = grpsta;
    }

    public String getOprtye() {
        return oprtye;
    }

    public void setOprtye(String oprtye) {
        this.oprtye = oprtye;
    }

    public String getInptno() {
        return inptno;
    }

    public void setInptno(String inptno) {
        this.inptno = inptno;
    }

    public String getInporg() {
        return inporg;
    }

    public void setInporg(String inporg) {
        this.inporg = inporg;
    }

    @Override
    public String toString() {
        return "G11003ReqDto{" +
                "grouna='" + grouna + '\'' +
                ", custno='" + custno + '\'' +
                ", custln='" + custln + '\'' +
                ", custna='" + custna + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", oadate='" + oadate + '\'' +
                ", homead='" + homead + '\'' +
                ", socino='" + socino + '\'' +
                ", busino='" + busino + '\'' +
                ", ofaddr='" + ofaddr + '\'' +
                ", crpdeg='" + crpdeg + '\'' +
                ", crpcno='" + crpcno + '\'' +
                ", ctigno='" + ctigno + '\'' +
                ", citorg='" + citorg + '\'' +
                ", grpsta='" + grpsta + '\'' +
                ", oprtye='" + oprtye + '\'' +
                ", inptno='" + inptno + '\'' +
                ", inporg='" + inporg + '\'' +
                '}';
    }
}
