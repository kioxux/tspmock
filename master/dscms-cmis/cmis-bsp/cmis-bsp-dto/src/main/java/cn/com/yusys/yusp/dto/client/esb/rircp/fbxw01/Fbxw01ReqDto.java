package cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：授信申请提交接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午5:34:07
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxw01ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd; // 处理码
    @JsonProperty(value = "servtp")
    private String servtp; // 渠道
    @JsonProperty(value = "servsq")
    private String servsq; // 渠道流水
    @JsonProperty(value = "userid")
    private String userid; // 柜员号
    @JsonProperty(value = "brchno")
    private String brchno; // 部门号
    @JsonProperty(value = "channel_type")
    private String channel_type; // 渠道来源
    @JsonProperty(value = "co_platform")
    private String co_platform; // 合作平台
    @JsonProperty(value = "prd_type")
    private String prd_type; // 产品类别
    @JsonProperty(value = "prd_code")
    private String prd_code; // 产品代码（零售智能风控内部代码）
    @JsonProperty(value = "op_flag")
    private String op_flag; // 操作类型
    @JsonProperty(value = "apply_no")
    private String apply_no; // 业务唯一编号
    @JsonProperty(value = "cert_type")
    private String cert_type; // 证件类型
    @JsonProperty(value = "cert_code")
    private String cert_code; // 证件号码
    @JsonProperty(value = "cust_name")
    private String cust_name; // 客户姓名
    @JsonProperty(value = "phone")
    private String phone; // 移动电话
    @JsonProperty(value = "cust_id_core")
    private String cust_id_core; // 核心客户号
    @JsonProperty(value = "spouse_name")
    private String spouse_name; // 配偶姓名
    @JsonProperty(value = "spouse_cert_type")
    private String spouse_cert_type; // 配偶证件类型
    @JsonProperty(value = "spouse_cert_code")
    private String spouse_cert_code; // 配偶证件号码
    @JsonProperty(value = "old_bill_no")
    private String old_bill_no; // 原借据编号
    @JsonProperty(value = "biz_manager_id")
    private String biz_manager_id; // 客户经理号
    @JsonProperty(value = "biz_org_id")
    private String biz_org_id; // 归属机构

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getOp_flag() {
        return op_flag;
    }

    public void setOp_flag(String op_flag) {
        this.op_flag = op_flag;
    }

    public String getApply_no() {
        return apply_no;
    }

    public void setApply_no(String apply_no) {
        this.apply_no = apply_no;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCust_id_core() {
        return cust_id_core;
    }

    public void setCust_id_core(String cust_id_core) {
        this.cust_id_core = cust_id_core;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_cert_type() {
        return spouse_cert_type;
    }

    public void setSpouse_cert_type(String spouse_cert_type) {
        this.spouse_cert_type = spouse_cert_type;
    }

    public String getSpouse_cert_code() {
        return spouse_cert_code;
    }

    public void setSpouse_cert_code(String spouse_cert_code) {
        this.spouse_cert_code = spouse_cert_code;
    }

    public String getOld_bill_no() {
        return old_bill_no;
    }

    public void setOld_bill_no(String old_bill_no) {
        this.old_bill_no = old_bill_no;
    }

    public String getBiz_manager_id() {
        return biz_manager_id;
    }

    public void setBiz_manager_id(String biz_manager_id) {
        this.biz_manager_id = biz_manager_id;
    }

    public String getBiz_org_id() {
        return biz_org_id;
    }

    public void setBiz_org_id(String biz_org_id) {
        this.biz_org_id = biz_org_id;
    }

    @Override
    public String toString() {
        return "Fbxw01Dto{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", channel_type='" + channel_type + '\'' +
                ", co_platform='" + co_platform + '\'' +
                ", prd_type='" + prd_type + '\'' +
                ", prd_code='" + prd_code + '\'' +
                ", op_flag='" + op_flag + '\'' +
                ", apply_no='" + apply_no + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", phone='" + phone + '\'' +
                ", cust_id_core='" + cust_id_core + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", spouse_cert_type='" + spouse_cert_type + '\'' +
                ", spouse_cert_code='" + spouse_cert_code + '\'' +
                ", old_bill_no='" + old_bill_no + '\'' +
                ", biz_manager_id='" + biz_manager_id + '\'' +
                ", biz_org_id='" + biz_org_id + '\'' +
                '}';
    }
}