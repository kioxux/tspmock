package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/9/8 21:58
 * @since 2021/9/8 21:58
 */
@JsonPropertyOrder(alphabetic = true)
public class Plage_list implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "plage_serno")
    private String plage_serno;//抵质押品流水号
    @JsonProperty(value = "plage_addr")
    private String plage_addr;//抵质押品地址
    @JsonProperty(value = "plage_owner")
    private String plage_owner;//抵质押品所有权人
    @JsonProperty(value = "reason")
    private String reason;//修改原因

    public String getPlage_serno() {
        return plage_serno;
    }

    public void setPlage_serno(String plage_serno) {
        this.plage_serno = plage_serno;
    }

    public String getPlage_addr() {
        return plage_addr;
    }

    public void setPlage_addr(String plage_addr) {
        this.plage_addr = plage_addr;
    }

    public String getPlage_owner() {
        return plage_owner;
    }

    public void setPlage_owner(String plage_owner) {
        this.plage_owner = plage_owner;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Plage_list{" +
                "plage_serno='" + plage_serno + '\'' +
                ", plage_addr='" + plage_addr + '\'' +
                ", plage_owner='" + plage_owner + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
