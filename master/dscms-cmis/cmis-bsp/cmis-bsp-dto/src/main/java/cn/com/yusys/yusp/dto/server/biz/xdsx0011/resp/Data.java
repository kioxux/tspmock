package cn.com.yusys.yusp.dto.server.biz.xdsx0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author zhugenrong
 * @date 2021/4/23-19:02
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "preferPoints")
    private String preferPoints;//优惠点数
    @JsonProperty(value = "preferTimes")
    private String preferTimes;//优惠次数

    public String getPreferPoints() {
        return preferPoints;
    }

    public void setPreferPoints(String preferPoints) {
        this.preferPoints = preferPoints;
    }

    public String getPreferTimes() {
        return preferTimes;
    }

    public void setPreferTimes(String preferTimes) {
        this.preferTimes = preferTimes;
    }

    @Override
    public String toString() {
        return "Data{" +
                "preferPoints='" + preferPoints + '\'' +
                "preferTimes='" + preferTimes + '\'' +
                '}';
    }
}