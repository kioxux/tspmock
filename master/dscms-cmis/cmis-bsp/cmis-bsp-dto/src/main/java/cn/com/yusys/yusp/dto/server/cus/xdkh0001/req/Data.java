package cn.com.yusys.yusp.dto.server.cus.xdkh0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：个人客户基本信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    private String queryType;//查询类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//证件号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    @Override
    public String toString() {
        return "Data{" +
                    "queryType='" + queryType + '\'' +
                    "cusId='" + cusId + '\'' +
                    "certType='" + certType + '\'' +
                    "certCode='" + certCode + '\'' +
                    "cusName='" + cusName + '\'' +
                    '}';
    }
}
