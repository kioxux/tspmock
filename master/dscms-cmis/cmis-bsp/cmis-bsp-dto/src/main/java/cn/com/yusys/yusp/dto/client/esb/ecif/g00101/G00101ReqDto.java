package cn.com.yusys.yusp.dto.client.esb.ecif.g00101;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：对公客户综合信息查询</br>
 * V1.1:删除字段sorgfname同业机构全称，删除字段sorgtpj同业机构类型1（检测表），删除字段sorgtpg同业机构类型2（G24）
 *
 * @author zhangpeng
 * @author ZRC
 * @version 1.1
 */
@JsonPropertyOrder(alphabetic = true)
public class G00101ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "resotp")
    private String resotp;
    @JsonProperty(value = "custno")
    private String custno;
    @JsonProperty(value = "idtftp")
    private String idtftp;
    @JsonProperty(value = "idtfno")
    private String idtfno;
    @JsonProperty(value = "custna")
    private String custna;

    public String getResotp() {
        return resotp;
    }

    public void setResotp(String resotp) {
        this.resotp = resotp;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    @Override
    public String toString() {
        return "G00101ReqDto{" +
                "resotp='" + resotp + '\'' +
                ", custno='" + custno + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", custna='" + custna + '\'' +
                '}';
    }
}

