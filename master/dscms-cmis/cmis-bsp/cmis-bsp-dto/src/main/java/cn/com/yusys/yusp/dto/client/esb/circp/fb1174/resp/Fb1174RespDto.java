package cn.com.yusys.yusp.dto.client.esb.circp.fb1174.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：房抵e点贷尽调结果通知
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1174RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Fb1174RespDto{" +
                '}';
    }
}  
