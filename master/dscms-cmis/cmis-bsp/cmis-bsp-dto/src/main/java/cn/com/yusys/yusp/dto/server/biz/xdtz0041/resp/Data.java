package cn.com.yusys.yusp.dto.server.biz.xdtz0041.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据客户号前往信贷查找房贷借据信息
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款结束日
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款开始日
    @JsonProperty(value = "settlDate")
    private String settlDate;//结清日期
    @JsonProperty(value = "billAmt")
    private BigDecimal billAmt;//借据金额

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getSettlDate() {
        return settlDate;
    }

    public void setSettlDate(String settlDate) {
        this.settlDate = settlDate;
    }

    public BigDecimal getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(BigDecimal billAmt) {
        this.billAmt = billAmt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billNo='" + billNo + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", loanStartDate='" + loanStartDate + '\'' +
                ", settlDate='" + settlDate + '\'' +
                ", billAmt=" + billAmt +
                '}';
    }
}
