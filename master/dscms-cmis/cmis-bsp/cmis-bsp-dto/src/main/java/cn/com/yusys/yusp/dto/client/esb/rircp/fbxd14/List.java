package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询还款（合约）明细历史表（利翃）中的全量借据一览
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contract_no")
    private String contract_no;//利翃平台贷款合同号

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "List{" +
                "contract_no='" + contract_no + '\'' +
                '}';
    }
}
