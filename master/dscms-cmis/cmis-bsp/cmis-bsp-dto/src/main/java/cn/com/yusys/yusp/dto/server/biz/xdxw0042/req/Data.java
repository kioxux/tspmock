package cn.com.yusys.yusp.dto.server.biz.xdxw0042.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：惠享贷模型结果推送给信贷
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//业务流水
    @JsonProperty(value = "model_appr_time")
    private String model_appr_time;//模型审批时间
    @JsonProperty(value = "model_grade")
    private String model_grade;//模型评级
    @JsonProperty(value = "model_score")
    private String model_score;//模型得分
    @JsonProperty(value = "model_amt")
    private BigDecimal model_amt;//模型金额
    @JsonProperty(value = "model_rate")
    private BigDecimal model_rate;//模型利率
    @JsonProperty(value = "judi_case_judge")
    private String judi_case_judge;//司法案件判断
    @JsonProperty(value = "result_status")
    private String result_status;//模型结果状态
    @JsonProperty(value = "apply_no")
    private String apply_no;//业务唯一编号
    @JsonProperty(value = "opinion")
    private String opinion;//模型意见
    @JsonProperty(value = "init_model_amt")
    private BigDecimal init_model_amt;//初始额度

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getModel_appr_time() {
        return model_appr_time;
    }

    public void setModel_appr_time(String model_appr_time) {
        this.model_appr_time = model_appr_time;
    }

    public String getModel_grade() {
        return model_grade;
    }

    public void setModel_grade(String model_grade) {
        this.model_grade = model_grade;
    }

    public String getModel_score() {
        return model_score;
    }

    public void setModel_score(String model_score) {
        this.model_score = model_score;
    }

    public BigDecimal getModel_amt() {
        return model_amt;
    }

    public void setModel_amt(BigDecimal model_amt) {
        this.model_amt = model_amt;
    }

    public BigDecimal getModel_rate() {
        return model_rate;
    }

    public void setModel_rate(BigDecimal model_rate) {
        this.model_rate = model_rate;
    }

    public String getJudi_case_judge() {
        return judi_case_judge;
    }

    public void setJudi_case_judge(String judi_case_judge) {
        this.judi_case_judge = judi_case_judge;
    }

    public String getResult_status() {
        return result_status;
    }

    public void setResult_status(String result_status) {
        this.result_status = result_status;
    }

    public String getApply_no() {
        return apply_no;
    }

    public void setApply_no(String apply_no) {
        this.apply_no = apply_no;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    public BigDecimal getInit_model_amt() {
        return init_model_amt;
    }

    public void setInit_model_amt(BigDecimal init_model_amt) {
        this.init_model_amt = init_model_amt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "survey_serno='" + survey_serno + '\'' +
                "model_appr_time='" + model_appr_time + '\'' +
                "model_grade='" + model_grade + '\'' +
                "model_score='" + model_score + '\'' +
                "model_amt='" + model_amt + '\'' +
                "model_rate='" + model_rate + '\'' +
                "judi_case_judge='" + judi_case_judge + '\'' +
                "result_status='" + result_status + '\'' +
                "apply_no='" + apply_no + '\'' +
                "opinion='" + opinion + '\'' +
                "init_model_amt='" + init_model_amt + '\'' +
                '}';
    }
}  
