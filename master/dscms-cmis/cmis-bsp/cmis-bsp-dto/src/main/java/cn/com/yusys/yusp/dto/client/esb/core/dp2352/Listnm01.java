package cn.com.yusys.yusp.dto.client.esb.core.dp2352;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 推荐人信息列表
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Listnm01 implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "tjrminc1")
    private String tjrminc1;//推荐人名称1
     @JsonProperty(value = "tjrgonh1")
    private String tjrgonh1;//推荐人工号1
     @JsonProperty(value = "tjrenbl1")
    private BigDecimal tjrenbl1;//推荐人比例1

    public String getTjrminc1() {
        return tjrminc1;
    }

    public void setTjrminc1(String tjrminc1) {
        this.tjrminc1 = tjrminc1;
    }

    public String getTjrgonh1() {
        return tjrgonh1;
    }

    public void setTjrgonh1(String tjrgonh1) {
        this.tjrgonh1 = tjrgonh1;
    }

    public BigDecimal getTjrenbl1() {
        return tjrenbl1;
    }

    public void setTjrenbl1(BigDecimal tjrenbl1) {
        this.tjrenbl1 = tjrenbl1;
    }

    @Override
    public String toString() {
        return "Listnm01{" +
                "tjrminc1='" + tjrminc1 + '\'' +
                ", tjrgonh1='" + tjrgonh1 + '\'' +
                ", tjrenbl1=" + tjrenbl1 +
                '}';
    }
}
