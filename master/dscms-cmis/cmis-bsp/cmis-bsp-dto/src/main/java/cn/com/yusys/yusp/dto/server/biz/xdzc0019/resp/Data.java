package cn.com.yusys.yusp.dto.server.biz.xdzc0019.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 20:55
 * @since 2021/6/8 20:55
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalSize")
    private String totalSize;//总记录数
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.server.biz.xdzc0019.resp.List> list;

    public String getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(String totalSize) {
        this.totalSize = totalSize;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "totalSize='" + totalSize + '\'' +
                ", list=" + list +
                '}';
    }
}
