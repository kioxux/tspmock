package cn.com.yusys.yusp.dto.server.biz.xddh0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 15:24:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 15:24
 * @since 2021/5/22 15:24
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bill_no")
    private String bill_no;//借据号

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    @Override
    public String toString() {
        return "Xddh0009ReqDto{" +
                "bill_no='" + bill_no + '\'' +
                '}';
    }
}
