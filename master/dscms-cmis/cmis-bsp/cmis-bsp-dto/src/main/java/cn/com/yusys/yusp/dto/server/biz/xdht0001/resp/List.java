package cn.com.yusys.yusp.dto.server.biz.xdht0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询省心E付合同信息
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "avlBal")
    private BigDecimal avlBal;//可用余额
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同到期日
    @JsonProperty(value = "curtLPR")
    private BigDecimal curtLPR;//当前lpr
    @JsonProperty(value = "irAdjustType")
    private String irAdjustType;//利率调整方式
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行利率
    @JsonProperty(value = "isFst")
    private String isFst;//是否首次

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getAvlBal() {
        return avlBal;
    }

    public void setAvlBal(BigDecimal avlBal) {
        this.avlBal = avlBal;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public BigDecimal getCurtLPR() {
        return curtLPR;
    }

    public void setCurtLPR(BigDecimal curtLPR) {
        this.curtLPR = curtLPR;
    }

    public String getIrAdjustType() {
        return irAdjustType;
    }

    public void setIrAdjustType(String irAdjustType) {
        this.irAdjustType = irAdjustType;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public String getIsFst() {
        return isFst;
    }

    public void setIsFst(String isFst) {
        this.isFst = isFst;
    }

    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                "cnContNo='" + cnContNo + '\'' +
                "contAmt='" + contAmt + '\'' +
                "avlBal='" + avlBal + '\'' +
                "contStartDate='" + contStartDate + '\'' +
                "contEndDate='" + contEndDate + '\'' +
                "curtLPR='" + curtLPR + '\'' +
                "irAdjustType='" + irAdjustType + '\'' +
                "realityIrY='" + realityIrY + '\'' +
                "isFst='" + isFst + '\'' +
                '}';
    }
}