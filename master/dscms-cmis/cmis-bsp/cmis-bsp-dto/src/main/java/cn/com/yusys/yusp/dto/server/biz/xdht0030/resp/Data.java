package cn.com.yusys.yusp.dto.server.biz.xdht0030.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询客户我行合作次数
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "coopTimes")
    private Integer coopTimes;//合作次数

    public Integer getCoopTimes() {
        return coopTimes;
    }

    public void setCoopTimes(Integer coopTimes) {
        this.coopTimes = coopTimes;
    }

    @Override
    public String toString() {
        return "Data{" +
                "coopTimes='" + coopTimes + '\'' +
                '}';
    }
}