package cn.com.yusys.yusp.dto.client.esb.core.dp2280;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应DTO：子账户序号查询接口
 *
 * @author
 * @version 1.0
 * @since 2021/4/16上午10:17:34
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2280RespDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "listnm")
    private ListNm listnm; // 账户对照表信息
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo; // 客户号
    @JsonProperty(value = "kehuzhmc")
    private String kehuzhmc; // 客户账户名称
    @JsonProperty(value = "kehuleix")
    private String kehuleix; // 客户类型
    @JsonProperty(value = "doqiriqi")
    private String doqiriqi; // 客户号证件到期日期
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn; // 支付条件
    @JsonProperty(value = "zhjnzlei")
    private String zhjnzlei; // 证件种类
    @JsonProperty(value = "zhjhaoma")
    private String zhjhaoma; // 证件号码
    @JsonProperty(value = "pngzzlei")
    private String pngzzlei; // 凭证种类
    @JsonProperty(value = "pngzphao")
    private String pngzphao; // 凭证批号
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao; // 凭证序号
    @JsonProperty(value = "pngzhhao")
    private String pngzhhao; // 凭证号
    @JsonProperty(value = "tduifwei")
    private String tduifwei; // 通兑范围
    @JsonProperty(value = "glpinzbz")
    private String glpinzbz; // 关联凭证标志
    @JsonProperty(value = "kehuzhlx")
    private String kehuzhlx; // 客户账号类型
    @JsonProperty(value = "kehumich")
    private String kehumich; // 客户名称
    @JsonProperty(value = "kehuzhwm")
    private String kehuzhwm; // 客户中文名
    @JsonProperty(value = "kehuywmc")
    private String kehuywmc; // 客户英文名
    @JsonProperty(value = "lminzhhu")
    private String lminzhhu; // 联名账户标志
    @JsonProperty(value = "gxileixn")
    private String gxileixn; // 关系类型
    @JsonProperty(value = "khzhglbz")
    private String khzhglbz; // 客户账户关联标志
    @JsonProperty(value = "baomibzz")
    private String baomibzz; // 保密标志
    @JsonProperty(value = "kzjedjbz")
    private String kzjedjbz; // 客户账户金额冻结标志
    @JsonProperty(value = "kzfbdjbz")
    private String kzfbdjbz; // 客户账户封闭冻结标志
    @JsonProperty(value = "kzzsbfbz")
    private String kzzsbfbz; // 客户账户只收不付标志
    @JsonProperty(value = "kzzfbsbz")
    private String kzzfbsbz; // 客户账户只付不收标志
    @JsonProperty(value = "kzhuztai")
    private String kzhuztai; // 客户账户状态
    @JsonProperty(value = "dspzsyzt")
    private String dspzsyzt; // 凭证状态
    @JsonProperty(value = "kachapbh")
    private String kachapbh; // 卡产品编号
    @JsonProperty(value = "sbkbiaoz")
    private String sbkbiaoz; // 社保卡标志
    @JsonProperty(value = "mimaztai")
    private String mimaztai; // 密码状态
    @JsonProperty(value = "dangqhsh")
    private BigDecimal dangqhsh; // 当前行数
    @JsonProperty(value = "kadxiang")
    private String kadxiang; // 卡对象
    @JsonProperty(value = "kadengji")
    private String kadengji; // 卡等级

    public ListNm getListnm() {
        return listnm;
    }

    public void setListnm(ListNm listnm) {
        this.listnm = listnm;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getKehuleix() {
        return kehuleix;
    }

    public void setKehuleix(String kehuleix) {
        this.kehuleix = kehuleix;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPngzphao() {
        return pngzphao;
    }

    public void setPngzphao(String pngzphao) {
        this.pngzphao = pngzphao;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getPngzhhao() {
        return pngzhhao;
    }

    public void setPngzhhao(String pngzhhao) {
        this.pngzhhao = pngzhhao;
    }

    public String getTduifwei() {
        return tduifwei;
    }

    public void setTduifwei(String tduifwei) {
        this.tduifwei = tduifwei;
    }

    public String getGlpinzbz() {
        return glpinzbz;
    }

    public void setGlpinzbz(String glpinzbz) {
        this.glpinzbz = glpinzbz;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getKehumich() {
        return kehumich;
    }

    public void setKehumich(String kehumich) {
        this.kehumich = kehumich;
    }

    public String getKehuzhwm() {
        return kehuzhwm;
    }

    public void setKehuzhwm(String kehuzhwm) {
        this.kehuzhwm = kehuzhwm;
    }

    public String getKehuywmc() {
        return kehuywmc;
    }

    public void setKehuywmc(String kehuywmc) {
        this.kehuywmc = kehuywmc;
    }

    public String getLminzhhu() {
        return lminzhhu;
    }

    public void setLminzhhu(String lminzhhu) {
        this.lminzhhu = lminzhhu;
    }

    public String getGxileixn() {
        return gxileixn;
    }

    public void setGxileixn(String gxileixn) {
        this.gxileixn = gxileixn;
    }

    public String getKhzhglbz() {
        return khzhglbz;
    }

    public void setKhzhglbz(String khzhglbz) {
        this.khzhglbz = khzhglbz;
    }

    public String getBaomibzz() {
        return baomibzz;
    }

    public void setBaomibzz(String baomibzz) {
        this.baomibzz = baomibzz;
    }

    public String getKzjedjbz() {
        return kzjedjbz;
    }

    public void setKzjedjbz(String kzjedjbz) {
        this.kzjedjbz = kzjedjbz;
    }

    public String getKzfbdjbz() {
        return kzfbdjbz;
    }

    public void setKzfbdjbz(String kzfbdjbz) {
        this.kzfbdjbz = kzfbdjbz;
    }

    public String getKzzsbfbz() {
        return kzzsbfbz;
    }

    public void setKzzsbfbz(String kzzsbfbz) {
        this.kzzsbfbz = kzzsbfbz;
    }

    public String getKzzfbsbz() {
        return kzzfbsbz;
    }

    public void setKzzfbsbz(String kzzfbsbz) {
        this.kzzfbsbz = kzzfbsbz;
    }

    public String getKzhuztai() {
        return kzhuztai;
    }

    public void setKzhuztai(String kzhuztai) {
        this.kzhuztai = kzhuztai;
    }

    public String getDspzsyzt() {
        return dspzsyzt;
    }

    public void setDspzsyzt(String dspzsyzt) {
        this.dspzsyzt = dspzsyzt;
    }

    public String getKachapbh() {
        return kachapbh;
    }

    public void setKachapbh(String kachapbh) {
        this.kachapbh = kachapbh;
    }

    public String getSbkbiaoz() {
        return sbkbiaoz;
    }

    public void setSbkbiaoz(String sbkbiaoz) {
        this.sbkbiaoz = sbkbiaoz;
    }

    public String getMimaztai() {
        return mimaztai;
    }

    public void setMimaztai(String mimaztai) {
        this.mimaztai = mimaztai;
    }

    public BigDecimal getDangqhsh() {
        return dangqhsh;
    }

    public void setDangqhsh(BigDecimal dangqhsh) {
        this.dangqhsh = dangqhsh;
    }

    public String getKadxiang() {
        return kadxiang;
    }

    public void setKadxiang(String kadxiang) {
        this.kadxiang = kadxiang;
    }

    public String getKadengji() {
        return kadengji;
    }

    public void setKadengji(String kadengji) {
        this.kadengji = kadengji;
    }

    @Override
    public String toString() {
        return "Dp2280RespDto{" +
                "listnm=" + listnm +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzhmc='" + kehuzhmc + '\'' +
                ", kehuleix='" + kehuleix + '\'' +
                ", doqiriqi='" + doqiriqi + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                ", zhjnzlei='" + zhjnzlei + '\'' +
                ", zhjhaoma='" + zhjhaoma + '\'' +
                ", pngzzlei='" + pngzzlei + '\'' +
                ", pngzphao='" + pngzphao + '\'' +
                ", pngzxhao='" + pngzxhao + '\'' +
                ", pngzhhao='" + pngzhhao + '\'' +
                ", tduifwei='" + tduifwei + '\'' +
                ", glpinzbz='" + glpinzbz + '\'' +
                ", kehuzhlx='" + kehuzhlx + '\'' +
                ", kehumich='" + kehumich + '\'' +
                ", kehuzhwm='" + kehuzhwm + '\'' +
                ", kehuywmc='" + kehuywmc + '\'' +
                ", lminzhhu='" + lminzhhu + '\'' +
                ", gxileixn='" + gxileixn + '\'' +
                ", khzhglbz='" + khzhglbz + '\'' +
                ", baomibzz='" + baomibzz + '\'' +
                ", kzjedjbz='" + kzjedjbz + '\'' +
                ", kzfbdjbz='" + kzfbdjbz + '\'' +
                ", kzzsbfbz='" + kzzsbfbz + '\'' +
                ", kzzfbsbz='" + kzzfbsbz + '\'' +
                ", kzhuztai='" + kzhuztai + '\'' +
                ", dspzsyzt='" + dspzsyzt + '\'' +
                ", kachapbh='" + kachapbh + '\'' +
                ", sbkbiaoz='" + sbkbiaoz + '\'' +
                ", mimaztai='" + mimaztai + '\'' +
                ", dangqhsh=" + dangqhsh +
                ", kadxiang='" + kadxiang + '\'' +
                ", kadengji='" + kadengji + '\'' +
                '}';
    }
}