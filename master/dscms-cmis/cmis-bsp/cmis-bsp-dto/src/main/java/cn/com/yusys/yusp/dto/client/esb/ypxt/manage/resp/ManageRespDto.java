package cn.com.yusys.yusp.dto.client.esb.ypxt.manage.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：管护权移交信息同步
 * @author lihh
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class ManageRespDto implements Serializable {
	   private static final long serialVersionUID = 1L;
}
