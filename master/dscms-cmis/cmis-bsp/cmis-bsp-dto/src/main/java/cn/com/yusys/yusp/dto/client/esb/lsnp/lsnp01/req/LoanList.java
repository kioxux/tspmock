package cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LoanList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "busi_type")
    private String busi_type;//业务品种
    @JsonProperty(value = "repay_type")
    private String repay_type;//还款方式
    @JsonProperty(value = "guar_type")
    private String guar_type;//担保方式
    @JsonProperty(value = "credit_amt_div_eval_val")
    private BigDecimal credit_amt_div_eval_val;//原始贷款金额占合同层抵质押物市场评估价值总和的百分比
    @JsonProperty(value = "credit_amt_div_firm_value")
    private BigDecimal credit_amt_div_firm_value;//原始贷款金额占合同层抵质押物实际认定价值总和的百分比
    @JsonProperty(value = "house_prop")
    private String house_prop;//房屋性质
    @JsonProperty(value = "estate_type")
    private String estate_type;//房产类型
    @JsonProperty(value = "loan_amt")
    private BigDecimal loan_amt;//贷款金额
    @JsonProperty(value = "loan_term")
    private Integer loan_term;//贷款期限
    @JsonProperty(value = "upper_limit")
    private BigDecimal upper_limit;//上限控制额度
    @JsonProperty(value = "regu_rate")
    private BigDecimal regu_rate;//银行规定利率
    @JsonProperty(value = "car_sale_price")
    private BigDecimal car_sale_price;//车辆销售价格

    public String getBusi_type() {
        return busi_type;
    }

    public void setBusi_type(String busi_type) {
        this.busi_type = busi_type;
    }

    public String getRepay_type() {
        return repay_type;
    }

    public void setRepay_type(String repay_type) {
        this.repay_type = repay_type;
    }

    public String getGuar_type() {
        return guar_type;
    }

    public void setGuar_type(String guar_type) {
        this.guar_type = guar_type;
    }

    public BigDecimal getCredit_amt_div_eval_val() {
        return credit_amt_div_eval_val;
    }

    public void setCredit_amt_div_eval_val(BigDecimal credit_amt_div_eval_val) {
        this.credit_amt_div_eval_val = credit_amt_div_eval_val;
    }

    public BigDecimal getCredit_amt_div_firm_value() {
        return credit_amt_div_firm_value;
    }

    public void setCredit_amt_div_firm_value(BigDecimal credit_amt_div_firm_value) {
        this.credit_amt_div_firm_value = credit_amt_div_firm_value;
    }

    public String getHouse_prop() {
        return house_prop;
    }

    public void setHouse_prop(String house_prop) {
        this.house_prop = house_prop;
    }

    public String getEstate_type() {
        return estate_type;
    }

    public void setEstate_type(String estate_type) {
        this.estate_type = estate_type;
    }

    public BigDecimal getLoan_amt() {
        return loan_amt;
    }

    public void setLoan_amt(BigDecimal loan_amt) {
        this.loan_amt = loan_amt;
    }

    public Integer getLoan_term() {
        return loan_term;
    }

    public void setLoan_term(Integer loan_term) {
        this.loan_term = loan_term;
    }

    public BigDecimal getUpper_limit() {
        return upper_limit;
    }

    public void setUpper_limit(BigDecimal upper_limit) {
        this.upper_limit = upper_limit;
    }

    public BigDecimal getRegu_rate() {
        return regu_rate;
    }

    public void setRegu_rate(BigDecimal regu_rate) {
        this.regu_rate = regu_rate;
    }

    public BigDecimal getCar_sale_price() {
        return car_sale_price;
    }

    public void setCar_sale_price(BigDecimal car_sale_price) {
        this.car_sale_price = car_sale_price;
    }

    @Override
    public String toString() {
        return "LoanList{" +
                "busi_type='" + busi_type + '\'' +
                ", repay_type='" + repay_type + '\'' +
                ", guar_type='" + guar_type + '\'' +
                ", credit_amt_div_eval_val=" + credit_amt_div_eval_val +
                ", credit_amt_div_firm_value=" + credit_amt_div_firm_value +
                ", house_prop='" + house_prop + '\'' +
                ", estate_type='" + estate_type + '\'' +
                ", loan_amt='" + loan_amt + '\'' +
                ", loan_term=" + loan_term +
                ", upper_limit=" + upper_limit +
                ", regu_rate=" + regu_rate +
                ", car_sale_price=" + car_sale_price +
                '}';
    }
}
