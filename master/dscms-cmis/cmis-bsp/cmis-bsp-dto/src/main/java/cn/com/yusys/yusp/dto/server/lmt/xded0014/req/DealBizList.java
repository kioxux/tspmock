package cn.com.yusys.yusp.dto.server.lmt.xded0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：台账恢复接口
 */
@JsonPropertyOrder(alphabetic = true)
public class DealBizList implements Serializable {
    private static final long serialVersionUID = 1L;
    //台账编号
    @JsonProperty(value = "dealBizNo")
    private String dealBizNo ;

    //恢复类型
    @JsonProperty(value = "recoverType")
    private String recoverType ;

    //恢复总额(人民币)
    @JsonProperty(value = "recoverAmtCny")
    private BigDecimal recoverAmtCny ;

    //恢复敞口金额(人民币)
    @JsonProperty(value = "recoverSpacAmtCny")
    private BigDecimal recoverSpacAmtCny ;

    //追加后保证金比例
    @JsonProperty(value = "dealBizBailPreRate")
    private BigDecimal dealBizBailPreRate ;

    //追加后保证金比例
    @JsonProperty(value = "securityAmt")
    private BigDecimal securityAmt ;

    //追加后保证金比例
    @JsonProperty(value = "recoverStd")
    private String recoverStd ;

    //追加后保证金比例
    @JsonProperty(value = "balanceAmtCny")
    private BigDecimal balanceAmtCny ;

    //追加后保证金比例
    @JsonProperty(value = "spacBalanceAmtCny")
    private BigDecimal spacBalanceAmtCny ;

    //信用证修改总额（人民币）
    @JsonProperty(value = "creditTotal")
    private BigDecimal creditTotal ;

    //信用证修改总敞口金额（人民币）
    @JsonProperty(value = "creditSpacTotal")
    private BigDecimal creditSpacTotal ;

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getRecoverType() {
        return recoverType;
    }

    public void setRecoverType(String recoverType) {
        this.recoverType = recoverType;
    }

    public BigDecimal getRecoverAmtCny() {
        return recoverAmtCny;
    }

    public void setRecoverAmtCny(BigDecimal recoverAmtCny) {
        this.recoverAmtCny = recoverAmtCny;
    }

    public BigDecimal getRecoverSpacAmtCny() {
        return recoverSpacAmtCny;
    }

    public void setRecoverSpacAmtCny(BigDecimal recoverSpacAmtCny) {
        this.recoverSpacAmtCny = recoverSpacAmtCny;
    }

    public BigDecimal getDealBizBailPreRate() {
        return dealBizBailPreRate;
    }

    public void setDealBizBailPreRate(BigDecimal dealBizBailPreRate) {
        this.dealBizBailPreRate = dealBizBailPreRate;
    }

    public BigDecimal getSecurityAmt() {
        return securityAmt;
    }

    public void setSecurityAmt(BigDecimal securityAmt) {
        this.securityAmt = securityAmt;
    }

    public String getRecoverStd() {
        return recoverStd;
    }

    public void setRecoverStd(String recoverStd) {
        this.recoverStd = recoverStd;
    }

    public BigDecimal getBalanceAmtCny() {
        return balanceAmtCny;
    }

    public void setBalanceAmtCny(BigDecimal balanceAmtCny) {
        this.balanceAmtCny = balanceAmtCny;
    }

    public BigDecimal getSpacBalanceAmtCny() {
        return spacBalanceAmtCny;
    }

    public void setSpacBalanceAmtCny(BigDecimal spacBalanceAmtCny) {
        this.spacBalanceAmtCny = spacBalanceAmtCny;
    }

    public BigDecimal getCreditTotal() {
        return creditTotal;
    }

    public void setCreditTotal(BigDecimal creditTotal) {
        this.creditTotal = creditTotal;
    }

    public BigDecimal getCreditSpacTotal() {
        return creditSpacTotal;
    }

    public void setCreditSpacTotal(BigDecimal creditSpacTotal) {
        this.creditSpacTotal = creditSpacTotal;
    }

    @Override
    public String toString() {
        return "DealBizList{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", recoverType='" + recoverType + '\'' +
                ", recoverAmtCny=" + recoverAmtCny +
                ", recoverSpacAmtCny=" + recoverSpacAmtCny +
                ", dealBizBailPreRate=" + dealBizBailPreRate +
                ", securityAmt=" + securityAmt +
                ", recoverStd='" + recoverStd + '\'' +
                ", balanceAmtCny=" + balanceAmtCny +
                ", spacBalanceAmtCny=" + spacBalanceAmtCny +
                ", creditTotal=" + creditTotal +
                ", creditSpacTotal=" + creditSpacTotal +
                '}';
    }
}
