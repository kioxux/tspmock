package cn.com.yusys.yusp.dto.client.esb.core.ln3138;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：等额等本贷款推算
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3138RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "lixijine")
    private BigDecimal lixijine;//利息金额
    @JsonProperty(value = "hkmingx")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3138.Hkmingx> hkmingx;//还款明细[LIST]

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getLixijine() {
        return lixijine;
    }

    public void setLixijine(BigDecimal lixijine) {
        this.lixijine = lixijine;
    }

    public List<Hkmingx> getHkmingx() {
        return hkmingx;
    }

    public void setHkmingx(List<Hkmingx> hkmingx) {
        this.hkmingx = hkmingx;
    }

    @Override
    public String toString() {
        return "Ln3138RespDto{" +
                "huankfsh='" + huankfsh + '\'' +
                ", huankjee=" + huankjee +
                ", zhchbjin=" + zhchbjin +
                ", zhchlilv=" + zhchlilv +
                ", lixijine=" + lixijine +
                ", hkmingx=" + hkmingx +
                '}';
    }
}
