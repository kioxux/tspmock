package cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷记入账状态查询申请往帐
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class DjztcxReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "hvflag")
    private String hvflag;//大小额交易标志
    @JsonProperty(value = "functp")
    private String functp;//业务类型
    @JsonProperty(value = "subprc")
    private String subprc;//前台交易码
    @JsonProperty(value = "oribusinum")
    private String oribusinum;//原业务受理编号
    @JsonProperty(value = "businum")
    private String businum;//业务受理编号

    @JsonProperty(value = "brchno")
    private String brchno;//    部门号,取账务机构号

    public String getHvflag() {
        return hvflag;
    }

    public void setHvflag(String hvflag) {
        this.hvflag = hvflag;
    }

    public String getFunctp() {
        return functp;
    }

    public void setFunctp(String functp) {
        this.functp = functp;
    }

    public String getSubprc() {
        return subprc;
    }

    public void setSubprc(String subprc) {
        this.subprc = subprc;
    }

    public String getOribusinum() {
        return oribusinum;
    }

    public void setOribusinum(String oribusinum) {
        this.oribusinum = oribusinum;
    }

    public String getBusinum() {
        return businum;
    }

    public void setBusinum(String businum) {
        this.businum = businum;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    @Override
    public String toString() {
        return "DjztcxReqDto{" +
                "hvflag='" + hvflag + '\'' +
                ", functp='" + functp + '\'' +
                ", subprc='" + subprc + '\'' +
                ", oribusinum='" + oribusinum + '\'' +
                ", businum='" + businum + '\'' +
                ", brchno='" + brchno + '\'' +
                '}';
    }
}
