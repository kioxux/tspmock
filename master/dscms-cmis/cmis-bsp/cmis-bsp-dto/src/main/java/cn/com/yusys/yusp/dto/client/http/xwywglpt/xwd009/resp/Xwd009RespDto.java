package cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：利率申请接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwd009RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "code")
    private String code;//响应码
    @JsonProperty(value = "message")
    private String message;//响应信息
    @JsonProperty(value = "businessId")
    private String businessId;//行内业务流水号
    @JsonProperty(value = "reqId")
    private String reqId;//利率申请业务流水号
    @JsonProperty(value = "isApproval")
    private String isApproval;//是否需要审批
    @JsonProperty(value = "data")
    private String data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getIsApproval() {
        return isApproval;
    }

    public void setIsApproval(String isApproval) {
        this.isApproval = isApproval;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xwd009RespDto{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", businessId='" + businessId + '\'' +
                ", reqId='" + reqId + '\'' +
                ", isApproval='" + isApproval + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
