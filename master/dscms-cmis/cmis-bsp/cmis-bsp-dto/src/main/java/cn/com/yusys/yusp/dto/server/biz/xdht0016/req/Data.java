package cn.com.yusys.yusp.dto.server.biz.xdht0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "servdt")
    private String servdt;//请求方日期
    @JsonProperty(value = "servti")
    private String servti;//请求方时间
    @JsonProperty(value = "ipaddr")
    private String ipaddr;//请求方IP
    @JsonProperty(value = "mac")
    private String mac;//请求方MAC
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号码
    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号
    @JsonProperty(value = "grtContNo")
    private String grtContNo;//担保合同号
    @JsonProperty(value = "mainClaimsPldContNo")
    private String mainClaimsPldContNo;//主债权合同及抵押合同表编号
    @JsonProperty(value = "realEstateRegiAppbookNo")
    private String realEstateRegiAppbookNo;//不动产登记申请书编号
    @JsonProperty(value = "loanContSignStatus")
    private String loanContSignStatus;//借款合同签约状态
    @JsonProperty(value = "grtContSignStatus")
    private String grtContSignStatus;//担保合同签约状态
    @JsonProperty(value = "mainClaimsPldContSignStatus")
    private String mainClaimsPldContSignStatus;//主债权合同及抵押合同表签约状态
    @JsonProperty(value = "realEstateRegiAppbookSignStatus")
    private String realEstateRegiAppbookSignStatus;//不动产登记申请书签约状态
    @JsonProperty(value = "imageCatalog")
    private String imageCatalog;//影像目录
    @JsonProperty(value = "loanContImageSerno")
    private String loanContImageSerno;//借款合同影像流水号
    @JsonProperty(value = "grtContImageSerno")
    private String grtContImageSerno;//担保合同影像流水号
    @JsonProperty(value = "mainClaimsContImageSerno")
    private String mainClaimsContImageSerno;//主债权合同影像流水号
    @JsonProperty(value = "regiAppbookImageSerno")
    private String regiAppbookImageSerno;//登记申请书影像流水号
    @JsonProperty(value = "isMborrow")
    private String isMborrow;//是否主借款人

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getGrtContNo() {
        return grtContNo;
    }

    public void setGrtContNo(String grtContNo) {
        this.grtContNo = grtContNo;
    }

    public String getMainClaimsPldContNo() {
        return mainClaimsPldContNo;
    }

    public void setMainClaimsPldContNo(String mainClaimsPldContNo) {
        this.mainClaimsPldContNo = mainClaimsPldContNo;
    }

    public String getRealEstateRegiAppbookNo() {
        return realEstateRegiAppbookNo;
    }

    public void setRealEstateRegiAppbookNo(String realEstateRegiAppbookNo) {
        this.realEstateRegiAppbookNo = realEstateRegiAppbookNo;
    }

    public String getLoanContSignStatus() {
        return loanContSignStatus;
    }

    public void setLoanContSignStatus(String loanContSignStatus) {
        this.loanContSignStatus = loanContSignStatus;
    }

    public String getGrtContSignStatus() {
        return grtContSignStatus;
    }

    public void setGrtContSignStatus(String grtContSignStatus) {
        this.grtContSignStatus = grtContSignStatus;
    }

    public String getMainClaimsPldContSignStatus() {
        return mainClaimsPldContSignStatus;
    }

    public void setMainClaimsPldContSignStatus(String mainClaimsPldContSignStatus) {
        this.mainClaimsPldContSignStatus = mainClaimsPldContSignStatus;
    }

    public String getRealEstateRegiAppbookSignStatus() {
        return realEstateRegiAppbookSignStatus;
    }

    public void setRealEstateRegiAppbookSignStatus(String realEstateRegiAppbookSignStatus) {
        this.realEstateRegiAppbookSignStatus = realEstateRegiAppbookSignStatus;
    }

    public String getImageCatalog() {
        return imageCatalog;
    }

    public void setImageCatalog(String imageCatalog) {
        this.imageCatalog = imageCatalog;
    }

    public String getLoanContImageSerno() {
        return loanContImageSerno;
    }

    public void setLoanContImageSerno(String loanContImageSerno) {
        this.loanContImageSerno = loanContImageSerno;
    }

    public String getGrtContImageSerno() {
        return grtContImageSerno;
    }

    public void setGrtContImageSerno(String grtContImageSerno) {
        this.grtContImageSerno = grtContImageSerno;
    }

    public String getMainClaimsContImageSerno() {
        return mainClaimsContImageSerno;
    }

    public void setMainClaimsContImageSerno(String mainClaimsContImageSerno) {
        this.mainClaimsContImageSerno = mainClaimsContImageSerno;
    }

    public String getRegiAppbookImageSerno() {
        return regiAppbookImageSerno;
    }

    public void setRegiAppbookImageSerno(String regiAppbookImageSerno) {
        this.regiAppbookImageSerno = regiAppbookImageSerno;
    }

    public String getIsMborrow() {
        return isMborrow;
    }

    public void setIsMborrow(String isMborrow) {
        this.isMborrow = isMborrow;
    }

    @Override
    public String toString() {
        return "Data{" +
                "servsq='" + servsq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", mac='" + mac + '\'' +
                ", certNo='" + certNo + '\'' +
                ", loanContNo='" + loanContNo + '\'' +
                ", grtContNo='" + grtContNo + '\'' +
                ", mainClaimsPldContNo='" + mainClaimsPldContNo + '\'' +
                ", realEstateRegiAppbookNo='" + realEstateRegiAppbookNo + '\'' +
                ", loanContSignStatus='" + loanContSignStatus + '\'' +
                ", grtContSignStatus='" + grtContSignStatus + '\'' +
                ", mainClaimsPldContSignStatus='" + mainClaimsPldContSignStatus + '\'' +
                ", realEstateRegiAppbookSignStatus='" + realEstateRegiAppbookSignStatus + '\'' +
                ", imageCatalog='" + imageCatalog + '\'' +
                ", loanContImageSerno='" + loanContImageSerno + '\'' +
                ", grtContImageSerno='" + grtContImageSerno + '\'' +
                ", mainClaimsContImageSerno='" + mainClaimsContImageSerno + '\'' +
                ", regiAppbookImageSerno='" + regiAppbookImageSerno + '\'' +
                ", isMborrow='" + isMborrow + '\'' +
                '}';
    }
}
