package cn.com.yusys.yusp.dto.server.biz.xdcz0029.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：网银推送省心快贷审核任务至信贷
 *
 * @Author zhangpeng
 * @Date 2021/4/27 21:52
 * @Version 1.0
 */
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "flag")
    private String flag;//标志
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//放款金额
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//发放日期
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//到期日期
    @JsonProperty(value = "loanAcctNo")
    private String loanAcctNo;//放款账号
    @JsonProperty(value = "entruPayAcctNo")
    private String entruPayAcctNo;//受托支付账号
    @JsonProperty(value = "entruPayAcctName")
    private String entruPayAcctName;//受托支付账号中文
    @JsonProperty(value = "amt")
    private BigDecimal amt;//金额
    @JsonProperty(value = "isBankFlag")
    private String isBankFlag;//是否行内行外
    @JsonProperty(value = "acctsvcrAcctNo")
    private String acctsvcrAcctNo;//开户行号
    @JsonProperty(value = "acctsvcrName")
    private String acctsvcrName;//开户行名
    @JsonProperty(value = "entruPayAcctNoNum")
    private String entruPayAcctNoNum;//受托支付账户数

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanAcctNo() {
        return loanAcctNo;
    }

    public void setLoanAcctNo(String loanAcctNo) {
        this.loanAcctNo = loanAcctNo;
    }

    public String getEntruPayAcctNo() {
        return entruPayAcctNo;
    }

    public void setEntruPayAcctNo(String entruPayAcctNo) {
        this.entruPayAcctNo = entruPayAcctNo;
    }

    public String getEntruPayAcctName() {
        return entruPayAcctName;
    }

    public void setEntruPayAcctName(String entruPayAcctName) {
        this.entruPayAcctName = entruPayAcctName;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public String getIsBankFlag() {
        return isBankFlag;
    }

    public void setIsBankFlag(String isBankFlag) {
        this.isBankFlag = isBankFlag;
    }

    public String getAcctsvcrAcctNo() {
        return acctsvcrAcctNo;
    }

    public void setAcctsvcrAcctNo(String acctsvcrAcctNo) {
        this.acctsvcrAcctNo = acctsvcrAcctNo;
    }

    public String getAcctsvcrName() {
        return acctsvcrName;
    }

    public void setAcctsvcrName(String acctsvcrName) {
        this.acctsvcrName = acctsvcrName;
    }

    public String getEntruPayAcctNoNum() {
        return entruPayAcctNoNum;
    }

    public void setEntruPayAcctNoNum(String entruPayAcctNoNum) {
        this.entruPayAcctNoNum = entruPayAcctNoNum;
    }

    @Override
    public String toString() {
        return "Xdcz0028ReqDto{" +
                "flag='" + flag + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "billNo='" + billNo + '\'' +
                "contNo='" + contNo + '\'' +
                "loanAmt='" + loanAmt + '\'' +
                "loanStartDate='" + loanStartDate + '\'' +
                "loanEndDate='" + loanEndDate + '\'' +
                "loanAcctNo='" + loanAcctNo + '\'' +
                "entruPayAcctNo='" + entruPayAcctNo + '\'' +
                "entruPayAcctName='" + entruPayAcctName + '\'' +
                "amt='" + amt + '\'' +
                "isBankFlag='" + isBankFlag + '\'' +
                "acctsvcrAcctNo='" + acctsvcrAcctNo + '\'' +
                "acctsvcrName='" + acctsvcrName + '\'' +
                "entruPayAcctNoNum='" + entruPayAcctNoNum + '\'' +
                '}';
    }
}
