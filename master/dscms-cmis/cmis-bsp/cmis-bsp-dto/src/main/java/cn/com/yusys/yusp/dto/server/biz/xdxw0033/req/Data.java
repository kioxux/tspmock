package cn.com.yusys.yusp.dto.server.biz.xdxw0033.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据流水号查询征信报告关联信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//调查流水号

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "indgtSerno='" + indgtSerno + '\'' +
                '}';
    }
}
