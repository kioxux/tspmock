package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//企业获取客户最新授权
@JsonPropertyOrder(alphabetic = true)
public class R006 implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CREDITDOCID")
    private String CREDITDOCID;// 影像编号
    @JsonIgnore
    public String getCREDITDOCID() {
        return CREDITDOCID;
    }
    @JsonIgnore
    public void setCREDITDOCID(String CREDITDOCID) {
        this.CREDITDOCID = CREDITDOCID;
    }

    @Override
    public String toString() {
        return "R006{" +
                "CREDITDOCID='" + CREDITDOCID + '\'' +
                '}';
    }
}
