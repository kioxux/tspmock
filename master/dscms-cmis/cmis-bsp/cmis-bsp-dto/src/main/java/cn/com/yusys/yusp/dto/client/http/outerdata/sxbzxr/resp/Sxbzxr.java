package cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/21 11:13
 * @since 2021/8/21 11:13
 */
@JsonPropertyOrder(alphabetic = true)
public class Sxbzxr implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xb")
    private String xb;//性别
    @JsonProperty(value = "zzjgdm")
    private String zzjgdm;//组织机构代码
    @JsonProperty(value = "qyfr")
    private String qyfr;//企业法人
    @JsonProperty(value = "zxfy")
    private String zxfy;//执行法院
    @JsonProperty(value = "sf")
    private String sf;//省份
    @JsonProperty(value = "zxyjwh")
    private String zxyjwh;//执行依据文号
    @JsonProperty(value = "larq")
    private String larq;//立案时间（日期）
    @JsonProperty(value = "ah")
    private String ah;//案号
    @JsonProperty(value = "zxyjdw")
    private String zxyjdw;//出执行依据单位
    @JsonProperty(value = "yw")
    private String yw;//生效法律文书确定的义务
    @JsonProperty(value = "lxqk")
    private String lxqk;//被执行人的履行情况
    @JsonProperty(value = "xwqx")
    private String xwqx;//失信被执行人行为具体情形
    @JsonProperty(value = "fbrq")
    private String fbrq;//发布时间（日期）

    public String getXb() {
        return xb;
    }

    public void setXb(String xb) {
        this.xb = xb;
    }

    public String getZzjgdm() {
        return zzjgdm;
    }

    public void setZzjgdm(String zzjgdm) {
        this.zzjgdm = zzjgdm;
    }

    public String getQyfr() {
        return qyfr;
    }

    public void setQyfr(String qyfr) {
        this.qyfr = qyfr;
    }

    public String getZxfy() {
        return zxfy;
    }

    public void setZxfy(String zxfy) {
        this.zxfy = zxfy;
    }

    public String getSf() {
        return sf;
    }

    public void setSf(String sf) {
        this.sf = sf;
    }

    public String getZxyjwh() {
        return zxyjwh;
    }

    public void setZxyjwh(String zxyjwh) {
        this.zxyjwh = zxyjwh;
    }

    public String getLarq() {
        return larq;
    }

    public void setLarq(String larq) {
        this.larq = larq;
    }

    public String getAh() {
        return ah;
    }

    public void setAh(String ah) {
        this.ah = ah;
    }

    public String getZxyjdw() {
        return zxyjdw;
    }

    public void setZxyjdw(String zxyjdw) {
        this.zxyjdw = zxyjdw;
    }

    public String getYw() {
        return yw;
    }

    public void setYw(String yw) {
        this.yw = yw;
    }

    public String getLxqk() {
        return lxqk;
    }

    public void setLxqk(String lxqk) {
        this.lxqk = lxqk;
    }

    public String getXwqx() {
        return xwqx;
    }

    public void setXwqx(String xwqx) {
        this.xwqx = xwqx;
    }

    public String getFbrq() {
        return fbrq;
    }

    public void setFbrq(String fbrq) {
        this.fbrq = fbrq;
    }

    @Override
    public String toString() {
        return "Sxbzxr{" +
                "xb='" + xb + '\'' +
                ", zzjgdm='" + zzjgdm + '\'' +
                ", qyfr='" + qyfr + '\'' +
                ", zxfy='" + zxfy + '\'' +
                ", sf='" + sf + '\'' +
                ", zxyjwh='" + zxyjwh + '\'' +
                ", larq='" + larq + '\'' +
                ", ah='" + ah + '\'' +
                ", zxyjdw='" + zxyjdw + '\'' +
                ", yw='" + yw + '\'' +
                ", lxqk='" + lxqk + '\'' +
                ", xwqx='" + xwqx + '\'' +
                ", fbrq='" + fbrq + '\'' +
                '}';
    }
}
