package cn.com.yusys.yusp.dto.client.esb.core.ln3005;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：机构控制信息对象
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstcpsyfw implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xzhileix")
    private String xzhileix;//限制类型
    @JsonProperty(value = "shiyleix")
    private String shiyleix;//适用类型
    @JsonProperty(value = "jigshybz")
    private String jigshybz;//机构适用标志
    @JsonProperty(value = "shiyfanw")
    private String shiyfanw;//适用范围

    public String getXzhileix() {
        return xzhileix;
    }

    public void setXzhileix(String xzhileix) {
        this.xzhileix = xzhileix;
    }

    public String getShiyleix() {
        return shiyleix;
    }

    public void setShiyleix(String shiyleix) {
        this.shiyleix = shiyleix;
    }

    public String getJigshybz() {
        return jigshybz;
    }

    public void setJigshybz(String jigshybz) {
        this.jigshybz = jigshybz;
    }

    public String getShiyfanw() {
        return shiyfanw;
    }

    public void setShiyfanw(String shiyfanw) {
        this.shiyfanw = shiyfanw;
    }

    @Override
    public String toString() {
        return "Lstcpsyfw{" +
                "xzhileix='" + xzhileix + '\'' +
                "shiyleix='" + shiyleix + '\'' +
                "jigshybz='" + jigshybz + '\'' +
                "shiyfanw='" + shiyfanw + '\'' +
                '}';
    }
}  
