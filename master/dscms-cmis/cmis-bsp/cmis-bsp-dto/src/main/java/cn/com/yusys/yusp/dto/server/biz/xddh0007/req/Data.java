package cn.com.yusys.yusp.dto.server.biz.xddh0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：还款记录列表查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "startPageNum")
    private Integer startPageNum;//起始页数
    @JsonProperty(value = "pageSize")
    private Integer pageSize;//分页大小

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Integer getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(Integer startPageNum) {
        this.startPageNum = startPageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billNo='" + billNo + '\'' +
                "startPageNum='" + startPageNum + '\'' +
                "pageSize='" + pageSize + '\'' +
                '}';
    }
}  
