package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：对客借据与内部借据关联关系
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdknbgl implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "nbjiejuh")
    private String nbjiejuh;//内部借据号
    @JsonProperty(value = "nbjjxzhi")
    private String nbjjxzhi;//内部借据性质

    public String getNbjiejuh() {
        return nbjiejuh;
    }

    public void setNbjiejuh(String nbjiejuh) {
        this.nbjiejuh = nbjiejuh;
    }

    public String getNbjjxzhi() {
        return nbjjxzhi;
    }

    public void setNbjjxzhi(String nbjjxzhi) {
        this.nbjjxzhi = nbjjxzhi;
    }

    @Override
    public String toString() {
        return "LstdknbglRespDto{" +
                "nbjiejuh='" + nbjiejuh + '\'' +
                "nbjjxzhi='" + nbjjxzhi + '\'' +
                '}';
    }
}  
