package cn.com.yusys.yusp.dto.server.lmt.xded0058.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    //系统编号
    @JsonProperty(value = "sysNo")
    private String sysNo;
    //恢复批次号
    @JsonProperty(value = "serno")
    private String serno;

    //恢复类型
    @JsonProperty(value = "recoverType")
    private String recoverType;

    //登记人
    @JsonProperty(value = "inputId")
    private String inputId;

    //登记机构
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    //登记日期
    @JsonProperty(value = "inputDate")
    private String inputDate;

    //恢复额度明细
    @JsonProperty(value = "recoverList")
    private List<RecoverListDto> recoverListDtoList;

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public List<RecoverListDto> getRecoverListDtoList() {
        return recoverListDtoList;
    }

    public void setRecoverListDtoList(List<RecoverListDto> recoverListDtoList) {
        this.recoverListDtoList = recoverListDtoList;
    }

    public String getRecoverType() {
        return recoverType;
    }

    public void setRecoverType(String recoverType) {
        this.recoverType = recoverType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "sysNo='" + sysNo + '\'' +
                ", serno='" + serno + '\'' +
                ", recoverType='" + recoverType + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", recoverListDtoList=" + recoverListDtoList +
                '}';
    }
}
