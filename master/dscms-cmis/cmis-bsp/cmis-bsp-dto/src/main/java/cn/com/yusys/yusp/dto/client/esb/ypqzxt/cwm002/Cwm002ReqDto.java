package cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 请求DTO：本异地借阅出库接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Cwm002ReqDto {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "userName")
    private String userName; // 柜员名称
    @JsonProperty(value = "orgcode")
    private String orgcode; // 操作员机构号
    @JsonProperty(value = "orgname")
    private String orgname; // 操作员机构名称
    @JsonProperty(value = "acctBrch")
    private String acctBrch; // 账务机构
    @JsonProperty(value = "acctBrchName")
    private String acctBrchName; // 账务机构名称
    @JsonProperty(value = "gageId")
    private String gageId; // 核心担保品编号
    @JsonProperty(value = "gageType")
    private String gageType; // 抵质押类型
    @JsonProperty(value = "gageTypeName")
    private String gageTypeName; // 抵质押类型名称
    @JsonProperty(value = "gageUser")
    private String gageUser; // 抵押人
    @JsonProperty(value = "inType")
    private String inType; // 入库类型
    @JsonProperty(value = "isEstate")
    private String isEstate; // 是否张家港地区不动产
    @JsonProperty(value = "isElectronic")
    private String isElectronic; // 是否电子类押品
    @JsonProperty(value = "isMortgage")
    private String isMortgage; // 是否住房按揭
    @JsonProperty(value = "jyid")
    private String jyid; // 借阅ID
    @JsonProperty(value = "maxAmt")
    private String maxAmt; // 权利价值
    @JsonProperty(value = "pawn")
    private String pawn; // 抵押物名称
    @JsonProperty(value = "remark")
    private String remark; // 备注描述
    @JsonProperty(value = "isyd")
    private String isyd; // 是否异地支行


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getAcctBrch() {
        return acctBrch;
    }

    public void setAcctBrch(String acctBrch) {
        this.acctBrch = acctBrch;
    }

    public String getAcctBrchName() {
        return acctBrchName;
    }

    public void setAcctBrchName(String acctBrchName) {
        this.acctBrchName = acctBrchName;
    }

    public String getGageId() {
        return gageId;
    }

    public void setGageId(String gageId) {
        this.gageId = gageId;
    }

    public String getGageType() {
        return gageType;
    }

    public void setGageType(String gageType) {
        this.gageType = gageType;
    }

    public String getGageTypeName() {
        return gageTypeName;
    }

    public void setGageTypeName(String gageTypeName) {
        this.gageTypeName = gageTypeName;
    }

    public String getGageUser() {
        return gageUser;
    }

    public void setGageUser(String gageUser) {
        this.gageUser = gageUser;
    }

    public String getInType() {
        return inType;
    }

    public void setInType(String inType) {
        this.inType = inType;
    }

    public String getIsEstate() {
        return isEstate;
    }

    public void setIsEstate(String isEstate) {
        this.isEstate = isEstate;
    }

    public String getIsElectronic() {
        return isElectronic;
    }

    public void setIsElectronic(String isElectronic) {
        this.isElectronic = isElectronic;
    }

    public String getIsMortgage() {
        return isMortgage;
    }

    public void setIsMortgage(String isMortgage) {
        this.isMortgage = isMortgage;
    }

    public String getJyid() {
        return jyid;
    }

    public void setJyid(String jyid) {
        this.jyid = jyid;
    }

    public String getMaxAmt() {
        return maxAmt;
    }

    public void setMaxAmt(String maxAmt) {
        this.maxAmt = maxAmt;
    }

    public String getPawn() {
        return pawn;
    }

    public void setPawn(String pawn) {
        this.pawn = pawn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIsyd() {
        return isyd;
    }

    public void setIsyd(String isyd) {
        this.isyd = isyd;
    }

    @Override
    public String toString() {
        return "Cwm002ReqDto{" +
                "userName='" + userName + '\'' +
                ", orgcode='" + orgcode + '\'' +
                ", orgname='" + orgname + '\'' +
                ", acctBrch='" + acctBrch + '\'' +
                ", acctBrchName='" + acctBrchName + '\'' +
                ", gageId='" + gageId + '\'' +
                ", gageType='" + gageType + '\'' +
                ", gageTypeName='" + gageTypeName + '\'' +
                ", gageUser='" + gageUser + '\'' +
                ", inType='" + inType + '\'' +
                ", isEstate='" + isEstate + '\'' +
                ", isElectronic='" + isElectronic + '\'' +
                ", isMortgage='" + isMortgage + '\'' +
                ", jyid='" + jyid + '\'' +
                ", maxAmt='" + maxAmt + '\'' +
                ", pawn='" + pawn + '\'' +
                ", remark='" + remark + '\'' +
                ", isyd='" + isyd + '\'' +
                '}';
    }
}
