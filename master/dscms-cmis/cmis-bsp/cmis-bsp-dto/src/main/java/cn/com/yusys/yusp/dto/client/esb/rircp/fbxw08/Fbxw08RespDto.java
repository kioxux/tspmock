package cn.com.yusys.yusp.dto.client.esb.rircp.fbxw08;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
/**
 * 响应DTO：授信审批作废接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/16 下午4:47:11
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxw08RespDto implements Serializable {
    // 此接口不返回业务字段
}
