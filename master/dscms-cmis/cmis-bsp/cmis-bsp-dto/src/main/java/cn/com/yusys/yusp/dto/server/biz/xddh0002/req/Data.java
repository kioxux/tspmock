package cn.com.yusys.yusp.dto.server.biz.xddh0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/22 13:47:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 13:47
 * @since 2021/5/22 13:47
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "repayChannel")
    private String repayChannel;//还款渠道：A-手机银行；B-个人网银；C-企业网银；D-直销银行；E-企业手机；F-智能超柜
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "fstRepayMode")
    private String fstRepayMode;//还款模式；01-提前还款；02-归还拖欠
    @JsonProperty(value = "sedRepayMode")
    private String sedRepayMode;//还款模式明细 03-提前归还本金及全部利息 04-提前归还本金及本金对应利息 05-归还拖欠本息 08-灵活还款
    @JsonProperty(value = "repayAmt")
    private BigDecimal repayAmt;//还款金额
    @JsonProperty(value = "hkbj")
    private BigDecimal hkbj;//还款本金
    @JsonProperty(value = "loanReclaimType")
    private String loanReclaimType;//贷款回收方式
    @JsonProperty(value = "repayAcctNo")
    private String repayAcctNo;//还款账号
    @JsonProperty(value = "curType")
    private String curType;//账户币种
    @JsonProperty(value = "acctName")
    private String acctName;//账户名称
    @JsonProperty(value = "acctSeqNo")
    private String acctSeqNo;//账户序号
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理

    public String getRepayChannel() {
        return repayChannel;
    }

    public void setRepayChannel(String repayChannel) {
        this.repayChannel = repayChannel;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getFstRepayMode() {
        return fstRepayMode;
    }

    public void setFstRepayMode(String fstRepayMode) {
        this.fstRepayMode = fstRepayMode;
    }

    public String getSedRepayMode() {
        return sedRepayMode;
    }

    public void setSedRepayMode(String sedRepayMode) {
        this.sedRepayMode = sedRepayMode;
    }

    public BigDecimal getRepayAmt() {
        return repayAmt;
    }

    public void setRepayAmt(BigDecimal repayAmt) {
        this.repayAmt = repayAmt;
    }

    public BigDecimal getHkbj() {
        return hkbj;
    }

    public void setHkbj(BigDecimal hkbj) {
        this.hkbj = hkbj;
    }

    public String getLoanReclaimType() {
        return loanReclaimType;
    }

    public void setLoanReclaimType(String loanReclaimType) {
        this.loanReclaimType = loanReclaimType;
    }

    public String getRepayAcctNo() {
        return repayAcctNo;
    }

    public void setRepayAcctNo(String repayAcctNo) {
        this.repayAcctNo = repayAcctNo;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getAcctSeqNo() {
        return acctSeqNo;
    }

    public void setAcctSeqNo(String acctSeqNo) {
        this.acctSeqNo = acctSeqNo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Xddh0002ReqDto{" +
                "billNo='" + billNo + '\'' +
                "fstRepayMode='" + fstRepayMode + '\'' +
                "sedRepayMode='" + sedRepayMode + '\'' +
                "repayAmt='" + repayAmt + '\'' +
                "hkbj='" + hkbj + '\'' +
                "loanReclaimType='" + loanReclaimType + '\'' +
                "repayAcctNo='" + repayAcctNo + '\'' +
                "acctName='" + acctName + '\'' +
                "acctSeqNo='" + acctSeqNo + '\'' +
                "managerId='" + managerId + '\'' +
                '}';
    }
}
