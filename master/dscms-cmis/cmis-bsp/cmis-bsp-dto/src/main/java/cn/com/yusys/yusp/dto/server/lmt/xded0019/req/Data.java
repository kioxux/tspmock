package cn.com.yusys.yusp.dto.server.lmt.xded0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/10 17:07
 * @since 2021/6/10 17:07
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "isQuryGrp")
    private String isQuryGrp;//是否查询集团向下
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsQuryGrp() {
        return isQuryGrp;
    }

    public void setIsQuryGrp(String isQuryGrp) {
        this.isQuryGrp = isQuryGrp;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "instuCde='" + instuCde + '\'' +
                ", cusId='" + cusId + '\'' +
                ", isQuryGrp='" + isQuryGrp + '\'' +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}
