package cn.com.yusys.yusp.dto.server.biz.xdht0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class AccountList implements Serializable {

    @JsonProperty(value = "acct")
    private String acct;//账户
    @JsonProperty(value = "acctName")
    private String acctName;//账户名称
    @JsonProperty(value = "subAcctSeq")
    private String subAcctSeq;//子序号

    public String getAcct() {
        return acct;
    }

    public void setAcct(String acct) {
        this.acct = acct;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getSubAcctSeq() {
        return subAcctSeq;
    }

    public void setSubAcctSeq(String subAcctSeq) {
        this.subAcctSeq = subAcctSeq;
    }

    @Override
    public String toString() {
        return "AccountList{" +
                "acct='" + acct + '\'' +
                "acctName='" + acctName + '\'' +
                "subAcctSeq='" + subAcctSeq + '\'' +
                '}';
    }
}
