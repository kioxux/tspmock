package cn.com.yusys.yusp.dto.client.esb.core.ln3077.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：客户账本息调整
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3077ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "yingjitx")
    private BigDecimal yingjitx;//应计贴息
    @JsonProperty(value = "yingshtx")
    private BigDecimal yingshtx;//应收贴息
    @JsonProperty(value = "hexiaobj")
    private BigDecimal hexiaobj;//核销本金
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "yihxbjlx")
    private BigDecimal yihxbjlx;//已核销本金利息
    @JsonProperty(value = "yingshfj")
    private BigDecimal yingshfj;//应收罚金
    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额
    @JsonProperty(value = "hexiaoje")
    private BigDecimal hexiaoje;//核销来源金额
    @JsonProperty(value = "hexiaozh")
    private String hexiaozh;//核销来源账号
    @JsonProperty(value = "hexiaozx")
    private String hexiaozx;//核销来源账号子序号
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public BigDecimal getHexiaoje() {
        return hexiaoje;
    }

    public void setHexiaoje(BigDecimal hexiaoje) {
        this.hexiaoje = hexiaoje;
    }

    public String getHexiaozh() {
        return hexiaozh;
    }

    public void setHexiaozh(String hexiaozh) {
        this.hexiaozh = hexiaozh;
    }

    public String getHexiaozx() {
        return hexiaozx;
    }

    public void setHexiaozx(String hexiaozx) {
        this.hexiaozx = hexiaozx;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    @Override
    public String toString() {
        return "Ln3077ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "zhchbjin='" + zhchbjin + '\'' +
                "yuqibjin='" + yuqibjin + '\'' +
                "dzhibjin='" + dzhibjin + '\'' +
                "daizbjin='" + daizbjin + '\'' +
                "ysyjlixi='" + ysyjlixi + '\'' +
                "csyjlixi='" + csyjlixi + '\'' +
                "ysqianxi='" + ysqianxi + '\'' +
                "csqianxi='" + csqianxi + '\'' +
                "ysyjfaxi='" + ysyjfaxi + '\'' +
                "csyjfaxi='" + csyjfaxi + '\'' +
                "yshofaxi='" + yshofaxi + '\'' +
                "cshofaxi='" + cshofaxi + '\'' +
                "yingjifx='" + yingjifx + '\'' +
                "fuxiiiii='" + fuxiiiii + '\'' +
                "yingjitx='" + yingjitx + '\'' +
                "yingshtx='" + yingshtx + '\'' +
                "hexiaobj='" + hexiaobj + '\'' +
                "hexiaolx='" + hexiaolx + '\'' +
                "yihxbjlx='" + yihxbjlx + '\'' +
                "yingshfj='" + yingshfj + '\'' +
                "huankjee='" + huankjee + '\'' +
                "hexiaoje='" + hexiaoje + '\'' +
                "hexiaozh='" + hexiaozh + '\'' +
                "hexiaozx='" + hexiaozx + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                '}';
    }
}  
