package cn.com.yusys.yusp.dto.server.biz.xdtz0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据身份证号获取借据信息
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xdtz0010DataReqDto{" +
                "certNo='" + certNo + '\'' +
                '}';
    }
}
