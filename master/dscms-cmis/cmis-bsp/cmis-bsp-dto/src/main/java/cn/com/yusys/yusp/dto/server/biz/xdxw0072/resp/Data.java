package cn.com.yusys.yusp.dto.server.biz.xdxw0072.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    private String managerId;//客户经理ID
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理姓名

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    @Override
    public String toString() {
        return "Data{" +
                "managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                '}';
    }
}
