package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	股权出质信息（新）-变更信息
@JsonPropertyOrder(alphabetic = true)
public class STOCKPAWNALT implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "STK_PAWN_BGNR")
    private String STK_PAWN_BGNR;//	变更内容
    @JsonProperty(value = "STK_PAWN_BGRQ")
    private String STK_PAWN_BGRQ;//	变更日期
    @JsonProperty(value = "URL")
    private String URL;//	关联内容

    @JsonIgnore
    public String getSTK_PAWN_BGNR() {
        return STK_PAWN_BGNR;
    }

    @JsonIgnore
    public void setSTK_PAWN_BGNR(String STK_PAWN_BGNR) {
        this.STK_PAWN_BGNR = STK_PAWN_BGNR;
    }

    @JsonIgnore
    public String getSTK_PAWN_BGRQ() {
        return STK_PAWN_BGRQ;
    }

    @JsonIgnore
    public void setSTK_PAWN_BGRQ(String STK_PAWN_BGRQ) {
        this.STK_PAWN_BGRQ = STK_PAWN_BGRQ;
    }

    @JsonIgnore
    public String getURL() {
        return URL;
    }

    @JsonIgnore
    public void setURL(String URL) {
        this.URL = URL;
    }

    @Override
    public String toString() {
        return "STOCKPAWNALT{" +
                "STK_PAWN_BGNR='" + STK_PAWN_BGNR + '\'' +
                ", STK_PAWN_BGRQ='" + STK_PAWN_BGRQ + '\'' +
                ", URL='" + URL + '\'' +
                '}';
    }
}
