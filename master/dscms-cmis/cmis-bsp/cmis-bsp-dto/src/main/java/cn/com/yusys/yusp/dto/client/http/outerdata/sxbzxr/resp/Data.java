package cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/21 11:13
 * @since 2021/8/21 11:13
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sxbzxr")
    private java.util.List<Sxbzxr> sxbzxr;

    public List<Sxbzxr> getSxbzxr() {
        return sxbzxr;
    }

    public void setSxbzxr(List<Sxbzxr> sxbzxr) {
        this.sxbzxr = sxbzxr;
    }

    @Override
    public String toString() {
        return "Data{" +
                "sxbzxr=" + sxbzxr +
                '}';
    }
}
