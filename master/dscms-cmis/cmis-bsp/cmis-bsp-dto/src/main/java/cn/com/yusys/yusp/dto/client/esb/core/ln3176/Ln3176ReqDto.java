package cn.com.yusys.yusp.dto.client.esb.core.ln3176;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：本交易用户贷款多个还款账户进行还款
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3176ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "huankzle")
    private String huankzle;//还款种类
    @JsonProperty(value = "dktqhkzl")
    private String dktqhkzl;//提前还款种类
    @JsonProperty(value = "tqhktysx")
    private BigDecimal tqhktysx;//退客户预收息金额
    @JsonProperty(value = "tqhktzfs")
    private String tqhktzfs;//提前还款调整计划方式
    @JsonProperty(value = "tqhkhxfs")
    private String tqhkhxfs;//提前还款还息方式
    @JsonProperty(value = "lstLnHuankDZhhObj")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3176.LstLnHuankDZhhObj> lstLnHuankDZhhObj;//多还款账户对象

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHuankzle() {
        return huankzle;
    }

    public void setHuankzle(String huankzle) {
        this.huankzle = huankzle;
    }

    public String getDktqhkzl() {
        return dktqhkzl;
    }

    public void setDktqhkzl(String dktqhkzl) {
        this.dktqhkzl = dktqhkzl;
    }

    public BigDecimal getTqhktysx() {
        return tqhktysx;
    }

    public void setTqhktysx(BigDecimal tqhktysx) {
        this.tqhktysx = tqhktysx;
    }

    public String getTqhktzfs() {
        return tqhktzfs;
    }

    public void setTqhktzfs(String tqhktzfs) {
        this.tqhktzfs = tqhktzfs;
    }

    public String getTqhkhxfs() {
        return tqhkhxfs;
    }

    public void setTqhkhxfs(String tqhkhxfs) {
        this.tqhkhxfs = tqhkhxfs;
    }

    public List<LstLnHuankDZhhObj> getLstLnHuankDZhhObj() {
        return lstLnHuankDZhhObj;
    }

    public void setLstLnHuankDZhhObj(List<LstLnHuankDZhhObj> lstLnHuankDZhhObj) {
        this.lstLnHuankDZhhObj = lstLnHuankDZhhObj;
    }

    @Override
    public String toString() {
        return "Ln3176ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", huankzle='" + huankzle + '\'' +
                ", dktqhkzl='" + dktqhkzl + '\'' +
                ", tqhktysx=" + tqhktysx +
                ", tqhktzfs='" + tqhktzfs + '\'' +
                ", tqhkhxfs='" + tqhkhxfs + '\'' +
                ", lstLnHuankDZhhObj=" + lstLnHuankDZhhObj +
                '}';
    }
}
