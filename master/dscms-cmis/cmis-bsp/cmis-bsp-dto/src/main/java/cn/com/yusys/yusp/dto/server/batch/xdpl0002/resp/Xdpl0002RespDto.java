package cn.com.yusys.yusp.dto.server.batch.xdpl0002.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：提供日终调度平台查询批量任务状态
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpl0002RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;//data

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdpl0001RespDto{" +
                "data=" + data +
                '}';
    }
}
