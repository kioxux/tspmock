package cn.com.yusys.yusp.dto.server.cus.xdkh0025.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：优企贷、优农贷客户信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户编号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "com_init_loan_date")
    private String com_init_loan_date;//建立信贷关系时间
    @JsonProperty(value = "com_ins_code")
    private String com_ins_code;//组织机构代码
    @JsonProperty(value = "bas_acc_flg")
    private String bas_acc_flg;//基本存款账户是否在本机构
    @JsonProperty(value = "bas_acc_date")
    private String bas_acc_date;//基本存款账户开户日期

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCom_init_loan_date() {
        return com_init_loan_date;
    }

    public void setCom_init_loan_date(String com_init_loan_date) {
        this.com_init_loan_date = com_init_loan_date;
    }

    public String getCom_ins_code() {
        return com_ins_code;
    }

    public void setCom_ins_code(String com_ins_code) {
        this.com_ins_code = com_ins_code;
    }

    public String getBas_acc_flg() {
        return bas_acc_flg;
    }

    public void setBas_acc_flg(String bas_acc_flg) {
        this.bas_acc_flg = bas_acc_flg;
    }

    public String getBas_acc_date() {
        return bas_acc_date;
    }

    public void setBas_acc_date(String bas_acc_date) {
        this.bas_acc_date = bas_acc_date;
    }

    @Override
    public String toString() {
        return "List{" +
                "cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", com_init_loan_date='" + com_init_loan_date + '\'' +
                ", com_ins_code='" + com_ins_code + '\'' +
                ", bas_acc_flg='" + bas_acc_flg + '\'' +
                ", bas_acc_date='" + bas_acc_date + '\'' +
                '}';
    }
}