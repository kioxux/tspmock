package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//抵押方式
@JsonPropertyOrder(alphabetic = true)
public class MortgageMethod implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "code")
    private int code;// 代码
    @JsonProperty(value = "name")
    private String name;//名称

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MortgageMethod{" +
                "code=" + code +
                ", name='" + name + '\'' +
                '}';
    }
}
