package cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/5 14:12
 * @since 2021/6/5 14:12
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_no")
    private String guar_no;//押品统一编号
    @JsonProperty(value = "assetNo")
    private String assetNo;

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    @Override
    public String toString() {
        return "List{" +
                "guar_no='" + guar_no + '\'' +
                ", assetNo='" + assetNo + '\'' +
                '}';
    }
}
