package cn.com.yusys.yusp.dto.server.biz.xdht0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/5/6 13:45
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "unifiedCreditCode")
    private String unifiedCreditCode;//统一社会信用代码

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getUnifiedCreditCode() {
        return unifiedCreditCode;
    }

    public void setUnifiedCreditCode(String unifiedCreditCode) {
        this.unifiedCreditCode = unifiedCreditCode;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", unifiedCreditCode='" + unifiedCreditCode + '\'' +
                '}';
    }
}
