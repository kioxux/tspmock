package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款多委托人账户
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkwtxx implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bhchzibz")
    private String bhchzibz;//本行出资标志
    @JsonProperty(value = "bhjiejuh")
    private String bhjiejuh;//本行借据号
    @JsonProperty(value = "bhzhaobz")
    private String bhzhaobz;//本行账号标志
    @JsonProperty(value = "hkzjhzfs")
    private String hkzjhzfs;//还款资金划转方式
    @JsonProperty(value = "zhkaihhh")
    private String zhkaihhh;//账户开户行行号
    @JsonProperty(value = "zhkaihhm")
    private String zhkaihhm;//账户开户行行名
    @JsonProperty(value = "wtrkehuh")
    private String wtrkehuh;//委托人客户号
    @JsonProperty(value = "wtrckuzh")
    private String wtrckuzh;//委托人存款账号
    @JsonProperty(value = "wtckzhao")
    private String wtckzhao;//委托存款账号
    @JsonProperty(value = "baozjzhh")
    private String baozjzhh;//保证金账号
    @JsonProperty(value = "weituoje")
    private BigDecimal weituoje;//委托金额
    @JsonProperty(value = "wtrckzxh")
    private String wtrckzxh;//委托人存款账号子序号
    @JsonProperty(value = "wtckzixh")
    private String wtckzixh;//委托存款账号子序号
    @JsonProperty(value = "baozjzxh")
    private String baozjzxh;//保证金账号子序号
    @JsonProperty(value = "wtrmingc")
    private String wtrmingc;//委托人名称

    public String getBhchzibz() {
        return bhchzibz;
    }

    public void setBhchzibz(String bhchzibz) {
        this.bhchzibz = bhchzibz;
    }

    public String getBhjiejuh() {
        return bhjiejuh;
    }

    public void setBhjiejuh(String bhjiejuh) {
        this.bhjiejuh = bhjiejuh;
    }

    public String getBhzhaobz() {
        return bhzhaobz;
    }

    public void setBhzhaobz(String bhzhaobz) {
        this.bhzhaobz = bhzhaobz;
    }

    public String getHkzjhzfs() {
        return hkzjhzfs;
    }

    public void setHkzjhzfs(String hkzjhzfs) {
        this.hkzjhzfs = hkzjhzfs;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public String getWtrkehuh() {
        return wtrkehuh;
    }

    public void setWtrkehuh(String wtrkehuh) {
        this.wtrkehuh = wtrkehuh;
    }

    public String getWtrckuzh() {
        return wtrckuzh;
    }

    public void setWtrckuzh(String wtrckuzh) {
        this.wtrckuzh = wtrckuzh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getBaozjzhh() {
        return baozjzhh;
    }

    public void setBaozjzhh(String baozjzhh) {
        this.baozjzhh = baozjzhh;
    }

    public BigDecimal getWeituoje() {
        return weituoje;
    }

    public void setWeituoje(BigDecimal weituoje) {
        this.weituoje = weituoje;
    }

    public String getWtrckzxh() {
        return wtrckzxh;
    }

    public void setWtrckzxh(String wtrckzxh) {
        this.wtrckzxh = wtrckzxh;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getBaozjzxh() {
        return baozjzxh;
    }

    public void setBaozjzxh(String baozjzxh) {
        this.baozjzxh = baozjzxh;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    @Override
    public String toString() {
        return "LstdkwtxxRespDto{" +
                "bhchzibz='" + bhchzibz + '\'' +
                "bhjiejuh='" + bhjiejuh + '\'' +
                "bhzhaobz='" + bhzhaobz + '\'' +
                "hkzjhzfs='" + hkzjhzfs + '\'' +
                "zhkaihhh='" + zhkaihhh + '\'' +
                "zhkaihhm='" + zhkaihhm + '\'' +
                "wtrkehuh='" + wtrkehuh + '\'' +
                "wtrckuzh='" + wtrckuzh + '\'' +
                "wtckzhao='" + wtckzhao + '\'' +
                "baozjzhh='" + baozjzhh + '\'' +
                "weituoje='" + weituoje + '\'' +
                "wtrckzxh='" + wtrckzxh + '\'' +
                "wtckzixh='" + wtckzixh + '\'' +
                "baozjzxh='" + baozjzxh + '\'' +
                "wtrmingc='" + wtrmingc + '\'' +
                '}';
    }
}  
