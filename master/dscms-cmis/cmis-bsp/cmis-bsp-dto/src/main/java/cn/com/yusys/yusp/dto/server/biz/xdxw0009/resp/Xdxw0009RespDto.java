package cn.com.yusys.yusp.dto.server.biz.xdxw0009.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：担保人人数查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0009RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdxw0009RespDto{" +
				"data=" + data +
				'}';
	}
}
