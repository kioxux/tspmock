package cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：利率定价测算提交接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午8:20:33
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxw06RespDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ols_tran_no")
    private String ols_tran_no; // 交易流水号
    @JsonProperty(value = "ols_date")
    private String ols_date; // 交易日期
    @JsonProperty(value = "app_no")
    private String app_no; // 申请流水号
    @JsonProperty(value = "rate")
    private String rate; // 参考利率
    @JsonProperty(value = "result")
    private String result; // 评级结果

    public String getOls_tran_no() {
        return ols_tran_no;
    }

    public void setOls_tran_no(String ols_tran_no) {
        this.ols_tran_no = ols_tran_no;
    }

    public String getOls_date() {
        return ols_date;
    }

    public void setOls_date(String ols_date) {
        this.ols_date = ols_date;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Fbxw06RespDto{" +
                "ols_tran_no='" + ols_tran_no + '\'' +
                ", ols_date='" + ols_date + '\'' +
                ", app_no='" + app_no + '\'' +
                ", rate='" + rate + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}