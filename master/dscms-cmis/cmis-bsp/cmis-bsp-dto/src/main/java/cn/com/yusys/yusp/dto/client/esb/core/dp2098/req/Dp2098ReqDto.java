package cn.com.yusys.yusp.dto.client.esb.core.dp2098.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：待清算账户查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dp2098ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//客户账号
    @JsonProperty(value = "huobdaih")
    private String huobdaih;//币种

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    @Override
    public String toString() {
        return "Dp2098ReqDto{" +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehuzhao='" + kehuzhao + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                '}';
    }
}  
