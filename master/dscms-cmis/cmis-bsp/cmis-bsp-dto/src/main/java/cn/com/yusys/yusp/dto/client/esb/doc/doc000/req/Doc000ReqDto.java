package cn.com.yusys.yusp.dto.client.esb.doc.doc000.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：入库接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Doc000ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "orgId")
    private String orgId;//机构号
    @JsonProperty(value = "orgName")
    private String orgName;//机构名称
    @JsonProperty(value = "typeCode")
    private String typeCode;//档案类型；   信贷档案:0100050006
    @JsonProperty(value = "fileCode")
    private String fileCode;//档案代码；2001：放款资料A，2002：放款资料B
    @JsonProperty(value = "fileName")
    private String fileName;//档案名称
    @JsonProperty(value = "fileDesc")
    private String fileDesc;//备注
    @JsonProperty(value = "resourceType")
    private String resourceType;//档案来源；001:会计,002:信贷
    @JsonProperty(value = "type")
    private String type;//交易类型；001:档案登记,002:档案登记撤销
    @JsonProperty(value = "requestType")
    private String requestType;//默认 100001：信贷档案借口
    @JsonProperty(value = "opeateUserId")
    private String opeateUserId;//操作员号
    @JsonProperty(value = "opeateDate")
    private String opeateDate;//操作日期
    @JsonProperty(value = "fileNum")
    private String fileNum;//档案编号：交易类型为002必传
    @JsonProperty(value = "result")
    private String result;//对收到的信息进行处理的结果的反馈
    @JsonProperty(value = "is_aj")
    private String is_aj;//是否按揭
    @JsonProperty(value = "is_yd")
    private String is_yd;//是否异地
    @JsonProperty(value = "kfbh")
    private String kfbh;//库房（本地A 异地B 村镇C）
    @JsonProperty(value = "real_mng_br_id")
    private String real_mng_br_id;//真实的机构号
    @JsonProperty(value = "list_index")
    private List<List_index> list_index;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDesc() {
        return fileDesc;
    }

    public void setFileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getOpeateUserId() {
        return opeateUserId;
    }

    public void setOpeateUserId(String opeateUserId) {
        this.opeateUserId = opeateUserId;
    }

    public String getOpeateDate() {
        return opeateDate;
    }

    public void setOpeateDate(String opeateDate) {
        this.opeateDate = opeateDate;
    }

    public String getFileNum() {
        return fileNum;
    }

    public void setFileNum(String fileNum) {
        this.fileNum = fileNum;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getIs_aj() {
        return is_aj;
    }

    public void setIs_aj(String is_aj) {
        this.is_aj = is_aj;
    }

    public String getIs_yd() {
        return is_yd;
    }

    public void setIs_yd(String is_yd) {
        this.is_yd = is_yd;
    }

    public String getKfbh() {
        return kfbh;
    }

    public void setKfbh(String kfbh) {
        this.kfbh = kfbh;
    }

    public String getReal_mng_br_id() {
        return real_mng_br_id;
    }

    public void setReal_mng_br_id(String real_mng_br_id) {
        this.real_mng_br_id = real_mng_br_id;
    }

    public List<List_index> getList_index() {
        return list_index;
    }

    public void setList_index(List<List_index> list_index) {
        this.list_index = list_index;
    }

    @Override
    public String toString() {
        return "Doc000ReqDto{" +
                "orgId='" + orgId + '\'' +
                ", orgName='" + orgName + '\'' +
                ", typeCode='" + typeCode + '\'' +
                ", fileCode='" + fileCode + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileDesc='" + fileDesc + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", type='" + type + '\'' +
                ", requestType='" + requestType + '\'' +
                ", opeateUserId='" + opeateUserId + '\'' +
                ", opeateDate='" + opeateDate + '\'' +
                ", fileNum='" + fileNum + '\'' +
                ", result='" + result + '\'' +
                ", is_aj='" + is_aj + '\'' +
                ", is_yd='" + is_yd + '\'' +
                ", kfbh='" + kfbh + '\'' +
                ", real_mng_br_id='" + real_mng_br_id + '\'' +
                ", list_index=" + list_index +
                '}';
    }
}
