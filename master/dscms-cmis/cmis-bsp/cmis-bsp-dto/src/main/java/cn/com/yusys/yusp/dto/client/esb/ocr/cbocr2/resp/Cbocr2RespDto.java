package cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.resp;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 响应Dto：识别任务的模板匹配结果列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cbocr2RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "success")
    private String success;//结果消息
    @JsonProperty(value = "erorcd")
    private String erorcd;//状态码
    @JsonProperty(value = "erortx")
    private String erortx;//true 成功
    @JsonProperty(value = "list")
    private List<Data> list;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public List<Data> getList() {
		return list;
	}

	public void setList(List<Data> list) {
		this.list = list;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((erorcd == null) ? 0 : erorcd.hashCode());
		result = prime * result + ((erortx == null) ? 0 : erortx.hashCode());
		result = prime * result + ((list == null) ? 0 : list.hashCode());
		result = prime * result + ((success == null) ? 0 : success.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cbocr2RespDto other = (Cbocr2RespDto) obj;
		if (erorcd == null) {
			if (other.erorcd != null)
				return false;
		} else if (!erorcd.equals(other.erorcd))
			return false;
		if (erortx == null) {
			if (other.erortx != null)
				return false;
		} else if (!erortx.equals(other.erortx))
			return false;
		if (list == null) {
			if (other.list != null)
				return false;
		} else if (!list.equals(other.list))
			return false;
		if (success == null) {
			if (other.success != null)
				return false;
		} else if (!success.equals(other.success))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cbocr2RespDto [success=" + success + ", erorcd=" + erorcd
				+ ", erortx=" + erortx + ", list=" + list + "]";
	}
   
}
