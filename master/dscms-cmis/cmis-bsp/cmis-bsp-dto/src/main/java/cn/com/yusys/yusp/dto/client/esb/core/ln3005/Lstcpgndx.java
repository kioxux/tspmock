package cn.com.yusys.yusp.dto.client.esb.core.ln3005;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：贷款产品功能对象
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstcpgndx implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "gndxiang")
    private String gndxiang;//功能对象
    @JsonProperty(value = "dxmingch")
    private String dxmingch;//对象名称
    @JsonProperty(value = "bzhguize")
    private String bzhguize;//币种规则
    @JsonProperty(value = "zhidbizh")
    private String zhidbizh;//指定币种

    public String getGndxiang() {
        return gndxiang;
    }

    public void setGndxiang(String gndxiang) {
        this.gndxiang = gndxiang;
    }

    public String getDxmingch() {
        return dxmingch;
    }

    public void setDxmingch(String dxmingch) {
        this.dxmingch = dxmingch;
    }

    public String getBzhguize() {
        return bzhguize;
    }

    public void setBzhguize(String bzhguize) {
        this.bzhguize = bzhguize;
    }

    public String getZhidbizh() {
        return zhidbizh;
    }

    public void setZhidbizh(String zhidbizh) {
        this.zhidbizh = zhidbizh;
    }

    @Override
    public String toString() {
        return "Lstcpgndx{" +
                "gndxiang='" + gndxiang + '\'' +
                "dxmingch='" + dxmingch + '\'' +
                "bzhguize='" + bzhguize + '\'' +
                "zhidbizh='" + zhidbizh + '\'' +
                '}';
    }
}  
