package cn.com.yusys.yusp.dto.client.esb.yphsxt.gyypbh.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：通过共有人编号查询押品编号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class GyypbhRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_no")
    private String guar_no;//押品编号
    @JsonProperty(value = "common_owner_name")
    private String common_owner_name;//共有人客户名称
    @JsonProperty(value = "common_hold_portio")
    private BigDecimal common_hold_portio;//共有人所占份额

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    public String getCommon_owner_name() {
        return common_owner_name;
    }

    public void setCommon_owner_name(String common_owner_name) {
        this.common_owner_name = common_owner_name;
    }

    public BigDecimal getCommon_hold_portio() {
        return common_hold_portio;
    }

    public void setCommon_hold_portio(BigDecimal common_hold_portio) {
        this.common_hold_portio = common_hold_portio;
    }

    @Override
    public String toString() {
        return "GyypbhRespDto{" +
                "guar_no='" + guar_no + '\'' +
                "common_owner_name='" + common_owner_name + '\'' +
                "common_hold_portio='" + common_hold_portio + '\'' +
                '}';
    }
}  
