package cn.com.yusys.yusp.dto.server.biz.xdxw0026.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：学区信息列表查询（分页）
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "schoolAreaList")
    private List<SchoolAreaList> schoolAreaList ;//start


    public List<SchoolAreaList> getSchoolAreaList() {
        return schoolAreaList;
    }

    public void setSchoolAreaList(List<SchoolAreaList> schoolAreaList) {
        this.schoolAreaList = schoolAreaList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "schoolAreaList=" + schoolAreaList +
                '}';
    }
}
