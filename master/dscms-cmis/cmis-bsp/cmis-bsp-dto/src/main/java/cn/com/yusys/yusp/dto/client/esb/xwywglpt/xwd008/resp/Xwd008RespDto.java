package cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：新信贷同步用户账号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwd008RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "code")
    private String code;//响应码
    @JsonProperty(value = "message")
    private String message;//响应信息

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Xwd008RespDto{" +
                "code='" + code + '\'' +
                "message='" + message + '\'' +
                '}';
    }
}  
