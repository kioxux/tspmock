package cn.com.yusys.yusp.dto.server.biz.xdcz0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：查询敞口额度及保证金校验
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "toppAcctNo")
    private BigDecimal toppAcctNo;//交易对手账号
    @JsonProperty(value = "toppName")
    private String toppName;//交易对手名称
    @JsonProperty(value = "toppAmt")
    private BigDecimal toppAmt;//交易金额
    @JsonProperty(value = "toorgNo")
    private BigDecimal toorgNo;//交易对手行号
    @JsonProperty(value = "toorgName")
    private BigDecimal toorgName;//交易对手行名
    @JsonProperty(value = "isBankAcctNo")
    private BigDecimal isBankAcctNo;//是否我行账户
    @JsonProperty(value = "entruPayChangeSerno")
    private BigDecimal entruPayChangeSerno;//受托变更固定流水
    @JsonProperty(value = "isDoChange")
    private BigDecimal isDoChange;//是否进行变更
    @JsonProperty(value = "topp")
    private BigDecimal topp;//是否受托成功

    public BigDecimal getToppAcctNo() {
        return toppAcctNo;
    }

    public void setToppAcctNo(BigDecimal toppAcctNo) {
        this.toppAcctNo = toppAcctNo;
    }

    public String getToppName() {
        return toppName;
    }

    public void setToppName(String toppName) {
        this.toppName = toppName;
    }

    public BigDecimal getToppAmt() {
        return toppAmt;
    }

    public void setToppAmt(BigDecimal toppAmt) {
        this.toppAmt = toppAmt;
    }

    public BigDecimal getToorgNo() {
        return toorgNo;
    }

    public void setToorgNo(BigDecimal toorgNo) {
        this.toorgNo = toorgNo;
    }

    public BigDecimal getToorgName() {
        return toorgName;
    }

    public void setToorgName(BigDecimal toorgName) {
        this.toorgName = toorgName;
    }

    public BigDecimal getIsBankAcctNo() {
        return isBankAcctNo;
    }

    public void setIsBankAcctNo(BigDecimal isBankAcctNo) {
        this.isBankAcctNo = isBankAcctNo;
    }

    public BigDecimal getEntruPayChangeSerno() {
        return entruPayChangeSerno;
    }

    public void setEntruPayChangeSerno(BigDecimal entruPayChangeSerno) {
        this.entruPayChangeSerno = entruPayChangeSerno;
    }

    public BigDecimal getIsDoChange() {
        return isDoChange;
    }

    public void setIsDoChange(BigDecimal isDoChange) {
        this.isDoChange = isDoChange;
    }

    public BigDecimal getTopp() {
        return topp;
    }

    public void setTopp(BigDecimal topp) {
        this.topp = topp;
    }

    @Override
    public String toString() {
        return "List{" +
                "toppAcctNo=" + toppAcctNo +
                ", toppName='" + toppName + '\'' +
                ", toppAmt=" + toppAmt +
                ", toorgNo=" + toorgNo +
                ", toorgName=" + toorgName +
                ", isBankAcctNo=" + isBankAcctNo +
                ", entruPayChangeSerno=" + entruPayChangeSerno +
                ", isDoChange=" + isDoChange +
                ", topp=" + topp +
                '}';
    }
}
