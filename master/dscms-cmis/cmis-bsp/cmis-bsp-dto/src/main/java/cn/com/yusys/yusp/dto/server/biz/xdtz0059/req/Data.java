package cn.com.yusys.yusp.dto.server.biz.xdtz0059.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：更新信贷台账信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applid")
    private String applid;//对接记录ID
    @JsonProperty(value = "fixtyp")
    private String fixtyp;//操作类型
    @JsonProperty(value = "entnam")
    private String entnam;//企业名称
    @JsonProperty(value = "entpid")
    private String entpid;//企业代码
    @JsonProperty(value = "itemid")
    private String itemid;//科目
    @JsonProperty(value = "loanno")
    private String loanno;//借据编号
    @JsonProperty(value = "curren")
    private String curren;//币种
    @JsonProperty(value = "psprcp")
    private BigDecimal psprcp;//发放金额
    @JsonProperty(value = "osprcp")
    private BigDecimal osprcp;//剩余本金
    @JsonProperty(value = "stdate")
    private String stdate;//起始日期
    @JsonProperty(value = "endate")
    private String endate;//到期日期
    @JsonProperty(value = "inrate")
    private BigDecimal inrate;//执行利率
    @JsonProperty(value = "status")
    private String status;//状态

    public String getApplid() {
        return applid;
    }

    public void setApplid(String applid) {
        this.applid = applid;
    }

    public String getFixtyp() {
        return fixtyp;
    }

    public void setFixtyp(String fixtyp) {
        this.fixtyp = fixtyp;
    }

    public String getEntnam() {
        return entnam;
    }

    public void setEntnam(String entnam) {
        this.entnam = entnam;
    }

    public String getEntpid() {
        return entpid;
    }

    public void setEntpid(String entpid) {
        this.entpid = entpid;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getLoanno() {
        return loanno;
    }

    public void setLoanno(String loanno) {
        this.loanno = loanno;
    }

    public String getCurren() {
        return curren;
    }

    public void setCurren(String curren) {
        this.curren = curren;
    }

    public BigDecimal getPsprcp() {
        return psprcp;
    }

    public void setPsprcp(BigDecimal psprcp) {
        this.psprcp = psprcp;
    }

    public BigDecimal getOsprcp() {
        return osprcp;
    }

    public void setOsprcp(BigDecimal osprcp) {
        this.osprcp = osprcp;
    }

    public String getStdate() {
        return stdate;
    }

    public void setStdate(String stdate) {
        this.stdate = stdate;
    }

    public String getEndate() {
        return endate;
    }

    public void setEndate(String endate) {
        this.endate = endate;
    }

    public BigDecimal getInrate() {
        return inrate;
    }

    public void setInrate(BigDecimal inrate) {
        this.inrate = inrate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Data{" +
                "applid='" + applid + '\'' +
                ", fixtyp='" + fixtyp + '\'' +
                ", entnam='" + entnam + '\'' +
                ", entpid='" + entpid + '\'' +
                ", itemid='" + itemid + '\'' +
                ", loanno='" + loanno + '\'' +
                ", curren='" + curren + '\'' +
                ", psprcp=" + psprcp +
                ", osprcp=" + osprcp +
                ", stdate='" + stdate + '\'' +
                ", endate='" + endate + '\'' +
                ", inrate=" + inrate +
                ", status='" + status + '\'' +
                '}';
    }
}
