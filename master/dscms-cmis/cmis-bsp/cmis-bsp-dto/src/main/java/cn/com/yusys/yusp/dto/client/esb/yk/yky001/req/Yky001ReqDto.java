package cn.com.yusys.yusp.dto.client.esb.yk.yky001.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：印控申请接口
 *
 * @author xuwh
 * @version 1.0
 * @since 2021/8/18下午17:02:26
 */
@JsonPropertyOrder(alphabetic = true)
public class Yky001ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd; // 处理码
    @JsonProperty(value = "servtp")
    private String servtp; // 渠道
    @JsonProperty(value = "servsq")
    private String servsq; // 渠道流水
    @JsonProperty(value = "userid")
    private String userid; // 柜员号
    @JsonProperty(value = "brchno")
    private String brchno; // 部门号
    @JsonProperty(value = "servdt")
    private String servdt;// 请求方日期
    @JsonProperty(value = "servti")
    private String servti;// 请求方时间

    @JsonProperty(value = "oaId")
    private String oaId; // 流程号
    @JsonProperty(value = "operatorCode")
    private String operatorCode; // 操作员号
    @JsonProperty(value = "operatorName")
    private String operatorName; // 操作员名
    @JsonProperty(value = "orgNo")
    private String orgNo; // 机构号
    @JsonProperty(value = "orgName")
    private String orgName; // 机构名
    @JsonProperty(value = "title")
    private String title; // 标题
    @JsonProperty(value = "accessory")
    private String accessory; // 文件目录
    @JsonProperty(value = "applyTime")
    private String applyTime; // 交易时间
    @JsonProperty(value = "stampEmpOrg")
    private String stampEmpOrg; // 机构号
    @JsonProperty(value = "list")
    private java.util.List<List> list; // 印控列表

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getOaId() {
        return oaId;
    }

    public void setOaId(String oaId) {
        this.oaId = oaId;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccessory() {
        return accessory;
    }

    public void setAccessory(String accessory) {
        this.accessory = accessory;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getStampEmpOrg() {
        return stampEmpOrg;
    }

    public void setStampEmpOrg(String stampEmpOrg) {
        this.stampEmpOrg = stampEmpOrg;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }
}