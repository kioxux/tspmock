package cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：新信贷同步用户账号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwd008ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "createUser")
    private String createUser;//创建人
    @JsonProperty(value = "id")
    private String id;//账号
    @JsonProperty(value = "loginName")
    private String loginName;//工号
    @JsonProperty(value = "name")
    private String name;//姓名
    @JsonProperty(value = "status")
    private String status;//状态 A有效
    @JsonProperty(value = "brhId")
    private String brhId;//所属部门
    @JsonProperty(value = "orgId")
    private String orgId;//所在机构
    @JsonProperty(value = "teamType")
    private String teamType;//团队类型 1车易贷 2快贷 3优企贷
    @JsonProperty(value = "phone")
    private String phone;//移动电话
    @JsonProperty(value = "tel")
    private String tel;//办公电话
    @JsonProperty(value = "grantOrg")
    private String grantOrg;//授权机构id
    @JsonProperty(value = "regionList")
    private String regionList;//区域编号数组
    @JsonProperty(value = "positionList")
    private String positionList;//岗位编号数组
    @JsonProperty(value = "roleList")
    private String roleList;//角色编号数组

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBrhId() {
        return brhId;
    }

    public void setBrhId(String brhId) {
        this.brhId = brhId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getGrantOrg() {
        return grantOrg;
    }

    public void setGrantOrg(String grantOrg) {
        this.grantOrg = grantOrg;
    }

    public String getRegionList() {
        return regionList;
    }

    public void setRegionList(String regionList) {
        this.regionList = regionList;
    }

    public String getPositionList() {
        return positionList;
    }

    public void setPositionList(String positionList) {
        this.positionList = positionList;
    }

    public String getRoleList() {
        return roleList;
    }

    public void setRoleList(String roleList) {
        this.roleList = roleList;
    }

    @Override
    public String toString() {
        return "Xwd008ReqDto{" +
                "createUser='" + createUser + '\'' +
                "id='" + id + '\'' +
                "loginName='" + loginName + '\'' +
                "name='" + name + '\'' +
                "status='" + status + '\'' +
                "brhId='" + brhId + '\'' +
                "orgId='" + orgId + '\'' +
                "teamType='" + teamType + '\'' +
                "phone='" + phone + '\'' +
                "tel='" + tel + '\'' +
                "grantOrg='" + grantOrg + '\'' +
                "regionList='" + regionList + '\'' +
                "positionList='" + positionList + '\'' +
                "roleList='" + roleList + '\'' +
                '}';
    }
}  
