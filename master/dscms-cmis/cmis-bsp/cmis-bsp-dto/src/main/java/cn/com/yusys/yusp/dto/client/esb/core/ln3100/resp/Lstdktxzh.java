package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款贴息账户属性
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdktxzh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "tiexizhh")
    private String tiexizhh;//贴息账号
    @JsonProperty(value = "txzhhzxh")
    private String txzhhzxh;//贴息账号子序号
    @JsonProperty(value = "txzhhmch")
    private String txzhhmch;//贴息账户名称
    @JsonProperty(value = "shengxrq")
    private String shengxrq;//生效日期
    @JsonProperty(value = "tiexibil")
    private BigDecimal tiexibil;//贴息比率
    @JsonProperty(value = "fxjxbzhi")
    private String fxjxbzhi;//贴息计复利标志
    @JsonProperty(value = "zdkoukbz")
    private String zdkoukbz;//自动扣款标志

    public String getTiexizhh() {
        return tiexizhh;
    }

    public void setTiexizhh(String tiexizhh) {
        this.tiexizhh = tiexizhh;
    }

    public String getTxzhhzxh() {
        return txzhhzxh;
    }

    public void setTxzhhzxh(String txzhhzxh) {
        this.txzhhzxh = txzhhzxh;
    }

    public String getTxzhhmch() {
        return txzhhmch;
    }

    public void setTxzhhmch(String txzhhmch) {
        this.txzhhmch = txzhhmch;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public BigDecimal getTiexibil() {
        return tiexibil;
    }

    public void setTiexibil(BigDecimal tiexibil) {
        this.tiexibil = tiexibil;
    }

    public String getFxjxbzhi() {
        return fxjxbzhi;
    }

    public void setFxjxbzhi(String fxjxbzhi) {
        this.fxjxbzhi = fxjxbzhi;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    @Override
    public String toString() {
        return "LstdktxzhRespDto{" +
                "tiexizhh='" + tiexizhh + '\'' +
                "txzhhzxh='" + txzhhzxh + '\'' +
                "txzhhmch='" + txzhhmch + '\'' +
                "shengxrq='" + shengxrq + '\'' +
                "tiexibil='" + tiexibil + '\'' +
                "fxjxbzhi='" + fxjxbzhi + '\'' +
                "zdkoukbz='" + zdkoukbz + '\'' +
                '}';
    }
}  
