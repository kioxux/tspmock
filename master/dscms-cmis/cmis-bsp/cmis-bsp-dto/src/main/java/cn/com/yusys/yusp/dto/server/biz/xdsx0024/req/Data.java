package cn.com.yusys.yusp.dto.server.biz.xdsx0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：省心快贷plus授信，风控自动审批结果推送信贷
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "lmt_serno")
    private String lmt_serno;//授信流水号
    @JsonProperty(value = "sp_result_code")
    private String sp_result_code;//自动审批结果代码
    @JsonProperty(value = "list")
    java.util.List<List> list;

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getSp_result_code() {
        return sp_result_code;
    }

    public void setSp_result_code(String sp_result_code) {
        this.sp_result_code = sp_result_code;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "lmt_serno=" + lmt_serno +
                "sp_result_code=" + sp_result_code +
                "list=" + list +
                '}';
    }
}
