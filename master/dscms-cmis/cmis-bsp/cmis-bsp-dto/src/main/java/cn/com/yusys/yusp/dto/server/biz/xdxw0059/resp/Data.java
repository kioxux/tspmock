package cn.com.yusys.yusp.dto.server.biz.xdxw0059.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：根据业务唯一编号查询在信贷系统中的抵押率
 * @Author zhangpeng
 * @Date 2021/4/25 8:56
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pldRate")
    private BigDecimal pldRate;//抵押率

    public BigDecimal getPldRate() {
        return pldRate;
    }

    public void setPldRate(BigDecimal pldRate) {
        this.pldRate = pldRate;
    }
}
