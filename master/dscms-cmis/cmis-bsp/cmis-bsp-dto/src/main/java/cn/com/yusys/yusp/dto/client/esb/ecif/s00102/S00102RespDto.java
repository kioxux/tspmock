package cn.com.yusys.yusp.dto.client.esb.ecif.s00102;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：对私客户综合信息维护
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class S00102RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custno")
    private String custno;//      客户编号
    @JsonProperty(value = "custna")
    private String custna;//      客户名称
    @JsonProperty(value = "custtp")
    private String custtp;//      客户状态
    @JsonProperty(value = "tsywsq")
    private String tsywsq;//      特殊业务流水号
    @JsonProperty(value = "exchmb")
    private String exchmb;//      改客户名标志
    @JsonProperty(value = "prcstx")
    private String prcstx;//      业务对比字段
    @JsonProperty(value = "nnjytp")
    private String nnjytp;//      交易类型
    @JsonProperty(value = "nnjyna")
    private String nnjyna;//      交易名称
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//      核心流水
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//      核心交易日期

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCusttp() {
        return custtp;
    }

    public void setCusttp(String custtp) {
        this.custtp = custtp;
    }

    public String getTsywsq() {
        return tsywsq;
    }

    public void setTsywsq(String tsywsq) {
        this.tsywsq = tsywsq;
    }

    public String getExchmb() {
        return exchmb;
    }

    public void setExchmb(String exchmb) {
        this.exchmb = exchmb;
    }

    public String getPrcstx() {
        return prcstx;
    }

    public void setPrcstx(String prcstx) {
        this.prcstx = prcstx;
    }

    public String getNnjytp() {
        return nnjytp;
    }

    public void setNnjytp(String nnjytp) {
        this.nnjytp = nnjytp;
    }

    public String getNnjyna() {
        return nnjyna;
    }

    public void setNnjyna(String nnjyna) {
        this.nnjyna = nnjyna;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    @Override
    public String toString() {
        return "S00102RespDto{" +
                "custno='" + custno + '\'' +
                ", custna='" + custna + '\'' +
                ", custtp='" + custtp + '\'' +
                ", tsywsq='" + tsywsq + '\'' +
                ", exchmb='" + exchmb + '\'' +
                ", prcstx='" + prcstx + '\'' +
                ", nnjytp='" + nnjytp + '\'' +
                ", nnjyna='" + nnjyna + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                '}';
    }
}
