package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：从票据系统获取当日到期票的日出备款金额
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "batchNo")
    private String batchNo;// 批次号
    @JsonProperty(value = "totalFAmount")
    private String totalFAmount;//票面总金额
    @JsonProperty(value = "successAmount")
    private String successAmount;//成功金额

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getTotalFAmount() {
        return totalFAmount;
    }

    public void setTotalFAmount(String totalFAmount) {
        this.totalFAmount = totalFAmount;
    }

    public String getSuccessAmount() {
        return successAmount;
    }

    public void setSuccessAmount(String successAmount) {
        this.successAmount = successAmount;
    }

    @Override
    public String toString() {
        return "list{" +
                "batchNo='" + batchNo + '\'' +
                "totalFAmount='" + totalFAmount + '\'' +
                "successAmount='" + successAmount + '\'' +
                '}';
    }
}