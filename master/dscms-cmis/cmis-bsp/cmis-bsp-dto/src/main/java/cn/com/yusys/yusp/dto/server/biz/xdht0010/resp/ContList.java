package cn.com.yusys.yusp.dto.server.biz.xdht0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/5/6 13:46
 * @Version 1.0
 */
public class ContList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "avlBal")
    private BigDecimal avlBal;//可用余额
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同到期日
    @JsonProperty(value = "curtLPR")
    private BigDecimal curtLPR;//当前lpr
    @JsonProperty(value = "irAdjustType")
    private String irAdjustType;//利率调整方式
    @JsonProperty(value = "realityIrY")
    private String realityIrY;//执行利率
    @JsonProperty(value = "onTheWayAmt")
    private BigDecimal onTheWayAmt;//在途金额
    @JsonProperty(value = "isOnTheWay")
    private String isOnTheWay;//是否在途

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getAvlBal() {
        return avlBal;
    }

    public void setAvlBal(BigDecimal avlBal) {
        this.avlBal = avlBal;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public BigDecimal getCurtLPR() {
        return curtLPR;
    }

    public void setCurtLPR(BigDecimal curtLPR) {
        this.curtLPR = curtLPR;
    }

    public String getIrAdjustType() {
        return irAdjustType;
    }

    public void setIrAdjustType(String irAdjustType) {
        this.irAdjustType = irAdjustType;
    }

    public String getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(String realityIrY) {
        this.realityIrY = realityIrY;
    }

    public BigDecimal getOnTheWayAmt() {
        return onTheWayAmt;
    }

    public void setOnTheWayAmt(BigDecimal onTheWayAmt) {
        this.onTheWayAmt = onTheWayAmt;
    }

    public String getIsOnTheWay() {
        return isOnTheWay;
    }

    public void setIsOnTheWay(String isOnTheWay) {
        this.isOnTheWay = isOnTheWay;
    }

    @Override
    public String toString() {
        return "ContList{" +
                "contNo='" + contNo + '\'' +
                "cnContNo='" + cnContNo + '\'' +
                "contAmt='" + contAmt + '\'' +
                "avlBal='" + avlBal + '\'' +
                "contStartDate='" + contStartDate + '\'' +
                "contEndDate='" + contEndDate + '\'' +
                "curtLPR='" + curtLPR + '\'' +
                "irAdjustType='" + irAdjustType + '\'' +
                "realityIrY='" + realityIrY + '\'' +
                "onTheWayAmt='" + onTheWayAmt + '\'' +
                "isOnTheWay='" + isOnTheWay + '\'' +
                '}';
    }
}
