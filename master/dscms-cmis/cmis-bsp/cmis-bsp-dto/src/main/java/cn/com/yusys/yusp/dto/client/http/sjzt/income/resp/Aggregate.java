package cn.com.yusys.yusp.dto.client.http.sjzt.income.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/20 18:45
 * @since 2021/8/20 18:45
 */
public class Aggregate implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalIncome")
    private BigDecimal totalIncome;//总收益
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "interestIncome")
    private BigDecimal interestIncome;//利息收益
    @JsonProperty(value = "discountIncome")
    private BigDecimal discountIncome;//利率优惠收益

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public BigDecimal getInterestIncome() {
        return interestIncome;
    }

    public void setInterestIncome(BigDecimal interestIncome) {
        this.interestIncome = interestIncome;
    }

    public BigDecimal getDiscountIncome() {
        return discountIncome;
    }

    public void setDiscountIncome(BigDecimal discountIncome) {
        this.discountIncome = discountIncome;
    }

    @Override
    public String toString() {
        return "Aggregate{" +
                "totalIncome=" + totalIncome +
                ", cusId='" + cusId + '\'' +
                ", interestIncome=" + interestIncome +
                ", discountIncome=" + discountIncome +
                '}';
    }
}
