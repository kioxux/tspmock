package cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd01.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dhxd01RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
}
