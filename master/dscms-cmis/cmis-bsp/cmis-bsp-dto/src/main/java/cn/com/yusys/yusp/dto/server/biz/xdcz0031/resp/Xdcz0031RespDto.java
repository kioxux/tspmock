package cn.com.yusys.yusp.dto.server.biz.xdcz0031.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0031.resp.Data;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;


@JsonPropertyOrder(alphabetic = true)
public class Xdcz0031RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;//data

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdcz0031RespDto{" +
				"data=" + data +
				'}';
	}
}
