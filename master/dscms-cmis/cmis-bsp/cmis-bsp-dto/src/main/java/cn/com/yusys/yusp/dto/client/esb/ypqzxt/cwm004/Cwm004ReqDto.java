package cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求DTO：押品权证出库接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Cwm004ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("applyTime")
    private String applyTime; // 申请时间	否	date	是	例：2020-10-19
    @JsonProperty("gageId")
    private String gageId; // 核心担保品编号	否	varchar(32)	是
    @JsonProperty("gageType")
    private String gageType; // 抵质押类型	否	varchar(6)	是
    @JsonProperty("gageTypeName")
    private String gageTypeName; // 抵质押类型名称	否	varchar(50)	否
    @JsonProperty("maxAmt")
    private BigDecimal maxAmt; // 权利价值	否	number(16,2)	是
    @JsonProperty("gageUser")
    private String gageUser; // 抵押人	否	varchar(50)	否
    @JsonProperty("acctBrchNo")
    private String acctBrchNo; // 账务机构	否	varchar(10)	是
    @JsonProperty("acctBrchName")
    private String acctBrchName; // 账务机构名称	否	varchar(50)	是
    @JsonProperty("applyBrchNo")
    private String applyBrchNo; // 申请支行号	否	varchar(10)	是
    @JsonProperty("applyBrchName")
    private String applyBrchName; // 申请支行名称	否	varchar(50)	是
    @JsonProperty("applyUserCode")
    private String applyUserCode; // 申请人操作号	否	varchar(10)	是
    @JsonProperty("applyUserName")
    private String applyUserName; // 申请人名称	否	varchar(50)	是
    @JsonProperty("isMortgage")
    private String isMortgage; // 是否住房按揭	否	char(1)	是	 1：是          0：否
    @JsonProperty("remark")
    private String remark; // 描述，备注	否	varchar(200)	否
    @JsonProperty("gageName")
    private String gageName; // 抵押物名称	否	varchar(100)	否
    @JsonProperty("isLocal")
    private String isLocal; // 是否张家港地区不动产	否	char(1)	是	 0：不是      1：是
    @JsonProperty("isElectronic")
    private String isElectronic; // 押品类型	否	char(1)	是	 1：电子      2：实物
    @JsonProperty("iszhRegist")
    private String iszhRegist; // 是否总行办理注销登记	否	char(1)	是	 1：是         2：否

    @JsonProperty("userName")
    private String userName; // 柜员名称
    @JsonProperty("acctBrch")
    private String acctBrch; // 财务机构

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getGageId() {
        return gageId;
    }

    public void setGageId(String gageId) {
        this.gageId = gageId;
    }

    public String getGageType() {
        return gageType;
    }

    public void setGageType(String gageType) {
        this.gageType = gageType;
    }

    public String getGageTypeName() {
        return gageTypeName;
    }

    public void setGageTypeName(String gageTypeName) {
        this.gageTypeName = gageTypeName;
    }

    public BigDecimal getMaxAmt() {
        return maxAmt;
    }

    public void setMaxAmt(BigDecimal maxAmt) {
        this.maxAmt = maxAmt;
    }

    public String getGageUser() {
        return gageUser;
    }

    public void setGageUser(String gageUser) {
        this.gageUser = gageUser;
    }

    public String getAcctBrchNo() {
        return acctBrchNo;
    }

    public void setAcctBrchNo(String acctBrchNo) {
        this.acctBrchNo = acctBrchNo;
    }

    public String getAcctBrchName() {
        return acctBrchName;
    }

    public void setAcctBrchName(String acctBrchName) {
        this.acctBrchName = acctBrchName;
    }

    public String getApplyBrchNo() {
        return applyBrchNo;
    }

    public void setApplyBrchNo(String applyBrchNo) {
        this.applyBrchNo = applyBrchNo;
    }

    public String getApplyBrchName() {
        return applyBrchName;
    }

    public void setApplyBrchName(String applyBrchName) {
        this.applyBrchName = applyBrchName;
    }

    public String getApplyUserCode() {
        return applyUserCode;
    }

    public void setApplyUserCode(String applyUserCode) {
        this.applyUserCode = applyUserCode;
    }

    public String getApplyUserName() {
        return applyUserName;
    }

    public void setApplyUserName(String applyUserName) {
        this.applyUserName = applyUserName;
    }

    public String getIsMortgage() {
        return isMortgage;
    }

    public void setIsMortgage(String isMortgage) {
        this.isMortgage = isMortgage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getGageName() {
        return gageName;
    }

    public void setGageName(String gageName) {
        this.gageName = gageName;
    }

    public String getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(String isLocal) {
        this.isLocal = isLocal;
    }

    public String getIsElectronic() {
        return isElectronic;
    }

    public void setIsElectronic(String isElectronic) {
        this.isElectronic = isElectronic;
    }

    public String getIszhRegist() {
        return iszhRegist;
    }

    public void setIszhRegist(String iszhRegist) {
        this.iszhRegist = iszhRegist;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAcctBrch() {
        return acctBrch;
    }

    public void setAcctBrch(String acctBrch) {
        this.acctBrch = acctBrch;
    }

    @Override
    public String toString() {
        return "Cwm004ReqDto{" +
                "applyTime='" + applyTime + '\'' +
                ", gageId='" + gageId + '\'' +
                ", gageType='" + gageType + '\'' +
                ", gageTypeName='" + gageTypeName + '\'' +
                ", maxAmt=" + maxAmt +
                ", gageUser='" + gageUser + '\'' +
                ", acctBrchNo='" + acctBrchNo + '\'' +
                ", acctBrchName='" + acctBrchName + '\'' +
                ", applyBrchNo='" + applyBrchNo + '\'' +
                ", applyBrchName='" + applyBrchName + '\'' +
                ", applyUserCode='" + applyUserCode + '\'' +
                ", applyUserName='" + applyUserName + '\'' +
                ", isMortgage='" + isMortgage + '\'' +
                ", remark='" + remark + '\'' +
                ", gageName='" + gageName + '\'' +
                ", isLocal='" + isLocal + '\'' +
                ", isElectronic='" + isElectronic + '\'' +
                ", iszhRegist='" + iszhRegist + '\'' +
                ", userName='" + userName + '\'' +
                ", acctBrch='" + acctBrch + '\'' +
                '}';
    }
}
