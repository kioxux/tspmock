package cn.com.yusys.yusp.dto.client.esb.core.ln3031;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：贷款资料维护（关于还款）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3031ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkkhczbz")
    private String dkkhczbz;//开户操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "dbdkkksx")
    private Integer dbdkkksx;//多笔贷款扣款顺序
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "hetongje")
    private BigDecimal hetongje;//合同金额
    @JsonProperty(value = "jiejuuje")
    private BigDecimal jiejuuje;//借据金额
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "qigscfsh")
    private String qigscfsh;//期供生成方式
    @JsonProperty(value = "duophkbz")
    private String duophkbz;//多频率还款标志
    @JsonProperty(value = "dzhhkjih")
    private String dzhhkjih;//定制还款计划
    @JsonProperty(value = "rchukkbz")
    private String rchukkbz;//日初扣款标志
    @JsonProperty(value = "zdkoukbz")
    private String zdkoukbz;//自动扣款标志
    @JsonProperty(value = "zdjqdkbz")
    private String zdjqdkbz;//自动结清贷款标志
    @JsonProperty(value = "dhkzhhbz")
    private String dhkzhhbz;//多还款账户标志
    @JsonProperty(value = "qyxhdkbz")
    private String qyxhdkbz;//签约循环贷款标志
    @JsonProperty(value = "llbgtzjh")
    private String llbgtzjh;//利率变更调整计划
    @JsonProperty(value = "dcfktzjh")
    private String dcfktzjh;//多次放款调整计划
    @JsonProperty(value = "tqhktzjh")
    private String tqhktzjh;//提前还款调整计划
    @JsonProperty(value = "yunxtqhk")
    private String yunxtqhk;//允许提前还款
    @JsonProperty(value = "xycihkrq")
    private String xycihkrq;//下一次还款日
    @JsonProperty(value = "leijqjsh")
    private Integer leijqjsh;//累进区间期数
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "xhdkqyzh")
    private String xhdkqyzh;//签约账号
    @JsonProperty(value = "hkqixian")
    private String hkqixian;//还款期限(月)
    @JsonProperty(value = "hkshxubh")
    private String hkshxubh;//还款顺序编号
    @JsonProperty(value = "leijinzh")
    private BigDecimal leijinzh;//累进值
    @JsonProperty(value = "baoliuje")
    private BigDecimal baoliuje;//保留金额
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "xhdkzhxh")
    private String xhdkzhxh;//签约账号子序号
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "huanbzhq")
    private String huanbzhq;//还本周期
    @JsonProperty(value = "yuqhkzhq")
    private String yuqhkzhq;//逾期还款周期
    @JsonProperty(value = "lstdkhbjh")
    private List<Lstdkhbjh> lstdkhbjh;//贷款还本计划
    @JsonProperty(value = "lstdkhkfs")
    private List<Lstdkhkfs> lstdkhkfs;//贷款组合还款方式
    @JsonProperty(value = "lstdkhkzh")
    private List<Lstdkhkzh> lstdkhkzh;//贷款多还款账户
    @JsonProperty(value = "qxbgtzjh")
    private String qxbgtzjh;//期限变更调整计划
    @JsonProperty(value = "tiaozhkf")
    private String tiaozhkf;//允许调整还款方式
    @JsonProperty(value = "scihkrbz")
    private String scihkrbz;//首次还款日模式
    @JsonProperty(value = "mqihkfsh")
    private String mqihkfsh;//末期还款方式
    @JsonProperty(value = "suoqcish")
    private Integer suoqcish;//缩期次数
    @JsonProperty(value = "bzuekkfs")
    private String bzuekkfs;//不足额扣款方式
    @JsonProperty(value = "yqbzkkfs")
    private String yqbzkkfs;//逾期不足额扣款方式
    @JsonProperty(value = "hntmihku")
    private String hntmihku;//允许行内同名账户还款
    @JsonProperty(value = "hnftmhku")
    private String hnftmhku;//允许行内非同名帐户还款
    @JsonProperty(value = "nbuzhhku")
    private String nbuzhhku;//允许内部账户还款
    @JsonProperty(value = "tiqhksdq")
    private Integer tiqhksdq;//提前还款锁定期
    @JsonProperty(value = "sfyxkuxq")
    private String sfyxkuxq;//是否有宽限期
    @JsonProperty(value = "kuanxqts")
    private Integer kuanxqts;//宽限期天数
    @JsonProperty(value = "kxqzdcsh")
    private Integer kxqzdcsh;//宽限期最大次数
    @JsonProperty(value = "kxqjixgz")
    private String kxqjixgz;//宽限期计息方式
    @JsonProperty(value = "kxqhjxgz")
    private String kxqhjxgz;//宽限期后计息规则
    @JsonProperty(value = "kxqzyqgz")
    private String kxqzyqgz;//宽限期转逾期规则
    @JsonProperty(value = "kxqjjrgz")
    private String kxqjjrgz;//宽限期节假日规则
    @JsonProperty(value = "yunxsuoq")
    private String yunxsuoq;//允许缩期
    @JsonProperty(value = "kxqshxgz")
    private String kxqshxgz;//宽限期收息规则


    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public Integer getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Integer dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public String getDuophkbz() {
        return duophkbz;
    }

    public void setDuophkbz(String duophkbz) {
        this.duophkbz = duophkbz;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getRchukkbz() {
        return rchukkbz;
    }

    public void setRchukkbz(String rchukkbz) {
        this.rchukkbz = rchukkbz;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getZdjqdkbz() {
        return zdjqdkbz;
    }

    public void setZdjqdkbz(String zdjqdkbz) {
        this.zdjqdkbz = zdjqdkbz;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public String getQyxhdkbz() {
        return qyxhdkbz;
    }

    public void setQyxhdkbz(String qyxhdkbz) {
        this.qyxhdkbz = qyxhdkbz;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    public String getXycihkrq() {
        return xycihkrq;
    }

    public void setXycihkrq(String xycihkrq) {
        this.xycihkrq = xycihkrq;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getXhdkqyzh() {
        return xhdkqyzh;
    }

    public void setXhdkqyzh(String xhdkqyzh) {
        this.xhdkqyzh = xhdkqyzh;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public String getHkshxubh() {
        return hkshxubh;
    }

    public void setHkshxubh(String hkshxubh) {
        this.hkshxubh = hkshxubh;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getXhdkzhxh() {
        return xhdkzhxh;
    }

    public void setXhdkzhxh(String xhdkzhxh) {
        this.xhdkzhxh = xhdkzhxh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    public List<Lstdkhbjh> getLstdkhbjh() {
        return lstdkhbjh;
    }

    public void setLstdkhbjh(List<Lstdkhbjh> lstdkhbjh) {
        this.lstdkhbjh = lstdkhbjh;
    }

    public List<Lstdkhkfs> getLstdkhkfs() {
        return lstdkhkfs;
    }

    public void setLstdkhkfs(List<Lstdkhkfs> lstdkhkfs) {
        this.lstdkhkfs = lstdkhkfs;
    }

    public List<Lstdkhkzh> getLstdkhkzh() {
        return lstdkhkzh;
    }

    public void setLstdkhkzh(List<Lstdkhkzh> lstdkhkzh) {
        this.lstdkhkzh = lstdkhkzh;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getTiaozhkf() {
        return tiaozhkf;
    }

    public void setTiaozhkf(String tiaozhkf) {
        this.tiaozhkf = tiaozhkf;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public Integer getSuoqcish() {
        return suoqcish;
    }

    public void setSuoqcish(Integer suoqcish) {
        this.suoqcish = suoqcish;
    }

    public String getBzuekkfs() {
        return bzuekkfs;
    }

    public void setBzuekkfs(String bzuekkfs) {
        this.bzuekkfs = bzuekkfs;
    }

    public String getYqbzkkfs() {
        return yqbzkkfs;
    }

    public void setYqbzkkfs(String yqbzkkfs) {
        this.yqbzkkfs = yqbzkkfs;
    }

    public String getHntmihku() {
        return hntmihku;
    }

    public void setHntmihku(String hntmihku) {
        this.hntmihku = hntmihku;
    }

    public String getHnftmhku() {
        return hnftmhku;
    }

    public void setHnftmhku(String hnftmhku) {
        this.hnftmhku = hnftmhku;
    }

    public String getNbuzhhku() {
        return nbuzhhku;
    }

    public void setNbuzhhku(String nbuzhhku) {
        this.nbuzhhku = nbuzhhku;
    }

    public Integer getTiqhksdq() {
        return tiqhksdq;
    }

    public void setTiqhksdq(Integer tiqhksdq) {
        this.tiqhksdq = tiqhksdq;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public Integer getKxqzdcsh() {
        return kxqzdcsh;
    }

    public void setKxqzdcsh(Integer kxqzdcsh) {
        this.kxqzdcsh = kxqzdcsh;
    }

    public String getKxqjixgz() {
        return kxqjixgz;
    }

    public void setKxqjixgz(String kxqjixgz) {
        this.kxqjixgz = kxqjixgz;
    }

    public String getKxqhjxgz() {
        return kxqhjxgz;
    }

    public void setKxqhjxgz(String kxqhjxgz) {
        this.kxqhjxgz = kxqhjxgz;
    }

    public String getKxqzyqgz() {
        return kxqzyqgz;
    }

    public void setKxqzyqgz(String kxqzyqgz) {
        this.kxqzyqgz = kxqzyqgz;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    public String getYunxsuoq() {
        return yunxsuoq;
    }

    public void setYunxsuoq(String yunxsuoq) {
        this.yunxsuoq = yunxsuoq;
    }

    public String getKxqshxgz() {
        return kxqshxgz;
    }

    public void setKxqshxgz(String kxqshxgz) {
        this.kxqshxgz = kxqshxgz;
    }

    @Override
    public String toString() {
        return "Ln3031ReqDto{" +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "dbdkkksx='" + dbdkkksx + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "hetongje='" + hetongje + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "dechligz='" + dechligz + '\'' +
                "qigscfsh='" + qigscfsh + '\'' +
                "duophkbz='" + duophkbz + '\'' +
                "dzhhkjih='" + dzhhkjih + '\'' +
                "rchukkbz='" + rchukkbz + '\'' +
                "zdkoukbz='" + zdkoukbz + '\'' +
                "zdjqdkbz='" + zdjqdkbz + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "qyxhdkbz='" + qyxhdkbz + '\'' +
                "llbgtzjh='" + llbgtzjh + '\'' +
                "dcfktzjh='" + dcfktzjh + '\'' +
                "tqhktzjh='" + tqhktzjh + '\'' +
                "yunxtqhk='" + yunxtqhk + '\'' +
                "xycihkrq='" + xycihkrq + '\'' +
                "leijqjsh='" + leijqjsh + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "meiqhbje='" + meiqhbje + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "xhdkqyzh='" + xhdkqyzh + '\'' +
                "hkqixian='" + hkqixian + '\'' +
                "hkshxubh='" + hkshxubh + '\'' +
                "leijinzh='" + leijinzh + '\'' +
                "baoliuje='" + baoliuje + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "xhdkzhxh='" + xhdkzhxh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "huanbzhq='" + huanbzhq + '\'' +
                "yuqhkzhq='" + yuqhkzhq + '\'' +
                "lstdkhbjh='" + lstdkhbjh + '\'' +
                "lstdkhkfs='" + lstdkhkfs + '\'' +
                "lstdkhkzh='" + lstdkhkzh + '\'' +
                "qxbgtzjh='" + qxbgtzjh + '\'' +
                "tiaozhkf='" + tiaozhkf + '\'' +
                "scihkrbz='" + scihkrbz + '\'' +
                "mqihkfsh='" + mqihkfsh + '\'' +
                "suoqcish='" + suoqcish + '\'' +
                "bzuekkfs='" + bzuekkfs + '\'' +
                "yqbzkkfs='" + yqbzkkfs + '\'' +
                "hntmihku='" + hntmihku + '\'' +
                "hnftmhku='" + hnftmhku + '\'' +
                "nbuzhhku='" + nbuzhhku + '\'' +
                "tiqhksdq='" + tiqhksdq + '\'' +
                "sfyxkuxq='" + sfyxkuxq + '\'' +
                "kuanxqts='" + kuanxqts + '\'' +
                "kxqzdcsh='" + kxqzdcsh + '\'' +
                "kxqjixgz='" + kxqjixgz + '\'' +
                "kxqhjxgz='" + kxqhjxgz + '\'' +
                "kxqzyqgz='" + kxqzyqgz + '\'' +
                "kxqjjrgz='" + kxqjjrgz + '\'' +
                "yunxsuoq='" + yunxsuoq + '\'' +
                "kxqshxgz='" + kxqshxgz + '\'' +
                '}';
    }
}  
