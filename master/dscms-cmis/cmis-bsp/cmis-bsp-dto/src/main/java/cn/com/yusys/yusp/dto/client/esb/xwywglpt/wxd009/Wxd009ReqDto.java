package cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷系统请求小V平台推送合同信息接口
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxd009ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "reqid")
    private String reqid;//（小微）系统流水号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同编号
    @JsonProperty(value = "cont_state")
    private String cont_state;//合同状态
    @JsonProperty(value = "cont_type")
    private String cont_type;//合同类型
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//合同到期日
    @JsonProperty(value = "sign_date")
    private String sign_date;//合同签订日

    public String getReqid() {
        return reqid;
    }

    public void setReqid(String reqid) {
        this.reqid = reqid;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getSign_date() {
        return sign_date;
    }

    public void setSign_date(String sign_date) {
        this.sign_date = sign_date;
    }

    @Override
    public String toString() {
        return "Wxd009ReqDto{" +
                "reqid='" + reqid + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", cont_state='" + cont_state + '\'' +
                ", cont_type='" + cont_type + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", sign_date='" + sign_date + '\'' +
                '}';
    }
}
