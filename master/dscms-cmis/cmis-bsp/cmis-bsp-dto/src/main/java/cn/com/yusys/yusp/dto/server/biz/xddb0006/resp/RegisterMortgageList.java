package cn.com.yusys.yusp.dto.server.biz.xddb0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 10:04
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class RegisterMortgageList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "mmcenu")
    private String mmcenu;//登记簿抵押信息-不动产登记证明号
    @JsonProperty(value = "mormor")
    private String mormor;//登记簿抵押信息-抵押权人
    @JsonProperty(value = "mormog")
    private String mormog;//登记簿抵押信息-抵押人
    @JsonProperty(value = "mogusc")
    private String mogusc;//登记簿抵押信息-担保范围
    @JsonProperty(value = "mogord")
    private String mogord;//登记簿抵押信息-顺位
    @JsonProperty(value = "momome")
    private String momome;//登记簿抵押信息-抵押方式
    @JsonProperty(value = "modeam")
    private BigDecimal modeam;//登记簿抵押信息-债权数额
    @JsonProperty(value = "mordst")
    private String mordst;//登记簿抵押信息-债务履行开始时间
    @JsonProperty(value = "mordet")
    private String mordet;//登记簿抵押信息-债务履行结束时间
    @JsonProperty(value = "morbod")
    private String morbod;//登记簿抵押信息-登簿日期

    public String getMmcenu() {
        return mmcenu;
    }

    public void setMmcenu(String mmcenu) {
        this.mmcenu = mmcenu;
    }

    public String getMormor() {
        return mormor;
    }

    public void setMormor(String mormor) {
        this.mormor = mormor;
    }

    public String getMormog() {
        return mormog;
    }

    public void setMormog(String mormog) {
        this.mormog = mormog;
    }

    public String getMogusc() {
        return mogusc;
    }

    public void setMogusc(String mogusc) {
        this.mogusc = mogusc;
    }

    public String getMogord() {
        return mogord;
    }

    public void setMogord(String mogord) {
        this.mogord = mogord;
    }

    public String getMomome() {
        return momome;
    }

    public void setMomome(String momome) {
        this.momome = momome;
    }

    public BigDecimal getModeam() {
        return modeam;
    }

    public void setModeam(BigDecimal modeam) {
        this.modeam = modeam;
    }

    public String getMordst() {
        return mordst;
    }

    public void setMordst(String mordst) {
        this.mordst = mordst;
    }

    public String getMordet() {
        return mordet;
    }

    public void setMordet(String mordet) {
        this.mordet = mordet;
    }

    public String getMorbod() {
        return morbod;
    }

    public void setMorbod(String morbod) {
        this.morbod = morbod;
    }

    @Override
    public String toString() {
        return "RegisterMortgageList{" +
                "mmcenu='" + mmcenu + '\'' +
                ", mormor='" + mormor + '\'' +
                ", mormog='" + mormog + '\'' +
                ", mogusc='" + mogusc + '\'' +
                ", mogord='" + mogord + '\'' +
                ", momome='" + momome + '\'' +
                ", modeam=" + modeam +
                ", mordst='" + mordst + '\'' +
                ", mordet='" + mordet + '\'' +
                ", morbod='" + morbod + '\'' +
                '}';
    }
}
