package cn.com.yusys.yusp.dto.server.biz.hyy.babiur01;

import cn.com.yusys.yusp.dto.server.biz.hyy.babigc01.Items;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：押品状态变更推送
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "items")
    private List<Boolean> items;

    public List<Boolean> getItems() {
        return items;
    }

    public void setItems(List<Boolean> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Data{" +
                "items=" + items +
                '}';
    }
}
