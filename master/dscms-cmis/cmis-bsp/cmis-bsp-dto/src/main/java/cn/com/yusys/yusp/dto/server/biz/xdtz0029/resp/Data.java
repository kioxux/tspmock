package cn.com.yusys.yusp.dto.server.biz.xdtz0029.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 21:26
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "porderNo")
    private String porderNo;//汇票号码
    @JsonProperty(value = "porderAmt")
    private BigDecimal porderAmt;//票面金额
    @JsonProperty(value = "accStatus")
    private String accStatus;//台账状态

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

    public BigDecimal getPorderAmt() {
        return porderAmt;
    }

    public void setPorderAmt(BigDecimal porderAmt) {
        this.porderAmt = porderAmt;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    @Override
    public String toString() {
        return "Xdtz0029RespDto{" +
                "porderNo='" + porderNo + '\'' +
                "porderAmt='" + porderAmt + '\'' +
                "accStatus='" + accStatus + '\'' +
                '}';
    }
}
