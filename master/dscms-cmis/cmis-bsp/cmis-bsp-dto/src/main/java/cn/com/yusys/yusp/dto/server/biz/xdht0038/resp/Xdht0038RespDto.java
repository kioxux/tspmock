package cn.com.yusys.yusp.dto.server.biz.xdht0038.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：乐悠金根据核心客户号查询房贷首付款比例进行额度计算
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0038RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "data")
	private Data data;


	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdht0038RespDto{" +
				" data=" + data +
				'}';
	}
}
