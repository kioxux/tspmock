package cn.com.yusys.yusp.dto.client.esb.ypxt.xdypdywcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询抵押物信息
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdypdywcxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "addr")
    private String addr;//地址
    @JsonProperty(value = "num")
    private String num;//数量
    @JsonProperty(value = "reg_no")
    private String reg_no;//编号

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    @Override
    public String toString() {
        return "XdypdywcxRespDto{" +
                "addr='" + addr + '\'' +
                ", num='" + num + '\'' +
                ", reg_no='" + reg_no + '\'' +
                '}';
    }
}