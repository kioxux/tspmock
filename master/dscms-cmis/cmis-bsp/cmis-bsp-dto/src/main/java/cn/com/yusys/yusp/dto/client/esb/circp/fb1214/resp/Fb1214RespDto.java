package cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：房产信息修改同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1214RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CRD_ITEM_NUM")
    private String CRD_ITEM_NUM;//授信分项编号
    @JsonProperty(value = "NEW_PRE_CRD_AMT")
    private BigDecimal NEW_PRE_CRD_AMT;//重新计算后的授信额度

    @JsonProperty(value = "OLS_TRAN_NO")
    private String OLS_TRAN_NO;//系统交易流水
    @JsonProperty(value = "OLS_DATE")
    private String OLS_DATE;//系统交易日期
    @JsonProperty(value = "HOSE_LIST")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp.HOUSE_LIST> HOSE_LIST;

    @JsonIgnore
    public String getCRD_ITEM_NUM() {
        return CRD_ITEM_NUM;
    }

    @JsonIgnore
    public void setCRD_ITEM_NUM(String CRD_ITEM_NUM) {
        this.CRD_ITEM_NUM = CRD_ITEM_NUM;
    }

    @JsonIgnore
    public BigDecimal getNEW_PRE_CRD_AMT() {
        return NEW_PRE_CRD_AMT;
    }

    @JsonIgnore
    public void setNEW_PRE_CRD_AMT(BigDecimal NEW_PRE_CRD_AMT) {
        this.NEW_PRE_CRD_AMT = NEW_PRE_CRD_AMT;
    }

    @JsonIgnore
    public String getOLS_TRAN_NO() {
        return OLS_TRAN_NO;
    }

    @JsonIgnore
    public void setOLS_TRAN_NO(String OLS_TRAN_NO) {
        this.OLS_TRAN_NO = OLS_TRAN_NO;
    }

    @JsonIgnore
    public String getOLS_DATE() {
        return OLS_DATE;
    }

    @JsonIgnore
    public void setOLS_DATE(String OLS_DATE) {
        this.OLS_DATE = OLS_DATE;
    }

    @JsonIgnore
    public List<HOUSE_LIST> getHOSE_LIST() {
        return HOSE_LIST;
    }

    @JsonIgnore
    public void setHOSE_LIST(List<HOUSE_LIST> HOSE_LIST) {
        this.HOSE_LIST = HOSE_LIST;
    }

    @Override
    public String toString() {
        return "Fb1214RespDto{" +
                "CRD_ITEM_NUM='" + CRD_ITEM_NUM + '\'' +
                ", NEW_PRE_CRD_AMT=" + NEW_PRE_CRD_AMT +
                ", OLS_TRAN_NO='" + OLS_TRAN_NO + '\'' +
                ", OLS_DATE='" + OLS_DATE + '\'' +
                ", HOSE_LIST=" + HOSE_LIST +
                '}';
    }
}
