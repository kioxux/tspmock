package cn.com.yusys.yusp.dto.server.biz.xdht0029.resp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：客户非小微业务经营性贷款总金额查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "totlApplyAmount")
    private String totlApplyAmount;//贷款总金额

    public String getTotlApplyAmount() {
        return totlApplyAmount;
    }

    public void setTotlApplyAmount(String totlApplyAmount) {
        this.totlApplyAmount = totlApplyAmount;
    }

    @Override
    public String toString() {
        return "Data{" +
                "totlApplyAmount='" + totlApplyAmount + '\'' +
                '}';
    }
}