package cn.com.yusys.yusp.dto.server.biz.xdls0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 14:24
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serviceId")
    private String serviceId;//服务编号
    @JsonProperty(value = "opflag")
    private String opflag;//操作类型

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getOpflag() {
        return opflag;
    }

    public void setOpflag(String opflag) {
        this.opflag = opflag;
    }
}
