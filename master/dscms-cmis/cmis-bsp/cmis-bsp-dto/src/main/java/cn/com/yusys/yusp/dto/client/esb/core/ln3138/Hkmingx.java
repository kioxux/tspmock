package cn.com.yusys.yusp.dto.client.esb.core.ln3138;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 响应Dto：等额等本贷款推算
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Hkmingx implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额
    @JsonProperty(value = "benjinnn")
    private BigDecimal benjinnn;//本金
    @JsonProperty(value = "guihlixi")
    private BigDecimal guihlixi;//归还利息
    @JsonProperty(value = "yueeeeee")
    private BigDecimal yueeeeee;//余额

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public BigDecimal getBenjinnn() {
        return benjinnn;
    }

    public void setBenjinnn(BigDecimal benjinnn) {
        this.benjinnn = benjinnn;
    }

    public BigDecimal getGuihlixi() {
        return guihlixi;
    }

    public void setGuihlixi(BigDecimal guihlixi) {
        this.guihlixi = guihlixi;
    }

    public BigDecimal getYueeeeee() {
        return yueeeeee;
    }

    public void setYueeeeee(BigDecimal yueeeeee) {
        this.yueeeeee = yueeeeee;
    }
}
