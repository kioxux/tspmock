package cn.com.yusys.yusp.dto.client.esb.core.ln3160;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LstKlnb_dkzrhz {
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "bjyskzhh")
    private String bjyskzhh;//其他应收款账号(本金)
    @JsonProperty(value = "lxyskzhh")
    private String lxyskzhh;//其他应收款账号(利息)
    @JsonProperty(value = "fyyskzhh")
    private String fyyskzhh;//其他应收款账号(费用)
    @JsonProperty(value = "fjyskzhh")
    private String fjyskzhh;//其他应收款账号(罚金)
    @JsonProperty(value = "bjysfzhh")
    private String bjysfzhh;//其他应付款账号(本金)
    @JsonProperty(value = "lxysfzhh")
    private String lxysfzhh;//其他应付款账号(利息)
    @JsonProperty(value = "fyyfkzhh")
    private String fyyfkzhh;//其他应付款账号(费用)
    @JsonProperty(value = "fjyfkzhh")
    private String fjyfkzhh;//其他应付款账号(罚金)
    @JsonProperty(value = "qyskzhzh")
    private String qyskzhzh;//其他应收款账号子序号(本金)
    @JsonProperty(value = "lxyskzxh")
    private String lxyskzxh;//其他应收款账号子序号(利息)
    @JsonProperty(value = "fyyskzxh")
    private String fyyskzxh;//其他应收款账号子序号(费用)
    @JsonProperty(value = "fjyskzxh")
    private String fjyskzxh;//其他应收款账号子序号(罚金)
    @JsonProperty(value = "bjysfzxh")
    private String bjysfzxh;//其他应付款账号子序号(本金)
    @JsonProperty(value = "qtyfzhzx")
    private String qtyfzhzx;//其他应付款账号子序号(利息)
    @JsonProperty(value = "fyyfkzxh")
    private String fyyfkzxh;//其他应付款账号子序号(费用)
    @JsonProperty(value = "fjyfkzxh")
    private String fjyfkzxh;//其他应付款账号子序号(罚金)
    @JsonProperty(value = "sunyrzzh")
    private String sunyrzzh;//损益入账账号
    @JsonProperty(value = "syrzzhxh")
    private String syrzzhxh;//损益入账账号子序号
    @JsonProperty(value = "syrzzhmc")
    private String syrzzhmc;//损益入账账户名称
    @JsonProperty(value = "dfkbjzhh")
    private String dfkbjzhh;//待付款本金账号
    @JsonProperty(value = "dfkbjzxh")
    private String dfkbjzxh;//待付款本金账号子序号
    @JsonProperty(value = "dfklxzhh")
    private String dfklxzhh;//待付款利息账号
    @JsonProperty(value = "dfklxzxh")
    private String dfklxzxh;//待付款利息账号子序号
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "bjghrzxh")
    private String bjghrzxh;//本金归还入账账号子序号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号
    @JsonProperty(value = "lxghrzxh")
    private String lxghrzxh;//利息归还入账账号子序号
    @JsonProperty(value = "yywsrzhh")
    private String yywsrzhh;//营业外收入账号
    @JsonProperty(value = "yywsrzxh")
    private String yywsrzxh;//营业外收入账号子序号
    @JsonProperty(value = "yywzczhh")
    private String yywzczhh;//营业外支出账号
    @JsonProperty(value = "yywzczxh")
    private String yywzczxh;//营业外支出账号子序号
    @JsonProperty(value = "dailihbz")
    private String dailihbz;//代理行标志

}
