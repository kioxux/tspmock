package cn.com.yusys.yusp.dto.client.esb.core.ln3196;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 账号子序号查询交易，支持查询内部户、负债账户
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/15 11:13
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3196ReqDto implements Serializable {
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao;//	客户账号		BaseType.U_KEHUZHAO	string	(35)		否	''
    @JsonProperty(value = "khzhhzxh")
    private String khzhhzxh;//	客户账号子序号		BaseType.U_ZHHAOXUH	string	(8)		否	''

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    @Override
    public String toString() {
        return "Ln3196ReqService{" +
                "kehuzhao='" + kehuzhao + '\'' +
                ", khzhhzxh='" + khzhhzxh + '\'' +
                '}';
    }
}
