package cn.com.yusys.yusp.dto.server.biz.xdsx0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 16:05:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 16:05
 * @since 2021/5/19 16:05
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zf_flag")
    private String zf_flag;//是否作废成功

    public String getZf_flag() {
        return zf_flag;
    }

    public void setZf_flag(String zf_flag) {
        this.zf_flag = zf_flag;
    }

    @Override
    public String toString() {
        return "Xdsx0014RespDto{" +
                "zf_flag='" + zf_flag + '\'' +
                '}';
    }
}
