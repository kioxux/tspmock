package cn.com.yusys.yusp.dto.client.esb.com001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：额度同步
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Com001ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "data")
    private Data data;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Com001ReqDto{" +
                "serno='" + serno + '\'' +
                "instuCde='" + instuCde + '\'' +
                ", data=" + data +
                '}';
    }
}

