package cn.com.yusys.yusp.dto.server.biz.xdht0017.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 15:58
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pdfDepoAddr")
    private String pdfDepoAddr;//pdf存放地址
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getPdfDepoAddr() {
        return pdfDepoAddr;
    }

    public void setPdfDepoAddr(String pdfDepoAddr) {
        this.pdfDepoAddr = pdfDepoAddr;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xdht0017RespDto{" +
                "pdfDepoAddr='" + pdfDepoAddr + '\'' +
                "opMsg='" + opMsg + '\'' +
                '}';
    }
}
