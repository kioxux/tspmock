package cn.com.yusys.yusp.dto.client.esb.core.da3303;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：抵债资产信息维护
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3303RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "daizdzzc")
    private BigDecimal daizdzzc;//待转抵债资产
    @JsonProperty(value = "dcldzzic")
    private BigDecimal dcldzzic;//待处理抵债资产
    @JsonProperty(value = "dbxdzzic")
    private BigDecimal dbxdzzic;//待变现抵债资产
    @JsonProperty(value = "hfeijine")
    private BigDecimal hfeijine;//还费金额
    @JsonProperty(value = "huanbjee")
    private BigDecimal huanbjee;//还本金额
    @JsonProperty(value = "hxijinee")
    private BigDecimal hxijinee;//还息金额
    @JsonProperty(value = "hqitjine")
    private BigDecimal hqitjine;//还其它金额

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public BigDecimal getDaizdzzc() {
        return daizdzzc;
    }

    public void setDaizdzzc(BigDecimal daizdzzc) {
        this.daizdzzc = daizdzzc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public BigDecimal getHqitjine() {
        return hqitjine;
    }

    public void setHqitjine(BigDecimal hqitjine) {
        this.hqitjine = hqitjine;
    }

    @Override
    public String toString() {
        return "Da3303RespDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehuzwmc='" + kehuzwmc + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "daizdzzc='" + daizdzzc + '\'' +
                "dcldzzic='" + dcldzzic + '\'' +
                "dbxdzzic='" + dbxdzzic + '\'' +
                "hfeijine='" + hfeijine + '\'' +
                "huanbjee='" + huanbjee + '\'' +
                "hxijinee='" + hxijinee + '\'' +
                "hqitjine='" + hqitjine + '\'' +
                '}';
    }
}  
