package cn.com.yusys.yusp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

@EnableDiscoveryClient
@SpringBootApplication
@MapperScan({"cn.com.yusys.yusp.**.repository.mapper"})
@EnableFeignClients("cn.com.yusys.yusp")
@EnableTransactionManagement
public class CmisCusStarterMicroserviceApp {
    public static void main(String[] args) {
        SpringApplication.run(CmisCusStarterMicroserviceApp.class, args).getEnvironment();
    }
}
