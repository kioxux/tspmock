package cn.com.yusys.yusp.demo.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/api/async")
public class AsyncController {
    private static final Logger log = LoggerFactory.getLogger(AsyncController.class);

    @Autowired
    private TimeService messageService;

    @GetMapping("/withreturn")
    public void asyncWithReturn() {

        log.info("开始异步调用, 当前线程名:{}", Thread.currentThread().getName());

        Future<String> future = messageService.sendMessageWithReturn();
        String value = "";
        try {
            value = future.get();
        } catch (InterruptedException e) {
            log.error("异步调用失败", e);
            Thread.currentThread().interrupt();
            value = "异步调用失败";
        } catch (ExecutionException e) {
            log.error("异步调用失败", e);
            value = "异步调用失败";
        }

        log.info("异步调用结束, 返回结果{}, 当前线程名{}", value, Thread.currentThread().getName());
    }

    @GetMapping("/withoutreturn")
    public void aysncWithoutReturn() {
        log.info("开始异步调用, 当前线程名:{}", Thread.currentThread().getName());
        messageService.sendMessageWithoutReturn();
        log.info("开始异步结束, 当前线程名:{}", Thread.currentThread().getName());
    }

    /**
     * 错误示范,不会触发异步
     */
    @GetMapping("/test")
    public void aysncTest() {
        log.info("开始异步调用, 当前线程名:{}", Thread.currentThread().getName());
        this.test();
        log.info("开始异步结束, 当前线程名:{}", Thread.currentThread().getName());
    }

    @Async
    public void test() {
        log.info("当前线程:{}", Thread.currentThread().getName());
    }
}
