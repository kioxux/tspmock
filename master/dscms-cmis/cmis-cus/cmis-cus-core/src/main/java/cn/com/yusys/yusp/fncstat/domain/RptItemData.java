package cn.com.yusys.yusp.fncstat.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * @Classname RptItemData.java
 * @Version 1.0
 * @Since 1.0 Oct 13, 2008 2:05:36 PM
 * @Copyright yuchengtech
 * @Author Eric
 * @Description：item信息VO |用户操作具体的报表时，组装sql语句用的数据对象
 * @Lastmodified
 * @Author
 */
public class RptItemData {
    private static final Logger log = LoggerFactory.getLogger(RptItemData.class);
    private String cusId = null; // 客户ID
    private String tableName = null; // 表名
    //报表周期类型 1--月 2--季 3--半年 4--年
    private String statPrdStyle = null;
    //数据列数 1 ：代表数据只有一列值 3 ：代表数据有三列值
    private int dataColumn = 0; // 期数数
    private String rptYear = null; // 年
    private String rptMonth = null; // 月
    private String itemId = null; // 项目编号
    private String fncConfStyle = null; // 报表类型11资产12利润
    private BigDecimal data1 = BigDecimal.ZERO;
    private BigDecimal data2 = BigDecimal.ZERO;
    private BigDecimal data3 = BigDecimal.ZERO;
    //表字段名称
    private String dataField1 = null;
    private String dataField2 = null;
    private String dataField3 = null;
    //报表口径STAT_STYLE 新增联合主键 1 本部报表 2 联合报表 9 未知
    private String statStyle = null;


    public String getStatStyle() {
        return statStyle;
    }

    public void setStatStyle(String statStyle) {
        this.statStyle = statStyle;
    }

    public RptItemData() {
    }


    /**
     * 构造函数，用户初始化数据
     *
     * @param cusId        客户ID
     * @param tableName    报表名
     * @param rptCycleType 报表期间类型 1--月 2--季 3--半年 4--年
     * @param rptYear      报表年份
     * @param rptMonth     报表月份
     * @param itemId       项目编号
     * @param statPrdStyle 期数数
     */
    public RptItemData(String cusId, String tableName, String rptCycleType, String rptYear, String rptMonth, String itemId, int statPrdStyle, String fncConfStyle) {
        this.cusId = cusId;
        this.tableName = tableName;
        this.statPrdStyle = rptCycleType;
        this.rptYear = rptYear;
        this.rptMonth = rptMonth;
        this.itemId = itemId;
        this.dataColumn = statPrdStyle;
        this.fncConfStyle = fncConfStyle;
    }


    /**
     * 构造函数，用户初始化数据 新增
     *
     * @param cusId        客户ID
     * @param tableName    报表名
     * @param rptCycleType 报表期间类型 1--月 2--季 3--半年 4--年
     * @param rptYear      报表年份
     * @param rptMonth     报表月份
     * @param itemId       项目编号
     * @param statPrdStyle 期数数
     * @param statStyle    报表口径 1-本部 2-合并 9-其他
     */
    public RptItemData(String cusId, String tableName, String rptCycleType, String rptYear, String rptMonth, String itemId, String statStyle, int statPrdStyle, String fncConfStyle) {
        this.cusId = cusId;
        this.tableName = tableName;
        this.statPrdStyle = rptCycleType;
        this.rptYear = rptYear;
        this.rptMonth = rptMonth;
        this.itemId = itemId;
        this.dataColumn = statPrdStyle;
        this.fncConfStyle = fncConfStyle;
        this.statStyle = statStyle;
    }


    /**
     * 构造函数，用户初始化数据 新增
     *
     * @param cusId        客户ID
     * @param tableName    报表名
     * @param rptCycleType 报表期间类型 1--月 2--季 3--半年 4--年
     * @param rptYear      报表年份
     * @param rptMonth     报表月份
     * @param itemId       项目编号
     * @param statPrdStyle 期数数
     */
    public RptItemData(String cusId, String tableName, String rptCycleType, String rptYear, String rptMonth, String itemId, int statPrdStyle) {
        this.cusId = cusId;
        this.tableName = tableName;
        this.statPrdStyle = rptCycleType;
        this.rptYear = rptYear;
        this.rptMonth = rptMonth;
        this.itemId = itemId;
        this.dataColumn = statPrdStyle;
    }


    /**
     * 新增
     *
     * @param cusId
     * @param tableName
     * @param rptCycleType
     * @param rptYear
     * @param rptMonth
     * @param itemId
     * @param statStyle
     * @param statPrdStyle
     */
    public RptItemData(String cusId, String tableName, String rptCycleType, String rptYear, String rptMonth, String itemId, String statStyle, int statPrdStyle) {
        this.cusId = cusId;
        this.tableName = tableName;
        this.statPrdStyle = rptCycleType;
        this.rptYear = rptYear;
        this.rptMonth = rptMonth;
        this.itemId = itemId;
        this.dataColumn = statPrdStyle;
        this.statStyle = statStyle;
    }


    /**
     * 根据成员变量的报表口径，和期数数确定要操作的字段名称并初始化 成员变量 dataField1 和 dataField2
     *
     * @throws Exception
     */
    public void initdataField() throws Exception {
        if ((null == this.statPrdStyle) || (null) == this.rptMonth || 0 == this.dataColumn) {
            throw new Exception("初始化对象的数据不完整，不能生成指定sql");
        }
        int iMonth = Integer.parseInt(this.rptMonth); // 用于存储报表月份
        if ("1".equals(this.statPrdStyle)) { // 月
            if (1 == this.dataColumn) { // 期数1
                this.dataField1 = "STAT_INIT_AMT" + iMonth;
            } else if (2 == this.dataColumn) { // 期数2
                this.dataField1 = "STAT_INIT_AMT" + iMonth;
                if("13".equals(this.fncConfStyle)){
                    this.dataField2 = "STAT_INIT_AMT_LY1";
                }else {
                    this.dataField2 = "STAT_END_AMT" + iMonth;
                }
            } else if (3 == this.dataColumn) { // 期数2
                this.dataField1 = "STAT_INIT_AMT_LY2";
                this.dataField2 = "STAT_INIT_AMT_LY1";
                this.dataField3 = "STAT_INIT_AMT" + iMonth;
            } else {
                throw new Exception("表fnc_conf_styles中fnc_conf_data_column字段值非法只能是1、2、3！");
            }

        } else if ("2".equals(this.statPrdStyle)) { // 季度
            if (1 == this.dataColumn) { // 期数1
                if (3 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q1";
                } else if (6 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q2";
                } else if (9 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q3";
                } else if (12 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q4";
                } else {
                    throw new Exception("输入的月份不符，季报对应的会计期间为03、06、09、12!");
                }
            } else if (2 == this.dataColumn) { // 期数2
                if (3 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q1";
                    this.dataField2 = "STAT_END_AMT_Q1";
                } else if (6 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q2";
                    this.dataField2 = "STAT_END_AMT_Q2";
                } else if (9 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q3";
                    this.dataField2 = "STAT_END_AMT_Q3";
                } else if (12 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q4";
                    this.dataField2 = "STAT_END_AMT_Q4";
                } else {
                    throw new Exception("输入的月份不符，季报对应的会计期间为03、06、09、12!");
                }
            } else if (3 == this.dataColumn) { // 期数2
                if (3 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q1";
                    this.dataField2 = "STAT_INIT_AMT_LY1";
                    this.dataField3 = "STAT_INIT_AMT_LY2";
                } else if (6 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q2";
                    this.dataField2 = "STAT_INIT_AMT_LY1";
                    this.dataField3 = "STAT_INIT_AMT_LY2";
                } else if (9 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q3";
                    this.dataField2 = "STAT_INIT_AMT_LY1";
                    this.dataField3 = "STAT_INIT_AMT_LY2";
                } else if (12 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Q4";
                    this.dataField2 = "STAT_INIT_AMT_LY1";
                    this.dataField3 = "STAT_INIT_AMT_LY2";
                } else {
                    throw new Exception("输入的月份不符，季报对应的会计期间为03、06、09、12!");
                }
            } else {
                throw new Exception("表fnc_conf_styles中fnc_conf_data_column字段值非法只能是1、2、3！");
            }
        } else if ("3".equals(this.statPrdStyle)) { // 半年
            if (1 == this.dataColumn) { // 期数1
                if (6 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Y1";
                } else if (12 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Y1";
                } else {
                    throw new Exception("输入的月份不符，半年报对应的会计期间应为06、12!");
                }
            } else if (2 == this.dataColumn) { // 期数2
                if (6 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Y1";
                    this.dataField2 = "STAT_END_AMT_Y1";
                } else if (12 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Y2";
                    this.dataField2 = "STAT_END_AMT_Y2";
                } else {
                    throw new Exception("输入的月份不符，半年报对应的会计期间应为06、12!");
                }
            } else if (3 == this.dataColumn) { // 期数2
                if (6 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Y1";
                    this.dataField2 = "STAT_INIT_AMT_LY1";
                    this.dataField3 = "STAT_INIT_AMT_LY2";
                } else if (12 == iMonth) {
                    this.dataField1 = "STAT_INIT_AMT_Y2";
                    this.dataField2 = "STAT_INIT_AMT_LY1";
                    this.dataField3 = "STAT_INIT_AMT_LY2";
                } else {
                    throw new Exception("输入的月份不符，半年报对应的会计期间应为06、12!");
                }
            } else {
                throw new Exception("表fnc_conf_styles中fnc_conf_data_column字段值非法只能是1、2、3！");
            }
        } else if ("4".equals(this.statPrdStyle)) { // 年
            if (1 == this.dataColumn) { // 期数1
                this.dataField1 = "STAT_INIT_AMT_Y";
            } else if (2 == this.dataColumn) { // 期数2
                this.dataField1 = "STAT_INIT_AMT_Y";
                this.dataField2 = "STAT_END_AMT_Y";
            } else if (3 == this.dataColumn) { // 期数2
                this.dataField1 = "STAT_INIT_AMT_Y";
                this.dataField2 = "STAT_INIT_AMT_LY1";
                this.dataField3 = "STAT_INIT_AMT_LY2";
            } else {
                throw new Exception("表fnc_conf_styles中fnc_conf_data_column字段值非法只能是1、2、3！");
            }
        }
    }

    public int getStatPrdStyle() {
        return dataColumn;
    }

    public void setStatPrdStyle(int statPrdStyle) {
        this.dataColumn = statPrdStyle;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public BigDecimal getData1() {
        return data1;
    }

    public void setData1(BigDecimal data1) {
        this.data1 = data1;
    }

    public BigDecimal getData2() {
        return data2;
    }

    public void setData2(BigDecimal data2) {
        this.data2 = data2;
    }

    public BigDecimal getData3() {
        return data3;
    }

    public void setData3(BigDecimal data3) {
        this.data3 = data3;
    }

    public String getDataField1() {
        return dataField1;
    }

    public void setDataField1(String dataField1) {
        this.dataField1 = dataField1;
    }

    public String getDataField2() {
        return dataField2;
    }

    public void setDataField2(String dataField2) {
        this.dataField2 = dataField2;
    }

    public String getDataField3() {
        return dataField3;
    }

    public void setDataField3(String dataField3) {
        this.dataField3 = dataField3;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getRptCycleType() {
        return statPrdStyle;
    }

    public void setRptCycleType(String rptCycleType) {
        this.statPrdStyle = rptCycleType;
    }

    public String getRptMonth() {
        return rptMonth;
    }

    public void setRptMonth(String rptMonth) {
        this.rptMonth = rptMonth;
    }

    public String getRptYear() {
        return rptYear;
    }

    public void setRptYear(String rptYear) {
        this.rptYear = rptYear;
    }

    public int getDataColumn() {
        return dataColumn;
    }

    public void setDataColumn(int dataColumn) {
        this.dataColumn = dataColumn;
    }

    public String getFncConfStyle() {
        return fncConfStyle;
    }

    public void setFncConfStyle(String fncConfStyle) {
        this.fncConfStyle = fncConfStyle;
    }

    public void setStatPrdStyle(String statPrdStyle) {
        this.statPrdStyle = statPrdStyle;
    }

    public RptItemBean getUpdateBean() {
        RptItemBean rptItemBean = new RptItemBean();
        rptItemBean.setCusId(this.cusId);
        try {
            if (null == this.dataField1) {
                initdataField();
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        rptItemBean.setDataField1(this.dataField1);
        rptItemBean.setDataField2(this.dataField2);
        rptItemBean.setDataField3(this.dataField3);
        rptItemBean.setRptYear(this.rptYear);
        rptItemBean.setStatStyle(this.statStyle);
        rptItemBean.setItemId(this.itemId);
        rptItemBean.setTableName(this.tableName);
        return rptItemBean;
    }

    /**
     * 根据设置的成员变量拼装一个项目信息的新增sql语句
     *
     * @return sql 例如： INSERT INTO fnc_stat_bs(cus_id,stat_year,stat_item_id,stat_item_amt) VALUES('01','01','01',0.0);
     * @throws Exception
     */
    public String AssembleInsertSql() throws Exception {
        if ((null == this.cusId) || (null == this.tableName) || (null == this.statPrdStyle) || (null) == this.rptYear
                || (null) == this.rptMonth || (null == this.itemId) || 0 == this.dataColumn) {
            throw new Exception("初始化对象的数据不完整，不能生成指定sql");
        }
        if (null == this.dataField1) {
            this.initdataField();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO " + this.tableName + " (cus_id,stat_year,stat_item_id," + this.dataField1);
        if (null != this.statStyle) {
            sb.append(",stat_style");
        }
        if (null != this.dataField2) {
            sb.append("," + this.dataField2);
        }
        if (null != this.dataField3) {
            sb.append("," + this.dataField3);
        }
        sb.append(")").append(" VALUES('" + this.cusId);
        sb.append("','" + this.rptYear + "','" + this.itemId + "'," + this.data1);
        if (null != this.statStyle) {
            sb.append("," + this.statStyle);
        }
        if (null != this.dataField2) {
            sb.append("," + this.data2);
        }
        if (null != this.dataField3) {
            sb.append("," + this.data3);
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * 根据设置的成员变量拼装一个项目信息的更新sql语句
     *
     * @return sql 例如： UPDATE fnc_stat_bs SET stat_item_amt=0.0 WHERE cus_id='01' AND stat_year='01' AND stat_item_id='01'
     * @throws Exception
     */
    public String AssembleUpdateSql() throws Exception {
        if ((null == this.cusId) || (null == this.tableName) || (null == this.statPrdStyle) || (null) == this.rptYear
                || (null) == this.rptMonth || (null == this.itemId) || 0 == this.dataColumn || null == this.statStyle) {
            throw new Exception("初始化对象的数据不完整，不能生成指定sql");
        }
        if (null == this.dataField1) {
            this.initdataField();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE " + this.tableName);
        sb.append(" SET " + this.dataField1 + "=" + this.data1);
        if (null != this.dataField2) {
            sb.append("," + this.dataField2 + "=" + this.data2);
        }
        if (null != this.dataField3) {
            sb.append("," + this.dataField3 + "=" + this.data3);
        }
        sb.append(" WHERE CUS_ID='" + this.cusId + "' AND STAT_YEAR='" + this.rptYear)
                .append("' AND STAT_ITEM_ID='" + this.itemId + "' AND STAT_STYLE='" + this.statStyle).append("'");
        return sb.toString();
    }

    /**
     * 根据设置的成员变量拼装一个项目信息的查询sql语句
     *
     * @return sql 例如： select 1 FROM fnc_stat_bs WHERE cus_id='01' AND stat_year='01' AND stat_item_id='01' AND stat_style='1'
     * @throws Exception
     */
    public String assembleSelectSql() throws Exception {
        if ((null == this.cusId) || (null == this.tableName) || (null) == this.rptYear
                || (null == this.itemId) || null == this.statStyle) {
            throw new Exception("初始化对象的数据不完整，不能生成指定sql");
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT 1 FROM " + this.tableName);
        sb.append(" WHERE CUS_ID='" + this.cusId + "' AND STAT_YEAR='" + this.rptYear)
                .append("' AND STAT_ITEM_ID='" + this.itemId + "' AND STAT_STYLE='" + this.statStyle).append("'");
        return sb.toString();
    }
}
