/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.dto.CusLstDedkkhYjsxTaskDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstDedkkhYjsxTask;
import cn.com.yusys.yusp.repository.mapper.CusLstDedkkhYjsxTaskMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhYjsxTaskService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstDedkkhYjsxTaskService {

    @Autowired
    private CusLstDedkkhYjsxTaskMapper cusLstDedkkhYjsxTaskMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstDedkkhYjsxTask selectByPrimaryKey(String taskNo) {
        return cusLstDedkkhYjsxTaskMapper.selectByPrimaryKey(taskNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstDedkkhYjsxTask> selectAll(QueryModel model) {
        List<CusLstDedkkhYjsxTask> records = (List<CusLstDedkkhYjsxTask>) cusLstDedkkhYjsxTaskMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstDedkkhYjsxTask> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstDedkkhYjsxTask> list = cusLstDedkkhYjsxTaskMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstDedkkhYjsxTask record) {
        return cusLstDedkkhYjsxTaskMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstDedkkhYjsxTask record) {
        return cusLstDedkkhYjsxTaskMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstDedkkhYjsxTask record) {
        return cusLstDedkkhYjsxTaskMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstDedkkhYjsxTask record) {
        return cusLstDedkkhYjsxTaskMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String taskNo) {
        return cusLstDedkkhYjsxTaskMapper.deleteByPrimaryKey(taskNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstDedkkhYjsxTaskMapper.deleteByIds(ids);
    }

    /**
     * 根据客户号查找出该客户存在到期未完成的压降任务
     * @author zhangliang15
     * @date 2021/9/28 0:06
     **/
    public List<CusLstDedkkhYjsxTaskDto> selectcusLstDedkkhYjsxTaskDataByParams(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CusLstDedkkhYjsxTaskDto> list= cusLstDedkkhYjsxTaskMapper.selectcusLstDedkkhYjsxTaskDataByParams(queryModel);
        PageHelper.clearPage();
        return list;
    }
}
