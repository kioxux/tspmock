/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CusLstYndLinkman;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import cn.com.yusys.yusp.repository.mapper.CusLstYndJyxxAppMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndJyxxAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:18:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYndJyxxAppService {

    @Autowired
    private CusLstYndJyxxAppMapper cusLstYndJyxxAppMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstYndJyxxApp selectByPrimaryKey(String pkId) {
        return cusLstYndJyxxAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYndJyxxApp> selectAll(QueryModel model) {
        List<CusLstYndJyxxApp> records = (List<CusLstYndJyxxApp>) cusLstYndJyxxAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstYndJyxxApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYndJyxxApp> list = cusLstYndJyxxAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstYndJyxxApp record) {
        return cusLstYndJyxxAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstYndJyxxApp record) {
        return cusLstYndJyxxAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstYndJyxxApp record) {
        return cusLstYndJyxxAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstYndJyxxApp record) {
        return cusLstYndJyxxAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusLstYndJyxxAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYndJyxxAppMapper.deleteByIds(ids);
    }

    /**
     * @param cusLstYndJyxxApp
     * @return int
     * @author hubp
     * @date 2021/7/15 15:36
     * @version 1.0.0
     * @desc    新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int addCusLstYndJyxxApp(CusLstYndJyxxApp cusLstYndJyxxApp) {
        int result = 0;
        if (StringUtils.isBlank(cusLstYndJyxxApp.getPkId())) {
            cusLstYndJyxxApp.setPkId(StringUtils.getUUID());
            cusLstYndJyxxApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            result = cusLstYndJyxxAppMapper.insert(cusLstYndJyxxApp);
        } else {
            CusLstYndJyxxApp clyl = cusLstYndJyxxAppMapper.selectByPrimaryKey(cusLstYndJyxxApp.getPkId());
            if (null == clyl) {
                cusLstYndJyxxApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                result = cusLstYndJyxxAppMapper.insert(cusLstYndJyxxApp);
            } else {
                result = cusLstYndJyxxAppMapper.updateByPrimaryKey(cusLstYndJyxxApp);
            }
        }
        return result;
    }

    /**
     * @param serno
     * @return java.util.List<cn.com.yusys.yusp.domain.CusLstYndJyxxApp>
     * @author hubp
     * @date 2021/7/16 10:49
     * @version 1.0.0
     * @desc    通过流水号查询经营信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(readOnly=true)
    public List<CusLstYndJyxxApp> selectBySerno(String serno) {
        List<CusLstYndJyxxApp> list = cusLstYndJyxxAppMapper.selectBySerno(serno);
        return list;
    }

    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/7/16 14:25
     * @version 1.0.0
     * @desc 通过流水号删除经营信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int deleteBySerno(String serno) {
        return cusLstYndJyxxAppMapper.deleteBySerno(serno);
    }


    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/9/28 13:56
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int updateTypeBySerno (String serno) {
        return cusLstYndJyxxAppMapper.updateTypeBySerno(serno);
    }
}
