/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.CusCorpRelationsDto;
import cn.com.yusys.yusp.dto.CusCorpRelationsRequestDto;
import cn.com.yusys.yusp.dto.CusRelationsRequestDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusCorpApital;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus模块
 * @类名称: CusCorpApitalMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-23 09:49:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusCorpApitalMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusCorpApital selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusCorpApital> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusCorpApital record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusCorpApital record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusCorpApital record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusCorpApital record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 获取股东信息
     * @param certTyp
     * @param certCode
     * @return
     */
    CusCorpApital selectCusCorpApiByCodeAndType(@Param("certTyp")String certTyp, @Param("certCode")String certCode);

    /**
     * 获取股东关系
     * @param cusCorpRelationsRequestDto
     * @return
     */
    List<CusCorpRelationsDto> selectCusCorpApiComDto(CusCorpRelationsRequestDto cusCorpRelationsRequestDto);

    /**
     * 根据关联客户编号查询股东信息
     * @param cusIdRel
     * @return
     */
    List<Map<String,String>> queryCusInfoByCusIdRel(@Param("cusIdRel") String cusIdRel);

    /**
     * 根据关联客户号查询
     * @param cusIdRel
     * @return
     */
    List<CusCorpApitalDto> selectByCusIdRel(String cusIdRel);
}