/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.domain.CusLstDedkkh;
import cn.com.yusys.yusp.domain.CusLstDedkkhFormAndTableData;
import cn.com.yusys.yusp.domain.CusLstDedkkhYjsx;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.service.CusLstDedkkhService;
import cn.com.yusys.yusp.vo.CusLstDedkkhVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-03 14:36:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstdedkkh")
public class CusLstDedkkhResource {
    @Autowired
    private CusLstDedkkhService cusLstDedkkhService;

    private final Logger logger = LoggerFactory.getLogger(CusLstDedkkhResource.class);

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstDedkkh>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstDedkkh> list = cusLstDedkkhService.selectAll(queryModel);
        return new ResultDto<List<CusLstDedkkh>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryListData")
    protected ResultDto<List<CusLstDedkkh>> queryListData(@RequestBody QueryModel queryModel) {
        List<CusLstDedkkh> list = cusLstDedkkhService.selectByModel(queryModel);
        return new ResultDto<List<CusLstDedkkh>>(list);
    }

    @GetMapping("/xp")
    protected ResultDto<List<Map<String, Object>>> indexYuxp(QueryModel queryModel) {
        List<Map<String, Object>> list = cusLstDedkkhService.selectByModelxp(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{listSerno}")
    protected ResultDto<CusLstDedkkh> show(@PathVariable("listSerno") String listSerno) {
        CusLstDedkkh cusLstDedkkh = cusLstDedkkhService.selectByPrimaryKey(listSerno);
        return new ResultDto<CusLstDedkkh>(cusLstDedkkh);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstDedkkh> create(@RequestBody CusLstDedkkh cusLstDedkkh) throws URISyntaxException {
        cusLstDedkkhService.insert(cusLstDedkkh);
        return new ResultDto<CusLstDedkkh>(cusLstDedkkh);
    }

    /**
     * @函数名称:insertOrUpdateData
     * @函数描述:新增 修改
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertorupdatedata")
    protected ResultDto<Map<String, Object>> insertOrUpdateData(@RequestBody CusLstDedkkhFormAndTableData cusLstDedkkhFormAndTableData) throws URISyntaxException {
        Map<String, Object> data = cusLstDedkkhService.insertOrUpdateData(cusLstDedkkhFormAndTableData);
        return new ResultDto<Map<String, Object>>(data);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstDedkkh cusLstDedkkh) throws URISyntaxException {
        int result = cusLstDedkkhService.update(cusLstDedkkh);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateSelective
     * @函数描述:对象修改，公共API接口,只更新非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateselective")
    protected ResultDto<Integer> updateSelective(@RequestBody CusLstDedkkh cusLstDedkkh) {
        int result = cusLstDedkkhService.updateSelective(cusLstDedkkh);
        return ResultDto.success(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{listSerno}")
    protected ResultDto<Integer> delete(@PathVariable("listSerno") String listSerno) {
        int result = cusLstDedkkhService.deleteByPrimaryKey(listSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstDedkkhService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = cusLstDedkkhService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库
        try {
            ExcelUtils.syncImport(CusLstDedkkhVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return cusLstDedkkhService.insertInBatch(dataList);
           }), true);
        } catch (Exception e) {
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }

    /**
     * @函数名称:queryNewDekhDataByCusId
     * @函数描述:根据客户号查询最新的信息 审查报告
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryNewDekhDataByCusId")
    protected ResultDto<CusLstDedkkh> queryNewDekhDataByCusId(@RequestBody CusLstDedkkh cusLstDedkkh) {
        CusLstDedkkh result = cusLstDedkkhService.queryNewDekhDataByCusId(cusLstDedkkh);
        return new ResultDto<CusLstDedkkh>(result);
    }

    @PostMapping("/queryNewDekhByCusId")
    protected ResultDto<Map> queryNewDekhByCusId(@RequestBody String cusId){
        return new ResultDto<Map>(cusLstDedkkhService.queryNewDekhByCusId(cusId));
    }


    @PostMapping("/updateStatusPass")
    protected ResultDto<Integer> updateStatusPass(@RequestBody String cusId){
        return new ResultDto<Integer>(cusLstDedkkhService.updateStatusPass(cusId));
    }
}
