package cn.com.yusys.yusp.reportconf.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;


/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfStyles
 * @类描述: FNC_CONF_STYLES数据实体类
 * @功能描述: 
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-20 18:19:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Entity
@Table(name = "FNC_CONF_STYLES")
public class FncConfStyles extends BaseDomain implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 报表样式编号 **/
	@Id
	@Column(name = "STYLE_ID", unique = true, nullable = false, length = 6)
	private String styleId;

	/** 报表名称 **/
	@Column(name = "FNC_NAME", unique = false, nullable = true, length = 200)
	private String fncName;
	
	/** 显示名称 **/
	@Column(name = "FNC_CONF_DIS_NAME", unique = false, nullable = true, length = 200)
	private String fncConfDisName;
	
	/** 所属报表种类STD_ZB_FNC_TYP **/
	@Column(name = "FNC_CONF_TYP", unique = false, nullable = true, length = 2)
	private String fncConfTyp;
	
	/** 数据列数STD_ZB_FNC_COL **/
	@Column(name = "FNC_CONF_DATA_COL", unique = false, nullable = true, length = 0)
	private Integer fncConfDataCol;
	
	/** 栏位STD_ZB_FNC_COTES **/
	@Column(name = "FNC_CONF_COTES", unique = false, nullable = true, length = 0)
	private Integer fncConfCotes;
	
	/** 新旧报表标志STD_ZB_FNC_ON_TYP **/
	@Column(name = "NO_IND", unique = false, nullable = true, length = 1)
	private String noInd;
	
	/** 企事业报表标志STD_ZB_FNC_COMIND **/
	@Column(name = "COM_IND", unique = false, nullable = true, length = 1)
	private String comInd;
	
	/** 表头左部描述 **/
	@Column(name = "HEAD_LEFT", unique = false, nullable = true, length = 200)
	private String headLeft;
	
	/** 表头中部描述 **/
	@Column(name = "HEAD_CENTER", unique = false, nullable = true, length = 200)
	private String headCenter;
	
	/** 表头右部描述 **/
	@Column(name = "HEAD_RIGHT", unique = false, nullable = true, length = 200)
	private String headRight;
	
	/** 表尾左部描述 **/
	@Column(name = "FOOD_LEFT", unique = false, nullable = true, length = 200)
	private String foodLeft;
	
	/** 表尾中部描述 **/
	@Column(name = "FOOD_CENTER", unique = false, nullable = true, length = 200)
	private String foodCenter;
	
	/** 表尾右部描述 **/
	@Column(name = "FOOD_RIGHT", unique = false, nullable = true, length = 200)
	private String foodRight;
	
	/** 第一列数据描述STD_ZB_FNC_DCD **/
	@Column(name = "DATA_DEC1", unique = false, nullable = true, length = 2)
	private String dataDec1;
	
	/** 第二列数据描述STD_ZB_FNC_DCD **/
	@Column(name = "DATA_DEC2", unique = false, nullable = true, length = 2)
	private String dataDec2;
	
	/** 第三列数据描述STD_ZB_FNC_DCD **/
	@Column(name = "DATA_DEC3", unique = false, nullable = true, length = 2)
	private String dataDec3;
	
	/** 第四列数据描述STD_ZB_FNC_DCD **/
	@Column(name = "DATA_DEC4", unique = false, nullable = true, length = 2)
	private String dataDec4;
	
	/** 第五列数据描述STD_ZB_FNC_DCD **/
	@Column(name = "DATA_DEC5", unique = false, nullable = true, length = 2)
	private String dataDec5;
	
	/** 第六列数据描述STD_ZB_FNC_DCD **/
	@Column(name = "DATA_DEC6", unique = false, nullable = true, length = 2)
	private String dataDec6;
	
	/** 第七列数据描述STD_ZB_FNC_DCD **/
	@Column(name = "DATA_DEC7", unique = false, nullable = true, length = 2)
	private String dataDec7;
	
	/** 第八列数据描述STD_ZB_FNC_DCD **/
	@Column(name = "DATA_DEC8", unique = false, nullable = true, length = 2)
	private String dataDec8;
	@Transient
	private List<FncConfDefFmt> fncConfDefFmtList;
	
	public FncConfStyles() {
		super();
	}
	
	/**
	 * @param styleId
	 */
	public void setStyleId(String styleId) {
		this.styleId = styleId == null ? null : styleId.trim();
	}
	
    /**
     * @return StyleId
     */	
	public String getStyleId() {
		return this.styleId;
	}
	
	/**
	 * @param fncName
	 */
	public void setFncName(String fncName) {
		this.fncName = fncName == null ? null : fncName.trim();
	}
	
    /**
     * @return FncName
     */	
	public String getFncName() {
		return this.fncName;
	}
	
	/**
	 * @param fncConfDisName
	 */
	public void setFncConfDisName(String fncConfDisName) {
		this.fncConfDisName = fncConfDisName == null ? null : fncConfDisName.trim();
	}
	
    /**
     * @return FncConfDisName
     */	
	public String getFncConfDisName() {
		return this.fncConfDisName;
	}
	
	/**
	 * @param fncConfTyp
	 */
	public void setFncConfTyp(String fncConfTyp) {
		this.fncConfTyp = fncConfTyp == null ? null : fncConfTyp.trim();
	}
	
    /**
     * @return FncConfTyp
     */	
	public String getFncConfTyp() {
		return this.fncConfTyp;
	}
	
	/**
	 * @param fncConfDataCol
	 */
	public void setFncConfDataCol(Integer fncConfDataCol) {
		this.fncConfDataCol = fncConfDataCol;
	}
	
    /**
     * @return FncConfDataCol
     */	
	public Integer getFncConfDataCol() {
		return this.fncConfDataCol;
	}
	
	/**
	 * @param fncConfCotes
	 */
	public void setFncConfCotes(Integer fncConfCotes) {
		this.fncConfCotes = fncConfCotes;
	}
	
    /**
     * @return FncConfCotes
     */	
	public Integer getFncConfCotes() {
		return this.fncConfCotes;
	}
	
	/**
	 * @param noInd
	 */
	public void setNoInd(String noInd) {
		this.noInd = noInd == null ? null : noInd.trim();
	}
	
    /**
     * @return NoInd
     */	
	public String getNoInd() {
		return this.noInd;
	}
	
	/**
	 * @param comInd
	 */
	public void setComInd(String comInd) {
		this.comInd = comInd == null ? null : comInd.trim();
	}
	
    /**
     * @return ComInd
     */	
	public String getComInd() {
		return this.comInd;
	}
	
	/**
	 * @param headLeft
	 */
	public void setHeadLeft(String headLeft) {
		this.headLeft = headLeft == null ? null : headLeft.trim();
	}
	
    /**
     * @return HeadLeft
     */	
	public String getHeadLeft() {
		return this.headLeft;
	}
	
	/**
	 * @param headCenter
	 */
	public void setHeadCenter(String headCenter) {
		this.headCenter = headCenter == null ? null : headCenter.trim();
	}
	
    /**
     * @return HeadCenter
     */	
	public String getHeadCenter() {
		return this.headCenter;
	}
	
	/**
	 * @param headRight
	 */
	public void setHeadRight(String headRight) {
		this.headRight = headRight == null ? null : headRight.trim();
	}
	
    /**
     * @return HeadRight
     */	
	public String getHeadRight() {
		return this.headRight;
	}
	
	/**
	 * @param foodLeft
	 */
	public void setFoodLeft(String foodLeft) {
		this.foodLeft = foodLeft == null ? null : foodLeft.trim();
	}
	
    /**
     * @return FoodLeft
     */	
	public String getFoodLeft() {
		return this.foodLeft;
	}
	
	/**
	 * @param foodCenter
	 */
	public void setFoodCenter(String foodCenter) {
		this.foodCenter = foodCenter == null ? null : foodCenter.trim();
	}
	
    /**
     * @return FoodCenter
     */	
	public String getFoodCenter() {
		return this.foodCenter;
	}
	
	/**
	 * @param foodRight
	 */
	public void setFoodRight(String foodRight) {
		this.foodRight = foodRight == null ? null : foodRight.trim();
	}
	
    /**
     * @return FoodRight
     */	
	public String getFoodRight() {
		return this.foodRight;
	}
	
	/**
	 * @param dataDec1
	 */
	public void setDataDec1(String dataDec1) {
		this.dataDec1 = dataDec1 == null ? null : dataDec1.trim();
	}
	
    /**
     * @return DataDec1
     */	
	public String getDataDec1() {
		return this.dataDec1;
	}
	
	/**
	 * @param dataDec2
	 */
	public void setDataDec2(String dataDec2) {
		this.dataDec2 = dataDec2 == null ? null : dataDec2.trim();
	}
	
    /**
     * @return DataDec2
     */	
	public String getDataDec2() {
		return this.dataDec2;
	}
	
	/**
	 * @param dataDec3
	 */
	public void setDataDec3(String dataDec3) {
		this.dataDec3 = dataDec3 == null ? null : dataDec3.trim();
	}
	
    /**
     * @return DataDec3
     */	
	public String getDataDec3() {
		return this.dataDec3;
	}
	
	/**
	 * @param dataDec4
	 */
	public void setDataDec4(String dataDec4) {
		this.dataDec4 = dataDec4 == null ? null : dataDec4.trim();
	}
	
    /**
     * @return DataDec4
     */	
	public String getDataDec4() {
		return this.dataDec4;
	}
	
	/**
	 * @param dataDec5
	 */
	public void setDataDec5(String dataDec5) {
		this.dataDec5 = dataDec5 == null ? null : dataDec5.trim();
	}
	
    /**
     * @return DataDec5
     */	
	public String getDataDec5() {
		return this.dataDec5;
	}
	
	/**
	 * @param dataDec6
	 */
	public void setDataDec6(String dataDec6) {
		this.dataDec6 = dataDec6 == null ? null : dataDec6.trim();
	}
	
    /**
     * @return DataDec6
     */	
	public String getDataDec6() {
		return this.dataDec6;
	}
	
	/**
	 * @param dataDec7
	 */
	public void setDataDec7(String dataDec7) {
		this.dataDec7 = dataDec7 == null ? null : dataDec7.trim();
	}
	
    /**
     * @return DataDec7
     */	
	public String getDataDec7() {
		return this.dataDec7;
	}
	
	/**
	 * @param dataDec8
	 */
	public void setDataDec8(String dataDec8) {
		this.dataDec8 = dataDec8 == null ? null : dataDec8.trim();
	}
	
    /**
     * @return DataDec8
     */	
	public String getDataDec8() {
		return this.dataDec8;
	}

	public List<FncConfDefFmt> getFncConfDefFmtList() {
		return fncConfDefFmtList;
	}

	public void setFncConfDefFmtList(List<FncConfDefFmt> fncConfDefFmtList) {
		this.fncConfDefFmtList = fncConfDefFmtList;
	}
	

}