/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusCorpSingUpholdDto;
import cn.com.yusys.yusp.service.CusCorpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-10 10:48:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags= "对公客户信息")
@RestController
@RequestMapping("/api/cuscorp")
public class CusCorpResource {
    @Autowired
    private CusCorpService cusCorpService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusCorp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusCorp> list = cusCorpService.selectAll(queryModel);
        return new ResultDto<List<CusCorp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusCorp>> index(QueryModel queryModel) {
        List<CusCorp> list = cusCorpService.selectByModel(queryModel);
        return new ResultDto<List<CusCorp>>(list);
    }

    /**
     * @函数名称:queryCuscrop
     * @函数描述:国空类查询
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     * @author: zhoumw
     */
    @GetMapping("/querycuscrop")
    protected ResultDto<List<Map<String,Object>>> queryCuscrop(QueryModel queryModel) {
        List<Map<String,Object>> list = cusCorpService.queryCuscrop(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }

    /**
     * @函数名称:queryCuscrop
     * @函数描述:银企合作信息
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     * @author: zhoumw
     */
    @GetMapping("/querycuscropbank")
    protected ResultDto<List<Map<String,Object>>> queryCuscropBank(QueryModel queryModel) {
        List<Map<String,Object>> list = cusCorpService.queryCuscropBank(queryModel);
        return new ResultDto<List<Map<String,Object>>>(list);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{cusId}")
    protected ResultDto<CusCorp> show(@PathVariable("cusId") String cusId) {
//        CusCorp cusCorp = cusCorpService.selectByPrimaryKey(cusId);
        CusCorp cusCorp = cusCorpService.selectCropAndBaseByPrimarKey(cusId);
        return new ResultDto<CusCorp>(cusCorp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusCorp> create(@RequestBody CusCorp cusCorp) throws URISyntaxException {
        cusCorpService.insert(cusCorp);
        return new ResultDto<CusCorp>(cusCorp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusCorp cusCorp) throws URISyntaxException {
        CusCorp cp = cusCorpService.selectByPrimaryKey(cusCorp.getCusId());
        int result=0;
        if(cp==null){
             result = cusCorpService.insertSelective(cusCorp);
        }else {
             result = cusCorpService.updateSelective(cusCorp);
        }
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:updateSelective
     * @函数描述:对象修改，只更新非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateselective")
    protected ResultDto<Integer> updateSelective(@RequestBody CusCorp cusCorp) {
        int result = cusCorpService.updateSelective(cusCorp);
        return ResultDto.success(result);
    }
    /**
     * @函数名称:batchUpdateSelective
     * @函数描述:批量修改对象，只更新非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchupdateselective")
    protected ResultDto<Integer> batchUpdateSelective(@RequestBody List<CusCorp> cusCorpList) {
          int  inserFlag = cusCorpService.batchUpdateSelective(cusCorpList);
        return new ResultDto<Integer>(inserFlag);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cusId}")
    protected ResultDto<Integer> delete(@PathVariable("cusId") String cusId) {
        int result = cusCorpService.deleteByPrimaryKey(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusCorpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveTemp
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述: 客户创建暂存
     */
    @ApiOperation(value = "对公客户创建暂存")
    @PostMapping("/savecus")
    protected ResultDto<Integer> saveTemp(@Validated @RequestBody CusCorpDto cusCorpDto) {
        return ResultDto.success(cusCorpService.saveTemp(cusCorpDto));
    }

    /**
     * @函数名称:saveTemp
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述: 开户
     */
    @PostMapping("/openAccount")
    protected ResultDto<String> openAccount(@Validated @RequestBody CusCorpDto cusCorpDto) throws Exception {
        return ResultDto.success(cusCorpService.openAccount(cusCorpDto));
    }


    /**
     * @函数名称:index
     * @函数描述:对公客户基本信息列表查询
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModelXp1")
    protected ResultDto<List<Map<String, Object>>> selectByModelXp1(QueryModel queryModel) {
        List<Map<String, Object>> list = cusCorpService.selectByModelXp1(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }

    /**
     * @函数名称:queryCorpEvalInfoXp
     * @函数描述:查询对公企业评级必要信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCorpEvalInfoXp/{cusId}")
    protected ResultDto<Map<String, Object>> queryCorpEvalInfoXp(@PathVariable("cusId") String cusId) {
        Map<String, Object> result = cusCorpService.queryCorpEvalInfoXp(cusId);
        return new ResultDto<Map<String, Object>>(result);
    }

    /**
     * @函数名称:selectCusCorpSignUpholdByModel
     * @函数描述:条件查询国控类标识维护
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectCusCorpSignUpholdByModel")
    public ResultDto<List<CusCorpSingUpholdDto>> selectCusCorpSignUpholdByModel(@RequestBody QueryModel queryModel){
        List<CusCorpSingUpholdDto> result = cusCorpService.selectCusCorpSignUpholdByModel(queryModel);
        return  new ResultDto<List<CusCorpSingUpholdDto>>(result);
    }

	/**
     * @函数名称:queryCusCropDtoByCusId
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCusCropDtoByCusId")
    protected ResultDto<CusCorpDto> queryCusCropDtoByCusId(@RequestBody String cusId) {
        CusCorpDto cusCorpDto = cusCorpService.queryCusCropDtoByCusId(cusId);
        return new ResultDto<CusCorpDto>(cusCorpDto);
    }

 /**
     * @函数名称:show
     * @函数描述:通过客户号查询公司客户信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人:周茂伟
     */
    @ApiOperation(value = "对公客户查询")
    @PostMapping("/selectCrop")
    protected ResultDto<CusCorp> selectCrop(@Validated @RequestBody CusCorpIdDto CusCorpIdDto) {
        CusCorp cusCorp = cusCorpService.selectCropAndBaseByPrimarKey(CusCorpIdDto.getCusId());
        return new ResultDto<CusCorp>(cusCorp);
    }

    static class CusCorpIdDto{
        @NotBlank(message = "客户号不能为空")
        private String cusId;

        public CusCorpIdDto() {
        }

        public String getCusId() {
            return cusId;
        }

        public void setCusId(String cusId) {
            this.cusId = cusId;
        }
    }


    /**
     * @函数名称:selectCusCorp
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @PostMapping("/selectCusCorpList")
    protected ResultDto<List<CusCorp>> selectCusCorpList(@RequestBody QueryModel queryModel) {
        List<CusCorp> list = cusCorpService.selectByModel(queryModel);
        return new ResultDto<List<CusCorp>>(list);
    }
    /**
     * @函数名称:selectCusCorp
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:  为其他模块调用做准备
     * @param queryModel 分页查询类
     * @算法描述:
     * @创建人：xs
     */
    @ApiOperation(value = "对公客户多条件列表查询")
    @PostMapping("/selectCusCorpDtoList")
    protected @ResponseBody ResultDto<List<CusCorpDto>> selectCusCorpDtoList(@RequestBody QueryModel queryModel) {
        List<CusCorp> CusCorplist = cusCorpService.selectByModel(queryModel);
        List<CusCorpDto> CusCorpDtolist = CusCorplist.stream().map(e -> {
            CusCorpDto cusCorpDto = new CusCorpDto();
            BeanUtils.copyProperties(e, cusCorpDto);
            return cusCorpDto;
        }).collect(Collectors.toList());
        return new ResultDto<List<CusCorpDto>>(CusCorpDtolist);
    }


    /**
     * @Description:根据客户号删除客户在CusCorp表中的信息(逻辑删除),返回删除成功标志
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:15
     * @param cusId:客户号
     * @return: cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     **/
    @PostMapping("/deleteCusCorpByCusId")
    protected  ResultDto<Boolean> deleteCusCorpByCusId(@RequestBody String cusId){
        boolean deleteFlag= cusCorpService.deleteCusCorpByCusId(cusId);
        return new ResultDto<>(deleteFlag);
    }

    /**
     * @函数名称:checkAccount
     * @函数描述:校验用户在ECIF是否是正式用户
     * @param cusCorpDto
     */
    @PostMapping("/checkaccount")
    protected ResultDto<Integer> checkAccount(@Validated @RequestBody CusCorpDto cusCorpDto) {
        cusCorpService.checkAccount(cusCorpDto);
        return new ResultDto<>();
    }
    /**
     * @函数名称:fastCorpCreate
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述: 快捷公司客户开户
     */
    @PostMapping("/fastcorpcreate")
    protected ResultDto<String> fastCorpCreate(@Validated @RequestBody CusCorpDto cusCorpDto) throws Exception {
        return ResultDto.success(cusCorpService.fastCorpCreate(cusCorpDto));
    }

    /**
     * @函数名称:checkComScaleOp
     * @函数描述:校验企业规模
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkcomscaleop")
    protected ResultDto<String> checkComScaleOp(@RequestBody CusCorp cusCorp) {
        return ResultDto.success(cusCorpService.checkComScaleOp(cusCorp.getCusId(), cusCorp.getTradeClass()));
    }

    /**
     * @函数名称:selectCropByCusId
     * @函数描述:通过客户号查询公司客户信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人:zhuzr
     */
    @PostMapping("/selectcropbycusid")
    protected ResultDto<CusCorp> selectCropByCusId(@RequestBody String cusId) {
        CusCorp cusCorp = cusCorpService.selectCropAndBaseByCusId(cusId);
        return new ResultDto<CusCorp>(cusCorp);
    }

    /**
     * @函数名称:queryMinBankLoanLevel
     * @函数描述:获取该客户及其所在集团成员客户最低的外部评级
     * @参数与返回说明:
     * @算法描述:
     * @创建人:dumd
     */
    @PostMapping("/queryminbankloanlevel")
    protected ResultDto<String> queryMinBankLoanLevel(@RequestBody Map<String, String> map) {
        String minBankLoanLevel = cusCorpService.queryMinBankLoanLevel(map);
        return new ResultDto<>(minBankLoanLevel);
    }

    /**
     * @函数名称:updateEcifInfo
     * @函数描述:一键同步Ecif系统
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateecifinfo")
    protected ResultDto<Object> updateEcifInfo(@RequestBody String cusId) {
        cusCorpService.updateEcifInfo(cusId);
        return new ResultDto<>();
    }

}
