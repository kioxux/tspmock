/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.server.cmiscus0011.req.CmisCus0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.resp.CmisCus0011RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.req.CmisCus0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.resp.CmisCus0019RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.req.CmisCus0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.resp.CmisCus0025RespDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.CusLstYndApp;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:20:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusLstYndAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusLstYndApp selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectYndLstByCertCode
     * @方法描述: 查询优农贷名单信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    CmisCus0011RespDto selectYndLstByCertCode(CmisCus0011ReqDto cmisCus0011ReqDto);

    /**
     * @方法名称: selectYndLstByCertCode
     * @方法描述: 查询优农贷名单
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectYndCountByCertCode(CmisCus0019ReqDto cmisCus0019ReqDto);

    /**
     * @方法名称: selectdeatilYndCountByCertCode
     * @方法描述: 优农贷名单详细信息查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    CmisCus0025RespDto selectdeatilYndCountByCertCode(CmisCus0025ReqDto cmisCus0025ReqDto);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusLstYndApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusLstYndApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusLstYndApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusLstYndApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusLstYndApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
   
}