package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpSingUpholdDto
 * @类描述: CusCorpSingUpholdDto数据实体类
 * @功能描述: 国控类标识维护
 * @创建人: Zhengfq
 * @创建时间: 2021-04-13 10:06:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusCorpSingUpholdDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 客户分类 **/
	private String cusRankCls;

	/** 客户大类 **/
	private String cusCatalog;

	/** 管户客户经理 **/
	private String managerId;

	/** 主管机构 **/
	private String mainBrId;

	/** 管户客户经理 **/
	private String managerBrId;
	/** 客户名称 **/
	private String cusName;

	/** 客户名称 **/
	private String cusType;

	/** 客户编号 **/
	private String cusId;

	/** 证件类型 **/
	private String certType;

	/** 证件号码 **/
	private String certCode;

	/** 是否国控 **/
	private String isNatctl;

	/** 国控层级 **/
	private String natctlLevel;

	public String getCusRankCls() {
		return cusRankCls;
	}

	public void setCusRankCls(String cusRankCls) {
		this.cusRankCls = cusRankCls;
	}

	public String getCusCatalog() {
		return cusCatalog;
	}

	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getMainBrId() {
		return mainBrId;
	}

	public void setMainBrId(String mainBrId) {
		this.mainBrId = mainBrId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getIsNatctl() {
		return isNatctl;
	}

	public void setIsNatctl(String isNatctl) {
		this.isNatctl = isNatctl;
	}

	public String getNatctlLevel() {
		return natctlLevel;
	}

	public void setNatctlLevel(String natctlLevel) {
		this.natctlLevel = natctlLevel;
	}
}