package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusWebFinWhiteList
 * @类描述: cus_web_fin_white_list数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:24:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusWebFinWhiteListDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 名单流水号 **/
	private String serno;
	
	/** 客户编号 **/
	private String cusNo;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certNo;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 导入原因 **/
	private String inportResn;
	
	/** 经办人 **/
	private String huser;
	
	/** 经办机构 **/
	private String handOrg;
	
	/** 名单状态 **/
	private String listStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String lastUpdateId;
	
	/** 最后修改机构 **/
	private String lastUpdateBrId;
	
	/** 最后修改日期 **/
	private String lastUpdateDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 操作类型 **/
	private String oprType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusNo
	 */
	public void setCusNo(String cusNo) {
		this.cusNo = cusNo == null ? null : cusNo.trim();
	}
	
    /**
     * @return CusNo
     */	
	public String getCusNo() {
		return this.cusNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo == null ? null : certNo.trim();
	}
	
    /**
     * @return CertNo
     */	
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param inportResn
	 */
	public void setInportResn(String inportResn) {
		this.inportResn = inportResn == null ? null : inportResn.trim();
	}
	
    /**
     * @return InportResn
     */	
	public String getInportResn() {
		return this.inportResn;
	}
	
	/**
	 * @param huser
	 */
	public void setHuser(String huser) {
		this.huser = huser == null ? null : huser.trim();
	}
	
    /**
     * @return Huser
     */	
	public String getHuser() {
		return this.huser;
	}
	
	/**
	 * @param handOrg
	 */
	public void setHandOrg(String handOrg) {
		this.handOrg = handOrg == null ? null : handOrg.trim();
	}
	
    /**
     * @return HandOrg
     */	
	public String getHandOrg() {
		return this.handOrg;
	}
	
	/**
	 * @param listStatus
	 */
	public void setListStatus(String listStatus) {
		this.listStatus = listStatus == null ? null : listStatus.trim();
	}
	
    /**
     * @return ListStatus
     */	
	public String getListStatus() {
		return this.listStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param lastUpdateId
	 */
	public void setLastUpdateId(String lastUpdateId) {
		this.lastUpdateId = lastUpdateId == null ? null : lastUpdateId.trim();
	}
	
    /**
     * @return LastUpdateId
     */	
	public String getLastUpdateId() {
		return this.lastUpdateId;
	}
	
	/**
	 * @param lastUpdateBrId
	 */
	public void setLastUpdateBrId(String lastUpdateBrId) {
		this.lastUpdateBrId = lastUpdateBrId == null ? null : lastUpdateBrId.trim();
	}
	
    /**
     * @return LastUpdateBrId
     */	
	public String getLastUpdateBrId() {
		return this.lastUpdateBrId;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate == null ? null : lastUpdateDate.trim();
	}
	
    /**
     * @return LastUpdateDate
     */	
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}