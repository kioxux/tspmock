/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusManaTaskFnc
 * @类描述: cus_mana_task_fnc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 19:33:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_mana_task_fnc")
public class CusManaTaskFnc extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户代码 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
	private String cusId;
	
	/** 报表口径 **/
	@Column(name = "STAT_STYLE", unique = false, nullable = false, length = 1)
	private String statStyle;
	
	/** 报表周期类型 **/
	@Column(name = "STAT_PRD_STYLE", unique = false, nullable = false, length = 1)
	private String statPrdStyle;
	
	/** 报表期间 **/
	@Column(name = "STAT_PRD", unique = false, nullable = false, length = 6)
	private String statPrd;
	
	/** 资产样式编号 **/
	@Column(name = "STAT_BS_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statBsStyleId;
	
	/** 损益表编号 **/
	@Column(name = "STAT_PL_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statPlStyleId;
	
	/** 现金流量表编号 **/
	@Column(name = "STAT_CF_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statCfStyleId;
	
	/** 财务指标表编号 **/
	@Column(name = "STAT_FI_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statFiStyleId;
	
	/** 所有者权益变动表编号 **/
	@Column(name = "STAT_SOE_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statSoeStyleId;
	
	/** 财务简表编号 **/
	@Column(name = "STAT_SL_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statSlStyleId;
	
	/** 会计科目余额 **/
	@Column(name = "STAT_ACC_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statAccStyleId;
	
	/** 经济合作社财务收支明细 **/
	@Column(name = "STAT_DE_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statDeStyleId;
	
	/** 保留1 **/
	@Column(name = "STYLE_ID1", unique = false, nullable = true, length = 6)
	private String styleId1;
	
	/** 保留2 **/
	@Column(name = "STYLE_ID2", unique = false, nullable = true, length = 6)
	private String styleId2;
	
	/** 状态 **/
	@Column(name = "STATE_FLG", unique = false, nullable = true, length = 9)
	private String stateFlg;
	
	/** 是否新报表 **/
	@Column(name = "STAT_IS_NRPT", unique = false, nullable = true, length = 1)
	private String statIsNrpt;
	
	/** 是否经过审计 **/
	@Column(name = "STAT_IS_AUDIT", unique = false, nullable = true, length = 1)
	private String statIsAudit;
	
	/** 审计单位 **/
	@Column(name = "STAT_ADT_ENTR", unique = false, nullable = true, length = 60)
	private String statAdtEntr;
	
	/** 审计结论 **/
	@Column(name = "STAT_ADT_CONC", unique = false, nullable = true, length = 200)
	private String statAdtConc;
	
	/** 是否经过调整 **/
	@Column(name = "STAT_IS_ADJT", unique = false, nullable = true, length = 1)
	private String statIsAdjt;
	
	/** 财务报表调整原因 **/
	@Column(name = "STAT_ADJ_RSN", unique = false, nullable = true, length = 200)
	private String statAdjRsn;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "LAST_UPD_ID", unique = false, nullable = true, length = 20)
	private String lastUpdId;
	
	/** 更新日期 **/
	@Column(name = "LAST_UPD_DATE", unique = false, nullable = true, length = 10)
	private String lastUpdDate;
	
	/** 是否可修改 **/
	@Column(name = "IND_UPDATE", unique = false, nullable = true, length = 1)
	private String indUpdate;
	
	/** 资产样式(新)编号 **/
	@Column(name = "STAT_SOFP_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statSofpStyleId;
	
	/** 利润样式编号 **/
	@Column(name = "STAT_PS_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statPsStyleId;
	
	/** 资产负债类财务信息样式标号 **/
	@Column(name = "STAT_BSS_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statBssStyleId;
	
	/** 利润表类财务信息样式标号 **/
	@Column(name = "STAT_PSS_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statPssStyleId;
	
	/** 财务分析指标样式标号 **/
	@Column(name = "STAT_FAS_STYLE_ID", unique = false, nullable = true, length = 6)
	private String statFasStyleId;
	
	/** 是否简表{0:否,1:是} **/
	@Column(name = "BRIEF_FLAG", unique = false, nullable = true, length = 1)
	private String briefFlag;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 调整原因 **/
	@Column(name = "ADJEST_DESC", unique = false, nullable = true, length = 1000)
	private String adjestDesc;
	
	/** 报表录入类型 **/
	@Column(name = "REPORT_TYPE", unique = false, nullable = true, length = 4)
	private String reportType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param statStyle
	 */
	public void setStatStyle(String statStyle) {
		this.statStyle = statStyle;
	}
	
    /**
     * @return statStyle
     */
	public String getStatStyle() {
		return this.statStyle;
	}
	
	/**
	 * @param statPrdStyle
	 */
	public void setStatPrdStyle(String statPrdStyle) {
		this.statPrdStyle = statPrdStyle;
	}
	
    /**
     * @return statPrdStyle
     */
	public String getStatPrdStyle() {
		return this.statPrdStyle;
	}
	
	/**
	 * @param statPrd
	 */
	public void setStatPrd(String statPrd) {
		this.statPrd = statPrd;
	}
	
    /**
     * @return statPrd
     */
	public String getStatPrd() {
		return this.statPrd;
	}
	
	/**
	 * @param statBsStyleId
	 */
	public void setStatBsStyleId(String statBsStyleId) {
		this.statBsStyleId = statBsStyleId;
	}
	
    /**
     * @return statBsStyleId
     */
	public String getStatBsStyleId() {
		return this.statBsStyleId;
	}
	
	/**
	 * @param statPlStyleId
	 */
	public void setStatPlStyleId(String statPlStyleId) {
		this.statPlStyleId = statPlStyleId;
	}
	
    /**
     * @return statPlStyleId
     */
	public String getStatPlStyleId() {
		return this.statPlStyleId;
	}
	
	/**
	 * @param statCfStyleId
	 */
	public void setStatCfStyleId(String statCfStyleId) {
		this.statCfStyleId = statCfStyleId;
	}
	
    /**
     * @return statCfStyleId
     */
	public String getStatCfStyleId() {
		return this.statCfStyleId;
	}
	
	/**
	 * @param statFiStyleId
	 */
	public void setStatFiStyleId(String statFiStyleId) {
		this.statFiStyleId = statFiStyleId;
	}
	
    /**
     * @return statFiStyleId
     */
	public String getStatFiStyleId() {
		return this.statFiStyleId;
	}
	
	/**
	 * @param statSoeStyleId
	 */
	public void setStatSoeStyleId(String statSoeStyleId) {
		this.statSoeStyleId = statSoeStyleId;
	}
	
    /**
     * @return statSoeStyleId
     */
	public String getStatSoeStyleId() {
		return this.statSoeStyleId;
	}
	
	/**
	 * @param statSlStyleId
	 */
	public void setStatSlStyleId(String statSlStyleId) {
		this.statSlStyleId = statSlStyleId;
	}
	
    /**
     * @return statSlStyleId
     */
	public String getStatSlStyleId() {
		return this.statSlStyleId;
	}
	
	/**
	 * @param statAccStyleId
	 */
	public void setStatAccStyleId(String statAccStyleId) {
		this.statAccStyleId = statAccStyleId;
	}
	
    /**
     * @return statAccStyleId
     */
	public String getStatAccStyleId() {
		return this.statAccStyleId;
	}
	
	/**
	 * @param statDeStyleId
	 */
	public void setStatDeStyleId(String statDeStyleId) {
		this.statDeStyleId = statDeStyleId;
	}
	
    /**
     * @return statDeStyleId
     */
	public String getStatDeStyleId() {
		return this.statDeStyleId;
	}
	
	/**
	 * @param styleId1
	 */
	public void setStyleId1(String styleId1) {
		this.styleId1 = styleId1;
	}
	
    /**
     * @return styleId1
     */
	public String getStyleId1() {
		return this.styleId1;
	}
	
	/**
	 * @param styleId2
	 */
	public void setStyleId2(String styleId2) {
		this.styleId2 = styleId2;
	}
	
    /**
     * @return styleId2
     */
	public String getStyleId2() {
		return this.styleId2;
	}
	
	/**
	 * @param stateFlg
	 */
	public void setStateFlg(String stateFlg) {
		this.stateFlg = stateFlg;
	}
	
    /**
     * @return stateFlg
     */
	public String getStateFlg() {
		return this.stateFlg;
	}
	
	/**
	 * @param statIsNrpt
	 */
	public void setStatIsNrpt(String statIsNrpt) {
		this.statIsNrpt = statIsNrpt;
	}
	
    /**
     * @return statIsNrpt
     */
	public String getStatIsNrpt() {
		return this.statIsNrpt;
	}
	
	/**
	 * @param statIsAudit
	 */
	public void setStatIsAudit(String statIsAudit) {
		this.statIsAudit = statIsAudit;
	}
	
    /**
     * @return statIsAudit
     */
	public String getStatIsAudit() {
		return this.statIsAudit;
	}
	
	/**
	 * @param statAdtEntr
	 */
	public void setStatAdtEntr(String statAdtEntr) {
		this.statAdtEntr = statAdtEntr;
	}
	
    /**
     * @return statAdtEntr
     */
	public String getStatAdtEntr() {
		return this.statAdtEntr;
	}
	
	/**
	 * @param statAdtConc
	 */
	public void setStatAdtConc(String statAdtConc) {
		this.statAdtConc = statAdtConc;
	}
	
    /**
     * @return statAdtConc
     */
	public String getStatAdtConc() {
		return this.statAdtConc;
	}
	
	/**
	 * @param statIsAdjt
	 */
	public void setStatIsAdjt(String statIsAdjt) {
		this.statIsAdjt = statIsAdjt;
	}
	
    /**
     * @return statIsAdjt
     */
	public String getStatIsAdjt() {
		return this.statIsAdjt;
	}
	
	/**
	 * @param statAdjRsn
	 */
	public void setStatAdjRsn(String statAdjRsn) {
		this.statAdjRsn = statAdjRsn;
	}
	
    /**
     * @return statAdjRsn
     */
	public String getStatAdjRsn() {
		return this.statAdjRsn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param lastUpdId
	 */
	public void setLastUpdId(String lastUpdId) {
		this.lastUpdId = lastUpdId;
	}
	
    /**
     * @return lastUpdId
     */
	public String getLastUpdId() {
		return this.lastUpdId;
	}
	
	/**
	 * @param lastUpdDate
	 */
	public void setLastUpdDate(String lastUpdDate) {
		this.lastUpdDate = lastUpdDate;
	}
	
    /**
     * @return lastUpdDate
     */
	public String getLastUpdDate() {
		return this.lastUpdDate;
	}
	
	/**
	 * @param indUpdate
	 */
	public void setIndUpdate(String indUpdate) {
		this.indUpdate = indUpdate;
	}
	
    /**
     * @return indUpdate
     */
	public String getIndUpdate() {
		return this.indUpdate;
	}
	
	/**
	 * @param statSofpStyleId
	 */
	public void setStatSofpStyleId(String statSofpStyleId) {
		this.statSofpStyleId = statSofpStyleId;
	}
	
    /**
     * @return statSofpStyleId
     */
	public String getStatSofpStyleId() {
		return this.statSofpStyleId;
	}
	
	/**
	 * @param statPsStyleId
	 */
	public void setStatPsStyleId(String statPsStyleId) {
		this.statPsStyleId = statPsStyleId;
	}
	
    /**
     * @return statPsStyleId
     */
	public String getStatPsStyleId() {
		return this.statPsStyleId;
	}
	
	/**
	 * @param statBssStyleId
	 */
	public void setStatBssStyleId(String statBssStyleId) {
		this.statBssStyleId = statBssStyleId;
	}
	
    /**
     * @return statBssStyleId
     */
	public String getStatBssStyleId() {
		return this.statBssStyleId;
	}
	
	/**
	 * @param statPssStyleId
	 */
	public void setStatPssStyleId(String statPssStyleId) {
		this.statPssStyleId = statPssStyleId;
	}
	
    /**
     * @return statPssStyleId
     */
	public String getStatPssStyleId() {
		return this.statPssStyleId;
	}
	
	/**
	 * @param statFasStyleId
	 */
	public void setStatFasStyleId(String statFasStyleId) {
		this.statFasStyleId = statFasStyleId;
	}
	
    /**
     * @return statFasStyleId
     */
	public String getStatFasStyleId() {
		return this.statFasStyleId;
	}
	
	/**
	 * @param briefFlag
	 */
	public void setBriefFlag(String briefFlag) {
		this.briefFlag = briefFlag;
	}
	
    /**
     * @return briefFlag
     */
	public String getBriefFlag() {
		return this.briefFlag;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param adjestDesc
	 */
	public void setAdjestDesc(String adjestDesc) {
		this.adjestDesc = adjestDesc;
	}
	
    /**
     * @return adjestDesc
     */
	public String getAdjestDesc() {
		return this.adjestDesc;
	}
	
	/**
	 * @param reportType
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	
    /**
     * @return reportType
     */
	public String getReportType() {
		return this.reportType;
	}


}