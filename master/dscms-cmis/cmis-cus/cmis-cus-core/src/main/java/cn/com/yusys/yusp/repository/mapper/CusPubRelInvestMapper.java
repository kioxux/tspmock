/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.CusCorpRelationsDto;
import cn.com.yusys.yusp.dto.CusCorpRelationsRequestDto;
import cn.com.yusys.yusp.dto.CusRelationsRequestDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusPubRelInvest;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus模块
 * @类名称: CusPubRelInvestMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-22 21:11:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusPubRelInvestMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusPubRelInvest selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: queryBycusId
     * @方法描述: 根据客户号查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    List<CusPubRelInvest> queryBycusId(@Param("cusId") String cusId);
    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusPubRelInvest> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusPubRelInvest record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusPubRelInvest record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusPubRelInvest record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusPubRelInvest record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 获取对外投资信息
     * @param certType
     * @param certCode
     * @return
     */
    CusPubRelInvest selectCusCorpRelByCodeAndType(@Param("certType")String certType, @Param("certCode")String certCode,@Param("cusId")String cusId);

    /**
     * 获取对外投资关系
     * @param cusCorpRelationsRequestDto
     * @return
     */
    List<CusCorpRelationsDto> selectCusCorpPubComDto(CusCorpRelationsRequestDto cusCorpRelationsRequestDto);

    /**
     * 根据被投资人客户编号查询股东信息
     * @param cusId
     * @return
     */
    List<Map<String,String>> queryCusInfoByCusId(@Param("cusId")String cusId);

}