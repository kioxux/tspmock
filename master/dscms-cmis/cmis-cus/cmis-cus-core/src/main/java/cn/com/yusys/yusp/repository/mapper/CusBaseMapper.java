/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0008.req.CmisCus0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.resp.CmisCus0008RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.req.Xdkh0019DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: CusBaseMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-14 09:59:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusBaseMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusBase selectByPrimaryKey(@Param("cusId") String cusId);

    /**
     * @方法名称: queryCusInfoByCertCode
     * @方法描述: 根据证件查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusBase queryCusInfoByCertCode(@Param("certCode") String certCode);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusBase> selectByModel(QueryModel model);

    /**
     * @方法名称: selectBaseIndiv
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusBaseDto> selectBaseIndiv(QueryModel model);

    /**
     * @方法名称: selectBaseCrop
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusBaseDto> selectBaseCrop(QueryModel model);

    /**
     * @方法名称: selectBaseCrop
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusBaseDto> selectBaseCropDis(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CusBase record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CusBase record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CusBase record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CusBase record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("cusId") String cusId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    CusRelationsDto findCusBaseByCusId(@Param("cusId") String cusId);

    CusCorpRelationsDto findCusCorpBaseByCusId(@Param("cusId") String cusId);

    /**
     * 更新客户状态
     *
     * @param cusBase
     * @return
     */
    int updateCusBaseByCusState(CusBase cusBase);

    /**
     * 查询cusbase 和 cusIndiv   个人客户信息
     *
     * @param cusId
     * @return
     */
    Map<String, String> getCusInfoByCusId(@Param("cusId") String cusId);

    /**
     * @param cusQueryInfoMap
     * @return
     */
    List<CusBase> queryCusInfo(Map<String, Object> cusQueryInfoMap);

    /**
     * 根据证件号证件类型查询客户编号
     *
     * @param xdkh0019DataReqDto
     * @return
     */
    String getCusIdByCertCode(Xdkh0019DataReqDto xdkh0019DataReqDto);

    /**
     * 根据证件号证件类型查询客户编号
     *
     * @param cusIndivDto
     * @return
     */
    CusBase selectByCertCode(CusIndivDto cusIndivDto);

    /**
     * 根据证件号列表查询客户信息
     *
     * @param certCodes
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdkh0024.resp.List> getCusListByCusIds(java.util.List certCodes);

    /**
     * 根据客户编号查询法人名称,名称，证件类型，证件号，电话
     *
     * @param cmisCus0008ReqDto
     * @return
     */
    CmisCus0008RespDto getCmiscus0008(CmisCus0008ReqDto cmisCus0008ReqDto);

    /**
     * 根据客户号查询客户大类
     *
     * @param cusId
     * @return
     */
    String selectCusCatalogByCusId(@Param("cusId") String cusId);

    /**
     * @param certCode
     * @return
     * @方法描述:查看该身份证号码是否已经被开户
     * @创建人:chenyong
     */
    int isExistAccount(@Param("certCode") String certCode);

    /**
     * @param certCode
     * @return
     * @方法描述:查询客户状态
     * @创建人:chenyong
     */
    String queryCusStatus(@Param("certCode") String certCode);

    /**
     * @param model
     * @return
     * @方法描述:更新客户号和客户状态为正式客户
     * @创建人:chenyong
     */
    int updateCusStates(QueryModel model);

    /**
     * @param model
     * @return
     * @方法描述:已经为正式客户更新信贷客户信息手机号码
     * @创建人:chenyong
     */
    int updateMobileNo(QueryModel model);

    /**
     * @param cusId
     * @return
     * @方法描述:这里面还存在一种情况就是身份证号没有被开户，但是在信贷开户时传的别人的客户号，这个当然不可以
     * @创建人:chenyong
     */
    int isExistCusID(@Param("cusId") String cusId);

    /**
     * @param model
     * @return
     * @方法描述:CusBase表
     * @创建人:chenyong
     */
    int inertCusBase(QueryModel model);

    /**
     * @param model
     * @return
     * @方法描述：更新CusIndiv
     * @创建人:chenyong
     */
    int updateCusIndiv(QueryModel model);

    /**
     * @param model
     * @return
     * @方法描述：更新 CusIndivContact
     * @创建人:chenyong
     */
    int updateCusIndivContact(QueryModel model);

    /**
     * @param model
     * @return
     * @方法描述：更新 CusIndivUnit
     * @创建人:chenyong
     */
    int updateCusIndivUnit(QueryModel model);


    /**
     * @param model
     * @return
     * @方法描述：优享贷开户时风控传入是否农户值更新cus_indiv表
     * @创建人:chenyong
     */
    int updateIsAgriFlg(QueryModel model);

    /**
     * @param cusId
     * @return
     * @方法描述：判断是否存在cus_id在CUS_INDIV表
     * @创建人:chenyong
     */
    int queryIsExistCusIdInCusIndiv(@Param("cusId") String cusId);

    /**
     * @param cusId
     * @return
     * @方法描述：判断是否存在cus_id在cusIndivContact表
     * @创建人:chenyong
     */
    int queryIsExistCusIdInCusIndivContact(@Param("cusId") String cusId);

    /**
     * @param cusId
     * @return
     * @方法描述：判断是否存在cus_id在CusIndivUnit表
     * @创建人:chenyong
     */
    int queryIsExistCusIdInCusIndivUnit(@Param("cusId") String cusId);

    /**
     * @param cusId
     * @return
     * @方法描述：判断是否存在cus_id在cusbase表
     * @创建人:chenyong
     */
    String queryCusBaseByCusid(@Param("cusId") String cusId);
    /**
     * @param model
     * @return
     * @方法描述：更新 CusBase
     * @创建人:ouyangjie
     */
    int updateCusIdByCusId(QueryModel model);
    /**
     * @param model
     * @return
     * @方法描述:插入CusBase表
     * @创建人:ouyangjie
     */
    int inertCusBaseSomeInfo(QueryModel model);
    /**
     * 通过证件号查询客户编号和名称
     *
     * @param certCode
     * @return
     * @创建人:ouyangjie
     */
    Map<String, String> getCusInfoByCertCode(@Param("certCode") String certCode);
    /**
     * @param model
     * @return
     * @方法描述:插入CusBase表
     * @创建人:ouyangjie
     */
    int inertCusBaseSomeInfo2(QueryModel model);

    /**
     * @Description:根据客户号删除客户在cusBase表中的信息(逻辑删除)
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:22
     * @param cusId: 客户号
     * @return: boolean
     **/
    int deleteCusBaseByCusId(String cusId);


    /**
     * @param model
     * @return
     * @author wzy
     * @date 2021/6/18 14:34
     * @version 1.0.0
     * @desc 根据条件查询个人客户列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<CusBaseDto> selectByCondition(QueryModel model);

    /**
     * @创建人 WH
     * @创建时间 2021/8/4 15:26
     * @注释 拼接表查询增享贷客户信息额外参数
     */
    CusForZxdDto selectbycusidforzxd(String cusId);

    /**
     * 根据客户经理号获取其管户客户号
     * @author jijian_yx
     * @date 2021/8/13 21:13
     **/
    List<String> queryCusIdByManagerId(String managerId);

    /**
     * 获取机构及下属机构客户
     * @author jijian_yx
     * @date 2021/8/30 21:52
     **/
    List<CusBase> selectByOrgs(QueryModel model);

    /**
     * @方法名称: queryCusBaseByCusId
     * @方法描述: 根据客户编号查询信息插入被告人信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusBase queryCusBaseByCusId(@Param("cusId") String cusId);

    /**
     * 根据客户编号查询信息
     * @param cusId
     * @return
     */
    CusBase selectCusBaseByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: selectCusByModel
     * @方法描述: 查询客户对象列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusBase> selectCusByModel(QueryModel model);

    /**
     * @方法名称: selectBaseCrop
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusBaseDto> selectBaseIntbank(QueryModel model);

    /**
     * 根据客户号获取年份集合
     * @author jijian_yx
     * @date 2021/9/27 15:37
     **/
    List<String> getAccTimeList(@Param("cusId") String cusId);

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     * @author jijian_yx
     * @date 2021/9/28 0:06
     **/
    List<DocAccSearchDto> queryAccListByDocTypeAndYear(QueryModel queryModel);

    /**
     * @方法名称: queryCusBaseByCusId
     * @方法描述: 根据客户编号查询信息插入被告人信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusBaseDto queryCusBaseCropByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: queryCusBaseByCusId
     * @方法描述: 根据客户编号查询信息插入被告人信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusBaseDto queryCusBaseIndivByCusId(@Param("cusId") String cusId);

}
