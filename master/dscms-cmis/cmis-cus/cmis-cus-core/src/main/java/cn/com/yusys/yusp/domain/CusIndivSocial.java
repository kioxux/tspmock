/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivSocial
 * @类描述: cus_indiv_social数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-10 14:15:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_indiv_social")
public class CusIndivSocial extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 是否家庭成员 **/
	@Column(name = "IS_FAMILY_MEM", unique = false, nullable = true, length = 5)
	private String isFamilyMem;
	
	/** 与客户关系 **/
	@Column(name = "INDIV_CUS_REL", unique = false, nullable = true, length = 5)
	private String indivCusRel;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 20)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 姓名 **/
	@Column(name = "NAME", unique = false, nullable = true, length = 10)
	private String name;
	
	/** 关联客户编号 **/
	@Column(name = "CUS_ID_REL", unique = false, nullable = true, length = 40)
	private String cusIdRel;
	
	/** 性别 **/
	@Column(name = "SEX", unique = false, nullable = true, length = 5)
	private String sex;
	
	/** 年收入（元） **/
	@Column(name = "YEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearn;
	
	/** 职业 **/
	@Column(name = "OCCU", unique = false, nullable = true, length = 20)
	private String occu;
	
	/** 职务 **/
	@Column(name = "DUTY", unique = false, nullable = true, length = 20)
	private String duty;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 10)
	private String oprType;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 200)
	private String remark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 10)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 80)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 10)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 80)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 10)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param isFamilyMem
	 */
	public void setIsFamilyMem(String isFamilyMem) {
		this.isFamilyMem = isFamilyMem;
	}
	
    /**
     * @return isFamilyMem
     */
	public String getIsFamilyMem() {
		return this.isFamilyMem;
	}
	
	/**
	 * @param indivCusRel
	 */
	public void setIndivCusRel(String indivCusRel) {
		this.indivCusRel = indivCusRel;
	}
	
    /**
     * @return indivCusRel
     */
	public String getIndivCusRel() {
		return this.indivCusRel;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certNo
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
    /**
     * @return name
     */
	public String getName() {
		return this.name;
	}

	public void setCusIdRel(String cusIdRel) {
		this.cusIdRel = cusIdRel;
	}
	/**
	 * @return cusIdRel
	 */
	public String getCusIdRel() {
		return this.cusIdRel;
	}

	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param yearn
	 */
	public void setYearn(java.math.BigDecimal yearn) {
		this.yearn = yearn;
	}
	
    /**
     * @return yearn
     */
	public java.math.BigDecimal getYearn() {
		return this.yearn;
	}
	
	/**
	 * @param occu
	 */
	public void setOccu(String occu) {
		this.occu = occu;
	}
	
    /**
     * @return occu
     */
	public String getOccu() {
		return this.occu;
	}
	
	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}
	
    /**
     * @return duty
     */
	public String getDuty() {
		return this.duty;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}