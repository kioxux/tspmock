package cn.com.yusys.yusp.service.client.bsp.ecif.s00102;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.RelArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2EcifClientService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 业务逻辑处理类：对私客户创建及维护接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
@Transactional
public class S00102Service {
    private static final Logger logger = LoggerFactory.getLogger(S00102Service.class);

    // 1）注入：BSP封装调用ECIF系统的接口
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    /**
     * 业务逻辑处理方法：对私客户创建及维护接口
     *
     * @param s00102ReqDto
     * @return
     */
    @Transactional
    public S00102RespDto s00102(S00102ReqDto s00102ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value, JSON.toJSONString(s00102ReqDto));
        ResultDto<S00102RespDto> s00102ResultDto = dscms2EcifClientService.s00102(s00102ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value, JSON.toJSONString(s00102ResultDto));

        String s00102Code = Optional.ofNullable(s00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String s00102Meesage = Optional.ofNullable(s00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        S00102RespDto s00102RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, s00102ResultDto.getCode())) {
            //  获取相关的值并解析
            s00102RespDto = s00102ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(s00102Code, s00102Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value);
        return s00102RespDto;
    }


}
