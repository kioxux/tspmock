/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.CusIndivContactDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.service.CusIndivService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusIndivContact;
import cn.com.yusys.yusp.service.CusIndivContactService;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusIndivContactResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-11-16 11:44:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags= "个人客户地址联系信息")
@RestController
@RequestMapping("/api/cusindivcontact")
public class CusIndivContactResource {
    @Autowired
    private CusIndivContactService cusIndivContactService;

    @Autowired
    private CusIndivService cusIndivService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusIndivContact>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusIndivContact> list = cusIndivContactService.selectAll(queryModel);
        return new ResultDto<List<CusIndivContact>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusIndivContact>> index(QueryModel queryModel) {
        List<CusIndivContact> list = cusIndivContactService.selectByModel(queryModel);
        return new ResultDto<List<CusIndivContact>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cusId}")
    protected ResultDto<CusIndivContact> show(@PathVariable("cusId") String cusId) {
        CusIndivContact cusIndivContact = cusIndivContactService.selectByPrimaryKey(cusId);
        return new ResultDto<CusIndivContact>(cusIndivContact);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusIndivContact> create(@RequestBody CusIndivContact cusIndivContact) throws URISyntaxException {
        cusIndivContactService.insert(cusIndivContact);
        return new ResultDto<CusIndivContact>(cusIndivContact);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusIndivContact cusIndivContact) throws URISyntaxException {
        int result = cusIndivContactService.update(cusIndivContact);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cusId}")
    protected ResultDto<Integer> delete(@PathVariable("cusId") String cusId) {
        int result = cusIndivContactService.deleteByPrimaryKey(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusIndivContactService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "个人客户地址信息查询")
    @PostMapping("/queryCusIndivContact")
    protected ResultDto<CusIndivContact> queryCusIndivContact(@Validated @RequestBody CusIndivContact cusIndivContact) {
        CusIndivContact cusIndivContacts = cusIndivContactService.selectByPrimaryKey(cusIndivContact.getCusId());
        return new ResultDto<CusIndivContact>(cusIndivContacts);
    }

    /**
     * 个人客户 概况信息  联系信息保存按钮业务逻辑
     * @param cusIndivContact
     * @return
     */
    @ApiOperation(value = "个人客户地址信息保存")
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody CusIndivContact cusIndivContact) {
        int result = cusIndivContactService.save(cusIndivContact);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCusIndivByCusId")
    protected CusIndivContactDto queryCusIndivByCusId(@RequestBody String cusId) {
        CusIndivContact cusIndivContact = cusIndivContactService.selectByPrimaryKey(cusId);
        CusIndivContactDto cusIndivContactDto = null;
        if (!Objects.isNull(cusIndivContact)) {
            cusIndivContactDto = new CusIndivContactDto();
            BeanUtils.copyProperties(cusIndivContact, cusIndivContactDto);
        }
        return cusIndivContactDto;
    }

    /**
     * @param cusIndivContact
     * @return cn.com.yusys.yusp.dto.CusIndivContactDto
     * @author hubp
     * @date 2021/8/30 21:06
     * @version 1.0.0
     * @desc  通过客户ID查找信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbycusid")
    protected ResultDto<CusIndivContact> selectByCusId(@RequestBody CusIndivContact cusIndivContact) {
        CusIndivContact cusIndiv = cusIndivContactService.selectByPrimaryKey(cusIndivContact.getCusId());
        return new ResultDto<CusIndivContact>(cusIndiv);
    }
}
