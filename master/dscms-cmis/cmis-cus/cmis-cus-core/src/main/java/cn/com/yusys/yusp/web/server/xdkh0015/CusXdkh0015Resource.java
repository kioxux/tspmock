package cn.com.yusys.yusp.web.server.xdkh0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0015.req.Xdkh0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0015.resp.Xdkh0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0015.Xdkh0015Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:农拍档接受白名单维护
 *
 * @author code-generator
 * @version 1.0
 */
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0015Resource.class);

    @Autowired
    private Xdkh0015Service xdkh0015Service;

    /**
     * 交易码：xdkh0015
     * 交易描述：农拍档接受白名单维护
     *
     * @param xdkh0015DataReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdkh0015")
    protected @ResponseBody
    ResultDto<Xdkh0015DataRespDto> xdkh0015(@Validated @RequestBody Xdkh0015DataReqDto xdkh0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, JSON.toJSONString(xdkh0015DataReqDto));
        // 响应Dto:农拍档接受白名单维护
        Xdkh0015DataRespDto xdkh0015DataRespDto = new Xdkh0015DataRespDto();
        ResultDto<Xdkh0015DataRespDto> xdkh0015DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, JSON.toJSONString(xdkh0015DataReqDto));
            xdkh0015DataRespDto = xdkh0015Service.xdkh0015(xdkh0015DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, JSON.toJSONString(xdkh0015DataRespDto));
            //响应Data:农拍档接受白名单维护
            String opFlag = xdkh0015DataRespDto.getOpFlag();
            String opMessage = xdkh0015DataRespDto.getOpMsg();
            //如果失败，返回9999
            if ("F".equals(opFlag)) {//失败
                xdkh0015DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdkh0015DataResultDto.setMessage(opMessage);
            } else {
                xdkh0015DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdkh0015DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            xdkh0015DataRespDto.setOpFlag("F");
            xdkh0015DataRespDto.setOpMsg(EpbEnum.EPB099999.value);
            // 封装xdkh0015DataResultDto中异常返回码和返回信息
            xdkh0015DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0015DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0015DataRespDto到xdkh0015DataResultDto中
        xdkh0015DataResultDto.setData(xdkh0015DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, JSON.toJSONString(xdkh0015DataResultDto));
        return xdkh0015DataResultDto;
    }
}
