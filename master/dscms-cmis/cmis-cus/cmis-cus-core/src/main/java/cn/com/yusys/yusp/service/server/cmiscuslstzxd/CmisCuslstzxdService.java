package cn.com.yusys.yusp.service.server.cmiscuslstzxd;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusLstZxd;
import cn.com.yusys.yusp.dto.BizCusLstZxdDto;
import cn.com.yusys.yusp.dto.CusLstZxdDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.CmisBizClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 接口处理类: 查询增享贷白名单
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class CmisCuslstzxdService {

    private static final Logger logger = LoggerFactory.getLogger(CmisCuslstzxdService.class);

    @Autowired
    private CmisBizClientService cmisBizClientService;

    /**
     * 查询增享贷白名单
     * @param serno
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public CusLstZxdDto cmiscuslstzxd(String serno) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.key, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.value, serno);
        CusLstZxdDto  cusLstZxdDto = new CusLstZxdDto();
        try {
            BizCusLstZxdDto bizCusLstZxdDto = cmisBizClientService.selectByPrimaryKey(serno).getData();
            BeanUtils.copyProperties(bizCusLstZxdDto, cusLstZxdDto);
            Integer repayTerm = bizCusLstZxdDto.getRepayTerm();
            cusLstZxdDto.setRepayTerm(repayTerm == null ? null : repayTerm.toString());
            Integer overdueCount = bizCusLstZxdDto.getOverdueCount();
            cusLstZxdDto.setOverdueCount(overdueCount == null ? null : overdueCount.toString());
            Integer memberOverdueCount = bizCusLstZxdDto.getMemberOverdueCount();
            cusLstZxdDto.setMemberOverdueCount(memberOverdueCount == null ? null : memberOverdueCount.toString());
        } catch (BeansException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.key, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.key, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.value, JSON.toJSONString(cusLstZxdDto));
        return cusLstZxdDto;
    }
}
