package cn.com.yusys.yusp.web.server.cmiscus0024;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0024.req.CmisCus0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0024.resp.CmisCus0024RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.cmiscus0024.CmisCus0024Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(tags = "CmisCus0024:根据行号或BICCODE获取同业客户号")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0024Resource.class);

    @Autowired
    private CmisCus0024Service cmisCus0024Service;

    @ApiOperation("根据行号或BICCODE获取同业客户号")
    @PostMapping("/cmiscus0024")
    protected @ResponseBody ResultDto<CmisCus0024RespDto> cmiscus0024(
            @Validated @RequestBody CmisCus0024ReqDto cmisCus0024ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0024.key, DscmsEnum.TRADE_CODE_CMISCUS0024.value, JSON.toJSONString(cmisCus0024ReqDto));
        ResultDto<CmisCus0024RespDto> cmisCus0024ResultDto = new ResultDto<>();
        CmisCus0024RespDto cmisCus0024RespDto = null;
        try {
            cmisCus0024RespDto = cmisCus0024Service.execute(cmisCus0024ReqDto);
        } catch (YuspException e) {
            e.printStackTrace();
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0023.key, DscmsEnum.TRADE_CODE_CMISCUS0023.value, e.getMessage());
            // 封装cmisCus0023ResultDto中异常返回码和返回信息
            cmisCus0024ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0024ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        cmisCus0024ResultDto.setData(cmisCus0024RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0024.key, DscmsEnum.TRADE_CODE_CMISCUS0024.value, JSON.toJSONString(cmisCus0024ResultDto));
        return cmisCus0024ResultDto;
    }

}
