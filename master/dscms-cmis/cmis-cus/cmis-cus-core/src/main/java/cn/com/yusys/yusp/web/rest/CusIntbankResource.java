/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;


import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusIntbank;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CusIntbankDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CusIntbankService;
import cn.com.yusys.yusp.service.MessageSendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.management.Query;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIntbankResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 乐友先生
 * @创建时间: 2021-04-08 11:10:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusintbank")
public class CusIntbankResource {
    @Autowired
    private CusIntbankService cusIntbankService;
    @Autowired
    MessageSendService messageSendService;
    @Autowired
    AdminSmUserService adminSmUserService;

    private static final Logger logger = LoggerFactory.getLogger(CusIntbankResource.class);
    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusIntbank>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusIntbank> list = cusIntbankService.selectAll(queryModel);
        return new ResultDto<List<CusIntbank>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CusIntbank>> index(@RequestBody QueryModel queryModel) {
        List<CusIntbank> list = cusIntbankService.selectByModel(queryModel);
        return new ResultDto<List<CusIntbank>>(list);
    }
    /**
    * 同业客户查询，无控制；
    *
    *
    */
    @PostMapping("/queryall")
    protected ResultDto<List<CusIntbank>> queryall(@RequestBody QueryModel queryModel) {
        List<CusIntbank> list = cusIntbankService.selectByModel(queryModel);
        return new ResultDto<List<CusIntbank>>(list);
    }
    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querylist")
    protected ResultDto<List<CusIntbank>> queryList(@RequestBody QueryModel queryModel) {
        List<CusIntbank> list = cusIntbankService.selectByModel(queryModel);
        return new ResultDto<List<CusIntbank>>(list);
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cusId}")
    protected ResultDto<CusIntbank> show(@PathVariable("cusId") String cusId) {
        CusIntbank cusIntbank = cusIntbankService.selectByPrimaryKey(cusId);
        return new ResultDto<CusIntbank>(cusIntbank);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CusIntbank> create(@RequestBody CusIntbank cusIntbank) throws URISyntaxException {
        cusIntbankService.insert(cusIntbank);
        return new ResultDto<CusIntbank>(cusIntbank);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusIntbank cusIntbank) throws URISyntaxException {
        int result = cusIntbankService.update(cusIntbank);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cusId}")
    protected ResultDto<Integer> delete(@PathVariable("cusId") String cusId) {
        int result = cusIntbankService.deleteByPrimaryKey(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusIntbankService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/query/selectByModeList")
    protected ResultDto<List<CusIntbank>> selectModeList(@RequestBody CusIntbank cusIntbank) {
        List<CusIntbank> list = cusIntbankService.selectModeList(cusIntbank);
        return new ResultDto<List<CusIntbank>>(list);
    }

    /**
     * @函数名称:openAccount
     * @函数描述:同业客户验证
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/openAccount")
    protected ResultDto<CusIntbankDto> openAccount(@Validated @RequestBody CusIntbankDto cusIntbankDto) throws Exception {
        CusIntbankDto cusIntbankDtoData = cusIntbankService.openAccount(cusIntbankDto);
        return ResultDto.success(cusIntbankDtoData);
    }

    /**
     * @函数名称:saveTemp
     * @函数描述:
     * @参数与返回说明:
     * @算法描述: 同业客户创建暂存/提交
     */
    @Transactional
    @PostMapping("/saveTempcusIntbank")
    protected ResultDto<Integer> saveTemp(@RequestBody CusIntbank cusIntbank) {
        Integer resultDto = cusIntbankService.saveTemp(cusIntbank);
        try {
            MessageSendDto messageSendDto = new MessageSendDto();
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            ReceivedUserDto receivedUserDto = new ReceivedUserDto();
            receivedUserDto.setReceivedUserType("1");
            receivedUserDto.setUserId(cusIntbank.getManagerId());
            ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(cusIntbank.getManagerId());
            receivedUserDto.setMobilePhone(byLoginCode.getData().getUserMobilephone());
            receivedUserList.add(receivedUserDto);
            messageSendDto.setMessageType("MSG_KH_M_0001");
            Map<String,String> map = new HashMap<>();
            map.put("cusName",cusIntbank.getCusName());
            messageSendDto.setParams(map);
            messageSendDto.setReceivedUserList(receivedUserList);
            messageSendService.sendMessage(messageSendDto);
        } catch (Exception e) {
            //异常
            logger.info(e.getMessage());
        }
        return new ResultDto<Integer>(resultDto);
    }

    @PostMapping("/savecusIntbank")
    protected ResultDto<Integer> save(@RequestBody CusIntbank cusIntbank) {
        return ResultDto.success(cusIntbankService.saveTemp(cusIntbank));
    }


    /**
     * @函数名称:queryCusIntbankBySocialCreditCode
     * @函数描述:根据社会信用代码查询同业客户信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCusIntbankBySocialCreditCode")
    protected ResultDto<CusIntbank> queryCusIntbankBySocialCreditCode(@RequestBody CusIntbank cusIntbankData) {
        CusIntbank cusIntbank = cusIntbankService.queryCusIntbankBySocialCreditCode(cusIntbankData);
        return new ResultDto<CusIntbank>(cusIntbank);
    }

}
