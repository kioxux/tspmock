package cn.com.yusys.yusp.service.server.xdkh0018;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.RelArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.req.IdchekReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0018.req.Xdkh0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0018.resp.Xdkh0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import cn.com.yusys.yusp.service.client.bsp.ecif.g00101.G00101Service;
import cn.com.yusys.yusp.service.client.bsp.ecif.g00102.G00102Service;
import cn.com.yusys.yusp.service.client.bsp.ecif.s00101.S00101Service;
import cn.com.yusys.yusp.service.client.bsp.ecif.s00102.S00102Service;
import cn.com.yusys.yusp.service.client.bsp.gaps.idchek.IdchekService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0018Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0018Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0018Service.class);
    @Autowired
    private G00102Service g00102Service;//业务逻辑处理类：对公客户综合信息维护
    @Autowired
    private S00102Service s00102Service;//业务逻辑处理类：对私客户创建及维护接口
    @Autowired
    private S00101Service s00101Service;//业务逻辑处理类：对私客户查询
    @Autowired
    private G00101Service g00101Service;//业务逻辑处理类：对公客户信息查询
    @Autowired
    private IdchekService idchekService;//业务逻辑处理类：身份证核查
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private CusBaseMapper cusBaseMapper;
    @Resource
    private CusIndivMapper cusIndivMapper;
    @Resource
    private CusCorpMapper cusCorpMapper;

    /**
     * 临时客户信息维护
     *
     * @param xdkh0018DataReqDto
     * @return
     */

    @Transactional
    public Xdkh0018DataRespDto xdkh0018(Xdkh0018DataReqDto xdkh0018DataReqDto)  throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value);
        Xdkh0018DataRespDto xdkh0018DataRespDto = new Xdkh0018DataRespDto();
        // 从xdkh0018DataReqDto获取业务值进行业务逻辑处理
        String dgdslx = xdkh0018DataReqDto.getDgdslx();//对公对私类型
        String zjtype = xdkh0018DataReqDto.getZjtype();//证件类型
        String zjcode = xdkh0018DataReqDto.getZjcode();//证件号码
        String kehumc = xdkh0018DataReqDto.getKehumc();//客户名称
        String contry = xdkh0018DataReqDto.getContry();//国别
        String kehulx = xdkh0018DataReqDto.getKehulx();//客户类型
        String whgudg = xdkh0018DataReqDto.getWhgudg();//是否我行股东
        String xigbie = xdkh0018DataReqDto.getXigbie();//性别（对私必输）
        String csriqi = xdkh0018DataReqDto.getCsriqi();//出生日期（对私必输）
        String zuigxl = xdkh0018DataReqDto.getZuigxl();//最高学历（对私必输）
        String zuigxw = xdkh0018DataReqDto.getZuigxw();//最高学位（对私必输）
        String xdzhiy = xdkh0018DataReqDto.getXdzhiy();//职业（对私必输）
        String xdduty = xdkh0018DataReqDto.getXdduty();//职务（对私必输）
        String xdzhic = xdkh0018DataReqDto.getXdzhic();//职称（对私必输）
        String txdizh = xdkh0018DataReqDto.getTxdizh();//通讯地址（对私必输）
        String zzdiah = xdkh0018DataReqDto.getZzdiah();//住宅电话（对私必输）
        String sfdanb = xdkh0018DataReqDto.getSfdanb();//是否为担保公司（对公必输）
        String tzzhut = xdkh0018DataReqDto.getTzzhut();//投资主体
        String kgtype = xdkh0018DataReqDto.getKgtype();//控股类型
        String qyguim = xdkh0018DataReqDto.getQyguim();//企业规模
        String hyfenl = xdkh0018DataReqDto.getHyfenl();//行业分类
        String cldate = xdkh0018DataReqDto.getCldate();//成立日期
        String xinzqh = xdkh0018DataReqDto.getXinzqh();//注册地行政区划
        String inptid = xdkh0018DataReqDto.getInptid();//登记人
        String mangid = xdkh0018DataReqDto.getMangid();//责任人
        String inbrid = xdkh0018DataReqDto.getInbrid();//登记人机构
        String mabrid = xdkh0018DataReqDto.getMabrid();//责任人机构
        String indate = xdkh0018DataReqDto.getIndate();//登记日期
        String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前系统日期
        S00102ReqDto s00102ReqDto = new S00102ReqDto();//对私客户创建及维护接口
        S00102RespDto s00102RespDto = new S00102RespDto();//对私客户创建及维护接口
        S00101ReqDto s00101ReqDto = new S00101ReqDto();//对私客户综合信息查询
        S00101RespDto s00101RespDto = new S00101RespDto();//对私客户综合信息查询
        G00102ReqDto g00102ReqDto = new G00102ReqDto();//对公客户综合信息维护
        G00102RespDto g00102RespDto = new G00102RespDto();//对公客户综合信息维护
        G00101ReqDto g00101ReqDto = new G00101ReqDto();//对公客户综合信息查询
        G00101RespDto g00101RespDto = new G00101RespDto();//对公客户综合信息查询
        CusBase cusBase = new CusBase();//客户信息主表
        CusIndiv cusIndiv = new CusIndiv();//个人客户基础信息表
        CusCorp cusCorp = new CusCorp();//对公客户信息主表
        try {
            //通过客户号查询客户是否在已在信贷存在
            //证件号码检验
            Optional.ofNullable(xdkh0018DataReqDto.getZjcode()).orElseThrow(() -> new YuspException(EcsEnum.ECS040004.key, EcsEnum.ECS040004.value));
            CusBase cusBaseInfo = cusBaseMapper.queryCusInfoByCertCode(zjcode);
            //客户在信贷存在，直接返回客户号
            if (null != cusBaseInfo) {
                xdkh0018DataRespDto.setCustid(cusBaseInfo.getCusId());// 客户号
            }else {
                //信贷不存在，则发送ecif查询客户信息
                //对公对私类型
                Optional.ofNullable(xdkh0018DataReqDto.getDgdslx()).orElseThrow(() -> new YuspException(EcsEnum.ECS040007.key, EcsEnum.ECS040007.value));
                //对公类型
                if (CmisCommonConstants.DGDSLX_COM.equals(dgdslx)) {
                    //发送ecif G00101查询客户信息
                    g00101ReqDto.setResotp("3");//识别方式 1.按照客户编号查询 2.按照证件号码和证件类型查询 3.按照证件号码、证件类型和客户名称查询
                    g00101ReqDto.setIdtftp(zjtype);//证件类型
                    g00101ReqDto.setIdtfno(zjcode);//证件号码
                    g00101ReqDto.setCustna(kehumc);//客户名称
                    g00101RespDto = g00101Service.g00101(g00101ReqDto);
                    if (null != g00101RespDto && null != g00101RespDto.getCustno() && !"".equals(g00101RespDto.getCustno())){
                        String ecifCusId = s00101RespDto.getCustno();
                        //ecif返回客户号存在，信贷根据该客户号查询信贷系统
                        CusBase cusBaseInfoB = cusBaseMapper.selectByPrimaryKey(ecifCusId);
                        if (null == cusBaseInfoB){
                            //cusbase表数据插入
                            cusBase.setCusId(ecifCusId);//客户号
                            cusBase.setCusName(kehumc);//客户名
                            cusBase.setCertType(zjtype);//证件类型
                            cusBase.setCertCode(zjcode);//证件号码
                            cusBase.setOpenType(kehulx);//开户类型
                            cusBase.setOpenDate(openDay);//开户日期
                            cusBase.setInputId(inptid);//登记人
                            cusBase.setInputBrId(inbrid);//登记机构
                            cusBase.setInputDate(openDay);//登记日期
                            cusBase.setManagerId(mangid);//责任人
                            cusBase.setManagerBrId(mabrid);//责任机构
                            cusBaseMapper.insertSelective(cusBase);

                            //CusCorp 表数据插入
                            cusCorp.setCusId(ecifCusId);//客户号
                            cusCorp.setCusName(kehumc);//客户名
                            cusCorp.setCusType(kehulx);//客户类型
                            cusCorp.setCertCode(zjcode);//证件号码
                            cusCorp.setInputId(inptid);//登记人
                            cusCorp.setInputBrId(inbrid);//登记机构
                            cusCorp.setInputDate(openDay);//登记日期
                            cusCorp.setCountry(contry);//国籍
                            cusCorp.setInvestMbody(tzzhut);//投资主体
                            cusCorp.setHoldType(kgtype);//控股类型
                            cusCorp.setTradeClass(hyfenl);//行业分类
                            cusCorp.setBuildDate(cldate);//成立日期
                            cusCorp.setCorpScale(qyguim);//企业规模
                            cusCorp.setRegiAreaCode(xinzqh);//注册行政区划
                            cusCorp.setInitLoanDate(openDay);//建立信贷关系时间
                            cusCorpMapper.insertSelective(cusCorp);

                            xdkh0018DataRespDto.setCustid(ecifCusId);// 客户号
                        }else {
                            xdkh0018DataRespDto.setCustid(ecifCusId);// 客户号
                        }
                    }else {
                        //则发送ecif开户请求
                        g00102ReqDto.setCustna(kehumc);//客户名称
                        g00102ReqDto.setCusttp(kehulx);//客户类型
                        g00102ReqDto.setCustst("");//客户状态
                        g00102ReqDto.setIdtfno(zjcode);//证件号码
                        g00102ReqDto.setIdtftp(zjtype);//证件类型
                        g00102ReqDto.setBusisp("");//经营范围
                        g00102ReqDto.setBusiad(txdizh);//经营地址
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, JSON.toJSONString(g00102ReqDto));
                        g00102RespDto = g00102Service.g00102(g00102ReqDto);
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, JSON.toJSONString(g00102RespDto));
                        if (null != g00102RespDto && null != g00102RespDto.getCustno() && !"".equals(g00102RespDto.getCustno())){
                            String ecifCusId = g00102RespDto.getCustno();
                            //cusbase表数据插入
                            cusBase.setCusId(ecifCusId);//客户号
                            cusBase.setCusName(kehumc);//客户名
                            cusBase.setCertType(zjtype);//证件类型
                            cusBase.setCertCode(zjcode);//证件号码
                            cusBase.setOpenType(kehulx);//开户类型
                            cusBase.setOpenDate(openDay);//开户日期
                            cusBase.setInputId(inptid);//登记人
                            cusBase.setInputBrId(inbrid);//登记机构
                            cusBase.setInputDate(openDay);//登记日期
                            cusBase.setManagerId(mangid);//责任人
                            cusBase.setManagerBrId(mabrid);//责任机构
                            cusBaseMapper.insert(cusBase);

                            //CusCorp 表数据插入
                            cusCorp.setCusId(ecifCusId);//客户号
                            cusCorp.setCusName(kehumc);//客户名
                            cusCorp.setCusType(kehulx);//客户类型
                            cusCorp.setCertCode(zjcode);//证件号码
                            cusCorp.setInputId(inptid);//登记人
                            cusCorp.setInputBrId(inbrid);//登记机构
                            cusCorp.setInputDate(openDay);//登记日期
                            cusCorp.setCountry(contry);//国籍
                            cusCorp.setInvestMbody(tzzhut);//投资主体
                            cusCorp.setHoldType(kgtype);//控股类型
                            cusCorp.setTradeClass(hyfenl);//行业分类
                            cusCorp.setBuildDate(cldate);//成立日期
                            cusCorp.setCorpScale(qyguim);//企业规模
                            cusCorp.setRegiAreaCode(xinzqh);//注册行政区划
                            cusCorp.setInitLoanDate(openDay);//建立信贷关系时间
                            cusCorpMapper.insert(cusCorp);
                            xdkh0018DataRespDto.setCustid(ecifCusId);// 客户号
                        }
                    }

                } else if (CmisCommonConstants.DGDSLX_INDIV.equals(dgdslx)) {
                    //对私类型
                    //联网核查
                    String flag="";
                    IdchekReqDto idchekReqDto = new IdchekReqDto();
                    idchekReqDto.setSfhm(zjcode);//身份证号码
                    idchekReqDto.setSfmc(kehumc);//姓名
                    idchekReqDto.setYhhh("314305670002");//发起行行号
                    idchekReqDto.setYhhm("张家港农村商业银行");//发起行名称
                    idchekReqDto.setFlcmc(Strings.EMPTY);//分理处名称
                    idchekReqDto.setFlcdz("张家港市人民中路66号");//分理处地址
                    idchekReqDto.setFlcdh("0512-58236366");//分理处电话
                    idchekReqDto.setYwlx("05");//业务类型
                    IdchekRespDto idchekRespDto = idchekService.idchek(idchekReqDto);
                    if("10".equals(zjtype) || "11".equals(zjtype)){
                        if(idchekRespDto!=null){
                            flag = idchekRespDto.getResult();
                        }
                    }else{
                        flag = "00";
                    }
                    if("00".equals(flag)||"01".equals(flag)){
                        //发送ecif S00101查询客户信息
                        s00101ReqDto.setResotp("2");//识别方式
                        s00101ReqDto.setIdtftp(zjtype);//证件类型
                        s00101ReqDto.setIdtfno(zjcode);//证件号码
                        s00101ReqDto.setCustna(kehumc);//客户名称
                        s00101ReqDto.setCustno("");//客户编号
                        s00101ReqDto.setCustst("");//客户状态
                        s00101RespDto = s00101Service.s00101(s00101ReqDto);
                        if (null != s00101RespDto && null != s00101RespDto.getCustno() && !"".equals(s00101RespDto.getCustno())) {
                            String ecifCusId = s00101RespDto.getCustno();
                            //ecif返回客户号存在，信贷根据该客户号查询信贷系统
                            CusBase cusBaseInfoA = cusBaseMapper.selectByPrimaryKey(ecifCusId);
                            //若该客户号客户信息在信贷中不存在，
                            if (null == cusBaseInfoA) {
                                //cusbase表数据插入
                                cusBase.setCusId(ecifCusId);//客户号
                                cusBase.setCusName(kehumc);//客户名
                                cusBase.setCertType(zjtype);//证件类型
                                cusBase.setCertCode(zjcode);//证件号码
                                cusBase.setOpenType(kehulx);//开户类型
                                cusBase.setOpenDate(openDay);//开户日期
                                cusBase.setInputId(inptid);//登记人
                                cusBase.setInputBrId(inbrid);//登记机构
                                cusBase.setInputDate(openDay);//登记日期
                                cusBase.setManagerId(mangid);//责任人
                                cusBase.setManagerBrId(mabrid);//责任机构
                                cusBaseMapper.insert(cusBase);

                                //cusIndiv表数据插入
                                cusIndiv.setCusId(ecifCusId);//客户号
                                cusIndiv.setCusName(kehumc);//客户名称
                                cusIndiv.setNation(contry);//国籍
                                cusIndiv.setSex(xigbie);//性别
                                cusIndiv.setCusType(kehulx);//客户类型
                                cusIndiv.setIndivPolSt(csriqi);//出生日期
                                cusIndiv.setIndivEdt(zuigxl);//学历
                                cusIndiv.setIndivDgr(zuigxw);//学位
                                cusIndiv.setInitLoanDate(openDay);//建立信贷关系时间
                                cusIndiv.setInputId(inptid);//登记人
                                cusIndiv.setInputBrId(inbrid);//登记机构
                                cusIndiv.setInputDate(openDay);//登记日期
                                cusIndivMapper.insert(cusIndiv);

                                xdkh0018DataRespDto.setCustid(ecifCusId);// 客户号

                            }else {
                                //直接返回客户号
                                xdkh0018DataRespDto.setCustid(ecifCusId);// 客户号
                            }
                        }else {
                            //则发送ecif开户请求
                            s00102ReqDto.setCustna(kehumc);//客户名称
                            s00102ReqDto.setIdtftp(zjtype);//证件类型
                            s00102ReqDto.setIdtfno(zjcode);//证件号码
                            s00102ReqDto.setCusstp(kehulx);//客户类型
                            s00102ReqDto.setInefdt(Optional.ofNullable(s00102ReqDto.getInefdt()).orElse(StringUtils.EMPTY));//证件失效日期
                            s00102ReqDto.setCustst(Optional.ofNullable(s00102ReqDto.getCustst()).orElse(StringUtils.EMPTY));//客户状态
                            s00102ReqDto.setPrpsex(Optional.ofNullable(xigbie).orElse(StringUtils.EMPTY));//性别
                            s00102ReqDto.setIndidp(Optional.ofNullable(xigbie).orElse(StringUtils.EMPTY));//学位
                            s00102ReqDto.setProjob(Optional.ofNullable(xdzhiy).orElse(StringUtils.EMPTY));//职业
                            s00102ReqDto.setPoston(Optional.ofNullable(xdduty).orElse(StringUtils.EMPTY));//职务
                            s00102ReqDto.setPosttl(Optional.ofNullable(xdzhic).orElse(StringUtils.EMPTY));//职称
                            s00102ReqDto.setNation(contry);//国籍
                            s00102ReqDto.setSthdfg(whgudg);//是否我行股东
                            s00102ReqDto.setTxpstp(Optional.ofNullable(s00102ReqDto.getTxpstp()).orElse(StringUtils.EMPTY));//税收居民类型
                            s00102ReqDto.setMobitl(Optional.ofNullable(s00102ReqDto.getMobitl()).orElse(StringUtils.EMPTY));//移动电话
                            s00102ReqDto.setOffctl(Optional.ofNullable(zzdiah).orElse(StringUtils.EMPTY));//固定电话
                            s00102ReqDto.setHomead(Optional.ofNullable(txdizh).orElse(StringUtils.EMPTY));//联系地址信息
                            s00102ReqDto.setCtigno(mangid);//客户经理
                            //关联关系类型
                            List<RelArrayInfo> relArrayInfoList = new ArrayList<>();
                            RelArrayInfo relArrayInfo1 = new RelArrayInfo();
                            relArrayInfo1.setEduclv("123");
                            relArrayInfoList.add(relArrayInfo1);
                            RelArrayInfo relArrayInfo2 = new RelArrayInfo();
                            relArrayInfo2.setEmailx("test@email.com");
                            relArrayInfoList.add(relArrayInfo2);
                            //s00102ReqDto.setRelArrayInfo(relArrayInfoList);
                            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value, JSON.toJSONString(s00102ReqDto));
                            s00102RespDto = s00102Service.s00102(s00102ReqDto);
                            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value, JSON.toJSONString(s00102RespDto));
                            if (null != s00102RespDto && null != s00102RespDto.getCustno() && !"".equals(s00102RespDto.getCustno())) {
                                //获取ecif返回的客户号及其他客户信息插入信贷客户主表及对私客户信息表
                                String ecifCusId = s00102RespDto.getCustno();
                                //cusbase表数据插入
                                cusBase.setCusId(ecifCusId);//客户号
                                cusBase.setCusName(kehumc);//客户名
                                cusBase.setCertType(zjtype);//证件类型
                                cusBase.setCertCode(zjcode);//证件号码
                                cusBase.setOpenType(kehulx);//开户类型
                                cusBase.setOpenDate(openDay);//开户日期
                                cusBase.setInputId(inptid);//登记人
                                cusBase.setInputBrId(inbrid);//登记机构
                                cusBase.setInputDate(openDay);//登记日期
                                cusBase.setManagerId(mangid);//责任人
                                cusBase.setManagerBrId(mabrid);//责任机构
                                cusBaseMapper.insert(cusBase);
                                //cusIndiv表数据插入
                                cusIndiv.setCusId(ecifCusId);//客户号
                                cusIndiv.setCusName(kehumc);//客户名称
                                cusIndiv.setNation(contry);//国籍
                                cusIndiv.setSex(xigbie);//性别
                                cusIndiv.setCusType(kehulx);//客户类型
                                cusIndiv.setIndivPolSt(csriqi);//出生日期
                                cusIndiv.setIndivEdt(zuigxl);//学历
                                cusIndiv.setIndivDgr(zuigxw);//学位
                                cusIndiv.setInitLoanDate(openDay);//建立信贷关系时间
                                cusIndiv.setInputId(inptid);//登记人
                                cusIndiv.setInputBrId(inbrid);//登记机构
                                cusIndiv.setInputDate(openDay);//登记日期
                                cusIndivMapper.insert(cusIndiv);

                                xdkh0018DataRespDto.setCustid(ecifCusId);// 客户号
                            }
                        }
                    }
                }
            }
        } catch (YuspException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value, e.getMessage());
            throw new YuspException(e.getCode(), e.getMessage());
        }catch (Exception e){
            throw new Exception(EpbEnum.EPB099999.value);
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value);
        return xdkh0018DataRespDto;
    }
}
