/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndApp
 * @类描述: CUS_LST_YND_APP数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:20:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "CUS_LST_YND_APP")
public class CusLstYndApp extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;

	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 20)
	private String appDate;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
	private String cusId;

	/** 客户姓名 **/
	@Column(name = "CUS_NAME", unique = false, nullable = false, length = 80)
	private String cusName;

	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = false, length = 40)
	private String certCode;

	/** 手机号码 **/
	@Column(name = "MOBILE_NO", unique = false, nullable = false, length = 20)
	private String mobileNo;

	/** 性别 **/
	@Column(name = "SEX", unique = false, nullable = true, length = 5)
	private String sex;

	/** 学历 **/
	@Column(name = "EDU", unique = false, nullable = false, length = 5)
	private String edu;

	/** 有无子女 **/
	@Column(name = "IS_HAVE_CHILDREN", unique = false, nullable = false, length = 5)
	private String isHaveChildren;

	/** 居住场所类型 **/
	@Column(name = "RESI_TYPE", unique = false, nullable = false, length = 80)
	private String resiType;

	/** 家庭地址 **/
	@Column(name = "FAMILY_ADDR", unique = false, nullable = false, length = 250)
	private String familyAddr;

	/** 本地居住年限 **/
	@Column(name = "LOCAL_RESI_LMT", unique = false, nullable = false, length = 40)
	private String localResiLmt;

	/** 本地户口 **/
	@Column(name = "LOCAL_REGIST", unique = false, nullable = false, length = 250)
	private String localRegist;

	/** 经营地址 **/
	@Column(name = "OPER_ADDR", unique = false, nullable = false, length = 250)
	private String operAddr;

	/** 经营年限 **/
	@Column(name = "OPER_LMT", unique = false, nullable = false, length = 40)
	private String operLmt;

	/** 婚姻状况 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = false, length = 5)
	private String marStatus;

	/** 配偶姓名 **/
	@Column(name = "SPOUSE_NAME", unique = false, nullable = false, length = 40)
	private String spouseName;

	/** 配偶身份证号码 **/
	@Column(name = "SPOUSE_IDCARD_NO", unique = false, nullable = false, length = 80)
	private String spouseIdcardNo;

	/** 配偶手机号码 **/
	@Column(name = "SPOUSE_MOBILE_NO", unique = false, nullable = false, length = 20)
	private String spouseMobileNo;

	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = false, length = 5)
	private String approveStatus;

	/** 影像编号 **/
	@Column(name = "IMAGE_NO", unique = false, nullable = false, length = 80)
	private String imageNo;

	/** 经办人 **/
	@Column(name = "HUSER", unique = false, nullable = false, length = 20)
	private String huser;

	/** 经办机构 **/
	@Column(name = "HAND_ORG", unique = false, nullable = false, length = 20)
	private String handOrg;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = false, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = false, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = false, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = false, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = false, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date updateTime;

	/** 推荐机构 **/
	@Column(name = "RECOMMEND_ORG", unique = false, nullable = true, length = 20)
	private String recommendOrg;



	/** 暂存提交标识**/
	@Column(name = "SAVE_FLAG", unique = false, nullable = true, length = 20)
	private String saveFlag;

	public String getSaveFlag() {
		return saveFlag;
	}

	public void setSaveFlag(String saveFlag) {
		this.saveFlag = saveFlag;
	}
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}

	/**
	 * @return appDate
	 */
	public String getAppDate() {
		return this.appDate;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	/**
	 * @return certCode
	 */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return mobileNo
	 */
	public String getMobileNo() {
		return this.mobileNo;
	}

	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}

	/**
	 * @return sex
	 */
	public String getSex() {
		return this.sex;
	}

	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu;
	}

	/**
	 * @return edu
	 */
	public String getEdu() {
		return this.edu;
	}

	/**
	 * @param isHaveChildren
	 */
	public void setIsHaveChildren(String isHaveChildren) {
		this.isHaveChildren = isHaveChildren;
	}

	/**
	 * @return isHaveChildren
	 */
	public String getIsHaveChildren() {
		return this.isHaveChildren;
	}

	/**
	 * @param resiType
	 */
	public void setResiType(String resiType) {
		this.resiType = resiType;
	}

	/**
	 * @return resiType
	 */
	public String getResiType() {
		return this.resiType;
	}

	/**
	 * @param familyAddr
	 */
	public void setFamilyAddr(String familyAddr) {
		this.familyAddr = familyAddr;
	}

	/**
	 * @return familyAddr
	 */
	public String getFamilyAddr() {
		return this.familyAddr;
	}

	/**
	 * @param localResiLmt
	 */
	public void setLocalResiLmt(String localResiLmt) {
		this.localResiLmt = localResiLmt;
	}

	/**
	 * @return localResiLmt
	 */
	public String getLocalResiLmt() {
		return this.localResiLmt;
	}

	/**
	 * @param localRegist
	 */
	public void setLocalRegist(String localRegist) {
		this.localRegist = localRegist;
	}

	/**
	 * @return localRegist
	 */
	public String getLocalRegist() {
		return this.localRegist;
	}

	/**
	 * @param operAddr
	 */
	public void setOperAddr(String operAddr) {
		this.operAddr = operAddr;
	}

	/**
	 * @return operAddr
	 */
	public String getOperAddr() {
		return this.operAddr;
	}

	/**
	 * @param operLmt
	 */
	public void setOperLmt(String operLmt) {
		this.operLmt = operLmt;
	}

	/**
	 * @return operLmt
	 */
	public String getOperLmt() {
		return this.operLmt;
	}

	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}

	/**
	 * @return marStatus
	 */
	public String getMarStatus() {
		return this.marStatus;
	}

	/**
	 * @param spouseName
	 */
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	/**
	 * @return spouseName
	 */
	public String getSpouseName() {
		return this.spouseName;
	}

	/**
	 * @param spouseIdcardNo
	 */
	public void setSpouseIdcardNo(String spouseIdcardNo) {
		this.spouseIdcardNo = spouseIdcardNo;
	}

	/**
	 * @return spouseIdcardNo
	 */
	public String getSpouseIdcardNo() {
		return this.spouseIdcardNo;
	}

	/**
	 * @param spouseMobileNo
	 */
	public void setSpouseMobileNo(String spouseMobileNo) {
		this.spouseMobileNo = spouseMobileNo;
	}

	/**
	 * @return spouseMobileNo
	 */
	public String getSpouseMobileNo() {
		return this.spouseMobileNo;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}

	/**
	 * @return imageNo
	 */
	public String getImageNo() {
		return this.imageNo;
	}

	/**
	 * @param huser
	 */
	public void setHuser(String huser) {
		this.huser = huser;
	}

	/**
	 * @return huser
	 */
	public String getHuser() {
		return this.huser;
	}

	/**
	 * @param handOrg
	 */
	public void setHandOrg(String handOrg) {
		this.handOrg = handOrg;
	}

	/**
	 * @return handOrg
	 */
	public String getHandOrg() {
		return this.handOrg;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param recommendOrg
	 */
	public void setRecommendOrg(String recommendOrg) {
		this.recommendOrg = recommendOrg;
	}

	/**
	 * @return recommendOrg
	 */
	public String getRecommendOrg() {
		return this.recommendOrg;
	}


}
