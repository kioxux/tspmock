package cn.com.yusys.yusp.reportconf.service;

import java.util.List;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.ErrorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.reportconf.domain.FncConfItems;
import cn.com.yusys.yusp.reportconf.repository.mapper.FncConfItemsMapper;
import cn.com.yusys.yusp.commons.module.adapter.exception.Message;
import cn.com.yusys.yusp.commons.mapper.CommonMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.service.CommonService;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfItemsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 10:05:50
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class FncConfItemsService extends CommonService {
	private static final Logger logger = LoggerFactory.getLogger(FncConfItemsService.class);
	@Autowired
	private FncConfItemsMapper fncConfItemsMapper;
//	@Autowired
//	private MessageProviderService messageProviderService;

	@Override
	protected CommonMapper<?> getMapper() {
		return fncConfItemsMapper;
	}

	/**
	 * 科目列表查询展示
	 * 
	 * @param model
	 * @return
	 */
	public List<FncConfItems> queryFncConfItemsList(QueryModel model) {
		PageHelper.startPage(model.getPage(), model.getSize());
		List<FncConfItems> list = fncConfItemsMapper.selectByModel(model);
		PageHelper.clearPage();
		if (null == list) {
//			Message msg = new Message(); messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20203326);
			logger.error(ErrorConstants.NRCS_CMS_T20203326);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20203326, "报表项目配置查询错误");
		}
		return list;
	}

	/**
	 * 科目配置全量查询 或 根据 fnc_conf_typ 字段条件查询
	 * 
	 * @param model
	 * @return
	 */
	public List<FncConfItems> queryFncConfItemsAllListWithFncConfTyp(QueryModel model) {
		List<FncConfItems> list = fncConfItemsMapper.selectFncConfItemsAllListWithFncConfTyp(model);
		if (null == list) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20203326);
			logger.error(ErrorConstants.NRCS_CMS_T20203326);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20203326, "报表项目配置查询错误");
		}
		return list;
	}
    
	/**
	 * 科目配置 添加功能
	 * @param fncConfItems
	 * @return
	 */
	@Transactional
	public int addFncConfItems(FncConfItems fncConfItems) {
		int result = fncConfItemsMapper.insertSelective(fncConfItems);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20203327);
			logger.error(ErrorConstants.NRCS_CMS_T20203327);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20203327, "报表项目配置新增错误");
		}
		return result;
	}
	
	/**
	 *  科目配置 删除功能
	 * @param itemId，主键，科目id
	 * @return int
	 */
	@Transactional
	public int deleteFncConfItems(String itemId) {
		int result = fncConfItemsMapper.deleteByPrimaryKey(itemId);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20203329);
			logger.error(ErrorConstants.NRCS_CMS_T20203329);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20203329, "报表项目配置删除错误");
		}
		return result;
	}
    
	/**
	 * 科目配置修改 功能
	 * @param fncConfItems 实体类，默认按主键update
	 * @return
	 */
	@Transactional
	public int updateFncConfItems(FncConfItems fncConfItems) {
		int result = fncConfItemsMapper.updateByPrimaryKeySelective(fncConfItems);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20203328);
			logger.error(ErrorConstants.NRCS_CMS_T20203328);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20203328, "报表项目配置修改错误");
		}
		return result;
	}

	/**
	 * 科目配置 查看 功能 
	 * @param itemId ，科目id，主键
	 * @return
	 */
	public FncConfItems queryFncConfItemsByKey(String itemId) {
		FncConfItems fncConfItems = fncConfItemsMapper.selectByPrimaryKey(itemId);
		if (null == fncConfItems) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20203330);
			logger.error(ErrorConstants.NRCS_CMS_T20203330);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20203330, "报表项目配置查看错误");
		}
		return fncConfItems;
	}

}
