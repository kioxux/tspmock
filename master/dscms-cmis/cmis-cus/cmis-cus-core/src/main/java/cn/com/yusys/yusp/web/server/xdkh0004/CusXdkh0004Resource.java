package cn.com.yusys.yusp.web.server.xdkh0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0004.req.Xdkh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.resp.Xdkh0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0004.Xdkh0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * 接口处理类:查询客户配偶信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0004:查询客户配偶信息")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0004Resource.class);

    @Autowired
    private Xdkh0004Service xdkh0004Service;

    /**
     * 交易码：xdkh0004
     * 交易描述：查询客户配偶信息
     *
     * @param xdkh0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdkh0004:对公客户基本信息查询")
    @PostMapping("/xdkh0004")
    protected @ResponseBody
    ResultDto<Xdkh0004DataRespDto> xdkh0004(@Validated @RequestBody Xdkh0004DataReqDto xdkh0004DataReqDto) throws Exception {
        Xdkh0004DataRespDto xdkh0004DataRespDto = new Xdkh0004DataRespDto();// 响应Dto:查询客户配偶信息
        ResultDto<Xdkh0004DataRespDto> xdkh0004DataResultDto = new ResultDto<>();
        // 从xdkh0004DataReqDto获取业务值进行业务逻辑处理
        String cusId = xdkh0004DataReqDto.getCusId();//客户代码
        String certType = xdkh0004DataReqDto.getCertType();//借款人证件类型
        String certNo = xdkh0004DataReqDto.getCertNo();//借款人证件号码
        try {
            // 调用xdkh0004Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0004.key, DscmsEnum.TRADE_CODE_XDKH0004.value, certNo);
            xdkh0004DataRespDto = xdkh0004Service.getSpouseBaseData(xdkh0004DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0004.key, DscmsEnum.TRADE_CODE_XDKH0004.value, JSON.toJSONString(xdkh0004DataRespDto));

            // 封装xdkh0004DataResultDto中正确的返回码和返回信息
            xdkh0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            // 封装xdkh0004DataResultDto中异常返回码和返回信息
            xdkh0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0004DataRespDto到xdkh0004DataResultDto中
        xdkh0004DataResultDto.setData(xdkh0004DataRespDto);
        return xdkh0004DataResultDto;
    }
}