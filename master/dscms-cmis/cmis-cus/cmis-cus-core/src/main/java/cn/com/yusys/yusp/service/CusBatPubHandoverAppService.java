/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005Req;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusBatPubHandoverApp;
import cn.com.yusys.yusp.domain.CusBatPubHandoverLst;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.repository.mapper.CusBatPubHandoverAppMapper;
import cn.com.yusys.yusp.repository.mapper.CusBatPubHandoverLstMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBatPubHandoverAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-24 21:53:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusBatPubHandoverAppService {
    private static final String commTypeFilePath = "definition";
    private static final String FILE_PATH_SUFFIX = "客户移交批量导入清单.xlsx";

    private static final Logger log = LoggerFactory.getLogger(CusBatPubHandoverAppService.class);

    @Autowired
    private CusBatPubHandoverAppMapper cusBatPubHandoverAppMapper;

    @Autowired
    private CusBatPubHandoverLstMapper cusBatPubHandoverLstMapper;

    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBatPubHandoverApp selectByPrimaryKey(String cbphaSerno) {
        return cusBatPubHandoverAppMapper.selectByPrimaryKey(cbphaSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusBatPubHandoverApp> selectAll(QueryModel model) {
        List<CusBatPubHandoverApp> records = (List<CusBatPubHandoverApp>) cusBatPubHandoverAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBatPubHandoverApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBatPubHandoverApp> list = cusBatPubHandoverAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusBatPubHandoverApp record) {
        return cusBatPubHandoverAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusBatPubHandoverApp record) {
        return cusBatPubHandoverAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int update(CusBatPubHandoverApp record) {
        // 提交时，调用批量作业更新业务数据
        if("02".equals(record.getBatchHandoStatus())){
            // 查询移交客户数据
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("cbphaSerno", record.getCbphaSerno());
            List<CusBatPubHandoverLst> list = cusBatPubHandoverLstMapper.selectByModel(queryModel);
            Cmisbatch0005ReqDto reqDto = new Cmisbatch0005ReqDto();
            List<Cmisbatch0005Req> cmisbatch0005ReqList = new ArrayList<>();
            if(CollectionUtils.nonEmpty(list)){
                list.stream().forEach(lst -> {
                    Cmisbatch0005Req cmisbatch0005Req = new Cmisbatch0005Req();
                    cmisbatch0005Req.setHandoverType(CmisBizConstants.KHGL06_HANDOVER_MODE);
                    cmisbatch0005Req.setManagerId(lst.getReceiverId());
//                    cmisbatch0005Req.setManagerBrId(record.getInputBrId());
                    cmisbatch0005Req.setConditionValue(lst.getCusId());
                    //判断是否有在途业务
                    ResultDto<Integer>  count = cmisBizClientService.selectXwztCount(lst.getCusId());
                    log.info("批量客户移交申请【{}】，在途业务数【{}】", lst.getCusId(), count.getData().intValue());
                    if(count.getData().intValue() >0){
                        lst.setPrcRst("失败");
                        lst.setRstDec("存在在途业务");
                        // 更新明细表处理情况
                        log.info("批量客户移交申请【{}】，更新明细表成功【{}】",record.getCbphaSerno(), lst.getCusId());
                        cusBatPubHandoverLstMapper.updateByPrimaryKeySelective(lst);
                        return;
                    }else{
                        lst.setPrcRst("成功");
                        //lst.setRstDec();
                        cmisbatch0005ReqList.add(cmisbatch0005Req);
                        // 更新明细表处理情况
                        log.info("批量客户移交申请【{}】，更新明细表成功【{}】",record.getCbphaSerno(), lst.getCusId());
                        cusBatPubHandoverLstMapper.updateByPrimaryKeySelective(lst);
                    }


                });
            }

            reqDto.setCmisbatch0005ReqList(cmisbatch0005ReqList);
            ResultDto<Cmisbatch0005RespDto> result = cmisBatchClientService.cmisbatch0005(reqDto);
            if(!"0".equals(result.getCode())){
                log.error("批量客户移交处理失败，调用批量服务失败", result.getMessage());
                throw BizException.error(null, "99999","批量客户移交处理失败，调用批量服务失败!");
            }
        }
        //批量移交成功后更新主表
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("cbphaSerno", record.getCbphaSerno());
        List<CusBatPubHandoverApp> listCusBatPubHandoverApp =  cusBatPubHandoverAppMapper.selectByModel(queryModel);
        record.setBatchHandoStatus("04"); //批量客户移交主表状态[{"key":"01","value":"暂存"},{"key":"02","value":"已提交待处理"},{"key":"03","value":"系统处理中"},{"key":"04","value":"系统处理完毕"}]
        if(CollectionUtils.isEmpty(listCusBatPubHandoverApp)){
            return cusBatPubHandoverAppMapper.insert(record);
        }
        return cusBatPubHandoverAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusBatPubHandoverApp record) {
        return cusBatPubHandoverAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cbphaSerno) {
        return cusBatPubHandoverAppMapper.deleteByPrimaryKey(cbphaSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusBatPubHandoverAppMapper.deleteByIds(ids);
    }

    /**
     * 下载模板导入清单
     *
     * @return InputStream excel导入模板的文件流
     */
    public InputStream getExcelTemp() throws Exception {
        /* 读取模板及参数详情 */
        ClassPathResource classPathResource = new ClassPathResource(commTypeFilePath + File.separator + FILE_PATH_SUFFIX);
        InputStream in = classPathResource.getInputStream();
        return in;
    }
}
