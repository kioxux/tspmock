package cn.com.yusys.yusp.demo.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Future;

@Service
public class TimeService {
    private static final Logger log = LoggerFactory.getLogger(TimeService.class);

    private String getCurrentTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        LocalDateTime now = LocalDateTime.now();
        return now.format(formatter);

    }

    /**
     * 带返回结果
     * @return
     */
    @Async
    public Future<String> sendMessageWithReturn(){
        String currentTime = getCurrentTime();
        log.info("当前线程:{}, 返回当前时间:{}", Thread.currentThread().getName(), currentTime);
        return AsyncResult.forValue(currentTime);
    }

    /**
     * 不带返回结果
     * @return
     */
    @Async
    public void sendMessageWithoutReturn(){
        log.info("当前线程:{}", Thread.currentThread().getName());
    }
}
