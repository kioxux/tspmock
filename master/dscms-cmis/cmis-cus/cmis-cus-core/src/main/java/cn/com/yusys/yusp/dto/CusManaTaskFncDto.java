package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusManaTaskFnc
 * @类描述: cus_mana_task_fnc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-01 19:33:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusManaTaskFncDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 客户代码 **/
	private String cusId;
	
	/** 报表口径 **/
	private String statStyle;
	
	/** 报表周期类型 **/
	private String statPrdStyle;
	
	/** 报表期间 **/
	private String statPrd;
	
	/** 资产样式编号 **/
	private String statBsStyleId;
	
	/** 损益表编号 **/
	private String statPlStyleId;
	
	/** 现金流量表编号 **/
	private String statCfStyleId;
	
	/** 财务指标表编号 **/
	private String statFiStyleId;
	
	/** 所有者权益变动表编号 **/
	private String statSoeStyleId;
	
	/** 财务简表编号 **/
	private String statSlStyleId;
	
	/** 会计科目余额 **/
	private String statAccStyleId;
	
	/** 经济合作社财务收支明细 **/
	private String statDeStyleId;
	
	/** 保留1 **/
	private String styleId1;
	
	/** 保留2 **/
	private String styleId2;
	
	/** 状态 **/
	private String stateFlg;
	
	/** 是否新报表 **/
	private String statIsNrpt;
	
	/** 是否经过审计 **/
	private String statIsAudit;
	
	/** 审计单位 **/
	private String statAdtEntr;
	
	/** 审计结论 **/
	private String statAdtConc;
	
	/** 是否经过调整 **/
	private String statIsAdjt;
	
	/** 财务报表调整原因 **/
	private String statAdjRsn;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String lastUpdId;
	
	/** 更新日期 **/
	private String lastUpdDate;
	
	/** 是否可修改 **/
	private String indUpdate;
	
	/** 资产样式(新)编号 **/
	private String statSofpStyleId;
	
	/** 利润样式编号 **/
	private String statPsStyleId;
	
	/** 资产负债类财务信息样式标号 **/
	private String statBssStyleId;
	
	/** 利润表类财务信息样式标号 **/
	private String statPssStyleId;
	
	/** 财务分析指标样式标号 **/
	private String statFasStyleId;
	
	/** 是否简表{0:否,1:是} **/
	private String briefFlag;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 调整原因 **/
	private String adjestDesc;
	
	/** 报表录入类型 **/
	private String reportType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param statStyle
	 */
	public void setStatStyle(String statStyle) {
		this.statStyle = statStyle == null ? null : statStyle.trim();
	}
	
    /**
     * @return StatStyle
     */	
	public String getStatStyle() {
		return this.statStyle;
	}
	
	/**
	 * @param statPrdStyle
	 */
	public void setStatPrdStyle(String statPrdStyle) {
		this.statPrdStyle = statPrdStyle == null ? null : statPrdStyle.trim();
	}
	
    /**
     * @return StatPrdStyle
     */	
	public String getStatPrdStyle() {
		return this.statPrdStyle;
	}
	
	/**
	 * @param statPrd
	 */
	public void setStatPrd(String statPrd) {
		this.statPrd = statPrd == null ? null : statPrd.trim();
	}
	
    /**
     * @return StatPrd
     */	
	public String getStatPrd() {
		return this.statPrd;
	}
	
	/**
	 * @param statBsStyleId
	 */
	public void setStatBsStyleId(String statBsStyleId) {
		this.statBsStyleId = statBsStyleId == null ? null : statBsStyleId.trim();
	}
	
    /**
     * @return StatBsStyleId
     */	
	public String getStatBsStyleId() {
		return this.statBsStyleId;
	}
	
	/**
	 * @param statPlStyleId
	 */
	public void setStatPlStyleId(String statPlStyleId) {
		this.statPlStyleId = statPlStyleId == null ? null : statPlStyleId.trim();
	}
	
    /**
     * @return StatPlStyleId
     */	
	public String getStatPlStyleId() {
		return this.statPlStyleId;
	}
	
	/**
	 * @param statCfStyleId
	 */
	public void setStatCfStyleId(String statCfStyleId) {
		this.statCfStyleId = statCfStyleId == null ? null : statCfStyleId.trim();
	}
	
    /**
     * @return StatCfStyleId
     */	
	public String getStatCfStyleId() {
		return this.statCfStyleId;
	}
	
	/**
	 * @param statFiStyleId
	 */
	public void setStatFiStyleId(String statFiStyleId) {
		this.statFiStyleId = statFiStyleId == null ? null : statFiStyleId.trim();
	}
	
    /**
     * @return StatFiStyleId
     */	
	public String getStatFiStyleId() {
		return this.statFiStyleId;
	}
	
	/**
	 * @param statSoeStyleId
	 */
	public void setStatSoeStyleId(String statSoeStyleId) {
		this.statSoeStyleId = statSoeStyleId == null ? null : statSoeStyleId.trim();
	}
	
    /**
     * @return StatSoeStyleId
     */	
	public String getStatSoeStyleId() {
		return this.statSoeStyleId;
	}
	
	/**
	 * @param statSlStyleId
	 */
	public void setStatSlStyleId(String statSlStyleId) {
		this.statSlStyleId = statSlStyleId == null ? null : statSlStyleId.trim();
	}
	
    /**
     * @return StatSlStyleId
     */	
	public String getStatSlStyleId() {
		return this.statSlStyleId;
	}
	
	/**
	 * @param statAccStyleId
	 */
	public void setStatAccStyleId(String statAccStyleId) {
		this.statAccStyleId = statAccStyleId == null ? null : statAccStyleId.trim();
	}
	
    /**
     * @return StatAccStyleId
     */	
	public String getStatAccStyleId() {
		return this.statAccStyleId;
	}
	
	/**
	 * @param statDeStyleId
	 */
	public void setStatDeStyleId(String statDeStyleId) {
		this.statDeStyleId = statDeStyleId == null ? null : statDeStyleId.trim();
	}
	
    /**
     * @return StatDeStyleId
     */	
	public String getStatDeStyleId() {
		return this.statDeStyleId;
	}
	
	/**
	 * @param styleId1
	 */
	public void setStyleId1(String styleId1) {
		this.styleId1 = styleId1 == null ? null : styleId1.trim();
	}
	
    /**
     * @return StyleId1
     */	
	public String getStyleId1() {
		return this.styleId1;
	}
	
	/**
	 * @param styleId2
	 */
	public void setStyleId2(String styleId2) {
		this.styleId2 = styleId2 == null ? null : styleId2.trim();
	}
	
    /**
     * @return StyleId2
     */	
	public String getStyleId2() {
		return this.styleId2;
	}
	
	/**
	 * @param stateFlg
	 */
	public void setStateFlg(String stateFlg) {
		this.stateFlg = stateFlg == null ? null : stateFlg.trim();
	}
	
    /**
     * @return StateFlg
     */	
	public String getStateFlg() {
		return this.stateFlg;
	}
	
	/**
	 * @param statIsNrpt
	 */
	public void setStatIsNrpt(String statIsNrpt) {
		this.statIsNrpt = statIsNrpt == null ? null : statIsNrpt.trim();
	}
	
    /**
     * @return StatIsNrpt
     */	
	public String getStatIsNrpt() {
		return this.statIsNrpt;
	}
	
	/**
	 * @param statIsAudit
	 */
	public void setStatIsAudit(String statIsAudit) {
		this.statIsAudit = statIsAudit == null ? null : statIsAudit.trim();
	}
	
    /**
     * @return StatIsAudit
     */	
	public String getStatIsAudit() {
		return this.statIsAudit;
	}
	
	/**
	 * @param statAdtEntr
	 */
	public void setStatAdtEntr(String statAdtEntr) {
		this.statAdtEntr = statAdtEntr == null ? null : statAdtEntr.trim();
	}
	
    /**
     * @return StatAdtEntr
     */	
	public String getStatAdtEntr() {
		return this.statAdtEntr;
	}
	
	/**
	 * @param statAdtConc
	 */
	public void setStatAdtConc(String statAdtConc) {
		this.statAdtConc = statAdtConc == null ? null : statAdtConc.trim();
	}
	
    /**
     * @return StatAdtConc
     */	
	public String getStatAdtConc() {
		return this.statAdtConc;
	}
	
	/**
	 * @param statIsAdjt
	 */
	public void setStatIsAdjt(String statIsAdjt) {
		this.statIsAdjt = statIsAdjt == null ? null : statIsAdjt.trim();
	}
	
    /**
     * @return StatIsAdjt
     */	
	public String getStatIsAdjt() {
		return this.statIsAdjt;
	}
	
	/**
	 * @param statAdjRsn
	 */
	public void setStatAdjRsn(String statAdjRsn) {
		this.statAdjRsn = statAdjRsn == null ? null : statAdjRsn.trim();
	}
	
    /**
     * @return StatAdjRsn
     */	
	public String getStatAdjRsn() {
		return this.statAdjRsn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param lastUpdId
	 */
	public void setLastUpdId(String lastUpdId) {
		this.lastUpdId = lastUpdId == null ? null : lastUpdId.trim();
	}
	
    /**
     * @return LastUpdId
     */	
	public String getLastUpdId() {
		return this.lastUpdId;
	}
	
	/**
	 * @param lastUpdDate
	 */
	public void setLastUpdDate(String lastUpdDate) {
		this.lastUpdDate = lastUpdDate == null ? null : lastUpdDate.trim();
	}
	
    /**
     * @return LastUpdDate
     */	
	public String getLastUpdDate() {
		return this.lastUpdDate;
	}
	
	/**
	 * @param indUpdate
	 */
	public void setIndUpdate(String indUpdate) {
		this.indUpdate = indUpdate == null ? null : indUpdate.trim();
	}
	
    /**
     * @return IndUpdate
     */	
	public String getIndUpdate() {
		return this.indUpdate;
	}
	
	/**
	 * @param statSofpStyleId
	 */
	public void setStatSofpStyleId(String statSofpStyleId) {
		this.statSofpStyleId = statSofpStyleId == null ? null : statSofpStyleId.trim();
	}
	
    /**
     * @return StatSofpStyleId
     */	
	public String getStatSofpStyleId() {
		return this.statSofpStyleId;
	}
	
	/**
	 * @param statPsStyleId
	 */
	public void setStatPsStyleId(String statPsStyleId) {
		this.statPsStyleId = statPsStyleId == null ? null : statPsStyleId.trim();
	}
	
    /**
     * @return StatPsStyleId
     */	
	public String getStatPsStyleId() {
		return this.statPsStyleId;
	}
	
	/**
	 * @param statBssStyleId
	 */
	public void setStatBssStyleId(String statBssStyleId) {
		this.statBssStyleId = statBssStyleId == null ? null : statBssStyleId.trim();
	}
	
    /**
     * @return StatBssStyleId
     */	
	public String getStatBssStyleId() {
		return this.statBssStyleId;
	}
	
	/**
	 * @param statPssStyleId
	 */
	public void setStatPssStyleId(String statPssStyleId) {
		this.statPssStyleId = statPssStyleId == null ? null : statPssStyleId.trim();
	}
	
    /**
     * @return StatPssStyleId
     */	
	public String getStatPssStyleId() {
		return this.statPssStyleId;
	}
	
	/**
	 * @param statFasStyleId
	 */
	public void setStatFasStyleId(String statFasStyleId) {
		this.statFasStyleId = statFasStyleId == null ? null : statFasStyleId.trim();
	}
	
    /**
     * @return StatFasStyleId
     */	
	public String getStatFasStyleId() {
		return this.statFasStyleId;
	}
	
	/**
	 * @param briefFlag
	 */
	public void setBriefFlag(String briefFlag) {
		this.briefFlag = briefFlag == null ? null : briefFlag.trim();
	}
	
    /**
     * @return BriefFlag
     */	
	public String getBriefFlag() {
		return this.briefFlag;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param adjestDesc
	 */
	public void setAdjestDesc(String adjestDesc) {
		this.adjestDesc = adjestDesc == null ? null : adjestDesc.trim();
	}
	
    /**
     * @return AdjestDesc
     */	
	public String getAdjestDesc() {
		return this.adjestDesc;
	}
	
	/**
	 * @param reportType
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType == null ? null : reportType.trim();
	}
	
    /**
     * @return ReportType
     */	
	public String getReportType() {
		return this.reportType;
	}


}