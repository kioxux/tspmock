package cn.com.yusys.yusp.demo.workflow.biz;

import cn.com.yusys.yusp.flow.client.ClientBizFactory;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BizMessageListener {
    private final Logger log = LoggerFactory.getLogger(BizMessageListener.class);
    
    @Autowired
    private ClientBizFactory clientBizFactory;

    @RabbitListener(queuesToDeclare = {@Queue("yusp-flow.EXAMPLE"), @Queue("yusp-flow.TESTGAOLQ")})// 队列名称为【yusp-flow.流程编号】,可以添加多个
    @RabbitHandler
    public void receiveQueue(ResultInstanceDto message) {
    	log.info("客户端业务监听:"+message);
    	clientBizFactory.processBiz(message);
    }
}