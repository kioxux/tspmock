/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.CusLstYndLinkman;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import cn.com.yusys.yusp.service.CusLstYndJyxxAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndJyxxAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:18:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstyndjyxxapp")
public class CusLstYndJyxxAppResource {
    @Autowired
    private CusLstYndJyxxAppService cusLstYndJyxxAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstYndJyxxApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstYndJyxxApp> list = cusLstYndJyxxAppService.selectAll(queryModel);
        return new ResultDto<List<CusLstYndJyxxApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstYndJyxxApp>> index(QueryModel queryModel) {
        List<CusLstYndJyxxApp> list = cusLstYndJyxxAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYndJyxxApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusLstYndJyxxApp> show(@PathVariable("pkId") String pkId) {
        CusLstYndJyxxApp cusLstYndJyxxApp = cusLstYndJyxxAppService.selectByPrimaryKey(pkId);
        return new ResultDto<CusLstYndJyxxApp>(cusLstYndJyxxApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstYndJyxxApp> create(@RequestBody CusLstYndJyxxApp cusLstYndJyxxApp) throws URISyntaxException {
        cusLstYndJyxxAppService.insert(cusLstYndJyxxApp);
        return new ResultDto<CusLstYndJyxxApp>(cusLstYndJyxxApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstYndJyxxApp cusLstYndJyxxApp) throws URISyntaxException {
        int result = cusLstYndJyxxAppService.update(cusLstYndJyxxApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusLstYndJyxxAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstYndJyxxAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param cusLstYndJyxxApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/7/15 15:22
     * @version 1.0.0
     * @desc    新增经营信息-----移动OA
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addcuslstyndjyxxapp")
    protected ResultDto<Integer> addCusLstYndJyxxApp(@RequestBody CusLstYndJyxxApp cusLstYndJyxxApp) throws URISyntaxException {
        return new ResultDto<Integer>(cusLstYndJyxxAppService.addCusLstYndJyxxApp(cusLstYndJyxxApp));
    }

    /**
     * @param cusLstYndJyxxApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/7/15 15:30
     * @version 1.0.0
     * @desc    删除经营信息-----移动OA
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deletecuslstyndjyxxapp")
    protected ResultDto<Integer> deleteCusLstYndJyxxApp(@RequestBody CusLstYndJyxxApp cusLstYndJyxxApp) {
        int result = cusLstYndJyxxAppService.deleteByPrimaryKey(cusLstYndJyxxApp.getPkId());
        return new ResultDto<Integer>(result);
    }

    /**
     * @param CusLstYndJyxxApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.CusLstYndJyxxApp>>
     * @author hubp
     * @date 2021/7/16 10:48
     * @version 1.0.0
     * @desc    通过流水号查询经营信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<List<CusLstYndJyxxApp>> selectBySerno(@RequestBody CusLstYndJyxxApp CusLstYndJyxxApp) {
        List<CusLstYndJyxxApp> list = cusLstYndJyxxAppService.selectBySerno(CusLstYndJyxxApp.getSerno());
        return new ResultDto<List<CusLstYndJyxxApp>>(list);
    }

    /**
     * @param CusLstYndJyxxApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndJyxxApp>
     * @author hubp
     * @date 2021/7/16 16:09
     * @version 1.0.0
     * @desc    通过主键查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbypkid")
    protected ResultDto<CusLstYndJyxxApp> selectByPkId(@RequestBody CusLstYndJyxxApp CusLstYndJyxxApp) {
        CusLstYndJyxxApp result = cusLstYndJyxxAppService.selectByPrimaryKey(CusLstYndJyxxApp.getPkId());
        return new ResultDto<CusLstYndJyxxApp>(result);
    }
}
