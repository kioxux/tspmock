package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpMemberApp
 * @类描述: cus_grp_member_app数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-08 19:54:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusGrpMemberAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 集团编号 **/
	private String grpNo;
	
	/** 成员客户编号 **/
	private String cusId;
	
	/** 集团关联关系类型 STD_ZB_GRP_CO_TYPE **/
	private String grpCorreType;
	
	/** 集团关联关系描述 **/
	private String grpCorreDetail;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 确认状态 STD_ZB_CONFIRM_STS **/
	private String conStatus;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 主管经理 **/
	private String managerId;
	
	/** 主管部门 **/
	private String managerBrId;
	
	/** 成员客户类型 **/
	private String cusType;
	
	/** 成员客户名称 **/
	private String cusName;
	
	/** 变更类型 **/
	private String modifyType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo == null ? null : grpNo.trim();
	}
	
    /**
     * @return GrpNo
     */	
	public String getGrpNo() {
		return this.grpNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param grpCorreType
	 */
	public void setGrpCorreType(String grpCorreType) {
		this.grpCorreType = grpCorreType == null ? null : grpCorreType.trim();
	}
	
    /**
     * @return GrpCorreType
     */	
	public String getGrpCorreType() {
		return this.grpCorreType;
	}
	
	/**
	 * @param grpCorreDetail
	 */
	public void setGrpCorreDetail(String grpCorreDetail) {
		this.grpCorreDetail = grpCorreDetail == null ? null : grpCorreDetail.trim();
	}
	
    /**
     * @return GrpCorreDetail
     */	
	public String getGrpCorreDetail() {
		return this.grpCorreDetail;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param conStatus
	 */
	public void setConStatus(String conStatus) {
		this.conStatus = conStatus == null ? null : conStatus.trim();
	}
	
    /**
     * @return ConStatus
     */	
	public String getConStatus() {
		return this.conStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param modifyType
	 */
	public void setModifyType(String modifyType) {
		this.modifyType = modifyType == null ? null : modifyType.trim();
	}
	
    /**
     * @return modifyType
     */	
	public String getModifyType() {
		return this.modifyType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}