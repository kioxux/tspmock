package cn.com.yusys.yusp.service.server.xdkh0030;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0030.req.Xdkh0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0030.resp.Xdkh0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusCorpMgrMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;


/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0030Service
 * @类描述: #服务类
 * @功能描述:更新对公客户信息中锁定状态字段并且返回对公客户高管信息
 * @创建人: xll
 * @创建时间: 2021-05-04 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0030Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdkh0030Service.class);

    @Autowired
    private CusCorpMapper cusCorpMapper;
    @Autowired
    private CusCorpMgrMapper cusCorpMgrMapper;

    /**
     * 公司客户评级相关信息同步
     *
     * @param xdkh0030DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0030DataRespDto xdkh0030(Xdkh0030DataReqDto xdkh0030DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value);
        Xdkh0030DataRespDto xdkh0030DataRespDto = new Xdkh0030DataRespDto();

        try {
            String cudId = xdkh0030DataReqDto.getCusId();//客户号
            String lookStatus = xdkh0030DataReqDto.getLockStatus();//锁定状态

            //先更新同业客户信息表
            Map map = new HashMap();
            map.put("cusId", cudId);
            map.put("lookStatus", lookStatus);
            //更改对公客户的锁定状态
            cusCorpMapper.updateLockStatusBycusId(map);

            //查询对公客户客户信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cudId);
            java.util.List<cn.com.yusys.yusp.dto.server.xdkh0030.resp.CusInfoList> lists = cusCorpMapper.selectCusCorpList(map);
            for (cn.com.yusys.yusp.dto.server.xdkh0030.resp.CusInfoList cusInfoList:lists) {
                if(CmisBizConstants.STD_ZB_YES_NO_Y.equals(cusInfoList.getIsSteelTrade())){
                    cusInfoList.setIsSteelTrade("020");
                }else{
                    cusInfoList.setIsSteelTrade("010");
                }
                switch (cusInfoList.getCertType()){
                    case "A" : cusInfoList.setCertType("10");break;
                    case "C" : cusInfoList.setCertType("11");break;
                    case "B" : cusInfoList.setCertType("12");break;
                    case "G" : cusInfoList.setCertType("13");break;
                    case "H" : cusInfoList.setCertType("14");break;
                    case "D" : cusInfoList.setCertType("15");break;
                    case "E" : cusInfoList.setCertType("16");break;
                    case "X" : cusInfoList.setCertType("17");break;
                    case "S" : cusInfoList.setCertType("18");break;
                    case "Y" : cusInfoList.setCertType("19");break;
                    case "Q" : cusInfoList.setCertType("20");break;
                    case "W" : cusInfoList.setCertType("21");break;
                    case "V" : cusInfoList.setCertType("22");break;
                    default: cusInfoList.setCertType("1X");break;
                }
            }
            xdkh0030DataRespDto.setCusInfoList(lists);
            //查询对公高管客户信息
            java.util.List<cn.com.yusys.yusp.dto.server.xdkh0030.resp.MainPersonInfoList> mgrLists = cusCorpMgrMapper.selectCusCorpMgrList(map);
            for (cn.com.yusys.yusp.dto.server.xdkh0030.resp.MainPersonInfoList mainPersonInfoList: mgrLists) {
                mainPersonInfoList.getMrgEdt();
                switch (mainPersonInfoList.getMrgEdt()){
                    case "10" : mainPersonInfoList.setMrgEdt("00");break;
                    case "20" : mainPersonInfoList.setMrgEdt("10");break;
                    case "30" : mainPersonInfoList.setMrgEdt("20");break;
                    case "40" : mainPersonInfoList.setMrgEdt("30");break;
                    case "50" : mainPersonInfoList.setMrgEdt("40");break;
                    case "60" : mainPersonInfoList.setMrgEdt("50");break;
                    case "70" : mainPersonInfoList.setMrgEdt("60");break;
                    case "80" : mainPersonInfoList.setMrgEdt("70");break;
                    case "90" : mainPersonInfoList.setMrgEdt("80");break;
                    case "A0" : mainPersonInfoList.setMrgEdt("90");break;
                    default: mainPersonInfoList.setMrgEdt("99");
                }
            }
            xdkh0030DataRespDto.setMainPersonInfoList(mgrLists);

        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value);
        return xdkh0030DataRespDto;
    }
}
