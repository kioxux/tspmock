package cn.com.yusys.yusp.service.server.cmiscus0023;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0023.req.CmisCus0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0023.resp.CmisCus0023RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusIntbankService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 业务逻辑处理类：根据大额行号查询客户号
 *
 * @author zhangjw
 * @version 1.0
 * @since 20210727
 */
@Service
public class CmisCus0023Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0023Service.class);

    @Autowired
    private CusIntbankService cusIntbankService;

    @Transactional
    public CmisCus0023RespDto execute(CmisCus0023ReqDto reqDto) throws YuspException {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0023.key, DscmsEnum.TRADE_CODE_CMISCUS0023.value);

        CmisCus0023RespDto respDto = new CmisCus0023RespDto();
        //行号
        String bankNo = reqDto.getBankNo();

        try{
            String cusId = cusIntbankService.selectCusIdByBankNo(bankNo);

            respDto.setCusId(cusId);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsEnum.TRADE_CODE_CMISCUS0023.key+"报错：",e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0023.key, DscmsEnum.TRADE_CODE_CMISCUS0023.value);
        return respDto;
    }
}