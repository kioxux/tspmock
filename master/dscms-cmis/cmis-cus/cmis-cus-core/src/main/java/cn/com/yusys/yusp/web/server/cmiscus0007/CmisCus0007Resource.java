package cn.com.yusys.yusp.web.server.cmiscus0007;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.server.cmiscus0007.req.CmisCus0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto;
import cn.com.yusys.yusp.enums.cache.CacheKeyEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusIndivService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 接口处理类: 查询个人客户基本信息
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CmisCus0007:查询个人客户基本信息")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0007Resource.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CusIndivService cusIndivService;
    @Autowired
    private YuspRedisTemplate yuspRedisTemplate;


    /**
     * 交易码：cmiscus0007
     * 交易描述：查询个人客户基本信息
     *
     * @param cmisCus0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询个人客户基本信息")
    @PostMapping("/cmiscus0007")
    protected @ResponseBody
    ResultDto<CmisCus0007RespDto> cmiscus0007(@Validated @RequestBody CmisCus0007ReqDto cmisCus0007ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0007ReqDto));
        CmisCus0007RespDto cmisCus0007RespDto = new CmisCus0007RespDto();// 响应Dto:查询个人客户基本信息
        ResultDto<CmisCus0007RespDto> cmisCus0007ResultDto = new ResultDto<>();
        try {
            // 使用RedisHelper的keys方法代替stringRedisTemplate的keys方法扫描Key
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId", cmisCus0007ReqDto.getCusId());
            queryModel.addCondition("certCode", cmisCus0007ReqDto.getCertCode());
            List<CusIndivDto> cusIndivList = cusIndivService.getCusIndivDtoList(queryModel);
            cmisCus0007RespDto.setCusIndivList(cusIndivList);
            // 封装cmisCus0007ResultDto中正确的返回码和返回信息
            cmisCus0007ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0007ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, e.getMessage());
            // 封装cmisCus0007ResultDto中异常返回码和返回信息
            cmisCus0007ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0007ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0007RespDto到cmisCus0007ResultDto中
        cmisCus0007ResultDto.setData(cmisCus0007RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0007ResultDto));
        return cmisCus0007ResultDto;
    }
}
