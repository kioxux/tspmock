package cn.com.yusys.yusp.service.server.xdkh0015;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.GetUserInfoByRoleCodeDto;
import cn.com.yusys.yusp.dto.server.xdkh0015.req.Xdkh0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0015.resp.Xdkh0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmRoleService;
import cn.com.yusys.yusp.service.CmisBizClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.*;

@Service
public class Xdkh0015Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0015Service.class);

    @Autowired
    private AdminSmRoleService adminSmRoleService;
    @Resource
    private CusLstYndMapper cusLstYndMapper;
    @Resource
    private CusLstYndAppMapper cusLstYndAppMapper;
    @Resource
    private CusLstYndCmdcAppMapper cusLstYndCmdcAppMapper;
    @Resource
    private CusLstYndDcjlAppMapper cusLstYndDcjlAppMapper;
    @Resource
    private CusLstYndJyxxAppMapper cusLstYndJyxxAppMapper;
    @Resource
    private CusLstYndLinkmanMapper cusLstYndLinkmanMapper;
    @Resource
    private CmisBizClientService cmisBizClientService;
    @Resource
    private SequenceTemplateClient sequenceTemplateClient;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CusBaseMapper cusBaseMapper;

    @Transactional(rollbackFor = Exception.class)
    public Xdkh0015DataRespDto xdkh0015(Xdkh0015DataReqDto xdkh0015DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value);
        Xdkh0015DataRespDto xdkh0015DataRespDto = new Xdkh0015DataRespDto();
        // 从xdkh0015DataReqDto获取业务值进行业务逻辑处理
        String cusName = xdkh0015DataReqDto.getCusName();//客户姓名
        String certNo = xdkh0015DataReqDto.getCertNo();//身份证号
        String actOperAddr = xdkh0015DataReqDto.getActOperAddr();//经营地址
        String mobile = xdkh0015DataReqDto.getMobile();//手机号码
        String sex = xdkh0015DataReqDto.getSex();//性别
        String edu = xdkh0015DataReqDto.getIndivEdt();//学历
        String IsHavingChild = xdkh0015DataReqDto.getIsHavingChild();//是否
        String IndivMarSt = xdkh0015DataReqDto.getIndivMarSt();//婚姻状态
        String OperYear = xdkh0015DataReqDto.getOperYear();//经营年限
        String SpouseName = xdkh0015DataReqDto.getSpouseName();//配偶姓名
        String SpouseCertNo = xdkh0015DataReqDto.getSpouseCertNo();//配偶证件
        String SpouseMobile = xdkh0015DataReqDto.getSpouseMobile();//配偶手机
        String supTradeCode = xdkh0015DataReqDto.getSupTradeCode();//一级行业代码
        String supTradeName = xdkh0015DataReqDto.getSupTradeName();//一级行业名称
        String detailTradeCode = xdkh0015DataReqDto.getDetailTradeCode();//细分行业代码
        String detailTradeName = xdkh0015DataReqDto.getDetailTradeName();//细分行业名称
        String curtBreedingScale = xdkh0015DataReqDto.getCurtBreedingScale();//当前种养殖规模
        String saleAmtYear = xdkh0015DataReqDto.getSaleAmtYear();//年销售收入
        String profitYear = xdkh0015DataReqDto.getProfitYear();//年利润
        String recomOrgName = xdkh0015DataReqDto.getRecomOrgName();//推荐机构名称
        String urgentLinkmanName = xdkh0015DataReqDto.getUrgentLinkmanName();//紧急联系人姓名
        String urgentLinkmanPhone = xdkh0015DataReqDto.getUrgentLinkmanPhone();//紧急联系人电话
        String sideIndgtChnl = xdkh0015DataReqDto.getSideIndgtChnl();//侧面调查渠道
        String sideIndgtPhone = xdkh0015DataReqDto.getSideIndgtPhone();//侧面调查电话
        String sideIndgtMemo = xdkh0015DataReqDto.getSideIndgtMemo();//侧面调查备注
        String isAgri = xdkh0015DataReqDto.getIsAgri();//是否农户
        String isOperNormal = xdkh0015DataReqDto.getIsOperNormal();//是否经营正常
        String coopYearLmt = xdkh0015DataReqDto.getCoopYearLmt();//合作年限
        String tranAmtYear = xdkh0015DataReqDto.getTranAmtYear();//年交易流水
        String chnlSuggestLmt = xdkh0015DataReqDto.getChnlSuggestLmt();//渠道建议额度
        String cusCdtEvlu = xdkh0015DataReqDto.getCusCdtEvlu();//客户信用评价
        String recomTime = xdkh0015DataReqDto.getRecomTime();//推荐时间
        String recomMemo = xdkh0015DataReqDto.getRecomMemo();//推荐备注


        boolean pass = true;
        String returnMessage = "系统检测到";
        if (StringUtils.isEmpty(cusName)) {
            pass = false;
            returnMessage += "客户姓名为空!";
        } else if (StringUtils.isEmpty(certNo)) {
            pass = false;
            returnMessage += "身份证号为空!";
        } else if (StringUtils.isEmpty(actOperAddr)) {
            pass = false;
            returnMessage += "经营地址为空!";
        } else if (StringUtils.isEmpty(mobile)) {
            pass = false;
            returnMessage += "手机号码为空!";
        } else if (StringUtils.isEmpty(urgentLinkmanName)) {
            pass = false;
            returnMessage += "紧急联系人姓名为空!";
        } else if (StringUtils.isEmpty(sideIndgtChnl)) {
            pass = false;
            returnMessage += "侧面调查渠道为空!";
        } else if (StringUtils.isEmpty(sideIndgtPhone)) {
            pass = false;
            returnMessage += "侧面调查电话为空!";
        } else if (StringUtils.isEmpty(sideIndgtMemo)) {
            pass = false;
            returnMessage += "侧面调查备注为空!";
        } else if (StringUtils.isEmpty(isAgri)) {
            pass = false;
            returnMessage += "是否农户为空!";
        } else if (StringUtils.isEmpty(isOperNormal)) {
            pass = false;
            returnMessage += "是否经营正常为空!";
        }
        // 1.必输项校验
        if (pass) {
            try {
                //获取营业日期
                String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
                // 2.如果客户存在于新信贷【优农贷名单信息】中，且名单状态LIST_STATUS=否决，字典STD_ZB_STATUS，则返回提示“该客户名单已否决！
                QueryModel model = new QueryModel();
                model.addCondition("certNo", certNo);
                model.addCondition("listStatus", "0");//名单状态-0	失效；1	生效；2	暂存
                List<CusLstYnd> list = cusLstYndMapper.selectByModel(model);
                if (list.size() > 0) {
                    xdkh0015DataRespDto.setOpFlag(CmisCommonConstants.OP_FLAG_F);// 操作成功标志位
                    xdkh0015DataRespDto.setOpMsg("该客户名单已否决!");// 描述信息
                    xdkh0015DataRespDto.setSerno(StringUtils.EMPTY);// 流水号
                    return xdkh0015DataRespDto;
                }
                // 3.查询新【新信贷调查主表】LMT_SURVEY_REPORT_BASIC_INFO.APPROVE_STATUS，字典STD_ZB_APPR_STATUS，如果该客户有审批中的记录，则返回提示“该客户已在审批中！”。
                Integer count = cmisBizClientService.queryLmtSurveyReportByCusNo(certNo);
                if (count > 0) {
                    xdkh0015DataRespDto.setOpFlag(CmisCommonConstants.OP_FLAG_F);// 操作成功标志位
                    xdkh0015DataRespDto.setOpMsg("该客户已在审批中!");// 描述信息
                    xdkh0015DataRespDto.setSerno(StringUtils.EMPTY);// 流水号
                    return xdkh0015DataRespDto;
                }
                //  4.根据角色1128取中台审核人员，选取规则为当天，当前业务类型数量最少的人，相同则随机
                GetUserInfoByRoleCodeDto userInfoByRoleCodeDto = new GetUserInfoByRoleCodeDto();
                userInfoByRoleCodeDto.setRoleCode("1128");
                userInfoByRoleCodeDto.setPageNum(1);
                userInfoByRoleCodeDto.setPageSize(100);
                List<AdminSmUserDto> adminSmUserDtos = adminSmRoleService.getuserlist(userInfoByRoleCodeDto).getData();
                //(List<AdminSmUserDto>) cusCommonService.getByLoginCode("1128");
                String ids = "";
                if (adminSmUserDtos != null && adminSmUserDtos.size() > 0) {
                    AdminSmUserDto adminSmUserDto = null;
                    for (int i = 0; i < adminSmUserDtos.size(); i++) {
                        adminSmUserDto = adminSmUserDtos.get(i);
                        if (i != adminSmUserDtos.size()) {
                            ids += adminSmUserDto.getLoginCode() + ",";
                        } else {
                            ids += adminSmUserDto.getLoginCode();
                        }
                    }
                }
                //******
                List loginIdList = cusLstYndMapper.selectYndLstByUserList(ids, openDay);
                int k = 1;
                for (int i = 0; i < loginIdList.size() - 1; i++) {
                    Map loMap = (Map) loginIdList.get(i);
                    Map lpMap = (Map) loginIdList.get(i + 1);
                    if (new BigDecimal(loMap.get("cou").toString()).compareTo(new BigDecimal(lpMap.get("cou").toString())) > 0) {
                        break;
                    } else {
                        k = i;
                    }
                }
                Random r = SecureRandom.getInstanceStrong();
                Map inpMap = (Map) loginIdList.get(r.nextInt(k));
                // 生成优农贷业务流水
                String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.IQP_SERNO, new HashMap<>());
                //查询客户信息
                CusBase cusBase = cusBaseMapper.queryCusInfoByCertCode(xdkh0015DataReqDto.getCertNo());
                // 5.插入优农贷名单表：cus_lst_ynd
                CusLstYnd cusLstYnd = new CusLstYnd();
                //优农贷名单信息 cus_lst_ynd
                BeanUtils.copyProperties(xdkh0015DataReqDto, cusLstYnd);
                cusLstYnd.setSerno(serno);
                cusLstYnd.setCusId(cusBase.getCusId());
                cusLstYnd.setIdcardNo(certNo);//证件号
                cusLstYnd.setMobileNo(mobile);//手机
                cusLstYnd.setEdu(edu);//学历
                cusLstYnd.setIsHaveChildren(IsHavingChild);//是否有子女
                cusLstYnd.setMarStatus(IndivMarSt);//婚姻状态
                cusLstYnd.setInputId(inpMap.get("loginCode").toString());
                cusLstYnd.setManagerId(inpMap.get("loginCode").toString());
                cusLstYnd.setBelgOrg(cusBase.getManagerBrId());//所属机构
                cusLstYnd.setListStatus("1");//名单状态-0	失效；1	生效；2	暂存
                cusLstYnd.setStorageDate(openDay);//入库日期
                cusLstYnd.setOperAddr(actOperAddr);//经营地址
                cusLstYnd.setOperLmt(OperYear);//经营年限
                cusLstYnd.setRecommendOrg(cusBase.getManagerBrId());//推荐构机
                cusLstYnd.setResiType("");//居住场所类型
                cusLstYnd.setFamilyAddr("");//家庭地址
                cusLstYnd.setSpouseName(SpouseName);//配偶姓名
                cusLstYnd.setSpouseIdcardNo(SpouseCertNo);//配偶身份证号码
                cusLstYnd.setSpouseMobileNo(SpouseMobile);//配偶手机号码
                cusLstYnd.setInputDate(openDay);
                logger.info("**********优农贷名单信息cus_lst_ynd新增记录开始,插入参数为:{}", JSON.toJSONString(cusLstYnd));
                cusLstYndMapper.insert(cusLstYnd);
                /*****************优农贷名单申请基本信息 cus_lst_ynd_app****************/
                CusLstYndApp cusLstYndApp = new CusLstYndApp();
                BeanUtils.copyProperties(xdkh0015DataReqDto, cusLstYndApp);
                cusLstYndApp.setSerno(serno);
                cusLstYndApp.setAppDate(openDay);//申请日期
                cusLstYndApp.setCusId(cusBase.getCusId());//客户编号
                cusLstYndApp.setMobileNo(mobile);//手机
                cusLstYndApp.setCertCode(certNo);
                cusLstYndApp.setEdu(edu);//学历
                cusLstYndApp.setIsHaveChildren(IsHavingChild);//是否有子女
                cusLstYndApp.setMarStatus(IndivMarSt);//婚姻状态
                cusLstYndApp.setInputId(inpMap.get("loginCode").toString());
                cusLstYndApp.setOperAddr(actOperAddr);//经营地址
                cusLstYndApp.setOperLmt(OperYear);//经营年限
                cusLstYndApp.setRecommendOrg("");//推荐机构
                cusLstYndApp.setResiType("");//居住场所类型
                cusLstYndApp.setFamilyAddr("");//家庭地址
                cusLstYndApp.setSpouseName(SpouseName);//配偶姓名
                cusLstYndApp.setSpouseIdcardNo(SpouseCertNo);//配偶身份证号码
                cusLstYndApp.setSpouseMobileNo(SpouseMobile);//配偶手机号码
                cusLstYndApp.setSaveFlag("");//提交标识
                cusLstYndApp.setInputId(inpMap.get("loginCode").toString());
                cusLstYndApp.setInputDate(openDay);
                logger.info("**********优农贷名单申请基本信息CUS_LST_YND_APP新增记录开始,插入参数为:{}", JSON.toJSONString(cusLstYndApp));
                cusLstYndAppMapper.insert(cusLstYndApp);
                /*********************优农贷名单申请侧面调查 cus_lst_ynd_cmdc_app********************/
                CusLstYndCmdcApp cusLstYndCmdcApp = new CusLstYndCmdcApp();
                BeanUtils.copyProperties(xdkh0015DataReqDto, cusLstYndCmdcApp);
                cusLstYndCmdcApp.setSerno(serno);
                cusLstYndCmdcApp.setChnl(sideIndgtChnl);
                cusLstYndCmdcApp.setPhone(sideIndgtPhone);
                cusLstYndCmdcApp.setMemo(sideIndgtMemo);
                cusLstYndCmdcApp.setInputId(inpMap.get("loginCode").toString());
                cusLstYndCmdcApp.setInputDate(openDay);
                cusLstYndCmdcApp.setOprType("01");//操作类型
                logger.info("**********优农贷名单申请侧面调查cus_lst_ynd_cmdc_app新增记录开始,插入参数为:{}", JSON.toJSONString(cusLstYndCmdcApp));
                cusLstYndCmdcAppMapper.insert(cusLstYndCmdcApp);
                /**************************优农贷名单申请调查结论 cus_lst_ynd_dcjl_app*******************************/
                CusLstYndDcjlApp cusLstYndDcjlApp = new CusLstYndDcjlApp();
                BeanUtils.copyProperties(xdkh0015DataReqDto, cusLstYndDcjlApp);
                cusLstYndDcjlApp.setSerno(serno);
                cusLstYndDcjlApp.setAgriFlag(isAgri);
                cusLstYndDcjlApp.setCusOperIsNormal(isOperNormal);
                cusLstYndDcjlApp.setInputId(inpMap.get("loginCode").toString());
                cusLstYndDcjlApp.setInputDate(openDay);
                cusLstYndDcjlApp.setOprType("01");//操作类型
                logger.info("**********优农贷名单申请调查结论cus_lst_ynd_dcjl_app新增记录开始,插入参数为:{}", JSON.toJSONString(cusLstYndDcjlApp));
                cusLstYndDcjlAppMapper.insert(cusLstYndDcjlApp);
                /***************************优农贷名单申请经营信息 cus_lst_ynd_jyxx_app*******************************/
                CusLstYndJyxxApp cusLstYndJyxxApp = new CusLstYndJyxxApp();
                BeanUtils.copyProperties(xdkh0015DataReqDto, cusLstYndJyxxApp);
                cusLstYndJyxxApp.setSerno(serno);
                cusLstYndJyxxApp.setOneLevelTradeCode(supTradeCode);
                cusLstYndJyxxApp.setOneLevelTradeName(supTradeName);
                cusLstYndJyxxApp.setDetailsTradeCode(detailTradeCode);
                cusLstYndJyxxApp.setDetailsTradeName(detailTradeName);
                cusLstYndJyxxApp.setOperScale(curtBreedingScale);
                cusLstYndJyxxApp.setYearSaleIncome(new BigDecimal("".equals(saleAmtYear) ? "0" : saleAmtYear));
                cusLstYndJyxxApp.setYearProfit(new BigDecimal("".equals(profitYear) ? "0" : profitYear));
                cusLstYndJyxxApp.setInputId(inpMap.get("loginCode").toString());
                cusLstYndJyxxApp.setInputDate(openDay);
                cusLstYndJyxxApp.setOprType("01");//操作类型
                logger.info("**********优农贷名单申请经营信息cus_lst_ynd_jyxx_app新增记录开始,插入参数为:{}", JSON.toJSONString(cusLstYndJyxxApp));
                cusLstYndJyxxAppMapper.insert(cusLstYndJyxxApp);
                //优农贷紧急联系人信息 cus_lst_ynd_linkman
                CusLstYndLinkman cusLstYndLinkman = new CusLstYndLinkman();
                BeanUtils.copyProperties(xdkh0015DataReqDto, cusLstYndLinkman);
                cusLstYndLinkman.setSerno(serno);
                cusLstYndLinkman.setEmgLinkmanName(urgentLinkmanName);
                cusLstYndLinkman.setEmgLinkmanPhone(urgentLinkmanPhone);
                cusLstYndLinkman.setInputId(inpMap.get("loginCode").toString());
                cusLstYndLinkman.setInputDate(openDay);
                cusLstYndLinkman.setOprType("01");//操作类型
                logger.info("**********优农贷紧急联系人信息cus_lst_ynd_linkman新增记录开始,插入参数为:{}", JSON.toJSONString(cusLstYndLinkman));
                cusLstYndLinkmanMapper.insert(cusLstYndLinkman);
                //  封装xdkh0015DataRespDto对象开始
                xdkh0015DataRespDto.setOpFlag(CmisCommonConstants.OP_FLAG_S);// 操作成功标志位
                xdkh0015DataRespDto.setOpMsg(CmisCommonConstants.OP_MSG_S);// 描述信息
                xdkh0015DataRespDto.setSerno(serno);// 流水号
                //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            } catch (Exception e) {
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, e.getMessage());
                // 封装xdkh0015DataResultDto中异常返回码和返回信息
                xdkh0015DataRespDto.setOpFlag(CmisCommonConstants.OP_FLAG_F);// 操作成功标志位
                xdkh0015DataRespDto.setOpMsg(e.getMessage());// 描述信息
                xdkh0015DataRespDto.setSerno(StringUtils.EMPTY);// 流水号
                throw e;
            }
        } else {
            xdkh0015DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
            xdkh0015DataRespDto.setOpMsg(returnMessage);
            xdkh0015DataRespDto.setSerno(StringUtils.EMPTY);// 流水号
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, JSON.toJSONString(xdkh0015DataRespDto));
        return xdkh0015DataRespDto;
    }
}
