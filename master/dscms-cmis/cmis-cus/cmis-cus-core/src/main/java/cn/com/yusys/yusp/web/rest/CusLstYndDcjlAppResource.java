/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYndDcjlApp;
import cn.com.yusys.yusp.service.CusLstYndDcjlAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndDcjlAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:17:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstynddcjlapp")
public class CusLstYndDcjlAppResource {
    @Autowired
    private CusLstYndDcjlAppService cusLstYndDcjlAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstYndDcjlApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstYndDcjlApp> list = cusLstYndDcjlAppService.selectAll(queryModel);
        return new ResultDto<List<CusLstYndDcjlApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstYndDcjlApp>> index(QueryModel queryModel) {
        List<CusLstYndDcjlApp> list = cusLstYndDcjlAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYndDcjlApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstYndDcjlApp> show(@PathVariable("serno") String serno) {
        CusLstYndDcjlApp cusLstYndDcjlApp = cusLstYndDcjlAppService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstYndDcjlApp>(cusLstYndDcjlApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstYndDcjlApp> create(@RequestBody CusLstYndDcjlApp cusLstYndDcjlApp) throws URISyntaxException {
        CusLstYndDcjlApp cusLstYndDcjl = cusLstYndDcjlAppService.selectByPrimaryKey(cusLstYndDcjlApp.getSerno());
        if(cusLstYndDcjl!=null){
            cusLstYndDcjlAppService.updateSelective(cusLstYndDcjlApp);
        }else{
            cusLstYndDcjlAppService.insert(cusLstYndDcjlApp);
        }
        return new ResultDto<CusLstYndDcjlApp>(cusLstYndDcjlApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstYndDcjlApp cusLstYndDcjlApp) throws URISyntaxException {
        int result = cusLstYndDcjlAppService.update(cusLstYndDcjlApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstYndDcjlAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstYndDcjlAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param cusLstYndDcjlApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndDcjlApp>
     * @author hubp
     * @date 2021/7/16 11:04
     * @version 1.0.0
     * @desc    通过流水号查询调查结论
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<CusLstYndDcjlApp> selectBySerno(@RequestBody CusLstYndDcjlApp cusLstYndDcjlApp) {
        CusLstYndDcjlApp result = cusLstYndDcjlAppService.selectByPrimaryKey(cusLstYndDcjlApp.getSerno());
        return new ResultDto<CusLstYndDcjlApp>(result);
    }

    /**
     * @param cusLstYndDcjlApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndDcjlApp>
     * @author hubp
     * @date 2021/7/20 10:25
     * @version 1.0.0
     * @desc    调查结论新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addcuslstynddcjlapp")
    protected ResultDto<CusLstYndDcjlApp> addCusLstYndDcjlApp(@RequestBody CusLstYndDcjlApp cusLstYndDcjlApp) {
        return cusLstYndDcjlAppService.addCusLstYndDcjlApp(cusLstYndDcjlApp);
    }
}
