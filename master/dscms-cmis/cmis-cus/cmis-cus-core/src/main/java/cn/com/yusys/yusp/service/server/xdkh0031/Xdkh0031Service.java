package cn.com.yusys.yusp.service.server.xdkh0031;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusIntbank;
import cn.com.yusys.yusp.dto.server.xdkh0031.req.Xdkh0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0031.resp.Xdkh0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0031Service
 * @类描述: #服务类
 * @功能描述:更新金融同业客户表锁定状态且返回同业客户信息
 * @创建人: xll
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0031Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0031Service.class);

    @Resource
    private CusIntbankMapper cusIntbankMapper;

    /**
     * 同业客户评级相关信息同步
     *
     * @param xdkh0031DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0031DataRespDto xdkh0031(Xdkh0031DataReqDto xdkh0031DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031DataReqDto));
        Xdkh0031DataRespDto xdkh0031DataRespDto = new Xdkh0031DataRespDto();
        try {
            String cudId = xdkh0031DataReqDto.getCusId();//客户号
            String lookStatus = xdkh0031DataReqDto.getLockStatus();//锁定状态

            //先更新同业客户信息表
            Map map = new HashMap();
            map.put("cusId", cudId);
            map.put("lookStatus", lookStatus);
            //更新锁定状态
            cusIntbankMapper.updateLockStatusBycusId(map);

            //查询同业客户信息
            CusIntbank cusIntbank = cusIntbankMapper.selectByPrimaryKey(cudId);
            //返回报文
            xdkh0031DataRespDto.setCusIdCore(cusIntbank.getCusId());// 核心客户号
            xdkh0031DataRespDto.setSameOrgNo(cusIntbank.getCusEnName());// 同业机构（行）号
            xdkh0031DataRespDto.setSameOrgCnname(cusIntbank.getCusName());// 同业机构（行）号名称
            xdkh0031DataRespDto.setSameOrgType(cusIntbank.getIntbankType());// 同业机构（行）类型
            xdkh0031DataRespDto.setRegStateCode(cusIntbank.getRegiAreaCode());// 注册地行政区划代码
            xdkh0031DataRespDto.setManagerId(cusIntbank.getManagerId());// 主管客户经理编号
            xdkh0031DataRespDto.setManagerorgId(cusIntbank.getManagerBrId());// 主管客户经理所属机构编号
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031DataRespDto));
        return xdkh0031DataRespDto;
    }
}
