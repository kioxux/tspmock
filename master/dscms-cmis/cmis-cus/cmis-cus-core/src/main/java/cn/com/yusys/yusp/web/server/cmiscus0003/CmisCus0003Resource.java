package cn.com.yusys.yusp.web.server.cmiscus0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0003.req.CmisCus0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0003.resp.CmisCus0003RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscus0003.CmisCus0003Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 法人关联客户列表查询
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0003:查询法人关联客户列表")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0003Resource.class);

    @Autowired
    private CmisCus0003Service cmisCus0003Service;

    /**
     * 交易码：cmiscus0003
     * 交易描述：法人关联客户列表查询
     *
     * @param cmisCus0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("法人关联客户列表查询")
    @PostMapping("/cmiscus0003")
    protected @ResponseBody
    ResultDto<CmisCus0003RespDto> cmiscus0003(@Validated @RequestBody CmisCus0003ReqDto cmisCus0003ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0003.key, DscmsEnum.TRADE_CODE_CMISCUS0003.value, JSON.toJSONString(cmisCus0003ReqDto));
        CmisCus0003RespDto cmisCus0003RespDto = new CmisCus0003RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0003RespDto> cmisCus0003ResultDto = new ResultDto<>();
        try {
            // 封装cmisCus0003ResultDto中正确的返回码和返回信息
            cmisCus0003RespDto = cmisCus0003Service.execute(cmisCus0003ReqDto);
            cmisCus0003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0003ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0003.key, DscmsEnum.TRADE_CODE_CMISCUS0003.value, e.getMessage());
            // 封装cmisCus0003ResultDto中异常返回码和返回信息
            cmisCus0003ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0003ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0003RespDto到cmisCus0003ResultDto中
        cmisCus0003ResultDto.setData(cmisCus0003RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0003.key, DscmsEnum.TRADE_CODE_CMISCUS0003.value, JSON.toJSONString(cmisCus0003ResultDto));
        return cmisCus0003ResultDto;
    }
}
