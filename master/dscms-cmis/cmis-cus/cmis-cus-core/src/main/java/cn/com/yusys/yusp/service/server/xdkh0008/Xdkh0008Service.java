package cn.com.yusys.yusp.service.server.xdkh0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.dto.server.xdkh0008.resp.GroupMapList;
import cn.com.yusys.yusp.dto.server.xdkh0008.resp.Xdkh0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusGrpMapper;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0008Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-28 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0008Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0008Service.class);
    @Resource
    private CusGrpMapper cusGrpMapper;
    @Resource
    private CusGrpMemberRelMapper cusGrpMemberRelMapper;

    /**
     * 集团关联信息查询
     *
     * @param cusId
     * @return
     */
    @Transactional
    public Xdkh0008DataRespDto getXdht0008(String cusId) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, "请求参数cusId:" + cusId);
        Xdkh0008DataRespDto xdkh0008DataRespDto = new Xdkh0008DataRespDto();
        try {
            //根据客户号查询集团编号和集团名称
            CusGrpMemberRel cusGrpMemberRel = cusGrpMemberRelMapper.getGruopInfoByCusId(cusId);
            if (cusGrpMemberRel != null) {
                logger.info("集团成员编号[{}]", cusGrpMemberRel.getGrpNo());
                xdkh0008DataRespDto.setGrpNo(cusGrpMemberRel.getGrpNo());
                xdkh0008DataRespDto.setGrpName(cusGrpMemberRel.getGrpName());
                //根据集团编号查询成员集团信息
                List<GroupMapList> list = cusGrpMemberRelMapper.getGroupList(cusGrpMemberRel.getGrpNo());
                xdkh0008DataRespDto.setGroupMapList(list);
            } else {
                throw BizException.error(null, EcsEnum.ECS040006.key, EcsEnum.ECS040006.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, xdkh0008DataRespDto);
        return xdkh0008DataRespDto;
    }
}
