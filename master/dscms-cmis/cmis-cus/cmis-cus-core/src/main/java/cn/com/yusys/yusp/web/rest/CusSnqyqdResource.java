/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusSnqyqd;
import cn.com.yusys.yusp.service.CusSnqyqdService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusSnqyqdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-21 23:55:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cussnqyqd")
public class CusSnqyqdResource {
    @Autowired
    private CusSnqyqdService cusSnqyqdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusSnqyqd>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusSnqyqd> list = cusSnqyqdService.selectAll(queryModel);
        return new ResultDto<List<CusSnqyqd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusSnqyqd>> index(QueryModel queryModel) {
        List<CusSnqyqd> list = cusSnqyqdService.selectByModel(queryModel);
        return new ResultDto<List<CusSnqyqd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{areaCode}")
    protected ResultDto<CusSnqyqd> show(@PathVariable("areaCode") String areaCode) {
        CusSnqyqd cusSnqyqd = cusSnqyqdService.selectByPrimaryKey(areaCode);
        return new ResultDto<CusSnqyqd>(cusSnqyqd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusSnqyqd> create(@RequestBody CusSnqyqd cusSnqyqd) throws URISyntaxException {
        cusSnqyqdService.insert(cusSnqyqd);
        return new ResultDto<CusSnqyqd>(cusSnqyqd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusSnqyqd cusSnqyqd) throws URISyntaxException {
        int result = cusSnqyqdService.update(cusSnqyqd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{areaCode}")
    protected ResultDto<Integer> delete(@PathVariable("areaCode") String areaCode) {
        int result = cusSnqyqdService.deleteByPrimaryKey(areaCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusSnqyqdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectByPrimaryKey
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbyprimarykey")
    protected ResultDto<CusSnqyqd> selectByPrimaryKey(@RequestBody CusSnqyqd cusSnqyqd) {
        return  new ResultDto<CusSnqyqd>(cusSnqyqdService.selectByPrimaryKey(cusSnqyqd.getAreaCode()));
    }
}
