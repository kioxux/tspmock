/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CusLstGlf;
import cn.com.yusys.yusp.dto.CusGrpMemberRelDto;
import cn.com.yusys.yusp.service.CusLstGlfService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.service.CusGrpMemberRelService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpMemberRelResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-10 10:46:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusgrpmemberrel")
public class CusGrpMemberRelResource {
    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;
    @Autowired
    private CusLstGlfService cusLstGlfService;
	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusGrpMemberRel>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusGrpMemberRel> list = cusGrpMemberRelService.selectAll(queryModel);
        return new ResultDto<List<CusGrpMemberRel>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusGrpMemberRel>> index(@RequestBody QueryModel queryModel) {
        List<CusGrpMemberRel> list = cusGrpMemberRelService.selectByModel(queryModel);
        return new ResultDto<List<CusGrpMemberRel>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusGrpMemberRel>> indexQuery(@RequestBody QueryModel queryModel) {
        List<CusGrpMemberRel> list = cusGrpMemberRelService.selectByModel(queryModel);
        return new ResultDto<List<CusGrpMemberRel>>(list);
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusGrpMemberRel> show(@PathVariable("pkId") String pkId) {
        CusGrpMemberRel cusGrpMemberRel = cusGrpMemberRelService.selectByPrimaryKey(pkId);
        return new ResultDto<CusGrpMemberRel>(cusGrpMemberRel);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusGrpMemberRel> create(@RequestBody CusGrpMemberRel cusGrpMemberRel) throws URISyntaxException {
        cusGrpMemberRelService.insert(cusGrpMemberRel);
        return new ResultDto<CusGrpMemberRel>(cusGrpMemberRel);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusGrpMemberRel cusGrpMemberRel) throws URISyntaxException {
        int result = cusGrpMemberRelService.update(cusGrpMemberRel);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusGrpMemberRelService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusGrpMemberRelService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: queryCusGrpMemberRelDtoList
     * @方法描述: 根据集团客户编号查询成员列表信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-15 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团客户编号查询成员列表信息")
    @PostMapping("/querycusgrpmemberreldtolist")
    protected ResultDto<List<CusGrpMemberRelDto>> queryCusGrpMemberRelDtoList(@RequestBody CusGrpMemberRelDto cusGrpMemberRelDto) {
        return cusGrpMemberRelService.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto);
    }

    /**
     * @方法名称: queryGrpNoByParams
     * @方法描述: 根据成员客户编号查询集团客户
     * @参数与返回说明:
     * @算法描述:
     * @创建人: qw
     * @创建时间: 2021-07-23 20:11:11
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据成员客户编号查询集团客户")
    @PostMapping("/querygrpnobyparams")
    protected ResultDto<CusGrpMemberRel> queryGrpNoByParams(@RequestBody Map map) {
        map.put("availableInd", CmisCommonConstants.STD_ZB_YES_NO_1);
        return ResultDto.success(cusGrpMemberRelService.queryGrpNoByParams(map));
    }

    /**
     * @方法名称: queryDataAndGrp
     * @方法描述: 根据成员客户编号查询是否集团客户以及是否合作方
     * @参数与返回说明:
     * @算法描述:
     * @创建人: ljs
     * @创建时间: 20210830
     * @修改记录: 修改时间    修改人员    修改原因
     */

    @PostMapping("/queryDataAndGrp")
    protected ResultDto<Map<String,Object>> queryDataAndGrp(@RequestBody QueryModel queryModel){
        Map<String,Object> map = new HashMap();
        queryModel.addCondition("availableInd",CmisCommonConstants.STD_ZB_YES_NO_1);
        List<CusGrpMemberRel> list = cusGrpMemberRelService.selectByModel(queryModel);
        if(list.size()==0) {
            map.put("isGrpCus",CmisCommonConstants.STD_ZB_YES_NO_0);
        }else{
            map.put("isGrpCus",CmisCommonConstants.STD_ZB_YES_NO_1);
        }
        queryModel.addCondition("status",CmisCommonConstants.STD_ZB_YES_NO_1);
        List<CusLstGlf> list2 = cusLstGlfService.selectByModel(queryModel);
        if(list2.size()==0) {
            map.put("isBankCorre",CmisCommonConstants.STD_ZB_YES_NO_0);
        }else{
            map.put("isBankCorre",CmisCommonConstants.STD_ZB_YES_NO_1);
        }
        return new ResultDto<Map<String,Object>>(map);
    }

}
