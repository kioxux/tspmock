package cn.com.yusys.yusp.service.server.xdkh0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004.Wxd004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004.Wxd004RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0020.req.Xdkh0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0020.resp.Xdkh0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Objects;

/**
 * <br>
 * 0.2ZRC:2021/6/5 19:51:<br>
 * desc: 东方微银信贷户转正交易
 *
 * @author 王玉坤
 * @version 0.1
 * @date 2021/06/05 19:51
 * @since 2021/06/05 19:51
 */
@Service
public class Xdkh0020Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0020Service.class);
    @Resource
    private CusIndivService cusIndivService;
    @Resource
    private CusBaseService cusBaseService;
    @Resource
    private CusIndivContactService cusIndivContactService;
    @Resource
    private CusIndivUnitService cusIndivUnitService;

    @Transactional
    public Xdkh0020DataRespDto xdkh0020(Xdkh0020DataReqDto xdkh0020DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0020.key, DscmsEnum.TRADE_CODE_XDKH0020.value);
        Xdkh0020DataRespDto xdkh0020DataRespDto = new Xdkh0020DataRespDto();
        String cusId = xdkh0020DataReqDto.getCusId();//客户号
        String cusName = xdkh0020DataReqDto.getCusName();//客户名称
        String certType = xdkh0020DataReqDto.getCertType();//证件类型
        String certCode = xdkh0020DataReqDto.getCertCode();//身份证号码
        String managerBrId = xdkh0020DataReqDto.getManagerBrId();//机构号
        String managerId = xdkh0020DataReqDto.getManagerId();//当前客户经理
        String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);//当前日期

        boolean pass = true;
        String returnMessage = "系统检测到";
        if (StringUtils.isEmpty(cusId)) {
            pass = false;
            returnMessage += "客户号为空!";
        } else if (StringUtils.isEmpty(cusName)) {
            pass = false;
            returnMessage += "客户姓名为空!";
        } else if (StringUtils.isEmpty(certType)) {
            pass = false;
            returnMessage += "证件类型为空!";
        } else if (StringUtils.isEmpty(certCode)) {
            pass = false;
            returnMessage += "证件号码为空!";
        } else if (StringUtils.isEmpty(managerBrId)) {
            pass = false;
            returnMessage += "机构号为空!";
        } else if (StringUtils.isEmpty(managerId)) {
            pass = false;
            returnMessage += "所属客户经理为空!";
        }
        //必填项检验通过
        if (pass) {
            try {
                // 1、根据客户号查询客户基本信息
                logger.info("根据客户编号【{}】, 查询客户个人信息开始", cusId);
                CusIndiv cusIndiv = cusIndivService.selectByPrimaryKey(cusId);
                logger.info("根据客户编号【{}】, 查询客户个人信息结束", cusId);
                if (Objects.isNull(cusIndiv)) {
                    throw BizException.error(null, null, "抱歉，未查询到客户个人信息，无法转正！");
                }

                logger.info("根据客户编号【{}】, 查询客户基本信息开始", cusId);
                CusBase cusBase = cusBaseService.selectByPrimaryKey(cusId);
                logger.info("根据客户编号【{}】, 查询客户基本信息结束", cusId);
                if (Objects.isNull(cusBase)) {
                    throw BizException.error(null, null, "抱歉，未查询到客户基础信息，无法转正！");
                }

                // 2、判断证件号码与客户已存在信息是否匹配
                if (!Objects.equals(certCode, cusBase.getCertCode())) {
                    throw BizException.error(null, null, "抱歉，证件号码与客户号不匹配，无法转正！");
                }

                // 3、更新客户信息
                // 更新Cus_Indiv
                cusIndiv.setSex(xdkh0020DataReqDto.getSexcode());
                cusIndiv.setIndivDtOfBirth(xdkh0020DataReqDto.getBorndate());
                cusIndiv.setMarStatus(xdkh0020DataReqDto.getIndivMarSt());
                cusIndiv.setIndivEdt(xdkh0020DataReqDto.getIndivEdt());
                cusIndiv.setAgriFlg(xdkh0020DataReqDto.getAgriFlg());
                cusIndiv.setNation("CHN");
                cusIndiv.setCusType("150");
                cusIndivService.update(cusIndiv);

                // 更新客户状态为正式户 2--生效
                cusBase.setCusState("2");
                // 01-正式客户 02-临时客户
                cusBase.setCusRankCls("01");
                cusBase.setManagerId(managerId);
                cusBase.setManagerBrId(managerBrId);
                cusBaseService.update(cusBase);

                // 更新 cus_indiv_unit
                CusIndivUnit cusIndivUnit = cusIndivUnitService.selectByCusId(cusId);
                if (null != cusIndivUnit) {
                    cusIndivUnit.setOcc(xdkh0020DataReqDto.getIndivOcc());
                    cusIndivUnit.setUnitName(xdkh0020DataReqDto.getIndivComName());
                    cusIndivUnit.setJobTtl(xdkh0020DataReqDto.getIndivComJobTtl());
                    cusIndivUnitService.update(cusIndivUnit);
                } else {
                    cusIndivUnit = new CusIndivUnit();
                    cusIndivUnit.setOcc(xdkh0020DataReqDto.getIndivOcc());
                    cusIndivUnit.setUnitName(xdkh0020DataReqDto.getIndivComName());
                    cusIndivUnit.setJobTtl(xdkh0020DataReqDto.getIndivComJobTtl());
                    cusIndivUnit.setCusId(cusId);
                    cusIndivUnitService.insertSelective(cusIndivUnit);
                }

                // 更新CUS_INDIV_CONTACT
                CusIndivContact cusIndivContact = cusIndivContactService.selectByPrimaryKey(cusId);
                if (null != cusIndivContact) {
                    cusIndivContact.setSendAddr(xdkh0020DataReqDto.getPostAddr());
                    cusIndivContact.setPostcode(xdkh0020DataReqDto.getPostCode());
                    cusIndivContact.setIndivRsdAddr(xdkh0020DataReqDto.getIndivRsdAddr());
                    cusIndivContact.setIndivZipCode(xdkh0020DataReqDto.getIndivZipCode());
                    cusIndivContact.setIndivRsdSt(xdkh0020DataReqDto.getIndivRsdSt());
                    cusIndivContact.setRegionalism(xdkh0020DataReqDto.getAreaCode());
                    cusIndivContact.setIsAgri(xdkh0020DataReqDto.getAgriFlg());
                    cusIndivContactService.update(cusIndivContact);
                } else {
                    cusIndivContact = new CusIndivContact();
                    cusIndivContact.setCusId(cusId);
                    cusIndivContact.setSendAddr(xdkh0020DataReqDto.getPostAddr());
                    cusIndivContact.setPostcode(xdkh0020DataReqDto.getPostCode());
                    cusIndivContact.setIndivRsdAddr(xdkh0020DataReqDto.getIndivRsdAddr());
                    cusIndivContact.setIndivZipCode(xdkh0020DataReqDto.getIndivZipCode());
                    cusIndivContact.setIndivRsdSt(xdkh0020DataReqDto.getIndivRsdSt());
                    cusIndivContact.setRegionalism(xdkh0020DataReqDto.getAreaCode());
                    cusIndivContact.setIsAgri(xdkh0020DataReqDto.getAgriFlg());
                    cusIndivContactService.insertSelective(cusIndivContact);
                }
            } catch (BizException e) {
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0020.key, DscmsEnum.TRADE_CODE_XDKH0020.value, e.getMessage());
                throw BizException.error(null, e.getErrorCode(), e.getMessage());
            } catch (Exception e) {
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0020.key, DscmsEnum.TRADE_CODE_XDKH0020.value, e.getMessage());
            }
        } else {
            throw BizException.error(null, null, returnMessage);
        }
        xdkh0020DataRespDto.setCusId(cusId);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0020.key, DscmsEnum.TRADE_CODE_XDKH0020.value);
        return xdkh0020DataRespDto;
    }
}
