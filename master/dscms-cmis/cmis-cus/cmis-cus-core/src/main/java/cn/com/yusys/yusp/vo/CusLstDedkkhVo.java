/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 名单管理导入模板
 */
@ExcelCsv(namePrefix = "大额贷款客户名单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CusLstDedkkhVo {

    /**
     * 客户编号
     **/
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /**
     * 客户名称
     **/
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;

    /**
     * 管户客户经理
     **/
    @ExcelField(title = "管户客户经理", viewLength = 20)
    private String managerId;

    /**
     * 所属机构
     **/
    @ExcelField(title = "所属机构", viewLength = 20)
    private String belgOrg;

    /**
     * 本年度计划总压降金额（万元）
     **/
    @ExcelField(title = "本年度计划总压降金额（万元）", viewLength = 30)
    private String planAmt;

    /**
     * 压降年度
     **/
    @ExcelField(title = "压降年度", viewLength = 20)
    private String pressureDropYearly;

    /**
     * 风险缓释措施
     **/
    @ExcelField(title = "风险缓释措施", viewLength = 20)
    private String riskRelieveStep;

    /**
     * 其他说明
     **/
    @ExcelField(title = "其他说明", viewLength = 30)
    private String otherMemo;

    /**
     * 登记人
     **/
    @ExcelField(title = "登记人", viewLength = 20)
    private String inputId;
    /**
     * 压降事项
     **/
    @ExcelField(title = "压降事项", viewLength = 500)
    private String pressureDropItem;
    /**
     * 压降金额(万元)
     **/
    @ExcelField(title = "压降金额(万元)", viewLength = 16)
    private String pressureDropAmt;
    /**
     * 压降要求完成时间
     **/
    @ExcelField(title = "压降要求完成时间", viewLength = 20)
    private String pressureDropFinishTime;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getBelgOrg() {
        return belgOrg;
    }

    public void setBelgOrg(String belgOrg) {
        this.belgOrg = belgOrg;
    }

    public String getPlanAmt() {
        return planAmt;
    }

    public void setPlanAmt(String planAmt) {
        this.planAmt = planAmt;
    }

    public String getPressureDropYearly() {
        return pressureDropYearly;
    }

    public void setPressureDropYearly(String pressureDropYearly) {
        this.pressureDropYearly = pressureDropYearly;
    }

    public String getRiskRelieveStep() {
        return riskRelieveStep;
    }

    public void setRiskRelieveStep(String riskRelieveStep) {
        this.riskRelieveStep = riskRelieveStep;
    }

    public String getOtherMemo() {
        return otherMemo;
    }

    public void setOtherMemo(String otherMemo) {
        this.otherMemo = otherMemo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getPressureDropItem() {
        return pressureDropItem;
    }

    public void setPressureDropItem(String pressureDropItem) {
        this.pressureDropItem = pressureDropItem;
    }

    public String getPressureDropAmt() {
        return pressureDropAmt;
    }

    public void setPressureDropAmt(String pressureDropAmt) {
        this.pressureDropAmt = pressureDropAmt;
    }

    public String getPressureDropFinishTime() {
        return pressureDropFinishTime;
    }

    public void setPressureDropFinishTime(String pressureDropFinishTime) {
        this.pressureDropFinishTime = pressureDropFinishTime;
    }
}