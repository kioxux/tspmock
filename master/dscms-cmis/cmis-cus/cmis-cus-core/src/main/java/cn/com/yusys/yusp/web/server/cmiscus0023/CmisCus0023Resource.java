package cn.com.yusys.yusp.web.server.cmiscus0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0023.req.CmisCus0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0023.resp.CmisCus0023RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.cmiscus0023.CmisCus0023Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 根据大额行号查询客户号
 *
 * @author zhangjw
 * @version 1.0
 */
@Api(tags = "CmisCus0023:根据大额行号查询客户号")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0023Resource.class);

    @Autowired
    private CmisCus0023Service cmisCus0023Service;

    /**
     * 交易码：cmiscus0023
     * 交易描述：根据大额行号查询客户号
     *
     * @param cmisCus0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据大额行号查询客户号")
    @PostMapping("/cmiscus0023")
    protected @ResponseBody
    ResultDto<CmisCus0023RespDto> cmiscus0023(@Validated @RequestBody CmisCus0023ReqDto cmisCus0023ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0023.key, DscmsEnum.TRADE_CODE_CMISCUS0023.value, JSON.toJSONString(cmisCus0023ReqDto));
        ResultDto<CmisCus0023RespDto> cmisCus0023ResultDto = new ResultDto<>();
        CmisCus0023RespDto respDto = new CmisCus0023RespDto();
        try {
            respDto = cmisCus0023Service.execute(cmisCus0023ReqDto);

        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0023.key, DscmsEnum.TRADE_CODE_CMISCUS0023.value, e.getMessage());
            // 封装cmisCus0023ResultDto中异常返回码和返回信息
            cmisCus0023ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0023ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0023RespDto到cmisCus0023ResultDto中
        cmisCus0023ResultDto.setData(respDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0023.key, DscmsEnum.TRADE_CODE_CMISCUS0023.value, JSON.toJSONString(cmisCus0023ResultDto));
        return cmisCus0023ResultDto;
    }
}
