/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.CusLstWhbxdDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstWhbxd;
import cn.com.yusys.yusp.service.CusLstWhbxdService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWhbxdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: pl
 * @创建时间: 2021-04-07 11:15:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstwhbxd")
public class CusLstWhbxdResource {
    @Autowired
    private CusLstWhbxdService cusLstWhbxdService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstWhbxd>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstWhbxd> list = cusLstWhbxdService.selectAll(queryModel);
        return new ResultDto<List<CusLstWhbxd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstWhbxd>> index(QueryModel queryModel) {
        List<CusLstWhbxd> list = cusLstWhbxdService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWhbxd>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param
     *
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstWhbxd>> query(@RequestBody QueryModel queryModel) {
        List<CusLstWhbxd> list = cusLstWhbxdService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWhbxd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cusId}")
    protected ResultDto<CusLstWhbxd> show(@PathVariable("cusId") String cusId) {
        CusLstWhbxd cusLstWhbxd = cusLstWhbxdService.selectByPrimaryKey(cusId);
        return new ResultDto<CusLstWhbxd>(cusLstWhbxd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstWhbxd> create(@RequestBody CusLstWhbxd cusLstWhbxd) throws URISyntaxException {
        cusLstWhbxdService.insert(cusLstWhbxd);
        return new ResultDto<CusLstWhbxd>(cusLstWhbxd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstWhbxd cusLstWhbxd) throws URISyntaxException {
        int result = cusLstWhbxdService.update(cusLstWhbxd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cusId}")
    protected ResultDto<Integer> delete(@PathVariable("cusId") String cusId) {
        int result = cusLstWhbxdService.deleteByPrimaryKey(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstWhbxdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected cn.com.yusys.yusp.commons.data.model.ResultDto<Integer> updateSelective(@RequestBody CusLstWhbxd cusLstWhbxd) {
        int result = cusLstWhbxdService.updateSelective(cusLstWhbxd);
        return cn.com.yusys.yusp.commons.data.model.ResultDto.success(result);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addCusLstWhbxd")
    protected ResultDto<Integer> addCusLstWhbxd(@RequestBody CusLstWhbxdDto cusLstWhbxdDto) {
        return new ResultDto<Integer>(cusLstWhbxdService.addCusLstWhbxd(cusLstWhbxdDto));
    }

}
