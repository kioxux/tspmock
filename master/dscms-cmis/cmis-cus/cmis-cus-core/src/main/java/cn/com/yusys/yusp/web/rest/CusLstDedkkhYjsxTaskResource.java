/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CusLstDedkkhYjsxTaskDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstDedkkhYjsxTask;
import cn.com.yusys.yusp.service.CusLstDedkkhYjsxTaskService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhYjsxTaskResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstdedkkhyjsxtask")
public class CusLstDedkkhYjsxTaskResource {
    @Autowired
    private CusLstDedkkhYjsxTaskService cusLstDedkkhYjsxTaskService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstDedkkhYjsxTask>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstDedkkhYjsxTask> list = cusLstDedkkhYjsxTaskService.selectAll(queryModel);
        return new ResultDto<List<CusLstDedkkhYjsxTask>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstDedkkhYjsxTask>> index(QueryModel queryModel) {
        List<CusLstDedkkhYjsxTask> list = cusLstDedkkhYjsxTaskService.selectByModel(queryModel);
        return new ResultDto<List<CusLstDedkkhYjsxTask>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<CusLstDedkkhYjsxTask>> queryList(@RequestBody QueryModel queryModel) {
        List<CusLstDedkkhYjsxTask> list = cusLstDedkkhYjsxTaskService.selectByModel(queryModel);
        return new ResultDto<List<CusLstDedkkhYjsxTask>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{taskNo}")
    protected ResultDto<CusLstDedkkhYjsxTask> show(@PathVariable("taskNo") String taskNo) {
        CusLstDedkkhYjsxTask cusLstDedkkhYjsxTask = cusLstDedkkhYjsxTaskService.selectByPrimaryKey(taskNo);
        return new ResultDto<CusLstDedkkhYjsxTask>(cusLstDedkkhYjsxTask);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstDedkkhYjsxTask> create(@RequestBody CusLstDedkkhYjsxTask cusLstDedkkhYjsxTask) throws URISyntaxException {
        cusLstDedkkhYjsxTaskService.insert(cusLstDedkkhYjsxTask);
        return new ResultDto<CusLstDedkkhYjsxTask>(cusLstDedkkhYjsxTask);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstDedkkhYjsxTask cusLstDedkkhYjsxTask) throws URISyntaxException {
        int result = cusLstDedkkhYjsxTaskService.update(cusLstDedkkhYjsxTask);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{taskNo}")
    protected ResultDto<Integer> delete(@PathVariable("taskNo") String taskNo) {
        int result = cusLstDedkkhYjsxTaskService.deleteByPrimaryKey(taskNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstDedkkhYjsxTaskService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据客户号查找出该客户存在到期未完成的压降任务
     * @author zhangliang15
     * @date 2021/9/28 0:05
     **/
    @PostMapping("/selectcusLstDedkkhYjsxTaskDataByParams")
    protected ResultDto<List<CusLstDedkkhYjsxTaskDto>> selectcusLstDedkkhYjsxTaskDataByParams(@RequestBody QueryModel queryModel){
        List<CusLstDedkkhYjsxTaskDto> result = cusLstDedkkhYjsxTaskService.selectcusLstDedkkhYjsxTaskDataByParams(queryModel);
        return new ResultDto<>(result);
    }
}
