/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: OcrTask
 * @类描述: ocr_task数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 19:39:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "ocr_task")
public class OcrTask extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
	private String cusId;
	
	/** 报表期间 **/
	@Column(name = "STAT_PRD", unique = false, nullable = false, length = 6)
	private String statPrd;
	
	/** 任务编号 **/
	@Column(name = "TASK_ID", unique = false, nullable = false, length = 40)
	private String taskId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param statPrd
	 */
	public void setStatPrd(String statPrd) {
		this.statPrd = statPrd;
	}
	
    /**
     * @return statPrd
     */
	public String getStatPrd() {
		return this.statPrd;
	}
	
	/**
	 * @param taskId
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
    /**
     * @return taskId
     */
	public String getTaskId() {
		return this.taskId;
	}


}