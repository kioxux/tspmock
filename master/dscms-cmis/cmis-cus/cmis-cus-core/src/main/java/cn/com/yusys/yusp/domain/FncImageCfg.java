/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncImageCfg
 * @类描述: fnc_image_cfg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 19:39:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "fnc_image_cfg")
public class FncImageCfg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** serno **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 根节点(1级目录编号) **/
	@Column(name = "root_node", unique = false, nullable = true, length = 10)
	private String rootNode;
	
	/** 子节点(3级目录编号资产负债表) **/
	@Column(name = "child_bs_node", unique = false, nullable = true, length = 20)
	private String childBsNode;
	
	/** 子节点(3级目录编号利润表[损益表]) **/
	@Column(name = "child_is_node", unique = false, nullable = true, length = 20)
	private String childIsNode;
	
	/** 应用年份 **/
	@Column(name = "apply_year", unique = false, nullable = true, length = 10)
	private String applyYear;
	
	/** 应用月份 **/
	@Column(name = "apply_month", unique = false, nullable = true, length = 10)
	private String applyMonth;
	
	/** 客户类型 **/
	@Column(name = "cus_type", unique = false, nullable = true, length = 6)
	private String cusType;
	
	/** 客户大类 1 对私 2 对工 **/
	@Column(name = "CUS_CATALOG", unique = false, nullable = true, length = 1)
	private String cusCatalog;
	
	/** 报表录入类型 **/
	@Column(name = "report_type", unique = false, nullable = true, length = 4)
	private String reportType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param rootNode
	 */
	public void setRootNode(String rootNode) {
		this.rootNode = rootNode;
	}
	
    /**
     * @return rootNode
     */
	public String getRootNode() {
		return this.rootNode;
	}
	
	/**
	 * @param childBsNode
	 */
	public void setChildBsNode(String childBsNode) {
		this.childBsNode = childBsNode;
	}
	
    /**
     * @return childBsNode
     */
	public String getChildBsNode() {
		return this.childBsNode;
	}
	
	/**
	 * @param childIsNode
	 */
	public void setChildIsNode(String childIsNode) {
		this.childIsNode = childIsNode;
	}
	
    /**
     * @return childIsNode
     */
	public String getChildIsNode() {
		return this.childIsNode;
	}
	
	/**
	 * @param applyYear
	 */
	public void setApplyYear(String applyYear) {
		this.applyYear = applyYear;
	}
	
    /**
     * @return applyYear
     */
	public String getApplyYear() {
		return this.applyYear;
	}
	
	/**
	 * @param applyMonth
	 */
	public void setApplyMonth(String applyMonth) {
		this.applyMonth = applyMonth;
	}
	
    /**
     * @return applyMonth
     */
	public String getApplyMonth() {
		return this.applyMonth;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}
	
    /**
     * @return cusCatalog
     */
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param reportType
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	
    /**
     * @return reportType
     */
	public String getReportType() {
		return this.reportType;
	}


}