/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusLstZlkh;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusLstZlkhMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.vo.CusLstWtsxExportVo;
import cn.com.yusys.yusp.vo.CusLstZlkhExportVo;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstWtsx;
import cn.com.yusys.yusp.repository.mapper.CusLstWtsxMapper;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWtsxService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:17:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstWtsxService {

    @Autowired
    private CusLstWtsxMapper cusLstWtsxMapper;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CusBaseMapper cusBaseMapper;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private AdminSmOrgService adminSmOrgService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstWtsx selectByPrimaryKey(String serno) {
        return cusLstWtsxMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstWtsx> selectAll(QueryModel model) {
        List<CusLstWtsx> records = (List<CusLstWtsx>) cusLstWtsxMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstWtsx> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstWtsx> list = cusLstWtsxMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstWtsx record) {
        return cusLstWtsxMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstWtsx record) {
        return cusLstWtsxMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstWtsx record) {
        return cusLstWtsxMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstWtsx record) {
        return cusLstWtsxMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstWtsxMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstWtsxMapper.deleteByIds(ids);
    }

    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusLstWtsxExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param perCustInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> perCustInfoList) {
        List<CusLstWtsx> cusLstWtsxList = (List<CusLstWtsx>) BeanUtils.beansCopy(perCustInfoList, CusLstWtsx.class);
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CusLstWtsxMapper cusLstWtsxMapper = sqlSession.getMapper(CusLstWtsxMapper.class);
            for (CusLstWtsx cusLstWtsx : cusLstWtsxList) {
                if (StringUtil.isEmpty(cusLstWtsx.getCusId())) {
                    throw BizException.error(null, "9999", "客户编号不能为空");
                }
                if (StringUtil.isEmpty(cusLstWtsx.getCusName())) {
                    throw BizException.error(null, "9999", "客户名称不能为空");
                }
                if (StringUtil.isEmpty(cusLstWtsx.getManagerId())) {
                    throw BizException.error(null, "9999", "管户客户经理不能为空");
                }
                if (StringUtil.isEmpty(cusLstWtsx.getBelgOrg())) {
                    throw BizException.error(null, "9999", "所属机构不能为空");
                }
                ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(cusLstWtsx.getManagerId());
                if (manager.getData().getLoginCode() == null) {
                    throw BizException.error(null, "9999", "管户客户经理号不存在");
                }
                ResultDto<AdminSmOrgDto> Sorg = adminSmOrgService.getByOrgCode(cusLstWtsx.getBelgOrg());
                if (Sorg.getData().getOrgId() == null) {
                    throw BizException.error(null, "9999", "所属机构号不存在");
                }
                QueryModel cqm = new QueryModel();
                cqm.getCondition().put("cusId", cusLstWtsx.getCusId());
                List<CusBase> exists = cusBaseMapper.selectByModel(cqm);
                if(exists.isEmpty()){
                    throw BizException.error(null, "9999", cusLstWtsx.getCusId()+"客户编号不存在");
                }else {
                    for (int i = 0; i<exists.size();i++) {
                        if(!exists.get(i).getCusName().equals(cusLstWtsx.getCusName())){
                            throw BizException.error(null, "9999", "导入的客户编号:"+cusLstWtsx.getCusId()+"对应的客户姓名"
                                    +cusLstWtsx.getCusName()+"与客户信息的名称"+exists.get(i).getCusName()+"不一致");
                        }
                    }
                }
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                cusLstWtsx.setSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>()));
                cusLstWtsx.setInputBrId(userInfo.getOrg().getCode());
                cusLstWtsx.setInputId(userInfo.getLoginCode());
                cusLstWtsx.setInputDate(date);
                cusLstWtsx.setCreateTime(date1);
                cusLstWtsx.setUpdateTime(date1);
                cusLstWtsx.setUpdId(userInfo.getLoginCode());
                cusLstWtsx.setImportMode("01"); //人工维护
                cusLstWtsx.setStatus("1");
                cusLstWtsx.setUpdBrId(userInfo.getOrg().getCode());
                cusLstWtsxMapper.insertSelective(cusLstWtsx);
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perCustInfoList.size();
    }

    /**
     * @函数名称:queryCuslstwtsxByCusId
     * @函数描述:
     * @创建者: yangwl
     * @创建时间: 2021/6/15
     * @参数与返回说明:
     * @算法描述:
     */
    public CusLstWtsx queryCuslstwtsxByCusId(String cusId) {

        CusLstWtsx cusLstWtsx = cusLstWtsxMapper.queryCuslstwtsxByCusId(cusId);

        return cusLstWtsx;
    }
}
