/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.dto.CusAccountInfoDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusAccountInfo;
import cn.com.yusys.yusp.service.CusAccountInfoService;

import javax.management.Query;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusAccountInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 10:18:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusaccountinfo")
public class CusAccountInfoResource {
    @Autowired
    private CusAccountInfoService cusAccountInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusAccountInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusAccountInfo> list = cusAccountInfoService.selectAll(queryModel);
        return new ResultDto<List<CusAccountInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusAccountInfo>> index(QueryModel queryModel) {
        List<CusAccountInfo> list = cusAccountInfoService.selectByModel(queryModel);
        return new ResultDto<List<CusAccountInfo>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusAccountInfo> create(@RequestBody CusAccountInfo cusAccountInfo) throws URISyntaxException {
        if(cusAccountInfo.getIsEffect().equals("1")) {
            QueryModel qm = new QueryModel();
            qm.addCondition("cusId", cusAccountInfo.getCusId());
            List<CusAccountInfo> list = cusAccountInfoService.selectByModel(qm);
            for (CusAccountInfo cai : list) {
                if (cai.getIsEffect().equals("1")) {
                    throw BizException.error(null, EcbEnum.ECB020037.key, EcbEnum.ECB020037.value);
                }
            }
        }
        cusAccountInfoService.insert(cusAccountInfo);
        return new ResultDto<CusAccountInfo>(cusAccountInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusAccountInfo cusAccountInfo) throws URISyntaxException {
        if(cusAccountInfo.getIsEffect().equals("1")) {
            QueryModel qm = new QueryModel();
            qm.addCondition("cusId", cusAccountInfo.getCusId());
            List<CusAccountInfo> list = cusAccountInfoService.selectByModel(qm);
            for (CusAccountInfo cai : list) {
                if (cai.getIsEffect().equals("1")) {
                    throw BizException.error(null, EcbEnum.ECB020037.key, EcbEnum.ECB020037.value);
                }
            }
        }
        int result = cusAccountInfoService.update(cusAccountInfo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusAccountInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusAccountInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param cusId
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryAccount")
    protected ResultDto<List<CusAccountInfoDto>> queryAccount(@RequestBody String cusId) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("isEffect", "1");
        List<CusAccountInfo> list = cusAccountInfoService.selectByModel(queryModel);
        List<CusAccountInfoDto> collect = list.stream().map(cusAccountInfo -> {
            CusAccountInfoDto cusAccountInfoDto = new CusAccountInfoDto();
            BeanUtils.copyProperties(cusAccountInfo, cusAccountInfoDto);
            return cusAccountInfoDto;
        }).collect(Collectors.toList());
        return new ResultDto<List<CusAccountInfoDto>>(collect);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param map
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querycusaccountinfobyparams")
    protected ResultDto<List<CusAccountInfoDto>> queryCusAccountInfoByParams(@RequestBody Map map) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId", map.get("cusId"));
        List<CusAccountInfo> list = cusAccountInfoService.selectByModel(queryModel);
        List<CusAccountInfoDto> collect = list.stream().map(cusAccountInfo -> {
            CusAccountInfoDto cusAccountInfoDto = new CusAccountInfoDto();
            BeanUtils.copyProperties(cusAccountInfo, cusAccountInfoDto);
            return cusAccountInfoDto;
        }).collect(Collectors.toList());
        return new ResultDto<List<CusAccountInfoDto>>(collect);
    }
}
