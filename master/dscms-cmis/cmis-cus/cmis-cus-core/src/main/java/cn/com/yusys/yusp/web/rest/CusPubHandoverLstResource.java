/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusPubHandoverLst;
import cn.com.yusys.yusp.service.CusPubHandoverLstService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusPubHandoverLstResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: pc
 * @创建时间: 2021-05-10 16:17:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuspubhandoverlst")
public class CusPubHandoverLstResource {
    @Autowired
    private CusPubHandoverLstService cusPubHandoverLstService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusPubHandoverLst>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusPubHandoverLst> list = cusPubHandoverLstService.selectAll(queryModel);
        return new ResultDto<List<CusPubHandoverLst>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusPubHandoverLst>> index(QueryModel queryModel) {
        List<CusPubHandoverLst> list = cusPubHandoverLstService.selectByModel(queryModel);
        return new ResultDto<List<CusPubHandoverLst>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusPubHandoverLst>> query(@RequestBody  QueryModel queryModel) {
        queryModel.setSort("createTime desc");
        List<CusPubHandoverLst> list = cusPubHandoverLstService.selectByModel(queryModel);
        return new ResultDto<List<CusPubHandoverLst>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusPubHandoverLst> show(@PathVariable("pkId") String pkId) {
        CusPubHandoverLst cusPubHandoverLst = cusPubHandoverLstService.selectByPrimaryKey(pkId);
        return new ResultDto<CusPubHandoverLst>(cusPubHandoverLst);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusPubHandoverLst> create(@RequestBody CusPubHandoverLst cusPubHandoverLst) throws URISyntaxException {
        cusPubHandoverLstService.insert(cusPubHandoverLst);
        return new ResultDto<CusPubHandoverLst>(cusPubHandoverLst);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusPubHandoverLst cusPubHandoverLst) throws URISyntaxException {
        int result = cusPubHandoverLstService.update(cusPubHandoverLst);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusPubHandoverLstService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusPubHandoverLstService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/query/selectByModeList")
    protected ResultDto<List<CusPubHandoverLst>> selectByModeList(@RequestBody CusPubHandoverLst cusPubHandoverLst) {
        List<CusPubHandoverLst> list = cusPubHandoverLstService.selectByModeList(cusPubHandoverLst);
        return new ResultDto<List<CusPubHandoverLst>>(list);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletebyparam")
    protected ResultDto<Integer> deleteByParam(@RequestBody CusPubHandoverLst cusPubHandoverLst) {
        cusPubHandoverLstService.deleteByParam(cusPubHandoverLst);
        return new ResultDto<Integer>(1);
    }


    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selecthandvoerinwayapp")
    protected ResultDto<Integer> selectHandvoerInWayApp(@RequestBody CusPubHandoverLst cusPubHandoverLst) {
        int result = cusPubHandoverLstService.selectHandvoerInWayApp(cusPubHandoverLst);
        return new ResultDto<Integer>(result);
    }

}
