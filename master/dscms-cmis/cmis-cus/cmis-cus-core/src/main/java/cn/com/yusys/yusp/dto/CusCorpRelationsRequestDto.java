package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * 客户关系网
 */
public class CusCorpRelationsRequestDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 层级 **/
    private String cusLevel;
    /** 客户编号 **/
    private String cusId;
    /** 高管关系 **/
    private String mgrRelations;
    /** 法人家庭关系 **/
    private String famRelations;
    /** 对外投资关系 **/
    private String pubRelations;
    /** 股东关系 **/
    private String apiRelations;
    /**所在关联集团信息**/
    private String cusGrpMembes;

    public String getCusLevel() {
        return cusLevel;
    }

    public void setCusLevel(String cusLevel) {
        this.cusLevel = cusLevel;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getMgrRelations() {
        return mgrRelations;
    }

    public void setMgrRelations(String mgrRelations) {
        this.mgrRelations = mgrRelations;
    }

    public String getFamRelations() {
        return famRelations;
    }

    public void setFamRelations(String famRelations) {
        this.famRelations = famRelations;
    }

    public String getPubRelations() {
        return pubRelations;
    }

    public void setPubRelations(String pubRelations) {
        this.pubRelations = pubRelations;
    }

    public String getApiRelations() {
        return apiRelations;
    }

    public void setApiRelations(String apiRelations) {
        this.apiRelations = apiRelations;
    }

    public String getCusGrpMembes() {
        return cusGrpMembes;
    }

    public void setCusGrpMembes(String cusGrpMembes) {
        this.cusGrpMembes = cusGrpMembes;
    }
}
