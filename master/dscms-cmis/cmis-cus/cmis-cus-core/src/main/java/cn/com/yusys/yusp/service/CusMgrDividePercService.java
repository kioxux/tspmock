/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.CusMgrDividePercDto;
import cn.com.yusys.yusp.service.client.bsp.outerdata.idcheck.OrgCheckService;
import cn.com.yusys.yusp.workflow.service.KHGL13BizService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusMgrDividePerc;
import cn.com.yusys.yusp.repository.mapper.CusMgrDividePercMapper;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusMgrDividePercService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 14:22:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusMgrDividePercService {

    @Resource
    private CusMgrDividePercMapper cusMgrDividePercMapper;

    @Autowired
    private OrgCheckService orgCheckService;

    @Resource
    private AdminSmOrgService adminSmOrgService;

    private final Logger log = LoggerFactory.getLogger(CusMgrDividePercService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusMgrDividePerc selectByPrimaryKey(String pkId, String cusId, String managerId) {
        return cusMgrDividePercMapper.selectByPrimaryKey(pkId, cusId, managerId);
    }

    /**
     * @方法名称: selectByPkId
     * @方法描述: 根据 PkId 查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusMgrDividePerc> selectByPkId(String pkId) {
        return cusMgrDividePercMapper.selectByPkId(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusMgrDividePerc> selectAll(QueryModel model) {
        List<CusMgrDividePerc> records = (List<CusMgrDividePerc>) cusMgrDividePercMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusMgrDividePerc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusMgrDividePerc> list = cusMgrDividePercMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusMgrDividePerc record) {
        return cusMgrDividePercMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusMgrDividePerc record) {
        return cusMgrDividePercMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusMgrDividePerc record) {
        return cusMgrDividePercMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusMgrDividePerc record) {
        return cusMgrDividePercMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId, String managerId) {
        return cusMgrDividePercMapper.deleteByPrimaryKey(cusId, managerId);
    }

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusMgrDividePerc> selectByCusId(QueryModel model) {
        String cusId = (String) model.getCondition().get("cusId");
        List<CusMgrDividePerc> list = cusMgrDividePercMapper.selectByCusId(cusId);
        return list;
    }

    /**
     * @方法名称: queryBeforeCreate
     * @方法描述: 创建任务
     * @参数与返回说明:
     * @算法描述: 无
     * @return
     */

    public String queryBeforeCreate(List<CusMgrDividePerc> record) throws Exception {
        String pkId = null;
        if (record.size() > 0) {
            pkId = UUID.randomUUID().toString().trim().replace("-", "");
            for (int i = 0; i < record.size(); i++) {
                String cusId = record.get(i).getCusId();
                String managerId = record.get(i).getManagerId();
                String managerProp = record.get(i).getManagerProp();
                List<CusMgrDividePerc> list = cusMgrDividePercMapper.selectByParam(cusId, managerId, managerProp);
                if (list.size()>0){
                    throw BizException.error(null,"9999","该客户的客户经理分成比例申请正在审批中,请稍后处理！");
                }
                User userInfo = SessionUtils.getUserInformation();
                record.get(i).setPkId(pkId);
                record.get(i).setApproveStatus("000");
                record.get(i).setInputId(userInfo.getLoginCode());
                record.get(i).setInputBrId(userInfo.getOrg().getCode());
                record.get(i).setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                record.get(i).setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                record.get(i).setIsEffect("2");
                cusMgrDividePercMapper.insert(record.get(i));
            }
        } else {
            throw BizException.error(null,"9999","传入数据为空！");
        }
        return pkId;
    }

    /**
     * @方法名称: deleteByParam
     * @方法描述: 根据传入参数删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByParam(CusMgrDividePerc cusMgrDividePerc) {
        String pkId= null;
        String cusId= null;
        String managerId= null;
        String managerProp= null;
        if (cusMgrDividePerc != null) {
            if (!"".equals(cusMgrDividePerc.getPkId()) && cusMgrDividePerc.getPkId() != null) {
                pkId = cusMgrDividePerc.getPkId();
            }
            if (!"".equals(cusMgrDividePerc.getCusId()) && cusMgrDividePerc.getCusId() != null) {
                cusId = cusMgrDividePerc.getCusId();
            }
            if (!"".equals(cusMgrDividePerc.getManagerId()) && cusMgrDividePerc.getManagerId() != null) {
                managerId = cusMgrDividePerc.getManagerId();
            }
            if (!"".equals(cusMgrDividePerc.getManagerProp()) && cusMgrDividePerc.getManagerProp() != null) {
                managerProp = cusMgrDividePerc.getManagerProp();
            }
        }
        return cusMgrDividePercMapper.deleteByParam(pkId, cusId, managerId, managerProp);
    }

    /**
     * @方法名称: updateAll
     * @方法描述: 整条记录更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public String updateAll(List<CusMgrDividePerc> record) {
        String pkId = null;
        String approveStatus = null;
        String isEffect = null;
        if (record.size() > 0) {
            Date currentTime = DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue());
            for (int i = 0; i < record.size(); i++) {
                if (!"".equals(record.get(i).getPkId()) && record.get(i).getPkId() != null) {
                    pkId = record.get(i).getPkId();
                    int deleteResult = this.deleteByParam(record.get(i));
                    if (deleteResult == 1) {
                        record.get(i).setUpdateTime(currentTime);
                        cusMgrDividePercMapper.insert(record.get(i));
                    }
                } else {
                    for (int j = 0; j < record.size(); j++) {
                        if (!"".equals(record.get(j).getPkId()) && record.get(j).getPkId() != null) {
                            pkId = record.get(j).getPkId();
                            approveStatus = record.get(j).getApproveStatus();
                            isEffect = record.get(j).getIsEffect();
                            break;
                        }
                    }
                    User userInfo = SessionUtils.getUserInformation();
                    record.get(i).setPkId(pkId);
                    record.get(i).setApproveStatus(approveStatus);
                    record.get(i).setInputId(userInfo.getLoginCode());
                    record.get(i).setInputBrId(userInfo.getOrg().getCode());
                    record.get(i).setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    record.get(i).setCreateTime(currentTime);
                    record.get(i).setIsEffect(isEffect);
                    cusMgrDividePercMapper.insert(record.get(i));
                }
            }
        }
        return pkId;
    }

    /**
     * @方法名称: selectAllByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusMgrDividePerc> selectAllByCusId(String cusId) {
        List<CusMgrDividePerc> list = cusMgrDividePercMapper.selectAllByCusId(cusId);
        return list;
    }

    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 更新审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateApproveStatus(String pkId, String approveStatus, Date updateTime) {
        return cusMgrDividePercMapper.updateApproveStatus(pkId, approveStatus, updateTime);
    }

    /**
     * @方法名称: updateApproveStatusAndEffect
     * @方法描述: 更新审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateApproveStatusAndEffect(String pkId, String approveStatus, String isEffect, Date updateTime) {
        return cusMgrDividePercMapper.updateApproveStatusAndEffect(pkId, approveStatus, isEffect, updateTime);
    }

    /**
     * @方法名称: checkIsSameOrg
     * @方法描述: 检查主办与协办客户经理所属机构是否一致
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, Object> checkIsSameOrg(String managerId, String managerProp){
        Map<String, Object> resultMap = new HashMap<String, Object>();
        ResultDto<AdminSmOrgDto> resultDto = null;
        User userInfo = SessionUtils.getUserInformation();
        String loginUserId = userInfo.getLoginCode();
        String loginOrgId = userInfo.getOrg().getCode();
        String loginOrgName = null;
        String loginUpOrgId = null;
        resultDto = adminSmOrgService.getByOrgCode(loginOrgId);
        String code = resultDto.getCode();
        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
            loginOrgName = resultDto.getData().getOrgName();
            loginUpOrgId = resultDto.getData().getUpOrgId();
        }
        String orgId = null;
        String orgName = null;
        resultMap = orgCheckService.checkIsSameOrg(managerId, managerProp);
        if (resultMap.containsKey("orgId") && resultMap.get("orgId") != null && !"".equals(resultMap.get("orgId"))) {
            orgId = (String) resultMap.get("orgId");
            orgName = (String) resultMap.get("orgName");
        }
        String upOrgId = null;
        if (resultMap.containsKey("upOrgId") && !"".equals(resultMap.get("upOrgId")) && resultMap.get("upOrgId") != null) {
            upOrgId = (String) resultMap.get("upOrgId");
        }
        if ("1".equals(managerProp)) {
            if (loginUserId.equals(managerId)) {
                log.info("当前客户经理为：" + managerId + "当前客户经理性质为：" + managerProp);
            } else {
                resultMap.put("message", "主办客户经理必须为当前客户的管护经理！");
                resultMap.put("code", "-1");
            }
        }
        if ("2".equals(managerProp)) {
            if (!loginUserId.equals(managerId) && loginOrgId.equals(orgId)) {
                log.info("当前客户经理为：" + managerId + "当前客户经理性质为：" + managerProp);
            } else {
                resultMap.put("message", "协办客户经理不能是当前客户的管户经理，并且所属机构应与主办客户经理一致！");
                resultMap.put("code", "-1");
            }
        }
        if ("3".equals(managerProp)) {
            if (loginUserId.equals(managerId)){
                resultMap.put("message", "虚拟客户经理不能是当前客户的管户经理，并且所属机构应与主办客户经理一致或为下辖机构！");
                resultMap.put("code", "-1");
            }else {
                //当前登录人的登录机构为分行本部时
                if (loginOrgName.contains("分行本部")) {
                    loginOrgId = loginUpOrgId;
                    //判断性质为无的分成客户经理所属机构的上级机构与当前登录机构的上级机构是否同为相同分行
                    if ( !loginOrgId.equals(upOrgId)) {
                        resultMap.put("message", "虚拟客户经理不能是当前客户的管户经理，并且所属机构应与主办客户经理一致或为下辖机构！");
                        resultMap.put("code", "-1");
                    } else {
                        log.info("当前客户经理为：" + managerId + "当前客户经理性质为：" + managerProp);
                    }
                // 当前分成客户经理是分行本部
                } else if(orgName.contains("分行本部")){
                    orgId = upOrgId;
                    //判断性质为无的分成客户经理所属机构的上级机构与当前登录机构的上级机构是否同为相同分行
                    if ( !loginUpOrgId.equals(orgId)) {
                        resultMap.put("message", "虚拟客户经理不能是当前客户的管户经理，并且所属机构应与主办客户经理一致或为下辖机构！");
                        resultMap.put("code", "-1");
                    } else {
                        log.info("当前客户经理为：" + managerId + "当前客户经理性质为：" + managerProp);
                    }
                }else {
                    // 当前登录人与分成客户经理机构处同一机构
                    if (!loginOrgId.equals(orgId)) {
                        resultMap.put("message", "虚拟客户经理不能是当前客户的管户经理，并且所属机构应与主办客户经理一致或为下辖机构！");
                        resultMap.put("code", "-1");
                    } else {
                        log.info("当前客户经理为：" + managerId + "当前客户经理性质为：" + managerProp);
                    }
                }
            }
        }
        return  resultMap;
    }

    /**
     * @方法名称: selectEffectListByCusId
     * @方法描述: 根据客户编号查询已生效的列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusMgrDividePerc> selectEffectListByCusId(QueryModel model) {
        String cusId = (String) model.getCondition().get("cusId");
        List<CusMgrDividePerc> list = cusMgrDividePercMapper.selectEffectListByCusId(cusId);
        return list;
    }

    /**
     * @方法名称: orgAccessCheck
     * @方法描述: 查询当前机构是否可做分成比例
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Boolean orgAccessCheck() {
        Boolean flag;
        User userInfo = SessionUtils.getUserInformation();
        String loginOrgId = userInfo.getOrg().getCode();
        flag = orgCheckService.orgAccessCheck(loginOrgId);
        return flag;
    }


    /**
     * @方法名称: selectXBManagerId
     * @方法描述: 查询当前机构是否可做分成比例
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusMgrDividePercDto selectXBManagerId(String cusId) {
        return cusMgrDividePercMapper.selectXBManagerId(cusId);
    }

    /**
     * @方法名称: isVirtualCusManager
     * @方法描述: 查询是否为虚拟客户经理
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Boolean isVirtualCusManager(String managerId, String managerProp) {
        Boolean flag;
        flag = orgCheckService.isVirtualCusManager(managerId, managerProp);
        return flag;
    }

    /**
     * @方法名称: updatecusMgrDividePerc
     * @方法描述: 修改分成比例
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updatecusMgrDividePerc(String pkId, String cusId,String approveStatus, String isEffect, Date updateTime) {
        int count=0;
        // 分成比例审批通过后先查询是否存在生效的分成结果，存在则先使其失效再重新更改分成比例
        List<CusMgrDividePerc> effectListByCusIdList =cusMgrDividePercMapper.selectEffectListByCusId(cusId);
        //判断查询结果是否为空
        if(CollectionUtils.nonEmpty(effectListByCusIdList)){
            // 如果存在生效的，先修改生效的失效，再重新新增
            for(CusMgrDividePerc list :effectListByCusIdList){
                String oldPkId=list.getPkId();
                count =cusMgrDividePercMapper.updateApproveStatusAndEffect(oldPkId, null, "2", updateTime);
                if(count>0){
                    count = cusMgrDividePercMapper.updateApproveStatusAndEffect(pkId, approveStatus, isEffect, updateTime);
                }
            }
        }else {
            // 如果不存在生效的直接修改
           count = cusMgrDividePercMapper.updateApproveStatusAndEffect(pkId, approveStatus, isEffect, updateTime);
        }
        return count ;
    }
}
