package cn.com.yusys.yusp.service.server.xdkh0036;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusIntbank;
import cn.com.yusys.yusp.dto.server.xdkh0036.req.Xdkh0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0036.resp.Xdkh0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMapper;
import cn.com.yusys.yusp.service.CusCorpService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0036Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-11 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0036Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0036Service.class);

    @Resource
    private CusCorpMapper cusCorpMapper;
    @Resource
    private CusIntbankMapper cusIntbankMapper;
    @Resource
    private CusCorpService cusCorpService;

    /**
     * 信息锁定标志同步
     *
     * @param xdkh0036DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0036DataRespDto xdkh0036(Xdkh0036DataReqDto xdkh0036DataReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, JSON.toJSONString(xdkh0036DataReqDto));
        Xdkh0036DataRespDto xdkh0036DataRespDto = new Xdkh0036DataRespDto();
        String cusId = xdkh0036DataReqDto.getCusId();//客户编号

        try {
            CusCorp cusCorp = cusCorpService.selectCropAndBaseByPrimarKey(cusId);
            if (Objects.nonNull(cusCorp)){
                xdkh0036DataRespDto.setCusName(cusCorp.getCusName());//客户名称
                xdkh0036DataRespDto.setAddress(cusCorp.getSendAddr());//送达地址
                xdkh0036DataRespDto.setFreqLinkman(cusCorp.getFreqLinkman());// 常用联系人
                xdkh0036DataRespDto.setMobile(cusCorp.getFreqLinkmanTel()); // 常用联系人地址
                xdkh0036DataRespDto.setQq(cusCorp.getQq());// qq
                xdkh0036DataRespDto.setWechat(cusCorp.getWechatNo());// 微信
                xdkh0036DataRespDto.setOtherContract(cusCorp.getOperAddrAct()); // 实际经营地址
                xdkh0036DataRespDto.setEmail(cusCorp.getLinkmanEmail()); //邮箱
                xdkh0036DataRespDto.setFaxCode(cusCorp.getFax()); //传真
//                xdkh0036DataRespDto.setinsCode(cusCorp.getInsCode()); //组织机构代码

            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, e.getMessage());
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, JSON.toJSONString(xdkh0036DataReqDto));
        return xdkh0036DataRespDto;
    }
}
