/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CusBizAssignAppDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusBizAssignApp;
import cn.com.yusys.yusp.service.CusBizAssignAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBizAssignAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-07 16:37:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusbizassignapp")
public class CusBizAssignAppResource {
    @Autowired
    private CusBizAssignAppService cusBizAssignAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusBizAssignApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusBizAssignApp> list = cusBizAssignAppService.selectAll(queryModel);
        return new ResultDto<List<CusBizAssignApp>>(list);
    }

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query")
    protected ResultDto<List<CusBizAssignApp>> queryAll(@RequestBody QueryModel queryModel) {
        queryModel.setSort("createTime desc");
        List<CusBizAssignApp> list = cusBizAssignAppService.selectByModel(queryModel);
        return new ResultDto<List<CusBizAssignApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusBizAssignApp>> index(QueryModel queryModel) {
        List<CusBizAssignApp> list = cusBizAssignAppService.selectByModel(queryModel);
        return new ResultDto<List<CusBizAssignApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{assignAppSerno}")
    protected ResultDto<CusBizAssignApp> show(@PathVariable("assignAppSerno") String assignAppSerno) {
        CusBizAssignApp cusBizAssignApp = cusBizAssignAppService.selectByPrimaryKey(assignAppSerno);
        return new ResultDto<CusBizAssignApp>(cusBizAssignApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口   BRIGHT_SASSIGN_APPLY
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusBizAssignApp> create(@RequestBody CusBizAssignApp cusBizAssignApp) throws URISyntaxException {
        cusBizAssignAppService.insert(cusBizAssignApp);
        return new ResultDto<CusBizAssignApp>(cusBizAssignApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusBizAssignApp cusBizAssignApp) throws URISyntaxException {
        int result = cusBizAssignAppService.update(cusBizAssignApp);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:updateSelective
     * @函数描述:对象修改，公共API接口,只修改非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatemore")
    protected ResultDto<Integer> updateMore(@RequestBody CusBizAssignAppDto dto) throws URISyntaxException {
        int result = cusBizAssignAppService.updateMore(dto);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{assignAppSerno}")
    protected ResultDto<Integer> delete(@PathVariable("assignAppSerno") String assignAppSerno) {
        int result =cusBizAssignAppService.delete(assignAppSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusBizAssignAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
