/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstYndLinkman;
import cn.com.yusys.yusp.repository.mapper.CusLstYndLinkmanMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndLinkmanService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:19:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYndLinkmanService {

    @Autowired
    private CusLstYndLinkmanMapper cusLstYndLinkmanMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstYndLinkman selectByPrimaryKey(String pkId) {
        return cusLstYndLinkmanMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYndLinkman> selectAll(QueryModel model) {
        List<CusLstYndLinkman> records = (List<CusLstYndLinkman>) cusLstYndLinkmanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstYndLinkman> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYndLinkman> list = cusLstYndLinkmanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstYndLinkman record) {
        return cusLstYndLinkmanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstYndLinkman record) {
        return cusLstYndLinkmanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstYndLinkman record) {
        return cusLstYndLinkmanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstYndLinkman record) {
        return cusLstYndLinkmanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusLstYndLinkmanMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYndLinkmanMapper.deleteByIds(ids);
    }

    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/7/16 14:29
     * @version 1.0.0
     * @desc  通过流水号删除紧急联系人
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int deleteBySerno(String serno) {
        return cusLstYndLinkmanMapper.deleteBySerno(serno);
    }

    /**
     * @param cusLstYndLinkman
     * @return int
     * @author hubp
     * @date 2021/7/16 15:31
     * @version 1.0.0
     * @desc    新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int addCusLstYndLinkman(CusLstYndLinkman cusLstYndLinkman) {
        int result = 0;
        if (StringUtils.isBlank(cusLstYndLinkman.getPkId())) {
            cusLstYndLinkman.setPkId(StringUtils.getUUID());
            cusLstYndLinkman.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            result = cusLstYndLinkmanMapper.insert(cusLstYndLinkman);
        } else {
            CusLstYndLinkman clyl = cusLstYndLinkmanMapper.selectByPrimaryKey(cusLstYndLinkman.getPkId());
            if (null == clyl) {
                cusLstYndLinkman.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                result = cusLstYndLinkmanMapper.insert(cusLstYndLinkman);
            } else {
                result = cusLstYndLinkmanMapper.updateByPrimaryKey(cusLstYndLinkman);
            }
        }
        return result;
    }
    /**
     * @param serno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/7/16 15:35
     * @version 1.0.0
     * @desc  通过流水号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<CusLstYndLinkman> selectBySerno(String serno){
        return cusLstYndLinkmanMapper.selectBySerno(serno);
    }
}
