/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.server.xdkh0028.req.Xdkh0028DataReqDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusLstYnd;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 09:50:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusLstYndMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusLstYnd selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusLstYnd> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusLstYnd record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusLstYnd record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusLstYnd record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusLstYnd record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectBlacklist
     * @方法描述: 优农贷黑名单查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<cn.com.yusys.yusp.dto.server.xdkh0028.resp.List> selectBlacklist(Xdkh0028DataReqDto xdkh0028DataReqDto);

    /**
     * @方法名称: selectYndLstByUserList
     * @方法描述: 查询当前业务类型数量最少的人
     * @参数与返回说明:
     * @算法描述: 无
     */

    List selectYndLstByUserList(@Param("ids") String ids,@Param("openDay") String openDay);
}