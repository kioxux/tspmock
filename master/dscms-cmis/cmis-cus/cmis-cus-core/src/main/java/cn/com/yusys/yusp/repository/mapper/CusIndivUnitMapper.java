/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusIndivUnit;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus模块
 * @类名称: CusIndivUnitMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-11-16 11:44:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusIndivUnitMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusIndivUnit selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusIndivUnit selectByCusId(@Param("cusId") String cusId);


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusIndivUnit> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusIndivUnit record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusIndivUnit record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusIndivUnit record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusIndivUnit record);

    /**
     * @方法名称: updateCusIndivUnitByCusId
     * @方法描述: 根据客户号更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateCusIndivUnitByCusId(CusIndivUnit record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectBycusId
     * @方法描述: 根据客户号查询
     * @参数与返回说明:
     * @算法描述: 无
     * @修改人： 周茂伟
     */

    CusIndivUnit selectBycusId(@Param("cusId") String cusId);

    /**
     * 根据客户号更新-只更新非空字段
     * @param record
     * @return
     */
   int updateByCusIdSelective(CusIndivUnit record);

    /**
     * 根据客户号查询关联企业客户号
     * @param cusId
     * @return
     */
   Map<String,String> selectUnitIdByCusId(@Param("cusId") String cusId);
}