package cn.com.yusys.yusp.web.server.xdkh0026;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0026.req.Xdkh0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0026.resp.Xdkh0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0026.Xdkh0026Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优企贷、优农贷行内关联人基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0026:优企贷、优农贷行内关联人基本信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0026Resource.class);

    @Autowired
    private Xdkh0026Service xdkh0026Service;

    /**
     * 交易码：xdkh0026
     * 交易描述：优企贷、优农贷行内关联人基本信息查询
     *
     * @param xdkh0026DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷行内关联人基本信息查询")
    @PostMapping("/xdkh0026")
    protected @ResponseBody
    ResultDto<Xdkh0026DataRespDto> xdkh0026(@Validated @RequestBody Xdkh0026DataReqDto xdkh0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026DataReqDto));
        Xdkh0026DataRespDto xdkh0026DataRespDto = new Xdkh0026DataRespDto();// 响应Dto:优企贷、优农贷行内关联人基本信息查询
        ResultDto<Xdkh0026DataRespDto> xdkh0026DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026DataReqDto));
            xdkh0026DataRespDto = xdkh0026Service.xdkh0026(xdkh0026DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026DataRespDto));

            // 封装xdkh0026DataResultDto中正确的返回码和返回信息
            xdkh0026DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0026DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, e.getMessage());
            // 封装xdkh0026DataResultDto中异常返回码和返回信息
            xdkh0026DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0026DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0026DataRespDto到xdkh0026DataResultDto中
        xdkh0026DataResultDto.setData(xdkh0026DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026DataResultDto));
        return xdkh0026DataResultDto;
    }
}