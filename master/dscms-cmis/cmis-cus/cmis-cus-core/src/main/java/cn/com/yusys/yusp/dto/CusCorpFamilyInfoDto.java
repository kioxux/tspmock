package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

public class CusCorpFamilyInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //上一级客户ID
    private String upperCusId;
    private List<CusCorpRelationsDto> cusCorpRelationsDto;

    public String getUpperCusId() {
        return upperCusId;
    }

    public void setUpperCusId(String upperCusId) {
        this.upperCusId = upperCusId;
    }

    public List<CusCorpRelationsDto> getCusCorpRelationsDto() {
        return cusCorpRelationsDto;
    }

    public void setCusCorpRelationsDto(List<CusCorpRelationsDto> cusCorpRelationsDto) {
        this.cusCorpRelationsDto = cusCorpRelationsDto;
    }
}
