package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.util.Date;

@ExcelCsv(namePrefix = "批量客户移交清单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CusBatPubHandoverLstVo {

    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /** 客户名称 **/
    @ExcelField(title = "客户名称", viewLength = 30)
    private String cusName;

    /** 移出人工号 **/
    @ExcelField(title = "移出人工号", viewLength = 20)
    private String handoverId;

    /** 移出人姓名 **/
    @ExcelField(title = "移出人姓名", viewLength = 20)
    private String handoverName;

    /** 接收人工号 **/
    @ExcelField(title = "接收人工号", viewLength = 20)
    private String receiverId;

    /** 接收人姓名 **/
    @ExcelField(title = "接收人姓名", viewLength = 20)
    private String receiverName;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getHandoverId() {
        return handoverId;
    }

    public void setHandoverId(String handoverId) {
        this.handoverId = handoverId;
    }

    public String getHandoverName() {
        return handoverName;
    }

    public void setHandoverName(String handoverName) {
        this.handoverName = handoverName;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
}
