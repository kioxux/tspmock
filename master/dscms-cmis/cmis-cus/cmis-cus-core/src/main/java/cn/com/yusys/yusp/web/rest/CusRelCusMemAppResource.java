/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusRelCusMemApp;
import cn.com.yusys.yusp.service.CusRelCusMemAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusRelCusMemAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: liqiang
 * @创建时间: 2021-05-07 21:29:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusrelcusmemapp")
public class CusRelCusMemAppResource {
    @Autowired
    private CusRelCusMemAppService cusRelCusMemAppService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusRelCusMemApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusRelCusMemApp> list = cusRelCusMemAppService.selectAll(queryModel);
        return new ResultDto<List<CusRelCusMemApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusRelCusMemApp>> index(@RequestBody QueryModel queryModel) {
        List<CusRelCusMemApp> list = cusRelCusMemAppService.selectByModel(queryModel);
        return new ResultDto<List<CusRelCusMemApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusRelCusMemApp> show(@PathVariable("pkId") String pkId) {
        CusRelCusMemApp cusRelCusMemApp = cusRelCusMemAppService.selectByPrimaryKey(pkId);
        return new ResultDto<CusRelCusMemApp>(cusRelCusMemApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusRelCusMemApp> create(@RequestBody CusRelCusMemApp cusRelCusMemApp) throws URISyntaxException {
        cusRelCusMemAppService.insert(cusRelCusMemApp);
        return new ResultDto<CusRelCusMemApp>(cusRelCusMemApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusRelCusMemApp cusRelCusMemApp) throws URISyntaxException {
        int result = cusRelCusMemAppService.update(cusRelCusMemApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusRelCusMemAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusRelCusMemAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:修改部分数据
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelect")
    protected ResultDto<Integer> updateSelect(@RequestBody CusRelCusMemApp cusRelCusMemApp) {
        int result = cusRelCusMemAppService.updateSelective(cusRelCusMemApp);
        return new ResultDto<Integer>(result);
    }
}
