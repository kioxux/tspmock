/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusRelCus;
import cn.com.yusys.yusp.service.CusRelCusService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusRelCusResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: liqiang
 * @创建时间: 2021-05-07 21:29:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusrelcus")
public class CusRelCusResource {
    @Autowired
    private CusRelCusService cusRelCusService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusRelCus>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusRelCus> list = cusRelCusService.selectAll(queryModel);
        return new ResultDto<List<CusRelCus>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusRelCus>> index(@RequestBody QueryModel queryModel) {
        List<CusRelCus> list = cusRelCusService.selectByModel(queryModel);
        return new ResultDto<List<CusRelCus>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{correNo}")
    protected ResultDto<CusRelCus> show(@PathVariable("correNo") String correNo) {
        CusRelCus cusRelCus = cusRelCusService.selectByPrimaryKey(correNo);
        return new ResultDto<CusRelCus>(cusRelCus);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusRelCus> create(@RequestBody CusRelCus cusRelCus) throws URISyntaxException {
        cusRelCusService.insert(cusRelCus);
        return new ResultDto<CusRelCus>(cusRelCus);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusRelCus cusRelCus) throws URISyntaxException {
        int result = cusRelCusService.update(cusRelCus);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{correNo}")
    protected ResultDto<Integer> delete(@PathVariable("correNo") String correNo) {
        int result = cusRelCusService.deleteByPrimaryKey(correNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusRelCusService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
