package cn.com.yusys.yusp.web.server.cmiscus0010;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusBaseService;
import cn.com.yusys.yusp.service.CusIntbankService;
import cn.com.yusys.yusp.web.server.cmiscus0010.CmisCus0010Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 根据客户编号,证件类型，证件号码查询同业客户基本信息
 *
 * @author 蒋云龙
 * @version 1.0
 */
@Api(tags = "CmisCus0010:根据客户编号,证件类型，证件号码查询同业客户基本信息")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0010Resource.class);

    @Autowired
    private CusBaseService cusBaseService;
    @Autowired
    private CusIntbankService cusIntbankService;

    /**
     * 交易码：cmiscus0010
     * 交易描述：查询个人客户基本信息
     *
     * @param cmisCus0010ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户编号,证件类型，证件号码查询同业客户基本信息")
    @PostMapping("/cmiscus0010")
    protected @ResponseBody
    ResultDto<CmisCus0010RespDto> cmiscus0010(@Validated @RequestBody CmisCus0010ReqDto cmisCus0010ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0010.key, DscmsEnum.TRADE_CODE_CMISCUS0010.value, JSON.toJSONString(cmisCus0010ReqDto));
        CmisCus0010RespDto cmisCus0010RespDto = new CmisCus0010RespDto();// 响应Dto:查询同业客户基本信息
        ResultDto<CmisCus0010RespDto> cmisCus0010ResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0010.key, DscmsEnum.TRADE_CODE_CMISCUS0010.value, JSON.toJSONString(cmisCus0010ReqDto));
            cmisCus0010RespDto = cusIntbankService.cmiscus0010(cmisCus0010ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0010.key, DscmsEnum.TRADE_CODE_CMISCUS0010.value, JSON.toJSONString(cmisCus0010RespDto));
            // 封装cmisCus0010ResultDto中正确的返回码和返回信息
            cmisCus0010ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0010ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0010.key, DscmsEnum.TRADE_CODE_CMISCUS0010.value, e.getMessage());
            // 封装cmisCus0010ResultDto中异常返回码和返回信息
            //cmisCus0010ResultDto.setCode(e.get);
            cmisCus0010ResultDto.setMessage(e.getMessage());
        }
        // 封装cmisCus0010RespDto到cmisCus0010ResultDto中
        cmisCus0010ResultDto.setData(cmisCus0010RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0010.key, DscmsEnum.TRADE_CODE_CMISCUS0010.value, JSON.toJSONString(cmisCus0010ResultDto));
        return cmisCus0010ResultDto;
    }
}
