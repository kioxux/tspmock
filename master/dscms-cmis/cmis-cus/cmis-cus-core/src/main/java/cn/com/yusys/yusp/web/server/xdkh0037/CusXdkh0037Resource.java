package cn.com.yusys.yusp.web.server.xdkh0037;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0037.req.Xdkh0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0037.resp.Xdkh0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0037.Xdkh0037Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:微业贷新开户信息入库
 *
 * @author zdl
 * @version 1.0
 */
@Api(tags = "XDKH0037:微业贷新开户信息入库")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0037Resource.class);

    @Autowired
    private Xdkh0037Service xdkh0037Service;
    /**
     * 交易码：xdkh0037
     * 交易描述：微业贷新开户信息入库
     * @param xdkh0037DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("微业贷新开户信息入库")
    @PostMapping("/xdkh0037")
    protected @ResponseBody
    ResultDto<Xdkh0037DataRespDto> xdkh0037(@Validated @RequestBody Xdkh0037DataReqDto xdkh0037DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, JSON.toJSONString(xdkh0037DataReqDto));
        Xdkh0037DataRespDto xdkh0037DataRespDto = new Xdkh0037DataRespDto();// 响应Dto:微业贷新开户信息入库
        ResultDto<Xdkh0037DataRespDto> xdkh0037DataResultDto = new ResultDto<Xdkh0037DataRespDto>();
        try {

            xdkh0037DataRespDto = xdkh0037Service.xdkh0037(xdkh0037DataReqDto);

            xdkh0037DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0037DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        }catch (BizException be){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, be.getMessage());
            xdkh0037DataResultDto.setCode(be.getErrorCode());
            xdkh0037DataResultDto.setMessage(be.getMessage());
        }catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, e.getMessage());
            // 封装xdkh0037DataResultDto中异常返回码和返回信息
            xdkh0037DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0037DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0037DataRespDto到xdkh0037DataResultDto中
        xdkh0037DataResultDto.setData(xdkh0037DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, JSON.toJSONString(xdkh0037DataResultDto));
        return xdkh0037DataResultDto;
    }
}
