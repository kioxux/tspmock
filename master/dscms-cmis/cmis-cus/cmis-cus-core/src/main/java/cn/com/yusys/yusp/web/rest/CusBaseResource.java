/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.service.CusBaseService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusBaseResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-14 09:59:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusbase")
public class CusBaseResource {
    private static final Logger log = LoggerFactory.getLogger(CusBaseResource.class);

    @Autowired
    private CusBaseService cusBaseService;
	/**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusBase>> query() {
        QueryModel queryModel = new QueryModel();

        List<CusBase> list = cusBaseService.selectAll(queryModel);
        return new ResultDto<List<CusBase>>(list);
    }

    /**
     * @param
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CusBase>> index(@RequestBody QueryModel queryModel) {
        List<CusBase> list = cusBaseService.selectByModel(queryModel);
        return new ResultDto<List<CusBase>>(list);
    }


    /**
     * @param
     * @函数名称:用于资金同业：主体授信、产品授信 选取客户
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryAll")
    protected ResultDto<List<CusBase>> queryAll(@RequestBody QueryModel queryModel) {
        List<CusBase> list = cusBaseService.selectByModel(queryModel);
        return new ResultDto<List<CusBase>>(list);
    }

    /**
     * @param
     * @函数名称:query
     * @函数描述:查询客户对象列表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querybymodel")
    protected ResultDto<List<CusBase>> queryByModel(@RequestBody QueryModel queryModel) {
        List<CusBase> list = cusBaseService.selectCusByModel(queryModel);
        return new ResultDto<List<CusBase>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cusId}")
    protected ResultDto<CusBase> show(@PathVariable("cusId") String cusId) {
        CusBase cusBase = cusBaseService.selectByPrimaryKey(cusId);
        return new ResultDto<CusBase>(cusBase);
    }

    /**
     * @函数名称:queryCus
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCus")
    protected CusBaseClientDto queryCus(@RequestBody String cusId) {
        CusBase cusBase = cusBaseService.selectByPrimaryKey(cusId);
        CusBaseClientDto cusBaseClientDto = new CusBaseClientDto();
        BeanUtils.beanCopy(cusBase,cusBaseClientDto);
        return cusBaseClientDto;
    }

    /**
     * @方法名称：cusBaseInfo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/31 15:09
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/cusBaseInfo")
    protected ResultDto<CusBase> cusBaseInfo(@RequestBody String cusId) {
        CusBase cusBase = cusBaseService.selectByPrimaryKey(cusId);
        return new ResultDto<>(cusBase);
    }

    /**
     * @函数名称:queryCusByCertCode
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCusInfo")
    protected CusBaseClientDto queryCusByCertCode(@RequestBody String certCode) {
        CusBase cusBase = cusBaseService.queryCusInfoByCertCode(certCode);
        CusBaseClientDto cusBaseClientDto = new CusBaseClientDto();
        BeanUtils.beanCopy(cusBase,cusBaseClientDto);
        return cusBaseClientDto;
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CusBase> create(@RequestBody CusBase cusBase) throws URISyntaxException {
        cusBaseService.insert(cusBase);
        return new ResultDto<CusBase>(cusBase);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusBase cusBase) throws URISyntaxException {
        int result = cusBaseService.update(cusBase);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: updateSelective
     * @函数描述: 更新非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CusBase cusBase) {
        int rtnData = cusBaseService.updateSelective(cusBase);
        return new ResultDto<Integer>(rtnData);
    }

    /**
     * @param cusBase
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author 王玉坤
     * @date 2021/11/14 23:09
     * @version 1.0.0
     * @desc 更新客户信息（上面的方法有坑）
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/updateSelectivenew")
    protected ResultDto<Integer> updateSelectivenew(@RequestBody CusBase cusBase) {
        int rtnData = cusBaseService.updateByPrimaryKeySelective(cusBase);
        return new ResultDto<Integer>(rtnData);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cusId}")
    protected ResultDto<Integer> delete(@PathVariable("cusId") String cusId) {
        int result = cusBaseService.deleteByPrimaryKey(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusBaseService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @GetMapping("/getCusInfoByCusId/{cusId}")
    protected ResultDto<Map> getCusInfoByCusId(@PathVariable("cusId") String cusId){

        return new ResultDto<>(cusBaseService.getCusInfoByCusId(cusId));
    }

    /**
     *
     * @param cusInfoQueryDto
     * @return
     */
    @PostMapping("/querycusinfo")
    public List<CusBaseDto> queryCusInfo(@RequestBody CusInfoQueryDto cusInfoQueryDto) {
        List<CusBase> cusBaseList = cusBaseService.queryCusInfo(cusInfoQueryDto.getCusIds());
        return (List<CusBaseDto>) BeanUtils.beansCopy(cusBaseList, CusBaseDto.class);
    }

    /**
     *根据证件号查询客户信息,如果是本行客户则返回,否则去ecif查询,ecif没有去ecif开户,后数据本地落库
     * @param cusBase
     * @return

    @PostMapping("/queryCusByCertCodeToEcif")
    public ResultDto<CusBase> queryCusByCertCodeToEcif(@RequestBody CusBase cusBase) {
        CusBase result = cusBaseService.queryCusByCertCodeToEcif(cusBase);
        return new ResultDto<CusBase>(result);
    }
     */
    /**
     * @param
     * @函数名称:selectBaseIndiv
     * @函数描述:查询对象列表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/baseIndiv")
    protected ResultDto<List<CusBaseDto>> selectBaseIndiv(@RequestBody QueryModel queryModel) {
        List<CusBaseDto> list = cusBaseService.selectBaseIndiv(queryModel);
        return new ResultDto<List<CusBaseDto>>(list);
    }

    @PostMapping("/baseIndivByManageId")
    protected ResultDto<List<CusBaseDto>> baseIndivByManageId(@RequestBody QueryModel queryModel) {
        List<CusBaseDto> list = cusBaseService.selectBaseIndiv(queryModel);
        return new ResultDto<List<CusBaseDto>>(list);
    }
    /**
     * @param
     * @函数名称:selectBaseCrop
     * @函数描述:查询对象列表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/baseCrop")
    protected ResultDto<List<CusBaseDto>> selectBaseCrop(@RequestBody QueryModel queryModel) {
        List<CusBaseDto> list = cusBaseService.selectBaseCrop(queryModel);
        return new ResultDto<List<CusBaseDto>>(list);
    }

    /**
     * @param
     * @函数名称:selectBaseCrop
     * @函数描述:查询对象列表
     * @参数与返回说明:
     * @算法描述:客户信息列表查询-权限控制
     */
    @PostMapping("/baseCropByManageId")
    protected ResultDto<List<CusBaseDto>> baseCropByManageId(@RequestBody QueryModel queryModel) {
        List<CusBaseDto> list = cusBaseService.selectBaseCrop(queryModel);
        return new ResultDto<List<CusBaseDto>>(list);
    }

    /**
     * @param
     * @函数名称:selectBaseCrop
     * @函数描述:查询对象列表
     * @参数与返回说明:
     * @算法描述:客户信息列表查询-权限控制
     */
    @PostMapping("/baseCropByManageIdForList")
    protected ResultDto<List<CusBaseDto>> baseCropByManageIdForList(@RequestBody QueryModel queryModel) {
        List<CusBaseDto> list = cusBaseService.selectBaseCrop(queryModel);
        return new ResultDto<List<CusBaseDto>>(list);
    }
    /**
     * @param
     * @函数名称:selectBaseCrop
     * @函数描述:查询对象列表
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/baseCropDis")
    protected ResultDto<List<CusBaseDto>> selectBaseCropDis(@RequestBody QueryModel queryModel) {
        List<CusBaseDto> list = cusBaseService.selectBaseCropDis(queryModel);
        return new ResultDto<List<CusBaseDto>>(list);
    }

    /**
     * @Description:根据客户号删除客户在cusBase表中的信息(逻辑删除),返回删除成功标志
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:15
     * @param cusId:客户号
     * @return: cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     **/
    @PostMapping("/deleteCusBaseByCusId")
    protected  ResultDto<Boolean> deleteCusBaseByCusId(@RequestBody String cusId){
        boolean deleteFlag= cusBaseService.deleteCusBaseByCusId(cusId);
        return new ResultDto<>(deleteFlag);
    }

    /**
     * @param cusUpdateInitLoanDateDto
     * @函数名称:updateinintloandate
     * @函数描述:首笔授信申请审批通过，更新个人客户信息表建立信贷关系时间
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "首笔授信申请审批通过，更新个人客户信息表建立信贷关系时间")
    @PostMapping("/updateinitloandate")
    protected ResultDto<Integer> updateInitLoanDate(@RequestBody CusUpdateInitLoanDateDto cusUpdateInitLoanDateDto) {
        int ret = cusBaseService.updateInitLoanDate(cusUpdateInitLoanDateDto);
        return new ResultDto<Integer> (ret);
    }

    /**
     * @param queryModel
     * @return
     * @author wzy
     * @date 2021/6/18 14:41
     * @version 1.0.0
     * @desc  根据条件查询关联个人客户列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "根据条件查询关联个人客户列表")
    @PostMapping("/selectbycondition")
    protected ResultDto<List<CusBaseDto>> selectByCondition(@RequestBody QueryModel queryModel) {
        List<CusBaseDto> list = cusBaseService.selectByCondition(queryModel);
        return new ResultDto<List<CusBaseDto>>(list);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/8/4 15:18
     * @注释 增享贷查询客户额外数据
     */
    @ApiOperation(value = "根据客户号查询客户额外补充数据")
    @PostMapping("/selectbycusidforzxd")
    protected ResultDto<CusForZxdDto> selectbycusidforzxd(@RequestBody String cusId) {
        CusForZxdDto cusForZxdDto = cusBaseService.selectbycusidforzxd(cusId);
        return new ResultDto<>(cusForZxdDto);

    }

    /**
     * 根据客户经理号获取其管户客户号
     * @author jijian_yx
     * @date 2021/8/13 21:06
     **/
    @ApiOperation(value = "根据客户经理号获取其管户客户号")
    @PostMapping("/queryCusIdByManagerId")
    protected ResultDto<List<String>> queryCusIdByManagerId(@RequestBody String managerId){
        List<String> result = cusBaseService.queryCusIdByManagerId(managerId);
        return new ResultDto<>(result);
    }

    /**
     * 获取当前登录人机构及下属机构客户
     * @author jijian_yx
     * @date 2021/8/30 17:00
     **/
    @PostMapping("/queryCusByLogin")
    protected ResultDto<List<CusBase>> queryCusByLogin(@RequestBody QueryModel queryModel){
        List<CusBase> result = cusBaseService.queryCusByLogin(queryModel);
        return new ResultDto<>(result);
    }

    /**
     * @param cusbase
     * @return ResultDto
     * @author wzy
     * @date 2021/8/23 9:59
     * @version 1.0.0
     * @desc  根据证件号查询客户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/querycusbycertcode")
    protected ResultDto<CusBaseClientDto> queryCusByCertCode(@RequestBody CusBase cusbase) {
        CusBase cusBase = cusBaseService.queryCusInfoByCertCode(cusbase.getCertCode());
        CusBaseClientDto cusBaseClientDto = new CusBaseClientDto();
        BeanUtils.beanCopy(cusBase,cusBaseClientDto);
        return new ResultDto<>(cusBaseClientDto);
    }

    /**
     * @方法名称：queryCusBaseByCusId
     * @方法描述：
     * @创建人：liuquan
     * 根据客户编号查询信息插入被告人信息表
     */
    @PostMapping("/queryCusBaseByCusId")
    protected ResultDto<CusBase> queryCusBaseByCusId(@RequestBody String cusId) {
        CusBase cusBase = cusBaseService.queryCusBaseByCusId(cusId);
        return new ResultDto<>(cusBase);
    }
    /**
     * @param
     * @函数名称:selectBaseCrop
     * @函数描述:查询对象列表
     * @参数与返回说明:
     * @算法描述:
     * 创建者：周茂伟
     */
    @PostMapping("/queryCusBaseList")
    protected ResultDto<List<CusBaseDto>> queryCusBaseList(@RequestBody QueryModel queryModel) {
        List<CusBaseDto> list = cusBaseService.queryCusBaseList(queryModel);
        return new ResultDto<List<CusBaseDto>>(list);
    }

    /**
     * 根据客户号获取年份集合
     * @author jijian_yx
     * @date 2021/9/27 15:36
     **/
    @PostMapping("/getacctimelist")
    protected ResultDto<List<String>> getAccTimeList(@RequestBody String cusId){
        List<String> result = cusBaseService.getAccTimeList(cusId);
        return new ResultDto<>(result);
    }

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     * @author jijian_yx
     * @date 2021/9/28 0:05
     **/
    @PostMapping("/queryacclistbydoctypeandyear")
    protected ResultDto<List<DocAccSearchDto>> queryAccListByDocTypeAndYear(@RequestBody QueryModel queryModel){
        List<DocAccSearchDto> result = cusBaseService.queryAccListByDocTypeAndYear(queryModel);
        return new ResultDto<>(result);
    }

    /**
     * @方法名称：queryCusBaseCropByCusId
     * @方法描述：
     * @创建人：liuquan
     * 根据客户编号查询信息插入被告人信息表
     * @周茂伟
     */
    @PostMapping("/queryCusBaseCropByCusId")
    protected ResultDto<CusBaseDto> queryCusBaseCropByCusId(@RequestBody String cusId) {
        CusBaseDto cusBase = cusBaseService.queryCusBaseCropByCusId(cusId);
        return new ResultDto<>(cusBase);
    }

    /**
     * @方法名称：queryCusBaseByCusId
     * @方法描述：
     * @创建人：liuquan
     * 根据客户编号查询信息插入被告人信息表
     * @周茂伟
     */
    @PostMapping("/queryCusBaseIndivByCusId")
    protected ResultDto<CusBaseDto> queryCusBaseIndivByCusId(@RequestBody String cusId) {
        CusBaseDto cusBase = cusBaseService.queryCusBaseIndivByCusId(cusId);
        return new ResultDto<>(cusBase);
    }
}
