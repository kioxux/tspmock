package cn.com.yusys.yusp.service.server.cmiscus0018;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusLstZxd;
import cn.com.yusys.yusp.dto.BizCusLstZxdDto;
import cn.com.yusys.yusp.dto.CusLstZxdDto;
import cn.com.yusys.yusp.dto.OperationRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.CmisBizClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 业务逻辑处理类：更新增享贷白名单
 *
 * @author xuchao
 * @version 1.0
 * @since 2021年6月9日
 */
@Service
public class Cmiscus0018Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmiscus0018Service.class);

    @Autowired
    private CmisBizClientService cmisBizClientService;

    /**
     * 更新增享贷白名单
     * @param cusLstZxdDto
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public OperationRespDto cmiscus0018(CusLstZxdDto cusLstZxdDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0016.key, DscmsEnum.TRADE_CODE_CMISCUS0016.value, cusLstZxdDto.toString());
        OperationRespDto operationRespDto = new OperationRespDto();
        try {
            //CusLstZxd cusLstZxd = new CusLstZxd();
            //BeanUtils.copyProperties(cusLstZxdDto,cusLstZxd);
            BizCusLstZxdDto bizCusLstZxdDto = new BizCusLstZxdDto();
            BeanUtils.copyProperties(cusLstZxdDto, bizCusLstZxdDto);
            String repayTerm = cusLstZxdDto.getRepayTerm();
            String overdueCount = cusLstZxdDto.getOverdueCount();
            String memberOverdueCount = cusLstZxdDto.getMemberOverdueCount();
            bizCusLstZxdDto.setRepayTerm(Integer.valueOf(repayTerm == null ? null : repayTerm));
            bizCusLstZxdDto.setOverdueCount(Integer.valueOf(overdueCount == null ? null : overdueCount));
            bizCusLstZxdDto.setMemberOverdueCount(Integer.valueOf(memberOverdueCount == null ? null : memberOverdueCount));
            cmisBizClientService.updateByPrimaryKeySelective(bizCusLstZxdDto);
            operationRespDto.setOpFlag("S");
            operationRespDto.setOpMsg("更新成功");

        } catch (BeansException e) {
            operationRespDto.setOpFlag("F");
            operationRespDto.setOpMsg("更新失败");
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0016.key, DscmsEnum.TRADE_CODE_CMISCUS0016.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0016.key, DscmsEnum.TRADE_CODE_CMISCUS0016.value, operationRespDto.toString());
        return operationRespDto;
    }
}
