package cn.com.yusys.yusp.service.server.xdkh0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.req.Xdkh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.resp.Xdkh0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CusIndivSocialMapper;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * 接口处理类:查询客户配偶信息
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdkh0004Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdkh0004Service.class);

    @Autowired
    private CusIndivSocialMapper cusIndivSocialMapper;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    /**
     * 查询客户配偶信息
     * @param xdkh0004DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0004DataRespDto getSpouseBaseData(Xdkh0004DataReqDto xdkh0004DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0004.key, DscmsEnum.TRADE_CODE_XDKH0004.value, xdkh0004DataReqDto);
        Xdkh0004DataRespDto result = null;
        try {
            result = Optional.ofNullable(cusIndivSocialMapper.getSpouseBaseData(xdkh0004DataReqDto)).orElse(new Xdkh0004DataRespDto());
            if (StringUtils.isNotBlank(result.getCorreManagerId())) {
                logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, result.getCorreManagerId());
                ResultDto<AdminSmUserDto> resultDto = Optional.ofNullable(adminSmUserService.getByLoginCode(result.getCorreManagerId())).orElse(new ResultDto<>());
                logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, JSON.toJSONString(resultDto));
                AdminSmUserDto adminSmUserDto = Optional.ofNullable(resultDto.getData()).orElse(new AdminSmUserDto());
                result.setCorreManagerName(adminSmUserDto.getUserName());
            }
            if (StringUtils.isNotBlank(result.getCorreManagerBrId())) {
                logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETORGBYORGCODE.key, DscmsEnum.TRADE_CODE_GETORGBYORGCODE.value, result.getCorreManagerBrId());
                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(result.getCorreManagerBrId());
                logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETORGBYORGCODE.key, DscmsEnum.TRADE_CODE_GETORGBYORGCODE.value, JSON.toJSONString(resultDto));
                AdminSmOrgDto adminSmOrgDto = Optional.ofNullable(resultDto.getData()).orElse(new AdminSmOrgDto());
                result.setCorreManagerBrName(adminSmOrgDto.getOrgName());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0004.key, DscmsEnum.TRADE_CODE_XDKH0004.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0004.key, DscmsEnum.TRADE_CODE_XDKH0004.value, JSON.toJSONString(result));
        return result;
    }
}
