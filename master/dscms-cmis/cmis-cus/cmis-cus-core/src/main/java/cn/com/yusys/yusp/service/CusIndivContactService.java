/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusIndivContact;
import cn.com.yusys.yusp.repository.mapper.CusIndivContactMapper;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusIndivContactService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-11-16 11:44:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CusIndivContactService {

    @Autowired
    private CusIndivContactMapper cusIndivContactMapper;

    @Autowired
    private CusIndivMapper cusIndivMapper;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusIndivContact selectByPrimaryKey(String cusId) {
        return cusIndivContactMapper.selectByPrimaryKey(cusId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusIndivContact> selectAll(QueryModel model) {
        List<CusIndivContact> records = (List<CusIndivContact>) cusIndivContactMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusIndivContact> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIndivContact> list = cusIndivContactMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusIndivContact record) {
        return cusIndivContactMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusIndivContact record) {
        //同步更新个人客户信息主表的是否农户标识
        CusIndiv cusindiv = new CusIndiv();
        cusindiv.setCusId(record.getCusId());
        cusindiv.setAgriFlg(record.getIsAgri());
        cusIndivMapper.updateByPrimaryKeySelective(cusindiv);
        return cusIndivContactMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusIndivContact record) {
        //同步更新个人客户信息主表的是否农户标识
        CusIndiv cusindiv = new CusIndiv();
        cusindiv.setCusId(record.getCusId());
        cusindiv.setAgriFlg(record.getIsAgri());
        cusIndivMapper.updateByPrimaryKeySelective(cusindiv);
        return cusIndivContactMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusIndivContact record) {
        //同步更新个人客户信息主表的是否农户标识
        CusIndiv cusindiv = new CusIndiv();
        cusindiv.setCusId(record.getCusId());
        cusindiv.setAgriFlg(record.getIsAgri());
        cusIndivMapper.updateByPrimaryKeySelective(cusindiv);
        return cusIndivContactMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId) {
        return cusIndivContactMapper.deleteByPrimaryKey(cusId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusIndivContactMapper.deleteByIds(ids);
    }

    /**
     * 个人客户 概况信息  客户联系信息保存按钮业务逻辑
     * @param cusIndivContact
     * @return
     */
    public int save(CusIndivContact cusIndivContact) {
        CusIndivContact cusIndivContact1 = selectByPrimaryKey(cusIndivContact.getCusId());
        int  result=0;
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 创建日期
        String inputDate= "";
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if(userInfo != null){
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
            // 申请时间
            inputDate = DateUtils.getCurrDateStr();
        }
        if (cusIndivContact1 !=null){
            // 修改人
            cusIndivContact.setUpdId(inputId);
            // 修改机构
            cusIndivContact.setUpdBrId(inputBrId);
            // 修改日期
            cusIndivContact.setUpdDate(inputDate);
            // 修改时间
            cusIndivContact.setUpdateTime(createTime);
            //说明执行得是更新得操作
            result = this.cusIndivContactMapper.updateByPrimaryKeySelective(cusIndivContact);
        }else{
            // 申请人
            cusIndivContact.setInputId(inputId);
            // 申请机构
            cusIndivContact.setInputBrId(inputBrId);
            // 申请时间
            cusIndivContact.setInputDate(inputDate);
            // 创建时间
            cusIndivContact.setCreateTime(createTime);
            // 修改人
            cusIndivContact.setUpdId(inputId);
            // 修改机构
            cusIndivContact.setUpdBrId(inputBrId);
            // 修改日期
            cusIndivContact.setUpdDate(inputDate);
            // 修改时间
            cusIndivContact.setUpdateTime(createTime);
            result = this.cusIndivContactMapper.insertSelective(cusIndivContact);
        }
        return result;
    }
}
