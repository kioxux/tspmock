/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.dto.CusGrpDto;
import cn.com.yusys.yusp.service.CusGrpMemberRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusGrp;
import cn.com.yusys.yusp.service.CusGrpService;

import javax.management.Query;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-08 15:58:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusgrp")
public class CusGrpResource {
    @Autowired
    private CusGrpService cusGrpService;
    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusGrp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusGrp> list = cusGrpService.selectAll(queryModel);
        return new ResultDto<List<CusGrp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusGrp>> index(@RequestBody QueryModel queryModel) {
        List<CusGrp> list = cusGrpService.selectByModel(queryModel);
        return new ResultDto<List<CusGrp>>(list);
    }
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querylist")
    protected ResultDto<List<CusGrp>> querylist(@RequestBody QueryModel queryModel) {
        List<CusGrp> list = cusGrpService.selectByModel(queryModel);
        return new ResultDto<List<CusGrp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{grpNo}")
    protected ResultDto<CusGrp> show(@PathVariable("grpNo") String grpNo) {
        CusGrp cusGrp = cusGrpService.selectByPrimaryKey(grpNo);
        return new ResultDto<CusGrp>(cusGrp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusGrp> create(@RequestBody CusGrp cusGrp) throws URISyntaxException {
        cusGrpService.insert(cusGrp);
        return new ResultDto<CusGrp>(cusGrp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusGrp cusGrp) throws URISyntaxException {
        int result = cusGrpService.updateSelective(cusGrp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{grpNo}")
    protected ResultDto<Integer> delete(@PathVariable("grpNo") String grpNo) {
        int result = cusGrpService.deleteByPrimaryKey(grpNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusGrpService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:集团客户查询
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @算法描述:
     * @创建人：周茂伟
     */
    @PostMapping("/selectCusGrpList")
    protected ResultDto<List<CusGrp>> selectCusGrpList(@RequestBody QueryModel queryModel) {
        List<CusGrp> list = cusGrpService.selectByModel(queryModel);
        return new ResultDto<List<CusGrp>>(list);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @PostMapping("/selectCusGrp")
    protected ResultDto<CusGrp> selectCusGrp(@Validated @RequestBody CusGrp cusGrp) {
        CusGrp cusGrps = cusGrpService.selectByPrimaryKey(cusGrp.getGrpNo());
        return new ResultDto<CusGrp>(cusGrps);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbygrpno")
    protected ResultDto<CusGrp> selectByPrimaryKey(@RequestBody String grpNo) {
        CusGrp cusGrp = cusGrpService.selectByPrimaryKey(grpNo);
        return new ResultDto<CusGrp>(cusGrp);
    }

    /**
     * 根据集团号查询集团信息
     * @param grpNo
     * @return
     */
    @PostMapping("/selectCusGrpDtoByGrpNo")
    protected ResultDto<CusGrpDto> selectCusGrpDtoByGrpNo(@RequestBody String grpNo) {
        CusGrpDto cusGrpDto = cusGrpService.selectCusGrpDtoByGrpNo(grpNo);
        return new ResultDto<CusGrpDto>(cusGrpDto);
    }

    /**
     * 根据集团号查询当前登录人是否可以查看该条记录
     * @param cusGrp
     * @return
     */
    @PostMapping("/selectManagerByGrpNo")
    protected boolean selectManagerByGrpNo(@RequestBody CusGrp cusGrp) {
        CusGrp cusGrpInfo = cusGrpService.selectByPrimaryKey(cusGrp.getGrpNo());
        if(cusGrp.getManagerId().equals(cusGrpInfo.getManagerId())){
            return true;
        }else{
            QueryModel qm = new QueryModel();
            qm.getCondition().put("grpNo",cusGrp.getGrpNo());
            List<CusGrpMemberRel> cusGrpMemberRelList = cusGrpMemberRelService.selectByModel(qm);
            for (CusGrpMemberRel cusGrpMemberRel:cusGrpMemberRelList) {
                if(cusGrp.getManagerId().equals(cusGrpMemberRel.getManagerId())){
                    return true;
                }
            }
        }
        return false;
    }
}
