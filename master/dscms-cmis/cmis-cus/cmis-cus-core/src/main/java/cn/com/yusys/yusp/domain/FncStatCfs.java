/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncStatCfs
 * @类描述: fnc_stat_cfs数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 14:38:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "fnc_stat_cfs")
public class FncStatCfs extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户代码 **/
	@Id
	@Column(name = "cus_id")
	private String cusId;
	
	/** 报表口径STD_ZB_FNC_STYLE **/
	@Id
	@Column(name = "stat_style")
	private String statStyle;
	
	/** 报表年 **/
	@Id
	@Column(name = "stat_year")
	private String statYear;
	
	/** 项目编号 **/
	@Id
	@Column(name = "stat_item_id")
	private String statItemId;
	
	/** 一月期初数 **/
	@Column(name = "stat_init_amt1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt1;
	
	/** 一月期末数 **/
	@Column(name = "stat_end_amt1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt1;
	
	/** 二月期初数 **/
	@Column(name = "stat_init_amt2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt2;
	
	/** 二月期末数 **/
	@Column(name = "stat_end_amt2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt2;
	
	/** 三月期初数 **/
	@Column(name = "stat_init_amt3", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt3;
	
	/** 三月期末数 **/
	@Column(name = "stat_end_amt3", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt3;
	
	/** 四月期初数 **/
	@Column(name = "stat_init_amt4", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt4;
	
	/** 四月期末数 **/
	@Column(name = "stat_end_amt4", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt4;
	
	/** 五月期初数 **/
	@Column(name = "stat_init_amt5", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt5;
	
	/** 五月期末数 **/
	@Column(name = "stat_end_amt5", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt5;
	
	/** 六月期初数 **/
	@Column(name = "stat_init_amt6", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt6;
	
	/** 六月期末数 **/
	@Column(name = "stat_end_amt6", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt6;
	
	/** 七月期初数 **/
	@Column(name = "stat_init_amt7", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt7;
	
	/** 七月期末数 **/
	@Column(name = "stat_end_amt7", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt7;
	
	/** 八月期初数 **/
	@Column(name = "stat_init_amt8", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt8;
	
	/** 八月期末数 **/
	@Column(name = "stat_end_amt8", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt8;
	
	/** 九月期初数 **/
	@Column(name = "stat_init_amt9", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt9;
	
	/** 九月期末数 **/
	@Column(name = "stat_end_amt9", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt9;
	
	/** 十月期初数 **/
	@Column(name = "stat_init_amt10", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt10;
	
	/** 十月期末数 **/
	@Column(name = "stat_end_amt10", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt10;
	
	/** 十一月期初数 **/
	@Column(name = "stat_init_amt11", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt11;
	
	/** 十一月期末数 **/
	@Column(name = "stat_end_amt11", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt11;
	
	/** 十二月期初数 **/
	@Column(name = "stat_init_amt12", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt12;
	
	/** 十二月期末数 **/
	@Column(name = "stat_end_amt12", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmt12;
	
	/** 一季度期初数 **/
	@Column(name = "stat_init_amt_q1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtQ1;
	
	/** 一季度期末数 **/
	@Column(name = "stat_end_amt_q1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmtQ1;
	
	/** 二季度期初数 **/
	@Column(name = "stat_init_amt_q2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtQ2;
	
	/** 二季度期末数 **/
	@Column(name = "stat_end_amt_q2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmtQ2;
	
	/** 三季度期初数 **/
	@Column(name = "stat_init_amt_q3", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtQ3;
	
	/** 三季度期末数 **/
	@Column(name = "stat_end_amt_q3", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmtQ3;
	
	/** 四季度期初数 **/
	@Column(name = "stat_init_amt_q4", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtQ4;
	
	/** 四季度期末数 **/
	@Column(name = "stat_end_amt_q4", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmtQ4;
	
	/** 上半年期初数 **/
	@Column(name = "stat_init_amt_y1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtY1;
	
	/** 上半年期末数 **/
	@Column(name = "stat_end_amt_y1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmtY1;
	
	/** 下半年期初数 **/
	@Column(name = "stat_init_amt_y2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtY2;
	
	/** 下半年期末数 **/
	@Column(name = "stat_end_amt_y2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmtY2;
	
	/** 年期初数 **/
	@Column(name = "stat_init_amt_y", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtY;
	
	/** 年期末数 **/
	@Column(name = "stat_end_amt_y", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statEndAmtY;
	
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param statStyle
	 */
	public void setStatStyle(String statStyle) {
		this.statStyle = statStyle;
	}
	
    /**
     * @return statStyle
     */
	public String getStatStyle() {
		return this.statStyle;
	}
	
	/**
	 * @param statYear
	 */
	public void setStatYear(String statYear) {
		this.statYear = statYear;
	}
	
    /**
     * @return statYear
     */
	public String getStatYear() {
		return this.statYear;
	}
	
	/**
	 * @param statItemId
	 */
	public void setStatItemId(String statItemId) {
		this.statItemId = statItemId;
	}
	
    /**
     * @return statItemId
     */
	public String getStatItemId() {
		return this.statItemId;
	}
	
	/**
	 * @param statInitAmt1
	 */
	public void setStatInitAmt1(java.math.BigDecimal statInitAmt1) {
		this.statInitAmt1 = statInitAmt1;
	}
	
    /**
     * @return statInitAmt1
     */
	public java.math.BigDecimal getStatInitAmt1() {
		return this.statInitAmt1;
	}
	
	/**
	 * @param statEndAmt1
	 */
	public void setStatEndAmt1(java.math.BigDecimal statEndAmt1) {
		this.statEndAmt1 = statEndAmt1;
	}
	
    /**
     * @return statEndAmt1
     */
	public java.math.BigDecimal getStatEndAmt1() {
		return this.statEndAmt1;
	}
	
	/**
	 * @param statInitAmt2
	 */
	public void setStatInitAmt2(java.math.BigDecimal statInitAmt2) {
		this.statInitAmt2 = statInitAmt2;
	}
	
    /**
     * @return statInitAmt2
     */
	public java.math.BigDecimal getStatInitAmt2() {
		return this.statInitAmt2;
	}
	
	/**
	 * @param statEndAmt2
	 */
	public void setStatEndAmt2(java.math.BigDecimal statEndAmt2) {
		this.statEndAmt2 = statEndAmt2;
	}
	
    /**
     * @return statEndAmt2
     */
	public java.math.BigDecimal getStatEndAmt2() {
		return this.statEndAmt2;
	}
	
	/**
	 * @param statInitAmt3
	 */
	public void setStatInitAmt3(java.math.BigDecimal statInitAmt3) {
		this.statInitAmt3 = statInitAmt3;
	}
	
    /**
     * @return statInitAmt3
     */
	public java.math.BigDecimal getStatInitAmt3() {
		return this.statInitAmt3;
	}
	
	/**
	 * @param statEndAmt3
	 */
	public void setStatEndAmt3(java.math.BigDecimal statEndAmt3) {
		this.statEndAmt3 = statEndAmt3;
	}
	
    /**
     * @return statEndAmt3
     */
	public java.math.BigDecimal getStatEndAmt3() {
		return this.statEndAmt3;
	}
	
	/**
	 * @param statInitAmt4
	 */
	public void setStatInitAmt4(java.math.BigDecimal statInitAmt4) {
		this.statInitAmt4 = statInitAmt4;
	}
	
    /**
     * @return statInitAmt4
     */
	public java.math.BigDecimal getStatInitAmt4() {
		return this.statInitAmt4;
	}
	
	/**
	 * @param statEndAmt4
	 */
	public void setStatEndAmt4(java.math.BigDecimal statEndAmt4) {
		this.statEndAmt4 = statEndAmt4;
	}
	
    /**
     * @return statEndAmt4
     */
	public java.math.BigDecimal getStatEndAmt4() {
		return this.statEndAmt4;
	}
	
	/**
	 * @param statInitAmt5
	 */
	public void setStatInitAmt5(java.math.BigDecimal statInitAmt5) {
		this.statInitAmt5 = statInitAmt5;
	}
	
    /**
     * @return statInitAmt5
     */
	public java.math.BigDecimal getStatInitAmt5() {
		return this.statInitAmt5;
	}
	
	/**
	 * @param statEndAmt5
	 */
	public void setStatEndAmt5(java.math.BigDecimal statEndAmt5) {
		this.statEndAmt5 = statEndAmt5;
	}
	
    /**
     * @return statEndAmt5
     */
	public java.math.BigDecimal getStatEndAmt5() {
		return this.statEndAmt5;
	}
	
	/**
	 * @param statInitAmt6
	 */
	public void setStatInitAmt6(java.math.BigDecimal statInitAmt6) {
		this.statInitAmt6 = statInitAmt6;
	}
	
    /**
     * @return statInitAmt6
     */
	public java.math.BigDecimal getStatInitAmt6() {
		return this.statInitAmt6;
	}
	
	/**
	 * @param statEndAmt6
	 */
	public void setStatEndAmt6(java.math.BigDecimal statEndAmt6) {
		this.statEndAmt6 = statEndAmt6;
	}
	
    /**
     * @return statEndAmt6
     */
	public java.math.BigDecimal getStatEndAmt6() {
		return this.statEndAmt6;
	}
	
	/**
	 * @param statInitAmt7
	 */
	public void setStatInitAmt7(java.math.BigDecimal statInitAmt7) {
		this.statInitAmt7 = statInitAmt7;
	}
	
    /**
     * @return statInitAmt7
     */
	public java.math.BigDecimal getStatInitAmt7() {
		return this.statInitAmt7;
	}
	
	/**
	 * @param statEndAmt7
	 */
	public void setStatEndAmt7(java.math.BigDecimal statEndAmt7) {
		this.statEndAmt7 = statEndAmt7;
	}
	
    /**
     * @return statEndAmt7
     */
	public java.math.BigDecimal getStatEndAmt7() {
		return this.statEndAmt7;
	}
	
	/**
	 * @param statInitAmt8
	 */
	public void setStatInitAmt8(java.math.BigDecimal statInitAmt8) {
		this.statInitAmt8 = statInitAmt8;
	}
	
    /**
     * @return statInitAmt8
     */
	public java.math.BigDecimal getStatInitAmt8() {
		return this.statInitAmt8;
	}
	
	/**
	 * @param statEndAmt8
	 */
	public void setStatEndAmt8(java.math.BigDecimal statEndAmt8) {
		this.statEndAmt8 = statEndAmt8;
	}
	
    /**
     * @return statEndAmt8
     */
	public java.math.BigDecimal getStatEndAmt8() {
		return this.statEndAmt8;
	}
	
	/**
	 * @param statInitAmt9
	 */
	public void setStatInitAmt9(java.math.BigDecimal statInitAmt9) {
		this.statInitAmt9 = statInitAmt9;
	}
	
    /**
     * @return statInitAmt9
     */
	public java.math.BigDecimal getStatInitAmt9() {
		return this.statInitAmt9;
	}
	
	/**
	 * @param statEndAmt9
	 */
	public void setStatEndAmt9(java.math.BigDecimal statEndAmt9) {
		this.statEndAmt9 = statEndAmt9;
	}
	
    /**
     * @return statEndAmt9
     */
	public java.math.BigDecimal getStatEndAmt9() {
		return this.statEndAmt9;
	}
	
	/**
	 * @param statInitAmt10
	 */
	public void setStatInitAmt10(java.math.BigDecimal statInitAmt10) {
		this.statInitAmt10 = statInitAmt10;
	}
	
    /**
     * @return statInitAmt10
     */
	public java.math.BigDecimal getStatInitAmt10() {
		return this.statInitAmt10;
	}
	
	/**
	 * @param statEndAmt10
	 */
	public void setStatEndAmt10(java.math.BigDecimal statEndAmt10) {
		this.statEndAmt10 = statEndAmt10;
	}
	
    /**
     * @return statEndAmt10
     */
	public java.math.BigDecimal getStatEndAmt10() {
		return this.statEndAmt10;
	}
	
	/**
	 * @param statInitAmt11
	 */
	public void setStatInitAmt11(java.math.BigDecimal statInitAmt11) {
		this.statInitAmt11 = statInitAmt11;
	}
	
    /**
     * @return statInitAmt11
     */
	public java.math.BigDecimal getStatInitAmt11() {
		return this.statInitAmt11;
	}
	
	/**
	 * @param statEndAmt11
	 */
	public void setStatEndAmt11(java.math.BigDecimal statEndAmt11) {
		this.statEndAmt11 = statEndAmt11;
	}
	
    /**
     * @return statEndAmt11
     */
	public java.math.BigDecimal getStatEndAmt11() {
		return this.statEndAmt11;
	}
	
	/**
	 * @param statInitAmt12
	 */
	public void setStatInitAmt12(java.math.BigDecimal statInitAmt12) {
		this.statInitAmt12 = statInitAmt12;
	}
	
    /**
     * @return statInitAmt12
     */
	public java.math.BigDecimal getStatInitAmt12() {
		return this.statInitAmt12;
	}
	
	/**
	 * @param statEndAmt12
	 */
	public void setStatEndAmt12(java.math.BigDecimal statEndAmt12) {
		this.statEndAmt12 = statEndAmt12;
	}
	
    /**
     * @return statEndAmt12
     */
	public java.math.BigDecimal getStatEndAmt12() {
		return this.statEndAmt12;
	}
	
	/**
	 * @param statInitAmtQ1
	 */
	public void setStatInitAmtQ1(java.math.BigDecimal statInitAmtQ1) {
		this.statInitAmtQ1 = statInitAmtQ1;
	}
	
    /**
     * @return statInitAmtQ1
     */
	public java.math.BigDecimal getStatInitAmtQ1() {
		return this.statInitAmtQ1;
	}
	
	/**
	 * @param statEndAmtQ1
	 */
	public void setStatEndAmtQ1(java.math.BigDecimal statEndAmtQ1) {
		this.statEndAmtQ1 = statEndAmtQ1;
	}
	
    /**
     * @return statEndAmtQ1
     */
	public java.math.BigDecimal getStatEndAmtQ1() {
		return this.statEndAmtQ1;
	}
	
	/**
	 * @param statInitAmtQ2
	 */
	public void setStatInitAmtQ2(java.math.BigDecimal statInitAmtQ2) {
		this.statInitAmtQ2 = statInitAmtQ2;
	}
	
    /**
     * @return statInitAmtQ2
     */
	public java.math.BigDecimal getStatInitAmtQ2() {
		return this.statInitAmtQ2;
	}
	
	/**
	 * @param statEndAmtQ2
	 */
	public void setStatEndAmtQ2(java.math.BigDecimal statEndAmtQ2) {
		this.statEndAmtQ2 = statEndAmtQ2;
	}
	
    /**
     * @return statEndAmtQ2
     */
	public java.math.BigDecimal getStatEndAmtQ2() {
		return this.statEndAmtQ2;
	}
	
	/**
	 * @param statInitAmtQ3
	 */
	public void setStatInitAmtQ3(java.math.BigDecimal statInitAmtQ3) {
		this.statInitAmtQ3 = statInitAmtQ3;
	}
	
    /**
     * @return statInitAmtQ3
     */
	public java.math.BigDecimal getStatInitAmtQ3() {
		return this.statInitAmtQ3;
	}
	
	/**
	 * @param statEndAmtQ3
	 */
	public void setStatEndAmtQ3(java.math.BigDecimal statEndAmtQ3) {
		this.statEndAmtQ3 = statEndAmtQ3;
	}
	
    /**
     * @return statEndAmtQ3
     */
	public java.math.BigDecimal getStatEndAmtQ3() {
		return this.statEndAmtQ3;
	}
	
	/**
	 * @param statInitAmtQ4
	 */
	public void setStatInitAmtQ4(java.math.BigDecimal statInitAmtQ4) {
		this.statInitAmtQ4 = statInitAmtQ4;
	}
	
    /**
     * @return statInitAmtQ4
     */
	public java.math.BigDecimal getStatInitAmtQ4() {
		return this.statInitAmtQ4;
	}
	
	/**
	 * @param statEndAmtQ4
	 */
	public void setStatEndAmtQ4(java.math.BigDecimal statEndAmtQ4) {
		this.statEndAmtQ4 = statEndAmtQ4;
	}
	
    /**
     * @return statEndAmtQ4
     */
	public java.math.BigDecimal getStatEndAmtQ4() {
		return this.statEndAmtQ4;
	}
	
	/**
	 * @param statInitAmtY1
	 */
	public void setStatInitAmtY1(java.math.BigDecimal statInitAmtY1) {
		this.statInitAmtY1 = statInitAmtY1;
	}
	
    /**
     * @return statInitAmtY1
     */
	public java.math.BigDecimal getStatInitAmtY1() {
		return this.statInitAmtY1;
	}
	
	/**
	 * @param statEndAmtY1
	 */
	public void setStatEndAmtY1(java.math.BigDecimal statEndAmtY1) {
		this.statEndAmtY1 = statEndAmtY1;
	}
	
    /**
     * @return statEndAmtY1
     */
	public java.math.BigDecimal getStatEndAmtY1() {
		return this.statEndAmtY1;
	}
	
	/**
	 * @param statInitAmtY2
	 */
	public void setStatInitAmtY2(java.math.BigDecimal statInitAmtY2) {
		this.statInitAmtY2 = statInitAmtY2;
	}
	
    /**
     * @return statInitAmtY2
     */
	public java.math.BigDecimal getStatInitAmtY2() {
		return this.statInitAmtY2;
	}
	
	/**
	 * @param statEndAmtY2
	 */
	public void setStatEndAmtY2(java.math.BigDecimal statEndAmtY2) {
		this.statEndAmtY2 = statEndAmtY2;
	}
	
    /**
     * @return statEndAmtY2
     */
	public java.math.BigDecimal getStatEndAmtY2() {
		return this.statEndAmtY2;
	}
	
	/**
	 * @param statInitAmtY
	 */
	public void setStatInitAmtY(java.math.BigDecimal statInitAmtY) {
		this.statInitAmtY = statInitAmtY;
	}
	
    /**
     * @return statInitAmtY
     */
	public java.math.BigDecimal getStatInitAmtY() {
		return this.statInitAmtY;
	}
	
	/**
	 * @param statEndAmtY
	 */
	public void setStatEndAmtY(java.math.BigDecimal statEndAmtY) {
		this.statEndAmtY = statEndAmtY;
	}
	
    /**
     * @return statEndAmtY
     */
	public java.math.BigDecimal getStatEndAmtY() {
		return this.statEndAmtY;
	}


}