/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CusLstGlf;
import cn.com.yusys.yusp.dto.CusLstGlfDto;
import cn.com.yusys.yusp.dto.CusLstGlfGljyyjedDto;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfMapper;
import cn.com.yusys.yusp.vo.CusLstGlfGljyyjedVo;
import cn.com.yusys.yusp.vo.CusLstGlfVo;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstGlfGljyyjed;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfGljyyjedMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstGlfGljyyjedService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstGlfGljyyjedService {

    @Autowired
    private CusLstGlfGljyyjedMapper cusLstGlfGljyyjedMapper;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusLstGlfGljyyjed selectByPrimaryKey(String serno) {
        return cusLstGlfGljyyjedMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusLstGlfGljyyjed> selectAll(QueryModel model) {
        List<CusLstGlfGljyyjed> records = (List<CusLstGlfGljyyjed>) cusLstGlfGljyyjedMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusLstGlfGljyyjed> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstGlfGljyyjed> list = cusLstGlfGljyyjedMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public ResultDto<CusLstGlfGljyyjed> insert(CusLstGlfGljyyjed record) {
        //检查当前证件号码和证件类型是否已经存在
        ResultDto<CusLstGlfGljyyjed> dto = new ResultDto();
        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("relatedPartyCertType", record.getRelatedPartyCertType());
        queryModel.getCondition().put("relatedPartyCertNo", record.getRelatedPartyCertNo());
        List<CusLstGlfGljyyjed> list = cusLstGlfGljyyjedMapper.selectByModel(queryModel);
        if (list != null && list.size() > 0) {
            dto.setCode("1");
            dto.setMessage("当前关联方客户已存在");
            return dto;
        }
        cusLstGlfGljyyjedMapper.insert(record);
        dto.setData(record);
        return dto;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusLstGlfGljyyjed record) {
        return cusLstGlfGljyyjedMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusLstGlfGljyyjed record) {
        return cusLstGlfGljyyjedMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusLstGlfGljyyjed record) {
        return cusLstGlfGljyyjedMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstGlfGljyyjedMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstGlfGljyyjedMapper.deleteByIds(ids);
    }

    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusLstGlfGljyyjedVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param cusLstGlfGljyyjedVoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> cusLstGlfGljyyjedVoList) {
        User userInfo = SessionUtils.getUserInformation();
        List<CusLstGlfGljyyjedVo> cusBatCusLstGlf = (List<CusLstGlfGljyyjedVo>) BeanUtils.beansCopy(cusLstGlfGljyyjedVoList, CusLstGlfGljyyjedVo.class);
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CusLstGlfMapper cusLstGlfMapper = sqlSession.getMapper(CusLstGlfMapper.class);

            for (CusLstGlfGljyyjedVo cusLstGlfGljyyjedVo : cusBatCusLstGlf) {
                CusLstGlfGljyyjed clg = new CusLstGlfGljyyjed();
                BeanUtils.beanCopy(cusLstGlfGljyyjedVo, clg);
                QueryModel qm = new QueryModel();
                qm.getCondition().put("relatedPartyCertNo", clg.getRelatedPartyCertNo());
                qm.getCondition().put("relatedPartyCertType", clg.getRelatedPartyCertType());
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                clg.setInputBrId(userInfo.getOrg().getCode());
                clg.setInputId(userInfo.getLoginCode());
                clg.setInputDate(date);
                clg.setCreateTime(date1);
                clg.setUpdateTime(date1);
                clg.setUpdId(userInfo.getLoginCode());
                clg.setUpdBrId(userInfo.getOrg().getCode());
                List<CusLstGlfGljyyjed> glfList = cusLstGlfGljyyjedMapper.selectByModel(qm);
                if (glfList != null && glfList.size() > 0) {
                    glfList.stream().forEach(ded -> {
                        clg.setSerno(ded.getSerno());
                        cusLstGlfGljyyjedMapper.updateByPrimaryKey(clg);
                    });
                } else {
                    clg.setSerno(UUID.randomUUID().toString());
                    cusLstGlfGljyyjedMapper.insertSelective(clg);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return cusBatCusLstGlf == null ? 0 : cusBatCusLstGlf.size();
    }

    /**
     * 根据客户号查询关联额度信息
     * @param cusId
     * @return
     */
    public CusLstGlfGljyyjedDto queryCusLstGlfGljyyjedDtoByCusId(String cusId){
        return cusLstGlfGljyyjedMapper.queryCusLstGlfGljyyjedDtoByCusId(cusId);
    }

}
