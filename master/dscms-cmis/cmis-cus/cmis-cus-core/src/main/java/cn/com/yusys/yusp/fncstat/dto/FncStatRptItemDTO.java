package cn.com.yusys.yusp.fncstat.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel("财务报表科目")
public class FncStatRptItemDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3278449521971546267L;
	/**
	 * 项目编号
	 */
	@ApiModelProperty("项目编号")
	private String itemId;
	/**
	 * 输入列1
	 */
	@ApiModelProperty("输入列1")
	private String data1;
	/**
	 * 输入列2
	 */
	@ApiModelProperty("输入列2")
	private String data2;
	/**
	 * 输入列3
	 */
	@ApiModelProperty("输入列3")
	private String data3;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}

	public String getData3() {
		return data3;
	}

	public void setData3(String data3) {
		this.data3 = data3;
	}


}
