/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusManaTask;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusCorpMgr;
import cn.com.yusys.yusp.repository.mapper.CusCorpMgrMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpMgrService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-03-22 14:57:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusCorpMgrService {

    @Autowired
    private CusCorpMgrMapper cusCorpMgrMapper;

    @Autowired
    private CusIndivMapper cusIndivMapper;

    @Autowired
    private CusBaseService cusBaseService;

    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    @Autowired
    private Dscms2OuterdataClientService dscms2ImageClientService;

    @Autowired
    private CusManaTaskService cusManaTaskService;

    @Autowired
    private CusLstGlfService cusLstGlfService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusCorpMgr selectByPrimaryKey(String pkId) {
        return cusCorpMgrMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusCorpMgr> selectAll(QueryModel model) {
        List<CusCorpMgr> records = (List<CusCorpMgr>) cusCorpMgrMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusCorpMgr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusCorpMgr> list = cusCorpMgrMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusCorpMgr record) {
        return cusCorpMgrMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusCorpMgr record) {
        return cusCorpMgrMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusCorpMgr record) {
        return cusCorpMgrMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusCorpMgr record) {
        return cusCorpMgrMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusCorpMgrMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusCorpMgrMapper.deleteByIds(ids);
    }

    /**
     * 获取高管信息
     * @param mrgType
     * @param mrgCertCode
     * @return
     */
    public CusCorpMgr selectCusCorpMgrByCodeAndType(String mrgType, String mrgCertCode) {
        return cusCorpMgrMapper.selectCusCorpMgrByCodeAndType(mrgType,mrgCertCode);
    }

    /**
     * @方法名称: getCusCorpMgrMessageByCusId
     * @方法描述: 根据客户编号查询其作为高管的公司信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> getCusCorpMgrMessageByCusId(Map queryMap) {
        return cusCorpMgrMapper.getCusCorpMgrMessageByCusId(queryMap);
    }

    /**
     * @方法名称: getCusCorpMgrMessageByCertCode
     * @方法描述: 根据证件号查询其作为高管的公司信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> getCusCorpMgrMessageByCertCode(@Param("certCode") String certCode) {
        return cusCorpMgrMapper.getCusCorpMgrMessageByCertCode(certCode);
    }

    /**
     * @方法名称: getCusCorpMgrMessageBymrgCertType
     * @方法描述: 根据客户编号查询其作为高管的公司信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> getCusCorpMgrMessageBymrgCertType(Map queryMap) {
        return cusCorpMgrMapper.getCusCorpMgrMessageBymrgCertType(queryMap);
    }

    /**
     * 获取高管
     * @param cusCorpRelationsRequestDto
     * @return
     */
    public CusCorpMgrComDto cuCorpMgrRelations(CusCorpRelationsRequestDto cusCorpRelationsRequestDto) {
        List<CusCorpRelationsDto> cusCorpRelationsDtos = cusCorpMgrMapper.selectCusCorpMgrComDto(cusCorpRelationsRequestDto);
        if(CollectionUtils.isEmpty(cusCorpRelationsDtos) && cusCorpRelationsDtos.size() <= 0){
            return null;
        }

        CusCorpMgrComDto cusCorpMgrComDto = new CusCorpMgrComDto();
        cusCorpMgrComDto.setUpperCusId(cusCorpRelationsRequestDto.getCusId());
        cusCorpMgrComDto.setCusCorpRelationsDtos(cusCorpRelationsDtos);
        return cusCorpMgrComDto;
    }

    /**
     * 查询法人关联高管信息
     * @param cusIdRel
     * @return
     */
    public List<Map<String,String>> selectCusInfoByCusIdRel(String cusIdRel){
        return cusCorpMgrMapper.selectCusInfoByCusIdRel(cusIdRel);
    }

    /**
     * 根据客户编号查询其作为高管的公司信息
     * @param cusId
     * @return
     */
    public List<Map<String,String>> selectCusRelInfoByCusId(String cusId){
        return cusCorpMgrMapper.selectCusRelInfoByCusId(cusId);
    }

    /**
     * 公司客户创建向导
     * 以及Ecif校验
     * @param cusIndivDto
     * @author 徐鑫
     */
    @Transactional
    public CusIndivDto createCusIndiv(CusIndivDto cusIndivDto) {
        if (cusIndivDto != null) {
            // 第一部 查询客户基本信息表，通过证件类型证件号码判断客户是否在信贷系统存在，不存在则继续后面步骤
            CusBase cusBase = cusBaseService.selectByCertCode(cusIndivDto);
            if (cusBase != null) {
                // 客户信息已经存在
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,EcsEnum.E_CUS_INFO_EXIST.value);
            }
            //证件类型为身份证的时候进行身份校验
            String certType = cusIndivDto.getCertType();
            String cusState = "";
            S10501ReqDto s10501ReqDto = new S10501ReqDto();
            s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
            s10501ReqDto.setIdtftp(cusIndivDto.getCertType());//   证件类型
            s10501ReqDto.setIdtfno(cusIndivDto.getCertCode());//   证件号码
            //  StringUtils.EMPTY的实际值待确认 开始
            // 发送客户三要素发送ecfi查询
            ResultDto<S10501RespDto> s10501ResultDto = dscms2EcifClientService.s10501(s10501ReqDto);
            String s10501Code = Optional.ofNullable(s10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String s10501Meesage = Optional.ofNullable(s10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            S10501RespDto s10501RespDto = null;
            if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                //  获取查询结果 todo联调验证交易成功查询失败情况
                s10501RespDto = s10501ResultDto.getData();
                if(s10501RespDto.getListnm() !=null){
                    //cusId=StringUtils.getUUID();
                    List<ListArrayInfo> listArrayInfo =s10501RespDto.getListArrayInfo();
                    for(int i=0;i<listArrayInfo.size();i++){
                        // 客户号
                        cusIndivDto.setCusId(listArrayInfo.get(0).getCustno());
                    }
                }else{
                    // 交易失败
                    // 若ecif系统检验不通过，则发送开户接口进行开户
                    S00102ReqDto s00102ReqDto = new S00102ReqDto();
                    // 客户名称
                    s00102ReqDto.setCustna(cusIndivDto.getCusName());
                    // 证件号码
                    s00102ReqDto.setIdtfno(cusIndivDto.getCertCode());//cusIndivDto.getCertType()
                    // 证件类型
                    s00102ReqDto.setIdtftp("A");
                    // 客户状态
                    s00102ReqDto.setCustst("3");

                    s00102ReqDto.setCtcktg("1");

                    s00102ReqDto.setResult("00");
                    // 发送Ecif开户
                    ResultDto<S00102RespDto> s00102ResultDto = dscms2EcifClientService.s00102(s00102ReqDto);
                    String g00102Code = Optional.ofNullable(s00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String g00102Meesage = Optional.ofNullable(s00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    S00102RespDto s00102RespDto = null;
                    if (Objects.equals(g00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //  获取相关的值并解析
                        s00102RespDto = s00102ResultDto.getData();
                        if(s00102RespDto!=null){
                            cusIndivDto.setCusId(s00102RespDto.getCustno());
                        }
                    } else {
                        //  抛出错误异常
                        throw BizException.error(null,g00102Code,EcsEnum.CUS_INDIV_CREATE_RESULT.value+g00102Meesage);
                    }
                }
            } else {
                //  抛出错误异常
                throw BizException.error(null,s10501Code,EcsEnum.CUS_INDIV_QUERY_RESULT.value+s10501Meesage);
            }
        }else{
            throw BizException.error(null,EcsEnum.CUS_INDIV_CHECK_RESULT01.key,EcsEnum.CUS_INDIV_CHECK_RESULT01.value);
        }
        return cusIndivDto;
    }

    /**
     * 查询是否我行股东
     *
     * @return
     * @paramcusId
     * @author xx
     * @date 2021年4月25日23:49:18
     */
    public int queryCountByCusId(String cusId) {
        int count = 0;
        if (StringUtils.nonEmpty(cusId)) {
            count = cusLstGlfService.selectCount(cusId);
        }
        return count;
    }

    public CusCorpMgrDto getCusCorpMgrByMrgType(String mrgType,String cusId){
        return cusCorpMgrMapper.getCusCorpMgrByMrgType(mrgType,cusId);
    }

    public CusCorpMgrDto getCusCorpMgrByParams(String mrgType,String cusIdRel){
        return cusCorpMgrMapper.getCusCorpMgrByParams(mrgType,cusIdRel);
    }

    /**
     * @方法名称: queryCusCorpMgr
     * @方法描述: 查询高管信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusCorpMgrDto> queryCusCorpMgr(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusCorpMgrDto> list = cusCorpMgrMapper.queryCusCorpMgr(model);
        PageHelper.clearPage();
        return list;
    }
}
