package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusComGrade
 * @类描述: cus_com_grade数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 20:41:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusComGradeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 评级年度 **/
	private String gradeYear;
	
	/** 评级类型:STD_ZB_GRADE_TYPE **/
	private String gradeType;
	
	/** 评级模型:STD_ZB_GRADE_MODE **/
	private String gradeMode;
	
	/** 系统初始等级:STD_ZB_GRADE_RANK **/
	private String sysRank;
	
	/** 调整后等级:STD_ZB_GRADE_RANK **/
	private String adjRank;
	
	/** 建议等级:STD_ZB_GRADE_RANK **/
	private String suggestRank;
	
	/** 最终等级:STD_ZB_GRADE_RANK **/
	private String finalRank;
	
	/** 非零内评生效日期 **/
	private String effDt;
	
	/** 非零内评到期日期 **/
	private String dueDt;
	
	/** 非零内评失效日期 **/
	private String loseEffDt;
	
	/** 非零内评违约概率 **/
	private String pd;
	
	/** 非零内评风险大类 **/
	private String bigriskclass;
	
	/** 非零内评风险中类 **/
	private String midriskclass;
	
	/** 非零内评风险小类 **/
	private String fewriskclass;
	
	/** 非零内评风险划分日期 **/
	private String riskdividedate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param gradeYear
	 */
	public void setGradeYear(String gradeYear) {
		this.gradeYear = gradeYear == null ? null : gradeYear.trim();
	}
	
    /**
     * @return GradeYear
     */	
	public String getGradeYear() {
		return this.gradeYear;
	}
	
	/**
	 * @param gradeType
	 */
	public void setGradeType(String gradeType) {
		this.gradeType = gradeType == null ? null : gradeType.trim();
	}
	
    /**
     * @return GradeType
     */	
	public String getGradeType() {
		return this.gradeType;
	}
	
	/**
	 * @param gradeMode
	 */
	public void setGradeMode(String gradeMode) {
		this.gradeMode = gradeMode == null ? null : gradeMode.trim();
	}
	
    /**
     * @return GradeMode
     */	
	public String getGradeMode() {
		return this.gradeMode;
	}
	
	/**
	 * @param sysRank
	 */
	public void setSysRank(String sysRank) {
		this.sysRank = sysRank == null ? null : sysRank.trim();
	}
	
    /**
     * @return SysRank
     */	
	public String getSysRank() {
		return this.sysRank;
	}
	
	/**
	 * @param adjRank
	 */
	public void setAdjRank(String adjRank) {
		this.adjRank = adjRank == null ? null : adjRank.trim();
	}
	
    /**
     * @return AdjRank
     */	
	public String getAdjRank() {
		return this.adjRank;
	}
	
	/**
	 * @param suggestRank
	 */
	public void setSuggestRank(String suggestRank) {
		this.suggestRank = suggestRank == null ? null : suggestRank.trim();
	}
	
    /**
     * @return SuggestRank
     */	
	public String getSuggestRank() {
		return this.suggestRank;
	}
	
	/**
	 * @param finalRank
	 */
	public void setFinalRank(String finalRank) {
		this.finalRank = finalRank == null ? null : finalRank.trim();
	}
	
    /**
     * @return FinalRank
     */	
	public String getFinalRank() {
		return this.finalRank;
	}
	
	/**
	 * @param effDt
	 */
	public void setEffDt(String effDt) {
		this.effDt = effDt == null ? null : effDt.trim();
	}
	
    /**
     * @return EffDt
     */	
	public String getEffDt() {
		return this.effDt;
	}
	
	/**
	 * @param dueDt
	 */
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt == null ? null : dueDt.trim();
	}
	
    /**
     * @return DueDt
     */	
	public String getDueDt() {
		return this.dueDt;
	}
	
	/**
	 * @param loseEffDt
	 */
	public void setLoseEffDt(String loseEffDt) {
		this.loseEffDt = loseEffDt == null ? null : loseEffDt.trim();
	}
	
    /**
     * @return LoseEffDt
     */	
	public String getLoseEffDt() {
		return this.loseEffDt;
	}
	
	/**
	 * @param pd
	 */
	public void setPd(String pd) {
		this.pd = pd == null ? null : pd.trim();
	}
	
    /**
     * @return Pd
     */	
	public String getPd() {
		return this.pd;
	}
	
	/**
	 * @param bigriskclass
	 */
	public void setBigriskclass(String bigriskclass) {
		this.bigriskclass = bigriskclass == null ? null : bigriskclass.trim();
	}
	
    /**
     * @return Bigriskclass
     */	
	public String getBigriskclass() {
		return this.bigriskclass;
	}
	
	/**
	 * @param midriskclass
	 */
	public void setMidriskclass(String midriskclass) {
		this.midriskclass = midriskclass == null ? null : midriskclass.trim();
	}
	
    /**
     * @return Midriskclass
     */	
	public String getMidriskclass() {
		return this.midriskclass;
	}
	
	/**
	 * @param fewriskclass
	 */
	public void setFewriskclass(String fewriskclass) {
		this.fewriskclass = fewriskclass == null ? null : fewriskclass.trim();
	}
	
    /**
     * @return Fewriskclass
     */	
	public String getFewriskclass() {
		return this.fewriskclass;
	}
	
	/**
	 * @param riskdividedate
	 */
	public void setRiskdividedate(String riskdividedate) {
		this.riskdividedate = riskdividedate == null ? null : riskdividedate.trim();
	}
	
    /**
     * @return Riskdividedate
     */	
	public String getRiskdividedate() {
		return this.riskdividedate;
	}


}