package cn.com.yusys.yusp.service.server.xdkh0026;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0026.req.Xdkh0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0026.resp.List;
import cn.com.yusys.yusp.dto.server.xdkh0026.resp.Xdkh0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0026Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0026Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0026Service.class);

    @Resource
    private CusLstGlfMapper cusLstGlfMapper;

    /**
     * 优企贷、优农贷行内关联人基本信息查询
     *
     * @param xdkh0026DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0026DataRespDto xdkh0026(Xdkh0026DataReqDto xdkh0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026DataReqDto));
        Xdkh0026DataRespDto xdkh0026DataRespDto = new Xdkh0026DataRespDto();
        try {
            java.util.List<List> list = null;
            if (CmisCommonConstants.QUERY_TYPE_INDIV.equals(xdkh0026DataReqDto.getQuery_type())) {
                //按个人查询
                list = cusLstGlfMapper.getIndivRel(xdkh0026DataReqDto);
            } else if (CmisCommonConstants.QUERY_TYPE_COM.equals(xdkh0026DataReqDto.getQuery_type())) {
                list = cusLstGlfMapper.getComRel(xdkh0026DataReqDto);
            }
            xdkh0026DataRespDto.setList(list);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026DataRespDto));
        return xdkh0026DataRespDto;
    }
}
