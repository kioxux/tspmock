/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYndApp;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.domain.CusLstWjkhApp;
import cn.com.yusys.yusp.service.CusLstWjkhAppService;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusLstWjkhAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: pl
 * @创建时间: 2021-04-07 09:50:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstwjkhapp")
public class CusLstWjkhAppResource {
    @Autowired
    private CusLstWjkhAppService cusLstWjkhAppService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstWjkhApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstWjkhApp> list = cusLstWjkhAppService.selectAll(queryModel);
        return new ResultDto<List<CusLstWjkhApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstWjkhApp>> index(QueryModel queryModel) {
        List<CusLstWjkhApp> list = cusLstWjkhAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWjkhApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstWjkhApp>> query(@RequestBody QueryModel queryModel) {
        List<CusLstWjkhApp> list = cusLstWjkhAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWjkhApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstWjkhApp> show(@PathVariable("serno") String serno) {
        CusLstWjkhApp cusLstWjkhApp = cusLstWjkhAppService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstWjkhApp>(cusLstWjkhApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstWjkhApp> create(@RequestBody CusLstWjkhApp cusLstWjkhApp) throws URISyntaxException {
        cusLstWjkhAppService.insert(cusLstWjkhApp);
        return new ResultDto<CusLstWjkhApp>(cusLstWjkhApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstWjkhApp cusLstWjkhApp) throws URISyntaxException {
        int result = cusLstWjkhAppService.update(cusLstWjkhApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstWjkhAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstWjkhAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("暂存:单个对象保存(新增或修改)")
    @PostMapping("/tempSave")
    protected ResultDto<Integer> tempSave(@RequestBody CusLstWjkhApp cusLstWjkhApp) {
        int result = cusLstWjkhAppService.tempSave(cusLstWjkhApp);
        return new ResultDto<Integer>(result);
    }
}
