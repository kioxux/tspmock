/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusComGrade
 * @类描述: cus_com_grade数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 20:41:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_com_grade")
public class CusComGrade extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 评级年度 **/
	@Column(name = "GRADE_YEAR", unique = false, nullable = true, length = 4)
	private String gradeYear;
	
	/** 评级类型:STD_ZB_GRADE_TYPE **/
	@Column(name = "GRADE_TYPE", unique = false, nullable = true, length = 5)
	private String gradeType;
	
	/** 评级模型:STD_ZB_GRADE_MODE **/
	@Column(name = "GRADE_MODE", unique = false, nullable = true, length = 10)
	private String gradeMode;
	
	/** 系统初始等级:STD_ZB_GRADE_RANK **/
	@Column(name = "SYS_RANK", unique = false, nullable = true, length = 5)
	private String sysRank;
	
	/** 调整后等级:STD_ZB_GRADE_RANK **/
	@Column(name = "ADJ_RANK", unique = false, nullable = true, length = 5)
	private String adjRank;
	
	/** 建议等级:STD_ZB_GRADE_RANK **/
	@Column(name = "SUGGEST_RANK", unique = false, nullable = true, length = 5)
	private String suggestRank;
	
	/** 最终等级:STD_ZB_GRADE_RANK **/
	@Column(name = "FINAL_RANK", unique = false, nullable = true, length = 5)
	private String finalRank;
	
	/** 非零内评生效日期 **/
	@Column(name = "EFF_DT", unique = false, nullable = true, length = 10)
	private String effDt;
	
	/** 非零内评到期日期 **/
	@Column(name = "DUE_DT", unique = false, nullable = true, length = 10)
	private String dueDt;
	
	/** 非零内评失效日期 **/
	@Column(name = "LOSE_EFF_DT", unique = false, nullable = true, length = 10)
	private String loseEffDt;
	
	/** 非零内评违约概率 **/
	@Column(name = "PD", unique = false, nullable = true, length = 10)
	private String pd;
	
	/** 非零内评风险大类 **/
	@Column(name = "BIGRISKCLASS", unique = false, nullable = true, length = 10)
	private String bigriskclass;
	
	/** 非零内评风险中类 **/
	@Column(name = "MIDRISKCLASS", unique = false, nullable = true, length = 10)
	private String midriskclass;
	
	/** 非零内评风险小类 **/
	@Column(name = "FEWRISKCLASS", unique = false, nullable = true, length = 10)
	private String fewriskclass;
	
	/** 非零内评风险划分日期 **/
	@Column(name = "RISKDIVIDEDATE", unique = false, nullable = true, length = 20)
	private String riskdividedate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param gradeYear
	 */
	public void setGradeYear(String gradeYear) {
		this.gradeYear = gradeYear;
	}
	
    /**
     * @return gradeYear
     */
	public String getGradeYear() {
		return this.gradeYear;
	}
	
	/**
	 * @param gradeType
	 */
	public void setGradeType(String gradeType) {
		this.gradeType = gradeType;
	}
	
    /**
     * @return gradeType
     */
	public String getGradeType() {
		return this.gradeType;
	}
	
	/**
	 * @param gradeMode
	 */
	public void setGradeMode(String gradeMode) {
		this.gradeMode = gradeMode;
	}
	
    /**
     * @return gradeMode
     */
	public String getGradeMode() {
		return this.gradeMode;
	}
	
	/**
	 * @param sysRank
	 */
	public void setSysRank(String sysRank) {
		this.sysRank = sysRank;
	}
	
    /**
     * @return sysRank
     */
	public String getSysRank() {
		return this.sysRank;
	}
	
	/**
	 * @param adjRank
	 */
	public void setAdjRank(String adjRank) {
		this.adjRank = adjRank;
	}
	
    /**
     * @return adjRank
     */
	public String getAdjRank() {
		return this.adjRank;
	}
	
	/**
	 * @param suggestRank
	 */
	public void setSuggestRank(String suggestRank) {
		this.suggestRank = suggestRank;
	}
	
    /**
     * @return suggestRank
     */
	public String getSuggestRank() {
		return this.suggestRank;
	}
	
	/**
	 * @param finalRank
	 */
	public void setFinalRank(String finalRank) {
		this.finalRank = finalRank;
	}
	
    /**
     * @return finalRank
     */
	public String getFinalRank() {
		return this.finalRank;
	}
	
	/**
	 * @param effDt
	 */
	public void setEffDt(String effDt) {
		this.effDt = effDt;
	}
	
    /**
     * @return effDt
     */
	public String getEffDt() {
		return this.effDt;
	}
	
	/**
	 * @param dueDt
	 */
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}
	
    /**
     * @return dueDt
     */
	public String getDueDt() {
		return this.dueDt;
	}
	
	/**
	 * @param loseEffDt
	 */
	public void setLoseEffDt(String loseEffDt) {
		this.loseEffDt = loseEffDt;
	}
	
    /**
     * @return loseEffDt
     */
	public String getLoseEffDt() {
		return this.loseEffDt;
	}
	
	/**
	 * @param pd
	 */
	public void setPd(String pd) {
		this.pd = pd;
	}
	
    /**
     * @return pd
     */
	public String getPd() {
		return this.pd;
	}
	
	/**
	 * @param bigriskclass
	 */
	public void setBigriskclass(String bigriskclass) {
		this.bigriskclass = bigriskclass;
	}
	
    /**
     * @return bigriskclass
     */
	public String getBigriskclass() {
		return this.bigriskclass;
	}
	
	/**
	 * @param midriskclass
	 */
	public void setMidriskclass(String midriskclass) {
		this.midriskclass = midriskclass;
	}
	
    /**
     * @return midriskclass
     */
	public String getMidriskclass() {
		return this.midriskclass;
	}
	
	/**
	 * @param fewriskclass
	 */
	public void setFewriskclass(String fewriskclass) {
		this.fewriskclass = fewriskclass;
	}
	
    /**
     * @return fewriskclass
     */
	public String getFewriskclass() {
		return this.fewriskclass;
	}
	
	/**
	 * @param riskdividedate
	 */
	public void setRiskdividedate(String riskdividedate) {
		this.riskdividedate = riskdividedate;
	}
	
    /**
     * @return riskdividedate
     */
	public String getRiskdividedate() {
		return this.riskdividedate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}