package cn.com.yusys.yusp.constants;

/**
 * 财务报表相关常量
 *
 * @author zzbankwb473
 */
public abstract class FncStatConstants {
	/**
	 * 简报模板ID
	 */
	public static final String BRIEF_FNC_ID = "fnc_99";
	/**
	 * 简报-资产负债样式ID
	 */
	public static final String BRIEF_BS_STYLEID = "BS_99";
	/**
	 * 简报-利润表样式ID
	 */
	public static final String BRIEF_PL_STYLEID = "PS_99";
	/**
	 * 简报-财务指标样式ID
	 */
	public static final String BRIEF_FI_STYLEID = "FA_99";

	public static final String FNC_STAT_EXCEL_YEAR = "年";

	public static final String FNC_STAT_EXCEL_MONTH = "月";

	public static final String FNC_STAT_EXCEL_UNIT = "编制单位：";
	
	public static final String FNC_STAT_EXCEL_WRITE_DATE = "填报日期：";
	
	public static final String FNC_STAT_EXCEL_UNIT_MONEY = "单位：元";

	/**
	 * 所属报表种类std_zb_fnc_typ枚举
	 *
	 * @author zzbankwb473
	 */
	public enum FncConfTypEnum {
		BS("01", "资产负债表", "FNC_STAT_BS", 0), IS("02", "损益表", "FNC_STAT_IS", 1), CFS("03", "现金流量表", "FNC_STAT_CFS",
				2), FI("04", "财务指标表", "FNC_INDEX_RPT", 3), BSS("13", "资产负债类财务信息", "FNC_STAT_BSS", 4), PSS("14",
						"利润表类财务信息", "FNC_STAT_PSS",
						5), FAS("15", "财务分析指标", "FNC_STAT_FAS", 6), DEPOT("07", "其它财务报表", "FNC_STAT_DE", 7);

		/**
		 * 报表种类代码
		 */
		private String code;
		/**
		 * 报表种类名称
		 */
		private String desc;
		/**
		 * 该类报表数据保存的表名
		 */
		private String tableName;
		/**
		 * 该类报表在fnc_stat_base.stateflg的第几位
		 */
		private int index;

		FncConfTypEnum(String code, String desc, String tableName, int index) {
			this.code = code;
			this.desc = desc;
			this.tableName = tableName;
			this.index = index;
		}

		public static FncConfTypEnum byCode(String code) {
			for (FncConfTypEnum constant : FncConfTypEnum.values()) {
				if (constant.getCode().equals(code)) {
					return constant;
				}
			}
			throw new IllegalArgumentException("No FncConfTypEnum found by code: " + code);
		}

		public String getCode() {
			return code;
		}

		public String getDesc() {
			return desc;
		}

		public String getTableName() {
			return tableName;
		}

		public int getIndex() {
			return index;
		}
	}

	/**
	 * 报表口径STD_ZB_FNC_STYLE枚举
	 *
	 * @author zzbankwb473
	 */
	public enum FncStatStyleEnum {
		BB("1", "本部报表"), HB("2", "合并报表");

		private String code;
		private String desc;

		FncStatStyleEnum(String code, String desc) {
			this.code = code;
			this.desc = desc;
		}

		public static FncStatStyleEnum byCode(String code) {
			for (FncStatStyleEnum constant : FncStatStyleEnum.values()) {
				if (constant.getCode().equals(code)) {
					return constant;
				}
			}
			throw new IllegalArgumentException("No FncStatStyleEnum found by code: " + code);
		}

		public String getCode() {
			return code;
		}

		public String getDesc() {
			return desc;
		}
	}

	/**
	 * 项目编辑方式枚举
	 *
	 * @author zzbankwb473
	 */
	public enum FncStatPrdStyleEnum {
		MONTH("1", "月报"), SEASON("2", "季报"), HALFY("3", "半年报"), YEAR("4", "年报");

		private String code;
		private String desc;

		FncStatPrdStyleEnum(String code, String desc) {
			this.code = code;
			this.desc = desc;
		}

		public static FncStatPrdStyleEnum byCode(String code) {
			for (FncStatPrdStyleEnum constant : FncStatPrdStyleEnum.values()) {
				if (constant.getCode().equals(code)) {
					return constant;
				}
			}
			throw new IllegalArgumentException("No FncStatPrdStyleEnum found by code: " + code);
		}

		public String getCode() {
			return code;
		}

		public String getDesc() {
			return desc;
		}
	}

	/**
	 * @author zzbankwb473
	 */
	public enum FncItemEditTypEnum {
		CONSTANT("0", "常量项"), MANUAL("1", "手工输入项"), CALC("2", "自动计算"), TITLE("3", "标题项");

		private String code;
		private String desc;

		FncItemEditTypEnum(String code, String desc) {
			this.code = code;
			this.desc = desc;
		}

		public static FncItemEditTypEnum byCode(String code) {
			for (FncItemEditTypEnum constant : FncItemEditTypEnum.values()) {
				if (constant.getCode().equals(code)) {
					return constant;
				}
			}
			throw new IllegalArgumentException("No FncItemEditTypEnum found by code: " + code);
		}

		public String getCode() {
			return code;
		}

		public String getDesc() {
			return desc;
		}
	}

	public enum FncTypExcelTitleEnum{
//    	new 
		CONSTANT("0", ""), MANUAL("1", "手工输入项"), CALC("2", "自动计算"), TITLE("3", "标题项");

		private String code;
		private String desc;

		FncTypExcelTitleEnum(String code, String desc) {
			this.code = code;
			this.desc = desc;
		}

		public static FncItemEditTypEnum byCode(String code) {
			for (FncItemEditTypEnum constant : FncItemEditTypEnum.values()) {
				if (constant.getCode().equals(code)) {
					return constant;
				}
			}
			throw new IllegalArgumentException("No FncItemEditTypEnum foun by code: " + code);
		}

		public String getCode() {
			return code;
		}

		public String getDesc() {
			return desc;
		}
	}
}
