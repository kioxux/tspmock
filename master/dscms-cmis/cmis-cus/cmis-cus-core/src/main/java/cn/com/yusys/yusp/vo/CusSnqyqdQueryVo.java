package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusSnqyqd
 * @类描述: cus_snqyqd数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-12 16:49:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusSnqyqdQueryVo extends PageQuery{
	
	/** 地址 **/
	private String address;
	
	/** 是否农户标识 **/
	private String isSn;
	
	
	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}
	
    /**
     * @return Address
     */	
	public String getAddress() {
		return this.address;
	}
	
	/**
	 * @param isSn
	 */
	public void setIsSn(String isSn) {
		this.isSn = isSn == null ? null : isSn.trim();
	}
	
    /**
     * @return IsSn
     */	
	public String getIsSn() {
		return this.isSn;
	}


}