package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusYndListAppBasInfo
 * @类描述: cus_ynd_list_app_bas_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:26:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusYndListAppBasInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 申请日期 **/
	private String appDate;
	
	/** 客户姓名 **/
	private String cusName;
	
	/** 身份证号码 **/
	private String idcardNo;
	
	/** 手机号码 **/
	private String mobileNo;
	
	/** 性别 **/
	private String sex;
	
	/** 学历 **/
	private String edu;
	
	/** 有无子女 **/
	private String isHaveChildren;
	
	/** 居住场所类型 **/
	private String resiType;
	
	/** 家庭地址 **/
	private String familyAddr;
	
	/** 本地居住年限 **/
	private String localResiLmt;
	
	/** 本地户口 **/
	private String localRegist;
	
	/** 经营地址 **/
	private String operAddr;
	
	/** 经营年限 **/
	private String operLmt;
	
	/**  婚姻状况 **/
	private String marStatus;
	
	/** 配偶姓名 **/
	private String spouseName;
	
	/** 配偶身份证号码 **/
	private String spouseIdcardNo;
	
	/** 配偶手机号码 **/
	private String spouseMobileNo;
	
	/** 经办人 **/
	private String huser;
	
	/**  经办机构 **/
	private String handOrg;
	
	/** 影像编号 **/
	private String imageNo;
	
	/** 登记人 **/
	private String inputId;
	
	/**  登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/**  更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新时间 **/
	private String updDate;
	
	/**  操作类型 **/
	private String oprType;
	
	/** 审批状态 **/
	private String approveoveStatus;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}
	
    /**
     * @return AppDate
     */	
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param idcardNo
	 */
	public void setIdcardNo(String idcardNo) {
		this.idcardNo = idcardNo == null ? null : idcardNo.trim();
	}
	
    /**
     * @return IdcardNo
     */	
	public String getIdcardNo() {
		return this.idcardNo;
	}
	
	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo == null ? null : mobileNo.trim();
	}
	
    /**
     * @return MobileNo
     */	
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex == null ? null : sex.trim();
	}
	
    /**
     * @return Sex
     */	
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu == null ? null : edu.trim();
	}
	
    /**
     * @return Edu
     */	
	public String getEdu() {
		return this.edu;
	}
	
	/**
	 * @param isHaveChildren
	 */
	public void setIsHaveChildren(String isHaveChildren) {
		this.isHaveChildren = isHaveChildren == null ? null : isHaveChildren.trim();
	}
	
    /**
     * @return IsHaveChildren
     */	
	public String getIsHaveChildren() {
		return this.isHaveChildren;
	}
	
	/**
	 * @param resiType
	 */
	public void setResiType(String resiType) {
		this.resiType = resiType == null ? null : resiType.trim();
	}
	
    /**
     * @return ResiType
     */	
	public String getResiType() {
		return this.resiType;
	}
	
	/**
	 * @param familyAddr
	 */
	public void setFamilyAddr(String familyAddr) {
		this.familyAddr = familyAddr == null ? null : familyAddr.trim();
	}
	
    /**
     * @return FamilyAddr
     */	
	public String getFamilyAddr() {
		return this.familyAddr;
	}
	
	/**
	 * @param localResiLmt
	 */
	public void setLocalResiLmt(String localResiLmt) {
		this.localResiLmt = localResiLmt == null ? null : localResiLmt.trim();
	}
	
    /**
     * @return LocalResiLmt
     */	
	public String getLocalResiLmt() {
		return this.localResiLmt;
	}
	
	/**
	 * @param localRegist
	 */
	public void setLocalRegist(String localRegist) {
		this.localRegist = localRegist == null ? null : localRegist.trim();
	}
	
    /**
     * @return LocalRegist
     */	
	public String getLocalRegist() {
		return this.localRegist;
	}
	
	/**
	 * @param operAddr
	 */
	public void setOperAddr(String operAddr) {
		this.operAddr = operAddr == null ? null : operAddr.trim();
	}
	
    /**
     * @return OperAddr
     */	
	public String getOperAddr() {
		return this.operAddr;
	}
	
	/**
	 * @param operLmt
	 */
	public void setOperLmt(String operLmt) {
		this.operLmt = operLmt == null ? null : operLmt.trim();
	}
	
    /**
     * @return OperLmt
     */	
	public String getOperLmt() {
		return this.operLmt;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus == null ? null : marStatus.trim();
	}
	
    /**
     * @return MarStatus
     */	
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param spouseName
	 */
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName == null ? null : spouseName.trim();
	}
	
    /**
     * @return SpouseName
     */	
	public String getSpouseName() {
		return this.spouseName;
	}
	
	/**
	 * @param spouseIdcardNo
	 */
	public void setSpouseIdcardNo(String spouseIdcardNo) {
		this.spouseIdcardNo = spouseIdcardNo == null ? null : spouseIdcardNo.trim();
	}
	
    /**
     * @return SpouseIdcardNo
     */	
	public String getSpouseIdcardNo() {
		return this.spouseIdcardNo;
	}
	
	/**
	 * @param spouseMobileNo
	 */
	public void setSpouseMobileNo(String spouseMobileNo) {
		this.spouseMobileNo = spouseMobileNo == null ? null : spouseMobileNo.trim();
	}
	
    /**
     * @return SpouseMobileNo
     */	
	public String getSpouseMobileNo() {
		return this.spouseMobileNo;
	}
	
	/**
	 * @param huser
	 */
	public void setHuser(String huser) {
		this.huser = huser == null ? null : huser.trim();
	}
	
    /**
     * @return Huser
     */	
	public String getHuser() {
		return this.huser;
	}
	
	/**
	 * @param handOrg
	 */
	public void setHandOrg(String handOrg) {
		this.handOrg = handOrg == null ? null : handOrg.trim();
	}
	
    /**
     * @return HandOrg
     */	
	public String getHandOrg() {
		return this.handOrg;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo == null ? null : imageNo.trim();
	}
	
    /**
     * @return ImageNo
     */	
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param approveoveStatus
	 */
	public void setApproveoveStatus(String approveoveStatus) {
		this.approveoveStatus = approveoveStatus == null ? null : approveoveStatus.trim();
	}
	
    /**
     * @return ApproveoveStatus
     */	
	public String getApproveoveStatus() {
		return this.approveoveStatus;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}