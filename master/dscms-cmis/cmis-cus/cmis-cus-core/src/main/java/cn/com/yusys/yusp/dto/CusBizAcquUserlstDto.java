package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CusBizAarApp;
import cn.com.yusys.yusp.domain.CusBizAarCusLst;
import cn.com.yusys.yusp.domain.CusBizAcquUserLst;
import cn.com.yusys.yusp.domain.CusBizAssignApp;

import java.util.List;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/4/26 4:39 下午
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class CusBizAcquUserlstDto {
    private List<CusBizAcquUserLst> list;
    private CusBizAssignApp cusBizAssignApp;

    public List<CusBizAcquUserLst> getList() {
        return list;
    }

    public void setList(List<CusBizAcquUserLst> list) {
        this.list = list;
    }

    public CusBizAssignApp getCusBizAssignApp() {
        return cusBizAssignApp;
    }

    public void setCusBizAssignApp(CusBizAssignApp cusBizAssignApp) {
        this.cusBizAssignApp = cusBizAssignApp;
    }
}
