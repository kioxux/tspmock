package cn.com.yusys.yusp.reportconf.web.rest;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.reportconf.domain.FncConfDefFmt;
import cn.com.yusys.yusp.reportconf.service.FncConfDefFmtService;
//import cn.com.yusys.yusp.trace.annotation.TraceBaggage;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfDefFmtResource
 * @类描述: #资源类
 * @功能描述: 报表配置定义,即样式科目关联配置
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 11:26:38
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/nrcs-cms/fncconfdeffmt")
@Api(tags = "FncConfDefFmtResource", description = "财务报表样式科目配置")
public class FncConfDefFmtResource {
	@Autowired
	private FncConfDefFmtService fncConfDefFmtService;

	/**
	 * 财务报表样式科目配置查询
	 * 
	 * @param styleId
	 *            条件封装
	 * @return
	 */
	@GetMapping("/q/fncconfdeffmt/list")
	@ApiOperation("NRCS603271财务报表样式科目配置查询")
//	@TraceBaggage(functionCode = "NRCS603271")
	protected ResultDto<List<FncConfDefFmt>> queryFncConfDefFmtList(@RequestParam String styleId) {

		return new ResultDto<>(fncConfDefFmtService.queryFncConfDefFmtList(styleId));
	}

	/**
	 * 财务报表样式科目配置新增
	 * 
	 * @param fncConfDefFmt
	 *            实体类
	 * @return
	 */
	@PostMapping("/s/fncconfdeffmt/add")
	@ApiOperation("NRCS603283财务报表样式科目配置新增")
//	@TraceBaggage(functionCode = "NRCS603283")
	protected ResultDto<Integer> addFncConfDefFmt(@RequestBody FncConfDefFmt fncConfDefFmt) {
		return new ResultDto<Integer>(fncConfDefFmtService.addFncConfDefFmt(fncConfDefFmt));
	}

	/**
	 * 财务报表样式科目配置修改
	 * 
	 * @param fncConfDefFmt
	 *            实体类
	 * @return
	 */
	@PostMapping("/s/fncconfdeffmt/update")
	@ApiOperation("NRCS603295财务报表样式科目配置修改")
//	@TraceBaggage(functionCode = "NRCS603295")
	protected ResultDto<Integer> updateFncConfDefFmt(@RequestBody FncConfDefFmt fncConfDefFmt) {
		return new ResultDto<Integer>(fncConfDefFmtService.updateFncConfDefFmt(fncConfDefFmt));
	}

	/**
	 * 财务报表样式科目配置删除
	 * 
	 * @param styleId
	 *            报表样式编号
	 * @param itemId
	 *            项目编号
	 * @return
	 */
	@PostMapping("/s/fncconfdeffmt/delete")
	@ApiOperation("NRCS603304财务报表样式科目配置删除")
//	@TraceBaggage(functionCode = "NRCS603304")
	protected ResultDto<Integer> deleteFncConfDefFmt(@RequestBody HashMap<String, String> map) {
		return new ResultDto<Integer>(fncConfDefFmtService.deleteFncConfDefFmt(map.get("styleId"), map.get("itemId")));
	}

	/**
	 * 财务报表样式科目配置查看 暂时不使用
	 * 
	 * @param styleId
	 * @param itemId
	 * @return
	 */
	/*
	 * @GetMapping("/q/fncconfdeffmt/detail/{}/{}")
	 * 
	 * @ApiOperation("财务报表样式科目配置查看")
	 * 
	 * @TraceBaggage(functionCode = "NRCS603305") protected ResultDto<FncConfDefFmt>
	 * queryFncConfDefFmtByKey(@RequestBody HashMap<String, String> map) { return
	 * new ResultDto<FncConfDefFmt>(
	 * fncConfDefFmtService.queryFncConfDefFmtByKey(map.get("styleId"),
	 * map.get("itemId"))); }
	 */

}
