package cn.com.yusys.yusp.service.server.xdkh0025;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0025.resp.Xdkh0025RespDto4Null;
import cn.com.yusys.yusp.dto.server.xdkh0025.req.Xdkh0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0025.resp.List;
import cn.com.yusys.yusp.dto.server.xdkh0025.resp.Xdkh0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0025Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0025Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0025Service.class);

    @Resource
    private CusCorpMapper cusCorpMapper;

    /**
     * 优企贷、优农贷客户信息查询
     *
     * @param xdkh0025DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0025DataRespDto xdkh0025(Xdkh0025DataReqDto xdkh0025DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025DataReqDto));
        Xdkh0025DataRespDto xdkh0025DataRespDto = new Xdkh0025DataRespDto();

        try {
            if (StringUtils.isEmpty(xdkh0025DataReqDto.getCus_id()) && StringUtils.isEmpty(xdkh0025DataReqDto.getCus_name())) {
                throw BizException.error(null, EcsEnum.ECS040008.key, EcsEnum.ECS040008.value);
            }
            Map param = new HashMap<>();
            if (StringUtils.isNotEmpty(xdkh0025DataReqDto.getCus_id())) {
                String cusIds = xdkh0025DataReqDto.getCus_id();
                java.util.List<String> cusIdList = new ArrayList<>();
                if (cusIds.contains(",")) {
                    String[] bizTypeAry = cusIds.split(",");
                    for (int i = 0; i < bizTypeAry.length; i++) {
                        String cusid = bizTypeAry[i];
                        cusIdList.add(cusid);
                    }
                } else {
                    cusIdList.add(cusIds);
                }
                param.put("cusIdList",cusIdList);
            }
            if (StringUtils.isNotEmpty(xdkh0025DataReqDto.getCus_name())) {
                String cusNames = xdkh0025DataReqDto.getCus_name();
                java.util.List<String> cusNameList = new ArrayList<>();
                if (cusNames.contains(",")) {
                    String[] bizTypeAry = cusNames.split(",");
                    for (int i = 0; i < bizTypeAry.length; i++) {
                        String cusName = bizTypeAry[i];
                        cusNameList.add(cusName);
                    }
                } else {
                    cusNameList.add(cusNames);
                }
                param.put("cusNameList", cusNameList);
            }
            //返回有下滑线接不到数据
            java.util.List<Xdkh0025RespDto4Null> lists = cusCorpMapper.getXdkh0025(param);
            java.util.List<List> listBase = new ArrayList<>();
            if (CollectionUtils.nonEmpty(lists)) {
                listBase = lists.parallelStream().map(xdkh0025RespDto4Null -> {
                    cn.com.yusys.yusp.dto.server.xdkh0025.resp.List respList = new cn.com.yusys.yusp.dto.server.xdkh0025.resp.List();
                    respList.setCus_id(Optional.ofNullable(xdkh0025RespDto4Null.getCusId()).orElse(StringUtils.EMPTY));
                    respList.setCus_name(Optional.ofNullable(xdkh0025RespDto4Null.getCusName()).orElse(StringUtils.EMPTY));
                    respList.setBas_acc_date(Optional.ofNullable(xdkh0025RespDto4Null.getBasAccDate()).orElse(StringUtils.EMPTY));
                    respList.setBas_acc_flg(Optional.ofNullable(xdkh0025RespDto4Null.getBasAccFlg()).orElse(StringUtils.EMPTY));
                    respList.setCom_init_loan_date(Optional.ofNullable(xdkh0025RespDto4Null.getComInitLoanDate()).orElse(StringUtils.EMPTY));
                    respList.setCom_ins_code(Optional.ofNullable(xdkh0025RespDto4Null.getComInsCode()).orElse(StringUtils.EMPTY));
                    return respList;
                }).collect(Collectors.toList());
            }
            xdkh0025DataRespDto.setList(listBase);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025DataReqDto));
        return xdkh0025DataRespDto;
    }
}
