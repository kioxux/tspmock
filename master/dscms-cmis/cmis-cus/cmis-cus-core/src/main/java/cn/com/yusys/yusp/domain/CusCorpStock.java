/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpStock
 * @类描述: cus_corp_stock数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-25 17:54:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_corp_stock")
public class CusCorpStock extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
	private String cusId;
	
	/** 股票代码 **/
	@Column(name = "STK_CODE", unique = false, nullable = false, length = 10)
	private String stkCode;
	
	/** 股票名称 **/
	@Column(name = "STK_NAME", unique = false, nullable = true, length = 100)
	private String stkName;
	
	/** 上市日期 **/
	@Column(name = "STK_MRK_DT", unique = false, nullable = true, length = 10)
	private String stkMrkDt;
	
	/** 上市公司标志
STD_ZB_STOCK_CPRT_FLAG    **/
	@Column(name = "STOCK_CPRT_FLAG", unique = false, nullable = true, length = 5)
	private String stockCprtFlag;
	
	/** 上市地 **/
	@Column(name = "STK_MRK_PLACE", unique = false, nullable = true, length = 5)
	private String stkMrkPlace;
	
	/** 上市地国别 STD_ZB_COUNTRY **/
	@Column(name = "STOCK_AREA_COUNTRY", unique = false, nullable = true, length = 5)
	private String stockAreaCountry;
	
	/** 交易所名称 **/
	@Column(name = "STK_MRK_BRS", unique = false, nullable = true, length = 100)
	private String stkMrkBrs;
	
	/** 首次发行价（元） **/
	@Column(name = "STK_INIT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal stkInitAmt;
	
	/** 股票当前价（元） **/
	@Column(name = "STK_CUR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal stkCurAmt;
	
	/** 股票评估价（元） **/
	@Column(name = "STK_EVA_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal stkEvaAmt;
	
	/** 当前股本总量（万股） **/
	@Column(name = "STK_CAP_QNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal stkCapQnt;
	
	/** 当前流通股量（万股） **/
	@Column(name = "STK_CUR_QNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal stkCurQnt;
	
	/** 是否质押 **/
	@Column(name = "COM_ISORNO_GAGE", unique = false, nullable = true, length = 5)
	private String comIsornoGage;
	
	/** 质押股数 **/
	@Column(name = "COM_GAGE_STK", unique = false, nullable = true, length = 10)
	private Integer comGageStk;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/** 出资类型
STD_ZB_INVT_TYPE **/
	@Column(name = "INVT_TYPE", unique = false, nullable = true, length = 5)
	private String invtType;
	
	/** 国别  STD_ZB_COUNTRY **/
	@Column(name = "COUNTRY", unique = false, nullable = true, length = 5)
	private String country;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param stkCode
	 */
	public void setStkCode(String stkCode) {
		this.stkCode = stkCode;
	}
	
    /**
     * @return stkCode
     */
	public String getStkCode() {
		return this.stkCode;
	}
	
	/**
	 * @param stkName
	 */
	public void setStkName(String stkName) {
		this.stkName = stkName;
	}
	
    /**
     * @return stkName
     */
	public String getStkName() {
		return this.stkName;
	}
	
	/**
	 * @param stkMrkDt
	 */
	public void setStkMrkDt(String stkMrkDt) {
		this.stkMrkDt = stkMrkDt;
	}
	
    /**
     * @return stkMrkDt
     */
	public String getStkMrkDt() {
		return this.stkMrkDt;
	}
	
	/**
	 * @param stockCprtFlag
	 */
	public void setStockCprtFlag(String stockCprtFlag) {
		this.stockCprtFlag = stockCprtFlag;
	}
	
    /**
     * @return stockCprtFlag
     */
	public String getStockCprtFlag() {
		return this.stockCprtFlag;
	}
	
	/**
	 * @param stkMrkPlace
	 */
	public void setStkMrkPlace(String stkMrkPlace) {
		this.stkMrkPlace = stkMrkPlace;
	}
	
    /**
     * @return stkMrkPlace
     */
	public String getStkMrkPlace() {
		return this.stkMrkPlace;
	}
	
	/**
	 * @param stockAreaCountry
	 */
	public void setStockAreaCountry(String stockAreaCountry) {
		this.stockAreaCountry = stockAreaCountry;
	}
	
    /**
     * @return stockAreaCountry
     */
	public String getStockAreaCountry() {
		return this.stockAreaCountry;
	}
	
	/**
	 * @param stkMrkBrs
	 */
	public void setStkMrkBrs(String stkMrkBrs) {
		this.stkMrkBrs = stkMrkBrs;
	}
	
    /**
     * @return stkMrkBrs
     */
	public String getStkMrkBrs() {
		return this.stkMrkBrs;
	}
	
	/**
	 * @param stkInitAmt
	 */
	public void setStkInitAmt(java.math.BigDecimal stkInitAmt) {
		this.stkInitAmt = stkInitAmt;
	}
	
    /**
     * @return stkInitAmt
     */
	public java.math.BigDecimal getStkInitAmt() {
		return this.stkInitAmt;
	}
	
	/**
	 * @param stkCurAmt
	 */
	public void setStkCurAmt(java.math.BigDecimal stkCurAmt) {
		this.stkCurAmt = stkCurAmt;
	}
	
    /**
     * @return stkCurAmt
     */
	public java.math.BigDecimal getStkCurAmt() {
		return this.stkCurAmt;
	}
	
	/**
	 * @param stkEvaAmt
	 */
	public void setStkEvaAmt(java.math.BigDecimal stkEvaAmt) {
		this.stkEvaAmt = stkEvaAmt;
	}
	
    /**
     * @return stkEvaAmt
     */
	public java.math.BigDecimal getStkEvaAmt() {
		return this.stkEvaAmt;
	}
	
	/**
	 * @param stkCapQnt
	 */
	public void setStkCapQnt(java.math.BigDecimal stkCapQnt) {
		this.stkCapQnt = stkCapQnt;
	}
	
    /**
     * @return stkCapQnt
     */
	public java.math.BigDecimal getStkCapQnt() {
		return this.stkCapQnt;
	}
	
	/**
	 * @param stkCurQnt
	 */
	public void setStkCurQnt(java.math.BigDecimal stkCurQnt) {
		this.stkCurQnt = stkCurQnt;
	}
	
    /**
     * @return stkCurQnt
     */
	public java.math.BigDecimal getStkCurQnt() {
		return this.stkCurQnt;
	}
	
	/**
	 * @param comIsornoGage
	 */
	public void setComIsornoGage(String comIsornoGage) {
		this.comIsornoGage = comIsornoGage;
	}
	
    /**
     * @return comIsornoGage
     */
	public String getComIsornoGage() {
		return this.comIsornoGage;
	}
	
	/**
	 * @param comGageStk
	 */
	public void setComGageStk(Integer comGageStk) {
		this.comGageStk = comGageStk;
	}
	
    /**
     * @return comGageStk
     */
	public Integer getComGageStk() {
		return this.comGageStk;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param invtType
	 */
	public void setInvtType(String invtType) {
		this.invtType = invtType;
	}
	
    /**
     * @return invtType
     */
	public String getInvtType() {
		return this.invtType;
	}
	
	/**
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	
    /**
     * @return country
     */
	public String getCountry() {
		return this.country;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}