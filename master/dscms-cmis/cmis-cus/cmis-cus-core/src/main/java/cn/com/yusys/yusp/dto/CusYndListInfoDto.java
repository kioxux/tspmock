package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusYndListInfo
 * @类描述: cus_ynd_list_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:27:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusYndListInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 名单流水号 **/
	private String serno;
	
	/** 客户姓名 **/
	private String cusName;
	
	/** 身份证号码 **/
	private String idcardNo;
	
	/** 手机号码 **/
	private String mobileNo;
	
	/** 性别 **/
	private String sex;
	
	/** 学历 **/
	private String edu;
	
	/** 有无子女 **/
	private String indivMarBaby;
	
	/** 婚姻状况 **/
	private String indivMarSt;
	
	/** 客户经理 **/
	private String managerId;
	
	/** 所属机构 **/
	private String belgOrg;
	
	/** 名单状态 **/
	private String listStatus;
	
	/** 入库日期 **/
	private String storageDate;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新时间 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 经营地址 **/
	private String operAddr;
	
	/** 经营年限 **/
	private String operLmt;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param idcardNo
	 */
	public void setIdcardNo(String idcardNo) {
		this.idcardNo = idcardNo == null ? null : idcardNo.trim();
	}
	
    /**
     * @return IdcardNo
     */	
	public String getIdcardNo() {
		return this.idcardNo;
	}
	
	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo == null ? null : mobileNo.trim();
	}
	
    /**
     * @return MobileNo
     */	
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex == null ? null : sex.trim();
	}
	
    /**
     * @return Sex
     */	
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu == null ? null : edu.trim();
	}
	
    /**
     * @return Edu
     */	
	public String getEdu() {
		return this.edu;
	}
	
	/**
	 * @param indivMarBaby
	 */
	public void setIndivMarBaby(String indivMarBaby) {
		this.indivMarBaby = indivMarBaby == null ? null : indivMarBaby.trim();
	}
	
    /**
     * @return IndivMarBaby
     */	
	public String getIndivMarBaby() {
		return this.indivMarBaby;
	}
	
	/**
	 * @param indivMarSt
	 */
	public void setIndivMarSt(String indivMarSt) {
		this.indivMarSt = indivMarSt == null ? null : indivMarSt.trim();
	}
	
    /**
     * @return IndivMarSt
     */	
	public String getIndivMarSt() {
		return this.indivMarSt;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg == null ? null : belgOrg.trim();
	}
	
    /**
     * @return BelgOrg
     */	
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param listStatus
	 */
	public void setListStatus(String listStatus) {
		this.listStatus = listStatus == null ? null : listStatus.trim();
	}
	
    /**
     * @return ListStatus
     */	
	public String getListStatus() {
		return this.listStatus;
	}
	
	/**
	 * @param storageDate
	 */
	public void setStorageDate(String storageDate) {
		this.storageDate = storageDate == null ? null : storageDate.trim();
	}
	
    /**
     * @return StorageDate
     */	
	public String getStorageDate() {
		return this.storageDate;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param operAddr
	 */
	public void setOperAddr(String operAddr) {
		this.operAddr = operAddr == null ? null : operAddr.trim();
	}
	
    /**
     * @return OperAddr
     */	
	public String getOperAddr() {
		return this.operAddr;
	}
	
	/**
	 * @param operLmt
	 */
	public void setOperLmt(String operLmt) {
		this.operLmt = operLmt == null ? null : operLmt.trim();
	}
	
    /**
     * @return OperLmt
     */	
	public String getOperLmt() {
		return this.operLmt;
	}


}