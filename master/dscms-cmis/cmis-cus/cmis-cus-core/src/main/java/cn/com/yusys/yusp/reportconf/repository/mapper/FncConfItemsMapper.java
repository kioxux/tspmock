package cn.com.yusys.yusp.reportconf.repository.mapper;

import java.util.List;

import cn.com.yusys.yusp.reportconf.domain.FncConfItems;

import cn.com.yusys.yusp.commons.mapper.CommonMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfItemsMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 10:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface FncConfItemsMapper extends CommonMapper<FncConfItems> {
	
	/**
	 * 根据fnc_conf_typ 查询 科目列表，否则全量查询
	 * @param model
	 * @return
	 */
	List<FncConfItems> selectFncConfItemsAllListWithFncConfTyp(QueryModel model);
	
}