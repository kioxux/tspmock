package cn.com.yusys.yusp.service.client.bsp.gaps.idchek;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.req.IdchekReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2GapsClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author xuchao
 * @version 1.0.0
 * @date 2021/6/28 9:51
 * @desc    身份证核查
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class IdchekService {
    private static final Logger logger = LoggerFactory.getLogger(IdchekService.class);
    // 1）注入：BSP封装调用外部数据平台的接口
    @Autowired
    private Dscms2GapsClientService dscms2GapsClientService;

    /**
     * @param idchekReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto
     * @author xuchao
     * @date 2021/6/28 9:51
     * @version 1.0.0
     * @desc    身份证核查
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public IdchekRespDto idchek(IdchekReqDto idchekReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value, JSON.toJSONString(idchekReqDto));
        ResultDto<IdchekRespDto> idchekRespDtoResultDto = dscms2GapsClientService.idchek(idchekReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value, JSON.toJSONString(idchekRespDtoResultDto));
        String idchekCode = Optional.ofNullable(idchekRespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String idchekMeesage = Optional.ofNullable(idchekRespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        IdchekRespDto idchekRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, idchekRespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            idchekRespDto = idchekRespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(idchekCode, idchekMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value);
        return idchekRespDto;
    }
}
