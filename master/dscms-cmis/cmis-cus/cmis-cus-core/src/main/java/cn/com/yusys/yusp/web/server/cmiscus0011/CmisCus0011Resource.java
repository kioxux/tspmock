package cn.com.yusys.yusp.web.server.cmiscus0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0011.req.CmisCus0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.resp.CmisCus0011RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusLstYndAppService;
import cn.com.yusys.yusp.web.server.cmiscus0007.CmisCus0007Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 查询优农贷名单信息
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "CmisCus0011:查询优农贷名单信息")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0007Resource.class);

    @Autowired
    private CusLstYndAppService cusLstYndAppService;
    /**
     * 交易码：cmiscus0011
     * 交易描述：查询优农贷名单信息
     *
     * @param cmisCus0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优农贷名单信息")
    @PostMapping("/cmiscus0011")
    protected @ResponseBody
    ResultDto<CmisCus0011RespDto> cmiscus0011(@Validated @RequestBody CmisCus0011ReqDto cmisCus0011ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0011.key, DscmsEnum.TRADE_CODE_CMISCUS0011.value, JSON.toJSONString(cmisCus0011ReqDto));
        CmisCus0011RespDto cmisCus0011RespDto = new CmisCus0011RespDto();// 响应Dto:查询优农贷名单信息
        ResultDto<CmisCus0011RespDto> cmisCus0011ResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0011.key, DscmsEnum.TRADE_CODE_CMISCUS0011.value, JSON.toJSONString(cmisCus0011ReqDto));
            cmisCus0011RespDto = cusLstYndAppService.selectYndLstByCertCode(cmisCus0011ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0011.key, DscmsEnum.TRADE_CODE_CMISCUS0011.value, JSON.toJSONString(cmisCus0011RespDto));
            // 封装cmisCus0011ResultDto中正确的返回码和返回信息
            cmisCus0011ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0011ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, e.getMessage());
            // 封装cmisCus0011ResultDto中异常返回码和返回信息
            cmisCus0011ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0011ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0011RespDto到cmisCus0007ResultDto中
        cmisCus0011ResultDto.setData(cmisCus0011RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0011ResultDto));
        return cmisCus0011ResultDto;
    }
}
