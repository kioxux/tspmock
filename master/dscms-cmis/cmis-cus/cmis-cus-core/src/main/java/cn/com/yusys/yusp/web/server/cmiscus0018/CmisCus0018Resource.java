package cn.com.yusys.yusp.web.server.cmiscus0018;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusLstZxdDto;
import cn.com.yusys.yusp.dto.OperationRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscus0018.Cmiscus0018Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 更新增享贷白名单
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0018:更新增享贷白名单")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0018Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisCus0018Resource.class);

    @Autowired
    private Cmiscus0018Service cmiscus0018Service;

    /**
     * 交易码：cmiscus0018
     * 交易描述：更新增享贷白名单
     *
     * @param cusLstZxdDto
     * @return
     * @throws Exception
     */
    @ApiOperation("更新增享贷白名单")
    @PostMapping("/cmiscus0018")
    protected @ResponseBody
    ResultDto<OperationRespDto> cmiscus0018(@Validated @RequestBody CusLstZxdDto cusLstZxdDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0018.key, DscmsEnum.TRADE_CODE_CMISCUS0018.value, JSON.toJSONString(cusLstZxdDto));
        OperationRespDto operationRespDto = new OperationRespDto();// 响应Dto:查询个人无还本续贷表信息
        ResultDto<OperationRespDto> operationRespDtoResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0018.key, DscmsEnum.TRADE_CODE_CMISCUS0018.value, JSON.toJSONString(cusLstZxdDto));
            operationRespDto = cmiscus0018Service.cmiscus0018(cusLstZxdDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0018.key, DscmsEnum.TRADE_CODE_CMISCUS0018.value, JSON.toJSONString(operationRespDto));
            // 封装cmisCus0018ResultDto中正确的返回码和返回信息
            operationRespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            operationRespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0018.key, DscmsEnum.TRADE_CODE_CMISCUS0018.value, e.getMessage());
            // 封装cmisCus0018ResultDto中异常返回码和返回信息
            operationRespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            operationRespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0018RespDto到cmisCus0018ResultDto中
        operationRespDtoResultDto.setData(operationRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0018.key, DscmsEnum.TRADE_CODE_CMISCUS0018.value, JSON.toJSONString(operationRespDtoResultDto));
        return operationRespDtoResultDto;
    }

}
