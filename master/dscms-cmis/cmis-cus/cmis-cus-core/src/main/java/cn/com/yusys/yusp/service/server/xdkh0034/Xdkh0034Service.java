package cn.com.yusys.yusp.service.server.xdkh0034;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0034.resp.List;
import cn.com.yusys.yusp.dto.server.xdkh0034.req.Xdkh0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0034.resp.Xdkh0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusComGradeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0034Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0034Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdkh0034Service.class);

    @Autowired
    private CusComGradeMapper cusComGradeMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 查询企业在我行客户评级信息
     *
     * @param xdkh0034DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdkh0034DataRespDto xdkh0034(Xdkh0034DataReqDto xdkh0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value);
        Xdkh0034DataRespDto xdkh0034DataRespDto = new Xdkh0034DataRespDto();

        try {
            String cudId = xdkh0034DataReqDto.getCusId();//客户号

            //获取系统日期
            String openday = stringRedisTemplate.opsForValue().get("openDay");

            //先更新同业客户信息表
            Map QueryMap = new HashMap();
            QueryMap.put("cusId", cudId);
            QueryMap.put("sysDate", openday);
            //更改对公客户的锁定状态
            java.util.List<List> lists = cusComGradeMapper.selcectFinalRankByCusId(QueryMap);

            //返回信息
            xdkh0034DataRespDto.setList(lists);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value);
        return xdkh0034DataRespDto;
    }
}
