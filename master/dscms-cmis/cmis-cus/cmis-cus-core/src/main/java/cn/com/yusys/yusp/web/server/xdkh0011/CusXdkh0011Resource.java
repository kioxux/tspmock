package cn.com.yusys.yusp.web.server.xdkh0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0011.req.Xdkh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0011.resp.Xdkh0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0011.Xdkh0011Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:对私客户信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0011:对私客户信息同步")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0011Resource.class);

    @Autowired
    private Xdkh0011Service xdkh0011Service;

    /**
     * 交易码：xdkh0011
     * 交易描述：对私客户信息同步
     *
     * @param xdkh0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对私客户信息同步")
    @PostMapping("/xdkh0011")
    protected @ResponseBody
    ResultDto<Xdkh0011DataRespDto> xdkh0011(@Validated @RequestBody Xdkh0011DataReqDto xdkh0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011DataReqDto));
        Xdkh0011DataRespDto xdkh0011DataRespDto = new Xdkh0011DataRespDto();// 响应Dto:对私客户信息同步
        ResultDto<Xdkh0011DataRespDto> xdkh0011DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011DataReqDto));
            xdkh0011DataRespDto = xdkh0011Service.xdkh0011(xdkh0011DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011DataRespDto));
            // 封装xdkh0011DataResultDto中正确的返回码和返回信息
            xdkh0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, e.getMessage());
            // 封装xdkh0011DataResultDto中异常返回码和返回信息
            xdkh0011DataResultDto.setCode(e.getErrorCode());
            xdkh0011DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, e.getMessage());
            // 封装xdkh0011DataResultDto中异常返回码和返回信息
            xdkh0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0011DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0011DataRespDto到xdkh0011DataResultDto中
        xdkh0011DataResultDto.setData(xdkh0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011DataResultDto));
        return xdkh0011DataResultDto;
    }
}