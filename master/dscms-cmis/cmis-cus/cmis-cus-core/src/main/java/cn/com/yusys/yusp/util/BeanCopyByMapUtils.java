package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.dto.CusIndivAllDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.enums.BeanMapInterface;
import cn.com.yusys.yusp.enums.IndivToEcifMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.FatalBeanException;
import org.springframework.util.ClassUtils;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 *
 */
public class BeanCopyByMapUtils {

    public static void copyByEnum(Object source, Object target, BeanMapInterface map){
        Field[] sourceFields = source.getClass().getDeclaredFields();
        for (Field sourceField: sourceFields) {
            String sourceFieldName = sourceField.getName();
            String targetFieldName = map.getTargetFieldBySource(sourceFieldName);
//            String targetFieldName = null;
//            for (Enum enumData : EnumSet.allOf(Enum.class)) {
//                EnumSet.
//                targetFieldName = enumData.getTargetFieldBySource(sourceFieldName);
//                 if(targetFieldName != null){
//                    break;
//                 }
//            }
            if(targetFieldName != null){
                PropertyDescriptor sourcePd = BeanUtils.getPropertyDescriptor(source.getClass(), sourceFieldName);
                PropertyDescriptor targetPd = BeanUtils.getPropertyDescriptor(target.getClass(), targetFieldName);
                System.out.println(sourceFieldName + ">>>>>>" + targetFieldName);
                if (sourcePd != null){
                    Method readMethod = sourcePd.getReadMethod();
                    Method writeMethod = targetPd.getWriteMethod();
                    if (readMethod != null && writeMethod != null && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                        try {
                            if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                                readMethod.setAccessible(true);
                            }

                            Object value = readMethod.invoke(source);
                            if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                                writeMethod.setAccessible(true);
                            }

                            writeMethod.invoke(target, value);
                        } catch (Throwable e) {
                            throw new FatalBeanException("映射字段数据从【" + sourceFieldName + "】 到 【" + targetFieldName + "】时失败！", e);
                        }
                    }
                }
            }
        }
    }


    public static void main(String[] args){
        CusIndivAllDto source = new CusIndivAllDto();
        S00102ReqDto target = new S00102ReqDto();
        target.setProjob("1");
        target.setCustna("1");
        target.setIdtfno("1");
        target.setCustst("1");
        BeanCopyByMapUtils BeanCopyByMapUtils = new BeanCopyByMapUtils();
        BeanCopyByMapUtils.copyByEnum(target, source, new IndivToEcifMap());
        System.out.println(source.toString());
    }
}
