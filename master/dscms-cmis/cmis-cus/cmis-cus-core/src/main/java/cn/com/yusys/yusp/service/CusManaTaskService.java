/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SecurityUtils;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CusManaTask;
import cn.com.yusys.yusp.repository.mapper.CusManaTaskMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusManaTaskService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-09 19:55:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusManaTaskService {

    @Autowired
    private CusManaTaskMapper cusManaTaskMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusManaTask selectByPrimaryKey(String serno) {
        return cusManaTaskMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectCount
     * @方法描述: 根据类型查询条数
     * @参数与返回说明:
     * @算法描述: 无
     * @修改人: zhoumw
     */

    public int selectCount(String type) {
        // 条数
        int count = 0;
        if(!StringUtils.isEmpty(type)){
            count = cusManaTaskMapper.selectCount(type);
        }
        return count;
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusManaTask> selectAll(QueryModel model) {
        List<CusManaTask> records = (List<CusManaTask>) cusManaTaskMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusManaTask> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusManaTask> list = cusManaTaskMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusManaTask record) {
        int count = 0;
        count = cusManaTaskMapper.selectByCusId(record.getCusId());
        if (count > 0) {
            throw BizException.error(null,"9999","该客户存在待处理任务,请先处理");
        }
        return cusManaTaskMapper.insert(record);
    }

    /**
     * @方法名称: createTask
     * @方法描述: 创建任务
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int createTask(CusManaTask record) throws Exception {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",record.getCusId());
        queryModel.addCondition("bizType",record.getBizType());
        queryModel.addCondition("taskStatus",record.getTaskStatus());
        List<CusManaTask> cusManaTasks = cusManaTaskMapper.selectByModel(queryModel);
        if (cusManaTasks.size()>0){
            throw BizException.error(null,"9999","该客户存在待处理的客户信息创建或维护任务,请先处理！");
        }
        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
        record.setSerno(serno);
        return cusManaTaskMapper.insert(record);
    }
    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusManaTask record) {
        return cusManaTaskMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusManaTask record) {
        return cusManaTaskMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusManaTask record) {
        return cusManaTaskMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusManaTaskMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusManaTaskMapper.deleteByIds(ids);
    }
    /**
     * @方法名称: save
     * @方法描述: 个人客户任务保存，有则更新，无责保存
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String save(CusManaTask cusManaTask) {
        String result= "";
        int count=0;
        if(cusManaTask!=null){
            int count1 = 0;
            count1 = cusManaTaskMapper.selectByCusId(cusManaTask.getCusId());
            if (count1 > 0) {
                throw BizException.error(null,"9999","该客户存在待处理任务,请先处理");
            }
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            if(userInfo!=null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
                String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
                cusManaTask.setSerno(serno);
                // 申请人
                cusManaTask.setInputId(inputId);
                // 申请机构
                cusManaTask.setInputBrId(inputBrId);
                // 申请时间
                cusManaTask.setInputDate(inputDate);
                // 创建时间
                cusManaTask.setCreateTime(DateUtils.getCurrTimestamp());
                // 审批状态
                cusManaTask.setApproveStatus(CmisCusConstants.APPLY_STATE_TODO);
                // 处理状态待处理
                cusManaTask.setTaskStatus(CmisCusConstants.STD_TASK_STATUS_1);
                count=this.cusManaTaskMapper.insertSelective(cusManaTask);
                if(count == 1){
                    result =serno;
                }
            }
            return result;
    }
    /**
     * @方法名称: selectByPrimarySerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusManaTask selectByPrimarySerno(String serno) {
        return cusManaTaskMapper.selectByPrimarySerno(serno);
    }


    /**
     * @方法名称: queryCusManaTask
     * @方法描述: 条件查询 -
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public List<CusManaTask> queryCusManaTask(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CusManaTask> list = cusManaTaskMapper.queryCusManaTask(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @函数名称:selectCountByBizType
     * @函数描述:根据业务类型查询任务条数
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public int selectCountByBizType(List<String> bizTypeList) {

        // 条数
        int count = 0;
        if(bizTypeList.size()>0){
            count = cusManaTaskMapper.selectCountByBizType(bizTypeList);
        }
        return count;
    }

    /**
     * @函数名称:updateTaskStatusByCusId
     * @函数描述:根据客户号、业务类型更改任务状态为3 已处理
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public int updateTaskStatusByCusId(CusManaTask cusManaTask) {
        return cusManaTaskMapper.updateTaskStatusByCusId(cusManaTask);
    }

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    public List<Map<String, Object>> selectNumByInputId(QueryModel queryModel) {
        return cusManaTaskMapper.selectNumByInputId(queryModel);
    }
}
