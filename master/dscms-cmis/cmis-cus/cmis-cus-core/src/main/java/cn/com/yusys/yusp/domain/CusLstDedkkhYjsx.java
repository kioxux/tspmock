/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhYjsx
 * @类描述: cus_lst_dedkkh_yjsx数据实体类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_dedkkh_yjsx")
public class CusLstDedkkhYjsx extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键流水 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 名单流水号 **/
	@Column(name = "LIST_SERNO", unique = false, nullable = true, length = 40)
	private String listSerno;
	
	/** 压降事项 **/
	@Column(name = "PRESSURE_DROP_ITEM", unique = false, nullable = true, length = 500)
	private String pressureDropItem;
	
	/** 压降金额（万元） **/
	@Column(name = "PRESSURE_DROP_AMT", unique = false, nullable = true, length = 16)
	private String pressureDropAmt;
	
	/** 压降要求完成时间 **/
	@Column(name = "PRESSURE_DROP_FINISH_TIME", unique = false, nullable = true, length = 20)
	private String pressureDropFinishTime;
	
	/** 实际压降金额（万元） **/
	@Column(name = "ACT_PDO_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal actPdoAmt;
	
	/** 压降实际完成时间 **/
	@Column(name = "ACT_PDO_FINISH_DATE", unique = false, nullable = true, length = 20)
	private String actPdoFinishDate;
	
	/** 压降落实情况说明 **/
	@Column(name = "PDO_ACT_CASE_MEMO", unique = false, nullable = true, length = 20)
	private String pdoActCaseMemo;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 10)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 10)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date updateTime;
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param listSerno
	 */
	public void setListSerno(String listSerno) {
		this.listSerno = listSerno;
	}
	
    /**
     * @return listSerno
     */
	public String getListSerno() {
		return this.listSerno;
	}
	
	/**
	 * @param pressureDropItem
	 */
	public void setPressureDropItem(String pressureDropItem) {
		this.pressureDropItem = pressureDropItem;
	}
	
    /**
     * @return pressureDropItem
     */
	public String getPressureDropItem() {
		return this.pressureDropItem;
	}
	
	/**
	 * @param pressureDropAmt
	 */
	public void setPressureDropAmt(String pressureDropAmt) {
		this.pressureDropAmt = pressureDropAmt;
	}
	
    /**
     * @return pressureDropAmt
     */
	public String getPressureDropAmt() {
		return this.pressureDropAmt;
	}
	
	/**
	 * @param pressureDropFinishTime
	 */
	public void setPressureDropFinishTime(String pressureDropFinishTime) {
		this.pressureDropFinishTime = pressureDropFinishTime;
	}
	
    /**
     * @return pressureDropFinishTime
     */
	public String getPressureDropFinishTime() {
		return this.pressureDropFinishTime;
	}
	
	/**
	 * @param actPdoAmt
	 */
	public void setActPdoAmt(java.math.BigDecimal actPdoAmt) {
		this.actPdoAmt = actPdoAmt;
	}
	
    /**
     * @return actPdoAmt
     */
	public java.math.BigDecimal getActPdoAmt() {
		return this.actPdoAmt;
	}
	
	/**
	 * @param actPdoFinishDate
	 */
	public void setActPdoFinishDate(String actPdoFinishDate) {
		this.actPdoFinishDate = actPdoFinishDate;
	}
	
    /**
     * @return actPdoFinishDate
     */
	public String getActPdoFinishDate() {
		return this.actPdoFinishDate;
	}
	
	/**
	 * @param pdoActCaseMemo
	 */
	public void setPdoActCaseMemo(String pdoActCaseMemo) {
		this.pdoActCaseMemo = pdoActCaseMemo;
	}
	
    /**
     * @return pdoActCaseMemo
     */
	public String getPdoActCaseMemo() {
		return this.pdoActCaseMemo;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}