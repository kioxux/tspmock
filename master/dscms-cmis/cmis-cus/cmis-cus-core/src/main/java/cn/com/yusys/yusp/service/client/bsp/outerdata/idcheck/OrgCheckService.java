package cn.com.yusys.yusp.service.client.bsp.outerdata.idcheck;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusMgrDividePerc;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务逻辑处理类：客户经理机构核查
 *
 * @author cp
 * @version 1.0
 * @since 2021年8月28日 下午1:22:06
 */
@Service
@Transactional
public class OrgCheckService {
    private static final Logger logger = LoggerFactory.getLogger(OrgCheckService.class);
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private AdminSmOrgService adminSmOrgService;

    /**
     * 业务逻辑处理方法：客户经理机构核查
     *
     * @param managerId
     * @return
     */
    @Transactional
    public Map<String, Object> checkIsSameOrg(String managerId, String managerProp) throws YuspException {
        ResultDto<AdminSmUserDto> adminSmUserDto = null;
        ResultDto<AdminSmOrgDto> resultDto = null;
        Map<String, Object> resultMap = new HashMap<>();
        String orgId = null;
        String managerName = null;
        String orgName = null;
        String upOrgId = null;
        adminSmUserDto = adminSmUserService.getByLoginCode(managerId);
        String code = adminSmUserDto.getCode();
        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
            if (adminSmUserDto != null && adminSmUserDto.getData().getOrgId() != null && !"".equals(adminSmUserDto.getData().getOrgId())) {
                orgId = adminSmUserDto.getData().getOrgId();
            }
            if (adminSmUserDto != null && adminSmUserDto.getData().getUserName() != null && !"".equals(adminSmUserDto.getData().getUserName())) {
                managerName = adminSmUserDto.getData().getUserName();
            }
            if (orgId != null && !"".equals(orgId)) {
                resultDto = adminSmOrgService.getByOrgCode(orgId);
                String code2 = resultDto.getCode();
                if (StringUtil.isNotEmpty(code2) && CmisBizConstants.NUM_ZERO.equals(code2)) {
                    orgName = resultDto.getData().getOrgName();
                    upOrgId = resultDto.getData().getUpOrgId();
                }
            }
            resultMap.put("upOrgId", upOrgId);
            resultMap.put("managerName", managerName);
            resultMap.put("orgId", orgId);
            resultMap.put("orgName", orgName);
        }
        return resultMap;
    }

    /**
     * 业务逻辑处理方法：客户经理机构核查
     *
     * @param loginOrgId
     * @return
     */
    @Transactional
    public Boolean orgAccessCheck(String loginOrgId) throws YuspException {
        ResultDto<AdminSmOrgDto> resultDto = null;
        Boolean flag = false;
        String orgType = null;
        resultDto = adminSmOrgService.getByOrgCode(loginOrgId);
        String code2 = resultDto.getCode();
        if (StringUtil.isNotEmpty(code2) && CmisBizConstants.NUM_ZERO.equals(code2)) {
            orgType = resultDto.getData().getOrgType();
            if ("3".equals(orgType)) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 业务逻辑处理方法：查询客户经理是否为虚拟客户经理
     *
     * @param managerId
     * @return
     */
    @Transactional
    public Boolean isVirtualCusManager(String managerId, String managerProp) throws YuspException {
        ResultDto<AdminSmUserDto> adminSmUserDto = null;
        Boolean flag = false;
        String userName = null;
        String code = null;
        if ("3".equals(managerProp)) {
            adminSmUserDto = adminSmUserService.getByLoginCode(managerId);
            code = adminSmUserDto.getCode();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                if (adminSmUserDto != null && adminSmUserDto.getData().getUserName() != null && !"".equals(adminSmUserDto.getData().getUserName())) {
                    userName = adminSmUserDto.getData().getUserName();
                    if (userName.contains("虚拟")) {
                        flag = true;
                    }
                }
            }
        }
        return flag;
    }
}
