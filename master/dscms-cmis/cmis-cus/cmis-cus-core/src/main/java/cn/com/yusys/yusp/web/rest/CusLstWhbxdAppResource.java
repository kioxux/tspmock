/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstWhbxdApp;
import cn.com.yusys.yusp.service.CusLstWhbxdAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWhbxdAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:33:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstwhbxdapp")
public class CusLstWhbxdAppResource {
    @Autowired
    private CusLstWhbxdAppService cusLstWhbxdAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstWhbxdApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstWhbxdApp> list = cusLstWhbxdAppService.selectAll(queryModel);
        return new ResultDto<List<CusLstWhbxdApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstWhbxdApp>> index(QueryModel queryModel) {
        List<CusLstWhbxdApp> list = cusLstWhbxdAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWhbxdApp>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstWhbxdApp>> query(@RequestBody QueryModel queryModel) {
        List<CusLstWhbxdApp> list = cusLstWhbxdAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWhbxdApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{lwaSerno}")
    protected ResultDto<CusLstWhbxdApp> show(@PathVariable("lwaSerno") String lwaSerno) {
        CusLstWhbxdApp cusLstWhbxdApp = cusLstWhbxdAppService.selectByPrimaryKey(lwaSerno);
        return new ResultDto<CusLstWhbxdApp>(cusLstWhbxdApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstWhbxdApp> create(@RequestBody CusLstWhbxdApp cusLstWhbxdApp) throws URISyntaxException {
        cusLstWhbxdAppService.insert(cusLstWhbxdApp);
        return new ResultDto<CusLstWhbxdApp>(cusLstWhbxdApp);
    }
    @PostMapping("/check")
    protected ResultDto<CusLstWhbxdApp> show2(@RequestBody CusLstWhbxdApp cusLstWhbxdApp2) {
        CusLstWhbxdApp cusLstWhbxdApp = cusLstWhbxdAppService.selectByPrimaryKey(cusLstWhbxdApp2.getLwaSerno());
        return new ResultDto<CusLstWhbxdApp>(cusLstWhbxdApp);
    }
    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstWhbxdApp cusLstWhbxdApp) throws URISyntaxException {
        int result = cusLstWhbxdAppService.update(cusLstWhbxdApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{lwaSerno}")
    protected ResultDto<Integer> delete(@PathVariable("lwaSerno") String lwaSerno) {
        int result = cusLstWhbxdAppService.deleteByPrimaryKey(lwaSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstWhbxdAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
