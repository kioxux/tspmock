/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

/**
 * 关联方管理导入模板
 */
@ExcelCsv(namePrefix = "关联方名单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CusLstGlfVo {

    /**
     * 关联方类型
     **/
    @ExcelField(title = "关联方类型", viewLength = 20)
    private String relatedPartyType;

    /**
     * 客户名称
     **/
    @ExcelField(title = "关联方名称", viewLength = 80)
    private String relatedPartyName;

    /**
     * 证件类型
     **/
    @ExcelField(title = "证件类型", viewLength = 5)
    private String certType;

    /**
     * 证件号码
     **/
    @ExcelField(title = "证件号码", viewLength = 40)
    private String certCode;

    /**
     * 境内/境外标识
     **/
    @ExcelField(title = "境内/境外标识", viewLength = 5)
    private String lcaos;

    /**
     * 归属组别
     **/
    @ExcelField(title = "归属组别", viewLength = 20)
    private String belongGroup;

    /**
     * 层级标识
     **/
    @ExcelField(title = "层级标识", viewLength = 20)
    private String levels;

    /**
     * 上一级关联方名称
     **/
    @ExcelField(title = "上一级关联方名称", viewLength = 80)
    private String parebtRelatedPartyName;

    /**
     * 上一级关联方证件号码
     **/
    @ExcelField(title = "上一级关联方证件号码", viewLength = 40)
    private String parebtRelatedPartyCertNo;

    /**
     * 与上一级关联方关系
     **/
    @ExcelField(title = "与上一级关联方关系", viewLength = 20)
    private String parebtRelatedPartyRela;

    /**
     * 与上一级之间是否有权益份额
     **/
    @ExcelField(title = "与上一级之间是否有权益份额", viewLength = 20)
    private String parebtBetweenIsRightsShare;

    /**
     * 持有比例
     **/
    @ExcelField(title = "持有比例", viewLength = 20)
    private String holdPerc;

    /**
     * 关联关系说明
     **/
    @ExcelField(title = "关联关系说明", viewLength = 20)
    private String correMemo;

    public String getRelatedPartyType() {
        return relatedPartyType;
    }

    public void setRelatedPartyType(String relatedPartyType) {
        this.relatedPartyType = relatedPartyType;
    }

    public String getRelatedPartyName() {
        return relatedPartyName;
    }

    public void setRelatedPartyName(String relatedPartyName) {
        this.relatedPartyName = relatedPartyName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getLcaos() {
        return lcaos;
    }

    public void setLcaos(String lcaos) {
        this.lcaos = lcaos;
    }

    public String getBelongGroup() {
        return belongGroup;
    }

    public void setBelongGroup(String belongGroup) {
        this.belongGroup = belongGroup;
    }

    public String getLevels() {
        return levels;
    }

    public void setLevels(String levels) {
        this.levels = levels;
    }

    public String getParebtRelatedPartyName() {
        return parebtRelatedPartyName;
    }

    public void setParebtRelatedPartyName(String parebtRelatedPartyName) {
        this.parebtRelatedPartyName = parebtRelatedPartyName;
    }

    public String getParebtRelatedPartyCertNo() {
        return parebtRelatedPartyCertNo;
    }

    public void setParebtRelatedPartyCertNo(String parebtRelatedPartyCertNo) {
        this.parebtRelatedPartyCertNo = parebtRelatedPartyCertNo;
    }

    public String getParebtRelatedPartyRela() {
        return parebtRelatedPartyRela;
    }

    public void setParebtRelatedPartyRela(String parebtRelatedPartyRela) {
        this.parebtRelatedPartyRela = parebtRelatedPartyRela;
    }

    public String getParebtBetweenIsRightsShare() {
        return parebtBetweenIsRightsShare;
    }

    public void setParebtBetweenIsRightsShare(String parebtBetweenIsRightsShare) {
        this.parebtBetweenIsRightsShare = parebtBetweenIsRightsShare;
    }

    public String getHoldPerc() {
        return holdPerc;
    }

    public void setHoldPerc(String holdPerc) {
        this.holdPerc = holdPerc;
    }

    public String getCorreMemo() {
        return correMemo;
    }

    public void setCorreMemo(String correMemo) {
        this.correMemo = correMemo;
    }
}