/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;

import cn.com.yusys.yusp.dto.CusLstWhbxdDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstWhbxd;
import cn.com.yusys.yusp.repository.mapper.CusLstWhbxdMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWhbxdService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: pl
 * @创建时间: 2021-04-07 11:15:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstWhbxdService {

    @Autowired
    private CusLstWhbxdMapper cusLstWhbxdMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstWhbxd selectByPrimaryKey(String cusId) {
        return cusLstWhbxdMapper.selectByPrimaryKey(cusId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstWhbxd> selectAll(QueryModel model) {
        List<CusLstWhbxd> records = (List<CusLstWhbxd>) cusLstWhbxdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstWhbxd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstWhbxd> list = cusLstWhbxdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstWhbxd record) {
        return cusLstWhbxdMapper.insert(record);
    }

    /**
     * @方法名称: addCusLstWhbxd
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int addCusLstWhbxd(CusLstWhbxdDto cusLstWhbxdDto) {
        CusLstWhbxd cusLstWhbxdOld = selectByPrimaryKey(cusLstWhbxdDto.getCusId());
        if(Objects.nonNull(cusLstWhbxdOld)) {
            BeanUtils.copyProperties(cusLstWhbxdDto,cusLstWhbxdOld);
            return cusLstWhbxdMapper.updateByPrimaryKeySelective(cusLstWhbxdOld);
        } else {
            cusLstWhbxdOld = new CusLstWhbxd();
            BeanUtils.copyProperties(cusLstWhbxdDto,cusLstWhbxdOld);
            return cusLstWhbxdMapper.insert(cusLstWhbxdOld);
        }
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstWhbxd record) {
        return cusLstWhbxdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstWhbxd record) {
        return cusLstWhbxdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstWhbxd record) {
        return cusLstWhbxdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId) {
        return cusLstWhbxdMapper.deleteByPrimaryKey(cusId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstWhbxdMapper.deleteByIds(ids);
    }
}
