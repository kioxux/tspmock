package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusLstWjkhApp;
import cn.com.yusys.yusp.domain.CusLstWjkhMxxx;
import cn.com.yusys.yusp.domain.CusLstWjkhWhite;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CusLstWjkhAppService;
import cn.com.yusys.yusp.service.CusLstWjkhMxxxService;
import cn.com.yusys.yusp.service.CusLstWjkhWhiteService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 网金客户白名单准入、停用审批流程
 *
 * @author liucheng
 * @version 1.0.0
 */
@Service
public class KHGL12BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(KHGL12BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CusLstWjkhAppService service;

    @Autowired
    private CusLstWjkhWhiteService cusLstWjkhWhiteService;

    @Autowired
    private CusLstWjkhMxxxService cusLstWjkhMxxxService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String floeBizType = resultInstanceDto.getBizType();
        log.info("后业务处理类型" + currentOpType);
        try {
            CusLstWjkhApp domain = service.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
//                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
//                service.update(domain);
                log.info("网金客户白名单准入、停用审批流程【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("网金客户白名单准入、停用审批流程【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("网金客户白名单准入、停用审批流程【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                // 1、插入cus_lst_wjkh_white 表
                if("KH021".equals(floeBizType)){ // 网金客户白名单准入
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("serno", serno);
                    List<CusLstWjkhMxxx> cusLstWjkhMxxxes = cusLstWjkhMxxxService.selectByModel(queryModel);
                    for (CusLstWjkhMxxx cusLstWjkhMxxxe : cusLstWjkhMxxxes) {
                        CusLstWjkhWhite cusLstWjkhWhite = new CusLstWjkhWhite();
                        BeanUtils.beanCopy(cusLstWjkhMxxxe, cusLstWjkhWhite);
                        cusLstWjkhWhite.setListStatus(CmisBizConstants.STD_ZB_STATUS_01);
                        cusLstWjkhWhite.setHuser(domain.getHuser());//经办人
                        cusLstWjkhWhite.setHandOrg(domain.getHandOrg());//经办机构
                        cusLstWjkhWhiteService.insert(cusLstWjkhWhite);
                    }
                }else if("KH022".equals(floeBizType)){ // 网金客户白名单停用
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("serno", serno);
                    List<CusLstWjkhMxxx> cusLstWjkhMxxxes = cusLstWjkhMxxxService.selectByModel(queryModel);
                    for (CusLstWjkhMxxx cusLstWjkhMxxxe : cusLstWjkhMxxxes) {
                        CusLstWjkhWhite cusLstWjkhWhite = new CusLstWjkhWhite();
//                        BeanUtils.beanCopy(cusLstWjkhMxxxe, cusLstWjkhWhite);
                        cusLstWjkhWhite.setCusId(cusLstWjkhMxxxe.getCusId());
                        cusLstWjkhWhite.setListStatus(CmisBizConstants.STD_ZB_STATUS_02);
                        cusLstWjkhWhiteService.updateSelectiveByCusId(cusLstWjkhWhite);
                    }
                }

                // 2、更新申请表
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                service.update(domain);
                log.info("网金客户白名单准入、停用审批流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("网金客户白名单准入、停用审批流程【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 判断是否打回发起人节点
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                    domain.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    service.update(domain);
                }
                log.info("网金客户白名单准入、停用审批流程【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("网金客户白名单准入、停用审批流程【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("网金客户白名单准入、停用审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("网金客户白名单准入、停用审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                service.update(domain);
            } else {
                log.info("网金客户白名单准入、停用审批流程【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("网金客户白名单准入、停用审批流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "KHGL12".equals(flowCode);
    }
}

