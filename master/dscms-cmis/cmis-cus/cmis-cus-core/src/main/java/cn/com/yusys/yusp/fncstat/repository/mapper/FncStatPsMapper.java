/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.fncstat.domain.FncStatPs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatPsMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zzbankwb369
 * @创建时间: 2019-10-08 09:46:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface FncStatPsMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    FncStatPs selectByPrimaryKey(@Param("cusId") String cusId, @Param("statStyle") String statStyle, @Param("statYear") String statYear, @Param("statItemId") String statItemId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<FncStatPs> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(FncStatPs record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int insertSelective(FncStatPs record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int updateByPrimaryKey(FncStatPs record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(FncStatPs record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("cusId") String cusId, @Param("statStyle") String statStyle, @Param("statYear") String statYear, @Param("statItemId") String statItemId);

}