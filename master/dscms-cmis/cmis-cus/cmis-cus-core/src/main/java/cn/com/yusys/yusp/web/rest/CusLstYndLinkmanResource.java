/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYndLinkman;
import cn.com.yusys.yusp.service.CusLstYndLinkmanService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndLinkmanResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:19:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstyndlinkman")
public class CusLstYndLinkmanResource {
    @Autowired
    private CusLstYndLinkmanService cusLstYndLinkmanService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstYndLinkman>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstYndLinkman> list = cusLstYndLinkmanService.selectAll(queryModel);
        return new ResultDto<List<CusLstYndLinkman>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstYndLinkman>> index(QueryModel queryModel) {
        List<CusLstYndLinkman> list = cusLstYndLinkmanService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYndLinkman>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusLstYndLinkman> show(@PathVariable("pkId") String pkId) {
        CusLstYndLinkman cusLstYndLinkman = cusLstYndLinkmanService.selectByPrimaryKey(pkId);
        return new ResultDto<CusLstYndLinkman>(cusLstYndLinkman);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstYndLinkman> create(@RequestBody CusLstYndLinkman cusLstYndLinkman) throws URISyntaxException {
        cusLstYndLinkmanService.insert(cusLstYndLinkman);
        return new ResultDto<CusLstYndLinkman>(cusLstYndLinkman);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstYndLinkman cusLstYndLinkman) throws URISyntaxException {
        int result = cusLstYndLinkmanService.update(cusLstYndLinkman);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusLstYndLinkmanService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstYndLinkmanService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param cusLstYndLinkman
     * @return int
     * @author hubp
     * @date 2021/7/16 15:31
     * @version 1.0.0
     * @desc    新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addcuslstyndlinkman")
    protected ResultDto<Integer> addCusLstYndLinkman(@RequestBody CusLstYndLinkman cusLstYndLinkman) {
        int result = cusLstYndLinkmanService.addCusLstYndLinkman(cusLstYndLinkman);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param cusLstYndLinkman
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/7/16 15:35
     * @version 1.0.0
     * @desc  通过流水号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<List<CusLstYndLinkman>> selectBySerno(@RequestBody CusLstYndLinkman cusLstYndLinkman) {
        List<CusLstYndLinkman> list = cusLstYndLinkmanService.selectBySerno(cusLstYndLinkman.getSerno());
        return new ResultDto<List<CusLstYndLinkman>>(list);
    }

    /**
     * @param cusLstYndLinkman
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndLinkman>
     * @author hubp
     * @date 2021/7/16 16:09
     * @version 1.0.0
     * @desc    通过主键查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbypkid")
    protected ResultDto<CusLstYndLinkman> selectByPkId(@RequestBody CusLstYndLinkman cusLstYndLinkman) {
        CusLstYndLinkman result = cusLstYndLinkmanService.selectByPrimaryKey(cusLstYndLinkman.getPkId());
        return new ResultDto<CusLstYndLinkman>(result);
    }

    /**
     * @param cusLstYndLinkman
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/7/16 16:35
     * @version 1.0.0
     * @desc    通过主键删除
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deletecuslstyndlinkman")
    protected ResultDto<Integer> deleteCusLstYndLinkman(@RequestBody CusLstYndLinkman cusLstYndLinkman) {
        int result = cusLstYndLinkmanService.deleteByPrimaryKey(cusLstYndLinkman.getPkId());
        return new ResultDto<Integer>(result);
    }
}
