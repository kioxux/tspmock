/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpMemberRel
 * @类描述: cus_grp_member_rel数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-10 10:46:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_grp_member_rel")
public class CusGrpMemberRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 集团编号 **/
	@Column(name = "GRP_NO", unique = false, nullable = true, length = 30)
	private String grpNo;
	
	/** 成员客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 集团名称 **/
	@Column(name = "GRP_NAME", unique = false, nullable = true, length = 80)
	private String grpName;
	
	/** 成员客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 10)
	private String cusType;
	
	/** 集团关系类型 **/
	@Column(name = "GRP_CORRE_TYPE", unique = false, nullable = true, length = 5)
	private String grpCorreType;
	
	/** 集团关系描述 **/
	@Column(name = "GRP_CORRE_DEC", unique = false, nullable = true, length = 250)
	private String grpCorreDec;
	
	/** 是否集团主客户 **/
	@Column(name = "IS_MAIN_CUS", unique = false, nullable = true, length = 5)
	private String isMainCus;
	
	/** 集团客户情况说明 **/
	@Column(name = "GRP_CASE_MEMO", unique = false, nullable = true, length = 250)
	private String grpCaseMemo;
	
	/** 主管人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 是否有效 **/
	@Column(name = "AVAILABLE_IND", unique = false, nullable = true, length = 5)
	private String availableInd;
	
	/** 统一社会信用代码 **/
	@Column(name = "SOCI_CRED_CODE", unique = false, nullable = true, length = 18)
	private String sociCredCode;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 客户大类 **/
	@Column(name = "CUS_CATALOG", unique = false, nullable = true, length = 1)
	private String cusCatalog;
	
	/** 是否保留集团额度**/
	@Column(name = "RESERVE_GRP_LIMIT_IND", unique = false, nullable = true, length = 1)
	private String reserveGrpLimitInd;
	
	/** 成员客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo;
	}
	
    /**
     * @return grpNo
     */
	public String getGrpNo() {
		return this.grpNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param grpName
	 */
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}
	
    /**
     * @return grpName
     */
	public String getGrpName() {
		return this.grpName;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param grpCorreType
	 */
	public void setGrpCorreType(String grpCorreType) {
		this.grpCorreType = grpCorreType;
	}
	
    /**
     * @return grpCorreType
     */
	public String getGrpCorreType() {
		return this.grpCorreType;
	}
	
	/**
	 * @param grpCorreDec
	 */
	public void setGrpCorreDec(String grpCorreDec) {
		this.grpCorreDec = grpCorreDec;
	}
	
    /**
     * @return grpCorreDec
     */
	public String getGrpCorreDec() {
		return this.grpCorreDec;
	}
	
	/**
	 * @param isMainCus
	 */
	public void setIsMainCus(String isMainCus) {
		this.isMainCus = isMainCus;
	}
	
    /**
     * @return isMainCus
     */
	public String getIsMainCus() {
		return this.isMainCus;
	}
	
	/**
	 * @param grpCaseMemo
	 */
	public void setGrpCaseMemo(String grpCaseMemo) {
		this.grpCaseMemo = grpCaseMemo;
	}
	
    /**
     * @return grpCaseMemo
     */
	public String getGrpCaseMemo() {
		return this.grpCaseMemo;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param availableInd
	 */
	public void setAvailableInd(String availableInd) {
		this.availableInd = availableInd;
	}
	
    /**
     * @return availableInd
     */
	public String getAvailableInd() {
		return this.availableInd;
	}
	
	/**
	 * @param sociCredCode
	 */
	public void setSociCredCode(String sociCredCode) {
		this.sociCredCode = sociCredCode;
	}
	
    /**
     * @return sociCredCode
     */
	public String getSociCredCode() {
		return this.sociCredCode;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getCusCatalog() {
		return cusCatalog;
	}

	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}

	public String getReserveGrpLimitInd() {
		return reserveGrpLimitInd;
	}

	public void setReserveGrpLimitInd(String reserveGrpLimitInd) {
		this.reserveGrpLimitInd = reserveGrpLimitInd;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
}