package cn.com.yusys.yusp.web.server.xdkh0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0008.req.Xdkh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0008.resp.Xdkh0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0008.Xdkh0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:集团关联信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0008:集团关联信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0008Resource.class);

    @Resource
    private Xdkh0008Service xdkh0008Service;

    /**
     * 交易码：xdkh0008
     * 交易描述：集团关联信息查询
     *
     * @param xdkh0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("集团关联信息查询")
    @PostMapping("/xdkh0008")
    protected @ResponseBody
    ResultDto<Xdkh0008DataRespDto> xdkh0008(@Validated @RequestBody Xdkh0008DataReqDto xdkh0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, JSON.toJSONString(xdkh0008DataReqDto));
        Xdkh0008DataRespDto xdkh0008DataRespDto = new Xdkh0008DataRespDto();// 响应Dto:集团关联信息查询
        ResultDto<Xdkh0008DataRespDto> xdkh0008DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, JSON.toJSONString(xdkh0008DataReqDto));
            xdkh0008DataRespDto = xdkh0008Service.getXdht0008(xdkh0008DataReqDto.getCusId());
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, JSON.toJSONString(xdkh0008DataRespDto));
            // 封装xdkh0008DataResultDto中正确的返回码和返回信息
            xdkh0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, e.getMessage());
            // 封装xdkh0008DataResultDto中异常返回码和返回信息
            xdkh0008DataResultDto.setCode(e.getErrorCode());
            xdkh0008DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, e.getMessage());
            xdkh0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0008DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0008DataRespDto到xdkh0008DataResultDto中
        xdkh0008DataResultDto.setData(xdkh0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, JSON.toJSONString(xdkh0008DataResultDto));
        return xdkh0008DataResultDto;
    }
}
