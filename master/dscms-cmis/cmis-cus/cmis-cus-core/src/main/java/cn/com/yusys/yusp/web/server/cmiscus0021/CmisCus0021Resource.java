package cn.com.yusys.yusp.web.server.cmiscus0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import cn.com.yusys.yusp.dto.server.cmiscus0021.req.CmisCus0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0021.resp.CmisCus0021RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusLstYndJyxxAppService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 优农贷（经营信息）记录生成
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0021:优农贷（经营信息）记录生成")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0021Resource.class);

    @Autowired
    private CusLstYndJyxxAppService cusLstYndJyxxAppService;

    /**
     * 交易码：cmiscus0021
     * 交易描述：优农贷（经营信息）记录生成
     *
     * @param cmisCus0021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷（经营信息）记录生成")
    @PostMapping("/cmiscus0021")
    protected @ResponseBody
    ResultDto<CmisCus0021RespDto> cmiscus0021(@Validated @RequestBody CmisCus0021ReqDto cmisCus0021ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0021.key, DscmsEnum.TRADE_CODE_CMISCUS0021.value, JSON.toJSONString(cmisCus0021ReqDto));
        CmisCus0021RespDto cmisCus0021RespDto = new CmisCus0021RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0021RespDto> cmisCus0021ResultDto = new ResultDto<>();
        try {
            CusLstYndJyxxApp record = new CusLstYndJyxxApp();
            BeanUtils.copyProperties(cmisCus0021ReqDto, record); // 参数复制
            // 封装cmisCus0021ResultDto中正确的返回码和返回信息
            int count = cusLstYndJyxxAppService.insertSelective(record);
            cmisCus0021RespDto.setResult(count);
            cmisCus0021ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0021ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0021.key, DscmsEnum.TRADE_CODE_CMISCUS0021.value, e.getMessage());
            // 封装cmisCus0021ResultDto中异常返回码和返回信息
            cmisCus0021ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0021ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0021RespDto到cmisCus0021ResultDto中
        cmisCus0021ResultDto.setData(cmisCus0021RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0021.key, DscmsEnum.TRADE_CODE_CMISCUS0021.value, JSON.toJSONString(cmisCus0021ResultDto));
        return cmisCus0021ResultDto;
    }
}
