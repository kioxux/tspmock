/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusIndivContactMapper;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: CusIndivService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: yangx
 * @创建时间: 2020-11-14 14:41:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CusIndivService {
    private static final Logger log = LoggerFactory.getLogger(CusIndivService.class);

    @Autowired
    private CusIndivMapper cusIndivMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateService;
    @Autowired
    private CusBaseService cusBaseService;
    @Autowired
    private CusIndivContactService cusIndivContactService;
    @Autowired
    private CusIndivUnitService cusIndivUnitService;
    @Autowired
    private IGuarClientService iGuarClientService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;
    @Autowired
    private CusManaTaskService cusManaTaskService;
    @Autowired
    private CusLstGlfService cusLstGlfService;
    @Autowired
    private CusIndivAttrService cusIndivAttrService;
    @Autowired
    private Dscms2OuterdataClientService dscms2ImageClientService;
    @Autowired
    private CusIndivSocialService cusIndivSocialService;
    @Autowired
    private CusBaseMapper cusBaseMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusIndiv selectByPrimaryKey(String cusId) {
        CusIndiv cusIndiv = cusIndivMapper.selectByPrimaryKey(cusId);
        return cusIndiv;
    }

    /**
     *
     *
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusIndiv> selectAll(QueryModel model) {
        List<CusIndiv> records = (List<CusIndiv>) cusIndivMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusIndiv> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIndiv> list = cusIndivMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusIndiv record) {
        return cusIndivMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusIndiv record) {
        return cusIndivMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusIndiv record) {
        return cusIndivMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     * @修改人： 周茂伟
     */

    public int updateSelective(CusIndiv record) {
        // 修改人
        String updId = "";
        // 修改机构
        String updBrId = "";
        // 修改日期
        String updDate = "";
        // 修改时间
        Date updateTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        if(userInfo != null){
            // 修改人
            updId = userInfo.getLoginCode();
            // 修改机构
            updBrId = userInfo.getOrg().getCode();
            // 修改日期
            updDate = DateUtils.getCurrDateStr();
        }
        record.setUpdId(updId);
        record.setUpdBrId(updBrId);
        record.setUpdDate(updDate);
        record.setUpdateTime(updateTime);
        return cusIndivMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId) {
        return cusIndivMapper.deleteByPrimaryKey(cusId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusIndivMapper.deleteByIds(ids);
    }

    /**
     * 个人客户引入引导
     *
     * @param indivCusCreateDto
     * @
     */
    public IndivCusCreateDto queryCusIndivImport(IndivCusCreateDto indivCusCreateDto) {
        return cusIndivMapper.selectCusIndivDto(indivCusCreateDto);
    }

    /**
     * 发送Ecif接口
     *
     * @return
     */
    public boolean sendEcif(IndivCusCreateDto indivCusCreateDto) {
        if ("123456789|32342333212313|350526199807231013|998665|19990123".indexOf(indivCusCreateDto.getCertCode()) > -1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取客户关系
     *
     * @param cusRelationsDto
     * @return
     */
    public CusRelationsDto cusRelations(CusRelationsRequestDto cusRelationsRequestDto, CusRelationsDto cusRelationsDto) {
        String custid = cusRelationsRequestDto.getCusId();
        String cusLevel = cusRelationsRequestDto.getCusLevel();
        if (StringUtils.isBlank(custid)) {
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"客户编号\"" + EcsEnum.E_CUS_INFO_ISNULL.value);
        }
        if (StringUtils.isBlank(cusLevel)) {
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"层级\"" + EcsEnum.E_CUS_INFO_ISNULL.value);
        }
        int level = Integer.parseInt(cusLevel);
        /** 获取基本信息 **/
        if (cusRelationsDto == null) {
            cusRelationsDto = cusBaseService.findCusBaseByCusId(custid);
        }
        //将字符串转换成符合SQL查询的条件值
        convert(cusRelationsRequestDto);
        /** 获取担保信息**/
        CusIndivObisAssureDto cusIndivObisAssureDto = cusIndivObisAssureRelations(cusRelationsRequestDto);
//        CusIndivObisAssureDto cusIndivObisAssureDto = cusIndivObisAssureService.CusIndivObisAssureRelations(cusRelationsRequestDto);
        cusRelationsDto.setCusIndivObisAssureDto(cusIndivObisAssureDto);
        level--;
        if (level == 0) {
            return cusRelationsDto;
        }

        /** 获取亲友信息 **/
        if (cusRelationsDto.getCusIndivFamilyInfoDto() != null) {
            List<CusRelationsDto> cusRelationsDtos = cusRelationsDto.getCusIndivFamilyInfoDto().getCusRelationsDto();
            if (!CollectionUtils.isEmpty(cusRelationsDtos) || cusRelationsDtos.size() > 0) {
                for (CusRelationsDto cusRelationsDtoTemp : cusRelationsDtos) {
                    cusRelationsRequestDto.setCusId(cusRelationsDtoTemp.getCusId());
                    cusRelationsRequestDto.setCusLevel(String.valueOf(level));
                    cusRelations(cusRelationsRequestDto, cusRelationsDtoTemp);
                }
            }
        }
        return cusRelationsDto;
    }

    /**
     * 担保关系（担保他人（借款人）信息）
     *
     * @param cusRelationsRequestDto
     * @return
     */
    public CusIndivObisAssureDto cusIndivObisAssureRelations(CusRelationsRequestDto cusRelationsRequestDto) {
        String cusId = cusRelationsRequestDto.getCusId();
        //1、调用押品服务，获取押品详细（保证）信息中保证人信息
        GuarInfGuarantorClientDto guarInfGuarantorClientDto = new GuarInfGuarantorClientDto();
        guarInfGuarantorClientDto.setGuareeNo(cusId);
        List<GuarInfGuarantorClientDto> GuarInfGuarantorDtos = iGuarClientService.getGuarInfGuarantor(guarInfGuarantorClientDto);
        //2、调用BIZ服务中担保合同中的借款人ID
        CusIndivObisAssureDto cusIndivObisAssureDto = new CusIndivObisAssureDto();
        cusIndivObisAssureDto.setUpperCusId(cusId);
        List<CusRelationsDto> cusRelationsDto = new ArrayList<CusRelationsDto>();
        if (!CollectionUtils.isEmpty(GuarInfGuarantorDtos) && GuarInfGuarantorDtos.size() > 0) {
            for (GuarInfGuarantorClientDto guarInfGuarantorDtoTmp : GuarInfGuarantorDtos) {
                GrtGuarContClientDto grtGuarContClientDto = new GrtGuarContClientDto();
                grtGuarContClientDto.setGuarNo(guarInfGuarantorDtoTmp.getGuarNo());
                List<GrtGuarContClientDto> grtGuarContClientDtos = cmisBizClientService.getGrtGuarCont(grtGuarContClientDto);
                if (CollectionUtils.isEmpty(grtGuarContClientDtos) && grtGuarContClientDtos.size() > 0) {
                    //3、根据借款人ID获取借款人的详细信息
                    for (GrtGuarContClientDto grtGuarContClientDtoTmp : grtGuarContClientDtos) {
                        CusRelationsDto cusRelationsDtoTmp = cusBaseService.findCusBaseByCusId(grtGuarContClientDtoTmp.getBorrowerId());
                        cusRelationsDtoTmp.setRelations(grtGuarContClientDtoTmp.getGuarWay());
                        cusRelationsDto.add(cusRelationsDtoTmp);
                    }
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
        cusIndivObisAssureDto.setCusRelationsDto(cusRelationsDto);
        return cusIndivObisAssureDto;
    }

    private void convert(CusRelationsRequestDto cusRelationsRequestDto) {
        String comRelations = cusRelationsRequestDto.getComRelations();
        String famRelations = cusRelationsRequestDto.getFamRelations();
        String assureRelations = cusRelationsRequestDto.getAssureRelations();
        if (StringUtils.nonBlank(comRelations) && comRelations.contains(";")) {
            comRelations = comRelations.substring(0, comRelations.lastIndexOf(";"));
            String tempArr[] = comRelations.split(";");
            String tempStr = "";
            for (String str : tempArr) {
                tempStr = tempStr + "'" + str + "',";
            }
            comRelations = tempStr.substring(0, tempStr.lastIndexOf(","));
            cusRelationsRequestDto.setComRelations(comRelations);
        }

        if (StringUtils.nonBlank(famRelations) && famRelations.contains(";")) {
            famRelations = famRelations.substring(0, famRelations.lastIndexOf(";"));
            String trmpArr[] = famRelations.split(";");
            String tempStr = "";
            for (String str : trmpArr) {
                tempStr = tempStr + "'" + str + "',";
            }
            famRelations = tempStr.substring(0, tempStr.lastIndexOf(","));
            cusRelationsRequestDto.setFamRelations(famRelations);
        }


        if (StringUtils.nonBlank(assureRelations) && assureRelations.contains(";")) {
            assureRelations = assureRelations.substring(0, assureRelations.lastIndexOf(";"));
            String trmpArr[] = assureRelations.split(";");
            String tempStr = "";
            for (String str : trmpArr) {
                tempStr = tempStr + "'" + str + "',";
            }
            assureRelations = tempStr.substring(0, tempStr.lastIndexOf(","));
            cusRelationsRequestDto.setAssureRelations(assureRelations);
        }

    }

    /**
     * 个人客户创建向导
     * 以及Ecif校验
     * @param cusIndivDto
     * @author 周茂伟
     */
    @Transactional
    public CusIndivDto createCusIndiv(CusIndivDto cusIndivDto) {
        if (cusIndivDto != null) {
            String cusId="";
            // 如果ECIF系统返回ECIF客户编号及客户信息，则直接添加这个客户信息到任务表并跳转到信息录入页签
            User userInfo = SessionUtils.getUserInformation();
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            // 第一部 查询客户基本信息表，通过证件类型证件号码判断客户是否在信贷系统存在，不存在则继续后面步骤
            CusBase cusBase = cusBaseService.selectByCertCode(cusIndivDto);
            if (cusBase != null && cusBase.getCusRankCls() == null) {
                // 存在正式客户信息
                throw BizException.error(null,"9999","存在该证件号码的客户信息，请核对！");
            }else if (cusBase != null && cusBase.getCusRankCls().equals("01")) {
                // 已经存在正式客户信息
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,EcsEnum.E_CUS_INFO_EXIST.value);
            }else  if (cusBase != null && cusBase.getCusRankCls().equals("02")) {
                // 已经存在临时客户信息
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST_TMP.key,EcsEnum.E_CUS_INFO_EXIST_TMP.value);
            }
            //证件类型为身份证的时候进行身份校验
            String certType = cusIndivDto.getCertType();
            // A 身份证
            if("A".equals(certType)){
                // 第二步 校验客户信息  发送身份校验
                IdCheckReqDto idCheckReqDto = new IdCheckReqDto();
                // 证件类型 0101身份证
                idCheckReqDto.setCert_type("0101");//(cusIndivDto.getCertType()
                // 用户名称
                idCheckReqDto.setUser_name(cusIndivDto.getCusName());
                // 证件号码
                idCheckReqDto.setId_number(cusIndivDto.getCertCode());
                // 机构类型 2：银行营业厅
                idCheckReqDto.setSource_type("2");
                // 网上营业厅申请人IP地址  StringUtils.EMPTY 待确认
                idCheckReqDto.setIp("10.87.3.37");//StringUtils.EMPTY
                ResultDto<IdCheckRespDto> idCheckResultDto = dscms2ImageClientService.idCheck(idCheckReqDto);
                String idCheckCode = Optional.ofNullable(idCheckResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String  idCheckMeesage = Optional.ofNullable(idCheckResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                IdCheckRespDto idCheckRespDto = null;
                if (Objects.equals(idCheckCode, SuccessEnum.CMIS_SUCCSESS.key)) {
                    //  获取相关的值并解析
                    idCheckRespDto = idCheckResultDto.getData();
                    if(!EcsEnum.CUS_INDIV_CHECK_RESULT00.key.equals(idCheckRespDto.getCheckresult())){
                        throw BizException.error(null,idCheckCode,idCheckMeesage);
                    }
                    log.info("个人客户信息idCheck校验成功，客户名称[{}]证件号码[{}]",cusIndivDto.getCusName(), cusIndivDto.getCertCode());
                } else {
                    //抛出错误异常
                    throw BizException.error(null,idCheckCode,EcsEnum.CUS_INDIV_CHECK_RESULT01.value + idCheckMeesage);
                }
            }
            // 查询成功，判断查询结果
//            if(EcsEnum.CUS_INDIV_CHECK_RESULT00.key.equals(idCheckRespDto.getCheckresult())){
            // 第三步 再第一步成功的基础上，进行客户三要素发送ecfi查询，
            //识别方式:
            //1- 按证件类型、证件号码查询
            //2- 按证件号码查询
            //3- 按客户编号查询
            //4- 按客户名称模糊查询
            //以下接口必填一种查询方式；
            String cusState = "";
            S10501ReqDto s10501ReqDto = new S10501ReqDto();
            s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
            s10501ReqDto.setIdtftp(cusIndivDto.getCertType());//   证件类型
            s10501ReqDto.setIdtfno(cusIndivDto.getCertCode());//   证件号码
            //  StringUtils.EMPTY的实际值待确认 开始
            // 发送客户三要素发送ecfi查询
            ResultDto<S10501RespDto> s10501ResultDto = dscms2EcifClientService.s10501(s10501ReqDto);
            String s10501Code = Optional.ofNullable(s10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String s10501Meesage = Optional.ofNullable(s10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            S10501RespDto s10501RespDto = null;
            if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                //  获取查询结果 todo联调验证交易成功查询失败情况
                s10501RespDto = s10501ResultDto.getData();
                if(s10501RespDto.getListnm() !=null){
                    //cusId=StringUtils.getUUID();
                    List<ListArrayInfo> listArrayInfo =s10501RespDto.getListArrayInfo();
                    for(int i=0;i<listArrayInfo.size();i++){
                        // 客户号
                        cusId=listArrayInfo.get(0).getCustno();
                        cusIndivDto.setCusId(cusId);
                        if(!StringUtils.isEmpty(cusId)){
                            cusIndivDto.setIsEcif("1");
                        }
                    }
                    // 保存任务信息
                    CusManaTask cusManaTask = new CusManaTask();
                    BeanUtils.copyProperties(cusIndivDto, cusManaTask);
                    // 任务状态 1待处理
                    cusManaTask.setTaskStatus(CmisCusConstants.STD_TASK_STATUS_1);
                    // 审批状态 000 待发起
                    cusManaTask.setApproveStatus(CmisCusConstants.APPLY_STATE_TODO);
                    // 申请人
                    cusManaTask.setInputId(inputId);
                    // 申请机构
                    cusManaTask.setInputBrId(inputBrId);
                    // 申请时间
                    cusManaTask.setInputDate(inputDate);
                    // 创建时间
                    cusManaTask.setCreateTime(createTime);
                    // 修改人
                    cusManaTask.setUpdId(inputId);
                    // 修改机构
                    cusManaTask.setUpdBrId(inputBrId);
                    // 修改日期
                    cusManaTask.setUpdDate(inputDate);
                    // 修改时间
                    cusManaTask.setUpdateTime(createTime);
                    String results = cusManaTaskService.save(cusManaTask);
                    if (!StringUtils.isEmpty(results)) {
                        cusBase = new CusBase();
                        // 保存客户信息到cus_base客户基本信息表
                        BeanUtils.copyProperties(cusIndivDto, cusBase);
                        if("B01".equals(cusIndivDto.getBizType())){
                            cusBase.setCusRankCls("01");
                        }else if("B02".equals(cusIndivDto.getBizType())){
                            cusBase.setCusRankCls("02");
                        }
                        // 客户状态 1暂存
                        cusBase.setCusState(CmisCusConstants.STD_CUS_STATE_1);
                        //客户大类 2 个人
                        cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PRI);
                        // 申请人
                        cusBase.setInputId(inputId);
                        //管户人
                        cusBase.setManagerId(inputId);
                        // 申请机构
                        cusBase.setInputBrId(inputBrId);
                        // 管户机构
                        cusBase.setManagerBrId(inputBrId);
                        // 申请时间
                        cusBase.setInputDate(inputDate);
                        // 创建时间
                        cusBase.setCreateTime(createTime);
                        // 修改人
                        cusBase.setUpdId(inputId);
                        // 修改机构
                        cusBase.setUpdBrId(inputBrId);
                        // 修改日期
                        cusBase.setUpdDate(inputDate);
                        // 修改时间
                        cusBase.setUpdateTime(createTime);
                        //取系统日期
                        String openday = stringRedisTemplate.opsForValue().get("openDay");//开户日期
                        cusBase.setOpenDate(openday);
                        int result= cusBaseService.insertSelective(cusBase);
                        if(result>0){
                            // 判断是否我行股东
                            int count = queryCountByCertCode(cusBase.getCertCode());
                            if (count == 1) {
                                cusIndivDto.setIsBankSharehd(CmisCusConstants.COMMON_FLAG_YES);
                            } else {
                                cusIndivDto.setIsBankSharehd(CmisCusConstants.COMMON_FLAG_NO);
                            }
                            cusIndivDto.setSerno(results);
                            /*
                             * 5.3 保存cusIndiv表
                             */
                            CusIndiv cusIndiv = new CusIndiv();
                            BeanUtils.copyProperties(cusIndivDto, cusIndiv);
                            cusIndivMapper.insertSelective(cusIndiv);
                            return cusIndivDto;
                        } else {
                            throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                        }
                    } else {
                        throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存任务信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                    }
                }else{
                    // 交易失败
                    // 判断如果是临时客户 可直接开户 01正式 02 临时
                    // 如果是正式客户 直接返回，并提示
                    if("01".equals(cusIndivDto.getCusRankCls())){
                        throw BizException.error(null,EcsEnum.CUS_INDIV_QUERY.key,EcsEnum.CUS_INDIV_QUERY.value);
                    }
                    // 若ecif系统检验不通过，则发送开户接口进行开户
                    S00102ReqDto s00102ReqDto = new S00102ReqDto();
                    // 客户名称
                    s00102ReqDto.setCustna(cusIndivDto.getCusName());
                    // 证件号码
                    s00102ReqDto.setIdtfno(cusIndivDto.getCertCode());//cusIndivDto.getCertType()
                    // 证件类型
                    s00102ReqDto.setIdtftp(cusIndivDto.getCertType());
                    // 客户状态
                    s00102ReqDto.setCustst("3");

                    s00102ReqDto.setCtcktg("1");

                    s00102ReqDto.setResult("00");
                    // 发送Ecif开户
                    ResultDto<S00102RespDto> s00102ResultDto = dscms2EcifClientService.s00102(s00102ReqDto);
                    String g00102Code = Optional.ofNullable(s00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String g00102Meesage = Optional.ofNullable(s00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    S00102RespDto s00102RespDto = null;
                    if (Objects.equals(g00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //  获取相关的值并解析
                        s00102RespDto = s00102ResultDto.getData();
                        if(s00102RespDto!=null){
                            cusId=s00102RespDto.getCustno();
                            cusIndivDto.setCusId(cusId);
                            // 如果ECIF系统返回ECIF客户编号及客户信息，则直接添加这个客户信息到任务表并跳转到信息录入页签
                            CusManaTask cusManaTask = new CusManaTask();
                            BeanUtils.copyProperties(cusIndivDto, cusManaTask);
                            // 任务状态 1待处理
                            cusManaTask.setTaskStatus(CmisCusConstants.STD_TASK_STATUS_1);
                            // 审批状态 000 待发起
                            cusManaTask.setApproveStatus(CmisCusConstants.APPLY_STATE_TODO);
                            // 申请人
                            cusManaTask.setInputId(inputId);
                            // 申请机构
                            cusManaTask.setInputBrId(inputBrId);
                            // 申请时间
                            cusManaTask.setInputDate(inputDate);
                            // 创建时间
                            cusManaTask.setCreateTime(createTime);
                            // 修改人
                            cusManaTask.setUpdId(inputId);
                            // 修改机构
                            cusManaTask.setUpdBrId(inputBrId);
                            // 修改日期
                            cusManaTask.setUpdDate(inputDate);
                            // 修改时间
                            cusManaTask.setUpdateTime(createTime);
                            String results = cusManaTaskService.save(cusManaTask);
                            if (!StringUtils.isEmpty(results)) {
                                cusBase = new CusBase();
                                // 保存客户信息到cus_base客户基本信息表
                                BeanUtils.copyProperties(cusIndivDto, cusBase);
                                // 客户状态 1暂存
                                cusBase.setCusState(CmisCusConstants.STD_CUS_STATE_1);
                                //客户大类 2 个人
                                cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PRI);
                                // 申请人
                                cusBase.setInputId(inputId);
                                //管户人
                                cusBase.setManagerId(inputId);
                                // 申请机构
                                cusBase.setInputBrId(inputBrId);
                                // 管户机构
                                cusBase.setManagerBrId(inputBrId);
                                // 申请时间
                                cusBase.setInputDate(inputDate);
                                // 创建时间
                                cusBase.setCreateTime(createTime);
                                // 修改人
                                cusBase.setUpdId(inputId);
                                // 修改机构
                                cusBase.setUpdBrId(inputBrId);
                                // 修改日期
                                cusBase.setUpdDate(inputDate);
                                // 修改时间
                                cusBase.setUpdateTime(createTime);
                                //取系统日期
                                String openday = stringRedisTemplate.opsForValue().get("openDay");//开户日期
                                cusBase.setOpenDate(openday);
                                if(StringUtils.isEmpty(cusBase.getCusId())){
                                    throw BizException.error(null,EcsEnum.ECS040008.key,EcsEnum.ECS040008.value);
                                }
                                int result= cusBaseService.insertSelective(cusBase);
                                if(result>0){
                                    // 判断是否我行股东
                                    int count = queryCountByCertCode(cusBase.getCertCode());
                                    if (count == 1) {
                                        cusIndivDto.setIsBankSharehd(CmisCusConstants.COMMON_FLAG_YES);
                                    } else {
                                        cusIndivDto.setIsBankSharehd(CmisCusConstants.COMMON_FLAG_NO);
                                    }
                                    cusIndivDto.setSerno(results);
                                    /*
                                     * 5.3 保存cusIndiv表
                                     */
                                    CusIndiv cusIndiv = new CusIndiv();
                                    BeanUtils.copyProperties(cusIndivDto, cusIndiv);
                                    cusIndivMapper.insertSelective(cusIndiv);
                                    return cusIndivDto;
                                } else {
                                    throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                                }
                            } else {
                                throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                            }
                        }
                    } else {
                        //  抛出错误异常
                        throw BizException.error(null,g00102Code,EcsEnum.CUS_INDIV_CREATE_RESULT.value+g00102Meesage);
                    }
                }
            } else {
                //  抛出错误异常
                throw BizException.error(null,s10501Code,EcsEnum.CUS_INDIV_QUERY_RESULT.value+s10501Meesage);
            }
        }else{
            throw BizException.error(null,EcsEnum.CUS_INDIV_CHECK_RESULT01.key,EcsEnum.CUS_INDIV_CHECK_RESULT01.value);
        }
//        } else {
//            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"传入值为空\"" + EcsEnum.E_CUS_INFO_ISNULL.value);
//        }
        return null;
    }

    /**
     * @方法名称: selectRankScoreByCusId
     * @方法描述: 通过客户号查询评级信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CusIndivRankScoreDto selectRankScoreByCusId(CusIndivDto cusIndivDto) {
        String cusId="";
        CusIndivRankScoreDto cusIndivRankScoreDto = null;
        if(cusIndivDto != null){
            cusId = cusIndivDto.getCusId();
            cusIndivRankScoreDto = cusIndivMapper.selectRankScoreByCusId(cusId);
            String age = figureUpAge(cusIndivRankScoreDto);
            cusIndivRankScoreDto.setAge(age);
        }
        return cusIndivRankScoreDto;
    }

    /**
     * 个人客户 基本信息保存
     *
     * @param cusIndivDto
     * @return 缺更新时间待修复
     */
    public int saveCusIndiv(CusIndivDto cusIndivDto){
        // 获取用户信息
        User userInfo = SessionUtils.getUserInformation();
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String inputDate = "";
        // 创建时间
        Date createTime = new Date();
        if (userInfo != null) {
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
            // 申请时间
            inputDate = DateUtils.getCurrDateStr();
            // 创建时间
            createTime = DateUtils.getCurrTimestamp();
        }
        int result = 0;
        // 客户号
        String cusId = "";
        if (cusIndivDto != null) {
            if (StringUtils.nonEmpty(cusIndivDto.getCusId())) {
                cusId = cusIndivDto.getCusId();
            }
            CusIndiv cusIndiv = selectByPrimaryKey(cusId);
            if (cusIndiv != null) {//有则修改
                BeanUtils.copyProperties(cusIndivDto, cusIndiv);
                //修改人
                cusIndiv.setUpdId(inputId);
                //修改机构
                cusIndiv.setUpdBrId(inputBrId);
                //修改日期
                cusIndiv.setUpdDate(inputDate);
                //修改时间
                cusIndiv.setUpdateTime(createTime);
                result = this.cusIndivMapper.updateByPrimaryKeySelective(cusIndiv);
            } else { //无责新增
                cusIndiv = new CusIndiv();
                BeanUtils.copyProperties(cusIndivDto, cusIndiv);
                // 获取用户信息
                // 申请人
                cusIndiv.setInputId(inputId);
                // 申请机构
                cusIndiv.setInputBrId(inputBrId);
                // 申请时间
                cusIndiv.setInputDate(inputDate);
                // 创建时间
                cusIndiv.setCreateTime(createTime);
                //修改人
                cusIndiv.setUpdId(inputId);
                //修改机构
                cusIndiv.setUpdBrId(inputBrId);
                //修改日期
                cusIndiv.setUpdDate(inputDate);
                //修改时间
                cusIndiv.setUpdateTime(createTime);
                result =this.cusIndivMapper.insertSelective(cusIndiv);
            }
            // 如果保存成功保存数据到cus_base
            if (result == 1) {
                CusBase cusBase = cusBaseService.selectByPrimaryKey(cusId);
                if (cusBase != null) {
                    BeanUtils.copyProperties(cusIndivDto, cusBase);
                    //修改人
                    cusBase.setUpdId(inputId);
                    //修改机构
                    cusBase.setUpdBrId(inputBrId);
                    //修改日期
                    cusBase.setUpdDate(inputDate);
                    //修改时间
                    cusBase.setUpdateTime(createTime);
                    result = cusBaseService.updateSelective(cusBase);
                } else {
                    cusBase = new CusBase();
                    BeanUtils.copyProperties(cusIndivDto, cusBase);
                    // 申请人
                    cusBase.setInputId(inputId);
                    // 申请机构
                    cusBase.setInputBrId(inputBrId);
                    // 申请时间
                    cusBase.setInputDate(inputDate);
                    // 创建时间
                    cusBase.setCreateTime(createTime);
                    //修改人
                    cusBase.setUpdId(inputId);
                    //修改机构
                    cusBase.setUpdBrId(inputBrId);
                    //修改日期
                    cusBase.setUpdDate(inputDate);
                    //修改时间
                    cusBase.setUpdateTime(createTime);
                    result = cusBaseService.insertSelective(cusBase);
                }
            }
        }
        return result;
    }

    /**
     * 查询是否我行股东
     *
     * @return
     * @paramcusId
     * @author 周茂伟
     * @date 2021年4月25日23:49:18
     */
    public int queryCountByCertCode(String certCode) {
        int count = 0;
        if (StringUtils.nonEmpty(certCode)) {
            count = cusLstGlfService.selectCount(certCode);
        }
        return count;
    }

    /**
     * @方法名称: update
     * @方法描述: 根据客户号修改建立信贷时间
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateComInitLoanDate(CusIndivReqClientDto cusIndivDto) {
        return cusIndivMapper.updateComInitLoanDateBycusId(cusIndivDto);
    }

    public List<Map<String, Object>> selectByModelxp(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = cusIndivMapper.selectByModelxp(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryindivCus
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     * @author zhoumw
     */

    public List<Map<String, Object>> queryindivCus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = cusIndivMapper.queryindivCus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryCusIndivInfo
     * @方法描述: 个人客户详细信息查询
     * @参数与返回说明: IndivCusQueryDto
     * @算法描述: 无
     * @创建人：周茂伟
     */
    public IndivCusQueryDto queryCusIndivInfo(CusIndivDto cusIndivDto) {
        if(StringUtils.isBlank(cusIndivDto.getCusId())){
            throw new YuspException(EcsEnum.ECS040002.key,EcsEnum.ECS040002.value);
        }
        IndivCusQueryDto indivCusQueryDto = cusIndivMapper.queryCusIndivInfo(cusIndivDto.getCusId());
        return indivCusQueryDto;
    }

    /**
     * @方法名称: getCertByCodeAndType
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusIndivDto getCertByCodeAndType(CusBase cusBase) {
        return cusIndivMapper.getCertByCodeAndType(cusBase);
    }


    /**
     * @方法名称: saveAllCusIndiv
     * @方法描述: 保存所有客户信息包括4个tab
     * @参数与返回说明: int
     * @算法描述: 无
     * @创建人：周茂伟
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveAllCusIndiv(CusIndivAllDto cusIndivAllDto) {
        int result = 0;
        if (cusIndivAllDto != null) {
            String cusId = cusIndivAllDto.getCusId();
            // 保存个人基础信息
            CusIndivDto cusIndivDto = new CusIndivDto();
            BeanUtils.copyProperties(cusIndivAllDto, cusIndivDto);
            //是否农户标识特殊处理
            cusIndivDto.setAgriFlg(cusIndivAllDto.getIsAgri());
            result = saveCusIndiv(cusIndivDto);
            if (result > 0) {
                // 保存联系信息
                CusIndivContact cusIndivContact = new CusIndivContact();
                BeanUtils.copyProperties(cusIndivAllDto, cusIndivContact);
                result = cusIndivContactService.save(cusIndivContact);
                // 保存工作信息
                CusIndivUnit cusIndivUnit = new CusIndivUnit();
                BeanUtils.copyProperties(cusIndivAllDto, cusIndivUnit);
                result = cusIndivUnitService.save(cusIndivUnit);
                // 保存客户属性
                CusIndivAttr cusIndivAttr = new CusIndivAttr();
                BeanUtils.copyProperties(cusIndivAllDto, cusIndivAttr);
                result = cusIndivAttrService.save(cusIndivAttr);
                // 修改客户基础表
                CusBase cusBase = new CusBase();
                BeanUtils.copyProperties(cusIndivAllDto, cusBase);
                result = cusBaseService.updateSelective(cusBase);
                // 如果是提交，则修改任务状态
                if (StringUtils.nonEmpty(cusIndivAllDto.getTaskStatus()) && CmisCusConstants.STD_TASK_STATUS_3.equals(cusIndivAllDto.getTaskStatus())) {
                    //提交后同步客户信息至Ecif，s00102
                    S00102ReqDto s00102ReqDto = new S00102ReqDto();
                    // 客户名称
                    s00102ReqDto.setCustna(cusIndivDto.getCusName());
                    // 证件号码
                    s00102ReqDto.setIdtfno(cusIndivDto.getCertCode());//cusIndivDto.getCertType()
                    // 证件类型
                    s00102ReqDto.setIdtftp(cusIndivDto.getCertType());
                    /**个人客户管理业务类型字典
                     B01	个人正式客户创建
                     B02	个人临时客户创建
                     B03	个人正式客户信息维护
                     B04	个人临时客户信息维护
                     B05	个人客户财务报表新增
                     B06	个人客户财务报表修改
                     B07	个人客户财务报表新增（集中作业）
                     B08	个人客户财务报表修改（集中作业）
                     B09	个人客户转正**/
                    if (cusIndivDto.getBizType().equals("B02") || cusIndivDto.getBizType().equals("B04")) {
                        s00102ReqDto.setCustst("3"); // 客户状态ECIF 临时客户,
                    } else if (cusIndivDto.getBizType().equals("B01") || cusIndivDto.getBizType().equals("B03") || cusIndivDto.getBizType().equals("B09")) {
                            s00102ReqDto.setCustst("1"); // 客户状态ECIF 正式客户
                        //增加正式客户信息创建校验规则
                        //1、婚姻状况为已婚，必须有配偶记录[{"key":"10","value":"未婚"},{"key":"20","value":"已婚"},{"key":"30","value":"丧偶"},{"key":"40","value":"离婚"}]
                        if(cusIndivDto.getMarStatus() !=null &&"20".equals(cusIndivDto.getMarStatus())){
                            QueryModel queryModel = new QueryModel();
                            queryModel.addCondition("indivCusRel","102000");
                            queryModel.addCondition("cusIdRel",cusBase.getCusId());
                            List<CusIndivSocial>  list = cusIndivSocialService.selectByModel(queryModel);
                            if(list.size()<=0){
                                throw BizException.error(null,EcsEnum.CUS_INDIV_CHECK_HYZK.key,null,EcsEnum.CUS_INDIV_CHECK_HYZK.value );

                            }
                        }
                    }

                        s00102ReqDto.setCtcktg("1");

                        s00102ReqDto.setResult("00");
                        // 客户编号
                        s00102ReqDto.setCustno(cusIndivAllDto.getCusId());
                        // 客户名称
                        s00102ReqDto.setCustna(cusIndivDto.getCusName());
                        // 证件号码
                        s00102ReqDto.setIdtfno(cusIndivDto.getCertCode());
                        //证件有效期
                        s00102ReqDto.setInefdt(DateUtils.formatDate10To8(cusIndivDto.getCertEndDt()));
                        //发证机关国家或地区
                        //s00102ReqDto.setAreaad();
                        //发证机关
                        // s00102ReqDto.setDepart();
                        //拼音名称
                        //s00102ReqDto.setCsspna();
                        //客户英文名
                        //s00102ReqDto.setCsenna();
                        //性别
                        s00102ReqDto.setPrpsex(cusIndivDto.getSex());
                        //籍贯
                        s00102ReqDto.setNation(cusIndivDto.getIndivBrtPlace());
                        //民族
                        s00102ReqDto.setEthnic(cusIndivDto.getIndivFolk());
                        //国籍
                        s00102ReqDto.setNation(cusIndivDto.getNation());
                        //出生日期
                        //s00102ReqDto.setBorndt(cusIndivDto.getIndivDtOfBirth());
                        //政治面貌
                        s00102ReqDto.setPolist(cusIndivDto.getIndivPolSt());
                        //婚姻状态
                        s00102ReqDto.setMarist(cusIndivDto.getMarStatus());
                        //是否有子女
                        //s00102ReqDto.setHavetg(cusIndivDto.getIsHaveChildren());
                        //抚养人数
                        //s00102ReqDto.setBrinnm();
                        //教育水平（学历）
                        s00102ReqDto.setEduclv(cusIndivDto.getIndivEdt());
                        //学位
                        s00102ReqDto.setIndidp(cusIndivDto.getIndivDgr());
                        //毕业院校
                        //s00102ReqDto.setGradna();
                        //毕业日期
                        //s00102ReqDto.setGraddt();
                        //职业
                        s00102ReqDto.setProjob(cusIndivUnit.getOcc());
                        //职务
                        s00102ReqDto.setPoston(cusIndivUnit.getJobTtl());
                        //职称
                        s00102ReqDto.setPosttl(cusIndivUnit.getIndivCrtfctn());
                        //任职部门
                        //s00102ReqDto.getOffibr(cusIndivUnit.getUbietyDept());
                        //工作单位
                        s00102ReqDto.setWkutna(cusIndivUnit.getUnitName());
                        //单位性质
                        s00102ReqDto.setUnquty(cusIndivUnit.getIndivComTyp());
                        //单位地址
                        s00102ReqDto.setCompad(cusIndivUnit.getUnitAddr());
                        //工作单位雇员人数
                        //s00102ReqDto.setUnitnm();
                        //参加工作日期
                        s00102ReqDto.setWorkdt(cusIndivUnit.getWorkDate());
                        //参加本行业工作日期
                        s00102ReqDto.setIdwkdt(cusIndivUnit.getUnitEndDate());
                        //参加本单位工作日期
                        s00102ReqDto.setUtwkdt(cusIndivUnit.getUnitDate());
                        //单位所属行业
                        //s00102ReqDto.setCorppr(cusIndivUnit.getIndivComTrade());
                        //行业经验年限
                        //s00102ReqDto.setExpryr(cusIndivUnit.getWorkDate());
                        //现单位工作年限
                        //s00102ReqDto.setUtwkyr();
                        //是否联网核查
                        //s00102ReqDto.setCtcktg("1");
                        //联网核查结果
                        //s00102ReqDto.setResult("0");
                        s00102ReqDto.setLivest(cusIndivContact.getIndivRsdSt());  // 居住状况
                        //s00102ReqDto.setLvbgdt(); // 本城市居住起始日期
                        //s00102ReqDto.setFamisz(); // 家庭人口
                        //s00102ReqDto.setMainic(); // 主要收入来源
                        s00102ReqDto.setCurrcy(cusIndivUnit.getEarningCurType()); // 收入币种
                        //s00102ReqDto.setPrmnic(; // 个人月收入
                        s00102ReqDto.setPryric(cusIndivUnit.getYScore()); // 个人年收入
                        //s00102ReqDto.setFmmnic(); // 家庭月收入
                        s00102ReqDto.setFmyric(cusIndivUnit.getFamilyYScore()); // 家庭年收入
                        //s00102ReqDto.setEmpltg(cusIndivDto.getIsBankEmployee); // 本行员工标志
                        //s00102ReqDto.setCtmgno(); // 本行员工号
                        s00102ReqDto.setFarmtg(cusIndivContact.getIsAgri()); // 是否农户
                        s00102ReqDto.setSthdfg(cusIndivDto.getIsBankSharehd()); // 是否我行股东
                        //s00102ReqDto.setRelgon(); // 宗教信仰
                        //s00102ReqDto.setProptg(); // 居民标志
                        //s00102ReqDto.setOnacnm(); // 一类户数量
                        //s00102ReqDto.setTotlnm(); // 总户数
                        //s00102ReqDto.setAmlrat(); // 反洗钱评级
                        //s00102ReqDto.setResiyr(); // 居住年限
                        s00102ReqDto.setHealst(cusIndivDto.getHealthStatus()); // 健康状况
                        //s00102ReqDto.setSocist(); // 社会保障情况
                        //s00102ReqDto.setHobyds(); // 爱好
                        //s00102ReqDto.setPsrttp(); // 个人评级类型
                        s00102ReqDto.setRichtg(cusIndivAttr.getIsLoan()); // 是否强村富民贷款
                        //s00102ReqDto.setVipptg(); // 是否VIP客户
                        //s00102ReqDto.setCredlv(); // 信用等级
                        //s00102ReqDto.setFscddt(); // 首次建立信贷关系年月
                        //s00102ReqDto.setCnbgdt(); // 往来起始日期
                        //s00102ReqDto.setCneddt(); // 往来终止日期
                        s00102ReqDto.setImagno(cusIndivDto.getImageNo()); // 影像编号
                        s00102ReqDto.setCtigno(cusIndivDto.getManagerId()); // 客户经理号
                        //s00102ReqDto.setCtigna(); // 客户经理姓名
                        //s00102ReqDto.setCtigph(); // 客户经理手机号
                        s00102ReqDto.setTxpstp("1"); // 税收居民类型
                        //s00102ReqDto.setAddttr(); // 税收现居英文地址
                        //s00102ReqDto.setBrctry(); // 税收出生国家或地区
                        //s00102ReqDto.setTxnion(); // 税收居民国（地区）
                        //s00102ReqDto.setTaxnum(); // 纳税人识别号
                        //s00102ReqDto.setNotxrs(); // 不能提供纳税人识别号原因
                        s00102ReqDto.setMobitl(cusIndivContact.getMobileNo()); // 移动电话
                        s00102ReqDto.setOffctl(cusIndivContact.getMobile()); // 固定电话
                        //s00102ReqDto.setCncktg(); // 联系方式验证标识
                        s00102ReqDto.setPostcd(cusIndivContact.getPostcode()); // 联系地址邮编
                        //s00102ReqDto.setCncuty(); // 联系地址所在国家
                        //s00102ReqDto.setCnprov(); // 联系地址所在省
                        //s00102ReqDto.setCncity(); // 联系地址所在市
                        //s00102ReqDto.setCnarea(); // 联系地址所在区
                        //s00102ReqDto.setHomead(); // 联系地址信息
                        s00102ReqDto.setQqnumb(cusIndivContact.getQq()); // QQ
                        s00102ReqDto.setWechat(cusIndivContact.getWechatNo()); // 微信
                        s00102ReqDto.setEmailx(cusIndivContact.getEmail()); // 邮箱
                        ///s00102ReqDto.setChflag(); // 境内标识
                        s00102ReqDto.setComadd(cusIndivContact.getIndivRsdAddr()); // 常用居住地址
                        //s00102ReqDto.setNnjytp(); // 交易类型
                        //s00102ReqDto.setNnjyna(); // 交易名称
                        // s00102ReqDto.setPrelttp();// 电话关系人
                        //s00102ReqDto.setPassno(); // 通行证号码
                        s00102ReqDto.setUphone(cusIndivUnit.getUnitPhn()); // 单位电话
                        s00102ReqDto.setCusstp(cusIndivDto.getCusType()); // 对私客户类型
                        s00102ReqDto.setResare(cusIndivContact.getRegionalism()); // 居住区域编号
                        s00102ReqDto.setResvue(cusIndivContact.getRegionName()); // 居住区域名称
                        s00102ReqDto.setResadd(cusIndivContact.getIndivRsdAddr()); // 居住地址
                        // 发送Ecif开户
                    if (cusIndivDto.getBizType().equals("B02") || cusIndivDto.getBizType().equals("B04")) {
                        //如果是临时的创建或维护时，不发送ecif
                    }else{
                        ResultDto<S00102RespDto> s00102ResultDto = dscms2EcifClientService.s00102(s00102ReqDto);
                        String g00102Code = Optional.ofNullable(s00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                        String g00102Meesage = Optional.ofNullable(s00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                        S00102RespDto s00102RespDto = null;
                        if (Objects.equals(g00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                            s00102RespDto = s00102ResultDto.getData();
                            if (s00102RespDto != null) {
                                cusIndivDto.setCusId(s00102RespDto.getCustno());
                            }
                        } else {
                            throw BizException.error(null, g00102Code, EcsEnum.CUS_INDIV_CREATE_RESULT.value + g00102Meesage);
                        }
                    }
                    if(StringUtils.isEmpty(cusIndivDto.getCusId())){
                        throw BizException.error(null,EcsEnum.ECS040008.key,EcsEnum.ECS040008.value);
                    }
                        //提交时修改待处理任务状态
                        CusManaTask cusManaTask = new CusManaTask();
                        BeanUtils.copyProperties(cusIndivAllDto, cusManaTask);
                        cusManaTask.setApproveStatus(CmisCusConstants.APPLY_STATE_PASS); // 997 审批通过不走流程
                        cusManaTask.setTaskStatus(CmisCusConstants.STD_TASK_STATUS_3);    // 3 处理完成
                        result = cusManaTaskService.updateSelective(cusManaTask);
                    }
                }
            }
            return result;
    }
    /**
     * @方法名称: exCheangeProjob
     * @方法描述: 信贷职业occ->ecif projob
     * @参数与返回说明:
     * @算法描述: 无
     * 创建人：xuxin
     */
    private String exCheangeProjob(String occ) {
        String projob = "";
        if("010101".equals(occ) || "020200".equals(occ) || "020300".equals(occ) || "030101".equals(occ) || "030201".equals(occ) || "030301".equals(occ) || "030401".equals(occ)){
            projob = "10600";
        }
        if("010106".equals(occ) || "010107".equals(occ) || "010306".equals(occ)){
            projob = "20600";
        }
        if("010214".equals(occ)){
            projob = "20700";
        }
        if("020417".equals(occ)){
            projob = "20800";
        }
        if("020418".equals(occ)){
            projob = "20900";
        }
        if("010400".equals(occ)){
            projob = "30200";
        }
        if("010600".equals(occ)||"010601".equals(occ)){
            projob = "40100";
        }
        if("011113".equals(occ)){
            projob = "40200";
        }
        if("011010".equals(occ)){
            projob = "40300";
        }
        if("010303".equals(occ) || "010504".equals(occ)){
            projob = "40400";
        }
        if("010916".equals(occ)){
            projob = "41300";
        }
        if("010211".equals(occ)){
            projob = "41400";
        }
        if("010209".equals(occ) || "020408".equals(occ) || "019901".equals(occ) || "010000".equals(occ)){
            projob = "49900";
        }
        if("020419".equals(occ)){
            projob = "50100";
        }
        if("010818".equals(occ)){
            projob = "59900";
        }
        if("011212".equals(occ)){
            projob = "62900";
        }
        if("010719".equals(occ) || "020100".equals(occ) || "020400".equals(occ) || "039901".equals(occ) || "020000".equals(occ) || "030000".equals(occ)){
            projob = "69900";
        }
        if("990101".equals(occ)){
            projob = "80001";
        }
        if("990201".equals(occ)){
            projob = "80002";
        }
        if("990301".equals(occ)){
            projob = "80003";
        }
        if("020402".equals(occ)){
            projob = "80005";
        }
        if("999999".equals(occ) || "020403".equals(occ) || "990000".equals(occ)){
            projob = "89900";
        }
        return projob;
    }

    /**
     * @方法名称: queryCusIndivByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     * 创建人：周茂伟
     */

    public List<CusIndivDto> queryCusIndivByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIndivDto> list = cusIndivMapper.queryCusIndivByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: figureUpAge
     * @方法描述: 通过证件号码计算年龄
     * @参数与返回说明:
     * @算法描述: 无
     * 创建人：周茂伟
     */
    public String figureUpAge(CusIndivRankScoreDto cusIndivRankScoreDto){
        String age="";
        if(cusIndivRankScoreDto != null){
            // 证件类型
            String certType = cusIndivRankScoreDto.getCertType();
            // A身份证
            if("A".equals(certType)){
                String certCode = cusIndivRankScoreDto.getCertCode();
                if(certCode.length()==18){
                    String dates= certCode.substring(6,10);
                    int year= DateUtils.getYear(new Date());
                    age=String.valueOf(Integer.parseInt(dates)-year);
                }
            }
        }
        return age;
    }
    /**
     * @方法名称: queryCusIndivByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     * 创建人：周茂伟
     */

    public List<CusIndivAllDto> queryAllCusIndivByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIndivAllDto> list = new ArrayList<>();

        if (!model.getCondition().isEmpty()
                && (model.getCondition().get("certNo") != null || model.getCondition().get("cusRankCls") != null)) {
            model.getCondition().put("certCode",model.getCondition().get("certNo"));
            List<CusBase> cusBaseList = cusBaseService.selectByModel(model);
            List<String> ids = new ArrayList<>();
            for (CusBase cusBase : cusBaseList) {
                ids.add(cusBase.getCusId());
            }
            if(!ids.isEmpty()) {
                list = cusIndivMapper.selectByCusIds(ids);
            }
        }else {
            list = cusIndivMapper.queryAllCusIndivByModel(model);
        }

        //组装数据
        for (CusIndivAllDto cusIndivAllDto : list) {
            String cusId = cusIndivAllDto.getCusId();

            CusBase cusBase = cusBaseService.selectByPrimaryKey(cusId);
            if(cusBase !=null) {CusBaseDataAssembly(cusBase, cusIndivAllDto);}

            CusIndivAttr cusIndivAttr = cusIndivAttrService.selectByPrimaryKey(cusId);
            if(cusIndivAttr !=null) {cusIndivAllDto.setIsLoan(cusIndivAttr.getIsLoan());}

            CusIndivContact cusIndivContact = cusIndivContactService.selectByPrimaryKey(cusId);
            if(cusIndivContact !=null) {CusIndivContactDataAssembly(cusIndivContact, cusIndivAllDto);}

            CusIndivUnit cusIndivUnit = cusIndivUnitService.selectByPrimaryKey(cusId);
            if(cusIndivUnit != null) {CusIndivUnitDataAssembly(cusIndivUnit,cusIndivAllDto);}
        }

        PageHelper.clearPage();
        return list;
    }

    private void CusBaseDataAssembly(CusBase cusBase, CusIndivAllDto cusIndivAllDto) {
        cusIndivAllDto.setCusName(cusBase.getCusName());
        cusIndivAllDto.setCertType(cusBase.getCertType());
        cusIndivAllDto.setCertCode(cusBase.getCertCode());
        cusIndivAllDto.setInputDate(cusBase.getInputDate());
        cusIndivAllDto.setCusState(cusBase.getCusState());
        cusIndivAllDto.setManagerId(cusBase.getManagerId());
        cusIndivAllDto.setManagerBrId(cusBase.getManagerBrId());
        cusIndivAllDto.setCusRankCls(cusBase.getCusRankCls());
    }

    private void CusIndivContactDataAssembly(CusIndivContact cusIndivContact, CusIndivAllDto cusIndivAllDto) {
        cusIndivAllDto.setPostcode(cusIndivContact.getPostcode());
        cusIndivAllDto.setRegionalism(cusIndivContact.getRegionalism());
        cusIndivAllDto.setRegionName(cusIndivContact.getRegionName());
        cusIndivAllDto.setMobileNo(cusIndivContact.getMobileNo());
        cusIndivAllDto.setQq(cusIndivContact.getQq());
        cusIndivAllDto.setWechatNo(cusIndivContact.getWechatNo());
        cusIndivAllDto.setFaxCode(cusIndivContact.getFaxCode());
        
        cusIndivAllDto.setEmail(cusIndivContact.getEmail());
        cusIndivAllDto.setIndivRsdAddr(cusIndivContact.getIndivRsdAddr());
        cusIndivAllDto.setIndivZipCode(cusIndivContact.getIndivZipCode());
        cusIndivAllDto.setIndivRsdSt(cusIndivContact.getIndivRsdSt());
        cusIndivAllDto.setResiTime(cusIndivContact.getResiTime());
        cusIndivAllDto.setMobile(cusIndivContact.getMobile());
        cusIndivAllDto.setSendAddr(cusIndivContact.getSendAddr());
    }

    private void CusIndivUnitDataAssembly(CusIndivUnit cusIndivUnit, CusIndivAllDto cusIndivAllDto) {
        cusIndivAllDto.setEmployeeStatus(cusIndivUnit.getEmployeeStatus());
        cusIndivAllDto.setOcc(cusIndivUnit.getOcc());
        cusIndivAllDto.setUnitName(cusIndivUnit.getUnitName());
        cusIndivAllDto.setIndivComTyp(cusIndivUnit.getIndivComTyp());
        cusIndivAllDto.setIndivComTrade(cusIndivUnit.getIndivComTrade());
        cusIndivAllDto.setUnitAddr(cusIndivUnit.getUnitAddr());
        cusIndivAllDto.setUnitPhn(cusIndivUnit.getUnitPhn());
        cusIndivAllDto.setUnitDate(cusIndivUnit.getUnitDate());
        cusIndivAllDto.setJobTtl(cusIndivUnit.getJobTtl());
        cusIndivAllDto.setIndivCrtfctn(cusIndivUnit.getIndivCrtfctn());
        cusIndivAllDto.setEarningCurType(cusIndivUnit.getEarningCurType());
        cusIndivAllDto.setIndivYearn(cusIndivUnit.getIndivYearn());
        cusIndivAllDto.setFamilyYScore(cusIndivUnit.getFamilyYScore());
        cusIndivAllDto.setIndivComTrade(cusIndivUnit.getIndivComTrade());
    }

    /**
     * @param cusIndivDto
     * @return
     * @author wzy
     * @date 2021/6/17 15:15
     * @version 1.0.0
     * @desc 信用卡预录入开户
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public CusIndivDto createCusIndivByCard(CusIndivDto cusIndivDto) {
        if (cusIndivDto != null) {
            String cusId="";
            // 如果ECIF系统返回ECIF客户编号及客户信息，则直接添加这个客户信息到任务表并跳转到信息录入页签
            User userInfo = SessionUtils.getUserInformation();
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            // 第一部 查询客户基本信息表，通过证件类型证件号码判断客户是否在信贷系统存在，返回客户号
            CusBase cusBase = cusBaseService.selectByCertCode(cusIndivDto);
            if (cusBase != null) {
                cusIndivDto.setCusId(cusBase.getCusId());
                return  cusIndivDto;
            }
            //证件类型为身份证的时候进行身份校验
            String certType = cusIndivDto.getCertType();
            // A 身份证
            if("A".equals(certType)){
                // 第二步 校验客户信息  发送身份校验
                IdCheckReqDto idCheckReqDto = new IdCheckReqDto();
                // 证件类型 0101身份证
                idCheckReqDto.setCert_type("0101");
                // 用户名称
                idCheckReqDto.setUser_name(cusIndivDto.getCusName());
                // 证件号码
                idCheckReqDto.setId_number(cusIndivDto.getCertCode());
                // 机构类型 2：银行营业厅
                idCheckReqDto.setSource_type("2");
                // 网上营业厅申请人IP地址  StringUtils.EMPTY 待确认
                idCheckReqDto.setIp("10.87.3.37");//StringUtils.EMPTY
                ResultDto<IdCheckRespDto> idCheckResultDto = dscms2ImageClientService.idCheck(idCheckReqDto);
                String idCheckCode = Optional.ofNullable(idCheckResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String  idCheckMeesage = Optional.ofNullable(idCheckResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                IdCheckRespDto idCheckRespDto = null;
                if (Objects.equals(idCheckCode, SuccessEnum.CMIS_SUCCSESS.key)) {
                    //  获取相关的值并解析
                    idCheckRespDto = idCheckResultDto.getData();
                    if(!EcsEnum.CUS_INDIV_CHECK_RESULT00.key.equals(idCheckRespDto.getCheckresult())){
                        throw BizException.error(null,idCheckCode,EcsEnum.CUS_INDIV_CHECK_RESULT01.value);
                    }
                } else {
                    //抛出错误异常
                    throw BizException.error(null,idCheckCode,EcsEnum.CUS_INDIV_CHECK_RESULT01.value + idCheckMeesage);
                }
            }
            // 查询成功，判断查询结果
//            if(EcsEnum.CUS_INDIV_CHECK_RESULT00.key.equals(idCheckRespDto.getCheckresult())){
            // 第三步 再第一步成功的基础上，进行客户三要素发送ecfi查询，
            //识别方式:
            //1- 按证件类型、证件号码查询
            //2- 按证件号码查询
            //3- 按客户编号查询
            //4- 按客户名称模糊查询
            //以下接口必填一种查询方式；
            String cusState = "";
            S10501ReqDto s10501ReqDto = new S10501ReqDto();
            s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
            s10501ReqDto.setIdtftp(cusIndivDto.getCertType());//   证件类型
            s10501ReqDto.setIdtfno(cusIndivDto.getCertCode());//   证件号码
            //  StringUtils.EMPTY的实际值待确认 开始
            // 发送客户三要素发送ecfi查询
            ResultDto<S10501RespDto> s10501ResultDto = dscms2EcifClientService.s10501(s10501ReqDto);
            String s10501Code = Optional.ofNullable(s10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String s10501Meesage = Optional.ofNullable(s10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            S10501RespDto s10501RespDto = null;
            if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                //  获取查询结果 todo联调验证交易成功查询失败情况
                s10501RespDto = s10501ResultDto.getData();
                if(s10501RespDto.getListnm() !=null){
                    //cusId=StringUtils.getUUID();
                    List<ListArrayInfo> listArrayInfo =s10501RespDto.getListArrayInfo();
                    for(int i=0;i<listArrayInfo.size();i++){
                        // 客户号
                        cusId=listArrayInfo.get(0).getCustno();
                        cusIndivDto.setCusId(cusId);
                        if(!StringUtils.isEmpty(cusId)){
                            cusIndivDto.setIsEcif("1");
                        }
                    }
                    CusIndiv cusIndiv = selectByPrimaryKey(cusId);
                    if (cusIndiv != null) {//有则修改
                        BeanUtils.copyProperties(cusIndivDto, cusIndiv);
                        //修改人
                        cusIndiv.setUpdId(inputId);
                        //修改机构
                        cusIndiv.setUpdBrId(inputBrId);
                        //修改日期
                        cusIndiv.setUpdDate(inputDate);
                        //修改时间
                        cusIndiv.setUpdateTime(createTime);
                        int result = this.cusIndivMapper.updateByPrimaryKeySelective(cusIndiv);
                        if(result !=1 ){
                            throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                        }
                    } else { //无责新增
                        cusIndiv = new CusIndiv();
                        BeanUtils.copyProperties(cusIndivDto, cusIndiv);
                        // 获取用户信息
                        // 申请人
                        cusIndiv.setInputId(inputId);
                        // 申请机构
                        cusIndiv.setInputBrId(inputBrId);
                        // 申请时间
                        cusIndiv.setInputDate(inputDate);
                        // 创建时间
                        cusIndiv.setCreateTime(createTime);
                        //修改人
                        cusIndiv.setUpdId(inputId);
                        //修改机构
                        cusIndiv.setUpdBrId(inputBrId);
                        //修改日期
                        cusIndiv.setUpdDate(inputDate);
                        //修改时间
                        cusIndiv.setUpdateTime(createTime);
                        int result = this.cusIndivMapper.insertSelective(cusIndiv);
                        if(result !=1 ){
                            throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                        }
                    }
                    // 保存任务信息
                    cusBase = new CusBase();
                    // 保存客户信息到cus_base客户基本信息表
                    BeanUtils.copyProperties(cusIndivDto, cusBase);
                    if("B01".equals(cusIndivDto.getBizType())){
                        cusBase.setCusRankCls("01");
                    }else if("B02".equals(cusIndivDto.getBizType())){
                        cusBase.setCusRankCls("02");
                    }
                    // 客户状态 1暂存
                    cusBase.setCusState(CmisCusConstants.STD_CUS_STATE_1);
                    //客户大类 2 个人
                    cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PRI);
                    // 申请人
                    cusBase.setInputId(inputId);
                    //管户人
                    cusBase.setManagerId(inputId);
                    // 申请机构
                    cusBase.setInputBrId(inputBrId);
                    // 管户机构
                    cusBase.setManagerBrId(inputBrId);
                    // 申请时间
                    cusBase.setInputDate(inputDate);
                    // 创建时间
                    cusBase.setCreateTime(createTime);
                    // 修改人
                    cusBase.setUpdId(inputId);
                    // 修改机构
                    cusBase.setUpdBrId(inputBrId);
                    // 修改日期
                    cusBase.setUpdDate(inputDate);
                    // 修改时间
                    cusBase.setUpdateTime(createTime);
                    cusBase.setCusRankCls("01");
                    int result= cusBaseService.insertSelective(cusBase);
                    if(result>0){
                        // 判断是否我行股东
                        int count = queryCountByCertCode(cusBase.getCertCode());
                        if (count == 1) {
                            cusIndivDto.setIsBankSharehd(CmisCusConstants.COMMON_FLAG_YES);
                        } else {
                            cusIndivDto.setIsBankSharehd(CmisCusConstants.COMMON_FLAG_NO);
                        }
                        return cusIndivDto;
                        } else {
                            throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                        }
                }else{
                    // 交易失败
                    // 判断如果是临时客户 可直接开户 01正式 02 临时
                    // 若ecif系统检验不通过，则发送开户接口进行开户
                    S00102ReqDto s00102ReqDto = new S00102ReqDto();
                    // 客户名称
                    s00102ReqDto.setCustna(cusIndivDto.getCusName());
                    // 证件号码
                    s00102ReqDto.setIdtfno(cusIndivDto.getCertCode());//cusIndivDto.getCertType()
                    // 证件类型
                    s00102ReqDto.setIdtftp(cusIndivDto.getCertType());
                    // 客户状态
                    s00102ReqDto.setCustst("3");
                    s00102ReqDto.setCtcktg("1");
                    s00102ReqDto.setResult("00");
                    // 发送Ecif开户
                    ResultDto<S00102RespDto> s00102ResultDto = dscms2EcifClientService.s00102(s00102ReqDto);
                    String g00102Code = Optional.ofNullable(s00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String g00102Meesage = Optional.ofNullable(s00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    S00102RespDto s00102RespDto = null;
                    if (Objects.equals(g00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //  获取相关的值并解析
                        s00102RespDto = s00102ResultDto.getData();
                        if(s00102RespDto!=null){
                            cusId=s00102RespDto.getCustno();
                            cusIndivDto.setCusId(cusId);
                            CusIndiv cusIndiv = selectByPrimaryKey(cusId);
                            if (cusIndiv != null) {//有则修改
                                BeanUtils.copyProperties(cusIndivDto, cusIndiv);
                                //修改人
                                cusIndiv.setUpdId(inputId);
                                //修改机构
                                cusIndiv.setUpdBrId(inputBrId);
                                //修改日期
                                cusIndiv.setUpdDate(inputDate);
                                //修改时间
                                cusIndiv.setUpdateTime(createTime);
                                int result = this.cusIndivMapper.updateByPrimaryKeySelective(cusIndiv);
                                if(result !=1 ){
                                    throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                                }
                            } else { //无责新增
                                cusIndiv = new CusIndiv();
                                BeanUtils.copyProperties(cusIndivDto, cusIndiv);
                                // 获取用户信息
                                // 申请人
                                cusIndiv.setInputId(inputId);
                                // 申请机构
                                cusIndiv.setInputBrId(inputBrId);
                                // 申请时间
                                cusIndiv.setInputDate(inputDate);
                                // 创建时间
                                cusIndiv.setCreateTime(createTime);
                                //修改人
                                cusIndiv.setUpdId(inputId);
                                //修改机构
                                cusIndiv.setUpdBrId(inputBrId);
                                //修改日期
                                cusIndiv.setUpdDate(inputDate);
                                //修改时间
                                cusIndiv.setUpdateTime(createTime);
                                int result = this.cusIndivMapper.insertSelective(cusIndiv);
                                if(result !=1 ){
                                    throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                                }
                            }
                            cusBase = new CusBase();
                            // 保存客户信息到cus_base客户基本信息表
                            BeanUtils.copyProperties(cusIndivDto, cusBase);
                            // 客户状态 1暂存
                            cusBase.setCusState(CmisCusConstants.STD_CUS_STATE_1);
                            //客户大类 2 个人
                            cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PRI);
                            // 申请人
                            cusBase.setInputId(inputId);
                            //管户人
                            cusBase.setManagerId(inputId);
                            // 申请机构
                            cusBase.setInputBrId(inputBrId);
                            // 管户机构
                            cusBase.setManagerBrId(inputBrId);
                            // 申请时间
                            cusBase.setInputDate(inputDate);
                            // 创建时间
                            cusBase.setCreateTime(createTime);
                            // 修改人
                            cusBase.setUpdId(inputId);
                            // 修改机构
                            cusBase.setUpdBrId(inputBrId);
                            // 修改日期
                            cusBase.setUpdDate(inputDate);
                            // 修改时间
                            cusBase.setUpdateTime(createTime);
                            cusBase.setCusRankCls("01");
                            int result= cusBaseService.insertSelective(cusBase);
                            if(result>0){
                                // 判断是否我行股东
                                int count = queryCountByCertCode(cusBase.getCertCode());
                                if (count == 1) {
                                    cusIndivDto.setIsBankSharehd(CmisCusConstants.COMMON_FLAG_YES);
                                } else {
                                    cusIndivDto.setIsBankSharehd(CmisCusConstants.COMMON_FLAG_NO);
                                }
                                return cusIndivDto;
                            } else {
                                throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存客户信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                            }

                        }
                    } else {
                        //  抛出错误异常
                        throw BizException.error(null,g00102Code,EcsEnum.CUS_INDIV_CREATE_RESULT.value+g00102Meesage);
                    }
                }
            } else {
                //  抛出错误异常
                throw BizException.error(null,s10501Code,EcsEnum.CUS_INDIV_QUERY_RESULT.value+s10501Meesage);
            }
        }else{
            throw BizException.error(null,EcsEnum.CUS_INDIV_CHECK_RESULT01.key,EcsEnum.CUS_INDIV_CHECK_RESULT01.value);
        }
        return cusIndivDto;
    }

    /**
     *
     * @param queryModel
     * @return
     */
    public List<cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto> getCusIndivDtoList(QueryModel queryModel) {
        return cusIndivMapper.getCusIndivDtoList(queryModel);
    }

    /**
     * @方法名称: selectByCondition
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CusIndiv> selectByCondition(QueryModel model) {
        List<CusIndiv> list = cusIndivMapper.selectByCondition(model);
        return list;
    }

    /**
     * @方法名称: selectByCondition
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CusIndivDto> selectDtoByCondition(QueryModel model) {
        List<CusIndiv> list = cusIndivMapper.selectByCondition(model);
        if (CollectionUtils.nonEmpty(list)) {
            List<CusIndivDto> collect = list.parallelStream().map(cusIndiv -> {
                CusIndivDto cusIndivDto = new CusIndivDto();
                BeanUtils.copyProperties(cusIndiv, cusIndivDto);
                return cusIndivDto;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }
    private void checkIsByCusId(CusIndivDto cusIndivDto) {
        QueryModel model = new QueryModel();
        model.addCondition("cusId", cusIndivDto.getCusId());
        List<CusBase> exists = cusBaseMapper.selectByModel(model);
        if (!exists.isEmpty() && exists.size() != 0) {
            throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"ECIF返回的客户号重复，请联系系统管理员");
        }
    }
    private void checkOnthWay(CusIndivDto cusIndivDto) throws Exception {
        QueryModel model = new QueryModel();
        model.addCondition("certType", cusIndivDto.getCertType());
        model.addCondition("certCode", cusIndivDto.getCertCode());
        List<CusBase> exists = cusBaseMapper.selectByModel(model);
        if (!exists.isEmpty() && exists.size() != 0) {
            String message ="客戶信息";
            if("01".equals(exists.get(0).getCusRankCls())){
                message ="正式客户";
            }else{
                message ="临时客户";
            }
            throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"存在该证件号码的"+message +"，请核对!");
            // throw new Exception("证件号码:" + cusCorpDto.getCertCode() + "的客户已存在！");
        }
    }
    @Transactional
    public CusIndivDto fastIndivCreate(CusIndivDto cusIndivDto) throws Exception {
        // 第一步检查本地
        checkOnthWay(cusIndivDto);
            // 第二步检查ECIf
            S10501ReqDto s10501ReqDto = new S10501ReqDto();
            s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
            s10501ReqDto.setIdtftp(cusIndivDto.getCertType());//   证件类型
            s10501ReqDto.setIdtfno(cusIndivDto.getCertCode());//   证件号码
            //  StringUtils.EMPTY的实际值待确认 开始
            // 发送客户三要素发送ecif查询
            ResultDto<S10501RespDto> s10501ResultDto = dscms2EcifClientService.s10501(s10501ReqDto);
            String s10501Code = Optional.ofNullable(s10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String s10501Meesage = Optional.ofNullable(s10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            S10501RespDto s10501RespDto = null;
            if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                //  获取查询结果 todo联调验证交易成功查询失败情况
                s10501RespDto = s10501ResultDto.getData();
                if(s10501RespDto.getListnm()>0){
                    //cusId=StringUtils.getUUID();
                    if(CollectionUtils.nonEmpty(s10501RespDto.getListArrayInfo())){
                        List<cn.com.yusys.yusp.dto.client.esb.ecif.s10501.ListArrayInfo> listArrayInfo =s10501RespDto.getListArrayInfo();
                        for(int i=0;i<listArrayInfo.size();i++){
                            // 客户号
                            cusIndivDto.setCusId(listArrayInfo.get(0).getCustno());
                        }
                    }
                }else{
                    // 交易失败
                    // 若ecif系统检验不通过，则发送开户接口进行开户
                    S00102ReqDto s00102ReqDto = new S00102ReqDto();
                    // 客户名称
                    if(cusIndivDto.getCusName()!=null&& !cusIndivDto.getCusName().equals("")){
                        s00102ReqDto.setCustna(cusIndivDto.getCusName());}
                    // 证件号码
                    s00102ReqDto.setIdtfno(cusIndivDto.getCertCode());//cusIndivDto.getCertType()
                    // 证件类型
                    s00102ReqDto.setIdtftp(cusIndivDto.getCertType());
                    // 客户状态
                    s00102ReqDto.setCustst("3");
                    s00102ReqDto.setCtcktg("1");
                    s00102ReqDto.setResult("00");
                    // 发送Ecif开户
                    ResultDto<S00102RespDto> s00102ResultDto = dscms2EcifClientService.s00102(s00102ReqDto);
                    String s00102Code = Optional.ofNullable(s00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String s00102Meesage = Optional.ofNullable(s00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    S00102RespDto s00102RespDto = null;
                    if (Objects.equals(s00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //  获取相关的值并解析
                        s00102RespDto = s00102ResultDto.getData();
                        if(s00102RespDto!=null){
                            cusIndivDto.setCusId(s00102RespDto.getCustno());
                        }
                    } else {
                        //  抛出错误异常
                        throw BizException.error(null,s00102Code,EcsEnum.CUS_INDIV_CREATE_RESULT.value+s00102Meesage);
                    }
                }
            } else {
                //  抛出错误异常
                throw BizException.error(null,s10501Code,EcsEnum.CUS_INDIV_QUERY_RESULT.value+s10501Meesage);
            }
            checkIsByCusId(cusIndivDto);

        if(StringUtils.isEmpty(cusIndivDto.getCusId())){
            throw BizException.error(null,EcsEnum.ECS040008.key,EcsEnum.ECS040008.value);
        }
//        /*
//         *保存cusBase表
//         */
            CusBase cusBase2 = new CusBase();
            BeanUtils.copyProperties(cusIndivDto, cusBase2);
//        //客户大类 1-对私 2-对公
            cusBase2.setCusCatalog("1");
//        //客户状态 STD_CUS_STATE 1-暂存2-生效
            cusBase2.setCusState("2");
        // 客户状态 02临时客户
            cusBase2.setCusRankCls("02");
//        //设置管护机构
            User userInfo2 = SessionUtils.getUserInformation();
            cusBase2.setManagerId(userInfo2.getLoginCode());
            cusBase2.setManagerBrId(userInfo2.getOrg().getCode());
            cusBaseMapper.insertSelective(cusBase2);
//
//        /*
//         * 保存cusCorp表
//         */
            CusIndiv cusIndiv = new CusIndiv();
            BeanUtils.copyProperties(cusIndivDto, cusIndiv);
            cusIndivMapper.insertSelective(cusIndiv);
            BeanUtils.copyProperties(cusIndiv, cusIndivDto);
            return cusIndivDto;
    }

    /**
     * @函数名称: cusInit
     * @param cusId
     * @return Map
     * @author 尚智勇
     * @version 1.0.0
     * @desc 新增页面初始化，反显客户基本信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Map<String, String> cusInit(String cusId) {
        return cusIndivMapper.selectCusAllInfo(cusId);
    }

    /**
     * @函数名称: cusInit
     * @函数描述: 新增页面初始化，反显客户基本信息
     * @参数与返回说明:
     * @算法描述:
     */
    public CreditCardAppInfoDto createCreditCardCusInfo(CreditCardAppInfoDto creditCardAppInfoDto) {
        //信用卡客戶信息修改
        CusBase  cusbase = new CusBase();
        CusIndiv  cusIndiv = new CusIndiv();
        String cusId = creditCardAppInfoDto.getCusId();
        log.info("根据客户号："+cusId+"更新个人客户信息开始！");
        cusIndiv = cusIndivMapper.selectByPrimaryKey(cusId);
        if(cusIndiv != null){
            cusIndiv.setSex(creditCardAppInfoDto.getSex());
            cusIndiv.setNation(creditCardAppInfoDto.getNation());
            /*C	已婚有子女
            D	离异
            M	已婚无子女
            O	其他
            S	未婚
            W	丧偶*/
            /*20	已婚
            40	离婚
            10	未婚
            30	丧偶*/
            String marStatus = creditCardAppInfoDto.getMarStatus();
            if("C".equals(marStatus) || "M".equals(marStatus) ){
                marStatus ="20";
            }else if("D".equals(marStatus)){
                marStatus ="40";
            }else if("S".equals(marStatus)){
                marStatus ="10";
            }else if("W".equals(marStatus)){
                marStatus ="30";
            }
            cusIndiv.setMarStatus(marStatus);
            /*D	大专
            C	本科
            G	初中及以下
            B	硕士
            A	博士及以上
            F	高中
            E	中专及技校*/
            /*10	博士研究生
            50	中等专业学校
            A0	文盲或半文盲
            20	硕士研究生
            80	初中
            60	技工学校
            40	大学专科和专科学校
            90	小学
            30	大学本科
            70	高中*/
            String qualification = creditCardAppInfoDto.getQualification();
            if("D".equals(qualification)){
                qualification = "40";
            }else if("C".equals(qualification)){
                qualification = "30";
            }else if("G".equals(qualification)){
                qualification = "80";
            }else if("B".equals(qualification)){
                qualification = "20";
            }else if("A".equals(qualification)){
                qualification = "10";
            }else if("F".equals(qualification)){
                qualification = "70";
            }else if("E".equals(qualification)){
                qualification = "50";
            }
            cusIndiv.setIndivEdt(qualification);
            cusIndiv.setIndivDtOfBirth(creditCardAppInfoDto.getBirthday());
            cusIndiv.setCertStartDt(creditCardAppInfoDto.getCertStartDate());
            cusIndiv.setCertEndDt(creditCardAppInfoDto.getCertEndDate());
            cusIndiv.setIndivFolk(creditCardAppInfoDto.getFolk());
            cusIndiv.setCusType("110");
            int result =  cusIndivMapper.updateByPrimaryKeySelective(cusIndiv);
            if(result !=1){
                //  抛出错误异常
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"更新客户信息异常！");
            }
        }
        CusIndivUnit  cusIndivUnit = cusIndivUnitService.selectByCusId(cusId);
        /*D	商业、服务业人员
        A	国家机关、党群组织、企业、事业单位负责人
        B	专业技术人员
        F	生产、运输设备操作人员及有关人员
        G	军人
        H	其他从业人员
        E	农、林、牧、渔、水利业生产人员
        C	办事人员和有关人员*/
        String occu = creditCardAppInfoDto.getOccu();
        if("D".equals(occu)){
            occu ="40500";
        }else if("A".equals(occu)){
            occu ="10200";
        }else if("B".equals(occu)){
            occu ="20200";
        }else if("F".equals(occu)){
            occu ="63000";
        }else if("G".equals(occu)){
            occu ="70001";
        }else if("H".equals(occu)){
            occu ="89900";
        }else if("E".equals(occu)){
            occu ="59900";
        }else if("C".equals(occu)){
            occu ="30100";
        }
        /*E	内资企业
        I	联营企业
        G	集体企业
        Z	其他
        C	军队
        Q	外商投资股份有限公司(含港、澳、台)
        K	股份有限公司
        D	社会团体
        N	中外合资经营企业(含港、澳、台)
        P	外资企业(含港、澳、台)
        F	国有企业
        A	党政机关
        M	外商投资企业(含港、澳、台)
        B	事业单位
        R	个体经营
        H	股份合作企业
        L	私营企业
        O	中外合作经营企业(含港、澳、台)
        J	有限责任公司*/
        String  corpCategory =  creditCardAppInfoDto.getCorpCategory();
        if("E".equals(corpCategory)){
            corpCategory ="500";
        }else if("I".equals(corpCategory)){
            corpCategory ="540";
        }else if("G".equals(corpCategory)){
            corpCategory ="520";
        }else if("Z".equals(corpCategory)){
            corpCategory ="800";
        }else if("C".equals(corpCategory)){
            corpCategory ="300";
        }else if("Q".equals(corpCategory)){
            corpCategory ="640";
        }else if("K".equals(corpCategory)){
            corpCategory ="560";
        }else if("D".equals(corpCategory)){
            corpCategory ="400";
        }else if("N".equals(corpCategory)){
            corpCategory ="620";
        }else if("P".equals(corpCategory)){
            corpCategory ="630";
        }else if("F".equals(corpCategory)){
            corpCategory ="510";
        }else if("A".equals(corpCategory)){
            corpCategory ="100";
        }else if("M".equals(corpCategory)){
            corpCategory ="600";
        }else if("B".equals(corpCategory)){
            corpCategory ="200";
        }else if("R".equals(corpCategory)){
            corpCategory ="700";
        }else if("H".equals(corpCategory)){
            corpCategory ="530";
        }else if("L".equals(corpCategory)){
            corpCategory ="570";
        }else if("O".equals(corpCategory)){
            corpCategory ="620";
        }else{
            corpCategory ="550";
        }

        /*D	一般员工
        A	高层管理人员
        C	低层管理人员
        B	中层管理人员*/
        String duty = creditCardAppInfoDto.getDuty();
        if("D".equals(duty)){
            duty ="3";
        }else if("A".equals(duty)){
            duty ="1";
        }else if("B".equals(duty)){
            duty ="2";
        }else{
            duty ="4";
        }
        if(cusIndivUnit == null){
            cusIndivUnit = new CusIndivUnit();
            cusIndivUnit.setPkId(UUID.randomUUID().toString());
            cusIndivUnit.setCusId(cusId);
            cusIndivUnit.setOcc(occu);
            cusIndivUnit.setUnitName(creditCardAppInfoDto.getCorpName());
            cusIndivUnit.setIndivComTyp(corpCategory);
            cusIndivUnit.setIndivComTrade(creditCardAppInfoDto.getTrade());
            cusIndivUnit.setIndivYearn(creditCardAppInfoDto.getYearIncome().multiply(new BigDecimal(10000)).toPlainString());
            cusIndivUnit.setUnitAddr(creditCardAppInfoDto.getUnitDetailAddr());
            cusIndivUnit.setJobTtl(duty);
            cusIndivUnit.setEarningCurType("CNY");
            cusIndivUnit.setUnitPhn(creditCardAppInfoDto.getUnitPhone());
            cusIndivUnit.setWorkDate(String.valueOf(creditCardAppInfoDto.getWorkYears()));
             int result = cusIndivUnitService.insertSelective(cusIndivUnit);
            if(result !=1){
                //  抛出错误异常
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"更新客户行业信息异常！");
            }
        }else{
            cusIndivUnit.setOcc(occu);
            cusIndivUnit.setUnitName(creditCardAppInfoDto.getCorpName());
            cusIndivUnit.setIndivComTyp(corpCategory);
            cusIndivUnit.setIndivComTrade(creditCardAppInfoDto.getTrade());
            cusIndivUnit.setIndivYearn(creditCardAppInfoDto.getYearIncome().multiply(new BigDecimal(10000)).toPlainString());
            cusIndivUnit.setUnitAddr(creditCardAppInfoDto.getUnitDetailAddr());
            cusIndivUnit.setJobTtl(duty);
            cusIndivUnit.setEarningCurType("CNY");
            cusIndivUnit.setUnitPhn(creditCardAppInfoDto.getUnitPhone());
            cusIndivUnit.setWorkDate(String.valueOf(creditCardAppInfoDto.getWorkYears()));
            int result = cusIndivUnitService.updateSelective(cusIndivUnit);
            if(result !=1){
                //  抛出错误异常
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"更新客户行业信息异常！");
            }
        }
        CusIndivContact cusIndivContact = cusIndivContactService.selectByPrimaryKey(cusId);
        if( cusIndivContact == null){
            cusIndivContact = new CusIndivContact();
            cusIndivContact.setCusId(creditCardAppInfoDto.getCusId());
            cusIndivContact.setIndivRsdAddr(creditCardAppInfoDto.getFamilyDetailAddr());
            cusIndivContact.setMobile(creditCardAppInfoDto.getFamilyPhone());
            cusIndivContact.setEmail(creditCardAppInfoDto.getEmail());
            cusIndivContact.setMobile(creditCardAppInfoDto.getPhone());
            cusIndivContact.setRegionalism(creditCardAppInfoDto.getUnitAddrPost());
            int result = cusIndivContactService.insert(cusIndivContact);
            if(result !=1){
                //  抛出错误异常
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"更新客户联系信息异常！");
            }
        }else{
            cusIndivContact.setIndivRsdAddr(creditCardAppInfoDto.getFamilyDetailAddr());
            cusIndivContact.setMobile(creditCardAppInfoDto.getFamilyPhone());
            cusIndivContact.setEmail(creditCardAppInfoDto.getEmail());
            cusIndivContact.setMobile(creditCardAppInfoDto.getPhone());
            cusIndivContact.setRegionalism(creditCardAppInfoDto.getUnitAddrPost());
            int result = cusIndivContactService.updateSelective(cusIndivContact);
            if(result !=1){
                //  抛出错误异常
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"更新客户联系信息异常！");
            }
        }

        return creditCardAppInfoDto;
    }
}



