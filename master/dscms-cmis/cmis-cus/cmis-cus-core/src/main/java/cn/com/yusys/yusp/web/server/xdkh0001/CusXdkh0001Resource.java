package cn.com.yusys.yusp.web.server.xdkh0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0001.Xdkh0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:个人客户基本信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0001:个人客户基本信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0001Resource.class);

    @Autowired
    private Xdkh0001Service xdkh0001Service;

    /**
     * 交易码：xdkh0001
     * 交易描述：个人客户基本信息查询
     *
     * @param xdkh0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("个人客户基本信息查询")
    @PostMapping("/xdkh0001")
    protected @ResponseBody
    ResultDto<Xdkh0001DataRespDto> xdkh0001(@Validated @RequestBody Xdkh0001DataReqDto xdkh0001DataReqDto) throws BizException,Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
        Xdkh0001DataRespDto xdkh0001DataRespDto = new Xdkh0001DataRespDto();// 响应Dto:个人客户基本信息查询
        ResultDto<Xdkh0001DataRespDto> xdkh0001DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
            xdkh0001DataRespDto = xdkh0001Service.getIndivCusBaseInfo(xdkh0001DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataRespDto));
            // 封装xdkh0001DataResultDto中正确的返回码和返回信息
            xdkh0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            // 封装xdkh0001DataResultDto中异常返回码和返回信息
            xdkh0001DataResultDto.setCode(e.getErrorCode());
            xdkh0001DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            // 封装xdkh0001DataResultDto中异常返回码和返回信息
            xdkh0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0001DataRespDto到xdkh0001DataResultDto中
        xdkh0001DataResultDto.setData(xdkh0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataRespDto));
        return xdkh0001DataResultDto;
    }
}
