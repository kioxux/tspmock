package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusGrpApp;
import cn.com.yusys.yusp.domain.CusGrpMemberApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 集团客户认定、变更、解散审批流程
 * @author liucheng
 * @version 1.0.0
 * @date 2021/4/12 下午12:42
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class KHGL03BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(KHGL03BizService.class);
    @Autowired
    private CusGrpAppService service;
    @Autowired
    private CusGrpService cusGrpService;

    @Autowired
    private CusGrpMemberAppService cusGrpMemberAppService;
    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private CusGrpAppService cusGrpAppService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("后业务处理类型" + currentOpType);
        try {
            // 加载路由条件
            put2VarParam(resultInstanceDto, serno);

            CusGrpApp domain = service.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
//                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
//                service.update(domain);
                log.info("集团客户认定、变更、解散审批流程【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("集团客户认定、变更、解散审批流程【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("集团客户认定、变更、解散审批流程【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("集团客户认定、变更、解散审批流程【{}】，流程同意操作，流程参数【{}】", serno, resultInstanceDto);

                cusGrpAppService.handleBusinessAfterEnd(serno,bizType,resultInstanceDto);

                log.info("集团客户认定、变更、解散审批流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("集团客户认定、变更、解散审批流程【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 判断是否打回发起人节点
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                    domain.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    service.update(domain);
                }
                log.info("集团客户认定、变更、解散审批流程【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("集团客户认定、变更、解散审批流程【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("集团客户认定、变更、解散审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("集团客户认定、变更、解散审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                service.update(domain);
            } else {
                log.info("集团客户认定、变更、解散审批流程【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("集团客户认定、变更、解散审批流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "KHGL03".equals(flowCode) ;
    }

    /**
     * @方法名称: put2VarParam
     * @方法描述: 重置流程参数
     * @参数与返回说明:
     * @算法描述: 获取路由参数放置流程中
     * @创建人: dumingdi
     * @创建时间: 2021-09-23 23:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public void put2VarParam(ResultInstanceDto resultInstanceDto, String serno) {
        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(resultInstanceDto.getBizId());
        param.setInstanceId(resultInstanceDto.getInstanceId());

        String dutys = (String)resultInstanceDto.getParam().get("dutys");
        String isManager = "0";
        String isLarge = "0";

        if(dutys.indexOf("FZH01") > -1 || dutys.indexOf("FZH02") > -1 || dutys.indexOf("XWB01") > -1 || dutys.indexOf("WJB04") > -1) {
            isManager = "1";
            //客户经理、授信额度大于1500万，公司业务部核查岗
            BigDecimal amount = getGrpMemberLmtAmt(serno);

            if(amount.compareTo(new BigDecimal("15000000")) > 0) {
                isLarge = "1";
            }
        }

        Map<String, Object> params = new HashMap<>();
        params.put("isManager",isManager);
        params.put("isLarge",isLarge);
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);
    }

    /**
     * 获取集团成员的敞口额度合计总和
     * @param serno
     * @return
     */
    protected BigDecimal getGrpMemberLmtAmt(String serno) {
        BigDecimal totalAmount = BigDecimal.ZERO;
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", serno);
        queryModel.addCondition("oprType", "01");
        List<CusGrpMemberApp> cusGrpMemberList = cusGrpMemberAppService.selectByModel(queryModel);

        if (CollectionUtils.nonEmpty(cusGrpMemberList)) {

            for (CusGrpMemberApp cusGrpMemberApp : cusGrpMemberList) {
                String cusId = cusGrpMemberApp.getCusId();
                ResultDto<BigDecimal> resultDto = cmisBizClientService.selectOpenTotalLmtAmtByCusId(cusId);
                BigDecimal openTotalLmtAmt = resultDto.getData();
                totalAmount = totalAmount.add(openTotalLmtAmt);
            }
        }

        return totalAmount;
    }
}