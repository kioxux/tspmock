/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstGlf
 * @类描述: cus_lst_glf数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-13 20:17:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_glf")
public class CusLstGlf extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 关联方类型 **/
	@Column(name = "RELATED_PARTY_TYPE", unique = false, nullable = true, length = 20)
	private String relatedPartyType;
	
	/** 关联方名称 **/
	@Column(name = "RELATED_PARTY_NAME", unique = false, nullable = true, length = 20)
	private String relatedPartyName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 20)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 境内/境外 **/
	@Column(name = "LCAOS", unique = false, nullable = true, length = 20)
	private String lcaos;
	
	/** 归属组别 **/
	@Column(name = "BELONG_GROUP", unique = false, nullable = true, length = 20)
	private String belongGroup;
	
	/** 层级标识 **/
	@Column(name = "LEVELS", unique = false, nullable = true, length = 20)
	private String levels;
	
	/** 上一级关联方名称 **/
	@Column(name = "PAREBT_RELATED_PARTY_NAME", unique = false, nullable = true, length = 20)
	private String parebtRelatedPartyName;
	
	/** 上一级关联方证件号码 **/
	@Column(name = "PAREBT_RELATED_PARTY_CERT_NO", unique = false, nullable = true, length = 20)
	private String parebtRelatedPartyCertNo;
	
	/** 与上一级关联方关系 **/
	@Column(name = "PAREBT_RELATED_PARTY_RELA", unique = false, nullable = true, length = 20)
	private String parebtRelatedPartyRela;
	
	/** 与上一级之间是否有权益份额 **/
	@Column(name = "PAREBT_BETWEEN_IS_RIGHTS_SHARE", unique = false, nullable = true, length = 20)
	private String parebtBetweenIsRightsShare;
	
	/** 持有比例 **/
	@Column(name = "HOLD_PERC", unique = false, nullable = true, length = 20)
	private String holdPerc;
	
	/** 关联关系说明 **/
	@Column(name = "CORRE_MEMO", unique = false, nullable = true, length = 20)
	private String correMemo;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 255)
	private String status;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param relatedPartyType
	 */
	public void setRelatedPartyType(String relatedPartyType) {
		this.relatedPartyType = relatedPartyType;
	}
	
    /**
     * @return relatedPartyType
     */
	public String getRelatedPartyType() {
		return this.relatedPartyType;
	}
	
	/**
	 * @param relatedPartyName
	 */
	public void setRelatedPartyName(String relatedPartyName) {
		this.relatedPartyName = relatedPartyName;
	}
	
    /**
     * @return relatedPartyName
     */
	public String getRelatedPartyName() {
		return this.relatedPartyName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param lcaos
	 */
	public void setLcaos(String lcaos) {
		this.lcaos = lcaos;
	}
	
    /**
     * @return lcaos
     */
	public String getLcaos() {
		return this.lcaos;
	}
	
	/**
	 * @param belongGroup
	 */
	public void setBelongGroup(String belongGroup) {
		this.belongGroup = belongGroup;
	}
	
    /**
     * @return belongGroup
     */
	public String getBelongGroup() {
		return this.belongGroup;
	}
	
	/**
	 * @param levels
	 */
	public void setLevels(String levels) {
		this.levels = levels;
	}
	
    /**
     * @return levels
     */
	public String getLevels() {
		return this.levels;
	}
	
	/**
	 * @param parebtRelatedPartyName
	 */
	public void setParebtRelatedPartyName(String parebtRelatedPartyName) {
		this.parebtRelatedPartyName = parebtRelatedPartyName;
	}
	
    /**
     * @return parebtRelatedPartyName
     */
	public String getParebtRelatedPartyName() {
		return this.parebtRelatedPartyName;
	}
	
	/**
	 * @param parebtRelatedPartyCertNo
	 */
	public void setParebtRelatedPartyCertNo(String parebtRelatedPartyCertNo) {
		this.parebtRelatedPartyCertNo = parebtRelatedPartyCertNo;
	}
	
    /**
     * @return parebtRelatedPartyCertNo
     */
	public String getParebtRelatedPartyCertNo() {
		return this.parebtRelatedPartyCertNo;
	}
	
	/**
	 * @param parebtRelatedPartyRela
	 */
	public void setParebtRelatedPartyRela(String parebtRelatedPartyRela) {
		this.parebtRelatedPartyRela = parebtRelatedPartyRela;
	}
	
    /**
     * @return parebtRelatedPartyRela
     */
	public String getParebtRelatedPartyRela() {
		return this.parebtRelatedPartyRela;
	}
	
	/**
	 * @param parebtBetweenIsRightsShare
	 */
	public void setParebtBetweenIsRightsShare(String parebtBetweenIsRightsShare) {
		this.parebtBetweenIsRightsShare = parebtBetweenIsRightsShare;
	}
	
    /**
     * @return parebtBetweenIsRightsShare
     */
	public String getParebtBetweenIsRightsShare() {
		return this.parebtBetweenIsRightsShare;
	}
	
	/**
	 * @param holdPerc
	 */
	public void setHoldPerc(String holdPerc) {
		this.holdPerc = holdPerc;
	}
	
    /**
     * @return holdPerc
     */
	public String getHoldPerc() {
		return this.holdPerc;
	}
	
	/**
	 * @param correMemo
	 */
	public void setCorreMemo(String correMemo) {
		this.correMemo = correMemo;
	}
	
    /**
     * @return correMemo
     */
	public String getCorreMemo() {
		return this.correMemo;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}