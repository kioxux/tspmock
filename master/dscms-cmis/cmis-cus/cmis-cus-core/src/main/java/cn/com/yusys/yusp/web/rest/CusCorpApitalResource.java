/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.CusCorpMgr;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusCorpApital;
import cn.com.yusys.yusp.service.CusCorpApitalService;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusCorpApitalResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-23 09:49:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuscorpapital")
public class CusCorpApitalResource {
    @Autowired
    private CusCorpApitalService cusCorpApitalService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusCorpApital>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusCorpApital> list = cusCorpApitalService.selectAll(queryModel);
        return new ResultDto<List<CusCorpApital>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusCorpApital>> index(QueryModel queryModel) {
        List<CusCorpApital> list = cusCorpApitalService.selectByModel(queryModel);
        return new ResultDto<List<CusCorpApital>>(list);
    }

    /**
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusCorpApital>> query(@RequestBody QueryModel queryModel) {
        List<CusCorpApital> list = cusCorpApitalService.selectByModel(queryModel);
        return new ResultDto<List<CusCorpApital>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusCorpApital> show(@PathVariable("pkId") String pkId) {
        CusCorpApital cusCorpApital = cusCorpApitalService.selectByPrimaryKey(pkId);
        return new ResultDto<CusCorpApital>(cusCorpApital);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusCorpApital> create(@RequestBody CusCorpApital cusCorpApital) throws URISyntaxException {
        cusCorpApitalService.insert(cusCorpApital);
        return new ResultDto<CusCorpApital>(cusCorpApital);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusCorpApital cusCorpApital) throws URISyntaxException {
        int result = cusCorpApitalService.update(cusCorpApital);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusCorpApitalService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusCorpApitalService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getApital")
    protected ResultDto<CusCorpApital> getCert(@RequestBody CusCorpApital cusCorpApital) {
        QueryModel queryModel = new QueryModel();
        CusCorpApital cusApi= cusCorpApitalService.selectCusCorpApiByCodeAndType(cusCorpApital.getCertTyp(),cusCorpApital.getCertCode());
        return new ResultDto<CusCorpApital>(cusApi);
    }
    /**
     * 根据关联客户号查询
     * @param cusIdRel
     * @return
     */
    @PostMapping("/selectByCusIdRel")
    protected ResultDto<List<CusCorpApitalDto>> selectByCusIdRel(@RequestBody String cusIdRel){
        return new ResultDto<List<CusCorpApitalDto>>(cusCorpApitalService.selectByCusIdRel(cusIdRel));
    }
}
