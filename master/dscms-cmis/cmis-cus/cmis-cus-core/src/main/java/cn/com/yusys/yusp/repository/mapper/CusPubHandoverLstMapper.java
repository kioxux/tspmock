/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusPubHandoverLst;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusPubHandoverLstMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: pc
 * @创建时间: 2021-05-10 16:17:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusPubHandoverLstMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusPubHandoverLst selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusPubHandoverLst> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CusPubHandoverLst record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CusPubHandoverLst record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CusPubHandoverLst record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CusPubHandoverLst record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: cusPubHandoverLst
     * @方法描述: 根据参数查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusPubHandoverLst> selectByModeList(CusPubHandoverLst cusPubHandoverLst);

    /**
     * @方法名称: deleteByParam
     * @方法描述: 根据参数删除数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    void deleteByParam(Map map);

    /**
     * @方法名称: selectHandvoerInWayApp
     * @方法描述: 根据map 中的参数，查询是否存在在途数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectHandvoerInWayApp(Map map);

}