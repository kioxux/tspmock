package cn.com.yusys.yusp.reportconf.service;

import java.util.List;

import cn.com.yusys.yusp.commons.mapper.exception.YuMapperException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.ErrorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.reportconf.domain.FncConfTemplate;
import cn.com.yusys.yusp.reportconf.repository.mapper.FncConfTemplateMapper;
import cn.com.yusys.yusp.commons.module.adapter.exception.Message;
import cn.com.yusys.yusp.commons.mapper.CommonMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.service.CommonService;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfTemplateService
 * @类描述: service类
 * @功能描述: 财务报表模板配置
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 14:37:37
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class FncConfTemplateService extends CommonService {
	private static final Logger logger = LoggerFactory.getLogger(FncConfTemplateService.class);
	@Autowired
	private FncConfTemplateMapper fncConfTemplateMapper;

	@Override
	protected CommonMapper<?> getMapper() {
		return fncConfTemplateMapper;
	}

	/**
	 * 财务报表模板配置查询
	 * 
	 * @param model
	 * @return
	 */
	public List<FncConfTemplate> queryFncConfTemplateList(QueryModel model) {
		logger.info("===============请求参数：{}================", JSON.toJSONString(model));
		PageHelper.startPage(model.getPage(), model.getSize());
		List<FncConfTemplate> list = fncConfTemplateMapper.selectByModel(model);
		PageHelper.clearPage();
		if (null == list) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20201335);
			logger.error(ErrorConstants.NRCS_CMS_T20201335);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20201335, "财务报表配置查询错误");
		}
		return list;
	}

	/**
	 * 财务报表模板配置全量查询
	 * 
	 * @param model
	 * @return
	 */
	public List<FncConfTemplate> queryFncConfTemplateListAll(QueryModel model) {
		logger.info("===============请求参数：{}================", JSON.toJSONString(model));
		List<FncConfTemplate> list = fncConfTemplateMapper.selectByModel(model);
		if (null == list) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20201335);
			logger.error(ErrorConstants.NRCS_CMS_T20201335);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20201335, "财务报表配置查询错误");
		}
		return list;
	}

	/**
	 * 财务报表模板新增
	 * 
	 * @param fncConfTemplate
	 * @return
	 */
	@Transactional
	public int addFncConfTemplate(FncConfTemplate fncConfTemplate) {
		logger.info("===============请求参数：{}================", JSON.toJSONString(fncConfTemplate));
		int result = fncConfTemplateMapper.insertSelective(fncConfTemplate);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20201336);
			logger.error(ErrorConstants.NRCS_CMS_T20201336);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20201336, "财务报表配置新增错误");
		}
		return result;
	}

	/**
	 * 财务报表模板删除
	 * 
	 * @param fncId
	 * @return
	 */
	@Transactional
	public int deleteFncConfTemplate(String fncId) {
		logger.info("===============请求参数：{}================", JSON.toJSONString(fncId));
		int result = fncConfTemplateMapper.deleteByPrimaryKey(fncId);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20201338);
			logger.error(ErrorConstants.NRCS_CMS_T20201338);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20201338, "财务报表配置删除错误");
		}
		return result;
	}

	/**
	 * 财务报表模板修改
	 * 
	 * @param fncConfTemplate
	 * @return
	 */
	@Transactional
	public int updateFncConfTemplate(FncConfTemplate fncConfTemplate) {
		logger.info("===============请求参数：{}================", JSON.toJSONString(fncConfTemplate));
		int result = fncConfTemplateMapper.updateByPrimaryKeySelective(fncConfTemplate);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20201337);
			logger.error(ErrorConstants.NRCS_CMS_T20201337);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20201337, "财务报表配置修改错误");
		}
		return result;
	}

	/**
	 * 根据 fncId 查询模板
	 * 
	 * @param fncId
	 * @return
	 */
	public FncConfTemplate queryFncConfTemplateByKey(String fncId) {
		logger.info("===============请求参数：{}================", JSON.toJSONString(fncId));
		FncConfTemplate fncConfTemplate = fncConfTemplateMapper.selectByPrimaryKey(fncId);
		if (null == fncConfTemplate) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20201339);
			logger.error(ErrorConstants.NRCS_CMS_T20201339);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20201339, "财务报表配置查看错误");
		}
		return fncConfTemplate;
	}

}
