package cn.com.yusys.yusp.reportconf.service;

import java.util.List;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.ErrorConstants;
import cn.com.yusys.yusp.fncstat.domain.RptItemBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.reportconf.domain.FncConfStyles;
import cn.com.yusys.yusp.reportconf.repository.mapper.FncConfStylesMapper;
import cn.com.yusys.yusp.commons.module.adapter.exception.Message;
import cn.com.yusys.yusp.commons.mapper.CommonMapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.service.CommonService;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfStylesService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-20 18:19:46
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class FncConfStylesService extends CommonService {
	private static final Logger logger = LoggerFactory.getLogger(FncConfStylesService.class);
	@Autowired
	private FncConfStylesMapper fncConfStylesMapper;
//	@Autowired
//	private MessageProviderService messageProviderService;

	@Override
	protected CommonMapper<?> getMapper() {
		return fncConfStylesMapper;
	}

	public int dynamicUpdate(RptItemBean rptItemBean) {
		return fncConfStylesMapper.dynamicUpdate(rptItemBean);
	}

	public int dynamicUpdateAndData(RptItemBean rptItemBean) {
		return fncConfStylesMapper.dynamicUpdateAndData(rptItemBean);
	}

	/**
	 * 样式配置列表查询
	 * 
	 * @param model
	 * @return
	 */
	public List<FncConfStyles> queryFncConfStylesList(QueryModel model) {
		PageHelper.startPage(model.getPage(), model.getSize());
		List<FncConfStyles> list = fncConfStylesMapper.selectByModel(model);
		PageHelper.clearPage();
		if (null == list) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20204320);
			logger.error(ErrorConstants.NRCS_CMS_T20204320);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20204320, "报表样式配置查询错误");
		}
		return list;
	}

	/**
	 * 样式配置列表全量查询
	 * 
	 * @param model
	 * @return
	 */
	public List<FncConfStyles> queryFncConfStylesListAll(QueryModel model) {
		List<FncConfStyles> list = fncConfStylesMapper.selectByModel(model);
		if (null == list) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20204320);
			logger.error(ErrorConstants.NRCS_CMS_T20204320);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20204320, "报表样式配置查询错误");
		}
		return list;
	}

	/**
	 * 样式配置 新增功能
	 * 
	 * @param fncConfStyles
	 * @return
	 */
	@Transactional
	public int addFncConfStyles(FncConfStyles fncConfStyles) {
		int result = fncConfStylesMapper.insertSelective(fncConfStyles);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20204321);
			logger.error(ErrorConstants.NRCS_CMS_T20204321);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20204321, "报表样式配置新增错误");
		}
		return result;
	}

	/**
	 * 样式配置 删除
	 * 
	 * @param styleId，
	 *            样式id
	 * @return
	 */
	@Transactional
	public int deleteFncConfStyles(String styleId) {
		int result = fncConfStylesMapper.deleteByPrimaryKey(styleId);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20204323);
			logger.error(ErrorConstants.NRCS_CMS_T20204323);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20204323, "报表样式配置删除错误");
		}
		return result;
	}

	/**
	 * 样式配置 修改
	 * 
	 * @param fncConfStyles
	 *            实体类
	 * @return
	 */
	@Transactional
	public int updateFncConfStyles(FncConfStyles fncConfStyles) {
		int result = fncConfStylesMapper.updateByPrimaryKeySelective(fncConfStyles);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20204322);
			logger.error(ErrorConstants.NRCS_CMS_T20204322);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20204322, "报表样式配置修改错误");
		}
		return result;
	}

	/**
	 * 根据样式配置 styleId 查询
	 * 
	 * @param styleId
	 * @return
	 */
	public FncConfStyles queryFncConfStylesByKey(String styleId) {
		FncConfStyles fncConfStyles = fncConfStylesMapper.selectByPrimaryKey(styleId);
		if (null == fncConfStyles) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20204324);
			logger.error(ErrorConstants.NRCS_CMS_T20204324);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20204324, "报表样式配置查看错误");
		}
		return fncConfStyles;
	}

}
