/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.dto.CusLstDedkkhYjsxDto;
import cn.com.yusys.yusp.repository.mapper.CusLstDedkkhMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstDedkkhYjsx;
import cn.com.yusys.yusp.repository.mapper.CusLstDedkkhYjsxMapper;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhYjsxService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:07
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstDedkkhYjsxService {

    @Autowired
    private CusLstDedkkhYjsxMapper cusLstDedkkhYjsxMapper;
    @Autowired
    private CusLstDedkkhMapper cusLstDedkkhMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusLstDedkkhYjsx selectByPrimaryKey(String serno) {
        return cusLstDedkkhYjsxMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusLstDedkkhYjsx> selectAll(QueryModel model) {
        List<CusLstDedkkhYjsx> records = (List<CusLstDedkkhYjsx>) cusLstDedkkhYjsxMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusLstDedkkhYjsx> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstDedkkhYjsx> list = cusLstDedkkhYjsxMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusLstDedkkhYjsx record) {
        return cusLstDedkkhYjsxMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusLstDedkkhYjsx record) {
        return cusLstDedkkhYjsxMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusLstDedkkhYjsx record) {
        return cusLstDedkkhYjsxMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusLstDedkkhYjsx record) {
        return cusLstDedkkhYjsxMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstDedkkhYjsxMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstDedkkhYjsxMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertMore
     * @方法描述: 根据dto中数据进行事务插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertMore(CusLstDedkkhYjsxDto dto) {
        int row = cusLstDedkkhMapper.insertSelective(dto.getCusLstDedkkh());
        if (row != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return -1;
        }
        for (CusLstDedkkhYjsx cusLstDedkkhYjsx : dto.getList()) {
            int rows = cusLstDedkkhYjsxMapper.insertSelective(cusLstDedkkhYjsx);
            if (rows != 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return -1;
            }
        }
        return 1;
    }

    /**
     * @方法名称: updateMore
     * @方法描述: 根据dto中数据进行事务插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateMore(CusLstDedkkhYjsxDto dto) {
        //插入主表cusLstDedkkh信息
        int row = cusLstDedkkhMapper.updateByPrimaryKeySelective(dto.getCusLstDedkkh());
        if (row != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return -1;
        }
        //获取原来主表关联的子表cusLstDedkkhYjsx信息
        List<CusLstDedkkhYjsx> yjsxList = cusLstDedkkhYjsxMapper.selectByListSerno(dto.getList().get(0).getListSerno());
        for (int i = 0; i < yjsxList.size(); i++) {
            boolean log = false;
            for (int j = 0; j < dto.getList().size(); j++) {
                if (yjsxList.get(i).getSerno().equals(dto.getList().get(j).getSerno())) {
                    log = true;
                }
            }
            //如果原来子表有数据不在新上传的list中，则将它删除
            if (!log) {
                int rows = cusLstDedkkhYjsxMapper.deleteByPrimaryKey(yjsxList.get(i).getSerno());
                if (rows != 1) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return -1;
                }
            }
        }
        //将新上传的数据，属于新增的，将其插入
        for (CusLstDedkkhYjsx cusLstDedkkhYjsx : dto.getList()) {
            if (cusLstDedkkhYjsxMapper.selectByPrimaryKey(cusLstDedkkhYjsx.getSerno()) == null) {
                int rows = cusLstDedkkhYjsxMapper.insertSelective(cusLstDedkkhYjsx);
                if (rows != 1) {
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return -1;
                }
            }
        }
        return 1;
    }
}