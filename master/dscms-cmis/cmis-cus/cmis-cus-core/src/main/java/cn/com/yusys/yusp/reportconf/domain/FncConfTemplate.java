package cn.com.yusys.yusp.reportconf.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfTemplate
 * @类描述: FNC_CONF_TEMPLATE数据实体类
 * @功能描述:
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 14:37:37
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Entity
@Table(name = "FNC_CONF_TEMPLATE")
@ApiModel("财务报表配置")
public class FncConfTemplate extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 报表编号 **/
	@Id
	@Column(name = "FNC_ID", unique = true, nullable = false, length = 6)
	@ApiModelProperty("报表编号")
	private String fncId;

	/** 报表名称 **/
	@Column(name = "FNC_NAME", unique = false, nullable = true, length = 200)
	@ApiModelProperty("报表名称")
	private String fncName;

	/** 资产负债表(新)样式编号 **/
	@Column(name = "FNC_SOFP_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("资产负债表(新)样式编号")
	private String fncSofpStyleId;

	/** 利润表 **/
	@Column(name = "FNC_PS_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("利润表")
	private String fncPsStyleId;

	/** 资产负债表样式编号 **/
	@Column(name = "FNC_BS_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty(" 资产负债表样式编号")
	private String fncBsStyleId;

	/** 损益表 **/
	@Column(name = "FNC_PL_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("损益表")
	private String fncPlStyleId;

	/** 现金流量 **/
	@Column(name = "FNC_CF_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("现金流量")
	private String fncCfStyleId;

	/** 财务指标 **/
	@Column(name = "FNC_FI_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("财务指标")
	private String fncFiStyleId;

	/** 会计科目余额表 **/
	@Column(name = "FNC_ACC_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("会计科目余额表")
	private String fncAccStyleId;

	/** 经济合作社财务收支明细表 **/
	@Column(name = "FNC_DE_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("经济合作社财务收支明细表")
	private String fncDeStyleId;

	/** 所有者权益变动 **/
	@Column(name = "FNC_RI_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("所有者权益变动")
	private String fncRiStyleId;

	/** 简表 **/
	@Column(name = "FNC_SMP_STYLE_ID", unique = false, nullable = true, length = 6)
	@ApiModelProperty("简表")
	private String fncSmpStyleId;

	/** 保留1 **/
	@Column(name = "FNC_STYLE_ID1", unique = false, nullable = true, length = 6)
	@ApiModelProperty("保留1")
	private String fncStyleId1;

	/** 保留2 **/
	@Column(name = "FNC_STYLE_ID2", unique = false, nullable = true, length = 6)
	@ApiModelProperty("保留2")
	private String fncStyleId2;

	/** 新旧报表标志STD_ZB_FNC_ON_TYP **/
	@Column(name = "NO_IND", unique = false, nullable = true, length = 1)
	@ApiModelProperty("新旧报表标志")
	private String noInd;

	/** 企事业报表标志STD_ZB_FNC_COMIND **/
	@Column(name = "COM_IND", unique = false, nullable = true, length = 1)
	@ApiModelProperty("企事业报表标志")
	private String comInd;

	/** 资产负债类财务信息样式标号FNC_BSS_STYLE_ID **/
	@Column(name = "FNC_BSS_STYLE_ID", unique = false, nullable = true, length = 1)
	@ApiModelProperty("资产负债类财务信息样式标号")
	private String fncBssStyleId;

	/** 利润表类财务信息样式标号FNC_PSS_STYLE_ID **/
	@Column(name = "FNC_PSS_STYLE_ID", unique = false, nullable = true, length = 1)
	@ApiModelProperty("利润表类财务信息样式标号")
	private String fncPssStyleId;

	/** 财务分析指标样式标号FNC_FAS_STYLE_ID **/
	@Column(name = "FNC_FAS_STYLE_ID", unique = false, nullable = true, length = 1)
	@ApiModelProperty("财务分析指标样式标号")
	private String fncFasStyleId;

	/**
	 * @param fncId
	 */
	public void setFncId(String fncId) {
		this.fncId = fncId == null ? null : fncId.trim();
	}

	/**
	 * @return FncId
	 */
	public String getFncId() {
		return this.fncId;
	}

	/**
	 * @param fncName
	 */
	public void setFncName(String fncName) {
		this.fncName = fncName == null ? null : fncName.trim();
	}

	/**
	 * @return FncName
	 */
	public String getFncName() {
		return this.fncName;
	}

	/**
	 * @param fncBsStyleId
	 */
	public void setFncBsStyleId(String fncBsStyleId) {
		this.fncBsStyleId = fncBsStyleId == null ? null : fncBsStyleId.trim();
	}

	/**
	 * @return FncBsStyleId
	 */
	public String getFncBsStyleId() {
		return this.fncBsStyleId;
	}

	/**
	 * @param fncPlStyleId
	 */
	public void setFncPlStyleId(String fncPlStyleId) {
		this.fncPlStyleId = fncPlStyleId == null ? null : fncPlStyleId.trim();
	}

	/**
	 * @return FncPlStyleId
	 */
	public String getFncPlStyleId() {
		return this.fncPlStyleId;
	}

	/**
	 * @param fncCfStyleId
	 */
	public void setFncCfStyleId(String fncCfStyleId) {
		this.fncCfStyleId = fncCfStyleId == null ? null : fncCfStyleId.trim();
	}

	/**
	 * @return FncCfStyleId
	 */
	public String getFncCfStyleId() {
		return this.fncCfStyleId;
	}

	/**
	 * @param fncFiStyleId
	 */
	public void setFncFiStyleId(String fncFiStyleId) {
		this.fncFiStyleId = fncFiStyleId == null ? null : fncFiStyleId.trim();
	}

	/**
	 * @return FncFiStyleId
	 */
	public String getFncFiStyleId() {
		return this.fncFiStyleId;
	}

	/**
	 * @param fncAccStyleId
	 */
	public void setFncAccStyleId(String fncAccStyleId) {
		this.fncAccStyleId = fncAccStyleId == null ? null : fncAccStyleId.trim();
	}

	/**
	 * @return FncAccStyleId
	 */
	public String getFncAccStyleId() {
		return this.fncAccStyleId;
	}

	/**
	 * @param fncDeStyleId
	 */
	public void setFncDeStyleId(String fncDeStyleId) {
		this.fncDeStyleId = fncDeStyleId == null ? null : fncDeStyleId.trim();
	}

	/**
	 * @return FncDeStyleId
	 */
	public String getFncDeStyleId() {
		return this.fncDeStyleId;
	}

	/**
	 * @param fncRiStyleId
	 */
	public void setFncRiStyleId(String fncRiStyleId) {
		this.fncRiStyleId = fncRiStyleId == null ? null : fncRiStyleId.trim();
	}

	/**
	 * @return FncRiStyleId
	 */
	public String getFncRiStyleId() {
		return this.fncRiStyleId;
	}

	/**
	 * @param fncSmpStyleId
	 */
	public void setFncSmpStyleId(String fncSmpStyleId) {
		this.fncSmpStyleId = fncSmpStyleId == null ? null : fncSmpStyleId.trim();
	}

	/**
	 * @return FncSmpStyleId
	 */
	public String getFncSmpStyleId() {
		return this.fncSmpStyleId;
	}

	/**
	 * @param fncStyleId1
	 */
	public void setFncStyleId1(String fncStyleId1) {
		this.fncStyleId1 = fncStyleId1 == null ? null : fncStyleId1.trim();
	}

	/**
	 * @return FncStyleId1
	 */
	public String getFncStyleId1() {
		return this.fncStyleId1;
	}

	/**
	 * @param fncStyleId2
	 */
	public void setFncStyleId2(String fncStyleId2) {
		this.fncStyleId2 = fncStyleId2 == null ? null : fncStyleId2.trim();
	}

	/**
	 * @return FncStyleId2
	 */
	public String getFncStyleId2() {
		return this.fncStyleId2;
	}

	/**
	 * @param noInd
	 */
	public void setNoInd(String noInd) {
		this.noInd = noInd == null ? null : noInd.trim();
	}

	/**
	 * @return NoInd
	 */
	public String getNoInd() {
		return this.noInd;
	}

	/**
	 * @param comInd
	 */
	public void setComInd(String comInd) {
		this.comInd = comInd == null ? null : comInd.trim();
	}

	/**
	 * @return ComInd
	 */
	public String getComInd() {
		return this.comInd;
	}

	public String getFncSofpStyleId() {
		return fncSofpStyleId;
	}

	public void setFncSofpStyleId(String fncSofpStyleId) {
		this.fncSofpStyleId = fncSofpStyleId;
	}

	public String getFncPsStyleId() {
		return fncPsStyleId;
	}

	public void setFncPsStyleId(String fncPsStyleId) {
		this.fncPsStyleId = fncPsStyleId;
	}

	public String getFncBssStyleId() {
		return fncBssStyleId;
	}

	public void setFncBssStyleId(String fncBssStyleId) {
		this.fncBssStyleId = fncBssStyleId;
	}

	public String getFncPssStyleId() {
		return fncPssStyleId;
	}

	public void setFncPssStyleId(String fncPssStyleId) {
		this.fncPssStyleId = fncPssStyleId;
	}

	public String getFncFasStyleId() {
		return fncFasStyleId;
	}

	public void setFncFasStyleId(String fncFasStyleId) {
		this.fncFasStyleId = fncFasStyleId;
	}

}