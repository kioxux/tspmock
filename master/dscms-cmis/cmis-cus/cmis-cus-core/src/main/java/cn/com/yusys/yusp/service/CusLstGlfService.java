/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusLstDedkkh;
import cn.com.yusys.yusp.domain.CusLstGlf;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusLstGlfDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501RespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstDedkkhMapper;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfMapper;
import cn.com.yusys.yusp.vo.CusLstDedkkhVo;
import cn.com.yusys.yusp.vo.CusLstGlfVo;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstGlfService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-13 20:17:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstGlfService {

    @Autowired
    private CusLstGlfMapper cusLstGlfMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    @Autowired
    private CusCorpService cusCorpService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstGlf selectByPrimaryKey(String serno) {
        return cusLstGlfMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstGlf> selectAll(QueryModel model) {
        List<CusLstGlf> records = cusLstGlfMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstGlf> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstGlf> list = cusLstGlfMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstGlf record) {
        return cusLstGlfMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstGlf record) {
        return cusLstGlfMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstGlf record) {
        return cusLstGlfMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstGlf record) {
        return cusLstGlfMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstGlfMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstGlfMapper.deleteByIds(ids);
    }

    public List<Map<String,Object>> selectTreeList(String certNo) {
        QueryModel model = new QueryModel();
        model.addCondition("status","1");//关联方树只展示生效的数据
        model.setSort("cus_id desc");
        List<CusLstGlf> list = cusLstGlfMapper.selectByModel(model);
        //将所有对象放到map中 格式是 <pid,List<Node>>
        Map<String, ArrayList<CusLstGlf>> allMap = new HashMap<>(16);
        for (CusLstGlf item:list) {
            if(allMap.containsKey(item.getParebtRelatedPartyCertNo())){
                allMap.get(item.getParebtRelatedPartyCertNo()).add(item);
            }else{
                ArrayList<CusLstGlf> sublist = new ArrayList<>();
                sublist.add(item);
                allMap.put(item.getParebtRelatedPartyCertNo(),sublist);
            }
        }
        return getTree(certNo,allMap);
    }

    /**
     * @param root, all]
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @author tangxun
     * @date 2021/1/12 9:52
     * @version 1.0.0
     * @desc 组装前端要求的格式
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private List<Map<String,Object>> getTree(String root, Map<String, ArrayList<CusLstGlf>> all) {
        if(all.containsKey(root)){
            return all.get(root).stream().map(item->{
                Map<String,Object> map = new LinkedHashMap<>();
                map.put("label", item.getRelatedPartyName());
                map.put("id", item.getCertCode());
                map.put("data", item);
                List list = getTree(item.getCertCode(),all);
                if(!list.isEmpty()){
                    map.put("children",list);
                }
                return map;
            }).collect(Collectors.toList());
        }else{
            return new ArrayList<>();
        }
    }
    /**
     * @param certCode
     * @return
     * @author 周茂伟
     * @date 2021年4月25日23:48:11
     * @version 1.0.0
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int selectCount(String certCode){
        return cusLstGlfMapper.selectCount(certCode);
    }


    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusLstGlfVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param cusLstGlfVoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> cusLstGlfVoList) {
        User userInfo = SessionUtils.getUserInformation();
        List<CusLstGlfVo> cusBatCusLstGlf = (List<CusLstGlfVo>) BeanUtils.beansCopy(cusLstGlfVoList, CusLstGlfVo.class);
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CusLstGlfMapper cusLstGlfMapper = sqlSession.getMapper(CusLstGlfMapper.class);

            for (CusLstGlfVo cusLstGlfVo : cusBatCusLstGlf) {
                CusLstGlf clg = new CusLstGlf();
                BeanUtils.beanCopy(cusLstGlfVo, clg);
                QueryModel qm = new QueryModel();
                qm.getCondition().put("certCode", clg.getCertCode());
                qm.getCondition().put("certType", clg.getCertType());
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                clg.setInputBrId(userInfo.getOrg().getCode());
                clg.setInputId(userInfo.getLoginCode());
                clg.setInputDate(date);
                clg.setCreateTime(date1);
                clg.setUpdateTime(date1);
                clg.setUpdId(userInfo.getLoginCode());
                clg.setStatus("1");
                clg.setUpdBrId(userInfo.getOrg().getCode());
                List<CusLstGlf> glfList = cusLstGlfMapper.selectByModel(qm);
                if (glfList != null && glfList.size()>0) {
                    glfList.stream().forEach(ded -> {
                        clg.setSerno(ded.getSerno());
                        cusLstGlfMapper.updateByPrimaryKey(clg);
                    });
                } else {
                    cusLstGlfMapper.insertSelective(clg);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return cusBatCusLstGlf == null ? 0 : cusBatCusLstGlf.size();
    }


    /**
     * @方法名称: updateCusId
     * @方法描述: 一键更新客户号
     * @参数与返回说明:
     * @算法描述:
     */
    public int updateCusId() {
        //查询客户号为空的数据
        List<CusLstGlf> cusLstGlfList = cusLstGlfMapper.selectCusIdIsNull();
        for (CusLstGlf cusLstGlf : cusLstGlfList) {
            if(cusLstGlf.getRelatedPartyType() == null ||cusLstGlf.getRelatedPartyType().equals("")){
                continue;
            }

            if (cusLstGlf.getRelatedPartyType().equals(CmisCusConstants.STD_ZB_RELATED_PARTY_TYPE_O2)) {//02-自然人
                try {
                    S10501ReqDto s10501ReqDto = new S10501ReqDto();
                    s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
                    s10501ReqDto.setIdtftp(cusLstGlf.getCertType());//   证件类型
                    s10501ReqDto.setIdtfno(cusLstGlf.getCertCode());//   证件号码
                    // 发送客户三要素发送ecfi查询
                    ResultDto<S10501RespDto> s10501ResultDto = dscms2EcifClientService.s10501(s10501ReqDto);
                    String s10501Code = Optional.ofNullable(s10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String s10501Meesage = Optional.ofNullable(s10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    S10501RespDto s10501RespDto = null;
                    if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //  获取查询结果 todo联调验证交易成功查询失败情况
                        s10501RespDto = s10501ResultDto.getData();
                        if (s10501RespDto.getListnm() != null) {
                            List<ListArrayInfo> listArrayInfo = s10501RespDto.getListArrayInfo();
                            // 客户号
                            cusLstGlf.setCusId(listArrayInfo.get(0).getCustno());
                            cusLstGlfMapper.updateByPrimaryKeySelective(cusLstGlf);
                        }
                    }
                }catch (Exception e){

                }

            } else {
                try{
                    CusCorpDto cusCorpDto = new CusCorpDto();
                    cusCorpDto.setCertCode(cusLstGlf.getCertCode());
                    cusCorpDto.setCertType(cusLstGlf.getCertType());
                    cusLstGlf.setCusId(cusCorpService.queryG10501(cusCorpDto));
                    cusLstGlfMapper.updateByPrimaryKeySelective(cusLstGlf);
                }catch (Exception e){
                    //不抛出异常
                }

            }
        }
        return 0;
    }

    /**
     * 根据客户号查询生效的记录数
     * @param cusId
     * @return
     */
    public int queryCountsByCusId(String cusId){
        return cusLstGlfMapper.queryCountsByCusId(cusId);
    }

    /**
     * 根据客户号查询关联方名单信息
     * @param cusId
     * @return
     */
    public CusLstGlfDto queryCusLstGlfByCusId(String cusId){
        return cusLstGlfMapper.queryCusLstGlfByCusId(cusId);
    }
    /**
     * 查询生效的自然人关联方
     * @param
     * @return
     */
    public List<CusLstGlfDto> queryCusLstGlfForIndiv() {
        List<CusLstGlfDto> list = cusLstGlfMapper.queryCusLstGlfForIndiv();
        return list;
    }
}
