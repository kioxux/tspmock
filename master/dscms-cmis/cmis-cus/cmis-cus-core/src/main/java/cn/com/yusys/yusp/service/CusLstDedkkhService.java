/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusBatPubHandoverLstMapper;
import cn.com.yusys.yusp.repository.mapper.CusLstDedkkhMapper;
import cn.com.yusys.yusp.vo.CusLstDedkkhVo;
import cn.com.yusys.yusp.vo.CusLstJjycExportVo;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-03 14:36:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstDedkkhService {

    @Autowired
    private CusLstDedkkhMapper cusLstDedkkhMapper;

    @Autowired
    private CusLstDedkkhYjsxService cusLstDedkkhYjsxService;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private CusBaseMapper cusBaseMapper;

    @Resource
    private AdminSmUserService adminSmUserService;

    @Resource
    private AdminSmOrgService adminSmOrgService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusLstDedkkh selectByPrimaryKey(String listSerno) {
        return cusLstDedkkhMapper.selectByPrimaryKey(listSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusLstDedkkh> selectAll(QueryModel model) {
        List<CusLstDedkkh> records = (List<CusLstDedkkh>) cusLstDedkkhMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusLstDedkkh> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstDedkkh> list = cusLstDedkkhMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusLstDedkkh record) {
        return cusLstDedkkhMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusLstDedkkh record) {
        return cusLstDedkkhMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusLstDedkkh record) {
        return cusLstDedkkhMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusLstDedkkh record) {
        return cusLstDedkkhMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String listSerno) {
        return cusLstDedkkhMapper.deleteByPrimaryKey(listSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstDedkkhMapper.deleteByIds(ids);
    }

    public List<Map<String, Object>> selectByModelxp(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = cusLstDedkkhMapper.selectByModelxp(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @函数名称:insertOrUpdateData
     * @函数描述:新增 修改
     * @参数与返回说明:
     * @算法描述:
     */

    public Map<String, Object> insertOrUpdateData(CusLstDedkkhFormAndTableData cusLstDedkkhFormAndTableData) {
        //获取当前登录信息
        Map<String, Object> resultMap = new HashMap<>();
        CusLstDedkkh cusLstDedkkh = cusLstDedkkhFormAndTableData.getCusLstDedkkhData();
        List<CusLstDedkkhYjsx> list = cusLstDedkkhFormAndTableData.getCusLstDedkkhYjsxData();
        User userInfo = SessionUtils.getUserInformation();
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        // 主表 新增  修改
        QueryModel qm = new QueryModel();
        qm.getCondition().put("cusId", cusLstDedkkh.getCusId());
        qm.getCondition().put("pressureDropYearly", cusLstDedkkh.getPressureDropYearly());
        List<CusLstDedkkh> dedkkhList = cusLstDedkkhMapper.selectByModel(qm);
        resultMap.put("rtCode", "0000");
        resultMap.put("rtMessage", "");
        //如果该客户对应年度存在数据
        if (!dedkkhList.isEmpty()) {
            if (dedkkhList.size() == 1) {
                if ("1".equals(dedkkhList.get(0).getStatus())) {// 生效
                    resultMap.put("rtCode", "9999");
                    resultMap.put("rtMessage", "该客户已存在对应年度：" + cusLstDedkkh.getPressureDropYearly() + "生效的压降计划！");
                }
                if ("2".equals(dedkkhList.get(0).getStatus())) { //暂存
                    //修改
                    cusLstDedkkh.setUpdId(userInfo.getLoginCode());
                    cusLstDedkkh.setUpdBrId(userInfo.getOrg().getCode());
                    cusLstDedkkh.setUpdDate(nowDate);
                    cusLstDedkkh.setUpdateTime(DateUtils.getCurrDate());
                    cusLstDedkkhMapper.updateByPrimaryKey(cusLstDedkkh);
                    for (CusLstDedkkhYjsx cusLstDedkkhYjsx : list) {
                        CusLstDedkkhYjsx isCusLstDedkkhYjsx = cusLstDedkkhYjsxService.selectByPrimaryKey(cusLstDedkkhYjsx.getSerno());
                        // 主表修改  子表 需要判断
                        if (isCusLstDedkkhYjsx == null) {
                            cusLstDedkkhYjsx.setListSerno(cusLstDedkkh.getListSerno());
                            cusLstDedkkhYjsx.setUpdId(userInfo.getLoginCode());
                            cusLstDedkkhYjsx.setUpdBrId(userInfo.getOrg().getCode());
                            cusLstDedkkhYjsx.setUpdDate(nowDate);
                            cusLstDedkkhYjsx.setCreateTime(DateUtils.getCurrDate());
                            if (cusLstDedkkhYjsx.getPressureDropAmt() == null || "".equals(cusLstDedkkhYjsx.getPressureDropAmt())) {
                                cusLstDedkkhYjsx.setPressureDropAmt("0");
                                cusLstDedkkhYjsx.setActPdoAmt(new BigDecimal(0));
                            }
                            cusLstDedkkhYjsxService.insertSelective(cusLstDedkkhYjsx);
                        } else {
                            cusLstDedkkhYjsx.setUpdId(userInfo.getLoginCode());
                            cusLstDedkkhYjsx.setUpdBrId(userInfo.getOrg().getCode());
                            cusLstDedkkhYjsx.setUpdDate(nowDate);
                            cusLstDedkkhYjsx.setUpdateTime(DateUtils.getCurrDate());
                            if ("".equals(cusLstDedkkhYjsx.getPressureDropAmt())) {
                                cusLstDedkkhYjsx.setPressureDropAmt("0");
                                cusLstDedkkhYjsx.setActPdoAmt(new BigDecimal(0));
                            }
                            cusLstDedkkhYjsxService.updateSelective(cusLstDedkkhYjsx);
                        }
                    }
                }
                if ("0".equals(dedkkhList.get(0).getStatus())) {//失效
                    resultMap.put("rtCode", "9999");
                    resultMap.put("rtMessage", "该客户已存在对应年度：" + cusLstDedkkh.getPressureDropYearly() + "失效的压降计划！");
                }
            }
            if (dedkkhList.size() > 1) {
                resultMap.put("rtCode", "9999");
                resultMap.put("rtMessage", "该客户对应年度：" + cusLstDedkkh.getPressureDropYearly() + "存在多条压降计划，请先在大额贷款客户名单中维护！");
            }
        } else {
                //新增一条新数据
                cusLstDedkkh.setCreateTime(DateUtils.getCurrDate());
                insertSelective(cusLstDedkkh);
                for (CusLstDedkkhYjsx cusLstDedkkhYjsx : list) {
                    // 主表新增 子表一定新增
                    if (cusLstDedkkhYjsx.getPressureDropAmt() == null || "".equals(cusLstDedkkhYjsx.getPressureDropAmt())) {
                        cusLstDedkkhYjsx.setPressureDropAmt("0");
                        cusLstDedkkhYjsx.setActPdoAmt(new BigDecimal(0));
                    }
                    cusLstDedkkhYjsx.setListSerno(cusLstDedkkh.getListSerno());
                    cusLstDedkkhYjsx.setCreateTime(DateUtils.getCurrDate());
                    cusLstDedkkhYjsxService.insertSelective(cusLstDedkkhYjsx);
                }
        }
        return resultMap;
    }


    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusLstDedkkhVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param dedkkhVoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> dedkkhVoList) {
        User userInfo = SessionUtils.getUserInformation();
        List<CusLstDedkkhVo> cusBatCusLstDedkkh = (List<CusLstDedkkhVo>) BeanUtils.beansCopy(dedkkhVoList, CusLstDedkkhVo.class);
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CusLstDedkkhMapper cusLstDedkkhMapper = sqlSession.getMapper(CusLstDedkkhMapper.class);

            for (CusLstDedkkhVo cusLstDedkkhVo : cusBatCusLstDedkkh) {
                if (StringUtil.isEmpty(cusLstDedkkhVo.getCusId())) {
                    throw BizException.error(null, "9999", "客户编号不能为空");
                }
                if (StringUtil.isEmpty(cusLstDedkkhVo.getCusName())) {
                    throw BizException.error(null, "9999", "客户名称不能为空");
                }
                if (StringUtil.isEmpty(cusLstDedkkhVo.getManagerId())) {
                    throw BizException.error(null, "9999", "管户客户经理不能为空");
                }
                if (StringUtil.isEmpty(cusLstDedkkhVo.getBelgOrg())) {
                    throw BizException.error(null, "9999", "所属机构不能为空");
                }
                if (StringUtil.isEmpty(cusLstDedkkhVo.getPlanAmt())) {
                    throw BizException.error(null, "9999", "本年度计划总压降金额（万元）不能为空");
                }
                if (StringUtil.isEmpty(cusLstDedkkhVo.getPressureDropYearly())) {
                    throw BizException.error(null, "9999", "压降年度不能为空");
                }
                if (StringUtil.isEmpty(cusLstDedkkhVo.getRiskRelieveStep())) {
                    throw BizException.error(null, "9999", "风险缓释措施不能为空");
                }
                if (StringUtil.isEmpty(cusLstDedkkhVo.getInputId())) {
                    throw BizException.error(null, "9999", "登记人不能为空");
                }
                ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(cusLstDedkkhVo.getManagerId());
                if (manager.getData().getLoginCode() == null) {
                    throw BizException.error(null, "9999", "管户客户经理号不存在");
                }
                ResultDto<AdminSmOrgDto> Sorg = adminSmOrgService.getByOrgCode(cusLstDedkkhVo.getBelgOrg());
                if (Sorg.getData().getOrgId() == null) {
                    throw BizException.error(null, "9999", "所属机构号不存在");
                }

                    //客户编号，客户经理必须存在，
                QueryModel cqm = new QueryModel();
                cqm.getCondition().put("cusId", cusLstDedkkhVo.getCusId());
                List<CusBase> exists = cusBaseMapper.selectByModel(cqm);
                if(exists.isEmpty()){
                    throw BizException.error(null, "9999", cusLstDedkkhVo.getCusId()+"客户编号不存在");
                }else {
                    for (int i = 0; i<exists.size();i++) {
                      if(!exists.get(i).getCusName().equals(cusLstDedkkhVo.getCusName())){
                          throw BizException.error(null, "9999", "导入的客户编号:"+cusLstDedkkhVo.getCusId()+"对应的客户姓名"
                                  +cusLstDedkkhVo.getCusName()+"与客户信息的名称"+exists.get(i).getCusName()+"不一致");
                      }
                    }
                }
      //          ResultDto<AdminSmUserDto> adminSmUserResultDto = adminSmUserService.getByLoginCode(cusLstDedkkhVo.getManagerId());
                CusLstDedkkh kkh = new CusLstDedkkh();
                BeanUtils.beanCopy(cusLstDedkkhVo, kkh);
                QueryModel qm = new QueryModel();
                qm.getCondition().put("cusId", kkh.getCusId());
                qm.getCondition().put("pressureDropYearly", kkh.getPressureDropYearly());
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                kkh.setInputBrId(userInfo.getOrg().getCode());
                kkh.setInputId(userInfo.getLoginCode());
                kkh.setInputDate(date);
                kkh.setCreateTime(date1);
                kkh.setUpdateTime(date1);
                kkh.setUpdId(userInfo.getLoginCode());
                kkh.setStatus("1");//初始化生效状态
                kkh.setEnterListFlag("01");
                kkh.setUpdBrId(userInfo.getOrg().getCode());
                List<CusLstDedkkh> dedkkhList = cusLstDedkkhMapper.selectByModel(qm);
                QueryModel queryModel = new QueryModel();
                queryModel.getCondition().put("listSerno",kkh.getListSerno());
                CusLstDedkkhYjsx cusLstDedkkhYjsx = new CusLstDedkkhYjsx();
                //压降事项新增或修改；
                if (cusLstDedkkhYjsx.getPressureDropAmt() == null || "".equals(cusLstDedkkhYjsx.getPressureDropAmt())) {
                    cusLstDedkkhYjsx.setPressureDropAmt("0");
                    cusLstDedkkhYjsx.setActPdoAmt(new BigDecimal(0));
                }
                cusLstDedkkhYjsx.setListSerno(kkh.getListSerno());
                if(StringUtils.isNotEmpty(cusLstDedkkhVo.getPressureDropItem())){
                    cusLstDedkkhYjsx.setPressureDropItem(cusLstDedkkhVo.getPressureDropItem());
                    queryModel.getCondition().put("pressureDropItem",cusLstDedkkhVo.getPressureDropItem());
                }
                if(StringUtils.isNotEmpty(cusLstDedkkhVo.getPressureDropAmt())){
                    cusLstDedkkhYjsx.setPressureDropAmt(cusLstDedkkhVo.getPressureDropAmt());
                    queryModel.getCondition().put("pressureDropAmt",cusLstDedkkhVo.getPressureDropAmt());
                }
                if(StringUtils.isNotEmpty(cusLstDedkkhVo.getPressureDropFinishTime())){
                    cusLstDedkkhYjsx.setPressureDropFinishTime(cusLstDedkkhVo.getPressureDropFinishTime());
                    queryModel.getCondition().put("pressureDropFinishTime",cusLstDedkkhVo.getPressureDropFinishTime());
                }
                cusLstDedkkhYjsx.setListSerno(kkh.getListSerno());
                cusLstDedkkhYjsx.setCreateTime(DateUtils.getCurrDate());
                List<CusLstDedkkhYjsx> dedkkhYjsxList = cusLstDedkkhYjsxService.selectByModel(queryModel);
                if (dedkkhList != null && dedkkhList.size() >0) {
                    dedkkhList.stream().forEach(ded -> {
                        kkh.setListSerno(ded.getListSerno());
                        cusLstDedkkhMapper.updateByPrimaryKey(kkh);
                        cusLstDedkkhYjsx.setListSerno(ded.getListSerno());
                        if(dedkkhYjsxList!= null && dedkkhYjsxList.size() >0){
                            cusLstDedkkhYjsxService.updateSelective(cusLstDedkkhYjsx);
                        }else{
                            cusLstDedkkhYjsxService.insertSelective(cusLstDedkkhYjsx);
                        }

                    });
                } else {
                    cusLstDedkkhMapper.insertSelective(kkh);
                    cusLstDedkkhYjsx.setListSerno(kkh.getListSerno());
                    cusLstDedkkhYjsxService.insertSelective(cusLstDedkkhYjsx);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return cusBatCusLstDedkkh == null ? 0 : cusBatCusLstDedkkh.size();
    }

    /**
     * @函数名称:queryNewDekhDataByCusId
     * @函数描述:根据客户号查询最新的信息 审查报告
     * @参数与返回说明:
     * @算法描述:
     */

    public CusLstDedkkh queryNewDekhDataByCusId(CusLstDedkkh cusLstDedkkh) {
        CusLstDedkkh cusLstDedkkh1 = cusLstDedkkhMapper.queryNewDekhDataByCusId(cusLstDedkkh);
        return cusLstDedkkh1;
    }

    public Map queryNewDekhByCusId(String cusId){
        return cusLstDedkkhMapper.queryNewDekhByCusId(cusId);
    }

    public int updateStatusPass(String cusId) {
        return cusLstDedkkhMapper.updateStatusPass(cusId);
    }
}
