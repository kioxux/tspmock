/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CusLstWjkhApp;
import cn.com.yusys.yusp.repository.mapper.CusLstWjkhAppMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusLstWjkhAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: pl
 * @创建时间: 2021-04-07 09:50:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstWjkhAppService {

    @Autowired
    private CusLstWjkhAppMapper cusLstWjkhAppMapper;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusLstWjkhApp selectByPrimaryKey(String serno) {
        return cusLstWjkhAppMapper.selectByPrimaryKey(serno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstWjkhApp> selectAll(QueryModel model) {
        List<CusLstWjkhApp> records = (List<CusLstWjkhApp>) cusLstWjkhAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusLstWjkhApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstWjkhApp> list = cusLstWjkhAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusLstWjkhApp record) {
        return cusLstWjkhAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusLstWjkhApp record) {
        return cusLstWjkhAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusLstWjkhApp record) {
        return cusLstWjkhAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusLstWjkhApp record) {
        return cusLstWjkhAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstWjkhAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstWjkhAppMapper.deleteByIds(ids);
    }

    /**
     * 银票签发白名单申请提交，后续的业务处理
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterStart(String serno) {
        CusLstWjkhApp cusLstYpqfWhiteApp = cusLstWjkhAppMapper.selectByPrimaryKey(serno);
        cusLstYpqfWhiteApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        cusLstWjkhAppMapper.updateByPrimaryKey(cusLstYpqfWhiteApp);
    }


    /**
     * 网金签发白名单申请拒绝，后续的业务处理
     *
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterRefuse(String serno) {
        CusLstWjkhApp cusLstYpqfWhiteApp = cusLstWjkhAppMapper.selectByPrimaryKey(serno);
        cusLstYpqfWhiteApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        cusLstWjkhAppMapper.updateByPrimaryKey(cusLstYpqfWhiteApp);
    }

    /**
     * 网金签发白名单申请打回，后续的业务处理
     *
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterBack(String serno) {
        CusLstWjkhApp cusLstYpqfWhiteApp = cusLstWjkhAppMapper.selectByPrimaryKey(serno);
        cusLstYpqfWhiteApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        cusLstWjkhAppMapper.updateByPrimaryKey(cusLstYpqfWhiteApp);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    public int tempSave(CusLstWjkhApp cusLstWjkhApp) {
        String serno = cusLstWjkhApp.getSerno();
        //查询该对象是否存在
        CusLstWjkhApp queryCusLstWjkhApp = cusLstWjkhAppMapper.selectByPrimaryKey(serno);
        if (null == queryCusLstWjkhApp){
            return cusLstWjkhAppMapper.insert(cusLstWjkhApp);
        }else{
            return cusLstWjkhAppMapper.updateByPrimaryKey(cusLstWjkhApp);
        }
    }
}
