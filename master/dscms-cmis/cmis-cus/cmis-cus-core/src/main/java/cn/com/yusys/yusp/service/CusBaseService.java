/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.req.CmisCus0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.resp.CmisCus0008RespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static cn.com.yusys.yusp.constants.CmisCusConstants.STD_ZB_CUS_CATALOG_1;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: CusBaseService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 15430
 * @创建时间: 2020-11-14 09:59:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CusBaseService {
    private static final Logger logger = LoggerFactory.getLogger(CusBaseService.class);
    @Autowired
    private CusBaseMapper cusBaseMapper;
    @Autowired
    private CusIndivMapper cusIndivMapper;
    @Autowired
    private CusCorpMapper cusCorpMapper;
    @Autowired
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBase selectByPrimaryKey(String cusId) {
        return cusBaseMapper.selectByPrimaryKey(cusId);
    }

    /**
     * @方法名称: queryCusInfoByCertCode
     * @方法描述: 根据证件号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBase queryCusInfoByCertCode(String certCode) {
        return cusBaseMapper.queryCusInfoByCertCode(certCode);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusBase> selectAll(QueryModel model) {
        List<CusBase> records = (List<CusBase>) cusBaseMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBase> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBase> list = cusBaseMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusBase record) {
        return cusBaseMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusBase record) {
        return cusBaseMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusBase record) {
        // 更新人
        record.setUpdId(SessionUtils.getLoginCode());
        // 更新机构
        record.setUpdBrId(SessionUtils.getUserOrganizationId());
        // 更新时间
        record.setUpdDate(DateUtils.getCurrDateStr());
        // 修改时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return cusBaseMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusBase record) {
        // 更新人
        record.setUpdId(SessionUtils.getLoginCode());
        // 更新机构
        record.setUpdBrId(SessionUtils.getUserOrganizationId());
        //判断该客户管护客户经理是否为空
        String cusId = record.getCusId();
        CusBase cus = cusBaseMapper.selectByPrimaryKey(cusId);
        if(cus.getManagerId()==null || "".equals(cus.getManagerId())){
            // 更新管护人
            record.setManagerId(SessionUtils.getLoginCode());
            // 更新管护机构
            record.setManagerBrId(SessionUtils.getUserOrganizationId());
        }
        // 更新时间
        record.setUpdDate(DateUtils.getCurrDateStr());
        // 修改时间
        record.setUpdateTime(DateUtils.getCurrTimestamp());
        return cusBaseMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @param record
     * @return int
     * @author 王玉坤
     * @date 2021/11/14 23:07
     * @version 1.0.0
     * @desc 更新客户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int updateByPrimaryKeySelective(CusBase record) {
        return cusBaseMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId) {
        return cusBaseMapper.deleteByPrimaryKey(cusId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusBaseMapper.deleteByIds(ids);
    }

    public CusRelationsDto findCusBaseByCusId(String cusId) {
        return cusBaseMapper.findCusBaseByCusId(cusId);
    }

    public CusCorpRelationsDto findCusCorpBaseByCusId(String cusId) {
        return cusBaseMapper.findCusCorpBaseByCusId(cusId);
    }

    public Map<String, String> getCusInfoByCusId(String cusId) {
        return cusBaseMapper.getCusInfoByCusId(cusId);
    }

    public List<CusBase> queryCusInfo(List<String> ids) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("ids", ids);
        return cusBaseMapper.queryCusInfo(queryMap);
    }

    /**
     *根据证件号查询客户信息,如果是本行客户则返回,否则去ecif查询,ecif没有去ecif开户,后数据本地落库
     * @param cusBase
     * @return

    public CusBase queryCusByCertCodeToEcif(CusBase cusBase) {
        String certCode = cusBase.getCertCode();
        String cusName = cusBase.getCusName();
        //身份证
        String certType = "";
        //客户名,证件号为空判断
        if (!StringUtils.isBlank(certCode) && !StringUtils.isBlank(cusName)){
            CusBase cusBaseData = cusBaseMapper.queryCusInfoByCertCode(certCode);
            if (cusBaseData == null){
                // 第二步 校验客户信息  发送身份校验
                IdCheckReqDto reqDto = new IdCheckReqDto();
                reqDto.setCert_type(certType);
                reqDto.setUser_name(cusName);
                reqDto.setId_number(certCode);
                //ResultDto<Map<String, Object>> result = dscms2EcifClientService.idCheck(reqDto);
                //log.info("发送身份校验,请求{}，返回{}", reqDto, result);
                //TODO 判断接口返回结果

                // 第三步 再第一步成功的基础上，进行客户三要素发送ecfi查询，
                //识别方式:
                //1- 按证件类型、证件号码查询
                //2- 按证件号码查询
                //3- 按客户编号查询
                //4- 按客户名称模糊查询
                //以下接口必填一种查询方式；
                String cusState = "";
                S10501ReqDto s10501ReqDto = new S10501ReqDto();
                s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_1.key);//   识别方式
                s10501ReqDto.setIdtftp(certType);//   证件类型
                s10501ReqDto.setIdtfno(certCode);//   证件号码
                //  StringUtils.EMPTY的实际值待确认 开始
                s10501ReqDto.setResotp(StringUtils.EMPTY);//   识别方式
                //  完善后续逻辑
                //验证CIF返回报文
                if (true) {

                }else{
                    // 若ecif系统检验不通过，则发送开户接口进行开户
                    G00102ReqDto g00102ReqDto = new G00102ReqDto();
                    // 客户名称
                    g00102ReqDto.setCustna(cusName);
                    // 证件类型
                    g00102ReqDto.setIdtfno(certType);
                    // 证件号码
                    //g00102ReqDto.setEfctdt(indivCusCreateDto.getCertCode());
                    //ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
                    //G00102RespDto g00102ResDto = g00102ResultDto.getData();
                    //第三步，拿到客户信息后先插入任务表，并返回客户信息到信息补录页签
                    //ifExists = sendEcif(indivCusCreateDto);
                    //验证CIF返回报文
                }
            }else{
                return cusBaseData;
            }
        }else{
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"传入值为空\"" + EcsEnum.E_CUS_INFO_ISNULL.value);
        }
        return null;

    }*/
    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据证件类型证件号码查询客户信息
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
     public CusBase selectByCertCode(CusIndivDto cusIndivDto) {
         return cusBaseMapper.selectByCertCode(cusIndivDto);
     }

    /**
     * 根据客户编号查询法人名称,名称，证件类型，证件号，电话
     * @param cmisCus0008ReqDto
     * @return
     */
    public CmisCus0008RespDto cmiscus0008(CmisCus0008ReqDto cmisCus0008ReqDto) {
        return cusBaseMapper.getCmiscus0008(cmisCus0008ReqDto);
    }

    /**
     * @方法名称: selectBaseIndiv
     * @方法描述: 查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBaseDto> selectBaseIndiv(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBaseDto> list = cusBaseMapper.selectBaseIndiv(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectBaseCrop
     * @方法描述: 查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBaseDto> selectBaseCrop(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBaseDto> list = cusBaseMapper.selectBaseCrop(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectBaseCropDis
     * @方法描述: 查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBaseDto> selectBaseCropDis(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBaseDto> list = cusBaseMapper.selectBaseCropDis(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @Description:根据客户号删除客户在cusBase表中的信息(逻辑删除)
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:21
     * @param cusId: 客户号
     * @return: boolean
     **/
    public boolean deleteCusBaseByCusId(String cusId) {
        int i = cusBaseMapper.deleteCusBaseByCusId(cusId);
        return i==1?true:false;
    }

    /**
     * @方法名称: updateInitLoanDate
     * @方法描述: 首笔授信申请审批通过，更新个人客户信息表建立信贷关系时间
     * @参数与返回说明:
     * @算法描述: 无
     * @修改人： xuchi
     */
    public int updateInitLoanDate(CusUpdateInitLoanDateDto cusUpdateInitLoanDateDto) {
        CusBase cusBase = cusBaseMapper.selectByPrimaryKey(cusUpdateInitLoanDateDto.getCusId());
        int ret = 0;
        if(cusBase.getCusCatalog().equals(CmisCusConstants.STD_ZB_CUS_CATALOG_1)){
            CusIndiv cusIndiv =  cusIndivMapper.selectByPrimaryKey(cusUpdateInitLoanDateDto.getCusId());
            if(cusIndiv.getInitLoanDate() == null ||cusIndiv.getInitLoanDate().equals("")){
                cusIndiv.setInitLoanDate(cusUpdateInitLoanDateDto.getInitLoanDate());
                cusIndiv.setUpdDate(DateUtils.getCurrDateStr());
                cusIndiv.setUpdateTime(DateUtils.getCurrTimestamp());
                ret =  cusIndivMapper.updateByPrimaryKeySelective(cusIndiv);
            }

        }else if(cusBase.equals(CmisCusConstants.STD_ZB_CUS_CATALOG_2)){
            CusCorp cusCorp =  cusCorpMapper.selectByPrimaryKey(cusUpdateInitLoanDateDto.getCusId());
            if(cusCorp.getInitLoanDate() == null ||cusCorp.getInitLoanDate().equals("")){
                cusCorp.setInitLoanDate(cusUpdateInitLoanDateDto.getInitLoanDate());
                cusCorp.setUpdDate(DateUtils.getCurrDateStr());
                cusCorp.setUpdateTime(DateUtils.getCurrTimestamp());
                ret =  cusCorpMapper.updateByPrimaryKeySelective(cusCorp);
            }
        }
        return ret;
    }


    /**
     * @param model
     * @return
     * @author wzy
     * @date 2021/6/18 14:35
     * @version 1.0.0
     * @desc  个人列表查询
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public List<CusBaseDto> selectByCondition(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBaseDto> list = cusBaseMapper.selectByCondition(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/8/4 15:20
     * @注释 增享贷查询客户额外参数
     */
    public CusForZxdDto selectbycusidforzxd(String cusId) {
        CusForZxdDto cusForZxdDto  = cusBaseMapper.selectbycusidforzxd(cusId);
        return cusForZxdDto;
    }

    /**
     * 根据客户经理号获取其管户客户号
     * @author jijian_yx
     * @date 2021/8/13 21:12
     **/
    public List<String> queryCusIdByManagerId(String managerId) {
        return cusBaseMapper.queryCusIdByManagerId(managerId);
    }

    /**
     * 获取当前登录人机构下客户号
     * @author jijian_yx
     * @date 2021/8/30 17:05
     **/
    public List<CusBase> queryCusByLogin(QueryModel model) {

        boolean flag = false;

        // 角色控制数据权限
        User loginUser = SessionUtils.getUserInformation();
        List<? extends UserIdentity> roles = loginUser.getRoles();
        if (null != roles && roles.size() > 0) {
            for (int i = 0; i < roles.size(); i++) {
                // 综合客户经理 R0020; 零售客户经理 R0030; 小企业客户经理 R0050
                // 小微客户经理 R0010；客户经理（寿光） RSG01；客户经理（东海） RDH01
                if ("R0020".equals(roles.get(i).getCode()) || "R0030".equals(roles.get(i).getCode()) || "R0050".equals(roles.get(i).getCode())
                        || "R0010".equals(roles.get(i).getCode()) || "RSG01".equals(roles.get(i).getCode()) || "RDH01".equals(roles.get(i).getCode())) {
                    flag = true;
                    break;
                }
            }
        }
        if (flag) {
            model.addCondition("managerId", loginUser.getLoginCode());
        }
//        else {
//            User userinfo = SessionUtils.getUserInformation();
//            String orgCode = userinfo.getOrg().getCode();
//            // 权限范围登陆机构及其下级机构
//            List<String> orgCodes = getLowerOrgId(orgCode);
//            String orgCodeList = "";
//            if (null != orgCodes && orgCodes.size() > 0) {
//                orgCodeList = org.apache.commons.lang.StringUtils.join(orgCodes, ",");
//            }
//            model.addCondition("orgCodeList", orgCodeList);
//        }

        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBase> list = cusBaseMapper.selectByOrgs(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryCusBaseByCusId
     * @方法描述: 根据客户编号查询信息插入被告人信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBase queryCusBaseByCusId(String cusId) {
        return cusBaseMapper.queryCusBaseByCusId(cusId);
    }

    /**
     * @方法名称: selectCusByModel
     * @方法描述: 查询客户对象列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CusBase> selectCusByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBase> list = cusBaseMapper.selectCusByModel(model);
        PageHelper.clearPage();
        return list;
    }


    /**
     * @方法名称: selectBaseCrop
     * @方法描述: 查询
     * @参数与返回说明:
     * @算法描述: 无
     * 创建者：周茂伟
     */
    public List<CusBaseDto> queryCusBaseList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBaseDto> list = new ArrayList<>();
        // 根据角色判断当前登录人是客户经理还是其他
        boolean flag = false;// 客户经理标识
        boolean xwflag = false;// 小微标识
        boolean xwflags = false;// 小微标识
        List<String>  managerIdList = new ArrayList<>();
        List<String>  managerIdLists = new ArrayList<>();
        String userNo = SessionUtils.getLoginCode();
        Map<String, List<String>> managerIdMap;
        User userinfo = SessionUtils.getUserInformation();
        String orgCode = userinfo.getOrg().getCode();
        // 根据角色控制数据权限
        if (null != model.getCondition().get("roleList")) {
            String roleList = model.getCondition().get("roleList").toString();
            if (roleList.contains("code=R0010") || roleList.contains("code=R0020")||roleList.contains("code=R0030")||roleList.contains("code=R0050")||roleList.contains("code=RSG01")||roleList.contains("code=RDH01")) {
                // 小微客户经理 R0010 ; 零售客户经理 R0020 小企业客户经理R0050， 综合客户经理R0030  客户经理寿光RSG01 客户经理东海RDH01
                flag = true;
            }else if(roleList.contains("code=R1001") || roleList.contains("code=R1059")){
                // 小微分中心负责人，小微分部部长
                xwflag = true;
            }
        }
        if(xwflag){
            xwflags = true;
            // 查询小微分部或小微分中心负责人下属客户经理
            managerIdMap = cmisBizClientService.queryManagerByXwRole(userNo);
            if(CollectionUtils.nonEmpty(managerIdMap)){
                // 小微分部或者小微分中心客户集合
                managerIdList= managerIdMap.get(userNo);
            }
            //查询全部小微客户经理集合
        }
        //小微用户特殊处理(除小微客户经理外其他小微角色查询客户)
        if("016000".equals(orgCode)){
            xwflags = true;
            if(!xwflag){
                // 查询小微分部或小微分中心负责人下属客户经理
                managerIdMap = cmisBizClientService.loadXwAreaManagerDataAuthWithUser();
                if(CollectionUtils.nonEmpty(managerIdMap)){
                    // 小微分部或者小微分中心客户集合
                    managerIdMap.forEach((a, b) -> {
                        b.parallelStream().forEach(s1->{
                            managerIdLists.add(s1);
                        });
                    });
                }
            }
        }
        // 获取机构信息
        AdminSmOrgDto AdminSmOrgDto= getAdminSmOrgDto(orgCode);
        String  orgType= "";
        if(AdminSmOrgDto != null){
            orgType = AdminSmOrgDto.getOrgType();
        }
        // 总行部室特殊处理 (排除小微金融事业部)
        if(StringUtils.isNotBlank(orgType) && "0".equals(orgType) && !xwflags){
            model.getCondition().put("managerId","");// 总行部室查全部
        }else if(flag){// 如果是客户经理则查询客户经理管户客户
           String managerId = SessionUtils.getLoginCode();
           model.getCondition().put("managerId",managerId);
           model.getCondition().put("source","2");
        }else if(xwflags){
            if(CollectionUtils.nonEmpty(managerIdList)){
                model.getCondition().put("managerIdList",managerIdList);
            }else{
                model.getCondition().put("managerIdList",managerIdLists);
            }
        }else{// 查询责任机构及机构向下数据
            // 权限范围登陆机构及其下级机构
            List<String> orgCodes = getLowerOrgId(orgCode);
            String orgCodeList = "";
            if (null != orgCodes && orgCodes.size() > 0) {
                orgCodeList = StringUtils.join(orgCodes, ",");
            }
            model.getCondition().put("flag", "other");
            model.getCondition().put("orgCodeList", orgCodeList);
        }
        String cusCatalog ="";
        if(null != model.getCondition().get("cusCatalog")){
            cusCatalog=model.getCondition().get("cusCatalog").toString();
        }
        if("1".equals(cusCatalog)){
           list = cusBaseMapper.selectBaseIndiv(model);
        }else if("2".equals(cusCatalog)){
           list = cusBaseMapper.selectBaseCrop(model);
        }else if("3".equals(cusCatalog)){
           list= selectBaseIntbank(model);
        }
        PageHelper.clearPage();
        return list;
    }
    /**
     * @方法名称: selectBaseIntbank
     * @方法描述: 查询同业客户
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBaseDto> selectBaseIntbank(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBaseDto> list = cusBaseMapper.selectBaseIntbank(model);
        PageHelper.clearPage();
        return list;
    }

    /*
     *根据机构编号获取下级机构
     */
    public List<String> getLowerOrgId(String orgCode) {
        logger.info("根据机构编号获取下级机构号开始", "请求参数:" + orgCode);
        List<String> orgCodes = new ArrayList<>();
        ResultDto<List<String>> resultDto = null;
        try {
            resultDto = adminSmOrgService.getLowerOrgId(orgCode);
            if (ResultDto.success().getCode().equals(resultDto.getCode())) {
                orgCodes = resultDto.getData();
            }
        } catch (Exception e) {
            logger.info("根据机构编号获取下级机构号异常", "异常信息为:" + e.getMessage());
        }
        logger.info("根据机构编号获取下级机构号结束", "响应参数:" + JSON.toJSONString(resultDto));
        return orgCodes;
    }

    /**
     * 获取机构信息
     * @param orgCode
     * @return
     */
    public AdminSmOrgDto getAdminSmOrgDto(String orgCode){
        ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(orgCode);
        AdminSmOrgDto adminSmOrgDto= new AdminSmOrgDto();
        if (null != resultDto && null != resultDto.getData()) {
            adminSmOrgDto= resultDto.getData();
        }
        return adminSmOrgDto;
    }

    /**
     * 根据客户号获取年份集合
     * @author jijian_yx
     * @date 2021/9/27 15:37
     **/
    public List<String> getAccTimeList(String cusId) {
        return cusBaseMapper.getAccTimeList(cusId);
    }

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     * @author jijian_yx
     * @date 2021/9/28 0:06
     **/
    public List<DocAccSearchDto> queryAccListByDocTypeAndYear(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<DocAccSearchDto> list= cusBaseMapper.queryAccListByDocTypeAndYear(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryCusBaseByCusId
     * @方法描述: 根据客户编号查询信息插入被告人信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBaseDto queryCusBaseCropByCusId(String cusId) {
        return cusBaseMapper.queryCusBaseCropByCusId(cusId);
    }
    /**
     * @方法名称: queryCusBaseByCusId
     * @方法描述: 根据客户编号查询信息插入被告人信息表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBaseDto queryCusBaseIndivByCusId(String cusId) {
        return cusBaseMapper.queryCusBaseIndivByCusId(cusId);
    }
}
