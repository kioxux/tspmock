package cn.com.yusys.yusp.enums;

public interface BeanMapInterface {

    String getSourceFieldByTarget(String datatargetField);

    String getTargetFieldBySource(String datasourceField);

}
