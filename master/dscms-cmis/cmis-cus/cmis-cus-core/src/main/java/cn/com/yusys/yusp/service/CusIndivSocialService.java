/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.domain.CusIndivSocial;
import cn.com.yusys.yusp.domain.CusManaTask;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.CusIndivSocialDto;
import cn.com.yusys.yusp.dto.CusIndivSocialResp;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusIndivSocialMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivSocialService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-10 14:15:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusIndivSocialService {

    @Autowired
    private CusIndivSocialMapper cusIndivSocialMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateService;
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;
    @Autowired
    public CusBaseService cusBaseService;
    @Autowired
    private Dscms2OuterdataClientService dscms2ImageClientService;
    @Autowired
    private CusIndivService cusIndivService;

    /**
     *
     * @param cusId
     * @return
     */
    public  List<CusIndivSocialResp> selectCusIndivSocialByCusId(String cusId){
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusIdRel",cusId);
        List<CusIndivSocial> cusIndivSocialList = cusIndivSocialMapper.selectByModel(queryModel);
        List<CusIndivSocialResp> cusIndivSocialDtoList = cusIndivSocialList.stream().map(e->{
            CusIndivSocialResp CusIndivSocialResp = new CusIndivSocialResp();
            BeanUtils.copyProperties(e, CusIndivSocialResp);
            return CusIndivSocialResp;
        }).collect(Collectors.toList());
        return cusIndivSocialDtoList;
    }
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusIndivSocial selectByPrimaryKey(String pkId) {
        return cusIndivSocialMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusIndivSocial> selectAll(QueryModel model) {
        List<CusIndivSocial> records = (List<CusIndivSocial>) cusIndivSocialMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusIndivSocial> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIndivSocial> list = cusIndivSocialMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusIndivSocial record) {
        return cusIndivSocialMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusIndivSocial record) {
        return cusIndivSocialMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public String update(CusIndivSocial record) throws Exception {
        String cusIdRel = "";
        if(null != record){
            cusIdRel = record.getCusIdRel();
        }else{
            return EcsEnum.E_PASS_PARAM_FAIL.value;
        }
        int count = 0;
        int num = 0;
        CusIndivSocial cusIndivSocial = cusIndivSocialMapper.selectByPrimaryKey(record.getPkId());
        if(cusIndivSocial.getIndivCusRel().equals("102000")){
            num = cusIndivSocialMapper.updateByPrimaryKey(record);
        } else {
            if(StringUtils.nonEmpty(cusIdRel) && record.getIndivCusRel().equals("102000")){
                // 判断客户是否已经有配偶信息
                count = cusIndivSocialMapper.selectCount(cusIdRel);
            }
            if(count>0){
                // 存在配偶直接返回有配偶信息
                return EcsEnum.E_INDIV_CUS_REL.value;
            } else {
                num = cusIndivSocialMapper.updateByPrimaryKey(record);
            }
        }
        if(num >0){
            return EcsEnum.E_SAVE_SUCCESS.value;
        }
        else{
            return EcsEnum.E_SAVE_FAIL.value;
        }
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusIndivSocial record) {
        return cusIndivSocialMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusIndivSocialMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusIndivSocialMapper.deleteByIds(ids);
    }

    /**
     * 社会信息保存
     * @param cusIndivSocial
     * @return
     * @author 周茂伟
     */
     public String save(CusIndivSocial cusIndivSocial){
         String cusIdRel = "";
         if(null != cusIndivSocial){
             cusIdRel = cusIndivSocial.getCusIdRel();
         }else{
             return EcsEnum.E_PASS_PARAM_FAIL.value;
         }
         // 申请人
         String inputId = "";
         // 申请机构
         String inputBrId = "";
         // 申请时间
         String inputDate = "";
         // 创建时间
         Date createTime = DateUtils.getCurrTimestamp();
         // 获取用户信息
         User userInfo = SessionUtils.getUserInformation();
         if(userInfo != null){
             // 申请人
             inputId = userInfo.getLoginCode();
             // 申请机构
             inputBrId = userInfo.getOrg().getCode();
             // 申请时间
             inputDate = DateUtils.getCurrDateStr();
         }
         int count = 0;
         int num = 0;
         if(StringUtils.nonEmpty(cusIdRel) && cusIndivSocial.getIndivCusRel().equals("102000")){
             // 判断客户是否已经有配偶信息
             count = cusIndivSocialMapper.selectCount(cusIdRel);
         }
         if(count>0){
             // 存在配偶直接返回有配偶信息
             return EcsEnum.E_INDIV_CUS_REL.value;
         }else{
             cusIndivSocial.setPkId(StringUtils.getUUID());
             cusIndivSocial.setInputId(inputId);
             cusIndivSocial.setInputBrId(inputBrId);
             cusIndivSocial.setInputDate(inputDate);
             cusIndivSocial.setCreateTime(createTime);
             cusIndivSocial.setUpdId(inputId);
             cusIndivSocial.setUpdBrId(inputBrId);
             cusIndivSocial.setUpdDate(inputDate);
             cusIndivSocial.setUpdateTime(createTime);
             num = insert(cusIndivSocial);
             if(num >0){
                 return EcsEnum.E_SAVE_SUCCESS.value;
             }
             else{
                 return EcsEnum.E_SAVE_FAIL.value;
             }
         }
     }

    /**
     * @方法名称: getCusMsg
     * @方法描述: 发送ecif维护
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional
    public CusIndivSocialDto getCusMsg(CusIndivSocialDto cusIndivSocialDto) {
        return cusIndivSocialMapper.getCusMsg(cusIndivSocialDto);
    }

    /**
     * 根据实际控制人列表查询其配偶、父母、子女
     * @param cusIdRelList
     * @return
     */
    public List<Map<String,String>> selectSocialInfoByCusIdRelList(List<String> cusIdRelList){
        return cusIndivSocialMapper.selectSocialInfoByCusIdRelList(cusIdRelList);
    }


    /**
     * 根据证件编号查询配偶客户编号
     * @param certCode
     * @return
     */
    public CmisCus0013RespDto selectSocialByCertCode(String certCode) {
        return cusIndivSocialMapper.selectSocialByCertCode(certCode);
    }

    /**
     * 个人客户创建向导
     * @param cusIndivSocialDto
     * @author xuxin
     */
    @Transactional(rollbackFor = Exception.class)
    public CusIndivSocialDto createCus(CusIndivSocialDto cusIndivSocialDto) {
        if (cusIndivSocialDto != null) {
            String cusId="";
            // 如果ECIF系统返回ECIF客户编号及客户信息，则直接添加这个客户信息到任务表并跳转到信息录入页签
            User userInfo = SessionUtils.getUserInformation();
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            // 第一部 查询客户基本信息表，通过证件类型证件号码判断客户是否在信贷系统存在，不存在则继续后面步骤
            CusIndivDto cusIndivDto = new CusIndivDto();
            BeanUtils.copyProperties(cusIndivSocialDto,cusIndivDto);
            CusBase cusBase = cusBaseService.selectByCertCode(cusIndivDto);
            if (cusBase != null) {
                // 客户信息已经存在
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,EcsEnum.E_CUS_INFO_EXIST.value);
            }
            //证件类型为身份证的时候进行身份校验
            String certType = cusIndivDto.getCertType();
            // A 身份证
            if("A".equals(certType)){
                // 第二步 校验客户信息  发送身份校验
                IdCheckReqDto idCheckReqDto = new IdCheckReqDto();
                // 证件类型 0101身份证
                idCheckReqDto.setCert_type("0101");//(cusIndivDto.getCertType()
                // 用户名称
                idCheckReqDto.setUser_name(cusIndivDto.getCusName());
                // 证件号码
                idCheckReqDto.setId_number(cusIndivDto.getCertCode());
                // 机构类型 2：银行营业厅
                idCheckReqDto.setSource_type("2");
                // 网上营业厅申请人IP地址  StringUtils.EMPTY 待确认
                idCheckReqDto.setIp("10.87.3.37");//StringUtils.EMPTY
                ResultDto<IdCheckRespDto> idCheckResultDto = dscms2ImageClientService.idCheck(idCheckReqDto);
                String idCheckCode = Optional.ofNullable(idCheckResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String  idCheckMeesage = Optional.ofNullable(idCheckResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                IdCheckRespDto idCheckRespDto = null;
                if (Objects.equals(idCheckCode, SuccessEnum.CMIS_SUCCSESS.key)) {
                    //  获取相关的值并解析
                    idCheckRespDto = idCheckResultDto.getData();
                    if(!EcsEnum.CUS_INDIV_CHECK_RESULT00.key.equals(idCheckRespDto.getCheckresult())){
                        throw BizException.error(null,idCheckCode,idCheckMeesage);
                    }
                } else {
                    //抛出错误异常
                    throw BizException.error(null,idCheckCode,EcsEnum.CUS_INDIV_CHECK_RESULT01.value + idCheckMeesage);
                }
            }
            S10501ReqDto s10501ReqDto = new S10501ReqDto();
            s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
            s10501ReqDto.setIdtftp(cusIndivDto.getCertType());//   证件类型
            s10501ReqDto.setIdtfno(cusIndivDto.getCertCode());//   证件号码
            //  StringUtils.EMPTY的实际值待确认 开始
            // 发送客户三要素发送ecfi查询
            ResultDto<S10501RespDto> s10501ResultDto = dscms2EcifClientService.s10501(s10501ReqDto);
            String s10501Code = Optional.ofNullable(s10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String s10501Meesage = Optional.ofNullable(s10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            S10501RespDto s10501RespDto = null;
            if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                //  获取查询结果 todo联调验证交易成功查询失败情况
                s10501RespDto = s10501ResultDto.getData();
                if (s10501RespDto.getListnm() != null) {
                    //cusId=StringUtils.getUUID();
                    List<ListArrayInfo> listArrayInfo = s10501RespDto.getListArrayInfo();
                    for (int i = 0; i < listArrayInfo.size(); i++) {
                        // 客户号
                        cusId = listArrayInfo.get(0).getCustno();
                        cusIndivDto.setCusId(cusId);
                    }

                    cusBase = new CusBase();
                    // 保存客户信息到cus_base客户基本信息表
                    BeanUtils.copyProperties(cusIndivDto, cusBase);
                    cusBase.setCusRankCls("02");
                    // 客户状态 1暂存
                    cusBase.setCusState(CmisCusConstants.STD_CUS_STATE_1);
                    //客户大类 2 个人
                    cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PRI);
                    // 申请人
                    cusBase.setInputId(inputId);
                    //管户人
                    cusBase.setManagerId(inputId);
                    // 申请机构
                    cusBase.setInputBrId(inputBrId);
                    // 管户机构
                    cusBase.setManagerBrId(inputBrId);
                    // 申请时间
                    cusBase.setInputDate(inputDate);
                    // 创建时间
                    cusBase.setCreateTime(createTime);
                    // 修改人
                    cusBase.setUpdId(inputId);
                    // 修改机构
                    cusBase.setUpdBrId(inputBrId);
                    // 修改日期
                    cusBase.setUpdDate(inputDate);
                    // 修改时间
                    cusBase.setUpdateTime(createTime);
                    cusBaseService.insertSelective(cusBase);

                    CusIndiv cusIndiv = new CusIndiv();
                    BeanUtils.copyProperties(cusBase, cusIndiv);
                    cusIndivService.insertSelective(cusIndiv);
                    BeanUtils.copyProperties(cusBase, cusIndivSocialDto);
                } else {
                    // 交易失败
                    // 判断如果是临时客户 可直接开户 01正式 02 临时
                    // 如果是正式客户 直接返回，并提示
                    if ("01".equals(cusIndivDto.getCusRankCls())) {
                        throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,EcsEnum.E_CUS_INFO_EXIST.value);
                    }
                    // 若ecif系统检验不通过，则发送开户接口进行开户
                    S00102ReqDto s00102ReqDto = new S00102ReqDto();
                    // 客户名称
                    s00102ReqDto.setCustna(cusIndivDto.getCusName());
                    // 证件号码
                    s00102ReqDto.setIdtfno(cusIndivDto.getCertCode());//cusIndivDto.getCertType()
                    // 证件类型
                    s00102ReqDto.setIdtftp(cusIndivDto.getCertType());
                    // 客户状态
                    s00102ReqDto.setCustst("3");

                    s00102ReqDto.setCtcktg("1");

                    s00102ReqDto.setResult("00");
                    // 发送Ecif开户
                    ResultDto<S00102RespDto> s00102ResultDto = dscms2EcifClientService.s00102(s00102ReqDto);
                    String g00102Code = Optional.ofNullable(s00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                    String g00102Meesage = Optional.ofNullable(s00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                    S00102RespDto s00102RespDto = null;
                    if (Objects.equals(g00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                        //  获取相关的值并解析
                        s00102RespDto = s00102ResultDto.getData();
                        if (s00102RespDto != null) {
                            cusId = s00102RespDto.getCustno();
                            cusIndivDto.setCusId(cusId);
                            cusBase = new CusBase();
                            // 保存客户信息到cus_base客户基本信息表
                            BeanUtils.copyProperties(cusIndivDto, cusBase);
                            // 客户状态 1暂存
                            cusBase.setCusState(CmisCusConstants.STD_CUS_STATE_1);
                            //客户大类 2 个人
                            cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PRI);
                            // 申请人
                            cusBase.setInputId(inputId);
                            //管户人
                            cusBase.setManagerId(inputId);
                            // 申请机构
                            cusBase.setInputBrId(inputBrId);
                            // 管户机构
                            cusBase.setManagerBrId(inputBrId);
                            // 申请时间
                            cusBase.setInputDate(inputDate);
                            // 创建时间
                            cusBase.setCreateTime(createTime);
                            // 修改人
                            cusBase.setUpdId(inputId);
                            // 修改机构
                            cusBase.setUpdBrId(inputBrId);
                            // 修改日期
                            cusBase.setUpdDate(inputDate);
                            // 修改时间
                            cusBase.setUpdateTime(createTime);
                            cusBaseService.insertSelective(cusBase);

                            CusIndiv cusIndiv = new CusIndiv();
                            BeanUtils.copyProperties(cusBase, cusIndiv);
                            cusIndivService.insertSelective(cusIndiv);
                            BeanUtils.copyProperties(cusBase, cusIndivSocialDto);
                        }
                    } else {
                        //  抛出错误异常
                        throw BizException.error(null,g00102Code,EcsEnum.CUS_INDIV_CREATE_RESULT.value + g00102Meesage);
                    }
                }
            } else {
                throw BizException.error(null,s10501Code,EcsEnum.CUS_INDIV_QUERY_RESULT.value+s10501Meesage);
            }
        }
        return cusIndivSocialDto;
    }

    /**
     * @方法名称: queryCusSocial
     * @方法描述: 查询社会关系人信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusIndivSocialResp> queryCusSocial(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIndivSocialResp> list = cusIndivSocialMapper.queryCusSocial(model);
        PageHelper.clearPage();
        return list;
    }
}
