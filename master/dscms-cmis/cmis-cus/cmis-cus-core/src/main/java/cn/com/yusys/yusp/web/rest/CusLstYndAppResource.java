/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CusLstYndAppDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYndApp;
import cn.com.yusys.yusp.service.CusLstYndAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:20:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstyndapp")
public class CusLstYndAppResource {
    @Autowired
    private CusLstYndAppService cusLstYndAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstYndApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstYndApp> list = cusLstYndAppService.selectAll(queryModel);
        return new ResultDto<List<CusLstYndApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstYndApp>> index(QueryModel queryModel) {
        List<CusLstYndApp> list = cusLstYndAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYndApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstYndApp>> query(@RequestBody QueryModel queryModel) {
        List<CusLstYndApp> list = cusLstYndAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYndApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstYndApp> show(@PathVariable("serno") String serno) {
        CusLstYndApp cusLstYndApp = cusLstYndAppService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstYndApp>(cusLstYndApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstYndApp> create(@RequestBody CusLstYndApp cusLstYndApp) throws URISyntaxException {
        cusLstYndAppService.insert(cusLstYndApp);
        return new ResultDto<CusLstYndApp>(cusLstYndApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstYndApp cusLstYndApp) throws URISyntaxException {
        int result = cusLstYndAppService.update(cusLstYndApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstYndAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstYndAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("暂存:单个对象保存(新增或修改)")
    @PostMapping("/tempSave")
    protected ResultDto<Integer> tempSave(@RequestBody CusLstYndApp cusLstYndApp) {
        int result = cusLstYndAppService.tempSave(cusLstYndApp);
        return new ResultDto<Integer>(result);
    }

    /**
     *根据证件号查询客户信息,如果是本行客户则返回,否则去ecif查询,ecif没有去ecif开户,后数据本地落库
     * @param cusLstYndApp
     * @return
     */
    @PostMapping("/queryCusByCertCodeToEcif")
    public ResultDto<CusLstYndApp> queryCusByCertCodeToEcif(@RequestBody CusLstYndApp cusLstYndApp) {
        CusLstYndApp result = cusLstYndAppService.queryCusByCertCodeToEcif(cusLstYndApp);
        return new ResultDto<CusLstYndApp>(result);
    }

    /**
     * @param cusLstYndApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndApp>
     * @author hubp
     * @date 2021/7/16 10:26
     * @version 1.0.0
     * @desc   根据流水号查询优农贷基本信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<CusLstYndApp> selectBySerno(@RequestBody CusLstYndApp cusLstYndApp) {
        CusLstYndApp result = cusLstYndAppService.selectByPrimaryKey(cusLstYndApp.getSerno());;
        return new ResultDto<CusLstYndApp>(result);
    }

    /**
     * @param cusLstYndApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndApp>
     * @author hubp
     * @date 2021/7/16 13:59
     * @version 1.0.0
     * @desc    通过调查流水号--级联删除优农贷信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deletebyserno")
    protected ResultDto<Integer> deleteBySerno(@RequestBody CusLstYndApp cusLstYndApp) {
        return cusLstYndAppService.deleteBySerno(cusLstYndApp.getSerno());
    }

    /**
     * @param cusLstYndApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/7/21 14:11
     * @version 1.0.0
     * @desc 
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addcuslstyndapp")
    protected ResultDto addCusLstYndApp(@RequestBody CusLstYndApp cusLstYndApp) {
        return cusLstYndAppService.addCusLstYndApp(cusLstYndApp);
    }

    /**
     * @param cusLstYndAppDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/9/15 19:46
     * @version 1.0.0
     * @desc  BIZ调用，插入待发起的申请
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/insertyndapp")
    protected ResultDto insertYndApp(@RequestBody CusLstYndAppDto cusLstYndAppDto) {
        return cusLstYndAppService.insertYndApp(cusLstYndAppDto);
    }
}
