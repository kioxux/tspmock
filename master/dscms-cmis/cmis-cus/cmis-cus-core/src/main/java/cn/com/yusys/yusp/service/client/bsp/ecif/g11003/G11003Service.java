package cn.com.yusys.yusp.service.client.bsp.ecif.g11003;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2EcifClientService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 业务逻辑处理类：客户集团信息维护 (new)接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class G11003Service {
    private static final Logger logger = LoggerFactory.getLogger(G11003Service.class);

    // 1）注入：BSP封装调用ECIF系统的接口
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    /**
     * 业务逻辑处理方法：客户集团信息维护 (new)接口
     *
     * @param g11003ReqDto
     * @return
     */
    @Transactional
    public G11003RespDto g11003(G11003ReqDto g11003ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value);
        validateAndSetValue(g11003ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value, JSON.toJSONString(g11003ReqDto));
        ResultDto<G11003RespDto> g11003ResultDto = dscms2EcifClientService.g11003(g11003ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value, JSON.toJSONString(g11003ResultDto));
        String g11003Code = Optional.ofNullable(g11003ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g11003Meesage = Optional.ofNullable(g11003ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G11003RespDto g11003RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, g11003ResultDto.getCode())) {
            //  获取相关的值并解析
            g11003RespDto = g11003ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(g11003Code, g11003Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value);
        return g11003RespDto;
    }

    /**
     * 校验g11003ReqDto对象并赋值
     *
     * @param g11003ReqDto
     */
    private void validateAndSetValue(G11003ReqDto g11003ReqDto) {
        logger.info(TradeLogConstants.SERVICE_VALIDATE_SET_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value, JSON.toJSONString(g11003ReqDto));
        g11003ReqDto.setGrouna(Optional.ofNullable(g11003ReqDto.getGrouna()).orElse(StringUtils.EMPTY));//  集团客户名称
        g11003ReqDto.setCustno(Optional.ofNullable(g11003ReqDto.getCustno()).orElse(StringUtils.EMPTY));//  客户编号
        g11003ReqDto.setCustln(Optional.ofNullable(g11003ReqDto.getCustln()).orElse(StringUtils.EMPTY));//  客户中证码
        g11003ReqDto.setCustna(Optional.ofNullable(g11003ReqDto.getCustna()).orElse(StringUtils.EMPTY));//  客户名称
        g11003ReqDto.setIdtfno(Optional.ofNullable(g11003ReqDto.getIdtfno()).orElse(StringUtils.EMPTY));//  证件号码
        g11003ReqDto.setOadate(Optional.ofNullable(g11003ReqDto.getOadate()).orElse(StringUtils.EMPTY));//  更新办公地址日期
        g11003ReqDto.setHomead(Optional.ofNullable(g11003ReqDto.getHomead()).orElse(StringUtils.EMPTY));//  地址
        g11003ReqDto.setSocino(Optional.ofNullable(g11003ReqDto.getSocino()).orElse(StringUtils.EMPTY));//  社会信用代码
        g11003ReqDto.setBusino(Optional.ofNullable(g11003ReqDto.getBusino()).orElse(StringUtils.EMPTY));//  工商登记注册号
        g11003ReqDto.setOfaddr(Optional.ofNullable(g11003ReqDto.getOfaddr()).orElse(StringUtils.EMPTY));//  办公地址行政区划
        g11003ReqDto.setCrpdeg(Optional.ofNullable(g11003ReqDto.getCrpdeg()).orElse(StringUtils.EMPTY));//  集团紧密程度
        g11003ReqDto.setCrpcno(Optional.ofNullable(g11003ReqDto.getCrpcno()).orElse(StringUtils.EMPTY));//  集团客户情况说明
        g11003ReqDto.setCtigno(Optional.ofNullable(g11003ReqDto.getCtigno()).orElse(StringUtils.EMPTY));//  管户客户经理
        g11003ReqDto.setCitorg(Optional.ofNullable(g11003ReqDto.getCitorg()).orElse(StringUtils.EMPTY));//  所属机构
        g11003ReqDto.setGrpsta(Optional.ofNullable(g11003ReqDto.getGrpsta()).orElse(StringUtils.EMPTY));//  集团客户状态
        g11003ReqDto.setOprtye(Optional.ofNullable(g11003ReqDto.getOprtye()).orElse(StringUtils.EMPTY));//  操作类型
        g11003ReqDto.setInptno(Optional.ofNullable(g11003ReqDto.getInptno()).orElse(StringUtils.EMPTY));//  登记人
        g11003ReqDto.setInporg(Optional.ofNullable(g11003ReqDto.getInporg()).orElse(StringUtils.EMPTY));//  登记机构

        cn.com.yusys.yusp.dto.client.esb.ecif.g11003.CircleArrayMem temp = new cn.com.yusys.yusp.dto.client.esb.ecif.g11003.CircleArrayMem();// 集团成员信息_ARRAY
        temp.setCustno(Optional.ofNullable(temp.getCustno()).orElse(StringUtils.EMPTY));//集团成员客户号
        g11003ReqDto.setCircleArrayMem(Optional.ofNullable(g11003ReqDto.getCircleArrayMem()).orElse(Arrays.asList(temp)));
        g11003ReqDto.setCircleArrayMem(
                g11003ReqDto.getCircleArrayMem().parallelStream().map(circleArrayMem -> {
                    circleArrayMem.setCustno(Optional.ofNullable(circleArrayMem.getCustno()).orElse(StringUtils.EMPTY));//集团成员客户号
                    circleArrayMem.setCortype(Optional.ofNullable(circleArrayMem.getCortype()).orElse(StringUtils.EMPTY));//集团关系类型
                    circleArrayMem.setGrpdesc(Optional.ofNullable(circleArrayMem.getGrpdesc()).orElse(StringUtils.EMPTY));//集团关系描述
                    circleArrayMem.setIscust(Optional.ofNullable(circleArrayMem.getIscust()).orElse(StringUtils.EMPTY));//是否集团主客户
                    circleArrayMem.setMainno(Optional.ofNullable(circleArrayMem.getMainno()).orElse(StringUtils.EMPTY));//主管人
                    circleArrayMem.setMaiorg(Optional.ofNullable(circleArrayMem.getMaiorg()).orElse(StringUtils.EMPTY));//主管机构
                    circleArrayMem.setSocino(Optional.ofNullable(circleArrayMem.getSocino()).orElse(StringUtils.EMPTY));//统一社会信用代码
                    circleArrayMem.setCustst(Optional.ofNullable(circleArrayMem.getCustst()).orElse(StringUtils.EMPTY));//成员状态
                    return circleArrayMem;
                }).collect(Collectors.toList())
        );
        logger.info(TradeLogConstants.SERVICE_VALIDATE_SET_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value, JSON.toJSONString(g11003ReqDto));
    }
}
