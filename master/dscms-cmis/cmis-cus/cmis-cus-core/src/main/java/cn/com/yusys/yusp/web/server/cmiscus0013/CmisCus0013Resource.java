package cn.com.yusys.yusp.web.server.cmiscus0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusIndivSocial;
import cn.com.yusys.yusp.dto.CusIndivSocialDto;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusIndivSocialService;
import cn.com.yusys.yusp.service.CusLstYndAppService;
import cn.com.yusys.yusp.web.server.cmiscus0007.CmisCus0007Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 根据证件编号查询配偶客户编号
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "CmisCus0013:根据证件编号查询个人社会关系")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0013Resource.class);

    @Autowired
    private CusIndivSocialService cusIndivSocialService;
    /**
     * 交易码：cmiscus0013
     * 交易描述：根据证件编号查询配偶客户编号
     *
     * @param certCode
     * @return
     * @throws Exception
     */
    @ApiOperation("根据证件编号查询配偶客户编号")
    @PostMapping("/cmiscus0013")
    protected ResultDto<CmisCus0013RespDto> cmiscus0013(@RequestBody String certCode) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0013.key, DscmsEnum.TRADE_CODE_CMISCUS0013.value, "请求参数"+certCode);
        CmisCus0013RespDto cmisCus0013RespDto = new CmisCus0013RespDto();// 响应Dto:查询优农贷名单信息
        ResultDto<CmisCus0013RespDto> cmisCus0013RespDtoResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0013.key, DscmsEnum.TRADE_CODE_CMISCUS0013.value, "请求参数"+certCode);
            cmisCus0013RespDto = cusIndivSocialService.selectSocialByCertCode(certCode);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0013.key, DscmsEnum.TRADE_CODE_CMISCUS0013.value, JSON.toJSONString(cmisCus0013RespDto));
            // 封装cmisCus0013ResultDto中正确的返回码和返回信息
            cmisCus0013RespDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0013RespDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, e.getMessage());
            // 封装cmisCus0013ResultDto中异常返回码和返回信息
            cmisCus0013RespDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0013RespDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0013RespDto到cmisCus0007ResultDto中
        cmisCus0013RespDtoResultDto.setData(cmisCus0013RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0013.key, DscmsEnum.TRADE_CODE_CMISCUS0013.value, JSON.toJSONString(cmisCus0013RespDtoResultDto));
        return cmisCus0013RespDtoResultDto;
    }
}
