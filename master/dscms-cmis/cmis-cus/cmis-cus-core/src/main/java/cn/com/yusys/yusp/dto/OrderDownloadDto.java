package cn.com.yusys.yusp.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

public class OrderDownloadDto  implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	private String pkId;

	/** 数据源(比如是biz，就在biz自动起个任务，然后接口发的一个记录给cfg) **/
	private String dataSource;

	/** 预约时间cron **/
	private String cronTime;

	/** 预约次数 下载要执行几次 **/
	private Integer orderNum;

	/** 下载报表的查询类 编号 或者 其他具体待定 **/
	private String downloadAction;

	/** 过滤条件 灵活查询的时候可以用用 **/
	private String queryCons;

	/** 文件名 **/
	private String fileName;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最后修改人 **/
	private String updId;

	/** 最后修改机构 **/
	private String updBrId;

	/** 最后修改日期 **/
	private String updDate;

	/** 状态 1运行中 2未运行 **/
	private String status;

	/** 导出类型 csv 还是  xlsx **/
	private String exportType;

	/** 关联水印配置id **/
	private String watermarkId;

	/** 关联导出权限id 控制哪几个字段可修改啥的 **/
	private String rowCfgId;

	/** 查询数据的形式 1 来源于灵活查询 2固定的提前写好的方法 **/
	private String orderType;

	/** 水印字体大小 **/
	private Short waterFontSize;

	/** 水印内容 1 用户 2 机构 3时间 4自定义文字 **/
	private String waterContent;

	/** 水印文字 **/
	private String waterWords;

	/** 已下载次数 **/
	private Integer downloadCount;
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param dataSource
	 */
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

    /**
     * @return dataSource
     */
	public String getDataSource() {
		return this.dataSource;
	}

	/**
	 * @param cronTime
	 */
	public void setCronTime(String cronTime) {
		this.cronTime = cronTime;
	}

    /**
     * @return cronTime
     */
	public String getCronTime() {
		return this.cronTime;
	}

	/**
	 * @param downloadAction
	 */
	public void setDownloadAction(String downloadAction) {
		this.downloadAction = downloadAction;
	}

    /**
     * @return downloadAction
     */
	public String getDownloadAction() {
		return this.downloadAction;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}

	public String getQueryCons() {
		return queryCons;
	}

	public void setQueryCons(String queryCons) {
		this.queryCons = queryCons;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public String getExportType() {
		return exportType;
	}

	public void setExportType(String exportType) {
		this.exportType = exportType;
	}

	public String getWatermarkId() {
		return watermarkId;
	}

	public void setWatermarkId(String watermarkId) {
		this.watermarkId = watermarkId;
	}

	public String getRowCfgId() {
		return rowCfgId;
	}

	public void setRowCfgId(String rowCfgId) {
		this.rowCfgId = rowCfgId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Short getWaterFontSize() {
		return waterFontSize;
	}

	public void setWaterFontSize(Short waterFontSize) {
		this.waterFontSize = waterFontSize;
	}

	public String getWaterContent() {
		return waterContent;
	}

	public void setWaterContent(String waterContent) {
		this.waterContent = waterContent;
	}

	public String getWaterWords() {
		return waterWords;
	}

	public void setWaterWords(String waterWords) {
		this.waterWords = waterWords;
	}

	public Integer getDownloadCount() {
		return downloadCount;
	}

	public void setDownloadCount(Integer downloadCount) {
		this.downloadCount = downloadCount;
	}



}
