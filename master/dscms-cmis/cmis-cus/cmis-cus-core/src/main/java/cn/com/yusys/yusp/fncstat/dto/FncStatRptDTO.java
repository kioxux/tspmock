package cn.com.yusys.yusp.fncstat.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
//import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

@ApiModel("公司客户报表")
public class FncStatRptDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3727132202543829183L;
	/**
	 * 客户代码
	 */
	@ApiModelProperty("客户代码")
	private String cusId;
	/**
	 * 报表口径
	 */
	@ApiModelProperty("报表口径")
	private String statStyle;
	/**
	 * 报表周期类型
	 */
	@ApiModelProperty("报表周期类型")
	private String statPrdStyle;
	/**
	 * 报表期间
	 */
	@ApiModelProperty("报表期间")
	private String statPrd;
	/**
	 * 所属报表种类
	 */
	@ApiModelProperty("所属报表种类")
	private String fncConfTyp;
	/**
	 * 报表样式编号
	 */
	@ApiModelProperty("报表样式编号")
	private String statConfStyleId;
	/**
	 * 报表科目集合
	 */
	@ApiModelProperty("报表科目集合")
	private List<FncStatRptItemDTO> items;

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getStatStyle() {
		return statStyle;
	}

	public void setStatStyle(String statStyle) {
		this.statStyle = statStyle;
	}

	public String getStatPrdStyle() {
		return statPrdStyle;
	}

	public void setStatPrdStyle(String statPrdStyle) {
		this.statPrdStyle = statPrdStyle;
	}

	public String getStatPrd() {
		return statPrd;
	}

	public void setStatPrd(String statPrd) {
		this.statPrd = statPrd;
	}

	public String getStatConfStyleId() {
		return statConfStyleId;
	}

	public void setStatConfStyleId(String statConfStyleId) {
		this.statConfStyleId = statConfStyleId;
	}

	public List<FncStatRptItemDTO> getItems() {
		return items;
	}

	public void setItems(List<FncStatRptItemDTO> items) {
		this.items = items;
	}

	public String getFncConfTyp() {
		return fncConfTyp;
	}

	public void setFncConfTyp(String fncConfTyp) {
		this.fncConfTyp = fncConfTyp;
	}

//	@Override
//	public String toString() {
//		return ToStringBuilder.reflectionToString(this);
//	}

	@Override
	public String toString() {
		return "FncStatRptDTO{" +
				"cusId='" + cusId + '\'' +
				", statStyle='" + statStyle + '\'' +
				", statPrdStyle='" + statPrdStyle + '\'' +
				", statPrd='" + statPrd + '\'' +
				", fncConfTyp='" + fncConfTyp + '\'' +
				", statConfStyleId='" + statConfStyleId + '\'' +
				", items=" + items +
				'}';
	}

}
