/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.web.rest;

//import cn.com.yusys.yusp.trace.annotation.TraceBaggage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.fncstat.domain.FncStatBase;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptBriefDTO;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptDTO;
import cn.com.yusys.yusp.fncstat.service.FncStatDtlService;

/**
 * @version 1.0.0
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatDtlResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: zzbankwb473
 * @创建时间: 2019-12-02 09:42:19
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/nrcs-cms/fncstatdtl")
@Api(tags = "FncStatDtlResource", description = "财务报表详细信息")
public class FncStatDtlResource {
    @Autowired
    private FncStatDtlService fncStatDtlService;

    @GetMapping("/q/fncstatdtl/detail")
    @ApiOperation("NRCS601103财务报表信息详情")
//    @TraceBaggage(functionCode = "NRCS601103")
    public ResultDto<Map<String, Object>> queryFncStatDetailByType(@RequestParam String cusId,
                                                                   @RequestParam String statStyle, @RequestParam String statPrdStyle, @RequestParam String statPrd,
                                                                   @RequestParam String fncConfTyp) {
        return new ResultDto<Map<String, Object>>(
                fncStatDtlService.queryFncStatDtlByType(cusId, statStyle, statPrdStyle, statPrd, fncConfTyp));
    }

    @PostMapping("/s/fncstatdtl/tempsave")
    @ApiOperation("NRCS601104财务报表详情暂存")
//    @TraceBaggage(functionCode = "NRCS601104")
    public ResultDto<Integer> tempSaveFncStatDtl(@RequestBody FncStatRptDTO fncStatRptDto) {
        return new ResultDto<Integer>(fncStatDtlService.tempSaveFncStatDtl(fncStatRptDto));
    }

    @GetMapping("/q/fncstatdtl/detailall")
    @ApiOperation("NRCS601105财务报表信息详情")
//    @TraceBaggage(functionCode = "NRCS601105")
    public ResultDto<Map<String, Object>> queryAllFncStatDetail(@RequestParam String cusId,
                                                                @RequestParam String statStyle, @RequestParam String statPrdStyle, @RequestParam String statPrd) {
        return new ResultDto<Map<String, Object>>(
                fncStatDtlService.queryAllFncStatDtl(cusId, statStyle, statPrdStyle, statPrd));
    }

    @PostMapping("/s/fncstatdtl/save")
    @ApiOperation("NRCS601106财务报表详情完成")
//    @TraceBaggage(functionCode = "NRCS601106")
    public ResultDto saveFncStatDtl(@RequestBody FncStatRptDTO fncStatRptDto) {
        return fncStatDtlService.saveFncStatDtl(fncStatRptDto);
    }

    /**
     * 财务报表导出
     *
     * @param cusId
     * @param statStyle
     * @param statPrdStyle
     * @param statPrd
     * @return
     */
    @GetMapping("/q/fncstatdtl/export")
    @ApiOperation("NRCS601107财务报表导出")
//    @TraceBaggage(functionCode = "NRCS601107")
    public ResponseEntity<byte[]> queryAllFncStatExport(@RequestParam String cusId, @RequestParam String statStyle,
                                                        @RequestParam String statPrdStyle, @RequestParam String statPrd) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Disposition", "attachment; filename=" + cusId + ".xls");
        headers.add("Content-Type", MediaType.APPLICATION_OCTET_STREAM_VALUE);
        return new ResponseEntity<>(
                fncStatDtlService.queryAllFncStatExport(cusId, statStyle, statPrdStyle, statPrd), headers,
                HttpStatus.OK);

    }
    
    /**
     * 导入报表
     * @param fileId
     * @return
     */
    @PostMapping("/q/fncstatdtl/import")
    public ResultDto<ProgressDto> importExcel(@RequestParam String fileId, @RequestBody FncStatBase fncStatBase) {
    	ResultDto<ProgressDto> result = fncStatDtlService.importFncStatDtlExcel(fileId, fncStatBase);
        return result;
    }

    @PostMapping("/s/fncstatdtl/tempsavebrief")
    @ApiOperation("NRCS601108财务报表简表暂存")
//    @TraceBaggage(functionCode = "NRCS601108")
    public ResultDto<Integer> tempSaveFncStatDtlBrief(@RequestBody FncStatRptBriefDTO fncStatRptDtos) {
        return new ResultDto<Integer>(fncStatDtlService.tempSaveFncStatDtlBrief(fncStatRptDtos));
    }

    @PostMapping("/s/fncstatdtl/savebrief")
    @ApiOperation("NRCS601109财务报表简表完成")
//    @TraceBaggage(functionCode = "NRCS601109")
    public ResultDto saveFncStatDtlBrief(@RequestBody FncStatRptBriefDTO fncStatRptDtos) {
        return fncStatDtlService.saveFncStatDtlBrief(fncStatRptDtos);
    }

    @GetMapping("/q/fncstatdtl/querycapcalc")
    @ApiOperation("NRCS601110对公客户资金测算")
//    @TraceBaggage(functionCode = "NRCS601110")
    public ResultDto<Map<String, Object>> queryCapitalCalcData(@RequestParam String cusId) {
        return new ResultDto<>(fncStatDtlService.queryCapitalCalcData(cusId));
    }
    
    /**
     * 从OCR获取财报数据
     * @return
     */
    @GetMapping("/q/fncstatdtl/getFncStatDtlFromOcr")
    public ResultDto<Integer> getFncStatDtlFromOcr() {
    	return ResultDto.success();
    }
    
}
