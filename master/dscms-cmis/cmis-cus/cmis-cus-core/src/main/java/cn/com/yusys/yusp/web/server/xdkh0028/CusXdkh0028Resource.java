package cn.com.yusys.yusp.web.server.xdkh0028;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0028.req.Xdkh0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0028.resp.Xdkh0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0028.Xdkh0028Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;

/**
 * 接口处理类:优农贷黑名单查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0028:优农贷黑名单查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0028Resource.class);

    @Resource
    private Xdkh0028Service xdkh0028Service;
    /**
     * 交易码：xdkh0028
     * 交易描述：优农贷黑名单查询
     *
     * @param xdkh0028DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷黑名单查询")
    @PostMapping("/xdkh0028")
    protected @ResponseBody
    ResultDto<Xdkh0028DataRespDto> xdkh0028(@Validated @RequestBody Xdkh0028DataReqDto xdkh0028DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, JSON.toJSONString(xdkh0028DataReqDto));
        Xdkh0028DataRespDto xdkh0028DataRespDto = new Xdkh0028DataRespDto();// 响应Dto:优农贷黑名单查询
        ResultDto<Xdkh0028DataRespDto> xdkh0028DataResultDto = new ResultDto<>();
        try {
            xdkh0028DataRespDto = xdkh0028Service.getIndivCusBaseInfo(xdkh0028DataReqDto);
            // 封装xdkh0028DataResultDto中正确的返回码和返回信息
            xdkh0028DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0028DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, e.getMessage());
            // 封装xdkh0028DataResultDto中异常返回码和返回信息
            xdkh0028DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0028DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0028DataRespDto到xdkh0028DataResultDto中
        xdkh0028DataResultDto.setData(xdkh0028DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, JSON.toJSONString(xdkh0028DataResultDto));
        return xdkh0028DataResultDto;
    }
}