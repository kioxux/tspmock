/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYxdJbxxApp;
import cn.com.yusys.yusp.service.CusLstYxdJbxxAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYxdJbxxAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:15:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstyxdjbxxapp")
public class CusLstYxdJbxxAppResource {
    @Autowired
    private CusLstYxdJbxxAppService cusLstYxdJbxxAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstYxdJbxxApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstYxdJbxxApp> list = cusLstYxdJbxxAppService.selectAll(queryModel);
        return new ResultDto<List<CusLstYxdJbxxApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstYxdJbxxApp>> index(QueryModel queryModel) {
        List<CusLstYxdJbxxApp> list = cusLstYxdJbxxAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYxdJbxxApp>>(list);
    }



    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstYxdJbxxApp>> queryAll(@RequestBody QueryModel queryModel) {
        List<CusLstYxdJbxxApp> list = cusLstYxdJbxxAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYxdJbxxApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstYxdJbxxApp> show(@PathVariable("serno") String serno) {
        CusLstYxdJbxxApp cusLstYxdJbxxApp = cusLstYxdJbxxAppService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstYxdJbxxApp>(cusLstYxdJbxxApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstYxdJbxxApp> create(@RequestBody CusLstYxdJbxxApp cusLstYxdJbxxApp) throws URISyntaxException {
        cusLstYxdJbxxAppService.insert(cusLstYxdJbxxApp);
        return new ResultDto<CusLstYxdJbxxApp>(cusLstYxdJbxxApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstYxdJbxxApp cusLstYxdJbxxApp) throws URISyntaxException {
        int result = cusLstYxdJbxxAppService.update(cusLstYxdJbxxApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstYxdJbxxAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstYxdJbxxAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("暂存:单个对象保存(新增或修改)")
    @PostMapping("/tempSave")
    protected ResultDto<Integer> tempSave(@RequestBody CusLstYxdJbxxApp cusLstYxdJbxxApp) {
        int result = cusLstYxdJbxxAppService.tempSave(cusLstYxdJbxxApp);
        return new ResultDto<Integer>(result);
    }
}
