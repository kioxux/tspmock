package cn.com.yusys.yusp.service.server.xdkh0032;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0032.req.Xdkh0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0032.resp.Xdkh0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0032Service
 * @类描述: #服务类
 * @功能描述:根据区分标识更新对公信息表、同业客户表、授信申请表中锁定状态
 * @创建人: xll
 * @创建时间: 2021-05-11 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0032Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0032Service.class);

    @Resource
    private CusCorpMapper cusCorpMapper;
    @Resource
    private CusIntbankMapper cusIntbankMapper;

    /**
     * 信息锁定标志同步
     *
     * @param xdkh0032DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0032DataRespDto xdkh0032(Xdkh0032DataReqDto xdkh0032DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, JSON.toJSONString(xdkh0032DataReqDto));
        Xdkh0032DataRespDto xdkh0032DataRespDto = new Xdkh0032DataRespDto();
        String cusId = xdkh0032DataReqDto.getCusId();//客户号
        String dtghFlag = xdkh0032DataReqDto.getDtghFlag();//区分标识
        String lockStatus = xdkh0032DataReqDto.getLockStatus();//锁定状态

        try {
            //返回结果
            int result = 0;
            //查询条件
            Map map = new HashMap<>();
            map.put("cusId", cusId);
            map.put("lockStatus", lockStatus);

            if (DscmsCusEnum.DTGH_FLAG_01.key.equals(dtghFlag)) {//1 公司客户
                result = cusCorpMapper.updateLockStatusBycusId(map);
            } else if (DscmsCusEnum.DTGH_FLAG_02.key.equals(dtghFlag)) {//2同业客户
                result = cusIntbankMapper.updateLockStatusBycusId(map);
            } else if (DscmsCusEnum.DTGH_FLAG_03.key.equals(dtghFlag)) {//3专业贷款
                //TODO 逻辑处理待完善
                result = 1;
            }
            //返回结果
            if (result > 0) {
                xdkh0032DataRespDto.setOpFlag(DscmsCusEnum.RETURN_SUCCESS.key);
                xdkh0032DataRespDto.setOpMsg(DscmsCusEnum.RETURN_SUCCESS.value);
            } else {
                xdkh0032DataRespDto.setOpFlag(DscmsCusEnum.RETURN_FAIL.key);
                xdkh0032DataRespDto.setOpMsg(DscmsCusEnum.RETURN_FAIL.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, JSON.toJSONString(xdkh0032DataReqDto));
        return xdkh0032DataRespDto;
    }
}
