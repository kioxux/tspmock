package cn.com.yusys.yusp.web.server.cmiscus0017;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0017.req.Cmiscus0017ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0017.resp.Cmiscus0017RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscus0017.Cmiscus0017Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 查询客户去年12期财报中最大的主营业务收入
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0017:查询个人无还本续贷表信息")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0017Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisCus0017Resource.class);

    @Autowired
    private Cmiscus0017Service cmiscus0017Service;

    /**
     * 交易码：cmiscus0017
     * 交易描述：查询客户去年12期财报中最大的主营业务收入z
     *
     * @param cmisCus0017ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询个人无还本续贷表信息")
    @PostMapping("/cmiscus0017")
    protected @ResponseBody
    ResultDto<Cmiscus0017RespDto> cmiscus0017(@Validated @RequestBody Cmiscus0017ReqDto cmisCus0017ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0017.key, DscmsEnum.TRADE_CODE_CMISCUS0017.value, JSON.toJSONString(cmisCus0017ReqDto));
        Cmiscus0017RespDto cmisCus0017RespDto = new Cmiscus0017RespDto();// 响应Dto:查询个人无还本续贷表信息
        ResultDto<Cmiscus0017RespDto> cmisCus0017ResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0017.key, DscmsEnum.TRADE_CODE_CMISCUS0017.value, JSON.toJSONString(cmisCus0017ReqDto));
            cmisCus0017RespDto = cmiscus0017Service.cmiscus0017(cmisCus0017ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0017.key, DscmsEnum.TRADE_CODE_CMISCUS0017.value, JSON.toJSONString(cmisCus0017RespDto));
            // 封装cmisCus0017ResultDto中正确的返回码和返回信息
            cmisCus0017ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0017ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0017.key, DscmsEnum.TRADE_CODE_CMISCUS0017.value, e.getMessage());
            // 封装cmisCus0017ResultDto中异常返回码和返回信息
            cmisCus0017ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0017ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0017RespDto到cmisCus0017ResultDto中
        cmisCus0017ResultDto.setData(cmisCus0017RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0017.key, DscmsEnum.TRADE_CODE_CMISCUS0017.value, JSON.toJSONString(cmisCus0017ResultDto));
        return cmisCus0017ResultDto;
    }

}
