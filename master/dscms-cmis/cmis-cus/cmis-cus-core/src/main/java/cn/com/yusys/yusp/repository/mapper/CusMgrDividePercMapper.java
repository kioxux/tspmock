/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.CusMgrDividePercDto;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import cn.com.yusys.yusp.domain.CusMgrDividePerc;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusMgrDividePercMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 14:22:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusMgrDividePercMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusMgrDividePerc selectByPrimaryKey(@Param("pkId") String pkId, @Param("cusId") String cusId, @Param("managerId") String managerId);

    /**
     * @方法名称: selectByPkId
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<CusMgrDividePerc> selectByPkId(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusMgrDividePerc> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusMgrDividePerc record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusMgrDividePerc record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusMgrDividePerc record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusMgrDividePerc record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("cusId") String cusId, @Param("managerId") String managerId);

    /**
     * @方法名称: selectByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusMgrDividePerc> selectByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: deleteByParam
     * @方法描述: 根据传入参数删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByParam(@Param("pkId") String pkId, @Param("cusId") String cusId, @Param("managerId") String managerId, @Param("managerProp") String managerProp);

    /**
     * @方法名称: selectByParam
     * @方法描述: 根据指定参数查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusMgrDividePerc> selectByParam(@Param("cusId") String cusId, @Param("managerId") String managerId, @Param("managerProp") String managerProp);

    /**
     * @方法名称: selectAllByCusId
     * @方法描述: 根据客户编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusMgrDividePerc> selectAllByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: updateApproveStatus
     * @方法描述: 更新流程审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApproveStatus(@Param("pkId") String pkId, @Param("approveStatus") String approveStatus, @Param("updateTime") Date updateTime);

    /**
     * @方法名称: updateApproveStatusAndEffect
     * @方法描述: 更新流程审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateApproveStatusAndEffect(@Param("pkId") String pkId, @Param("approveStatus") String approveStatus, @Param("isEffect") String isEffect, @Param("updateTime") Date updateTime);

    /**
     * @方法名称: selectEffectListByCusId
     * @方法描述: 根据客户编号查询已生效的列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusMgrDividePerc> selectEffectListByCusId(@Param("cusId") String cusId);

    /**
     * @方法名称: selectManagerId
     * @方法描述: 根据客户编号查询协办客户经理
     * @参数与返回说明:
     * @算法描述: 无
     */
    CusMgrDividePercDto selectXBManagerId(@Param("cusId") String cusId);
}