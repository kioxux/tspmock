package cn.com.yusys.yusp.service.client.bsp.outerdata.idcheck;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2OuterdataClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：个人身份核查
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
@Transactional
public class IdCheckService {
    private static final Logger logger = LoggerFactory.getLogger(IdCheckService.class);

    // 1）注入：BSP封装调用ECIF系统的接口
    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    /**
     * 业务逻辑处理方法：个人身份核查
     *
     * @param idCheckReqDto
     * @return
     */
    @Transactional
    public IdCheckRespDto idCheck(IdCheckReqDto idCheckReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, JSON.toJSONString(idCheckReqDto));
        ResultDto<IdCheckRespDto> idCheckResultDto = dscms2OuterdataClientService.idCheck(idCheckReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, JSON.toJSONString(idCheckResultDto));

        String idCheckCode = Optional.ofNullable(idCheckResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String idCheckMeesage = Optional.ofNullable(idCheckResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        IdCheckRespDto idCheckRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, idCheckResultDto.getCode())) {
            //  获取相关的值并解析
            idCheckRespDto = idCheckResultDto.getData();
        } else {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, idCheckMeesage);
            //  抛出错误异常
            throw new YuspException(idCheckCode, idCheckMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value);
        return idCheckRespDto;
    }
}
