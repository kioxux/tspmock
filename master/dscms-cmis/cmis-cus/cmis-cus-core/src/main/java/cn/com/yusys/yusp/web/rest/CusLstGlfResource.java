/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.dto.CusLstGlfDto;
import cn.com.yusys.yusp.service.CusIndivService;
import cn.com.yusys.yusp.vo.CusLstDedkkhVo;
import cn.com.yusys.yusp.vo.CusLstGlfVo;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstGlf;
import cn.com.yusys.yusp.service.CusLstGlfService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstGlfResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-13 20:17:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstglf")
public class CusLstGlfResource {
    @Autowired
    private CusLstGlfService cusLstGlfService;
    @Autowired
    private CusIndivService cusIndivService;

    private final Logger logger = LoggerFactory.getLogger(CusLstGlfResource.class);
    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstGlf>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstGlf> list = cusLstGlfService.selectAll(queryModel);
        return new ResultDto<List<CusLstGlf>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstGlf>> index(QueryModel queryModel) {
        List<CusLstGlf> list = cusLstGlfService.selectByModel(queryModel);
        return new ResultDto<List<CusLstGlf>>(list);
    }

    @PostMapping("/treelist")
    protected ResultDto<List<Map<String, Object>>> indextree(@RequestBody String certNo) {
        List<Map<String, Object>> list = cusLstGlfService.selectTreeList(certNo);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstGlf> show(@PathVariable("serno") String serno) {
        CusLstGlf cusLstGlf = cusLstGlfService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstGlf>(cusLstGlf);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstGlf> create(@RequestBody CusLstGlf cusLstGlf) throws URISyntaxException {
        cusLstGlfService.insert(cusLstGlf);
        return new ResultDto<CusLstGlf>(cusLstGlf);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstGlf cusLstGlf) throws URISyntaxException {
        int result = cusLstGlfService.update(cusLstGlf);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CusLstGlf cusLstGlf) throws URISyntaxException {
        int result = cusLstGlfService.updateSelective(cusLstGlf);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstGlfService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstGlfService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:查询条数
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCount/{certCode}")
    protected ResultDto<Integer> queryCount(@PathVariable String certCode) throws URISyntaxException {
        int result = cusLstGlfService.selectCount(certCode);
        return new ResultDto<Integer>(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = cusLstGlfService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        // TODO  修改业务流水号
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库
        ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(CusLstGlfVo.class)
                // 批量操作需要将batch设置为true
                .batch(true)
                .file(tempFile)
                .dataStorage(ExcelUtils.batchConsumer(cusLstGlfService::insertInBatch)));
        logger.info("开始执行异步导出，导入taskId为[{}];", progressDto.getTaskId());
        return ResultDto.success(progressDto);
    }

    /**
     * 一键更新客户号
     */
    @PostMapping("/updatecusid")
    public ResultDto<Integer> updateCusId() {
        int result = cusLstGlfService.updateCusId();
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:querycuslstglfbycusid
     * @函数描述:根据客户号查询问题关联名单信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querycuslstglfbycusid")
    protected ResultDto<CusLstGlfDto> queryCusLstGlfByCusId(@RequestBody String cusId) {
        return ResultDto.success(cusLstGlfService.queryCusLstGlfByCusId(cusId));
    }


    /**
     * @函数名称:querycuslstglfforindiv
     * @函数描述:查询生效的自然人关联方
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querycuslstglfforindiv")
    protected List<CusLstGlfDto> queryCusLstGlfForIndiv() {
        return cusLstGlfService.queryCusLstGlfForIndiv();
    }

}
