package cn.com.yusys.yusp.reportconf.service;

import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.ErrorConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.github.pagehelper.PageHelper;
import cn.com.yusys.yusp.reportconf.domain.FncConfDefFmt;
import cn.com.yusys.yusp.reportconf.repository.mapper.FncConfDefFmtMapper;
import cn.com.yusys.yusp.commons.module.adapter.exception.Message;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfDefFmtService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 11:26:38
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class FncConfDefFmtService {
	private static final Logger logger = LoggerFactory.getLogger(FncConfDefFmtService.class);
	@Autowired
	private FncConfDefFmtMapper fncConfDefFmtMapper;
//	@Autowired
//	private MessageProviderService messageProviderService;

	/**
	 * 样式科目详情页面
	 * 
	 * @param styleId
	 *            样式id
	 * @return
	 */
	public List<FncConfDefFmt> queryFncConfDefFmtList(String styleId) {
		List<FncConfDefFmt> list = fncConfDefFmtMapper.getFncConfDefFormatFromDB(styleId);
		if (null == list) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20202331);
			logger.error(ErrorConstants.NRCS_CMS_T20202331);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20202331, "报表配置定义查询错误");
		}
		return list;
	}

	/**
	 * 样式科目 新增功能
	 * 
	 * @param fncConfDefFmt
	 *            实体类
	 * @return
	 */
	@Transactional
	public int addFncConfDefFmt(FncConfDefFmt fncConfDefFmt) {
		int result = fncConfDefFmtMapper.insertSelective(fncConfDefFmt);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20202332);
			logger.error(ErrorConstants.NRCS_CMS_T20202332);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20202332, "报表配置定义新增错误");
		}
		return result;
	}

	/**
	 * 样式科目 修改功能
	 * 
	 * @param fncConfDefFmt
	 *            实体类
	 * @return
	 */
	@Transactional
	public int updateFncConfDefFmt(FncConfDefFmt fncConfDefFmt) {
		int result = fncConfDefFmtMapper.updateByPrimaryKeySelective(fncConfDefFmt);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20202333);
			logger.error(ErrorConstants.NRCS_CMS_T20202333);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20202333, "报表配置定义修改错误");
		}
		return result;
	}

	/**
	 * 样式科目 删除功能
	 * 
	 * @param styleId
	 *            样式id
	 * @param itemId
	 *            科目id
	 * @return
	 */
	@Transactional
	public int deleteFncConfDefFmt(String styleId, String itemId) {
		int result = fncConfDefFmtMapper.deleteByPrimaryKey(styleId, itemId);
		if (0 == result) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20202334);
			logger.error(ErrorConstants.NRCS_CMS_T20202334);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20202334, "报表配置定义删除错误");
		}
		return result;
	}

	/**
	 * 获取样式科目
	 * 
	 * @param styleId，样式id
	 * @return
	 */
	public List<FncConfDefFmt> getFncConfDefFormatFromDB(String styleId) {
		return fncConfDefFmtMapper.getFncConfDefFormatFromDB(styleId);
	}

	/**
	 * 获取样式科目
	 * 
	 * @param styleId，样式id
	 * @param itemId，科目id
	 * @return
	 */
	public FncConfDefFmt queryFncConfDefFmtByKey(String styleId, String itemId) {
		FncConfDefFmt fncConfDefFmt = fncConfDefFmtMapper.selectByPrimaryKey(styleId, itemId);
		if (null == fncConfDefFmt) {
//			Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20202335);
			logger.error(ErrorConstants.NRCS_CMS_T20202335);
			throw new YuspException(ErrorConstants.NRCS_CMS_T20202335, "报表配置定义查看错误");
		}
		return fncConfDefFmt;
	}

	/**
	 * @方法名称: selectByPrimaryKey
	 * @方法描述: 根据主键查询
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	public FncConfDefFmt selectByPrimaryKey(String styleId, String itemId) {
		return fncConfDefFmtMapper.selectByPrimaryKey(styleId, itemId);
	}

	/**
	 * @方法名称: selectAll
	 * @方法描述: 查询所有数据
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	@Transactional(readOnly = true)
	public List<FncConfDefFmt> selectAll(QueryModel model) {
		List<FncConfDefFmt> records = (List<FncConfDefFmt>) fncConfDefFmtMapper.selectByModel(model);
		return records;
	}

	/**
	 * @方法名称: selectByModel
	 * @方法描述: 条件查询 - 查询进行分页
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	public List<FncConfDefFmt> selectByModel(QueryModel model) {
		PageHelper.startPage(model.getPage(), model.getSize());
		List<FncConfDefFmt> list = fncConfDefFmtMapper.selectByModel(model);
		PageHelper.clearPage();
		return list;
	}

	/**
	 * @方法名称: insert
	 * @方法描述: 插入
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	public int insert(FncConfDefFmt record) {
		return fncConfDefFmtMapper.insert(record);
	}

	/**
	 * @方法名称: insertSelective
	 * @方法描述: 插入 - 只插入非空字段
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	public int insertSelective(FncConfDefFmt record) {
		return fncConfDefFmtMapper.insertSelective(record);
	}

	/**
	 * @方法名称: update
	 * @方法描述: 根据主键更新
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	public int update(FncConfDefFmt record) {
		return fncConfDefFmtMapper.updateByPrimaryKey(record);
	}

	/**
	 * @方法名称: updateSelective
	 * @方法描述: 根据主键更新 - 只更新非空字段
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	public int updateSelective(FncConfDefFmt record) {
		return fncConfDefFmtMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * @方法名称: deleteByPrimaryKey
	 * @方法描述: 根据主键删除
	 * @参数与返回说明:
	 * @算法描述: 无
	 */
	public int deleteByPrimaryKey(String styleId, String itemId) {
		return fncConfDefFmtMapper.deleteByPrimaryKey(styleId, itemId);
	}

	/**
	 * 财务报表信息 资产负债、损益、现金流量、 财务指标调用
	 * 
	 * @param fncConfDefFmt
	 * @return
	 */
	public List<FncConfDefFmt> queryFncConfDefFmtList1(FncConfDefFmt fncConfDefFmt) {
		return fncConfDefFmtMapper.queryFncConfDefFmtList1(fncConfDefFmt);
	}

	/**
	 * 整体拼接动态sql直接执行
	 * 
	 * @param preSql
	 * @return
	 */
	public List<FncConfDefFmt> queryFncConfDefFmtList2(String preSql) {
		return fncConfDefFmtMapper.queryFncConfDefFmtList2(preSql);
	}

}
