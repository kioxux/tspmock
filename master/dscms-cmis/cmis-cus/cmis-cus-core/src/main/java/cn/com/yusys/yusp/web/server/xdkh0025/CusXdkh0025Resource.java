package cn.com.yusys.yusp.web.server.xdkh0025;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0025.req.Xdkh0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0025.resp.Xdkh0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0025.Xdkh0025Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优企贷、优农贷客户信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0025:优企贷、优农贷客户信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0025Resource.class);

    @Autowired
    private Xdkh0025Service xdkh0025Service;

    /**
     * 交易码：xdkh0025
     * 交易描述：优企贷、优农贷客户信息查询
     *
     * @param xdkh0025DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷客户信息查询")
    @PostMapping("/xdkh0025")
    protected @ResponseBody
    ResultDto<Xdkh0025DataRespDto> xdkh0025(@Validated @RequestBody Xdkh0025DataReqDto xdkh0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025DataReqDto));
        Xdkh0025DataRespDto xdkh0025DataRespDto = new Xdkh0025DataRespDto();// 响应Dto:优企贷、优农贷客户信息查询
        ResultDto<Xdkh0025DataRespDto> xdkh0025DataResultDto = new ResultDto<>();
        try {
            String cusIds = xdkh0025DataReqDto.getCus_id();
            String cusNames = xdkh0025DataReqDto.getCus_name();
            if (StringUtils.isBlank(cusIds) && StringUtils.isBlank(cusNames)) {
                xdkh0025DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdkh0025DataResultDto.setMessage("查询参数不能都为空！");
                return xdkh0025DataResultDto;
            }
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025DataReqDto));
            xdkh0025DataRespDto = xdkh0025Service.xdkh0025(xdkh0025DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025DataReqDto));
            // 封装xdkh0025DataResultDto中正确的返回码和返回信息
            xdkh0025DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0025DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, e.getMessage());
            // 封装xdkh0025DataResultDto中异常返回码和返回信息
            xdkh0025DataResultDto.setCode(e.getErrorCode());
            xdkh0025DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, e.getMessage());
            // 封装xdkh0025DataResultDto中异常返回码和返回信息
            xdkh0025DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0025DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0025DataRespDto到xdkh0025DataResultDto中
        xdkh0025DataResultDto.setData(xdkh0025DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025DataResultDto));
        return xdkh0025DataResultDto;
    }
}
