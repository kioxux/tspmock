/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0005.req.Xdkh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0005.resp.Xdkh0005DataRespDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusIntbank;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIntbankMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-04-08 11:10:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusIntbankMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusIntbank selectByPrimaryKey(@Param("cusId") String cusId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusIntbank> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusIntbank record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusIntbank record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusIntbank record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusIntbank record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("cusId") String cusId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);


    /**
     * @方法名称: selectByModeList
     * @方法描述: 根据条件查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusIntbank> selectByModeList(CusIntbank cusIntbank);


    /**
     * @方法名称: selectCusIdByBankNo
     * @方法描述: 根据行号查找客户号
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectCusIdByBankNo(String bankNo);

    /**
     * 查询金融同业客户信息
     * @param xdkh0005DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdkh0005.resp.List> getBankInfo(Xdkh0005DataReqDto xdkh0005DataReqDto);

    /**
     * @方法名称: updateLockStatusBycusId
     * @方法描述: 根据客户号更新是否锁定
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateLockStatusBycusId(Map map);


    /**
     * @函数名称:queryCusIntbankBySocialCreditCode
     * @函数描述:根据社会信用代码查询同业客户信息
     * @参数与返回说明:
     * @算法描述:
     */
    CusIntbank queryCusIntbankBySocialCreditCode(CusIntbank CusIntbank);

    /**
     * 根据客户编号,证件类型，证件号码查询同业客户基本信息
     *
     * @param cmisCus0010ReqDto
     * @return
     */
    List<CmisCus0010RespDto> getCmiscus0010(CmisCus0010ReqDto cmisCus0010ReqDto);

    /**
     * @方法名称: updateGradeRankAndPdBycusId
     * @方法描述: 根据客户号更新同业客户本行即期信用等级及平均违约概率
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateGradeRankAndPdBycusId(Map map);
}