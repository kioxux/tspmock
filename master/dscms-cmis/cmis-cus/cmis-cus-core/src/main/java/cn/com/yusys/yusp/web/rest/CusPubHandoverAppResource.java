/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusPubHandoverAppDto;
import cn.com.yusys.yusp.service.CusBaseService;
import cn.com.yusys.yusp.service.CusCorpService;
import cn.com.yusys.yusp.service.CusIndivAttrService;
import cn.com.yusys.yusp.service.CusPubHandoverAppService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusPubHandoverAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: pc
 * @创建时间: 2021-05-10 14:12:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuspubhandoverapp")
public class CusPubHandoverAppResource {
    private static final Logger log = LoggerFactory.getLogger(CusPubHandoverAppResource.class);


    @Autowired
    private CusPubHandoverAppService cusPubHandoverAppService;
    @Autowired
    private CusBaseService cusBaseService;
    @Autowired
    private CusIndivAttrService cusIndivAttrService;
    @Autowired
    private CusCorpService cusCorpService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusPubHandoverApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusPubHandoverApp> list = cusPubHandoverAppService.selectAll(queryModel);
        return new ResultDto<List<CusPubHandoverApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusPubHandoverApp>> index(QueryModel queryModel) {
        List<CusPubHandoverApp> list = cusPubHandoverAppService.selectByModel(queryModel);
        return new ResultDto<List<CusPubHandoverApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusPubHandoverApp>> query(@RequestBody QueryModel queryModel) {
        queryModel.setSort("createTime desc");
        List<CusPubHandoverApp> list = cusPubHandoverAppService.selectByModel(queryModel);
        return new ResultDto<List<CusPubHandoverApp>>(list);
    }

    @PostMapping("/querybyserno")
    protected ResultDto<List<CusPubHandoverApp>> querybyserno(@RequestBody QueryModel queryModel) {
        queryModel.setSort("createTime desc");
        List<CusPubHandoverApp> list = cusPubHandoverAppService.selectByModel(queryModel);
        return new ResultDto<List<CusPubHandoverApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusPubHandoverApp> show(@PathVariable("serno") String serno) {
        CusPubHandoverApp cusPubHandoverApp = cusPubHandoverAppService.selectByPrimaryKey(serno);
        return new ResultDto<CusPubHandoverApp>(cusPubHandoverApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusPubHandoverApp> create(@RequestBody CusPubHandoverApp cusPubHandoverApp) throws URISyntaxException {
        cusPubHandoverAppService.insert(cusPubHandoverApp);
        return new ResultDto<CusPubHandoverApp>(cusPubHandoverApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusPubHandoverApp cusPubHandoverApp) throws URISyntaxException {
        int result = cusPubHandoverAppService.update(cusPubHandoverApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusPubHandoverAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusPubHandoverAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:operate
     * @函数描述:数据插入和更新操作
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/operate")
    protected ResultDto<Integer> operate(@PathVariable String ids) {
        int result = cusPubHandoverAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insert
     * @函数描述:数据插入
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/inserthandoverapp")
    protected ResultDto<CusPubHandoverAppDto> insertHandoverApp(@RequestBody CusPubHandoverAppDto cusPubHandoverAppDto) {
        cusPubHandoverAppService.insertHandoverApp(cusPubHandoverAppDto);

        ResultDto<CusPubHandoverAppDto> result = new ResultDto<CusPubHandoverAppDto>(cusPubHandoverAppDto);
        List<CusPubHandoverLst> list = cusPubHandoverAppDto.getList();
        String isContainSm = "0";//默认不包含小企业客户
        for (CusPubHandoverLst lst :list) {
            CusBase cusBase = cusBaseService.selectByPrimaryKey(lst.getCusId());
            if(CmisCusConstants.STD_ZB_CUS_CATALOG_1.equals(cusBase.getCusCatalog())) {
                CusIndivAttr cusIndivAttr = cusIndivAttrService.selectByPrimaryKey(lst.getCusId());
                if(cusIndivAttr!=null){
                    if (StringUtils.isNotEmpty(cusIndivAttr.getIsSmconCus()) && "1".equals(cusIndivAttr.getIsSmconCus())){
                        isContainSm = "1";
                        break;
                    }
                }
            }else if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBase.getCusCatalog())){
                CusCorp cusCorp = cusCorpService.selectByPrimaryKey(lst.getCusId());
                if(cusCorp!=null){
                    if(StringUtils.isNotEmpty(cusCorp.getIsSmconCus())&& "1".equals(cusCorp.getIsSmconCus())){
                        isContainSm = "1";
                        break;
                    }
                }
            }
        }
        result.extParam("isContainSm",isContainSm);
        return result;
    }

}
