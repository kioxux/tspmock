package cn.com.yusys.yusp.service.client.bsp.ecif.g00102;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.RelArrayInfo;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2EcifClientService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 业务逻辑处理类：对公客户综合信息维护
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class G00102Service {
    private static final Logger logger = LoggerFactory.getLogger(G00102Service.class);

    // 1）注入：BSP封装调用ECIF系统的接口
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    /**
     * 业务逻辑处理方法：对公客户综合信息维护
     *
     * @param g00102ReqDto
     * @return
     */
    @Transactional
    public G00102RespDto g00102(G00102ReqDto g00102ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value);
        validateAndSetValue(g00102ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, JSON.toJSONString(g00102ReqDto));
        ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, JSON.toJSONString(g00102ResultDto));
        String g00102Code = Optional.ofNullable(g00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g00102Meesage = Optional.ofNullable(g00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G00102RespDto g00102RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, g00102ResultDto.getCode())) {
            //  获取相关的值并解析
            g00102RespDto = g00102ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(g00102Code, g00102Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value);
        return g00102RespDto;
    }

    /**
     * 校验g00102ReqDto对象并赋值
     *
     * @param g00102ReqDto
     */
    private void validateAndSetValue(G00102ReqDto g00102ReqDto) {
        logger.info(TradeLogConstants.SERVICE_VALIDATE_SET_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, JSON.toJSONString(g00102ReqDto));
        g00102ReqDto.setCustno(Optional.ofNullable(g00102ReqDto.getCustno()).orElse(StringUtils.EMPTY));//   客户编号
        g00102ReqDto.setCustna(Optional.ofNullable(g00102ReqDto.getCustna()).orElse(StringUtils.EMPTY));//   客户名称
        g00102ReqDto.setCsstna(Optional.ofNullable(g00102ReqDto.getCsstna()).orElse(StringUtils.EMPTY));//   客户简称
        g00102ReqDto.setCsenna(Optional.ofNullable(g00102ReqDto.getCsenna()).orElse(StringUtils.EMPTY));//   客户英文名称
        g00102ReqDto.setCustst(Optional.ofNullable(g00102ReqDto.getCustst()).orElse(StringUtils.EMPTY));//   客户状态
        g00102ReqDto.setCusttp(Optional.ofNullable(g00102ReqDto.getCusttp()).orElse(StringUtils.EMPTY));//   客户类型
        g00102ReqDto.setCustsb(Optional.ofNullable(g00102ReqDto.getCustsb()).orElse(StringUtils.EMPTY));//   对公客户类型细分
        g00102ReqDto.setIdtftp(Optional.ofNullable(g00102ReqDto.getIdtftp()).orElse(StringUtils.EMPTY));//   证件类型
        g00102ReqDto.setIdtfno(Optional.ofNullable(g00102ReqDto.getIdtfno()).orElse(StringUtils.EMPTY));//   证件号码
        g00102ReqDto.setEfctdt(Optional.ofNullable(g00102ReqDto.getEfctdt()).orElse(StringUtils.EMPTY));//   证件生效日期
        g00102ReqDto.setInefdt(Optional.ofNullable(g00102ReqDto.getInefdt()).orElse(StringUtils.EMPTY));//   证件失效日期
        g00102ReqDto.setAreaad(Optional.ofNullable(g00102ReqDto.getAreaad()).orElse(StringUtils.EMPTY));//   发证机关国家或地区
        g00102ReqDto.setDepart(Optional.ofNullable(g00102ReqDto.getDepart()).orElse(StringUtils.EMPTY));//   发证机关
        g00102ReqDto.setGvrgno(Optional.ofNullable(g00102ReqDto.getGvrgno()).orElse(StringUtils.EMPTY));//   营业执照号码
        g00102ReqDto.setGvbgdt(Optional.ofNullable(g00102ReqDto.getGvbgdt()).orElse(StringUtils.EMPTY));//   营业执照生效日期
        g00102ReqDto.setGveddt(Optional.ofNullable(g00102ReqDto.getGveddt()).orElse(StringUtils.EMPTY));//   营业执照失效日期
        g00102ReqDto.setGvtion(Optional.ofNullable(g00102ReqDto.getGvtion()).orElse(StringUtils.EMPTY));//   营业执照发证机关
        g00102ReqDto.setCropcd(Optional.ofNullable(g00102ReqDto.getCropcd()).orElse(StringUtils.EMPTY));//   组织机构代码
        g00102ReqDto.setOreddt(Optional.ofNullable(g00102ReqDto.getOreddt()).orElse(StringUtils.EMPTY));//   组织机构代码失效日期
        g00102ReqDto.setNttxno(Optional.ofNullable(g00102ReqDto.getNttxno()).orElse(StringUtils.EMPTY));//   税务证件号
        g00102ReqDto.setSwbgdt(Optional.ofNullable(g00102ReqDto.getSwbgdt()).orElse(StringUtils.EMPTY));//   税务证件生效日期
        g00102ReqDto.setSweddt(Optional.ofNullable(g00102ReqDto.getSweddt()).orElse(StringUtils.EMPTY));//   税务证件失效日期
        g00102ReqDto.setSwtion(Optional.ofNullable(g00102ReqDto.getSwtion()).orElse(StringUtils.EMPTY));//   税务证件发证机关
        g00102ReqDto.setJgxyno(Optional.ofNullable(g00102ReqDto.getJgxyno()).orElse(StringUtils.EMPTY));//   机构信用代码证
        g00102ReqDto.setCropdt(Optional.ofNullable(g00102ReqDto.getCropdt()).orElse(StringUtils.EMPTY));//   机构信用代码证失效日期
        g00102ReqDto.setOpcfno(Optional.ofNullable(g00102ReqDto.getOpcfno()).orElse(StringUtils.EMPTY));//   开户许可证
        g00102ReqDto.setFoundt(Optional.ofNullable(g00102ReqDto.getFoundt()).orElse(StringUtils.EMPTY));//   成立日期
        g00102ReqDto.setCorppr(Optional.ofNullable(g00102ReqDto.getCorppr()).orElse(StringUtils.EMPTY));//   行业类别
        g00102ReqDto.setCoppbc(Optional.ofNullable(g00102ReqDto.getCoppbc()).orElse(StringUtils.EMPTY));//   人行行业类别
        g00102ReqDto.setEntetp(Optional.ofNullable(g00102ReqDto.getEntetp()).orElse(StringUtils.EMPTY));//   产业分类
        g00102ReqDto.setCorpnu(Optional.ofNullable(g00102ReqDto.getCorpnu()).orElse(StringUtils.EMPTY));//   企业性质
        g00102ReqDto.setBlogto(Optional.ofNullable(g00102ReqDto.getBlogto()).orElse(StringUtils.EMPTY));//   隶属关系
        g00102ReqDto.setPropts(Optional.ofNullable(g00102ReqDto.getPropts()).orElse(StringUtils.EMPTY));//   居民性质
        g00102ReqDto.setFinctp(Optional.ofNullable(g00102ReqDto.getFinctp()).orElse(StringUtils.EMPTY));//   控股类型（出资人经济类型）
        g00102ReqDto.setInvesj(Optional.ofNullable(g00102ReqDto.getInvesj()).orElse(StringUtils.EMPTY));//   投资主体
        g00102ReqDto.setRegitp(Optional.ofNullable(g00102ReqDto.getRegitp()).orElse(StringUtils.EMPTY));//   登记注册类型
        g00102ReqDto.setRegidt(Optional.ofNullable(g00102ReqDto.getRegidt()).orElse(StringUtils.EMPTY));//   注册日期
        g00102ReqDto.setRegicd(Optional.ofNullable(g00102ReqDto.getRegicd()).orElse(StringUtils.EMPTY));//   注册地国家代码
        g00102ReqDto.setRegchn(Optional.ofNullable(g00102ReqDto.getRegchn()).orElse(StringUtils.EMPTY));//   人民银行注册地区代码
        g00102ReqDto.setRgctry(Optional.ofNullable(g00102ReqDto.getRgctry()).orElse(StringUtils.EMPTY));//   注册地行政区划
        g00102ReqDto.setRgcrcy(Optional.ofNullable(g00102ReqDto.getRgcrcy()).orElse(StringUtils.EMPTY));//   注册资本币别
        g00102ReqDto.setRgcpam(Optional.ofNullable(g00102ReqDto.getRgcpam()).orElse(null));//   注册资本金额
        g00102ReqDto.setCacrcy(Optional.ofNullable(g00102ReqDto.getCacrcy()).orElse(StringUtils.EMPTY));//   实收资本币别
        g00102ReqDto.setCacpam(Optional.ofNullable(g00102ReqDto.getCacpam()).orElse(null));//   实收资本金额
        g00102ReqDto.setOrgatp(Optional.ofNullable(g00102ReqDto.getOrgatp()).orElse(StringUtils.EMPTY));//   组织机构类型
        g00102ReqDto.setSpectp(Optional.ofNullable(g00102ReqDto.getSpectp()).orElse(StringUtils.EMPTY));//   特殊经济区类型
        g00102ReqDto.setBusstp(Optional.ofNullable(g00102ReqDto.getBusstp()).orElse(StringUtils.EMPTY));//   企业经济类型
        g00102ReqDto.setNaticd(Optional.ofNullable(g00102ReqDto.getNaticd()).orElse(StringUtils.EMPTY));//   国民经济部门
        g00102ReqDto.setDebttp(Optional.ofNullable(g00102ReqDto.getDebttp()).orElse(StringUtils.EMPTY));//   境内主体类型
        g00102ReqDto.setEnvirt(Optional.ofNullable(g00102ReqDto.getEnvirt()).orElse(StringUtils.EMPTY));//   企业环保级别
        g00102ReqDto.setFinacd(Optional.ofNullable(g00102ReqDto.getFinacd()).orElse(StringUtils.EMPTY));//   金融机构类型代码
        g00102ReqDto.setSwifcd(Optional.ofNullable(g00102ReqDto.getSwifcd()).orElse(StringUtils.EMPTY));//   SWIFT编号
        g00102ReqDto.setSpntcd(Optional.ofNullable(g00102ReqDto.getSpntcd()).orElse(StringUtils.EMPTY));//   该SPV或壳机构所属国家/地区
        g00102ReqDto.setAdmnti(Optional.ofNullable(g00102ReqDto.getAdmnti()).orElse(StringUtils.EMPTY));//   经营期限
        g00102ReqDto.setAdmnst(Optional.ofNullable(g00102ReqDto.getAdmnst()).orElse(StringUtils.EMPTY));//   经营状态
        g00102ReqDto.setCorpsz(Optional.ofNullable(g00102ReqDto.getCorpsz()).orElse(StringUtils.EMPTY));//   企业规模
        g00102ReqDto.setBusisp(Optional.ofNullable(g00102ReqDto.getBusisp()).orElse(StringUtils.EMPTY));//   经营范围
        g00102ReqDto.setAcesbs(Optional.ofNullable(g00102ReqDto.getAcesbs()).orElse(StringUtils.EMPTY));//   客户兼营业务
        g00102ReqDto.setEmplnm(Optional.ofNullable(g00102ReqDto.getEmplnm()).orElse(null));//   员工总数
        g00102ReqDto.setComput(Optional.ofNullable(g00102ReqDto.getComput()).orElse(StringUtils.EMPTY));//   主管单位
        g00102ReqDto.setUpcrps(Optional.ofNullable(g00102ReqDto.getUpcrps()).orElse(StringUtils.EMPTY));//   主管单位法人名称
        g00102ReqDto.setUpidtp(Optional.ofNullable(g00102ReqDto.getUpidtp()).orElse(StringUtils.EMPTY));//   主管单位法人证件类型
        g00102ReqDto.setUpidno(Optional.ofNullable(g00102ReqDto.getUpidno()).orElse(StringUtils.EMPTY));//   主管单位法人证件号码
        g00102ReqDto.setLoanno(Optional.ofNullable(g00102ReqDto.getLoanno()).orElse(StringUtils.EMPTY));//   贷款卡编码
        g00102ReqDto.setWkflar(Optional.ofNullable(g00102ReqDto.getWkflar()).orElse(null));//   经营场地面积
        g00102ReqDto.setWkflfe(Optional.ofNullable(g00102ReqDto.getWkflfe()).orElse(StringUtils.EMPTY));//   经营场地所有权
        g00102ReqDto.setBaseno(Optional.ofNullable(g00102ReqDto.getBaseno()).orElse(StringUtils.EMPTY));//   基本账户核准号
        g00102ReqDto.setAcctnm(Optional.ofNullable(g00102ReqDto.getAcctnm()).orElse(StringUtils.EMPTY));//   基本账户开户行名称
        g00102ReqDto.setAcctbr(Optional.ofNullable(g00102ReqDto.getAcctbr()).orElse(StringUtils.EMPTY));//   基本账户开户行号
        g00102ReqDto.setAcctno(Optional.ofNullable(g00102ReqDto.getAcctno()).orElse(StringUtils.EMPTY));//   基本账户账号
        g00102ReqDto.setAcctdt(Optional.ofNullable(g00102ReqDto.getAcctdt()).orElse(StringUtils.EMPTY));//   基本账户开户日期
        g00102ReqDto.setDepotp(Optional.ofNullable(g00102ReqDto.getDepotp()).orElse(StringUtils.EMPTY));//   存款人类别
        g00102ReqDto.setRhtype(Optional.ofNullable(g00102ReqDto.getRhtype()).orElse(StringUtils.EMPTY));//   人行统计分类
        g00102ReqDto.setSumast(Optional.ofNullable(g00102ReqDto.getSumast()).orElse(null));//   公司总资产
        g00102ReqDto.setNetast(Optional.ofNullable(g00102ReqDto.getNetast()).orElse(null));//   公司净资产
        g00102ReqDto.setSthdfg(Optional.ofNullable(g00102ReqDto.getSthdfg()).orElse(StringUtils.EMPTY));//   是否我行股东
        g00102ReqDto.setSharhd(Optional.ofNullable(g00102ReqDto.getSharhd()).orElse(null));//   拥有我行股份金额
        g00102ReqDto.setListtg(Optional.ofNullable(g00102ReqDto.getListtg()).orElse(StringUtils.EMPTY));//   是否上市企业
        g00102ReqDto.setListld(Optional.ofNullable(g00102ReqDto.getListld()).orElse(StringUtils.EMPTY));//   上市地
        g00102ReqDto.setStoctp(Optional.ofNullable(g00102ReqDto.getStoctp()).orElse(StringUtils.EMPTY));//   股票类别
        g00102ReqDto.setStoccd(Optional.ofNullable(g00102ReqDto.getStoccd()).orElse(StringUtils.EMPTY));//   股票代码
        g00102ReqDto.setOnacnm(Optional.ofNullable(g00102ReqDto.getOnacnm()).orElse(null));//   一类户数量
        g00102ReqDto.setTotlnm(Optional.ofNullable(g00102ReqDto.getTotlnm()).orElse(null));//   总户数
        g00102ReqDto.setGrpctg(Optional.ofNullable(g00102ReqDto.getGrpctg()).orElse(StringUtils.EMPTY));//   是否集团客户
        g00102ReqDto.setReletg(Optional.ofNullable(g00102ReqDto.getReletg()).orElse(StringUtils.EMPTY));//   是否是我行关联客户
        g00102ReqDto.setFamltg(Optional.ofNullable(g00102ReqDto.getFamltg()).orElse(StringUtils.EMPTY));//   是否家族企业
        g00102ReqDto.setHightg(Optional.ofNullable(g00102ReqDto.getHightg()).orElse(StringUtils.EMPTY));//   是否高新技术企业
        g00102ReqDto.setIsbdcp(Optional.ofNullable(g00102ReqDto.getIsbdcp()).orElse(StringUtils.EMPTY));//   是否是担保公司
        g00102ReqDto.setSpcltg(Optional.ofNullable(g00102ReqDto.getSpcltg()).orElse(StringUtils.EMPTY));//   是否为特殊经济区内企业
        g00102ReqDto.setArimtg(Optional.ofNullable(g00102ReqDto.getArimtg()).orElse(StringUtils.EMPTY));//   是否地区重点企业
        g00102ReqDto.setFinatg(Optional.ofNullable(g00102ReqDto.getFinatg()).orElse(StringUtils.EMPTY));//   是否融资类企业
        g00102ReqDto.setResutg(Optional.ofNullable(g00102ReqDto.getResutg()).orElse(StringUtils.EMPTY));//   是否国资委所属企业
        g00102ReqDto.setSctlpr(Optional.ofNullable(g00102ReqDto.getSctlpr()).orElse(StringUtils.EMPTY));//   是否国家宏观调控限控行业
        g00102ReqDto.setFarmtg(Optional.ofNullable(g00102ReqDto.getFarmtg()).orElse(StringUtils.EMPTY));//   是否农户
        g00102ReqDto.setHgegtg(Optional.ofNullable(g00102ReqDto.getHgegtg()).orElse(StringUtils.EMPTY));//   是否高能耗企业
        g00102ReqDto.setExegtg(Optional.ofNullable(g00102ReqDto.getExegtg()).orElse(StringUtils.EMPTY));//   是否产能过剩企业
        g00102ReqDto.setElmntg(Optional.ofNullable(g00102ReqDto.getElmntg()).orElse(StringUtils.EMPTY));//   是否属于淘汰产能目录
        g00102ReqDto.setEnvifg(Optional.ofNullable(g00102ReqDto.getEnvifg()).orElse(StringUtils.EMPTY));//   是否环保企业
        g00102ReqDto.setHgplcp(Optional.ofNullable(g00102ReqDto.getHgplcp()).orElse(StringUtils.EMPTY));//   是否高污染企业
        g00102ReqDto.setIstdcs(Optional.ofNullable(g00102ReqDto.getIstdcs()).orElse(StringUtils.EMPTY));//   是否是我行贸易融资客户
        g00102ReqDto.setSpclfg(Optional.ofNullable(g00102ReqDto.getSpclfg()).orElse(StringUtils.EMPTY));//   特种经营标识
        g00102ReqDto.setSteltg(Optional.ofNullable(g00102ReqDto.getSteltg()).orElse(StringUtils.EMPTY));//   是否钢贸企业
        g00102ReqDto.setDomitg(Optional.ofNullable(g00102ReqDto.getDomitg()).orElse(StringUtils.EMPTY));//   是否优势企业
        g00102ReqDto.setStrutp(Optional.ofNullable(g00102ReqDto.getStrutp()).orElse(StringUtils.EMPTY));//   产业结构调整类型
        g00102ReqDto.setStratp(Optional.ofNullable(g00102ReqDto.getStratp()).orElse(StringUtils.EMPTY));//   战略新兴产业类型
        g00102ReqDto.setIndutg(Optional.ofNullable(g00102ReqDto.getIndutg()).orElse(StringUtils.EMPTY));//   工业转型升级标识
        g00102ReqDto.setLeadtg(Optional.ofNullable(g00102ReqDto.getLeadtg()).orElse(StringUtils.EMPTY));//   是否龙头企业
        g00102ReqDto.setCncatg(Optional.ofNullable(g00102ReqDto.getCncatg()).orElse(StringUtils.EMPTY));//   中资标志
        g00102ReqDto.setImpotg(Optional.ofNullable(g00102ReqDto.getImpotg()).orElse(StringUtils.EMPTY));//   进出口权标志
        g00102ReqDto.setClostg(Optional.ofNullable(g00102ReqDto.getClostg()).orElse(StringUtils.EMPTY));//   企业关停标志
        g00102ReqDto.setPlattg(Optional.ofNullable(g00102ReqDto.getPlattg()).orElse(StringUtils.EMPTY));//   是否苏州综合平台企业
        g00102ReqDto.setFibrtg(Optional.ofNullable(g00102ReqDto.getFibrtg()).orElse(StringUtils.EMPTY));//   是否为工业园区、经济开发区等行政管理区
        g00102ReqDto.setGvfntg(Optional.ofNullable(g00102ReqDto.getGvfntg()).orElse(StringUtils.EMPTY));//   是否政府投融资平台
        g00102ReqDto.setGvlnlv(Optional.ofNullable(g00102ReqDto.getGvlnlv()).orElse(StringUtils.EMPTY));//   地方政府融资平台隶属关系
        g00102ReqDto.setGvlwtp(Optional.ofNullable(g00102ReqDto.getGvlwtp()).orElse(StringUtils.EMPTY));//   地方政府融资平台法律性质
        g00102ReqDto.setGvlntp(Optional.ofNullable(g00102ReqDto.getGvlntp()).orElse(StringUtils.EMPTY));//   政府融资贷款类型
        g00102ReqDto.setPetytg(Optional.ofNullable(g00102ReqDto.getPetytg()).orElse(StringUtils.EMPTY));//   是否小额贷款公司
        g00102ReqDto.setFscddt(Optional.ofNullable(g00102ReqDto.getFscddt()).orElse(StringUtils.EMPTY));//   首次与我行建立信贷关系时间
        g00102ReqDto.setSupptg(Optional.ofNullable(g00102ReqDto.getSupptg()).orElse(StringUtils.EMPTY));//   是否列入重点扶持产业
        g00102ReqDto.setLicetg(Optional.ofNullable(g00102ReqDto.getLicetg()).orElse(StringUtils.EMPTY));//   是否一照一码
        g00102ReqDto.setLandtg(Optional.ofNullable(g00102ReqDto.getLandtg()).orElse(StringUtils.EMPTY));//   是否土地整治机构
        g00102ReqDto.setPasvfg(Optional.ofNullable(g00102ReqDto.getPasvfg()).orElse(StringUtils.EMPTY));//   是否消极非金融机构
        g00102ReqDto.setImagno(Optional.ofNullable(g00102ReqDto.getImagno()).orElse(StringUtils.EMPTY));//   影像编号
        g00102ReqDto.setTaxptp(Optional.ofNullable(g00102ReqDto.getTaxptp()).orElse(StringUtils.EMPTY));//   纳税人身份
        g00102ReqDto.setNeedfp(Optional.ofNullable(g00102ReqDto.getNeedfp()).orElse(StringUtils.EMPTY));//   是否需要开具增值税专用发票
        g00102ReqDto.setTaxpna(Optional.ofNullable(g00102ReqDto.getTaxpna()).orElse(StringUtils.EMPTY));//   纳税人全称
        g00102ReqDto.setTaxpad(Optional.ofNullable(g00102ReqDto.getTaxpad()).orElse(StringUtils.EMPTY));//   纳税人地址
        g00102ReqDto.setTaxptl(Optional.ofNullable(g00102ReqDto.getTaxptl()).orElse(StringUtils.EMPTY));//   纳税人电话
        g00102ReqDto.setTxbkna(Optional.ofNullable(g00102ReqDto.getTxbkna()).orElse(StringUtils.EMPTY));//   纳税人行名
        g00102ReqDto.setTaxpac(Optional.ofNullable(g00102ReqDto.getTaxpac()).orElse(StringUtils.EMPTY));//   纳税人账号
        g00102ReqDto.setTxpstp(Optional.ofNullable(g00102ReqDto.getTxpstp()).orElse(StringUtils.EMPTY));//   税收居民类型
        g00102ReqDto.setTaxadr(Optional.ofNullable(g00102ReqDto.getTaxadr()).orElse(StringUtils.EMPTY));//   税收单位英文地址
        g00102ReqDto.setTxnion(Optional.ofNullable(g00102ReqDto.getTxnion()).orElse(StringUtils.EMPTY));//   税收居民国（地区）
        g00102ReqDto.setTxennm(Optional.ofNullable(g00102ReqDto.getTxennm()).orElse(StringUtils.EMPTY));//   税收英文姓名
        g00102ReqDto.setTxbcty(Optional.ofNullable(g00102ReqDto.getTxbcty()).orElse(StringUtils.EMPTY));//   税收出生国家或地区
        g00102ReqDto.setTaxnum(Optional.ofNullable(g00102ReqDto.getTaxnum()).orElse(StringUtils.EMPTY));//   纳税人识别号
        g00102ReqDto.setNotxrs(Optional.ofNullable(g00102ReqDto.getNotxrs()).orElse(StringUtils.EMPTY));//   不能提供纳税人识别号原因
        g00102ReqDto.setCtigno(Optional.ofNullable(g00102ReqDto.getCtigno()).orElse(StringUtils.EMPTY));//   客户经理号
        g00102ReqDto.setCtigna(Optional.ofNullable(g00102ReqDto.getCtigna()).orElse(StringUtils.EMPTY));//   客户经理姓名
        g00102ReqDto.setCtigph(Optional.ofNullable(g00102ReqDto.getCtigph()).orElse(StringUtils.EMPTY));//   客户经理手机号
        g00102ReqDto.setCorptl(Optional.ofNullable(g00102ReqDto.getCorptl()).orElse(StringUtils.EMPTY));//   公司电话
        g00102ReqDto.setHometl(Optional.ofNullable(g00102ReqDto.getHometl()).orElse(StringUtils.EMPTY));//   联系电话
        g00102ReqDto.setRgpost(Optional.ofNullable(g00102ReqDto.getRgpost()).orElse(StringUtils.EMPTY));//   注册地址邮编
        g00102ReqDto.setRgcuty(Optional.ofNullable(g00102ReqDto.getRgcuty()).orElse(StringUtils.EMPTY));//   注册地址所在国家
        g00102ReqDto.setRgprov(Optional.ofNullable(g00102ReqDto.getRgprov()).orElse(StringUtils.EMPTY));//   注册地址所在省
        g00102ReqDto.setRgcity(Optional.ofNullable(g00102ReqDto.getRgcity()).orElse(StringUtils.EMPTY));//   注册地址所在市
        g00102ReqDto.setRgerea(Optional.ofNullable(g00102ReqDto.getRgerea()).orElse(StringUtils.EMPTY));//   注册地址所在区
        g00102ReqDto.setRegadr(Optional.ofNullable(g00102ReqDto.getRegadr()).orElse(StringUtils.EMPTY));//   注册地址
        g00102ReqDto.setBspost(Optional.ofNullable(g00102ReqDto.getBspost()).orElse(StringUtils.EMPTY));//   经营地址邮编
        g00102ReqDto.setBscuty(Optional.ofNullable(g00102ReqDto.getBscuty()).orElse(StringUtils.EMPTY));//   经营地址所在国家
        g00102ReqDto.setBsprov(Optional.ofNullable(g00102ReqDto.getBsprov()).orElse(StringUtils.EMPTY));//   经营地址所在省
        g00102ReqDto.setBscity(Optional.ofNullable(g00102ReqDto.getBscity()).orElse(StringUtils.EMPTY));//   经营地址所在市
        g00102ReqDto.setBserea(Optional.ofNullable(g00102ReqDto.getBserea()).orElse(StringUtils.EMPTY));//   经营地址所在区
        g00102ReqDto.setBusiad(Optional.ofNullable(g00102ReqDto.getBusiad()).orElse(StringUtils.EMPTY));//   经营地址
        g00102ReqDto.setEnaddr(Optional.ofNullable(g00102ReqDto.getEnaddr()).orElse(StringUtils.EMPTY));//   英文地址
        g00102ReqDto.setWebsit(Optional.ofNullable(g00102ReqDto.getWebsit()).orElse(StringUtils.EMPTY));//   公司网址
        g00102ReqDto.setFaxfax(Optional.ofNullable(g00102ReqDto.getFaxfax()).orElse(StringUtils.EMPTY));//   传真
        g00102ReqDto.setEmailx(Optional.ofNullable(g00102ReqDto.getEmailx()).orElse(StringUtils.EMPTY));//   邮箱
        g00102ReqDto.setSorgno(Optional.ofNullable(g00102ReqDto.getSorgno()).orElse(StringUtils.EMPTY));//   同业机构(行)号
        g00102ReqDto.setSorgtp(Optional.ofNullable(g00102ReqDto.getSorgtp()).orElse(StringUtils.EMPTY));//   同业机构(行)类型
        g00102ReqDto.setOrgsit(Optional.ofNullable(g00102ReqDto.getOrgsit()).orElse(StringUtils.EMPTY));//   同业机构(行)网址
        g00102ReqDto.setBkplic(Optional.ofNullable(g00102ReqDto.getBkplic()).orElse(StringUtils.EMPTY));//   同业客户金融业务许可证
        g00102ReqDto.setReguno(Optional.ofNullable(g00102ReqDto.getReguno()).orElse(StringUtils.EMPTY));//   同业非现场监管统计机构编码
        g00102ReqDto.setPdcpat(Optional.ofNullable(g00102ReqDto.getPdcpat()).orElse(null));//   同业客户实际到位资金(万元)
        g00102ReqDto.setSorglv(Optional.ofNullable(g00102ReqDto.getSorglv()).orElse(StringUtils.EMPTY));//   同业授信评估等级
        g00102ReqDto.setEvalda(Optional.ofNullable(g00102ReqDto.getEvalda()).orElse(StringUtils.EMPTY));//   同业客户评级到期日期
        g00102ReqDto.setReldgr(Optional.ofNullable(g00102ReqDto.getReldgr()).orElse(StringUtils.EMPTY));//   同业客户与我行合作关系
        g00102ReqDto.setLimind(Optional.ofNullable(g00102ReqDto.getLimind()).orElse(StringUtils.EMPTY));//   同业客户是否授信
        g00102ReqDto.setMabrid(Optional.ofNullable(g00102ReqDto.getMabrid()).orElse(StringUtils.EMPTY));//   同业客户主管机构(同业)
        g00102ReqDto.setManmgr(Optional.ofNullable(g00102ReqDto.getManmgr()).orElse(StringUtils.EMPTY));//   同业客户主管客户经理(同业)
        g00102ReqDto.setBriskc(Optional.ofNullable(g00102ReqDto.getBriskc()).orElse(StringUtils.EMPTY));//   同业客户风险大类
        g00102ReqDto.setMriskc(Optional.ofNullable(g00102ReqDto.getMriskc()).orElse(StringUtils.EMPTY));//   同业客户风险种类
        g00102ReqDto.setFriskc(Optional.ofNullable(g00102ReqDto.getFriskc()).orElse(StringUtils.EMPTY));//   同业客户风险小类
        g00102ReqDto.setRiskda(Optional.ofNullable(g00102ReqDto.getRiskda()).orElse(StringUtils.EMPTY));//   同业客户风险划分日期
        g00102ReqDto.setChflag(Optional.ofNullable(g00102ReqDto.getChflag()).orElse(StringUtils.EMPTY));//   境内标识
        g00102ReqDto.setNnjytp(Optional.ofNullable(g00102ReqDto.getNnjytp()).orElse(StringUtils.EMPTY));//   交易类型
        g00102ReqDto.setNnjyna(Optional.ofNullable(g00102ReqDto.getNnjyna()).orElse(StringUtils.EMPTY));//   交易名称

        // 关联信息_ARRAY
        RelArrayInfo temp = new RelArrayInfo();
        g00102ReqDto.setRelArrayInfo(Optional.ofNullable(g00102ReqDto.getRelArrayInfo()).orElse(Arrays.asList(temp)));
        g00102ReqDto.setRelArrayInfo(
                g00102ReqDto.getRelArrayInfo().parallelStream().map(relArrayInfo -> {
                    relArrayInfo.setCustrelid(Optional.ofNullable(relArrayInfo.getCustrelid()).orElse(StringUtils.EMPTY));//参与人之间编号
                    relArrayInfo.setCstno1(Optional.ofNullable(relArrayInfo.getCstno1()).orElse(StringUtils.EMPTY));//关系人客户编号
                    relArrayInfo.setValitg(Optional.ofNullable(relArrayInfo.getValitg()).orElse(StringUtils.EMPTY));//有效标志
                    relArrayInfo.setPrentg(Optional.ofNullable(relArrayInfo.getPrentg()).orElse(StringUtils.EMPTY));//自然人企业标志
                    relArrayInfo.setRelttp(Optional.ofNullable(relArrayInfo.getRelttp()).orElse(StringUtils.EMPTY));//关系类型
                    relArrayInfo.setRealna(Optional.ofNullable(relArrayInfo.getRealna()).orElse(StringUtils.EMPTY));//关系人姓名
                    relArrayInfo.setRlennm(Optional.ofNullable(relArrayInfo.getRlennm()).orElse(StringUtils.EMPTY));//关系人英文名称
                    relArrayInfo.setIdtftp(Optional.ofNullable(relArrayInfo.getIdtftp()).orElse(StringUtils.EMPTY));//证件类型
                    relArrayInfo.setIdtfno(Optional.ofNullable(relArrayInfo.getIdtfno()).orElse(StringUtils.EMPTY));//证件号码
                    relArrayInfo.setEfctdt(Optional.ofNullable(relArrayInfo.getEfctdt()).orElse(StringUtils.EMPTY));//证件生效日期
                    relArrayInfo.setInefdt(Optional.ofNullable(relArrayInfo.getInefdt()).orElse(StringUtils.EMPTY));//证件失效日期
                    relArrayInfo.setInvswy(Optional.ofNullable(relArrayInfo.getInvswy()).orElse(StringUtils.EMPTY));//出资方式
                    relArrayInfo.setCurrcy(Optional.ofNullable(relArrayInfo.getCurrcy()).orElse(StringUtils.EMPTY));//出资币种
                    relArrayInfo.setInvsam(Optional.ofNullable(relArrayInfo.getInvsam()).orElse(null));//出资金额
                    relArrayInfo.setInvsrt(Optional.ofNullable(relArrayInfo.getInvsrt()).orElse(null));//出资占比
                    relArrayInfo.setSthdtg(Optional.ofNullable(relArrayInfo.getSthdtg()).orElse(StringUtils.EMPTY));//是否控股股东
                    relArrayInfo.setLrattr(Optional.ofNullable(relArrayInfo.getLrattr()).orElse(StringUtils.EMPTY));//法人属性
                    relArrayInfo.setHomead(Optional.ofNullable(relArrayInfo.getHomead()).orElse(StringUtils.EMPTY));//联系地址
                    relArrayInfo.setHometl(Optional.ofNullable(relArrayInfo.getHometl()).orElse(StringUtils.EMPTY));//联系电话
                    relArrayInfo.setTxpstp(Optional.ofNullable(relArrayInfo.getTxpstp()).orElse(StringUtils.EMPTY));//税收居民类型
                    relArrayInfo.setTxbcty(Optional.ofNullable(relArrayInfo.getTxbcty()).orElse(StringUtils.EMPTY));//税收出生国家或地区
                    relArrayInfo.setTxnion(Optional.ofNullable(relArrayInfo.getTxnion()).orElse(StringUtils.EMPTY));//税收居民国（地区）
                    relArrayInfo.setTaxnum(Optional.ofNullable(relArrayInfo.getTaxnum()).orElse(StringUtils.EMPTY));//纳税人识别号
                    relArrayInfo.setAddttr(Optional.ofNullable(relArrayInfo.getAddttr()).orElse(StringUtils.EMPTY));//税收现居英文地址
                    relArrayInfo.setNotxrs(Optional.ofNullable(relArrayInfo.getNotxrs()).orElse(StringUtils.EMPTY));//不能提供纳税人识别号原因
                    relArrayInfo.setPrpsex(Optional.ofNullable(relArrayInfo.getPrpsex()).orElse(StringUtils.EMPTY));//性别
                    relArrayInfo.setBorndt(Optional.ofNullable(relArrayInfo.getBorndt()).orElse(StringUtils.EMPTY));//出生日期
                    relArrayInfo.setNation(Optional.ofNullable(relArrayInfo.getNation()).orElse(StringUtils.EMPTY));//国籍
                    relArrayInfo.setCutycd(Optional.ofNullable(relArrayInfo.getCutycd()).orElse(StringUtils.EMPTY));//民族
                    relArrayInfo.setPropts(Optional.ofNullable(relArrayInfo.getPropts()).orElse(StringUtils.EMPTY));//居民性质
                    relArrayInfo.setEduclv(Optional.ofNullable(relArrayInfo.getEduclv()).orElse(StringUtils.EMPTY));//教育水平（学历）
                    relArrayInfo.setWkutna(Optional.ofNullable(relArrayInfo.getWkutna()).orElse(StringUtils.EMPTY));//工作单位
                    relArrayInfo.setProjob(Optional.ofNullable(relArrayInfo.getProjob()).orElse(StringUtils.EMPTY));//职业
                    relArrayInfo.setPoston(Optional.ofNullable(relArrayInfo.getPoston()).orElse(StringUtils.EMPTY));//职务
                    relArrayInfo.setIncome(Optional.ofNullable(relArrayInfo.getIncome()).orElse(null));//月收入
                    relArrayInfo.setTbcome(Optional.ofNullable(relArrayInfo.getTbcome()).orElse(null));//年收入
                    relArrayInfo.setProvce(Optional.ofNullable(relArrayInfo.getProvce()).orElse(StringUtils.EMPTY));//省/直辖市/自治区
                    relArrayInfo.setCityna(Optional.ofNullable(relArrayInfo.getCityna()).orElse(StringUtils.EMPTY));//州市(地市或城市)
                    relArrayInfo.setCounty(Optional.ofNullable(relArrayInfo.getCounty()).orElse(StringUtils.EMPTY));//县/区
                    relArrayInfo.setEmailx(Optional.ofNullable(relArrayInfo.getEmailx()).orElse(StringUtils.EMPTY));//邮箱
                    relArrayInfo.setEtenna(Optional.ofNullable(relArrayInfo.getEtenna()).orElse(StringUtils.EMPTY));//企业英文名称
                    relArrayInfo.setEcontp(Optional.ofNullable(relArrayInfo.getEcontp()).orElse(StringUtils.EMPTY));//经济类型
                    relArrayInfo.setFoundt(Optional.ofNullable(relArrayInfo.getFoundt()).orElse(StringUtils.EMPTY));//成立日期
                    relArrayInfo.setBusisp(Optional.ofNullable(relArrayInfo.getBusisp()).orElse(StringUtils.EMPTY));//经营范围
                    relArrayInfo.setEmplnm(Optional.ofNullable(relArrayInfo.getEmplnm()).orElse(null));//员工总数
                    relArrayInfo.setCorppr(Optional.ofNullable(relArrayInfo.getCorppr()).orElse(StringUtils.EMPTY));//所属行业
                    relArrayInfo.setLawcna(Optional.ofNullable(relArrayInfo.getLawcna()).orElse(StringUtils.EMPTY));//法人代表名称
                    relArrayInfo.setLwidtf(Optional.ofNullable(relArrayInfo.getLwidtf()).orElse(StringUtils.EMPTY));//法人代表证件类型
                    relArrayInfo.setLwidno(Optional.ofNullable(relArrayInfo.getLwidno()).orElse(StringUtils.EMPTY));//法人代表证件号码
                    relArrayInfo.setLwidph(Optional.ofNullable(relArrayInfo.getLwidph()).orElse(StringUtils.EMPTY));//法人代表联系电话
                    return relArrayInfo;
                }).collect(Collectors.toList())
        );
        logger.info(TradeLogConstants.SERVICE_VALIDATE_SET_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, JSON.toJSONString(g00102ReqDto));
    }
}
