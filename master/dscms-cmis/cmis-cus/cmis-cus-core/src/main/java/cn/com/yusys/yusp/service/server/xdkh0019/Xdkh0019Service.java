package cn.com.yusys.yusp.service.server.xdkh0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.req.Xdkh0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.resp.Xdkh0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.bsp.ecif.s00101.S00101Service;
import cn.com.yusys.yusp.service.client.bsp.ecif.s00102.S00102Service;
import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0019Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0019Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0019Service.class);

    @Resource
    private CusBaseMapper cusBaseMapper;

    @Resource
    private CusIndivMapper cusIndivMapper;

    @Autowired
    private S00101Service s00101Service;

    @Autowired
    private S00102Service s00102Service;

    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 客户查询并开户
     *
     * @param xdkh0019DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0019DataRespDto xdkh0019(Xdkh0019DataReqDto xdkh0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value);
        Xdkh0019DataRespDto xdkh0019DataRespDto = new Xdkh0019DataRespDto();
        //个人客户
        CusIndiv cusIndiv = new CusIndiv();
        //客户基本信息表
        CusBase cusBase = new CusBase();
        S00101ReqDto s00101ReqDto = new S00101ReqDto();//对私客户信息查询接口
        S00101RespDto s00101RespDto = new S00101RespDto();//对私客户信息查询接口
        S00102ReqDto s00102ReqDto = new S00102ReqDto();//对私客户创建及维护接口
        S00102RespDto s00102RespDto = new S00102RespDto();//对私客户创建及维护接口
        // 获取当前系统日期
        String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        // ecif客户号
        String ecifCusId = "";
        try {
            //新微贷面谈调查客户查询，若为新户，创建客户号并返回；若为老户则直接返回现有客户号
            if (StringUtils.isEmpty(xdkh0019DataReqDto.getCertNo())) {
                throw BizException.error(null, EcsEnum.ECS040004.key, EcsEnum.ECS040004.value);
            }
            if (StringUtils.isEmpty(xdkh0019DataReqDto.getCertType())) {
                throw BizException.error(null, EcsEnum.ECS040003.key, EcsEnum.ECS040003.value);
            }
            //操作类型
            String opType = xdkh0019DataReqDto.getOpType();
            CusBase cusBaseInfo = cusBaseMapper.queryCusInfoByCertCode(xdkh0019DataReqDto.getCertNo());
            if (Objects.equals(opType, CmisCommonConstants.OP_TYPE_01)) {
                if (null == cusBaseInfo) {
                    //调用客户端接口s00101查询ECIF客户信息
                    String certNo = xdkh0019DataReqDto.getCertNo();
                    s00101ReqDto.setIdtfno(certNo);
                    s00101ReqDto.setIdtftp(xdkh0019DataReqDto.getCertType());
                    s00101ReqDto.setCustna(xdkh0019DataReqDto.getCusName());
                    s00101ReqDto.setResotp("3");
                    s00101RespDto = s00101Service.s00101(s00101ReqDto);
                    // 若ecif存在该客户信息，获取ecif返回的客户号
                    if (StringUtils.isEmpty(s00101RespDto.getCustno()) || null == s00101RespDto.getCustno()) {
                        // ecif不存在该客户信息，调用客户端接口s00102创建客户信息，获取ecif客户号
                        s00102ReqDto.setCustna(xdkh0019DataReqDto.getCusName());//客户名称
                        s00102ReqDto.setIdtftp(xdkh0019DataReqDto.getCertType());//证件类型
                        s00102ReqDto.setIdtfno(xdkh0019DataReqDto.getCertNo());//证件号码
                        // 客户状态
                        s00102ReqDto.setCustst("3");

                        s00102ReqDto.setCtcktg("1");

                        s00102ReqDto.setResult("00");
                        s00102RespDto = s00102Service.s00102(s00102ReqDto);
                        if (StringUtils.isEmpty(s00102RespDto.getCustno())) {
                            throw BizException.error(null, EcsEnum.ECS040002.key, EcsEnum.ECS040002.value);
                        }
                        ecifCusId = s00102RespDto.getCustno();
                        // 获取客户号后，将返回的Ecif客户号与请求信息按临时客户插入信贷客户主表以及对公客户表中
                        //更新cusBase表
                        BeanUtils.copyProperties(xdkh0019DataReqDto, cusBase);
                        //客户号
                        cusBase.setCusId(ecifCusId);
                        //客户名称
                        cusBase.setCusName(xdkh0019DataReqDto.getCusName());
                        //证件类型
                        cusBase.setCertType(xdkh0019DataReqDto.getCertType());
                        //证件号码
                        cusBase.setCertCode(xdkh0019DataReqDto.getCertNo());
                        //客户分类
                        cusBase.setCusRankCls(xdkh0019DataReqDto.getCusType());
                        //工号
                        String managerId = xdkh0019DataReqDto.getManagerId();
                        //机构
                        String managerBrId = xdkh0019DataReqDto.getManagerBrId();

                        if (!Objects.equals("xwd00001", managerId)) {
                            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                            AdminSmUserDto adminSmUserDto = resultDto.getData();
                            if (Objects.nonNull(adminSmUserDto)) {
                                managerBrId = adminSmUserDto.getOrgId();
                            }
                        }
                        //登记人
                        cusBase.setInputId(xdkh0019DataReqDto.getManagerId());
                        //登记机构
                        cusBase.setInputBrId(managerBrId);
                        //登记日期
                        cusBase.setInputDate(openDay);
                        //管户客户经理
                        cusBase.setManagerId(managerId);
                        //主管机构
                        cusBase.setManagerBrId(managerBrId);
                        // 客户状态 1暂存
                        cusBase.setCusState(CmisCusConstants.STD_CUS_STATE_2);
                        //客户大类 2 个人
                        cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PRI);
                        if(cusBase.getCusRankCls() == null || "".equals(cusBase.getCusRankCls())){
                            cusBase.setCusRankCls("02");//临时客户
                        }
                        cusBaseMapper.insert(cusBase);
                        //更新cusIndiv表
                        BeanUtils.copyProperties(xdkh0019DataReqDto, cusIndiv);
                        cusIndiv.setCusType(cn.com.yusys.yusp.commons.util.StringUtils.EMPTY);//接口cusType字段传过来的是客户分类，此处置空
                        cusIndiv.setCusId(ecifCusId);//客户号
                        cusIndiv.setCusName(xdkh0019DataReqDto.getCusName());//客户名称
                        cusIndiv.setInputId(managerId);//登记人
                        cusIndiv.setInputBrId(managerBrId);//登记机构
                        cusIndiv.setInputDate(openDay);//登记日期
                        cusIndiv.setCusType("110");//客户类型：110-一般自然人 120-个体工商户(无字号)
                        cusIndivMapper.insert(cusIndiv);
                        xdkh0019DataRespDto.setCusId(ecifCusId);
                    } else {
                        // 接收ecif客户号，查询信贷客户信息表
                        CusBase cusBaseSelect = cusBaseMapper.selectByPrimaryKey(s00101RespDto.getCustno());
                        if (cusBaseSelect != null) {
                            // 该客户在信贷中存在，则直接返回客户号
                            xdkh0019DataRespDto.setCusId(s00101RespDto.getCustno());
                        } else {
                            // 将返回的Ecif客户号与请求信息按临时客户插入信贷客户主表以及对公客户表中
                            ecifCusId = s00101RespDto.getCustno();
                            //更新cusBase表
                            BeanUtils.copyProperties(xdkh0019DataReqDto, cusBase);
                            //客户号
                            cusBase.setCusId(ecifCusId);
                            //客户名称
                            cusBase.setCusName(xdkh0019DataReqDto.getCusName());
                            //证件类型
                            cusBase.setCertType(xdkh0019DataReqDto.getCertType());
                            //证件号码
                            cusBase.setCertCode(xdkh0019DataReqDto.getCertNo());
                            //客户分类
                            cusBase.setCusRankCls(xdkh0019DataReqDto.getCusType());
                            //工号
                            String managerId = xdkh0019DataReqDto.getManagerId();
                            //机构
                            String managerBrId = xdkh0019DataReqDto.getManagerBrId();

                            if (!Objects.equals("xwd00001", managerId)) {
                                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                                AdminSmUserDto adminSmUserDto = resultDto.getData();
                                if (Objects.nonNull(adminSmUserDto)) {
                                    managerBrId = adminSmUserDto.getOrgId();
                                }
                            }
                            //登记人
                            cusBase.setInputId(managerId);
                            //登记机构
                            cusBase.setInputBrId(managerBrId);
                            //登记日期
                            cusBase.setInputDate(openDay);
                            //管户客户经理
                            cusBase.setManagerId(managerId);
                            //主管机构
                            cusBase.setManagerBrId(managerBrId);
                            // 客户状态 1暂存
                            cusBase.setCusState(CmisCusConstants.STD_CUS_STATE_1);
                            //客户大类 2 个人
                            cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PRI);
                            cusBaseMapper.insert(cusBase);
                            //更新cusIndiv表
                            BeanUtils.copyProperties(xdkh0019DataReqDto, cusIndiv);
                            //cusIndiv.setCusType(cn.com.yusys.yusp.commons.util.StringUtils.EMPTY);//接口cusType字段传过来的是客户分类，此处置空
                            cusIndiv.setCusId(ecifCusId);//客户号
                            cusIndiv.setCusName(xdkh0019DataReqDto.getCusName());//客户名称
                            cusIndiv.setInputId(managerId);//登记人
                            cusIndiv.setInputBrId(managerBrId);//登记机构
                            cusIndiv.setInputDate(openDay);//登记日期
                            cusIndiv.setCusType("110");//客户类型：110-一般自然人 120-个体工商户(无字号)
                            cusIndivMapper.insert(cusIndiv);
                            xdkh0019DataRespDto.setCusId(ecifCusId);
                        }
                    }
                    xdkh0019DataRespDto.setIsNew(CmisCommonConstants.YES_NO_1);
                } else {
                    //是否新开户
                    xdkh0019DataRespDto.setIsNew(CmisCommonConstants.YES_NO_0);
                    //客户号
                    xdkh0019DataRespDto.setCusId(cusBaseInfo.getCusId());
                }
            } else if (Objects.equals(opType, CmisCommonConstants.OP_TYPE_02)) {
                String managerId = xdkh0019DataReqDto.getManagerId();
                String managerBrId = xdkh0019DataReqDto.getManagerBrId();
                if (Objects.equals("xwd00001", cusBaseInfo.getManagerId())) {
                    cusBaseInfo.setManagerId(managerId);
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (Objects.nonNull(adminSmUserDto)) {
                        managerBrId = adminSmUserDto.getOrgId();
                    }
                    cusBaseInfo.setManagerId(managerId);
                    cusBaseInfo.setManagerBrId(managerBrId);
                    cusBaseInfo.setInputId(managerId);
                    cusBaseInfo.setInputBrId(managerBrId);
                    cusBaseMapper.updateByPrimaryKey(cusBaseInfo);
                    cusIndiv = cusIndivMapper.selectByPrimaryKey(cusBaseInfo.getCusId());
                    if (cusIndiv == null) {
                        BeanUtils.copyProperties(xdkh0019DataReqDto, cusIndiv);
                        cusIndiv.setCusId(ecifCusId);//客户号
                        cusIndiv.setCusName(xdkh0019DataReqDto.getCusName());//客户名称
                        cusIndiv.setInputId(managerId);//登记人
                        cusIndiv.setInputBrId(managerBrId);//登记机构
                        cusIndiv.setInputDate(openDay);//登记日期
                        cusIndiv.setCusType(xdkh0019DataReqDto.getCusType());
                        cusIndivMapper.insert(cusIndiv);
                    } else {
                        cusIndiv.setInputId(managerId);//登记人
                        cusIndiv.setInputBrId(managerBrId);//登记机构
                        cusIndivMapper.updateByPrimaryKey(cusIndiv);
                    }
                }
                //是否新开户
                xdkh0019DataRespDto.setIsNew(CmisCommonConstants.YES_NO_0);
                //客户号
                xdkh0019DataRespDto.setCusId(cusBaseInfo.getCusId());
            } else {
                //操作类型异常
                throw BizException.error(null, EcsEnum.ECS040015.key, EcsEnum.ECS040015.value);
            }
            //根据证件号查询管护经理号
            CusBase cusBaseInfo1 = cusBaseMapper.queryCusInfoByCertCode(xdkh0019DataReqDto.getCertNo());
            if (Objects.nonNull(cusBaseInfo1)) {
                xdkh0019DataRespDto.setManagerId(Optional.ofNullable(cusBaseInfo1.getManagerId()).orElse(Strings.EMPTY));
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (YuspException e) {
            logger.info(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
            throw new YuspException(e.getCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, JSON.toJSONString(xdkh0019DataRespDto));
        return xdkh0019DataRespDto;
    }
}
