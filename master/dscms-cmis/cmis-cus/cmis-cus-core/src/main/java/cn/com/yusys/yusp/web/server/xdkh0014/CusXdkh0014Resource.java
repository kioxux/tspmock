package cn.com.yusys.yusp.web.server.xdkh0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdkh0014.req.Xdkh0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0014.resp.Xdkh0014DataRespDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0014.Xdkh0014Service;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 接口处理类:省心E付白名单信息维护
 *
 * @author code-generator
 * @version 1.0
 */
@RestController
@RequestMapping("/api/cus4bsp")
@RefreshScope
public class CusXdkh0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0014Resource.class);

    @Autowired
    private Xdkh0014Service xdkh0014Service;

    /**
     * 交易码：xdkh0014
     * 交易描述：省心E付白名单信息维护
     *
     * @param xdkh0014DataReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdkh0014")
    protected @ResponseBody
    ResultDto<Xdkh0014DataRespDto> xdkh0014(@Validated @RequestBody Xdkh0014DataReqDto xdkh0014DataReqDto) throws Exception {
        Xdkh0014DataRespDto xdkh0014DataRespDto = new Xdkh0014DataRespDto();// 响应Dto:省心E付白名单信息维护
        ResultDto<Xdkh0014DataRespDto> xdkh0014DataResultDto = new ResultDto<>();
        // 从xdkh0014DataReqDto获取业务值进行业务逻辑处理
        String flag = xdkh0014DataReqDto.getFlag();//标识
        String serno = xdkh0014DataReqDto.getSerno();//流水号
        String cusId = xdkh0014DataReqDto.getCusId();//客户号
        String cusName = xdkh0014DataReqDto.getCusName();//客户名称
        String acctNo = xdkh0014DataReqDto.getAcctNo();//账号
        String acctName = xdkh0014DataReqDto.getAcctName();//户名
        String acctsvcr = xdkh0014DataReqDto.getAcctsvcr();//开户行
        String acctsvcrName = xdkh0014DataReqDto.getAcctsvcrName();//开户行名
        String isBankFlag = xdkh0014DataReqDto.getIsBankFlag();//行内外标识
        String loanUseType = xdkh0014DataReqDto.getLoanUseType();//贷款用途
        String fileUpload = xdkh0014DataReqDto.getFileUpload();//附件
        String startPageNum = xdkh0014DataReqDto.getStartPageNum();//起始页数
        String pageSize = xdkh0014DataReqDto.getPageSize();//分页大小
        try {
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xdkh0014DataRespDto对象开始
            //响应Data:省心E付白名单信息维护
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
            xdkh0014DataRespDto.setTotalQnt(StringUtils.EMPTY);// 总数
            cn.com.yusys.yusp.dto.server.xdkh0014.resp.List list = new cn.com.yusys.yusp.dto.server.xdkh0014.resp.List();
            list.setAcctName(StringUtils.EMPTY);
            list.setAcctNo(StringUtils.EMPTY);
            list.setAcctsvcr(StringUtils.EMPTY);
            list.setAcctsvcrName(StringUtils.EMPTY);
            list.setCusId(StringUtils.EMPTY);
            list.setCusName(StringUtils.EMPTY);
            list.setFileUpload(StringUtils.EMPTY);
            list.setIsBankFlag(StringUtils.EMPTY);
            list.setLoanUseType(StringUtils.EMPTY);
            list.setSerno(StringUtils.EMPTY);
            list.setStatus(StringUtils.EMPTY);
            xdkh0014DataRespDto.setList(Arrays.asList(list));// list

            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            // 封装xdkh0014DataRespDto对象结束
            // 封装xdkh0014DataResultDto中正确的返回码和返回信息
            xdkh0014DataResultDto.setCode("00000");
            xdkh0014DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            // 封装xdkh0014DataResultDto中异常返回码和返回信息
            xdkh0014DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0014DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0014DataRespDto到xdkh0014DataResultDto中
        xdkh0014DataResultDto.setData(xdkh0014DataRespDto);
        return xdkh0014DataResultDto;
    }
}
