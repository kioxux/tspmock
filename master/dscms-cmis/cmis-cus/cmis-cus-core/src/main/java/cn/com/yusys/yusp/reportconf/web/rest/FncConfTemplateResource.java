package cn.com.yusys.yusp.reportconf.web.rest;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.web.rest.CommonResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.reportconf.domain.FncConfTemplate;
import cn.com.yusys.yusp.reportconf.service.FncConfTemplateService;
//import cn.com.yusys.yusp.trace.annotation.TraceBaggage;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.service.CommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfTemplateResource
 * @类描述: 资源类
 * @功能描述: 财务报表模板配置
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 14:37:37
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/nrcs-cms/fncconftemplate")
@Api(tags = "FncConfTemplateResource", description = "财务报表模板配置")
public class FncConfTemplateResource extends CommonResource<FncConfTemplate, String> {
	@Autowired
	private FncConfTemplateService fncConfTemplateService;

	@Override
	protected CommonService getCommonService() {
		return fncConfTemplateService;
	}

	/**
	 * 财务报表模板配置查询
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/q/fncconftemplate/list")
	@ApiOperation("NRCS603261财务报表模板配置查询")
//	@TraceBaggage(functionCode = "NRCS603261")
	public ResultDto<List<FncConfTemplate>> queryFncConfTemplateList(QueryModel model) {
		return new ResultDto<List<FncConfTemplate>>(fncConfTemplateService.queryFncConfTemplateList(model));
	}

	/**
	 * 财务报表模板配置全量查询
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/q/fncconftemplate/all/list")
	@ApiOperation("NRCS603311财务报表模板配置全量查询")
//	@TraceBaggage(functionCode = "NRCS603311")
	public ResultDto<List<FncConfTemplate>> queryFncConfTemplateListAll(QueryModel model) {
		return new ResultDto<List<FncConfTemplate>>(fncConfTemplateService.queryFncConfTemplateListAll(model));
	}

	/**
	 * 财务报表模板新增
	 * 
	 * @param fncConfTemplate
	 * @return
	 */
	@PostMapping("/s/fncconftemplate/add")
	@ApiOperation("NRCS603323财务报表模板新增")
//	@TraceBaggage(functionCode = "NRCS603323")
	public ResultDto<Integer> addFncConfTemplate(@RequestBody FncConfTemplate fncConfTemplate) {
		return new ResultDto<Integer>(fncConfTemplateService.addFncConfTemplate(fncConfTemplate));
	}

	/**
	 * 财务报表模板删除
	 * 
	 * @param map
	 * @return
	 */
	@PostMapping("/s/fncconftemplate/delete")
	@ApiOperation("NRCS603344财务报表模板删除")
//	@TraceBaggage(functionCode = "NRCS603344")
	public ResultDto<Integer> deleteFncConfTemplate(@RequestBody HashMap<String, String> map) {
		return new ResultDto<Integer>(fncConfTemplateService.deleteFncConfTemplate(map.get("fncId")));
	}

	/**
	 * 财务报表模板修改
	 * 
	 * @param fncConfTemplate
	 * @return
	 */
	@PostMapping("/s/fncconftemplate/update")
	@ApiOperation("NRCS603335财务报表模板修改")
//	@TraceBaggage(functionCode = "NRCS603335")
	public ResultDto<Integer> updateFncConfTemplate(@RequestBody FncConfTemplate fncConfTemplate) {
		return new ResultDto<Integer>(fncConfTemplateService.updateFncConfTemplate(fncConfTemplate));
	}

	/**
	 * 财务报表模板配置条件查询
	 * 
	 * @param fncId
	 * @return
	 */
	@GetMapping("/q/fncconftemplate/detail")
	@ApiOperation("NRCS603367财务报表模板配置条件查询")
//	@TraceBaggage(functionCode = "NRCS603367")
	public ResultDto<FncConfTemplate> queryFncConfTemplateListByFncid(@RequestParam String fncId) {
		return new ResultDto<FncConfTemplate>(fncConfTemplateService.queryFncConfTemplateByKey(fncId));
	}

	/**
	 * 财务报表模板查看 暂时不使用此方法
	 * 
	 * @param fncId
	 *            主键
	 * @return
	 */
	/*
	 * @GetMapping("/q/fncconftemplate/detail")
	 * 
	 * @ApiOperation("NRCS603352财务报表模板查看")
	 * 
	 * @TraceBaggage(functionCode = "NRCS603352") public ResultDto<FncConfTemplate>
	 * queryFncConfTemplateByKey(@RequestBody HashMap<String, String> map) { return
	 * new
	 * ResultDto<FncConfTemplate>(fncConfTemplateService.queryFncConfTemplateByKey(
	 * map.get("fncId"))); }
	 */
}
