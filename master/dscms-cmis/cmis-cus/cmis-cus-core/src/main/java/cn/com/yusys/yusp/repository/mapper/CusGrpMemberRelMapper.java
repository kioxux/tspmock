/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.CusGrpMemberRelDto;
import cn.com.yusys.yusp.dto.server.xdkh0008.resp.GroupMapList;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpMemberRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-10 10:46:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusGrpMemberRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusGrpMemberRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusGrpMemberRel> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusGrpMemberRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusGrpMemberRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusGrpMemberRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusGrpMemberRel record);


    /**
     * 根据集团编号+成员客户编号 删除数据
     * @方法名称: updateSelectiveByGrpnoAndCusid
     * @方法描述: 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByGrpnoAndCusid(CusGrpMemberRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: updateByContinionAvai
     * @方法描述: 根据产生条件，更新成的状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByContinionAvai(Map paramMap);


    /**
     * @方法名称: queryCusGrpMemberRelDtoList
     * @方法描述: 根据集团客户编号查询成员列表信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-15 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<CusGrpMemberRelDto> queryCusGrpMemberRelDtoList(String grpNo);

    /**
     * 根据客户编号查询集团信息
     * @param cusId
     * @return
     */
    CusGrpMemberRel getGruopInfoByCusId(@Param("cusId")String cusId);

    /**
     * 根据集团编号查询集团成员列表
     * @param grpNo
     * @return
     */
    List<GroupMapList> getGroupList(@Param("grpNo")String grpNo);

    /**
     * 根据queryModel查询集团编号
     * @param queryModel
     * @return
     */
    Map<String,String> queryGrpNoByQueryModel(QueryModel queryModel);

    /**
     * 根据集团编号查询集团成员列表信息
     * @param grpNo
     * @return
     */
    List<Map<String,String>> queryGrpMemberListByGrpNo(@Param("grpNo")String grpNo);

    /**
     * @方法名称: queryGrpNoByParams
     * @方法描述: 根据成员客户编号查询集团客户编号
     * @参数与返回说明:
     * @算法描述:
     * @创建人: qw
     * @创建时间: 2021-07-23 20:11:11
     * @修改记录: 修改时间    修改人员    修改原因
     */
    CusGrpMemberRel queryGrpNoByParams(Map map);

    /**
     * 根据集团编号查询成员客户编号
     * @param grpNo
     * @return
     */
    String queryGrpMemberCusIdByGrpNo(@Param("grpNo") String grpNo);
}