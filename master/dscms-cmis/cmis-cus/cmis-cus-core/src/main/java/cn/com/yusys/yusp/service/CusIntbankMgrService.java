/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusCorpMgr;
import cn.com.yusys.yusp.repository.mapper.CusCorpMgrMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusIntbankMgr;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMgrMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIntbankMgrService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-25 17:50:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusIntbankMgrService {

    @Autowired
    private CusIntbankMgrMapper cusIntbankMgrMapper;

    @Autowired
    private CusCorpMgrMapper cusCorpMgrMapper;

    @Autowired
    private CusBaseService cusBaseService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusIntbankMgr selectByPrimaryKey(String pkId) {
        return cusIntbankMgrMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusIntbankMgr> selectAll(QueryModel model) {
        List<CusIntbankMgr> records = (List<CusIntbankMgr>) cusIntbankMgrMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusIntbankMgr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIntbankMgr> list = cusIntbankMgrMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(rollbackFor = Exception.class)
    public int insert(CusIntbankMgr record) {
        CusCorpMgr cusCorpMgr = new CusCorpMgr();
        try {
            // 同业客户高管类型为实际控制人类型不同步对公高管信息表
            if (!"02".equals(record.getMrgCls())) {
                BeanUtils.copyProperties(record, cusCorpMgr);
                cusCorpMgr.setCusIdRel(record.getIntbankCusId());
                CusBase cusBase = cusBaseService.queryCusInfoByCertCode(record.getMrgCertCode());
                if (cusBase != null) {
                    cusCorpMgr.setCusId(cusBase.getCusId());
                    cusCorpMgr.setMrgType(convertType(record.getMrgCls()));
                    cusCorpMgrMapper.insert(cusCorpMgr);
                }
            }
        } catch (Exception e) {
            //若是出现异常则需要回滚，因此直接抛出异常
            throw new YuspException("9999", e.getMessage());
        }
        return cusIntbankMgrMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusIntbankMgr record) {
        return cusIntbankMgrMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusIntbankMgr record) {
        return cusIntbankMgrMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusIntbankMgr record) {
        return cusIntbankMgrMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        CusIntbankMgr cusIntbankMgr = selectByPrimaryKey(pkId);
        if (cusIntbankMgr != null) {
            QueryModel model = new QueryModel();
            model.getCondition().put("cusIdRel", cusIntbankMgr.getIntbankCusId());
            List<CusCorpMgr> list = cusCorpMgrMapper.selectByModel(model);
            if (list.size() > 0) {
                for (CusCorpMgr cusCorpMgr : list) {
                    cusCorpMgrMapper.deleteByPrimaryKey(cusCorpMgr.getPkId());
                }
            }
        }
        return cusIntbankMgrMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusIntbankMgrMapper.deleteByIds(ids);
    }

    public String convertType(String mrgCls) {
        if ("01".equals(mrgCls)) {
            mrgCls = "200400";
        }
        if ("02".equals(mrgCls)) {
            mrgCls = "201200";
        }
        if ("03".equals(mrgCls)) {
            mrgCls = "200800";
        }
        if ("04".equals(mrgCls)) {
            mrgCls = "201300";
        }
        return mrgCls;
    }
}
