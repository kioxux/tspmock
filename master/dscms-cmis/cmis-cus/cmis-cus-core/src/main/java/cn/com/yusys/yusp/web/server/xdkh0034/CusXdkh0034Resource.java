package cn.com.yusys.yusp.web.server.xdkh0034;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0034.req.Xdkh0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0034.resp.Xdkh0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0034.Xdkh0034Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询企业在我行客户评级信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0034:公司客户评级相关信息同步")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0034Resource.class);

    @Autowired
    private Xdkh0034Service xdkh0034Service;

    /**
     * 交易码：xdkh0034
     * 交易描述：查询企业在我行客户评级信息
     *
     * @param xdkh0034DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询企业在我行客户评级信息")
    @PostMapping("/xdkh0034")
    protected @ResponseBody
    ResultDto<Xdkh0034DataRespDto> xdkh0034(@Validated @RequestBody Xdkh0034DataReqDto xdkh0034DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, JSON.toJSONString(xdkh0034DataReqDto));
        Xdkh0034DataRespDto xdkh0034DataRespDto = new Xdkh0034DataRespDto();// 响应Dto:公司客户评级相关信息同步
        ResultDto<Xdkh0034DataRespDto> xdkh0034DataResultDto = new ResultDto<>();
        try {
            // 从xdkh0034DataReqDto获取业务值进行业务逻辑处理
            String cusId = xdkh0034DataReqDto.getCusId();//客户编号
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, JSON.toJSONString(xdkh0034DataReqDto));
            xdkh0034DataRespDto = xdkh0034Service.xdkh0034(xdkh0034DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0034DataRespDto));
            // 封装xdkh0034DataResultDto中正确的返回码和返回信息
            xdkh0034DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0034DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, e.getMessage());
            // 封装xdkh0034DataResultDto中异常返回码和返回信息
            xdkh0034DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0034DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0034DataRespDto到xdkh0034DataResultDto中
        xdkh0034DataResultDto.setData(xdkh0034DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, JSON.toJSONString(xdkh0034DataResultDto));
        return xdkh0034DataResultDto;
    }
}
