/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * 优农贷名单导入模板
 */
@ExcelCsv(namePrefix = "优农贷名单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CusLstYndVo {

    /** 客户编号 **/
    @ExcelField(title = "客户编号", viewLength = 40)
    private String cusId;

    /** 客户姓名 **/
    @ExcelField(title = "客户姓名", viewLength = 80)
    private String cusName;

    /** 身份证号码 **/
    @ExcelField(title = "身份证号码", viewLength = 40)
    private String idcardNo;

    /** 手机号码 **/
    @ExcelField(title = "手机号码", viewLength = 20)
    private String mobileNo;

    /** 性别 **/
    @ExcelField(title = "性别", viewLength = 5)
    private String sex;

    /** 学历 **/
    @ExcelField(title = "学历", viewLength = 5)
    private String edu;

    /** 有无子女 **/
    @ExcelField(title = "有无子女", viewLength = 5)
    private String isHaveChildren;

    /** 婚姻状况 **/
    @ExcelField(title = "婚姻状况", viewLength = 5)
    private String marStatus;

    /** 管户客户经理 **/
    @ExcelField(title = "管户客户经理", viewLength = 20)
    private String managerId;

    /** 所属机构 **/
    @ExcelField(title = "所属机构", viewLength = 20)
    private String belgOrg;

    /** 入库日期 **/
    @ExcelField(title = "入库日期", viewLength = 20)
    private String storageDate;

    /** 经营地址 **/
    @ExcelField(title = "经营地址", viewLength = 250)
    private String operAddr;

    /** 经营年限 **/
    @ExcelField(title = "经营年限", viewLength = 20)
    private String operLmt;

    /** 推荐机构 **/
    @ExcelField(title = "推荐机构", viewLength = 20)
    private String recommendOrg;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getIdcardNo() {
        return idcardNo;
    }

    public void setIdcardNo(String idcardNo) {
        this.idcardNo = idcardNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getIsHaveChildren() {
        return isHaveChildren;
    }

    public void setIsHaveChildren(String isHaveChildren) {
        this.isHaveChildren = isHaveChildren;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getBelgOrg() {
        return belgOrg;
    }

    public void setBelgOrg(String belgOrg) {
        this.belgOrg = belgOrg;
    }

    public String getStorageDate() {
        return storageDate;
    }

    public void setStorageDate(String storageDate) {
        this.storageDate = storageDate;
    }

    public String getOperAddr() {
        return operAddr;
    }

    public void setOperAddr(String operAddr) {
        this.operAddr = operAddr;
    }

    public String getOperLmt() {
        return operLmt;
    }

    public void setOperLmt(String operLmt) {
        this.operLmt = operLmt;
    }

    public String getRecommendOrg() {
        return recommendOrg;
    }

    public void setRecommendOrg(String recommendOrg) {
        this.recommendOrg = recommendOrg;
    }
}