package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.domain.CusIndivContact;

public class IndivCusSaveDto {
    //客户基本信息
    private CusBase cusBase;

    //个人客户基本信息
    private CusIndiv cusIndiv;

    //客户联系信息
    private CusIndivContact cusIndivContact;

    public CusBase getCusBase() {
        return cusBase;
    }

    public void setCusBase(CusBase cusBase) {
        this.cusBase = cusBase;
    }

    public CusIndiv getCusIndiv() {
        return cusIndiv;
    }

    public void setCusIndiv(CusIndiv cusIndiv) {
        this.cusIndiv = cusIndiv;
    }

    public CusIndivContact getCusIndivContact() {
        return cusIndivContact;
    }

    public void setCusIndivContact(CusIndivContact cusIndivContact) {
        this.cusIndivContact = cusIndivContact;
    }
}
