/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWhbxdApp
 * @类描述: cus_lst_whbxd_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:33:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_whbxd_app")
public class CusLstWhbxdApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "LWA_SERNO")
	private String lwaSerno;
	
	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 10)
	private String appDate;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户规模 **/
	@Column(name = "CUS_SCALE", unique = false, nullable = true, length = 40)
	private String cusScale;
	
	/** 是否优良信贷客户 **/
	@Column(name = "IS_FINE_CRET_CUS", unique = false, nullable = true, length = 10)
	private String isFineCretCus;
	
	/** 授信金额 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 内部评级 **/
	@Column(name = "INNER_EVAL", unique = false, nullable = true, length = 10)
	private String innerEval;
	
	/** 管户客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 所属机构 **/
	@Column(name = "BELG_ORG", unique = false, nullable = true, length = 40)
	private String belgOrg;
	
	/** 名单归属 **/
	@Column(name = "LIST_BELG", unique = false, nullable = true, length = 40)
	private String listBelg;
	
	/** 申请说明 **/
	@Column(name = "APP_EXPL", unique = false, nullable = true, length = 255)
	private String appExpl;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date updateTime;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = false, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = false, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = false, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = false, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = false, length = 20)
	private String updDate;
	
	
	/**
	 * @param lwaSerno
	 */
	public void setLwaSerno(String lwaSerno) {
		this.lwaSerno = lwaSerno;
	}
	
    /**
     * @return lwaSerno
     */
	public String getLwaSerno() {
		return this.lwaSerno;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusScale
	 */
	public void setCusScale(String cusScale) {
		this.cusScale = cusScale;
	}
	
    /**
     * @return cusScale
     */
	public String getCusScale() {
		return this.cusScale;
	}
	
	/**
	 * @param isFineCretCus
	 */
	public void setIsFineCretCus(String isFineCretCus) {
		this.isFineCretCus = isFineCretCus;
	}
	
    /**
     * @return isFineCretCus
     */
	public String getIsFineCretCus() {
		return this.isFineCretCus;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param innerEval
	 */
	public void setInnerEval(String innerEval) {
		this.innerEval = innerEval;
	}
	
    /**
     * @return innerEval
     */
	public String getInnerEval() {
		return this.innerEval;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg;
	}
	
    /**
     * @return belgOrg
     */
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param listBelg
	 */
	public void setListBelg(String listBelg) {
		this.listBelg = listBelg;
	}
	
    /**
     * @return listBelg
     */
	public String getListBelg() {
		return this.listBelg;
	}
	
	/**
	 * @param appExpl
	 */
	public void setAppExpl(String appExpl) {
		this.appExpl = appExpl;
	}
	
    /**
     * @return appExpl
     */
	public String getAppExpl() {
		return this.appExpl;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}


}