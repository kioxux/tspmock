package cn.com.yusys.yusp.web.server.xdkh0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0013.req.Xdkh0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0013.resp.Xdkh0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0013.Xdkh0013Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * 接口处理类:优享贷客户白名单信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDKH0013:优享贷客户白名单信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0013Resource.class);

    @Autowired
    private Xdkh0013Service xdkh0013Service;

    /**
     * 交易码：xdkh0013
     * 交易描述：优享贷客户白名单信息查询
     *
     * @param xdkh0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优享贷客户白名单信息查询")
    @PostMapping("/xdkh0013")
    protected @ResponseBody
    ResultDto<Xdkh0013DataRespDto> xdkh0013(@Validated @RequestBody Xdkh0013DataReqDto xdkh0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(xdkh0013DataReqDto));
        Xdkh0013DataRespDto xdkh0013DataRespDto = new Xdkh0013DataRespDto();// 响应Dto:优享贷客户白名单信息查询
        ResultDto<Xdkh0013DataRespDto> xdkh0013DataResultDto = new ResultDto<>();
        // 从xdkh0013DataReqDto获取业务值进行业务逻辑处理
        String certNo = xdkh0013DataReqDto.getCertNo();//证件号码
        try {

            // 调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(certNo));
            xdkh0013DataRespDto = Optional.ofNullable(xdkh0013Service.getWhiteListByCertNo(certNo))
                    .orElse(new Xdkh0013DataRespDto(new BigDecimal("0"), new BigDecimal("0"), StringUtils.EMPTY));
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(xdkh0013DataRespDto));
            // 封装xdkh0013DataResultDto中正确的返回码和返回信息
            xdkh0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0013DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, e.getMessage());
            // 封装xdkh0013DataResultDto中异常返回码和返回信息
            xdkh0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0013DataRespDto到xdkh0013DataResultDto中
        xdkh0013DataResultDto.setData(xdkh0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(xdkh0013DataResultDto));
        return xdkh0013DataResultDto;
    }
}
