/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.dto.CusRelationsDto;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusClientHandleMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 15430
 * @创建时间: 2020-11-14 09:59:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Repository
public interface CusClientHandleMapper {

    /**
     * @方法名称: deleteCusIndivOtherAsset
     * @方法描述: 通过业务申请流水号对个人其他资产进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteCusIndivOtherAsset(Map param);

    /**
     * @方法名称: deleteCusIndivCarInfo
     * @方法描述: 通过业务申请流水号对个人车产信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteCusIndivCarInfo(Map param);

    /**
     * @方法名称: deleteCusIndivCarInfo
     * @方法描述: 通过业务申请流水号对个人家庭负债信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteCusIndivFamilyDebt(Map param);

    /**
     * @方法名称: deleteCusIndivFearn
     * @方法描述: 通过业务申请流水号对个人家庭收入信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteCusIndivFearn(Map param);

    /**
     * @方法名称: deleteCusIndivFinAsset
     * @方法描述: 通过业务申请流水号对个人金融资产信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteCusIndivFinAsset(Map param);

    /**
     * @方法名称: deleteCusIndivFpayout
     * @方法描述: 通过业务申请流水号对个人家庭支出信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteCusIndivFpayout(Map param);

    /**
     * @方法名称: deleteCusIndivOperCom
     * @方法描述: 通过业务申请流水号对企业经营信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteCusIndivOperCom(Map param);

    /**
     * @方法名称: deleteCusIndivHouseInfo
     * @方法描述: 通过业务申请流水号对个人房产信息进行逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    int deleteCusIndivHouseInfo(Map param);

}