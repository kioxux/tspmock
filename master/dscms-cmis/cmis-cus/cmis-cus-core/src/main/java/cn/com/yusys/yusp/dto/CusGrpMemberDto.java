package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 关联集团信息
 */
public class CusGrpMemberDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //上一级客户ID
    private String upperCusId;
    private List<CusGrpMemberDto> cusCorpRelationsDtos;

    public String getUpperCusId() {
        return upperCusId;
    }

    public void setUpperCusId(String upperCusId) {
        this.upperCusId = upperCusId;
    }

    public List<CusGrpMemberDto> getCusCorpRelationsDtos() {
        return cusCorpRelationsDtos;
    }

    public void setCusCorpRelationsDtos(List<CusGrpMemberDto> cusCorpRelationsDtos) {
        this.cusCorpRelationsDtos = cusCorpRelationsDtos;
    }
}
