package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.domain.CusIndiv;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;


public class IndivCusCreateDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 客户简称 **/
    private String cusShortName;

    /** 客户类型 **/
    private String cusType;

    /** 证件类型 STD_ZB_CERT_TYP **/
    private String certType;

    /** 证件号码 **/
    private String certCode;

    /** 开户日期 **/
    private String openDate;

    /** 开户类型 STD_ZB_OPEN_TYP **/
    private String openType;

    /** 客户大类 **/
    private String cusCatalog;

    /** 所属条线 STD_ZB_BIZ_BELG **/
    private String belgLine;

    /** 信用等级 STD_ZB_CREDIT_GRADE **/
    private String cusCrdGrade;

    /** 信用评定到期日期 **/
    private String cusCrdDt;

    /** 客户状态 STD_ZB_CUS_ST **/
    private String cusState;

    /** 主管机构 **/
    private String mainBrId;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 更新人 **/
    private String updId;

    /** 更新机构 **/
    private String updBrId;

    /** 更新日期 **/
    private String updDate;

    /** 操作类型  STD_ZB_OPR_TYPE **/
    private String oprType;

    /** 国籍 **/
    private String nation;

    /** 性别 STD_ZB_SEX **/
    private String sex;

    /** 是否长期证件 **/
    private String isLongIndiv;

    /** 证件到期日期 **/
    private String certEndDt;

    /** 职业 STD_ZB_OCC **/
    private String occ;

    /** 手机号码1 **/
    private String mobileA;

    /** 客户经理 **/
    private String managerId;

    /** 是否我行股东**/
    private String isBankSharehd;

    /** 业务类型**/
    private String bizType;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusShortName() {
        return cusShortName;
    }

    public void setCusShortName(String cusShortName) {
        this.cusShortName = cusShortName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getCusCrdGrade() {
        return cusCrdGrade;
    }

    public void setCusCrdGrade(String cusCrdGrade) {
        this.cusCrdGrade = cusCrdGrade;
    }

    public String getCusCrdDt() {
        return cusCrdDt;
    }

    public void setCusCrdDt(String cusCrdDt) {
        this.cusCrdDt = cusCrdDt;
    }

    public String getCusState() {
        return cusState;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    public String getMainBrId() {
        return mainBrId;
    }

    public void setMainBrId(String mainBrId) {
        this.mainBrId = mainBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getsSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIsLongIndiv() {
        return isLongIndiv;
    }

    public void setIsLongIndiv(String isLongIndiv) {
        this.isLongIndiv = isLongIndiv;
    }

    public String getCertEndDt() {
        return certEndDt;
    }

    public void setCertEndDt(String certEndDt) {
        this.certEndDt = certEndDt;
    }

    public String getOcc() {
        return occ;
    }

    public void setOcc(String occ) {
        this.occ = occ;
    }

    public String getMobileA() {
        return mobileA;
    }

    public String getIsBankSharehd() {
        return isBankSharehd;
    }

    public void setMobileA(String mobileA) {
        this.mobileA = mobileA;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public void setIsBankSharehd(String isBankSharehd) {
        this.isBankSharehd = isBankSharehd;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }
}
