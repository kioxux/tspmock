/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusGrp;
import cn.com.yusys.yusp.domain.CusGrpApp;
import cn.com.yusys.yusp.domain.CusGrpMemberApp;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.CusRelationsDto;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusGrpMapper;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberAppMapper;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;

import com.github.pagehelper.PageHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpMemberAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-08 19:54:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusGrpMemberAppService {

    private final Logger log = LoggerFactory.getLogger(CusGrpAppService.class);

    @Autowired
    private CusGrpMemberAppMapper cusGrpMemberAppMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CusBaseService cusBaseService;

    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper;
    @Autowired
    private CusGrpMapper cusGrpMapper;
    
    @Autowired
    private CusIndivMapper cusIndivMapper;
    
    @Autowired
    private CusCorpMapper cusCorpMapper;

    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusGrpMemberApp selectByPrimaryKey(String pkId) {
        return cusGrpMemberAppMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusGrpMemberApp> selectAll(QueryModel model) {
        List<CusGrpMemberApp> records = (List<CusGrpMemberApp>) cusGrpMemberAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusGrpMemberApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusGrpMemberApp> list = cusGrpMemberAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusGrpMemberApp record) {
        return cusGrpMemberAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusGrpMemberApp record) {
        return cusGrpMemberAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusGrpMemberApp record) {
        return cusGrpMemberAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusGrpMemberApp record) {
        return cusGrpMemberAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusGrpMemberAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusGrpMemberAppMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: insertByCusGrpApp
     * @方法描述: 根据集团申请中的信息，获取集团核心客户信息，集团成员信息中自动生成母公司信息
     * @参数与返回说明: record 集团申请实例  返回 true  处理成功
     * @算法描述: 无
     */
    public boolean insertByCusGrpApp(CusGrpApp record) {
        //检验成功，母公司数据插入到子公司列表中
        CusGrpMemberApp memberApp = new CusGrpMemberApp();
        String pkValue = sequenceTemplateClient.getSequenceTemplate("PK_VALUE", new HashMap<>());
        //主键
        memberApp.setPkId(pkValue);
        //申请流水号
        memberApp.setSerno(record.getSerno());
        //集团编号
        memberApp.setGrpNo(record.getGrpNo());
        //获取核心客户号
        String cusId = record.getCoreCusId();
        //根据客户号查询客户信息
        CusRelationsDto cusBase = cusBaseService.findCusBaseByCusId(cusId);
        if (cusBase == null) {
            throw new BizException(null,EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key,null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.value);
        }
        memberApp.setCusId(cusId);
        //核心客户号类型
        //memberApp.setCusType(cusBase.getCusType());
        //modify qiantj 2021-08-05 根据客户大类获取客户类型
        if (CmisCusConstants.CUS_CATALOG_PRI.equals(cusBase.getCusCatalog())) {
        	CusIndiv cusIndiv = cusIndivMapper.selectByPrimaryKey(cusId);
        	if (cusIndiv == null) {
        		log.error("根据客户号【{}】查询不到个人客户信息！", cusId);
                throw new BizException(null,EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key,null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.value);
        	}
        	memberApp.setCusType(cusIndiv.getCusType());
        } else if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusBase.getCusCatalog())) {
        	CusCorp cusCorp = cusCorpMapper.selectByPrimaryKey(cusId);
        	if (cusCorp == null) {
        		log.error("根据客户号【{}】查询不到企业客户信息！", cusId);
                throw new BizException(null,EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key,null,  EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.value);
        	}
        	memberApp.setCusType(cusCorp.getCusType());
        }
        
        //核心客户号名称
        memberApp.setCusName(cusBase.getCusName());
        //集团关系类型
        memberApp.setGrpCorreType(CmisCusConstants.STD_ZB_GRP_CORRE_TYPE_1);
        //集团变更类型  新增默认 01-原有不变
        memberApp.setModifyType(CmisCusConstants.STD_CUS_MODIFY_TYPE_01);
        //主管客户经理
        memberApp.setManagerId(record.getManagerId());
        //主管客户机构
        memberApp.setManagerBrId(record.getManagerBrId());
        //操作类型
        memberApp.setOprType(record.getOprType());
        //登记人
        memberApp.setInputId(record.getInputId());
        //登记机构
        memberApp.setInputBrId(record.getInputBrId());
        //登记日期
        memberApp.setInputDate(record.getInputDate());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createTime = simpleDateFormat.format(new Date());
        try {
            memberApp.setCreateTime(simpleDateFormat.parse(createTime));
        } catch (ParseException e) {
            log.error("创建日期转换失败！", e);
        }
        int resultCont = cusGrpMemberAppMapper.insert(memberApp);
        if (resultCont <= 0) {
            throw new BizException(null,EcsEnum.CUS_GRP_MEMBER_CHECK_DEF.key,null, EcsEnum.CUS_GRP_MEMBER_CHECK_DEF.value);
        }
        return true;
    }

    /**
     * @方法名称: updateCusGrpMemCorreType
     * @方法描述: 集团认定修改进入，如果集团紧密程度发生改变，则更新成员集团中的关联类型
     * 集团紧密程度如果时实体集团，则成员关联类型为：母公司、子公司
     * 集团紧密程度如果时一般关系，则集团关联类型为：核心成员，一般成员
     * @参数与返回说明:
     * @算法描述: 无
     */
    public void updateCusGrpMemCorreType(CusGrpApp cusGrpApp) {
        try {
            //获取核心客户号
            String coreCusId = cusGrpApp.getCoreCusId();
            //获取流水号
            String serno = cusGrpApp.getSerno();
            //获取集团紧密程度
            String grpCloselyDegree = cusGrpApp.getGrpCloselyDegree();
            //根据流水号查询向下的子公司
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", serno);
            queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<CusGrpMemberApp> cusGrpMemberAppList = cusGrpMemberAppMapper.selectByModel(queryModel);
            //核心客户号关联类型
            String coreCusCorreType;
            //普通成员关联类型
            String cusCorreType;
            if (CmisCusConstants.STD_ZB_CLOSELY_DEGRE_01.equals(grpCloselyDegree)) {
                coreCusCorreType = CmisCusConstants.STD_ZB_GRP_CORRE_TYPE_1;
                cusCorreType = CmisCusConstants.STD_ZB_GRP_CORRE_TYPE_2;
            } else {
                coreCusCorreType = CmisCusConstants.STD_ZB_GRP_CORRE_TYPE_3;
                cusCorreType = CmisCusConstants.STD_ZB_GRP_CORRE_TYPE_4;
            }
            //遍历更新关联类型
            for (CusGrpMemberApp cusGrpMemberApp : cusGrpMemberAppList) {
                String cusId = cusGrpMemberApp.getCusId();
                if (coreCusId.equals(cusId)) {
                    cusGrpMemberApp.setGrpCorreType(coreCusCorreType);
                } else {
                    cusGrpMemberApp.setGrpCorreType(cusCorreType);
                }
                cusGrpMemberAppMapper.updateByPrimaryKey(cusGrpMemberApp);
            }
        } catch (Exception e) {
            log.error("集团新增核心客户业务失败！", e);
            throw new YuspException(EcsEnum.CUS_GRP_APP_CHECK_DEF.key, EcsEnum.CUS_GRP_APP_CHECK_DEF.value);
        }
    }

    /**
     * @方法名称: checkCusGrpMemAppState
     * @方法描述: 集团认定新增成员，判断该成员是否存在其他成员修改中，查询状态为 000 待发起  111 审批中  991 拿回 992 打回
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, String> checkCusGrpMemAppState(CusGrpMemberApp cusGrpMemberApp) {
        //校验
        Map rtnData = new HashMap();
        //返回状态码
        String rtnCode = EcsEnum.CUS_GRP_SUCCESS_DEF.key;
        //返回报错信息
        String rtnMsg = EcsEnum.CUS_GRP_SUCCESS_DEF.value;
        try {
            //组装参数
            HashMap<String, String> paramMap = new HashMap<String, String>();
            paramMap.put("cusId", cusGrpMemberApp.getCusId());
            paramMap.put("serno", cusGrpMemberApp.getSerno());
            //查询该客户在途其他申请
            int resultCont = cusGrpMemberAppMapper.selectCusGrpMemAppInWayByParamMap(paramMap);
            //结果大于0 说明存在其他申请，校验不通过
            if (resultCont > 0) {
                throw new YuspException(EcsEnum.CUS_GRP_APP_CHECK_DEF.key, EcsEnum.CUS_GRP_APP_CHECK_DEF.value);
            }

            //校验该客户是否存已存在本次申请中
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno", cusGrpMemberApp.getSerno());
            queryModel.addCondition("cusId", cusGrpMemberApp.getCusId());
            queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            List<CusGrpMemberApp> cusGrpMemberAppList = cusGrpMemberAppMapper.selectByModel(queryModel);
            if (cusGrpMemberAppList.size() > 0) {
                throw new YuspException(EcsEnum.CUS_GRP_MEMBER_APP_CHECK_DEF.key, EcsEnum.CUS_GRP_MEMBER_APP_CHECK_DEF.value);
            }
            //校验正式集团
            QueryModel  queryModelRel= new QueryModel();
            queryModelRel.addCondition("cusId",cusGrpMemberApp.getCusId());
            queryModelRel.addCondition("availableInd","1");
            List<CusGrpMemberRel> list = cusGrpMemberRelService.selectByModel(queryModelRel);
            //结果大于0 说明是其他集团成员，校验不通过
            if (!CollectionUtils.isEmpty(list) && list.size()>0) {
                throw new YuspException(EcsEnum.CUS_GRP_CHECK_DEF.key, EcsEnum.CUS_GRP_CHECK_DEF.value);
            }
        } catch (YuspException e) {
            log.error(this.getClass().getMethods() + "校验成员客户是否存在在途！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }


    /**
     * @方法名称: checkCusGrpMemAppState
     * @方法描述: 1、集团认定新增成员，判断该成员是否存在其他成员修改中，查询状态为 000 待发起  111 审批中  991 拿回 992 打回
     * 2、校验该成员是否已存在正式集团中
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, String> checkCusGrpMemApp(CusGrpMemberApp cusGrpMemberApp) {
        //校验
        Map rtnData = new HashMap();
        //返回状态码
        String rtnCode = EcsEnum.CUS_GRP_SUCCESS_DEF.key;
        //返回报错信息
        String rtnMsg = EcsEnum.CUS_GRP_SUCCESS_DEF.value;
        try {
            //校验在途申请
            rtnData = checkCusGrpMemAppState(cusGrpMemberApp);
            if (!rtnData.get("rtnCode").equals(EcsEnum.CUS_GRP_SUCCESS_DEF.key)) {
                return rtnData;
            }
        } catch (Exception e) {
            log.error(this.getClass().getMethods() + "校验成员客户是否存在在途！", e);
            rtnCode = EcsEnum.CUS_GRP_APP_CHECK_DEF.key;
            rtnMsg = EcsEnum.CUS_GRP_APP_CHECK_DEF.value;
        } finally {
            if (rtnData.size() == 0) {
                rtnData.put("rtnMsg", rtnMsg);
                rtnData.put("rtnCode", rtnCode);
            }
        }
        return rtnData;
    }


    /**
     * @方法名称: updateCusGrpMemAppChgType
     * @方法描述: 更新集团成员中的变更类型
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, String> updateCusGrpMemAppChgType(Map map) {
        //校验
        Map rtnData = new HashMap();
        //返回状态码
        String rtnCode = EcsEnum.CUS_GRP_SUCCESS_DEF.key;
        //返回报错信息
        String rtnMsg = EcsEnum.CUS_GRP_SUCCESS_DEF.value;
        try {
            //主键
            String pkId = (String) map.get("pkId");
            //变更类型
            String chgType = (String) map.get("chgType");
            //根据主键获取对应申请成员信息
            CusGrpMemberApp cusGrpMemberApp = cusGrpMemberAppMapper.selectByPrimaryKey(pkId);
            cusGrpMemberApp.setModifyType(chgType);
            int resultCount = cusGrpMemberAppMapper.updateByPrimaryKey(cusGrpMemberApp);
            if (resultCount <= 0) {
                throw new YuspException(EcsEnum.CUS_GRP_SQL_UPDATE_DEF.key, EcsEnum.CUS_GRP_SQL_UPDATE_DEF.value);
            }
        } catch (YuspException e) {
            log.error(this.getClass().getMethods() + "更新成员变更类型失败！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } finally {
            if (rtnData.size() == 0) {
                rtnData.put("rtnMsg", rtnMsg);
                rtnData.put("rtnCode", rtnCode);
            }
        }
        return rtnData;
    }

    /**
     * @函数名称:getMemberFromRel
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:liqiang
     */
    @Transactional
    public void getMemberFromRel(Map map) {
        String grpNo = (String) map.get("grpNo");
        String serno = (String) map.get("serno");
        CusGrp cusGrp = cusGrpMapper.selectByPrimaryKey(grpNo);
        List<CusGrpMemberRel> listCusGrpMemberRel = new ArrayList<CusGrpMemberRel>();
        if(cusGrp!=null){
            QueryModel model = new QueryModel();
            //根据集团客户编号与是否有效标识查询成员信息
            model.addCondition("grpNo",grpNo);
            model.addCondition("availableInd",CmisCusConstants.STD_AVAILABLE_IND_1);
            listCusGrpMemberRel = cusGrpMemberRelMapper.selectByModel(model);
            if(listCusGrpMemberRel != null){
                for (int i = 0 ; i<listCusGrpMemberRel.size(); i++){
                    CusGrpMemberApp cusGrpMemberApp = new CusGrpMemberApp();
                    BeanUtils.beanCopy(listCusGrpMemberRel.get(i),cusGrpMemberApp);
                    cusGrpMemberApp.setPkId(null);
                    cusGrpMemberApp.setSerno(serno);
                    cusGrpMemberApp.setModifyType("01");
                    cusGrpMemberApp.setOprType("01");
                    cusGrpMemberAppMapper.insert(cusGrpMemberApp);
                }
            }
        }
    }
}
