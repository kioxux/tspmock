package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusManaTask;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * KHGL02--客户财报修改流程（季报年报）
 * @author liucheng
 */
@Service
public class KHGL02BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(KHGL02BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CusManaTaskService cusManaTaskService;

    @Autowired
    private CusCorpService cusCorpService;

    @Autowired
    private CusBaseService cusBaseService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private MessageSendService messageSendService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String flowBizType = resultInstanceDto.getBizType();
        log.info("后业务处理类型" + currentOpType);
        try {
            CusManaTask cusManaTask = cusManaTaskService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
//                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
//                cusManaTask.setTaskStatus("2");
//                cusManaTaskService.update(cusManaTask);
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程发起操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                cusManaTask.setTaskStatus("2");
                cusManaTaskService.update(cusManaTask);
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程提交操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                cusManaTask.setTaskStatus("2");
                cusManaTaskService.update(cusManaTask);
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程跳转操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                // 1、更新base表
                CusBase record = new CusBase();
                record.setCusId(cusManaTask.getCusId());
                record.setCusState("2"); //生效
                if("KH005".equals(flowBizType)){ // 公司客户转正（集中作业） 更新公司用户数据及任务表状态
                    record.setCusRankCls("01"); // 正式客户
                }
                cusBaseService.updateSelective(record);
                // 2、更新任务表
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                cusManaTask.setTaskStatus("3");
                cusManaTaskService.update(cusManaTask);
                //3、新增档案任务
                CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                centralFileTaskdto.setSerno(cusManaTask.getSerno());
                centralFileTaskdto.setCusId(cusManaTask.getCusId());
                centralFileTaskdto.setCusName(cusManaTask.getCusName());
                centralFileTaskdto.setBizType(flowBizType); // 客户资料
                centralFileTaskdto.setInputId(cusManaTask.getInputId());
                centralFileTaskdto.setInputBrId(cusManaTask.getInputBrId());
                centralFileTaskdto.setOptType("01"); // 纯指令
                centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                centralFileTaskdto.setTaskType("01"); // 档案接收
                centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                cmisBizClientService.createCentralFileTask(centralFileTaskdto);

                CusBase cusBase = cusBaseService.selectByPrimaryKey(cusManaTask.getCusId());
                MessageSendDto messageSendDto = new MessageSendDto();
                Map<String, String> map = new HashMap<>();
                map.put("cusId", cusBase.getCusName());
                map.put("flowName", "客户季报年报修改（集中作业）");
                map.put("result", "通过");
                List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                receivedUserDto.setReceivedUserType("1");// 发送人员类型
                receivedUserDto.setUserId(cusBase.getManagerId());// 客户经理工号
                receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                receivedUserList.add(receivedUserDto);
                messageSendDto.setMessageType("MSG_KH_M_0003");
                messageSendDto.setParams(map);
                messageSendDto.setReceivedUserList(receivedUserList);
                messageSendService.sendMessage(messageSendDto);
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程结束操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                CusBase cusBase = cusBaseService.selectByPrimaryKey(cusManaTask.getCusId());
                MessageSendDto messageSendDto = new MessageSendDto();
                Map<String, String> map = new HashMap<>();
                map.put("cusId", cusBase.getCusName());
                map.put("flowName", "客户季报年报修改（集中作业）");
                map.put("result", "退回");
                List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                receivedUserDto.setReceivedUserType("1");// 发送人员类型
                receivedUserDto.setUserId(cusBase.getManagerId());// 客户经理工号
                receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                receivedUserList.add(receivedUserDto);
                messageSendDto.setMessageType("MSG_KH_M_0003");
                messageSendDto.setParams(map);
                messageSendDto.setReceivedUserList(receivedUserList);
                messageSendService.sendMessage(messageSendDto);
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程退回操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 判断是否打回发起人节点
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                    cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    cusManaTaskService.update(cusManaTask);
                }
                CusBase cusBase = cusBaseService.selectByPrimaryKey(cusManaTask.getCusId());
                MessageSendDto messageSendDto = new MessageSendDto();
                Map<String, String> map = new HashMap<>();
                map.put("cusId", cusBase.getCusName());
                map.put("flowName", "客户季报年报修改（集中作业）");
                map.put("result", "退回");
                List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                receivedUserDto.setReceivedUserType("1");// 发送人员类型
                receivedUserDto.setUserId(cusBase.getManagerId());// 客户经理工号
                receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                receivedUserList.add(receivedUserDto);
                messageSendDto.setMessageType("MSG_KH_M_0003");
                messageSendDto.setParams(map);
                messageSendDto.setReceivedUserList(receivedUserList);
                messageSendService.sendMessage(messageSendDto);
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程打回操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程拿回操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                cusManaTaskService.update(cusManaTask);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程拿回初始节点操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                cusManaTaskService.update(cusManaTask);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程拿回初始节点操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                cusManaTaskService.update(cusManaTask);
            } else {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，未知操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("客户信息录入集中作业流程申请【{}】，场景【{}】，后业务处理失败{}", serno, flowBizType, e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "KHGL02".equals(flowCode);
    }
}
