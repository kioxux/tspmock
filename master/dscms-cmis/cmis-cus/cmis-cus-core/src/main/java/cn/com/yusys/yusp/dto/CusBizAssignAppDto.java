package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CusBizAcquUserLst;
import cn.com.yusys.yusp.domain.CusBizAssignApp;
import cn.com.yusys.yusp.domain.CusPrivCorreRel;

import java.util.List;

public class CusBizAssignAppDto {

    private List<CusPrivCorreRel> cusPrivCorreRel;
    private List<CusBizAcquUserLst> list;
    private CusBizAssignApp cusBizAssignApp;

    public List<CusPrivCorreRel> getCusPrivCorreRel() {
        return cusPrivCorreRel;
    }

    public void setCusPrivCorreRel(List<CusPrivCorreRel> cusPrivCorreRel) {
        this.cusPrivCorreRel = cusPrivCorreRel;
    }

    public List<CusBizAcquUserLst> getList() {
        return list;
    }

    public void setList(List<CusBizAcquUserLst> list) {
        this.list = list;
    }

    public CusBizAssignApp getCusBizAssignApp() {
        return cusBizAssignApp;
    }

    public void setCusBizAssignApp(CusBizAssignApp cusBizAssignApp) {
        this.cusBizAssignApp = cusBizAssignApp;
    }
}
