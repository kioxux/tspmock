package cn.com.yusys.yusp.service.server.cmiscus0014;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusCorpApital;
import cn.com.yusys.yusp.domain.CusCorpMgr;
import cn.com.yusys.yusp.dto.CusCorpMgrDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusIntbankMgrDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.req.CmisCus0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014StockHolderListRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMgrMapper;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMgrMapper;
import cn.com.yusys.yusp.service.CusComGradeService;
import cn.com.yusys.yusp.service.CusCorpApitalService;
import cn.com.yusys.yusp.service.CusCorpMgrService;
import cn.com.yusys.yusp.service.CusCorpService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 业务逻辑处理类：法人客户及股东信息查询
 *
 * @author dumd
 * @version 1.0
 * @since 2021年5月26日
 */
@Service
public class CmisCus0014Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0014Service.class);

    @Autowired
    private CusCorpService cusCorpService;

    @Autowired
    private CusCorpApitalService cusCorpApitalService;

    @Autowired
    private CusComGradeService cusComGradeService;

    @Autowired
    private CusIntbankMgrMapper cusIntbankMgrMapper;

    //cus_corp_mgr
    @Autowired
    private CusCorpMgrMapper cusCorpMgrMapper;

    @Transactional
    public CmisCus0014RespDto execute(CmisCus0014ReqDto reqDto) throws YuspException {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0014.key, DscmsEnum.TRADE_CODE_CMISCUS0014.value);

        CmisCus0014RespDto respDto = new CmisCus0014RespDto();
        List<CmisCus0014StockHolderListRespDto> stockHolderList = new ArrayList<>();

        //法人客户编号
        String cusId = reqDto.getCusId();
        //信用评级最终等级
        String finalRank = null;
        //非零内评到期日期
        String dueDt = null;

        try{
            //1.如果法人客户编号为空，则抛异常
            if (StringUtils.isEmpty(cusId)){
                throw new YuspException(EcsEnum.ECS040002.key, EcsEnum.ECS040002.value);
            }

            //2.如果不是法人客户，则抛异常
            CusCorp cusCorp = cusCorpService.selectByPrimaryKey(cusId);
            if (cusCorp==null){
                throw new YuspException(EcsEnum.ECS040013.key, EcsEnum.ECS040013.value);
            }

            //3.将cusCorp里的数据拷贝到respDto里
            copyCusCorpInfo(respDto, cusCorp);

            //4.查询法人客户的信用评级信息
            Map<String, String> gradeInfo = cusComGradeService.selectGradeInfoByCusId(cusId);

            if (gradeInfo!=null && !gradeInfo.isEmpty()){
                finalRank = gradeInfo.get("finalRank");
                dueDt = gradeInfo.get("dueDt");
            }

            //5.查询股东信息
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusIdRel",cusId);
//            queryModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);

            List<CusCorpApital> cusCorpApitalList = cusCorpApitalService.selectAll(queryModel);

            if (cusCorpApitalList!=null && cusCorpApitalList.size()>0){
                for (CusCorpApital cusCorpApital : cusCorpApitalList) {
                    CmisCus0014StockHolderListRespDto stockHolder = new CmisCus0014StockHolderListRespDto();
                    //5.将cusCorp里的数据拷贝到stockHolder里
                    copyCusCorpApitalInfo(stockHolder,cusCorpApital);
                    stockHolderList.add(stockHolder);
                }
            }

            //获取控股股东
            QueryModel queryModel2 = new QueryModel();
            queryModel2.addCondition("cusIdRel",cusId);
            List<CusCorpMgr> cusCorpMgrs = cusCorpMgrMapper.selectByModel(queryModel2);
            if (CollectionUtils.nonEmpty(cusCorpMgrs)){
                List<CusCorpMgr> cusCorpMgrList = cusCorpMgrs.stream().filter(
                        a -> CmisCusConstants.STD_CROP_MRG_TYPE_201200.equals(a.getMrgType())).collect(Collectors.toList());
                if (CollectionUtils.nonEmpty(cusCorpMgrList)){
                    List<CusCorpMgrDto> cusCorpMgrDtoList = new ArrayList<>();
                    cusCorpMgrList.forEach(a->{
                        CusCorpMgrDto cusCorpMgrDto = new CusCorpMgrDto();
                        BeanUtils.copyProperties(a,cusCorpMgrDto);
                        cusCorpMgrDtoList.add(cusCorpMgrDto);
                    });
                    respDto.setCusCorpMgrDtoList(cusCorpMgrDtoList);
                }
            }

            respDto.setFinalRank(finalRank);
            respDto.setDueDt(dueDt);
            respDto.setStockHolderList(stockHolderList);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsEnum.TRADE_CODE_CMISCUS0014.key+"报错：",e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0014.key, DscmsEnum.TRADE_CODE_CMISCUS0014.value);
        return respDto;
    }

    /**
     * 将cusCorp里的数据拷贝到respDto里
     * @param respDto
     * @param cusCorp
     * @return
     */
    public void copyCusCorpInfo(CmisCus0014RespDto respDto,CusCorp cusCorp){
        respDto.setCusId(cusCorp.getCusId());
        respDto.setCusNameEn(cusCorp.getCusNameEn());
        respDto.setCountry(cusCorp.getCountry());
        respDto.setCorpQlty(cusCorp.getCorpQlty());
        respDto.setCityType(cusCorp.getCityType());
        respDto.setAssTotal(cusCorp.getAssTotal());
        respDto.setAdminSubRel(cusCorp.getAdminSubRel());
        respDto.setSubTyp(cusCorp.getSubTyp());
        respDto.setInvestMbody(cusCorp.getInvestMbody());
        respDto.setHoldType(cusCorp.getHoldType());
        respDto.setTradeClass(cusCorp.getTradeClass());
        respDto.setCusCcrModelId(cusCorp.getCusCcrModelId());
        respDto.setCusCcrModelName(cusCorp.getCusCcrModelName());
        respDto.setCllType2(cusCorp.getCllType2());
        respDto.setBuildDate(cusCorp.getBuildDate());
        respDto.setFjobNum(cusCorp.getFjobNum());
        respDto.setSalVolume(cusCorp.getSalVolume());
        respDto.setOperIncome(cusCorp.getOperIncome());
        respDto.setCorpScale(cusCorp.getCorpScale());
        respDto.setCusScaleForm(cusCorp.getCusScaleForm());
        respDto.setNatEcoSec(cusCorp.getNatEcoSec());
        respDto.setUnitAlleInd(cusCorp.getUnitAlleInd());
        respDto.setAlleBunb(cusCorp.getAlleBunb());
        respDto.setFramInd(cusCorp.getFramInd());
        respDto.setFarmerCopInd(cusCorp.getFarmerCopInd());
        respDto.setAgrInd(cusCorp.getAgrInd());
        respDto.setInclusiveFinanceStatistics(cusCorp.getInclusiveFinanceStatistics());
        respDto.setInsCode(cusCorp.getInsCode());
        respDto.setInsRegDate(cusCorp.getInsRegDate());
        respDto.setInsEndDate(cusCorp.getInsEndDate());
        respDto.setInsOrg(cusCorp.getInsOrg());
        respDto.setInsAnnDate(cusCorp.getInsAnnDate());
        respDto.setLicenseType(cusCorp.getLicenseType());
        respDto.setRegiCode(cusCorp.getRegiCode());
        respDto.setRegiType(cusCorp.getRegiType());
        respDto.setQuliGarade(cusCorp.getQuliGarade());
        respDto.setAdminOrg(cusCorp.getAdminOrg());
        respDto.setApprOrg(cusCorp.getApprOrg());
        respDto.setApprDocNo(cusCorp.getApprDocNo());
        respDto.setRegiAreaCode(cusCorp.getRegiAreaCode());
        respDto.setRegiAddr(cusCorp.getRegiAddr());
        respDto.setRegiAddrEn(cusCorp.getRegiAddrEn());
        respDto.setAcuStateCode(cusCorp.getAcuStateCode());
        respDto.setOperAddrAct(cusCorp.getOperAddrAct());
        respDto.setMainOptScp(cusCorp.getMainOptScp());
        respDto.setPartOptScp(cusCorp.getPartOptScp());
        respDto.setRegiCurType(cusCorp.getRegiCurType());
        respDto.setRegiCapAmt(cusCorp.getRegiCapAmt());
        respDto.setPaidCapCurType(cusCorp.getPaidCapCurType());
        respDto.setPaidCapAmt(cusCorp.getPaidCapAmt());
        respDto.setRegiStartDate(cusCorp.getRegiStartDate());
        respDto.setRegiEndDate(cusCorp.getRegiEndDate());
        respDto.setRegAudit(cusCorp.getRegAudit());
        respDto.setRegAuditDate(cusCorp.getRegAuditDate());
        respDto.setRegAuditEndDate(cusCorp.getRegAuditEndDate());
        respDto.setNatTaxRegCode(cusCorp.getNatTaxRegCode());
        respDto.setNatTaxRegOrg(cusCorp.getNatTaxRegOrg());
        respDto.setNatTaxRegDt(cusCorp.getNatTaxRegDt());
        respDto.setNatTaxRegEndDt(cusCorp.getNatTaxRegEndDt());
        respDto.setNatTaxAnnDate(cusCorp.getNatTaxAnnDate());
        respDto.setLocTaxRegCode(cusCorp.getLocTaxRegCode());
        respDto.setLocTaxRegOrg(cusCorp.getLocTaxRegOrg());
        respDto.setLocTaxRegDt(cusCorp.getLocTaxRegDt());
        respDto.setLocTaxRegEndDt(cusCorp.getLocTaxRegEndDt());
        respDto.setLocTaxAnnDate(cusCorp.getLocTaxAnnDate());
        respDto.setLoanCardFlg(cusCorp.getLoanCardFlg());
        respDto.setLoanCardId(cusCorp.getLoanCardId());
        respDto.setLoanCardPwd(cusCorp.getLoanCardPwd());
        respDto.setLoanCardEffFlg(cusCorp.getLoanCardEffFlg());
        respDto.setLoanCardAnnDate(cusCorp.getLoanCardAnnDate());
        respDto.setCertIdate(cusCorp.getCertIdate());
        respDto.setConType(cusCorp.getConType());
        respDto.setCorpOwnersType(cusCorp.getCorpOwnersType());
        respDto.setIsBankShd(cusCorp.getIsBankShd());
        respDto.setIsSmconCus(cusCorp.getIsSmconCus());
        respDto.setDetailAddr(cusCorp.getDetailAddr());
        respDto.setProduceEquipYear(cusCorp.getProduceEquipYear());
        respDto.setQq(cusCorp.getQq());
        respDto.setProduceAbiYear(cusCorp.getProduceAbiYear());
        respDto.setIsNatctl(cusCorp.getIsNatctl());
        respDto.setFreqLinkman(cusCorp.getFreqLinkman());
        respDto.setNatctlLevel(cusCorp.getNatctlLevel());
        respDto.setLicOperPro(cusCorp.getLicOperPro());
        respDto.setFreqLinkmanTel(cusCorp.getFreqLinkmanTel());
        respDto.setCommonOperPro(cusCorp.getCommonOperPro());
        respDto.setOperStatus(cusCorp.getOperStatus());
        respDto.setIsLongVld(cusCorp.getIsLongVld());
        respDto.setBasicDepAccNoOpenLic(cusCorp.getBasicDepAccNoOpenLic());
        respDto.setBasicDepAccNo(cusCorp.getBasicDepAccNo());
        respDto.setIsBankBasicDepAccNo(cusCorp.getIsBankBasicDepAccNo());
        respDto.setBasicDepAccob(cusCorp.getBasicDepAccob());
        respDto.setBasicAccNoOpenDate(cusCorp.getBasicAccNoOpenDate());
        respDto.setCommonAccNoOpenDate(cusCorp.getCommonAccNoOpenDate());
        respDto.setMainPrdDesc(cusCorp.getMainPrdDesc());
        respDto.setCreditLevelOuter(cusCorp.getCreditLevelOuter());
        respDto.setEvalDate(cusCorp.getEvalDate());
        respDto.setFax(cusCorp.getFax());
        respDto.setEvalOrgId(cusCorp.getEvalOrgId());
        respDto.setLinkmanEmail(cusCorp.getLinkmanEmail());
        respDto.setSendAddr(cusCorp.getSendAddr());
        respDto.setInitLoanDate(cusCorp.getInitLoanDate());
        respDto.setWechatNo(cusCorp.getWechatNo());
        respDto.setIsStrgcCus(cusCorp.getIsStrgcCus());
        respDto.setFinaReportType(cusCorp.getFinaReportType());
        respDto.setAreaPriorCorp(cusCorp.getAreaPriorCorp());
        respDto.setSpOperFlag(cusCorp.getSpOperFlag());
        respDto.setIsNewBuildCorp(cusCorp.getIsNewBuildCorp());
        respDto.setGrpCusType(cusCorp.getGrpCusType());
        respDto.setRegiOrg(cusCorp.getRegiOrg());
        respDto.setMainBusNation(cusCorp.getMainBusNation());
        respDto.setCreditPadAccNo(cusCorp.getCreditPadAccNo());
        respDto.setLoanType(cusCorp.getLoanType());
        respDto.setOperPlaceOwnshp(cusCorp.getOperPlaceOwnshp());
        respDto.setOperPlaceSqu(cusCorp.getOperPlaceSqu());
        respDto.setMainProduceEquip(cusCorp.getMainProduceEquip());
        respDto.setInputId(cusCorp.getInputId());
        respDto.setInputBrId(cusCorp.getInputBrId());
        respDto.setInputDate(cusCorp.getInputDate());
        respDto.setIisSzjrfwCrop(cusCorp.getIisSzjrfwCrop());
        respDto.setIsSteelCus(cusCorp.getIsSteelCus());
        respDto.setIsStockCorp(cusCorp.getIsStockCorp());
        respDto.setIsCtinve(cusCorp.getIsCtinve());
        respDto.setCtinveLevel(cusCorp.getCtinveLevel());
        respDto.setGoverInvestPlat(cusCorp.getGoverInvestPlat());
        respDto.setImpexpFlag(cusCorp.getImpexpFlag());
        respDto.setRedcbizUnitTradeClass(cusCorp.getRedcbizUnitTradeClass());
        respDto.setCusType(cusCorp.getCusType());
        respDto.setCusName(cusCorp.getCusName());
        respDto.setIslocked(cusCorp.getIslocked());
    }

    /**
     * 将cusCorpApital里的数据拷贝到stockHolder里
     * @param stockHolder
     * @param cusCorpApital
     */
    public void copyCusCorpApitalInfo(CmisCus0014StockHolderListRespDto stockHolder,CusCorpApital cusCorpApital){
        //出资人客户编号
        stockHolder.setCusId(cusCorpApital.getCusId());
        //出资人性质
        stockHolder.setInvtTyp(cusCorpApital.getInvtTyp());
        //出资人证件类型
        stockHolder.setCertTyp(cusCorpApital.getCertTyp());
        //出资人证件号码
        stockHolder.setCertCode(cusCorpApital.getCertCode());
        //出资人名称
        stockHolder.setInvtName(cusCorpApital.getInvtName());
        //关联客户编号
        stockHolder.setCusIdRel(cusCorpApital.getCusIdRel());
        //国别
        stockHolder.setCountry(cusCorpApital.getCountry());
        //出资方式
        stockHolder.setInvtType(cusCorpApital.getInvtType());
        //币种
        stockHolder.setCurType(cusCorpApital.getCurType());
        //出资金额
        stockHolder.setInvtAmt(cusCorpApital.getInvtAmt());
        //出资比例
        stockHolder.setInvtPerc(cusCorpApital.getInvtPerc());
        //出资时间
        stockHolder.setInvDate(cusCorpApital.getInvDate());
        //出资说明
        stockHolder.setInvtDesc(cusCorpApital.getInvtDesc());
        //备注
        stockHolder.setRemark(cusCorpApital.getRemark());
    }
}