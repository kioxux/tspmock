package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusSnqyqd
 * @类描述: cus_snqyqd数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-21 23:55:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusSnqyqdDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 行政区划代码 **/
	private String areaCode;
	
	/** 行政区划名称 **/
	private String address;
	
	/** 是否农户标识 **/
	private String isSn;
	
	
	/**
	 * @param areaCode
	 */
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode == null ? null : areaCode.trim();
	}
	
    /**
     * @return AreaCode
     */	
	public String getAreaCode() {
		return this.areaCode;
	}
	
	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}
	
    /**
     * @return Address
     */	
	public String getAddress() {
		return this.address;
	}
	
	/**
	 * @param isSn
	 */
	public void setIsSn(String isSn) {
		this.isSn = isSn == null ? null : isSn.trim();
	}
	
    /**
     * @return IsSn
     */	
	public String getIsSn() {
		return this.isSn;
	}


}