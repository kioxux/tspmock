package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CusPubHandoverApp;
import cn.com.yusys.yusp.domain.CusPubHandoverLst;

import java.util.List;

public class CusPubHandoverAppDto {
    private CusPubHandoverApp cusPubHandoverApp;
    private List<CusPubHandoverLst> list ;

    public CusPubHandoverApp getCusPubHandoverApp() {
        return cusPubHandoverApp;
    }

    public void setCusPubHandoverApp(CusPubHandoverApp cusPubHandoverApp) {
        this.cusPubHandoverApp = cusPubHandoverApp;
    }

    public List<CusPubHandoverLst> getList() {
        return list;
    }

    public void setList(List<CusPubHandoverLst> list) {
        this.list = list;
    }
}
