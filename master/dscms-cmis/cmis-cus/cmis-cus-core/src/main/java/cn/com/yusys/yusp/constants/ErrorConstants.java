package cn.com.yusys.yusp.constants;

/**
 * 类描述： cms-core
 * 
 * @Auther：zzbankwb190
 * @CreateTime:2019年8月19日-下午7:38:04
 *
 */
public class ErrorConstants {

	/**
	 * 错误码：不宜贷款户修改
	 */
	public static final String NRCS_CMS_T20602283 = "NRCS.CMS.T20602283";
	/**
	 * 错误码：物流微秒贷更新授信到期日失败
	 */
	public static final String NRCS_CMS_T20604555 = "NRCS.CMS.T20604555";
	/**
	 * 错误码：客户合并广播留痕删除
	 */
	public static final String NRCS_CMS_T20605094 = "NRCS.CMS.T20605094";
	/**
	 * 错误码：综合查询客户高管、股东、实际控制人出错
	 */
	public static final String NRCS_CMS_T20605091 = "NRCS.CMS.T20605091";
	/**
	 * 错误码：公司客户新增证件号码异常
	 */
	public static final String NRCS_CMS_T20601305 = "NRCS.CMS.T20601305";
	/**
	 * 错误码：客户合并日志新增
	 */
	public static final String NRCS_CMS_T21604803 = "NRCS.CMS.T21604803";
	/**
	 * 错误码：白名单批次列表查询
	 */
	public static final String NRCS_CMS_T21604631 = "NRCS.CMS.T21604631";
	/**
	 * 错误码：白名单批次查看
	 */
	public static final String NRCS_CMS_T21604642 = "NRCS.CMS.T21604642";
	/**
	 * 错误码：白名单批次新增
	 */
	public static final String NRCS_CMS_T21604653 = "NRCS.CMS.T21604653";
	/**
	 * 错误码：白名单批次删除
	 */
	public static final String NRCS_CMS_T21604664 = "NRCS.CMS.T21604664";
	/**
	 * 错误码：白名单批次修改
	 */
	public static final String NRCS_CMS_T21604675 = "NRCS.CMS.T21604675";
	/**
	 * 错误码：企业经营贷列表查询
	 */
	public static final String NRCS_CMS_T21604461 = "NRCS.CMS.T21604461";
	/**
	 * 错误码：企业经营贷查看
	 */
	public static final String NRCS_CMS_T21604472 = "NRCS.CMS.T21604472";
	/**
	 * 错误码：企业经营贷新增
	 */
	public static final String NRCS_CMS_T21604483 = "NRCS.CMS.T21604483";
	/**
	 * 错误码：企业经营贷删除
	 */
	public static final String NRCS_CMS_T21604494 = "NRCS.CMS.T21604494";
	/**
	 * 错误码：企业经营贷修改
	 */
	public static final String NRCS_CMS_T21604505 = "NRCS.CMS.T21604505";
	/**
	 * 错误码：个人经营贷列表查询
	 */
	public static final String NRCS_CMS_T21604411 = "NRCS.CMS.T21604361";
	/**
	 * 错误码：个人经营贷查看
	 */
	public static final String NRCS_CMS_T21604422 = "NRCS.CMS.T21604422";
	/**
	 * 错误码：个人经营贷新增
	 */
	public static final String NRCS_CMS_T21604433 = "NRCS.CMS.T21604433";
	/**
	 * 错误码：个人经营贷删除
	 */
	public static final String NRCS_CMS_T21604444 = "NRCS.CMS.T21604444";
	/**
	 * 错误码：个人经营贷修改
	 */
	public static final String NRCS_CMS_T21604455 = "NRCS.CMS.T21604455";
	/**
	 * 错误码：个人消费贷列表查询
	 */
	public static final String NRCS_CMS_T21604361 = "NRCS.CMS.T21604361";
	/**
	 * 错误码：个人消费贷查看
	 */
	public static final String NRCS_CMS_T21604372 = "NRCS.CMS.T21604372";
	/**
	 * 错误码：个人消费贷新增
	 */
	public static final String NRCS_CMS_T21604383 = "NRCS.CMS.T21604383";
	/**
	 * 错误码：个人消费贷删除
	 */
	public static final String NRCS_CMS_T21604394 = "NRCS.CMS.T21604394";
	/**
	 * 错误码：个人消费贷修改
	 */
	public static final String NRCS_CMS_T21604405 = "NRCS.CMS.T21604405";
	/**
	 * 错误码：白名单类型列表查询
	 */
	public static final String NRCS_CMS_T21701292 = "NRCS.CMS.T21701292";
	/**
	 * 错误码：白名单类型新增
	 */
	public static final String NRCS_CMS_T21701293 = "NRCS.CMS.T21701293";
	/**
	 * 错误码：白名单类型修改
	 */
	public static final String NRCS_CMS_T21701294 = "NRCS.CMS.T21701294";
	/**
	 * 错误码：白名单类型删除
	 */
	public static final String NRCS_CMS_T21701295 = "NRCS.CMS.T21701295";
	/**
	 * 错误码：白名单类型查看
	 */
	public static final String NRCS_CMS_T21701296 = "NRCS.CMS.T21701296";
	/**
	 * 错误码：白名单类型模板下载
	 */
	public static final String NRCS_CMS_T21702201 = "NRCS.CMS.T21702201";
	/**
	 * 通用白名单列表查询错误码
	 */
	public static final String NRCS_CMS_T21702297 = "NRCS.CMS.T21702297";
	/**
	 * 通用白名单新增错误码
	 */
	public static final String NRCS_CMS_T21702298 = "NRCS.CMS.T21702298";
	/**
	 * 通用白名单修改错误码
	 */
	public static final String NRCS_CMS_T21702299 = "NRCS.CMS.T21702299";
	/**
	 * 通用白名单删除错误码
	 */
	public static final String NRCS_CMS_T21702300 = "NRCS.CMS.T21702300";
	/**
	 * 通用白名单查看错误码
	 */
	public static final String NRCS_CMS_T21702301 = "NRCS.CMS.T21702301";
	/**
	 * 通用白名单模板导出错误码
	 */
	public static final String NRCS_CMS_T21702303 = "NRCS.CMS.T21702303";
	/**
	 * 通用白名单模板导入错误码
	 */
	public static final String NRCS_CMS_T21702304 = "NRCS.CMS.T21702304";
	/**
	 * 财务分析纵向模板样式列表
	 */
	public static final String NRCS_CMS_T20603643 = "NRCS.CMS.T20603643";
	/**
	 * 财务分析纵向模板样式新增
	 */
	public static final String NRCS_CMS_T20603644 = "NRCS.CMS.T20603644";
	/**
	 * 财务分析纵向模板样式修改
	 */
	public static final String NRCS_CMS_T20603645 = "NRCS.CMS.T20603645";
	/**
	 * 财务分析纵向模板样式删除
	 */
	public static final String NRCS_CMS_T20603646 = "NRCS.CMS.T20603646";
	/**
	 * 财务分析纵向模板样式查看
	 */
	public static final String NRCS_CMS_T20603647 = "NRCS.CMS.T20603647";
	/**
	 * 财务分析模板描述查看
	 */
	public static final String NRCS_CMS_T20403647 = "NRCS.CMS.T20403647";
	/**
	 * 财务分析模板描述删除
	 */
	public static final String NRCS_CMS_T20403646 = "NRCS.CMS.T20403646";
	/**
	 * 财务分析模板描述修改
	 */
	public static final String NRCS_CMS_T20403645 = "NRCS.CMS.T20403645";
	/**
	 * 财务分析模板描述新增
	 */
	public static final String NRCS_CMS_T20403644 = "NRCS.CMS.T20403644";
	/**
	 * 财务分析模板描述列表
	 */
	public static final String NRCS_CMS_T20403643 = "NRCS.CMS.T20403643";
	/**
	 * 财务分析横向模板样式列表
	 */
	public static final String NRCS_CMS_T20403843 = "NRCS.CMS.T20403843";
	/**
	 * 财务分析横向模板样式新增
	 */
	public static final String NRCS_CMS_T20403844 = "NRCS.CMS.T20403844";
	/**
	 * 财务分析横向模板样式修改
	 */
	public static final String NRCS_CMS_T20403845 = "NRCS.CMS.T20403845";
	/**
	 * 财务分析横向模板样式删除
	 */
	public static final String NRCS_CMS_T20403846 = "NRCS.CMS.T20403846";
	/**
	 * 财务分析横向模板样式查看
	 */
	public static final String NRCS_CMS_T20403847 = "NRCS.CMS.T20403847";
	/**
	 * 逃废债情况查询错误码
	 */
	public static final String NRCS_CMS_T20402083 = "NRCS.CMS.T20402083";
	/**
	 * 逃废债情况新增错误码
	 */
	public static final String NRCS_CMS_T20402084 = "NRCS.CMS.T20402084";
	/**
	 * 逃废债情况删除错误码
	 */
	public static final String NRCS_CMS_T20402085 = "NRCS.CMS.T20402085";
	/**
	 * 逃废债情况修改错误码
	 */
	public static final String NRCS_CMS_T20402086 = "NRCS.CMS.T20402086";
	/**
	 * 逃废债情况查看错误码
	 */
	public static final String NRCS_CMS_T20402087 = "NRCS.CMS.T20402087";
	/**
	 * 欠息记录情况查询错误码
	 */
	public static final String NRCS_CMS_T20403088 = "NRCS.CMS.T20403088";
	/**
	 * 欠息记录情况新增错误码
	 */
	public static final String NRCS_CMS_T20403089 = "NRCS.CMS.T20403089";
	/**
	 * 欠息记录情况删除错误码
	 */
	public static final String NRCS_CMS_T20403090 = "NRCS.CMS.T20403090";
	/**
	 * 欠息记录情况修改错误码
	 */
	public static final String NRCS_CMS_T20403091 = "NRCS.CMS.T20403091";
	/**
	 * 欠息记录情况查看错误码
	 */
	public static final String NRCS_CMS_T20403092 = "NRCS.CMS.T20403092";
	/**
	 * 人寿保险情况查询错误码
	 */
	public static final String NRCS_CMS_T20305073 = "NRCS.CMS.T20305073";
	/**
	 * 人寿保险情况新增错误码
	 */
	public static final String NRCS_CMS_T20305074 = "NRCS.CMS.T20305074";
	/**
	 * 人寿保险情况删除错误码
	 */
	public static final String NRCS_CMS_T20305075 = "NRCS.CMS.T20305075";
	/**
	 * 人寿保险情况修改错误码
	 */
	public static final String NRCS_CMS_T20305076 = "NRCS.CMS.T20305076";
	/**
	 * 人寿保险情况查看错误码
	 */
	public static final String NRCS_CMS_T20305077 = "NRCS.CMS.T20305077";
	/**
	 * 挤占挪用贷款查询错误码
	 */
	public static final String NRCS_CMS_T20404093 = "NRCS.CMS.T20404093";
	/**
	 * 挤占挪用贷款新增错误码
	 */
	public static final String NRCS_CMS_T20404094 = "NRCS.CMS.T20404094";
	/**
	 * 挤占挪用贷款删除错误码
	 */
	public static final String NRCS_CMS_T20404095 = "NRCS.CMS.T20404095";
	/**
	 * 挤占挪用贷款修改错误码
	 */
	public static final String NRCS_CMS_T20404096 = "NRCS.CMS.T20404096";
	/**
	 * 挤占挪用贷款查看错误码
	 */
	public static final String NRCS_CMS_T20404097 = "NRCS.CMS.T20404097";
	/**
	 * 虚假资料情况查询错误码
	 */
	public static final String NRCS_CMS_T20405098 = "NRCS.CMS.T20405098";
	/**
	 * 虚假资料情况新增错误码
	 */
	public static final String NRCS_CMS_T20405099 = "NRCS.CMS.T20405099";
	/**
	 * 虚假资料情况删除错误码
	 */
	public static final String NRCS_CMS_T20405100 = "NRCS.CMS.T20405100";
	/**
	 * 虚假资料情况修改错误码
	 */
	public static final String NRCS_CMS_T20405101 = "NRCS.CMS.T20405101";
	/**
	 * 虚假资料情况查看错误码
	 */
	public static final String NRCS_CMS_T20405102 = "NRCS.CMS.T20405102";
	/**
	 * 其他大事信息查询错误码
	 */
	public static final String NRCS_CMS_T20406103 = "NRCS.CMS.T20406103";
	/**
	 * 其他大事信息新增错误码
	 */
	public static final String NRCS_CMS_T20406104 = "NRCS.CMS.T20406104";
	/**
	 * 其他大事信息删除错误码
	 */
	public static final String NRCS_CMS_T20406105 = "NRCS.CMS.T20406105";
	/**
	 * 其他大事信息修改错误码
	 */
	public static final String NRCS_CMS_T20406106 = "NRCS.CMS.T20406106";
	/**
	 * 其他大事信息查看错误码
	 */
	public static final String NRCS_CMS_T20406107 = "NRCS.CMS.T20406107";
	/**
	 * 被起诉情况查询错误码
	 */
	public static final String NRCS_CMS_T20401078 = "NRCS.CMS.T20401078";
	/**
	 * 被起诉情况新增错误码
	 */
	public static final String NRCS_CMS_T20401079 = "NRCS.CMS.T20401079";
	/**
	 * 被起诉情况删除错误码
	 */
	public static final String NRCS_CMS_T20401080 = "NRCS.CMS.T20401080";
	/**
	 * 被起诉情况修改错误码
	 */
	public static final String NRCS_CMS_T20401081 = "NRCS.CMS.T20401081";
	/**
	 * 被起诉情况查看错误码
	 */
	public static final String NRCS_CMS_T20401082 = "NRCS.CMS.T20401082";
	/**
	 * 报表样式配置查询错误码
	 */
	public static final String NRCS_CMS_T20204320 = "NRCS.BSS.T20204320";
	/**
	 * 报表样式配置新增错误码
	 */
	public static final String NRCS_CMS_T20204321 = "NRCS.BSS.T20204321";
	/**
	 * 报表样式配置修改错误码
	 */
	public static final String NRCS_CMS_T20204322 = "NRCS.BSS.T20204322";
	/**
	 * 报表样式配置删除错误码
	 */
	public static final String NRCS_CMS_T20204323 = "NRCS.BSS.T20204323";
	/**
	 * 报表样式配置查看错误码
	 */
	public static final String NRCS_CMS_T20204324 = "NRCS.BSS.T20204324";
	/**
	 * 报表项目配置查询错误码
	 */
	public static final String NRCS_CMS_T20203326 = "NRCS.BSS.T20203326";
	/**
	 * 报表项目配置新增错误码
	 */
	public static final String NRCS_CMS_T20203327 = "NRCS.BSS.T20203327";
	/**
	 * 报表项目配置修改错误码
	 */
	public static final String NRCS_CMS_T20203328 = "NRCS.BSS.T20203328";
	/**
	 * 报表项目配置删除错误码
	 */
	public static final String NRCS_CMS_T20203329 = "NRCS.BSS.T20203329";
	/**
	 * 报表项目配置查看错误码
	 */
	public static final String NRCS_CMS_T20203330 = "NRCS.BSS.T20203330";
	/**
	 * 报表配置定义查询错误码
	 */
	public static final String NRCS_CMS_T20202331 = "NRCS.BSS.T20202331";
	/**
	 * 报表配置定义新增错误码
	 */
	public static final String NRCS_CMS_T20202332 = "NRCS.BSS.T20202332";
	/**
	 * 报表配置定义修改错误码
	 */
	public static final String NRCS_CMS_T20202333 = "NRCS.BSS.T20202333";
	/**
	 * 报表配置定义删除错误码
	 */
	public static final String NRCS_CMS_T20202334 = "NRCS.BSS.T20202334";
	/**
	 * 报表配置定义查看错误码
	 */
	public static final String NRCS_CMS_T20202335 = "NRCS.BSS.T20202335";
	/**
	 * 财务报表配置查询错误码
	 */
	public static final String NRCS_CMS_T20201335 = "NRCS.BSS.T20201335";
	/**
	 * 财务报表配置新增错误码
	 */
	public static final String NRCS_CMS_T20201336 = "NRCS.BSS.T20201336";
	/**
	 * 财务报表配置修改错误码
	 */
	public static final String NRCS_CMS_T20201337 = "NRCS.BSS.T20201337";
	/**
	 * 财务报表配置删除错误码
	 */
	public static final String NRCS_CMS_T20201338 = "NRCS.BSS.T20201338";
	/**
	 * 财务报表配置查看错误码
	 */
	public static final String NRCS_CMS_T20201339 = "NRCS.BSS.T20201339";
	/**
	 * 行业分类查询错误码
	 */
	public static final String NRCS_CMS_T20205340 = "NRCS.BSS.T20205340";
	/**
	 * 行业分类新增错误码
	 */
	public static final String NRCS_CMS_T20205341 = "NRCS.BSS.T20205341";
	/**
	 * 行业分类修改错误码
	 */
	public static final String NRCS_CMS_T20205342 = "NRCS.BSS.T20205342";
	/**
	 * 行业分类删除错误码
	 */
	public static final String NRCS_CMS_T20205343 = "NRCS.BSS.T20205343";
	/**
	 * 行业分类查看错误码
	 */
	public static final String NRCS_CMS_T20205344 = "NRCS.BSS.T20205344";
	/**
	 * 行业标准值查询错误码
	 */
	public static final String NRCS_CMS_T20206345 = "NRCS.BSS.T20206345";
	/**
	 * 行业标准值新增错误码
	 */
	public static final String NRCS_CMS_T20206346 = "NRCS.BSS.T20206346";
	/**
	 * 行业标准值修改错误码
	 */
	public static final String NRCS_CMS_T20206347 = "NRCS.BSS.T20206347";
	/**
	 * 行业标准值删除错误码
	 */
	public static final String NRCS_CMS_T20206348 = "NRCS.BSS.T20206348";
	/**
	 * 行业标准值查看错误码
	 */
	public static final String NRCS_CMS_T20206349 = "NRCS.BSS.T20206349";
	/**
	 * 标准行业划型标准查询错误码
	 */
	public static final String NRCS_CMS_T20214350 = "NRCS.BSS.T20214350";
	/**
	 * 标准行业划型标准新增错误码
	 */
	public static final String NRCS_CMS_T20214351 = "NRCS.BSS.T20214351";
	/**
	 * 标准行业划型标准修改错误码
	 */
	public static final String NRCS_CMS_T20214352 = "NRCS.BSS.T20214352";
	/**
	 * 标准行业划型标准删除错误码
	 */
	public static final String NRCS_CMS_T20214353 = "NRCS.BSS.T20214353";
	/**
	 * 标准行业划型标准查看错误码
	 */
	public static final String NRCS_CMS_T20214354 = "NRCS.BSS.T20214354";
	/**
	 * 特殊行业划型标准查询错误码
	 */
	public static final String NRCS_CMS_T20215355 = "NRCS.BSS.T20215355";
	/**
	 * 特殊行业划型标准新增错误码
	 */
	public static final String NRCS_CMS_T20215356 = "NRCS.BSS.T20215356";
	/**
	 * 特殊行业划型标准修改错误码
	 */
	public static final String NRCS_CMS_T20215357 = "NRCS.BSS.T20215357";
	/**
	 * 特殊行业划型标准删除错误码
	 */
	public static final String NRCS_CMS_T20215358 = "NRCS.BSS.T20215358";
	/**
	 * 特殊行业划型标准查看错误码
	 */
	public static final String NRCS_CMS_T20215359 = "NRCS.BSS.T20215359";
	/**
	 * 财务分析范围配置查询错误码
	 */
	public static final String NRCS_CMS_T20231360 = "NRCS.BSS.T20231360";
	/**
	 * 财务分析范围配置新增错误码
	 */
	public static final String NRCS_CMS_T20231361 = "NRCS.BSS.T20231361";
	/**
	 * 财务分析范围配置修改错误码
	 */
	public static final String NRCS_CMS_T20231362 = "NRCS.BSS.T20231362";
	/**
	 * 财务分析范围配置删除错误码
	 */
	public static final String NRCS_CMS_T20231363 = "NRCS.BSS.T20231363";
	/**
	 * 财务分析范围配置查看错误码
	 */
	public static final String NRCS_CMS_T20231364 = "NRCS.BSS.T20231364";
	/**
	 * 财务分析模板定义查询错误码
	 */
	public static final String NRCS_CMS_T20233370 = "NRCS.BSS.T20233370";
	/**
	 * 财务分析模板定义新增错误码
	 */
	public static final String NRCS_CMS_T20233371 = "NRCS.BSS.T20233371";
	/**
	 * 财务分析模板定义修改错误码
	 */
	public static final String NRCS_CMS_T20233372 = "NRCS.BSS.T20233372";
	/**
	 * 财务分析模板定义删除错误码
	 */
	public static final String NRCS_CMS_T20233373 = "NRCS.BSS.T20233373";
	/**
	 * 财务分析模板定义查看错误码
	 */
	public static final String NRCS_CMS_T20233374 = "NRCS.BSS.T20233374";
	/**
	 * 财务分析描述配置查询错误码
	 */
	public static final String NRCS_CMS_T20232365 = "NRCS.BSS.T20232365";
	/**
	 * 财务分析描述配置新增错误码
	 */
	public static final String NRCS_CMS_T20232366 = "NRCS.BSS.T20232366";
	/**
	 * 财务分析描述配置修改错误码
	 */
	public static final String NRCS_CMS_T20232367 = "NRCS.BSS.T20232367";
	/**
	 * 财务分析描述配置删除错误码
	 */
	public static final String NRCS_CMS_T20232368 = "NRCS.BSS.T20232368";
	/**
	 * 财务分析描述配置查看错误码
	 */
	public static final String NRCS_CMS_T20232369 = "NRCS.BSS.T20232369";
	/**
	 * 财务横向模板定义查询错误码
	 */
	public static final String NRCS_CMS_T20234375 = "NRCS.BSS.T20234375";
	/**
	 * 财务横向模板定义新增错误码
	 */
	public static final String NRCS_CMS_T20234376 = "NRCS.BSS.T20234376";
	/**
	 * 财务横向模板定义修改错误码
	 */
	public static final String NRCS_CMS_T20234377 = "NRCS.BSS.T20234377";
	/**
	 * 财务横向模板定义删除错误码
	 */
	public static final String NRCS_CMS_T20234378 = "NRCS.BSS.T20234378";
	/**
	 * 财务横向模板定义查看错误码
	 */
	public static final String NRCS_CMS_T20234379 = "NRCS.BSS.T20234379";
	/**
	 * 财务纵向模板定义查询错误码
	 */
	public static final String NRCS_CMS_T20235380 = "NRCS.BSS.T20235380";
	/**
	 * 财务纵向模板定义新增错误码
	 */
	public static final String NRCS_CMS_T20235381 = "NRCS.BSS.T20235381";
	/**
	 * 财务纵向模板定义修改错误码
	 */
	public static final String NRCS_CMS_T20235382 = "NRCS.BSS.T20235382";
	/**
	 * 财务纵向模板定义删除错误码
	 */
	public static final String NRCS_CMS_T20235383 = "NRCS.BSS.T20235383";
	/**
	 * 财务纵向模板定义查看错误码
	 */
	public static final String NRCS_CMS_T20235384 = "NRCS.BSS.T20235384";
	/**
	 * 分析模板组合配置查询错误码
	 */
	public static final String NRCS_CMS_T20236385 = "NRCS.BSS.T20236385";
	/**
	 * 分析模板组合配置新增错误码
	 */
	public static final String NRCS_CMS_T20236386 = "NRCS.BSS.T20236386";
	/**
	 * 分析模板组合配置修改错误码
	 */
	public static final String NRCS_CMS_T20236387 = "NRCS.BSS.T20236387";
	/**
	 * 分析模板组合配置删除错误码
	 */
	public static final String NRCS_CMS_T20236388 = "NRCS.BSS.T20236388";
	/**
	 * 分析模板组合配置查看错误码
	 */
	public static final String NRCS_CMS_T20236389 = "NRCS.BSS.T20236389";
	/**
	 * 客户信息修改记录保存错误码
	 */
	public static final String NRCS_CMS_T20103009 = "NRCS.CMS.T20103009";
	/**
	 * 客户信息修改记录查询错误码
	 */
	public static final String NRCS_CMS_T20105010 = "NRCS.CMS.T20105010";
	/**
	 * 客户信息修改记录查看错误码
	 */
	public static final String NRCS_CMS_T20105011 = "NRCS.CMS.T20105011";
	/**
	 * 客户证件信息查询错误码
	 */
	public static final String NRCS_CMS_T20102005 = "NRCS.CMS.T20102005";
	/**
	 * 客户证件信息添加错误码
	 */
	public static final String NRCS_CMS_T20102006 = "NRCS.CMS.T20102006";
	/**
	 * 个人客户信息查询错误码
	 */
	public static final String NRCS_CMS_T20101001 = "NRCS.CMS.T20101001";
	/**
	 * 个人客户信息新建错误码
	 */
	public static final String NRCS_CMS_T20103002 = "NRCS.CMS.T20103002";
	/**
	 * 个人客户信息修改错误码
	 */
	public static final String NRCS_CMS_T20102003 = "NRCS.CMS.T20102003";
	/**
	 * 个人客户信息查看错误码
	 */
	public static final String NRCS_CMS_T20102004 = "NRCS.CMS.T20102004";
	/**
	 * 个人客户转贷款户错误码
	 */
	public static final String NRCS_CMS_T20102007 = "NRCS.CMS.T20102007";
	/**
	 * 个人客户信息维护错误码
	 */
	public static final String NRCS_CMS_T20104008 = "NRCS.CMS.T20104008";
	/**
	 * 单个白名单查询错误码
	 */
	public static final String NRCS_CMS_T21702305 = "NRCS.CMS.T21702305";
	/**
	 * 单个白名单新增错误码
	 */
	public static final String NRCS_CMS_T21702306 = "NRCS.CMS.T21702306";
	/**
	 * 单个白名单修改错误码
	 */
	public static final String NRCS_CMS_T21702307 = "NRCS.CMS.T21702307";
	/**
	 * 单个白名单删除错误码
	 */
	public static final String NRCS_CMS_T21702308 = "NRCS.CMS.T21702308";
	/**
	 * 单个白名单删除全部错误码
	 */
	public static final String NRCS_CMS_T21702309 = "NRCS.CMS.T21702309";
	/**
	 * 单个白名单查看错误码
	 */
	public static final String NRCS_CMS_T21702310 = "NRCS.CMS.T21702310";
	/**
	 * 白名单分配管户错误码
	 */
	public static final String NRCS_CMS_T21702302 = "NRCS.CMS.T21702302";
	/**
	 * 单个白名单列表导入错误码
	 */
	public static final String NRCS_CMS_T21702311 = "NRCS.CMS.T21702311";
	/**
	 * 单个白名单列表导出错误码
	 */
	public static final String NRCS_CMS_T21702312 = "NRCS.CMS.T21702312";
	/**
	 * 单个白名单发送短信错误码
	 */
	public static final String NRCS_CMS_T21702313 = "NRCS.CMS.T21702313";
	/**
	 * 个人社会关系查询错误码
	 */
	public static final String NRCS_CMS_T20202013 = "NRCS.CMS.T20202013";
	/**
	 * 个人社会关系新增错误码
	 */
	public static final String NRCS_CMS_T20202014 = "NRCS.CMS.T20202014";
	/**
	 * 个人社会关系删除错误码
	 */
	public static final String NRCS_CMS_T20202015 = "NRCS.CMS.T20202015";
	/**
	 * 个人社会关系修改错误码
	 */
	public static final String NRCS_CMS_T20202016 = "NRCS.CMS.T20202016";
	/**
	 * 个人社会关系查看错误码
	 */
	public static final String NRCS_CMS_T20202017 = "NRCS.CMS.T20202017";
	/**
	 * 客户共享信息查询错误码
	 */
	public static final String NRCS_CMS_T21801263 = "NRCS.CMS.T21801263";
	/**
	 * 客户共享信息查看错误码
	 */
	public static final String NRCS_CMS_T21801264 = "NRCS.CMS.T21801264";
	/**
	 * 客户共享信息新增错误码
	 */
	public static final String NRCS_CMS_T21801265 = "NRCS.CMS.T21801265";
	/**
	 * 客户共享信息删除错误码
	 */
	public static final String NRCS_CMS_T21801266 = "NRCS.CMS.T21801266";
	/**
	 * 财务报表信息查询错误码
	 */
	public static final String NRCS_CMS_T20501108 = "NRCS.CMS.T20501108";
	/**
	 * 财务报表信息新增错误码
	 */
	public static final String NRCS_CMS_T20501109 = "NRCS.CMS.T20501109";
	/**
	 * 财务报表信息删除错误码
	 */
	public static final String NRCS_CMS_T20501110 = "NRCS.CMS.T20501110";
	/**
	 * 财务报表信息修改错误码
	 */
	public static final String NRCS_CMS_T20501111 = "NRCS.CMS.T20501111";
	/**
	 * 财务报表信息查看错误码
	 */
	public static final String NRCS_CMS_T20501112 = "NRCS.CMS.T20501112";
	/**
	 * 财务报表信息导入错误码
	 */
	public static final String NRCS_CMS_T20501113 = "NRCS.CMS.T20501113";
	/**
	 * 财务报表信息导出错误码
	 */
	public static final String NRCS_CMS_T20501114 = "NRCS.CMS.T20501114";
	/**
	 * 财务报表信息分析错误码
	 */
	public static final String NRCS_CMS_T20501115 = "NRCS.CMS.T20501115";
	/**
	 * 财务报表信息解锁
	 */
	public static final String NRCS_CMS_T20501116 = "NRCS.CMS.T20501116";
	/**
	 * 财务报表审计信息维护错误码
	 */
	public static final String NRCS_CMS_T20501117 = "NRCS.CMS.T20501117";
	/**
	 * 财务报表基本信息维护错误码
	 */
	public static final String NRCS_CMS_T20501118 = "NRCS.CMS.T20501118";
	/**
	 * 财务报表-修改-暂存/完成错误码
	 */
	public static final String NRCS_CMS_T20501119 = "NRCS.CMS.T20501119";
}
