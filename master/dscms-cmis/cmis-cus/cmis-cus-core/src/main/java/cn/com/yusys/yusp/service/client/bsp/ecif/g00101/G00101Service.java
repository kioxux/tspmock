package cn.com.yusys.yusp.service.client.bsp.ecif.g00101;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2EcifClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：对公客户综合信息维护
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class G00101Service {
    private static final Logger logger = LoggerFactory.getLogger(G00101Service.class);

    // 1）注入：BSP封装调用ECIF系统的接口
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    /**
     * 业务逻辑处理方法：对公客户综合信息维护
     *
     * @param g00101ReqDto
     * @return
     */
    @Transactional
    public G00101RespDto g00101(G00101ReqDto g00101ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00101.key, EsbEnum.TRADE_CODE_G00101.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00101.key, EsbEnum.TRADE_CODE_G00101.value, JSON.toJSONString(g00101ReqDto));
        ResultDto<G00101RespDto> g00101ResultDto = dscms2EcifClientService.g00101(g00101ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00101.key, EsbEnum.TRADE_CODE_G00101.value, JSON.toJSONString(g00101ResultDto));

        String g00101Code = Optional.ofNullable(g00101ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g00101Meesage = Optional.ofNullable(g00101ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G00101RespDto g00101RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, g00101ResultDto.getCode())) {
            //  获取相关的值并解析
            g00101RespDto = g00101ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(g00101Code, g00101Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value);
        return g00101RespDto;
    }
}
