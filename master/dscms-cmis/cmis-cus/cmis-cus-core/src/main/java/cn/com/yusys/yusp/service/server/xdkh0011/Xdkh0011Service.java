package cn.com.yusys.yusp.service.server.xdkh0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.xdkh0011.req.Xdkh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0011.resp.Xdkh0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0011Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0011Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0011Service.class);

    @Resource
    private CusIndivMapper cusIndivMapper;

    @Resource
    private CusBaseMapper cusBaseMapper;

    @Resource
    private CusIndivUnitMapper cusIndivUnitMapper;

    @Resource
    private CusIndivContactMapper cusIndivContactMapper;

    @Resource
    private CusIndivAttrMapper cusIndivAttrMapper;

    @Resource
    private CusIndivSocialMapper cusIndivSocialMapper;

    /**
     * 对私客户信息同步
     *
     * @param xdkh0011DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0011DataRespDto xdkh0011(Xdkh0011DataReqDto xdkh0011DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011DataReqDto));
        Xdkh0011DataRespDto xdkh0011DataRespDto = new Xdkh0011DataRespDto();
        //个人客户
        CusIndiv cusIndiv = new CusIndiv();
        //客户基本信息表
        CusBase cusBase = new CusBase();
        //个人客户单位信息
        CusIndivUnit cusIndivUnit = new CusIndivUnit();
        //个人客户联系信息
        CusIndivContact cusIndivContact = new CusIndivContact();
        //个人客户属性信息
        CusIndivAttr cusIndivAttr = new CusIndivAttr();

        try {
            //客户编号
            String cusId = xdkh0011DataReqDto.getCusId();
            if (StringUtils.isEmpty(cusId)) {
                throw new YuspException(EcsEnum.ECS040002.key, EcsEnum.ECS040002.value);
            }
            //开始字典项映射
            //更新cusBase表
            //BeanUtils.copyProperties(xdkh0011DataReqDto, cusBase);
            //客户状态
            //cusBase.setCusState(xdkh0011DataReqDto.getCusRankCls());
            //信用等级
            cusBase.setCusCrdGrade(xdkh0011DataReqDto.getCredlv());
            //字典项映射
            //证件类型
            String certType = xdkh0011DataReqDto.getCertType();
            cusBase.setCertType(certType);
            cusBase.setCertCode(xdkh0011DataReqDto.getCertCode());
            //客户状态不更新-老信贷逻辑
            //cusBase.setCusState(null);
            cusBase.setCusId(cusId);
            cusBase.setCusName(xdkh0011DataReqDto.getCusName());
            cusBaseMapper.updateByPrimaryKeySelective(cusBase);
            //更新cusIndiv表
            BeanUtils.copyProperties(xdkh0011DataReqDto, cusIndiv);
            cusIndiv.setCusId(cusId);
            //客户名称
            //cusIndiv.setCusName(xdkh0011DataReqDto.getCusName());
            //cusIndiv.setCertEndDt(xdkh0011DataReqDto.getCertEndDate());
            //字典项转换-老信贷逻辑
            String sex = xdkh0011DataReqDto.getIndivSex();
            cusIndiv.setSex(sex);
            cusIndiv.setInitLoanDate(xdkh0011DataReqDto.getComInitLoanDate());
            //String日期格式转换
            //证件到期日
            String certEndDt = PUBUtilTools.changeGjDate2CMIS(xdkh0011DataReqDto.getCertEndDate());
            cusIndiv.setCertEndDt(certEndDt);
            //出生日期
            if(!StringUtils.isEmpty(xdkh0011DataReqDto.getIndivDtOfBirth())){
                String indivDtOfBirth = PUBUtilTools.changeGjDate2CMIS(xdkh0011DataReqDto.getIndivDtOfBirth());
                cusIndiv.setIndivDtOfBirth(indivDtOfBirth);
            }else{
                cusIndiv.setIndivDtOfBirth(null);
            }
            cusIndivMapper.updateByPrimaryKeySelective(cusIndiv);

            //更新cusIndivUnit表
            BeanUtils.copyProperties(xdkh0011DataReqDto, cusIndivUnit);
            cusIndivUnit.setJobTtl(xdkh0011DataReqDto.getDuty());

            cusIndivUnitMapper.updateByCusIdSelective(cusIndivUnit);

            //更新cusIndivContact
            BeanUtils.copyProperties(xdkh0011DataReqDto, cusIndivContact);
            //居住时间
            cusIndivContact.setResiTime(xdkh0011DataReqDto.getResiYears());
            //是否农户字典项映射
            String isAgri = xdkh0011DataReqDto.getIsAgri();
            cusIndivContact.setIsAgri(isAgri);

            cusIndivContactMapper.updateByPrimaryKeySelective(cusIndivContact);
            //更新客户属性信息
            BeanUtils.copyProperties(xdkh0011DataReqDto, cusIndivAttr);
            //字典项映射
            //是否强村富民贷款
            String isLoan = xdkh0011DataReqDto.getIsLoan();
            cusIndivAttr.setIsLoan(isLoan);
            cusIndivAttrMapper.updateByPrimaryKeySelective(cusIndivAttr);

            xdkh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_S);
            xdkh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_S);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, e.getMessage());
            xdkh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xdkh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, e.getMessage());
            xdkh0011DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xdkh0011DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw new Exception(EpbEnum.EPB099999.value);
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011DataRespDto));
        return xdkh0011DataRespDto;
    }
}
