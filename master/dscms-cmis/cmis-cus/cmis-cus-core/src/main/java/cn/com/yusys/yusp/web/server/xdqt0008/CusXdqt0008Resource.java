package cn.com.yusys.yusp.web.server.xdqt0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdqt0008.req.Xdqt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0008.resp.Xdqt0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdqt0008.Xdqt0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:贷乐悠金卡关联人查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0008:贷款申请预约（企业客户）")
@RestController
@RequestMapping("/api/bizqt4bsp")
public class CusXdqt0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdqt0008Resource.class);

    @Autowired
    private Xdqt0008Service xdqt0008Service;

    /**
     * 交易码：xdqt0008
     * 交易描述：乐悠金卡关联人查询
     *
     * @param xdqt0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("乐悠金卡关联人查询")
    @PostMapping("/xdqt0008")
    protected @ResponseBody
    ResultDto<Xdqt0008DataRespDto> xdqt0008(@Validated @RequestBody Xdqt0008DataReqDto xdqt0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, JSON.toJSONString(xdqt0008DataReqDto));
        Xdqt0008DataRespDto xdqt0008DataRespDto = new Xdqt0008DataRespDto();// 响应Dto:贷款申请预约（企业客户）
        ResultDto<Xdqt0008DataRespDto> xdqt0008DataResultDto = new ResultDto<>();
        try {
            String certCode = xdqt0008DataReqDto.getCertCode();
            if (StringUtils.isBlank(certCode)) {
                xdqt0008DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdqt0008DataResultDto.setMessage("证件号【certCode】不能为空！");
                return xdqt0008DataResultDto;
            }
            // 从xdqt0008DataReqDto获取业务值进行业务逻辑处理
            xdqt0008DataRespDto = xdqt0008Service.xdqt0008(xdqt0008DataReqDto);
            // 封装xdqt0008DataResultDto中正确的返回码和返回信息
            xdqt0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdqt0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, e.getMessage());
            // 封装xdqt0008DataResultDto中异常返回码和返回信息
            xdqt0008DataResultDto.setCode(EcbEnum.ECB019999.key);
            xdqt0008DataResultDto.setMessage(EcbEnum.ECB019999.value);
        }
        // 封装xdqt0008DataRespDto到xdqt0008DataResultDto中
        xdqt0008DataResultDto.setData(xdqt0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, JSON.toJSONString(xdqt0008DataResultDto));
        return xdqt0008DataResultDto;
    }
}
