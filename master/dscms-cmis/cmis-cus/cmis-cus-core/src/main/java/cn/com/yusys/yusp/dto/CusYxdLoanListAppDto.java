package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusYxdLoanListApp
 * @类描述: cus_yxd_loan_list_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:30:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusYxdLoanListAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 申请日期 **/
	private String appDate;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 手机号码 **/
	private String phoneNo;
	
	/** 学历 **/
	private String edu;
	
	/** 性别 **/
	private String sex;
	
	/** 是否本地户 **/
	private String isRegion;
	
	/** 居住年限 **/
	private String resiYears;
	
	/** 婚姻状态 **/
	private String marStatus;
	
	/** 年收入 **/
	private java.math.BigDecimal yearn;
	
	/** 工作年限 **/
	private String cprtYears;
	
	/** 职务 **/
	private String duty;
	
	/** 工作单位 **/
	private String cprtName;
	
	/** 居住地址 **/
	private String resiAddr;
	
	/** 申请金额 **/
	private java.math.BigDecimal appAmt;
	
	/** 年利率 **/
	private java.math.BigDecimal yearRate;
	
	/** 经办人 **/
	private String huser;
	
	/** 经办机构 **/
	private String handOrg;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 影像编号 **/
	private String imageNo;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新时间 **/
	private String updDate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}
	
    /**
     * @return AppDate
     */	
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param phoneNo
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo == null ? null : phoneNo.trim();
	}
	
    /**
     * @return PhoneNo
     */	
	public String getPhoneNo() {
		return this.phoneNo;
	}
	
	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu == null ? null : edu.trim();
	}
	
    /**
     * @return Edu
     */	
	public String getEdu() {
		return this.edu;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex == null ? null : sex.trim();
	}
	
    /**
     * @return Sex
     */	
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param isRegion
	 */
	public void setIsRegion(String isRegion) {
		this.isRegion = isRegion == null ? null : isRegion.trim();
	}
	
    /**
     * @return IsRegion
     */	
	public String getIsRegion() {
		return this.isRegion;
	}
	
	/**
	 * @param resiYears
	 */
	public void setResiYears(String resiYears) {
		this.resiYears = resiYears == null ? null : resiYears.trim();
	}
	
    /**
     * @return ResiYears
     */	
	public String getResiYears() {
		return this.resiYears;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus == null ? null : marStatus.trim();
	}
	
    /**
     * @return MarStatus
     */	
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param yearn
	 */
	public void setYearn(java.math.BigDecimal yearn) {
		this.yearn = yearn;
	}
	
    /**
     * @return Yearn
     */	
	public java.math.BigDecimal getYearn() {
		return this.yearn;
	}
	
	/**
	 * @param cprtYears
	 */
	public void setCprtYears(String cprtYears) {
		this.cprtYears = cprtYears == null ? null : cprtYears.trim();
	}
	
    /**
     * @return CprtYears
     */	
	public String getCprtYears() {
		return this.cprtYears;
	}
	
	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty == null ? null : duty.trim();
	}
	
    /**
     * @return Duty
     */	
	public String getDuty() {
		return this.duty;
	}
	
	/**
	 * @param cprtName
	 */
	public void setCprtName(String cprtName) {
		this.cprtName = cprtName == null ? null : cprtName.trim();
	}
	
    /**
     * @return CprtName
     */	
	public String getCprtName() {
		return this.cprtName;
	}
	
	/**
	 * @param resiAddr
	 */
	public void setResiAddr(String resiAddr) {
		this.resiAddr = resiAddr == null ? null : resiAddr.trim();
	}
	
    /**
     * @return ResiAddr
     */	
	public String getResiAddr() {
		return this.resiAddr;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return AppAmt
     */	
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param yearRate
	 */
	public void setYearRate(java.math.BigDecimal yearRate) {
		this.yearRate = yearRate;
	}
	
    /**
     * @return YearRate
     */	
	public java.math.BigDecimal getYearRate() {
		return this.yearRate;
	}
	
	/**
	 * @param huser
	 */
	public void setHuser(String huser) {
		this.huser = huser == null ? null : huser.trim();
	}
	
    /**
     * @return Huser
     */	
	public String getHuser() {
		return this.huser;
	}
	
	/**
	 * @param handOrg
	 */
	public void setHandOrg(String handOrg) {
		this.handOrg = handOrg == null ? null : handOrg.trim();
	}
	
    /**
     * @return HandOrg
     */	
	public String getHandOrg() {
		return this.handOrg;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo == null ? null : imageNo.trim();
	}
	
    /**
     * @return ImageNo
     */	
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}