package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivRankScore
 * @类描述: cus_indiv数据实体类
 * @功能描述: 
 * @创建人: zhoumw
 * @创建时间: 2021-04-11 14:16:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusIndivRankScoreDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 性别 **/
	private String sex;
	/** 婚姻状况 **/
	private String marStatus;
	/** 成为我行客户时间 **/
	private String initLoanDate;
	/** 经营企业名称 **/
	private String unitName;
	/** 手机号码 **/
	private String mobileNo;
	/** 客户编号 **/
	private String cusId;
	
	/** 开始日期 **/
	private String beginDate;
	
	/** 结束日期 **/
	private String endDate;
	
	/** 所在学校 **/
	private String ubietySchool;
	
	/** 所在院系 **/
	private String ubietyDepartment;
	
	/** 专业 **/
	private String major;
	
	/** 最高学历 STD_ZB_EDU **/
	private String indivEdt;
	
	/** 最高学位 STD_ZB_DEGREE **/
	private String indivDgr;
	
	/** 学历证书号 **/
	private String eduCode;

	/** 客户名称 **/
	private String cusName;

	/** 证件类型 **/
	private String certType;

	/** 证件号码 **/
	private String certCode;

	/** 年龄 **/
	private String age;

	/** 职业 **/
	private String occ;

	/** 成为我行客户时间 **/
	private String cusDuration;

	/** 经营企业名称 **/
	private String companyName;

	/** 经营企业统一社会信用代码 **/
	private String unifyCreditCode;

	/** 单位性质 **/
	private String indivComTyp;

	/** 单位所属行业 **/
	private String indivComTrade;
	
	/** 学位证书号 **/
	private String dgrCode;
	
	/** 备注 **/
	private String remark;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	/** 是否有子女 **/
	private String isHaveChildren;
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param beginDate
	 */
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate == null ? null : beginDate.trim();
	}
	
    /**
     * @return BeginDate
     */	
	public String getBeginDate() {
		return this.beginDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param ubietySchool
	 */
	public void setUbietySchool(String ubietySchool) {
		this.ubietySchool = ubietySchool == null ? null : ubietySchool.trim();
	}
	
    /**
     * @return UbietySchool
     */	
	public String getUbietySchool() {
		return this.ubietySchool;
	}
	
	/**
	 * @param ubietyDepartment
	 */
	public void setUbietyDepartment(String ubietyDepartment) {
		this.ubietyDepartment = ubietyDepartment == null ? null : ubietyDepartment.trim();
	}
	
    /**
     * @return UbietyDepartment
     */	
	public String getUbietyDepartment() {
		return this.ubietyDepartment;
	}
	
	/**
	 * @param major
	 */
	public void setMajor(String major) {
		this.major = major == null ? null : major.trim();
	}
	
    /**
     * @return Major
     */	
	public String getMajor() {
		return this.major;
	}
	
	/**
	 * @param indivEdt
	 */
	public void setIndivEdt(String indivEdt) {
		this.indivEdt = indivEdt == null ? null : indivEdt.trim();
	}
	
    /**
     * @return IndivEdt
     */	
	public String getIndivEdt() {
		return this.indivEdt;
	}
	
	/**
	 * @param indivDgr
	 */
	public void setIndivDgr(String indivDgr) {
		this.indivDgr = indivDgr == null ? null : indivDgr.trim();
	}
	
    /**
     * @return IndivDgr
     */	
	public String getIndivDgr() {
		return this.indivDgr;
	}
	
	/**
	 * @param eduCode
	 */
	public void setEduCode(String eduCode) {
		this.eduCode = eduCode == null ? null : eduCode.trim();
	}
	
    /**
     * @return EduCode
     */	
	public String getEduCode() {
		return this.eduCode;
	}
	
	/**
	 * @param dgrCode
	 */
	public void setDgrCode(String dgrCode) {
		this.dgrCode = dgrCode == null ? null : dgrCode.trim();
	}
	
    /**
     * @return DgrCode
     */	
	public String getDgrCode() {
		return this.dgrCode;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	public void setIsHaveChildren(String isHaveChildren) {
		this.isHaveChildren = isHaveChildren == null ? null : isHaveChildren.trim();
	}

	/**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}

	public String getAge() {
		return this.age;
	}

	public String getOcc() {
		return this.occ;
	}

	public String getCusDuration() {
		return this.cusDuration;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}

	public String getIndivComTyp() {
		return this.indivComTyp;
	}

	public String getIndivComTrade() {
		return this.indivComTrade;
	}

	public void setAge(String age) {
		this.age = age == null ? null : age.trim();
	}

	public void setOcc(String occ) {
		this.occ = occ == null ? null : occ.trim();
	}

	public void setCusDuration(String cusDuration) {
		this.cusDuration = cusDuration == null ? null : cusDuration.trim();
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName == null ? null : companyName.trim();
	}

	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode == null ? null : unifyCreditCode.trim();
	}

	public void setIndivComTyp(String indivComTyp) {
		this.indivComTyp = indivComTyp == null ? null : indivComTyp.trim();
	}

	public void setIndivComTrade(String indivComTrade) {
		this.indivComTrade = indivComTrade == null ? null : indivComTrade.trim();
	}

	public String getCusName() {
		return this.cusName;
	}

	public String getCertCode() {
		return this.certCode;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}

	public String getIsHaveChildren() {
		return isHaveChildren;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}

	public void setInitLoanDate(String initLoanDate) {
		this.initLoanDate = initLoanDate;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getSex() {
		return sex;
	}

	public String getMarStatus() {
		return marStatus;
	}

	public String getInitLoanDate() {
		return initLoanDate;
	}

	public String getUnitName() {
		return unitName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertType() {
		return certType;
	}
}