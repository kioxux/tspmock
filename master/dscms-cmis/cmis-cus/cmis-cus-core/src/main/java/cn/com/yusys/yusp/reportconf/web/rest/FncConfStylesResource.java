package cn.com.yusys.yusp.reportconf.web.rest;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.web.rest.CommonResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.reportconf.domain.FncConfStyles;
import cn.com.yusys.yusp.reportconf.service.FncConfStylesService;
//import cn.com.yusys.yusp.trace.annotation.TraceBaggage;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.service.CommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfStylesResource
 * @类描述: #资源类
 * @功能描述: 财务报表样式配置
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-20 18:19:46
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/nrcs-cms/fncconfstyles")
@Api(tags = "FncConfStylesResource", description = "财务报表样式配置")
public class FncConfStylesResource extends CommonResource<FncConfStyles, String> {
	@Autowired
	private FncConfStylesService fncConfStylesService;

	@Override
	protected CommonService getCommonService() {
		return fncConfStylesService;
	}

	/**
	 * 财务报表样式配置查询
	 * 
	 * @param model
	 *            条件封装
	 * @return
	 */
	@GetMapping("/q/fncconfstyles/list")
	@ApiOperation("NRCS603171财务报表样式配置查询")
//	@TraceBaggage(functionCode = "NRCS603171")
	public ResultDto<List<FncConfStyles>> queryFncConfStylesList(QueryModel model) {
		return new ResultDto<List<FncConfStyles>>(fncConfStylesService.queryFncConfStylesList(model));
	}

	/**
	 * 财务报表样式配置全量查询
	 * 
	 * @param model
	 *            条件封装
	 * @return
	 */
	@GetMapping("/q/fncconfstyles/all/list")
	@ApiOperation("NRCS602941财务报表样式配置全量查询")
//	@TraceBaggage(functionCode = "NRCS602941")
	public ResultDto<List<FncConfStyles>> queryFncConfStylesListAll(QueryModel model) {
		return new ResultDto<List<FncConfStyles>>(fncConfStylesService.queryFncConfStylesListAll(model));
	}

	/**
	 * 财务报表样式配置新增
	 * 
	 * @param fncConfStyles
	 *            实体类
	 * @return
	 */
	@PostMapping("/s/fncconfstyles/add")
	@ApiOperation("NRCS603173财务报表样式配置新增")
//	@TraceBaggage(functionCode = "NRCS603173")
	public ResultDto<Integer> addFncConfStyles(@RequestBody FncConfStyles fncConfStyles) {
		return new ResultDto<Integer>(fncConfStylesService.addFncConfStyles(fncConfStyles));
	}

	/**
	 * 财务报表样式配置删除
	 * 
	 * @param styleId
	 *            主键
	 * @return
	 */
	@PostMapping("/s/fncconfstyles/delete")
	@ApiOperation("NRCS603194财务报表样式配置删除")
//	@TraceBaggage(functionCode = "NRCS603194")
	public ResultDto<Integer> deleteFncConfStyles(@RequestBody HashMap<String, String> map) {
		return new ResultDto<Integer>(fncConfStylesService.deleteFncConfStyles(map.get("styleId")));
	}

	/**
	 * 财务报表样式配置修改
	 * 
	 * @param fncConfStyles
	 *            实体类
	 * @return
	 */
	@PostMapping("/s/fncconfstyles/update")
	@ApiOperation("NRCS603185财务报表样式配置修改")
//	@TraceBaggage(functionCode = "NRCS603185")
	public ResultDto<Integer> updateFncConfStyles(@RequestBody FncConfStyles fncConfStyles) {
		return new ResultDto<Integer>(fncConfStylesService.updateFncConfStyles(fncConfStyles));
	}

	/**
	 * 财务报表样式配置查看
	 * 
	 * @param styleId
	 *            主键
	 * @return
	 */
	/*
	 * @GetMapping("/q/fncconfstyles/detail")
	 * 
	 * @ApiOperation("NRCS603202财务报表样式配置查看")
	 * 
	 * @TraceBaggage(functionCode = "NRCS603202") public ResultDto<FncConfStyles>
	 * queryFncConfStylesByKey(@RequestBody HashMap<String, String> map) { return
	 * new
	 * ResultDto<FncConfStyles>(fncConfStylesService.queryFncConfStylesByKey(map.get
	 * ("styleId"))); }
	 */
}
