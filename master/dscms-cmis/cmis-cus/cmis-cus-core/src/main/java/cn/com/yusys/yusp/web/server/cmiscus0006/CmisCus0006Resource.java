package cn.com.yusys.yusp.web.server.cmiscus0006;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.enums.cache.CacheKeyEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusBaseService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类: 查询客户基本信息
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "CmisCus0006:查询客户基本信息")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0006Resource.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CusBaseService cusBaseService;
    @Autowired
    private YuspRedisTemplate yuspRedisTemplate;


    /**
     * 交易码：cmiscus0006
     * 交易描述：查询客户基本信息
     *
     * @param cmisCus0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户基本信息")
    @PostMapping("/cmiscus0006")
    protected @ResponseBody
    ResultDto<CmisCus0006RespDto> cmiscus0006(@Validated @RequestBody CmisCus0006ReqDto cmisCus0006ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
        CmisCus0006RespDto cmisCus0006RespDto = new CmisCus0006RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0006RespDto> cmisCus0006ResultDto = new ResultDto<>();
        String cusId = cmisCus0006ReqDto.getCusId();
        String certCode = cmisCus0006ReqDto.getCertCode();
        try {
            if (StringUtils.isBlank(cusId) && StringUtils.isBlank(certCode)) {
                cmisCus0006ResultDto.setCode(EpbEnum.EPB099999.key);
                cmisCus0006ResultDto.setMessage("参数不能为空");
                return cmisCus0006ResultDto;
            }
            QueryModel queryModel = new QueryModel();
            if (StringUtils.isNotBlank(certCode)) {
                queryModel.addCondition("certCode", certCode);
            }
            if (StringUtils.isNotBlank(cusId)) {
                queryModel.addCondition("cusId", cusId);
            }
            List<CusBase> cusBaseList = cusBaseService.selectAll(queryModel);
            if (CollectionUtils.nonEmpty(cusBaseList)) {
                List<CusBaseDto> collect = cusBaseList.parallelStream().map(cusBase -> {
                    CusBaseDto cusBaseDto = new CusBaseDto();
                    BeanUtils.copyProperties(cusBase, cusBaseDto);
                    return cusBaseDto;
                }).collect(Collectors.toList());
                cmisCus0006RespDto.setCusBaseList(collect);
            }
            // 封装cmisCus0006ResultDto中正确的返回码和返回信息
            cmisCus0006ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0006ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, e.getMessage());
            // 封装cmisCus0006ResultDto中异常返回码和返回信息
            cmisCus0006ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0006ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0006RespDto到cmisCus0006ResultDto中
        cmisCus0006ResultDto.setData(cmisCus0006RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ResultDto));
        return cmisCus0006ResultDto;
    }
}
