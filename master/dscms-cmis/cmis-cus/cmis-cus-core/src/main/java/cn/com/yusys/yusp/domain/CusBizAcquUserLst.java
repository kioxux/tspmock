/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBizAcquUserLst
 * @类描述: cus_biz_acqu_user_lst数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-05-05 09:33:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_biz_acqu_user_lst")
public class CusBizAcquUserLst extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "ASSIGN_APP_SERNO", unique = false, nullable = false, length = 40)
	private String assignAppSerno;
	
	/** 业务权获得人用户号 **/
	@Column(name = "BIZ_RIGHTS_RECIPIENT_USER", unique = false, nullable = true, length = 20)
	private String bizRightsRecipientUser;
	
	/** 业务权获得人姓名 **/
	@Column(name = "BIZ_RIGHTS_RECIPIENT_USER_NAME", unique = false, nullable = true, length = 20)
	private String bizRightsRecipientUserName;
	
	/** 业务权获得人所属机构 **/
	@Column(name = "BIZ_RIGHTS_RECIPIENT_BELG_ORG", unique = false, nullable = true, length = 20)
	private String bizRightsRecipientBelgOrg;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param assignAppSerno
	 */
	public void setAssignAppSerno(String assignAppSerno) {
		this.assignAppSerno = assignAppSerno;
	}
	
    /**
     * @return assignAppSerno
     */
	public String getAssignAppSerno() {
		return this.assignAppSerno;
	}
	
	/**
	 * @param bizRightsRecipientUser
	 */
	public void setBizRightsRecipientUser(String bizRightsRecipientUser) {
		this.bizRightsRecipientUser = bizRightsRecipientUser;
	}
	
    /**
     * @return bizRightsRecipientUser
     */
	public String getBizRightsRecipientUser() {
		return this.bizRightsRecipientUser;
	}
	
	/**
	 * @param bizRightsRecipientUserName
	 */
	public void setBizRightsRecipientUserName(String bizRightsRecipientUserName) {
		this.bizRightsRecipientUserName = bizRightsRecipientUserName;
	}
	
    /**
     * @return bizRightsRecipientUserName
     */
	public String getBizRightsRecipientUserName() {
		return this.bizRightsRecipientUserName;
	}
	
	/**
	 * @param bizRightsRecipientBelgOrg
	 */
	public void setBizRightsRecipientBelgOrg(String bizRightsRecipientBelgOrg) {
		this.bizRightsRecipientBelgOrg = bizRightsRecipientBelgOrg;
	}
	
    /**
     * @return bizRightsRecipientBelgOrg
     */
	public String getBizRightsRecipientBelgOrg() {
		return this.bizRightsRecipientBelgOrg;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}