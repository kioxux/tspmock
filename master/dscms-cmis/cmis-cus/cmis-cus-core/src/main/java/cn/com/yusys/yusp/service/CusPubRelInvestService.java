/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusManaTask;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.enums.out.ecif.EcifCertTypeEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusPubRelInvest;
import cn.com.yusys.yusp.repository.mapper.CusPubRelInvestMapper;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusPubRelInvestService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-22 21:11:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CusPubRelInvestService {
    private static final Logger log = LoggerFactory.getLogger(CusPubRelInvestService.class);

    @Autowired
    private CusPubRelInvestMapper cusPubRelInvestMapper;

    @Autowired
    private CusBaseMapper cusBaseMapper;

    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    @Autowired
    private CusBaseService cusBaseService;

    @Autowired
    private CusCorpService cusCorpService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusPubRelInvest selectByPrimaryKey(String pkId) {
        return cusPubRelInvestMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: queryBycusId
     * @方法描述: 根据客户号查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：zhoumw
     */

    public List<CusPubRelInvest> queryBycusId(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusPubRelInvest> records = (List<CusPubRelInvest>)cusPubRelInvestMapper.selectByModel(model);
        PageHelper.clearPage();
        return records;
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusPubRelInvest> selectAll(QueryModel model) {
        List<CusPubRelInvest> records = (List<CusPubRelInvest>) cusPubRelInvestMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusPubRelInvest> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusPubRelInvest> list = cusPubRelInvestMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusPubRelInvest record) {
        return cusPubRelInvestMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusPubRelInvest record) {
        return cusPubRelInvestMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusPubRelInvest record) {
        return cusPubRelInvestMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusPubRelInvest record) {
        return cusPubRelInvestMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusPubRelInvestMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusPubRelInvestMapper.deleteByIds(ids);
    }

    /**
     * 获取对外投资信息
     * @param certType
     * @param certCode
     * @return
     */
    public CusPubRelInvest selectCusCorpRelByCodeAndType(String certType, String certCode,String cusId) {
        CusPubRelInvest cusPubRelInvest = cusPubRelInvestMapper.selectCusCorpRelByCodeAndType(certType,certCode,cusId);
        return cusPubRelInvest;
    }

    /**
     * 获取对外投资关系
     * @param cusCorpRelationsRequestDto
     * @return
     */
    public CusCorpPubDto cusCorpPubRelations(CusCorpRelationsRequestDto cusCorpRelationsRequestDto) {
        List<CusCorpRelationsDto> cusCorpRelationsDtos = cusPubRelInvestMapper.selectCusCorpPubComDto(cusCorpRelationsRequestDto);
        if(CollectionUtils.isEmpty(cusCorpRelationsDtos) && cusCorpRelationsDtos.size() <= 0){
            return null;
        }

        CusCorpPubDto cusCorpPubDto = new CusCorpPubDto();
        cusCorpPubDto.setUpperCusId(cusCorpRelationsRequestDto.getCusId());
        cusCorpPubDto.setCusCorpRelationsDtos(cusCorpRelationsDtos);
        return cusCorpPubDto;
    }
    /**
     * @方法名称: save
     * @方法描述: 保存
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */

    public int save(CusPubRelInvest record) {
        int count=0;
        if(record!=null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            // 主键
            record.setPkId(StringUtils.getUUID());
            // 登记人
            record.setInputId(inputId);
            // 登记机构
            record.setInputBrId(inputBrId);
            // 登记日期
            record.setInputDate(inputDate);
            // 登记时间
            record.setCreateTime(createTime);
            // 修改人
            record.setUpdId(inputId);
            // 修改机构
            record.setUpdBrId(inputBrId);
            // 修改日期
            record.setUpdDate(inputDate);
            // 修改时间
            record.setUpdateTime(createTime);
            count=cusPubRelInvestMapper.insert(record);
        }
        return count;
    }

    /**
     * 根据被投资人客户编号查询关联客户信息
     * @param cusId
     * @return
     */
    public List<Map<String,String>> queryCusInfoByCusId(String cusId) {
        return cusPubRelInvestMapper.queryCusInfoByCusId(cusId);
    }

    /**
     * @return
     * @函数名称:openAccount
     * @函数描述: 开户调用接口逻辑
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional(rollbackFor = Exception.class)
    public CusPubRelInvest openAccount(CusCorpDto cusCorpDto) throws Exception {
        /*
         * 1.检查客户是否在本地已存在
         */
        checkOnthWay(cusCorpDto);
        /*
         * 2.证件类型为组织机构代码和统一社会信用证
         *   调用外部查询接口查询要素是否一致
         */
        // zsnew 调用接口验证
        boolean insCode = "Q".equals(cusCorpDto.getCertType());
        boolean certCode = "R".equals(cusCorpDto.getCertType());
        //证件类型为组织机构代码和统一社会信用证才查询，其他不查询
        if (insCode || certCode) {
            //调用外部接口查询 客户要素是否一致
            checkCertCodeAndName(cusCorpDto, insCode, certCode);
        }
        /*
         *  3.存在性查询
         *  调用ECIf接口查询客户是否存在 ，存在则返回客户号
         */
        CusCorpDto cusCorpDtos = queryG10501(cusCorpDto);
        /*
         * 4.判断客户是否存在 不存在则调用ecif接口开户 返回客户号
         */
        if (Objects.isNull(cusCorpDtos)) {
            cusCorpDtos = ecifCreateCus(cusCorpDto);
        }
        //5. ECIF开户成功，插入客户相关信息
        CusPubRelInvest cusPubRelInvest = new CusPubRelInvest();
        BeanUtils.copyProperties(cusCorpDtos,cusPubRelInvest);
        cusPubRelInvest.setCusNameRel(cusCorpDtos.getCusName());
        cusPubRelInvest.setCusIdRel(cusCorpDtos.getCusId());
        return cusPubRelInvest;
    }

    /**
     * 校验客户已存在
     *
     * @param cusCorpDto
     * @throws Exception
     */
    private void checkOnthWay(CusCorpDto cusCorpDto) throws Exception {
        QueryModel model = new QueryModel();
        model.addCondition("certType", cusCorpDto.getCertType());
        model.addCondition("certCode", cusCorpDto.getCertCode());
        List<CusBase> exists = cusBaseMapper.selectByModel(model);
        if (!exists.isEmpty()) {
            throw new Exception("证件号码:" + cusCorpDto.getCertCode() + "的客户已存在！");
        }
    }

    /**
     * 调用外部接口检查客户要素
     *
     * @param cusCorpDto
     */
    private void checkCertCodeAndName(CusCorpDto cusCorpDto, boolean insCode, boolean certCode) {
        ZsnewReqDto zsnewReqDto = new ZsnewReqDto();
        if (insCode) {
            zsnewReqDto.setOrgcode(cusCorpDto.getInsCode());     //组织机构代码 Q
        }
        if (certCode) {
            zsnewReqDto.setCreditcode(cusCorpDto.getCertCode()); //统一信用代码 R
        }
        zsnewReqDto.setName(cusCorpDto.getCusName());//企业名称
        zsnewReqDto.setEntstatus(cusCorpDto.getOperStatus());  //企业经营状态，1：在营，2：非在营
        zsnewReqDto.setId(StringUtils.EMPTY);         //中数企业ID
        zsnewReqDto.setRegno(cusCorpDto.getRegiCode());    //企业注册号
        zsnewReqDto.setVersion(StringUtils.EMPTY);  //高管识别码版本号,返回高管识别码时生效
        ResultDto<ZsnewRespDto> zsnewResultDto = dscms2OuterdataClientService.zsnew(zsnewReqDto);
        String zsnewCode = Optional.ofNullable(zsnewResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String zsnewMeesage = Optional.ofNullable(zsnewResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        ZsnewRespDto zsnewRespDto = null;
        if (SuccessEnum.CMIS_SUCCSESS.key.equals(zsnewCode)) {
            Map<String, Object> data = (Map<String, Object>) zsnewResultDto.getData().getData();
            if ("200".equals(Objects.toString(data.get("CODE")))) {
                //查询成功
                log.info("查询成功");
            } else {
                //没有查询到
                log.info("没有查询到");
            }
        } else {
            log.error("接口交易失败");
        }
    }

    private CusCorpDto queryG10501(CusCorpDto cusCorpDto) {
        // g10501 对公及同业客户清单查询
        /**
         * 识别方式:
         * 1- 按证件类型、证件号码查询
         * 2- 按证件号码查询
         * 3- 按客户编号查询
         * 4- 按客户名称模糊查询
         * 以下接口必填一种查询方式；
         */
        G10501ReqDto g10501ReqDto = new G10501ReqDto();
        // TODO StringUtils.EMPTY的实际值待确认 开始
        g10501ReqDto.setResotp("2");// 识别方式
//        g10501ReqDto.setCustna(cusCorpDto.getCusName());// 客户名称
//        g10501ReqDto.setIdtftp(cusCorpDto.getCertType());// 证件类型
        g10501ReqDto.setIdtfno(cusCorpDto.getCertCode());// 证件号码
//        g10501ReqDto.setCustst(cusCorpDto.getCusState());// 客户状态
        g10501ReqDto.setBginnm(StringUtils.EMPTY);// 起始笔数
        g10501ReqDto.setQurynm(StringUtils.EMPTY);// 查询笔数
        // TODO StringUtils.EMPTY的实际值待确认 结束
        //TODO 完善后续逻辑
        ResultDto<G10501RespDto> g10501ResultDto = dscms2EcifClientService.g10501(g10501ReqDto);
        String g10501Code = Optional.ofNullable(g10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g10501Meesage = Optional.ofNullable(g10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G10501RespDto g10501RespDto = null;
        if (Objects.equals(g10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            // TODO 获取相关的值并解析
            g10501RespDto = g10501ResultDto.getData();
            log.info(String.valueOf(g10501RespDto));
            if (g10501RespDto.getListnm() > 0) {
                cusCorpDto.setCusId(g10501RespDto.getListArrayInfo().get(0).getCustno());
                cusCorpDto.setCusName(g10501RespDto.getListArrayInfo().get(0).getCustna());

                CusBase cusBase = new CusBase();
                CusCorp cusCorp = new CusCorp();
                BeanUtils.copyProperties(cusCorpDto,cusBase);
                BeanUtils.copyProperties(cusCorpDto,cusCorp);
                cusBaseService.insertSelective(cusBase);
                cusCorpService.insertSelective(cusCorp);
                return cusCorpDto;
            }else{
                return null;
            }
        } else {
            // TODO 抛出错误异常
            throw BizException.error(null,g10501Code,g10501Meesage);
        }
    }

    /**
     * Ecif 开户接口
     * @param cusCorpDto
     */
    private CusCorpDto ecifCreateCus(CusCorpDto cusCorpDto) {
        String cusId = "";
        // 同业客户开户
        G00102ReqDto g00102ReqDto = new G00102ReqDto();
        /**
         * 1	正常客户
         * 2	注销客户
         * 3	临时客户
         */
        if ("A02".equals(cusCorpDto.getBizType())) {
            //临时客户开户
            g00102ReqDto.setCustst("3");//客户状态
        } else {
            //正式客户开户
            g00102ReqDto.setCustst("1");//客户状态
            //信贷系统不允许开立ECIF不存在的正式客户！
            throw BizException.error(null,EcsEnum.ECS040009.key,EcsEnum.ECS040009.value);
        }
        /**
         * 01	对公客户
         * 02	同业客户
         */
        g00102ReqDto.setCusttp("01");//客户类型               ,
        g00102ReqDto.setCustsb(StringUtils.EMPTY);//对公客户类型细分           ,
        g00102ReqDto.setIdtftp(EcifCertTypeEnum.lookup(cusCorpDto.getCertType()));//证件类型   TODO 修改码值            ,
        g00102ReqDto.setIdtfno(cusCorpDto.getCertCode());//证件号码               ,
        g00102ReqDto.setEfctdt(StringUtils.EMPTY);//证件生效日期             ,
        g00102ReqDto.setBusisp(StringUtils.EMPTY);//经营范围  提交之后更新
        g00102ReqDto.setRgprov(StringUtils.EMPTY);//注册地址所在省
        g00102ReqDto.setRgcity(StringUtils.EMPTY);//注册地址所在市
        g00102ReqDto.setRgerea(StringUtils.EMPTY);//注册地址所在地区
        g00102ReqDto.setRegadr(StringUtils.EMPTY);//注册地址
        g00102ReqDto.setCustna(cusCorpDto.getCusName());//客户名称
        g00102ReqDto.setJgxyno(StringUtils.EMPTY);//社会信用代码
        // TODO StringUtils.EMPTY的实际值待确认 结束
        // TODO 完善后续逻辑
        ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
        String g00202Code = Optional.ofNullable(g00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g00202Meesage = Optional.ofNullable(g00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G00102RespDto g00102RespDto = null;
        if (Objects.equals(g00202Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            // TODO 获取相关的值并解析
            g00102RespDto = g00102ResultDto.getData();
            cusCorpDto.setCusId(g00102RespDto.getCustno());
            cusCorpDto.setCusName(g00102ReqDto.getCustna());

            CusBase cusBase = new CusBase();
            CusCorp cusCorp = new CusCorp();
            BeanUtils.copyProperties(cusCorpDto,cusBase);
            BeanUtils.copyProperties(cusCorpDto,cusCorp);
            cusBaseService.insertSelective(cusBase);
            cusCorpService.insertSelective(cusCorp);
        } else {
            // TODO 抛出错误异常
            throw BizException.error(null,g00202Code,g00202Meesage);
        }
        return cusCorpDto;
    }
}
