/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CusIndivAttrDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusIndivAttr;
import cn.com.yusys.yusp.service.CusIndivAttrService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivAttrResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-09 17:41:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags= "个人客户客户属性")
@RestController
@RequestMapping("/api/cusindivattr")
public class CusIndivAttrResource {
    @Autowired
    private CusIndivAttrService cusIndivAttrService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusIndivAttr>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusIndivAttr> list = cusIndivAttrService.selectAll(queryModel);
        return new ResultDto<List<CusIndivAttr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusIndivAttr>> index(QueryModel queryModel) {
        List<CusIndivAttr> list = cusIndivAttrService.selectByModel(queryModel);
        return new ResultDto<List<CusIndivAttr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cusId}")
    protected ResultDto<CusIndivAttr> show(@PathVariable("cusId") String cusId) {
        CusIndivAttr cusIndivAttr = cusIndivAttrService.selectByPrimaryKey(cusId);
        return new ResultDto<CusIndivAttr>(cusIndivAttr);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "个人客户属性查询")
    @PostMapping("/queryCusIndivAttr")
    protected ResultDto<CusIndivAttr> queryCusIndivAttr(@Validated @RequestBody CusIndivAttr cusIndivAttr) {
        CusIndivAttr cusIndivAttrs = cusIndivAttrService.selectBycusId(cusIndivAttr.getCusId());
        return new ResultDto<CusIndivAttr>(cusIndivAttrs);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusIndivAttr> create(@RequestBody CusIndivAttr cusIndivAttr) throws URISyntaxException {
        cusIndivAttrService.insert(cusIndivAttr);
        return new ResultDto<CusIndivAttr>(cusIndivAttr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusIndivAttr cusIndivAttr) throws URISyntaxException {
        int result = cusIndivAttrService.update(cusIndivAttr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cusId}")
    protected ResultDto<Integer> delete(@PathVariable("cusId") String cusId) {
        int result = cusIndivAttrService.deleteByPrimaryKey(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusIndivAttrService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:save
     * @函数描述: 客户属性保存
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "个人客户属性保存")
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody CusIndivAttr cusIndivAttr) throws URISyntaxException {
        int result =cusIndivAttrService.save(cusIndivAttr);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：queryCusIndivAttrByCusId
     * @方法描述：
     * @创建人：zhangliang15
     * 根据客户编号查询个人客户属性信息表
     */
    @PostMapping("/queryCusIndivAttrByCusId")
    protected ResultDto<CusIndivAttrDto> queryCusIndivAttrByCusId(@RequestBody String cusId) {
        CusIndivAttrDto cusIndivAttr = cusIndivAttrService.queryCusIndivAttrByCusId(cusId);
        return new ResultDto<>(cusIndivAttr);
    }

}
