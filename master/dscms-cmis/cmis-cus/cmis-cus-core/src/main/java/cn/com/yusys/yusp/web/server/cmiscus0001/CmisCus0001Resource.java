package cn.com.yusys.yusp.web.server.cmiscus0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0001.req.CmisCus0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscus0001.CmisCus0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 集团成员列表查询
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0001:集团成员列表查询")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0001Resource.class);

    @Autowired
    private CmisCus0001Service cmisCus0001Service;

    /**
     * 交易码：cmiscus0001
     * 交易描述：集团成员列表查询
     *
     * @param cmisCus0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("集团成员列表查询")
    @PostMapping("/cmiscus0001")
    protected @ResponseBody
    ResultDto<CmisCus0001RespDto> cmiscus0001(@Validated @RequestBody CmisCus0001ReqDto cmisCus0001ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0001.key, DscmsEnum.TRADE_CODE_CMISCUS0001.value, JSON.toJSONString(cmisCus0001ReqDto));
        CmisCus0001RespDto cmisCus0001RespDto = new CmisCus0001RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0001RespDto> cmisCus0001ResultDto = new ResultDto<>();
        try {
            // 封装cmisCus0001ResultDto中正确的返回码和返回信息
            cmisCus0001RespDto = cmisCus0001Service.execute(cmisCus0001ReqDto);
            cmisCus0001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0001ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0001.key, DscmsEnum.TRADE_CODE_CMISCUS0001.value, e.getMessage());
            // 封装cmisCus0001ResultDto中异常返回码和返回信息
            cmisCus0001ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0001ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0001RespDto到cmisCus0001ResultDto中
        cmisCus0001ResultDto.setData(cmisCus0001RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0001.key, DscmsEnum.TRADE_CODE_CMISCUS0001.value, JSON.toJSONString(cmisCus0001ResultDto));
        return cmisCus0001ResultDto;
    }
}
