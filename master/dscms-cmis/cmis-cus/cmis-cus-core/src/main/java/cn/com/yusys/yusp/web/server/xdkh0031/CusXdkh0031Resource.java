package cn.com.yusys.yusp.web.server.xdkh0031;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0031.req.Xdkh0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0031.resp.Xdkh0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0031.Xdkh0031Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:同业客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0031:同业客户评级相关信息同步")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0031Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0031Resource.class);

    @Autowired
    private Xdkh0031Service xdkh0031Service;

    /**
     * 交易码：xdkh0031
     * 交易描述：同业客户评级相关信息同步
     *
     * @param xdkh0031DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("同业客户评级相关信息同步")
    @PostMapping("/xdkh0031")
    protected @ResponseBody
    ResultDto<Xdkh0031DataRespDto> xdkh0031(@Validated @RequestBody Xdkh0031DataReqDto xdkh0031DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031DataReqDto));
        Xdkh0031DataRespDto xdkh0031DataRespDto = new Xdkh0031DataRespDto();// 响应Dto:同业客户评级相关信息同步
        ResultDto<Xdkh0031DataRespDto> xdkh0031DataResultDto = new ResultDto<>();
        try {
            // 从xdkh0031DataReqDto获取业务值进行业务逻辑处理
            String cusId = xdkh0031DataReqDto.getCusId();//客户编号
            String lockStatus = xdkh0031DataReqDto.getLockStatus();//锁定状态
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031DataReqDto));
            xdkh0031DataRespDto = xdkh0031Service.xdkh0031(xdkh0031DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031DataRespDto));
            // 封装xdkh0031DataResultDto中正确的返回码和返回信息
            xdkh0031DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0031DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, e.getMessage());
            // 封装xdkh0031DataResultDto中异常返回码和返回信息
            xdkh0031DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0031DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0031DataRespDto到xdkh0031DataResultDto中
        xdkh0031DataResultDto.setData(xdkh0031DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031DataResultDto));
        return xdkh0031DataResultDto;
    }
}