/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CusBizAarAppCusLstDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusBizAarApp;
import cn.com.yusys.yusp.service.CusBizAarAppService;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBizAarAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: pc
 * @创建时间: 2021-05-07 22:13:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusbizaarapp")
public class CusBizAarAppResource {
    @Autowired
    private CusBizAarAppService cusBizAarAppService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusBizAarApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusBizAarApp> list = cusBizAarAppService.selectAll(queryModel);
        return new ResultDto<List<CusBizAarApp>>(list);
    }

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query")
    protected ResultDto<List<CusBizAarApp>> query(@RequestBody QueryModel queryModel) {
        queryModel.setSort("createTime desc");
        List<CusBizAarApp> list = cusBizAarAppService.selectByModel(queryModel);
        return new ResultDto<List<CusBizAarApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusBizAarApp>> index(QueryModel queryModel) {
        List<CusBizAarApp> list = cusBizAarAppService.selectByModel(queryModel);
        return new ResultDto<List<CusBizAarApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{aarAppSerno}")
    protected ResultDto<CusBizAarApp> show(@PathVariable("aarAppSerno") String aarAppSerno) {
        CusBizAarApp cusBizAarApp = cusBizAarAppService.selectByPrimaryKey(aarAppSerno);
        return new ResultDto<CusBizAarApp>(cusBizAarApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusBizAarApp> create(@RequestBody CusBizAarApp cusBizAarApp) throws URISyntaxException {
        cusBizAarAppService.insert(cusBizAarApp);
        return new ResultDto<CusBizAarApp>(cusBizAarApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusBizAarApp cusBizAarApp) throws URISyntaxException {
        int result = cusBizAarAppService.update(cusBizAarApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{aarAppSerno}")
    protected ResultDto<Integer> delete(@PathVariable("aarAppSerno") String aarAppSerno) {
        int result = cusBizAarAppService.delete(aarAppSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusBizAarAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:insertmore
     * @函数描述:新增业务申领
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertmore")
    protected ResultDto<CusBizAarAppCusLstDto> insertmore(@RequestBody CusBizAarAppCusLstDto dto) {
        cusBizAarAppService.insertmore(dto);
        return new ResultDto<CusBizAarAppCusLstDto>(dto);
    }

    /**
     * @函数名称:updatemore
     * @函数描述:修改业务申领
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatemore")
    protected ResultDto<CusBizAarAppCusLstDto> updatemore(@RequestBody CusBizAarAppCusLstDto dto) {
        cusBizAarAppService.updatemore(dto);
        return new ResultDto<CusBizAarAppCusLstDto>(dto);
    }

}
