/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseDto;
import cn.com.yusys.yusp.dto.CusIntbankDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.G00202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.G00202RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs11.req.Xirs11ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.req.CmisCus0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.resp.CmisCus0008RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusIntbankMgrDto;
import cn.com.yusys.yusp.enums.out.ecif.EcifCertTypeEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMapper;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMgrMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import com.github.pagehelper.PageHelper;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIntbankService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 乐友先生
 * @创建时间: 2021-04-08 11:10:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusIntbankService {
    private static final Logger log = LoggerFactory.getLogger(CusIntbankService.class);
    @Autowired
    private CusIntbankMapper cusIntbankMapper;
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;
    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;
    @Autowired
    private CusBaseMapper cusBaseMapper;
    @Autowired
    private SequenceTemplateService sequenceTemplateService;
    @Autowired
    private CusManaTaskService cusManaTaskMapper;
    @Autowired
    private CusIntbankMgrMapper cusIntbankMgrMapper;
    @Autowired
    private CusComGradeService cusComGradeService;
    @Autowired
    private Dscms2IrsClientService dscms2IrsClientService;
    @Autowired
    private CusCorpApitalService cusCorpApitalService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusIntbank selectByPrimaryKey(String cusId) {
        return cusIntbankMapper.selectByPrimaryKey(cusId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusIntbank> selectAll(QueryModel model) {
        List<CusIntbank> records = (List<CusIntbank>) cusIntbankMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusIntbank> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIntbank> list = cusIntbankMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusIntbank record) {
        return cusIntbankMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusIntbank record) {
        return cusIntbankMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusIntbank record) {
        return cusIntbankMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusIntbank record) {
        return cusIntbankMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId) {
        return cusIntbankMapper.deleteByPrimaryKey(cusId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusIntbankMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: selcetModeList
     * @方法描述: 根据条件查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CusIntbank> selectModeList(CusIntbank cusIntbank) {
        return cusIntbankMapper.selectByModeList(cusIntbank);
    }

    /**
     * @方法名称: selectCusIdByBankNo
     * @方法描述: 根据行号查找客户号
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectCusIdByBankNo(String bankNo) {
        return cusIntbankMapper.selectCusIdByBankNo(bankNo);
    }

    /**
     * @函数名称:openAccount
     * @函数描述: 同业客户开户调用接口逻辑
     * @参数与返回说明:
     * @算法描述:
     */
    public CusIntbankDto openAccount(CusIntbankDto cusIntbankDto) throws Exception {
        if (cusIntbankDto != null) {
            QueryModel model = new QueryModel();
            model.addCondition("certCode", cusIntbankDto.getCertCode());
            List<CusBase> exists = cusBaseMapper.selectByModel(model);
            List<CusIntbank> existsIntbank = cusIntbankMapper.selectByModel(model);
            List<CusManaTask> existsManaTask = cusManaTaskMapper.selectByModel(model);
            if (CollectionUtils.nonEmpty(exists)) {
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"已存在该证件号码的"+cusIntbankDto.getCertCode() +"的客户基础信息，请核对!");
            }
            if (CollectionUtils.nonEmpty(existsIntbank)) {
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"已存在该证件号码的"+cusIntbankDto.getCertCode() +"的同业客户信息，请核对!");
            }
            if (CollectionUtils.nonEmpty(existsManaTask)) {
                throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"已存在该证件号码的"+cusIntbankDto.getCertCode() +"的处理任务信息，请核对!");
            }
            //查询客户信息
            String cusId = queryG10501(cusIntbankDto);

            if (Objects.isNull(cusId)) {
                //没有查询到客户信息 掉用ecif开户
                cusId = createCusIntbank(cusIntbankDto);
            }
            //第三步 ECIF开户成功，插入客户相关信息
            cusIntbankDto.setCusId(cusId);
            /*
             * 3.1 保存任务表 待处理任务
             */
            CusManaTask cusManaTask = new CusManaTask();
            BeanUtils.copyProperties(cusIntbankDto, cusManaTask);
            cusManaTask.setTaskStatus("1");
            cusManaTask.setOprType("01");
            cusManaTask.setApproveStatus("000");
            cusManaTaskMapper.insertSelective(cusManaTask);
            /*
             * 3.2 保存cusBase表
             */
            CusBase cusBase = new CusBase();
            BeanUtils.copyProperties(cusIntbankDto, cusBase);
            //客户大类 1-对公 2-对私 3-同业 4-集团
            cusBase.setCusCatalog("3");
            //客户状态 STD_CUS_STATE 2-生效 1-暂存
            cusBase.setCusState("1");
            //设置管护机构
            User userInfo = SessionUtils.getUserInformation();
            cusBase.setManagerId(userInfo.getLoginCode());
            cusBase.setManagerBrId(userInfo.getOrg().getCode());
            //取系统日期
            String openday = stringRedisTemplate.opsForValue().get("openDay");//开户日期
            cusBase.setOpenDate(openday);
            cusBaseMapper.insertSelective(cusBase);

            /*
             * 3.3 保存cusIntbank表
             */
            ZsnewReqDto zsnewReqDto = new ZsnewReqDto();
            zsnewReqDto.setCreditcode(cusIntbankDto.getSocialCreditCode());
            zsnewReqDto.setName(cusIntbankDto.getCusName());
            zsnewReqDto.setId(StringUtils.EMPTY);
            zsnewReqDto.setVersion(StringUtils.EMPTY);
            ResultDto<ZsnewRespDto> zsnewResultDto = dscms2OuterdataClientService.zsnew(zsnewReqDto);
            String zsnewCode = Optional.ofNullable(zsnewResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String zsnewMeesage = Optional.ofNullable(zsnewResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            ZsnewRespDto zsnewRespDto = null;
            if (SuccessEnum.CMIS_SUCCSESS.key.equals(zsnewCode)) {
                String code = zsnewResultDto.getData().getData().getCODE();
                if ("200".equals(code)) {
                    //查询成功
                    log.info("查询成功");
                } else {
                    //没有查询到
                    log.info("没有查询到");
                }
            } else {
                log.error("接口交易失败");
            }
            CusIntbank cusIntbank = new CusIntbank();
            BeanUtils.copyProperties(cusIntbankDto, cusIntbank);
            if (zsnewResultDto.getData() != null){
                if (zsnewResultDto.getData().getData() != null){
                    if (zsnewResultDto.getData().getData().getENT_INFO() !=null){
                        if (zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO() != null){
                            if(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().size()!=0){
                                //机构简称
                                cusIntbank.setOrgForShort(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getCOMPSNAME());
                                //机构英文名称
                                cusIntbank.setCusEnName(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getENGNAME());
                                //注册地址
                                cusIntbank.setComRegAdd(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getREGADDR());
                                //经营范围
                                cusIntbank.setNatBusi(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getBIZSCOPE());
                                //经营地区
                                cusIntbank.setOperArea(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getREGION());
                            }
                        }
                        if (zsnewResultDto.getData().getData().getENT_INFO().getENTINV() != null){
                            if(zsnewResultDto.getData().getData().getENT_INFO().getENTINV().size()!=0){
                                //注册地行政区划
                                cusIntbank.setRegiAreaCode(zsnewResultDto.getData().getData().getENT_INFO().getENTINV().get(0).getREGORGCODE());
                            }
                        }
                        if (zsnewResultDto.getData().getData().getENT_INFO().getBASIC() != null){
                            //注册地行政区划
                            cusIntbank.setRegiAreaCode(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getREGORGCODE());
                        }
                    }
                }
            }
            cusIntbankMapper.insertSelective(cusIntbank);
            BeanUtils.copyProperties(cusIntbank, cusIntbankDto);
        } else {
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"传入值为空\"" + EcsEnum.E_CUS_INFO_ISNULL.value);
        }
        return cusIntbankDto;
    }

    /**
     * 调用开户接口
     *
     * @param cusIntbankDto
     * @return
     */
    private String createCusIntbank(CusIntbankDto cusIntbankDto) {
        // 同业客户开户
        G00102ReqDto g00102ReqDto = new G00102ReqDto();
        /**
         * 1	正常客户
         * 2	注销客户
         * 3	临时客户
         */
        g00102ReqDto.setCustst("3");//客户状态
        /**
         * 01	对公客户
         * 02	同业客户
         */
        g00102ReqDto.setCusttp("01");//客户类型               ,
        g00102ReqDto.setCustsb(StringUtils.EMPTY);//对公客户类型细分           ,
        // g00102ReqDto.setIdtftp("R");//证件类型 统一社会信用代码      ,
        g00102ReqDto.setIdtftp(cusIntbankDto.getCertType());
        g00102ReqDto.setIdtfno(cusIntbankDto.getCertCode());//证件号码               ,
        /**g00202ReqDto.setEfctdt(cusIntbankDto.getExpDateEnd());//证件生效日期
        g00202ReqDto.setInefdt(cusIntbankDto.getExpDateEnd().replace("-",""));//证件生效日期  ,
        g00202ReqDto.setBusisp(cusIntbankDto.getNatBusi());//经营范围  提交之后更新
        g00202ReqDto.setRgprov(cusIntbankDto.getRegiAreaCode());//注册地址所在省
        g00202ReqDto.setRgcity(cusIntbankDto.getRegiAreaCode());//注册地址所在市
        g00202ReqDto.setRgerea(cusIntbankDto.getRegiAreaCode());//注册地址所在地区
        g00202ReqDto.setRegadr(cusIntbankDto.getComRegAdd());//注册地址**/
        g00102ReqDto.setCustna(cusIntbankDto.getCusName());//客户名称
        //g00202ReqDto.setJgxyno(cusIntbankDto.getCertCode());//机构信用代码
        // TODO StringUtils.EMPTY的实际值待确认 结束
        // TODO 完善后续逻辑
        ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
        String g00102Code = Optional.ofNullable(g00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g00102Meesage = Optional.ofNullable(g00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(g00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            return g00102ResultDto.getData().getCustno();
        } else {
            throw BizException.error(null,g00102Code,g00102Meesage);
        }
    }

    /**
     * 同业客户查询
     *
     * @param cusIntbankDto
     * @return
     */
    private String queryG10501(CusIntbankDto cusIntbankDto) {
        // g10501 对公及同业客户清单查询
        G10501ReqDto g10501ReqDto = new G10501ReqDto();
        //System.out.println(cusIntbankDto.getCusName());
        /**
         * 识别方式:
         * 1- 按证件类型、证件号码查询
         * 2- 按证件号码查询
         * 3- 按客户编号查询
         * 4- 按客户名称模糊查询
         * 以下接口必填一种查询方式
         */
        g10501ReqDto.setResotp("2");// 识别方式
        g10501ReqDto.setCustna(cusIntbankDto.getCusName());// 客户名称
        g10501ReqDto.setIdtftp(cusIntbankDto.getCertType());// 证件类型
        g10501ReqDto.setIdtfno(cusIntbankDto.getCertCode());// 证件号码
        g10501ReqDto.setCustst(StringUtils.EMPTY);// 客户状态
        g10501ReqDto.setBginnm(StringUtils.EMPTY);// 起始笔数
        g10501ReqDto.setQurynm(StringUtils.EMPTY);// 查询笔数
        ResultDto<G10501RespDto> g10501ResultDto = dscms2EcifClientService.g10501(g10501ReqDto);
        String g10501Code = Optional.ofNullable(g10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g10501Meesage = Optional.ofNullable(g10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(g10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            G10501RespDto data = Optional.ofNullable(g10501ResultDto.getData()).orElse(new G10501RespDto());
            List<ListArrayInfo> listArrayInfo = data.getListArrayInfo();
            if (CollectionUtils.nonEmpty(listArrayInfo)) {
                log.info("根据证件类型[{}]证件号码[{}]，查询ECIF返回的客户号为[{}]",cusIntbankDto.getCertType(), cusIntbankDto.getCertCode(),g10501ResultDto.getData().getListArrayInfo().get(0).getCustno());
                return g10501ResultDto.getData().getListArrayInfo().get(0).getCustno();
            }
            log.info("根据证件类型[{}]证件号码[{}]，未查询到客户号",cusIntbankDto.getCertType(), cusIntbankDto.getCertCode());
        } else {
            throw BizException.error(null,g10501Code,g10501Meesage);
        }
        return null;
    }


    /**
     * @函数名称:saveTemp
     * @函数描述:客户创建暂存
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveTemp(CusIntbank cusIntbank) {
        CusIntbank cusIntbank1 = selectByPrimaryKey(cusIntbank.getCusId());
        User userInfo = SessionUtils.getUserInformation();
        int result = 0;
        if (cusIntbank1 != null) {
            //说明该客户得数据在数据库中已经存在，执行更新操作
            result = this.cusIntbankMapper.updateByPrimaryKeySelective(cusIntbank);
        } else {
            //说明该客户的数据在数据库中不存在，执行插入操作
            result = this.cusIntbankMapper.insertSelective(cusIntbank);
        }
        //更改cus_base客户状态
        CusBase cusBase = cusBaseMapper.selectByPrimaryKey(cusIntbank.getCusId());
        if(cusBase!=null){
            cusBase.setCusState(cusIntbank.getCusState());
            result = cusBaseMapper.updateByPrimaryKeySelective(cusBase);
        }
        log.info("保存同业客户信息成功，客户号[{}]客户状态[{}]",cusIntbank.getCusId(), cusIntbank.getCusState());
        // 如果生效数据，调非零内评同步用户数据
        if("2".equals(cusIntbank.getCusState())){
            log.info("调用非零内评Xirs11开始，客户号[{}]管户客户经理[{}]",cusIntbank.getCusId(), cusIntbank.getManagerId());
            Xirs11ReqDto reqDto = new Xirs11ReqDto();
            reqDto.setCustid(cusIntbank.getCusId());
            reqDto.setCantoncode(cusIntbank.getRegiAreaCode());
            reqDto.setInputorgid(cusIntbank.getManagerBrId());
            reqDto.setInputuserid(cusIntbank.getManagerId());
            reqDto.setSame_org_no(cusIntbank.getLargeBankNo());
            reqDto.setSame_org_name(cusIntbank.getCusName());
            reqDto.setSame_org_type(convertSameOrgType(cusIntbank.getIntbankType()));
            reqDto.setInputorgid("990000");// 同业客户固定机构990000
            reqDto.setInputuserid(cusIntbank.getManagerId());
            dscms2IrsClientService.xirs11(reqDto);
            log.info("调用非零内评Xirs11结束，客户号[{}]管户客户经理[{}]",cusIntbank.getCusId(), cusIntbank.getManagerId());

            // 同业客户信息维护ECIF
            log.info("调用ECIF同业客户信息维护g00102开始，客户号[{}]管户客户经理[{}]",cusIntbank.getCusId(), cusIntbank.getManagerId());
            G00102ReqDto g00102ReqDto = new G00102ReqDto();
            g00102ReqDto.setCustst("3");//客户状态cuscorp
            g00102ReqDto.setCusttp("02");//客户类型               ,
            g00102ReqDto.setCustno(cusIntbank.getCusId());
            g00102ReqDto.setCustsb(StringUtils.EMPTY);//对公客户类型细分           ,
            //g00102ReqDto.setIdtftp("R");//证件类型 统一社会信用代码      ,
            g00102ReqDto.setIdtftp(cusIntbank.getCertType());
            g00102ReqDto.setIdtfno(cusIntbank.getCertCode());//证件号码               ,
            //g00102ReqDto.setEfctdt(cusIntbank.getExpDateEnd());//证件生效日期
            g00102ReqDto.setInefdt(cusIntbank.getExpDateEnd().replace("-",""));//证件失效日期  ,
            g00102ReqDto.setBusisp(cusIntbank.getNatBusi());//经营范围  提交之后更新
            g00102ReqDto.setRgprov(convertCD0027(StringUtils.substring(cusIntbank.getRegiAreaCode(), 6)));//注册地址所在省
            g00102ReqDto.setRgcity(convertCD0027(StringUtils.substring(cusIntbank.getRegiAreaCode(), 6)));//注册地址所在市
            g00102ReqDto.setRgerea(convertCD0027(StringUtils.substring(cusIntbank.getRegiAreaCode(), 6)));//注册地址所在地区
            g00102ReqDto.setRegadr(cusIntbank.getComRegAdd());//注册地址**/
            g00102ReqDto.setCustna(cusIntbank.getCusName());//客户名称
            g00102ReqDto.setSorgno(cusIntbank.getLargeBankNo());//同业机构(行)号
            g00102ReqDto.setSorgtp(cusIntbank.getIntbankType());// 同业机构(行)类型
            g00102ReqDto.setBkplic(cusIntbank.getBankProLic()); //同业客户金融业务许可证
            //g00102ReqDto.setJgxyno(cusIntbank.getCertCode());//机构信用代码
            //g00102ReqDto.setEcegra(convertEcegra(cusIntbank.getIntbankEval())); //信用等级（外部）
            //g00102ReqDto.setEratdt(cusIntbank.getEvalEndDt()); //评定日期（外部）
            try {
                ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
                String g00102Code = Optional.ofNullable(g00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String g00102Meesage = Optional.ofNullable(g00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                if (Objects.equals(g00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                    log.info("调用ECIF同业客户信息维护g00102结束，客户号[{}]管户客户经理[{}]",cusIntbank.getCusId(), cusIntbank.getManagerId());
                } else {
                    log.error("调用ECIF同业客户信息维护g00102失败，失败原因："+g00102Meesage);
                }
            }catch (Exception e){
                log.error("调用ECIF同业客户信息维护g00102失败，失败原因："+e.getMessage());
            }
        }
        return result;
    }

    /**
     * @函数名称:save
     * @函数描述:客户创建提交保存
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional(rollbackFor = Exception.class)
    public int save(CusIntbank cusIntbank) {
        CusIntbank cusIntbank1 = selectByPrimaryKey(cusIntbank.getCusId());
        int result = 0;
        if (cusIntbank1 != null) {
            //说明该客户得数据在数据库中已经存在，执行更新操作
            result = this.cusIntbankMapper.updateByPrimaryKeySelective(cusIntbank);
        } else {
            //说明该客户的数据在数据库中不存在，执行插入操作
            result = this.cusIntbankMapper.insertSelective(cusIntbank);
        }

        return result;
    }

    /**
     * @函数名称:queryCusIntbankBySocialCreditCode
     * @函数描述:根据社会信用代码查询同业客户信息
     * @参数与返回说明:
     * @算法描述:
     */
    public CusIntbank queryCusIntbankBySocialCreditCode(CusIntbank cusIntbank) {
        return cusIntbankMapper.queryCusIntbankBySocialCreditCode(cusIntbank);
    }

    /**
     * 根据客户编号,证件类型，证件号码查询同业客户基本信息
     *
     * @param cmisCus0010ReqDto
     * @return
     */
    public CmisCus0010RespDto cmiscus0010(CmisCus0010ReqDto cmisCus0010ReqDto) {
        List<CmisCus0010RespDto> cmisCus0010List = cusIntbankMapper.getCmiscus0010(cmisCus0010ReqDto);
        CmisCus0010RespDto cmisCus0010RespDto = new CmisCus0010RespDto();
        if (cmisCus0010List.size() > 1) {
            throw BizException.error(null, EcsEnum.ECS040010.key, EcsEnum.ECS040010.value);
        } else if (cmisCus0010List.size() == 0) {
            return null;
        } else {
            cmisCus0010RespDto = cmisCus0010List.get(0);
            String cusId = cmisCus0010RespDto.getCusId();

            //查询客户的最新信用评级信息
            Map<String, String> gradeInfo = cusComGradeService.selectGradeInfoByCusId(cusId);

            if (gradeInfo != null && !gradeInfo.isEmpty()) {
                cmisCus0010RespDto.setFinalRank(gradeInfo.get("finalRank"));
                cmisCus0010RespDto.setDueDt(gradeInfo.get("dueDt"));
                cmisCus0010RespDto.setEffDt(gradeInfo.get("effDt"));
            }
            //cus_corp_apital

            //获取股东列表
            List<CusCorpApitalDto> cusCorpApitalDtoList = new ArrayList<>();
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusIdRel",cmisCus0010RespDto.getCusId());
            List<CusCorpApital> cusCorpApitals = cusCorpApitalService.selectAll(queryModel);
            if (CollectionUtils.nonEmpty(cusCorpApitals)){
                cusCorpApitals.stream().forEach(a->{
                    CusCorpApitalDto cusCorpApitalDto = new CusCorpApitalDto();
                    BeanUtils.copyProperties(a,cusCorpApitalDto);
                    cusCorpApitalDtoList.add(cusCorpApitalDto);
                });
            }
            cmisCus0010RespDto.setCusCorpApitalDtoList(cusCorpApitalDtoList);
            //获取实际控制人
            List<CusIntbankMgrDto> cusIntbankMgrDtoList = cusIntbankMgrMapper.selectByIntbankCusId(cmisCus0010RespDto.getCusId());
            if (CollectionUtils.nonEmpty(cusIntbankMgrDtoList)){
                List<CusIntbankMgrDto> cusIntbankMgrDtos = cusIntbankMgrDtoList.stream().filter(
                        a -> CmisBizConstants.STD_ZB_INTBANK_MRG_CLS_02.equals(a.getMrgCls()))
                        .collect(Collectors.toList());
                cmisCus0010RespDto.setCusIntbankMgrList(cusIntbankMgrDtos);
            }
        }
        return cmisCus0010RespDto;
    }

    /**
     * 信用等级转换 新信贷 ---> ecif
     *
     * @param ecegra
     * @return
     */
    public String convertEcegra(String ecegra) {
        switch (ecegra) {
            case "000":
                ecegra = "01";
                break;
            case "AAA":
                ecegra = "02";
                break;
            case "AA+":
                ecegra = "03";
                break;
            case "AA":
                ecegra = "04";
                break;
            case "AA-":
                ecegra = "05";
                break;
            case "A+":
                ecegra = "06";
                break;
            case "A":
                ecegra = "07";
                break;
            case "A-":
                ecegra = "08";
                break;
            case "BBB+":
                ecegra = "09";
                break;
            case "BBB":
                ecegra = "10";
                break;
            case "BBB-":
                ecegra = "11";
                break;
            case "BB+":
                ecegra = "12";
                break;
            case "BB":
                ecegra = "13";
                break;
            case "BB-":
                ecegra = "14";
                break;
            case "B+":
                ecegra = "15";
                break;
            case "B":
                ecegra = "16";
                break;
            case "CCC+":
                ecegra = "17";
                break;
            case "CCC":
                ecegra = "18";
                break;
            case "CCC-":
                ecegra = "19";
                break;
            case "CC":
                ecegra = "20";
                break;
            case "C":
                ecegra = "21";
                break;
            case "D":
                ecegra = "22";
                break;
            default:
                break;
        }
        return ecegra;
    }

    /**
     * 同业机构类型转换 新信贷 ---> 非零内评
     *
     * @param intbankType
     * @return
     */
    public String convertSameOrgType(String intbankType) {
        if ("C11200".equals(intbankType) || "C11300".equals(intbankType)) {
            intbankType = "01";
        }
        if ("C11100".equals(intbankType)) {
            intbankType = "02";
        }
        if ("C12111".equals(intbankType)) {
            intbankType = "03";
        }
        if ("C12112".equals(intbankType)) {
            intbankType = "04";
        }
        if ("C121311".equals(intbankType) || "C121312".equals(intbankType)) {
            intbankType = "05";
        }
        if ("C121321".equals(intbankType) || "C121322".equals(intbankType) || "C12140".equals(intbankType) || "C12142".equals(intbankType) || "C121421".equals(intbankType) || "C121422".equals(intbankType)) {
            intbankType = "06";
        }
        if ("C32100".equals(intbankType) || "C32200".equals(intbankType)) {
            intbankType = "07";
        }
        if ("C20000".equals(intbankType) || "C21100".equals(intbankType) || "C21200".equals(intbankType) || "C30000".equals(intbankType) || "C31100".equals(intbankType) || "C31200".equals(intbankType)) {
            intbankType = "08";
        }
        if ("C61000".equals(intbankType) || "C62000".equals(intbankType)) {
            intbankType = "09";
        }
        if ("C41000".equals(intbankType) || "C42000".equals(intbankType)) {
            intbankType = "10";
        }
        if ("C12141".equals(intbankType)) {
            intbankType = "21";
        }
        if ("E10000".equals(intbankType)) {
            intbankType = "22";
        }
        return intbankType;
    }

    public String convertCD0027(String data) {
        if ("540172".equals(data)) {
            data = "540000";
        }
        if ("540173".equals(data)) {
            data = "540000";
        }
        if ("540171".equals(data)) {
            data = "540000";
        }
        return data;
    }
}
