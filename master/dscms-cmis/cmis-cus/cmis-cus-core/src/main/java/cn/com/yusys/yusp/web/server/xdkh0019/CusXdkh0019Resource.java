package cn.com.yusys.yusp.web.server.xdkh0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0019.req.Xdkh0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.resp.Xdkh0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0019.Xdkh0019Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户查询并开户
 *
 * @author zhugenrong
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDKH0019:客户查询并开户")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0019Resource.class);
    @Autowired
    private Xdkh0019Service xdkh0019Service;

    /**
     * 交易码：xdkh0019
     * 交易描述：客户查询并开户
     *
     * @param xdkh0019DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdkh0019:客户查询并开户")
    @PostMapping("/xdkh0019")
    protected @ResponseBody
    ResultDto<Xdkh0019DataRespDto> xdkh0019(@Validated @RequestBody Xdkh0019DataReqDto xdkh0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, JSON.toJSONString(xdkh0019DataReqDto));
        Xdkh0019DataRespDto xdkh0019DataRespDto = new Xdkh0019DataRespDto();// 响应Dto:客户查询并开户
        ResultDto<Xdkh0019DataRespDto> xdkh0019DataResultDto = new ResultDto<>();
        try {
            xdkh0019DataRespDto = xdkh0019Service.xdkh0019(xdkh0019DataReqDto);
            // 封装xdkh0019DataResultDto中正确的返回码和返回信息
            xdkh0019DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0019DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
            // 封装xdkh0019DataResultDto中异常返回码和返回信息
            xdkh0019DataResultDto.setCode(e.getErrorCode());
            xdkh0019DataResultDto.setMessage(e.getMessage());
        } catch (YuspException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
            // 封装xdkh0019DataResultDto中异常返回码和返回信息
            xdkh0019DataResultDto.setCode(e.getCode());
            xdkh0019DataResultDto.setMessage(e.getMessage());
        }catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
            // 封装xdkh0019DataResultDto中异常返回码和返回信息
            xdkh0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0019DataRespDto到xdkh0019DataResultDto中
        xdkh0019DataResultDto.setData(xdkh0019DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, JSON.toJSONString(xdkh0019DataRespDto));
        return xdkh0019DataResultDto;
    }
}
