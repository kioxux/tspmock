/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusIndivUnit;
import cn.com.yusys.yusp.repository.mapper.CusIndivUnitMapper;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusIndivUnitService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-11-16 11:44:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CusIndivUnitService {

    @Autowired
    private CusIndivUnitMapper cusIndivUnitMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusIndivUnit selectByPrimaryKey(String pkId) {
        return cusIndivUnitMapper.selectByPrimaryKey(pkId);
    }
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据客户号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusIndivUnit selectByCusId(String cusId) {
        return cusIndivUnitMapper.selectByCusId(cusId);
    }

	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusIndivUnit> selectAll(QueryModel model) {
        List<CusIndivUnit> records = (List<CusIndivUnit>) cusIndivUnitMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusIndivUnit> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIndivUnit> list = cusIndivUnitMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusIndivUnit record) {
        return cusIndivUnitMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusIndivUnit record) {
        return cusIndivUnitMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusIndivUnit record) {
        return cusIndivUnitMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusIndivUnit record) {
        return cusIndivUnitMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateCusIndivUnitByCusId
     * @方法描述: 根据客户号更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateCusIndivUnitByCusId(CusIndivUnit record) {
        return cusIndivUnitMapper.updateCusIndivUnitByCusId(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusIndivUnitMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusIndivUnitMapper.deleteByIds(ids);
    }
    /**
     * @方法名称: save
     * @方法描述: 客户工作保存，有则更新，无责保存
     * @参数与返回说明:
     * @算法描述:
     * @修改人：周茂伟
     */
    public int save(CusIndivUnit cusIndivUnit) {
        CusIndivUnit cusIndivUnit1 = selectByCusId(cusIndivUnit.getCusId());
        User userInfo = SessionUtils.getUserInformation();
        // 申请人
        String inputId = "";
        // 申请机构
        String inputBrId = "";
        // 申请时间
        String inputDate = "";
        // 创建时间
        Date createTime = DateUtils.getCurrTimestamp();
        // 获取用户信息
        if(userInfo != null){
            // 申请人
            inputId = userInfo.getLoginCode();
            // 申请机构
            inputBrId = userInfo.getOrg().getCode();
            // 申请时间
            inputDate = DateUtils.getCurrDateStr();
        }
        int  result=0;
        if (cusIndivUnit1 != null){
            // 修改日期
            cusIndivUnit.setUpdDate(inputDate);
            // 修改时间
            cusIndivUnit.setUpdateTime(createTime);
            // 修改人
            cusIndivUnit.setUpdId(inputId);
            // 修改机构
            cusIndivUnit.setUpdBrId(inputBrId);
            //说明执行得是更新得操作
            result = this.cusIndivUnitMapper.updateByPrimaryKeySelective(cusIndivUnit);
        }else{
            String pkId= StringUtils.getUUID();
            cusIndivUnit.setPkId(pkId);
            // 申请人
            cusIndivUnit.setInputId(inputId);
            // 申请机构
            cusIndivUnit.setInputBrId(inputBrId);
            // 申请时间
            cusIndivUnit.setInputDate(inputDate);
            // 创建时间
            cusIndivUnit.setCreateTime(createTime);
            // 修改日期
            cusIndivUnit.setUpdDate(inputDate);
            // 修改时间
            cusIndivUnit.setUpdateTime(createTime);
            // 修改人
            cusIndivUnit.setUpdId(inputId);
            // 修改机构
            cusIndivUnit.setUpdBrId(inputBrId);
            result = this.cusIndivUnitMapper.insertSelective(cusIndivUnit);
        }
        return result;
    }
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据客户号查询查询
     * @参数与返回说明:
     * @算法描述: 无
     * @修改人 周茂伟
     */

    public CusIndivUnit selectBycusId(String cusId) {
        return cusIndivUnitMapper.selectBycusId(cusId);
    }

    /**
     * 根据客户号查询关联企业客户号
     * @param cusId
     * @return
     */
    public Map<String,String> selectUnitIdByCusId(String cusId){
        return cusIndivUnitMapper.selectUnitIdByCusId(cusId);
    }

}
