/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusCorpApital
 * @类描述: cus_corp_apital数据实体类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-23 09:49:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_corp_apital")
public class CusCorpApital extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 出资人性质 STD_ZB_INVY_TYP **/
	@Column(name = "INVT_TYP", unique = false, nullable = true, length = 5)
	private String invtTyp;
	
	/** 出资人证件类型  STD_ZB_CERT_TYP **/
	@Column(name = "CERT_TYP", unique = false, nullable = true, length = 5)
	private String certTyp;
	
	/** 出资人证件号码  **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 出资人客户编号 **/
	@Column(name = "CUS_ID_REL", unique = false, nullable = true, length = 30)
	private String cusIdRel;
	
	/** 出资人名称 **/
	@Column(name = "INVT_NAME", unique = false, nullable = true, length = 100)
	private String invtName;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 出资金额  **/
	@Column(name = "INVT_AMT", unique = false, nullable = true, length = 16)
	private BigDecimal invtAmt;
	
	/** 出资比例 **/
	@Column(name = "INVT_PERC", unique = false, nullable = true, length = 10)
	private BigDecimal invtPerc;
	
	/** 出资时间 **/
	@Column(name = "INV_DATE", unique = false, nullable = true, length = 10)
	private String invDate;
	
	/** 出资说明 **/
	@Column(name = "INVT_DESC", unique = false, nullable = true, length = 250)
	private String invtDesc;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 国别  STD_ZB_COUNTRY **/
	@Column(name = "COUNTRY", unique = false, nullable = true, length = 5)
	private String country;

	/** 出资方式  STD_ZB_INVT_TYPE **/
	@Column(name = "INVT_TYPE", unique = false, nullable = true, length = 5)
	private String invtType;



	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param invtTyp
	 */
	public void setInvtTyp(String invtTyp) {
		this.invtTyp = invtTyp;
	}
	
    /**
     * @return invtTyp
     */
	public String getInvtTyp() {
		return this.invtTyp;
	}
	
	/**
	 * @param certTyp
	 */
	public void setCertTyp(String certTyp) {
		this.certTyp = certTyp;
	}
	
    /**
     * @return certTyp
     */
	public String getCertTyp() {
		return this.certTyp;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}

	public String getCusIdRel() {
		return cusIdRel;
	}

	public void setCusIdRel(String cusIdRel) {
		this.cusIdRel = cusIdRel;
	}

	/**
	 * @param invtName
	 */
	public void setInvtName(String invtName) {
		this.invtName = invtName;
	}
	
    /**
     * @return invtName
     */
	public String getInvtName() {
		return this.invtName;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public BigDecimal getInvtPerc() {
		return invtPerc;
	}

	public void setInvtPerc(BigDecimal invtPerc) {
		this.invtPerc = invtPerc;
	}

	/**
	 * @param invDate
	 */
	public void setInvDate(String invDate) {
		this.invDate = invDate;
	}
	
    /**
     * @return invDate
     */
	public String getInvDate() {
		return this.invDate;
	}
	
	/**
	 * @param invtDesc
	 */
	public void setInvtDesc(String invtDesc) {
		this.invtDesc = invtDesc;
	}
	
    /**
     * @return invtDesc
     */
	public String getInvtDesc() {
		return this.invtDesc;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getInvtType() {
		return invtType;
	}

	public void setInvtType(String invtType) {
		this.invtType = invtType;
	}

	public BigDecimal getInvtAmt() {
		return invtAmt;
	}

	public void setInvtAmt(BigDecimal invtAmt) {
		this.invtAmt = invtAmt;
	}
}