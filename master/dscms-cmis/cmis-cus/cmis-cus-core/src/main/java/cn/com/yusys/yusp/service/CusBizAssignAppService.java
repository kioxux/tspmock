/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.CusBizAarCusLst;
import cn.com.yusys.yusp.domain.CusBizAcquUserLst;
import cn.com.yusys.yusp.domain.CusPrivCorreRel;
import cn.com.yusys.yusp.dto.CusBizAssignAppDto;
import cn.com.yusys.yusp.fncstat.domain.RptItemData;
import cn.com.yusys.yusp.repository.mapper.CusBizAcquUserLstMapper;
import cn.com.yusys.yusp.repository.mapper.CusPrivCorreRelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusBizAssignApp;
import cn.com.yusys.yusp.repository.mapper.CusBizAssignAppMapper;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBizAssignAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 12393
 * @创建时间: 2021-04-07 16:37:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusBizAssignAppService {
    private static final Logger log = LoggerFactory.getLogger(CusBizAssignAppService.class);
    @Autowired
    private CusBizAssignAppMapper cusBizAssignAppMapper;
    @Autowired
    private CusBizAcquUserLstMapper cusBizAcquUserLstMapper;
    @Autowired
    private CusPrivCorreRelMapper cusPrivCorreRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBizAssignApp selectByPrimaryKey(String assignAppSerno) {
        return cusBizAssignAppMapper.selectByPrimaryKey(assignAppSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusBizAssignApp> selectAll(QueryModel model) {
        List<CusBizAssignApp> records = (List<CusBizAssignApp>) cusBizAssignAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBizAssignApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBizAssignApp> list = cusBizAssignAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusBizAssignApp record) {
        return cusBizAssignAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusBizAssignApp record) {
        return cusBizAssignAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusBizAssignApp record) {
        return cusBizAssignAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusBizAssignApp record) {
        return cusBizAssignAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String assignAppSerno) {
        return cusBizAssignAppMapper.deleteByPrimaryKey(assignAppSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusBizAssignAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateMore
     * @方法描述: 根据DTO，更新数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateMore(CusBizAssignAppDto dto) {
        try {
            cusBizAssignAppMapper.updateByPrimaryKeySelective(dto.getCusBizAssignApp());
            //获取原来主表关联的子表cusBizAcquUserLst信息
            List<CusBizAcquUserLst> cusList = cusBizAcquUserLstMapper.selectAssignAppSerno(dto.getList().get(0).getAssignAppSerno());
            for (int i = 0; i < cusList.size(); i++) {
                boolean log = false;
                for (int j = 0; j < dto.getList().size(); j++) {
                    if (cusList.get(i).getAssignAppSerno().equals(dto.getList().get(j).getAssignAppSerno())) {
                        log = true;
                    }
                }
                //如果原来子表有数据不在新上传的list中，则将它删除
                if (!log) {
                    int rows = cusBizAcquUserLstMapper.deleteByPrimaryKey(cusList.get(i).getPkId());
                }
            }
            //将新上传的数据，属于新增的，将其插入
            for (CusBizAcquUserLst cusBizAcquUserLst : dto.getList()) {
                if (cusBizAcquUserLstMapper.selectByPrimaryKey(cusBizAcquUserLst.getPkId()) == null) {
                    int rows = cusBizAcquUserLstMapper.insertSelective(cusBizAcquUserLst);
                }
            }
            if (dto.getCusPrivCorreRel() != null) {
                for (CusPrivCorreRel cusPrivCorreRel : dto.getCusPrivCorreRel()) {
                    int rows = cusPrivCorreRelMapper.insertSelective(cusPrivCorreRel);
                }
            }
        } catch (RuntimeException e) {
            log.error(e.getMessage(),e);
            throw new RuntimeException(e);
        }
        return 1;
    }

    /**
     * @方法名称: 删除业务申请
     * @方法描述: 根据DTO，更新数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int delete(String assignAppSerno) {
        QueryModel qm = new QueryModel();
        qm.addCondition("assignAppSerno", assignAppSerno);
        List<CusBizAcquUserLst> list = cusBizAcquUserLstMapper.selectByModel(qm);
        if (list != null && list.size() > 0) {
            list.stream().forEach(cusBizAcquUserLst -> {
                cusBizAcquUserLstMapper.deleteByPrimaryKey(cusBizAcquUserLst.getPkId());
            });
        }
        int result = cusBizAssignAppMapper.deleteByPrimaryKey(assignAppSerno);
        return result;
    }
}
