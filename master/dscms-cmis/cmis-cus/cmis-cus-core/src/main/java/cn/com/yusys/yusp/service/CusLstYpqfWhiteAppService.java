/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CusLstYpqfWhite;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstYpqfWhiteApp;
import cn.com.yusys.yusp.repository.mapper.CusLstYpqfWhiteAppMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYpqfWhiteAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 17:24:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYpqfWhiteAppService {

    @Autowired
    private CusLstYpqfWhiteAppMapper cusLstYpqfWhiteAppMapper;

    @Autowired
    private CusLstYpqfWhiteService cusLstYpqfWhiteService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstYpqfWhiteApp selectByPrimaryKey(String lywaSerno) {
        return cusLstYpqfWhiteAppMapper.selectByPrimaryKey(lywaSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYpqfWhiteApp> selectAll(QueryModel model) {
        List<CusLstYpqfWhiteApp> records = (List<CusLstYpqfWhiteApp>) cusLstYpqfWhiteAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstYpqfWhiteApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYpqfWhiteApp> list = cusLstYpqfWhiteAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstYpqfWhiteApp record) {
        return cusLstYpqfWhiteAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstYpqfWhiteApp record) {
        return cusLstYpqfWhiteAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstYpqfWhiteApp record) {
        return cusLstYpqfWhiteAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstYpqfWhiteApp record) {
        return cusLstYpqfWhiteAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String lywaSerno) {
        return cusLstYpqfWhiteAppMapper.deleteByPrimaryKey(lywaSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYpqfWhiteAppMapper.deleteByIds(ids);
    }


    /**
     * 银票签发白名单申请提交，后续的业务处理
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterStart(String serno) {
        CusLstYpqfWhiteApp cusLstYpqfWhiteApp = cusLstYpqfWhiteAppMapper.selectByPrimaryKey(serno);
        cusLstYpqfWhiteApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        cusLstYpqfWhiteAppMapper.updateByPrimaryKey(cusLstYpqfWhiteApp);
    }


    /**
     * 银票签发白名单申请拒绝，后续的业务处理
     *
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterRefuse(String serno) {
        CusLstYpqfWhiteApp cusLstYpqfWhiteApp = cusLstYpqfWhiteAppMapper.selectByPrimaryKey(serno);
        cusLstYpqfWhiteApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        cusLstYpqfWhiteAppMapper.updateByPrimaryKey(cusLstYpqfWhiteApp);
    }

    /**
     * 银票签发白名单申请打回，后续的业务处理
     *
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterBack(String serno) {
        CusLstYpqfWhiteApp cusLstYpqfWhiteApp = cusLstYpqfWhiteAppMapper.selectByPrimaryKey(serno);
        cusLstYpqfWhiteApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        cusLstYpqfWhiteAppMapper.updateByPrimaryKey(cusLstYpqfWhiteApp);
    }

    /**
     * 银票签发白名单申请通过，后续的业务处理
     *
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterEnd(String serno) {

        CusLstYpqfWhiteApp cusLstYpqfWhiteApp = cusLstYpqfWhiteAppMapper.selectByPrimaryKey(serno);
        cusLstYpqfWhiteApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
        cusLstYpqfWhiteAppMapper.updateByPrimaryKey(cusLstYpqfWhiteApp);
        CusLstYpqfWhite cusLstYpqfWhite = new CusLstYpqfWhite();
        BeanUtils.copyProperties(cusLstYpqfWhiteApp, cusLstYpqfWhite);
        // TODO 临时状态
        cusLstYpqfWhite.setStatus("1");
        cusLstYpqfWhiteService.insert(cusLstYpqfWhite);

    }


}
