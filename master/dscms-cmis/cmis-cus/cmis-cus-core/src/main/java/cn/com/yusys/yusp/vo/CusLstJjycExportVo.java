package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.util.Date;

/**
 * 经济依存客户名单导入导出Vo
 *
 * @author yangzai
 * @since 2021/4/26
 **/
@ExcelCsv(namePrefix = "经济依存客户名单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CusLstJjycExportVo {



    /**
     * 客户类型
     **/
    @ExcelField(title = "客户类型", viewLength = 20)
    private String cusType;

    /**
     * 客户编号
     **/
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /**
     * 客户名称
     **/
    @ExcelField(title = "客户名称", viewLength = 80)
    private String cusName;

    /**
     * 依存客户编号
     **/
    @ExcelField(title = "依存客户编号", viewLength = 20)
    private String depCusNo;

    /**
     * 依存客户名称
     **/
    @ExcelField(title = "依存客户名称", viewLength = 80)
    private String depCusName;

    /**
     * 经济依存客户识别因素
     **/
    @ExcelField(title = "经济依存客户识别因素", viewLength = 20)
    private String ecoDepCusDistFactor;

    /**
     * 情况说明
     **/
    @ExcelField(title = "情况说明", viewLength = 20)
    private String caseMome;

    /**
     * 管户客户经理
     **/
    @ExcelField(title = "管户客户经理", viewLength = 20)
    private String managerId;

    /**
     * 所属机构
     **/
    @ExcelField(title = "所属机构", viewLength = 20)
    private String belgOrg;


    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getDepCusNo() {
        return depCusNo;
    }

    public void setDepCusNo(String depCusNo) {
        this.depCusNo = depCusNo;
    }

    public String getDepCusName() {
        return depCusName;
    }

    public void setDepCusName(String depCusName) {
        this.depCusName = depCusName;
    }

    public String getEcoDepCusDistFactor() {
        return ecoDepCusDistFactor;
    }

    public void setEcoDepCusDistFactor(String ecoDepCusDistFactor) {
        this.ecoDepCusDistFactor = ecoDepCusDistFactor;
    }

    public String getCaseMome() {
        return caseMome;
    }

    public void setCaseMome(String caseMome) {
        this.caseMome = caseMome;
    }

    public String getmanagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getBelgOrg() {
        return belgOrg;
    }

    public void setBelgOrg(String belgOrg) {
        this.belgOrg = belgOrg;
    }

}