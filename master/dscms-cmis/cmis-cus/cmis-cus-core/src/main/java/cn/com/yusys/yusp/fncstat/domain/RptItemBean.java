package cn.com.yusys.yusp.fncstat.domain;
/**
 * 财务报表信息-动态
 * @author zzbankwb369
 *
 */
public class RptItemBean {
	private String tableName;
	private String dataField1;
	private String dataField2;
	private String dataField3;
	private String cusId;
	private String rptYear;
	private String statStyle;
	private String itemId;
	private String data1;
	private String data2;
	private String data3;
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getDataField1() {
		return dataField1;
	}
	public void setDataField1(String dataField1) {
		this.dataField1 = dataField1;
	}
	public String getDataField2() {
		return dataField2;
	}
	public void setDataField2(String dataField2) {
		this.dataField2 = dataField2;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	public String getRptYear() {
		return rptYear;
	}
	public void setRptYear(String rptYear) {
		this.rptYear = rptYear;
	}
	public String getStatStyle() {
		return statStyle;
	}
	public void setStatStyle(String statStyle) {
		this.statStyle = statStyle;
	}
	public String getData1() {
		return data1;
	}
	public void setData1(String data1) {
		this.data1 = data1;
	}
	public String getData2() {
		return data2;
	}
	public void setData2(String data2) {
		this.data2 = data2;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getDataField3() {
		return dataField3;
	}
	public void setDataField3(String dataField3) {
		this.dataField3 = dataField3;
	}
	public String getData3() {
		return data3;
	}
	public void setData3(String data3) {
		this.data3 = data3;
	}
}
