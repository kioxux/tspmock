/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Queue;

import cn.com.yusys.yusp.domain.CusBizAarCusLst;
import cn.com.yusys.yusp.dto.CusBizAarAppCusLstDto;
import cn.com.yusys.yusp.repository.mapper.CusBizAarCusLstMapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusBizAarApp;
import cn.com.yusys.yusp.repository.mapper.CusBizAarAppMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBizAarAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: pc
 * @创建时间: 2021-05-07 22:13:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusBizAarAppService {

    @Autowired
    private CusBizAarAppMapper cusBizAarAppMapper;

    @Autowired
    private CusBizAarCusLstMapper cusBizAarCusLstMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBizAarApp selectByPrimaryKey(String aarAppSerno) {
        return cusBizAarAppMapper.selectByPrimaryKey(aarAppSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusBizAarApp> selectAll(QueryModel model) {
        List<CusBizAarApp> records = (List<CusBizAarApp>) cusBizAarAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBizAarApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBizAarApp> list = cusBizAarAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusBizAarApp record) {
        return cusBizAarAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusBizAarApp record) {
        return cusBizAarAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusBizAarApp record) {
        return cusBizAarAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusBizAarApp record) {
        return cusBizAarAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String aarAppSerno) {
        return cusBizAarAppMapper.deleteByPrimaryKey(aarAppSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusBizAarAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: delete
     * @方法描述: 删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int delete(String aarAppSerno) {
        QueryModel qm = new QueryModel();
        qm.addCondition("aarAppSerno", aarAppSerno);
        List<CusBizAarCusLst> list = cusBizAarCusLstMapper.selectByModel(qm);
        list.stream().forEach(cusBizAarCusLst -> {
            cusBizAarCusLstMapper.deleteByPrimaryKey(cusBizAarCusLst.getAarCusLstSerno());
        });
        int result = cusBizAarAppMapper.deleteByPrimaryKey(aarAppSerno);
        return result;
    }

    /**
     * @方法名称: insertmore
     * @方法描述: 新增
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertmore(CusBizAarAppCusLstDto dto) {
        CusBizAarApp app = dto.getCusBizAarApp();
        cusBizAarAppMapper.insert(app);
        List<CusBizAarCusLst> list = dto.getList();
        list.stream().forEach(e -> {
            CusBizAarCusLst cusBizAarCusLst = cusBizAarCusLstMapper.selectByPrimaryKey(e.getAarCusLstSerno());
            if (cusBizAarCusLst != null) {
                cusBizAarCusLstMapper.updateByPrimaryKey(e);
            } else {
                cusBizAarCusLstMapper.insert(e);
            }
        });
    }

    /**
     * @方法名称: updatemore
     * @方法描述: 更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public void updatemore(CusBizAarAppCusLstDto dto) {
        CusBizAarApp app = dto.getCusBizAarApp();
        cusBizAarAppMapper.updateByPrimaryKey(app);
        List<CusBizAarCusLst> list = dto.getList();
        list.stream().forEach(e -> {
            CusBizAarCusLst cusBizAarCusLst = cusBizAarCusLstMapper.selectByPrimaryKey(e.getAarCusLstSerno());
            if (cusBizAarCusLst != null) {
                cusBizAarCusLstMapper.updateByPrimaryKey(e);
            } else {
                cusBizAarCusLstMapper.insert(e);
            }
        });
    }
}
