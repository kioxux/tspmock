package cn.com.yusys.yusp.dto;

import java.io.Serializable;


public class IndivCusGuideDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 客户类型 **/
    private String cusType;

    /** 证件类型 STD_ZB_CERT_TYP **/
    private String certType;

    /** 证件号码 **/
    private String certCode;

    /** 开户日期 **/
    private String openDate;

    /** 开户类型 STD_ZB_OPEN_TYP **/
    private String openType;

    /** 客户大类 **/
    private String cusCatalog;

    /** 所属条线 STD_ZB_BIZ_BELG **/
    private String belgLine;

    /** 客户状态 STD_ZB_CUS_ST **/
    private String cusState;

    /** 操作类型  STD_ZB_OPR_TYPE **/
    private String oprType;

    /** 主管机构 **/
    private String mainBrId;

    /** 业务类型**/
    private String bizType;

    /** 是否我行股东**/
    private String isBankSharehd;
    /** 任务流水号**/
    private String serno;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getCusState() {
        return cusState;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    public String getMainBrId() {
        return mainBrId;
    }

    public void setMainBrId(String mainBrId) {
        this.mainBrId = mainBrId;
    }

    public String getBizType() {
        return bizType;
    }

    public String getSerno() {
        return serno;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getOprType() { return oprType; }

    public void setOprType(String oprType) { this.oprType = oprType; }

    public void setIsBankSharehd(String isBankSharehd) {
        this.isBankSharehd = isBankSharehd;
    }

    public String getIsBankSharehd() {
        return isBankSharehd;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }
}
