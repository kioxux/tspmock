/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusManaTask;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusManaTaskMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-04-09 19:55:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusManaTaskMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusManaTask selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusManaTask> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(CusManaTask record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(CusManaTask record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(CusManaTask record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(CusManaTask record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectCount
     * @方法描述: 查询记录数
     * @参数与返回说明:
     * @算法描述: 无
     * @修改人: zhoumw
     */

    int selectCount(@Param("opr_type") String type);

    /**
     * 通过客户号，操作类型查询
     *
     * @param cus_id
     * @return
     */
    int selectByCusId(@Param("cus_id") String cus_id);

    /**
     * @方法名称: selectByPrimarySerno
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusManaTask selectByPrimarySerno(@Param("serno") String serno);


    /**
     * @方法名称: queryCusManaTask
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    List<CusManaTask> queryCusManaTask(QueryModel model);

    /**
     * @函数名称:selectCountByBizType
     * @函数描述:根据业务类型查询任务条数
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    int selectCountByBizType(List<String> bizTypeList);

    /**
     * @函数名称:updateTaskStatusByCusId
     * @函数描述:根据客户号、业务类型更改任务状态为3 已处理
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    int updateTaskStatusByCusId(CusManaTask cusManaTask);

    /**
     * @函数名称:updateTaskStatusByCusId
     * @函数描述:根据客户号、业务类型更改任务状态为3 已处理
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    List<Map<String, Object>> selectNumByInputId(QueryModel model);
}