package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYpqfWhiteApp
 * @类描述: cus_lst_ypqf_white_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 17:23:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusLstYpqfWhiteAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String lywaSerno;
	
	/** 申请日期 **/
	private String appDate;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 管户客户经理 **/
	private String managerId;
	
	/** 所属机构 **/
	private String belgOrg;
	
	/** 申请理由 **/
	private String appResn;
	
	/** 申请有效期 **/
	private String appIdate;
	
	/** 补充说明 **/
	private String spplExpl;
	
	/** 经办人 **/
	private String huser;
	
	/** 经办机构 **/
	private String handOrg;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 最近修改人 **/
	private String lastUpdateId;
	
	/** 最近修改机构 **/
	private String lastUpdateBrId;
	
	/** 最近修改日期 **/
	private String lastUpdateDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新时间 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param lywaSerno
	 */
	public void setLywaSerno(String lywaSerno) {
		this.lywaSerno = lywaSerno == null ? null : lywaSerno.trim();
	}
	
    /**
     * @return LywaSerno
     */	
	public String getLywaSerno() {
		return this.lywaSerno;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}
	
    /**
     * @return AppDate
     */	
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg == null ? null : belgOrg.trim();
	}
	
    /**
     * @return BelgOrg
     */	
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn == null ? null : appResn.trim();
	}
	
    /**
     * @return AppResn
     */	
	public String getAppResn() {
		return this.appResn;
	}
	
	/**
	 * @param appIdate
	 */
	public void setAppIdate(String appIdate) {
		this.appIdate = appIdate == null ? null : appIdate.trim();
	}
	
    /**
     * @return AppIdate
     */	
	public String getAppIdate() {
		return this.appIdate;
	}
	
	/**
	 * @param spplExpl
	 */
	public void setSpplExpl(String spplExpl) {
		this.spplExpl = spplExpl == null ? null : spplExpl.trim();
	}
	
    /**
     * @return SpplExpl
     */	
	public String getSpplExpl() {
		return this.spplExpl;
	}
	
	/**
	 * @param huser
	 */
	public void setHuser(String huser) {
		this.huser = huser == null ? null : huser.trim();
	}
	
    /**
     * @return Huser
     */	
	public String getHuser() {
		return this.huser;
	}
	
	/**
	 * @param handOrg
	 */
	public void setHandOrg(String handOrg) {
		this.handOrg = handOrg == null ? null : handOrg.trim();
	}
	
    /**
     * @return HandOrg
     */	
	public String getHandOrg() {
		return this.handOrg;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param lastUpdateId
	 */
	public void setLastUpdateId(String lastUpdateId) {
		this.lastUpdateId = lastUpdateId == null ? null : lastUpdateId.trim();
	}
	
    /**
     * @return LastUpdateId
     */	
	public String getLastUpdateId() {
		return this.lastUpdateId;
	}
	
	/**
	 * @param lastUpdateBrId
	 */
	public void setLastUpdateBrId(String lastUpdateBrId) {
		this.lastUpdateBrId = lastUpdateBrId == null ? null : lastUpdateBrId.trim();
	}
	
    /**
     * @return LastUpdateBrId
     */	
	public String getLastUpdateBrId() {
		return this.lastUpdateBrId;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate == null ? null : lastUpdateDate.trim();
	}
	
    /**
     * @return LastUpdateDate
     */	
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}