/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndJyxxAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:18:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusLstYndJyxxAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusLstYndJyxxApp selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusLstYndJyxxApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusLstYndJyxxApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusLstYndJyxxApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusLstYndJyxxApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusLstYndJyxxApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @param serno
     * @return java.util.List<cn.com.yusys.yusp.domain.CusLstYndJyxxApp>
     * @author hubp
     * @date 2021/7/16 10:57
     * @version 1.0.0
     * @desc  通过流水号查询经营信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    List<CusLstYndJyxxApp> selectBySerno(String serno);

    /**
     * @方法名称: selectcusLstYndByModel
     * @方法描述: 调查基本信息查询(优农贷)
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0067.resp.List> selectcusLstYndByModel(Map QueryMap);

    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/7/16 14:27
     * @version 1.0.0
     * @desc    通过流水号删除经营信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int deleteBySerno(@Param("serno") String serno);

    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/9/28 13:56
     * @version 1.0.0
     * @desc   过流水号软删经营信息
     * @修改历史:  修改时间    修改人员    修改原因
     */
    int updateTypeBySerno(@Param("serno") String serno);
   
}