package cn.com.yusys.yusp.web.server.xdkh0024;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0024.req.Xdkh0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0024.resp.Xdkh0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0024.Xdkh0024Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:优企贷、优农贷客户基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0024:优企贷、优农贷客户基本信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0024Resource.class);
    @Autowired
    private Xdkh0024Service xdkh0024Service;

    /**
     * 交易码：xdkh0024
     * 交易描述：优企贷、优农贷客户基本信息查询
     *
     * @param xdkh0024DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷客户基本信息查询")
    @PostMapping("/xdkh0024")
    protected @ResponseBody
    ResultDto<Xdkh0024DataRespDto> xdkh0024(@Validated @RequestBody Xdkh0024DataReqDto xdkh0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024DataReqDto));
        Xdkh0024DataRespDto xdkh0024DataRespDto = new Xdkh0024DataRespDto();// 响应Dto:优企贷、优农贷客户基本信息查询
        ResultDto<Xdkh0024DataRespDto> xdkh0024DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024DataReqDto));
            xdkh0024DataRespDto = xdkh0024Service.xdkh0024(xdkh0024DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024DataRespDto));

            // 封装xdkh0024DataResultDto中正确的返回码和返回信息
            xdkh0024DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0024DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, e.getMessage());
            // 封装xdkh0024DataResultDto中异常返回码和返回信息
            xdkh0024DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0024DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0024DataRespDto到xdkh0024DataResultDto中
        xdkh0024DataResultDto.setData(xdkh0024DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024DataResultDto));
        return xdkh0024DataResultDto;
    }
}
