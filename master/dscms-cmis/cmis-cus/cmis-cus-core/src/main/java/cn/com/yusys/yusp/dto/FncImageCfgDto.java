package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncImageCfg
 * @类描述: fnc_image_cfg数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 19:39:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class FncImageCfgDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** serno **/
	private String serno;
	
	/** 根节点(1级目录编号) **/
	private String rootNode;
	
	/** 子节点(3级目录编号资产负债表) **/
	private String childBsNode;
	
	/** 子节点(3级目录编号利润表[损益表]) **/
	private String childIsNode;
	
	/** 应用年份 **/
	private String applyYear;
	
	/** 应用月份 **/
	private String applyMonth;
	
	/** 客户类型 **/
	private String cusType;
	
	/** 客户大类 1 对私 2 对工 **/
	private String cusCatalog;
	
	/** 报表录入类型 **/
	private String reportType;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param rootNode
	 */
	public void setRootNode(String rootNode) {
		this.rootNode = rootNode == null ? null : rootNode.trim();
	}
	
    /**
     * @return RootNode
     */	
	public String getRootNode() {
		return this.rootNode;
	}
	
	/**
	 * @param childBsNode
	 */
	public void setChildBsNode(String childBsNode) {
		this.childBsNode = childBsNode == null ? null : childBsNode.trim();
	}
	
    /**
     * @return ChildBsNode
     */	
	public String getChildBsNode() {
		return this.childBsNode;
	}
	
	/**
	 * @param childIsNode
	 */
	public void setChildIsNode(String childIsNode) {
		this.childIsNode = childIsNode == null ? null : childIsNode.trim();
	}
	
    /**
     * @return ChildIsNode
     */	
	public String getChildIsNode() {
		return this.childIsNode;
	}
	
	/**
	 * @param applyYear
	 */
	public void setApplyYear(String applyYear) {
		this.applyYear = applyYear == null ? null : applyYear.trim();
	}
	
    /**
     * @return ApplyYear
     */	
	public String getApplyYear() {
		return this.applyYear;
	}
	
	/**
	 * @param applyMonth
	 */
	public void setApplyMonth(String applyMonth) {
		this.applyMonth = applyMonth == null ? null : applyMonth.trim();
	}
	
    /**
     * @return ApplyMonth
     */	
	public String getApplyMonth() {
		return this.applyMonth;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType == null ? null : cusType.trim();
	}
	
    /**
     * @return CusType
     */	
	public String getCusType() {
		return this.cusType;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog == null ? null : cusCatalog.trim();
	}
	
    /**
     * @return CusCatalog
     */	
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param reportType
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType == null ? null : reportType.trim();
	}
	
    /**
     * @return ReportType
     */	
	public String getReportType() {
		return this.reportType;
	}


}