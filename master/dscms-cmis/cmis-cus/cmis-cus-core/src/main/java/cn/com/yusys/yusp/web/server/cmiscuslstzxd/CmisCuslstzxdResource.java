package cn.com.yusys.yusp.web.server.cmiscuslstzxd;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusLstZxdDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscuslstzxd.CmisCuslstzxdService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 查询增享贷白名单
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "CmisCuslstzxd:查询增享贷白名单")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCuslstzxdResource {

    private static final Logger logger = LoggerFactory.getLogger(CmisCuslstzxdResource.class);

    @Autowired
    private CmisCuslstzxdService cmisCuslstzxdService;

    /**
     * 交易码：cmiscuslstzxd
     * 交易描述：查询增享贷白名单
     *
     * @param serno
     * @return
     * @throws Exception
     */
    @ApiOperation("查询增享贷白名单")
    @PostMapping("/cmiscuslstzxd")
    protected @ResponseBody
    ResultDto<CusLstZxdDto> cmiscuslstzxd(@Validated @RequestBody String serno) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.key, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.value, serno);
        CusLstZxdDto cusLstZxdDto = new CusLstZxdDto();// 响应Dto:查询增享贷白名单
        ResultDto<CusLstZxdDto> cusLstZxdDtoResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.key, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.value, serno);
            cusLstZxdDto = cmisCuslstzxdService.cmiscuslstzxd(serno);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.key, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.value, JSON.toJSONString(cusLstZxdDto));
            // 封装cmisCus0016ResultDto中正确的返回码和返回信息
            cusLstZxdDtoResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cusLstZxdDtoResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.key, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.value, e.getMessage());
            // 封装cmisCus0016ResultDto中异常返回码和返回信息
            cusLstZxdDtoResultDto.setCode(EpbEnum.EPB099999.key);
            cusLstZxdDtoResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0016RespDto到cmisCus0016ResultDto中
        cusLstZxdDtoResultDto.setData(cusLstZxdDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.key, DscmsEnum.TRADE_CODE_CMISCUSLSTZXD.value, JSON.toJSONString(cusLstZxdDtoResultDto));
        return cusLstZxdDtoResultDto;
    }
}
