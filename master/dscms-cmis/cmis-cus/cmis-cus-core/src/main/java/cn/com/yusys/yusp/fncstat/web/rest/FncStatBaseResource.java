/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.web.rest;

import cn.com.yusys.yusp.fncstat.dto.FinanIndicAnalyDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.fncstat.domain.FncStatBase;
import cn.com.yusys.yusp.fncstat.service.FncStatBaseService;

/**
 * @version 1.0.0
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatBaseResource
 * @类描述: #资源类
 * @功能描述: 财务报表信息
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-23 11:39:41
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/nrcs-cms/fncstatbase")
@Api(tags = "FncStatBaseResource", description = "财务报表基本信息")
public class FncStatBaseResource {
    @Autowired
    private FncStatBaseService fncStatBaseService;

    /**
     * 财务报表信息查询
     *
     * @param model
     * @return
     */
    @GetMapping("/q/fncstatbase/list")
    @ApiOperation("NRCS601061财务报表信息查询")
//    @TraceBaggage(functionCode = "NRCS601061")
    public ResultDto<List<FncStatBase>> queryFncStatBaseList(QueryModel model) {
        return new ResultDto<List<FncStatBase>>(fncStatBaseService.queryFncStatBaseList(model));
    }

    /**
     * 财务报表信息查询-集团
     *
     * @param model
     * @return
     */
    @GetMapping("/q/fncstatbase/queryFncStatBaseGrpList")
    @ApiOperation("NRCS60106101集团财务报表信息查询")
//    @TraceBaggage(functionCode = "NRCS601061")
    public ResultDto<List<FncStatBase>> queryFncStatBaseGrpList(QueryModel model) {
        return new ResultDto<List<FncStatBase>>(fncStatBaseService.queryFncStatBaseGrpList(model));
    }

    /**
     * 最新完成财务报表信息查询
     *
     * @param fncStatBase
     * @return
     */
    @PostMapping("/q/fncstatbase/last")
    @ApiOperation("NRCS601062最新完成财务报表信息查询")
//    @TraceBaggage(functionCode = "NRCS601061")
    public ResultDto<FncStatBase> queryFncStatBaseLastReport(@RequestBody FncStatBase fncStatBase) {
        return new ResultDto<FncStatBase>(fncStatBaseService.queryFncStatBaseLastReport(fncStatBase.getCusId()));
    }

    /**
     * 财务报表信息新增
     *
     * @param fncStatBase
     * @return
     */
    @PostMapping("/s/fncstatbase/add")
    @ApiOperation("NRCS601073财务报表信息新增")
//    @TraceBaggage(functionCode = "NRCS601073")
    public ResultDto<FncStatBase> addFncStatBase(@RequestBody FncStatBase fncStatBase) {
        return new ResultDto<FncStatBase>(fncStatBaseService.addFncStatBase(fncStatBase));
    }

    /**
     * 财务报表信息修改
     *
     * @param fncStatBase
     * @return
     */
    /*
     * @PostMapping("/s/fncstatbase/update") public ResultDto<FncStatBase>
     * updateFncStatBase(@RequestBody FncStatBase fncStatBase){ return new
     * ResultDto<FncStatBase>(fncStatBaseService.updateFncStatBase(fncStatBase)); }
     */

    /**
     * 财务报表信息删除
     * @param map
//     * @param cusId        客户代码
//     * @param statStyle    报表口径
//     * @param statPrdStyle 报表周期类型
//     * @param statPrd      报表期间
     * @return
     */
    @PostMapping("/s/fncstatbase/delete")
    @ApiOperation("NRCS601084财务报表信息删除")
//    @TraceBaggage(functionCode = "NRCS601084")
    public ResultDto<Integer> deleteFncStatBase(@RequestBody HashMap<String, String> map) {
        ResultDto<Integer> dto = new ResultDto<Integer>();
        int result = fncStatBaseService.deleteFncStatBase(map.get("cusId"), map.get("statStyle"),
                map.get("statPrdStyle"), map.get("statPrd"));
        if (-1 == result) {
            dto.setCode("500");
            dto.setMessage("不存在对应的财务报表信息！");
        } else {
            dto.setData(result);
        }
        return dto;
    }

    /**
     * 财务报表信息查看
     *
     * @param cusId        客户代码
     * @param statStyle    报表口径
     * @param statPrdStyle 报表周期类型
     * @param statPrd      报表期间
     * @param fncConfTyp   报表种类(11:资产负债表(个人) 、12:损益表(个人) )
     * @return
     */
    @GetMapping("/q/fncstatbase/detail")
    @ApiOperation("NRCS601102财务报表信息查看")
//    @TraceBaggage(functionCode = "NRCS601102")
    public ResultDto<FncStatBase> queryFncStatBaseByKey(@RequestParam String cusId, @RequestParam String statStyle,
                                                        @RequestParam String statPrdStyle, @RequestParam String statPrd, @RequestParam String fncConfTyp) {
        return new ResultDto<FncStatBase>(
                fncStatBaseService.queryFncStatBaseByKey(cusId, statStyle, statPrdStyle, statPrd, fncConfTyp));
    }

    /*
     * @GetMapping("/q/fncstatbase/detail") public ResultDto<FncStatBase>
     * queryFncStatBaseByKey(@RequestBody HashMap<String, String> map) { return new
     * ResultDto<FncStatBase>(fncStatBaseService.queryFncStatBaseByKey(map.get(
     * "cusId"), map.get("statStyle"), map.get("statPrdStyle"), map.get("statPrd"),
     * map.get("fncConfTyp"))); }
     */

    /**
     * 财务报表信息解锁
     *
     * @param fncStatBase
     * @return
     */
    @PostMapping("/s/fncstatbaseunlock/update")
    @ApiOperation("NRCS601140财务报表信息解锁")
//    @TraceBaggage(functionCode = "NRCS601140")
    public ResultDto<Integer> updateUnLock(@RequestBody FncStatBase fncStatBase) {
        return new ResultDto<Integer>(fncStatBaseService.updateUnLock(fncStatBase));
    }

    /**
     * 维护审计信息
     *
     * @param fncStatBase
     * @return
     */
    @PostMapping("/s/fncstatbase/auditinfo/update")
    @ApiOperation("NRCS601094财务报表维护审计信息")
//    @TraceBaggage(functionCode = "NRCS601094")
    public ResultDto<Integer> updateAuditInfo(@RequestBody FncStatBase fncStatBase) {
        return new ResultDto<Integer>(fncStatBaseService.updateAuditInfo(fncStatBase));
    }
    /**
     * 财务报表导出模板
     *
     * @param response
     */

      @GetMapping("/q/fncstatbaseimport/exportTemplate")
      public void exportTemplate(HttpServletResponse response) {
      fncStatBaseService.exportTemplate(response);
      }

    /**
     * 财务报表导入
     *
     * @param multipartFile
     */

      @PostMapping("/s/fncstatbaseimport/add")
      public ResultDto<String> importExcelFile(@RequestParam("file") MultipartFile multipartFile) { return
      new ResultDto<String>(fncStatBaseService.importExcelFile(multipartFile)); }

    /**
     * 财务报表导出
     *
     * @param fetchSize
     *            导出数量，不设置为全部导出
     * @param model
     * @return
     */

      @GetMapping("/q/fncstatbaseexport/detail")
      public ResultDto<String> exportExcelFile(Integer fetchSize,QueryModel model) {
          return new ResultDto<String>(fncStatBaseService.exportExcelFile(fetchSize,model));
      }
      
      /**
       * 发送OCR识别
       * @return
       */
      @PostMapping("/q/fncstatbase/sendOcr")
      public ResultDto<Integer> sendOcr(@RequestBody FncStatBase fncStatBase) {
      	fncStatBaseService.sendOrc(fncStatBase);
      	return ResultDto.success().data(1);
      }
      
      /**
       * 查询ocr返回结果、保存结果
       * @param fncStatBase
       * @return
       */
      @PostMapping("/q/fncstatbase/saveOcrResult")
      public ResultDto<Integer> saveOcrResult(@RequestBody FncStatBase fncStatBase) {
    	  fncStatBaseService.saveOcrResult(fncStatBase);
    	  return ResultDto.success(1);
      }

    /**
     * 获取客户编号对应的财务指标分析列表字段数据
     * @param cusId
     * @return
     */
    @PostMapping("/selectfinanindicanalysfncitemdatabycusid")
    public ResultDto<List<FinanIndicAnalyDto>> selectFinanindicAnalysFncItemDataByCusId(@RequestBody String cusId) {
        return ResultDto.success(fncStatBaseService.selectFinanindicAnalysFncItemDataByCusId(cusId));
    }

    /**
     * 调查报告去财报利润总额字段
     * @param map
     * @return
     */
    @PostMapping("/getRptFncTotalProfit")
    public ResultDto<List<FinanIndicAnalyDto>> getRptFncTotalProfit(@RequestBody Map map){
        return new ResultDto<List<FinanIndicAnalyDto>>(fncStatBaseService.getRptFncTotalProfit(map));
    }

    /**
     *
     * @param cusId
     * @return
     */
    @PostMapping("/getRptFncTotalProfitForLmtHighCurfundEval")
    public ResultDto<Map<String,FinanIndicAnalyDto>> getRptFncTotalProfitForLmtHighCurfundEval(@RequestBody String cusId){
        return new ResultDto<Map<String,FinanIndicAnalyDto>>(fncStatBaseService.getRptFncTotalProfitForLmtHighCurfundEval(cusId));
    }

    /**
     * 财报科目净利润连续两年取值，上一年度或当期资产负债表的资产负债率取值
     * @创建者：zhangliang15
     * @param cusId
     * @return
     */
    @PostMapping("/getFinRepRetProAndRatOfLia")
    public ResultDto<Map<String,FinanIndicAnalyDto>> getFinRepRetProAndRatOfLia(@RequestBody String cusId){
        return new ResultDto<Map<String,FinanIndicAnalyDto>>(fncStatBaseService.getFinRepRetProAndRatOfLia(cusId));
    }

    @PostMapping("/selectFncByCusIdRptYear")
    public ResultDto<Map> selectFncByCusIdRptYear(@RequestBody Map map){
        String cusId = map.get("cusId").toString();
        String statPrd = map.get("statPrd").toString();
        return new ResultDto<Map>(fncStatBaseService.selectFncByCusIdRptYear(cusId,statPrd));
    }
}
