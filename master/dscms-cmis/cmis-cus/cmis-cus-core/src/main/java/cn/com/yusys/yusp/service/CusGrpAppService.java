/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.exception.PlatformException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.CusGrp;
import cn.com.yusys.yusp.domain.CusGrpApp;
import cn.com.yusys.yusp.domain.CusGrpMemberApp;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.dto.CusGrpDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.CircleArrayMem;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040MemberListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.req.CmisLmt0040ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0040.resp.CmisLmt0040RespDto;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.repository.mapper.CusGrpAppMapper;
import cn.com.yusys.yusp.repository.mapper.CusGrpMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: fangzhen
 * @创建时间: 2021-03-05 14:24:02
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusGrpAppService {
    private final Logger log = LoggerFactory.getLogger(CusGrpAppService.class);

    @Autowired
    private CusGrpAppMapper cusGrpAppMapper;

    @Autowired
    private CusGrpService cusGrpService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CusBaseService cusBaseService;

    @Autowired
    private CusGrpMemberAppService cusGrpMemberAppService;

    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;

    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    @Autowired
    private CusGrpMapper cusGrpMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusGrpApp selectByPrimaryKey(String serno) {
        return cusGrpAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusGrpApp> selectAll(QueryModel model) {
        List<CusGrpApp> records = (List<CusGrpApp>) cusGrpAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusGrpApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusGrpApp> list = cusGrpAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusGrpApp record) {
        return cusGrpAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusGrpApp record) {
        return cusGrpAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusGrpApp record) {
        return cusGrpAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusGrpApp record) {
        //集团客户编号
        String grpNo = record.getGrpNo();
        //集团客户申请类型
        String appType = record.getAppType();

        if (CmisCusConstants.STD_GRP_APP_TYPE_01.equals(appType)){
            //如果是集团客户认定，则需要校验集团编号是否已经存在
            CusGrpDto cusGrpDto = cusGrpService.selectCusGrpDtoByGrpNo(grpNo);

            if (cusGrpDto!=null && StringUtils.nonEmpty(cusGrpDto.getGrpNo())){
                throw new BizException(null,"",null,"集团客户编号已存在！");
            }
        }

        return cusGrpAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusGrpAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusGrpAppMapper.deleteByIds(ids);
    }

    /**
     * 改变标志 待发起000 -> 审批中111
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterStart(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请" + serno + "申请主表信息");
            CusGrpApp cusGrpApp = new CusGrpApp();
            cusGrpApp.setSerno(serno);
            CusGrpApp cusGrpApp1 = this.cusGrpAppMapper.selectByPrimaryKey(serno);
            if (cusGrpApp1 == null) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请" + serno + "流程审批状态为【111】-审批中");
            cusGrpApp.setApproveStatus("111");
            int result = this.cusGrpAppMapper.updateByPrimaryKeySelective(cusGrpApp);
            if (result < 0) {
                throw new YuspException(EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcsEnum.E_CUS_HANDLE_EXCEPTION.key, EcsEnum.E_CUS_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 改变标志 审批中111 -> 已通过997
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterEnd(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请" + serno + "申请主表信息");
            CusGrpApp cusGrpApp = new CusGrpApp();
            cusGrpApp.setSerno(serno);
            CusGrpApp cusGrpApp1 = this.cusGrpAppMapper.selectByPrimaryKey(serno);
            if (cusGrpApp1 == null) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请" + serno + "流程审批状态为【997】-已通过");
            cusGrpApp.setApproveStatus("997");
            int result = this.cusGrpAppMapper.updateByPrimaryKeySelective(cusGrpApp);

            if (result < 0) {
                throw new YuspException(EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
            //需要去判断执行的是更改还是认定的操作   根据该条件的cus_id  去查询集团成员信息表   是否主关联客户为是的数据  看是否有返回值如果返回值不为空 证明执行的是集团客户主客户变更  如果返回值为空执行的是主集团客户系只能操作
//            CusGrpMember cusGrpMemberNew = this.cusGrpMemberServic.selectByCusIdAndIsMainCus(cusGrpApp1.getParentCusId());
//            CusGrpMember cusGrpMember = new CusGrpMember();
//            int insertResult;
//            if (cusGrpMemberNew == null) {
//                //执行的是新增的操作
//                //需要向主表写入一条主关联客户信息
//                //封装数据  进行数据插入操作
//                cusGrpMember.setPkId(UUID.randomUUID().toString());
//                insertResult = this.cusGrpMemberServic.insert(cusGrpMember);
//            } else {
//                //更新数据
//                cusGrpMember.setPkId(cusGrpMemberNew.getPkId());
//                BeanUtils.copyProperties(cusGrpApp1, cusGrpMember);
//                cusGrpMember.setCusId(cusGrpApp1.getParentCusId());
//                cusGrpMember.setAvailableInd("A");
//                cusGrpMember.setIsMainCus("Y");
//                cusGrpMember.setMainBrId(cusGrpApp1.getManagerBrId());
//                cusGrpMember.setMainId(cusGrpApp1.getManagerId());
//                insertResult = this.cusGrpMemberServic.updateSelective(cusGrpMember);
//            }
//            if (insertResult < 0) {
//                throw new YuspException(EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
//            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcsEnum.E_CUS_HANDLE_EXCEPTION.key, EcsEnum.E_CUS_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * // 否决改变标志 审批中 111 -> 打回 992
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfteCallBack(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请" + serno + "申请主表信息");
            CusGrpApp cusGrpApp = new CusGrpApp();
            cusGrpApp.setSerno(serno);
            CusGrpApp cusGrpApp1 = this.cusGrpAppMapper.selectByPrimaryKey(serno);
            if (cusGrpApp1 == null) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请" + serno + "流程审批状态为【992】-打回");
            cusGrpApp.setApproveStatus("992");
            int result = this.cusGrpAppMapper.updateByPrimaryKeySelective(cusGrpApp);
            if (result < 0) {
                throw new YuspException(EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcsEnum.E_CUS_HANDLE_EXCEPTION.key, EcsEnum.E_CUS_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 否决改变标志 审批中 111 -> 拿回 991
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfteTackBack(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请" + serno + "申请主表信息");
            CusGrpApp cusGrpApp = new CusGrpApp();
            cusGrpApp.setSerno(serno);
            CusGrpApp cusGrpApp1 = this.cusGrpAppMapper.selectByPrimaryKey(serno);
            if (cusGrpApp1 == null) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请" + serno + "流程审批状态为【991】-拿回");
            cusGrpApp.setApproveStatus("991");
            int result = this.cusGrpAppMapper.updateByPrimaryKeySelective(cusGrpApp);
            if (result < 0) {
                throw new YuspException(EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcsEnum.E_CUS_HANDLE_EXCEPTION.key, EcsEnum.E_CUS_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 否决改变标志 审批中 111 -> 否决 998
     *
     * @param serno
     */
    @Transactional(rollbackFor = Exception.class)
    public void handleBusinessDataAfterRefuse(String serno) {
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.value);
            }

            log.info("流程发起-获取业务申请" + serno + "申请主表信息");
            CusGrpApp cusGrpApp = new CusGrpApp();
            cusGrpApp.setSerno(serno);
            CusGrpApp cusGrpApp1 = this.cusGrpAppMapper.selectByPrimaryKey(serno);
            if (cusGrpApp1 == null) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请" + serno + "流程审批状态为【998】-否决");
            cusGrpApp.setApproveStatus("998");
            int result = this.cusGrpAppMapper.updateByPrimaryKeySelective(cusGrpApp);
            if (result < 0) {
                throw new YuspException(EcsEnum.CUS_CLIENT_DEF_EXCEPTION.key, EcsEnum.CUS_CLIENT_DEF_EXCEPTION.value);
            }
        } catch (YuspException e) {
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcsEnum.E_CUS_HANDLE_EXCEPTION.key, EcsEnum.E_CUS_HANDLE_EXCEPTION.value);
        }
    }

    /**
     * 根据客户号查询该核心客户是否存在在途认定，变更，解散申请
     *
     * @param record
     * @return
     */
    public Map<String, String> selectByCusId(CusGrpApp record) {
        //校验
        Map rtnData = new HashMap();
        String rtnCode = EcsEnum.CUS_GRP_SUCCESS_DEF.key;
        String rtnMsg = EcsEnum.CUS_GRP_SUCCESS_DEF.value;
        try {

            //校验核心客户是否存在在途
            this.checkInWayGrpApp(record);

            //查看该核心客户是否已经存在集团核心客户中
            cusGrpService.checkCusGrpCoreId(record.getCoreCusId());

            //调用Ecif 客户集团信息查询,获取集团客户编号
            /**
             * 识别方式:
             * 1- 按集团编号查询
             * 2- 按客户编号查询
             */
            /***********************************  调用接口开始  ***************************************************************/
//            String grpNo = sequenceTemplateClient.getSequenceTemplate("CUS_GRP_SERNO", new HashMap<>());
//            record.setGrpNo(grpNo);
            G11004ReqDto g11004ReqDto = new G11004ReqDto();
            g11004ReqDto.setResotp("2");
            g11004ReqDto.setCustno(record.getCoreCusId());
            ResultDto<G11004RespDto> resultDto = dscms2EcifClientService.g11004(g11004ReqDto);
            String g11004Code = Optional.ofNullable(resultDto.getCode()).orElse(StringUtils.EMPTY);
            String g11004Meesage = Optional.ofNullable(resultDto.getMessage()).orElse(StringUtils.EMPTY);
            G11004RespDto g11004RespDto = null;
            // TODO 获取相关的值并解析
            // 获取查询结果 查询成功失败的情况
            g11004RespDto = resultDto.getData();
            if (!ObjectUtils.isEmpty(g11004RespDto) && !StringUtils.isEmpty(g11004RespDto.getCrpcno())) {
                //record.setCoreCusId(g11004RespDto.getCustno());
                record.setGrpNo(g11004RespDto.getGrouno());

                //数据落表到集团申请子表中
                int result = cusGrpAppMapper.insert(record);
                if (result>0){
                    //落地子公司信息
                    boolean resultCusMemApp = cusGrpMemberAppService.insertByCusGrpApp(record);
                    if (!resultCusMemApp){
                        throw new YuspException(EcsEnum.E_SAVE_FAIL.key,"\"落地子公司信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                    }
                } else {
                    throw new YuspException(EcsEnum.E_SAVE_FAIL.key,"\"保存集团申请表失败\"" + EcsEnum.E_SAVE_FAIL.value);
                }
            } else {
                //调用Ecif 创建集团客户接口，获取集团客户编号
                G11003ReqDto g11003ReqDto = new G11003ReqDto();
                /**
                 * 集团成员状态
                 * 0：无效
                 * 1：有效
                 */
                g11003ReqDto.setGrpsta("0");	//集团客户状态
                g11003ReqDto.setGrouna(record.getGrpName());	//集团客户名称
                g11003ReqDto.setCustno(record.getCoreCusId());	//客户编号
                g11003ReqDto.setCustln(record.getCoreCusLoanCardId());	//客户中证码
                g11003ReqDto.setCustna(record.getCoreCusName());	//客户名称
                g11003ReqDto.setIdtfno(record.getCoreCusCertNo());	//证件号码
                g11003ReqDto.setOadate(DateUtils.formatDate10To8(record.getUpdateOfficeAddrDate()));	//更新办公地址日期
                g11003ReqDto.setHomead(record.getOfficeAddr());	//地址
                g11003ReqDto.setSocino(record.getSociCredCode());	//社会信用代码
                g11003ReqDto.setBusino(record.getBusinessCirclesRegiNo());	//工商登记注册号
                g11003ReqDto.setOfaddr(record.getOfficeAddrAdminDiv());	//办公地址行政区划
                g11003ReqDto.setCrpdeg(record.getGrpCloselyDegree());	//集团紧密程度
                g11003ReqDto.setCrpcno(record.getGrpCaseMemo());	//集团客户情况说明
                g11003ReqDto.setCtigno(record.getManagerId());	//管户客户经理
                g11003ReqDto.setCitorg(record.getManagerBrId());	//所属机构
                g11003ReqDto.setOprtye(record.getOprType());	//操作类型
                g11003ReqDto.setInptno(record.getInputId());	//登记人
                g11003ReqDto.setInporg(record.getInputBrId());	//登记机构

                CircleArrayMem circleArrayMem = new CircleArrayMem();
                circleArrayMem.setCustno(record.getCoreCusId());	//集团成员客户号
                circleArrayMem.setCortype(StringUtils.EMPTY);	//集团关系类型
                circleArrayMem.setGrpdesc(StringUtils.EMPTY);	//集团关系描述
                circleArrayMem.setIscust("1");	//是否集团主客户
                circleArrayMem.setMainno(record.getManagerId());	//主管人
                circleArrayMem.setMaiorg(record.getManagerBrId());	//主管机构
                circleArrayMem.setSocino(record.getSociCredCode());	//统一社会信用代码
                circleArrayMem.setCustst("0");	//成员状态
                List<CircleArrayMem> circleArrayMemList = new ArrayList<>();
                circleArrayMemList.add(circleArrayMem);
                g11003ReqDto.setCircleArrayMem(circleArrayMemList);

                ResultDto<G11003RespDto> g11003ResultDto = dscms2EcifClientService.g11003(g11003ReqDto);
                String g11003Code = Optional.ofNullable(g11003ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String g11003Meesage = Optional.ofNullable(g11003ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                G11003RespDto g11003RespDto = null;
                //  获取相关的值并解析
                g11003RespDto = g11003ResultDto.getData();
                if (!ObjectUtils.isEmpty(g11003RespDto) && !StringUtils.isEmpty(g11003RespDto.getGrouno())) {
                    record.setGrpNo(g11003RespDto.getGrouno());
                } else {
                    //  抛出错误异常
                    throw new YuspException(g11003Code, g11003Meesage);
                }

                //数据落表到集团申请子表中
                int result = cusGrpAppMapper.insert(record);
                if (result>0){
                    //落地子公司信息
                    boolean resultCusMemApp = cusGrpMemberAppService.insertByCusGrpApp(record);
                    if (!resultCusMemApp){
                        throw new YuspException(EcsEnum.E_SAVE_FAIL.key,"\"落地子公司信息失败\"" + EcsEnum.E_SAVE_FAIL.value);
                    }
                } else {
                    throw new YuspException(EcsEnum.E_SAVE_FAIL.key,"\"保存集团申请表失败\"" + EcsEnum.E_SAVE_FAIL.value);
                }
            }

            /***********************************  调用接口结束  **************************************************************************************/

        } catch (YuspException e) {
            log.error("集团新增核心客户业务失败！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * @方法名称: updateCusGrpMemInfo
     * @方法描述: 集团修改进去，修改核心客户时，同步校验并更新成员中的母公司信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map<String, String> updateCusGrpMemInfo(CusGrpApp record) {
        //校验
        Map rtnData = new HashMap();
        String rtnCode = EcsEnum.CUS_GRP_SUCCESS_DEF.key;
        String rtnMsg = EcsEnum.CUS_GRP_SUCCESS_DEF.value;
        try {
            //校验核心客户是否存在在途
            this.checkInWayGrpApp(record);

            //查看该核心客户是否已经存在集团核心客户中
            cusGrpService.checkCusGrpCoreId(record.getCoreCusId());
            //更新申请信息
            cusGrpAppMapper.updateByPrimaryKey(record);

            //根据客户号以及操作类型，查询
            QueryModel queryModel = new QueryModel();
            //客户号
            queryModel.addCondition("grpCorreType", CmisCusConstants.STD_ZB_GRP_CORRE_TYPE_1);
            //申请流水号
            queryModel.addCondition("serno", record.getSerno());
            //操作类型 默认 01 新增
            queryModel.addCondition("oprType", CmisCusConstants.OPR_TYPE_INSERT);
            //获取对应的集团申请的成员信息
            List<CusGrpMemberApp> list = cusGrpMemberAppService.selectByModel(queryModel);
            if (list.size() > 0) {
                CusGrpMemberApp cusGrpMemberApp = list.get(0);
                //根据主键删除该信息
                cusGrpMemberAppService.deleteByPrimaryKey(cusGrpMemberApp.getPkId());
            }
            //重新落地子公司信息
            cusGrpMemberAppService.insertByCusGrpApp(record);
        } catch (YuspException e) {
            log.error("集团新增核心客户业务失败！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }

    /**
     * 描述：校验核心客户是否已经存在集团在途业务中
     *
     * @param record
     */
    public void checkInWayGrpApp(CusGrpApp record) {
        log.info("集团新增核心客户认定数据【{}】", JSONObject.toJSON(record));
        //查询客户是否存在途申请
        HashMap<String, String> paramMap = new HashMap();
        paramMap.put("coreCusId", record.getCoreCusId());
        int result = cusGrpAppMapper.selectByCusId(paramMap);
        if (result > 0) {
            throw new YuspException(EcsEnum.CUS_GRP_APP_CHECK_DEF.key, EcsEnum.CUS_GRP_APP_CHECK_DEF.value);
        }
    }

    /**
     * @方法名称: checkCusGrpAppInWay
     * @方法描述: 变更，解散校验是否存在在途
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map checkCusGrpAppInWay(CusGrpApp record) {
        log.info("集团变更，解散校验是否存在在途【{}】", JSONObject.toJSON(record));
        HashMap<String, String> resultMap = new HashMap();
        String rtnCode = EcsEnum.CUS_GRP_SUCCESS_DEF.key;
        String rtnMsg = EcsEnum.CUS_GRP_SUCCESS_DEF.value;
        try {
            //查询客户是否存在途申请
            HashMap<String, String> paramMap = new HashMap();
            paramMap.put("grpNo", record.getGrpNo());
            int result = cusGrpAppMapper.selectByCusId(paramMap);
            if (result > 0) {
                throw new YuspException(EcsEnum.CUS_GRP_APP_INWAY_CHECK_DEF.key, EcsEnum.CUS_GRP_APP_INWAY_CHECK_DEF.value);
            }
        } catch (YuspException e) {
            log.error("集团变更，解散校验是否存在在途失败！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            resultMap.put("rtnCode", rtnCode);
            resultMap.put("rtnMsg", rtnMsg);
        }

        return resultMap;
    }

    /**
     * 挡板处理，流程添加后，移植到-> handleBusinessDataAfterEnd 方法中
     * 否决改变标志 审批中 111 -> 否决 998
     *
     * @param cusGrpApp
     */
    public void flowApprovePass(CusGrpApp cusGrpApp) {
        try {
            String serno = cusGrpApp.getSerno();
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-更新业务申请" + serno + "流程审批状态为【997】-审批通过");
            cusGrpApp.setApproveStatus("997");
            int result = this.cusGrpAppMapper.updateByPrimaryKeySelective(cusGrpApp);
            if (result < 0) {
                throw new YuspException(EcsEnum.CUS_GRP_SQL_UPDATE_DEF.key, EcsEnum.CUS_GRP_SQL_UPDATE_DEF.value);
            }

            /*******************数据更新到正式表中**********/
            //获取集团客户号
            String grpNo = cusGrpApp.getGrpNo();
            int resultCount;
            //集团紧密程度
            CusGrp cusGrp = cusGrpService.createCusGrpRecordByCusGrpApp(cusGrpApp);
            //执行子成员落表操作
            QueryModel queryModel = new QueryModel();
            //申请流水号
            queryModel.addCondition("serno", cusGrpApp.getSerno());
            //操作类型 默认01 新增 02 删除
            queryModel.addCondition("oprType", cusGrpApp.getOprType());
            //查询本次申请中的集团成员信息（操作状态为01 新增）
            List<CusGrpMemberApp> cusGrpMemberAppList = cusGrpMemberAppService.selectByModel(queryModel);
            CusGrpMemberRel cusGrpMemberRel = new CusGrpMemberRel();

            //获取变更类型  01 认定  02 变更  03 解散
            String appType = cusGrpApp.getAppType();

            //调用Ecif接口维护集团信息
            cusGrp.setGrpNo(getEcifGrpNp(appType, cusGrpApp, cusGrp, cusGrpMemberAppList));

            //集团认定
            if (CmisCusConstants.STD_GRP_APP_TYPE_01.equals(appType)) {
                //数据落表
                resultCount = cusGrpService.insert(cusGrp);
                if (resultCount <= 0) {
                    throw new YuspException(EcsEnum.CUS_GRP_SQL_INSERT_DEF.key, EcsEnum.CUS_GRP_SQL_INSERT_DEF.value);
                }

                //遍历成员集团 cusGrpMemberRelService
                for (CusGrpMemberApp cusGrpMemberApp : cusGrpMemberAppList) {
                    cusGrpMemberRel = cusGrpMemberRelService.createCusGrpMemRelByCusMemApp(cusGrpMemberApp, cusGrpApp);
                    resultCount = cusGrpMemberRelService.insert(cusGrpMemberRel);
                    if (resultCount <= 0) {
                        throw new YuspException(EcsEnum.CUS_GRP_SQL_INSERT_DEF.key, EcsEnum.CUS_GRP_SQL_INSERT_DEF.value);
                    }
                }
            } else if (CmisCusConstants.STD_GRP_APP_TYPE_02.equals(appType)) {
                //集团变更
                resultCount = cusGrpService.update(cusGrp);
                if (resultCount <= 0) {
                    throw new YuspException(EcsEnum.CUS_GRP_SQL_UPDATE_DEF.key, EcsEnum.CUS_GRP_SQL_UPDATE_DEF.value);
                }

                //遍历成员集团 分项成员 处理
                for (CusGrpMemberApp cusGrpMemberApp : cusGrpMemberAppList) {
                    //变更类型
                    String chgType = cusGrpMemberApp.getModifyType();
                    //原有不变的不处理
                    if (CmisCusConstants.STD_CUS_MODIFY_TYPE_01.equals(chgType)) {

                    } else if (CmisCusConstants.STD_CUS_MODIFY_TYPE_02.equals(chgType)) {
                        //本次新增插入处理
                        cusGrpMemberRel = cusGrpMemberRelService.createCusGrpMemRelByCusMemApp(cusGrpMemberApp, cusGrpApp);
                        resultCount = cusGrpMemberRelService.insert(cusGrpMemberRel);
                        if (resultCount <= 0) {
                            throw new YuspException(EcsEnum.CUS_GRP_SQL_INSERT_DEF.key, EcsEnum.CUS_GRP_SQL_INSERT_DEF.value);
                        }
                    } else {
                        //本次退出 available_ind 是否有效 更改失效I
                        //查询客户信息，更新成员状态
                        queryModel = new QueryModel();
                        queryModel.addCondition("grpNo", grpNo);
                        queryModel.addCondition("cusId", cusGrpMemberApp.getCusId());
                        List<CusGrpMemberRel> cusGrpMemberRelList = cusGrpMemberRelService.selectByModel(queryModel);
                        if (cusGrpMemberRelList != null) {
                            if (cusGrpMemberRelList.size() != 1) {
                                throw new YuspException(EcsEnum.CUS_GRP_SQL_SELECT_DEF.key, EcsEnum.CUS_GRP_SQL_SELECT_DEF.value);
                            }
                            CusGrpMemberRel cusGrpMemberRel1 = cusGrpMemberRelList.get(0);
                            cusGrpMemberRel1.setAvailableInd(CmisCusConstants.STD_AVAILABLE_IND_0);
                            resultCount = cusGrpMemberRelService.update(cusGrpMemberRel1);
                            if (resultCount <= 0) {
                                throw new YuspException(EcsEnum.CUS_GRP_SQL_UPDATE_DEF.key, EcsEnum.CUS_GRP_SQL_UPDATE_DEF.value);
                            }
                        }
                    }
                }

            } else {
                //集团解散   集团客户状态更新为  02 失效
                cusGrp.setGroupCusStatus(CmisCusConstants.STD_GROUP_CUS_STATUS_02);
                resultCount = cusGrpService.update(cusGrp);
                if (resultCount <= 0) {
                    throw new YuspException(EcsEnum.CUS_GRP_SQL_UPDATE_DEF.key, EcsEnum.CUS_GRP_SQL_UPDATE_DEF.value);
                }
                Map paramMap = new HashMap();
                //遍历成员集团 分项成员 处理
                for (CusGrpMemberApp cusGrpMemberApp : cusGrpMemberAppList) {
                    paramMap.put("grpNo", grpNo);
                    paramMap.put("cusId", cusGrpMemberApp.getCusId());
                    resultCount = cusGrpMemberRelService.updateByContinionAvai(paramMap);
                    if (resultCount <= 0) {
                        throw new YuspException(EcsEnum.CUS_GRP_SQL_UPDATE_DEF.key, EcsEnum.CUS_GRP_SQL_UPDATE_DEF.value);
                    }
                }
            }
        } catch (YuspException e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw e;
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcsEnum.E_CUS_HANDLE_EXCEPTION.key, EcsEnum.E_CUS_HANDLE_EXCEPTION.value);
        }
    }

    /*** 
     * @param appType 操作类型
     * @param cusGrp 集团主表
     * @param cusGrpMemberAppList 集团成员
     * @return java.lang.String Ecif返回的集团编号
     * @author tangxun
     * @date 2021/4/15 下午4:20
     * @version 1.0.0
     * @desc 调用Ecif接口维护集团信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private String getEcifGrpNp(String appType, CusGrpApp cusGrpApp, CusGrp cusGrp, List<CusGrpMemberApp> cusGrpMemberAppList) {
        //组装报文 开始
        G11003ReqDto g11003ReqDto = new G11003ReqDto();
        g11003ReqDto.setGrouna(cusGrp.getGrpName());
        g11003ReqDto.setCustno(cusGrp.getCoreCusId());
        g11003ReqDto.setCustln(cusGrp.getCoreCusLoanCardId());
        g11003ReqDto.setIdtfno(cusGrp.getCoreCusCertNo());
        g11003ReqDto.setOadate(cusGrp.getUpdateOfficeAddrDate());
        g11003ReqDto.setHomead(cusGrp.getOfficeAddr());
        g11003ReqDto.setSocino(cusGrp.getSocialCreditCode());
        g11003ReqDto.setBusino(cusGrp.getBusinessCirclesRegiNo());
        g11003ReqDto.setOfaddr(cusGrp.getOfficeAddrAdminDiv());
        g11003ReqDto.setCrpdeg(cusGrp.getGrpCloselyDegree());
        g11003ReqDto.setCrpcno(cusGrp.getGrpCaseMemo());
        g11003ReqDto.setCtigno(cusGrp.getManagerId());
        g11003ReqDto.setCitorg(cusGrp.getManagerBrId());
        List<CircleArrayMem> list = new ArrayList<>();
        //组装报文 结束
        if (!CmisCusConstants.STD_GROUP_CUS_STATUS_03.equals(appType)) {
            //集团维护
            g11003ReqDto.setGrpsta("1");
        } else {
            //集团失效
            g11003ReqDto.setGrpsta("0");
        }
        for (CusGrpMemberApp cusGrpMemberApp : cusGrpMemberAppList) {
            CircleArrayMem circleArrayMem = new CircleArrayMem();
            circleArrayMem.setCustno(cusGrpMemberApp.getCusId());
            circleArrayMem.setCortype(cusGrpMemberApp.getGrpCorreType());
            circleArrayMem.setGrpdesc(cusGrpMemberApp.getGrpCorreDetail());
            //核心成员 或 母公司 STD_ZB_GRP_CORRE_TYPE
            if ("1,3".contains(cusGrpMemberApp.getGrpCorreType())) {
                circleArrayMem.setIscust("1");
            } else {
                circleArrayMem.setIscust("0");
            }
            circleArrayMem.setMainno(cusGrpMemberApp.getManagerId());
            circleArrayMem.setMaiorg(cusGrpMemberApp.getManagerBrId());
            CusGrpMemberRel cusGrpMemberRel = cusGrpMemberRelService.createCusGrpMemRelByCusMemApp(cusGrpMemberApp, cusGrpApp);
            circleArrayMem.setSocino(cusGrpMemberRel.getSociCredCode());
            //成员状态 有效，无效
            circleArrayMem.setCustst(CmisCusConstants.STD_AVAILABLE_IND_1.equals(cusGrpMemberRel.getAvailableInd()) ? "0" : "1");
            list.add(circleArrayMem);
        }

        g11003ReqDto.setOprtye(cusGrp.getOprType());
        g11003ReqDto.setInptno(cusGrp.getInputId());
        g11003ReqDto.setInporg(cusGrp.getInputBrId());
        g11003ReqDto.setCircleArrayMem(list);
        //调用接口
        ResultDto<G11003RespDto> resultDto = dscms2EcifClientService.g11003(g11003ReqDto);
        //判断接口返回值
        if (SuccessEnum.CMIS_SUCCSESS.key.equals(resultDto.getCode())) {
            return resultDto.getData().getGrouno();
        } else {
            throw new PlatformException(resultDto.getMessage());
        }
    }

    /**
     * @方法名称: commit
     * @方法描述: 集团客户提交引导
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int commit(CusGrpApp record) {
        //调用Ecif 客户集团信息查询,获取集团客户编号
        /**
         * 识别方式:
         * 1- 按集团编号查询
         * 2- 按客户编号查询
         */
        /***********************************  调用接口开始  ***************************************************************/
        G11004ReqDto g11004ReqDto = new G11004ReqDto();
        g11004ReqDto.setResotp("2");
        g11004ReqDto.setCustno(record.getCoreCusId());
        ResultDto<G11004RespDto> resultDto = dscms2EcifClientService.g11004(g11004ReqDto);
        String g11004Code = Optional.ofNullable(resultDto.getCode()).orElse(StringUtils.EMPTY);
        String g11004Meesage = Optional.ofNullable(resultDto.getMessage()).orElse(StringUtils.EMPTY);
        G11004RespDto g11004RespDto = null;
        // TODO 获取相关的值并解析
        // 获取查询结果 查询成功失败的情况
        g11004RespDto = resultDto.getData();
        if (!ObjectUtils.isEmpty(g11004RespDto) && !StringUtils.isEmpty(g11004RespDto.getCrpcno())) {
            record.setCoreCusId(g11004RespDto.getCustno());
        } else {
            //调用Ecif 创建集团客户接口，获取集团客户编号
            G11003ReqDto g11003ReqDto = new G11003ReqDto();
            /**
             * 集团成员状态
             * 0：无效
             * 1：有效
             */
            g11003ReqDto.setGrpsta("0");	//集团客户状态
            g11003ReqDto.setGrouna(record.getGrpName());	//集团客户名称
            g11003ReqDto.setCustno(record.getCoreCusId());	//客户编号
            g11003ReqDto.setCustln(record.getCoreCusLoanCardId());	//客户中证码
            g11003ReqDto.setCustna(record.getCoreCusName());	//客户名称
            g11003ReqDto.setIdtfno(record.getCoreCusCertNo());	//证件号码
            g11003ReqDto.setOadate(DateUtils.formatDate10To8(record.getUpdateOfficeAddrDate()));	//更新办公地址日期
            g11003ReqDto.setHomead(record.getOfficeAddr());	//地址
            g11003ReqDto.setSocino(record.getSociCredCode());	//社会信用代码
            g11003ReqDto.setBusino(record.getBusinessCirclesRegiNo());	//工商登记注册号
            g11003ReqDto.setOfaddr(record.getOfficeAddrAdminDiv());	//办公地址行政区划
            g11003ReqDto.setCrpdeg(record.getGrpCloselyDegree());	//集团紧密程度
            g11003ReqDto.setCrpcno(record.getGrpCaseMemo());	//集团客户情况说明
            g11003ReqDto.setCtigno(record.getManagerId());	//管户客户经理
            g11003ReqDto.setCitorg(record.getManagerBrId());	//所属机构
            g11003ReqDto.setOprtye(record.getOprType());	//操作类型
            g11003ReqDto.setInptno(record.getInputId());	//登记人
            g11003ReqDto.setInporg(record.getInputBrId());	//登记机构

            CircleArrayMem circleArrayMem = new CircleArrayMem();
            circleArrayMem.setCustno(record.getCoreCusId());	//集团成员客户号
            circleArrayMem.setCortype(StringUtils.EMPTY);	//集团关系类型
            circleArrayMem.setGrpdesc(StringUtils.EMPTY);	//集团关系描述
            circleArrayMem.setIscust("1");	//是否集团主客户
            circleArrayMem.setMainno(record.getManagerId());	//主管人
            circleArrayMem.setMaiorg(record.getManagerBrId());	//主管机构
            circleArrayMem.setSocino(record.getSociCredCode());	//统一社会信用代码
            circleArrayMem.setCustst("0");	//成员状态
            List<CircleArrayMem> circleArrayMemList = new ArrayList<>();
            circleArrayMemList.add(circleArrayMem);
            g11003ReqDto.setCircleArrayMem(circleArrayMemList);

            ResultDto<G11003RespDto> g11003ResultDto = dscms2EcifClientService.g11003(g11003ReqDto);
            String g11003Code = Optional.ofNullable(g11003ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String g11003Meesage = Optional.ofNullable(g11003ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            G11003RespDto g11003RespDto = null;
            //  获取相关的值并解析
            g11003RespDto = g11003ResultDto.getData();
            if (!ObjectUtils.isEmpty(g11003RespDto) && !StringUtils.isEmpty(g11003RespDto.getGrouno())) {
                record.setGrpNo(g11003RespDto.getGrouno());
            } else {
                //  抛出错误异常
                throw BizException.error(null,g11003Code,g11003Meesage);
            }

            //数据更新到集团申请子表中
            int result = cusGrpAppMapper.updateByPrimaryKeySelective(record);
            if (result>0){
            } else {
                throw BizException.error(null,EcsEnum.E_SAVE_FAIL.key,"\"保存集团申请表失败\"" + EcsEnum.E_SAVE_FAIL.value);
            }
        }
        return 0;
    }

    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterEnd(String serno, String bizType, ResultInstanceDto resultInstanceDto) {
        CusGrpApp domain = cusGrpAppMapper.selectByPrimaryKey(serno);
        CmisLmt0040ReqDto cmisLmt0040ReqDto = new CmisLmt0040ReqDto();
        cmisLmt0040ReqDto.setGrpNo(domain.getGrpNo());
        cmisLmt0040ReqDto.setGrpName(domain.getGrpName());
        cmisLmt0040ReqDto.setSerno(serno);
        cmisLmt0040ReqDto.setManagerId(domain.getManagerId());
        cmisLmt0040ReqDto.setManagerBrId(domain.getManagerBrId());

        //集团成员列表
        List<CmisLmt0040MemberListReqDto> memberList = new ArrayList<>();

        if("KH008".equals(bizType)){ // 集团客户认定
            log.info("集团客户认定，流水号【"+serno+"】");
            // 主表数据
            CusGrp cusGrp = new CusGrp();
            cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(domain, cusGrp);
            cusGrp.setSocialCreditCode(domain.getSociCredCode());
            cusGrp.setGroupCusStatus(CmisCusConstants.GROUP_CUS_STATUS_01); // 01 生效 02 失效
            cusGrpService.insert(cusGrp);

            //集团申请类型 01--集团客户认定
            cmisLmt0040ReqDto.setGrpAppType(CmisCusConstants.STD_GRP_APP_TYPE_01);
            // 子表数据
            QueryModel model = new QueryModel();
            model.addCondition("serno", serno);
            List<CusGrpMemberApp>  cusGrpMemberAppList = cusGrpMemberAppService.selectAll(model);

            if(!CollectionUtils.isEmpty(cusGrpMemberAppList)){
                log.info("集团客户认定，遍历集团成员列表开始");

                for (CusGrpMemberApp cusGrpMemberApp: cusGrpMemberAppList) {
                    CusGrpMemberRel cusGrpMemberRel = new CusGrpMemberRel();
                    cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(cusGrpMemberApp, cusGrpMemberRel);
                    cusGrpMemberRel.setGrpName(domain.getGrpName());
                    cusGrpMemberRel.setAvailableInd(CmisCusConstants.STD_AVAILABLE_IND_1);
                    cusGrpMemberRelService.insert(cusGrpMemberRel);

                    //集团成员
                    CmisLmt0040MemberListReqDto cmisLmt0040MemberListReqDto = new CmisLmt0040MemberListReqDto();
                    cmisLmt0040MemberListReqDto.setGrpMemberNo(cusGrpMemberApp.getCusId());
                    cmisLmt0040MemberListReqDto.setManagerId(cusGrpMemberApp.getManagerId());
                    cmisLmt0040MemberListReqDto.setManagerBrId(cusGrpMemberApp.getManagerBrId());
                    cmisLmt0040MemberListReqDto.setCusCatalog(cusGrpMemberApp.getCusCatalog());
                    memberList.add(cmisLmt0040MemberListReqDto);
                }

                log.info("集团客户认定，遍历集团成员列表结束");
            }

        }else if("KH009".equals(bizType)){ // 集团客户变更
            log.info("集团客户变更，流水号【"+serno+"】");
            // 1、主表更新
            CusGrp cusGrp = new CusGrp();
            cusGrp.setGrpNo(domain.getGrpNo());
            cusGrp.setUpdateOfficeAddrDate(domain.getUpdateOfficeAddrDate()); //UPDATE_OFFICE_ADDR_DATE 更新办公地址日期
            cusGrp.setSocialCreditCode(domain.getSociCredCode());//SOCIAL_CREDIT_CODE 社会信用代码
            cusGrp.setGrpCloselyDegree(domain.getGrpCloselyDegree());//GRP_CLOSELY_DEGREE 集团紧密程度
            cusGrp.setGrpCaseMemo(domain.getGrpCaseMemo());//GRP_CASE_MEMO 集团客户情况说明
            cusGrp.setGroupCusStatus(CmisCusConstants.GROUP_CUS_STATUS_01); // 01 生效 02 失效
            cusGrpService.updateSelective(cusGrp);
            // 2、子表更新
            QueryModel model = new QueryModel();
            model.addCondition("serno", serno);
            List<CusGrpMemberApp>  cusGrpMemberAppList = cusGrpMemberAppService.selectAll(model);

            if(!CollectionUtils.isEmpty(cusGrpMemberAppList)){
                log.info("集团客户变更，遍历集团成员列表开始");
                //集团申请类型为集团客户变更
                cmisLmt0040ReqDto.setGrpAppType(CmisLmtConstants.STD_GRP_APP_TYPE_02);

                for (CusGrpMemberApp cusGrpMemberApp: cusGrpMemberAppList) {
                    if("02".equals(cusGrpMemberApp.getModifyType())){// 02 本次新增
                        CusGrpMemberRel cusGrpMemberRel = new CusGrpMemberRel();
                        BeanUtils.beanCopy(cusGrpMemberApp, cusGrpMemberRel);
                        cusGrpMemberRel.setAvailableInd(CmisCusConstants.STD_AVAILABLE_IND_1); // 设置生成状态 1 是 0 否
                        cusGrpMemberRelService.insert(cusGrpMemberRel);

                        //集团成员
                        CmisLmt0040MemberListReqDto cmisLmt0040MemberListReqDto = new CmisLmt0040MemberListReqDto();
                        cmisLmt0040MemberListReqDto.setGrpMemberNo(cusGrpMemberApp.getCusId());
                        cmisLmt0040MemberListReqDto.setManagerId(cusGrpMemberApp.getManagerId());
                        cmisLmt0040MemberListReqDto.setManagerBrId(cusGrpMemberApp.getManagerBrId());
                        cmisLmt0040MemberListReqDto.setCusCatalog(cusGrpMemberApp.getCusCatalog());
                        //本次变更类型 02--本次新增
                        cmisLmt0040MemberListReqDto.setCusModifyType(CmisCusConstants.STD_CUS_MODIFY_TYPE_02);
                        memberList.add(cmisLmt0040MemberListReqDto);
                    }else if("03".equals(cusGrpMemberApp.getModifyType())){ // 03 本次退出
                        CusGrpMemberRel cusGrpMemberRel = new CusGrpMemberRel();
                        cusGrpMemberRel.setGrpNo(cusGrpMemberApp.getGrpNo());
                        cusGrpMemberRel.setCusId(cusGrpMemberApp.getCusId());
                        QueryModel queryModel = new QueryModel();
                        queryModel.addCondition("grpNo", cusGrpMemberApp.getGrpNo());
                        queryModel.addCondition("cusId", cusGrpMemberApp.getCusId());

                        List<CusGrpMemberRel> list = cusGrpMemberRelService.selectAll(queryModel);

                        if(CollectionUtils.nonEmpty(list)){
                            for (CusGrpMemberRel tempcusGrpMemberRel: list) {
                                tempcusGrpMemberRel.setAvailableInd(CmisCusConstants.STD_AVAILABLE_IND_0);
                                cusGrpMemberRelService.updateSelective(tempcusGrpMemberRel);
                            }
                        }
//                                cusGrpMemberRelService.deleteByGrpnoAndCusid(cusGrpMemberRel);

                        //组装报文
                        CmisLmt0040MemberListReqDto cmisLmt0040MemberListReqDto = new CmisLmt0040MemberListReqDto();
                        cmisLmt0040MemberListReqDto.setGrpMemberNo(cusGrpMemberApp.getCusId());
                        cmisLmt0040MemberListReqDto.setManagerId(cusGrpMemberApp.getManagerId());
                        cmisLmt0040MemberListReqDto.setManagerBrId(cusGrpMemberApp.getManagerBrId());
                        //本次变更类型 03--本次退出
                        cmisLmt0040MemberListReqDto.setCusModifyType(CmisCusConstants.STD_CUS_MODIFY_TYPE_03);
                        //是否保留额度默认为否
                        String isRetainLmt = CmisLmtConstants.YES_NO_N;
                        if(ObjectUtils.isEmpty(cusGrpMemberApp.getReserveGrpLimitInd()) || CmisLmtConstants.YES_NO_N.equals(cusGrpMemberApp.getReserveGrpLimitInd())){
                                isRetainLmt = CmisLmtConstants.YES_NO_N;
                        }else if (CmisLmtConstants.YES_NO_Y.equals(cusGrpMemberApp.getReserveGrpLimitInd())){
                            isRetainLmt = CmisLmtConstants.YES_NO_Y;
                        }
                        cmisLmt0040MemberListReqDto.setIsRetainLmt(isRetainLmt);
                        memberList.add(cmisLmt0040MemberListReqDto);
                    }else{
                        log.error("集团客户认定、变更、解散审批流程【{}】，流程结束操作，流程参数【{}】,未知的变更类型【{}】", serno, resultInstanceDto, cusGrpMemberApp.getModifyType());
                    }
                }

                log.info("集团客户变更，遍历集团成员列表结束");
            }

        }else if("KH010".equals(bizType)){ // 集团客户解散
            log.info("集团客户解散，流水号【"+serno+"】");
            CusGrp cusGrp = new CusGrp();
            cusGrp.setGrpNo(domain.getGrpNo());
            cusGrp.setGroupCusStatus(CmisCusConstants.GROUP_CUS_STATUS_02); // 失效
            cusGrpService.updateSelective(cusGrp);
            //集团成员表置为失效
            QueryModel model = new QueryModel();
            model.addCondition("grpNo", domain.getGrpNo());
            List<CusGrpMemberRel>  cusGrpMemberRelList = cusGrpMemberRelService.selectAll(model);
            if(!CollectionUtils.isEmpty(cusGrpMemberRelList)) {
                log.info("集团客户解散，遍历集团成员列表开始");
                for (CusGrpMemberRel cusGrpMemberRel: cusGrpMemberRelList) {
                    cusGrpMemberRel.setAvailableInd(CmisCusConstants.STD_AVAILABLE_IND_0);// 设置是否有效 1 是 0 否
                    cusGrpMemberRelService.updateSelective(cusGrpMemberRel);
                }
            }

            //集团申请类型为03--集团客户解散
            cmisLmt0040ReqDto.setGrpAppType(CmisLmtConstants.STD_GRP_APP_TYPE_03);
        }
        // 同步集团客户信息至ECIF

        cusGrpService.insertCusGrpMemberAppByCusGrpMemRel(domain);
        // 更新申请状态
        domain.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
        cusGrpAppMapper.updateByPrimaryKey(domain);

        //发送到额度系统
        cmisLmt0040ReqDto.setMemberList(memberList);
        sendCmisLmt0040(cmisLmt0040ReqDto,serno);

        log.info("集团客户认定、变更、解散审批流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
    }

    /**
     * 集团客户变更、解散后通知额度系统
     * @param reqDto
     * @param serno
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    public void sendCmisLmt0040(CmisLmt0040ReqDto reqDto,String serno) {
        // 调用额度接口
        log.info("根据集团客户变更、解散申请编号【" + serno + "】前往额度系统-建立额度请求报文：" + reqDto.toString());
        ResultDto<CmisLmt0040RespDto> respDto = cmisLmtClientService.cmislmt0040(reqDto);
        log.info("根据集团客户变更、解散申请编号【" + serno + "】前往额度系统-建立额度返回报文：" + respDto.toString());

        if (SuccessEnum.SUCCESS.key.equals(respDto.getData().getErrorCode())) {
            log.info("根据集团客户变更、解散申请编号【{}】,前往额度系统建立额度成功！", serno);
        } else {
            log.info("根据集团客户变更、解散申请编号【{}】,前往额度系统建立额度失败！", serno);
            throw new BizException(null,"",null,"额度系统-前往额度系统建立额度失败:" + respDto.getData().getErrorMsg());
        }
    }
}