package cn.com.yusys.yusp.web.server.cmiscus0019;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0019.req.CmisCus0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.resp.CmisCus0019RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusLstYndAppService;
import cn.com.yusys.yusp.web.server.cmiscus0007.CmisCus0007Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 查询优农贷名单信息
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "CmisCus0019:查询优农贷名单信息")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0007Resource.class);

    @Autowired
    private CusLstYndAppService cusLstYndAppService;
    /**
     * 交易码：cmiscus0019
     * 交易描述：查询优农贷名单信息
     *
     * @param cmisCus0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优农贷名单信息")
    @PostMapping("/cmiscus0019")
    protected @ResponseBody
    ResultDto<CmisCus0019RespDto> cmiscus0019(@Validated @RequestBody CmisCus0019ReqDto cmisCus0019ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0019.key, DscmsEnum.TRADE_CODE_CMISCUS0019.value, JSON.toJSONString(cmisCus0019ReqDto));
        CmisCus0019RespDto cmisCus0019RespDto = new CmisCus0019RespDto();// 响应Dto:查询优农贷名单信息
        ResultDto<CmisCus0019RespDto> cmisCus0019ResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0019.key, DscmsEnum.TRADE_CODE_CMISCUS0019.value, JSON.toJSONString(cmisCus0019ReqDto));
            int count = cusLstYndAppService.selectYndCountByCertCode(cmisCus0019ReqDto);
            cmisCus0019RespDto.setResult(count);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0019.key, DscmsEnum.TRADE_CODE_CMISCUS0019.value, JSON.toJSONString(cmisCus0019RespDto));
            // 封装cmisCus0019ResultDto中正确的返回码和返回信息
            cmisCus0019ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0019ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, e.getMessage());
            // 封装cmisCus0019ResultDto中异常返回码和返回信息
            cmisCus0019ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0019ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0019RespDto到cmisCus0007ResultDto中
        cmisCus0019ResultDto.setData(cmisCus0019RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0019ResultDto));
        return cmisCus0019ResultDto;
    }
}
