package cn.com.yusys.yusp.service.server.xdkh0028;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0028.req.Xdkh0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0028.resp.List;
import cn.com.yusys.yusp.dto.server.xdkh0028.resp.Xdkh0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstYndMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Service
public class Xdkh0028Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdkh0028.Xdkh0028Service.class);

    @Resource
    private CusLstYndMapper cusLstYndMapper;
    /**
     * 优农贷黑名单查询
     * @param xdkh0028DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0028DataRespDto getIndivCusBaseInfo(Xdkh0028DataReqDto xdkh0028DataReqDto)throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, JSON.toJSONString(xdkh0028DataReqDto));
        Xdkh0028DataRespDto xdkh0028DataRespDto = new Xdkh0028DataRespDto();
        // 查询条件 证件号(必输) 其余非必输
        try {
            if(StringUtils.isEmpty(xdkh0028DataReqDto.getCert_code())){
                throw new YuspException(EcsEnum.ECS040004.key,EcsEnum.ECS040004.value());
            }else {
                java.util.List<List> list = cusLstYndMapper.selectBlacklist(xdkh0028DataReqDto);
                xdkh0028DataRespDto.setList(list);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0028DataRespDto));
        return xdkh0028DataRespDto;
    }
}