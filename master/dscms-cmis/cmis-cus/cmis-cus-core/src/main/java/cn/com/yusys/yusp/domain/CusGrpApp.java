/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpApp
 * @类描述: cus_grp_app数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-08 21:45:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_grp_app")
public class CusGrpApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 申请类型 **/
	@Column(name = "APP_TYPE", unique = false, nullable = true, length = 5)
	private String appType;
	
	/** 集团编号 **/
	@Column(name = "GRP_NO", unique = false, nullable = false, length = 30)
	private String grpNo;
	
	/** 集团名称 **/
	@Column(name = "GRP_NAME", unique = false, nullable = true, length = 80)
	private String grpName;
	
	/** 核心客户编号 **/
	@Column(name = "CORE_CUS_ID", unique = false, nullable = true, length = 30)
	private String coreCusId;
	
	/** 核心客户中征码 **/
	@Column(name = "CORE_CUS_LOAN_CARD_ID", unique = false, nullable = true, length = 20)
	private String coreCusLoanCardId;
	
	/** 核心客户名称 **/
	@Column(name = "CORE_CUS_NAME", unique = false, nullable = true, length = 80)
	private String coreCusName;
	
	/** 核心客户证件号码 **/
	@Column(name = "CORE_CUS_CERT_NO", unique = false, nullable = true, length = 40)
	private String coreCusCertNo;
	
	/** 统一社会信用代码 **/
	@Column(name = "SOCI_CRED_CODE", unique = false, nullable = true, length = 18)
	private String sociCredCode;
	
	/** 工商登记注册号 **/
	@Column(name = "BUSINESS_CIRCLES_REGI_NO", unique = false, nullable = true, length = 10)
	private String businessCirclesRegiNo;
	
	/** 办公地址 **/
	@Column(name = "OFFICE_ADDR", unique = false, nullable = true, length = 300)
	private String officeAddr;
	
	/** 办公地址行政区划 **/
	@Column(name = "OFFICE_ADDR_ADMIN_DIV", unique = false, nullable = true, length = 255)
	private String officeAddrAdminDiv;
	
	/** 更新办公地址日期 **/
	@Column(name = "UPDATE_OFFICE_ADDR_DATE", unique = false, nullable = true, length = 10)
	private String updateOfficeAddrDate;
	
	/** 主申请关联(集团)客户号 **/
	@Column(name = "PARENT_CUS_ID", unique = false, nullable = true, length = 30)
	private String parentCusId;
	
	/** 集团客户情况说明 **/
	@Column(name = "GRP_CASE_MEMO", unique = false, nullable = true, length = 250)
	private String grpCaseMemo;
	
	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 10)
	private String appDate;
	
	/** 申请原因 **/
	@Column(name = "APP_RESN", unique = false, nullable = true, length = 400)
	private String appResn;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 集团紧密程度 **/
	@Column(name = "GRP_CLOSELY_DEGREE", unique = false, nullable = true, length = 5)
	private String grpCloselyDegree;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;

	/** 集团融资类型 **/
	@Column(name = "GRP_FINANCE_TYPE", unique = false, nullable = true, length = 5)
	private String grpFinanceType;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 集团性质*/
	@Column(name = "GRP_NATURE", unique = false, nullable = true, length = 2)
	private String grpNature;

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType;
	}

    /**
     * @return appType
     */
	public String getAppType() {
		return this.appType;
	}

	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo;
	}

    /**
     * @return grpNo
     */
	public String getGrpNo() {
		return this.grpNo;
	}

	/**
	 * @param grpName
	 */
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}

    /**
     * @return grpName
     */
	public String getGrpName() {
		return this.grpName;
	}

	/**
	 * @param coreCusId
	 */
	public void setCoreCusId(String coreCusId) {
		this.coreCusId = coreCusId;
	}

    /**
     * @return coreCusId
     */
	public String getCoreCusId() {
		return this.coreCusId;
	}

	/**
	 * @param coreCusLoanCardId
	 */
	public void setCoreCusLoanCardId(String coreCusLoanCardId) {
		this.coreCusLoanCardId = coreCusLoanCardId;
	}

    /**
     * @return coreCusLoanCardId
     */
	public String getCoreCusLoanCardId() {
		return this.coreCusLoanCardId;
	}

	/**
	 * @param coreCusName
	 */
	public void setCoreCusName(String coreCusName) {
		this.coreCusName = coreCusName;
	}

    /**
     * @return coreCusName
     */
	public String getCoreCusName() {
		return this.coreCusName;
	}

	/**
	 * @param coreCusCertNo
	 */
	public void setCoreCusCertNo(String coreCusCertNo) {
		this.coreCusCertNo = coreCusCertNo;
	}

    /**
     * @return coreCusCertNo
     */
	public String getCoreCusCertNo() {
		return this.coreCusCertNo;
	}

	/**
	 * @param sociCredCode
	 */
	public void setSociCredCode(String sociCredCode) {
		this.sociCredCode = sociCredCode;
	}

    /**
     * @return sociCredCode
     */
	public String getSociCredCode() {
		return this.sociCredCode;
	}

	/**
	 * @param businessCirclesRegiNo
	 */
	public void setBusinessCirclesRegiNo(String businessCirclesRegiNo) {
		this.businessCirclesRegiNo = businessCirclesRegiNo;
	}

    /**
     * @return businessCirclesRegiNo
     */
	public String getBusinessCirclesRegiNo() {
		return this.businessCirclesRegiNo;
	}

	/**
	 * @param officeAddr
	 */
	public void setOfficeAddr(String officeAddr) {
		this.officeAddr = officeAddr;
	}

    /**
     * @return officeAddr
     */
	public String getOfficeAddr() {
		return this.officeAddr;
	}

	/**
	 * @param officeAddrAdminDiv
	 */
	public void setOfficeAddrAdminDiv(String officeAddrAdminDiv) {
		this.officeAddrAdminDiv = officeAddrAdminDiv;
	}

    /**
     * @return officeAddrAdminDiv
     */
	public String getOfficeAddrAdminDiv() {
		return this.officeAddrAdminDiv;
	}

	/**
	 * @param updateOfficeAddrDate
	 */
	public void setUpdateOfficeAddrDate(String updateOfficeAddrDate) {
		this.updateOfficeAddrDate = updateOfficeAddrDate;
	}

    /**
     * @return updateOfficeAddrDate
     */
	public String getUpdateOfficeAddrDate() {
		return this.updateOfficeAddrDate;
	}

	/**
	 * @param parentCusId
	 */
	public void setParentCusId(String parentCusId) {
		this.parentCusId = parentCusId;
	}

    /**
     * @return parentCusId
     */
	public String getParentCusId() {
		return this.parentCusId;
	}

	/**
	 * @param grpCaseMemo
	 */
	public void setGrpCaseMemo(String grpCaseMemo) {
		this.grpCaseMemo = grpCaseMemo;
	}

    /**
     * @return grpCaseMemo
     */
	public String getGrpCaseMemo() {
		return this.grpCaseMemo;
	}

	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}

    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}

	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn;
	}

    /**
     * @return appResn
     */
	public String getAppResn() {
		return this.appResn;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param grpCloselyDegree
	 */
	public void setGrpCloselyDegree(String grpCloselyDegree) {
		this.grpCloselyDegree = grpCloselyDegree;
	}

    /**
     * @return grpCloselyDegree
     */
	public String getGrpCloselyDegree() {
		return this.grpCloselyDegree;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param grpFinanceType
	 */
	public void setGrpFinanceType(String grpFinanceType) {
		this.grpFinanceType = grpFinanceType;
	}
	
    /**
     * @return grpFinanceType
     */
	public String getGrpFinanceType() {
		return this.grpFinanceType;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	public String getGrpNature() {
		return grpNature;
	}

	public void setGrpNature(String grpNature) {
		this.grpNature = grpNature;
	}
}