package cn.com.yusys.yusp.service.server.xdkh0013;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0013.resp.Xdkh0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstYxdJbxxAppMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:优享贷客户白名单信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdkh0013Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdkh0013Service.class);

    @Autowired
    private CusLstYxdJbxxAppMapper cusLstYxdJbxxAppMapper;

    /**
     * 优享贷客户白名单信息查询
     * @param certNo
     * @return
     */
    @Transactional
    public Xdkh0013DataRespDto getWhiteListByCertNo(String certNo) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, certNo);
        Xdkh0013DataRespDto result = cusLstYxdJbxxAppMapper.getWhiteListByCertNo(certNo);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(result));
        return result;
    }

}
