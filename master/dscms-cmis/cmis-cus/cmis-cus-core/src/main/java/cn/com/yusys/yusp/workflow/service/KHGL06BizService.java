package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005Req;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0005.Cmisbatch0005RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusGrp;
import cn.com.yusys.yusp.domain.CusPubHandoverApp;
import cn.com.yusys.yusp.domain.CusPubHandoverLst;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdht0022.req.Xdht0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0022.resp.Xdht0022DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0042.req.Xdht0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0042.resp.Xdht0042DataRespDto;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.repository.mapper.CusGrpMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 客户移交审批流程
 *
 * @author liucheng
 * @version 1.0.0
 */
@Service
public class KHGL06BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(KHGL06BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private CusPubHandoverAppService service;

    @Autowired
    private CusPubHandoverLstService cusPubHandoverLstService;

    @Autowired
    private CusBaseService cusBaseService;

    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;
    @Autowired
    private CusGrpService cusGrpService;
    @Autowired
    private CusGrpMapper cusGrpMapper;
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
//        String bizType = resultInstanceDto.getBizType();
        log.info("后业务处理类型" + currentOpType);
        try {
            CusPubHandoverApp domain = service.selectByPrimaryKey(serno);
            QueryModel cusPubListModel = new QueryModel();
            cusPubListModel.addCondition("serno",serno);
            List<CusPubHandoverLst> cusPubHandoverLst = cusPubHandoverLstService.selectByModel(cusPubListModel);
            if (OpType.STRAT.equals(currentOpType)) {
                // domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                // service.update(domain);
                log.info("客户移交审批流程【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("客户移交审批流程【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("客户移交审批流程【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                //客户移交必输项校验
                if(domain.getHandoverMode() == null || "".equals(domain.getHandoverMode())){
                    throw new Exception("客户移交审批流程后业务处理,必输项【移交方式】检查不通过:"+serno);
                }
                if(domain.getReceiverId() == null || "".equals(domain.getReceiverId())){
                    throw new Exception("客户移交审批流程后业务处理,必输项检查不通过:"+serno);
                }
                if(domain.getReceiverBrId() == null || "".equals(domain.getReceiverBrId())){
                    throw new Exception("客户移交审批流程后业务处理,必输项检查不通过:"+serno);
                }
                // 1、更新申请表状态
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                service.update(domain);
                // 2、调用批量作业更新业务数据
                Cmisbatch0005ReqDto reqDto = new Cmisbatch0005ReqDto();
                List<Cmisbatch0005Req> cmisbatch0005ReqList = new ArrayList<>();

                //查询本次客户移交申请对应的客户列表数据
                QueryModel queryModel = new QueryModel();
                queryModel.getCondition().put(CmisBizConstants.KHGL06_SERNO, domain.getSerno());
                // queryModel.getCondition().put(CmisBizConstants.KHGL06_OPR_TYPE, CmisBizConstants.KHGL06_OPR_TYPE_VALUE);
                List<CusPubHandoverLst> list = cusPubHandoverLstService.selectAll(queryModel);
                if(CollectionUtils.nonEmpty(list)){
                    list.stream().forEach(lst -> {
                        Cmisbatch0005Req cmisbatch0005Req = new Cmisbatch0005Req();
                        cmisbatch0005Req.setHandoverType(domain.getHandoverMode());
                        cmisbatch0005Req.setManagerId(domain.getReceiverId());
                        cmisbatch0005Req.setManagerBrId(domain.getReceiverBrId());
                        if (CmisBizConstants.KHGL06_HANDOVER_MODE.equals(domain.getHandoverMode())) {//客户与业务
                            if(lst.getCusId() == null || "".equals(lst.getCusId())){
                                throw new RuntimeException("客户移交审批流程后业务处理,必输项检查不通过:"+serno);
                            }
                            cmisbatch0005Req.setConditionValue(lst.getCusId());
                        }else if("2".equals(domain.getHandoverMode())){//仅业务
                            if(lst.getContNo() == null || "".equals(lst.getContNo())){
                                throw new RuntimeException("客户移交审批流程后业务处理,必输项检查不通过:"+serno);
                            }
                            cmisbatch0005Req.setConditionValue(lst.getContNo());
                        }else{//仅客户
                            if(lst.getCusId() == null || "".equals(lst.getCusId())){
                                throw new RuntimeException("客户移交审批流程后业务处理,必输项检查不通过:"+serno);
                            }
                            CusBase cusBase = new CusBase();
                            cusBase.setCusId(lst.getCusId());
                            cusBase.setManagerId(domain.getReceiverId());
                            cusBase.setManagerBrId(domain.getReceiverBrId());
                            cusBaseService.updateSelective(cusBase);
                        }
                        cmisbatch0005ReqList.add(cmisbatch0005Req);
                    });
                }
                if(!"3".equals(domain.getHandoverMode())) {//1-客户与业务2-仅业务3-仅客户
                    reqDto.setCmisbatch0005ReqList(cmisbatch0005ReqList);
                    ResultDto<Cmisbatch0005RespDto> result = cmisBatchClientService.cmisbatch0005(reqDto);
                    if(!"0".equals(result.getCode())){
                        log.error("客户移交审批流程后业务处理失败，调用批量服务失败", result.getMessage());
                        throw new Exception("客户移交审批流程后业务处理失败，调用批量服务失败！");
                    }
                }

                //20211031-00029集团客户移交场景：如移交的客户为集团客户核心客户且移交类型为1-客户与业务的，则移交集团客户的管户与业务信息
                List<Cmisbatch0005Req> jtList = new ArrayList<>();
                if(CollectionUtils.nonEmpty(list)){
                    list.stream().forEach(lst -> {
                        Cmisbatch0005Req cmisbatch0005Req = new Cmisbatch0005Req();
                        cmisbatch0005Req.setHandoverType("JT");
                        cmisbatch0005Req.setManagerId(domain.getReceiverId());
                        cmisbatch0005Req.setManagerBrId(domain.getReceiverBrId());
                        if (!"3".equals(domain.getHandoverMode())) {//客户与业务
                            //查看该核心客户是否已经存在集团核心客户中
                            QueryModel queryModelGt = new QueryModel();
                            queryModelGt.addCondition("coreCusId", lst.getCusId());
                            //查询该核心客户是否存在有效状态的集团中
                            List<CusGrp> cusGrpList = cusGrpMapper.selectByModel(queryModelGt);
                            if (cusGrpList!=null && cusGrpList.size()>0) {
                                cmisbatch0005Req.setConditionValue(cusGrpList.get(0).getGrpNo());
                                jtList.add(cmisbatch0005Req);
                            }
                        }
                    });
                }

                if(jtList!=null && jtList.size()>0) {//1-客户与业务2-仅业务3-仅客户
                    reqDto.setCmisbatch0005ReqList(jtList);
                    ResultDto<Cmisbatch0005RespDto> result = cmisBatchClientService.cmisbatch0005(reqDto);
                    if(!"0".equals(result.getCode())){
                        log.error("集团客户移交审批流程后业务处理失败，调用批量服务失败", result.getMessage());
                        throw new Exception("集团客户移交审批流程后业务处理失败，调用批量服务失败！");
                    }
                }

                MessageSendDto messageSendDto = new MessageSendDto();
                ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(domain.getHandoverId());
                Map<String, String> map = new HashMap<>();
                map.put("handoverIdName", byLoginCode.getData().getUserName());
                List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                receivedUserDto.setReceivedUserType("1");// 发送人员类型
                receivedUserDto.setUserId(domain.getReceiverId());// 客户经理工号
                receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                receivedUserList.add(receivedUserDto);
                messageSendDto.setMessageType("MSG_KH_M_0002");
                messageSendDto.setParams(map);
                messageSendDto.setReceivedUserList(receivedUserList);
                messageSendService.sendMessage(messageSendDto);
                if("2".equals(domain.getHandoverMode())){
                    for (CusPubHandoverLst cphl:
                    cusPubHandoverLst) {
                        if(cphl.getPrdId().equals("SC010008")){
                            MessageSendDto messageSendDto2 = new MessageSendDto();
                            Map<String, String> sendmap = new HashMap<>();
                            sendmap.put("cusName", cphl.getCusName());
                            sendmap.put("prdName", "优企贷");
                            List<ReceivedUserDto> receivedUserList2 = new ArrayList<>();
                            ReceivedUserDto receivedUserDto2 = new ReceivedUserDto();
                            receivedUserDto2.setReceivedUserType("1");// 发送人员类型
                            receivedUserDto2.setUserId(domain.getReceiverId());// 客户经理工号
                            receivedUserDto2.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                            receivedUserList2.add(receivedUserDto2);
                            messageSendDto2.setMessageType("MSG_XW_M_0002");
                            messageSendDto2.setParams(sendmap);
                            messageSendDto2.setReceivedUserList(receivedUserList2);
                            messageSendService.sendMessage(messageSendDto2);
                        }
                    }
                }


                log.info("客户移交审批流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("客户移交审批流程【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 判断是否打回发起人节点
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                    domain.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    service.update(domain);
                }
                log.info("客户移交审批流程【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("客户移交审批流程【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("客户移交审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("客户移交审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                service.update(domain);
            } else {
                log.info("客户移交审批流程【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("客户移交审批流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }


    // 判定流程的业务类型能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto instanceInfo) {
        String flowCode = instanceInfo.getFlowCode();
        return "KHGL06".equals(flowCode);
    }
}

