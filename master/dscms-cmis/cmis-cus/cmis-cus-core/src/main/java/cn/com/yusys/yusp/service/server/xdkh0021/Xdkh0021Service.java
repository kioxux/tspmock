package cn.com.yusys.yusp.service.server.xdkh0021;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.domain.CusIndivContact;
import cn.com.yusys.yusp.domain.CusIndivUnit;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd02.Fbxd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd02.Fbxd02RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd03.Fbxd03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd03.Fbxd03RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0021.req.Xdkh0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0021.resp.Xdkh0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivContactMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivUnitMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * <br>
 * 0.2ZRC:2021/5/19 19:51:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 19:51
 * @since 2021/5/19 19:51
 */
@Service
public class Xdkh0021Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0021Service.class);
    @Resource
    private CusIndivMapper cusIndivMapper;
    @Resource
    private CusBaseMapper cusBaseMapper;
    @Resource
    private CusIndivContactMapper cusIndivContactMapper;
    @Resource
    private CusIndivUnitMapper cusIndivUnitMapper;
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AdminSmUserService adminSmUserService;


    @Transactional(readOnly = false, rollbackFor = {Exception.class, RuntimeException.class})
    public Xdkh0021DataRespDto xdkh0021(Xdkh0021DataReqDto xdkh0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0021.key, DscmsEnum.TRADE_CODE_XDKH0021.value, JSON.toJSONString(xdkh0021DataReqDto));
        Xdkh0021DataRespDto xdkh0021DataRespDto = new Xdkh0021DataRespDto();
        String cus_id = xdkh0021DataReqDto.getCus_id();//用户ID
        String cus_name = xdkh0021DataReqDto.getCus_name();//用户名
        String cert_type = xdkh0021DataReqDto.getCert_type();//证件类型
        String cert_code = xdkh0021DataReqDto.getCert_code();//身份证号码
        String orgid = xdkh0021DataReqDto.getOrgid();//机构号
        String currentuserid = xdkh0021DataReqDto.getCurrentuserid();//当前客户经理
        String agriFlg = xdkh0021DataReqDto.getAgriFlg();//是否农户
        String yxdauthor = xdkh0021DataReqDto.getYxdauthor();//是否优享贷客户
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        boolean pass = true;
        String returnMessage = "系统检测到";
        if (StringUtils.isEmpty(cus_id)) {
            pass = false;
            returnMessage += "客户号为空!";
        } else if (StringUtils.isEmpty(cus_name)) {
            pass = false;
            returnMessage += "客户姓名为空!";
        } else if (StringUtils.isEmpty(cert_type)) {
            pass = false;
            returnMessage += "证件类型为空!";
        } else if (StringUtils.isEmpty(cert_code)) {
            pass = false;
            returnMessage += "证件号码为空!";
        } else if (StringUtils.isEmpty(orgid)) {
            pass = false;
            returnMessage += "机构号为空!";
        } else if (StringUtils.isEmpty(currentuserid)) {
            pass = false;
            returnMessage += "所属客户经理为空!";
        }
        //是否农户
        if ("Y".equals(agriFlg)) {
            agriFlg = "1";
        } else if ("N".equals(agriFlg)) {
            agriFlg = "0";
        }
        //必填项检验通过
        if (pass) {
            try {
                //根据管户经理,查询管户机构
                logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(currentuserid);
                logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    orgid = adminSmUserDto.getOrgId();
                }
                //查看该身份证号码是否已经被开户
                int cusIndivByCertCodeCount = cusBaseMapper.isExistAccount(cert_code);
                if (cusIndivByCertCodeCount > 0) {
                    //查询客户状态
                    String tmpCusId = cusBaseMapper.queryCusStatus(cert_code);
                    //如果客户状态不是正式状态
                    if (StringUtil.isNotEmpty(tmpCusId)) {
                        //如果不是正式客户，那么就更新客户号和客户状态为正式客户
                        QueryModel model = new QueryModel();
                        model.addCondition("cusId", cus_id);
                        model.addCondition("tmpCusId", tmpCusId);
                        model.addCondition("managerId", currentuserid);//当前客户经理
                        int res = cusBaseMapper.updateCusStates(model);
                        logger.info("*****************存量信贷用户已转为正式用户！*******************");
                    } else {//已经是正式客户
                        Fbxd02ReqDto fbxd02ReqDto = new Fbxd02ReqDto();
                        fbxd02ReqDto.setCus_id(cus_id);
                        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD02.key, EsbEnum.TRADE_CODE_FBXD02.value, JSON.toJSONString(fbxd02ReqDto));
                        ResultDto<Fbxd02RespDto> fbxd02RespDtoResultDto = dscms2RircpClientService.fbxd02(fbxd02ReqDto);
                        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD02.key, EsbEnum.TRADE_CODE_FBXD02.value, JSON.toJSONString(fbxd02RespDtoResultDto));
                        String mobileNo = fbxd02RespDtoResultDto.getData().getIfmobl();//手机号
                        QueryModel queryModel = new QueryModel();
                        queryModel.addCondition("mobileNo", mobileNo);
                        queryModel.addCondition("cusId", cus_id);
                        //已经为正式客户更新信贷客户信息手机号码
                        int res = cusBaseMapper.updateMobileNo(queryModel);
                    }
                    //成功返回
                    xdkh0021DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                    xdkh0021DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);

                } else {
                    //这里面还存在一种情况就是身份证号没有被开户，但是在信贷开户时传的别人的客户号，这个当然不可以
                    int cusBaseByCusIdCount = cusBaseMapper.isExistCusID(cus_id);
                    if (cusBaseByCusIdCount > 0) {
                        xdkh0021DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
                        xdkh0021DataRespDto.setOpMsg("抱歉，当前客户号已被占用,请重新确认核心开户客户号！");
                        return xdkh0021DataRespDto;
                    } else {
                        //CusBase表
                        QueryModel insertModel = new QueryModel();
                        insertModel.addCondition("cusId", cus_id);
                        insertModel.addCondition("cusName", cus_name);
                        cert_type = PUBUtilTools.changeCertType(cert_type);
                        insertModel.addCondition("certType", cert_type);
                        insertModel.addCondition("certCode", cert_code);
                        insertModel.addCondition("orgId", orgid);
                        insertModel.addCondition("openDay", openDay);
                        //如果是优享贷新客户，会默认传入营销客户经理管户，此时暂不需要管户客户经理
                        if ("Y".equals(yxdauthor)) {
                            insertModel.addCondition("managerId", "");
                        } else {
                            insertModel.addCondition("managerId", currentuserid);
                        }
                        int res = cusBaseMapper.inertCusBase(insertModel);
                    }

                    //从风控获取客户详细信息
                    Fbxd03ReqDto fbxd03ReqDto = new Fbxd03ReqDto();
                    fbxd03ReqDto.setCus_id(cus_id);
                    logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD03.key, EsbEnum.TRADE_CODE_FBXD03.value, JSON.toJSONString(fbxd03ReqDto));
                    ResultDto<Fbxd03RespDto> fbxd03RespDtoResultDto = dscms2RircpClientService.fbxd03(fbxd03ReqDto);
                    logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD03.key, EsbEnum.TRADE_CODE_FBXD03.value, JSON.toJSONString(fbxd03RespDtoResultDto));
                    Fbxd03RespDto fbxd03RespDto = fbxd03RespDtoResultDto.getData();
                    //风控接口返回字段
                    String topDegree = fbxd03RespDto.getTop_degree();//最高学位
                    String custType = fbxd03RespDto.getCust_type();//客户类型
                    if (StringUtils.isEmpty(custType) || "1".equals(custType)) {
                        custType = "110";//一般自然人
                    }
                    String idDueDate = fbxd03RespDto.getId_due_date();//证件有效期
                    String stdZxEmploymet = fbxd03RespDto.getStd_zx_employmet();//行业类别
                    String indivAnnIncm = fbxd03RespDto.getIndiv_ann_incm();//个人年收入
                    String indivComFldName = fbxd03RespDto.getIndiv_com_name();//单位所属行业名称
                    String indivComAddr = fbxd03RespDto.getIndiv_com_addr();//单位地址
                    String indivComPhn = fbxd03RespDto.getIndiv_com_phn();//单位电话
                    //更新表1 CUS_INDIV
                    QueryModel cusIndivModel = new QueryModel();
                    cusIndivModel.addCondition("sex", fbxd03RespDto.getSex());//性別
                    cusIndivModel.addCondition("birthday", fbxd03RespDto.getBirthday());//出生日期
                    cusIndivModel.addCondition("ifwedd", fbxd03RespDto.getIfwedd());//婚姻状况
                    cusIndivModel.addCondition("ifeduc", fbxd03RespDto.getIfeduc());//学历
                    cusIndivModel.addCondition("agri_flg", agriFlg);//是否农户
                    cusIndivModel.addCondition("cusId", cus_id);
                    cusIndivModel.addCondition("openDay", openDay);
                    cusIndivModel.addCondition("topDegree", topDegree);//最高学位
                    cusIndivModel.addCondition("idDueDate", idDueDate);//证件到期日
                    //判断是否存在cus_id在CUS_INDIV表 不存在inert 否则 update
                    int CusIndivCount = cusBaseMapper.queryIsExistCusIdInCusIndiv(cus_id);
                    if (CusIndivCount == 0) {
                        CusIndiv cusIndiv = new CusIndiv();
                        cusIndiv.setSex(fbxd03RespDto.getSex());
                        cusIndiv.setIndivDtOfBirth(fbxd03RespDto.getBirthday());
                        cusIndiv.setMarStatus(fbxd03RespDto.getIfwedd());
                        cusIndiv.setIndivEdt(fbxd03RespDto.getIfeduc());
                        cusIndiv.setIndivDgr(topDegree);
                        cusIndiv.setCusType(custType);
                        cusIndiv.setAgriFlg(agriFlg);
                        cusIndiv.setCertEndDt(idDueDate);//证件到期日
                        cusIndiv.setCusId(cus_id);
                        cusIndiv.setCusName(cus_name);
                        cusIndiv.setName(cus_name);
                        cusIndiv.setInitLoanDate(openDay);
                        cusIndiv.setIsLongIndiv("1");//是否为长期证件
                        cusIndivMapper.insertSelective(cusIndiv);
                    } else {
                        int res = cusBaseMapper.updateCusIndiv(cusIndivModel);
                    }
                    //更新表2 CUS_INDIV_CONTACT
                    QueryModel cusIndivContactModel = new QueryModel();
                    cusIndivContactModel.addCondition("agri_flg", agriFlg);//是否农户
                    cusIndivContactModel.addCondition("addr", fbxd03RespDto.getAddr());//联系地址
                    cusIndivContactModel.addCondition("sizpcd", fbxd03RespDto.getSizpcd());//现居住地邮政编码
                    cusIndivContactModel.addCondition("ifmobl", fbxd03RespDto.getIfmobl());//微信 没有微信用手机号代替
                    cusIndivContactModel.addCondition("siaddr", fbxd03RespDto.getSiaddr());//现居住地
                    cusIndivContactModel.addCondition("zpcd", fbxd03RespDto.getZpcd());//邮政编码
                    cusIndivContactModel.addCondition("live_status", fbxd03RespDto.getLive_status());//居住状况
                    cusIndivContactModel.addCondition("zpcd", fbxd03RespDto.getZpcd());//现居住地邮政编码
                    cusIndivContactModel.addCondition("ifmobl", fbxd03RespDto.getIfmobl());//手机号
                    cusIndivContactModel.addCondition("cusId", cus_id);
                    //判断是否存在cus_id在CUS_INDIV_CONTACT表 不存在inert 否则 update
                    int count2 = cusBaseMapper.queryIsExistCusIdInCusIndivContact(cus_id);
                    if (count2 == 0) {
                        CusIndivContact cusIndivContact = new CusIndivContact();
                        cusIndivContact.setIsAgri(agriFlg);
                        cusIndivContact.setDeliveryStreet(fbxd03RespDto.getAddr());//送达地址街道/路
                        //cusIndivContact.setSendAddr(fbxd03RespDto.getAddr());//联系地址/送达地址(风控送的汉字，信贷存的区域码)
                        cusIndivContact.setIndivZipCode(fbxd03RespDto.getSizpcd());
                        cusIndivContact.setWechatNo(fbxd03RespDto.getIfmobl());
                        cusIndivContact.setIndivRsdAddr(fbxd03RespDto.getSiaddr());//居住地址
                        cusIndivContact.setRegionStreet(fbxd03RespDto.getSiaddr());//居住地址
                        cusIndivContact.setIndivRsdSt(fbxd03RespDto.getLive_status());
                        cusIndivContact.setPostcode(fbxd03RespDto.getZpcd());
                        cusIndivContact.setMobile(fbxd03RespDto.getIfmobl());
                        cusIndivContact.setMobileNo(fbxd03RespDto.getIfmobl());//手机号码
                        cusIndivContact.setCusId(cus_id);
                        cusIndivContactMapper.insertSelective(cusIndivContact);
                    } else {
                        int res2 = cusBaseMapper.updateCusIndivContact(cusIndivContactModel);
                    }
                    //更新表3 CUS_INDIV_UNIT
                    QueryModel cusIndivUnitModel = new QueryModel();
                    cusIndivUnitModel.addCondition("indiv_occ", fbxd03RespDto.getIndiv_occ());//职业
                    cusIndivUnitModel.addCondition("indiv_com_name", fbxd03RespDto.getIndiv_com_name());//经营企业名称/工作单位
                    cusIndivUnitModel.addCondition("indiv_com_job_ttl", fbxd03RespDto.getIndiv_com_job_ttl());//职务
                    cusIndivUnitModel.addCondition("cusId", cus_id);
                    //判断是否存在cus_id在CUS_INDIV_UNIT表 不存在inert 否则 update
                    int count3 = cusBaseMapper.queryIsExistCusIdInCusIndivUnit(cus_id);
                    if (count3 == 0) {
                        CusIndivUnit cusIndivUnit = new CusIndivUnit();
                        cusIndivUnit.setOcc(fbxd03RespDto.getIndiv_occ());
                        //cusIndivUnit.setUnitCntName(fbxd03RespDto.getIndiv_com_name());//单位联系人
                        cusIndivUnit.setUnitName(fbxd03RespDto.getIndiv_com_name());//经营企业名称/工作单位
                        cusIndivUnit.setJobTtl(fbxd03RespDto.getIndiv_com_job_ttl());
                        cusIndivUnit.setUnitAddr(indivComAddr);
                        cusIndivUnit.setUnitPhn(indivComPhn);//单位电话
                        cusIndivUnit.setIndivYearn(indivAnnIncm);//个人年收入
                        cusIndivUnit.setCusId(cus_id);
                        cusIndivUnitMapper.insertSelective(cusIndivUnit);
                    } else {
                        int res3 = cusBaseMapper.updateCusIndivUnit(cusIndivUnitModel);
                    }
                    //优享贷开户时风控传入是否农户值更新cus_indiv表
                    if (!"".equals(agriFlg)) {
                        cusIndivModel.addCondition("agri_flg", agriFlg);//是否农户
                        cusIndivModel.addCondition("cusId", cus_id);
                        cusBaseMapper.updateIsAgriFlg(cusIndivModel);
                    }
                    logger.info("******************结束***********************");
                    //成功返回
                    xdkh0021DataRespDto.setOpFlag(DscmsBizXwEnum.SUCCESS.key);
                    xdkh0021DataRespDto.setOpMsg(DscmsBizXwEnum.SUCCESS.value);

                }

            } catch (BizException e) {
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0021.key, DscmsEnum.TRADE_CODE_XDKH0021.value, e.getMessage());
                throw BizException.error(null, e.getErrorCode(), e.getMessage());
            } catch (Exception e) {
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0021.key, DscmsEnum.TRADE_CODE_XDKH0021.value, e.getMessage());
                throw new Exception(EpbEnum.EPB099999.value);
            }
        } else {
            xdkh0021DataRespDto.setOpFlag(DscmsBizXwEnum.FAIL.key);
            xdkh0021DataRespDto.setOpMsg(returnMessage);
            return xdkh0021DataRespDto;
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0021.key, DscmsEnum.TRADE_CODE_XDKH0021.value, xdkh0021DataRespDto);
        return xdkh0021DataRespDto;
    }
}
