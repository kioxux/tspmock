package cn.com.yusys.yusp.vo;

import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import cn.com.yusys.yusp.reportconf.domain.FncConfStyles;

/**
 * <p>Title:Sheet工作表的java对象模型</p>
 * <p>Copyright:yusys Copyright (c) 2021</p>
 * <p>Company: yusys</p>
 * @author qiantj
 * @version 1.0
 */
public class SheetVO {
	public FncConfStyles fncConfStyles;
	public int rownum;//记录行数
	public int colnum;//列数
	public Map<String,CellVO> cellVoMap;//存放了CellVO的集合key=itemId
	public HSSFSheet sheet;
}
