/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusIndivSocial;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.CusIndivSocialDto;
import cn.com.yusys.yusp.dto.CusIndivSocialResp;
import cn.com.yusys.yusp.service.CusIndivSocialService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivSocialResource
 * @类描述: 社会信息处理类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-10 14:15:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusindivsocial")
public class CusIndivSocialResource {
    @Autowired
    private CusIndivSocialService cusIndivSocialService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusIndivSocial>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusIndivSocial> list = cusIndivSocialService.selectAll(queryModel);
        return new ResultDto<List<CusIndivSocial>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusIndivSocial>> index(QueryModel queryModel) {
        List<CusIndivSocial> list = cusIndivSocialService.selectByModel(queryModel);
        return new ResultDto<List<CusIndivSocial>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusIndivSocial> show(@PathVariable("pkId") String pkId) {
        CusIndivSocial cusIndivSocial = cusIndivSocialService.selectByPrimaryKey(pkId);
        return new ResultDto<CusIndivSocial>(cusIndivSocial);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusIndivSocial> create(@RequestBody CusIndivSocial cusIndivSocial) throws URISyntaxException {
        cusIndivSocialService.insert(cusIndivSocial);
        return new ResultDto<CusIndivSocial>(cusIndivSocial);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<String> update(@RequestBody CusIndivSocial cusIndivSocial) throws Exception {
        String result = cusIndivSocialService.update(cusIndivSocial);
        return new ResultDto<String>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusIndivSocialService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusIndivSocialService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:save
     * @函数描述: 社会信息保存
     * @参数与返回说明:
     * @算法描述:
     * @author 周茂伟
     */
    @PostMapping("/save")
    protected ResultDto<String> save(@RequestBody CusIndivSocial cusIndivSocial) throws URISyntaxException {
        return new ResultDto<String>(cusIndivSocialService.save(cusIndivSocial));
    }

    /**
     * @函数名称:getCusMsg
     * @函数描述: 根据证件类型，证件号获取信贷客户信息
     * @参数与返回说明:
     * @算法描述:
     * @return
     */
    @PostMapping("/getCusMsg")
    protected ResultDto<CusIndivSocialDto> getCusMsg(@RequestBody CusIndivSocialDto cusIndivSocialDto) {
        return new ResultDto<CusIndivSocialDto>(cusIndivSocialService.getCusMsg(cusIndivSocialDto));
    }

    /**
     * @return
     * @函数名称:createCus
     * @函数描述: 个人客户创建向导
     * @参数与返回说明:
     * @算法描述:
     * @创建人: xuxin
     */
    @PostMapping("/createCus")
    protected ResultDto<CusIndivSocialDto> createCus(@Validated @RequestBody CusIndivSocialDto CusIndivSocialDto) throws URISyntaxException {
        CusIndivSocialDto cusIndivSocialDtos = cusIndivSocialService.createCus(CusIndivSocialDto);
        return new ResultDto<CusIndivSocialDto>(cusIndivSocialDtos);
    }

    /**
     * 全表查询.
     *
     *
     * @return
     */
    @PostMapping("/selectCusIndivSocialDtoList")
    protected   ResultDto<List<CusIndivSocialResp>> selectCusIndivSocialDtoList(@RequestParam("cusId") String cusId) {
        CusIndivSocialResp CusIndivSocialResp=new CusIndivSocialResp();
        List<CusIndivSocialResp> cusIndivSociallist = cusIndivSocialService.selectCusIndivSocialByCusId(cusId);
        return new ResultDto<List<CusIndivSocialResp>>(cusIndivSociallist);
    }

    /**
     * 全表查询.
     *
     *
     * @return
     */
    @PostMapping("/selectcusindivsocial")
    protected   ResultDto<List<CusIndivSocialResp>> selectCusIndivSocialDto(@RequestBody Map map) {
        CusIndivSocialResp CusIndivSocialResp=new CusIndivSocialResp();
        List<CusIndivSocialResp> cusIndivSociallist = cusIndivSocialService.selectCusIndivSocialByCusId((String)map.get("cusId"));
        return new ResultDto<List<CusIndivSocialResp>>(cusIndivSociallist);
    }

    /**
     * @函数名称:queryCusSocial
     * @函数描述: 查询社会关系人信息
     * @参数与返回说明:model
     * @算法描述:
     */
    @PostMapping("/querycussocial")
    protected ResultDto<List<CusIndivSocialResp>> queryCusSocial(@RequestBody QueryModel model) {
        List<CusIndivSocialResp> list = cusIndivSocialService.queryCusSocial(model);
        return new ResultDto<List<CusIndivSocialResp>>(list);
    }
}
