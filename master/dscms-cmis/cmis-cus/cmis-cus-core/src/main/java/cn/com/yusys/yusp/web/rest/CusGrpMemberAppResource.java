/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusGrpApp;
import cn.com.yusys.yusp.domain.CusGrpMemberApp;
import cn.com.yusys.yusp.service.CusGrpMemberAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpMemberAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: fangzhen
 * @创建时间: 2021-03-05 14:24:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusgrpmemberapp")
public class CusGrpMemberAppResource {
    @Autowired
    private CusGrpMemberAppService cusGrpMemberAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusGrpMemberApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusGrpMemberApp> list = cusGrpMemberAppService.selectAll(queryModel);
        return new ResultDto<List<CusGrpMemberApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusGrpMemberApp>> index(QueryModel queryModel) {
        List<CusGrpMemberApp> list = cusGrpMemberAppService.selectByModel(queryModel);
        return new ResultDto<List<CusGrpMemberApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusGrpMemberApp>> query(@RequestBody QueryModel queryModel) {
        List<CusGrpMemberApp> list = cusGrpMemberAppService.selectByModel(queryModel);
        return new ResultDto<List<CusGrpMemberApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusGrpMemberApp> show(@PathVariable("pkId") String pkId) {
        CusGrpMemberApp cusGrpMemberApp = cusGrpMemberAppService.selectByPrimaryKey(pkId);
        return new ResultDto<CusGrpMemberApp>(cusGrpMemberApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusGrpMemberApp> create(@RequestBody CusGrpMemberApp cusGrpMemberApp) throws URISyntaxException {
        cusGrpMemberAppService.insert(cusGrpMemberApp);
        return new ResultDto<CusGrpMemberApp>(cusGrpMemberApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusGrpMemberApp cusGrpMemberApp) throws URISyntaxException {
        int result = cusGrpMemberAppService.updateSelective(cusGrpMemberApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusGrpMemberAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusGrpMemberAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: updateCusGrpMemCorreType
     * @方法描述: 集团认定修改进入，如果集团紧密程度发生改变，则更新成员集团中的关联类型
     * 集团紧密程度如果时实体集团，则成员关联类型为：母公司、子公司
     * 集团紧密程度如果时一般关系，则集团关联类型为：核心成员，一般成员
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/updatecusgrpmemcorretype")
    protected ResultDto<Integer> updateCusGrpMemCorreType(@RequestBody CusGrpApp cusGrpApp) {
        cusGrpMemberAppService.updateCusGrpMemCorreType(cusGrpApp);
        return new ResultDto<Integer>(1);
    }

    /**
     * @函数名称:checkCusGrpMemAppState
     * @函数描述: 校验该成员是否存在在途申请
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkcusgrpmemappstate")
    protected ResultDto<Map> checkCusGrpMemAppState(@RequestBody CusGrpMemberApp cusGrpMemberApp) {
        Map result = cusGrpMemberAppService.checkCusGrpMemAppState(cusGrpMemberApp);
        return new ResultDto<Map>(result);
    }

    /**
     * @函数名称:checkCusGrpMemApp
     * @函数描述:校验该成员是否存在在途申请以及是否存在正式集团中
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkcusgrpmemapp")
    protected ResultDto<Map> checkCusGrpMemApp(@RequestBody CusGrpMemberApp cusGrpMemberApp) {
        Map result = cusGrpMemberAppService.checkCusGrpMemApp(cusGrpMemberApp);
        return new ResultDto<Map>(result);
    }


    /**
     * @函数名称:updateCusGrpMemAppChgType
     * @函数描述:更新成员分项中的变更类型
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatecusgrpmemappchgtype")
    protected ResultDto<Map> updateCusGrpMemAppChgType(@RequestBody Map map) {
        Map result = cusGrpMemberAppService.updateCusGrpMemAppChgType(map);
        return new ResultDto<Map>(result);
    }

    /**
     * @函数名称:getMemberFromRel
     * @函数描述:集团客户变更下一步逻辑
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getMemberFromRel")
    protected ResultDto<Void> getMemberFromRel(@RequestBody Map map) {
        cusGrpMemberAppService.getMemberFromRel(map);
        return new ResultDto<>();
    }
}
