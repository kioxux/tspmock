/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CusLstDedkkh;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 14:36:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusLstDedkkhMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusLstDedkkh selectByPrimaryKey(@Param("listSerno") String listSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusLstDedkkh> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusLstDedkkh record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusLstDedkkh record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusLstDedkkh record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusLstDedkkh record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("listSerno") String listSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    List<Map<String, Object>> selectByModelxp(QueryModel model);

    /**
     * @方法名称: updateStatusPass
     * @方法描述: 根据客户号更新审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateStatusPass(@Param("cusId") String cusId);

    /**
     * @方法名称: updateStatusRefuse
     * @方法描述: 根据主键更新审批状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateStatusRefuse(@Param("listSerno") String listSerno);

    /**
     * @函数名称:queryNewDekhDataByCusId
     * @函数描述:根据客户号查询最新的信息 审查报告
     * @参数与返回说明:
     * @算法描述:
     */

    CusLstDedkkh queryNewDekhDataByCusId(CusLstDedkkh cusLstDedkkh);

    Map queryNewDekhByCusId(String cusId);
}