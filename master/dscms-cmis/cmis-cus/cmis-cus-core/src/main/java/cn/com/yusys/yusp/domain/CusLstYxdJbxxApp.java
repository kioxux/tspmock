/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYxdJbxxApp
 * @类描述: cus_lst_yxd_jbxx_app数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:15:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_yxd_jbxx_app")
public class CusLstYxdJbxxApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 申请日期 **/
	@Column(name = "APP_DATE", unique = false, nullable = true, length = 10)
	private String appDate;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 30)
	private String certCode;
	
	/** 手机号码 **/
	@Column(name = "MOBILE_NO", unique = false, nullable = true, length = 20)
	private String mobileNo;
	
	/** 学历 **/
	@Column(name = "EDU", unique = false, nullable = true, length = 5)
	private String edu;
	
	/** 性别 **/
	@Column(name = "SEX", unique = false, nullable = true, length = 5)
	private String sex;
	
	/** 是否本地户 **/
	@Column(name = "IS_REGION", unique = false, nullable = true, length = 5)
	private String isRegion;
	
	/** 居住年限 **/
	@Column(name = "RESI_YEARS", unique = false, nullable = true, length = 5)
	private String resiYears;
	
	/** 婚姻状态 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
	private String marStatus;
	
	/** 年收入 **/
	@Column(name = "YEARN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearn;
	
	/** 工作年限 **/
	@Column(name = "CPRT_YEARS", unique = false, nullable = true, length = 5)
	private String cprtYears;
	
	/** 职务 **/
	@Column(name = "DUTY", unique = false, nullable = true, length = 5)
	private String duty;
	
	/** 工作单位 **/
	@Column(name = "WORK_UNIT", unique = false, nullable = true, length = 200)
	private String workUnit;
	
	/** 居住地址 **/
	@Column(name = "RESI_ADDR", unique = false, nullable = true, length = 200)
	private String resiAddr;
	
	/** 申请金额 **/
	@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;
	
	/** 年利率 **/
	@Column(name = "YEAR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearRate;
	
	/** 经办人 **/
	@Column(name = "HUSER", unique = false, nullable = true, length = 20)
	private String huser;
	
	/** 经办机构 **/
	@Column(name = "HAND_ORG", unique = false, nullable = true, length = 20)
	private String handOrg;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 影像编号 **/
	@Column(name = "IMAGE_NO", unique = false, nullable = true, length = 40)
	private String imageNo;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate;
	}
	
    /**
     * @return appDate
     */
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
    /**
     * @return mobileNo
     */
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu;
	}
	
    /**
     * @return edu
     */
	public String getEdu() {
		return this.edu;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param isRegion
	 */
	public void setIsRegion(String isRegion) {
		this.isRegion = isRegion;
	}
	
    /**
     * @return isRegion
     */
	public String getIsRegion() {
		return this.isRegion;
	}
	
	/**
	 * @param resiYears
	 */
	public void setResiYears(String resiYears) {
		this.resiYears = resiYears;
	}
	
    /**
     * @return resiYears
     */
	public String getResiYears() {
		return this.resiYears;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}
	
    /**
     * @return marStatus
     */
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param yearn
	 */
	public void setYearn(java.math.BigDecimal yearn) {
		this.yearn = yearn;
	}
	
    /**
     * @return yearn
     */
	public java.math.BigDecimal getYearn() {
		return this.yearn;
	}
	
	/**
	 * @param cprtYears
	 */
	public void setCprtYears(String cprtYears) {
		this.cprtYears = cprtYears;
	}
	
    /**
     * @return cprtYears
     */
	public String getCprtYears() {
		return this.cprtYears;
	}
	
	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty;
	}
	
    /**
     * @return duty
     */
	public String getDuty() {
		return this.duty;
	}
	
	/**
	 * @param workUnit
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	
    /**
     * @return workUnit
     */
	public String getWorkUnit() {
		return this.workUnit;
	}
	
	/**
	 * @param resiAddr
	 */
	public void setResiAddr(String resiAddr) {
		this.resiAddr = resiAddr;
	}
	
    /**
     * @return resiAddr
     */
	public String getResiAddr() {
		return this.resiAddr;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return appAmt
     */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param yearRate
	 */
	public void setYearRate(java.math.BigDecimal yearRate) {
		this.yearRate = yearRate;
	}
	
    /**
     * @return yearRate
     */
	public java.math.BigDecimal getYearRate() {
		return this.yearRate;
	}
	
	/**
	 * @param huser
	 */
	public void setHuser(String huser) {
		this.huser = huser;
	}
	
    /**
     * @return huser
     */
	public String getHuser() {
		return this.huser;
	}
	
	/**
	 * @param handOrg
	 */
	public void setHandOrg(String handOrg) {
		this.handOrg = handOrg;
	}
	
    /**
     * @return handOrg
     */
	public String getHandOrg() {
		return this.handOrg;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}
	
    /**
     * @return imageNo
     */
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}


}