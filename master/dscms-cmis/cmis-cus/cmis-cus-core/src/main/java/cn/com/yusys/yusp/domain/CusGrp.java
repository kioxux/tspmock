/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrp
 * @类描述: cus_grp数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-08 15:58:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_grp")
public class CusGrp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 集团客户编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "GRP_NO")
	private String grpNo;
	
	/** 集团客户名称 **/
	@Column(name = "GRP_NAME", unique = false, nullable = true, length = 80)
	private String grpName;
	
	/** 核心客户编号 **/
	@Column(name = "CORE_CUS_ID", unique = false, nullable = true, length = 30)
	private String coreCusId;
	
	/** 核心客户中征码 **/
	@Column(name = "CORE_CUS_LOAN_CARD_ID", unique = false, nullable = true, length = 20)
	private String coreCusLoanCardId;
	
	/** 核心客户名称 **/
	@Column(name = "CORE_CUS_NAME", unique = false, nullable = true, length = 80)
	private String coreCusName;
	
	/** 核心客户证件号码 **/
	@Column(name = "CORE_CUS_CERT_NO", unique = false, nullable = true, length = 40)
	private String coreCusCertNo;
	
	/** 更新办公地址日期 **/
	@Column(name = "UPDATE_OFFICE_ADDR_DATE", unique = false, nullable = true, length = 100)
	private String updateOfficeAddrDate;
	
	/** 办公地址 **/
	@Column(name = "OFFICE_ADDR", unique = false, nullable = true, length = 100)
	private String officeAddr;
	
	/** 社会信用代码 **/
	@Column(name = "SOCIAL_CREDIT_CODE", unique = false, nullable = true, length = 18)
	private String socialCreditCode;
	
	/** 工商登记注册号 **/
	@Column(name = "BUSINESS_CIRCLES_REGI_NO", unique = false, nullable = true, length = 10)
	private String businessCirclesRegiNo;
	
	/** 办公地址行政区划 **/
	@Column(name = "OFFICE_ADDR_ADMIN_DIV", unique = false, nullable = true, length = 255)
	private String officeAddrAdminDiv;
	
	/** 集团紧密程度 **/
	@Column(name = "GRP_CLOSELY_DEGREE", unique = false, nullable = true, length = 40)
	private String grpCloselyDegree;
	
	/** 集团客户情况说明 **/
	@Column(name = "GRP_CASE_MEMO", unique = false, nullable = true, length = 250)
	private String grpCaseMemo;
	
	/** 管户客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 所属机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 40)
	private String managerBrId;
	
	/** 集团客户状态 **/
	@Column(name = "GROUP_CUS_STATUS", unique = false, nullable = true, length = 5)
	private String groupCusStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 集团性质*/
	@Column(name = "GRP_NATURE", unique = false, nullable = true, length = 2)
	private String grpNature;
	
	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo;
	}
	
    /**
     * @return grpNo
     */
	public String getGrpNo() {
		return this.grpNo;
	}
	
	/**
	 * @param grpName
	 */
	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}
	
    /**
     * @return grpName
     */
	public String getGrpName() {
		return this.grpName;
	}
	
	/**
	 * @param coreCusId
	 */
	public void setCoreCusId(String coreCusId) {
		this.coreCusId = coreCusId;
	}
	
    /**
     * @return coreCusId
     */
	public String getCoreCusId() {
		return this.coreCusId;
	}
	
	/**
	 * @param coreCusLoanCardId
	 */
	public void setCoreCusLoanCardId(String coreCusLoanCardId) {
		this.coreCusLoanCardId = coreCusLoanCardId;
	}
	
    /**
     * @return coreCusLoanCardId
     */
	public String getCoreCusLoanCardId() {
		return this.coreCusLoanCardId;
	}
	
	/**
	 * @param coreCusName
	 */
	public void setCoreCusName(String coreCusName) {
		this.coreCusName = coreCusName;
	}
	
    /**
     * @return coreCusName
     */
	public String getCoreCusName() {
		return this.coreCusName;
	}
	
	/**
	 * @param coreCusCertNo
	 */
	public void setCoreCusCertNo(String coreCusCertNo) {
		this.coreCusCertNo = coreCusCertNo;
	}
	
    /**
     * @return coreCusCertNo
     */
	public String getCoreCusCertNo() {
		return this.coreCusCertNo;
	}
	
	/**
	 * @param updateOfficeAddrDate
	 */
	public void setUpdateOfficeAddrDate(String updateOfficeAddrDate) {
		this.updateOfficeAddrDate = updateOfficeAddrDate;
	}
	
    /**
     * @return updateOfficeAddrDate
     */
	public String getUpdateOfficeAddrDate() {
		return this.updateOfficeAddrDate;
	}
	
	/**
	 * @param officeAddr
	 */
	public void setOfficeAddr(String officeAddr) {
		this.officeAddr = officeAddr;
	}
	
    /**
     * @return officeAddr
     */
	public String getOfficeAddr() {
		return this.officeAddr;
	}
	
	/**
	 * @param socialCreditCode
	 */
	public void setSocialCreditCode(String socialCreditCode) {
		this.socialCreditCode = socialCreditCode;
	}
	
    /**
     * @return socialCreditCode
     */
	public String getSocialCreditCode() {
		return this.socialCreditCode;
	}
	
	/**
	 * @param businessCirclesRegiNo
	 */
	public void setBusinessCirclesRegiNo(String businessCirclesRegiNo) {
		this.businessCirclesRegiNo = businessCirclesRegiNo;
	}
	
    /**
     * @return businessCirclesRegiNo
     */
	public String getBusinessCirclesRegiNo() {
		return this.businessCirclesRegiNo;
	}
	
	/**
	 * @param officeAddrAdminDiv
	 */
	public void setOfficeAddrAdminDiv(String officeAddrAdminDiv) {
		this.officeAddrAdminDiv = officeAddrAdminDiv;
	}
	
    /**
     * @return officeAddrAdminDiv
     */
	public String getOfficeAddrAdminDiv() {
		return this.officeAddrAdminDiv;
	}
	
	/**
	 * @param grpCloselyDegree
	 */
	public void setGrpCloselyDegree(String grpCloselyDegree) {
		this.grpCloselyDegree = grpCloselyDegree;
	}
	
    /**
     * @return grpCloselyDegree
     */
	public String getGrpCloselyDegree() {
		return this.grpCloselyDegree;
	}
	
	/**
	 * @param grpCaseMemo
	 */
	public void setGrpCaseMemo(String grpCaseMemo) {
		this.grpCaseMemo = grpCaseMemo;
	}
	
    /**
     * @return grpCaseMemo
     */
	public String getGrpCaseMemo() {
		return this.grpCaseMemo;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param groupCusStatus
	 */
	public void setGroupCusStatus(String groupCusStatus) {
		this.groupCusStatus = groupCusStatus;
	}
	
    /**
     * @return groupCusStatus
     */
	public String getGroupCusStatus() {
		return this.groupCusStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	public String getUpdDate() {
		return updDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getGrpNature() {
		return grpNature;
	}

	public void setGrpNature(String grpNature) {
		this.grpNature = grpNature;
	}

}