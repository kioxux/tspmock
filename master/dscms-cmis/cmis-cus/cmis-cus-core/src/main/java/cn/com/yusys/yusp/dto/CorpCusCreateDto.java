package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 企业客户开户
 */
public class CorpCusCreateDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String cusId;
    private String cusName;
    private String certType;
    private String certCode;
    private String country;
    private String legalName;
    private String postAddr;
    private String legalPhone;
    private String cusType;
    private String certIdate;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getPostAddr() {
        return postAddr;
    }

    public void setPostAddr(String postAddr) {
        this.postAddr = postAddr;
    }

    public String getLegalPhone() {
        return legalPhone;
    }

    public void setLegalPhone(String legalPhone) {
        this.legalPhone = legalPhone;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCertIdate() {
        return certIdate;
    }

    public void setCertIdate(String certIdate) {
        this.certIdate = certIdate;
    }
}
