package cn.com.yusys.yusp.workflow.listener;

import cn.com.yusys.yusp.flow.client.ClientBizFactory;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 客户管理业务监听（客户端）
 */
@Service
public class CusBizMessageListener {
    private final Logger log = LoggerFactory.getLogger(CusBizMessageListener.class);

    @Autowired
    private ClientBizFactory clientBizFactory;

    @RabbitListener(queuesToDeclare =
            {
                    @Queue("yusp-flow.KHGL01"),
                    @Queue("yusp-flow.KHGL02"),
                    @Queue("yusp-flow.KHGL03"),
                    @Queue("yusp-flow.KHGL04"),
                    @Queue("yusp-flow.KHGL05"),
                    @Queue("yusp-flow.KHGL06"),
                    @Queue("yusp-flow.KHGL07"),
                    @Queue("yusp-flow.KHGL08"),
                    @Queue("yusp-flow.KHGL09"),
                    @Queue("yusp-flow.KHGL10"),
                    @Queue("yusp-flow.KHGL12"),
                    @Queue("yusp-flow.KHGL13"),
                    @Queue("yusp-flow.KHGL14"),
                    @Queue("yusp-flow.SGCZ01"),//客户移交审批流程（寿光）
                    @Queue("yusp-flow.SGCZ02"),//业务权申领审批流程（寿光）
                    @Queue("yusp-flow.SGCZ37"),// 银票签发白名单申请审批流程（寿光）
                    @Queue("yusp-flow.SGCZ38"),// 无还本续贷白名单申请审批流程（寿光）
                    @Queue("yusp-flow.DHCZ01"),//客户移交审批流程（东海）
                    @Queue("yusp-flow.DHCZ02"),//业务权申领审批流程（东海）
                    @Queue("yusp-flow.DHCZ38"),// 银票签发白名单申请审批流程（东海）
                    @Queue("yusp-flow.DHCZ39"),// 无还本续贷白名单申请审批流程（东海）

            })// 队列名称为【yusp-flow.流程申请类型】,可以添加多个
    @RabbitHandler
    public void receiveQueue(ResultInstanceDto message) throws Exception {
        log.info("客户端（客户管理）业务监听:" + message);
        clientBizFactory.processBiz(message);
    }
}