/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Queue;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstYndCmdcApp;
import cn.com.yusys.yusp.repository.mapper.CusLstYndCmdcAppMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndCmdcAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:19:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYndCmdcAppService {

    @Autowired
    private CusLstYndCmdcAppMapper cusLstYndCmdcAppMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstYndCmdcApp selectByPrimaryKey(String pkId) {
        return cusLstYndCmdcAppMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYndCmdcApp> selectAll(QueryModel model) {
        List<CusLstYndCmdcApp> records = (List<CusLstYndCmdcApp>) cusLstYndCmdcAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstYndCmdcApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYndCmdcApp> list = cusLstYndCmdcAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstYndCmdcApp record) {
        return cusLstYndCmdcAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstYndCmdcApp record) {
        return cusLstYndCmdcAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstYndCmdcApp record) {
        return cusLstYndCmdcAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstYndCmdcApp record) {
        return cusLstYndCmdcAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusLstYndCmdcAppMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYndCmdcAppMapper.deleteByIds(ids);
    }


    /**
     * @param cusLstYndCmdcApp
     * @return int
     * @author hubp
     * @date 2021/7/15 15:36
     * @version 1.0.0
     * @desc    新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int addCusLstYndCmdcApp(CusLstYndCmdcApp cusLstYndCmdcApp) {
        int result = 0;
        if (StringUtils.isBlank(cusLstYndCmdcApp.getPkId())) {
            cusLstYndCmdcApp.setPkId(StringUtils.getUUID());
            cusLstYndCmdcApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            result = cusLstYndCmdcAppMapper.insert(cusLstYndCmdcApp);
        } else {
            CusLstYndCmdcApp clyl = cusLstYndCmdcAppMapper.selectByPrimaryKey(cusLstYndCmdcApp.getPkId());
            if (null == clyl) {
                cusLstYndCmdcApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                result = cusLstYndCmdcAppMapper.insert(cusLstYndCmdcApp);
            } else {
                result = cusLstYndCmdcAppMapper.updateByPrimaryKey(cusLstYndCmdcApp);
            }
        }
        return result;
    }
    
    /**
     * @param serno
     * @return java.util.List
     * @author hubp
     * @date 2021/7/16 10:34
     * @version 1.0.0
     * @desc 通过流水号查询侧面调查信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(readOnly=true)
    public List<CusLstYndCmdcApp> selectBySerno(String serno){
        return cusLstYndCmdcAppMapper.selectBySerno(serno);
    }

    /**
     * @param serno
     * @return java.lang.Integer
     * @author hubp
     * @date 2021/7/16 14:20
     * @version 1.0.0
     * @desc   通过流水号批量删除侧面调查信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Integer deleteBySerno(String serno){
        return cusLstYndCmdcAppMapper.deleteBySerno(serno);
    }
}
