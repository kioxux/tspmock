package cn.com.yusys.yusp.web.server.xdkh0033;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0033.req.Xdkh0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0033.resp.Xdkh0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0033.Xdkh0033Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:客户评级结果同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0033:客户评级结果同步")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0033Resource.class);

    @Autowired
    private Xdkh0033Service xdkh0033Service;
    /**
     * 交易码：xdkh0033
     * 交易描述：客户评级结果同步
     *
     * @param xdkh0033DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户评级结果同步")
    @PostMapping("/xdkh0033")
    protected @ResponseBody
    ResultDto<Xdkh0033DataRespDto> xdkh0033(@Validated @RequestBody Xdkh0033DataReqDto xdkh0033DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, JSON.toJSONString(xdkh0033DataReqDto));
        Xdkh0033DataRespDto xdkh0033DataRespDto = new Xdkh0033DataRespDto();// 响应Dto:客户评级结果同步
        ResultDto<Xdkh0033DataRespDto> xdkh0033DataResultDto = new ResultDto<>();
        try {
            // TODO 调用XXXXXService层开始
            xdkh0033DataRespDto = xdkh0033Service.xdkh0033(xdkh0033DataReqDto);
            // TODO 调用XXXXXService层结束
            // 封装xdkh0033DataResultDto中正确的返回码和返回信息
            xdkh0033DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0033DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, e.getMessage());
            // 封装xdkh0033DataResultDto中异常返回码和返回信息
            xdkh0033DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0033DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0033DataRespDto到xdkh0033DataResultDto中
        xdkh0033DataResultDto.setData(xdkh0033DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, JSON.toJSONString(xdkh0033DataResultDto));
        return xdkh0033DataResultDto;
    }
}