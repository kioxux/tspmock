/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.CusCorpCertDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusCorpCert;
import cn.com.yusys.yusp.repository.mapper.CusCorpCertMapper;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpCertService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-25 16:21:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusCorpCertService {

    @Autowired
    private CusCorpCertMapper cusCorpCertMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusCorpCert selectByPrimaryKey(String pkId) {
        return cusCorpCertMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusCorpCert> selectAll(QueryModel model) {
        List<CusCorpCert> records = (List<CusCorpCert>) cusCorpCertMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusCorpCert> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusCorpCert> list = cusCorpCertMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusCorpCert record) {
        return cusCorpCertMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusCorpCert record) {
        return cusCorpCertMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusCorpCert record) {
        return cusCorpCertMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusCorpCert record) {
        return cusCorpCertMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusCorpCertMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusCorpCertMapper.deleteByIds(ids);
    }

    /**
     * 根据客户号查询
     * @param cusId
     * @return
     */
    public List<CusCorpCertDto> selectByCusId( String cusId){
        return cusCorpCertMapper.selectByCusId(cusId);
    }

    /**
     * 根据客户号查询
     * @param map
     * @return
     */
    public List<CusCorpCertDto> queryCusCorpCertDataByParams(Map map){
        return cusCorpCertMapper.queryCusCorpCertDataByParams(map);
    }

    /**
     * 根据证件号查询客户ID
     * @param certCode
     * @return
     */
    public String queryCusIdByCertCode(String certCode) {
        return cusCorpCertMapper.queryCusIdByCertCode(certCode);
    }
}
