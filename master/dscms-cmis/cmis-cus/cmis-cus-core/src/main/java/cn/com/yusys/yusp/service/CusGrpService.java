/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusGrp;
import cn.com.yusys.yusp.domain.CusGrpApp;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.dto.CusGrpDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.CircleArrayMem;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004RespDto;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusGrpMapper;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-08 15:58:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusGrpService {
    private final Logger log = LoggerFactory.getLogger(CusGrpService.class);

    @Autowired
    private CusGrpMapper cusGrpMapper;

    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;
    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;
    @Autowired
    private CusGrpService cusGrpService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusGrp selectByPrimaryKey(String grpNo) {
        return cusGrpMapper.selectByPrimaryKey(grpNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusGrp> selectAll(QueryModel model) {
        List<CusGrp> records = (List<CusGrp>) cusGrpMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusGrp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusGrp> list = cusGrpMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusGrp record) {
        return cusGrpMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusGrp record) {
        return cusGrpMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusGrp record) {
        return cusGrpMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusGrp record) {
        return cusGrpMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String grpNo) {
        return cusGrpMapper.deleteByPrimaryKey(grpNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusGrpMapper.deleteByIds(ids);
    }

    /**
     * @param coreCusId 核心客户号
     * @return true 校验通过
     * @方法描述：根据核心客户号，查询该客户是否已经存在生效的集团核心客户中，如果存在，则校验不通过
     */
    public boolean checkCusGrpCoreId(String coreCusId) {
        //查看该核心客户是否已经存在集团核心客户中
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("coreCusId", coreCusId);
        queryModel.addCondition("groupCusStatus", CmisCusConstants.GROUP_CUS_STATUS_01);
        queryModel.addCondition("oprType", CmisCusConstants.OPR_TYPE_INSERT);
        //查询该核心客户是否存在有效状态的集团中
        List<CusGrp> cusList = cusGrpMapper.selectByModel(queryModel);
        if (cusList.size() > 0) {
            throw new YuspException(EcsEnum.CUS_GRP_CHECK_DEF.key, EcsEnum.CUS_GRP_CHECK_DEF.value);
        }
        return true;
    }


    /**
     * @方法名称: createCusGrpRecordByCusGrpApp
     * @方法描述: 根据申请信息(CusGrpApp)创建CusGrp实例
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusGrp createCusGrpRecordByCusGrpApp(CusGrpApp cusGrpApp) {

//        User userInfo = SessionUtils.getUserInformation();
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        String nowTime = DateUtils.getCurrentDate(DateFormatEnum.DATETIME);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date currTime = null ;
        try{
            currTime = simpleDateFormat.parse(nowTime) ;
        } catch (ParseException e) {
            log.error(e.getMessage(),e);
        }


        CusGrp cusGrp = new CusGrp();
        BeanUtils.copyProperties(cusGrpApp, cusGrp);
        //集团机密程度，申请表和正式表字段不一致，此处转换  （搞不懂为什么不一致）
        cusGrp.setGrpCloselyDegree(cusGrpApp.getGrpCloselyDegree());
        //集团客户情况说明，搞不懂为啥不一致
        cusGrp.setGrpCaseMemo(cusGrpApp.getGrpCaseMemo());
        //集团客户状态赋值
        cusGrp.setGroupCusStatus(CmisCusConstants.STD_GROUP_CUS_STATUS_01);

        //登记人登记机构处理
//        cusGrp.setInputId(userInfo.getLoginCode());
//        cusGrp.setInputBrId(userInfo.getOrg().getCode());
        cusGrp.setInputDate(nowDate);
//        cusGrp.setUpdId(userInfo.getLoginCode());
//        cusGrp.setUpdBrId(userInfo.getOrg().getCode());
        cusGrp.setUpdDate(DateUtils.getCurrDateStr());
        cusGrp.setCreateTime(currTime);
        cusGrp.setUpdateTime(currTime);

        return cusGrp;
    }

    /**
     * @方法名称: insertCusGrpMemberAppByCusGrpMemRel
     * @方法描述: 集团选取成功，将集团中的客户落入到申请表中  cus_grp_member_rel ->cus_grp_member_App
     * @参数与返回说明:
     * @算法描述: 无
     */
    public Map insertCusGrpMemberAppByCusGrpMemRel(CusGrpApp domain) {
        log.info("集团变更，解散校验，成员信息落入成员申请表中【{}】", domain.getSerno());
        HashMap<String, String> resultMap = new HashMap();
        String rtnCode = EcsEnum.CUS_GRP_SUCCESS_DEF.key;
        String rtnMsg = EcsEnum.CUS_GRP_SUCCESS_DEF.value;
        //CusGrp finalCusGrp;
        try {
            /***********************************  调用接口开始  ***************************************************************/
            //finalCusGrp = cusGrpService.selectByPrimaryKey(domain.getGrpNo());
            G11004ReqDto g11004ReqDto = new G11004ReqDto();
            g11004ReqDto.setResotp("2");
            g11004ReqDto.setCustno(domain.getCoreCusId());
            ResultDto<G11004RespDto> resultDto = dscms2EcifClientService.g11004(g11004ReqDto);
            String g11004Code = Optional.ofNullable(resultDto.getCode()).orElse(StringUtils.EMPTY);
            String g11004Meesage = Optional.ofNullable(resultDto.getMessage()).orElse(StringUtils.EMPTY);
            G11004RespDto g11004RespDto = null;
            // TODO 获取相关的值并解析
            // 获取查询结果 查询成功失败的情况
            g11004RespDto = resultDto.getData();
            if (!ObjectUtils.isEmpty(g11004RespDto) && !StringUtils.isEmpty(g11004RespDto.getCrpcno())) {
                domain.setCoreCusId(g11004RespDto.getCustno());
            } else {
                //调用Ecif 创建集团客户接口，获取集团客户编号
                G11003ReqDto g11003ReqDto = new G11003ReqDto();
                g11003ReqDto.setGrpsta("1");	//集团客户状态 ECIF 0未生效 1生效 2调整中
                g11003ReqDto.setGrouna(domain.getGrpName());	//集团客户名称
                g11003ReqDto.setCustno(domain.getCoreCusId());	//客户编号
                g11003ReqDto.setCustln(domain.getCoreCusLoanCardId());	//客户中证码
                g11003ReqDto.setCustna(domain.getCoreCusName());	//客户名称
                g11003ReqDto.setIdtfno(domain.getCoreCusCertNo());	//证件号码
                g11003ReqDto.setOadate(DateUtils.formatDate10To8(domain.getUpdateOfficeAddrDate()));	//更新办公地址日期
                g11003ReqDto.setHomead(domain.getOfficeAddr());	//地址
                g11003ReqDto.setSocino(domain.getSociCredCode());	//社会信用代码
                g11003ReqDto.setBusino(domain.getBusinessCirclesRegiNo());	//工商登记注册号
                g11003ReqDto.setOfaddr(domain.getOfficeAddrAdminDiv());	//办公地址行政区划
                g11003ReqDto.setCrpdeg(domain.getGrpCloselyDegree());	//集团紧密程度
                g11003ReqDto.setCrpcno(domain.getGrpCaseMemo());	//集团客户情况说明
                g11003ReqDto.setCtigno(domain.getManagerId());	//管户客户经理
                g11003ReqDto.setCitorg(domain.getManagerBrId());	//所属机构
                g11003ReqDto.setOprtye(domain.getOprType());	//操作类型
                g11003ReqDto.setInptno(domain.getInputId());	//登记人
                g11003ReqDto.setInporg(domain.getInputBrId());	//登记机构

                //查询正式集团中的成员信息
                QueryModel queryModel = new QueryModel();
                //集团编号
                queryModel.addCondition("grpNo", domain.getGrpNo());
                //是否生效 1 生效
                queryModel.addCondition("availableInd", CmisCusConstants.STD_AVAILABLE_IND_1);
                //操作类型 默认 01 新增
                queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                //查询生效的
                List<CusGrpMemberRel> cusGrpMemberRelList = cusGrpMemberRelService.selectByModel(queryModel);
                List<CircleArrayMem> circleArrayMemList = new ArrayList<>();
                if(CollectionUtils.nonEmpty(cusGrpMemberRelList)){
                    for (CusGrpMemberRel cusGrpMemberRel: cusGrpMemberRelList) {
                        CircleArrayMem circleArrayMem = new CircleArrayMem();
                        circleArrayMem.setCustno(cusGrpMemberRel.getCusId());	//集团成员客户号
                        circleArrayMem.setCortype(cusGrpMemberRel.getGrpCorreType());	//集团关系类型
                        circleArrayMem.setGrpdesc(cusGrpMemberRel.getGrpCorreDec());	//集团关系描述
                        circleArrayMem.setIscust(cusGrpMemberRel.getIsMainCus());	//是否集团主客户
                        circleArrayMem.setMainno(cusGrpMemberRel.getManagerId());	//主管人
                        circleArrayMem.setMaiorg(cusGrpMemberRel.getManagerBrId());	//主管机构
                        circleArrayMem.setSocino(cusGrpMemberRel.getSociCredCode());	//统一社会信用代码
                        circleArrayMem.setCustst("0");	//成员状态 ECIF 0：有效 1：无效
                        circleArrayMemList.add(circleArrayMem);
                    }
                }
                g11003ReqDto.setCircleArrayMem(circleArrayMemList);
                ResultDto<G11003RespDto> g11003ResultDto = dscms2EcifClientService.g11003(g11003ReqDto);
                String g11003Code = Optional.ofNullable(g11003ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String g11003Meesage = Optional.ofNullable(g11003ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                G11003RespDto g11003RespDto = null;
                //  获取相关的值并解析
                g11003RespDto = g11003ResultDto.getData();
                if (!ObjectUtils.isEmpty(g11003RespDto) && !StringUtils.isEmpty(g11003RespDto.getGrouno())) {
                    domain.setGrpNo(g11003RespDto.getGrouno());
                } else {
                    //  抛出错误异常
                    throw new YuspException(g11003Code, g11003Meesage);
                }
            }




        } catch (YuspException e) {
            log.error("集团变更，解散拷贝成员数据到申请表！", e);
            rtnCode = e.getCode();
            rtnMsg = e.getMessage();
        } finally {
            resultMap.put("rtnCode", rtnCode);
            resultMap.put("rtnMsg", rtnMsg);
        }

        return resultMap;
    }

    /**
     * 根据集团号查询集团信息
     * @param grpNo
     * @return
     */
    public CusGrpDto selectCusGrpDtoByGrpNo(String grpNo){
        return cusGrpMapper.selectCusGrpDtoByGrpNo(grpNo);
    }
}
