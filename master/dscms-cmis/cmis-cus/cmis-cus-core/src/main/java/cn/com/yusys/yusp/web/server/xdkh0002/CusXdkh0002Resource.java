package cn.com.yusys.yusp.web.server.xdkh0002;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0002.req.Xdkh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.resp.Xdkh0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0002.Xdkh0002Service;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 接口处理类:对公客户基本信息查询
 *
 * @author LIHH
 * @version 1.0
 */
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0002Resource.class);

    @Autowired
    private Xdkh0002Service xdkh0002Service;

    /**
     * 交易码：xdkh0002
     * 交易描述：对公客户基本信息查询
     *
     * @param xdkh0002DataReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdkh0002")
    protected @ResponseBody
    ResultDto<Xdkh0002DataRespDto> xdkh0002(@Validated @RequestBody Xdkh0002DataReqDto xdkh0002DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002DataReqDto));
        Xdkh0002DataRespDto xdkh0002DataRespDto = new Xdkh0002DataRespDto();// 响应Dto:对公客户基本信息查询
        ResultDto<Xdkh0002DataRespDto> xdkh0002DataResultDto = new ResultDto<>();
        // 从xdkh0002DataReqDto获取业务值进行业务逻辑处理
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002DataReqDto));
            xdkh0002DataRespDto = Optional.ofNullable(xdkh0002Service.selectCusIdByOrgCode(xdkh0002DataReqDto)).orElseGet(() -> {
                Xdkh0002DataRespDto temp  = new Xdkh0002DataRespDto();
                temp.setCusId(StringUtils.EMPTY);
                temp.setManagerId(StringUtils.EMPTY);
                temp.setRegiType(StringUtils.EMPTY);
                return temp;
            });
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002DataRespDto));
            // 封装xdkh0002DataResultDto中正确的返回码和返回信息
            xdkh0002DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0002DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, e.getMessage());
            // 封装xdkh0002DataResultDto中异常返回码和返回信息
            xdkh0002DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0002DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0002DataRespDto到xdkh0002DataResultDto中
        xdkh0002DataResultDto.setData(xdkh0002DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002DataResultDto));
        return xdkh0002DataResultDto;
    }
}
