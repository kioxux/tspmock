/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusManaTask;
import cn.com.yusys.yusp.service.CusManaTaskService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusManaTaskResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-04-09 19:55:14
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusmanatask")
public class CusManaTaskResource {
    @Autowired
    private CusManaTaskService cusManaTaskService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusManaTask>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusManaTask> list = cusManaTaskService.selectAll(queryModel);
        return new ResultDto<List<CusManaTask>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CusManaTask>> index(@RequestBody QueryModel queryModel) {
        List<CusManaTask> list = cusManaTaskService.selectByModel(queryModel);
        return new ResultDto<List<CusManaTask>>(list);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusManaTask> show(@PathVariable("serno") String serno) {
        CusManaTask cusManaTask = cusManaTaskService.selectByPrimaryKey(serno);
        return new ResultDto<CusManaTask>(cusManaTask);
    }

    /**
     * @函数名称:createTask
     * @函数描述:新建任务,根据客户号和业务类型校验存在性
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新建任务,根据客户号和业务类型校验存在性")
    @PostMapping("/createTask")
    protected ResultDto<CusManaTask> createTask(@RequestBody CusManaTask cusManaTask) throws Exception {
        cusManaTaskService.createTask(cusManaTask);
        return new ResultDto<CusManaTask>(cusManaTask);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CusManaTask> create(@RequestBody CusManaTask cusManaTask) throws Exception {
        cusManaTaskService.createTask(cusManaTask);
        return new ResultDto<CusManaTask>(cusManaTask);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusManaTask cusManaTask) throws URISyntaxException {
        int result = cusManaTaskService.update(cusManaTask);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CusManaTask cusManaTask) {
        int result = cusManaTaskService.updateSelective(cusManaTask);
        return ResultDto.success(result);
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusManaTaskService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusManaTaskService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:create
     * @函数描述:保存任务
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<String> save(@RequestBody CusManaTask cusManaTask) throws URISyntaxException {
        String result = cusManaTaskService.save(cusManaTask);
        return new ResultDto<String>(result);
    }

    /**
     * @函数名称:queryCount
     * @函数描述:查询任务条数
     * @参数与返回说明:
     * @算法描述:
     * @修改人: zhoumw
     */
    @PostMapping("/queryCount/{type}")
    protected ResultDto<Integer> queryCount(@PathVariable String type) throws URISyntaxException {
        int result = cusManaTaskService.selectCount(type);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCusManaTask/{serno}")
    protected ResultDto<CusManaTask> queryCusManaTask(@PathVariable String serno) {
        CusManaTask cusManaTask = cusManaTaskService.selectByPrimarySerno(serno);
        return new ResultDto<CusManaTask>(cusManaTask);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCusManaTaskList")
    protected ResultDto<List<CusManaTask>> queryCusManaTask(@RequestBody QueryModel queryModel) {
        List<CusManaTask> list = cusManaTaskService.queryCusManaTask(queryModel);
        return new ResultDto<List<CusManaTask>>(list);
    }

    /**
     * @函数名称:selectCountByBizType
     * @函数描述:根据业务类型查询任务条数
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据业务类型查询任务条数")
    @PostMapping("/selectCountByBizType")
    protected ResultDto<Integer> selectCountByBizType(@RequestBody List<String> bizTypeList) throws URISyntaxException {
        int result = cusManaTaskService.selectCountByBizType(bizTypeList);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateTaskStatusByCusId
     * @函数描述:根据客户号、业务类型更改任务状态为3 已处理
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户号、业务类型更改任务状态为3 已处理 ")
    @PostMapping("/updateTaskStatusByCusId")
    protected ResultDto<Integer> updateTaskStatusByCusId(@RequestBody CusManaTask cusManaTask){
        int result = cusManaTaskService.updateTaskStatusByCusId(cusManaTask);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    @ApiOperation("根据客户经理工号查询待处理数据")
    @PostMapping("/selectNumByInputId")
    protected ResultDto<List<Map<String, Object>>> selectNumByInputId(@RequestBody QueryModel queryModel){
        List<Map<String, Object>> list = cusManaTaskService.selectNumByInputId(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }
}
