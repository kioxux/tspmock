/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstYxdJbxxApp;
import cn.com.yusys.yusp.repository.mapper.CusLstYxdJbxxAppMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYxdJbxxAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:15:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYxdJbxxAppService {

    @Autowired
    private CusLstYxdJbxxAppMapper cusLstYxdJbxxAppMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstYxdJbxxApp selectByPrimaryKey(String serno) {
        return cusLstYxdJbxxAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYxdJbxxApp> selectAll(QueryModel model) {
        List<CusLstYxdJbxxApp> records = (List<CusLstYxdJbxxApp>) cusLstYxdJbxxAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstYxdJbxxApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYxdJbxxApp> list = cusLstYxdJbxxAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstYxdJbxxApp record) {
        return cusLstYxdJbxxAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstYxdJbxxApp record) {
        return cusLstYxdJbxxAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstYxdJbxxApp record) {
        return cusLstYxdJbxxAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstYxdJbxxApp record) {
        return cusLstYxdJbxxAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstYxdJbxxAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYxdJbxxAppMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    public int tempSave(CusLstYxdJbxxApp cusLstYxdJbxxApp) {
        String serno = cusLstYxdJbxxApp.getSerno();
        //查询该对象是否存在
        CusLstYxdJbxxApp queryCusLstYxdJbxxApp = cusLstYxdJbxxAppMapper.selectByPrimaryKey(serno);
        if (null == queryCusLstYxdJbxxApp){
            return cusLstYxdJbxxAppMapper.insert(cusLstYxdJbxxApp);
        }else{
            return cusLstYxdJbxxAppMapper.updateByPrimaryKey(cusLstYxdJbxxApp);
        }
    }
}
