package cn.com.yusys.yusp.service.server.cmiscus0001;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0001.req.CmisCus0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001GrpMemberListRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusGrpMemberRelService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 业务逻辑处理类：集团成员列表查询
 *
 * @author dumd
 * @version 1.0
 * @since 2021年5月25日
 */
@Service
public class CmisCus0001Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0001Service.class);

    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;

    @Transactional
    public CmisCus0001RespDto execute(CmisCus0001ReqDto reqDto) throws YuspException {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0001.key, DscmsEnum.TRADE_CODE_CMISCUS0001.value);

        CmisCus0001RespDto respDto = new CmisCus0001RespDto();
        List<CmisCus0001GrpMemberListRespDto> grpMemberList = new ArrayList<>();
        //集团编号
        String grpNo = reqDto.getGrpNo();
        //成员客户编号
        String cusId = reqDto.getCusId();
        //集团名称
        String grpName = "";

        try{
            //如果集团编号和成员客户编号均为空，则抛异常
            if (StringUtils.isEmpty(grpNo) && StringUtils.isEmpty(cusId)){
                throw new YuspException(EcsEnum.ECS040011.key, EcsEnum.ECS040011.value);
            }

            //成员客户编号不为空，则根据成员客户编号和集团编号查询集团编号
            if (StringUtils.isNotEmpty(cusId)){
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("grpNo",grpNo);
                queryModel.addCondition("cusId",cusId);

                Map<String, String> grpNoMap = cusGrpMemberRelService.queryGrpNoByQueryModel(queryModel);

                //查询不到集团编号，则抛异常
                if (grpNoMap==null){
                    throw new YuspException(EcsEnum.ECS040012.key, EcsEnum.ECS040012.value);
                }
                //集团编号
                grpNo = grpNoMap.get("grpNo");
            }

            //根据集团编号查询成员信息列表
            List<Map<String, String>> grpInfoList = cusGrpMemberRelService.queryGrpMemberListByGrpNo(grpNo);

            if (grpInfoList!=null && grpInfoList.size()>0){

                //集团名称
                grpName = grpInfoList.get(0).get("grpName");

                //遍历集团成员信息
                for (Map<String, String> grpInfo : grpInfoList) {
                    //新建CmisCus0001GrpMemberListRespDto对象
                    CmisCus0001GrpMemberListRespDto grpMemberDto = new CmisCus0001GrpMemberListRespDto();
                    //成员客户编号
                    grpMemberDto.setCusId(grpInfo.get("cusId"));
                    //成员客户名称
                    grpMemberDto.setCusName(grpInfo.get("cusName"));
                    //成员客户大类
                    grpMemberDto.setCusCatalog(grpInfo.get("cusCatalog"));
                    //成员客户类型
                    grpMemberDto.setCusType(grpInfo.get("cusType"));
                    //成员管护客户经理ID
                    grpMemberDto.setManagerId(grpInfo.get("managerId"));
                    //成员管护机构
                    grpMemberDto.setManagerBrId(grpInfo.get("managerBrId"));
                    //集团关联关系类型
                    grpMemberDto.setGrpCorreType(grpInfo.get("grpCorreType"));
                    //放入grpMemberList里
                    grpMemberList.add(grpMemberDto);
                }
            }

            respDto.setGrpNo(grpNo);
            respDto.setGrpName(grpName);
            respDto.setGrpMemberList(grpMemberList);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsEnum.TRADE_CODE_CMISCUS0001.key+"报错：",e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0001.key, DscmsEnum.TRADE_CODE_CMISCUS0001.value);
        return respDto;
    }
}