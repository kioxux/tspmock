/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusSnqyqd
 * @类描述: cus_snqyqd数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-21 23:55:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_snqyqd")
public class CusSnqyqd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 行政区划代码 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "AREA_CODE")
	private String areaCode;
	
	/** 行政区划名称 **/
	@Column(name = "ADDRESS", unique = false, nullable = false, length = 80)
	private String address;
	
	/** 是否农户标识 **/
	@Column(name = "IS_SN", unique = false, nullable = true, length = 5)
	private String isSn;
	
	
	/**
	 * @param areaCode
	 */
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
    /**
     * @return areaCode
     */
	public String getAreaCode() {
		return this.areaCode;
	}
	
	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
    /**
     * @return address
     */
	public String getAddress() {
		return this.address;
	}
	
	/**
	 * @param isSn
	 */
	public void setIsSn(String isSn) {
		this.isSn = isSn;
	}
	
    /**
     * @return isSn
     */
	public String getIsSn() {
		return this.isSn;
	}


}