package cn.com.yusys.yusp.web.server.cmiscus0022;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import cn.com.yusys.yusp.dto.server.cmiscus0022.req.CmisCus0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0022.resp.CmisCus0022RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusLstYndJyxxAppService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 接口处理类: 优农贷（经营信息）记录生成
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0022:优农贷（经营信息）查询")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0022Resource.class);

    @Autowired
    private CusLstYndJyxxAppService cusLstYndJyxxAppService;

    /**
     * 交易码：cmiscus0022
     * 交易描述：优农贷（经营信息）查询
     *
     * @param cmisCus0022ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷（经营信息）查询")
    @PostMapping("/cmiscus0022")
    protected @ResponseBody
    ResultDto<CmisCus0022RespDto> cmiscus0022(@Validated @RequestBody CmisCus0022ReqDto cmisCus0022ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0022.key, DscmsEnum.TRADE_CODE_CMISCUS0022.value, JSON.toJSONString(cmisCus0022ReqDto));
        CmisCus0022RespDto cmisCus0022RespDto = new CmisCus0022RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0022RespDto> cmisCus0022ResultDto = new ResultDto<>();
        try {
            CusLstYndJyxxApp cusLstYndJyxxApp = new CusLstYndJyxxApp();
            // 查询条件
            String serno = cmisCus0022ReqDto.getSerno();
            QueryModel model = new QueryModel();
            model.addCondition("serno", serno);
            List<CusLstYndJyxxApp> list = cusLstYndJyxxAppService.selectByModel(model);
            if (list.size() > 0) {//存在记录
                cusLstYndJyxxApp = list.get(0);
                BeanUtils.copyProperties(cusLstYndJyxxApp, cmisCus0022RespDto); // 返回参数复制
            }
            cmisCus0022ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0022ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0022.key, DscmsEnum.TRADE_CODE_CMISCUS0022.value, e.getMessage());
            // 封装cmisCus0022ResultDto中异常返回码和返回信息
            cmisCus0022ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0022ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0022RespDto到cmisCus0022ResultDto中
        cmisCus0022ResultDto.setData(cmisCus0022RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0022.key, DscmsEnum.TRADE_CODE_CMISCUS0022.value, JSON.toJSONString(cmisCus0022ResultDto));
        return cmisCus0022ResultDto;
    }
}
