/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIntbank
 * @类描述: cus_intbank数据实体类
 * @功能描述:
 * @创建人: 乐友先生
 * @创建时间: 2021-04-08 11:10:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_intbank")
public class CusIntbank extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 客户编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CUS_ID")
	private String cusId;

	/** 同业机构(行)名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 同业机构(行)英文名称 **/
	@Column(name = "CUS_EN_NAME", unique = false, nullable = true, length = 80)
	private String cusEnName;

	/** 同业机构类型 STD_ZB_INTB_TYPE **/
	@Column(name = "INTBANK_TYPE", unique = false, nullable = true, length = 5)
	private String intbankType;

	/** 同业机构子类型 STD_ZB_INTB_SUB_TYPE **/
	@Column(name = "INTBANK_SUB_TYPE", unique = false, nullable = true, length = 5)
	private String intbankSubType;

	/** 大额行号 **/
	@Column(name = "LARGE_BANK_NO", unique = false, nullable = true, length = 30)
	private String largeBankNo;

	/** Swift代码 **/
	@Column(name = "SWIFT_CDE", unique = false, nullable = true, length = 12)
	private String swiftCde;

	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;

	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;

	/** 国民经济部门编号 **/
	@Column(name = "NAT_ECO_SEC", unique = false, nullable = true, length = 5)
	private String natEcoSec;

	/** 控股类型 STD_ZB_HOLD_TYPE **/
	@Column(name = "HOLD_TYPE", unique = false, nullable = true, length = 5)
	private String holdType;

	/** 客户规模 **/
	@Column(name = "CUS_SCALE", unique = false, nullable = true, length = 5)
	private String cusScale;

	/** 金融业企业分类 **/
	@Column(name = "FNA_CLL_TYP", unique = false, nullable = true, length = 5)
	private String fnaCllTyp;

	/** 中证码/贷款卡号 **/
	@Column(name = "LOAN_CARD_ID", unique = false, nullable = true, length = 16)
	private String loanCardId;

	/** 总行组织机构代码 **/
	@Column(name = "HEAD_INS_CDE", unique = false, nullable = true, length = 10)
	private String headInsCde;

	/** 国别 **/
	@Column(name = "COUNTRY", unique = false, nullable = true, length = 5)
	private String country;

	/** 法人姓名 **/
	@Column(name = "LEGAL_NAME", unique = false, nullable = true, length = 80)
	private String legalName;

	/** 法人证件类型 STD_ZB_CERT_TYP **/
	@Column(name = "LEGAL_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String legalCertType;

	/** 法人证件号码 **/
	@Column(name = "LEGAL_CERT_CODE", unique = false, nullable = true, length = 40)
	private String legalCertCode;

	/** 法人性别 STD_ZB_SEX **/
	@Column(name = "LEGAL_SEX", unique = false, nullable = true, length = 5)
	private String legalSex;

	/** 法人出生日期 **/
	@Column(name = "LEGAL_BIRTH", unique = false, nullable = true, length = 10)
	private String legalBirth;

	/** 税务登记证号（国税） **/
	@Column(name = "NAT_TAX_REG_CDE", unique = false, nullable = true, length = 20)
	private String natTaxRegCde;

	/** 税务登记证号（地税） **/
	@Column(name = "LOC_TAX_REG_CDE", unique = false, nullable = true, length = 20)
	private String locTaxRegCde;

	/** 营业执照号码 **/
	@Column(name = "REG_CDE", unique = false, nullable = true, length = 30)
	private String regCde;

	/** 总资产(万元) **/
	@Column(name = "RESSET_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ressetAmt;

	/** 注册/开办资金币种 STD_ZB_CUR_TYP **/
	@Column(name = "REG_CAP_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String regCapCurType;

	/** 注册/开办资金 **/
	@Column(name = "REG_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regCapAmt;

	/** 实收资本金额 **/
	@Column(name = "PAID_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCapAmt;

	/** 所有者权益(万元) **/
	@Column(name = "OWERS_EQUITY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal owersEquity;

	/** 基本账户开户行 **/
	@Column(name = "BAS_ACC_BANK", unique = false, nullable = true, length = 80)
	private String basAccBank;

	/** 基本账户账号 **/
	@Column(name = "BAS_ACC_NO", unique = false, nullable = true, length = 32)
	private String basAccNo;

	/** 是否上市/上市公司标志 STD_ZB_YES_NO **/
	@Column(name = "MRK_FLG", unique = false, nullable = true, length = 5)
	private String mrkFlg;

	/** 上市地1 STD_ZB_MRK_AREA **/
	@Column(name = "MRK_AREA_ONE", unique = false, nullable = true, length = 5)
	private String mrkAreaOne;

	/** 上市地2 STD_ZB_MRK_AREA **/
	@Column(name = "MRK_AREA_TWO", unique = false, nullable = true, length = 5)
	private String mrkAreaTwo;

	/** 股票代码 **/
	@Column(name = "STK_CODE", unique = false, nullable = true, length = 10)
	private String stkCode;

	/** 同业机构(行)成立日 **/
	@Column(name = "INTBANK_BUILD_DATE", unique = false, nullable = true, length = 10)
	private String intbankBuildDate;

	/** 金融业务许可证 **/
	@Column(name = "BANK_PRO_LIC", unique = false, nullable = true, length = 80)
	private String bankProLic;

	/** 同业机构行网址 **/
	@Column(name = "INTBANK_SITE", unique = false, nullable = true, length = 75)
	private String intbankSite;

	/** 通讯地址省市区/县 **/
	@Column(name = "CONT_AREA", unique = false, nullable = true, length = 20)
	private String contArea;

	/** 通讯地址村 **/
	@Column(name = "CONT_COUNTRY", unique = false, nullable = true, length = 20)
	private String contCountry;

	/** 通讯地址乡/镇 **/
	@Column(name = "CONT_TOWN", unique = false, nullable = true, length = 20)
	private String contTown;

	/** 通讯地址 **/
	@Column(name = "CONT_ADDR", unique = false, nullable = true, length = 150)
	private String contAddr;

	/** 邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 80)
	private String email;

	/** 监管评级 STD_ZB_SUPE_EVAL **/
	@Column(name = "SUPE_EVAL", unique = false, nullable = true, length = 5)
	private String supeEval;

	/** 外部评级 STD_ZB_EVAL_RST **/
	@Column(name = "INTBANK_EVAL", unique = false, nullable = true, length = 5)
	private String intbankEval;

	/** 评级到期日期 **/
	@Column(name = "EVAL_END_DT", unique = false, nullable = true, length = 10)
	private String evalEndDt;

	/** 拥有资质名称 **/
	@Column(name = "OWM_APT_NAME", unique = false, nullable = true, length = 100)
	private String owmAptName;

	/** 与我行合作关系 **/
	@Column(name = "REL_DRG", unique = false, nullable = true, length = 5)
	private String relDrg;

	/** 客户状态 STD_ZB_CUS_ST **/
	@Column(name = "CUS_STATE", unique = false, nullable = true, length = 5)
	private String cusState;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 社会信用代码 **/
	@Column(name = "SOCIAL_CREDIT_CODE", unique = false, nullable = true, length = 100)
	private String socialCreditCode;

	/** 机构简称 **/
	@Column(name = "ORG_FOR_SHORT", unique = false, nullable = true, length = 50)
	private String orgForShort;

	/** 评定机构外部STD_ZB_OUT_APPR_ORG   **/
	@Column(name = "OUT_APPR_ORG", unique = false, nullable = true, length = 5)
	private String outApprOrg;

	/** 同业非现场监管统计机构编码 **/
	@Column(name = "INTBANK_OSSS_ORG_CODE", unique = false, nullable = true, length = 100)
	private String intbankOsssOrgCode;

	/** 是否长期STD_ZB_YES_NO **/
	@Column(name = "IS_LT", unique = false, nullable = true, length = 5)
	private String isLt;

	/** 经营地区 **/
	@Column(name = "OPER_AREA", unique = false, nullable = true, length = 150)
	private String operArea;

	/** 机构类型说明 **/
	@Column(name = "ORG_TYPE_MEMO", unique = false, nullable = true, length = 150)
	private String orgTypeMemo;

	/** 管户客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 行业分类 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = true, length = 5)
	private String tradeClass;

	/** 营业执照到期日 **/
	@Column(name = "EXP_DATE_END", unique = false, nullable = true, length = 20)
	private String expDateEnd;

	/** 注册地行政区划 **/
	@Column(name = "REGI_AREA_CODE", unique = false, nullable = true, length = 12)
	private String regiAreaCode;

	/** 注册地址 **/
	@Column(name = "COM_REG_ADD", unique = false, nullable = true, length = 100)
	private String comRegAdd;

	/** 经营范围 **/
	@Column(name = "NAT_BUSI", unique = false, nullable = true, length = 40)
	private String natBusi;

	/** 主联系人电话 **/
	@Column(name = "LINKMAN_PHONE", unique = false, nullable = true, length = 20)
	private String linkmanPhone;

	/** 锁定状态 **/
	@Column(name = "ISLOCKED", unique = false, nullable = true, length = 5)
	private String islocked;

	/** 同业授信等级 **/
	@Column(name = "INTBANK_LEVEL", unique = false, nullable = true, length = 10)
	private String intbankLevel;

	/** 违约概率 **/
	@Column(name = "PD", unique = false, nullable = true, length = 10)
	private String pd;


	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param cusEnName
	 */
	public void setCusEnName(String cusEnName) {
		this.cusEnName = cusEnName;
	}

	/**
	 * @return cusEnName
	 */
	public String getCusEnName() {
		return this.cusEnName;
	}

	/**
	 * @param intbankType
	 */
	public void setIntbankType(String intbankType) {
		this.intbankType = intbankType;
	}

	/**
	 * @return intbankType
	 */
	public String getIntbankType() {
		return this.intbankType;
	}

	/**
	 * @param intbankSubType
	 */
	public void setIntbankSubType(String intbankSubType) {
		this.intbankSubType = intbankSubType;
	}

	/**
	 * @return intbankSubType
	 */
	public String getIntbankSubType() {
		return this.intbankSubType;
	}

	/**
	 * @param largeBankNo
	 */
	public void setLargeBankNo(String largeBankNo) {
		this.largeBankNo = largeBankNo;
	}

	/**
	 * @return largeBankNo
	 */
	public String getLargeBankNo() {
		return this.largeBankNo;
	}

	/**
	 * @param swiftCde
	 */
	public void setSwiftCde(String swiftCde) {
		this.swiftCde = swiftCde;
	}

	/**
	 * @return swiftCde
	 */
	public String getSwiftCde() {
		return this.swiftCde;
	}

	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}

	/**
	 * @return certType
	 */
	public String getCertType() {
		return this.certType;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	/**
	 * @return certCode
	 */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param natEcoSec
	 */
	public void setNatEcoSec(String natEcoSec) {
		this.natEcoSec = natEcoSec;
	}

	/**
	 * @return natEcoSec
	 */
	public String getNatEcoSec() {
		return this.natEcoSec;
	}

	/**
	 * @param holdType
	 */
	public void setHoldType(String holdType) {
		this.holdType = holdType;
	}

	/**
	 * @return holdType
	 */
	public String getHoldType() {
		return this.holdType;
	}

	/**
	 * @param cusScale
	 */
	public void setCusScale(String cusScale) {
		this.cusScale = cusScale;
	}

	/**
	 * @return cusScale
	 */
	public String getCusScale() {
		return this.cusScale;
	}

	/**
	 * @param fnaCllTyp
	 */
	public void setFnaCllTyp(String fnaCllTyp) {
		this.fnaCllTyp = fnaCllTyp;
	}

	/**
	 * @return fnaCllTyp
	 */
	public String getFnaCllTyp() {
		return this.fnaCllTyp;
	}

	/**
	 * @param loanCardId
	 */
	public void setLoanCardId(String loanCardId) {
		this.loanCardId = loanCardId;
	}

	/**
	 * @return loanCardId
	 */
	public String getLoanCardId() {
		return this.loanCardId;
	}

	/**
	 * @param headInsCde
	 */
	public void setHeadInsCde(String headInsCde) {
		this.headInsCde = headInsCde;
	}

	/**
	 * @return headInsCde
	 */
	public String getHeadInsCde() {
		return this.headInsCde;
	}

	/**
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return country
	 */
	public String getCountry() {
		return this.country;
	}

	/**
	 * @param legalName
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}

	/**
	 * @return legalName
	 */
	public String getLegalName() {
		return this.legalName;
	}

	/**
	 * @param legalCertType
	 */
	public void setLegalCertType(String legalCertType) {
		this.legalCertType = legalCertType;
	}

	/**
	 * @return legalCertType
	 */
	public String getLegalCertType() {
		return this.legalCertType;
	}

	/**
	 * @param legalCertCode
	 */
	public void setLegalCertCode(String legalCertCode) {
		this.legalCertCode = legalCertCode;
	}

	/**
	 * @return legalCertCode
	 */
	public String getLegalCertCode() {
		return this.legalCertCode;
	}

	/**
	 * @param legalSex
	 */
	public void setLegalSex(String legalSex) {
		this.legalSex = legalSex;
	}

	/**
	 * @return legalSex
	 */
	public String getLegalSex() {
		return this.legalSex;
	}

	/**
	 * @param legalBirth
	 */
	public void setLegalBirth(String legalBirth) {
		this.legalBirth = legalBirth;
	}

	/**
	 * @return legalBirth
	 */
	public String getLegalBirth() {
		return this.legalBirth;
	}

	/**
	 * @param natTaxRegCde
	 */
	public void setNatTaxRegCde(String natTaxRegCde) {
		this.natTaxRegCde = natTaxRegCde;
	}

	/**
	 * @return natTaxRegCde
	 */
	public String getNatTaxRegCde() {
		return this.natTaxRegCde;
	}

	/**
	 * @param locTaxRegCde
	 */
	public void setLocTaxRegCde(String locTaxRegCde) {
		this.locTaxRegCde = locTaxRegCde;
	}

	/**
	 * @return locTaxRegCde
	 */
	public String getLocTaxRegCde() {
		return this.locTaxRegCde;
	}

	/**
	 * @param regCde
	 */
	public void setRegCde(String regCde) {
		this.regCde = regCde;
	}

	/**
	 * @return regCde
	 */
	public String getRegCde() {
		return this.regCde;
	}

	/**
	 * @param ressetAmt
	 */
	public void setRessetAmt(java.math.BigDecimal ressetAmt) {
		this.ressetAmt = ressetAmt;
	}

	/**
	 * @return ressetAmt
	 */
	public java.math.BigDecimal getRessetAmt() {
		return this.ressetAmt;
	}

	/**
	 * @param regCapCurType
	 */
	public void setRegCapCurType(String regCapCurType) {
		this.regCapCurType = regCapCurType;
	}

	/**
	 * @return regCapCurType
	 */
	public String getRegCapCurType() {
		return this.regCapCurType;
	}

	/**
	 * @param regCapAmt
	 */
	public void setRegCapAmt(java.math.BigDecimal regCapAmt) {
		this.regCapAmt = regCapAmt;
	}

	/**
	 * @return regCapAmt
	 */
	public java.math.BigDecimal getRegCapAmt() {
		return this.regCapAmt;
	}

	/**
	 * @param paidCapAmt
	 */
	public void setPaidCapAmt(java.math.BigDecimal paidCapAmt) {
		this.paidCapAmt = paidCapAmt;
	}

	/**
	 * @return paidCapAmt
	 */
	public java.math.BigDecimal getPaidCapAmt() {
		return this.paidCapAmt;
	}

	/**
	 * @param owersEquity
	 */
	public void setOwersEquity(java.math.BigDecimal owersEquity) {
		this.owersEquity = owersEquity;
	}

	/**
	 * @return owersEquity
	 */
	public java.math.BigDecimal getOwersEquity() {
		return this.owersEquity;
	}

	/**
	 * @param basAccBank
	 */
	public void setBasAccBank(String basAccBank) {
		this.basAccBank = basAccBank;
	}

	/**
	 * @return basAccBank
	 */
	public String getBasAccBank() {
		return this.basAccBank;
	}

	/**
	 * @param basAccNo
	 */
	public void setBasAccNo(String basAccNo) {
		this.basAccNo = basAccNo;
	}

	/**
	 * @return basAccNo
	 */
	public String getBasAccNo() {
		return this.basAccNo;
	}

	/**
	 * @param mrkFlg
	 */
	public void setMrkFlg(String mrkFlg) {
		this.mrkFlg = mrkFlg;
	}

	/**
	 * @return mrkFlg
	 */
	public String getMrkFlg() {
		return this.mrkFlg;
	}

	/**
	 * @param mrkAreaOne
	 */
	public void setMrkAreaOne(String mrkAreaOne) {
		this.mrkAreaOne = mrkAreaOne;
	}

	/**
	 * @return mrkAreaOne
	 */
	public String getMrkAreaOne() {
		return this.mrkAreaOne;
	}

	/**
	 * @param mrkAreaTwo
	 */
	public void setMrkAreaTwo(String mrkAreaTwo) {
		this.mrkAreaTwo = mrkAreaTwo;
	}

	/**
	 * @return mrkAreaTwo
	 */
	public String getMrkAreaTwo() {
		return this.mrkAreaTwo;
	}

	/**
	 * @param stkCode
	 */
	public void setStkCode(String stkCode) {
		this.stkCode = stkCode;
	}

	/**
	 * @return stkCode
	 */
	public String getStkCode() {
		return this.stkCode;
	}

	/**
	 * @param intbankBuildDate
	 */
	public void setIntbankBuildDate(String intbankBuildDate) {
		this.intbankBuildDate = intbankBuildDate;
	}

	/**
	 * @return intbankBuildDate
	 */
	public String getIntbankBuildDate() {
		return this.intbankBuildDate;
	}

	/**
	 * @param bankProLic
	 */
	public void setBankProLic(String bankProLic) {
		this.bankProLic = bankProLic;
	}

	/**
	 * @return bankProLic
	 */
	public String getBankProLic() {
		return this.bankProLic;
	}

	/**
	 * @param intbankSite
	 */
	public void setIntbankSite(String intbankSite) {
		this.intbankSite = intbankSite;
	}

	/**
	 * @return intbankSite
	 */
	public String getIntbankSite() {
		return this.intbankSite;
	}

	/**
	 * @param contArea
	 */
	public void setContArea(String contArea) {
		this.contArea = contArea;
	}

	/**
	 * @return contArea
	 */
	public String getContArea() {
		return this.contArea;
	}

	/**
	 * @param contCountry
	 */
	public void setContCountry(String contCountry) {
		this.contCountry = contCountry;
	}

	/**
	 * @return contCountry
	 */
	public String getContCountry() {
		return this.contCountry;
	}

	/**
	 * @param contTown
	 */
	public void setContTown(String contTown) {
		this.contTown = contTown;
	}

	/**
	 * @return contTown
	 */
	public String getContTown() {
		return this.contTown;
	}

	/**
	 * @param contAddr
	 */
	public void setContAddr(String contAddr) {
		this.contAddr = contAddr;
	}

	/**
	 * @return contAddr
	 */
	public String getContAddr() {
		return this.contAddr;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @param supeEval
	 */
	public void setSupeEval(String supeEval) {
		this.supeEval = supeEval;
	}

	/**
	 * @return supeEval
	 */
	public String getSupeEval() {
		return this.supeEval;
	}

	/**
	 * @param intbankEval
	 */
	public void setIntbankEval(String intbankEval) {
		this.intbankEval = intbankEval;
	}

	/**
	 * @return intbankEval
	 */
	public String getIntbankEval() {
		return this.intbankEval;
	}

	/**
	 * @param evalEndDt
	 */
	public void setEvalEndDt(String evalEndDt) {
		this.evalEndDt = evalEndDt;
	}

	/**
	 * @return evalEndDt
	 */
	public String getEvalEndDt() {
		return this.evalEndDt;
	}

	/**
	 * @param owmAptName
	 */
	public void setOwmAptName(String owmAptName) {
		this.owmAptName = owmAptName;
	}

	/**
	 * @return owmAptName
	 */
	public String getOwmAptName() {
		return this.owmAptName;
	}

	/**
	 * @param relDrg
	 */
	public void setRelDrg(String relDrg) {
		this.relDrg = relDrg;
	}

	/**
	 * @return relDrg
	 */
	public String getRelDrg() {
		return this.relDrg;
	}

	/**
	 * @param cusState
	 */
	public void setCusState(String cusState) {
		this.cusState = cusState;
	}

	/**
	 * @return cusState
	 */
	public String getCusState() {
		return this.cusState;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param socialCreditCode
	 */
	public void setSocialCreditCode(String socialCreditCode) {
		this.socialCreditCode = socialCreditCode;
	}

	/**
	 * @return socialCreditCode
	 */
	public String getSocialCreditCode() {
		return this.socialCreditCode;
	}

	/**
	 * @param orgForShort
	 */
	public void setOrgForShort(String orgForShort) {
		this.orgForShort = orgForShort;
	}

	/**
	 * @return orgForShort
	 */
	public String getOrgForShort() {
		return this.orgForShort;
	}

	/**
	 * @param outApprOrg
	 */
	public void setOutApprOrg(String outApprOrg) {
		this.outApprOrg = outApprOrg;
	}

	/**
	 * @return outApprOrg
	 */
	public String getOutApprOrg() {
		return this.outApprOrg;
	}

	/**
	 * @param intbankOsssOrgCode
	 */
	public void setIntbankOsssOrgCode(String intbankOsssOrgCode) {
		this.intbankOsssOrgCode = intbankOsssOrgCode;
	}

	/**
	 * @return intbankOsssOrgCode
	 */
	public String getIntbankOsssOrgCode() {
		return this.intbankOsssOrgCode;
	}

	/**
	 * @param isLt
	 */
	public void setIsLt(String isLt) {
		this.isLt = isLt;
	}

	/**
	 * @return isLt
	 */
	public String getIsLt() {
		return this.isLt;
	}

	/**
	 * @param operArea
	 */
	public void setOperArea(String operArea) {
		this.operArea = operArea;
	}

	/**
	 * @return operArea
	 */
	public String getOperArea() {
		return this.operArea;
	}

	/**
	 * @param orgTypeMemo
	 */
	public void setOrgTypeMemo(String orgTypeMemo) {
		this.orgTypeMemo = orgTypeMemo;
	}

	/**
	 * @return orgTypeMemo
	 */
	public String getOrgTypeMemo() {
		return this.orgTypeMemo;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param expDateEnd
	 */
	public void setExpDateEnd(String expDateEnd) {
		this.expDateEnd = expDateEnd;
	}

	/**
	 * @return expDateEnd
	 */
	public String getExpDateEnd() {
		return this.expDateEnd;
	}

	/**
	 * @param regiAreaCode
	 */
	public void setRegiAreaCode(String regiAreaCode) {
		this.regiAreaCode = regiAreaCode;
	}

	/**
	 * @return regiAreaCode
	 */
	public String getRegiAreaCode() {
		return this.regiAreaCode;
	}

	/**
	 * @param comRegAdd
	 */
	public void setComRegAdd(String comRegAdd) {
		this.comRegAdd = comRegAdd;
	}

	/**
	 * @return comRegAdd
	 */
	public String getComRegAdd() {
		return this.comRegAdd;
	}

	/**
	 * @param natBusi
	 */
	public void setNatBusi(String natBusi) {
		this.natBusi = natBusi;
	}

	/**
	 * @return natBusi
	 */
	public String getNatBusi() {
		return this.natBusi;
	}

	/**
	 * @param intbankLevel
	 */
	public void setIntbankLevel(String intbankLevel) {
		this.intbankLevel = intbankLevel;
	}

	/**
	 * @return intbankLevel
	 */
	public String getIntbankLevel() {
		return this.intbankLevel;
	}

	/**
	 * @param pd
	 */
	public void setPd(String pd) {
		this.pd = pd;
	}

	/**
	 * @return pd
	 */
	public String getPd() {
		return this.pd;
	}


	public String getTradeClass() {
		return tradeClass;
	}

	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}

	public String getLinkmanPhone() {
		return linkmanPhone;
	}

	public void setLinkmanPhone(String linkmanPhone) {
		this.linkmanPhone = linkmanPhone;
	}

	public String getIslocked() {
		return islocked;
	}

	public void setIslocked(String islocked) {
		this.islocked = islocked;
	}
}
