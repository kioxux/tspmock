/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CusLstWjkhMxxx;
import cn.com.yusys.yusp.domain.CusLstWjkhWhite;
import cn.com.yusys.yusp.repository.mapper.CusLstWjkhMxxxMapper;
import cn.com.yusys.yusp.repository.mapper.CusLstWjkhWhiteMapper;
import cn.com.yusys.yusp.vo.CusLstWjkhMxxxExportVo;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWjkhMxxxService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-05-05 09:06:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstWjkhMxxxService {

    @Autowired
    private CusLstWjkhMxxxMapper cusLstWjkhMxxxMapper;
    @Autowired
    private CusLstWjkhWhiteMapper cusLstWjkhWhiteMapper;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstWjkhMxxx selectByPrimaryKey(String pkId) {
        return cusLstWjkhMxxxMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstWjkhMxxx> selectAll(QueryModel model) {
        List<CusLstWjkhMxxx> records = (List<CusLstWjkhMxxx>) cusLstWjkhMxxxMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstWjkhMxxx> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstWjkhMxxx> list = cusLstWjkhMxxxMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstWjkhMxxx record) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",record.getCusId());
        List<CusLstWjkhWhite>  listCoopPlanApp = cusLstWjkhWhiteMapper.selectByModel(queryModel);

        if(!CollectionUtils.isEmpty(listCoopPlanApp)){
            if("02".equals(record.getBusiType()) && !"01".equals(listCoopPlanApp.get(0).getListStatus())){
                throw BizException.error(null, null,"失效的网金白名单无法进行停用申请！");
            }
            if("01".equals(record.getBusiType()) && "01".equals(listCoopPlanApp.get(0).getListStatus())){
                throw BizException.error(null, null,"生效的网金白名单无法进行准入申请！");
            }
            //throw BizException.error(null, null,"已经存在的网金白名单无法进行申请！");
        }
        return cusLstWjkhMxxxMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstWjkhMxxx record) {
        return cusLstWjkhMxxxMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstWjkhMxxx record) {
        return cusLstWjkhMxxxMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstWjkhMxxx record) {
        return cusLstWjkhMxxxMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusLstWjkhMxxxMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstWjkhMxxxMapper.deleteByIds(ids);
    }

    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusLstWjkhMxxxExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param perCustInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<CusLstWjkhMxxxExportVo> perCustInfoList) {
        List<CusLstWjkhMxxx> cusLstWjkhMxxxList = (List<CusLstWjkhMxxx>) BeanUtils.beansCopy(perCustInfoList, CusLstWjkhMxxx.class);
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CusLstWjkhMxxxMapper cusLstWjkhMxxxMapper = sqlSession.getMapper(CusLstWjkhMxxxMapper.class);
            for (CusLstWjkhMxxx cusLstWjkhMxxx : cusLstWjkhMxxxList) {
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                cusLstWjkhMxxx.setPkId(UUID.randomUUID().toString());
                cusLstWjkhMxxx.setInputBrId(userInfo.getOrg().getCode());
                cusLstWjkhMxxx.setInputId(userInfo.getLoginCode());
                cusLstWjkhMxxx.setInputDate(date);
                cusLstWjkhMxxx.setCreateTime(date1);
                cusLstWjkhMxxx.setUpdateTime(date1);
                cusLstWjkhMxxx.setUpdId(userInfo.getLoginCode());
                cusLstWjkhMxxx.setUpdBrId(userInfo.getOrg().getCode());
                cusLstWjkhMxxxMapper.insertSelective(cusLstWjkhMxxx);
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perCustInfoList.size();
    }
}
