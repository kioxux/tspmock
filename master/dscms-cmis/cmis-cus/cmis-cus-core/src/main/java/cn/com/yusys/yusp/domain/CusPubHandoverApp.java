/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusPubHandoverApp
 * @类描述: cus_pub_handover_app数据实体类
 * @功能描述:
 * @创建人: pc
 * @创建时间: 2021-05-10 14:12:48
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_pub_handover_app")
public class CusPubHandoverApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 申请流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "SERNO")
    private String serno;

    /**
     * 接收机构与移出机构关系 STD_ZB_ORG_TYP. 10-机构内
     * 20-跨机构
     **/
    @Column(name = "ORG_TYPE", unique = false, nullable = true, length = 5)
    private String orgType;

    /**
     * 移交范围 STD_ZB_HAND_SCOP. 1-单个或多个客户
     * 2-按客户经理所有客户
     **/
    @Column(name = "HANDOVER_SCOPE", unique = false, nullable = true, length = 5)
    private String handoverScope;

    /**
     * 移交方式 STD_ZB_HAND_TYP. 1-仅客户资料移交
     * 2-客户与业务移交
     * 3-客户与业务移交-机构撤并
     **/
    @Column(name = "HANDOVER_MODE", unique = false, nullable = true, length = 5)
    private String handoverMode;

    /**
     * 移出人
     **/
    @Column(name = "HANDOVER_ID", unique = false, nullable = true, length = 20)
    private String handoverId;

    /**
     * 移出机构
     **/
    @Column(name = "HANDOVER_BR_ID", unique = false, nullable = true, length = 20)
    private String handoverBrId;

    /**
     * 接收人
     **/
    @Column(name = "RECEIVER_ID", unique = false, nullable = true, length = 20)
    private String receiverId;

    /**
     * 接收机构
     **/
    @Column(name = "RECEIVER_BR_ID", unique = false, nullable = true, length = 20)
    private String receiverBrId;

    /**
     * 移交说明
     **/
    @Column(name = "HANDOVER_DETAIL", unique = false, nullable = true, length = 250)
    private String handoverDetail;

    /**
     * 主办人
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 主办机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 接收日期
     **/
    @Column(name = "RECEIVE_DATE", unique = false, nullable = true, length = 10)
    private String receiveDate;

    /**
     * 操作类型  STD_ZB_OPR_TYP操作类型  STD_ZB_OPR_TYPE
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 申请日期
     **/
    @Column(name = "APP_DATE", unique = false, nullable = true, length = 20)
    private String appDate;

    /**
     * 移交理由
     **/
    @Column(name = "HANDOVER_RESN", unique = false, nullable = true, length = 2000)
    private String handoverResn;

    /**
     * 申请状态 STD_ZB_APP_ST
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最近修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最近修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    @Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;
    /**
     * 发起类型
     **/
    @Column(name = "LAUNCH_TYPE", unique = false, nullable = true, length = 5)
    private String launchType;


    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param orgType
     */
    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    /**
     * @return orgType
     */
    public String getOrgType() {
        return this.orgType;
    }

    /**
     * @param handoverScope
     */
    public void setHandoverScope(String handoverScope) {
        this.handoverScope = handoverScope;
    }

    /**
     * @return handoverScope
     */
    public String getHandoverScope() {
        return this.handoverScope;
    }

    /**
     * @param handoverMode
     */
    public void setHandoverMode(String handoverMode) {
        this.handoverMode = handoverMode;
    }

    /**
     * @return handoverMode
     */
    public String getHandoverMode() {
        return this.handoverMode;
    }

    /**
     * @param handoverId
     */
    public void setHandoverId(String handoverId) {
        this.handoverId = handoverId;
    }

    /**
     * @return handoverId
     */
    public String getHandoverId() {
        return this.handoverId;
    }

    /**
     * @param handoverBrId
     */
    public void setHandoverBrId(String handoverBrId) {
        this.handoverBrId = handoverBrId;
    }

    /**
     * @return handoverBrId
     */
    public String getHandoverBrId() {
        return this.handoverBrId;
    }

    /**
     * @param receiverId
     */
    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    /**
     * @return receiverId
     */
    public String getReceiverId() {
        return this.receiverId;
    }

    /**
     * @param receiverBrId
     */
    public void setReceiverBrId(String receiverBrId) {
        this.receiverBrId = receiverBrId;
    }

    /**
     * @return receiverBrId
     */
    public String getReceiverBrId() {
        return this.receiverBrId;
    }

    /**
     * @param handoverDetail
     */
    public void setHandoverDetail(String handoverDetail) {
        this.handoverDetail = handoverDetail;
    }

    /**
     * @return handoverDetail
     */
    public String getHandoverDetail() {
        return this.handoverDetail;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param receiveDate
     */
    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    /**
     * @return receiveDate
     */
    public String getReceiveDate() {
        return this.receiveDate;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param appDate
     */
    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    /**
     * @return appDate
     */
    public String getAppDate() {
        return this.appDate;
    }

    /**
     * @param handoverResn
     */
    public void setHandoverResn(String handoverResn) {
        this.handoverResn = handoverResn;
    }

    /**
     * @return handoverResn
     */
    public String getHandoverResn() {
        return this.handoverResn;
    }

    /**
     * @param approveStatus
     */
    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    /**
     * @return approveStatus
     */
    public String getApproveStatus() {
        return this.approveStatus;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getLaunchType() {
        return launchType;
    }

    public void setLaunchType(String launchType) {
        this.launchType = launchType;
    }

}