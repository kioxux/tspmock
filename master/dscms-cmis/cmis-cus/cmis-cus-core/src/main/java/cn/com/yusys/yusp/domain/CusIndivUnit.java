/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivUnit
 * @类描述: cus_indiv_unit数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-09 22:28:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_indiv_unit")
public class CusIndivUnit extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
	@NotBlank(message = "客户号不能为空")
	private String cusId;
	
	/** 是否当前单位 STD_ZB_YES_NO **/
	@Column(name = "IS_CURT_UNIT", unique = false, nullable = true, length = 2)
	private String isCurtUnit;
	
	/** 职业 STD_ZB_OCC **/
	@Column(name = "OCC", unique = false, nullable = true, length = 5)
	private String occ;
	
	/** 自由职业说明 **/
	@Column(name = "OCC_DESC", unique = false, nullable = true, length = 200)
	private String occDesc;
	
	/** 经营企业名称/工作单位 **/
	@Column(name = "UNIT_NAME", unique = false, nullable = true, length = 80)
	private String unitName;
	
	/** 所在部门 **/
	@Column(name = "UBIETY_DEPT", unique = false, nullable = true, length = 30)
	private String ubietyDept;
	
	/** 单位性质 **/
	@Column(name = "INDIV_COM_TYP", unique = false, nullable = true, length = 5)
	private String indivComTyp;
	
	/** 单位所属行业 **/
	@Column(name = "INDIV_COM_TRADE", unique = false, nullable = true, length = 5)
	private String indivComTrade;
	
	/** 职务 STD_ZB_JOB_TTL **/
	@Column(name = "JOB_TTL", unique = false, nullable = true, length = 5)
	private String jobTtl;
	
	/** 职称 STD_ZB_TITLE **/
	@Column(name = "INDIV_CRTFCTN", unique = false, nullable = true, length = 5)
	private String indivCrtfctn;
	
	/** 年收入(元) **/
	@Column(name = "Y_SCORE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yScore;
	
	/** 单位地址 **/
	@Column(name = "UNIT_ADDR", unique = false, nullable = true, length = 20)
	private String unitAddr;
	
	/** 单位地址街道/路 **/
	@Column(name = "UNIT_STREET", unique = false, nullable = true, length = 200)
	private String unitStreet;
	
	/** 单位邮政编码 **/
	@Column(name = "UNIT_ZIP_CODE", unique = false, nullable = true, length = 6)
	private String unitZipCode;
	
	/** 单位电话 **/
	@Column(name = "UNIT_PHN", unique = false, nullable = true, length = 25)
	private String unitPhn;
	
	/** 单位传真 **/
	@Column(name = "UNIT_FAX", unique = false, nullable = true, length = 25)
	private String unitFax;
	
	/** 单位联系人 **/
	@Column(name = "UNIT_CNT_NAME", unique = false, nullable = true, length = 30)
	private String unitCntName;
	
	/** 参加工作年份 **/
	@Column(name = "WORK_DATE", unique = false, nullable = true, length = 10)
	private String workDate;
	
	/** 本单位参加工作日期 **/
	@Column(name = "UNIT_DATE", unique = false, nullable = true, length = 10)
	private String unitDate;
	
	/** 本单位工作结束日期 **/
	@Column(name = "UNIT_END_DATE", unique = false, nullable = true, length = 10)
	private String unitEndDate;
	
	/** 本行业参加工作日期  **/
	@Column(name = "TRADE_DATE", unique = false, nullable = true, length = 10)
	private String tradeDate;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/** 个人工作履历标识 **/
	@Column(name = "PRSN_WORK_ID", unique = false, nullable = true, length = 40)
	private String prsnWorkId;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 雇佣状态 **/
	@Column(name = "EMPLOYEE_STATUS", unique = false, nullable = true, length = 5)
	private String employeeStatus;
	
	/** 营业执照号码 **/
	@Column(name = "REG_CDE", unique = false, nullable = true, length = 40)
	private String regCde;
	
	/** 经营状况 **/
	@Column(name = "OPER_STATUS", unique = false, nullable = true, length = 80)
	private String operStatus;
	
	/** 经营企业统一社会信用代码 **/
	@Column(name = "UNIFY_CREDIT_CODE", unique = false, nullable = true, length = 40)
	private String unifyCreditCode;
	
	/** 收入币种 **/
	@Column(name = "EARNING_CUR_TYPE", unique = false, nullable = true, length = 10)
	private String earningCurType;
	
	/** 家庭年收入(元) **/
	@Column(name = "FAMILY_Y_SCORE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal familyYScore;
	
	/** 个人年收入 **/
	@Column(name = "INDIV_YEARN", unique = false, nullable = true, length = 16)
	private String indivYearn;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建日期 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;

	/** 更新日期 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param isCurtUnit
	 */
	public void setIsCurtUnit(String isCurtUnit) {
		this.isCurtUnit = isCurtUnit;
	}

    /**
     * @return isCurtUnit
     */
	public String getIsCurtUnit() {
		return this.isCurtUnit;
	}

	/**
	 * @param occ
	 */
	public void setOcc(String occ) {
		this.occ = occ;
	}

    /**
     * @return occ
     */
	public String getOcc() {
		return this.occ;
	}

	/**
	 * @param occDesc
	 */
	public void setOccDesc(String occDesc) {
		this.occDesc = occDesc;
	}

    /**
     * @return occDesc
     */
	public String getOccDesc() {
		return this.occDesc;
	}

	/**
	 * @param unitName
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

    /**
     * @return unitName
     */
	public String getUnitName() {
		return this.unitName;
	}

	/**
	 * @param ubietyDept
	 */
	public void setUbietyDept(String ubietyDept) {
		this.ubietyDept = ubietyDept;
	}

    /**
     * @return ubietyDept
     */
	public String getUbietyDept() {
		return this.ubietyDept;
	}

	/**
	 * @param indivComTyp
	 */
	public void setIndivComTyp(String indivComTyp) {
		this.indivComTyp = indivComTyp;
	}

    /**
     * @return indivComTyp
     */
	public String getIndivComTyp() {
		return this.indivComTyp;
	}

	/**
	 * @param indivComTrade
	 */
	public void setIndivComTrade(String indivComTrade) {
		this.indivComTrade = indivComTrade;
	}

    /**
     * @return indivComTrade
     */
	public String getIndivComTrade() {
		return this.indivComTrade;
	}

	/**
	 * @param jobTtl
	 */
	public void setJobTtl(String jobTtl) {
		this.jobTtl = jobTtl;
	}

    /**
     * @return jobTtl
     */
	public String getJobTtl() {
		return this.jobTtl;
	}

	/**
	 * @param indivCrtfctn
	 */
	public void setIndivCrtfctn(String indivCrtfctn) {
		this.indivCrtfctn = indivCrtfctn;
	}

    /**
     * @return indivCrtfctn
     */
	public String getIndivCrtfctn() {
		return this.indivCrtfctn;
	}

	/**
	 * @param yScore
	 */
	public void setYScore(java.math.BigDecimal yScore) {
		this.yScore = yScore;
	}

    /**
     * @return yScore
     */
	public java.math.BigDecimal getYScore() {
		return this.yScore;
	}

	/**
	 * @param unitAddr
	 */
	public void setUnitAddr(String unitAddr) {
		this.unitAddr = unitAddr;
	}

    /**
     * @return unitAddr
     */
	public String getUnitAddr() {
		return this.unitAddr;
	}

	/**
	 * @param unitStreet
	 */
	public void setUnitStreet(String unitStreet) {
		this.unitStreet = unitStreet;
	}

    /**
     * @return unitStreet
     */
	public String getUnitStreet() {
		return this.unitStreet;
	}

	/**
	 * @param unitZipCode
	 */
	public void setUnitZipCode(String unitZipCode) {
		this.unitZipCode = unitZipCode;
	}

    /**
     * @return unitZipCode
     */
	public String getUnitZipCode() {
		return this.unitZipCode;
	}

	/**
	 * @param unitPhn
	 */
	public void setUnitPhn(String unitPhn) {
		this.unitPhn = unitPhn;
	}

    /**
     * @return unitPhn
     */
	public String getUnitPhn() {
		return this.unitPhn;
	}

	/**
	 * @param unitFax
	 */
	public void setUnitFax(String unitFax) {
		this.unitFax = unitFax;
	}

    /**
     * @return unitFax
     */
	public String getUnitFax() {
		return this.unitFax;
	}

	/**
	 * @param unitCntName
	 */
	public void setUnitCntName(String unitCntName) {
		this.unitCntName = unitCntName;
	}

    /**
     * @return unitCntName
     */
	public String getUnitCntName() {
		return this.unitCntName;
	}

	/**
	 * @param workDate
	 */
	public void setWorkDate(String workDate) {
		this.workDate = workDate;
	}

    /**
     * @return workDate
     */
	public String getWorkDate() {
		return this.workDate;
	}

	/**
	 * @param unitDate
	 */
	public void setUnitDate(String unitDate) {
		this.unitDate = unitDate;
	}

    /**
     * @return unitDate
     */
	public String getUnitDate() {
		return this.unitDate;
	}

	/**
	 * @param unitEndDate
	 */
	public void setUnitEndDate(String unitEndDate) {
		this.unitEndDate = unitEndDate;
	}

    /**
     * @return unitEndDate
     */
	public String getUnitEndDate() {
		return this.unitEndDate;
	}

	/**
	 * @param tradeDate
	 */
	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}

    /**
     * @return tradeDate
     */
	public String getTradeDate() {
		return this.tradeDate;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param prsnWorkId
	 */
	public void setPrsnWorkId(String prsnWorkId) {
		this.prsnWorkId = prsnWorkId;
	}

    /**
     * @return prsnWorkId
     */
	public String getPrsnWorkId() {
		return this.prsnWorkId;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param employeeStatus
	 */
	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus;
	}

    /**
     * @return employeeStatus
     */
	public String getEmployeeStatus() {
		return this.employeeStatus;
	}

	/**
	 * @param regCde
	 */
	public void setRegCde(String regCde) {
		this.regCde = regCde;
	}

    /**
     * @return regCde
     */
	public String getRegCde() {
		return this.regCde;
	}

	/**
	 * @param operStatus
	 */
	public void setOperStatus(String operStatus) {
		this.operStatus = operStatus;
	}

    /**
     * @return operStatus
     */
	public String getOperStatus() {
		return this.operStatus;
	}

	/**
	 * @param unifyCreditCode
	 */
	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode;
	}

    /**
     * @return unifyCreditCode
     */
	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}

	/**
	 * @param earningCurType
	 */
	public void setEarningCurType(String earningCurType) {
		this.earningCurType = earningCurType;
	}

    /**
     * @return earningCurType
     */
	public String getEarningCurType() {
		return this.earningCurType;
	}

	/**
	 * @param familyYScore
	 */
	public void setFamilyYScore(java.math.BigDecimal familyYScore) {
		this.familyYScore = familyYScore;
	}

    /**
     * @return familyYScore
     */
	public java.math.BigDecimal getFamilyYScore() {
		return this.familyYScore;
	}

	/**
	 * @param indivYearn
	 */
	public void setIndivYearn(String indivYearn) {
		this.indivYearn = indivYearn;
	}

    /**
     * @return indivYearn
     */
	public String getIndivYearn() {
		return this.indivYearn;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}