/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.fncstat.domain.FncStatBase;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatBaseMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-23 11:39:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface FncStatBaseMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    FncStatBase selectByPrimaryKey(@Param("cusId") String cusId, @Param("statStyle") String statStyle,
                                   @Param("statPrdStyle") String statPrdStyle, @Param("statPrd") String statPrd);

    /**
     * @方法名称: selectByPrimaryKeyWithAddInfo
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    FncStatBase selectByPrimaryKeyWithAddInfo(@Param("cusId") String cusId, @Param("statStyle") String statStyle,
                                              @Param("statPrdStyle") String statPrdStyle, @Param("statPrd") String statPrd);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<FncStatBase> selectByModel(QueryModel model);
    List<FncStatBase> selectByModelGrp(QueryModel model);
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(FncStatBase record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertSelective(FncStatBase record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKey(FncStatBase record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(FncStatBase record);

    Map selectFncByCusIdRptYear(@Param("cusId") String cusId, @Param("statPrd") String statPrd);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("cusId") String cusId, @Param("statStyle") String statStyle,
                           @Param("statPrdStyle") String statPrdStyle, @Param("statPrd") String statPrd);

    /**
     * 整合拼接动态sql,根据不同表名查询项目编号集合
     *
     * @param sql
     * @return
     */
    List<String> queryItemIdByTableName(@Param("sql") String sql);

    /**
     * 执行拼接好的insert或update语句
     *
     * @param sql
     * @return
     */
    int exeInsertOrUpdateSql(@Param("sql") String sql);

    /**
     * 执行拼接好的select语句
     *
     * @param sql
     * @return
     */
    Integer exeSelectSql(@Param("sql") String sql);

    /**
     * 更新是否允许修改字段
     *
     * @param record
     * @return
     */
    int updateIndUpdateByPrimaryKey(FncStatBase record);

    List<HashMap<String, BigDecimal>> queryItemValuePair(@Param("cusId") String cusId, @Param("statStyle") String statStyle,
                                                         @Param("lastYear") String lastYear,
                                                         @Param("itemId") String itemId, @Param("tableName") String tableName,
                                                         @Param("colStart") String colStart, @Param("colEnd") String colEnd);


    /**
     * 最新完成财务报表信息查询
     * @param cusId
     * @return
     */
    FncStatBase queryFncStatBaseLastReport(@Param("cusId") String cusId);

    List<FncStatBase> findOneFncStatBase(@Param("cusId") String cusId, @Param("year") String year);

    /**
     * 根据客户号取最新维护报表年月
     * @param cusId
     * @return
     */
    String getNewRptYear(String cusId);
}