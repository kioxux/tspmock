package cn.com.yusys.yusp.web.server.cmiscus0005;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0005.req.CmisCus0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0005.resp.CmisCus0005RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscus0005.CmisCus0005Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 客户销售总额查询
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0005:查询个人关联客户列表")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0005Resource.class);

    @Autowired
    private CmisCus0005Service cmisCus0005Service;

    /**
     * 交易码：cmiscus0005
     * 交易描述：客户销售总额查询
     *
     * @param cmisCus0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户销售总额查询")
    @PostMapping("/cmiscus0005")
    protected @ResponseBody
    ResultDto<CmisCus0005RespDto> cmiscus0005(@Validated @RequestBody CmisCus0005ReqDto cmisCus0005ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0005.key, DscmsEnum.TRADE_CODE_CMISCUS0005.value, JSON.toJSONString(cmisCus0005ReqDto));
        CmisCus0005RespDto cmisCus0005RespDto = new CmisCus0005RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0005RespDto> cmisCus0005ResultDto = new ResultDto<>();
        try {
            // 封装cmisCus0005ResultDto中正确的返回码和返回信息
            cmisCus0005RespDto = cmisCus0005Service.execute(cmisCus0005ReqDto);
            cmisCus0005ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0005ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0005.key, DscmsEnum.TRADE_CODE_CMISCUS0005.value, e.getMessage());
            // 封装cmisCus0005ResultDto中异常返回码和返回信息
            cmisCus0005ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0005ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0005RespDto到cmisCus0005ResultDto中
        cmisCus0005ResultDto.setData(cmisCus0005RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0005.key, DscmsEnum.TRADE_CODE_CMISCUS0005.value, JSON.toJSONString(cmisCus0005ResultDto));
        return cmisCus0005ResultDto;
    }
}