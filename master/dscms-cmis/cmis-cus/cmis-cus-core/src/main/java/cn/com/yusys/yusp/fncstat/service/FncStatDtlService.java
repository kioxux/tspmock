/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.service;


import bsh.EvalError;
import bsh.Interpreter;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.FileSystemException;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.ErrorConstants;
import cn.com.yusys.yusp.constants.FncStatConstants;
import cn.com.yusys.yusp.constants.FncStatConstants.FncConfTypEnum;
import cn.com.yusys.yusp.constants.FncStatConstants.FncItemEditTypEnum;
import cn.com.yusys.yusp.constants.FncStatConstants.FncStatPrdStyleEnum;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusManaTaskFnc;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.AssetDebt;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Loss;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Xirs28ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.resp.Xirs28RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.fncstat.domain.FncStatBase;
import cn.com.yusys.yusp.fncstat.domain.FncStatBs;
import cn.com.yusys.yusp.fncstat.domain.FncType;
import cn.com.yusys.yusp.fncstat.domain.RptItemData;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptBriefDTO;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptDTO;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptItemDTO;
import cn.com.yusys.yusp.fncstat.repository.mapper.FncStatBaseMapper;
import cn.com.yusys.yusp.fncstat.repository.mapper.FncStatBsMapper;
import cn.com.yusys.yusp.reportconf.domain.FncConfDefFmt;
import cn.com.yusys.yusp.reportconf.domain.FncConfStyles;
import cn.com.yusys.yusp.reportconf.repository.mapper.FncConfDefFmtMapper;
import cn.com.yusys.yusp.reportconf.repository.mapper.FncConfStylesMapper;
import cn.com.yusys.yusp.reportconf.service.FncConfDefFmtService;
import cn.com.yusys.yusp.reportconf.service.FncConfStylesService;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.service.CusCorpService;
import cn.com.yusys.yusp.service.CusManaTaskFncService;
import cn.com.yusys.yusp.service.Dscms2IrsClientService;
import cn.com.yusys.yusp.util.FncNumber;
import cn.com.yusys.yusp.vo.CellExtendVo;
import cn.com.yusys.yusp.vo.CellVO;
import cn.com.yusys.yusp.vo.SheetVO;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @version 1.0.0
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatBsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zzbankwb473
 * @创建时间: 2019-12-02 09:42:19
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class FncStatDtlService {
    private static final Logger logger = LoggerFactory.getLogger(FncStatDtlService.class);
    private final String EMPTY_RESULT = "0.0";
    private final String EMPTY_FORMULA = "Double.parseDouble(\"0.0\")";
    @Autowired
    private FncStatBsMapper fncStatBsMapper;
    @Autowired
    private FncStatBaseService fncStatBaseService;
    @Autowired
    private FncConfStylesService fncConfStylesService;
    @Autowired
    private FncConfDefFmtService fncConfDefFmtService;
    @Autowired
    private FncStatBaseMapper fncStatBaseMapper;
//    @Autowired
//    private MessageProviderService messageProviderService;
//    @Autowired
//    private UserProviderService userProviderService;
    @Autowired
    private Dscms2IrsClientService dscms2IrsClientService;
    @Autowired
    private FncConfDefFmtMapper fncConfDefFmtMapper;
    @Autowired
    private FncConfStylesMapper fncConfStylesMapper;
    @Autowired
    private CusCorpMapper cusCorpMapper;
    @Autowired
    private CusManaTaskFncService cusManaTaskFncService;
    @Autowired
    private CusCorpService cusCorpService;


    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<FncStatBs> selectAll(QueryModel model) {
        List<FncStatBs> records = (List<FncStatBs>) fncStatBsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<FncStatBs> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FncStatBs> list = fncStatBsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 查询一种类型的报表详情
     *
     * @param cusId        客户代码
     * @param statStyle    报表口径
     * @param statPrdStyle 报表周期类型
     * @param statPrd      报表期间
     * @param fncConfTyp   所属报表种类
     * @return
     */
    @Transactional(readOnly = true)
    public Map<String, Object> queryFncStatDtlByType(String cusId, String statStyle, String statPrdStyle,
                                                     String statPrd, String fncConfTyp) {
        Map<String, Object> result = Maps.newConcurrentMap();
        // 查询报表基本信息
        FncStatBase fncStatBase = fncStatBaseService.selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        if (null == fncStatBase)
            return result;
        result.put("statBase", fncStatBase);
        String styleId = null;
        switch (FncConfTypEnum.byCode(fncConfTyp)) {
            case BS: // 资产负债表
                styleId = fncStatBase.getStatBsStyleId();
                break;
            case CFS: // 现金流量表
                styleId = fncStatBase.getStatCfStyleId();
                break;
            case FI: // 财务指标表
                styleId = fncStatBase.getStatFiStyleId();
                break;
            case IS: // 损益表
                styleId = fncStatBase.getStatPlStyleId();
                break;
            case BSS:
                styleId = fncStatBase.getStatBssStyleId();
                break;
            case PSS:
                styleId = fncStatBase.getStatPssStyleId();
                break;
            case FAS:
                styleId = fncStatBase.getStatFasStyleId();
                break;
            default:
                styleId = fncStatBase.getStatBsStyleId();
                break;
        }
        // 查询报表样式
        if (null == styleId)
            return result;
        FncConfStyles fcs = fncConfStylesService.queryFncConfStylesByKey(styleId);
        if (null == fcs)
            return result;
        result.put("confStyles", fcs);
        // 查询科目列表
        List<FncConfDefFmt> list = getFncConfDefFmtList(fncStatBase, fncConfTyp, fcs);
        result.put("items", list);
        return result;
    }

    /**
     * 查询财务报表基本信息以及所有详细报表信息
     *
     * @param cusId        客户代码
     * @param statStyle    报表口径
     * @param statPrdStyle 报表周期类型
     * @param statPrd      报表期间
     * @return
     */
    @Transactional(readOnly = true)
    public Map<String, Object> queryAllFncStatDtl(String cusId, String statStyle, String statPrdStyle, String statPrd) {
        Map<String, Object> result = Maps.newConcurrentMap();
        // 查询报表基本信息
        FncStatBase fncStatBase = fncStatBaseService.selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        if (null == fncStatBase)
            return result;
        result.put("statBase", fncStatBase);
        String styleId = null;

        List<FncConfStyles> confStylesList = Lists.newArrayList();
        Map<String, List<FncConfDefFmt>> itemsMap = Maps.newConcurrentMap();

        for (FncConfTypEnum confTypEnum : FncConfTypEnum.values()) {
            switch (confTypEnum) {
                case BS: // 资产负债表
                    styleId = fncStatBase.getStatBsStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                case CFS: // 现金流量表
                    styleId = fncStatBase.getStatCfStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                case FI: // 财务指标表
                    styleId = fncStatBase.getStatFiStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                case IS: // 损益表
                    styleId = fncStatBase.getStatPlStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                case BSS:
                    styleId = fncStatBase.getStatBssStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                case PSS:
                    styleId = fncStatBase.getStatPssStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                case FAS:
                    styleId = fncStatBase.getStatFasStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                default:
                    break;
            }
        }

        result.put("confStylesList", confStylesList);
        result.put("itemsMap", itemsMap);
        return result;
    }

    /**
     * 财务报表修改-编辑-暂存
     *
     * @param dto
     * @return
     */
    @Transactional
    public Integer tempSaveFncStatDtl(FncStatRptDTO dto){
        String cusId = dto.getCusId();
        String statStyle = dto.getStatStyle();
        String statPrdStyle = dto.getStatPrdStyle();
        String statPrd = dto.getStatPrd();
        String styleId = dto.getStatConfStyleId();

        FncStatBase fncStatBase = fncStatBaseService.selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        FncConfStyles fncConfStyles = fncConfStylesService.selectByPrimaryKey(styleId);
        if (null == fncStatBase || null == fncConfStyles)
            return -1;
        String fncName = fncConfStyles.getFncName();
        // 如果是利润表类财务信息把data1和data3的值交换一下
        if (FncConfTypEnum.PSS.getCode().equals(fncConfStyles.getFncConfTyp())) {
            for (FncStatRptItemDTO itemDTO : dto.getItems()) {
                String data1 = itemDTO.getData1();
                String data3 = itemDTO.getData3();
                itemDTO.setData1(data3);
                itemDTO.setData3(data1);
            }
        }
        Map<String, FncStatRptItemDTO> itemDtos = convertItemMap(dto.getItems());

        // 构造准备写库的科目列表
        List<FncConfDefFmt> fcdfList = fncConfDefFmtService.getFncConfDefFormatFromDB(fncConfStyles.getStyleId());
        Map<String, RptItemData> riDataMap = geneRptItems(cusId, statStyle, statPrdStyle, fncStatBase, fncConfStyles,
                fncName, itemDtos, fcdfList);

        // 写入报表详细信息
        for (RptItemData riData : riDataMap.values()) {
            try {
                fncStatBaseMapper.exeInsertOrUpdateSql(riData.AssembleUpdateSql());
            } catch (Exception e) {
//                Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20501118);
                logger.error(ErrorConstants.NRCS_CMS_T20501118, e);
                throw new YuspException(ErrorConstants.NRCS_CMS_T20501118, "财务报表基本信息维护错误");
            }
        }

        // 计算报表基本信息状态,写入报表基本信息
        String[] stateFlags = fncStatBase.getStateFlg().split("");
        stateFlags[FncConfTypEnum.byCode(dto.getFncConfTyp()).getIndex()] = "1";
        fncStatBase.setStateFlg(StringUtils.join(stateFlags));
        fncStatBase.setLastUpdId(getSessionUser().getLoginCode());
        fncStatBase.setLastUpdDate(DateUtils.getCurrDateStr());
        fncStatBaseService.updateSelective(fncStatBase);
        //同步更新财报任务表的财报状态
        try{
            CusManaTaskFnc  cusManaTaskFnc = new CusManaTaskFnc();
            BeanUtils.copyProperties(cusManaTaskFnc,fncStatBase);
            cusManaTaskFncService.updateStatFlg(cusManaTaskFnc);
        }catch (Exception e){
            throw new BizException(null, "", null, "同步财报任务信息异常");
        }
        return 1;
    }
    
    @Transactional
    public ResultDto saveFncStatDtl(FncStatRptDTO dto){
    	return this.saveFncStatDtl(dto, true);
    }

    /**
     * 财务报表修改-编辑-完成
     *
     * @param dto
     * @return
     */
    @Transactional
    public ResultDto saveFncStatDtl(FncStatRptDTO dto, boolean check) {
        String cusId = dto.getCusId();
        String statStyle = dto.getStatStyle();
        String statPrdStyle = dto.getStatPrdStyle();
        String statPrd = dto.getStatPrd();
        String styleId = dto.getStatConfStyleId();
        String fncConfTyp = dto.getFncConfTyp();

        FncStatBase fncStatBase = fncStatBaseService.selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        FncConfStyles fncConfStyles = fncConfStylesService.selectByPrimaryKey(styleId);
        if (null == fncStatBase || null == fncConfStyles)
            return ResultDto.error("500", "参数错误");
        String fncName = fncConfStyles.getFncName();
        Map<String, FncStatRptItemDTO> itemDtos = convertItemMap(dto.getItems());

        // 构造准备写库的科目列表
        List<FncConfDefFmt> fcdfList = fncConfDefFmtService.getFncConfDefFormatFromDB(fncConfStyles.getStyleId());
        Map<String, RptItemData> riDataMap = geneRptItems(cusId, statStyle, statPrdStyle, fncStatBase, fncConfStyles,
                fncName, itemDtos, fcdfList);
        // 处理检查公式
        //如果是ocr导入、excel导入不进行校验
        if (check) {
            Map<String, String> invalidResultMap = getCheckRuleRet(fncConfStyles, fcdfList, riDataMap);
            if (invalidResultMap.size() > 0) {
                StringBuilder sbMsg = new StringBuilder();
                for (String msg : invalidResultMap.values()) {
                    sbMsg.append(msg + "\r\n");
                }
                return ResultDto.error("500", sbMsg.toString());
            }
        }

        // 写入报表详细信息
        for (RptItemData riData : riDataMap.values()) {
            try {
                fncStatBaseMapper.exeInsertOrUpdateSql(riData.AssembleUpdateSql());
            } catch (Exception e) {
                logger.error(ErrorConstants.NRCS_CMS_T20501118, e);
                throw new YuspException(ErrorConstants.NRCS_CMS_T20501118, "财务报表基本信息维护错误");
            }
        }

        // 算报表基本信息状态,写入报表基本信息
        String[] stateFlags = fncStatBase.getStateFlg().split("");
        stateFlags[FncConfTypEnum.byCode(dto.getFncConfTyp()).getIndex()] = "2";
        // 如果是“负债表”或“损益表”,要处理“现金流量表”和“财务指标表”
        treatCfsRpt(dto, fncStatBase, fncConfTyp, statPrdStyle, styleId, stateFlags);
        boolean completeFlag = true; // 只有所有报表状态都是2的时候才是完成状态
        for (int i = 0; i < stateFlags.length - 1; i++) {
            if ("0".equals(stateFlags[i]) || "1".equals(stateFlags[i])) {
                completeFlag = false;
                break;
            }
        }
        if (completeFlag){
            stateFlags[stateFlags.length - 1] = "2";
        }
        fncStatBase.setStateFlg(StringUtils.join(stateFlags));
        fncStatBase.setLastUpdId(getSessionUser().getLoginCode());
        fncStatBase.setLastUpdDate(DateUtils.getCurrDateStr());
        fncStatBaseService.updateSelective(fncStatBase);

        //同步更新财报任务表的财报状态
        //根据财报信息更新企业规模
        try{
            CusManaTaskFnc  cusManaTaskFnc = new CusManaTaskFnc();
            BeanUtils.copyProperties(cusManaTaskFnc,fncStatBase);
            cusManaTaskFncService.updateStatFlg(cusManaTaskFnc);
            CusCorp cusCorp = cusCorpService.selectByPrimaryKey(cusId);
            String scale = cusCorpService.checkComScaleOp(cusId,cusCorp.getTradeClass());
            logger.info("获取客户编号【" + cusId + "】规模成功："+scale);
            if(StringUtils.isNotEmpty(scale)){
                cusCorp.setCorpScale(scale);
                cusCorpService.updateSelective(cusCorp);
                logger.info("开始同步企业规模至ecif，客户编号：{}", cusId);
                cusCorpService.updateEcifInfo(cusId);
            }

        }catch (Exception e){
            throw new BizException(null, "", null, "同步财报任务信息异常");
        }

        if(completeFlag) {
            sendXirs28ReqDto(fncStatBase);
        }
        return ResultDto.success(1);
    }

    private void sendXirs28ReqDto(FncStatBase fncStatBase) {

        String cusId = fncStatBase.getCusId();

        CusCorp cus = this.cusCorpMapper.selectByPrimaryKey(cusId);
        String finRepType = cus != null ? cus.getFinaReportType() : null;
        Map<FncStatBase, List<FncType>> map = this.getFncDataList(fncStatBase);

        Iterator iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Xirs28ReqDto reqDto = new Xirs28ReqDto();

            Map.Entry entry = (Map.Entry) iter.next();
            FncStatBase key = (FncStatBase) entry.getKey();
            List<FncType> val = (List<FncType>) entry.getValue();

            reqDto.setCustid(key.getCusId());
            reqDto.setReportdate(key.getStatPrd().substring(0, 4) + "/" + key.getStatPrd().substring(4));
            reqDto.setReportsope(key.getStatStyle());
            reqDto.setReportcurrency("CNY");
            reqDto.setReportunit("01");
            reqDto.setReportaudit(key.getStatIsAdjt() == null ? "2" : key.getStatIsAdjt());
            reqDto.setAuditunit(key.getStatAdtEntr()== null ? "" : key.getStatAdtEntr());
            reqDto.setAuditopinion(key.getStatAdtConc()== null ? "" : key.getStatAdtConc());
            reqDto.setModelclass(finRepType==null?"PB0001":finRepType);
            //String v_flag=key.getSendFlag();
            //String flag=(v_flag==null || "".equals(v_flag))?"00":v_flag;
            reqDto.setFlag("02");

            //if (flag.equals("03"))
                //continue; // 删除类型没有明细

            Iterator ite1 = val.iterator();

            List<AssetDebt> assetDebt = new ArrayList<AssetDebt>();
            List<Loss> loss = new ArrayList<Loss>();
            while (ite1.hasNext()) {

                FncType ft = (FncType) ite1.next();
                if ("01".equals(ft.getFncConfTyp())) { // 负债表
                    List<FncConfDefFmt> list = ft.getFncItems();

                    Iterator<FncConfDefFmt> ite2 = list.iterator();
                    while (ite2.hasNext()) {
                        FncConfDefFmt fcdf = ite2.next();
                        AssetDebt asdt = new AssetDebt();
                        asdt.setSubjectno(fcdf.getItemId());
                        String data1 = fcdf.getData1();
                        data1 = StringUtils.isEmpty(data1) ? "0" : data1;
                        asdt.setColvalue1(new BigDecimal(data1));
                        String data2 = fcdf.getData2();
                        data2 = StringUtils.isEmpty(data2) ? "0" : data2;
                        asdt.setColvalue2(new BigDecimal(data2));
                        assetDebt.add(asdt);
                    }

                } else if ("02".equals(ft.getFncConfTyp()) || "07".equals(ft.getFncConfTyp())) { // 损益表
                    List<FncConfDefFmt> list = ft.getFncItems();

                    Iterator<FncConfDefFmt> ite2 = list.iterator();
                    while (ite2.hasNext()) {
                        FncConfDefFmt fcdf = ite2.next();
                        Loss los = new Loss();
                        los.setSubjectno(fcdf.getItemId());
                        String data1 = fcdf.getData1();
                        data1 = StringUtils.isEmpty(data1) ? "0" : data1;
                        los.setColvalue1(new BigDecimal(data1));
                        String data2 = fcdf.getData2();
                        data2 = StringUtils.isEmpty(data2) ? "0" : data2;
                        los.setColvalue2(new BigDecimal(data2));
                        loss.add(los);
                    }

                }
            }

            reqDto.setAssetDebt(assetDebt);
            reqDto.setLoss(loss);
            ResultDto<Xirs28RespDto> result = dscms2IrsClientService.xirs28(reqDto);
        }

    }

    private Map<FncStatBase, List<FncType>> getFncDataList(FncStatBase fncStatBase) {

        String cusId = fncStatBase.getCusId();
        String statPrd = fncStatBase.getStatPrd();
        List<FncStatBase> fncStatBaseList = this.fncStatBaseMapper.findOneFncStatBase(cusId, statPrd.substring(0,4)+"12");

        Map<String,FncConfStyles> fncsMap = new HashMap<String,FncConfStyles>();
        List<FncConfStyles> fncConfStylesList = this.fncConfStylesMapper.selectAll();
        for(FncConfStyles fncConfStyles : fncConfStylesList) {
            String styleId = fncConfStyles.getStyleId();
            List<FncConfDefFmt> fncConfDefFmtList = this.fncConfDefFmtService.getFncConfDefFormatFromDB(styleId);
            fncConfStyles.setFncConfDefFmtList(fncConfDefFmtList);
            fncsMap.put(styleId, fncConfStyles);
        }

        Map fncMap = new HashMap<FncStatBase,List<FncType>>();
        for(FncStatBase pfncStatBase : fncStatBaseList) {
            List<FncType> fncList=new ArrayList<FncType>();
            FncType  fnc1= new FncType("01",pfncStatBase.getStatBsStyleId());
            FncType  fnc2= new FncType("02",pfncStatBase.getStatPlStyleId());
            FncType  fnc3= new FncType("03",pfncStatBase.getStatCfStyleId());
            FncType  fnc4= new FncType("04",pfncStatBase.getStatFiStyleId());
            FncType  fnc5= new FncType("05",pfncStatBase.getStatSoeStyleId());
            FncType  fnc6= new FncType("06",pfncStatBase.getStatSlStyleId());
            FncType  fnc7= new FncType("07",pfncStatBase.getStyleId1());
            fncList.add(fnc1);
            fncList.add(fnc2);
            fncList.add(fnc3);
            fncList.add(fnc4);
            fncList.add(fnc5);
            fncList.add(fnc6);
            fncList.add(fnc7);

            //if("03".equals(pfncStatBase.getSendFlag())){//如果是删除的报表就不用查询 子财报信息
            //   continue;
            //}
            List<FncType> fncList1=new ArrayList<FncType>();

            for(FncType fnc : fncList) {
                if(fnc.getStyleId()==null || "".equals(fnc.getStyleId())){
                    continue;
                }
                /**
                 * 从系统缓存中读取取得报表样式信息对象，得到其中的所有项目列表
                 */
                FncConfStyles fcs = fncsMap.get(fnc.getStyleId());
                String tableName = fcs.getFncName();
                String fncConfTyp = fcs.getFncConfTyp();

                fnc.setFncName(fcs.getFncName());
                fnc.setFncConfDisName(fcs.getFncConfDisName());
                fnc.setCusId(pfncStatBase.getCusId());
                fnc.setStatPrdStyle(pfncStatBase.getStatPrdStyle());//statPrdStyle
                fnc.setStatPrd(pfncStatBase.getStatPrd());//statPrd
                fnc.setStatStyle(pfncStatBase.getStatStyle());//pk4_value

                //总的配置科目项
                List<FncConfDefFmt> outItems=fcs.getFncConfDefFmtList();
                //实际存放的科目项
                List<FncConfDefFmt> itemList = this.getFncConfDefFmt(cusId, pfncStatBase.getStatPrdStyle(),
                        pfncStatBase.getStatPrd(),fnc.getStyleId(),tableName,fncConfTyp,pfncStatBase.getStatStyle());

                //合并为总的科目配置
                outItems=this.getAllFncItems(outItems,itemList);
                //
                fnc.setFncItems(outItems);

                fncList1.add(fnc);
            }
            fncMap.put(pfncStatBase, fncList1);
        }

        return fncMap;
    }

    private List<FncConfDefFmt> getFncConfDefFmt(String cusId,String statPrdStyle,String statPrd,
                                                 String styleId,String tableName,String fncConfTyp,String statStyle) {

        String postfix = null; // 字段属性后缀的标示

        // 拆分报表期间statPrd
        String year = statPrd.substring(0, 4);
        String month = statPrd.substring(4);

        /**
         * 根据报表周期类型判断该报表的类型.进行拼sql
         */
        if ("1".equals(statPrdStyle)) { // 月报 _Amt6
            postfix = month;
            if (postfix.indexOf("0") == 0) {
                postfix = postfix.substring(1);
            }
        } else if ("2".equals(statPrdStyle)) { // 季报 _Amt_Q2
            postfix = FncNumber.getJibao(month);
        } else if ("3".equals(statPrdStyle)) { // 半年报 _Amt_Y1
            postfix = FncNumber.getBanNianBao(month);
        } else if ("4".equals(statPrdStyle)) { // 年报 _Amt_Y
            postfix = FncNumber.getNianBao(month);
        }

        /**
         * 开始组装sql语句(根据报表类型组装)
         */
        List<FncConfDefFmt> fncConfDefFmtList;
        String lastYear = String.valueOf(Integer.parseInt(year) - 1);
        if ("05".equals(fncConfTyp)) {
            fncConfDefFmtList = this.fncConfDefFmtMapper.queryForSoe(postfix, cusId, tableName, year, statStyle, styleId);
        } else {
            Double sum = this.fncStatBsMapper.querySumValue(cusId, year, statStyle);
            if (sum != null && sum > 0) {
                fncConfDefFmtList = this.fncConfDefFmtMapper.querySqlNew2(tableName, fncConfTyp, postfix, cusId, year, lastYear, statStyle, styleId);
            } else {
                fncConfDefFmtList = this.fncConfDefFmtMapper.querySqlNew(tableName, fncConfTyp, postfix, cusId, year, statStyle, styleId);
            }
        }

        return fncConfDefFmtList;

    }

    private  List<FncConfDefFmt> getAllFncItems(List<FncConfDefFmt> toFnc,List<FncConfDefFmt> srcFnc){
        List<FncConfDefFmt> fnc = toFnc;
        if(srcFnc==null || srcFnc.size()==0){
            logger.error("srcFnc 实际的财报科目为空");
        }
        int size=fnc.size();
        int size2 = 0;
        if(null != srcFnc){
             size2=srcFnc.size();
        }
        for(int i=0;i<size;i++){
            FncConfDefFmt temp=fnc.get(i);
            for(int j=0;j<size2;j++){
                if(temp.getItemId().equals(srcFnc.get(j).getItemId())){
                    fnc.set(i, srcFnc.get(j));
                    break;
                }
            }
        }
        return  fnc;
    }

    private Map<String, String> getCheckRuleRet(FncConfStyles fncConfStyles, List<FncConfDefFmt> fcdfList, Map<String, RptItemData> riDataMap) {
        Map<String, String> invalidResultMap = Maps.newConcurrentMap();
        for (FncConfDefFmt fncFormat : fcdfList) {
            String itemId = fncFormat.getItemId();
            String checkFormula = fncFormat.getFncConfChkFrm();
            if (StringUtils.isEmpty(checkFormula))
                continue;
            if (checkFormula.indexOf("@OPER")==-1)
                continue;
            String operator = StringUtils.substringBetween(checkFormula, "(", ")");
            String[] destItemIds =StringUtils.substringsBetween(checkFormula, "{", "}");
       /*     if (!riDataMap.containsKey(destItemId))
                continue;*/
            //xuchi
            String[] params = StringUtils.substringsBetween(checkFormula, "[", "]");
            if (params == null || params.length == 0)
                throw new IllegalArgumentException("检查公式配置错误：" + checkFormula);

            Integer dataCol = fncConfStyles.getFncConfDataCol();// 有几列输入框检查几次
            boolean checkResult = false;
            String bshFormula = "";

            String leftValue = null;
            String rightValue = null;
            String checkFormulaTmp = null;
            switch (dataCol) {
                case 1:
                    leftValue = String.valueOf(riDataMap.get(itemId).getData1());
                    checkFormulaTmp = checkFormula;
                    for (int i = 0; i < params.length; i++) {
                        String param = params[i];
                        String destItemId = "{"+destItemIds[i]+"}";
                        String value = "Double.parseDouble(\""+String.valueOf(riDataMap.get(param).getData1())+"\")";
                        checkFormulaTmp =  checkFormulaTmp.replace(destItemId,value);
                        rightValue = checkFormulaTmp.substring(checkFormulaTmp.indexOf(("("+operator+")"))).replace("("+operator+")","");
                    }

                    rightValue = checkFormulaTmp.substring(checkFormulaTmp.indexOf(("("+operator+")"))).replace("("+operator+")","");
                    bshFormula = "Double.parseDouble(\"" + getZeroIfNull(leftValue) + "\")" + operator + getZeroIfNull(rightValue);

                    checkResult = runChkBsh(checkFormula, bshFormula,leftValue,rightValue,operator);
                    break;
                case 2:
                    //如果是损益表，不校验本期数
                    if(!"SL01".equals(fncConfStyles.getStyleId()) && !"XSL01".equals(fncConfStyles.getStyleId())){
                        leftValue = String.valueOf(riDataMap.get(itemId).getData1());
                        checkFormulaTmp = checkFormula;
                        // rightValue = String.valueOf(riDataMap.get(destItemId).getData1());
                        for (int i = 0; i < params.length; i++) {
                            String param = params[i];
                            String destItemId = "{"+destItemIds[i]+"}";
                            String value = "Double.parseDouble(\""+String.valueOf(riDataMap.get(param).getData1())+"\")";
                            checkFormulaTmp =  checkFormulaTmp.replace(destItemId,value);
                            rightValue = checkFormulaTmp.substring(checkFormula.indexOf(("("+operator+")"))).replace("("+operator+")","");
                        }

                        rightValue = checkFormulaTmp.substring(checkFormulaTmp.indexOf(("("+operator+")"))).replace("("+operator+")","");
                        bshFormula = "Double.parseDouble(\"" + getZeroIfNull(leftValue) + "\")" + operator + getZeroIfNull(rightValue);
                        checkResult = runChkBsh(checkFormula, bshFormula,leftValue,rightValue,operator);
                    }else{
                        checkResult =true;
                    }
                    if (checkResult) {
                        leftValue = String.valueOf(riDataMap.get(itemId).getData2());
                        checkFormulaTmp = checkFormula;
                        for (int i = 0; i < params.length; i++) {
                            String param = params[i];
                            String destItemId = "{"+destItemIds[i]+"}";
                            String value = "Double.parseDouble(\""+String.valueOf(riDataMap.get(param).getData2())+"\")";
                            checkFormulaTmp =  checkFormulaTmp.replace(destItemId,value);
                            rightValue = checkFormulaTmp.substring(checkFormulaTmp.indexOf(("("+operator+")"))).replace("("+operator+")","");
                        }

                        rightValue = checkFormulaTmp.substring(checkFormulaTmp.indexOf(("("+operator+")"))).replace("("+operator+")","");
                        bshFormula = "Double.parseDouble(\"" + getZeroIfNull(leftValue) + "\")" + operator + getZeroIfNull(rightValue);

                        checkResult = runChkBsh(checkFormula, bshFormula,leftValue,rightValue,operator);
                    }
                    break;
                default:
                    break;
            }

            if (!checkResult)
                invalidResultMap.put(itemId, fncFormat.getItemName() + "[" + itemId + "]检查不通过,公式:" + checkFormula);
        }
        return invalidResultMap;
    }
    
    private String getZeroIfNull(String value) {
    	return value == null ? EMPTY_RESULT : value;
    }

    /**
     * 如果完成动作是“负债表”或“损益表”,要处理“现金流量表”和“财务指标表”
     *
     * @param dto
     * @param fncStatBase
     * @param fncConfTyp
     * @param statPrdStyle
     * @param styleId
     * @param stateFlags
     */
    private void treatCfsRpt(FncStatRptDTO dto, FncStatBase fncStatBase, String fncConfTyp, String statPrdStyle,
                             String styleId, String[] stateFlags) {
        if (!FncConfTypEnum.BS.getCode().equals(fncConfTyp) && !FncConfTypEnum.IS.getCode().equals(fncConfTyp))
            return;
        if (!"2".equals(stateFlags[0]) || !"2".equals(stateFlags[1]))
            return;
        if (StringUtils.isEmpty(fncStatBase.getStatCfStyleId()))
            return;
        FncStatRptDTO tmpDto = new FncStatRptDTO();
        tmpDto.setCusId(dto.getCusId());
        tmpDto.setStatPrd(dto.getStatPrd());
        tmpDto.setStatPrdStyle(dto.getStatPrdStyle());
        tmpDto.setStatStyle(dto.getStatStyle());
        tmpDto.setFncConfTyp(FncConfTypEnum.CFS.getCode()); // 现金流量表
        tmpDto.setStatConfStyleId(fncStatBase.getStatCfStyleId());
        tmpDto.setItems(Lists.newArrayList());
        this.tempSaveFncStatDtl(dto);
        /**
         * 如果是 非年报 或 年报（高校、医院、中专、中小学、事业法人），无须录入现金流量表，置现金流量表状态为 ‘2’ 已完成 modify by liyb
         * 2011-08-23
         */
        if (isIgnoreCfs(statPrdStyle, styleId)) {
            stateFlags[FncConfTypEnum.CFS.getIndex()] = "2";
            tmpDto.setFncConfTyp(FncConfTypEnum.FI.getCode()); // 财务指标表
            tmpDto.setStatConfStyleId(fncStatBase.getStatFiStyleId());
            this.tempSaveFncStatDtl(dto);
        }
    }

    /**
     * 如果是 非年报 或 年报（高校、医院、中专、中小学、事业法人），无须录入现金流量表，置现金流量表状态为 ‘2’ 已完成 modify by liyb
     * 2011-08-23
     */
    private boolean isIgnoreCfs(String statPrdStyle, String styleId) {
        return !statPrdStyle.equals("4") || styleId.equals("SL_16") || styleId.equals("SL_17")
                || styleId.equals("SL_18") || styleId.equals("SL_19") || styleId.equals("SL_20");
    }

    /**
     * 解释执行JAVA脚本
     *
     * @param checkFormula 原始公式
     * @param bshFormula   转换后可执行的公式
     * @return
     */
    private boolean runChkBsh(String checkFormula, String bshFormula,String leftValue,String rightValue,String operator) {
        Interpreter interpreter = new Interpreter();
        Boolean checkResult = false;
        Double leftValueDouble= Double.parseDouble("0");
        Double rightValueDouble= Double.parseDouble("0");
        DecimalFormat df = new DecimalFormat("#.##");
        try {
            bshFormula = bshFormula.replace("MAX(0", "Math.max(0.0");
            leftValue = "Double.parseDouble(\"" + getZeroIfNull(leftValue) + "\")";
            interpreter.set("checkResult", checkResult);
            interpreter.set("leftValueDouble", leftValueDouble);
            interpreter.set("rightValueDouble", rightValueDouble);
            interpreter.eval("try{leftValueDouble=(" + leftValue + ");}catch(ArithmeticException e){leftValueDouble=Double.parseDouble(\"0\");}");
            leftValueDouble = (Double) interpreter.get("leftValueDouble");
            interpreter.eval("try{rightValueDouble=(" + rightValue + ");}catch(ArithmeticException e){rightValueDouble=Double.parseDouble(\"0\");}");
            rightValueDouble = (Double) interpreter.get("rightValueDouble");

            bshFormula = "Double.parseDouble(\"" + getZeroIfNull(df.format(leftValueDouble)) + "\")" + operator + "Double.parseDouble(\"" +getZeroIfNull(df.format(rightValueDouble))+ "\")";
            interpreter.eval("try{checkResult=(" + bshFormula + ");}catch(ArithmeticException e){checkResult=false;}");
            checkResult = (Boolean) interpreter.get("checkResult");
        } catch (EvalError e) {
            logger.error("检查公式错误：" + checkFormula + " : " + bshFormula, e);
        }
        return checkResult;
    }
    /**
     * 组装写各类型报表详细信息的数据对象
     *
     * @param cusId
     * @param statStyle
     * @param statPrdStyle
     * @param fncStatBase
     * @param fncConfStyles
     * @param fncName
     * @param itemDtos
     * @param fcdfList
     * @return key->itemId 科目ID，value->RptItemData item信息VO
     * |用户操作具体的报表时，组装sql语句用的数据对象
     */
    private Map<String, RptItemData> geneRptItems(String cusId, String statStyle, String statPrdStyle,
                                                  FncStatBase fncStatBase, FncConfStyles fncConfStyles, String fncName,
                                                  Map<String, FncStatRptItemDTO> itemDtos, List<FncConfDefFmt> fcdfList) {
        Map<String, RptItemData> riDataMap = Maps.newConcurrentMap();

        String statYear = fncStatBase.getStatPrd().substring(0, 4);
        String statMonth = fncStatBase.getStatPrd().substring(4);
        String postfix = getPostfix(statPrdStyle, statMonth);
        for (FncConfDefFmt fncFormat : fcdfList) {
            RptItemData riData = new RptItemData(cusId, fncName, statPrdStyle, statYear, statMonth,
                    fncFormat.getItemId(), statStyle, fncConfStyles.getFncConfDataCol());

            if (!itemDtos.containsKey(riData.getItemId())
                    && !FncItemEditTypEnum.CALC.getCode().equals(fncFormat.getFncItemEditTyp())) // 没有传值并且不是计算科目
                continue;

            if (itemDtos.containsKey(riData.getItemId())
                    && !FncItemEditTypEnum.CALC.getCode().equals(fncFormat.getFncItemEditTyp())) {// 不需要计算的科目
                FncStatRptItemDTO itemDto = itemDtos.get(riData.getItemId());
                String data1 = itemDto.getData1();
                String data2 = itemDto.getData2();
                String data3 = itemDto.getData3();
                riData.setData1(StringUtils.isNotBlank(data1) ? new BigDecimal(data1): BigDecimal.ZERO);
                riData.setData2(StringUtils.isNotBlank(data2) ? new BigDecimal(data2) : BigDecimal.ZERO);
                riData.setData3(StringUtils.isNotBlank(data3) ? new BigDecimal(data1):BigDecimal.ZERO);
                riDataMap.put(riData.getItemId(), riData);
                continue;
            }

            // 处理计算公式 xuchi
            String calcFormula = fncFormat.getFncConfCalFrm();
            String[] params = StringUtils.substringsBetween(calcFormula, "{", "}");
            if (params == null || params.length == 0)
                throw new IllegalArgumentException("公式配置错误：" + calcFormula);
            String formulaScript;
            // boolean multiRptFlag = false; // 是否引用其他报表数据标识
            if (calcFormula.indexOf(".[") >= 0) {// [01]资产负债表.[fnc_201]短期借款.[2]期末数 这种格式需要从数据库取其它报表数据，计算一次即可
                // multiRptFlag = true;
                formulaScript = transferFormulaMultiRpt(cusId, statStyle, statYear, postfix, calcFormula, params);
                String data1 = runBsh(calcFormula, formulaScript);
                if(data1!=null && EMPTY_RESULT.equals(data1)){
                    riData.setData1(new BigDecimal(runBsh(calcFormula, formulaScript)));
                }else{
                    FncStatRptItemDTO itemDto = itemDtos.get(riData.getItemId());
                    String qdData1 = itemDto.getData1();
                    riData.setData1(StringUtils.isNotBlank(qdData1) ? new BigDecimal(qdData1): BigDecimal.ZERO);
                }

            } else {// [fnc_117]固定资产原价 这种格式取本报表其它科目数据，有几个输入框就要计算几次
                formulaScript = transferFormula(itemDtos, calcFormula, params, "data1");
                riData.setData1(new BigDecimal(runBsh(calcFormula, formulaScript)));
                switch (fncConfStyles.getFncConfDataCol()) {
                    case 2:
                        formulaScript = transferFormula(itemDtos, calcFormula, params, "data2");
                        riData.setData2(new BigDecimal(runBsh(calcFormula, formulaScript)));
                        break;
                    case 3:
                        formulaScript = transferFormula(itemDtos, calcFormula, params, "data2");
                        riData.setData2(new BigDecimal(runBsh(calcFormula, formulaScript)));
                        formulaScript = transferFormula(itemDtos, calcFormula, params, "data3");
                        riData.setData3(new BigDecimal(runBsh(calcFormula, formulaScript)));
                        break;
                    default:
                        break;
                }
            }
            riDataMap.put(riData.getItemId(), riData);
        }
        return riDataMap;
    }

    /**
     * 解析只引用本报表内科目数据的公式 [fnc_117]固定资产原价 这种格式取本报表其它科目数据，有几个输入框就要计算几次
     *
     * @param itemDtos
     * @param calcFormula
     * @param params
     * @param dataPropName
     * @return
     */
    private String transferFormula(Map<String, FncStatRptItemDTO> itemDtos, String calcFormula, String[] params,
                                   String dataPropName) {
        String formulaScript;
        String[] source = new String[params.length];
        String[] dest = new String[params.length];
        try {
            for (int i = 0; i < params.length; i++) {
                String param = params[i];
                source[i] = "{" + param + "}";
                String itemId = StringUtils.substringBetween(param, "[", "]");
                FncStatRptItemDTO valueDto = itemDtos.get(itemId);
                if (null == valueDto)
                    return EMPTY_FORMULA;
                dest[i] = "Double.parseDouble(\"" + getZeroIfNull(BeanUtils.getProperty(valueDto, dataPropName)) + "\")";
            }
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            logger.error("财务报表-处理计算公式错误: ", e);
        }
        formulaScript = StringUtils.replaceEach(calcFormula, source, dest);
        return formulaScript;
    }

    /**
     * 解析需要引用其他类型报表数据的公式 [01]资产负债表.[fnc_201]短期借款.[2]期末数 这种格式需要从数据库取其它报表数据，计算一次即可
     *
     * @param cusId
     * @param statStyle
     * @param statYear
     * @param postfix
     * @param calcFormula
     * @param params
     * @return
     */
    private String transferFormulaMultiRpt(String cusId, String statStyle, String statYear, String postfix,
                                           String calcFormula, String[] params) {
        String[] source = new String[params.length];
        String[] dest = new String[params.length];
        for (int i = 0; i < params.length; i++) {
            Map<String, String> queryParams = Maps.newConcurrentMap();
            String param = params[i];
            String itemValue = null;
            source[i] = "{" + param + "}";
            String[] sqlParams = StringUtils.substringsBetween(param, "[", "]");
            if (null == sqlParams || sqlParams.length != 3)
                throw new IllegalArgumentException("公式配置错误：" + calcFormula);
            String fncTyp = sqlParams[0];
            String itemId = sqlParams[1];
            String dataIndex = sqlParams[2];
            // 拼sql查询科目值
            if ("1".equals(dataIndex))// 期初数
                queryParams.put("dataField", "stat_init_amt" + postfix);
            else {// 期末数
                if (FncConfTypEnum.PSS.getCode().equals(fncTyp)) {
                    queryParams.put("dataField", "STAT_INIT_AMT_LY1");
                } else {
                    queryParams.put("dataField", "stat_end_amt" + postfix);
                }
            }
            queryParams.put("tableName", FncConfTypEnum.byCode(fncTyp).getTableName());
            queryParams.put("cusId", cusId);
            queryParams.put("statStyle", statStyle);
            queryParams.put("rptYear", statYear);
            queryParams.put("itemId", itemId);
            itemValue = fncStatBsMapper.queryItemValue(queryParams);
            if (StringUtils.isNotEmpty(itemValue))
                dest[i] = "Double.parseDouble(\"" + getZeroIfNull(itemValue) + "\")";
            else
                dest[i] = "Double.parseDouble(\"" + EMPTY_RESULT + "\")";
        }
        String formulaScript = StringUtils.replaceEach(calcFormula, source, dest);
        return formulaScript;
    }

    private Map<String, FncStatRptItemDTO> convertItemMap(List<FncStatRptItemDTO> items) {
        Map<String, FncStatRptItemDTO> ret = Maps.newConcurrentMap();
        for (FncStatRptItemDTO item : items) {
            ret.put(item.getItemId(), item);
        }
        return ret;
    }

    /**
     * 查询一个类型的报表详细信息
     *
     * @param confStylesList
     * @param itemsMap
     * @param styleId
     * @param fncStatBase
     * @param fncConfTyp     报表类型 @see
     *                       {@link cn.com.yusys.yusp.constants.FncStatConstants.FncConfTypEnum}
     */
    private void queryOneRpt(List<FncConfStyles> confStylesList, Map<String, List<FncConfDefFmt>> itemsMap,
                             String styleId, FncStatBase fncStatBase, String fncConfTyp) {
        // 查询报表样式
        if (null == styleId || "".equals(styleId))
            return;
        FncConfStyles fcs = fncConfStylesService.queryFncConfStylesByKey(styleId);
        if (null == fcs || "".equals(fcs))
            return;
        confStylesList.add(fcs);
        // 查询科目列表
        List<FncConfDefFmt> list = getFncConfDefFmtList(fncStatBase, fncConfTyp, fcs);
        itemsMap.put(fcs.getStyleId(), list);
    }

    /**
     * 查询报表样式配置的科目列表信息
     *
     * @param fncStatBase
     * @param fncConfTyp  报表类型 @see
     *                    {@link cn.com.yusys.yusp.constants.FncStatConstants.FncConfTypEnum}
     * @param fcs
     * @return
     */
    private List<FncConfDefFmt> getFncConfDefFmtList(FncStatBase fncStatBase, String fncConfTyp, FncConfStyles fcs) {
        String tableName = fcs.getFncName();
        // 拆分报表期间statPrd
        String year = fncStatBase.getStatPrd().substring(0, 4);
        String month = fncStatBase.getStatPrd().substring(4);
        // 根据报表周期类型判断该报表的类型.进行拼sql
        String postfix = getPostfix(fncStatBase.getStatPrdStyle(), month);
        FncConfDefFmt fncConfDefFmt = new FncConfDefFmt();
        fncConfDefFmt.setCusId(fncStatBase.getCusId());
        fncConfDefFmt.setStyleId(fcs.getStyleId());
        fncConfDefFmt.setTableName(tableName);
        fncConfDefFmt.setStatStyle(fncStatBase.getStatStyle());
        fncConfDefFmt.setStatYear(year);
        StringBuilder sb = new StringBuilder(); // 用于存放拼成的sql
        sb.append(
                "f.item_id,f.fnc_conf_order,f.fnc_conf_cotes,f.fnc_conf_row_flg,f.fnc_conf_indent,f.fnc_conf_prefix,f.fnc_item_edit_typ,"
                        + "f.fnc_cnf_app_row,f.fnc_conf_disp_tpy,f.fnc_conf_disp_amt,f.fnc_conf_chk_frm,f.fnc_conf_cal_frm,n.item_name,");
        sb.append(geneStatColSelSql(fncConfTyp, postfix));

        fncConfDefFmt.setPreSql(sb.toString());
        return fncConfDefFmtService.queryFncConfDefFmtList1(fncConfDefFmt);
    }

    /**
     * 根据报表期间类型，获得数据存储字段的后缀
     *
     * @param statPrdStyle
     * @param month
     * @return
     */
    private String getPostfix(String statPrdStyle, String month) {
        String postfix = null; // 字段属性后缀的标示
        if (FncStatPrdStyleEnum.MONTH.getCode().equals(statPrdStyle)) { // 月报 _Amt6
            postfix = month;
            if (postfix.indexOf("0") == 0) {
                postfix = postfix.substring(1);
            }
        } else if (FncStatPrdStyleEnum.SEASON.getCode().equals(statPrdStyle)) { // 季报 _Amt_Q2
            postfix = FncNumber.getJibao(month);
        } else if (FncStatPrdStyleEnum.HALFY.getCode().equals(statPrdStyle)) { // 半年报 _Amt_Y1
            postfix = FncNumber.getBanNianBao(month);
        } else if (FncStatPrdStyleEnum.YEAR.getCode().equals(statPrdStyle)) { // 年报 _Amt_Y
            postfix = FncNumber.getNianBao(month);
        }
        return postfix;
    }

    private String geneStatColSelSql(String fncConfTyp, String postfix) {
        StringBuilder sb = new StringBuilder();
        switch (FncConfTypEnum.byCode(fncConfTyp)) {
            case FI:
                sb.append("b.stat_init_amt" + postfix + " as data1 ");
                break;
            case BSS:
                sb.append("b.stat_init_amt" + postfix + " as data1 ");
                break;
            case FAS:
                sb.append("b.stat_init_amt" + postfix + " as data1 ");
                break;
            case PSS:
                sb.append("b.stat_init_amt" + postfix
                        + " as data3,b.stat_init_amt_ly1 as data2,b.stat_init_amt_ly2 as data1 ");
                break;
            default:
                sb.append("b.stat_init_amt" + postfix + " as data1, b.stat_end_amt" + postfix + "  as data2 ");
                break;
        }

        return sb.toString();
    }

    /**
     * Beanshell 用来把java当成脚本语言执行的解释器
     *
     * @param formula
     * @param formulaScript
     * @return
     */
    private String runBsh(String formula, String formulaScript) {
        Interpreter interpreter = new Interpreter();
        Float value = null;
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            interpreter.set("value", value);
            formulaScript = formulaScript.replace("MAX(0", "Math.max(0.0");
            interpreter.eval("try{value=(" + formulaScript + ");}catch(ArithmeticException e){value=0.0;}");
            if (interpreter.get("value") instanceof Double) {
            	Double temp = (Double) interpreter.get("value");
            	if (Double.isInfinite(temp) || Double.isNaN(temp))
                    return EMPTY_RESULT;
            	return df.format(temp);
            } else if (interpreter.get("value") instanceof Float) {
            	Float temp = (Float) interpreter.get("value");
            	if (Float.isInfinite(temp) || Float.isNaN(temp))
                    return EMPTY_RESULT;
            	return df.format(temp);
            } else {
            	throw new Error("不存在类型");
            }
        } catch (EvalError e) {
            logger.error("计算公式错误：" + formula + " || " + formulaScript, e);
            return EMPTY_RESULT;
        }
    }

    private User getSessionUser() {
        User user = SessionUtils.getUserInformation();
        if (null == user) {
            logger.error("个人客户管理-财务报表信息-获取缓存User信息-getSessionUser方法-User对象为空");
            throw new YuspException("500", "个人客户管理-财务报表信息-获取缓存User信息-getSessionUser方法-User对象为空");
        }
        return user;
    }

    /**
     * 财报统计excel导出
     *
     * @param cusId
     * @param statStyle
     * @param statPrdStyle
     * @param statPrd
     */
    public byte[] queryAllFncStatExport(String cusId, String statStyle, String statPrdStyle, String statPrd) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        // 根据cusId 获取财报统计数据
        Map<String, Object> fncStatMap = this.queryAllFncStatDtl(cusId, statStyle, statPrdStyle, statPrd);
        if (null == fncStatMap || fncStatMap.isEmpty()) {
            return os.toByteArray();
        }
        // bind data
        this.bindDataToExcel(fncStatMap, os);

        return os.toByteArray();
    }
    
    /**
     * 导入excel文件
     * @param fileId
     * @return
     */
    public ResultDto<ProgressDto> importFncStatDtlExcel(String fileId, FncStatBase fbcFncStatBase){
    	//读取excel文件
		FncStatRptBriefDTO rptBriefDTO = readFncStatDtlExcel(fileId, fbcFncStatBase);
		//导入数据
		ResultDto resultDto = saveFncStatDtlBrief(rptBriefDTO, false);
		if (!SuccessEnum.CMIS_SUCCSESS.key.equals(resultDto.getCode())) {
			throw new BizException(null, resultDto.getCode(), null, resultDto.getMessage());
		}
		ResultDto<ProgressDto> result = ResultDto.success(1);
		result.setMessage("导入成功");
		return result;
    }
    
    /**
     * 读取报表excel数据
     * @param fileId 文件名字
     * @return
     */
    private FncStatRptBriefDTO  readFncStatDtlExcel(String fileId, FncStatBase fncStatBase) {
		//读取excel
		POIFSFileSystem foiFileSystem = null;
		HSSFWorkbook book = null;
		try {
			FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
	    	FileInfoUtils.openDownloadStream(fileInfo);
			foiFileSystem = new POIFSFileSystem(FileInfoUtils.openDownloadStream(fileInfo));
			book = new HSSFWorkbook(foiFileSystem);
			int sheetNum = book.getNumberOfSheets();
			
			//获取财报配置数据
			String statPrdStyle = "";//报表周期类型
			HSSFCell tempCell = book.getSheetAt(0).getRow(0).getCell(1);
			for (FncStatPrdStyleEnum fncStatPrdStyleEnum : FncStatPrdStyleEnum.values()) {
				if (tempCell.getStringCellValue().contains(fncStatPrdStyleEnum.getDesc())) {
					statPrdStyle = fncStatPrdStyleEnum.getCode();
					break;
				}
			}
			if (!statPrdStyle.equals(fncStatBase.getStatPrdStyle())) {
				String errMsg = "导入财报中【周期类型】与选中记录不一致！";
				throw new BizException(null,ErrorConstants.NRCS_CMS_T20501113, null,errMsg);
			}
			
			tempCell = book.getSheetAt(0).getRow(1).getCell(1);
            String statPrd = null;
			if (tempCell.getCellType() == CellType.STRING ) {
                String tempValue = tempCell.getStringCellValue();
                statPrd = DateUtils.formatDate(DateUtils.parseDate(tempValue, "yyyy年MM月"), "yyyyMM");//报表期间

            } else {
                Date tempValue = tempCell.getDateCellValue();
                statPrd = DateUtils.formatDate(tempValue, "yyyyMM");//报表期间
            }

			if (statPrd == null || !statPrd.equals(fncStatBase.getStatPrd())) {
				String errMsg = "导入财报中【报表期间】与选中记录不一致！";
				throw new BizException(null,ErrorConstants.NRCS_CMS_T20501113, null,errMsg);
			}
			
			tempCell = book.getSheetAt(0).getRow(2).getCell(0);
			String tempValue = tempCell.getStringCellValue();
			String custId = tempValue.substring(tempValue.indexOf("(") + 1,tempValue.indexOf("["));//客户id
			if (custId == null || !custId.equals(fncStatBase.getCusId())) {
				String errMsg = "导入财报中【客户编号】与选中记录不一致！";
				throw new BizException(null,ErrorConstants.NRCS_CMS_T20501113, null,errMsg);
			}
			
			String statStyle = fncStatBase.getStatStyle();//报表口径
			
			Map<String, Object> fncStatMap = this.queryAllFncStatDtl(custId, statStyle, statPrdStyle, statPrd);
			Map<String, List<FncConfDefFmt>> itemsMap = (Map<String, List<FncConfDefFmt>>) fncStatMap.get("itemsMap");
			
			FncStatRptBriefDTO rptBriefDTO = new FncStatRptBriefDTO();
			List<FncStatRptDTO> rptDtoList = new ArrayList<FncStatRptDTO>();
			
			for (int i = 0 ; i < sheetNum ; i++) {
				HSSFSheet sheet = book.getSheetAt(i);
				//如果行数<=0
				if (sheet.getPhysicalNumberOfRows() <= 0) {
					continue;
				}
				String styleId = book.getSheetName(i).split("_")[1];
				//报表样式
				FncConfStyles fncConfStyles = fncConfStylesService.queryFncConfStylesByKey(styleId);
				//获取报表配置定义列表
				List<FncConfDefFmt> fmtList = itemsMap.get(styleId);
				
				//获得数据列数
				int dataCol = fncConfStyles.getFncConfDataCol();
				
				FncStatRptDTO rptDto = new FncStatRptDTO();
				rptDto.setCusId(custId);//客户代码
				rptDto.setStatStyle(statStyle);//报表口径
				rptDto.setStatPrdStyle(statPrdStyle);//报表周期类型
				rptDto.setStatPrd(statPrd);//报表期间
				//所属报表种类
				rptDto.setFncConfTyp(fncConfStyles.getFncConfTyp());
				//报表样式编号
				rptDto.setStatConfStyleId(styleId);
				
				List<FncStatRptItemDTO> items = new ArrayList<FncStatRptItemDTO>();
				//行号
				int rowIndex = 3;
				//栏位
				for (int k = 0 ; k < fmtList.size() ; k++) {
					FncConfDefFmt fmt = fmtList.get(k);
					if (k > 0 && fmtList.get(k -1).getFncConfCotes() != fmt.getFncConfCotes()) {
						rowIndex = 3;
					}
					rowIndex++;
					if (FncItemEditTypEnum.TITLE.getCode().equals(fmt.getFncItemEditTyp()) ||
							FncItemEditTypEnum.CALC.getCode().equals(fmt.getFncItemEditTyp())) {
                    	continue;
                    }
					
					FncStatRptItemDTO itemDto = new FncStatRptItemDTO();
					itemDto.setItemId(fmt.getItemId());
					//获取一行数据
					HSSFRow row = sheet.getRow(rowIndex);
					for (int j = 0 ; j < dataCol ; j++) {
						Cell cell = row.getCell(((fmt.getFncConfCotes().intValue() - 1) * (2 + dataCol)) + (2 + j));
						String value = null;
						if (CellType.NUMERIC == cell.getCellType()) {
							value = Double.toString(cell.getNumericCellValue());
						} else if (CellType.STRING == cell.getCellType()) {
							value = cell.getStringCellValue();
						}
						try {
							BeanUtils.setProperty(itemDto, "data" + (j + 1), value);
						} catch (IllegalAccessException
								| InvocationTargetException e) {
							logger.error("属性赋值报错：{}" , e.getLocalizedMessage());
						}
					}
					items.add(itemDto);
				}
				
				rptDto.setItems(items);
				rptDtoList.add(rptDto);
			}
			rptBriefDTO.setReportInfo(rptDtoList);
			return rptBriefDTO;
		} catch (FileSystemException | IOException e) {
			logger.error("read file error：{}", e.getMessage());
			throw new BizException(null,ErrorConstants.NRCS_CMS_T20501113, null,e.getMessage());
		} finally {
			if (book != null) {
				try {
					book.close();
				} catch (IOException e) {
					logger.error("close weekbook error：{}", e.getMessage());
				}
			}
			if (foiFileSystem != null) {
				try {
					foiFileSystem.close();
				} catch (IOException e) {
					logger.error("close weekbook error：{}", e.getMessage());
				}
			}
		}
    }

    /**
     * Excel 添加数据
     *
     * @param fncStatMap
     * @param os
     */
    @SuppressWarnings("unchecked")
    private void bindDataToExcel(Map<String, Object> fncStatMap, ByteArrayOutputStream os) {
        if (fncStatMap == null || fncStatMap.isEmpty()) {
            return;
        }
        List<FncConfStyles> confStylesList = (List<FncConfStyles>) fncStatMap.get("confStylesList");
        Map<String, List<FncConfDefFmt>> itemsMap = (Map<String, List<FncConfDefFmt>>) fncStatMap.get("itemsMap");
        FncStatBase fncStatBase = (FncStatBase) fncStatMap.get("statBase");
        // 拼接excel文件头
        Map<String, String> excelSheetTitle = this.setExcelSheetTitle(fncStatBase, confStylesList);
        // 拼接报表表头
        Map<String, List<String>> excelTitle = this.setFncStatExcelTitle(confStylesList, fncStatBase.getStatPrdStyle());
        // set data excel
        this.exportFncStatDtl2Excel(fncStatBase, confStylesList, itemsMap, excelTitle, excelSheetTitle, os);

    }

    /**
     * 构建数字单元格格式
     * @param workbook
     * @return
     */
    private HSSFCellStyle createNumberCellStyle(HSSFWorkbook workbook) {
    	HSSFCellStyle numberStyle = workbook.createCellStyle();
        numberStyle.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
        numberStyle.setBorderBottom(BorderStyle.THIN);
        numberStyle.setBorderTop(BorderStyle.THIN);
        numberStyle.setBorderLeft(BorderStyle.THIN);
        numberStyle.setBorderRight(BorderStyle.THIN);						
        numberStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
        numberStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFDataFormat format = workbook.createDataFormat();
		numberStyle.setDataFormat(format.getFormat("#,##0.00"));
		return numberStyle;
    }
    
    /**
     * 构建数字单元格格式
     * @param workbook
     * @return
     */
    private HSSFCellStyle createStringCellStyle(HSSFWorkbook workbook) {
    	HSSFCellStyle stringStyle = workbook.createCellStyle();
		stringStyle.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
		stringStyle.setBorderBottom(BorderStyle.THIN);
		stringStyle.setBorderTop(BorderStyle.THIN);
		stringStyle.setBorderLeft(BorderStyle.THIN);
		stringStyle.setBorderRight(BorderStyle.THIN);
		stringStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());
		stringStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return stringStyle;
    } 
    
    /**
     * 构建公式单元格格式
     * @param workbook
     * @return
     */
    private HSSFCellStyle createFormulaCellStyle(HSSFWorkbook workbook) {
    	HSSFCellStyle formulaStyle = workbook.createCellStyle();
		formulaStyle.setLocked(true);//不知为何没生效，先留在这里
		formulaStyle.setBottomBorderColor(HSSFColor.HSSFColorPredefined.BLACK.getIndex());
		formulaStyle.setBorderBottom(BorderStyle.THIN);
		formulaStyle.setBorderTop(BorderStyle.THIN);
		formulaStyle.setBorderLeft(BorderStyle.THIN);
		formulaStyle.setBorderRight(BorderStyle.THIN);						
		formulaStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.YELLOW.getIndex());
		formulaStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFDataFormat format = workbook.createDataFormat();
	    formulaStyle.setDataFormat(format.getFormat("#,##0.00"));
	    return formulaStyle;
    }
    /**
     * excel 初始化数据
     *
     * @param fncStatBase
     * @param confStylesList
     * @param itemsMap
     * @param excelTitle
     * @param excelSheetTitle
     * @param os
     */
    private void exportFncStatDtl2Excel(FncStatBase fncStatBase, List<FncConfStyles> confStylesList,
                                         Map<String, List<FncConfDefFmt>> itemsMap, Map<String, List<String>> excelTitle,
                                         Map<String, String> excelSheetTitle, OutputStream os) {
    	HSSFWorkbook workbook = null;
        try {
        	workbook = new HSSFWorkbook();
        	
            Map<CellType,HSSFCellStyle> cellTypeMap = new HashMap<CellType,HSSFCellStyle>();
            //标题样式
            HSSFCellStyle titleCellType = createStringCellStyle(workbook);
            titleCellType.setFillForegroundColor(HSSFColor.HSSFColorPredefined.LIGHT_ORANGE.getIndex());
            titleCellType.setAlignment(HorizontalAlignment.CENTER);
            titleCellType.setVerticalAlignment(VerticalAlignment.CENTER);
            cellTypeMap.put(CellType._NONE,titleCellType); 
            //数字格式样式
    		cellTypeMap.put(CellType.NUMERIC, createNumberCellStyle(workbook));
    		//字符格式样式
    		cellTypeMap.put(CellType.STRING, createStringCellStyle(workbook));
    		//公式样式
    	    cellTypeMap.put(CellType.FORMULA, createFormulaCellStyle(workbook));
    	    
    	    List<SheetVO> sheetVOs = new ArrayList<SheetVO>();
            for (int i = 0; i < confStylesList.size(); i++) {
                // 创建每页sheet
                HSSFSheet sheet = workbook.createSheet();
                FncConfStyles fncConfStyles = confStylesList.get(i);
                String sheetName = fncConfStyles.getFncConfDisName() + "_" + fncConfStyles.getStyleId();
                workbook.setSheetName(i, sheetName);
                // 设置宽度
                sheet.setDefaultColumnWidth(15);
                // 设置文件头、标题
                List<Object> resultList = this.setTitle(fncStatBase, sheet, fncConfStyles, excelSheetTitle, excelTitle, cellTypeMap);
                if (CollectionUtils.isEmpty(resultList)) {
                    continue;
                }
                // 添加数据
                Map<String,CellVO> cellVOMap = 
                		this.setData2ExcelSheet(cellTypeMap,resultList, fncConfStyles, itemsMap.get(fncConfStyles.getStyleId()));
                SheetVO sheetVo = new SheetVO();
                sheetVo.fncConfStyles = fncConfStyles;
                sheetVo.cellVoMap = cellVOMap;
                sheetVo.sheet = sheet;
                sheetVOs.add(sheetVo);
            }
            //处理所有含有计算公式的单元格
            handleFormulaCell(confStylesList, itemsMap, sheetVOs);
            workbook.write(os);
        } catch (IOException e) {
            logger.error(ErrorConstants.NRCS_CMS_T20501114, e);
            throw new YuspException(ErrorConstants.NRCS_CMS_T20501114, "财务报表信息导出错误");
        } finally {
        	if (workbook != null) {
        		try {
					workbook.close();
				} catch (IOException e) {
					logger.error("close workbook error:", e);
				}
        	}
        }
    }

    /**
     * 处理含有计算公式的单元格
     * @param confStylesList
     * @param itemsMap
     * @param sheetVoList
     */
    private void handleFormulaCell(List<FncConfStyles> confStylesList,
			Map<String, List<FncConfDefFmt>> itemsMap,
			List<SheetVO> sheetVoList) {
		for (FncConfStyles fncConfStyles : confStylesList) {
			List<FncConfDefFmt> itemList = itemsMap.get(fncConfStyles.getStyleId());
			if (CollectionUtils.isEmpty(itemList)) {
				continue;
			}
			itemList.forEach(item -> {
				handelSingleItem(item, sheetVoList);
			});
		}
	}

    //处理报表的单项
	private void handelSingleItem(FncConfDefFmt item, List<SheetVO> sheetVOs) {
		if (!FncItemEditTypEnum.CALC.getCode().equals(item.getFncItemEditTyp())) {
			return;
		}
		CellExtendVo cellExtendVo = seekCellByItemId(sheetVOs, item.getItemId());
		if (cellExtendVo == null) {
			return;
		}
		
		//列数
		int dataCol = cellExtendVo.getFncConfStyles().getFncConfDataCol();
		for (int j = 0 ; j < dataCol ; j++) {
			//结果表达式
			String retExp = formual(sheetVOs, item, "" + (j + 1));
			HSSFRow hssfRow = cellExtendVo.getSheet().getRow(cellExtendVo.getCellVO().cellrownum);
			HSSFCell hssfCell = hssfRow.getCell(cellExtendVo.getCellVO().cellcolnum + 1 + (j + 1));
			hssfCell.setCellFormula(retExp);
		}
	}

	/**
     * 遍历 赋值 sheet
     *
     * @param sheetList
     * @param fncConfStyles
     * @param items
     */
    private Map<String,CellVO> setData2ExcelSheet(Map<CellType,HSSFCellStyle> cellTypeMap, List<Object> sheetList, 
    		FncConfStyles fncConfStyles, List<FncConfDefFmt> items) {
        HSSFSheet sheet = (HSSFSheet) sheetList.get(0);
        // 每一栏列数
        int colNum = (int) sheetList.get(1);

        String fncConfTyp = fncConfStyles.getFncConfTyp();
        // 排序
        List<FncConfDefFmt> fmtListSorted = items.parallelStream()
                .sorted(Comparator.comparing(FncConfDefFmt::getFncConfOrder)).collect(Collectors.toList());
        // 根据栏数分组统计
        Map<Integer, List<FncConfDefFmt>> fncContMap = fmtListSorted.parallelStream()
                .collect(Collectors.groupingBy(FncConfDefFmt::getFncConfCotes));

        // sheet每一栏行数，升序存储
        Map<Integer, Long> calRowNumMap = this.calRowNum(items);

        HSSFRow row = null;
        
        Map<String,CellVO> cellVOMap = new HashMap<String, CellVO>();
        // 行次
        int rowNumTmp = 1;
        int rowNum = 0;
        // 遍历 简报表赋值sheet,资产负债类财务信息、财务分析指标
        if (FncConfTypEnum.BSS.getCode().equals(fncConfTyp) || FncConfTypEnum.FAS.getCode().equals(fncConfTyp)) {
            // 栏数
            for (int m = 0; m < fncConfStyles.getFncConfCotes(); m++) {
                List<FncConfDefFmt> fmtList = fncContMap.get(m + 1);
                if (null == fmtList || fmtList.size() == 0) {
                    continue;
                }
                rowNum = (int) (calRowNumMap.get(m + 1) + 0);
                // 行数
                for (int n = 0; n < rowNum; n++) {
                    row = sheet.getRow(4 + n);
                    if (row == null) {
                        row = sheet.createRow(4 + n);
                    }
                    HSSFCell cell = row.createCell(0 + colNum * m);
                    cell.setCellValue(parseItemName(fmtList.get(n)));
                    cell.setCellStyle(cellTypeMap.get(CellType.STRING));
                    
                    CellVO cellVo = new CellVO();
                    cellVo.cellvalue = fmtList.get(n).getItemId();
                    cellVo.cellrownum = row.getRowNum();
                    cellVo.cellcolnum = cell.getColumnIndex();
                    cellVOMap.put(fmtList.get(n).getItemId(), cellVo);
                    
                    if (FncItemEditTypEnum.TITLE.getCode().equals(fmtList.get(n).getFncItemEditTyp())) {
                    	createHSSFCell(cellTypeMap,row,1 + colNum * m, null,fmtList.get(n));
                    	continue;
                    }
                    createHSSFCell(cellTypeMap,row,1 + colNum * m, fmtList.get(n).getData1(),fmtList.get(n));
                }
            }
            return cellVOMap;
        }
        
        
        // 简报表 利润表类财务信息
        if (FncConfTypEnum.PSS.getCode().equals(fncConfTyp)) {
            List<FncConfDefFmt> fmtList = fncContMap.get(1);
            if (null == fmtList || fmtList.size() == 0) {
            	return cellVOMap;
            }
            rowNum = (int) (calRowNumMap.get(1) + 0);
            // 行数
            for (int n = 0; n < rowNum; n++) {
                row = sheet.getRow(4 + n);
                if (row == null) {
                    row = sheet.createRow(4 + n);
                }
                HSSFCell cell = row.createCell(0);
                cell.setCellValue(parseItemName(fmtList.get(n)));
                cell.setCellStyle(cellTypeMap.get(CellType.STRING));
                
                createHSSFCell(cellTypeMap,row,1, fmtList.get(n).getData1(),fmtList.get(n));
                if (FncItemEditTypEnum.TITLE.getCode().equals(fmtList.get(n).getFncItemEditTyp())) {
                	createHSSFCell(cellTypeMap,row,1, null,fmtList.get(n));
                	createHSSFCell(cellTypeMap,row,2, null,fmtList.get(n));
                	continue;
                }
                
                CellVO cellVo = new CellVO();
                cellVo.cellvalue = fmtList.get(n).getItemId();
                cellVo.cellrownum = row.getRowNum();
                cellVo.cellcolnum = cell.getColumnIndex();
                cellVOMap.put(fmtList.get(n).getItemId(), cellVo);
                
                
                createHSSFCell(cellTypeMap,row,2,fmtList.get(n).getData2(),fmtList.get(n));
                createHSSFCell(cellTypeMap,row,3,fmtList.get(n).getData3(),fmtList.get(n));
            }
            return cellVOMap;
        }

        // 普通报表一栏 3列数,包括项目、行次 2个固定列
        if (FncConfTypEnum.CFS.getCode().equals(fncConfTyp) || FncConfTypEnum.FI.getCode().equals(fncConfTyp)) {
            for (int i = 0; i < fncConfStyles.getFncConfCotes(); i++) {
                List<FncConfDefFmt> fmtList = fncContMap.get(i + 1);
                if (null == fmtList || fmtList.size() == 0) {
                    continue;
                }
                rowNum = (int) (calRowNumMap.get(i + 1) + 0);
                for (int j = 0; j < rowNum; j++) {
                    row = sheet.getRow(4 + j);
                    if (row == null) {
                        row = sheet.createRow(4 + j);
                    }
                    HSSFCell cell = row.createCell(0 + colNum * i);
                    cell.setCellValue(parseItemName(fmtList.get(j)));
                    cell.setCellStyle(cellTypeMap.get(CellType.STRING));
                    
                    HSSFCell cellFir = createHSSFCell(cellTypeMap,row,1 + colNum * i , "-1",fmtList.get(j));
                    if ("1".equals(fmtList.get(j).getFncConfRowFlg())) {
                        cellFir.setCellValue(rowNumTmp);
                        rowNumTmp++;
                    }
                    if (FncItemEditTypEnum.TITLE.getCode().equals(fmtList.get(j).getFncItemEditTyp())) {
                    	createHSSFCell(cellTypeMap,row,2 + colNum * i, null, fmtList.get(j));
                    	continue;
                    }
                    CellVO cellVo = new CellVO();
                    cellVo.cellvalue = fmtList.get(j).getItemId();
                    cellVo.cellrownum = row.getRowNum();
                    cellVo.cellcolnum = cell.getColumnIndex();
                    cellVOMap.put(fmtList.get(j).getItemId(), cellVo);
                    
                    createHSSFCell(cellTypeMap,row,2 + colNum * i, fmtList.get(j).getData1(), fmtList.get(j));
                }
            }
            return cellVOMap;
        }
        // 普通报表 一栏 4列数,包括项目、行次 2个固定列
        for (int m = 0; m < fncConfStyles.getFncConfCotes(); m++) {
            List<FncConfDefFmt> fmtList = fncContMap.get(m + 1);
            if (null == fmtList || fmtList.size() == 0) {
                continue;
            }
            rowNum = (int) (calRowNumMap.get(m + 1) + 0);
            for (int n = 0; n < rowNum; n++) {
            	FncConfDefFmt item = fmtList.get(n);
                row = sheet.getRow(4 + n);
                if (row == null) {
                    row = sheet.createRow(4 + n);
                }
                HSSFCell cell = row.createCell(0 + colNum * m);
                cell.setCellValue(parseItemName(item));
                cell.setCellStyle(cellTypeMap.get(CellType.STRING));
                
                HSSFCell cellFir = createHSSFCell(cellTypeMap,row,1 + colNum * m , "-1", item);
                if ("1".equals(fmtList.get(n).getFncConfRowFlg())) {
                    cellFir.setCellValue(rowNumTmp);
                    rowNumTmp++;
                }
                
                if (FncItemEditTypEnum.TITLE.getCode().equals(item.getFncItemEditTyp())) {
                	createHSSFCell(cellTypeMap,row,2 + colNum * m, null,item);
                    createHSSFCell(cellTypeMap,row,3 + colNum * m, null,item);
                	continue;
                }
                CellVO cellVo = new CellVO();
                cellVo.cellvalue = fmtList.get(n).getItemId();
                cellVo.cellrownum = row.getRowNum();
                cellVo.cellcolnum = cell.getColumnIndex();
                cellVOMap.put(fmtList.get(n).getItemId(), cellVo);
                
                createHSSFCell(cellTypeMap,row,2 + colNum * m, item.getData1(),item);
                createHSSFCell(cellTypeMap,row,3 + colNum * m, item.getData2(),item);
            }
        }
        return cellVOMap;
    }
    
    //创建单元格
    private HSSFCell createHSSFCell(Map<CellType,HSSFCellStyle> cellTypeMap,HSSFRow row,int columnIndex,String data, FncConfDefFmt item) {
    	HSSFCell cell = row.createCell(columnIndex);
    	if (FncItemEditTypEnum.TITLE.getCode().equals(item.getFncItemEditTyp()) || ("-1").equals(data)){
    		cell.setCellStyle(cellTypeMap.get(CellType.STRING));
    		cell.setCellType(CellType.BLANK);
    	} else if (FncItemEditTypEnum.CALC.getCode().equals(item.getFncItemEditTyp())) {
    		cell.setCellStyle(cellTypeMap.get(CellType.FORMULA));
    	} else {
    		cell.setCellStyle(cellTypeMap.get(CellType.NUMERIC));
    		cell.setCellType(CellType.NUMERIC);
    		if (StringUtils.isNotEmpty(data)) {
    			cell.setCellValue(Double.parseDouble(data));
    		}
    	}
    	return cell;
    } 
    
    //获取项目名称
    private String parseItemName (FncConfDefFmt item) {
    	StringBuffer blank = new StringBuffer();
    	for (int i = 0 ; i < item.getFncConfIndent().intValue() ; i++) {
    		blank.append(" ");
    	}
    	blank.append(item.getFncConfPrefix() == null ? "" : item.getFncConfPrefix());
    	blank.append(item.getItemName());
    	return blank.toString();
    }
    
    private String formual(List<SheetVO> sheetVOs , FncConfDefFmt defFmt, String subItemId) {
    	String calFrm = defFmt.getFncConfCalFrm();
    	return digestCalExp(sheetVOs, calFrm, subItemId);
    }
    
    //寻找计算公式中itemId的位置
    private CellExtendVo seekCellByItemId(List<SheetVO> sheetVoList, String itemId){
    	Iterator<SheetVO> iter = sheetVoList.iterator();
    	while (iter.hasNext()) {
    		SheetVO sheetVO = iter.next();
    		Map<String, CellVO> cellVo = sheetVO.cellVoMap;
    		if (cellVo != null && cellVo.containsKey(itemId)) {
    			CellExtendVo vo = new CellExtendVo();
    			vo.setCellVO(cellVo.get(itemId));
    			vo.setFncConfStyles(sheetVO.fncConfStyles);
    			vo.setSheet(sheetVO.sheet);
    			return vo;
    		}
    	}
    	return null;
	}
    
    public String digestCalExp(List<SheetVO> sheetVOList, String calExp, String subItemId) {
		StringBuffer retExp = new StringBuffer(); 
		/** 分解并得到表达式中每个项 */
		String[] expArray = calExp.split("");
		for(int n = 0; n < expArray.length; n++){
			if (!expArray[n].equals("{")) {
				retExp.append(expArray[n]);
				continue;
			}
			int index = calExp.indexOf("}", n);
			String itemExp = calExp.substring(n , index);
			
			//寻找confTyp
			String expConfTyp = peekFncConfTyp(itemExp);
			if (StringUtils.isNotEmpty(expConfTyp)) {
				FncConfStyles expStyles = seekFncConfStyles(sheetVOList, expConfTyp);
				if (expStyles == null) {
					throw new RuntimeException("报表配置类型ConfTyp【" + expConfTyp + "】在表格中不存在");
				}
				retExp.append(expStyles.getFncConfDisName() + "_" + expStyles.getStyleId());
				retExp.append("!");
			}
			
			//寻找itemId
			String expItemId = peekItemId(itemExp);
			String expSubItemId = peekSubItemId(itemExp);
            if(expSubItemId != null && !expSubItemId.trim().equals("")){ //公式中有指定则用优先用公式中
                subItemId = expSubItemId;
            }
			String excelExp = this.transCalExp2ExcelFormula(expItemId, subItemId, sheetVOList);
			if(excelExp == null || excelExp.trim().equals("")){
				//如果有一个公式项转换失败，则整个公式转换失败
				return null;
			}
			retExp.append(excelExp);
			n = index;
		}
		
		return retExp.toString();
	}
    
    private FncConfStyles seekFncConfStyles(List<SheetVO> sheetVoList, String fncConfTyp) {
    	Iterator<SheetVO> iter = sheetVoList.iterator();
    	while (iter.hasNext()) {
    		SheetVO vo = iter.next();
    		if (vo.fncConfStyles.getFncConfTyp().equals(fncConfTyp)) {
    			return vo.fncConfStyles;
    		}
    	}
    	return null;
    }
    
    private static String peekFncConfTyp(String itemExp){
    	String[] items = itemExp.split("\\.");
    	if(items.length == 3){
    		return items[0].substring(items[0].indexOf("[")+1, items[0].indexOf("]"));
    	} else if(items.length == 1){
    		return "";
    	}
    	
    	return null;
    }

	private static String peekItemId(String ItemExp){
		String[] items = ItemExp.split("\\.");
		if(items.length == 3){
			return items[1].substring(items[1].indexOf("[")+1, items[1].indexOf("]"));
		}else if(items.length == 1){
			return items[0].substring(items[0].indexOf("[")+1, items[0].indexOf("]"));
		}
		
		return null;
	}    
	
	private static String peekSubItemId(String ItemExp){
    	String[] items = ItemExp.split("\\.");
    	if(items.length == 3 ){
    		return items[2].substring(items[2].indexOf("[")+1, items[2].indexOf("]"));
    	} else if(items.length == 1){
    		return "";
    	}
    	return null;
    }
	
	private String transCalExp2ExcelFormula(String ItemId, String SubitemId,List<SheetVO> sheetVoList) {
		/**  根据报表项ID与报表子项ID确定当前公式所在Sheets中的Cell */
		//根据报表项ID得到相对位置
		CellExtendVo extendItem = this.seekCellByItemId(sheetVoList, ItemId);
		if(extendItem == null){
			logger.error("未能根据报表项ItemId【" + ItemId + "】定位到报表项");
			return null;
		}
		int row = extendItem.getCellVO().cellrownum + 1;
		int col = extendItem.getCellVO().cellcolnum + 1;
		//将0、1、2、...转成对应Excel的A、B、C、....   注：AB、AC...  这种格式不支持
		col += Integer.parseInt(SubitemId);
		char col4Excel = 'A' ;
		col4Excel += col;
		return col4Excel + "" + row;
	}

    /**
     * 设置excel文件头和标题 title
     *
     * @param fncStatBase
     * @param sheet
     * @param fncConfStyles
     * @param excelFileTitle
     * @param excelTitle
     * @return
     */
    private List<Object> setTitle(FncStatBase fncStatBase, HSSFSheet sheet, FncConfStyles fncConfStyles,
                                  Map<String, String> excelFileTitle, Map<String, List<String>> excelTitle, Map<CellType,HSSFCellStyle> cellTypeMap) {

    	HSSFCellStyle cellStyle = cellTypeMap.get(CellType.STRING);
        List<Object> resultList = new ArrayList<>(2);
        String statPrd = fncStatBase.getStatPrd();
        String orgId = fncStatBase.getInputBrId();
        String cusId = fncStatBase.getCusId();
        String cusName = fncStatBase.getCusName();
        String inputDate = fncStatBase.getInputDate();

        // 设置title
        String fileTitleStr = excelFileTitle.get(fncConfStyles.getStyleId());
        List<String> titleStr = excelTitle.get(fncConfStyles.getStyleId());
        // 一栏列数
        int colNum = titleStr.size();
        HSSFRow row = null;
        // 赋值excel文件头,前三行
        
        List<Integer> skipColumns = new ArrayList<Integer>();
        
        // 第一行
        row = sheet.createRow(0);
        HSSFCell cell = row.createCell(1);
        cell.setCellValue(fileTitleStr);
        cell.setCellStyle(cellStyle);
        skipColumns.add(1);
        fillBlankCell(row, skipColumns, fncConfStyles, colNum, cellStyle);
        skipColumns.clear();
        
        
        // 第二行
        row = sheet.createRow(1);
        HSSFCell cellFirst = row.createCell(1);
        String conStr = statPrd.substring(0, 4) + FncStatConstants.FNC_STAT_EXCEL_YEAR + statPrd.substring(4, 6)
                + FncStatConstants.FNC_STAT_EXCEL_MONTH;
        cellFirst.setCellValue(conStr);
        cellFirst.setCellStyle(cellStyle);
        skipColumns.add(1);
        fillBlankCell(row, skipColumns, fncConfStyles, colNum, cellStyle);
        skipColumns.clear();
        
        // 第三行
        row = sheet.createRow(2);
        HSSFCell cellTwo = row.createCell(0);
        skipColumns.add(0);
        cellTwo.setCellValue(FncStatConstants.FNC_STAT_EXCEL_UNIT + orgId + "(" + cusId + "[" + cusName + "])");
        cellTwo.setCellStyle(cellStyle);
        HSSFCell cellThre = row.createCell(1);
        skipColumns.add(1);
        cellThre.setCellValue(FncStatConstants.FNC_STAT_EXCEL_WRITE_DATE + inputDate);
        cellThre.setCellStyle(cellStyle);
        HSSFCell cellFou = row.createCell(2);
        skipColumns.add(2);
        cellFou.setCellValue(FncStatConstants.FNC_STAT_EXCEL_UNIT_MONEY);
        cellFou.setCellStyle(cellStyle);
        fillBlankCell(row, skipColumns, fncConfStyles, colNum, cellStyle);
        skipColumns.clear();
        
        // 第四行，列表标题项,普通报表
        row = sheet.createRow(3);
        HSSFCellStyle titleCellType = cellTypeMap.get(CellType._NONE);
        for (int i = 0; i < fncConfStyles.getFncConfCotes(); i++) {
            for (int j = 0; j < colNum; j++) {
                HSSFCell cellTmp = row.createCell(i * colNum + j);
                cellTmp.setCellValue(titleStr.get(j));
                cellTmp.setCellStyle(titleCellType);
            }
        }
        resultList.add(sheet);
        resultList.add(colNum);
        return resultList;
    }
    
    private void fillBlankCell(HSSFRow row, List<Integer> skipColumns, FncConfStyles fncConfStyles, int colNum, HSSFCellStyle cellStyle) {
    	for (int i = 0; i < fncConfStyles.getFncConfCotes(); i++) {
            for (int j = 0; j < colNum; j++) {
            	int columnIndex = i * colNum + j;
            	if (skipColumns.contains(columnIndex)) {
            		continue;
            	}
                HSSFCell cellTmp = row.createCell(columnIndex);
                cellTmp.setCellStyle(cellStyle);
            }
        }
    }

    /**
     * 统计每一栏的行数
     *
     * @param itemsMap
     * @return
     */
    private Map<Integer, Long> calRowNum(List<FncConfDefFmt> itemsMap) {

        // 分组统计 栏数
        Map<Integer, Long> fncContMap = itemsMap.stream()
                .collect(Collectors.groupingBy(FncConfDefFmt::getFncConfCotes, Collectors.counting()));
        // 分组，计数和排序
        Map<Integer, Long> calRowNumMap = new LinkedHashMap<>();
        fncContMap.entrySet().stream().sorted(Map.Entry.<Integer, Long>comparingByValue().reversed())
                .forEachOrdered(e -> calRowNumMap.put(e.getKey(), e.getValue()));
        return calRowNumMap;
    }

    /**
     * 拼接 excel文件头
     *
     * @param fncStatBase
     * @param confStylesList
     * @return
     */
    private Map<String, String> setExcelSheetTitle(FncStatBase fncStatBase, List<FncConfStyles> confStylesList) {
        Map<String, String> excelTitleMap = new HashMap<>(confStylesList.size());
        String statPrdStyle = fncStatBase.getStatPrdStyle();
        // 月报
        if (statPrdStyle.equals(FncStatPrdStyleEnum.MONTH.getCode())) {
            for (int i = 0; i < confStylesList.size(); i++) {
                String fncConfDisName = confStylesList.get(i).getFncConfDisName();
                String styldId = confStylesList.get(i).getStyleId();
                excelTitleMap.put(styldId, fncConfDisName + "(" + FncStatPrdStyleEnum.MONTH.getDesc() + ")");
            }
            return excelTitleMap;
        }
        // 季报
        if (statPrdStyle.equals(FncStatPrdStyleEnum.SEASON.getCode())) {
            for (int j = 0; j < confStylesList.size(); j++) {
                String fncConfDisName = confStylesList.get(j).getFncConfDisName();
                String styldId = confStylesList.get(j).getStyleId();
                excelTitleMap.put(styldId, fncConfDisName + "(" + FncStatPrdStyleEnum.SEASON.getDesc() + ")");
            }
            return excelTitleMap;
        }
        // 半年报
        if (statPrdStyle.equals(FncStatPrdStyleEnum.HALFY.getCode())) {
            for (int m = 0; m < confStylesList.size(); m++) {
                String fncConfDisName = confStylesList.get(m).getFncConfDisName();
                String styldId = confStylesList.get(m).getStyleId();
                excelTitleMap.put(styldId, fncConfDisName + "(" + FncStatPrdStyleEnum.HALFY.getDesc() + ")");
            }
            return excelTitleMap;
        }
        // 年报
        for (int n = 0; n < confStylesList.size(); n++) {
            String fncConfDisName = confStylesList.get(n).getFncConfDisName();
            String styldId = confStylesList.get(n).getStyleId();
            excelTitleMap.put(styldId, fncConfDisName + "(" + FncStatPrdStyleEnum.YEAR.getDesc() + ")");
        }
        return excelTitleMap;
    }

    /**
     * 设置excel 标题项
     *
     * @param confStylesList
     * @return
     */
    private Map<String, List<String>> setFncStatExcelTitle(List<FncConfStyles> confStylesList, String prdStyle) {
        Map<String, List<String>> excelFileTitleMap = new HashMap<>(confStylesList.size());
        for (int i = 0; i < confStylesList.size(); i++) {
            String fncConfTyp = confStylesList.get(i).getFncConfTyp();
            String styleId = confStylesList.get(i).getStyleId();
            int dataCol = confStylesList.get(i).getFncConfDataCol();
            List<String> titleList = new ArrayList<>();

            // 资产负债类财务信息
            if (FncConfTypEnum.BSS.getCode().equals(fncConfTyp)) {
                titleList.add("项目");
                titleList.add("期末数");
                dataCol = 0;
            }
            // 利润表类财务信息
            if (FncConfTypEnum.PSS.getCode().equals(fncConfTyp)) {
                titleList.add("项目");
                titleList.add("上二年累计数");
                titleList.add("上年累计数");
                titleList.add("累计数");
                dataCol = 0;
            }
            // 财务分析指标
            if (FncConfTypEnum.FAS.getCode().equals(fncConfTyp)) {
                titleList.add("项目");
                titleList.add("数值");
                dataCol = 0;
            }
            // 普通报表
            switch (dataCol) {
                case 1:
                    if ("04".equals(fncConfTyp)) {
                        titleList.add("项目");
                        titleList.add("行次");
                        titleList.add("指标比率(%)");
                    } else {
                        titleList.add("项目");
                        titleList.add("行次");
                        titleList.add("金额");
                    }
                    break;
                case 2:
                    if ("02".equals(fncConfTyp) || "03".equals(fncConfTyp)) {
                        titleList.add("项目");
                        titleList.add("行次");
                        titleList.add("本期数");
                        titleList.add("本年累计数");
                    } else {
                        if (prdStyle != null && prdStyle.equals("4")) {
                            titleList.add("项目");
                            titleList.add("行次");
                            titleList.add("期初数");
                            titleList.add("期末数");
                        } else {
                            titleList.add("项目");
                            titleList.add("行次");
                            titleList.add("年初数");
                            titleList.add("本期数");
                        }
                    }
                    break;
                default:
                    break;
            }
            excelFileTitleMap.put(styleId, titleList);
        }
        return excelFileTitleMap;
    }

    @Transactional
    public Integer tempSaveFncStatDtlBrief(FncStatRptBriefDTO fncStatRptDtos) {
        for (FncStatRptDTO fncStatRptDto : fncStatRptDtos.getReportInfo()) {
            tempSaveFncStatDtl(fncStatRptDto);
        }
        return 1;
    }

    @Transactional
    public ResultDto saveFncStatDtlBrief(FncStatRptBriefDTO fncStatRptDtos) {
        for (FncStatRptDTO fncStatRptDto : fncStatRptDtos.getReportInfo()) {
            ResultDto singleResult = saveFncStatDtl(fncStatRptDto);
            if (!"0".equals(singleResult.getCode()))
                return singleResult;
        }
        return ResultDto.success(1);
    }
    
    @Transactional
    public ResultDto saveFncStatDtlBrief(FncStatRptBriefDTO fncStatRptDtos,boolean check) {
        for (FncStatRptDTO fncStatRptDto : fncStatRptDtos.getReportInfo()) {
            ResultDto singleResult = saveFncStatDtl(fncStatRptDto,check);
            if (!"0".equals(singleResult.getCode()))
                return singleResult;
        }
        return ResultDto.success(1);
    }

    public Map<String, Object> queryCapitalCalcData(String cusId) {
        Map<String, Object> result = Maps.newConcurrentMap();
        String lastYear = String.valueOf(DateUtils.getCurrYear() - 1);
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId", cusId);
        queryModel.addCondition("statPrd", lastYear + "%");
        queryModel.setSort("stat.statPrd desc");
        List<FncStatBase> fncStatBaseList = fncStatBaseMapper.selectByModel(queryModel);
        if (null == fncStatBaseList || fncStatBaseList.size() <= 0)
            return result;
        FncStatBase fncStatBase = fncStatBaseList.get(0);
        // <简表>
        String briefFlag = fncStatBase.getBriefFlag();
        result.put("briefFlag", briefFlag);
        if ("1".equals(briefFlag)) {
            return queryCapitalCalcBrief(result, fncStatBase);
        }
        // </简表>
        String statPrd = fncStatBase.getStatPrd();
        // 查询上年12月报表
        if (!statPrd.equals(lastYear + "12")) {
            return result;
        }
        String statStyle = fncStatBase.getStatStyle();
        String statPrdStyle = fncStatBase.getStatPrdStyle();
        //期初数据字段
        String statInitAmt = null;
        //期末数据字段
        String statEndAmt = null;
        switch (FncStatPrdStyleEnum.byCode(statPrdStyle)) {
            case YEAR:
                statInitAmt = "stat_init_amt_y";
                statEndAmt = "stat_end_amt_y";
                break;
            case HALFY:
                statInitAmt = "stat_init_amt_y2";
                statEndAmt = "stat_end_amt_y2";
                break;
            case SEASON:
                statInitAmt = "stat_init_amt_q4";
                statEndAmt = "stat_end_amt_q4";
                break;
            case MONTH:
                statInitAmt = "stat_init_amt12";
                statEndAmt = "stat_end_amt12";
                break;
            default:
                break;
        }
        // 查询测算需要字段
        Map<String, String> calcMap = Maps.newConcurrentMap();
        // 报表期间
        calcMap.put("statPrd", fncStatBase.getStatPrd());
        /**
         * 01、上年销售收入：取损益表主营业务收入（损益表_通用SL_01——FNC_STAT_IS的fnc_501）
         */
        String fnc01 = StringUtils.EMPTY;
        List<HashMap<String, BigDecimal>> stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_501",
                FncConfTypEnum.IS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            fnc01 = String.format("%.2f", stat.get(0).get("dataend"));
        }
        calcMap.put("fnc01", fnc01);
        /**
         * 02、销售收入平均增长率：销售收入年增长率=（本年销售收入-上年销售收入）/上年销售收入*100%
         *                       取损益表主营业务收入（损益表_通用SL_01——FNC_STAT_IS的fnc_501）
         */
        String fnc05 = StringUtils.EMPTY;
        stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_501",
                FncConfTypEnum.IS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            BigDecimal datastart = stat.get(0).get("datastart");
            BigDecimal dataend = stat.get(0).get("dataend");
            if (datastart.compareTo(new BigDecimal(0)) != 0) {
                fnc05 = String.format("%.2f", dataend.divide(datastart, 4, BigDecimal.ROUND_HALF_UP).subtract(new BigDecimal(1)));
            }
        }
        calcMap.put("fnc05", fnc05);
        /**
         * 03、上年销售利润总额：取损益表利润总额（损益表_通用SL_01——FNC_STAT_IS的fnc_515）
         */
        String fnc06 = StringUtils.EMPTY;
        stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_515",
                FncConfTypEnum.IS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            fnc06 = String.format("%.2f", stat.get(0).get("dataend").doubleValue());
        }
        calcMap.put("fnc06", fnc06);
        /**
         * 04、平均应收账款：取资产负债表应收账款（资产负债表_通用SZ_01——FNC_STAT_BS的fnc_104）
         */
        String fnc07 = StringUtils.EMPTY;
        stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_104",
                FncConfTypEnum.BS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            fnc07 = avg(stat);
        }
        calcMap.put("fnc07", fnc07);
        /**
         * 05、平均预收账款：取资产负债表中预收账款（资产负债表_通用SZ_01——FNC_STAT_BS的fnc_204）
         */
        String fnc08 = StringUtils.EMPTY;
        stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_204",
                FncConfTypEnum.BS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            fnc08 = avg(stat);
        }
        calcMap.put("fnc08", fnc08);
        /**
         * 06、平均存货：取资产负债表存货（资产负债表_通用SZ_01——FNC_STAT_BS的fnc_110）
         */
        String fnc09 = StringUtils.EMPTY;
        stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_110",
                FncConfTypEnum.BS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            fnc09 = avg(stat);
        }
        calcMap.put("fnc09", fnc09);
        /**
         * 07、平均预付账款：取资产负债表中预付账款（资产负债表_通用SZ_01——FNC_STAT_BS的fnc_107）
         */
        String fnc10 = StringUtils.EMPTY;
        stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_107",
                FncConfTypEnum.BS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            fnc10 = avg(stat);
        }
        calcMap.put("fnc10", fnc10);
        /**
         * 08、平均应付账款：取资产负债表中应付账款（资产负债表_通用SZ_01——FNC_STAT_BS的fnc_203）
         */
        String fnc11 = StringUtils.EMPTY;
        stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_203",
                FncConfTypEnum.BS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            fnc11 = avg(stat);
        }
        calcMap.put("fnc11", fnc11);
        /**
         * 09、销售成本：取损益表主营业务成本期末数（损益表_通用SL_01——FNC_STAT_IS的fnc_502）
         */
        String fnc12 = StringUtils.EMPTY;
        stat = fncStatBaseMapper.queryItemValuePair(cusId, statStyle, lastYear, "fnc_502",
                FncConfTypEnum.IS.getTableName(), statInitAmt, statEndAmt);
        if (null != stat && stat.size() > 0) {
            fnc12 = String.format("%.2f", stat.get(0).get("dataend").doubleValue());
        }
        calcMap.put("fnc12", fnc12);
        result.put("calcData", calcMap);
        return result;
    }

    private String avg(List<HashMap<String, BigDecimal>> stat) {
        double datastart = stat.get(0).get("datastart").doubleValue();
        double dataend = stat.get(0).get("dataend").doubleValue();
        return String.format("%.2f", (datastart + dataend) / 2);
    }

    private Map<String, Object> queryCapitalCalcBrief(Map<String, Object> result, FncStatBase fncStatBase) {
        Map<String, Object> calcMap = Maps.newConcurrentMap();
        calcMap.put("statBase", fncStatBase);

        String styleId;

        List<FncConfStyles> confStylesList = Lists.newArrayList();
        Map<String, List<FncConfDefFmt>> itemsMap = Maps.newConcurrentMap();

        for (FncConfTypEnum confTypEnum : FncConfTypEnum.values()) {
            switch (confTypEnum) {
                case BSS:
                    styleId = fncStatBase.getStatBssStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                case PSS:
                    styleId = fncStatBase.getStatPssStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                case FAS:
                    styleId = fncStatBase.getStatFasStyleId();
                    queryOneRpt(confStylesList, itemsMap, styleId, fncStatBase, confTypEnum.getCode());
                    break;
                default:
                    break;
            }
        }

        calcMap.put("confStylesList", confStylesList);
        calcMap.put("itemsMap", itemsMap);
        result.put("calcData", calcMap);
        return result;
    }

    public static void main(String[] args) {
        try{
            Interpreter interpreter = new Interpreter();
            interpreter.set("checkResult", "true");


            String bshFormula ="Float.parseFloat(\"36684996.81\")==Float.parseFloat(\"38471950.78\")+Float.parseFloat(\"0.00\")+Float.parseFloat(\"-1786953.97\")-Float.parseFloat(\"36684996.81\")+Float.parseFloat(\"36684996.81\")";
            interpreter.eval("try{checkResult=(" + bshFormula + ");}catch(ArithmeticException e){checkResult=false;}");
            Boolean checkResult = (Boolean) interpreter.get("checkResult");

            System.out.println("checkResult:"+checkResult);
            String aaa ="Double.parseDouble(\"38471950.78\")+Double.parseDouble(\"0.00\")+Double.parseDouble(\"-1786953.97\")-Double.parseDouble(\"36684996.81\")+Double.parseDouble(\"36684996.81\")";
            interpreter.eval("try{checkResult=(" + aaa + ");}catch(ArithmeticException e){checkResult=false;}");
            Double checkResultDouble =(Double)interpreter.get("checkResult");
            System.out.println("checkResultFloat:"+checkResultDouble);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
