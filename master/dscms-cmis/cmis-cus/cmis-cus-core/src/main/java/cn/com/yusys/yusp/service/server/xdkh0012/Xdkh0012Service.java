package cn.com.yusys.yusp.service.server.xdkh0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusCorpStock;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs18.Irs18ReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0012.req.Xdkh0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0012.resp.Xdkh0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusCorpStockMapper;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.Dscms2IrsClientService;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0012Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0012Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdkh0012Service.class);


    @Resource
    private CusBaseMapper cusBaseMapper;

    @Resource
    private CusCorpMapper cusCorpMapper;

    @Resource
    private CusCorpStockMapper cusCorpStockMapper;

    @Resource
    private Dscms2IrsClientService dscms2IrsClientService;

    @Resource
    private AdminSmUserService adminSmUserService; // 用户信息
    @Resource
    private AdminSmOrgService adminSmOrgService; // 机构信息

    /**
     * 对公客户信息同步
     *
     * @param xdkh0012DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0012DataRespDto getXdkh0012(Xdkh0012DataReqDto xdkh0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012DataReqDto));
        Xdkh0012DataRespDto xdkh0012DataRespDto = new Xdkh0012DataRespDto();

        //客户基本信息
        CusBase cusBase = new CusBase();
        //对公客户基本信息
        CusCorp cusCorp = new CusCorp();
        //对公发行股票信息
        CusCorpStock cusCorpStock = new CusCorpStock();

        try {
            //客户编号
            String cusId = xdkh0012DataReqDto.getCusId();
            if (StringUtils.isEmpty(cusId)) {
                throw BizException.error(null, EcsEnum.ECS040002.key, EcsEnum.ECS040002.value);
            }
            //客户状态
            String cusStatus = xdkh0012DataReqDto.getCusStatus();
            if (!Objects.equals(CmisCusConstants.STD_ECIF_CUS_STATE_01, cusStatus) && !Objects.equals(CmisCusConstants.STD_ECIF_CUS_STATE_04, cusStatus) && cusStatus != null) {
                //客户基本信息同步
                BeanUtils.copyProperties(xdkh0012DataReqDto, cusBase);
                //字典项映射
                //证件类型
                //String certType = PUBUtilTools.changeCertTypeFromEcif(xdkh0012DataReqDto.getCertType());
                String certType = xdkh0012DataReqDto.getCertType(); //20211117-00056
                //证件号码
                String certCode = xdkh0012DataReqDto.getCertCode();
                //组织机构代码
                String cropcd = xdkh0012DataReqDto.getCropcd();
                //客户名称
                String cusName = xdkh0012DataReqDto.getCusName();
                //登记注册号
                String gvrgno = xdkh0012DataReqDto.getGvrgno();
                if (StringUtils.nonBlank(certType)) {
                    cusBase.setCertType(certType);
                }
                cusBase.setCusState(null);
                if (CmisCusConstants.STD_ZB_CERT_TYP_20.equals(certType)) {
                    certCode = cropcd;
                } else if (CmisCusConstants.STD_ZB_CERT_TYP_24.equals(certType)) {
                    certCode = gvrgno;
                }
                if (StringUtils.nonBlank(certCode)) {
                    cusBase.setCertCode(certCode);
                }
                if (StringUtils.nonBlank(cusName)) {
                    cusBase.setCusName(cusName);
                }
                cusBaseMapper.updateByPrimaryKeySelective(cusBase);
                //对公客户基本信息
                //BeanUtils.copyProperties(xdkh0012DataReqDto, cusCorp);
                cusCorp.setCusId(xdkh0012DataReqDto.getCusId());
                //字典项映射
                //注册资本/开办资金币种
                String regiCurType = PUBUtilTools.changeCurTypeFromEcif(xdkh0012DataReqDto.getRegiCurType());
                if (StringUtils.nonBlank(regiCurType)) {
                    cusCorp.setRegiCurType(regiCurType);
                }
                if(xdkh0012DataReqDto.getRegiCapAmt()!=null){
                    cusCorp.setRegiCapAmt(xdkh0012DataReqDto.getRegiCapAmt().multiply(new BigDecimal(10000))); //注册资本
                }

                //实收资本币种
                String paidCapCurType = PUBUtilTools.changeCurTypeFromEcif(xdkh0012DataReqDto.getPaidCapCurType());
                if (StringUtils.nonBlank(paidCapCurType)) {
                    cusCorp.setPaidCapCurType(paidCapCurType);
                }
                if(xdkh0012DataReqDto.getPaidCapAmt()!=null){
                    cusCorp.setPaidCapAmt(xdkh0012DataReqDto.getPaidCapAmt().multiply(new BigDecimal(10000))); //实收资本
                }
                //企业规模
                String corpScale = xdkh0012DataReqDto.getCorpScale();
                if (StringUtils.nonBlank(corpScale)) {
                    cusCorp.setCorpScale(corpScale);
                }
                //经营场地所有权
                String operPlaceOwnshp = xdkh0012DataReqDto.getOperPlaceOwnshp();
                if (StringUtils.nonBlank(operPlaceOwnshp)) {
                    cusCorp.setOperPlaceOwnshp(operPlaceOwnshp);
                }
                //经营状况
                String operStatus = PUBUtilTools.changeBusiStateFromEcif(xdkh0012DataReqDto.getOperStatus());
                if (StringUtils.nonBlank(operStatus)) {
                    cusCorp.setOperStatus(operStatus);
                }
                //是否我行股东
                String isBankShd = xdkh0012DataReqDto.getIsBankShd();
                if (StringUtils.nonBlank(isBankShd)) {
                    cusCorp.setIsBankShd(isBankShd);
                }
                //是否上市企业
                String isStockCorp = xdkh0012DataReqDto.getIsStockCorp();
                if (StringUtils.nonBlank(isStockCorp)) {
                    cusCorp.setIsStockCorp(isStockCorp);
                }
                //地区重点企业
                String areaPriorCopr = xdkh0012DataReqDto.getAreaPriorCopr();
                if (StringUtils.nonBlank(areaPriorCopr)) {
                    cusCorp.setAreaPriorCorp(areaPriorCopr);
                }
                //是否政府投融资平台
                String goverInvestPlat = xdkh0012DataReqDto.getGoverInvestPlat();
                if (StringUtils.nonBlank(goverInvestPlat)) {
                    cusCorp.setGoverInvestPlat(goverInvestPlat);
                }
                //是否钢贸企业
                String isSteelCus = xdkh0012DataReqDto.getIsSteelCus();
                if (StringUtils.nonBlank(isSteelCus)) {
                    cusCorp.setIsSteelCus(isSteelCus);
                }
                //特种经营标识
                String spOperFlag = xdkh0012DataReqDto.getSpOperFlag();
                if (StringUtils.nonBlank(spOperFlag)) {
                    cusCorp.setSpOperFlag(spOperFlag);
                }
                //进出口权标志
                String impexpFlag = xdkh0012DataReqDto.getImpexpFlag();
                if (StringUtils.nonBlank(impexpFlag)) {
                    cusCorp.setImpexpFlag(impexpFlag);
                }
                //基本账户存款开户日期
                String basicAccNoOpenDate = PUBUtilTools.changeGjDate2CMIS(xdkh0012DataReqDto.getBasicAccNoOpenDate());
                if (StringUtils.nonBlank(basicAccNoOpenDate)) {
                    cusCorp.setBasicAccNoOpenDate(basicAccNoOpenDate);
                }
                //成立日期
                String bulidDate = PUBUtilTools.changeGjDate2CMIS(xdkh0012DataReqDto.getBulidDate());
                if (StringUtils.nonBlank(bulidDate)) {
                    cusCorp.setBuildDate(bulidDate);
                }
                //首次与我行建立信贷关系时间
                String initLoanDate = PUBUtilTools.changeGjDate2CMIS(xdkh0012DataReqDto.getInitLoanDate());
                if (StringUtils.nonBlank(initLoanDate)) {
                    cusCorp.setInitLoanDate(initLoanDate);
                }
                //国别
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getRgcuty())) {
                    cusCorp.setCountry(xdkh0012DataReqDto.getRgcuty());
                }
                //组织机构代码
                if (StringUtils.nonBlank(cropcd)) {
                    cusCorp.setInsCode(cropcd);
                }
                //员工总数
                if (Integer.valueOf(Optional.ofNullable(xdkh0012DataReqDto.getEmplnm()).orElse(BigDecimal.ZERO).toString()) != 0) {
                    cusCorp.setFjobNum(Integer.valueOf(Optional.ofNullable(xdkh0012DataReqDto.getEmplnm()).orElse(BigDecimal.ZERO).toString()));
                }
                //组织机构代码失效日期
                String insEndDate = PUBUtilTools.changeGjDate2CMIS(xdkh0012DataReqDto.getOreddt());
                if (StringUtils.nonBlank(insEndDate)) {
                    cusCorp.setInsEndDate(insEndDate);
                }
                //登记注册号
                if (StringUtils.nonBlank(cropcd)) {
                    cusCorp.setRegiCode(cropcd);
                }
                //主管单位
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getComput())) {
                    cusCorp.setAdminOrg(xdkh0012DataReqDto.getComput());
                }
                //注册登记地址
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getRegadr())) {
                    cusCorp.setRegiAddr(xdkh0012DataReqDto.getRegadr());
                }
                //兼营范围
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getCommonOperPro())) {
                    cusCorp.setPartOptScp(xdkh0012DataReqDto.getCommonOperPro());
                }
                //注册登记地址
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getGvtion())) {
                    cusCorp.setRegiAddr(xdkh0012DataReqDto.getGvtion());
                }
                //国税税务登记代码
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getNttxno())) {
                    cusCorp.setNatTaxRegCode(xdkh0012DataReqDto.getNttxno());
                }
                //国税税务登记机关
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getSwtion())) {
                    cusCorp.setNatTaxRegOrg(xdkh0012DataReqDto.getSwtion());
                }
                //国税税务登记日期
                String natTaxRegDt = PUBUtilTools.changeGjDate2CMIS(xdkh0012DataReqDto.getSwbgdt());
                if (StringUtils.nonBlank(natTaxRegDt)) {
                    cusCorp.setNatTaxRegDt(natTaxRegDt);
                }
                //国税登记有效期
                String natTaxRegEndDt = PUBUtilTools.changeGjDate2CMIS(xdkh0012DataReqDto.getSweddt());
                if (StringUtils.nonBlank(natTaxRegEndDt)) {
                    cusCorp.setNatTaxRegEndDt(natTaxRegEndDt);
                }
                //地税税务登记代码
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getNttxno())) {
                    cusCorp.setLocTaxRegCode(xdkh0012DataReqDto.getNttxno());
                }
                //地税税务登记机关
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getSwtion())) {
                    cusCorp.setLocTaxRegOrg(xdkh0012DataReqDto.getSwtion());
                }
                //地税税务登记日期
                if (StringUtils.nonBlank(natTaxRegEndDt)) {
                    cusCorp.setLocTaxRegDt(natTaxRegDt);
                }
                //地税登记有效期
                if (StringUtils.nonBlank(natTaxRegEndDt)) {
                    cusCorp.setLocTaxRegEndDt(natTaxRegEndDt);
                }
                //经营场地面积(平方米)
                if (xdkh0012DataReqDto.getWkflar() != null) {
                    cusCorp.setOperPlaceSqu(xdkh0012DataReqDto.getWkflar());
                }
                //实际经营地址
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getBusiad())) {
                    cusCorp.setOperAddrAct(xdkh0012DataReqDto.getBusiad());
                }
                //是否苏州综合平台企业
                String iisSzjrfwCrop = xdkh0012DataReqDto.getPlattg();
                if (StringUtils.nonBlank(iisSzjrfwCrop)) {
                    cusCorp.setIisSzjrfwCrop(iisSzjrfwCrop);
                }
                //老信贷逻辑
                //基本存款账户开户许可证核准号
                String basicDepAccNoOpenLic = xdkh0012DataReqDto.getBasicDepAccNoOpenLic();
                if (basicDepAccNoOpenLic != null && basicDepAccNoOpenLic.length() == 14) {
                    cusCorp.setBasicDepAccNoOpenLic(basicDepAccNoOpenLic);
                } else {
                    cusCorp.setBasicDepAccNoOpenLic(null);
                }
                if (StringUtils.nonBlank(cusName)) {
                    cusCorp.setCusName(cusName);
                }
                //对公客户类型细分
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getCorpType())) {
                    cusCorp.setCusType(xdkh0012DataReqDto.getCorpType());
                }
                cusCorpMapper.updateByPrimaryKeySelective(cusCorp);
                //对公客户发行股票信息同步
                cusCorpStock.setCusId(xdkh0012DataReqDto.getCusId());
                //是否上市公司
                if (StringUtils.nonBlank(isStockCorp)) {
                    cusCorpStock.setStockCprtFlag(isStockCorp);
                }
                //股票代码
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getStoccd())) {
                    cusCorpStock.setStkCode(xdkh0012DataReqDto.getStoccd());
                }
                //上市地
                if (StringUtils.nonBlank(xdkh0012DataReqDto.getListld())) {
                    cusCorpStock.setStkMrkPlace(xdkh0012DataReqDto.getListld());
                }
                cusCorpStockMapper.updateByPrimaryKeySelective(cusCorpStock);
                /** 20211123-00155
                // 发送更新数据至内评系统（调用接口IRS18）
                CusBase cusBase1 = cusBaseMapper.selectByPrimaryKey(cusId);
                Irs18ReqDto irs18ReqDto = new Irs18ReqDto();
                irs18ReqDto.setCustid(cusId);
                if (cusBase1 != null) {
                    irs18ReqDto.setOrgid(cusBase1.getManagerId());
                    irs18ReqDto.setUsername(cusBase1.getManagerBrId());
                    ResultDto<AdminSmUserDto> resultUserDto  = adminSmUserService.getByLoginCode(cusBase1.getManagerId());
                    if (ResultDto.success().getCode().equals(resultUserDto.getCode())) {
                        AdminSmUserDto adminSmUserDto = resultUserDto.getData();
                        if (!Objects.isNull(adminSmUserDto)) {
                            irs18ReqDto.setUserid1(adminSmUserDto.getUserName());
                        }
                    }
                    ResultDto<AdminSmOrgDto> resultOrgDto = adminSmOrgService.getByOrgCode(cusBase1.getManagerBrId());
                    if (ResultDto.success().getCode().equals(resultOrgDto.getCode())) {
                        AdminSmOrgDto adminSmOrgDto = resultOrgDto.getData();
                        if (!Objects.isNull(adminSmOrgDto)) {
                            irs18ReqDto.setUserid1(adminSmOrgDto.getOrgName());
                        }
                    }
                    dscms2IrsClientService.irs18(irs18ReqDto);
                 }*/
                }
            xdkh0012DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_S);
            xdkh0012DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_S);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, e.getMessage());
            xdkh0012DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xdkh0012DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, e.getMessage());
            xdkh0012DataRespDto.setOpFlag(CmisCusConstants.OP_FLAG_F);
            xdkh0012DataRespDto.setOpMsg(CmisCusConstants.OP_MSG_F);
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012DataRespDto));
        return xdkh0012DataRespDto;
    }
}
