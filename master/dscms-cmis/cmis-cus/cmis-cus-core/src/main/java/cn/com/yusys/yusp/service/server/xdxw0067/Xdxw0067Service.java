package cn.com.yusys.yusp.service.server.xdxw0067;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0067.req.Xdxw0067DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0067.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0067.resp.Xdxw0067DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstYndJyxxAppMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0067Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0067Service.class);

    @Autowired
    private CusLstYndJyxxAppMapper cusLstYndJyxxAppMapper;

    /**
     * 调查基本信息查询
     *
     * @param xdxw0067DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0067DataRespDto xdxw0067(Xdxw0067DataReqDto xdxw0067DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067DataReqDto));
        Xdxw0067DataRespDto xdxw0067DataRespDto = new Xdxw0067DataRespDto();

        String certCode = xdxw0067DataReqDto.getCert_code();
        String status = xdxw0067DataReqDto.getApprove_status();
        String prdcode = xdxw0067DataReqDto.getPrd_code();
        String serno = xdxw0067DataReqDto.getSurvey_serno();

        try {
            Map queryMap = new HashMap();
            queryMap.put("certCode", certCode);//证件号
            queryMap.put("status", status);//
            queryMap.put("prdcode", prdcode);//
            queryMap.put("serno", serno);//
           java.util.List<List> list = cusLstYndJyxxAppMapper.selectcusLstYndByModel(queryMap);
            xdxw0067DataRespDto.setList(list);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067DataRespDto));
        return xdxw0067DataRespDto;
    }
}
