package cn.com.yusys.yusp.reportconf.repository.mapper;

import cn.com.yusys.yusp.fncstat.domain.RptItemBean;
import org.apache.ibatis.annotations.Param;
import cn.com.yusys.yusp.reportconf.domain.FncConfStyles;
import cn.com.yusys.yusp.commons.mapper.CommonMapper;

import java.util.List;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfStylesMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-20 18:19:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface FncConfStylesMapper extends CommonMapper<FncConfStyles> {
	public int dynamicUpdate(RptItemBean rptItemBean);
	public int dynamicUpdateAndData(RptItemBean rptItemBean);
	public int findOneFncConfStyles(RptItemBean rptItemBean);
	public int addOneFncConfStyles(@Param("sql") String sql);
	List<FncConfStyles> selectAll();
}