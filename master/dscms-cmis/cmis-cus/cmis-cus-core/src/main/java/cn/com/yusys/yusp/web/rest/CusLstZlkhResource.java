/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.CusLstDedkkhVo;
import cn.com.yusys.yusp.vo.CusLstJjycExportVo;
import cn.com.yusys.yusp.vo.CusLstZlkhExportVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstZlkh;
import cn.com.yusys.yusp.service.CusLstZlkhService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstZlkhResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:17:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstzlkh")
public class CusLstZlkhResource {
    @Autowired
    private CusLstZlkhService cusLstZlkhService;
    private final Logger logger = LoggerFactory.getLogger(CusLstJjycResource.class);
	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstZlkh>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstZlkh> list = cusLstZlkhService.selectAll(queryModel);
        return new ResultDto<List<CusLstZlkh>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstZlkh>> index(@RequestBody QueryModel queryModel) {
        List<CusLstZlkh> list = cusLstZlkhService.selectByModel(queryModel);
        return new ResultDto<List<CusLstZlkh>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<CusLstZlkh>> queryList(@RequestBody QueryModel queryModel) {
        List<CusLstZlkh> list = cusLstZlkhService.selectByModel(queryModel);
        return new ResultDto<List<CusLstZlkh>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstZlkh> show(@PathVariable("serno") String serno) {
        CusLstZlkh cusLstZlkh = cusLstZlkhService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstZlkh>(cusLstZlkh);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstZlkh> create(@RequestBody CusLstZlkh cusLstZlkh) throws URISyntaxException {
        cusLstZlkhService.insert(cusLstZlkh);
        return new ResultDto<CusLstZlkh>(cusLstZlkh);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstZlkh cusLstZlkh) throws URISyntaxException {
        int result = cusLstZlkhService.update(cusLstZlkh);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstZlkhService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstZlkhService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:updateSelective
     * @函数描述:启用/停用
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CusLstZlkh cusLstZlkh) {
        int result = cusLstZlkhService.updateSelective(cusLstZlkh);
        return ResultDto.success(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = cusLstZlkhService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库，StudentScore为导入数据的类
        try {
            ExcelUtils.syncImport(CusLstZlkhExportVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return cusLstZlkhService.insertInBatch(dataList);
            }), true);
        } catch (Exception e) {
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }
}
