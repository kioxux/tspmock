/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusGrpApp;
import cn.com.yusys.yusp.domain.CusGrpMemberApp;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.dto.CusGrpMemberRelDto;
import cn.com.yusys.yusp.repository.mapper.CusGrpMemberRelMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpMemberRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-10 10:46:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusGrpMemberRelService {
    private static final Logger log = LoggerFactory.getLogger(CusBizAssignAppService.class);
    @Autowired
    private CusGrpMemberRelMapper cusGrpMemberRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusGrpMemberRel selectByPrimaryKey(String pkId) {
        return cusGrpMemberRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusGrpMemberRel> selectAll(QueryModel model) {
        List<CusGrpMemberRel> records = (List<CusGrpMemberRel>) cusGrpMemberRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusGrpMemberRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusGrpMemberRel> list = cusGrpMemberRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusGrpMemberRel record) {
        return cusGrpMemberRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusGrpMemberRel record) {
        return cusGrpMemberRelMapper.insertSelective(record);
    }


    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusGrpMemberRel record) {
        return cusGrpMemberRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusGrpMemberRel record) {
        return cusGrpMemberRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * 根据集团编号+成员客户编号 删除数据
     * @方法名称: deleteByGrpnoAndCusid
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByGrpnoAndCusid(CusGrpMemberRel record) {
        return cusGrpMemberRelMapper.deleteByGrpnoAndCusid(record);
    }

    /**
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusGrpMemberRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusGrpMemberRelMapper.deleteByIds(ids);
    }



    /**
     * @方法名称: createCusGrpMemRelByCusMemApp
     * @方法描述: 根据集团申请成员信息，组装集团成员信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusGrpMemberRel createCusGrpMemRelByCusMemApp(CusGrpMemberApp cusGrpMemberApp, CusGrpApp cusGrpApp) {
        User userInfo = SessionUtils.getUserInformation();
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        String nowTime = DateUtils.getCurrentDate(DateFormatEnum.DATETIME);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        CusGrpMemberRel cusGrpMemberRel = new CusGrpMemberRel();

        Date currTime = null ;
        try{
            currTime = simpleDateFormat.parse(nowTime) ;
        } catch (ParseException e) {
            log.error(e.getMessage(),e);
        }

        BeanUtils.copyProperties(cusGrpMemberApp, cusGrpMemberRel);
        //集团名称插入
        cusGrpMemberRel.setGrpName(cusGrpApp.getGrpName());
        //集团关联关系描述 重置 申请和正式表不一致
        cusGrpMemberRel.setGrpCorreDec(cusGrpMemberApp.getGrpCorreDetail());

        //主管机构，主管客户经理处理
        cusGrpMemberRel.setManagerBrId(cusGrpMemberApp.getManagerBrId());
        cusGrpMemberRel.setManagerId(cusGrpMemberApp.getManagerId());

        //是否我行主客户
        if (CmisCusConstants.STD_ZB_GRP_CORRE_TYPE_1.equals(cusGrpMemberApp.getGrpCorreType()) ||
                CmisCusConstants.STD_ZB_GRP_CORRE_TYPE_3.equals(cusGrpMemberApp.getGrpCorreType())) {
            cusGrpMemberRel.setIsMainCus(CmisCommonConstants.YES_NO_1);
        } else {
            cusGrpMemberRel.setIsMainCus(CmisCommonConstants.YES_NO_0);
        }
        //是否有效
        cusGrpMemberRel.setAvailableInd(CmisCusConstants.STD_AVAILABLE_IND_1);
        cusGrpMemberRel.setSociCredCode("");
        //登记人登记机构处理
        cusGrpMemberRel.setInputId(userInfo.getLoginCode());
        cusGrpMemberRel.setInputBrId(userInfo.getOrg().getCode());
        cusGrpMemberRel.setInputDate(nowDate);
        cusGrpMemberRel.setUpdId(userInfo.getLoginCode());
        cusGrpMemberRel.setUpdBrId(userInfo.getOrg().getCode());
        cusGrpMemberRel.setUpdDate(nowDate);
        cusGrpMemberRel.setCreateTime(currTime);
        cusGrpMemberRel.setUpdateTime(currTime);
        return cusGrpMemberRel;
    }

    /**
     * @方法名称: updateByContinionAvai
     * @方法描述: 根据parmamap中的参数更新数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByContinionAvai(Map paramMap) {return cusGrpMemberRelMapper.updateByContinionAvai(paramMap) ;}

    /**
     * @方法名称: queryCusGrpMemberRelDtoList
     * @方法描述: 根据集团客户编号查询成员列表信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-15 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public ResultDto<List<CusGrpMemberRelDto>> queryCusGrpMemberRelDtoList(CusGrpMemberRelDto cusGrpMemberRelDto){
        return ResultDto.success().data(cusGrpMemberRelMapper.queryCusGrpMemberRelDtoList(cusGrpMemberRelDto.getGrpNo()));
    }

    /**
     * 根据queryModel查询集团编号
     * @param queryModel
     * @return
     */
    public Map<String,String> queryGrpNoByQueryModel(QueryModel queryModel){
        return cusGrpMemberRelMapper.queryGrpNoByQueryModel(queryModel);
    }

    /**
     * 根据集团编号查询成员列表信息
     * @param grpNo
     * @return
     */
    public List<Map<String,String>> queryGrpMemberListByGrpNo(String grpNo){
        return cusGrpMemberRelMapper.queryGrpMemberListByGrpNo(grpNo);
    }

    /**
     * @方法名称: queryGrpNoByParams
     * @方法描述: 根据集团客户编号查询成员列表信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: qw
     * @创建时间: 2021-07-23 20:11:11
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CusGrpMemberRel queryGrpNoByParams(Map map){
        return cusGrpMemberRelMapper.queryGrpNoByParams(map);
    }

    /**
     * 根据集团编号查询成员客户编号
     * @param grpNo
     * @return
     */
    public String queryGrpMemberCusIdByGrpNo(String grpNo){
        return cusGrpMemberRelMapper.queryGrpMemberCusIdByGrpNo(grpNo);
    }
}
