package cn.com.yusys.yusp.demo.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class TransactionService {
    private static final Logger log = LoggerFactory.getLogger(TransactionService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(rollbackFor = {Exception.class})
    public void process() throws Exception {
        String insertSql = "INSERT INTO test_bfp(id, username, usersex, userheight, " +
                "cert_type, cert_no, income, marriage, work, create_org) " +
                "VALUES ('666666', '666666', '666666', 666666, '666666', '666666', " +
                "'666666', '666666', '666666', '666666')";
        int num = jdbcTemplate.update(insertSql);
        log.info("插入{}条记录", num);

        String selectSql = "SELECT * FROM test_bfp WHERE id = '666666'";
        List<Map<String, Object>> reuslt = jdbcTemplate.queryForList(selectSql);
        log.info("查询结果:{}", reuslt);

        throw new Exception();
    }
}
