package cn.com.yusys.yusp.web.server.xdkh0018;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0018.req.Xdkh0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0018.resp.Xdkh0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0018.Xdkh0018Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:临时客户信息维护
 *
 * @author zhugenrong
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDKH0018:临时客户信息维护")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0018Resource.class);
    @Autowired
    private Xdkh0018Service xdkh0018Service;

    /**
     * 交易码：xdkh0018
     * 交易描述：临时客户信息维护
     *
     * @param xdkh0018DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdkh0018:临时客户信息维护")
    @PostMapping("/xdkh0018")
    protected @ResponseBody
    ResultDto<Xdkh0018DataRespDto> xdkh0018(@Validated @RequestBody Xdkh0018DataReqDto xdkh0018DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value, JSON.toJSONString(xdkh0018DataReqDto));
        Xdkh0018DataRespDto xdkh0018DataRespDto = new Xdkh0018DataRespDto();// 响应Dto:临时客户信息维护
        ResultDto<Xdkh0018DataRespDto> xdkh0018DataResultDto = new ResultDto<>();
        try {
            xdkh0018DataRespDto = xdkh0018Service.xdkh0018(xdkh0018DataReqDto);
            // 封装xdkh0018DataResultDto中正确的返回码和返回信息
            xdkh0018DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0018DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (YuspException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value, e.getMessage());
            // 封装xdkh0018DataResultDto中异常返回码和返回信息
            xdkh0018DataResultDto.setCode(e.getCode());
            xdkh0018DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value, e.getMessage());
            // 封装xdkh0018DataResultDto中异常返回码和返回信息
            xdkh0018DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0018DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0018DataRespDto到xdkh0018DataResultDto中
        xdkh0018DataResultDto.setData(xdkh0018DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value, JSON.toJSONString(xdkh0018DataRespDto));
        return xdkh0018DataResultDto;
    }
}
