/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.dto.CusComGradeDto;
import cn.com.yusys.yusp.dto.CusComGradeDto2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusComGrade;
import cn.com.yusys.yusp.service.CusComGradeService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusComGradeResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 20:41:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuscomgrade")
public class CusComGradeResource {
    @Autowired
    private CusComGradeService cusComGradeService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusComGrade>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusComGrade> list = cusComGradeService.selectAll(queryModel);
        return new ResultDto<List<CusComGrade>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusComGrade>> index(QueryModel queryModel) {
        List<CusComGrade> list = cusComGradeService.selectByModel(queryModel);
        return new ResultDto<List<CusComGrade>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusComGrade> show(@PathVariable("serno") String serno) {
        CusComGrade cusComGrade = cusComGradeService.selectByPrimaryKey(serno);
        return new ResultDto<CusComGrade>(cusComGrade);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusComGrade> create(@RequestBody CusComGrade cusComGrade) throws URISyntaxException {
        cusComGradeService.insert(cusComGrade);
        return new ResultDto<CusComGrade>(cusComGrade);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusComGrade cusComGrade) throws URISyntaxException {
        int result = cusComGradeService.update(cusComGrade);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusComGradeService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusComGradeService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<CusComGradeDto> query(@RequestBody String serno) {
        CusComGrade cusComGrade = cusComGradeService.selectByPrimaryKey(serno);
        CusComGradeDto cusComGradeDto = new CusComGradeDto();
        BeanUtils.beanCopy(cusComGrade,cusComGradeDto);
        return new ResultDto<CusComGradeDto>(cusComGradeDto);
    }

    /**
     * 根据客户号查询最新的客户评级信息
     * @param cusId
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/selectGradeInfoByCusId")
    protected ResultDto<Map<String,String>> selectGradeInfoByCusId(@RequestBody String cusId) {
        return new ResultDto<Map<String,String>>(cusComGradeService.selectGradeInfoByCusId(cusId));
    }

    /**
     * 根据客户号查询客户评级信息
     * @param cusId
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/getGradeInfoByCusId")
    protected ResultDto<Map<String,String>> getGradeInfoByCusId(@RequestBody String cusId) {
        return new ResultDto<Map<String,String>>(cusComGradeService.getGradeInfoByCusId(cusId));
    }

    /**
     * 根据客户号查询客户及其集团成员评级信息
     * @param queryModel
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/querygradeinfobycusid")
    protected ResultDto<List<CusComGrade>> queryGradeInfoByCusId(@RequestBody QueryModel queryModel) {
        String cusId = (String)queryModel.getCondition().get("cusId");
        return new ResultDto<List<CusComGrade>>(cusComGradeService.queryGradeInfoByCusId(cusId));
    }

    /**
     * 根据客户号查询集团成员的客户评级信息
     * @param cusId
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/querycuscomgrade")
    protected ResultDto<List<CusComGradeDto2>> queryCusComGrade(@RequestBody String cusId) {
        return new ResultDto<>(cusComGradeService.queryCusComGrade(cusId));
    }
}
