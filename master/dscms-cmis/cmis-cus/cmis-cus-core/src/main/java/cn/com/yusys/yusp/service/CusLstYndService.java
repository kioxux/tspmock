/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CusLstGlf;
import cn.com.yusys.yusp.domain.CusLstYndApp;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.vo.CusLstGlfVo;
import cn.com.yusys.yusp.vo.CusLstYndVo;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstYnd;
import cn.com.yusys.yusp.repository.mapper.CusLstYndMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 09:50:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYndService {

    @Autowired
    private CusLstYndMapper cusLstYndMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstYnd selectByPrimaryKey(String serno) {
        return cusLstYndMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYnd> selectAll(QueryModel model) {
        List<CusLstYnd> records = (List<CusLstYnd>) cusLstYndMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstYnd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYnd> list = cusLstYndMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstYnd record) {
        return cusLstYndMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstYnd record) {
        return cusLstYndMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstYnd record) {
        return cusLstYndMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstYnd record) {
        return cusLstYndMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstYndMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYndMapper.deleteByIds(ids);
    }


    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusLstYndVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * @方法名称: asyncExportList
     * @方法描述: 异步导出
     * @参数与返回说明: 导出进度信息
     * @算法描述: 无
     */
    public ProgressDto asyncExportList(QueryModel model) {
        DataAcquisition dataAcquisition = (size, page, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(size);
            queryModeTemp.setSize(page);
            PageHelper.startPage(model.getPage(), model.getSize());
            List<CusLstYnd> list = cusLstYndMapper.selectByModel(model);
            PageHelper.clearPage();
            return list;
        };
        ExportContext exportContext = ExportContext.of(CusLstYndVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }
    /**
     * 批量插入
     * @param  cusLstYndVoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> cusLstYndVoList) {
        User userInfo = SessionUtils.getUserInformation();
        List<CusLstYndVo> cusBatCusLstGlf = (List<CusLstYndVo>) BeanUtils.beansCopy(cusLstYndVoList, CusLstYndVo.class);
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CusLstYndMapper cusLstYndMapper = sqlSession.getMapper(CusLstYndMapper.class);

            for (CusLstYndVo cusLstYndVo : cusBatCusLstGlf) {
                CusLstYnd clg = new CusLstYnd();
                BeanUtils.beanCopy(cusLstYndVo, clg);
                QueryModel qm = new QueryModel();
                qm.getCondition().put("cusId",cusLstYndVo.getCusId());
                qm.getCondition().put("listStatus", "03");
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                clg.setInputBrId(userInfo.getOrg().getCode());
                clg.setInputId(userInfo.getLoginCode());
                clg.setInputDate(date);
                clg.setCreateTime(date1);
                clg.setUpdateTime(date1);
                clg.setUpdId(userInfo.getLoginCode());
                clg.setUpdBrId(userInfo.getOrg().getCode());
                clg.setListStatus("03");
                clg.setOprType("01");
                clg.setCusId(cusLstYndVo.getCusId());
                List<CusLstYnd> yndList = cusLstYndMapper.selectByModel(qm);
                if (yndList != null && yndList.size()>0) {
                    yndList.stream().forEach(ded -> {
                        clg.setSerno(ded.getSerno());
                        cusLstYndMapper.updateByPrimaryKey(clg);
                    });
                } else {
                    clg.setSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>()));
                    cusLstYndMapper.insertSelective(clg);
                }
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return cusBatCusLstGlf == null ? 0 : cusBatCusLstGlf.size();
    }
    /**
     * @param cusLstYnd
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/7/21 9:18
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto addCusLstYnd(CusLstYnd cusLstYnd) {
        String serno = cusLstYnd.getSerno();
        //查询该对象是否存在
        CusLstYnd queryCusLstYndApp = cusLstYndMapper.selectByPrimaryKey(serno);
        if (null == queryCusLstYndApp) {
            cusLstYnd.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            cusLstYndMapper.insert(cusLstYnd);
        } else {
            cusLstYndMapper.updateByPrimaryKey(cusLstYnd);
        }
        return new ResultDto(cusLstYnd);
    }
}
