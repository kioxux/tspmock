package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CusBizAarApp;
import cn.com.yusys.yusp.domain.CusBizAarCusLst;

import java.util.List;

public class CusBizAarAppCusLstDto {
    private List<CusBizAarCusLst> list;
    private CusBizAarApp cusBizAarApp;

    public List<CusBizAarCusLst> getList() {
        return list;
    }

    public void setList(List<CusBizAarCusLst> list) {
        this.list = list;
    }

    public CusBizAarApp getCusBizAarApp() {
        return cusBizAarApp;
    }

    public void setCusBizAarApp(CusBizAarApp cusBizAarApp) {
        this.cusBizAarApp = cusBizAarApp;
    }
}
