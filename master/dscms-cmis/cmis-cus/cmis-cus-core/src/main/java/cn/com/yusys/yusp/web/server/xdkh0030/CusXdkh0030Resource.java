package cn.com.yusys.yusp.web.server.xdkh0030;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0030.req.Xdkh0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0030.resp.Xdkh0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0030.Xdkh0030Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 接口处理类:公司客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0030:公司客户评级相关信息同步")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0030Resource.class);

    @Autowired
    private Xdkh0030Service xdkh0030Service;

    /**
     * 交易码：xdkh0030
     * 交易描述：公司客户评级相关信息同步
     *
     * @param xdkh0030DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("公司客户评级相关信息同步")
    @PostMapping("/xdkh0030")
    protected @ResponseBody
    ResultDto<Xdkh0030DataRespDto> xdkh0030(@Validated @RequestBody Xdkh0030DataReqDto xdkh0030DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, JSON.toJSONString(xdkh0030DataReqDto));
        Xdkh0030DataRespDto xdkh0030DataRespDto = new Xdkh0030DataRespDto();// 响应Dto:公司客户评级相关信息同步
        ResultDto<Xdkh0030DataRespDto> xdkh0030DataResultDto = new ResultDto<>();
        try {
            // 从xdkh0030DataReqDto获取业务值进行业务逻辑处理
            String cusId = xdkh0030DataReqDto.getCusId();//客户编号
            String lockStatus = xdkh0030DataReqDto.getLockStatus();//锁定状态
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0030DataReqDto));
            xdkh0030DataRespDto = xdkh0030Service.xdkh0030(xdkh0030DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0030DataRespDto));
            // 封装xdkh0030DataResultDto中正确的返回码和返回信息
            xdkh0030DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0030DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, e.getMessage());
            // 封装xdkh0030DataResultDto中异常返回码和返回信息
            xdkh0030DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0030DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0030DataRespDto到xdkh0030DataResultDto中
        xdkh0030DataResultDto.setData(xdkh0030DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, JSON.toJSONString(xdkh0030DataResultDto));
        return xdkh0030DataResultDto;
    }
}