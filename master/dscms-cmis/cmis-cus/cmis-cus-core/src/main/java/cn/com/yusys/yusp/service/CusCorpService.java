/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.RelArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.fncstat.domain.FncStatBase;
import cn.com.yusys.yusp.fncstat.repository.mapper.FncStatBsMapper;
import cn.com.yusys.yusp.fncstat.service.FncStatBaseService;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.PUBUtilTools;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 乐友先生
 * @创建时间: 2021-03-22 11:22:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusCorpService {

    private static final Logger log = LoggerFactory.getLogger(CusCorpService.class);

    @Autowired
    private CusCorpMapper cusCorpMapper;

    @Autowired
    private CusBaseMapper cusBaseMapper;

    @Autowired
    private SequenceTemplateService sequenceTemplateService;

    @Autowired
    private CusBaseService cusBaseService;

    @Autowired
    private CusCorpMgrService cusCorpMgrService;

    @Autowired
    private CusPubRelInvestService cusPubRelInvestService;

    @Autowired
    private CusCorpApitalService cusCorpApitalService;

    @Autowired
    private CusCorpService cusCorpService;

    @Autowired
    private CusManaTaskService cusManaTaskService;

    @Autowired
    private Dscms2IrsClientService dscms2IrsClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private FncStatBaseService fncStatBaseService;

    @Autowired
    private FncStatBsMapper fncStatBsMapper;

    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CusIntbankService cusIntbankService;

	@Autowired
    private CusCorpStockService cusCorpStockService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusCorp selectByPrimaryKey(String cusId) {
        return cusCorpMapper.selectByPrimaryKey(cusId);
    }

    /**
     * @方法名称: selectCropAndBaseByPrimarKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusCorp selectCropAndBaseByPrimarKey(String cusId) {
        return cusCorpMapper.selectCropAndBaseByPrimarKey(cusId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusCorp> selectAll(QueryModel model) {
        List<CusCorp> records = cusCorpMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusCorp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusCorp> list = cusCorpMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryCuscrop
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String, Object>> queryCuscrop(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = cusCorpMapper.queryCuscrop(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryCuscropBank
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:银企合作信息
     * @算法描述: 无
     */

    public List<Map<String, Object>> queryCuscropBank(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = cusCorpMapper.queryCuscropBank(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusCorp record) {
        return cusCorpMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusCorp record) {
        return cusCorpMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusCorp record) {
        return cusCorpMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusCorp record) {
        return cusCorpMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: batchUpdateSelective
     * @方法描述: 根据主键更新 - 更新多个字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor = Exception.class)
    public int batchUpdateSelective(List<CusCorp> cusCorpList) {
        for (CusCorp cusCorp : cusCorpList) {
            int rows = cusCorpMapper.updateByPrimaryKeySelective(cusCorp);
            if (rows != 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return -1;
            }
        }
        return 1;
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId) {
        return cusCorpMapper.deleteByPrimaryKey(cusId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusCorpMapper.deleteByIds(ids);
    }


    /**
     * @函数名称:saveTemp
     * @函数描述:客户创建暂存
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional(rollbackFor = Exception.class)
    public int saveTemp(CusCorpDto cusCorpDto) {
        log.info("{}保存对公客户信息:{}", Thread.currentThread().getName(), cusCorpDto);
        CusCorp cusCorp = new CusCorp();
        BeanUtils.copyProperties(cusCorpDto, cusCorp);
        if(cusCorpMapper.selectByPrimaryKey(cusCorp.getCusId()) != null){
            String scale = cusCorpService.checkComScaleOp(cusCorp.getCusId(),cusCorp.getTradeClass());
            log.info("维护客户信息-获取客户编号【" + cusCorp.getCusId() + "】规模成功："+scale);
            if(StringUtils.nonEmpty(scale)){
                cusCorp.setCorpScale(scale);
                cusCorpDto.setCorpScale(scale);
            }
            cusCorpMapper.updateByPrimaryKeySelective(cusCorp);
        }else{
            cusCorpMapper.insertSelective(cusCorp);
        }


        CusBase cusBase = new CusBase();
        BeanUtils.copyProperties(cusCorpDto, cusBase);
        if (cusBaseMapper.selectByPrimaryKey(cusCorp.getCusId()) != null) {
            cusBaseMapper.updateByPrimaryKeySelective(cusBase);
        } else {
            //取系统日期
            String openday = stringRedisTemplate.opsForValue().get("openDay");//开户日期
            cusBase.setOpenDate(openday);
            cusBaseMapper.insertSelective(cusBase);
        }

        if (Objects.nonNull(cusCorpDto.getTaskStatus()) && "3".equals(cusCorpDto.getTaskStatus())) {
            /*
             * 2.证件类型为组织机构代码和统一社会信用证
             *   调用外部查询接口查询要素是否一致
             */
            // zsnew 调用接口验证
            boolean insCode = "Q".equals(cusCorpDto.getCertType());
            boolean certCode = "R".equals(cusCorpDto.getCertType());
            //证件类型为组织机构代码和统一社会信用证才查询，其他不查询
            if (insCode || certCode) {
                //调用外部接口查询 客户要素是否一致
                checkCertCodeAndName(cusCorpDto, insCode, certCode);
            }
            /*
             *  3.存在性查询
             *  调用ECIf接口查询客户是否存在 ，存在则返回客户号
             */
            String cusId = cusCorpDto.getCusId();
            /*
             * 4.这里肯定能返回客户号，如果不存在则向导开户失败
             */
            if (Objects.isNull(cusId)) {
                cusId = queryG10501(cusCorpDto);;
            }
            // 临时客户不调用ecif的同步接口
            if (!"A02".equals(cusCorpDto.getBizType()) && !"A04".equals(cusCorpDto.getBizType())){
                cusId = ecifCreateCus(cusCorpDto);
            }
            //5. ECIF开户成功，插入客户相关信息
            cusCorpDto.setCusId(cusId);
            /*
             * 5.1 保存cusBase表
             */
            BeanUtils.copyProperties(cusCorpDto, cusBase);
            //客户大类 2-对公 1-对私
            cusBase.setCusCatalog(CmisCusConstants.CUS_CATALOG_PUB);
            //客户状态 STD_CUS_STATE 2-生效 1-暂存
            cusBase.setCusState("2");
            //设置管护机构
            CusBase tempCusBase = cusBaseMapper.selectByPrimaryKey(cusId);
            if(StringUtils.isEmpty(tempCusBase.getManagerId()) || StringUtils.isEmpty(tempCusBase.getManagerBrId())){
                User userInfo = SessionUtils.getUserInformation();
                cusBase.setManagerId(userInfo.getLoginCode());
                cusBase.setManagerBrId(userInfo.getOrg().getCode());
            }
            cusBaseMapper.updateByPrimaryKeySelective(cusBase);

            /*
             * 5.2 保存任务表 待处理任务
             */
            CusManaTask cusManaTask = new CusManaTask();
            BeanUtils.copyProperties(cusCorpDto, cusManaTask);
            cusManaTask.setTaskStatus("3");
            cusManaTask.setSerno(cusCorpDto.getTaskSerno());
            cusManaTaskService.updateSelective(cusManaTask);

            // 5.4 如果为公司正式客户创建，调非零内评同步用户数据（集中作业的在后处理里执行）；并创建归档任务

            // 20211115-00166修改为临时客户也进非零内评系统
                Xirs10ReqDto reqDto = new Xirs10ReqDto();
                reqDto.setCustid(cusCorpDto.getCusId());
                reqDto.setCustname(cusCorpDto.getCusName());
                reqDto.setCertid(cusCorpDto.getCertCode());
                reqDto.setCerttype(PUBUtilTools.changeCertTypeFromEcif(cusCorpDto.getCertType()));
                reqDto.setCusttype(cusCorpDto.getCusType());
                reqDto.setIndustrycode(cusCorpDto.getTradeClass());
                reqDto.setInputorgid(tempCusBase.getManagerBrId());
                reqDto.setInputuserid(tempCusBase.getManagerId());
                ResultDto<Xirs10RespDto> result = dscms2IrsClientService.xirs10(reqDto);
                log.info("公司正式客户创建（非集中作业）调非零内评同步用户数据失败，非零内评接口返回{}", result.toString());

//                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
//                docArchiveClientDto.setArchiveMode("02");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
//                docArchiveClientDto.setDocClass("01");//01:基础资料档案,02:授信资料档案,03:重要信息档案
//                docArchiveClientDto.setDocType("02");//02:对公客户资料
//                docArchiveClientDto.setBizSerno(cusCorpDto.getTaskSerno());
//                docArchiveClientDto.setCusId(cusCorp.getCusId());
//                docArchiveClientDto.setCusName(cusCorp.getCusName());
//                docArchiveClientDto.setCertType(cusCorp.getCertType());
//                docArchiveClientDto.setCertCode(cusCorp.getCertCode());
//                docArchiveClientDto.setManagerId(cusBase.getManagerId());
//                docArchiveClientDto.setManagerBrId(cusBase.getManagerBrId());
//                docArchiveClientDto.setInputId(cusBase.getInputId());
//                docArchiveClientDto.setInputBrId(cusBase.getInputBrId());
//                ResultDto<Integer> docArchiveBySys = cmisBizClientService.createDocArchiveBySys(docArchiveClientDto);
//                if(!docArchiveBySys.getCode().equals("0")){
//                    log.info("公司正式客户创建（非集中作业）生成归档任务失败，任务编号：{}，调用归档接口返回：{}", cusCorpDto.getTaskSerno(), docArchiveBySys.getMessage());
//                }

        }
        log.info("{}保存成功", Thread.currentThread().getName());
        return 1;
    }

    @Autowired
    private CusManaTaskService cusManaTaskMapper;
    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;

    /**
     * @return
     * @函数名称:openAccount
     * @函数描述: 开户调用接口逻辑
     * @参数与返回说明:
     * @算法描述:
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public CusCorpDto openAccount(CusCorpDto cusCorpDto) throws Exception {
        /*
         * 1.检查客户是否在本地已存在
         */
        checkOnthWay(cusCorpDto);
        /*
         *  2.存在性查询
         *  调用ECIf接口查询客户是否存在 ，存在则返回客户号
         */
        //正式客户不填客户名称，名称从g10501接口获取
        String cusId = queryG10501(cusCorpDto);
        cusCorpDto.setIsEcif("1");//1是0否
        /*
         * 3.证件类型为组织机构代码和统一社会信用证
         *   调用外部查询接口查询要素是否一致
         */
        // zsnew 调用接口验证
        boolean insCode = "Q".equals(cusCorpDto.getCertType());
        boolean certCode = "R".equals(cusCorpDto.getCertType());
        //证件类型为组织机构代码和统一社会信用证才查询，其他不查询
        if (insCode || certCode) {
            //调用外部接口查询 客户要素是否一致
            checkCertCodeAndName(cusCorpDto, insCode, certCode);
        }
        /*
         * 4.判断客户是否存在 不存在则调用ecif接口开户 返回客户号
         */
        if (StringUtils.isEmpty(cusId)) {
            cusCorpDto.setIsEcif("0");
            cusId = ecifCreate(cusCorpDto);
        }
        if(StringUtils.isEmpty(cusId)){
            throw BizException.error(null,EcsEnum.ECS040008.key,EcsEnum.ECS040008.value);
        }
        //5. ECIF开户成功，插入客户相关信息
        cusCorpDto.setCusId(cusId);
        //6. 将ECIF返回的cusId,再判断是否在本地已存在
        checkIsByCusId(cusCorpDto);
        /*
         * 6.1 保存任务表 待处理任务
         */
        CusManaTask cusManaTask = new CusManaTask();
        BeanUtils.copyProperties(cusCorpDto, cusManaTask);
        cusManaTask.setTaskStatus("1");
        cusManaTaskMapper.insertSelective(cusManaTask);
        cusCorpDto.setTaskSerno(cusManaTask.getSerno());
        /*
         * 6.2 保存cusBase表
         */
        CusBase cusBase = new CusBase();
        BeanUtils.copyProperties(cusCorpDto, cusBase);
        //客户大类 1-对私 2-对公
        cusBase.setCusCatalog("2");
        //客户状态 STD_CUS_STATE 1-暂存2-生效
        cusBase.setCusState("1");
        //设置管护机构
        User userInfo = SessionUtils.getUserInformation();
        cusBase.setManagerId(userInfo.getLoginCode());
        cusBase.setManagerBrId(userInfo.getOrg().getCode());
        //取系统日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");//开户日期
        cusBase.setOpenDate(openday);
        cusBaseMapper.insertSelective(cusBase);

        /*
         * 6.3 保存cusCorp表
         */
        CusCorp cusCorp = new CusCorp();
        BeanUtils.copyProperties(cusCorpDto, cusCorp);
        //证件类型为组织机构代码和统一社会信用证
        if (insCode || certCode) {
            ZsnewReqDto zsnewReqDto = new ZsnewReqDto();
            if (insCode) {
                zsnewReqDto.setOrgcode(cusCorpDto.getInsCode());     //组织机构代码 210
            }
            if (certCode) {
                zsnewReqDto.setCreditcode(cusCorpDto.getCertCode()); //统一信用代码 220
            }
            zsnewReqDto.setName(cusCorpDto.getCusName());//企业名称
            zsnewReqDto.setEntstatus(cusCorpDto.getOperStatus());  //企业经营状态，1：在营，2：非在营
            zsnewReqDto.setId(StringUtils.EMPTY);         //中数企业ID
            zsnewReqDto.setRegno(cusCorpDto.getRegiCode());    //企业注册号
            zsnewReqDto.setVersion(StringUtils.EMPTY);  //高管识别码版本号,返回高管识别码时生效
            ResultDto<ZsnewRespDto> zsnewResultDto = dscms2OuterdataClientService.zsnew(zsnewReqDto);
            String zsnewCode = Optional.ofNullable(zsnewResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String zsnewMeesage = Optional.ofNullable(zsnewResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            ZsnewRespDto zsnewRespDto = null;
            if (SuccessEnum.CMIS_SUCCSESS.key.equals(zsnewCode)) {
                String code = zsnewResultDto.getData().getData().getCODE();
                if ("200".equals(code)) {
                    //查询成功
                    log.info("查询成功");
                } else {
                    //没有查询到
                    log.info("没有查询到");
                }
            } else {
                log.error("接口交易失败");
            }
            try {
                if (zsnewResultDto.getData() != null){
                    if (zsnewResultDto.getData().getData() != null){
                        if (zsnewResultDto.getData().getData().getENT_INFO() != null){
                            if (zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO() != null){
                                if(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().size()!=0){
                                    cusCorp.setCusName(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getENTNAME());
                                    cusCorp.setCusShortName(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getCOMPSNAME());
                                    cusCorp.setCusNameEn(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getENGNAME());
                                    cusCorp.setFjobNum(Integer.parseInt(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getWORKFORCE()));
                                    cusCorp.setRegiAddr(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getREGADDR());
                                    cusCorp.setOperAddrAct(zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getOFFICEADDR());
                                    String country=zsnewResultDto.getData().getData().getENT_INFO().getLISTEDCOMPINFO().get(0).getCOUNTRY();
                                    if(country !=null){
                                        cusCorp.setCountry(CmisCusConstants.countryTypeMap.get(country));
                                    }
                                }
                            }
                            if (zsnewResultDto.getData().getData().getENT_INFO().getBASICLIST() != null){
                                if(zsnewResultDto.getData().getData().getENT_INFO().getBASICLIST().size()!=0){
                                    cusCorp.setCusName(zsnewResultDto.getData().getData().getENT_INFO().getBASICLIST().get(0).getENTNAME());
                                    //cusCorp.setConType(zsnewResultDto.getData().getData().getENT_INFO().getBASICLIST().get(0).getENTTYPE());
                                    cusCorp.setBuildDate(zsnewResultDto.getData().getData().getENT_INFO().getBASICLIST().get(0).getESDATE());
                                    cusCorp.setRegiCode(zsnewResultDto.getData().getData().getENT_INFO().getBASICLIST().get(0).getREGNO());
                                    BigDecimal regCap = BigDecimal.ZERO;
                                    if(!StringUtils.isEmpty(zsnewResultDto.getData().getData().getENT_INFO().getBASICLIST().get(0).getREGCAP())){
                                        regCap = new BigDecimal(zsnewResultDto.getData().getData().getENT_INFO().getBASICLIST().get(0).getREGCAP());
                                    }
                                    cusCorp.setRegiCapAmt(regCap);
                                }
                            }
                            if (zsnewResultDto.getData().getData().getENT_INFO().getBASIC()!=null){
                                cusCorp.setCusName(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getENTNAME());
                                //cusCorp.setConType(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getENTTYPE());
                                cusCorp.setBuildDate(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getESDATE());
                                cusCorp.setRegiAreaCode(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getREGORGCODE());
                                cusCorp.setRegiCode(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getREGNO());
                                BigDecimal regCap = BigDecimal.ZERO;
                                if(!StringUtils.isEmpty(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getREGCAP())){
                                    regCap = new BigDecimal(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getREGCAP());
                                }
                                cusCorp.setRegiCapAmt(regCap);
                                cusCorp.setLicOperPro(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getABUITEM());
                                cusCorp.setRegiOrg(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getREGORG());
                                cusCorp.setLinkmanEmail(zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getEMAIL());
                                String currency=zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getREGCAPCURCODE();
                                if (currency !=null){
                                    cusCorp.setRegiCurType(CmisCusConstants.moneyTypeMap.get(currency));
                                }
                                String operStatus=zsnewResultDto.getData().getData().getENT_INFO().getBASIC().getENTSTATUSCODE();
                                if (operStatus !=null){
                                    cusCorp.setOperStatus(CmisCusConstants.operTypeMap.get(operStatus));
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                log.error("查询工商信息发生异常！", e); // 工商信息异常处理
                //throw BizException.error(null, "9999", e.getMessage());
            }
        }
        cusCorpMapper.insertSelective(cusCorp);
        BeanUtils.copyProperties(cusCorp, cusCorpDto);

        //return SuccessEnum.SUCCESS.key;
        return cusCorpDto;
    }

    /**
     * Ecif 开户简易接口
     * @param cusCorpDto
     */
    private String ecifCreate(CusCorpDto cusCorpDto) {
        String cusId = "";
        // 同业客户开户
        G00102ReqDto g00102ReqDto = new G00102ReqDto();
        /**
         * 1	正常客户
         * 2	注销客户
         * 3	临时客户
         */
        if ("A02".equals(cusCorpDto.getBizType())) {
            //临时客户开户或维护
            g00102ReqDto.setCustst("3");//客户状态
        } else {
            //正式客户开户
            g00102ReqDto.setCustst("1");//客户状态
            //信贷系统不允许开立ECIF不存在的正式客户！
            throw BizException.error(null,EcsEnum.ECS040009.key,EcsEnum.ECS040009.value);
        }

        /**
         * 01	对公客户
         * 02	同业客户
         */
        g00102ReqDto.setCusttp("01");//客户类型               ,
        g00102ReqDto.setCustsb(StringUtils.EMPTY);//对公客户类型细分           ,
        g00102ReqDto.setIdtftp(cusCorpDto.getCertType());//证件类型            ,
        g00102ReqDto.setIdtfno(cusCorpDto.getCertCode());//证件号码               ,
        g00102ReqDto.setRegadr(Optional.ofNullable(cusCorpDto.getRegisterAddress()).orElse(cusCorpDto.getRegiAreaCode()));//注册地址
        g00102ReqDto.setCustna(cusCorpDto.getCusName());//客户名称
        RelArrayInfo relArrayInfo = new RelArrayInfo();
        relArrayInfo.setLawcna(cusCorpDto.getCusName());
        relArrayInfo.setCustrelid(cusCorpDto.getCusId());
        ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
        String g00202Code = Optional.ofNullable(g00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g00202Meesage = Optional.ofNullable(g00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G00102RespDto g00102RespDto = null;
        if (Objects.equals(g00202Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            g00102RespDto = g00102ResultDto.getData();
            cusId=g00102RespDto.getCustno();
        } else {
            throw BizException.error(null,g00202Code,g00202Meesage);
        }
        if(StringUtils.isEmpty(cusId)){
            throw BizException.error(null,EcsEnum.ECS040008.key,EcsEnum.ECS040008.value);
        }
        return cusId;
    }

    /**
     * Ecif 开户/维护接口
     * @param cusCorpDto
     */
    private String ecifCreateCus(CusCorpDto cusCorpDto) {
        String cusId = "";
        // 同业客户开户
        G00102ReqDto g00102ReqDto = new G00102ReqDto();
        /**
         * 1	正常客户
         * 2	注销客户
         * 3	临时客户
         */
        String cusSts = queryCusStsG10501(cusCorpDto);
        if (StringUtils.isBlank(cusSts)) {
            if ("A02".equals(cusCorpDto.getBizType()) || "A04".equals(cusCorpDto.getBizType())) {
                //临时客户开户或维护
                g00102ReqDto.setCustst("3");//客户状态
            } else if ("A09".equals(cusCorpDto.getBizType()) || "A06".equals(cusCorpDto.getBizType()) || "A05".equals(cusCorpDto.getBizType())) {
                //正式客户开户或维护
                g00102ReqDto.setCustst("1");//客户状态
            } else {
                //正式客户开户
                g00102ReqDto.setCustst("1");//客户状态
                //信贷系统不允许开立ECIF不存在的正式客户！
                throw BizException.error(null,EcsEnum.ECS040009.key,EcsEnum.ECS040009.value);
            }
        } else {
            g00102ReqDto.setCustst(cusSts);
        }

        /**
         * 01	对公客户
         * 02	同业客户
         */
        g00102ReqDto.setCusttp("01");//客户类型               ,
        g00102ReqDto.setTwcttp(convertCityType(cusCorpDto.getCityType()));//城乡类型
        g00102ReqDto.setAdmnst(cusCorpDto.getOperStatus());//经营状况
        g00102ReqDto.setCustsb(StringUtils.EMPTY);//对公客户类型细分           ,
        g00102ReqDto.setIdtftp(cusCorpDto.getCertType());//证件类型   TODO 修改码值            ,
        g00102ReqDto.setIdtfno(cusCorpDto.getCertCode());//证件号码               ,
        g00102ReqDto.setEfctdt(StringUtils.EMPTY);//证件生效日期             ,
        g00102ReqDto.setBusisp(cusCorpDto.getOperAddrAct());//经营范围  提交之后更新
        g00102ReqDto.setRgprov(StringUtils.EMPTY);//注册地址所在省
        g00102ReqDto.setRgcity(StringUtils.EMPTY);//注册地址所在市
        g00102ReqDto.setRgerea(StringUtils.EMPTY);//注册地址所在地区
        g00102ReqDto.setRegadr(Optional.ofNullable(cusCorpDto.getRegisterAddress()).orElse(cusCorpDto.getRegiAreaCode()));//注册地址
        g00102ReqDto.setCustna(cusCorpDto.getCusName());//客户名称
        g00102ReqDto.setJgxyno(StringUtils.EMPTY);//社会信用代码
        g00102ReqDto.setCustno(cusCorpDto.getCusId());	//客户编号
        g00102ReqDto.setCustna(cusCorpDto.getCusName());	//客户名称
        g00102ReqDto.setCsstna(cusCorpDto.getCusShortName());	//客户简称
        g00102ReqDto.setCsenna(cusCorpDto.getCusNameEn());	//客户英文名称
        //g00102ReqDto.setCustst();	//客户状态
        //g00102ReqDto.setCusttp("01");	//客户类型
        g00102ReqDto.setCustsb(cusCorpDto.getCusType());	//对公客户类型细分
        //g00102ReqDto.setIdtftp(cusCorpDto.getCertType());	//证件类型
        //g00102ReqDto.setEfctdt();	//证件生效日期
        g00102ReqDto.setInefdt(DateUtils.formatDate10To8(cusCorpDto.getCertIdate()));	//证件失效日期
        //g00102ReqDto.setAreaad();	//发证机关国家或地区
        //g00102ReqDto.setDepart();	//发证机关
        //g00102ReqDto.setGvrgno();	//营业执照号码
        //g00102ReqDto.setGvbgdt();	//营业执照生效日期
        //g00102ReqDto.setGveddt();	//营业执照失效日期
        //g00102ReqDto.setGvtion();	//营业执照发证机关
        //g00102ReqDto.setCropcd(cusCorpDto.getInsCode());	//组织机构代码
        //g00102ReqDto.setOreddt();	//组织机构代码失效日期
        //g00102ReqDto.setNttxno();	//税务证件号
        //g00102ReqDto.setSwbgdt();	//税务证件生效日期
        //g00102ReqDto.setSweddt();	//税务证件失效日期
        //g00102ReqDto.setSwtion();	//税务证件发证机关
        //g00102ReqDto.setJgxyno();	//机构信用代码证
        //g00102ReqDto.setCropdt();	//机构信用代码证失效日期
        //g00102ReqDto.setOpcfno();	//开户许可证
        //g00102ReqDto.setFoundt();	//成立日期
        g00102ReqDto.setCorppr(cusCorpDto.getTradeClass());//行业类别
        //g00102ReqDto.setCoppbc();	//人行行业类别
        //g00102ReqDto.setEntetp();	//产业分类
        g00102ReqDto.setCorpnu(cusCorpDto.getCorpQlty());	//企业性质
        g00102ReqDto.setBlogto(cusCorpDto.getSubTyp());	//隶属关系
        //g00102ReqDto.setPropts();	//居民性质
        g00102ReqDto.setFinctp(cusCorpDto.getHoldType());	//控股类型（出资人经济类型）
        g00102ReqDto.setInvesj(cusCorpDto.getInvestMbody());	//投资主体
        //g00102ReqDto.setRegitp(convertRegitp(cusCorpDto.getRegiType()));	//登记注册类型
        g00102ReqDto.setRegitp(cusCorpDto.getRegiType());	//登记注册类型-20210929数银更换标准
        g00102ReqDto.setRegidt(DateUtils.formatDate10To8(cusCorpDto.getRegiStartDate()));	//注册日期
        //g00102ReqDto.setRegicd();	//注册地国家代码
        //g00102ReqDto.setRegchn();	//人民银行注册地区代码
        g00102ReqDto.setRgctry(StringUtils.substring(cusCorpDto.getRegiAreaCode(), 6));	//注册地行政区划
        //g00102ReqDto.setRgcrcy();	//注册资本币别
        if(cusCorpDto.getRegiCapAmt()!=null){
            g00102ReqDto.setRgcpam(cusCorpDto.getRegiCapAmt().divide(new BigDecimal(10000)));	//注册资本金额
        }
        //g00102ReqDto.setCacrcy();	//实收资本币别
        if(cusCorpDto.getPaidCapAmt()!=null){
            g00102ReqDto.setCacpam(cusCorpDto.getPaidCapAmt().divide(new BigDecimal(10000)));	//实收资本金额
        }
        //g00102ReqDto.setOrgatp();	//组织机构类型
        //g00102ReqDto.setSpectp();	//特殊经济区类型
        //g00102ReqDto.setBusstp();	//企业经济类型
        //g00102ReqDto.setNaticd();	//国民经济部门
        //g00102ReqDto.setDebttp();	//境内主体类型
        //g00102ReqDto.setEnvirt();	//企业环保级别
        //g00102ReqDto.setFinacd();	//金融机构类型代码
        //g00102ReqDto.setSwifcd();	//SWIFT编号
        //g00102ReqDto.setSpntcd();	//该SPV或壳机构所属国家/地区
        //g00102ReqDto.setAdmnti();	//经营期限
        g00102ReqDto.setCorpsz(cusCorpDto.getCorpScale());	//企业规模
        //g00102ReqDto.setBusisp();	//经营范围
        //g00102ReqDto.setAcesbs();	//客户兼营业务
        //g00102ReqDto.setEmplnm();	//员工总数
        g00102ReqDto.setComput(cusCorpDto.getAdminOrg());	//主管单位
        //g00102ReqDto.setUpcrps();	//主管单位法人名称
        //g00102ReqDto.setUpidtp();	//主管单位法人证件类型
        //g00102ReqDto.setUpidno();	//主管单位法人证件号码
        //g00102ReqDto.setLoanno();	//贷款卡编码
        //g00102ReqDto.setWkflar(cusCorpDto.getOperPlaceSqu());	//经营场地面积
        g00102ReqDto.setWkflfe(cusCorpDto.getOperPlaceOwnshp());	//经营场地所有权
        //g00102ReqDto.setBaseno();	//基本账户核准号
        //g00102ReqDto.setAcctnm();	//基本账户开户行名称
        //g00102ReqDto.setAcctbr();	//基本账户开户行号
        //g00102ReqDto.setAcctno();	//基本账户账号
        g00102ReqDto.setAcctdt(cusCorpDto.getBasicAccNoOpenDate());	//基本账户开户日期
        //g00102ReqDto.setDepotp();	//存款人类别
        //g00102ReqDto.setRhtype();	//人行统计分类
        //g00102ReqDto.setSumast();	//公司总资产
        //g00102ReqDto.setNetast();	//公司净资产
        //g00102ReqDto.setSthdfg();	//是否我行股东
        //g00102ReqDto.setSharhd();	//拥有我行股份金额
        //g00102ReqDto.setListtg();	//是否上市企业
        //g00102ReqDto.setListld();	//上市地
        //g00102ReqDto.setStoctp();	//股票类别
        //g00102ReqDto.setStoccd();	//股票代码
        //g00102ReqDto.setOnacnm();	//一类户数量
        //g00102ReqDto.setTotlnm();	//总户数
        //g00102ReqDto.setGrpctg();	//是否集团客户
        //g00102ReqDto.setReletg();	//是否是我行关联客户
        //g00102ReqDto.setFamltg();	//是否家族企业
        //g00102ReqDto.setHightg();	//是否高新技术企业
        //g00102ReqDto.setIsbdcp();	//是否是担保公司
        //g00102ReqDto.setSpcltg();	//是否为特殊经济区内企业
        //g00102ReqDto.setArimtg();	//是否地区重点企业
        //g00102ReqDto.setFinatg();	//是否融资类企业
        //g00102ReqDto.setResutg();	//是否国资委所属企业
        //g00102ReqDto.setSctlpr();	//是否国家宏观调控限控行业
        //g00102ReqDto.setFarmtg();	//是否农户
        //g00102ReqDto.setHgegtg();	//是否高能耗企业
        //g00102ReqDto.setExegtg();	//是否产能过剩企业
        //g00102ReqDto.setElmntg();	//是否属于淘汰产能目录
        //g00102ReqDto.setEnvifg();	//是否环保企业
        //g00102ReqDto.setHgplcp();	//是否高污染企业
        //g00102ReqDto.setIstdcs();	//是否是我行贸易融资客户
        g00102ReqDto.setSpclfg(cusCorpDto.getSpOperFlag());	//特种经营标识
        g00102ReqDto.setSteltg(cusCorpDto.getIsSteelCus());	//是否钢贸企业
        //g00102ReqDto.setDomitg();	//是否优势企业
        //g00102ReqDto.setStrutp();	//产业结构调整类型
        //g00102ReqDto.setStratp();	//战略新兴产业类型
        //g00102ReqDto.setIndutg();	//工业转型升级标识
        //g00102ReqDto.setLeadtg();	//是否龙头企业
        //g00102ReqDto.setCncatg();	//中资标志
        //g00102ReqDto.setImpotg();	//进出口权标志
        //g00102ReqDto.setClostg();	//企业关停标志
        g00102ReqDto.setPlattg(cusCorpDto.getIisSzjrfwCrop());	//是否苏州综合平台企业
        //g00102ReqDto.setFibrtg();	//是否为工业园区、经济开发区等行政管理区
        //g00102ReqDto.setGvfntg();	//是否政府投融资平台
        //g00102ReqDto.setGvlnlv();	//地方政府融资平台隶属关系
        //g00102ReqDto.setGvlwtp();	//地方政府融资平台法律性质
        //g00102ReqDto.setGvlntp();	//政府融资贷款类型
        //g00102ReqDto.setPetytg();	//是否小额贷款公司
        //g00102ReqDto.setFscddt();	//首次与我行建立信贷关系时间
        //g00102ReqDto.setSupptg();	//是否列入重点扶持产业
        //g00102ReqDto.setLicetg();	//是否一照一码
        //g00102ReqDto.setLandtg();	//是否土地整治机构
        //g00102ReqDto.setPasvfg();	//是否消极非金融机构
        //g00102ReqDto.setImagno();	//影像编号
        //g00102ReqDto.setTaxptp();	//纳税人身份
        //g00102ReqDto.setNeedfp();	//是否需要开具增值税专用发票
        //g00102ReqDto.setTaxpna();	//纳税人全称
        //g00102ReqDto.setTaxpad();	//纳税人地址
        //g00102ReqDto.setTaxptl();	//纳税人电话
        //g00102ReqDto.setTxbkna();	//纳税人行名
        //g00102ReqDto.setTaxpac();	//纳税人账号
        //g00102ReqDto.setTxpstp();	//税收居民类型
        //g00102ReqDto.setTaxadr();	//税收单位英文地址
        //g00102ReqDto.setTxnion();	//税收居民国（地区）
        //g00102ReqDto.setTxennm();	//税收英文姓名
        //g00102ReqDto.setTxbcty();	//税收出生国家或地区
        //g00102ReqDto.setTaxnum();	//纳税人识别号
        //g00102ReqDto.setNotxrs();	//不能提供纳税人识别号原因
        //g00102ReqDto.setCtigno();	//客户经理号
        //g00102ReqDto.setCtigna();	//客户经理姓名
        //g00102ReqDto.setCtigph();	//客户经理手机号
        //g00102ReqDto.setCorptl();	//公司电话
        //g00102ReqDto.setHometl();	//联系电话
        //g00102ReqDto.setRgpost();	//注册地址邮编
        //g00102ReqDto.setRgcuty();	//注册地址所在国家
        //g00102ReqDto.setRgprov();	//注册地址所在省
        //g00102ReqDto.setRgcity();	//注册地址所在市
        //g00102ReqDto.setRgerea();	//注册地址所在区
        //g00102ReqDto.setRegadr();	//注册地址
        //g00102ReqDto.setBspost();	//经营地址邮编
        //g00102ReqDto.setBscuty();	//经营地址所在国家
        //g00102ReqDto.setBsprov();	//经营地址所在省
        //g00102ReqDto.setBscity();	//经营地址所在市
        //g00102ReqDto.setBserea();	//经营地址所在区
        //g00102ReqDto.setBusiad();	//经营地址
        //g00102ReqDto.setEnaddr();	//英文地址
        //g00102ReqDto.setWebsit();	//公司网址
        //g00102ReqDto.setFaxfax();	//传真
        //g00102ReqDto.setEmailx();	//邮箱
        //g00102ReqDto.setSorgno();	//同业机构(行)号
        //g00102ReqDto.setSorgtp();	//同业机构(行)类型
        //g00102ReqDto.setOrgsit();	//同业机构(行)网址
        //g00102ReqDto.setBkplic();	//同业客户金融业务许可证
        //g00102ReqDto.setReguno();	//同业非现场监管统计机构编码
        //g00102ReqDto.setPdcpat();	//同业客户实际到位资金(万元)
        //g00102ReqDto.setSorglv();	//同业授信评估等级
        //g00102ReqDto.setEvalda();	//同业客户评级到期日期
        //g00102ReqDto.setReldgr();	//同业客户与我行合作关系
        //g00102ReqDto.setLimind();	//同业客户是否授信
        //g00102ReqDto.setMabrid();	//同业客户主管机构(同业)
        //g00102ReqDto.setManmgr();	//同业客户主管客户经理(同业)
        //g00102ReqDto.setBriskc();	//同业客户风险大类
        //g00102ReqDto.setMriskc();	//同业客户风险种类
        //g00102ReqDto.setFriskc();	//同业客户风险小类
        //g00102ReqDto.setRiskda();	//同业客户风险划分日期
        //g00102ReqDto.setChflag();	//境内标识
        //g00102ReqDto.setNnjytp();	//交易类型
        //g00102ReqDto.setNnjyna();	//交易名称
        List<RelArrayInfo> list = new ArrayList<>();

        if ("A02".equals(cusCorpDto.getBizType())) {
            //获取高管信息
            QueryModel querymodel = new QueryModel();
            querymodel.addCondition("cusIdRel",cusCorpDto.getCusId());
            List<CusCorpMgr> cusCorpMgrList = cusCorpMgrService.selectByModel(querymodel);
            if(!CollectionUtils.isEmpty(cusCorpMgrList)){
                for (int i = 0; i <cusCorpMgrList.size() ; i++) {
                    RelArrayInfo relArrayInfo = new RelArrayInfo();
                    relArrayInfo.setLawcna(cusCorpDto.getCusName());
                    relArrayInfo.setCustrelid(cusCorpDto.getCusId());
                    /*
                     * 有效标志 0：新增 1：删除 2：维护
                     * */
                    relArrayInfo.setValitg("0");
                    /* 自然人企业标志 0：个人 1：公司
                     * */
                    relArrayInfo.setPrentg("0");
                    if("202200".equals(cusCorpMgrList.get(i).getMrgType()) || "201200".equals(cusCorpMgrList.get(i).getMrgType())){//主要实际控制人、实际控制人；按ecif要求转码300100控股股东或实际控制人
                        relArrayInfo.setRelttp("300100");
                    }else if("202100".equals(cusCorpMgrList.get(i).getMrgType())){//村组织负责人；209900其他
                        relArrayInfo.setRelttp("209900");
                    }else{
                        relArrayInfo.setRelttp(cusCorpMgrList.get(i).getMrgType());
                    }
                    relArrayInfo.setIdtftp(cusCorpMgrList.get(i).getMrgCertType());//证件类型
                    relArrayInfo.setIdtfno(cusCorpMgrList.get(i).getMrgCertCode());//证件号码
                    relArrayInfo.setEfctdt("");//证件生效日期
                    relArrayInfo.setInefdt(cusCorpMgrList.get(i).getCertIdate().replace("-",""));//证件失效日期
                    relArrayInfo.setHomead(cusCorpMgrList.get(i).getResiAddr());//联系地址
                    relArrayInfo.setPrpsex(cusCorpMgrList.get(i).getMrgSex());//性别
                    relArrayInfo.setBorndt(cusCorpMgrList.get(i).getMrgBday().replace("-",""));//生日
                    relArrayInfo.setNation(cusCorpMgrList.get(i).getCountry());//国籍
                    relArrayInfo.setRealna(cusCorpMgrList.get(i).getMrgName());//高管名称
                    list.add(relArrayInfo);
                }
            }
            g00102ReqDto.setRelArrayInfo(list);
        }
        ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
        String g00202Code = Optional.ofNullable(g00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g00202Meesage = Optional.ofNullable(g00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G00102RespDto g00102RespDto = null;
        if (Objects.equals(g00202Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            g00102RespDto = g00102ResultDto.getData();
            cusId=g00102RespDto.getCustno();
        } else {
            // TODO 抛出错误异常
            throw BizException.error(null,g00202Code,g00202Meesage);
        }
        return cusId;
    }

    /**
     * 查询客户是否存在
     * @param cusCorpDto
     * @return
     */
    public String queryG10501(CusCorpDto cusCorpDto) {
        // g10501 对公及同业客户清单查询
        /**
         * 识别方式:
         * 1- 按证件类型、证件号码查询
         * 2- 按证件号码查询
         * 3- 按客户编号查询
         * 4- 按客户名称模糊查询
         * 以下接口必填一种查询方式；
         */
        G10501ReqDto g10501ReqDto = new G10501ReqDto();
        // TODO StringUtils.EMPTY的实际值待确认 开始
        g10501ReqDto.setResotp("2");// 识别方式
//        g10501ReqDto.setCustna(cusCorpDto.getCusName());// 客户名称
        g10501ReqDto.setIdtftp(cusCorpDto.getCertType());// 证件类型
        g10501ReqDto.setIdtfno(cusCorpDto.getCertCode());// 证件号码
//        g10501ReqDto.setCustst(cusCorpDto.getCusState());// 客户状态
        g10501ReqDto.setBginnm(StringUtils.EMPTY);// 起始笔数
        g10501ReqDto.setQurynm(StringUtils.EMPTY);// 查询笔数
        // TODO StringUtils.EMPTY的实际值待确认 结束
        //TODO 完善后续逻辑
        ResultDto<G10501RespDto> g10501ResultDto = dscms2EcifClientService.g10501(g10501ReqDto);
        String g10501Code = Optional.ofNullable(g10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g10501Meesage = Optional.ofNullable(g10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G10501RespDto g10501RespDto = null;
        if (Objects.equals(g10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            // TODO 获取相关的值并解析
            g10501RespDto = g10501ResultDto.getData();
            log.info(String.valueOf(g10501RespDto));
            if (g10501RespDto.getListnm() > 0) {
                cusCorpDto.setCusName(g10501RespDto.getListArrayInfo().get(0).getCustna());
                return  g10501RespDto.getListArrayInfo().get(0).getCustno();
            }else{
                return null;
            }
        } else {
            // TODO 抛出错误异常
            throw BizException.error(null,g10501Code,g10501Meesage);
        }
    }

    /**
     * 查询客户是否存在,并返回客户状态
     * @param cusCorpDto
     * @return
     */
    public String queryCusStsG10501(CusCorpDto cusCorpDto) {
        // g10501 对公及同业客户清单查询
        /**
         * 识别方式:
         * 1- 按证件类型、证件号码查询
         * 2- 按证件号码查询
         * 3- 按客户编号查询
         * 4- 按客户名称模糊查询
         * 以下接口必填一种查询方式；
         */
        G10501ReqDto g10501ReqDto = new G10501ReqDto();
        // TODO StringUtils.EMPTY的实际值待确认 开始
        g10501ReqDto.setResotp("2");// 识别方式
//        g10501ReqDto.setCustna(cusCorpDto.getCusName());// 客户名称
        g10501ReqDto.setIdtftp(cusCorpDto.getCertType());// 证件类型
        g10501ReqDto.setIdtfno(cusCorpDto.getCertCode());// 证件号码
//        g10501ReqDto.setCustst(cusCorpDto.getCusState());// 客户状态
        g10501ReqDto.setBginnm(StringUtils.EMPTY);// 起始笔数
        g10501ReqDto.setQurynm(StringUtils.EMPTY);// 查询笔数
        // TODO StringUtils.EMPTY的实际值待确认 结束
        //TODO 完善后续逻辑
        ResultDto<G10501RespDto> g10501ResultDto = dscms2EcifClientService.g10501(g10501ReqDto);
        String g10501Code = Optional.ofNullable(g10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g10501Meesage = Optional.ofNullable(g10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G10501RespDto g10501RespDto = null;
        if (Objects.equals(g10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            // TODO 获取相关的值并解析
            g10501RespDto = g10501ResultDto.getData();
            log.info(String.valueOf(g10501RespDto));
            if (g10501RespDto.getListnm() > 0) {
                cusCorpDto.setCusName(g10501RespDto.getListArrayInfo().get(0).getCustna());
                return  g10501RespDto.getListArrayInfo().get(0).getCustst();
            }else{
                return null;
            }
        } else {
            // TODO 抛出错误异常
            throw BizException.error(null,g10501Code,g10501Meesage);
        }
    }

    /**
     * 调用外部接口检查客户要素
     *
     * @param cusCorpDto
     */
    private void checkCertCodeAndName(CusCorpDto cusCorpDto, boolean insCode, boolean certCode) {
        ZsnewReqDto zsnewReqDto = new ZsnewReqDto();
        if (insCode) {
            zsnewReqDto.setOrgcode(cusCorpDto.getInsCode());     //组织机构代码 210
        }
        if (certCode) {
            zsnewReqDto.setCreditcode(cusCorpDto.getCertCode()); //统一信用代码 220
        }
        zsnewReqDto.setName(cusCorpDto.getCusName());//企业名称
        zsnewReqDto.setEntstatus(cusCorpDto.getOperStatus());  //企业经营状态，1：在营，2：非在营
//          zsnewReqDto.setEnttype("1");   //企业类型:1-企业 2-个体
        zsnewReqDto.setId(StringUtils.EMPTY);         //中数企业ID
        zsnewReqDto.setRegno(cusCorpDto.getRegiCode());    //企业注册号
        zsnewReqDto.setVersion(StringUtils.EMPTY);  //高管识别码版本号,返回高管识别码时生效
        ResultDto<ZsnewRespDto> zsnewResultDto = dscms2OuterdataClientService.zsnew(zsnewReqDto);
        String zsnewCode = Optional.ofNullable(zsnewResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String zsnewMeesage = Optional.ofNullable(zsnewResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        ZsnewRespDto zsnewRespDto = null;
        if (SuccessEnum.CMIS_SUCCSESS.key.equals(zsnewCode)) {
            String code = zsnewResultDto.getData().getData().getCODE();
            if ("200".equals(code)) {
                //查询成功
                log.info("查询成功");
            } else {
                //没有查询到
                log.info("没有查询到");
            }
        } else {
            log.error("接口交易失败");
            // throw new YuspException(zsnewCode, zsnewMeesage);
        }
    }

    /**
     * 校验客户已存在
     *
     * @param cusCorpDto
     * @throws Exception
     */
    private void checkOnthWay(CusCorpDto cusCorpDto) throws Exception {
        QueryModel model = new QueryModel();
        model.addCondition("certType", cusCorpDto.getCertType());
        model.addCondition("certCode", cusCorpDto.getCertCode());
        List<CusBase> exists = cusBaseMapper.selectByModel(model);
        if (!exists.isEmpty() && exists.size() != 0) {
            String message ="客戶信息";
            if("01".equals(exists.get(0).getCusRankCls())){
                message ="正式客户";
            }else{
                message ="临时客户";
            }
            throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"存在该证件号码的"+message +"，请核对!");
           // throw new Exception("证件号码:" + cusCorpDto.getCertCode() + "的客户已存在！");
        }
    }

    /**
     * 根据cusId校验客户已存在
     *
     * @param cusCorpDto
     */
    private void checkIsByCusId(CusCorpDto cusCorpDto) {
        if (StringUtils.isEmpty(cusCorpDto.getCusId())){
            throw new BizException(null,"",null,"客户号不能为空！");
        }
        QueryModel model = new QueryModel();
        model.addCondition("cusId", cusCorpDto.getCusId());
        List<CusBase> exists = cusBaseMapper.selectByModel(model);
        if (!exists.isEmpty() && exists.size() != 0) {
            throw BizException.error(null,EcsEnum.E_CUS_INFO_EXIST.key,"ECIF返回的客户号"+cusCorpDto.getCusId()+"重复，请联系系统管理员！");
        }
    }

    /**
     * 获取客户关系
     *
     * @param cusCorpRelationsDto
     * @return
     */
    public CusCorpRelationsDto cusRelations(CusCorpRelationsRequestDto cusCorpRelationsRequestDto, CusCorpRelationsDto cusCorpRelationsDto) {
        String custid = cusCorpRelationsRequestDto.getCusId();
        String cusLevel = cusCorpRelationsRequestDto.getCusLevel();
        if (StringUtils.isBlank(custid)) {
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"客户编号\"" + EcsEnum.E_CUS_INFO_ISNULL.value);
        }
        if (StringUtils.isBlank(cusLevel)) {
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"层级\"" + EcsEnum.E_CUS_INFO_ISNULL.value);
        }
        int level = Integer.parseInt(cusLevel);
        /** 获取基本信息 **/
        if (cusCorpRelationsDto == null) {
            cusCorpRelationsDto = cusBaseService.findCusCorpBaseByCusId(custid);
        }
        //将字符串转换成符合SQL查询的条件值
        convert(cusCorpRelationsRequestDto);
        /** 获取高管信息 **/
        CusCorpMgrComDto cusCorpMgrComDto = cusCorpMgrService.cuCorpMgrRelations(cusCorpRelationsRequestDto);
        /** 获取担保信息**/
        CusCorpPubDto cusCorpPubDto = cusPubRelInvestService.cusCorpPubRelations(cusCorpRelationsRequestDto);
        /** 获取股东信息**/
        CusCorpApiDto cusCorpApiDto = cusCorpApitalService.cusCorpApiRelations(cusCorpRelationsRequestDto);

//        CusIndivObisAssureDto cusIndivObisAssureDto = cusIndivObisAssureService.CusIndivObisAssureRelations(cusRelationsRequestDto);
        cusCorpRelationsDto.setCusCorpMgrComDto(cusCorpMgrComDto);
        cusCorpRelationsDto.setCusCorpPubDto(cusCorpPubDto);
        cusCorpRelationsDto.setCusCorpApiDto(cusCorpApiDto);
        level--;
        if (level == 0) {
            return cusCorpRelationsDto;
        }

        /** 获取高管信息 **/
        if (cusCorpRelationsDto.getCusCorpMgrComDto() != null) {
            List<CusCorpRelationsDto> cusCorpRelationsDtos = cusCorpRelationsDto.getCusCorpMgrComDto().getCusCorpRelationsDtos();
            if (!CollectionUtils.isEmpty(cusCorpRelationsDtos) || cusCorpRelationsDtos.size() > 0) {
                for (CusCorpRelationsDto cusCorpRelationsDtoTemp : cusCorpRelationsDtos) {
                    cusCorpRelationsDtoTemp.setCusId(cusCorpRelationsDtoTemp.getCusId());
                    cusCorpRelationsRequestDto.setCusLevel(String.valueOf(level));
                    cusRelations(cusCorpRelationsRequestDto, cusCorpRelationsDtoTemp);
                }
            }
        }
        return cusCorpRelationsDto;
    }


    private void convert(CusCorpRelationsRequestDto cusCorpRelationsRequestDto) {
        String mgrRelations = cusCorpRelationsRequestDto.getMgrRelations();
        String famRelations = cusCorpRelationsRequestDto.getFamRelations();
        String pubRelations = cusCorpRelationsRequestDto.getPubRelations();
        String apiRelations = cusCorpRelationsRequestDto.getApiRelations();
        if (!StringUtils.isBlank(mgrRelations) && mgrRelations.contains(";")) {
            mgrRelations = mgrRelations.substring(0, mgrRelations.lastIndexOf(";"));
            String[] tempArr = mgrRelations.split(";");
            String tempStr = "";
            for (String str : tempArr) {
                tempStr = tempStr + "'" + str + "',";
            }
            mgrRelations = tempStr.substring(0, tempStr.lastIndexOf(","));
            cusCorpRelationsRequestDto.setMgrRelations(mgrRelations);
        }

        if (!StringUtils.isBlank(famRelations) && famRelations.contains(";")) {
            famRelations = famRelations.substring(0, famRelations.lastIndexOf(";"));
            String[] trmpArr = famRelations.split(";");
            String tempStr = "";
            for (String str : trmpArr) {
                tempStr = tempStr + "'" + str + "',";
            }
            famRelations = tempStr.substring(0, tempStr.lastIndexOf(","));
            cusCorpRelationsRequestDto.setFamRelations(famRelations);
        }


        if (!StringUtils.isBlank(pubRelations) && pubRelations.contains(";")) {
            pubRelations = pubRelations.substring(0, pubRelations.lastIndexOf(";"));
            String[] trmpArr = pubRelations.split(";");
            String tempStr = "";
            for (String str : trmpArr) {
                tempStr = tempStr + "'" + str + "',";
            }
            pubRelations = tempStr.substring(0, tempStr.lastIndexOf(","));
            cusCorpRelationsRequestDto.setPubRelations(pubRelations);
        }

        if (!StringUtils.isBlank(apiRelations) && apiRelations.contains(";")) {
            apiRelations = apiRelations.substring(0, apiRelations.lastIndexOf(";"));
            String[] trmpArr = apiRelations.split(";");
            String tempStr = "";
            for (String str : trmpArr) {
                tempStr = tempStr + "'" + str + "',";
            }
            apiRelations = tempStr.substring(0, tempStr.lastIndexOf(","));
            cusCorpRelationsRequestDto.setApiRelations(apiRelations);
        }

    }

    /**
     *
     * 根据客户cusId查询该客户的  客户经理managerId
     * @param cusId
     * @return
     */
//    public String selectManagerId(String cusId) {
//        return  this.cusCorpMapper.selectManagerId(cusId);
//    }

    /**
     * @方法名称: selectByModelXp1
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<Map<String, Object>> selectByModelXp1(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<Map<String, Object>> list = cusCorpMapper.selectByModelXp1(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: queryCorpEvalInfoXp
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public Map<String, Object> queryCorpEvalInfoXp(String cusId) {
        return cusCorpMapper.queryCorpEvalInfoXp(cusId);
    }

    /**
     * @方法名称: selectCusCorpSignUpholdByModel
     * @方法描述: 条件查询国控类标识维护
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CusCorpSingUpholdDto> selectCusCorpSignUpholdByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusCorpSingUpholdDto> list = cusCorpMapper.selectCusCorpSignUpholdByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */

    public CusCorpDto queryCusCropDtoByCusId(String cusId) {
        return cusCorpMapper.queryCusCropDtoByCusId(cusId);
    }

    /**
     * @Description:根据客户号删除客户在CusCorp表中的信息(逻辑删除)
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:21
     * @param cusId: 客户号
     * @return: boolean
     **/
    public boolean deleteCusCorpByCusId(String cusId) {
        int i = cusCorpMapper.deleteCusCorpByCusId(cusId);
        return i==1?true:false;
    }

    public void checkAccount(CusCorpDto cusCorpDto) {
        // g10501 对公及同业客户清单查询
        /**
         * 识别方式:
         * 1- 按证件类型、证件号码查询
         * 2- 按证件号码查询
         * 3- 按客户编号查询
         * 4- 按客户名称模糊查询
         * 以下接口必填一种查询方式；
         */
        G10501ReqDto g10501ReqDto = new G10501ReqDto();
        // TODO StringUtils.EMPTY的实际值待确认 开始
        g10501ReqDto.setResotp("2");// 识别方式
//        g10501ReqDto.setCustna(cusCorpDto.getCusName());// 客户名称
        g10501ReqDto.setIdtftp(cusCorpDto.getCertType());// 证件类型
        g10501ReqDto.setIdtfno(cusCorpDto.getCertCode());// 证件号码
//        g10501ReqDto.setCustst(cusCorpDto.getCusState());// 客户状态
        g10501ReqDto.setBginnm(StringUtils.EMPTY);// 起始笔数
        g10501ReqDto.setQurynm(StringUtils.EMPTY);// 查询笔数
        // TODO StringUtils.EMPTY的实际值待确认 结束
        //TODO 完善后续逻辑
        ResultDto<G10501RespDto> g10501ResultDto = dscms2EcifClientService.g10501(g10501ReqDto);
        String g10501Code = Optional.ofNullable(g10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g10501Meesage = Optional.ofNullable(g10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G10501RespDto g10501RespDto = null;
        if (Objects.equals(g10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            // TODO 获取相关的值并解析
            g10501RespDto = g10501ResultDto.getData();
            log.info(String.valueOf(g10501RespDto));
            /**
             * ECIF系统代码,查询客户状态
             * 正常客户 1
             * 注销客户 2
             * 临时客户 3
             * */
            if (g10501RespDto.getListnm() > 0) {
                if (!"1".equals(g10501RespDto.getListArrayInfo().get(0).getCustst())) {
                    throw BizException.error(null,"9999","该用户在核心系统非正式客户，无法转为正式客户");
                }
            }
        } else {
            // TODO 抛出错误异常
            throw BizException.error(null,g10501Code,g10501Meesage);
        }
    }

    @Transactional
    public CusCorpDto fastCorpCreate(CusCorpDto cusCorpDto) throws Exception {
        // 第一步检查本地
        checkOnthWay(cusCorpDto);
        // 第二步检查ECIf
        G10501ReqDto g10501ReqDto = new G10501ReqDto();
        g10501ReqDto.setResotp(DscmsCusEnum.RESOTP_2.key);//   识别方式
        g10501ReqDto.setIdtftp(cusCorpDto.getCertType());//   证件类型
        g10501ReqDto.setIdtfno(cusCorpDto.getCertCode());//   证件号码
        //  StringUtils.EMPTY的实际值待确认 开始
        // 发送客户三要素发送ecif查询
        ResultDto<G10501RespDto> g10501ResultDto = dscms2EcifClientService.g10501(g10501ReqDto);
        String g10501Code = Optional.ofNullable(g10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String g10501Meesage = Optional.ofNullable(g10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        G10501RespDto g10501RespDto = null;
        if (Objects.equals(g10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
            //  获取查询结果 todo联调验证交易成功查询失败情况
            g10501RespDto = g10501ResultDto.getData();
            if(g10501RespDto.getListnm()>0){
                //cusId=StringUtils.getUUID();
                if(CollectionUtils.nonEmpty(g10501RespDto.getListArrayInfo())){
                    List<ListArrayInfo> listArrayInfo =g10501RespDto.getListArrayInfo();
                    for(int i=0;i<listArrayInfo.size();i++){
                        // 客户号
                        cusCorpDto.setCusId(listArrayInfo.get(0).getCustno());
                    }
                }
            }else{
                // 交易失败
                // 若ecif系统检验不通过，则发送开户接口进行开户
                G00102ReqDto g00102ReqDto = new G00102ReqDto();
                // 客户名称
                if(cusCorpDto.getCusName()!=null&& !cusCorpDto.getCusName().equals("")){
                g00102ReqDto.setCustna(cusCorpDto.getCusName());}
                // 证件号码
                g00102ReqDto.setIdtfno(cusCorpDto.getCertCode());//cusIndivDto.getCertType()
                // 证件类型
                g00102ReqDto.setIdtftp(cusCorpDto.getCertType());
                // 客户状态
                g00102ReqDto.setCustst("3");
                g00102ReqDto.setCusttp("01");

                // 发送Ecif开户
                ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
                String g00102Code = Optional.ofNullable(g00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String g00102Meesage = Optional.ofNullable(g00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                G00102RespDto g00102RespDto = null;
                if (Objects.equals(g00102Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                    //  获取相关的值并解析
                    g00102RespDto = g00102ResultDto.getData();
                    if(g00102RespDto!=null){
                        cusCorpDto.setCusId(g00102RespDto.getCustno());
                    }
                } else {
                    //  抛出错误异常
                    throw BizException.error(null,g00102Code,EcsEnum.CUS_INDIV_CREATE_RESULT.value+g00102Meesage);
                }
            }
        } else {
            //  抛出错误异常
            throw BizException.error(null,g10501Code,EcsEnum.CUS_INDIV_QUERY_RESULT.value+g10501Meesage);
        }
        checkIsByCusId(cusCorpDto);


        if(StringUtils.isEmpty(cusCorpDto.getCusId())){
            throw BizException.error(null,EcsEnum.ECS040008.key,EcsEnum.ECS040008.value);
        }
//        /*
//         *保存cusBase表
//         */
        CusBase cusBase = new CusBase();
        BeanUtils.copyProperties(cusCorpDto, cusBase);
//        //客户大类 1-对私 2-对公
        cusBase.setCusCatalog("2");
//        //客户状态 STD_CUS_STATE 1-暂存2-生效
        cusBase.setCusState("2");
        // 客户状态 02临时客户
        cusBase.setCusRankCls("02");
//        //设置管护机构
        User userInfo = SessionUtils.getUserInformation();
        cusBase.setManagerId(userInfo.getLoginCode());
        cusBase.setManagerBrId(userInfo.getOrg().getCode());
        cusBaseMapper.insertSelective(cusBase);
//
//        /*
//         *保存cusCorp表
//         */
        CusCorp cusCorp = new CusCorp();
        BeanUtils.copyProperties(cusCorpDto, cusCorp);
        cusCorpMapper.insertSelective(cusCorp);
        BeanUtils.copyProperties(cusCorp, cusCorpDto);
        return cusCorpDto;
    }

    public String convertRegitp(String regitp) {
        switch (regitp) {
            case "100":
                regitp = "110";
                break;
            case "140":
                regitp = "141";
                break;
            case "150":
                regitp = "151";
                break;
            case "170":
                regitp = "171";
                break;
            case "175":
                regitp = "171";
                break;
            case "179":
                regitp = "171";
                break;
            case "190":
                regitp = "110";
                break;
            case "200":
                regitp = "210";
                break;
            case "290":
                regitp = "210";
                break;
            case "300":
                regitp = "310";
                break;
            case "390":
                regitp = "310";
                break;
            case "400":
                regitp = "580";
                break;
            case "900":
                regitp = "580";
                break;
            default:
                break;
        }
        return regitp;
    }

    /**
     * @方法名称: checkComScaleOp
     * @方法描述: 校验企业规模
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String checkComScaleOp(String cusId, String comCllType) {
        String scale = "";
        try {
            String stat_prd = "";
            String stat_style = "";

            CusCorp cusCorp = selectByPrimaryKey(cusId);
            if (cusCorp == null) {
                log.info("客户：{},不存在信息", cusId);
            }
            String com_city_flg = cusCorp.getCityType();
            if(com_city_flg != null && ("10".equals(com_city_flg) || "20".equals(com_city_flg))){
                QueryModel model = new QueryModel();
                model.getCondition().put("cusId", cusId);
                model.getCondition().put("stateFlg", "%2");
                model.getCondition().put("statPrd", "%12");
                model.setSort("statPrd desc");
                List<FncStatBase> fncStatBaseList = fncStatBaseService.selectByModel(model);
                if (fncStatBaseList != null && fncStatBaseList.size() > 0) {
                    FncStatBase fncStatBase = fncStatBaseList.get(0);
                    stat_prd = fncStatBase.getStatPrd();
                    stat_style = fncStatBase.getStatStyle();
                    scale = checkComScaleByFnc(cusId, stat_prd, comCllType, stat_style);
                } else {
                    QueryModel queryModel = new QueryModel();
                    queryModel.getCondition().put("cusId", cusId);
                    queryModel.getCondition().put("stateFlg", "%2");
                    queryModel.setSort("statPrd desc");
                    List<FncStatBase> fncBaseList = fncStatBaseService.selectByModel(queryModel);
                    if (fncBaseList != null && fncBaseList.size() > 0) {
                        FncStatBase fncBase = fncBaseList.get(0);
                        stat_prd = fncBase.getStatPrd();
                        stat_style = fncBase.getStatStyle();
                        scale = checkComScaleByFnc(cusId,stat_prd,comCllType, stat_style);
                    }
                }
            }
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return scale;
    }

    public String checkComScaleByFnc(String cusId, String stat_prd, String com_cll_type, String stat_style){
        String new_com_scale = "";
        String year = stat_prd.substring(0, 4);
        String month = stat_prd.substring(4, 6);
        if(!"10".equals(month) && !"11".equals(month) && !"12".equals(month)){
            month = month.substring(1, 2);
        }
        try {
            Map paramsOne = new HashMap();
            if ("12".equals(month)) {
                paramsOne.put("dataField", "stat_end_amt_y");
            } else {
                paramsOne.put("dataField", "stat_end_amt"+month+"");
            }
            paramsOne.put("tableName", "fnc_stat_bs");
            paramsOne.put("cusId", cusId);
            paramsOne.put("statStyle", stat_style);
            paramsOne.put("rptYear", year);
            paramsOne.put("itemId", "Z01000001");
            String zchj = fncStatBsMapper.queryItemValue(paramsOne);

            double zchj_double = hasLength(zchj) ? Double.parseDouble(zchj) : 0.0;
            double zcD = Double.parseDouble(new BigDecimal(zchj_double / 10000.00) + "");//资产合计

            Map paramsTwo = new HashMap();
            if ("12".equals(month)) {
                paramsTwo.put("dataField", "stat_end_amt_y");
            } else {
                paramsTwo.put("dataField", "stat_end_amt"+month+"");
            }
            paramsTwo.put("tableName", "fnc_stat_is");
            paramsTwo.put("cusId", cusId);
            paramsTwo.put("statStyle", stat_style);
            paramsTwo.put("rptYear", year);
            paramsTwo.put("itemId", "L01000000");
            String zyywsr = fncStatBsMapper.queryItemValue(paramsTwo);
            double zyywsr_double = hasLength(zyywsr) ? Double.parseDouble(zyywsr) : 0.0;
            double srD = Double.parseDouble(new BigDecimal(zyywsr_double / 10000.00) + "");//主营业务收入

            Map paramsThree = new HashMap();
            if ("12".equals(month)) {
                paramsThree.put("dataField", "stat_end_amt_y");
            } else {
                paramsThree.put("dataField", "stat_end_amt"+month+"");
            }
            paramsThree.put("tableName", "fnc_stat_is");
            paramsThree.put("cusId", cusId);
            paramsThree.put("statStyle", stat_style);
            paramsThree.put("rptYear", year);
            paramsThree.put("itemId", "L09020000");
            String zzzgrs = fncStatBsMapper.queryItemValue(paramsThree);
            double rsD = org.springframework.util.StringUtils.hasLength(zzzgrs) ? Double.parseDouble(zzzgrs) : 0.0;//在职职工人数

            String cll1 = com_cll_type.substring(0, 1);
            String cll3 = com_cll_type.substring(0, 3);
            String cll4 = com_cll_type.substring(0, 4);
            String cll5 = com_cll_type.substring(0, 5);

            log.info("企业规模判断参数：行业类型：{}，收入：{}，资产：{}，人数：{}，客户编号：{}", com_cll_type, srD, zcD, rsD, cusId);
            //根据行业类型(cll)、收入(srD)、资产(zcD)、人数(rsD)判断企业规模
            if(!"L".equals(cll1) &&
                    ("A,B,C,D,E,H".contains(cll1) || "F51,F52,I63,I64,I65,G54,G55,G56,G57,G58,G59,G60".contains(cll3) || "K7010,K7020".contains(cll5))
                    && srD == 0.00 ){
                if(("B,C,D".contains(cll1) && rsD < 20.00) ||
                        ("E".equals(cll1) && zcD < 300.00) ||
                        ("F51".equals(cll3) && rsD < 5.00) ||
                        ("F52,I63,I64,I65".contains(cll3) && rsD < 10.00) ||
                        ("G54,G55,G56,G57,G58,G59,G60".contains(cll3) && rsD < 20.00) ||
                        ("H,L".contains(cll1) && rsD < 10.00) ||
                        ("K7020".equals(cll5) && rsD < 100.00) ||
                        ("K7010".equals(cll5) && zcD < 2000.00) ||
                        ("A".equals(cll1) && srD != 0.00 && srD < 50.00)){
                    new_com_scale = "4";
                }else if(("B,C,D".contains(cll1) && rsD >= 20.00 && rsD < 300.00) ||
                        ("E".equals(cll1) && zcD >= 300.00 && zcD < 5000.00) ||
                        ("F51".equals(cll3) && rsD >= 5.00 && rsD < 20.00) ||
                        ("F52".equals(cll3) && rsD >= 10.00 && rsD < 50.00) ||
                        ("I63,I64,I65".contains(cll3) && rsD >= 10.00 && rsD < 100.00) ||
                        ("G59".equals(cll3) && rsD >= 20.00 && rsD < 100.00) ||
                        ("G54,G55,G56,G57,G58,G60".contains(cll3) && rsD >= 20.00 && rsD < 300.00) ||
                        ("H,L".contains(cll1) && rsD >= 10.00 && rsD < 100.00) ||
                        ("K7020".equals(cll5) && rsD >= 100.00 && rsD < 300.00) ||
                        ("K7010".equals(cll5) && zcD >= 2000.00 && zcD < 5000.00) ||
                        ("A".equals(cll1) && srD >= 50.00 && srD < 500.00)){
                    new_com_scale = "3";
                }else{
                    new_com_scale = "2";
                }
            }else{
                if("A".equals(cll1)){
                    //类型A: 		收入标准线	50-500-20000
                    if(srD >= 20000.00){
                        new_com_scale = "1";
                    }else if(srD < 20000.00 && srD >= 500.00){
                        new_com_scale = "2";
                    }else if(srD < 500.00 && srD >= 50.00){
                        new_com_scale = "3";
                    }else if(srD < 50.00){
                        new_com_scale = "4";
                    }
                }else if("B,C,D".contains(cll1)){
                    //类型B,C,D: 	人数标准线	20-300-1000
                    //				收入标准线	300-2000-40000
                    if(rsD >= 1000.00){
                        if(srD >= 40000.00){
                            new_com_scale = "1";
                        }else if(srD < 40000.00 && srD >= 2000.00){
                            new_com_scale = "2";
                        }else if(srD < 2000.00 && srD >= 300.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD >= 300.00 && rsD < 1000.00){
                        if(srD >= 2000.00){
                            new_com_scale = "2";
                        }else if(srD < 2000.00 && srD >= 300.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD >= 20.00 && rsD < 300.00){
                        if(srD >= 300.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("E".equals(cll1)){
                    //类型E：		收入标准线	300-6000-80000
                    //				资产标准线	300-5000-80000
                    if(srD >= 80000.00){
                        if(zcD >= 80000.00){
                            new_com_scale = "1";
                        }else if(zcD < 80000.00 && zcD >= 5000.00){
                            new_com_scale = "2";
                        }else if(zcD < 5000.00 && zcD >= 300.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(srD < 80000.00 && srD >= 6000.00){
                        if(zcD >=5000.00){
                            new_com_scale = "2";
                        }else if(zcD < 5000.00 && zcD >= 300.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(srD < 6000.00 && srD >= 300.00){
                        if(zcD >= 300.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("F51".equals(cll3)){
                    //类型F51:		人数标准线	5-20-200
                    //				收入标准线	1000-5000-40000
                    if(rsD >= 200.00){
                        if(srD >= 40000.00){
                            new_com_scale = "1";
                        }else if(srD < 40000.00 && srD >= 5000.00){
                            new_com_scale = "2";
                        }else if(srD < 5000.00 && srD >= 1000.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 200.00 && rsD >= 20.00){
                        if(srD >= 5000.00){
                            new_com_scale = "2";
                        }else if(srD < 5000.00 && srD >= 1000.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 20.00 && rsD >= 5.00){
                        if(srD >= 1000.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("F52".equals(cll3)){
                    //类型F52:		人数标准线	10-50-300
                    //				收入标准线	100-500-20000
                    if(rsD >= 300.00){
                        if(srD >= 20000.00){
                            new_com_scale = "1";
                        }else if(srD < 20000.00 && srD >= 500.00){
                            new_com_scale = "2";
                        }else if(srD < 500.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 300.00 && rsD >= 50.00){
                        if(srD >= 500.00){
                            new_com_scale = "2";
                        }else if(srD < 500.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 50.00 && rsD >= 10.00){
                        if(srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("G54,G55,G56,G57,G58".contains(cll3)){
                    //类型G54,G55,G56,G57,G58:		人数标准线	20-300-1000
                    //								收入标准线	200-3000-30000
                    if(rsD >= 1000.00){
                        if(srD >= 30000.00){
                            new_com_scale = "1";
                        }else if(srD < 30000.00 && srD >= 3000.00){
                            new_com_scale = "2";
                        }else if(srD < 3000.00 && srD >= 200.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 1000.00 && rsD >= 300.00){
                        if(srD >= 3000.00){
                            new_com_scale = "2";
                        }else if(srD < 3000.00 && srD >= 200.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 300.00 && rsD >= 20.00){
                        if(srD >= 200.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("G59".equals(cll3)){
                    //类型G59:		人数标准线	20-100-200
                    //				收入标准线	100-1000-30000
                    if(rsD >= 200.00){
                        if(srD >= 30000.00){
                            new_com_scale = "1";
                        }else if(srD < 30000.00 && srD >= 1000.00){
                            new_com_scale = "2";
                        }else if(srD < 1000.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 200.00 && rsD >= 100.00){
                        if(srD >= 1000.00){
                            new_com_scale = "2";
                        }else if(srD < 1000.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 100.00 && rsD >= 20.00){
                        if(srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("G60".equals(cll3)){
                    //类型G60:		人数标准线	20-300-1000
                    //				收入标准线	100-2000-30000
                    if(rsD >= 1000.00){
                        if(srD >= 30000.00){
                            new_com_scale = "1";
                        }else if(srD < 30000.00 && srD >= 2000.00){
                            new_com_scale = "2";
                        }else if(srD < 2000.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 1000.00 && rsD >= 300.00){
                        if(srD >= 2000.00){
                            new_com_scale = "2";
                        }else if(srD < 2000.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 300.00 && rsD >= 20.00){
                        if(srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("H".equals(cll1)){
                    //类型H:			人数标准线	10-100-300
                    //				收入标准线	100-2000-10000
                    if(rsD >= 300.00){
                        if(srD >= 10000.00){
                            new_com_scale = "1";
                        }else if(srD < 10000.00 && srD >= 2000.00){
                            new_com_scale = "2";
                        }else if(srD < 2000.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 300.00 && rsD >= 100.00){
                        if(srD >= 2000.00){
                            new_com_scale = "2";
                        }else if(srD < 2000.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 100.00 && rsD >= 10.00){
                        if(srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("I63,I64".contains(cll3)){
                    //类型I63,I64:			人数标准线	10-100-2000
                    //						收入标准线	100-1000-100000
                    if(rsD >= 2000.00){
                        if(srD >= 100000.00){
                            new_com_scale = "1";
                        }else if(srD < 100000.00 && srD >= 1000.00){
                            new_com_scale = "2";
                        }else if(srD < 1000.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 2000.00 && rsD >= 100.00){
                        if(srD >= 1000.00){
                            new_com_scale = "2";
                        }else if(srD < 1000.00 && srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 100.00 && rsD >= 10.00){
                        if(srD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("I65".equals(cll3)){
                    //类型I65:			人数标准线	10-100-300
                    //					收入标准线	50-1000-10000
                    if(rsD >= 300.00){
                        if(srD >= 10000.00){
                            new_com_scale = "1";
                        }else if(srD < 10000.00 && srD >= 1000.00){
                            new_com_scale = "2";
                        }else if(srD < 1000.00 && srD >= 50.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 300.00 && rsD >= 100.00){
                        if(srD >= 1000.00){
                            new_com_scale = "2";
                        }else if(srD < 1000.00 && srD >= 50.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 100.00 && rsD >= 10.00){
                        if(srD >= 50.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("K7010".equals(cll5)){
                    //类型K7010:			收入标准线	100-1000-200000
                    //					资产标准线	2000-5000-10000
                    if(srD >= 200000.00){
                        if(zcD >= 10000.00){
                            new_com_scale = "1";
                        }else if(zcD < 10000.00 && zcD >= 5000.00){
                            new_com_scale = "2";
                        }else if(zcD < 5000.00 && zcD >= 2000.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(srD < 200000.00 && srD >= 1000.00){
                        if(zcD >= 5000.00){
                            new_com_scale = "2";
                        }else if(zcD < 5000.00 && zcD >= 2000.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(srD < 1000.00 && srD >= 100.00){
                        if(zcD >= 2000.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("K7020".equals(cll5)){
                    //类型K7020:			人数标准线	100-300-1000
                    //					收入标准线	500-1000-5000
                    if(rsD >= 1000.00){
                        if(srD >= 5000.00){
                            new_com_scale = "1";
                        }else if(srD < 5000.00 && srD >= 1000.00){
                            new_com_scale = "2";
                        }else if(srD < 1000.00 && srD >= 500.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 1000.00 && rsD >= 300.00){
                        if(srD >= 1000.00){
                            new_com_scale = "2";
                        }else if(srD < 1000.00 && srD >= 500.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 300.00 && rsD >= 100.00){
                        if(srD >= 500.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("L".equals(cll1)){
                    //类型L:				人数标准线	10-100-300
                    //					资产标准线	100-8000-120000
                    if(rsD >= 300.00){
                        if(zcD >= 120000.00){
                            new_com_scale = "1";
                        }else if(zcD < 120000.00 && zcD >= 8000.00){
                            new_com_scale = "2";
                        }else if(zcD < 8000.00 && zcD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 300.00 && rsD >= 100.00){
                        if(zcD >= 8000.00){
                            new_com_scale = "2";
                        }else if(zcD < 8000.00 && zcD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else if(rsD < 100.00 && rsD >= 10.00){
                        if(zcD >= 100.00){
                            new_com_scale = "3";
                        }else{
                            new_com_scale = "4";
                        }
                    }else{
                        new_com_scale = "4";
                    }
                }else if("J67".equals(cll3)){
                    //类型J67:			资产标准线	100000-1000000-10000000
                    if(zcD >= 10000000.00){
                        new_com_scale = "1";
                    }else if(zcD < 10000000.00 && zcD >= 1000000.00){
                        new_com_scale = "2";
                    }else if(zcD < 1000000.00 && zcD >= 100000.00){
                        new_com_scale = "3";
                    }else{
                        new_com_scale = "4";
                    }
                }else if("J68".equals(cll3)){
                    //类型J68: 			资产标准线	200000-4000000-50000000
                    if(zcD >= 50000000.00){
                        new_com_scale = "1";
                    }else if(zcD < 50000000.00 && zcD >= 4000000.00){
                        new_com_scale = "2";
                    }else if(zcD < 4000000.00 && zcD >= 200000.00){
                        new_com_scale = "3";
                    }else{
                        new_com_scale = "4";
                    }
                }else if("J662,J692".contains(cll4)){
                    //类型J662,J692: 		资产标准线	500000-50000000-400000000
                    if(zcD >= 400000000.00){
                        new_com_scale = "1";
                    }else if(zcD < 400000000.00 && zcD >= 50000000.00){
                        new_com_scale = "2";
                    }else if(zcD < 50000000.00 && zcD >= 500000.00){
                        new_com_scale = "3";
                    }else{
                        new_com_scale = "4";
                    }
                }else if("J663,J693,J694,J699".contains(cll4)){
                    //类型J663,J693,J694,J699：			资产标准线	500000-2000000-10000000
                    if(zcD >= 10000000.00){
                        new_com_scale = "1";
                    }else if(zcD < 10000000.00 && zcD >= 2000000.00){
                        new_com_scale = "2";
                    }else if(zcD < 2000000.00 && zcD >= 500000.00){
                        new_com_scale = "3";
                    }else{
                        new_com_scale = "4";
                    }
                }else if("J691".equals(cll4)){
                    //类型J691: 			资产标准线	200000-4000000-10000000
                    if(zcD >= 10000000.00){
                        new_com_scale = "1";
                    }else if(zcD < 10000000.00 && zcD < 4000000.00){
                        new_com_scale = "2";
                    }else if(zcD < 4000000.00 && zcD < 200000.00){
                        new_com_scale = "3";
                    }else{
                        new_com_scale = "4";
                    }
                }else{
                    if(rsD >= 300.00){
                        new_com_scale = "1";
                    }else if(rsD < 300.00 && rsD >= 100.00){
                        new_com_scale = "2";
                    }else if(rsD < 100.00 && rsD >= 10.00){
                        new_com_scale = "3";
                    }else{
                        new_com_scale = "4";
                    }
                }
            }

        } catch (Exception e) {
            log.info("计算客户{}，的企业规模出现异常，异常信息{}", cusId, e.getMessage());
        }

        return new_com_scale;
    }

    /**
     * 判断字符串是否具有长度
     * @param string	要判断的字符串
     * @return	如果不为null且长度大于0，返回true；否则返回false
     */
    public boolean hasLength(String string)
    {
        return (string != null) && (string.length() > 0);
    }

    public String toScaleString(BigDecimal decimal, int scale, RoundingMode roundingMode) {
        if(decimal == null) {
            return null;
        } else if(scale < 0) {
            throw new IllegalArgumentException("scale can't be smaller than 0");
        } else if(roundingMode == null) {
            throw new IllegalArgumentException("must specify RoundingMode");
        }
        else
        {
            String format = "#0";
            if(scale > 0)
            {
                format += ".";
                for(int i = 0; i < scale; i++)
                {
                    format += "0";
                }
            }
            NumberFormat nf = new DecimalFormat(format);
            nf.setRoundingMode(roundingMode);
            return nf.format(decimal);
        }
    }

    /**
     * @方法名称: selectCropAndBaseByCusId
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusCorp selectCropAndBaseByCusId(String cusId) {
        return cusCorpMapper.selectCropAndBaseByCusId(cusId);
    }

    /**
     * 获取该客户及其所在集团成员客户最低的外部评级
     * @param map
     * @return
     */
    public String queryMinBankLoanLevel(Map<String, String> map){
        String cusId = map.get("cusId") ;
        String cusCatalog = map.get("cusCatalog") ;
        if (StringUtils.isEmpty(cusId)){
            throw new BizException(null,"",null,"客户编号cusId不能为空！");
        }

        //该客户的信用等级（外部）
        String creditLevelOuter = "";

        if (CmisCusConstants.STD_ZB_CUS_CATALOG_3.equals(cusCatalog)){
            //如果是同业客户，查询该客户的同业客户信息
            CusIntbank cusIntbank = cusIntbankService.selectByPrimaryKey(cusId);

            if (cusIntbank==null || StringUtils.isEmpty(cusIntbank.getCusId())){
                throw new BizException(null,"",null,"根据客户编号【"+cusId+"】没有从同业客户表查到信息！");
            }

            creditLevelOuter = cusIntbank.getIntbankEval();
        }else if (CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusCatalog)){
            //如果是对公客户，查询该客户的对公客户信息
            CusCorp cusCorp = cusCorpMapper.selectByPrimaryKey(cusId);

            if (cusCorp==null || StringUtils.isEmpty(cusCorp.getCusId())){
                throw new BizException(null,"",null,"根据客户编号【"+cusId+"】没有从对公客户表查到信息！");
            }
            creditLevelOuter = cusCorp.getCreditLevelOuter();
        }

        if (creditLevelOuter == null){
            creditLevelOuter = "";
        }

        creditLevelOuter = "["+creditLevelOuter+"]";

        if ("[D]".equals(creditLevelOuter)){
            //如果该客户的外部评级是最小的D，则不再查其所在集团成员客户的最低的外部评级
            return transferStr(creditLevelOuter);
        }

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        //查询该客户的所在集团编号
        Map<String, String> grpNoMap = cusGrpMemberRelService.queryGrpNoByQueryModel(queryModel);

        if (CollectionUtils.isEmpty(grpNoMap)){
            //查不到，则返回该客户的外部评级
            return transferStr(creditLevelOuter);
        }
        //集团编号
        String grpNo = grpNoMap.get("grpNo");
        //查询该客户的所在集团成员客户的最低的外部评级
        String creditLevelOuters = cusCorpMapper.selectCreditLevelOuterByGrpNo(grpNo);

        if (StringUtils.isEmpty(creditLevelOuters)){
            //如果该客户的所在集团成员客户的外部评级为空，则返回该客户的外部评级
            return transferStr(creditLevelOuter);
        }
        creditLevelOuters = "["+creditLevelOuters+"]";
        creditLevelOuters = creditLevelOuters.replace(",","],[");

        //该客户的所在集团成员客户的最低的外部评级
        String minCreditLevelOuter = "";

        String[] creditLevelOuterArr = CmisCusConstants.creditLevelOuterStr.split(",");

        for (String creditLevel : creditLevelOuterArr) {
            if (creditLevelOuters.contains(creditLevel)){
                minCreditLevelOuter = creditLevel;
                break;
            }
        }

        if (StringUtils.isEmpty(creditLevelOuter)){
            //如果该客户的外部评级为空，则返回该客户的所在集团成员客户的最低的外部评级
            return transferStr(minCreditLevelOuter);
        }

        if (StringUtils.isEmpty(minCreditLevelOuter)){
            //如果该客户的所在集团成员客户的最低的外部评级为空，则返回该客户的外部评级
            return transferStr(creditLevelOuter);
        }

        //该客户的外部评级的位置
        int index = CmisCusConstants.creditLevelOuterStr.indexOf(creditLevelOuter);
        //该客户的所在集团成员客户的最低的外部评级的位置
        int index2 = CmisCusConstants.creditLevelOuterStr.indexOf(minCreditLevelOuter);

        //比较该客户的外部评级和该客户的所在集团成员客户的最低的外部评级的位置大小，值小的是最低的外部评级
        if (index<index2){
            return transferStr(creditLevelOuter);
        }else{
            return transferStr(minCreditLevelOuter);
        }
    }

    /**
     * 将形如[AA]转换为AA
     * @param str
     * @return
     */
    public String transferStr(String str){
        str = str.replace("[","");
        str = str.replace("]","");
        return str;
    }

    /**
     * 一键同步Ecif系统
     * @param cusId
     */
    public void updateEcifInfo(String cusId) {
        if (StringUtils.isEmpty(cusId)) {
            throw BizException.error(null, "9999", "客户编号不能为空");
        }
        CusBase cusBase = cusBaseMapper.selectByPrimaryKey(cusId);
        CusCorp cusCorp = cusCorpMapper.selectByPrimaryKey(cusId);
        if (cusBase == null || cusCorp == null) {
            throw BizException.error(null, "9999", "查询客户信息异常");
        }

        if (cusBase != null && cusCorp != null) {
            // 维护客户
            G00102ReqDto g00102ReqDto = new G00102ReqDto();

            /**
             * 1	正常客户
             * 2	注销客户
             * 3	临时客户
             */
            CusCorpDto cusCorpDto = new CusCorpDto();
            cusCorpDto.setCertCode(cusBase.getCertCode());
            cusCorpDto.setCertType(cusBase.getCertType());
            String cusSts = queryCusStsG10501(cusCorpDto);
            if (StringUtils.isBlank(cusSts)) {
                throw BizException.error(null, "9999", "客户状态不能为空");
            }
            log.info("开始维护客户信息，同步Ecif系统，客户号：{}", cusId);
            g00102ReqDto.setCustst(cusSts);
            /**
             * 01	对公客户
             * 02	同业客户
             */
            g00102ReqDto.setCusttp("01");//客户类型
            g00102ReqDto.setCustno(cusId);	//客户编号
            g00102ReqDto.setTwcttp(convertCityType(cusCorp.getCityType()));//城乡类型
            g00102ReqDto.setCustna(cusBase.getCusName());//客户名称
            g00102ReqDto.setCsstna(cusBase.getCusShortName());	//客户简称
            g00102ReqDto.setCsenna(cusCorp.getCusNameEn());	//客户英文名称
            g00102ReqDto.setCustsb(cusCorp.getCusType());	//对公客户类型细分
            g00102ReqDto.setIdtftp(cusBase.getCertType());//证件类型
            g00102ReqDto.setIdtfno(cusBase.getCertCode());//证件号码
            g00102ReqDto.setAreaad(StringUtils.EMPTY);	//发证机关国家或地区
            g00102ReqDto.setEfctdt(StringUtils.EMPTY);//证件生效日期
            g00102ReqDto.setInefdt(DateUtils.formatDate10To8(cusCorp.getCertIdate()));	//证件失效日期
            g00102ReqDto.setDepart(StringUtils.EMPTY);	//发证机关
            g00102ReqDto.setGvrgno(StringUtils.EMPTY);	//营业执照号码
            g00102ReqDto.setGvbgdt(StringUtils.EMPTY);	//营业执照生效日期
            g00102ReqDto.setGveddt(StringUtils.EMPTY);	//营业执照失效日期
            g00102ReqDto.setGvtion(StringUtils.EMPTY);	//营业执照发证机关
            g00102ReqDto.setCropcd(cusCorp.getInsCode());	//组织机构代码
            g00102ReqDto.setOreddt(StringUtils.EMPTY);	//组织机构代码失效日期
            g00102ReqDto.setNttxno(cusCorp.getNatTaxRegCode());	//税务证件号
            g00102ReqDto.setSwbgdt(StringUtils.EMPTY);	//税务证件生效日期
            g00102ReqDto.setSweddt(StringUtils.EMPTY);	//税务证件失效日期
            g00102ReqDto.setSwtion(StringUtils.EMPTY);	//税务证件发证机关
            g00102ReqDto.setJgxyno(StringUtils.EMPTY);	//机构信用代码证
            g00102ReqDto.setCropdt("20991231");	//机构信用代码证失效日期
            g00102ReqDto.setOpcfno(StringUtils.EMPTY);	//开户许可证
            g00102ReqDto.setFoundt(StringUtils.EMPTY);	//成立日期
            g00102ReqDto.setCorppr(cusCorp.getTradeClass());//行业类别
            g00102ReqDto.setBlogto(cusCorp.getSubTyp());	//隶属关系
            g00102ReqDto.setFinctp(cusCorp.getHoldType());	//控股类型（出资人经济类型）
            g00102ReqDto.setInvesj(cusCorp.getInvestMbody());	//投资主体
            g00102ReqDto.setRegitp(cusCorp.getRegiType());	//登记注册类型-20210929数银更换标准
            g00102ReqDto.setRegidt(DateUtils.formatDate10To8(cusCorp.getRegiStartDate()));	//注册日期
            g00102ReqDto.setRegicd(cusCorp.getCountry());	//注册地国家代码
            g00102ReqDto.setRgctry(StringUtils.substring(cusCorp.getRegiAreaCode(), 6));	//注册地行政区划
            g00102ReqDto.setRgcrcy(cusCorp.getPaidCapCurType());	//注册资本币种
            if(cusCorp.getRegiCapAmt()!=null){
                g00102ReqDto.setRgcpam(cusCorp.getRegiCapAmt().divide(new BigDecimal(10000)));	//注册资本金额
            }

            g00102ReqDto.setCacrcy(cusCorp.getPaidCapCurType());	//实收资本币种
            if(cusCorp.getPaidCapAmt()!=null){
                g00102ReqDto.setCacpam(cusCorp.getPaidCapAmt().divide(new BigDecimal(10000)));	//实收资本金额
            }
            g00102ReqDto.setOrgatp(StringUtils.EMPTY);	//组织机构类型
            g00102ReqDto.setSwifcd(StringUtils.EMPTY);	//SWIFT编号
            g00102ReqDto.setSpntcd(StringUtils.EMPTY);	//该SPV或壳机构所属国家/地区
            g00102ReqDto.setAdmnti(StringUtils.EMPTY);	//经营期限
            g00102ReqDto.setAdmnst(cusCorp.getOperStatus());	//经营状态
            g00102ReqDto.setCorpsz(cusCorp.getCorpScale());	//企业规模
            g00102ReqDto.setBusisp(cusCorp.getOperAddrAct());//经营范围  提交之后更新
            g00102ReqDto.setAcesbs(StringUtils.EMPTY);	//客户兼营业务
            g00102ReqDto.setEmplnm(cusCorp.getFjobNum());	//员工总数
            g00102ReqDto.setComput(cusCorp.getAdminOrg());	//主管单位
            g00102ReqDto.setLoanno(cusCorp.getLoanCardId());	//贷款卡编码
            g00102ReqDto.setWkflar(cusCorp.getOperPlaceSqu()==null? null:cusCorp.getOperPlaceSqu().intValue());	//经营场地面积
            g00102ReqDto.setWkflfe(cusCorp.getOperPlaceOwnshp());	//经营场地所有权
            g00102ReqDto.setBaseno(StringUtils.EMPTY);	//基本账户核准号
            g00102ReqDto.setAcctnm(StringUtils.EMPTY);	//基本账户开户行名称
            g00102ReqDto.setAcctbr(StringUtils.EMPTY);
            g00102ReqDto.setAcctno(StringUtils.EMPTY);	//基本账户账号
            g00102ReqDto.setAcctdt(cusCorp.getBasicAccNoOpenDate());	//基本账户开户日期
            g00102ReqDto.setSthdfg(cusCorp.getIsBankShd());	//是否我行股东
            // g00102ReqDto.setSharhd();	//拥有我行股份金额
            g00102ReqDto.setListtg(cusCorp.getIsStockCorp());	//是否上市企业
            CusCorpStock cusCorpStock = null;
            if(!StringUtils.isEmpty(cusCorp.getIsStockCorp()) && "1".equals(cusCorp.getIsStockCorp())){
                QueryModel qmCusCorpStock= new QueryModel();
                qmCusCorpStock.addCondition("cusId",cusId);
                List<CusCorpStock> cusCorpStockList= cusCorpStockService.selectByModel(qmCusCorpStock);
                if(!CollectionUtils.isEmpty(cusCorpStockList)){
                    cusCorpStock = cusCorpStockList.get(0);
                    g00102ReqDto.setListld(cusCorpStock.getStkMrkPlace());	//上市地
                    g00102ReqDto.setStoccd(cusCorpStock.getStkCode());	//股票代码
                }
            }
            g00102ReqDto.setIsbdcp(StringUtils.EMPTY);	//是否是担保公司
            g00102ReqDto.setArimtg(cusCorp.getAreaPriorCorp());	//是否地区重点企业
            g00102ReqDto.setSctlpr(StringUtils.EMPTY);	//是否国家宏观调控限控行业
            g00102ReqDto.setSpclfg(cusCorp.getSpOperFlag());	//特种经营标识
            g00102ReqDto.setSteltg(cusCorp.getIsSteelCus());	//是否钢贸企业
            g00102ReqDto.setDomitg(StringUtils.EMPTY);	//是否优势企业
            g00102ReqDto.setStrutp(StringUtils.EMPTY);	//产业结构调整类型
            g00102ReqDto.setStratp(StringUtils.EMPTY);	//战略新兴产业类型
            g00102ReqDto.setIndutg(StringUtils.EMPTY);	//工业转型升级标识
            g00102ReqDto.setImpotg(cusCorp.getImpexpFlag());	//进出口权标志
            g00102ReqDto.setPlattg(cusCorp.getIisSzjrfwCrop());	//是否苏州综合平台企业
            g00102ReqDto.setGvfntg(cusCorp.getGoverInvestPlat());	//是否政府投融资平台
            g00102ReqDto.setFscddt(cusCorp.getInitLoanDate());	//首次与我行建立信贷关系时间
            g00102ReqDto.setCtigno(StringUtils.EMPTY);	//客户经理号
            g00102ReqDto.setCtigna(StringUtils.EMPTY);	//客户经理姓名
            g00102ReqDto.setHometl(StringUtils.EMPTY);	//联系电话
            g00102ReqDto.setRgpost(StringUtils.EMPTY);	//注册地址邮编
            g00102ReqDto.setRgcuty(StringUtils.EMPTY);	//注册地址所在国家
            g00102ReqDto.setRgprov(StringUtils.EMPTY);//注册地址所在省
            g00102ReqDto.setRgcity(StringUtils.EMPTY);//注册地址所在市
            g00102ReqDto.setRgerea(StringUtils.EMPTY);//注册地址所在地区
            g00102ReqDto.setRegadr(Optional.ofNullable(cusCorpDto.getRegisterAddress()).orElse(cusCorpDto.getRegiAreaCode()));//注册地址
            g00102ReqDto.setBscuty(StringUtils.EMPTY);	//经营地址所在国家
            g00102ReqDto.setBusiad(StringUtils.EMPTY);	//经营地址
            g00102ReqDto.setWebsit(StringUtils.EMPTY);	//公司网址
            g00102ReqDto.setFaxfax(StringUtils.EMPTY);	//传真
            ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
            String g00202Code = Optional.ofNullable(g00102ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String g00202Meesage = Optional.ofNullable(g00102ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            G00102RespDto g00102RespDto = null;
            if (Objects.equals(g00202Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                g00102RespDto = g00102ResultDto.getData();
                log.info("维护成功，响应：{}"+ g00102ReqDto);
            } else {
                // TODO 抛出错误异常
                throw BizException.error(null,g00202Code,g00202Meesage);
            }
        }
    }

    public String convertCityType(String cityType) {
        if (Objects.equals("10", cityType)) {
            cityType = "01";
        } else if (Objects.equals("11", cityType)) {
            cityType = "02";
        } else if (Objects.equals("21", cityType)) {
            cityType = "04";
        } else if (Objects.equals("20", cityType)) {
            cityType = "03";
        } else if (Objects.equals("22", cityType)) {
            cityType = "05";
        } else if (Objects.equals("23", cityType)) {
            cityType = "06";
        }
        return cityType;
    }
}
