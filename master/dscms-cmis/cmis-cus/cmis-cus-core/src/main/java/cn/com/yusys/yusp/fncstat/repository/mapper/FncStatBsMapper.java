/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.fncstat.domain.FncStatBs;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptItemDTO;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatBsMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: zzbankwb473
 * @创建时间: 2019-12-02 09:42:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public interface FncStatBsMapper {


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<FncStatBs> selectByModel(QueryModel model);

    /**
     * 执行拼接好的select语句
     *
     * @param params
     * @return
     */
    String queryItemValue(@Param("params") Map<String, String> params);

    /**
     * 查询上年报表数据
     *
     * @param cusId
     * @param statStyle
     * @param lastYear
     * @return
     */
    @MapKey("statItemId")
    Map<String, FncStatBs> selectLastYearRpt(@Param("cusId") String cusId, @Param("statStyle") String statStyle, @Param("lastYear") String lastYear);

    Double querySumValue(@Param("cusId") String cusId, @Param("year") String year, @Param("statStyle") String statStyle);

    /**
     * 查询报表数值信息----财务报表指标分析取数专用 固定值
     * @param params
     * @return
     */
    List<String> queryItemValueByParams(@Param("params") Map<String, String> params);

    /**
     * 查询报表数值信息----财务报表指标分析取数专用 非固定值
     * @param params
     * @return
     */
    List<FncStatRptItemDTO> queryItemValueByParams2(@Param("params") Map<String, String> params);

    /**
     * 查询报表数值信息----财务报表指标分析取数专用 非固定值 关联表
     * @param queryParams
     * @return
     */

    List<FncStatRptItemDTO> queryItemValueByParams3(@Param("params") Map<String, String> queryParams);
}