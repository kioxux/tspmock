package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class CusGrpMemberTreeDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 集团编号 **/
    private String grpNo;

    /** 集团名称 **/
    private String grpName;

    /** 成员客户编号 **/
    private String cusId;

    /** 成员客户名称 **/
    private String cusName;

    /** 集团关联关系类型 STD_ZB_GRP_CO_TYPE **/
    private String grpCorreType;

    /** 集团关联关系描述 **/
    private String grpCorreDetail;

    /** 集团融资类型 **/
    private String grpFinanceType;

    /** 是否集团主客户 STD_ZB_YES_NO **/
    private String isMainCus;

    /** 集团情况说明 **/
    private String grpDetail;

    /** 统一社会信用代码 **/
    private String sociCredCode;

    /** 主管人 **/
    private String mainId;

    /** 主管机构 **/
    private String mainBrId;

    /** 是否有效 STD_ZB_DATA_STS **/
    private String availableInd;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 集团紧密程度 STD_ZB_CLOSELY_DEGREE **/
    private String grpCloselyDegree;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getGrpNo() {
        return grpNo;
    }

    public void setGrpNo(String grpNo) {
        this.grpNo = grpNo;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGrpCorreType() {
        return grpCorreType;
    }

    public void setGrpCorreType(String grpCorreType) {
        this.grpCorreType = grpCorreType;
    }

    public String getGrpCorreDetail() {
        return grpCorreDetail;
    }

    public void setGrpCorreDetail(String grpCorreDetail) {
        this.grpCorreDetail = grpCorreDetail;
    }

    public String getGrpFinanceType() {
        return grpFinanceType;
    }

    public void setGrpFinanceType(String grpFinanceType) {
        this.grpFinanceType = grpFinanceType;
    }

    public String getIsMainCus() {
        return isMainCus;
    }

    public void setIsMainCus(String isMainCus) {
        this.isMainCus = isMainCus;
    }

    public String getGrpDetail() {
        return grpDetail;
    }

    public void setGrpDetail(String grpDetail) {
        this.grpDetail = grpDetail;
    }

    public String getSociCredCode() {
        return sociCredCode;
    }

    public void setSociCredCode(String sociCredCode) {
        this.sociCredCode = sociCredCode;
    }

    public String getMainId() {
        return mainId;
    }

    public void setMainId(String mainId) {
        this.mainId = mainId;
    }

    public String getMainBrId() {
        return mainBrId;
    }

    public void setMainBrId(String mainBrId) {
        this.mainBrId = mainBrId;
    }

    public String getAvailableInd() {
        return availableInd;
    }

    public void setAvailableInd(String availableInd) {
        this.availableInd = availableInd;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getGrpCloselyDegree() {
        return grpCloselyDegree;
    }

    public void setGrpCloselyDegree(String grpCloselyDegree) {
        this.grpCloselyDegree = grpCloselyDegree;
    }
}
