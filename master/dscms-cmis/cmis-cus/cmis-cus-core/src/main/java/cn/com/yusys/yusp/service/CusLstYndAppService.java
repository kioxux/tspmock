/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusLstYndAppDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.ListArrayInfo;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.req.CmisCus0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.resp.CmisCus0011RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.req.CmisCus0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.resp.CmisCus0019RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.req.CmisCus0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.resp.CmisCus0025RespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusLstYndAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:20:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYndAppService {
    private static final Logger log = LoggerFactory.getLogger(CusLstYndAppService.class);
    @Autowired
    private CusLstYndAppMapper cusLstYndAppMapper;
    @Autowired
    private CusBaseMapper cusBaseMapper;
    @Autowired
    private CusLstYndDcjlAppService cusLstYndDcjlAppService; // 优农贷调查结论
    @Autowired
    private CusLstYndJyxxAppService cusLstYndJyxxAppService; // 优农贷经营信息
    @Autowired
    private CusLstYndCmdcAppService cusLstYndCmdcAppService; // 优农贷侧面调查
    @Autowired
    private CusLstYndLinkmanService cusLstYndLinkmanService; // 优农贷紧急联系人
    @Autowired
    private CusLstYndService cusLstYndService; // 优农贷名单
    @Resource
    private SequenceTemplateClient sequenceTemplateClient; // 流水号获取
    @Autowired
    private Dscms2EcifClientService dscms2EcifClientService;
    @Autowired
    private CusBaseService cusBaseService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusLstYndApp selectByPrimaryKey(String serno) {
        return cusLstYndAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectYndLstByCertCode
     * @方法描述: 查询优农贷名单信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CmisCus0011RespDto selectYndLstByCertCode(CmisCus0011ReqDto cmisCus0011ReqDto) {
        return cusLstYndAppMapper.selectYndLstByCertCode(cmisCus0011ReqDto);
    }

    /**
     * @方法名称: selectYndLstByCertCode
     * @方法描述: 查询优农贷名单信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int selectYndCountByCertCode(CmisCus0019ReqDto cmisCus0019ReqDto) {
        return cusLstYndAppMapper.selectYndCountByCertCode(cmisCus0019ReqDto);
    }

    /**
     * @方法名称: selectdeatilYndCountByCertCode
     * @方法描述: 优农贷名单详细信息查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CmisCus0025RespDto selectdeatilYndCountByCertCode(CmisCus0025ReqDto cmisCus0025ReqDto) {
        return cusLstYndAppMapper.selectdeatilYndCountByCertCode(cmisCus0025ReqDto);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYndApp> selectAll(QueryModel model) {
        List<CusLstYndApp> records = (List<CusLstYndApp>) cusLstYndAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusLstYndApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYndApp> list = cusLstYndAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusLstYndApp record) {
        return cusLstYndAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusLstYndApp record) {
        return cusLstYndAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusLstYndApp record) {
        return cusLstYndAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusLstYndApp record) {
        return cusLstYndAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        CusLstYndApp cusLstYndApp = cusLstYndAppMapper.selectByPrimaryKey(serno);
        if (Objects.nonNull(cusLstYndApp) && CmisBizConstants.APPLY_STATE_CALL_BACK.equals(cusLstYndApp.getApproveStatus())){
            // 删除流程实例
            workflowCoreClient.deleteByBizId(serno);
        }
        return cusLstYndAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYndAppMapper.deleteByIds(ids);
    }


    /**
     * 优农贷准入名单申请提交，后续的业务处理
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterStart(String serno) {
        CusLstYndApp cusLstYndApp = cusLstYndAppMapper.selectByPrimaryKey(serno);
        cusLstYndApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
        cusLstYndAppMapper.updateByPrimaryKey(cusLstYndApp);
    }


    /**
     * 优农贷准入名单申请拒绝，后续的业务处理
     *
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterRefuse(String serno) {
        CusLstYndApp cusLstYndApp = cusLstYndAppMapper.selectByPrimaryKey(serno);
        cusLstYndApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
        cusLstYndAppMapper.updateByPrimaryKey(cusLstYndApp);
    }

    /**
     * 优农贷准入名单申请打回，后续的业务处理
     *
     * @param serno
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleBusinessAfterBack(String serno) {
        CusLstYndApp cusLstYndApp = cusLstYndAppMapper.selectByPrimaryKey(serno);
        cusLstYndApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
        cusLstYndAppMapper.updateByPrimaryKey(cusLstYndApp);
    }

    /**
     * @函数名称:tempSave
     * @函数描述:单个对象保存(新增或修改)
     * @参数与返回说明:
     * @算法描述:
     */
    public int tempSave(CusLstYndApp cusLstYndApp) {
        String serno = cusLstYndApp.getSerno();
        // 根据BUG12206 修改 2021年9月23日21:23:15  hubp
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusLstYndApp.getCusId());
        model.addCondition("approveStatus","111");
        List<CusLstYndApp> list = cusLstYndAppMapper.selectByModel(model);
        if(list.size() > 0){
            return -1;
        }
        //如果是提交，则校验关联的信息是否存在
        if(cusLstYndApp.getSaveFlag()!=null && "submit".equals(cusLstYndApp.getSaveFlag())){
            List<CusLstYndCmdcApp>  list1  = cusLstYndCmdcAppService.selectBySerno(serno);
            List<CusLstYndJyxxApp>  list2  = cusLstYndJyxxAppService.selectBySerno(serno);
            CusLstYndDcjlApp  cusLstYndDcjlApp  = cusLstYndDcjlAppService.selectByPrimaryKey(serno);
            if(list1.size()==0 ){
                throw BizException.error(null, "9999", "无侧面调查信息，无法提交！");
            }else if(list2.size() ==0 ){
                throw BizException.error(null, "9999", "无经营信息，无法提交！");
            }else if(cusLstYndDcjlApp==null){
                throw BizException.error(null, "9999", "无调查结论信息，无法提交！");
            }
        }

        if(StringUtils.isBlank(serno)){
            String thisSerno = sequenceTemplateClient.getSequenceTemplate("MD_SERNO", new HashMap());
            cusLstYndApp.setSerno(thisSerno);
            cusLstYndApp.setApproveStatus("000");
            return cusLstYndAppMapper.insert(cusLstYndApp);
        }else{
            //查询该对象是否存在
            CusLstYndApp queryCusLstYndApp = cusLstYndAppMapper.selectByPrimaryKey(serno);
            if (null == queryCusLstYndApp){
                return cusLstYndAppMapper.insert(cusLstYndApp);
            }else{
                return cusLstYndAppMapper.updateByPrimaryKey(cusLstYndApp);
            }
        }
    }

    /**
     *根据证件号查询客户信息,如果是本行客户则返回,否则去ecif查询,ecif没有去ecif开户,后数据本地落库
     * @param cusLstYndApp
     * @return
     */
    public CusLstYndApp queryCusByCertCodeToEcif(CusLstYndApp cusLstYndApp) {
        String certCode = cusLstYndApp.getCertCode();
        String cusName = cusLstYndApp.getCusName();
        //身份证
        String certType = "";

        CusLstYndApp cusLstYndAppData = null;
        //客户名,证件号为空判断
        if (!StringUtils.isBlank(certCode) && !StringUtils.isBlank(cusName)){
            //根据证件号客户名查询 有数据返回
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusName",cusName);
            queryModel.addCondition("certCode",certCode);
            List<CusLstYndApp> cusLstYndApps = cusLstYndAppMapper.selectByModel(queryModel);
            if (cusLstYndApps != null && cusLstYndApps.size() > 0){
            cusLstYndAppData = cusLstYndApps.get(0);
                return cusLstYndAppData;
            }
            CusBase cusBaseData = cusBaseMapper.queryCusInfoByCertCode(certCode);
            //cusbase这里从ecif查的数据没落库,待处理 //TODO
            if (cusBaseData == null){
                // 第二步 校验客户信息  发送身份校验
                IdCheckReqDto reqDto = new IdCheckReqDto();
                reqDto.setCert_type(certType);
                reqDto.setUser_name(cusName);
                reqDto.setId_number(certCode);
                //ResultDto<Map<String, Object>> result = dscms2EcifClientService.idCheck(reqDto);
                //log.info("发送身份校验,请求{}，返回{}", reqDto, result);
                //TODO 判断接口返回结果

                // 第三步 再第一步成功的基础上，进行客户三要素发送ecfi查询，
                //识别方式:
                //1- 按证件类型、证件号码查询
                //2- 按证件号码查询
                //3- 按客户编号查询
                //4- 按客户名称模糊查询
                //以下接口必填一种查询方式；
                String cusState = "";
                S10501ReqDto s10501ReqDto = new S10501ReqDto();
                s10501ReqDto.setResotp(DscmsCusEnum.RESOTP_1.key);//   识别方式
                s10501ReqDto.setIdtftp(certType);//   证件类型
                s10501ReqDto.setIdtfno(certCode);//   证件号码
                //  StringUtils.EMPTY的实际值待确认 开始
                s10501ReqDto.setResotp(StringUtils.EMPTY);//   识别方式
                //  完善后续逻辑
                ResultDto<S10501RespDto> s10501ResultDto = dscms2EcifClientService.s10501(s10501ReqDto);
                String s10501Code = Optional.ofNullable(s10501ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String s10501Meesage = Optional.ofNullable(s10501ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                S10501RespDto s10501RespDto = null;
                String cusId ="";
                if (Objects.equals(s10501Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                    //  获取查询结果 todo联调验证交易成功查询失败情况
                    s10501RespDto = s10501ResultDto.getData();
                    if(s10501RespDto.getListnm() !=null) {
                        List<ListArrayInfo> listArrayInfo = s10501RespDto.getListArrayInfo();
                            // 客户号
                            cusId = listArrayInfo.get(0).getCustno();
                        }
                    } else {
                    //  抛出错误异常
                    throw new BizException(null,s10501Code,null,s10501Meesage);
                }
                //  完善后续逻辑
                //验证CIF返回报文
                if ("".equals(cusId)){
                    // 若ecif系统检验不通过，则发送开户接口进行开户
                    G00102ReqDto g00102ReqDto = new G00102ReqDto();
                    // 客户名称
                    g00102ReqDto.setCustna(cusName);
                    // 证件类型
                    g00102ReqDto.setIdtfno(certType);
                    // 证件号码
                    //g00102ReqDto.setEfctdt(indivCusCreateDto.getCertCode());
                    //ResultDto<G00102RespDto> g00102ResultDto = dscms2EcifClientService.g00102(g00102ReqDto);
                    //G00102RespDto g00102ResDto = g00102ResultDto.getData();
                    //第三步，拿到客户信息后先插入任务表，并返回客户信息到信息补录页签
                    //ifExists = sendEcif(indivCusCreateDto);
                    //验证CIF返回报文
                }
            }else{
                BeanUtils.copyProperties(cusBaseData, cusLstYndAppData);
                return cusLstYndAppData;
            }
        }else{
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"传入值为空\"" + EcsEnum.E_CUS_INFO_ISNULL.value);
        }
        return null;

    }

    /**
     * @param serno
     * @return java.lang.Integer
     * @author hubp
     * @date 2021/7/16 14:02
     * @version 1.0.0
     * @desc    通过调查流水号--级联删除优农贷信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto deleteBySerno(String serno) {
        log.info("************通过调查流水号--级联删除优农贷信息开始***********", serno);
        //开始删除调查结论
        cusLstYndDcjlAppService.deleteByPrimaryKey(serno);
        // 开始删除侧面调查信息
        cusLstYndCmdcAppService.deleteBySerno(serno);
        // 开始删除经营信息
        cusLstYndJyxxAppService.deleteBySerno(serno);
        // 开始删除紧急联系人
        cusLstYndLinkmanService.deleteBySerno(serno);
        // 开始删除基本信息
        cusLstYndAppMapper.deleteByPrimaryKey(serno);
        log.info("************通过调查流水号--级联删除优农贷信息结束***********", serno);
        return new ResultDto(0);
    }

    /**
     * @param cusLstYndApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/7/21 9:18
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */

    public ResultDto addCusLstYndApp(CusLstYndApp cusLstYndApp) {
        String serno = cusLstYndApp.getSerno();
        if(StringUtils.isBlank(serno)){
            String thisSerno = sequenceTemplateClient.getSequenceTemplate("MD_SERNO", new HashMap());
            if (StringUtils.isBlank(cusLstYndApp.getCusId())) {
                String cusId = getCusId(cusLstYndApp.getCertCode());
                if (StringUtils.isBlank(cusId)) {
                    return new ResultDto().code(9999).message("该客户尚未开户！");
                } else {
                    cusLstYndApp.setCusId(cusId);
                }
            }
            cusLstYndApp.setSerno(thisSerno);
            cusLstYndApp.setApproveStatus("000");
            cusLstYndApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            cusLstYndAppMapper.insert(cusLstYndApp);
        }else{
            //查询该对象是否存在
            CusLstYndApp queryCusLstYndApp = cusLstYndAppMapper.selectByPrimaryKey(serno);
            if (StringUtils.isBlank(cusLstYndApp.getCusId())) {
                String cusId = getCusId(cusLstYndApp.getCertCode());
                if (StringUtils.isBlank(cusId)) {
                    return new ResultDto().code(9999).message("该客户尚未开户！");
                } else {
                    cusLstYndApp.setCusId(cusId);
                }
            }
            if (null == queryCusLstYndApp){
                cusLstYndApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                cusLstYndApp.setApproveStatus("000");
                cusLstYndAppMapper.insert(cusLstYndApp);
            }else{
                cusLstYndAppMapper.updateByPrimaryKeySelective(cusLstYndApp);
            }
        }
        return new ResultDto(cusLstYndApp);
    }

    /**
     * @param
     * @return
     * @author hubp
     * @date 2021/10/11 20:40
     * @version 1.0.0
     * @desc  通过证件号码返回CUSID
     * @修改历史: 修改时间    修改人员    修改原因
     */

    private String getCusId(String certCode) {
        String cusId = StringUtils.EMPTY;
        CusBase cusBase = cusBaseService.queryCusInfoByCertCode(certCode);
        log.info("********根据证件号码查询客户信息********，证件号码：【{}】,返回报文：【{}】", certCode,JSON.toJSONString(cusBase));
        if (Objects.isNull(cusBase)) {
            return cusId;
        } else {
            return cusBase.getCusId();
        }
    }
    /**
     * @param cusLstYndAppDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/9/15 20:29
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto insertYndApp(CusLstYndAppDto cusLstYndAppDto) {
        CusLstYndApp cusLstYndApp = new CusLstYndApp();
        String thisSerno = sequenceTemplateClient.getSequenceTemplate("MD_SERNO", new HashMap());
        BeanUtils.copyProperties(cusLstYndAppDto,cusLstYndApp);
        cusLstYndApp.setSerno(thisSerno);
        int result =cusLstYndAppMapper.insert(cusLstYndApp);
        return new ResultDto(result);
    }
}
