/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusRelCusMemberRel;
import cn.com.yusys.yusp.repository.mapper.CusRelCusMemberRelMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusRelCusMemberRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: liqiang
 * @创建时间: 2021-05-07 21:29:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusRelCusMemberRelService {

    @Autowired
    private CusRelCusMemberRelMapper cusRelCusMemberRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusRelCusMemberRel selectByPrimaryKey(String pkId) {
        return cusRelCusMemberRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusRelCusMemberRel> selectAll(QueryModel model) {
        List<CusRelCusMemberRel> records = (List<CusRelCusMemberRel>) cusRelCusMemberRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusRelCusMemberRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusRelCusMemberRel> list = cusRelCusMemberRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusRelCusMemberRel record) {
        return cusRelCusMemberRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusRelCusMemberRel record) {
        return cusRelCusMemberRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusRelCusMemberRel record) {
        return cusRelCusMemberRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusRelCusMemberRel record) {
        return cusRelCusMemberRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusRelCusMemberRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusRelCusMemberRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: deleteByCusNoAndMemCusNo
     * @方法描述: 根据关联编号、关联成员编号删除数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByCusNoAndMemCusNo(CusRelCusMemberRel record) {
        return cusRelCusMemberRelMapper.deleteByCusNoAndMemCusNo(record);
    }
}
