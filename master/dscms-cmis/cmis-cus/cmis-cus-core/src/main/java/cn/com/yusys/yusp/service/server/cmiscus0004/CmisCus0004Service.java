package cn.com.yusys.yusp.service.server.cmiscus0004;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0004.req.CmisCus0004ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0004.resp.CmisCus0004IndivCusRelListRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0004.resp.CmisCus0004RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 业务逻辑处理类：个人关联客户列表查询
 *
 * @author dumd
 * @version 1.0
 * @since 2021年5月18日
 */
@Service
public class CmisCus0004Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0004Service.class);

    @Autowired
    private CusCorpMgrService cusCorpMgrService;

    @Autowired
    private CusIndivSocialService cusIndivSocialService;

    @Autowired
    private CusCorpApitalService cusCorpApitalService;

    @Autowired
    private CusIndivUnitService cusIndivUnitService;

    @Transactional
    public CmisCus0004RespDto execute(CmisCus0004ReqDto reqDto) throws YuspException {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0004.key, DscmsEnum.TRADE_CODE_CMISCUS0004.value);

        CmisCus0004RespDto respDto = new CmisCus0004RespDto();
        List<CmisCus0004IndivCusRelListRespDto> cusRelList = new ArrayList<>();

        //key：高管类别字典项enname，value：高管类别字典项cnname
        Map<String,String> dictMap = new HashMap<>();

        try{
            String cusId = reqDto.getCusId();

            List<String> cusIdList = new ArrayList<>();
            cusIdList.add(cusId);
            List<Map<String, String>> cusSocialList = cusIndivSocialService.selectSocialInfoByCusIdRelList(cusIdList);

            if (cusSocialList!=null && cusSocialList.size()>0){

                for (Map<String, String> cusSocial : cusSocialList) {
                    //客户id
                    String cusIdRel = (String) cusSocial.get("cusId");
                    //与客户关系
                    String indivCusRel = (String) cusSocial.get("indivCusRel");
                    //客户姓名
                    String name = (String) cusSocial.get("name");

                    CmisCus0004IndivCusRelListRespDto indivCusRelListRespDto = new CmisCus0004IndivCusRelListRespDto();
                    indivCusRelListRespDto.setCusIdRel(cusIdRel);
                    indivCusRelListRespDto.setCusNameRel(name);
                    indivCusRelListRespDto.setCusTypeRel(CmisCusConstants.STD_ZB_CUS_CATALOG_1);//客户大类默认对私客户

                    if (CmisCusConstants.STD_ZB_INDIV_CUS_1.equals(indivCusRel)){
                        indivCusRelListRespDto.setRelType("配偶");
                    }else if (CmisCusConstants.STD_ZB_INDIV_CUS_2.equals(indivCusRel)){
                        indivCusRelListRespDto.setRelType("父母");
                    }else if (CmisCusConstants.STD_ZB_INDIV_CUS_3.equals(indivCusRel)){
                        indivCusRelListRespDto.setRelType("子女");
                    }
                    logger.info("查询【{}】的配偶、父母、子女等【{}】",cusId , JSON.toJSONString(indivCusRelListRespDto));
                    cusRelList.add(indivCusRelListRespDto);
                }
            }

            //查询关联企业客户编号
            Map<String, String> unitIdMap = cusIndivUnitService.selectUnitIdByCusId(cusId);

            if (unitIdMap!=null) {
                String unitId = unitIdMap.get("unitId");
                CmisCus0004IndivCusRelListRespDto indivCusRelListRespDto = new CmisCus0004IndivCusRelListRespDto();
                indivCusRelListRespDto.setCusIdRel((String) unitIdMap.get("unitId"));
                indivCusRelListRespDto.setCusNameRel((String) unitIdMap.get("cusName"));
                indivCusRelListRespDto.setCusTypeRel((String) unitIdMap.get("cusCatalog"));
                indivCusRelListRespDto.setRelType("所属企业");
                logger.info("查询【{}】的关联企业信息【{}】",cusId , JSON.toJSONString(indivCusRelListRespDto));
                cusRelList.add(indivCusRelListRespDto);

                if (StringUtils.isNotEmpty(unitId)){
                    //企业客户编号不为空
                    //1.查询企业的股东
                    //查询客户资本构成信息表里关联法人的出资人信息
                    List<Map<String, String>> corpApitalList = cusCorpApitalService.queryCusInfoByCusIdRel(unitId);

                    for (Map<String, String> corpApital : corpApitalList) {
                        indivCusRelListRespDto = new CmisCus0004IndivCusRelListRespDto();
                        indivCusRelListRespDto.setCusIdRel((String) corpApital.get("cusId"));
                        indivCusRelListRespDto.setCusNameRel((String) corpApital.get("invtName"));
                        indivCusRelListRespDto.setCusTypeRel((String) corpApital.get("cusCatalog"));
                        indivCusRelListRespDto.setRelType("股东");
                        logger.info("查询【{}】的股东信息【{}】",cusId , JSON.toJSONString(indivCusRelListRespDto));
                        cusRelList.add(indivCusRelListRespDto);
                    }

                    //2.查询企业高管
                    List<Map<String, String>> comMgrList = cusCorpMgrService.selectCusInfoByCusIdRel(unitId);

                    if (!comMgrList.isEmpty()){
                        //初始化高管类别字典项
                        initDictMap(dictMap);
                        for (Map<String, String> comMgr : comMgrList) {
                            indivCusRelListRespDto = new CmisCus0004IndivCusRelListRespDto();
                            //高管类别
                            String mrgType = (String) comMgr.get("mrgType");
                            String mrgTypeName = dictMap.get(mrgType) ;
                            if(StringUtils.isNotEmpty(mrgTypeName)){
                                //关联客户号
                                indivCusRelListRespDto.setCusIdRel((String) comMgr.get("cusId"));
                                //关联客户名称
                                indivCusRelListRespDto.setCusNameRel((String) comMgr.get("mrgName"));
                                //客户大类
                                indivCusRelListRespDto.setCusTypeRel((String) comMgr.get("cusCatalog"));
                                //从dictMap里获取对应的码值
                                indivCusRelListRespDto.setRelType(mrgTypeName);
                                logger.info("查询【{}】的【{}】信息【{}】",cusId, mrgTypeName, JSON.toJSONString(indivCusRelListRespDto));
                                cusRelList.add(indivCusRelListRespDto);
                            }
                        }
                    }
                }
            }


            //企业客户编号不为空
            //1.查询企业的股东
            //查询客户资本构成信息表里关联法人的出资人信息
            List<Map<String, String>> corpApitalList = cusCorpApitalService.queryCusInfoByCusIdRel(cusId);

            for (Map<String, String> corpApital : corpApitalList) {
                CmisCus0004IndivCusRelListRespDto indivCusRelListRespDto = new CmisCus0004IndivCusRelListRespDto();
                indivCusRelListRespDto.setCusIdRel((String) corpApital.get("cusId"));
                indivCusRelListRespDto.setCusNameRel((String) corpApital.get("invtName"));
                indivCusRelListRespDto.setCusTypeRel((String) corpApital.get("cusCatalog"));
                indivCusRelListRespDto.setRelType("股东");
                logger.info("查询【{}】股东信息【{}】",cusId , JSON.toJSONString(indivCusRelListRespDto));
                cusRelList.add(indivCusRelListRespDto);
            }

            //2.查询企业高管
            List<Map<String, String>> comMgrList = cusCorpMgrService.selectCusInfoByCusIdRel(cusId);
            if (!comMgrList.isEmpty()){
                //初始化高管类别字典项
                initDictMap(dictMap);
                for (Map<String, String> comMgr : comMgrList) {
                    String cusIdG = (String) comMgr.get("cusId") ;
                    //高管类别
                    String mrgType = (String) comMgr.get("mrgType");
                    String mrgTypeName = dictMap.get(mrgType) ;
                    if(StringUtils.isNotEmpty(mrgTypeName)){
                        CmisCus0004IndivCusRelListRespDto indivCusRelListRespDto = new CmisCus0004IndivCusRelListRespDto();
                        //关联客户号
                        indivCusRelListRespDto.setCusIdRel(cusIdG);
                        //关联客户名称
                        indivCusRelListRespDto.setCusNameRel((String) comMgr.get("mrgName"));
                        //客户大类
                        indivCusRelListRespDto.setCusTypeRel((String) comMgr.get("cusCatalog"));
                        //从dictMap里获取对应的码值
                        indivCusRelListRespDto.setRelType(mrgTypeName);
                        logger.info("查询【{}】企业高管【{}】",cusId , JSON.toJSONString(indivCusRelListRespDto));
                        cusRelList.add(indivCusRelListRespDto);
                    }
                }
            }

            //查询作为某个企业的高管或实际控制人
            List<Map<String, String>> cusRelInfoList = cusCorpMgrService.selectCusRelInfoByCusId(cusId);


            if (cusRelInfoList!=null && cusRelInfoList.size()>0){

                if (!cusRelInfoList.isEmpty() && dictMap.isEmpty()){
                    initDictMap(dictMap);
                }

                for (Map<String, String> cusRelInfoMap : cusRelInfoList) {
                    //高管类别
                    String mrgType = (String) cusRelInfoMap.get("mrgType");
                    String mrgTypeName = dictMap.get(mrgType) ;
                    if(StringUtils.isNotEmpty(mrgTypeName)){
                        CmisCus0004IndivCusRelListRespDto indivCusRelListRespDto = new CmisCus0004IndivCusRelListRespDto();
                        //关联客户号
                        indivCusRelListRespDto.setCusIdRel((String) cusRelInfoMap.get("cusIdRel"));
                        //关联客户名称
                        indivCusRelListRespDto.setCusNameRel((String) cusRelInfoMap.get("cusName"));
                        //客户大类
                        indivCusRelListRespDto.setCusTypeRel(CmisCusConstants.STD_ZB_CUS_CATALOG_2);//默认对公
                        //从dictMap里获取对应的码值
                        indivCusRelListRespDto.setRelType(mrgTypeName);
                        logger.info("查询【{}】企业的高管或实际控制人【{}】",cusId , JSON.toJSONString(indivCusRelListRespDto));
                        cusRelList.add(indivCusRelListRespDto);
                    }
                }
            }

            //key:cusId，value：CmisCus0004IndivCusRelListRespDto对象
            Map<String,CmisCus0004IndivCusRelListRespDto> respDtoMap = new HashMap<>();

            for (CmisCus0004IndivCusRelListRespDto IndivCusRelListRespDto : cusRelList) {
                String cus_id = IndivCusRelListRespDto.getCusIdRel();
                //如果客户号和本客户一直，跳过
                if(cusId.equals(cus_id)){
                    continue;
                }
                logger.info("查询【{}】遍历处理重复客户信息【{}】",cusId , JSON.toJSONString(IndivCusRelListRespDto));
                if (respDtoMap.containsKey(cus_id)){
                    CmisCus0004IndivCusRelListRespDto indivCusRelDto = respDtoMap.get(cus_id);
                    String relType = indivCusRelDto.getRelType();
                    String relType2 = indivCusRelDto.getRelType();

                    //如果一个客户有多个客户类型，则合并为一条记录，关联类型用“/”连接
                    if(StringUtils.isNotEmpty(relType)){
                        if (!relType.contains(relType2)){
                            relType = relType+"/"+relType2;
                            indivCusRelDto.setRelType(relType);
                            respDtoMap.put(cus_id,indivCusRelDto);
                        }
                    }
                }else{
                    respDtoMap.put(cus_id,IndivCusRelListRespDto);
                }
            }

            //清空cusRelList，将respDtoMap中的value放入cusRelList里
            cusRelList.clear();

            Set<Map.Entry<String, CmisCus0004IndivCusRelListRespDto>> entries = respDtoMap.entrySet();
            logger.info("查询【{}】组装报文信息【{}】",cusId , JSON.toJSONString(entries));
            for (Map.Entry<String, CmisCus0004IndivCusRelListRespDto> entry : entries) {
                cusRelList.add(entry.getValue());
            }

            respDto.setIndivCusRelList(cusRelList);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsEnum.TRADE_CODE_CMISCUS0004.key+"报错：",e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0004.key, DscmsEnum.TRADE_CODE_CMISCUS0004.value);
        logger.info("【{}】响应报文【{}】", DscmsEnum.TRADE_CODE_CMISCUS0004.key, JSON.toJSONString(respDto));
        return respDto;
    }

    /**
     * 初始化dictMap
     * @param dictMap
     * @return
     */
    private void initDictMap(Map<String,String> dictMap){
        //将高管类别字典项放入dictMap里
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200300,"高管人员");
       /* dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200400,"法人代表");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200500,"公司董事长");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200600,"公司总经理/厂长/CEO");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200700,"财务主管/CFO");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200800,"授权经办人");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200900,"部门经理");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201000,"董事");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201100,"监事");*/
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201200,"实际控制人");
       /* dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201300,"受益人");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201400,"公司副总经理级的管理人员");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201500,"合伙制企业的合伙人");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201600,"中层管理人员");*/
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201700,"股东");
/*        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201800,"顾问");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201900,"联系人");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_202000,"执行董事");
        dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_209900,"其他");*/
    }
}