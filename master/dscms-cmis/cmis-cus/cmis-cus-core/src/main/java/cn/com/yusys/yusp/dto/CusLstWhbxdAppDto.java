package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWhbxdApp
 * @类描述: cus_lst_whbxd_app数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:33:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusLstWhbxdAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务编号 **/
	private String lwaSerno;
	
	/** 申请日期 **/
	private String appDate;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 客户规模 **/
	private String cusScale;
	
	/** 是否优良信贷客户 **/
	private String isFineCretCus;
	
	/** 授信金额 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 内部评级 **/
	private String innerEval;
	
	/** 管户客户经理 **/
	private String managerId;
	
	/** 所属机构 **/
	private String belgOrg;
	
	/** 名单归属 **/
	private String listBelg;
	
	/** 申请说明 **/
	private String appExpl;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新时间 **/
	private String updDate;
	
	
	/**
	 * @param lwaSerno
	 */
	public void setLwaSerno(String lwaSerno) {
		this.lwaSerno = lwaSerno == null ? null : lwaSerno.trim();
	}
	
    /**
     * @return LwaSerno
     */	
	public String getLwaSerno() {
		return this.lwaSerno;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}
	
    /**
     * @return AppDate
     */	
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusScale
	 */
	public void setCusScale(String cusScale) {
		this.cusScale = cusScale == null ? null : cusScale.trim();
	}
	
    /**
     * @return CusScale
     */	
	public String getCusScale() {
		return this.cusScale;
	}
	
	/**
	 * @param isFineCretCus
	 */
	public void setIsFineCretCus(String isFineCretCus) {
		this.isFineCretCus = isFineCretCus == null ? null : isFineCretCus.trim();
	}
	
    /**
     * @return IsFineCretCus
     */	
	public String getIsFineCretCus() {
		return this.isFineCretCus;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param innerEval
	 */
	public void setInnerEval(String innerEval) {
		this.innerEval = innerEval == null ? null : innerEval.trim();
	}
	
    /**
     * @return InnerEval
     */	
	public String getInnerEval() {
		return this.innerEval;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg == null ? null : belgOrg.trim();
	}
	
    /**
     * @return BelgOrg
     */	
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param listBelg
	 */
	public void setListBelg(String listBelg) {
		this.listBelg = listBelg == null ? null : listBelg.trim();
	}
	
    /**
     * @return ListBelg
     */	
	public String getListBelg() {
		return this.listBelg;
	}
	
	/**
	 * @param appExpl
	 */
	public void setAppExpl(String appExpl) {
		this.appExpl = appExpl == null ? null : appExpl.trim();
	}
	
    /**
     * @return AppExpl
     */	
	public String getAppExpl() {
		return this.appExpl;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}