package cn.com.yusys.yusp.fncstat.domain;

import cn.com.yusys.yusp.reportconf.domain.FncConfDefFmt;

import java.util.List;

public class FncType {
    private String fncConfTyp;
    private String styleId;
    private String cusId;
    private String statPrdStyle;
    private String statPrd;
    private String statStyle;
    private String fncName; //对应数据库里面的哪一张表 例如FNC_Stat_BS
    private String fncConfDisName; //例如"资产负债表"
    //每种财报总的科目项
    private List<FncConfDefFmt> fncItems;

    public List<FncConfDefFmt> getFncItems() {
        return fncItems;
    }
    public void setFncItems(List<FncConfDefFmt> fncItems) {
        this.fncItems = fncItems;
    }
    public FncType(String fncConfTyp, String styleId) {
        this.fncConfTyp = fncConfTyp;
        this.styleId = styleId;
    }
    public String getFncConfTyp() {
        return fncConfTyp;
    }
    public void setFncConfTyp(String fncConfTyp) {
        this.fncConfTyp = fncConfTyp;
    }
    public String getStyleId() {
        return styleId;
    }
    public void setStyleId(String styleId) {
        this.styleId = styleId;
    }
    public String getFncName() {
        return fncName;
    }
    public void setFncName(String fncName) {
        this.fncName = fncName;
    }
    public String getFncConfDisName() {
        return fncConfDisName;
    }
    public void setFncConfDisName(String fncConfDisName) {
        this.fncConfDisName = fncConfDisName;
    }
    public String getCusId() {
        return cusId;
    }
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }
    public String getStatPrdStyle() {
        return statPrdStyle;
    }
    public void setStatPrdStyle(String statPrdStyle) {
        this.statPrdStyle = statPrdStyle;
    }
    public String getStatPrd() {
        return statPrd;
    }
    public void setStatPrd(String statPrd) {
        this.statPrd = statPrd;
    }
    public String getStatStyle() {
        return statStyle;
    }
    public void setStatStyle(String statStyle) {
        this.statStyle = statStyle;
    }

}
