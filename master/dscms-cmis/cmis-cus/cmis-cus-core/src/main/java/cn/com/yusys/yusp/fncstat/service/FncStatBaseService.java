/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.service;


import java.math.BigDecimal;
import java.util.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.fncstat.dto.FinanIndicAnalyDto;
import cn.com.yusys.yusp.service.*;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.ErrorConstants;
import cn.com.yusys.yusp.constants.FncStatConstants;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.domain.FncImageCfg;
import cn.com.yusys.yusp.domain.FncItemMap;
import cn.com.yusys.yusp.domain.OcrTask;
import cn.com.yusys.yusp.dto.CusRelationsDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.Cbocr1ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.resp.Cbocr1RespDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.req.Cbocr2ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.resp.Cbocr2RespDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.resp.Data;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.req.YxljcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.resp.YxljcxRespDto;
import cn.com.yusys.yusp.dto.client.http.ocr.req.Cbocr3ReqDto;
import cn.com.yusys.yusp.dto.client.http.ocr.resp.Cbocr3RespDto;
import cn.com.yusys.yusp.dto.client.http.ocr.resp.LeftSubject;
import cn.com.yusys.yusp.dto.client.http.ocr.resp.RightSubject;
import cn.com.yusys.yusp.dto.client.http.ocr.resp.StatisItem;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.fncstat.domain.FncStatBase;
import cn.com.yusys.yusp.fncstat.domain.FncStatBs;
import cn.com.yusys.yusp.fncstat.domain.OcrItem;
import cn.com.yusys.yusp.fncstat.domain.RptItemData;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptBriefDTO;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptDTO;
import cn.com.yusys.yusp.fncstat.dto.FncStatRptItemDTO;
import cn.com.yusys.yusp.fncstat.repository.mapper.FncStatBaseMapper;
import cn.com.yusys.yusp.fncstat.repository.mapper.FncStatBsMapper;
import cn.com.yusys.yusp.reportconf.domain.FncConfDefFmt;
import cn.com.yusys.yusp.reportconf.domain.FncConfStyles;
import cn.com.yusys.yusp.reportconf.domain.FncConfTemplate;
import cn.com.yusys.yusp.reportconf.service.FncConfDefFmtService;
import cn.com.yusys.yusp.reportconf.service.FncConfStylesService;
import cn.com.yusys.yusp.reportconf.service.FncConfTemplateService;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import cn.com.yusys.yusp.repository.mapper.FncImageCfgMapper;
import cn.com.yusys.yusp.repository.mapper.FncItemMapMapper;
import cn.com.yusys.yusp.repository.mapper.OcrTaskMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.util.FncNumber;
import cn.com.yusys.yusp.util.PUBUtilTools;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;


/**
 * @version 1.0.0
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatBaseService
 * @类描述: #服务类
 * @功能描述: 财务报表信息
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-23 11:39:41
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class FncStatBaseService {
    private static final Logger logger = LoggerFactory.getLogger(FncStatBaseService.class);
    @Resource
    private FncStatBaseMapper fncStatBaseMapper;
    @Autowired
    private FncConfStylesService fncConfStylesService;
    @Autowired
    private CusCorpService cusCorpService;
    @Autowired
    private FncConfTemplateService fncConfTemplateService;
    @Autowired
    private FncConfDefFmtService fncConfDefFmtService;
    //    @Autowired
//    private MessageProviderService messageProviderService;
    @Resource
    private FncStatBsMapper fncStatBsMapper;

    @Autowired
    private Dscms2YxxtClientService dscms2YxxtClientService;

    @Autowired
    private Dscms2OcrClientService dscms2OcrClientService;

    @Autowired
    private CusBaseService cusBaseService;

    @Resource
    private CusIndivMapper cusIndivMapper;

    @Resource
    private CusCorpMapper cusCorpMapper;

    @Resource
    private FncImageCfgMapper fncImageCfgMapper;

    @Resource
    private OcrTaskMapper ocrTaskMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    @Resource
    private FncItemMapMapper fncItemMapMapper;

    @Autowired
    private FncStatDtlService fncStatDtlService;

    @Autowired
    private AdminSmPropService adminSmPropService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public FncStatBase selectByPrimaryKey(String cusId, String statStyle, String statPrdStyle, String statPrd) {
        return fncStatBaseMapper.selectByPrimaryKeyWithAddInfo(cusId, statStyle, statPrdStyle, statPrd);
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<FncStatBase> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FncStatBase> list = fncStatBaseMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    public List<FncStatBase> selectByModelGrp(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FncStatBase> list = fncStatBaseMapper.selectByModelGrp(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(FncStatBase record) {
        return fncStatBaseMapper.insertSelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(FncStatBase record) {
        return fncStatBaseMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String cusId, String statStyle, String statPrdStyle, String statPrd) {
        return fncStatBaseMapper.deleteByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
    }

    public Map selectFncByCusIdRptYear(String cusId,String statPrd){
        return fncStatBaseMapper.selectFncByCusIdRptYear(cusId,statPrd);
    }

    public List<FncStatBase> queryFncStatBaseList(QueryModel model) {
        List<FncStatBase> list = selectByModel(model);
        for (FncStatBase fncStatBase : list) {
            fncStatBase
                    .setStatStyleDesc(FncStatConstants.FncStatStyleEnum.byCode(fncStatBase.getStatStyle()).getDesc());
            fncStatBase.setStatPrdStyleDesc(
                    FncStatConstants.FncStatPrdStyleEnum.byCode(fncStatBase.getStatPrdStyle()).getDesc());
            String stateflg = fncStatBase.getStateFlg();
            if (StringUtils.isNotEmpty(stateflg)) {
                if ("2".equals(StringUtils.substring(stateflg, -1)))
                    fncStatBase.setStateflgDesc("完成");
                else
                    fncStatBase.setStateflgDesc("暂存");
            }
        }
        if (null == list) {
//            Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20201335);
            logger.error(ErrorConstants.NRCS_CMS_T20201335);
            throw new YuspException(ErrorConstants.NRCS_CMS_T20201335, "财务报表配置查询错误");
        }
        return list;
    }

    public List<FncStatBase> queryFncStatBaseGrpList(QueryModel model) {
        List<FncStatBase> list = selectByModelGrp(model);
        for (FncStatBase fncStatBase : list) {
            fncStatBase
                    .setStatStyleDesc(FncStatConstants.FncStatStyleEnum.byCode(fncStatBase.getStatStyle()).getDesc());
            fncStatBase.setStatPrdStyleDesc(
                    FncStatConstants.FncStatPrdStyleEnum.byCode(fncStatBase.getStatPrdStyle()).getDesc());
            String stateflg = fncStatBase.getStateFlg();
            if (StringUtils.isNotEmpty(stateflg)) {
                if ("2".equals(StringUtils.substring(stateflg, -1)))
                    fncStatBase.setStateflgDesc("完成");
                else
                    fncStatBase.setStateflgDesc("暂存");
            }
        }
        if (null == list) {
//            Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20201335);
            logger.error(ErrorConstants.NRCS_CMS_T20201335);
            throw new YuspException(ErrorConstants.NRCS_CMS_T20201335, "财务报表配置查询错误");
        }
        return list;
    }

    /**
     * 最新完成财务报表信息查询
     *
     * @param cusId
     * @return
     */
    public FncStatBase queryFncStatBaseLastReport(String cusId) {
        return fncStatBaseMapper.queryFncStatBaseLastReport(cusId);
    }

    @Transactional
    public FncStatBase addFncStatBase(FncStatBase fncStatBase) {
        String cusId = fncStatBase.getCusId();
        String statStyle = fncStatBase.getStatStyle();
        String statPrdStyle = fncStatBase.getStatPrdStyle();
        String statPrd = fncStatBase.getStatPrd();
        if (null != selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd)) {
            throw BizException.error(null, ErrorConstants.NRCS_CMS_T20501118, "报表已存在，重新录入请先删除该期间报表");
        }
        if (FncStatConstants.FncStatPrdStyleEnum.YEAR.getCode().equals(statPrdStyle) && statPrd.length() == 4)
            statPrd += "12";


        String currDateStr = DateUtils.getCurrDateStr();
        fncStatBase.setInputId(getSessionUser().getLoginCode());// 登记人
        fncStatBase.setInputBrId(getSessionUser().getInstuOrg().getCode());
        fncStatBase.setInputDate(currDateStr);
        fncStatBase.setLastUpdDate(currDateStr);// 修改日期

        String cusFinType = fncStatBase.getFncConfTyp();
        if (StringUtils.isEmpty(cusFinType)) {
            CusCorp cusCorp = cusCorpService.selectByPrimaryKey(cusId);
            cusFinType = cusCorp.getFinaReportType();
        }
        if (StringUtils.isEmpty(cusFinType)) {// 小微企业使用简表模板
            throw new YuspException(ErrorConstants.NRCS_CMS_T20501109, "该客户未配置财报类型！"); // update by liucheng
//            cusFinType = FncStatConstants.BRIEF_FNC_ID;
//            fncStatBase.setBriefFlag("1");
        }
        FncConfTemplate fncTemp = fncConfTemplateService.selectByPrimaryKey(cusFinType);
        if (null == fncTemp) {
            throw new YuspException(ErrorConstants.NRCS_CMS_T20501109, "该客户配置财报类型不存在！"); // update by liucheng
//            return fncStatBase;
        }

        List<FncConfStyles> stylesList = Lists.newArrayList();
        // 获得状态以及模板包含的样式列表
        String stateFlg = extractConfStyles(fncStatBase, fncTemp, stylesList);
        fncStatBase.setStateFlg(stateFlg);
        int result = insertSelective(fncStatBase);// 新增一条记录信息(基表信息)
        // 写入子表
        if (result > 0) {
            String statYear = statPrd.substring(0, 4);
            String statMonth = StringUtils.substring(statPrd, -2);
            for (FncConfStyles fncConfStyles : stylesList) {
                List<FncConfDefFmt> fcdfList = fncConfDefFmtService
                        .getFncConfDefFormatFromDB(fncConfStyles.getStyleId());
                /**
                 * 如果是资产负债表；
                 * 查询上年年报数据是否存在(过滤条件:使用的相同的报表模板)；
                 * 如果存在，自动把上年年报的“期末数”写入新建报表的“期初数/年初数"字段;
                 */
                boolean readLastYearFlag = false;
                Map<String, FncStatBs> lastFncStatBs = Maps.newConcurrentMap();
                if (FncStatConstants.FncConfTypEnum.BS.getCode().equals(fncConfStyles.getFncConfTyp())) {
                    String thisYear = StringUtils.substring(statPrd, 0, 4);
                    String lastYear = (Integer.parseInt(thisYear) - 1) + "";
                    FncStatBase lastFncStatBase = fncStatBaseMapper.selectByPrimaryKey(cusId, statStyle, FncStatConstants.FncStatPrdStyleEnum.YEAR.getCode(), lastYear + "12");
                    if (null != lastFncStatBase) {
                        if (lastFncStatBase.getStatBsStyleId().equals(fncTemp.getFncBsStyleId())) {// 资产负债表StyleId相同
                            readLastYearFlag = true;
                            lastFncStatBs = fncStatBsMapper.selectLastYearRpt(cusId, statStyle, lastYear);
                        }
                    }
                }
                int size = fcdfList.size();
                for (int i = 0; i < size; i++) {
                    FncConfDefFmt fncFormat = fcdfList.get(i);
                    String itemId = fncFormat.getItemId();
                    RptItemData riData = new RptItemData(cusId, fncConfStyles.getFncName(), statPrdStyle, statYear,
                            statMonth, itemId, statStyle, fncConfStyles.getFncConfDataCol());
                    if (readLastYearFlag) {//去年年报期末数自动写入
                        if (lastFncStatBs.containsKey(itemId)) {
                            FncStatBs bs = lastFncStatBs.get(itemId);
                            riData.setData1(bs.getStatEndAmtY() == null ? BigDecimal.ZERO : bs.getStatEndAmtY());
                        }
                    }
                    // 先查询子表中是否有该数据存在，有执行修改操作，否则，执行新增操作
                    Integer selectRet;
                    try {
                        selectRet = fncStatBaseMapper.exeSelectSql(riData.assembleSelectSql());
                        int dynamicInsert = 0;
                        if (selectRet == null) {
                            dynamicInsert = fncStatBaseMapper.exeInsertOrUpdateSql(riData.AssembleInsertSql());
                        } else {
                            dynamicInsert = fncStatBaseMapper.exeInsertOrUpdateSql(riData.AssembleUpdateSql());
                        }
                        logger.info("财务报表信息-新增-新增样式表" + fncConfStyles.getFncName() + "结果：" + dynamicInsert);
                    } catch (Exception e) {
//                        Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20501118);
                        logger.error(ErrorConstants.NRCS_CMS_T20501118, e);
                        throw new YuspException(ErrorConstants.NRCS_CMS_T20501118, "财务报表基本信息维护错误");
                    }
                }
            }
        } else {
//            Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20501118);
            logger.error(ErrorConstants.NRCS_CMS_T20501118);
            throw new YuspException(ErrorConstants.NRCS_CMS_T20501118, "财务报表基本信息维护错误");
        }
        return fncStatBase;
    }

    private String extractConfStyles(FncStatBase fncStatBase, FncConfTemplate fncTemp, List<FncConfStyles> stylesList) {
        String fncBsStyleId = fncTemp.getFncBsStyleId();// 资产负债表样式编号
        String fncPlStyleId = fncTemp.getFncPlStyleId();// 损益表编号
        String fncCfStyleId = fncTemp.getFncCfStyleId();// 现金流量表编号
        String fncFiStyleId = fncTemp.getFncFiStyleId();// 财务指标表编号
//        String fncBssStyleId = fncTemp.getFncBssStyleId();// 资产负债类财务信息样式标号
//        String fncPssStyleId = fncTemp.getFncPssStyleId();// 利润表类财务信息样式标号
//        String fncFasStyleId = fncTemp.getFncFasStyleId();// 财务分析指标样式标号

        fncStatBase.setStatBsStyleId(fncBsStyleId);
        fncStatBase.setStatPlStyleId(fncPlStyleId);
        fncStatBase.setStatCfStyleId(fncCfStyleId);
        fncStatBase.setStatFiStyleId(fncFiStyleId);
//        fncStatBase.setStatBssStyleId(fncBssStyleId);
//        fncStatBase.setStatPssStyleId(fncPssStyleId);
//        fncStatBase.setStatFasStyleId(fncFasStyleId);
        /**
         * 调整stateFlg顺序，顺序必须同显示页面，修改状态更改时一致，顺序为 :0代表新增加 1 代表暂存 2 代表完成 9 不用的或者扩展位
         * 第一位表示资产负债表状态 第二位表示损益表状态 第三位表示现金流量状态 第四位表示财务指标状态 第五位表示简表-资产负债表状态 第六位表示简表-利润表状态
         * 第七位表示简表-财务指标表状态 第八位表示其它表状态
         */
        String[] arrStateFlg = new String[]{"9", "9", "9", "9", "9", "9", "9", "9", "1"};
        popState(0, fncBsStyleId, arrStateFlg, stylesList);
        popState(1, fncPlStyleId, arrStateFlg, stylesList);
        popState(2, fncCfStyleId, arrStateFlg, stylesList);
        popState(3, fncFiStyleId, arrStateFlg, stylesList);
//        popState(4, fncBssStyleId, arrStateFlg, stylesList);
//        popState(5, fncPssStyleId, arrStateFlg, stylesList);
//        popState(6, fncFasStyleId, arrStateFlg, stylesList);

        return StringUtils.join(arrStateFlg);
    }

    private void popState(int index, String stylesId, String[] arrStateFlg, List<FncConfStyles> stylesList) {
        if (StringUtils.isNotEmpty(stylesId)) {
            arrStateFlg[index] = "0";
            stylesList.add(fncConfStylesService.queryFncConfStylesByKey(stylesId));
        }
    }

    @Transactional
    public int deleteFncStatBase(String cusId, String statStyle, String statPrdStyle, String statPrd) {
        int result = 0;
        FncStatBase fsb = fncStatBaseMapper.selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        if (null == fsb)
            return -1;// 未查询到
        // 取得操作的表的ID
        List<String> styleIds = Lists.newArrayList();
        String fncBsStyleId = fsb.getStatBsStyleId();// 资产负债表样式编号
        String fncPlStyleId = fsb.getStatPlStyleId();// 损益表编号
        String fncCfStyleId = fsb.getStatCfStyleId();// 现金流量表编号
        String fncFiStyleId = fsb.getStatFiStyleId();// 财务指标表编号
        String fncBssStyleId = fsb.getStatBssStyleId();// 资产负债类财务信息样式标号
        String fncPssStyleId = fsb.getStatPssStyleId();// 利润表类财务信息样式标号
        String fncFasStyleId = fsb.getStatFasStyleId();// 财务分析指标样式标号
        if (StringUtils.isNotEmpty(fncBsStyleId))
            styleIds.add(fncBsStyleId);
        if (StringUtils.isNotEmpty(fncPlStyleId))
            styleIds.add(fncPlStyleId);
        if (StringUtils.isNotEmpty(fncCfStyleId))
            styleIds.add(fncCfStyleId);
        if (StringUtils.isNotEmpty(fncFiStyleId))
            styleIds.add(fncFiStyleId);
        if (StringUtils.isNotEmpty(fncBssStyleId))
            styleIds.add(fncBssStyleId);
        if (StringUtils.isNotEmpty(fncPssStyleId))
            styleIds.add(fncPssStyleId);
        if (StringUtils.isNotEmpty(fncFasStyleId))
            styleIds.add(fncFasStyleId);

        List<FncConfStyles> list = Lists.newArrayList();
        for (String styleId : styleIds) {
            FncConfStyles fcs = fncConfStylesService.queryFncConfStylesByKey(styleId);
            if (null != fcs)
                list.add(fcs);
        }
        // 删除基本信息表
        result = fncStatBaseMapper.deleteByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        if (result > 0) {
            // 更新样式表
            String statYear = statPrd.substring(0, 4);
            String statMonth = statPrd.substring(4, 6);
            for (FncConfStyles fncConfStyles : list) {
                RptItemData riData = new RptItemData(cusId, fncConfStyles.getFncName(), statPrdStyle, statYear,
                        statMonth, null, statStyle, fncConfStyles.getFncConfDataCol());
                int dynamicUpdate = fncConfStylesService.dynamicUpdate(riData.getUpdateBean());
                logger.info("财务报表信息-删除-更新样式表" + fncConfStyles.getFncName() + "结果：" + dynamicUpdate);
            }
        }
        return result;
    }

    /**
     * @deprecated 不要使用这个方法了，使用<code>com.zzbank.nrcs.fncstat.service.FncStatDtlService#queryAllFncStatDtl()</code>获取详情
     */
    @Deprecated
    @Transactional(readOnly = true)
    public FncStatBase queryFncStatBaseByKey(String cusId, String statStyle, String statPrdStyle, String statPrd,
                                             String fncConfTyp) {
        FncStatBase fncStatBase = selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        String styleId = null;
        if ("11".equals(fncConfTyp)) {// 资产负债表(新)FNC_STAT_SOFP
            styleId = fncStatBase.getStatSofpStyleId();
        } else if ("12".equals(fncConfTyp)) {// 利润表 FNC_STAT_PS
            styleId = fncStatBase.getStatPsStyleId();
        }
        FncConfStyles fcs = fncConfStylesService.queryFncConfStylesByKey(styleId);
        List<FncConfDefFmt> list = getFncConfDefFmtList(fncStatBase, fncConfTyp, styleId, fcs);
        int cote = fcs.getFncConfCotes();// 标签栏数(label+数据表示一列)
        fncStatBase.setDataCol(fcs.getFncConfDataCol());// 数据列数
        fncStatBase.setFncConfCotes(cote);// 栏位
        fncStatBase.setFncConfTyp(fcs.getFncConfTyp());// 报表类型
        fncStatBase.setStyleId1(styleId);// 当前报表样式编号
        int orderNum = 1; // 当前行次
        int size = list.size();
        List<FncConfDefFmt> fncConfDefFmtListA = new ArrayList<FncConfDefFmt>();
        List<FncConfDefFmt> fncConfDefFmtListB = new ArrayList<FncConfDefFmt>();
        if (1 == cote) {// 一栏
            for (int i = 0; i < size; i++) {
                FncConfDefFmt fmt = list.get(i);
                // 输入框列数(即数据列数，通常只有一列或两列)
                int num = 0;
                if ("1".equals(fmt.getFncConfRowFlg())) {// 是否显示行次
                    num = orderNum;
                    orderNum++;
                    fmt.setOrderNum(num);
                }
                if (1 == fmt.getFncConfCotes().intValue()) {
                    fncConfDefFmtListA.add(fmt);
                } else {
                    continue;
                }
            }
            fncStatBase.setFncConfDefFmtListA(fncConfDefFmtListA);
        } else if (2 == cote) {// 两栏
            for (int i = 0; i < size; i++) {
                FncConfDefFmt fmt = list.get(i);
                // 输入框列数(即数据列数，通常只有一列或两列)
                int confCote = fmt.getFncConfCotes().intValue();
                int num = 0;
                if ("1".equals(fmt.getFncConfRowFlg())) {// 是否显示行次
                    num = orderNum;
                    orderNum++;
                    fmt.setOrderNum(num);
                }
                if (1 == confCote) {
                    fncConfDefFmtListA.add(fmt);
                } else if (2 == confCote) {
                    fncConfDefFmtListB.add(fmt);
                } else {
                    continue;
                }
            }
            fncStatBase.setFncConfDefFmtListA(fncConfDefFmtListA);
            fncStatBase.setFncConfDefFmtListB(fncConfDefFmtListB);
        }
        return fncStatBase;
    }

    @Transactional
    public Integer updateUnLock(FncStatBase fncStatBase) {
        String indUpdate = fncStatBase.getIndUpdate();
        if (StringUtils.isEmpty(indUpdate)) {
            fncStatBase.setIndUpdate("1");// 是否可修改 1：是,2:否;
        } else {
            if (!"1".equals(indUpdate) && !"0".equals(indUpdate))
                returnError();
        }
        int result = fncStatBaseMapper.updateIndUpdateByPrimaryKey(fncStatBase);
        if (0 == result) {
            returnError();
        }
        return result;
    }

    private void returnError() {
//        Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20501116);
        logger.error(ErrorConstants.NRCS_CMS_T20501116);
        throw new YuspException(ErrorConstants.NRCS_CMS_T20501116, "财务报表信息解锁");
    }


    public void exportTemplate(HttpServletResponse response) {

    }

    public String importExcelFile(MultipartFile multipartFile) {

        return null;
    }

    public String exportExcelFile(Integer fetchSize, QueryModel model) {

        return null;
    }

    @Transactional
    public Integer updateAuditInfo(FncStatBase fncStatBase) {
        fncStatBase.setLastUpdId(getSessionUser().getLoginCode());
        fncStatBase.setLastUpdDate(DateUtils.getCurrDateStr());
        int result = fncStatBaseMapper.updateByPrimaryKeySelective(fncStatBase);
        if (0 == result) {
//            Message msg = messageProviderService.getMessage(ErrorConstants.NRCS_CMS_T20501117);
            logger.error(ErrorConstants.NRCS_CMS_T20501117);
            throw new YuspException(ErrorConstants.NRCS_CMS_T20501117, "财务报表审计信息维护错误");
        }
        return result;
    }

    private User getSessionUser() {
        User user = SessionUtils.getUserInformation();
        if (null == user) {
            logger.error("个人客户管理-财务报表信息-获取缓存User信息-getSessionUser方法-User对象为空");
            throw new YuspException("500", "个人客户管理-财务报表信息-获取缓存User信息-getSessionUser方法-User对象为空");
        }
        return user;
    }

    private List<FncConfDefFmt> getFncConfDefFmtList(FncStatBase fncStatBase, String fncConfTyp, String styleId,
                                                     FncConfStyles fcs) {
        String tableName = fcs.getFncName();
        // 拆分报表期间statPrd
        String year = fncStatBase.getStatPrd().substring(0, 4);
        String month = fncStatBase.getStatPrd().substring(4);
        // 根据报表周期类型判断该报表的类型.进行拼sql
        String postfix = null; // 字段属性后缀的标示
        if ("1".equals(fncStatBase.getStatPrdStyle())) { // 月报 _Amt6
            postfix = month;
            if (postfix.indexOf("0") == 0) {
                postfix = postfix.substring(1);
            }
        } else if ("2".equals(fncStatBase.getStatPrdStyle())) { // 季报 _Amt_Q2
            postfix = FncNumber.getJibao(month);
        } else if ("3".equals(fncStatBase.getStatPrdStyle())) { // 半年报 _Amt_Y1
            postfix = FncNumber.getBanNianBao(month);
        } else if ("4".equals(fncStatBase.getStatPrdStyle())) { // 年报 _Amt_Y
            postfix = FncNumber.getNianBao(month);
        }
        FncConfDefFmt fncConfDefFmt = new FncConfDefFmt();
        fncConfDefFmt.setCusId(fncStatBase.getCusId());
        fncConfDefFmt.setStyleId(styleId);
        fncConfDefFmt.setTableName(tableName);
        fncConfDefFmt.setStatStyle(fncStatBase.getStatStyle());
        fncConfDefFmt.setStatYear(year);
        StringBuilder sb = new StringBuilder(); // 用于存放拼成的sql
        sb.append(
                "f.item_id,f.fnc_conf_order,f.fnc_conf_cotes,f.fnc_conf_row_flg,f.fnc_conf_indent,f.fnc_conf_prefix,f.fnc_item_edit_typ,"
                        + "f.fnc_cnf_app_row,f.fnc_conf_disp_tpy,f.fnc_conf_disp_amt,f.fnc_conf_chk_frm,f.fnc_conf_cal_frm,n.item_name,");
        if ("11".equals(fncConfTyp)) {
            sb.append("b.stat_init_amt" + postfix + " as data1 ");
        } else if ("12".equals(fncConfTyp)) {
            sb.append("b.stat_init_amt" + postfix + " as data1 暂时修改");
            // sb.append("b.stat_init_amt" + postfix
            // + " as data1,b.stat_init_amt_y1 as data2,b.stat_init_amt_y2 as data3 ");
        }
        fncConfDefFmt.setPreSql(sb.toString());
        return fncConfDefFmtService.queryFncConfDefFmtList1(fncConfDefFmt);
    }

    /**
     * 请求OCR
     *
     * @param fncStatBase
     */
    @Transactional
    public void sendOrc(FncStatBase fncStatBase) {
        String cusId = fncStatBase.getCusId();//客户代码
        String statStyle = fncStatBase.getStatStyle();//报表口径
        String statPrdStyle = fncStatBase.getStatPrdStyle();//报表周期类型
        String statPrd = fncStatBase.getStatPrd();//报表期间
        String lastUpdId = fncStatBase.getLastUpdId();
        //查询财报基础表
        fncStatBase = fncStatBaseMapper.selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        if (fncStatBase == null) {
            String errMsg = "客户【" + cusId + "】报表期间【" + statPrd + "】的财报基础表不存在！";
            logger.error(errMsg);
            throw new BizException(null, ErrorConstants.NRCS_CMS_T20203327, null, errMsg);
        }
        //从财报对象中得到现金流动表的StyleId
        String styleId = fncStatBase.getStatBsStyleId();
        FncConfStyles fcs = fncConfStylesService.queryFncConfStylesByKey(styleId);
        String tableName = fcs.getFncName();
        String fncConfTyp = fcs.getFncConfTyp();
        //TODO

        //根据客户号查询客户信息
        CusRelationsDto cusBase = cusBaseService.findCusBaseByCusId(cusId);
        if (cusBase == null) {
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.value);
        }

        //客户类型
        String cusType = null;
        if (CmisCusConstants.CUS_CATALOG_PRI.equals(cusBase.getCusCatalog())) {
            CusIndiv cusIndiv = cusIndivMapper.selectByPrimaryKey(cusId);
            if (cusIndiv == null) {
                String errMsg = "根据客户号【" + cusId + "】查询不到个人客户信息！";
                logger.error(errMsg);
                throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
            }
            cusType = cusIndiv.getCusType();
        } else if (CmisCusConstants.CUS_CATALOG_PUB.equals(cusBase.getCusCatalog())) {
            CusCorp cusCorp = cusCorpMapper.selectByPrimaryKey(cusId);
            if (cusCorp == null) {
                String errMsg = "根据客户号【" + cusId + "】查询不到企业客户信息！";
                logger.error(errMsg);
                throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
            }
            cusType = cusCorp.getCusType();
        }

        //获取该期报表是否上传为自制报表或税务报表

        //报表类型字典编码
        String templateClassCode = this.checkPrdStyle(cusType);
        //报表类型名称
        String templateClassName = this.checkPrdStyleName(templateClassCode);
        ;
        //生成业务流水号
        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.PK_VALUE, new HashMap<>());

        //还需要根据报表周期     判断出选择报表当前月的自制报表资产负债表和自制报表利润表   以及后期拓展增加的年份
        //新增字段01-自制财报 02-税务财报
        String reportType = fncStatBase.getReportType();

        if (!"04".equals(templateClassCode) && StringUtils.isBlank(reportType)) {
            String errMsg = "请重新新增客户【" + cusId + "】期限【" + statPrd + "】报表！";
            logger.error(errMsg);
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
        }

        QueryModel queryModel = new QueryModel();
        queryModel.getCondition().put("applyYear", statPrd.substring(0, 4));
        queryModel.getCondition().put("applyMonth", statPrd.substring(4));
        queryModel.getCondition().put("cusCatalog", cusBase.getCusCatalog());
        if (!"04".equals(templateClassCode)) {
            queryModel.getCondition().put("reportType", reportType);
        }
        List<FncImageCfg> fncImageCfgList = fncImageCfgMapper.selectByModel(queryModel);
        if (CollectionUtils.isEmpty(fncImageCfgList)) {
            String errMsg = "期限【" + statPrd + "】报表影像配置不存在！";
            logger.error(errMsg);
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
        }
        if (fncImageCfgList.size() > 1) {
            String errMsg = "期限【" + statPrd + "】报表影像配置存在多条！";
            logger.error(errMsg);
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
        }

        String tpcode = fncImageCfgList.get(0).getRootNode();
        String sonCodeOne = fncImageCfgList.get(0).getChildBsNode();//子节点1
        String sonCodeTwo = fncImageCfgList.get(0).getChildIsNode();//子节点2
        if (StringUtils.isBlank(tpcode) || StringUtils.isBlank(sonCodeOne) || StringUtils.isBlank(sonCodeTwo)) {
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, "报表目录节点异常，请维护报表影像配置表！");
        }

        List<cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.List> uploadFileList = new ArrayList<>();
        //如果是个人的话，只识别一张简易报表
        if ("1".equals(cusBase.getCusCatalog())) {
            uploadFileList.add(queryImagePath(cusId, tpcode, sonCodeOne));//第二个子节点
        } else {
            //拼装报文发送影像返回上传文件名与路径url
            uploadFileList.add(queryImagePath(cusId, tpcode, sonCodeOne));//第一个子节点
            uploadFileList.add(queryImagePath(cusId, tpcode, sonCodeTwo));//第二个子节点
        }


        //发送ocrxd01识别返回信息
        ResultDto<Cbocr1RespDto> cbocr1ResultDto =
                this.cbocr1(cusId, serno, cusBase.getCusName(), statPrd, templateClassCode, templateClassName, uploadFileList, lastUpdId);

        if (!SuccessEnum.CMIS_SUCCSESS.key.equals(cbocr1ResultDto.getCode()) ||
                !SuccessEnum.OCR_SUCCESS_FLAG.key.equals(cbocr1ResultDto.getData().getSuccess()) ||
                !SuccessEnum.OCR_SUCCESS_CODE.key.equals(cbocr1ResultDto.getData().getErorcd())) {
            logger.error("发送OCR报错：{}", cbocr1ResultDto.getMessage());
            throw new BizException(null, cbocr1ResultDto.getData().getErorcd(), null, cbocr1ResultDto.getMessage());
        }
        //ocr任务Id
        String taskId = cbocr1ResultDto.getData().getTaskId();
        //插入OCR识别任务表
        QueryModel ocrQueyModel = new QueryModel();
        ocrQueyModel.getCondition().put("cusId", cusId);
        ocrQueyModel.getCondition().put("statPrd", statPrd);
        List<OcrTask> ocrTaskList = ocrTaskMapper.selectByModel(ocrQueyModel);
        if (CollectionUtils.isEmpty(ocrTaskList)) {
            OcrTask ocrTask = new OcrTask();
            ocrTask.setCusId(cusId);
            ocrTask.setSerno(serno);//TODO
            ocrTask.setStatPrd(statPrd);
            ocrTask.setTaskId(taskId);
            ocrTaskMapper.insertSelective(ocrTask);
        } else {
            OcrTask ocrTask = ocrTaskList.get(0);
            ocrTask.setTaskId(taskId);
            ocrTaskMapper.updateByPrimaryKeySelective(ocrTask);
        }
    }

    /**
     * 查询OCR结果信息
     *
     * @param fncStatBase
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOcrResult(FncStatBase fncStatBase) {
        String cusId = fncStatBase.getCusId();
        String statPrd = fncStatBase.getStatPrd();
        String fncConfTyp = fncStatBase.getFncConfTyp();
        String statPrdStyle = fncStatBase.getStatPrdStyle();
        String statStyle = fncStatBase.getStatStyle();
        fncStatBase = fncStatBaseMapper.selectByPrimaryKey(cusId, statStyle, statPrdStyle, statPrd);
        //获取OCR的taskId
        QueryModel ocrQueyModel = new QueryModel();
        ocrQueyModel.getCondition().put("cusId", cusId);
        ocrQueyModel.getCondition().put("statPrd", statPrd);
        List<OcrTask> ocrTaskList = ocrTaskMapper.selectByModel(ocrQueyModel);
        if (CollectionUtils.isEmpty(ocrTaskList)) {
            String errMsg = "客户【" + cusId + "】期限【" + statPrd + "】OCR任务记录不存在！";
            ;
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
        }
        if (ocrTaskList.size() > 1) {
            String errMsg = "客户【" + cusId + "】期限【" + statPrd + "】OCR任务记录存在多条！";
            ;
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
        }
        //任务Id
        String taskId = ocrTaskList.get(0).getTaskId();
        if (StringUtils.isBlank(taskId)) {
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null,
                    "客户【" + cusId + "】期限【" + statPrd + "】OCR任务id为空！");
        }
        ResultDto<Cbocr2RespDto> resultDto = this.cbocr2(taskId);
        if (!ResultDto.success().getCode().equals(resultDto.getCode()) ||
                !SuccessEnum.OCR_SUCCESS_FLAG.key.equals(resultDto.getData().getSuccess()) ||
                !SuccessEnum.OCR_SUCCESS_CODE.key.equals(resultDto.getData().getErorcd())) {
            String errMsg = "获取任务编号【" + taskId + "】财报结果失败";
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
        }
        List<Data> dataList = resultDto.getData().getList();
        if (CollectionUtils.isEmpty(dataList)) {
            String errMsg = "获取任务编号【" + taskId + "】财报结果为空";
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
        }
        List<Data> idList = new ArrayList<>();

        StringBuilder operMsg = new StringBuilder();
        Map<String, String> map = new HashMap<String, String>();
        for (Data data : dataList) {
            String subTaskId = data.getSubTaskId();
            String templateId = data.getTemplateId();
            String templateName = data.getTemplateName();
            String recogStatus = data.getRecogStatus();//0待识别，1识别中，2识别完成，3识别失败
            String checkStatus = data.getCheckStatus();//0待核对，1核对中，2核对完成
            String balanceStatus = data.getBalanceStatus();
            String totalFileNum = data.getTotalFileNum();
            String isMatched = data.getIsMatched();//
            if (dataList.size() == 2) {
                if ("02".equals(fncConfTyp) && templateName.contains("资产")) {
                    operMsg.append("未上传损益表影像！");
                    throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, operMsg.toString());
                }//如果客户经理只上传了该客户当期的报表损益表但进行了资产负债表中的OCR获取则拦截跳此提示
                else if ("01".equals(fncConfTyp) && templateName.contains("损益")) {
                    operMsg.append("未上传资产负债表影像！");
                    throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, operMsg.toString());
                }
            }
            if (dataList.size() == 1 && StringUtils.isBlank(templateId)) {
                operMsg.append("请检查OCR平台识别模板是否匹配上！");
                throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, operMsg.toString());
            }

            // 0 balanceStatus 试算平衡  2 recogStatus 识别完成  2 checkStatus核对完成
            if ("1".equals(isMatched)) {
                if (!"2".equals(recogStatus) && "01".equals(fncConfTyp) && templateName.contains("资产")) {
                    operMsg.append("OCR资产负债表财报未识别完成！");
                } else if (!"2".equals(recogStatus) && "02".equals(fncConfTyp) && templateName.contains("损益")) {
                    operMsg.append("OCR损益表财报未识别完成！");
                } else if (!"2".equals(recogStatus) && "01".equals(fncConfTyp) && templateName.contains("个人")) {
                    operMsg.append("OCR通用个人财报未识别完成！");
                }
                if (!"2".equals(checkStatus) && "01".equals(fncConfTyp) && templateName.contains("资产")) {
                    operMsg.append("OCR资产负债表财报未核对完成！");
                } else if (!"2".equals(checkStatus) && "02".equals(fncConfTyp) && templateName.contains("损益")) {
                    operMsg.append("OCR损益表财报未核对完成！");
                } else if (!"2".equals(checkStatus) && "01".equals(fncConfTyp) && templateName.contains("个人")) {
                    operMsg.append("OCR通用个人财报未核对完成！");
                }
            }
            //  1是匹配到  0是未匹配      与刘俊确认
            if ("1".equals(isMatched) && "2".equals(recogStatus) && "2".equals(checkStatus)) {
                if (templateName.contains("资产")) {
                    map.put(templateId, fncStatBase.getStatBsStyleId());
                } else if (templateName.contains("损益")) {
                    map.put(templateId, fncStatBase.getStatPlStyleId());
                } else if (templateName.contains("个人")) {
                    map.put(templateId, fncStatBase.getStatBsStyleId());
                }
                idList.add(data);
            } else {

            }
        }
        //保存报表数据
        //TODO
        insertFncData(idList, map, fncStatBase);
    }

    /**
     * 获取插入报表数据
     */
    private void insertFncData(List<Data> idList, Map<String, String> styleIdMap, FncStatBase fncStatBase) {
        if (CollectionUtils.isEmpty(idList)) {
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, "匹配到的财报数据为空");
        }

        FncStatRptBriefDTO brieDto = new FncStatRptBriefDTO();
        List<FncStatRptDTO> reportInfo = new ArrayList<>();
        idList.stream().forEach(id -> {
            String subTaskId = id.getSubTaskId();
            String templateId = id.getTemplateId();
            ResultDto<Cbocr3RespDto> resultDto = this.cbocr3(subTaskId, templateId);
            if (!ResultDto.success().getCode().equals(resultDto.getCode()) || !"00000000".equals(resultDto.getData().getCode())) {
                String errMsg = "获取子任务【" + subTaskId + "】模板Id【" + templateId + "】的财报数据异常";
                throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, errMsg);
            }
            cn.com.yusys.yusp.dto.client.http.ocr.resp.Data data = resultDto.getData().getData();
            List<StatisItem> statisItemList = data.getStatisItem();////科目对应的统计项列表
            String initStatisItemId = null;
            String endStatisItemId = null;
            for (StatisItem statisItem : statisItemList) {
                String statisItemName = statisItem.getStatisItemName();
                if (statisItemName.contains("初") || statisItemName.contains("本月")) {
                    initStatisItemId = statisItem.getStatisItemId();
                } else if (statisItemName.contains("末") || statisItemName.contains("本年")) {
                    endStatisItemId = statisItem.getStatisItemId();
                }
            }
            Map<String, OcrItem> map = new HashMap<>();
            List<LeftSubject> leftSubjectList = data.getLeftSubjectList();
            map.putAll(handleLeftSubjectList(leftSubjectList, initStatisItemId, endStatisItemId));

            List<RightSubject> rightSubjectList = data.getRightSubjectList();
            map.putAll(handleRightSubjectList(rightSubjectList, initStatisItemId, endStatisItemId));

            String styleId = styleIdMap.get(templateId);
            //TODO
            //报表样式
            FncConfStyles fncConfStyles = fncConfStylesService.queryFncConfStylesByKey(styleId);

            FncStatRptDTO rptDto = new FncStatRptDTO();
            rptDto.setCusId(fncStatBase.getCusId());//客户代码
            rptDto.setStatStyle(fncStatBase.getStatStyle());//报表口径
            rptDto.setStatPrdStyle(fncStatBase.getStatPrdStyle());//报表周期类型
            rptDto.setStatPrd(fncStatBase.getStatPrd());//报表期间
            //所属报表种类
            rptDto.setFncConfTyp(fncConfStyles.getFncConfTyp());
            //报表样式编号
            rptDto.setStatConfStyleId(styleId);

            List<FncStatRptItemDTO> items = new ArrayList<FncStatRptItemDTO>();

            List<FncConfDefFmt> fcdfList = fncConfDefFmtService.getFncConfDefFormatFromDB(styleId);
            for (FncConfDefFmt fncConfDefFmt : fcdfList) {
                if (FncStatConstants.FncItemEditTypEnum.TITLE.equals(fncConfDefFmt.getFncItemEditTyp())) {
                    continue;
                }
                if (!map.containsKey(fncConfDefFmt.getItemId())) {
                    continue;
                }
                FncStatRptItemDTO itemDto = new FncStatRptItemDTO();
                itemDto.setItemId(fncConfDefFmt.getItemId());
                OcrItem ocrItem = map.get(fncConfDefFmt.getItemId());
                try {
                    BeanUtils.copyProperties(itemDto, ocrItem);
                } catch (Exception e) {
                    logger.error("copy properties is error", e);
                    throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, "copy properties is error" + e.getMessage());
                }
                items.add(itemDto);
            }
            rptDto.setItems(items);
            reportInfo.add(rptDto);
        });
        brieDto.setReportInfo(reportInfo);
        ResultDto resultDto = fncStatDtlService.saveFncStatDtlBrief(brieDto, false);
        if (!SuccessEnum.CMIS_SUCCSESS.key.equals(resultDto.getCode())) {
            throw new BizException(null, resultDto.getCode(), null, resultDto.getMessage());
        }
    }

    private Map<String, OcrItem> handleLeftSubjectList(List<LeftSubject> leftSubjectList, String initStatisItemId, String endStatisItemId) {
        Map<String, OcrItem> map = new HashMap<String, OcrItem>();
        if (CollectionUtils.isEmpty(leftSubjectList)) {
            return map;
        }
        QueryModel queryModel = new QueryModel();
        leftSubjectList.stream().forEach(subject -> {
            subject.getSubjectName();
            subject.getSubjectId();
            String subjectCode = subject.getSubjectCode();//// 科目编码
            // 科目状态
            String tag = subject.getTag();
            // 科目所在文件的id
            String fileId = subject.getFileId();
            // 科目子科目状态，0无子科目，1有子科目
            Integer hasChildren = subject.getHasChildren();
            List<StatisItem> statisItems = subject.getStatisItem();
            if (CollectionUtils.isEmpty(statisItems)) {
                return;
            }
            queryModel.getCondition().put("ocrSubjectId", subjectCode);
            List<FncItemMap> fncItemMaps = fncItemMapMapper.selectByModel(queryModel);
            if (CollectionUtils.isEmpty(fncItemMaps)) {
                logger.error("OCR的科目号【{}】在信贷科目映射表没有匹配记录！", subjectCode);
                return;
                //throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, "OCR的科目号【" + subjectCode + "】在信贷科目映射表没有匹配记录！");
            }
            OcrItem ocrItem = this.getOcrItem(subjectCode, statisItems, initStatisItemId, endStatisItemId);
            map.put(fncItemMaps.get(0).getFncItemId(), ocrItem);
        });
        return map;
    }

    private Map<String, OcrItem> handleRightSubjectList(List<RightSubject> rightSubjectList, String initStatisItemId, String endStatisItemId) {
        Map<String, OcrItem> map = new HashMap<String, OcrItem>();
        if (CollectionUtils.isEmpty(rightSubjectList)) {
            return map;
        }
        QueryModel queryModel = new QueryModel();
        rightSubjectList.stream().forEach(subject -> {
            subject.getSubjectName();
            subject.getSubjectId();
            String subjectCode = subject.getSubjectCode();//// 科目编码
            // 科目状态
            String tag = subject.getTag();
            // 科目所在文件的id
            String fileId = subject.getFileId();
            // 科目子科目状态，0无子科目，1有子科目
            Integer hasChildren = subject.getHasChildren();
            List<StatisItem> statisItems = subject.getStatisItem();
            if (CollectionUtils.isEmpty(statisItems)) {
                return;
            }
            queryModel.getCondition().put("ocrSubjectId", subjectCode);
            List<FncItemMap> fncItemMaps = fncItemMapMapper.selectByModel(queryModel);
            if (CollectionUtils.isEmpty(fncItemMaps)) {
                //throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, "OCR的科目号【" + subjectCode + "】在信贷科目映射表中不存在！");
                logger.error("OCR的科目号【{}】在信贷科目映射表没有匹配记录！", subjectCode);
                return;
            }
            OcrItem ocrItem = this.getOcrItem(subjectCode, statisItems, initStatisItemId, endStatisItemId);
            map.put(fncItemMaps.get(0).getFncItemId(), ocrItem);
        });
        return map;
    }

    private OcrItem getOcrItem(String subjectCode, List<StatisItem> statisItems, String initStatisItemId, String endStatisItemId) {
        OcrItem ocrItem = new OcrItem();
        statisItems.stream().forEach(statisItem -> {
            String statisItemId = statisItem.getStatisItemId();
            String value = statisItem.getStatisItemValue();
            if (initStatisItemId.equals(statisItemId)) {
                try {
                    BeanUtils.setProperty(ocrItem, "data1", value);
                } catch (Exception e) {
                    logger.error("set property is error", e);
                    throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, null, "set property is error" + e.getMessage());
                }
            } else if (endStatisItemId.equals(statisItemId)) {
                try {
                    BeanUtils.setProperty(ocrItem, "data2", value);
                } catch (Exception e) {
                    logger.error("set property is error", e);
                    throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, null, "set property is error" + e.getMessage());
                }
            }
        });
        return ocrItem;
    }

    /**
     * 客户类型报表字典码转换
     *
     * @param cusType 客户类型
     * @return templateClassCode 报表类型字典码
     * @throws RuntimeException
     */
    private String checkPrdStyle(String cusType) throws RuntimeException {
        String comCusType = "211,212,221,222,231,232,241,242,250,260,270,280,299,315";
        String indivCusType = "150,110,120";
        String townCusType = "250";
        String templateClassCode = "";
        if (StringUtils.isBlank(cusType)) {
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, "该客户无客户类型");
        }
        if (comCusType.contains(cusType)) {
            templateClassCode = "00";
        } else if (townCusType.contains(cusType)) {
            templateClassCode = "01";
        } else if (indivCusType.contains(cusType)) {
            templateClassCode = "04";
        }
        return templateClassCode;
    }

    /**
     * 报表字典码与字典码名称转换
     *
     * @param templateClassCode 报表类型字典码
     * @return templateClassName 报表类型名称
     * @throws RuntimeException
     */
    private String checkPrdStyleName(String templateClassCode) throws RuntimeException {
        String templateClassName = "";
        if (StringUtils.isBlank(templateClassCode)) {
            throw new RuntimeException("检查报表类型！");
        }
        if ("00".equals(templateClassCode)) {
            templateClassName = "通用财务报表";
        } else if ("01".equals(templateClassCode)) {
            templateClassName = "村级财务报表";
        } else if ("04".equals(templateClassCode)) {
            templateClassName = "个人报表";
        }
        return templateClassName;
    }

    /**
     * 查询影响路径
     *
     * @param cusId
     * @param tpcode
     * @param sncode
     */
    private cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.List queryImagePath(String cusId, String tpcode, String sncode) {
        YxljcxReqDto reqDto = new YxljcxReqDto();
        reqDto.setPrcscd("yxljcx");
        reqDto.setServtp("NNT");
        reqDto.setServsq("NNT" + PUBUtilTools.getServsq().substring(3));
        //userid TODO
        reqDto.setRqtem2("1");
        //brchno
        reqDto.setDocidd(cusId);
        reqDto.setTpcode(tpcode);
        reqDto.setSncode(sncode);
        logger.info("请求：{}", JSON.toJSONString(reqDto));
        ResultDto<YxljcxRespDto> resultDto = dscms2YxxtClientService.yxljcx(reqDto);
        logger.info("响应：{}", JSON.toJSONString(resultDto));
        if (!ResultDto.success().getCode().equals(resultDto.getCode()) ||
                !"0000".equals(resultDto.getData().getErorcd())) {
            throw new BizException(null, resultDto.getData().getErorcd(), null, "获取影像资料异常,错误信息【" + resultDto.getMessage() + "】");
        }
        String pathss = resultDto.getData().getPathss();
        if (pathss.contains(";")) {
            throw new BizException(null, EcsEnum.CUS_GRP_SQL_SELECT_DATA_DEF.key, null, "请检查影像资料,上传了多个影像！");
        }
        String[] paths = pathss.split("#imageName#");
        if (paths == null || paths.length <= 0) {
            throw new BizException(null, null, null, "获取影像资料异常,请检查影像资料是否正确上传！");
        }
        cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.List fileList = new cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.List();
        logger.info("获取的影像文件路径：{}，文件名：{}", paths[0], paths[1]);
        fileList.setFilePath(paths[0]);
        fileList.setFileName(paths[1]);
        return fileList;
    }

    /**
     * 新增批次报表数据
     *
     * @param cusId
     * @param serno
     * @param cusName
     * @param statPrd
     * @param templateClassCode
     * @param templateClassName
     * @param uploadFileList
     * @param remark
     * @return
     */
    private ResultDto<Cbocr1RespDto> cbocr1(String cusId, String serno, String cusName, String statPrd, String templateClassCode,
                                            String templateClassName, List<cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.List> uploadFileList, String remark) {
        Cbocr1ReqDto reqDto = new Cbocr1ReqDto();
        reqDto.setClientNumber(cusId);
        reqDto.setFlowId(serno);
        reqDto.setOrganization(cusName);
        reqDto.setReportingPeriod(statPrd);
        reqDto.setTemplateClassCode(templateClassCode);
        reqDto.setTemplateClassName(templateClassName);
        reqDto.setList(uploadFileList);
        reqDto.setRemark(remark);
        logger.info("请求：{}", JSON.toJSONString(reqDto));
        ResultDto<Cbocr1RespDto> resultDto = dscms2OcrClientService.cbocr1(reqDto);
        logger.info("响应：{}", JSON.toJSONString(resultDto));
        return resultDto;
    }

    /**
     * 识别任务的模板匹配结果列表查询
     *
     * @param taskId 任务编号
     * @return
     */
    private ResultDto<Cbocr2RespDto> cbocr2(String taskId) {
        Cbocr2ReqDto reqDto = new Cbocr2ReqDto();
        reqDto.setTaskId(taskId);
        logger.info("请求：{}", JSON.toJSONString(reqDto));
        ResultDto<Cbocr2RespDto> resultDto = dscms2OcrClientService.cbocr2(reqDto);
        logger.info("响应：{}", JSON.toJSONString(resultDto));
        return resultDto;
    }

    /**
     * 报表核对统计总览列表查询
     *
     * @param subTaskId
     * @param templateId
     */
    private ResultDto<Cbocr3RespDto> cbocr3(String subTaskId, String templateId) {
        Cbocr3ReqDto reqDto = new Cbocr3ReqDto();
        reqDto.setSubTaskId(subTaskId);
        reqDto.setTemplateId(templateId);
        logger.info("请求：{}", JSON.toJSONString(reqDto));
        ResultDto<Cbocr3RespDto> resultDto = dscms2OuterdataClientService.cbocr3(reqDto);
        logger.info("响应：{}", JSON.toJSONString(resultDto));
        return resultDto;
    }

    /**
     * 获取客户编号对应的财务指标分析列表字段数据
     *
     * @param cusId 入参：客户号
     *              1、根据当前客户号去查询客户信息中的财务报表类型
     *              2、根据财报类型查询项下的资产负债表编号、损益表编号、财务指标表编号
     *              3、1）资产负债表数据：根据客户编号、当前年月（前一年年报。前两年年报。注：取前几年数据时，只需以当前年份报表类型中的字段为基准去找前几年的同名字段）、项目编号（注：根据资产负债表编号去确定项目编号）
     *              2）损益表同上
     *              3）财务指标表同上
     * @return
     */
    public List<FinanIndicAnalyDto> selectFinanindicAnalysFncItemDataByCusId(String cusId) {
        List<FinanIndicAnalyDto> list = new ArrayList<>();
        try {
            String newRptYear = fncStatBaseMapper.getNewRptYear(cusId);

            int curYear = Integer.parseInt(newRptYear.substring(0, 4));
            int curMonth = Integer.parseInt(newRptYear.substring(4));
            if (cusId.startsWith("8")) {
                CusCorp cusCorp = cusCorpService.selectByPrimaryKey(cusId);
                logger.info("查询当前客户基本信息:" + JSON.toJSONString(cusCorp));
                if (cusCorp != null && cn.com.yusys.yusp.commons.util.StringUtils.nonBlank(cusCorp.getFinaReportType())) {
                    FncConfTemplate fncConfTemplate = fncConfTemplateService.queryFncConfTemplateByKey(cusCorp.getFinaReportType());
                    logger.info("查询当前客户对应的财报类型信息:" + JSON.toJSONString(fncConfTemplate));
                    if (fncConfTemplate == null) {
                        logger.info("查询当前客户的财务报表类型{}配置异常,请确认客户信息是否完善正确!", JSON.toJSONString(cusCorp.getFinaReportType()));
                        return list;
                    }
                }
            }
            Map<String, String> queryParams = new HashMap<>();
            queryParams.put("cusId", cusId);
            queryParams.put("statStyle", "1");
            logger.info("资产负债表字段-固定资产净值 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "FNC_STAT_BS");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "Z01030110,ACC090117,XFZ0021,BS010110,GC0000013");
            List<String> itemValueList = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-固定资产净值当前年月数值字段:", JSON.toJSONString(itemValueList));
            for (String itemValue : itemValueList) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
            finanIndicAnalyDto.setItemId(CmisCommonConstants.STD_BS_ITEM_ID_01);
            finanIndicAnalyDto.setItemName(CmisCommonConstants.STD_BS_ITEM_NAME_01);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-固定资产净值上一年年报数值字段:", JSON.toJSONString(lastItemValueList));
            for (String lastItemValue : lastItemValueList) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-固定资产净值前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList));
            for (String lastTwoItemValue : lastTwoItemValueList) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto);
            logger.info("资产负债表字段-固定资产净值 处理-----------end--------------");

            logger.info("资产负债表字段-存货 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto1 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "FNC_STAT_BS");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "Z01011210,ACC090106,XFZ0010,GC0000005");
            List<String> itemValueList1 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-存货当前年月数值字段:", JSON.toJSONString(itemValueList1));
            for (String itemValue : itemValueList1) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto1.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto1.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto1.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
            finanIndicAnalyDto1.setItemId(CmisCommonConstants.STD_BS_ITEM_ID_02);
            finanIndicAnalyDto1.setItemName(CmisCommonConstants.STD_BS_ITEM_NAME_02);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto1.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto1.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList1 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-存货上一年年报数值字段:", JSON.toJSONString(lastItemValueList1));
            for (String lastItemValue : lastItemValueList1) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto1.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto1.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList1 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-存货前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList1));
            for (String lastTwoItemValue : lastTwoItemValueList1) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto1.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto1.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto1);
            logger.info("资产负债表字段-存货 处理-----------end--------------");

            logger.info("资产负债表字段-应收账款 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto2 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "FNC_STAT_BS");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "Z01010610,ACC090105,XFZ0005,GC0000006");
            List<String> itemValueList2 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-应收账款当前年月数值字段:", JSON.toJSONString(itemValueList2));
            for (String itemValue : itemValueList2) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto2.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto2.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto2.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
            finanIndicAnalyDto2.setItemId(CmisCommonConstants.STD_BS_ITEM_ID_03);
            finanIndicAnalyDto2.setItemName(CmisCommonConstants.STD_BS_ITEM_NAME_03);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto2.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto2.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList2 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-应收账款上一年年报数值字段:", JSON.toJSONString(lastItemValueList2));
            for (String lastItemValue : lastItemValueList2) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto2.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto2.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList2 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-应收账款前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList2));
            for (String lastTwoItemValue : lastTwoItemValueList2) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto2.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto2.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto2);
            logger.info("资产负债表字段-应收账款 处理-----------end--------------");

            logger.info("资产负债表字段-应付账款 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto3 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "FNC_STAT_BS");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "Z02010300,ACC090204,XFZ0040,GC0000021");
            List<String> itemValueList3 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-应付账款当前年月数值字段:", JSON.toJSONString(itemValueList3));
            for (String itemValue : itemValueList3) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto3.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto3.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto3.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
            finanIndicAnalyDto3.setItemId(CmisCommonConstants.STD_BS_ITEM_ID_04);
            finanIndicAnalyDto3.setItemName(CmisCommonConstants.STD_BS_ITEM_NAME_04);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto3.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto3.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList3 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-应付账款上一年年报数值字段:", JSON.toJSONString(lastItemValueList3));
            for (String lastItemValue : lastItemValueList3) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto3.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto3.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList3 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-应付账款前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList3));
            for (String lastTwoItemValue : lastTwoItemValueList3) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto3.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto3.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto3);
            logger.info("资产负债表字段-应付账款 处理-----------end--------------");

            logger.info("资产负债表字段-其他应收款 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto4 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "FNC_STAT_BS");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "Z01011100,XFZ0009,BS010104");
            List<String> itemValueList4 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-其他应收款当前年月数值字段:", JSON.toJSONString(itemValueList4));
            for (String itemValue : itemValueList4) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto4.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto4.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto4.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
            finanIndicAnalyDto4.setItemId(CmisCommonConstants.STD_BS_ITEM_ID_05);
            finanIndicAnalyDto4.setItemName(CmisCommonConstants.STD_BS_ITEM_NAME_05);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto4.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto4.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList4 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-其他应收款上一年年报数值字段:", JSON.toJSONString(lastItemValueList4));
            for (String lastItemValue : lastItemValueList4) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto4.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto4.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList4 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-其他应收款前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList4));
            for (String lastTwoItemValue : lastTwoItemValueList4) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto4.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto4.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto4);
            logger.info("资产负债表字段-其他应收款 处理-----------end--------------");

            logger.info("资产负债表字段-其他应付款 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto5 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "FNC_STAT_BS");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "Z02011000,XFZ0046,BS010205");
            List<String> itemValueList5 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-其他应付款当前年月数值字段:", JSON.toJSONString(itemValueList5));
            for (String itemValue : itemValueList5) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto5.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto5.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto5.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
            finanIndicAnalyDto5.setItemId(CmisCommonConstants.STD_BS_ITEM_ID_06);
            finanIndicAnalyDto5.setItemName(CmisCommonConstants.STD_BS_ITEM_NAME_06);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto5.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto5.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList5 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-其他应付款上一年年报数值字段:", JSON.toJSONString(lastItemValueList5));
            for (String lastItemValue : lastItemValueList5) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto5.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto5.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList5 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("资产负债表字段-其他应付款前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList5));
            for (String lastTwoItemValue : lastTwoItemValueList5) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto5.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto5.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto5);
            logger.info("资产负债表字段-其他应付款 处理-----------end--------------");
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(CmisCommonConstants.FNC_ASSET_PERCENT);
            String percent = adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
            logger.info("获取系统参数中比重配置{}", JSON.toJSONString(percent));
            logger.info("资产负债表字段-资产科目中其他占总资产比重大于10%的科目(10%可配置) 处理-----------start--------------");
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_ITEM_ID as itemId,STAT_END_AMT_Y as data1");
            } else {
                queryParams.put("dataField", "STAT_ITEM_ID as itemId,STAT_END_AMT".concat(String.valueOf(curMonth)).concat(" as data1"));
            }
            queryParams.put("tableName", "FNC_STAT_BS");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "BS010120,XFZ0035,Z01000001,ACC090123,GC0000014");
            List<FncStatRptItemDTO> totalAssetList = fncStatBsMapper.queryItemValueByParams2(queryParams);
            logger.info("资产负债表字段-资产科目中总资产:", JSON.toJSONString(totalAssetList));
            BigDecimal totalAsset = new BigDecimal(0);
            for (FncStatRptItemDTO fncStatRptItemDTO : totalAssetList) {
                if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())) != null && BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())).compareTo(new BigDecimal(0)) > 0) {
                    totalAsset = BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1()));
                    break;
                }
            }
            totalAsset = totalAsset.multiply(BigDecimal.valueOf(Double.parseDouble(percent)));
            logger.info("资产负债表字段-资产科目中 总资产*比重 之后:" + JSON.toJSONString(totalAsset));
            queryParams.put("itemId", "BS010102,BS010103,BS010105,BS010106,BS010107,BS010108,BS010109,XFZ0002,XFZ0003,XFZ0004,XFZ0006,XFZ0007,XFZ0008,XFZ0011,XFZ0012,XFZ0013,XFZ0016,XFZ0017,XFZ0018,XFZ0019,XFZ0020,XFZ0022,XFZ0023,XFZ0025,XFZ0026,XFZ0027,XFZ0028,XFZ0029,XFZ0030,XFZ0031,XFZ0032,Z01010100,Z01010200,Z01010300,Z01010400,Z01010500,Z01010600,Z01010700,Z01010800,Z01010900,Z01011000,Z01011200,Z01011300,Z01011400,Z01011500,Z01011600,Z01020001,Z01030100,Z01030300,Z01040100,Z01040300,Z01050001,ACC090103,ACC090104,ACC090109,ACC090110,ACC090119,ACC090122,GC0000001,GC0000002,GC0000003,GC0000004,GC0000008,GC0000009,GC0000010,GC0000011,GC0000012");
            if (curMonth == 12) {
                queryParams.put("dataField", "A.STAT_ITEM_ID as itemId,A.STAT_END_AMT_Y as data1,B.item_name as data2");
            } else {
                queryParams.put("dataField", "A.STAT_ITEM_ID as itemId,A.STAT_END_AMT".concat(String.valueOf(curMonth)).concat(" as data1,B.item_name as data2"));
            }
            queryParams.put("tableName", "FNC_STAT_BS A");
            List<FncStatRptItemDTO> willShowItemList = fncStatBsMapper.queryItemValueByParams3(queryParams);
            for (FncStatRptItemDTO fncStatRptItemDTO : willShowItemList) {
                logger.info("当前正在处理项目编号{[]},项目名称{[]}.", fncStatRptItemDTO.getItemId(), fncStatRptItemDTO.getData2());
                if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())) != null && BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())).compareTo(totalAsset) > -1) {
                    logger.info("当前正在处理的需要展示的当前年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO.getItemId(), fncStatRptItemDTO.getData1());
                    FinanIndicAnalyDto finanIndicAnalyDtoShow = new FinanIndicAnalyDto();
                    finanIndicAnalyDtoShow.setItemId(fncStatRptItemDTO.getItemId());
                    finanIndicAnalyDtoShow.setItemName(fncStatRptItemDTO.getData2());
                    if (curMonth == 11 || curMonth == 12) {
                        finanIndicAnalyDtoShow.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
                    } else {
                        finanIndicAnalyDtoShow.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
                    }
                    finanIndicAnalyDtoShow.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())));
                    queryParams.put("dataField", "STAT_END_AMT_Y as data1");
                    queryParams.put("rptYear", String.valueOf(curYear - 1));
                    queryParams.put("itemId", fncStatRptItemDTO.getItemId());
                    List<FncStatRptItemDTO> lastYearValueList = fncStatBsMapper.queryItemValueByParams2(queryParams);
                    for (FncStatRptItemDTO fncStatRptItemDTO1 : lastYearValueList) {
                        if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1())) != null && new BigDecimal(0).compareTo(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1()))) < 0) {
                            logger.info("当前正在处理的需要展示的上一年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO1.getItemId(), fncStatRptItemDTO1.getData1());
                            finanIndicAnalyDtoShow.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1())));
                            break;
                        }
                    }
                    queryParams.put("rptYear", String.valueOf(curYear - 2));
                    List<FncStatRptItemDTO> lastTwoYearValueList = fncStatBsMapper.queryItemValueByParams2(queryParams);
                    for (FncStatRptItemDTO fncStatRptItemDTO1 : lastTwoYearValueList) {
                        if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1())) != null && new BigDecimal(0).compareTo(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1()))) < 0) {
                            logger.info("当前正在处理的需要展示的前两年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO1.getItemId(), fncStatRptItemDTO1.getData1());
                            finanIndicAnalyDtoShow.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1())));
                            break;
                        }
                    }
                    finanIndicAnalyDtoShow.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
                    list.add(finanIndicAnalyDtoShow);
                }
            }
            logger.info("资产负债表字段-资产科目中其他占总资产比重大于10%的科目(10%可配置) 处理-----------end--------------");

            logger.info("资产负债表字段-负债科目中占 总负债 比重大于10%的科目(比率可配置) 处理-----------start--------------");
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_ITEM_ID as itemId,STAT_END_AMT_Y as data1");
            } else {
                queryParams.put("dataField", "STAT_ITEM_ID as itemId,STAT_END_AMT".concat(String.valueOf(curMonth)).concat(" as data1"));
            }
            queryParams.put("tableName", "FNC_STAT_BS");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "BS010221,XFZ0060,Z02000001,ACC090213,GC0000015");
            List<FncStatRptItemDTO> totalLiabilityList = fncStatBsMapper.queryItemValueByParams2(queryParams);
            logger.info("资产负债表字段-资产科目中总负债:", JSON.toJSONString(totalAssetList));
            BigDecimal totalLiability = new BigDecimal(0);
            for (FncStatRptItemDTO fncStatRptItemDTO : totalLiabilityList) {
                if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())) != null && BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())).compareTo(new BigDecimal(0)) > 0) {
                    totalLiability = BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1()));
                    break;
                }
            }
            totalLiability = totalLiability.multiply(BigDecimal.valueOf(Double.parseDouble(percent)));
            logger.info("资产负债表字段-资产科目中 总负债*比重 之后:" + JSON.toJSONString(totalLiability));
            queryParams.put("itemId", "BS0102011,BS010202,BS010203,BS010204,XFZ0037,XFZ0038,XFZ0039,XFZ0041,XFZ0042,XFZ0043,XFZ0044,XFZ0045,XFZ0047,XFZ0048,XFZ0049,XFZ0052,XFZ0053,XFZ0054,XFZ0055,XFZ0056,XFZ0057,XFZ0058,Z02010100,Z02010200,Z02010400,Z02010500,Z02010600,Z02010700,Z02010800,Z02010900,Z02011100,Z02011200,Z02011300,Z02011400,Z02020100,Z02020200,Z02020300,Z02020400,Z02020500,ACC090203,ACC090205,ACC090206,ACC090209,ACC090210,ACC090211,GC0000016,B31211133,B31231222");
            if (curMonth == 12) {
                queryParams.put("dataField", "A.STAT_ITEM_ID as itemId,A.STAT_END_AMT_Y as data1,B.item_name as data2");
            } else {
                queryParams.put("dataField", "A.STAT_ITEM_ID as itemId,A.STAT_END_AMT".concat(String.valueOf(curMonth)).concat(" as data1,B.item_name as data2"));
            }
            queryParams.put("tableName", "FNC_STAT_BS A");
            List<FncStatRptItemDTO> willShowItemList1 = fncStatBsMapper.queryItemValueByParams3(queryParams);
            for (FncStatRptItemDTO fncStatRptItemDTO : willShowItemList1) {
                logger.info("当前正在处理项目编号{[]},项目名称{[]}.", fncStatRptItemDTO.getItemId(), fncStatRptItemDTO.getData2());
                if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())) != null && BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())).compareTo(totalAsset) > -1) {
                    logger.info("当前正在处理的需要展示的当前年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO.getItemId(), fncStatRptItemDTO.getData1());
                    FinanIndicAnalyDto finanIndicAnalyDtoShow = new FinanIndicAnalyDto();
                    finanIndicAnalyDtoShow.setItemId(fncStatRptItemDTO.getItemId());
                    finanIndicAnalyDtoShow.setItemName(fncStatRptItemDTO.getData2());
                    if (curMonth == 11 || curMonth == 12) {
                        finanIndicAnalyDtoShow.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
                    } else {
                        finanIndicAnalyDtoShow.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
                    }
                    finanIndicAnalyDtoShow.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())));
                    queryParams.put("dataField", "STAT_END_AMT_Y as data1");
                    queryParams.put("rptYear", String.valueOf(curYear - 1));
                    queryParams.put("itemId", fncStatRptItemDTO.getItemId());
                    List<FncStatRptItemDTO> lastTwoYearValueList = fncStatBsMapper.queryItemValueByParams2(queryParams);
                    for (FncStatRptItemDTO fncStatRptItemDTO1 : lastTwoYearValueList) {
                        if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1())) != null && new BigDecimal(0).compareTo(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1()))) < 0) {
                            logger.info("当前正在处理的需要展示的上一年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO1.getItemId(), fncStatRptItemDTO1.getData1());
                            finanIndicAnalyDtoShow.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1())));
                            break;
                        }
                    }
                    queryParams.put("rptYear", String.valueOf(curYear - 2));
                    List<FncStatRptItemDTO> lastTwoYearValueList1 = fncStatBsMapper.queryItemValueByParams2(queryParams);
                    for (FncStatRptItemDTO fncStatRptItemDTO1 : lastTwoYearValueList1) {
                        if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1())) != null && new BigDecimal(0).compareTo(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1()))) < 0) {
                            logger.info("当前正在处理的需要展示的前两年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO1.getItemId(), fncStatRptItemDTO1.getData1());
                            finanIndicAnalyDtoShow.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO1.getData1())));
                            break;
                        }
                    }
                    finanIndicAnalyDtoShow.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
                    list.add(finanIndicAnalyDtoShow);
                }
            }
            logger.info("资产负债表字段-资产科目中其他占总资产比重大于10%的科目(10%可配置) 处理-----------end--------------");

            logger.info("损益表字段-营业成本 处理-----------start--------------");
            BigDecimal curYearOperCosts = new BigDecimal(0);
            BigDecimal lastYearOperCosts = new BigDecimal(0);
            BigDecimal lastTwoYearOperCosts = new BigDecimal(0);
            BigDecimal lastThreeYearOperCosts = new BigDecimal(0);
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_stat_is");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "L02020000,XSL0002");
            List<String> itemValueListNoEsxit1 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-营业成本当前年月数值字段:", JSON.toJSONString(itemValueListNoEsxit1));
            for (String itemValue : itemValueListNoEsxit1) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    continue;
                } else {
                    curYearOperCosts = BigDecimal.valueOf(Double.parseDouble(itemValue));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> itemValueListNo2 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-营业成本上一年年报数值字段:", JSON.toJSONString(itemValueListNo2));
            for (String lastItemValue : itemValueListNo2) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    continue;
                } else {
                    lastYearOperCosts = BigDecimal.valueOf(Double.parseDouble(lastItemValue));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> itemValueListNo3 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-营业成本前两年年报数值字段:", JSON.toJSONString(itemValueListNo3));
            for (String lastTwoItemValue : itemValueListNo3) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    continue;
                } else {
                    lastTwoYearOperCosts = BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue));
                    break;
                }
            }
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> itemValueListNo4 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-营业成本前三年年报数值字段:", JSON.toJSONString(itemValueListNo4));
            for (String lastThreeItemValue : itemValueListNo4) {
                if ("0".equals(lastThreeItemValue) || "".equals(lastThreeItemValue) || lastThreeItemValue == null) {
                    continue;
                } else {
                    lastThreeYearOperCosts = BigDecimal.valueOf(Double.parseDouble(lastThreeItemValue));
                    break;
                }
            }
            logger.info("损益表字段-营业成本 处理-----------end--------------当前年{[]},前一年{[]},前两年{[]},前三年{[]}", curYearOperCosts, lastYearOperCosts, lastTwoYearOperCosts, lastThreeYearOperCosts);

            logger.info("损益表字段-营业收入 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto6 = new FinanIndicAnalyDto();

            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_stat_is");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "L01000000,XSL0001,PL010110");
            List<String> itemValueList6 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-营业收入当前年月数值字段:", JSON.toJSONString(itemValueList6));
            for (String itemValue : itemValueList6) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto6.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto6.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto6.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_03);
            finanIndicAnalyDto6.setItemId(CmisCommonConstants.STD_IS_ITEM_ID_01);
            finanIndicAnalyDto6.setItemName(CmisCommonConstants.STD_IS_ITEM_NAME_01);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto6.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto6.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList6 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-营业收入上一年年报数值字段:", JSON.toJSONString(lastItemValueList6));
            for (String lastItemValue : lastItemValueList6) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto6.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto6.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList6 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-营业收入前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList6));
            for (String lastTwoItemValue : lastTwoItemValueList6) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto6.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto6.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            // 前三年营业收入
            BigDecimal lastThreeOperIncome = new BigDecimal(0);
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastThreeItemValueList6 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-营业收入前三年年报数值字段:", JSON.toJSONString(lastThreeItemValueList6));
            for (String lastTwoItemValue : lastThreeItemValueList6) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    lastThreeOperIncome = new BigDecimal(0);
                    continue;
                } else {
                    lastThreeOperIncome = BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue));
                    break;
                }
            }
            list.add(finanIndicAnalyDto6);
            logger.info("损益表字段-营业收入 处理-----------end--------------");

            logger.info("插队处理字段-毛利润增长率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto21 = new FinanIndicAnalyDto();
            // 毛利率增长率
            finanIndicAnalyDto21.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto21.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_14);
            finanIndicAnalyDto21.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_14);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto21.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto21.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            BigDecimal curMao = new BigDecimal(0);
            BigDecimal lastMao = new BigDecimal(0);
            BigDecimal lastTwoMao = new BigDecimal(0);
            try {
                curMao = ((finanIndicAnalyDto6.getCurYmValue().subtract(curYearOperCosts)).subtract(finanIndicAnalyDto6.getNearFirstValue().subtract(lastYearOperCosts))).divide(finanIndicAnalyDto6.getNearFirstValue().subtract(lastYearOperCosts));
                lastMao = ((finanIndicAnalyDto6.getNearFirstValue().subtract(lastYearOperCosts)).subtract(finanIndicAnalyDto6.getNearSecondValue().subtract(lastTwoYearOperCosts))).divide(finanIndicAnalyDto6.getNearSecondValue().subtract(lastTwoYearOperCosts));
                lastTwoMao = ((finanIndicAnalyDto6.getNearSecondValue().subtract(lastTwoYearOperCosts)).subtract(lastThreeOperIncome.subtract(lastThreeYearOperCosts))).divide(lastThreeOperIncome.subtract(lastThreeYearOperCosts));
            } catch (Exception e) {
                logger.info("计算毛利润增长率报错,请核对近几年财报中营业成本以及营业收入的数据是否正常,计算公式:（本年毛利润-上年毛利润）/上年毛利润---毛利润=营业收入(损益表)-营业成本(损益表)");
                curMao = new BigDecimal(0);
                lastMao = new BigDecimal(0);
                lastTwoMao = new BigDecimal(0);
            }
            finanIndicAnalyDto21.setCurYmValue(curMao);
            finanIndicAnalyDto21.setNearFirstValue(lastMao);
            finanIndicAnalyDto21.setNearSecondValue(lastTwoMao);
            list.add(finanIndicAnalyDto21);
            logger.info("插队处理字段-毛利润增长率 处理-----------end--------------");

            logger.info("损益表字段-净利润 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto7 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_END_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_END_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_stat_is");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "L06010000,XSL0017");
            List<String> itemValueList7 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-净利润-当前年月数值字段:", JSON.toJSONString(itemValueList7));
            for (String itemValue : itemValueList7) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto7.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto7.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto7.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_03);
            finanIndicAnalyDto7.setItemId(CmisCommonConstants.STD_IS_ITEM_ID_02);
            finanIndicAnalyDto7.setItemName(CmisCommonConstants.STD_IS_ITEM_NAME_02);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto7.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto7.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList7 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-净利润上一年年报数值字段:", JSON.toJSONString(lastItemValueList7));
            for (String lastItemValue : lastItemValueList7) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto7.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto7.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_END_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList7 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("损益表字段-净利润前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList7));
            for (String lastTwoItemValue : lastTwoItemValueList7) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto7.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto7.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto7);
            logger.info("损益表字段-净利润 处理-----------end--------------");

            logger.info("财报指标表字段-毛利率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto8 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C01130000,XSC0021");
            List<String> itemValueList8 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-毛利率-当前年月数值字段:", JSON.toJSONString(itemValueList8));
            for (String itemValue : itemValueList8) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto8.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto8.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto8.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto8.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_01);
            finanIndicAnalyDto8.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_01);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto8.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto8.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList8 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-毛利率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList8));
            for (String lastItemValue : lastItemValueList8) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto8.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto8.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList8 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-毛利率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList8));
            for (String lastTwoItemValue : lastTwoItemValueList8) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto8.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto8.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto8);
            logger.info("财报指标表字段-毛利率-处理-----------end--------------");

            logger.info("财报指标表字段-销售净利润率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto9 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C01040000,XSC0022");
            List<String> itemValueList9 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-销售净利润率-当前年月数值字段:" + JSON.toJSONString(itemValueList9));
            for (String itemValue : itemValueList9) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto9.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto9.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto9.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto9.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_02);
            finanIndicAnalyDto9.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_02);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto9.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto9.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList9 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-销售净利润率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList9));
            for (String lastItemValue : lastItemValueList9) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto9.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto9.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList9 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-销售净利润率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList9));
            for (String lastTwoItemValue : lastTwoItemValueList9) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto9.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto9.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto9);
            logger.info("财报指标表字段-销售净利润率-处理-----------end--------------");

            logger.info("财报指标表字段-总资产周转率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto10 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C02010000,XSC0015");
            List<String> itemValueList10 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-总资产周转率-当前年月数值字段:" + JSON.toJSONString(itemValueList10));
            for (String itemValue : itemValueList10) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto10.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto10.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto10.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto10.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_03);
            finanIndicAnalyDto10.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_03);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto10.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto10.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList10 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-总资产周转率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList10));
            for (String lastItemValue : lastItemValueList10) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto10.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto10.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList10 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-总资产周转率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList10));
            for (String lastTwoItemValue : lastTwoItemValueList10) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto10.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto10.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto10);
            logger.info("财报指标表字段-总资产周转率-处理-----------end--------------");

            logger.info("财报指标表字段-固定资产周转率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto11 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C02020000,XSC0034");
            List<String> itemValueList11 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-固定资产周转率-当前年月数值字段:" + JSON.toJSONString(itemValueList11));
            for (String itemValue : itemValueList11) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto11.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto11.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto11.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto11.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_04);
            finanIndicAnalyDto11.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_04);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto11.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto11.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList11 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-固定资产周转率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList11));
            for (String lastItemValue : lastItemValueList11) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto11.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto11.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList11 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-固定资产周转率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList11));
            for (String lastTwoItemValue : lastTwoItemValueList11) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto11.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto11.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto11);
            logger.info("财报指标表字段-固定资产周转率-处理-----------end--------------");

            logger.info("财报指标表字段-应收账款周转率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto12 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C02020000,XSC0034");
            List<String> itemValueList12 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-应收账款周转率-当前年月数值字段:" + JSON.toJSONString(itemValueList12));
            //特殊处理 财报指标表字段-应收账款周天数=365/应收账款周转率-当前年月数值字段
            BigDecimal item12CurYearDays = new BigDecimal(0);
            BigDecimal item12LastYearDays = new BigDecimal(0);
            BigDecimal item12LsatTwoYearDays = new BigDecimal(0);
            for (String itemValue : itemValueList12) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null || BigDecimal.valueOf(Double.parseDouble(itemValue)).compareTo(BigDecimal.ZERO) == 0) {
                    finanIndicAnalyDto12.setCurYmValue(new BigDecimal(0));
                    logger.info("财报指标表字段-应收账款周天数=365/应收账款周转率-当前应收账款周转率为0,所以天数字段也取0做处理" + JSON.toJSONString(new BigDecimal(0)));
                    item12CurYearDays = new BigDecimal(0);
                    continue;
                } else {
                    finanIndicAnalyDto12.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    logger.info("财报指标表字段-应收账款周天数=365/应收账款周转率:" + new BigDecimal(365).divide(new BigDecimal(itemValue),2,BigDecimal.ROUND_HALF_UP));
                    try{
                        item12CurYearDays = new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(itemValue)),2,BigDecimal.ROUND_HALF_UP);
                    }catch (Exception e){
                        item12CurYearDays = new BigDecimal(0);
                    }

                    break;
                }
            }
            finanIndicAnalyDto12.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto12.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_05);
            finanIndicAnalyDto12.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_05);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto12.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto12.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList12 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-应收账款周转率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList12));
            for (String lastItemValue : lastItemValueList12) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null || BigDecimal.valueOf(Double.parseDouble(lastItemValue)).compareTo(BigDecimal.ZERO) == 0) {
                    finanIndicAnalyDto12.setNearFirstValue(new BigDecimal(0));
                    logger.info("财报指标表字段上一年数据处理-应收账款周天数=365/应收账款周转率-当前应收账款周转率为0,所以天数字段也取0做处理" + JSON.toJSONString(new BigDecimal(0)));
                    item12LastYearDays = new BigDecimal(0);
                    continue;
                } else {
                    finanIndicAnalyDto12.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    logger.info("财报指标表字段上一年数据处理-应收账款周天数=365/应收账款周转率:" + new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(lastItemValue)),2,BigDecimal.ROUND_HALF_UP));
                    try{
                        item12LastYearDays = new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(lastItemValue)),2,BigDecimal.ROUND_HALF_UP);
                    }catch (Exception e){
                        item12LastYearDays = new BigDecimal(0);
                    }

                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList12 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-应收账款周转率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList12));
            for (String lastTwoItemValue : lastTwoItemValueList12) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null || BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)).compareTo(BigDecimal.ZERO) == 0) {
                    finanIndicAnalyDto12.setNearSecondValue(new BigDecimal(0));
                    logger.info("财报指标表字段前两年数据处理-应收账款周天数=365/应收账款周转率-当前应收账款周转率为0,所以天数字段也取0做处理" + JSON.toJSONString(new BigDecimal(0)));
                    item12LsatTwoYearDays = new BigDecimal(0);
                    continue;
                } else {
                    finanIndicAnalyDto12.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    logger.info("财报指标表字段前两年数据处理-应收账款周天数=365/应收账款周转率:" + new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)),2,BigDecimal.ROUND_HALF_UP));
                    try{
                        item12LsatTwoYearDays = new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)),2,BigDecimal.ROUND_HALF_UP);
                    }catch (Exception e){
                        item12LsatTwoYearDays = new BigDecimal(0);
                    }

                    break;
                }
            }
            list.add(finanIndicAnalyDto12);
            logger.info("财报指标表字段-应收账款周转天数=365/应收账款周转率(根据周转率重新处理)-处理start---");
            FinanIndicAnalyDto finanIndicAnalyDto13 = new FinanIndicAnalyDto();
            finanIndicAnalyDto13.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_06);
            finanIndicAnalyDto13.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_06);
            finanIndicAnalyDto13.setCurYmValue(item12CurYearDays);
            finanIndicAnalyDto13.setNearFirstValue(item12LastYearDays);
            finanIndicAnalyDto13.setNearSecondValue(item12LsatTwoYearDays);
            finanIndicAnalyDto13.setInputYear(finanIndicAnalyDto12.getInputYear());
            finanIndicAnalyDto13.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            list.add(finanIndicAnalyDto13);
            logger.info("财报指标表字段-应收账款周转天数=365/应收账款周转率(根据周转率重新处理)-处理end---");
            logger.info("财报指标表字段-应收账款周转率-处理-----------end--------------");

            logger.info("财报指标表字段-存货周转率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto14 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C02040000,XSC0014");
            List<String> itemValueList14 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-存货周转率-当前年月数值字段:" + JSON.toJSONString(itemValueList14));
            //特殊处理 财报指标表字段-存货周转天数=365/存货周转率-当前年月数值字段
            BigDecimal item14CurYearDays = new BigDecimal(0);
            BigDecimal item14LastYearDays = new BigDecimal(0);
            BigDecimal item14LsatTwoYearDays = new BigDecimal(0);
            for (String itemValue : itemValueList14) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null || BigDecimal.valueOf(Double.parseDouble(itemValue)).compareTo(BigDecimal.ZERO) == 0) {
                    finanIndicAnalyDto14.setCurYmValue(new BigDecimal(0));
                    logger.info("财报指标表字段-存货周转天数=365/存货周转率-当前存货周转率为0,所以天数字段也取0做处理" + JSON.toJSONString(new BigDecimal(0)));
                    item14CurYearDays = new BigDecimal(0);
                    continue;
                } else {
                    finanIndicAnalyDto14.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    logger.info("财报指标表字段-存货周转天数=365/存货周转率:" + new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(itemValue)),2,BigDecimal.ROUND_HALF_UP));
                    try{
                        item14CurYearDays = new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(itemValue)),2,BigDecimal.ROUND_HALF_UP);
                    }catch (Exception e){
                        item14CurYearDays = new BigDecimal(0);
                    }

                    break;
                }
            }
            finanIndicAnalyDto14.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto14.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_07);
            finanIndicAnalyDto14.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_07);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto14.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto14.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList14 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-存货周转率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList14));
            for (String lastItemValue : lastItemValueList14) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null || BigDecimal.valueOf(Double.parseDouble(lastItemValue)).compareTo(BigDecimal.ZERO) == 0) {
                    finanIndicAnalyDto14.setNearFirstValue(new BigDecimal(0));
                    logger.info("财报指标表字段上一年数据处理-存货周转天数=365/存货周转率-当前存货周转率为0,所以天数字段也取0做处理" + JSON.toJSONString(new BigDecimal(0)));
                    item14LastYearDays = new BigDecimal(0);
                    continue;
                } else {
                    finanIndicAnalyDto14.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    logger.info("财报指标表字段上一年数据处理-存货周转天数=365/存货周转率:" + new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(lastItemValue)),2,BigDecimal.ROUND_HALF_UP));
                    try{
                        item14LastYearDays = new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(lastItemValue)),2,BigDecimal.ROUND_HALF_UP);
                    }catch (Exception e){
                        item14LastYearDays = new BigDecimal(0);
                    }

                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList14 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-存货周转率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList14));
            for (String lastTwoItemValue : lastTwoItemValueList14) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null || BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)).compareTo(BigDecimal.ZERO) == 0) {
                    finanIndicAnalyDto14.setNearSecondValue(new BigDecimal(0));
                    logger.info("财报指标表字段前两年数据处理-存货周转天数=365/存货周转率-当前存货周转率为0,所以天数字段也取0做处理" + JSON.toJSONString(new BigDecimal(0)));
                    item14LsatTwoYearDays = new BigDecimal(0);
                    continue;
                } else {
                    finanIndicAnalyDto14.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    logger.info("财报指标表字段前两年数据处理-存货周转天数=365/存货周转率:" + new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)),2,BigDecimal.ROUND_HALF_UP));
                    try{
                        item14LsatTwoYearDays = new BigDecimal(365).divide(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)),2,BigDecimal.ROUND_HALF_UP);
                    }catch (Exception e){
                        item14LsatTwoYearDays = new BigDecimal(0);
                    }

                    break;
                }
            }
            list.add(finanIndicAnalyDto14);
            logger.info("财报指标表字段-存货周转天数=365/存货周转率(根据存货周转率重新处理)-处理start---");
            FinanIndicAnalyDto finanIndicAnalyDto15 = new FinanIndicAnalyDto();
            finanIndicAnalyDto15.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_08);
            finanIndicAnalyDto15.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_08);
            finanIndicAnalyDto15.setCurYmValue(item14CurYearDays);
            finanIndicAnalyDto15.setNearFirstValue(item14LastYearDays);
            finanIndicAnalyDto15.setNearSecondValue(item14LsatTwoYearDays);
            finanIndicAnalyDto15.setInputYear(finanIndicAnalyDto14.getInputYear());
            finanIndicAnalyDto15.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            list.add(finanIndicAnalyDto15);
            logger.info("财报指标表字段-存货周转天数=365/存货周转率(根据周转率重新处理)-处理end---");
            logger.info("财报指标表字段-存货周转率-处理-----------end--------------");

            logger.info("财报指标表字段-资产负债率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto16 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C03010000,XSC0003");
            List<String> itemValueList16 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-资产负债率-当前年月数值字段:" + JSON.toJSONString(itemValueList16));
            for (String itemValue : itemValueList16) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto16.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto16.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto16.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto16.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_09);
            finanIndicAnalyDto16.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_09);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto16.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto16.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList16 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-资产负债率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList16));
            for (String lastItemValue : lastItemValueList16) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto16.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto16.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList16 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-资产负债率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList16));
            for (String lastTwoItemValue : lastTwoItemValueList16) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto16.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto16.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto16);
            logger.info("财报指标表字段-资产负债率-处理-----------end--------------");

            logger.info("财报指标表字段-利息保障倍数 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto17 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C03040000,XSC0033");
            List<String> itemValueList17 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-利息保障倍数-当前年月数值字段:" + JSON.toJSONString(itemValueList17));
            for (String itemValue : itemValueList17) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto17.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto17.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto17.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto17.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_10);
            finanIndicAnalyDto17.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_10);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto17.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto17.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList17 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-利息保障倍数-上一年年报数值字段:", JSON.toJSONString(lastItemValueList17));
            for (String lastItemValue : lastItemValueList17) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto17.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto17.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList17 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-利息保障倍数-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList17));
            for (String lastTwoItemValue : lastTwoItemValueList17) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto17.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto17.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto17);
            logger.info("财报指标表字段-利息保障倍数-处理-----------end--------------");

            logger.info("财报指标表字段-流动比率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto18 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C04010000,XSC0005");
            List<String> itemValueList18 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-流动比率-当前年月数值字段:" + JSON.toJSONString(itemValueList18));
            for (String itemValue : itemValueList18) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto18.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto18.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto18.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto18.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_11);
            finanIndicAnalyDto18.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_11);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto18.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto18.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList18 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-流动比率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList18));
            for (String lastItemValue : lastItemValueList18) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto18.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto18.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList18 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-流动比率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList18));
            for (String lastTwoItemValue : lastTwoItemValueList18) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto18.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto18.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto18);
            logger.info("财报指标表字段-流动比率-处理-----------end--------------");

            logger.info("财报指标表字段-速动比率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto19 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C04020000,XSC0006");
            List<String> itemValueList19 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-速动比率-当前年月数值字段:" + JSON.toJSONString(itemValueList19));
            for (String itemValue : itemValueList19) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto19.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto19.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto19.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto19.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_12);
            finanIndicAnalyDto19.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_12);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto19.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto19.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList19 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-速动比率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList19));
            for (String lastItemValue : lastItemValueList19) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto19.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto19.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList19 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-速动比率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList19));
            for (String lastTwoItemValue : lastTwoItemValueList19) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto19.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto19.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto19);
            logger.info("财报指标表字段-速动比率-处理-----------end--------------");

            logger.info("财报指标表字段-营业收入增长率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto20 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C05050000,XSC0032");
            List<String> itemValueList20 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-营业收入增长率-当前年月数值字段:" + JSON.toJSONString(itemValueList20));
            for (String itemValue : itemValueList20) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto20.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto20.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto20.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto20.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_13);
            finanIndicAnalyDto20.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_13);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto20.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto20.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList20 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-营业收入增长率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList20));
            for (String lastItemValue : lastItemValueList20) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto20.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto20.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList20 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-营业收入增长率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList20));
            for (String lastTwoItemValue : lastTwoItemValueList20) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto20.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto20.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto20);
            logger.info("财报指标表字段-营业收入增长率-处理-----------end--------------");

            logger.info("财报指标表字段-净利润增长率 处理-----------start--------------");
            FinanIndicAnalyDto finanIndicAnalyDto22 = new FinanIndicAnalyDto();
            if (curMonth == 12) {
                queryParams.put("dataField", "STAT_INIT_AMT_Y");
            } else {
                queryParams.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(curMonth)));
            }
            queryParams.put("tableName", "fnc_index_rpt");
            queryParams.put("rptYear", String.valueOf(curYear));
            queryParams.put("itemId", "C05010000,XSC0030");
            List<String> itemValueList22 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-净利润增长率-当前年月数值字段:" + JSON.toJSONString(itemValueList22));
            for (String itemValue : itemValueList22) {
                if ("0".equals(itemValue) || "".equals(itemValue) || itemValue == null) {
                    finanIndicAnalyDto22.setCurYmValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto22.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(itemValue)));
                    break;
                }
            }
            finanIndicAnalyDto22.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_04);
            finanIndicAnalyDto22.setItemId(CmisCommonConstants.STD_SL_ITEM_ID_15);
            finanIndicAnalyDto22.setItemName(CmisCommonConstants.STD_SL_ITEM_NAME_15);
            if (curMonth == 11 || curMonth == 12) {
                finanIndicAnalyDto22.setInputYear(String.valueOf(curYear).concat(String.valueOf(curMonth)));
            } else {
                finanIndicAnalyDto22.setInputYear(String.valueOf(curYear).concat("0").concat(String.valueOf(curMonth)));
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 1));
            List<String> lastItemValueList22 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-净利润增长率-上一年年报数值字段:", JSON.toJSONString(lastItemValueList22));
            for (String lastItemValue : lastItemValueList22) {
                if ("0".equals(lastItemValue) || "".equals(lastItemValue) || lastItemValue == null) {
                    finanIndicAnalyDto22.setNearFirstValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto22.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastItemValue)));
                    break;
                }
            }
            queryParams.put("dataField", "STAT_INIT_AMT_Y");
            queryParams.put("rptYear", String.valueOf(curYear - 2));
            List<String> lastTwoItemValueList22 = fncStatBsMapper.queryItemValueByParams(queryParams);
            logger.info("财报指标表字段-净利润增长率-前两年年报数值字段:", JSON.toJSONString(lastTwoItemValueList22));
            for (String lastTwoItemValue : lastTwoItemValueList22) {
                if ("0".equals(lastTwoItemValue) || "".equals(lastTwoItemValue) || lastTwoItemValue == null) {
                    finanIndicAnalyDto22.setNearSecondValue(new BigDecimal(0));
                    continue;
                } else {
                    finanIndicAnalyDto22.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastTwoItemValue)));
                    break;
                }
            }
            list.add(finanIndicAnalyDto22);
            logger.info("财报指标表字段-净利润增长率-处理-----------end--------------");

            logger.info("财报指标表字段-销贷比-处理:销贷比 = 营业收入/银行融资-------------start-----------");
            logger.info("财报指标表字段-销贷比-处理:销贷比 = 营业收入/银行融资-------------end-----------");
        } catch (Exception e) {
            logger.error("set property is error", e);
            throw new BizException(null, null, null, null, "set property is error" + e.getMessage());
        }
        return list;
    }

    /**
     * 调查报告取财报字段
     *
     * @param map
     * @return
     */
    public List<FinanIndicAnalyDto> getRptFncTotalProfit(Map map) {
        List<FinanIndicAnalyDto> result = new ArrayList<>();
        String cusId = map.get("cusId").toString();
        FinanIndicAnalyDto finanIndicAnalyDto1 = new FinanIndicAnalyDto();
        Map<String, String> params1 = new HashMap();
        String rptYear = fncStatBaseMapper.getNewRptYear(cusId);
        if(StringUtils.isBlank(rptYear)){
            rptYear = DateUtils.getCurrentDate(DateFormatEnum.YEAR_MONTH);
        }
        int year = Integer.parseInt(rptYear.substring(0, 4));
        int lastYear = year - 1;
        int lastSecond = year - 2;
        int month = Integer.parseInt(rptYear.substring(4));
        params1.put("rptYear", String.valueOf(year));
        params1.put("cusId", cusId);
        params1.put("tableName", "fnc_stat_is");
        params1.put("statStyle", "1");
        if (month == 12) {
            params1.put("dataField", "STAT_END_AMT_Y");
        } else {
            params1.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params1.put("itemId", "XSL0015,L05010000");
        //获取当年利润总额
        List<String> currYearFnc = fncStatBsMapper.queryItemValueByParams(params1);
        finanIndicAnalyDto1.setItemName("利润总额");
        finanIndicAnalyDto1.setInputYear(rptYear);
        for (String currYearValue : currYearFnc) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto1.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto1.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params1.put("dataField", "STAT_END_AMT_Y");
        params1.put("rptYear", String.valueOf(lastYear));
        //取去年利润总额
        List<String> lastYearFnc = fncStatBsMapper.queryItemValueByParams(params1);
        for (String lastYearValue : lastYearFnc) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto1.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto1.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年利润总额
        params1.put("dataField", "STAT_END_AMT_Y");
        params1.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc = fncStatBsMapper.queryItemValueByParams(params1);
        for (String lastSecondValue : lastSecondYearFnc) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto1.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto1.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto1);
        //销售收入
        Map<String, String> params2 = new HashMap<>();
        params2.put("cusId", cusId);
        params2.put("tableName", "fnc_stat_is");
        params2.put("statStyle", "1");
        params2.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto2 = new FinanIndicAnalyDto();
        if (month == 12) {
            params2.put("dataField", "STAT_END_AMT_Y");
        } else {
            params2.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params2.put("itemId", "XSL0001,PL010110,L01000000");
        //获取当年销售收入
        List<String> currYearFnc2 = fncStatBsMapper.queryItemValueByParams(params2);
        finanIndicAnalyDto2.setItemName("销售收入");
        finanIndicAnalyDto2.setInputYear(rptYear);
        for (String currYearValue : currYearFnc2) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto2.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto2.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params2.put("dataField", "STAT_END_AMT_Y");
        params2.put("rptYear", String.valueOf(lastYear));
        //取去年销售收入
        List<String> lastYearFnc2 = fncStatBsMapper.queryItemValueByParams(params2);
        for (String lastYearValue : lastYearFnc2) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto2.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto2.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年销售收入
        params2.put("dataField", "STAT_END_AMT_Y");
        params2.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc2 = fncStatBsMapper.queryItemValueByParams(params2);
        for (String lastSecondValue : lastSecondYearFnc2) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto2.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto2.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto2);

        //主营业务利润
        Map<String, String> params3 = new HashMap<>();
        params3.put("cusId", cusId);
        params3.put("tableName", "fnc_stat_is");
        params3.put("statStyle", "1");
        params3.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto3 = new FinanIndicAnalyDto();
        if (month == 12) {
            params3.put("dataField", "STAT_END_AMT_Y");
        } else {
            params3.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params3.put("itemId", "L03010000");
        //获取主营业务利润
        List<String> currYearFnc3 = fncStatBsMapper.queryItemValueByParams(params3);
        finanIndicAnalyDto3.setItemName("主营业务利润");
        finanIndicAnalyDto3.setInputYear(rptYear);
        for (String currYearValue : currYearFnc3) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto3.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto3.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params3.put("dataField", "STAT_END_AMT_Y");
        params3.put("rptYear", String.valueOf(lastYear));
        //取去年主营业务利润
        List<String> lastYearFnc3 = fncStatBsMapper.queryItemValueByParams(params3);
        for (String lastYearValue : lastYearFnc3) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto3.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto3.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年主营业务利润
        params3.put("dataField", "STAT_END_AMT_Y");
        params3.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc3 = fncStatBsMapper.queryItemValueByParams(params3);
        for (String lastSecondValue : lastSecondYearFnc3) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto3.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto3.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }

        result.add(finanIndicAnalyDto3);
        //毛利率
        Map<String, String> params4 = new HashMap<>();
        params4.put("cusId", cusId);
        params4.put("tableName", "FNC_INDEX_RPT");
        params4.put("statStyle", "1");
        params4.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto4 = new FinanIndicAnalyDto();
        if (month == 12) {
            params4.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params4.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params4.put("itemId", "XSC0021,C01130000");
        //获取毛利率
        List<String> currYearFnc4 = fncStatBsMapper.queryItemValueByParams(params4);
        finanIndicAnalyDto4.setItemName("毛利率");
        finanIndicAnalyDto4.setInputYear(rptYear);
        for (String currYearValue : currYearFnc4) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto4.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto4.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params4.put("dataField", "STAT_INIT_AMT_Y");
        params4.put("rptYear", String.valueOf(lastYear));
        //取去年毛利率
        List<String> lastYearFnc4 = fncStatBsMapper.queryItemValueByParams(params4);
        for (String lastYearValue : lastYearFnc4) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto4.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto4.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年毛利率
        params4.put("dataField", "STAT_INIT_AMT_Y");
        params4.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc4 = fncStatBsMapper.queryItemValueByParams(params4);
        for (String lastSecondValue : lastSecondYearFnc4) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto4.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto4.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto4);
        //管理费用
        Map<String, String> params5 = new HashMap<>();
        params5.put("cusId", cusId);
        params5.put("tableName", "FNC_STAT_IS");
        params5.put("statStyle", "1");
        params5.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto5 = new FinanIndicAnalyDto();
        if (month == 12) {
            params5.put("dataField", "STAT_END_AMT_Y");
        } else {
            params5.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params5.put("itemId", "XSL0005,L03040000");
        //获取管理费用
        List<String> currYearFnc5 = fncStatBsMapper.queryItemValueByParams(params5);
        finanIndicAnalyDto5.setItemName("管理费用");
        finanIndicAnalyDto5.setInputYear(rptYear);
        for (String currYearValue : currYearFnc5) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto5.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto5.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params5.put("dataField", "STAT_END_AMT_Y");
        params5.put("rptYear", String.valueOf(lastYear));
        //取去年管理费用
        List<String> lastYearFnc5 = fncStatBsMapper.queryItemValueByParams(params5);
        for (String lastYearValue : lastYearFnc5) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto5.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto5.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年管理费用
        params5.put("dataField", "STAT_END_AMT_Y");
        params5.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc5 = fncStatBsMapper.queryItemValueByParams(params5);
        for (String lastSecondValue : lastSecondYearFnc5) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto5.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto5.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto5);
        //营业费用
        Map<String, String> params6 = new HashMap<>();
        params6.put("cusId", cusId);
        params6.put("tableName", "FNC_STAT_IS");
        params6.put("statStyle", "1");
        params6.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto6 = new FinanIndicAnalyDto();
        if (month == 12) {
            params6.put("dataField", "STAT_END_AMT_Y");
        } else {
            params6.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params6.put("itemId", "XSL0004,L03030000");
        //获取营业费用
        List<String> currYearFnc6 = fncStatBsMapper.queryItemValueByParams(params6);
        finanIndicAnalyDto6.setItemName("营业费用");
        finanIndicAnalyDto6.setInputYear(rptYear);
        for (String currYearValue : currYearFnc6) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto6.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto6.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params6.put("dataField", "STAT_END_AMT_Y");
        params6.put("rptYear", String.valueOf(lastYear));
        //取去年营业费用
        List<String> lastYearFnc6 = fncStatBsMapper.queryItemValueByParams(params6);
        for (String lastYearValue : lastYearFnc6) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto6.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto6.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年营业费用
        params6.put("dataField", "STAT_END_AMT_Y");
        params6.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc6 = fncStatBsMapper.queryItemValueByParams(params6);
        for (String lastSecondValue : lastSecondYearFnc6) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto6.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto6.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto6);
        //财务费用
        Map<String, String> params7 = new HashMap<>();
        params7.put("cusId", cusId);
        params7.put("tableName", "FNC_STAT_IS");
        params7.put("statStyle", "1");
        params7.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto7 = new FinanIndicAnalyDto();
        if (month == 12) {
            params7.put("dataField", "STAT_END_AMT_Y");
        } else {
            params7.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params7.put("itemId", "XSL0006,L03050000");
        //获取财务费用
        List<String> currYearFnc7 = fncStatBsMapper.queryItemValueByParams(params7);
        finanIndicAnalyDto7.setItemName("财务费用");
        finanIndicAnalyDto7.setInputYear(rptYear);
        for (String currYearValue : currYearFnc7) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto7.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto7.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params7.put("dataField", "STAT_END_AMT_Y");
        params7.put("rptYear", String.valueOf(lastYear));
        //取去年财务费用
        List<String> lastYearFnc7 = fncStatBsMapper.queryItemValueByParams(params7);
        for (String lastYearValue : lastYearFnc7) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto7.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto7.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年财务费用
        params7.put("dataField", "STAT_END_AMT_Y");
        params7.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc7 = fncStatBsMapper.queryItemValueByParams(params7);
        for (String lastSecondValue : lastSecondYearFnc7) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto7.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto7.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto7);
        //营业利润
        Map<String, String> params8 = new HashMap<>();
        params8.put("cusId", cusId);
        params8.put("tableName", "FNC_STAT_IS");
        params8.put("statStyle", "1");
        params8.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto8 = new FinanIndicAnalyDto();
        if (month == 12) {
            params8.put("dataField", "STAT_END_AMT_Y");
        } else {
            params8.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params8.put("itemId", "XSL0011,L04010000");
        //获取营业利润
        List<String> currYearFnc8 = fncStatBsMapper.queryItemValueByParams(params8);
        finanIndicAnalyDto8.setItemName("营业利润");
        finanIndicAnalyDto8.setInputYear(rptYear);
        for (String currYearValue : currYearFnc8) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto8.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto8.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params8.put("dataField", "STAT_END_AMT_Y");
        params8.put("rptYear", String.valueOf(lastYear));
        //取去年营业利润
        List<String> lastYearFnc8 = fncStatBsMapper.queryItemValueByParams(params8);
        for (String lastYearValue : lastYearFnc8) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto8.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto8.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年营业利润
        params8.put("dataField", "STAT_END_AMT_Y");
        params8.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc8 = fncStatBsMapper.queryItemValueByParams(params8);
        for (String lastSecondValue : lastSecondYearFnc8) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto8.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto8.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto8);
        //投资收益
        Map<String, String> params9 = new HashMap<>();
        params9.put("cusId", cusId);
        params9.put("tableName", "FNC_STAT_IS");
        params9.put("statStyle", "1");
        params9.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto9 = new FinanIndicAnalyDto();
        if (month == 12) {
            params9.put("dataField", "STAT_END_AMT_Y");
        } else {
            params9.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params9.put("itemId", "XSL0009");
        //获取投资收益
        List<String> currYearFnc9 = fncStatBsMapper.queryItemValueByParams(params9);
        finanIndicAnalyDto9.setItemName("投资收益");
        finanIndicAnalyDto9.setInputYear(rptYear);
        for (String currYearValue : currYearFnc9) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto9.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto9.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params9.put("dataField", "STAT_END_AMT_Y");
        params9.put("rptYear", String.valueOf(lastYear));
        //取去年投资收益
        List<String> lastYearFnc9 = fncStatBsMapper.queryItemValueByParams(params9);
        for (String lastYearValue : lastYearFnc9) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto9.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto9.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年投资收益
        params9.put("dataField", "STAT_END_AMT_Y");
        params9.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc9 = fncStatBsMapper.queryItemValueByParams(params9);
        for (String lastSecondValue : lastSecondYearFnc9) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto9.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto9.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto9);
        //所得税
        Map<String, String> params10 = new HashMap<>();
        params10.put("cusId", cusId);
        params10.put("tableName", "FNC_STAT_IS");
        params10.put("statStyle", "1");
        params10.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto10 = new FinanIndicAnalyDto();
        if (month == 12) {
            params10.put("dataField", "STAT_END_AMT_Y");
        } else {
            params10.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params10.put("itemId", "XSL0016,L05020000");
        //获取所得税
        List<String> currYearFnc10 = fncStatBsMapper.queryItemValueByParams(params10);
        finanIndicAnalyDto10.setItemName("所得税");
        finanIndicAnalyDto10.setInputYear(rptYear);
        for (String currYearValue : currYearFnc10) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto10.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto10.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params10.put("dataField", "STAT_END_AMT_Y");
        params10.put("rptYear", String.valueOf(lastYear));
        //取去年所得税
        List<String> lastYearFnc10 = fncStatBsMapper.queryItemValueByParams(params10);
        for (String lastYearValue : lastYearFnc10) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto10.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto10.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年所得税
        params10.put("dataField", "STAT_END_AMT_Y");
        params10.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc10 = fncStatBsMapper.queryItemValueByParams(params10);
        for (String lastSecondValue : lastSecondYearFnc10) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto10.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto10.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto10);
        //实际净利润
        Map<String, String> params11 = new HashMap<>();
        params11.put("cusId", cusId);
        params11.put("tableName", "FNC_STAT_IS");
        params11.put("statStyle", "1");
        params11.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto11 = new FinanIndicAnalyDto();
        if (month == 12) {
            params11.put("dataField", "STAT_END_AMT_Y");
        } else {
            params11.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params11.put("itemId", "XSL0017,L06010000");
        //获取实际净利润
        List<String> currYearFnc11 = fncStatBsMapper.queryItemValueByParams(params11);
        finanIndicAnalyDto11.setItemName("实际净利润");
        finanIndicAnalyDto11.setInputYear(rptYear);
        for (String currYearValue : currYearFnc11) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto11.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto11.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params11.put("dataField", "STAT_END_AMT_Y");
        params11.put("rptYear", String.valueOf(lastYear));
        //取去年实际净利润
        List<String> lastYearFnc11 = fncStatBsMapper.queryItemValueByParams(params11);
        for (String lastYearValue : lastYearFnc11) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto11.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto11.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年实际净利润
        params11.put("dataField", "STAT_END_AMT_Y");
        params11.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc11 = fncStatBsMapper.queryItemValueByParams(params11);
        for (String lastSecondValue : lastSecondYearFnc11) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto11.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto11.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto11);
        //净利率
        Map<String, String> params12 = new HashMap<>();
        params12.put("cusId", cusId);
        params12.put("tableName", "FNC_INDEX_RPT");
        params12.put("statStyle", "1");
        params12.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto12 = new FinanIndicAnalyDto();
        if (month == 12) {
            params12.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params12.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params12.put("itemId", "XSC0030,C05010000");
        //获取净利率
        List<String> currYearFnc12 = fncStatBsMapper.queryItemValueByParams(params12);
        finanIndicAnalyDto12.setItemName("净利率");
        finanIndicAnalyDto12.setInputYear(rptYear);
        for (String currYearValue : currYearFnc12) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto12.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto12.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params12.put("dataField", "STAT_INIT_AMT_Y");
        params12.put("rptYear", String.valueOf(lastYear));
        //取去年净利率
        List<String> lastYearFnc12 = fncStatBsMapper.queryItemValueByParams(params12);
        for (String lastYearValue : lastYearFnc12) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto12.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto12.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年净利率
        params12.put("dataField", "STAT_INIT_AMT_Y");
        params12.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc12 = fncStatBsMapper.queryItemValueByParams(params12);
        for (String lastSecondValue : lastSecondYearFnc12) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto12.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto12.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto12);
        //经营活动现金净流量
        Map<String, String> params13 = new HashMap<>();
        params13.put("cusId", cusId);
        params13.put("tableName", "FNC_STAT_CFS");
        params13.put("statStyle", "1");
        params13.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto13 = new FinanIndicAnalyDto();
        if (month == 12) {
            params13.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params13.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params13.put("itemId", "XSX0011,X01100000");
        //获取经营活动现金净流量
        List<String> currYearFnc13 = fncStatBsMapper.queryItemValueByParams(params13);
        finanIndicAnalyDto13.setItemName("经营活动现金净流量");
        finanIndicAnalyDto13.setInputYear(rptYear);
        for (String currYearValue : currYearFnc13) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto13.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto13.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params13.put("dataField", "STAT_INIT_AMT_Y");
        params13.put("rptYear", String.valueOf(lastYear));
        //取去年经营活动现金净流量
        List<String> lastYearFnc13 = fncStatBsMapper.queryItemValueByParams(params13);
        for (String lastYearValue : lastYearFnc13) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto13.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto13.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年经营活动现金净流量
        params13.put("dataField", "STAT_INIT_AMT_Y");
        params13.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc13 = fncStatBsMapper.queryItemValueByParams(params13);
        for (String lastSecondValue : lastSecondYearFnc13) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto13.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto13.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto13);
        //投资活动现金净流量
        Map<String, String> params14 = new HashMap<>();
        params14.put("cusId", cusId);
        params14.put("tableName", "FNC_STAT_CFS");
        params14.put("statStyle", "1");
        params14.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto14 = new FinanIndicAnalyDto();
        if (month == 12) {
            params14.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params14.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params14.put("itemId", "XSX0024,X02100000");
        //获取投资活动现金净流量
        List<String> currYearFnc14 = fncStatBsMapper.queryItemValueByParams(params14);
        finanIndicAnalyDto14.setItemName("投资活动现金净流量");
        finanIndicAnalyDto14.setInputYear(rptYear);
        for (String currYearValue : currYearFnc14) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto14.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto14.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params14.put("dataField", "STAT_INIT_AMT_Y");
        params14.put("rptYear", String.valueOf(lastYear));
        //取去年投资活动现金净流量
        List<String> lastYearFnc14 = fncStatBsMapper.queryItemValueByParams(params14);
        for (String lastYearValue : lastYearFnc14) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto14.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto14.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年投资活动现金净流量
        params14.put("dataField", "STAT_INIT_AMT_Y");
        params14.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc14 = fncStatBsMapper.queryItemValueByParams(params14);
        for (String lastSecondValue : lastSecondYearFnc14) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto14.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto14.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto14);
        //筹资活动现金净流量
        Map<String, String> params15 = new HashMap<>();
        params15.put("cusId", cusId);
        params15.put("tableName", "FNC_STAT_CFS");
        params15.put("statStyle", "1");
        params15.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto15 = new FinanIndicAnalyDto();
        if (month == 12) {
            params15.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params15.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params15.put("itemId", "XSX0034,X03090000");
        //获取筹资活动现金净流量
        List<String> currYearFnc15 = fncStatBsMapper.queryItemValueByParams(params15);
        finanIndicAnalyDto15.setItemName("筹资活动现金净流量");
        finanIndicAnalyDto15.setInputYear(rptYear);
        for (String currYearValue : currYearFnc15) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto15.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto15.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params15.put("dataField", "STAT_INIT_AMT_Y");
        params15.put("rptYear", String.valueOf(lastYear));
        //取去年筹资活动现金净流量
        List<String> lastYearFnc15 = fncStatBsMapper.queryItemValueByParams(params15);
        for (String lastYearValue : lastYearFnc15) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto15.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto15.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年筹资活动现金净流量
        params15.put("dataField", "STAT_INIT_AMT_Y");
        params15.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc15 = fncStatBsMapper.queryItemValueByParams(params15);
        for (String lastSecondValue : lastSecondYearFnc15) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto15.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto15.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto15);
        //资产负债率
        Map<String, String> params16 = new HashMap<>();
        params16.put("cusId", cusId);
        params16.put("tableName", "FNC_INDEX_RPT");
        params16.put("statStyle", "1");
        params16.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto16 = new FinanIndicAnalyDto();
        if (month == 12) {
            params16.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params16.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params16.put("itemId", "XSC0003,C03010000");
        //获取资产负债率
        List<String> currYearFnc16 = fncStatBsMapper.queryItemValueByParams(params16);
        finanIndicAnalyDto16.setItemName("资产负债率");
        finanIndicAnalyDto16.setInputYear(rptYear);
        for (String currYearValue : currYearFnc16) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto16.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto16.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params16.put("dataField", "STAT_INIT_AMT_Y");
        params16.put("rptYear", String.valueOf(lastYear));
        //取去年资产负债率
        List<String> lastYearFnc16 = fncStatBsMapper.queryItemValueByParams(params16);
        for (String lastYearValue : lastYearFnc16) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto16.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto16.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年资产负债率
        params16.put("dataField", "STAT_INIT_AMT_Y");
        params16.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc16 = fncStatBsMapper.queryItemValueByParams(params16);
        for (String lastSecondValue : lastSecondYearFnc16) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto16.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto16.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto16);
        //流动比率
        Map<String, String> params17 = new HashMap<>();
        params17.put("cusId", cusId);
        params17.put("tableName", "FNC_INDEX_RPT");
        params17.put("statStyle", "1");
        params17.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto17 = new FinanIndicAnalyDto();
        if (month == 12) {
            params17.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params17.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params17.put("itemId", "XSC0005,C04010000");
        //获取流动比率
        List<String> currYearFnc17 = fncStatBsMapper.queryItemValueByParams(params17);
        finanIndicAnalyDto17.setItemName("流动比率");
        finanIndicAnalyDto17.setInputYear(rptYear);
        for (String currYearValue : currYearFnc17) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto17.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto17.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params17.put("dataField", "STAT_INIT_AMT_Y");
        params17.put("rptYear", String.valueOf(lastYear));
        //取去年流动比率
        List<String> lastYearFnc17 = fncStatBsMapper.queryItemValueByParams(params17);
        for (String lastYearValue : lastYearFnc17) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto17.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto17.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年流动比率
        params17.put("dataField", "STAT_INIT_AMT_Y");
        params17.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc17 = fncStatBsMapper.queryItemValueByParams(params17);
        for (String lastSecondValue : lastSecondYearFnc17) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto17.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto17.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto17);
        //速动比率
        Map<String, String> params18 = new HashMap<>();
        params18.put("cusId", cusId);
        params18.put("tableName", "FNC_INDEX_RPT");
        params18.put("statStyle", "1");
        params18.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto18 = new FinanIndicAnalyDto();
        if (month == 12) {
            params18.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params18.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params18.put("itemId", "XSC0006,C04020000");
        //获取速动比率
        List<String> currYearFnc18 = fncStatBsMapper.queryItemValueByParams(params18);
        finanIndicAnalyDto18.setItemName("速动比率");
        finanIndicAnalyDto18.setInputYear(rptYear);
        for (String currYearValue : currYearFnc18) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto18.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto18.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params18.put("dataField", "STAT_INIT_AMT_Y");
        params18.put("rptYear", String.valueOf(lastYear));
        //取去年速动比率
        List<String> lastYearFnc18 = fncStatBsMapper.queryItemValueByParams(params18);
        for (String lastYearValue : lastYearFnc18) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto18.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto18.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年速动比率
        params18.put("dataField", "STAT_INIT_AMT_Y");
        params18.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc18 = fncStatBsMapper.queryItemValueByParams(params18);
        for (String lastSecondValue : lastSecondYearFnc18) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto18.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto18.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto18);
        //营运资金
        Map<String, String> params19 = new HashMap<>();
        params19.put("cusId", cusId);
        params19.put("tableName", "FNC_INDEX_RPT");
        params19.put("statStyle", "1");
        params19.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto19 = new FinanIndicAnalyDto();
        if (month == 12) {
            params19.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params19.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params19.put("itemId", "C04040000");
        //获取营运资金
        List<String> currYearFnc19 = fncStatBsMapper.queryItemValueByParams(params19);
        finanIndicAnalyDto19.setItemName("营运资金");
        finanIndicAnalyDto19.setInputYear(rptYear);
        for (String currYearValue : currYearFnc19) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto19.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto19.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params19.put("dataField", "STAT_INIT_AMT_Y");
        params19.put("rptYear", String.valueOf(lastYear));
        //取去年营运资金
        List<String> lastYearFnc19 = fncStatBsMapper.queryItemValueByParams(params19);
        for (String lastYearValue : lastYearFnc19) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto19.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto19.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年营运资金
        params19.put("dataField", "STAT_INIT_AMT_Y");
        params19.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc19 = fncStatBsMapper.queryItemValueByParams(params19);
        for (String lastSecondValue : lastSecondYearFnc19) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto19.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto19.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto19);
        //应收账款周转率
        Map<String, String> params20 = new HashMap<>();
        params20.put("cusId", cusId);
        params20.put("tableName", "FNC_INDEX_RPT");
        params20.put("statStyle", "1");
        params20.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto20 = new FinanIndicAnalyDto();
        if (month == 12) {
            params20.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params20.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params20.put("itemId", "XSC0013,C02030000");
        //获取应收账款周转率
        List<String> currYearFnc20 = fncStatBsMapper.queryItemValueByParams(params20);
        finanIndicAnalyDto20.setItemName("应收账款周转率");
        finanIndicAnalyDto20.setInputYear(rptYear);
        for (String currYearValue : currYearFnc20) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto20.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto20.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params20.put("dataField", "STAT_INIT_AMT_Y");
        params20.put("rptYear", String.valueOf(lastYear));
        //取去年应收账款周转率
        List<String> lastYearFnc20 = fncStatBsMapper.queryItemValueByParams(params20);
        for (String lastYearValue : lastYearFnc20) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto20.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto20.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年应收账款周转率
        params20.put("dataField", "STAT_INIT_AMT_Y");
        params20.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc20 = fncStatBsMapper.queryItemValueByParams(params20);
        for (String lastSecondValue : lastSecondYearFnc20) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto20.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto20.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto20);
        //存货周转率
        Map<String, String> params21 = new HashMap<>();
        params21.put("cusId", cusId);
        params21.put("tableName", "FNC_INDEX_RPT");
        params21.put("statStyle", "1");
        params21.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto21 = new FinanIndicAnalyDto();
        if (month == 12) {
            params21.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params21.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params21.put("itemId", "XSC0014,C02040000");
        //获取存货周转率
        List<String> currYearFnc21 = fncStatBsMapper.queryItemValueByParams(params21);
        finanIndicAnalyDto21.setItemName("存货周转率");
        finanIndicAnalyDto21.setInputYear(rptYear);
        for (String currYearValue : currYearFnc21) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto21.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto21.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params21.put("dataField", "STAT_INIT_AMT_Y");
        params21.put("rptYear", String.valueOf(lastYear));
        //取去年存货周转率
        List<String> lastYearFnc21 = fncStatBsMapper.queryItemValueByParams(params21);
        for (String lastYearValue : lastYearFnc21) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto21.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto21.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年存货周转率
        params21.put("dataField", "STAT_INIT_AMT_Y");
        params21.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc21 = fncStatBsMapper.queryItemValueByParams(params21);
        for (String lastSecondValue : lastSecondYearFnc21) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto21.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto21.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto21);
        //总资产报酬率
        Map<String, String> params22 = new HashMap<>();
        params22.put("cusId", cusId);
        params22.put("tableName", "FNC_INDEX_RPT");
        params22.put("statStyle", "1");
        params22.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto22 = new FinanIndicAnalyDto();
        if (month == 12) {
            params22.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params22.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params22.put("itemId", "XSC0017,C01100000");
        //获取总资产报酬率
        List<String> currYearFnc22 = fncStatBsMapper.queryItemValueByParams(params22);
        finanIndicAnalyDto22.setItemName("总资产报酬率");
        finanIndicAnalyDto22.setInputYear(rptYear);
        for (String currYearValue : currYearFnc22) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto22.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto22.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params22.put("dataField", "STAT_INIT_AMT_Y");
        params22.put("rptYear", String.valueOf(lastYear));
        //取去年总资产报酬率
        List<String> lastYearFnc22 = fncStatBsMapper.queryItemValueByParams(params22);
        for (String lastYearValue : lastYearFnc22) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto22.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto22.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年总资产报酬率
        params22.put("dataField", "STAT_INIT_AMT_Y");
        params22.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc22 = fncStatBsMapper.queryItemValueByParams(params22);
        for (String lastSecondValue : lastSecondYearFnc22) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto22.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto22.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto22);
        //净资产收益率
        Map<String, String> params23 = new HashMap<>();
        params23.put("cusId", cusId);
        params23.put("tableName", "FNC_INDEX_RPT");
        params23.put("statStyle", "1");
        params23.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto23 = new FinanIndicAnalyDto();
        if (month == 12) {
            params23.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params23.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params23.put("itemId", "XSC0026,C01070000");
        //获取净资产收益率
        List<String> currYearFnc23 = fncStatBsMapper.queryItemValueByParams(params23);
        finanIndicAnalyDto23.setItemName("净资产收益率");
        finanIndicAnalyDto23.setInputYear(rptYear);
        for (String currYearValue : currYearFnc23) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto23.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto23.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params23.put("dataField", "STAT_INIT_AMT_Y");
        params23.put("rptYear", String.valueOf(lastYear));
        //取去年净资产收益率
        List<String> lastYearFnc23 = fncStatBsMapper.queryItemValueByParams(params23);
        for (String lastYearValue : lastYearFnc23) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto23.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto23.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年净资产收益率
        params23.put("dataField", "STAT_INIT_AMT_Y");
        params23.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc23 = fncStatBsMapper.queryItemValueByParams(params23);
        for (String lastSecondValue : lastSecondYearFnc23) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto23.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto23.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto23);
        //营业收入增长率
        Map<String, String> params24 = new HashMap<>();
        params24.put("cusId", cusId);
        params24.put("tableName", "FNC_INDEX_RPT");
        params24.put("statStyle", "1");
        params24.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto24 = new FinanIndicAnalyDto();
        if (month == 12) {
            params24.put("dataField", "STAT_INIT_AMT_Y");
        } else {
            params24.put("dataField", "STAT_INIT_AMT".concat(String.valueOf(month)));
        }
        params24.put("itemId", "XSC0032,C05050000");
        //获取营业收入增长率
        List<String> currYearFnc24 = fncStatBsMapper.queryItemValueByParams(params24);
        finanIndicAnalyDto24.setItemName("营业收入增长率");
        finanIndicAnalyDto24.setInputYear(rptYear);
        for (String currYearValue : currYearFnc24) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto24.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto24.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params24.put("dataField", "STAT_INIT_AMT_Y");
        params24.put("rptYear", String.valueOf(lastYear));
        //取去年营业收入增长率
        List<String> lastYearFnc24 = fncStatBsMapper.queryItemValueByParams(params24);
        for (String lastYearValue : lastYearFnc24) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto24.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto24.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年营业收入增长率
        params24.put("dataField", "STAT_INIT_AMT_Y");
        params24.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc24 = fncStatBsMapper.queryItemValueByParams(params24);
        for (String lastSecondValue : lastSecondYearFnc24) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto24.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto24.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto24);
        Map<String, String> params25 = new HashMap<>();
        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName("RPT_FNC_PERC");
        params25.put("cusId", cusId);
        params25.put("statStyle", "1");
        String percent = adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
        logger.info("获取系统参数中比重配置{}", JSON.toJSONString(percent));
        if (month == 12) {
            params25.put("dataField", "STAT_ITEM_ID as itemId,STAT_END_AMT_Y as data1");
        } else {
            params25.put("dataField", "STAT_ITEM_ID as itemId,STAT_END_AMT".concat(String.valueOf(month)).concat(" as data1"));
        }
        params25.put("tableName", "FNC_STAT_BS");
        params25.put("rptYear", String.valueOf(year));
        params25.put("itemId", "BS010120,XFZ0035,Z01000001,ACC090123,GC0000014");
        List<FncStatRptItemDTO> totalAssetList = fncStatBsMapper.queryItemValueByParams2(params25);
        logger.info("资产负债表字段-资产科目中总资产:", JSON.toJSONString(totalAssetList));
        BigDecimal totalAsset = new BigDecimal(0);
        for (FncStatRptItemDTO fncStatRptItemDTO : totalAssetList) {
            if (new BigDecimal(fncStatRptItemDTO.getData1()) != null && new BigDecimal(fncStatRptItemDTO.getData1()).compareTo(new BigDecimal(0)) >= 0) {
                totalAsset = new BigDecimal(fncStatRptItemDTO.getData1());
                break;
            }
        }
        totalAsset = totalAsset.multiply(new BigDecimal(percent));
        logger.info("资产负债表字段-资产科目中 总资产*比重 之后:" + JSON.toJSONString(totalAsset));
        params25.put("itemId", "Z01030112,ACC090116,Z01030111,ACC090115,GC0000013,XFZ0021,BS010110,Z01010001,ACC090107,GC0000007,XFZ0014,BS010102,BS010103,BS010105,BS010106,BS010107,BS010108,BS010109,XFZ0002,XFZ0003,XFZ0004,XFZ0006,XFZ0007,XFZ0008,XFZ0011,XFZ0012,XFZ0013,XFZ0016,XFZ0017,XFZ0018,XFZ0019,XFZ0020,XFZ0022,XFZ0023,XFZ0025,XFZ0026,XFZ0027,XFZ0028,XFZ0029,XFZ0030,XFZ0031,XFZ0032,Z01010100,Z01010200,Z01010300,Z01010400,Z01010500,Z01010600,Z01010700,Z01010800,Z01010900,Z01011000,Z01011200,Z01011300,Z01011400,Z01011500,Z01011600,Z01020001,Z01030100,Z01030300,Z01040100,Z01040300,Z01050001,ACC090103,ACC090104,ACC090109,ACC090110,ACC090119,ACC090122,GC0000001,GC0000002,GC0000003,GC0000004,GC0000008,GC0000009,GC0000010,GC0000011,GC0000012,Z01011210,Z01011211,Z01011212,Z01030111,Z01030110,ACC090115,ACC090117,Z01010610,ACC090105,GC0000006,XFZ0005,Z01011100,XFZ0009,BS010104");
        if (month == 12) {
            params25.put("dataField", "A.STAT_ITEM_ID as itemId,A.STAT_END_AMT_Y as data1,B.item_name as data2");
        } else {
            params25.put("dataField", "A.STAT_ITEM_ID as itemId,A.STAT_END_AMT".concat(String.valueOf(month)).concat(" as data1,B.item_name as data2"));
        }
        params25.put("tableName", "FNC_STAT_BS A");
        List<FncStatRptItemDTO> willShowItemList = fncStatBsMapper.queryItemValueByParams3(params25);
        for (FncStatRptItemDTO fncStatRptItemDTO : willShowItemList) {
            logger.info("当前正在处理项目编号{[]},项目名称{[]}.", fncStatRptItemDTO.getItemId(), fncStatRptItemDTO.getData2());
            if (BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())) != null &&  BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())).compareTo(totalAsset) > 0) {
                logger.info("当前正在处理的需要展示的当前年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO.getItemId(), fncStatRptItemDTO.getData1());
                FinanIndicAnalyDto finanIndicAnalyDtoShow = new FinanIndicAnalyDto();
                finanIndicAnalyDtoShow.setItemId(fncStatRptItemDTO.getItemId());
                finanIndicAnalyDtoShow.setItemName(fncStatRptItemDTO.getData2());
                if (month == 11 || month == 12) {
                    finanIndicAnalyDtoShow.setInputYear(rptYear);
                } else {
                    finanIndicAnalyDtoShow.setInputYear(rptYear);
                }
                finanIndicAnalyDtoShow.setCurYmValue(new BigDecimal(fncStatRptItemDTO.getData1()));
                params25.put("dataField", "STAT_END_AMT_Y as data1");
                params25.put("rptYear", String.valueOf(lastYear));
                params25.put("itemId", fncStatRptItemDTO.getItemId());
                List<FncStatRptItemDTO> lastYearValueList = fncStatBsMapper.queryItemValueByParams2(params25);
                for (FncStatRptItemDTO fncStatRptItemDTO1 : lastYearValueList) {
                    if (new BigDecimal(fncStatRptItemDTO1.getData1()) != null && new BigDecimal(0).compareTo(new BigDecimal(fncStatRptItemDTO1.getData1())) < 0) {
                        logger.info("当前正在处理的需要展示的上一年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO1.getItemId(), fncStatRptItemDTO1.getData1());
                        finanIndicAnalyDtoShow.setNearFirstValue(new BigDecimal(fncStatRptItemDTO1.getData1()));
                        break;
                    }
                }
                params25.put("rptYear", String.valueOf(lastSecond));
                List<FncStatRptItemDTO> lastTwoYearValueList = fncStatBsMapper.queryItemValueByParams2(params25);
                for (FncStatRptItemDTO fncStatRptItemDTO1 : lastTwoYearValueList) {
                    if (new BigDecimal(fncStatRptItemDTO1.getData1()) != null && new BigDecimal(0).compareTo(new BigDecimal(fncStatRptItemDTO1.getData1())) < 0) {
                        logger.info("当前正在处理的需要展示的前两年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO1.getItemId(), fncStatRptItemDTO1.getData1());
                        finanIndicAnalyDtoShow.setNearSecondValue(new BigDecimal(fncStatRptItemDTO1.getData1()));
                        break;
                    }
                }
                finanIndicAnalyDtoShow.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
                result.add(finanIndicAnalyDtoShow);
            }
        }
        FinanIndicAnalyDto finanIndicAnalyDto26 = new FinanIndicAnalyDto();
        Map<String, String> params26 = new HashMap<>();
        if (month == 12) {
            params26.put("dataField", "STAT_END_AMT_Y");
        } else {
            params26.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params25.put("cusId", cusId);
        params25.put("statStyle", "1");

        //净资产
        Map<String, String> params27 = new HashMap<>();
        params27.put("cusId", cusId);
        params27.put("tableName", "FNC_STAT_BS");
        params27.put("statStyle", "1");
        params27.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto27 = new FinanIndicAnalyDto();
        if (month == 12) {
            params27.put("dataField", "STAT_END_AMT_Y");
        } else {
            params27.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params27.put("itemId", "BS010213");
        //获取净资产
        List<String> currYearFnc27 = fncStatBsMapper.queryItemValueByParams(params27);
        finanIndicAnalyDto27.setItemName("净资产");
        finanIndicAnalyDto27.setInputYear(rptYear);
        for (String currYearValue : currYearFnc27) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto27.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto27.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params27.put("dataField", "STAT_END_AMT_Y");
        params27.put("rptYear", String.valueOf(lastYear));
        //取去年净资产
        List<String> lastYearFnc27 = fncStatBsMapper.queryItemValueByParams(params27);
        for (String lastYearValue : lastYearFnc27) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto27.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto27.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年净资产
        params27.put("dataField", "STAT_END_AMT_Y");
        params27.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc27 = fncStatBsMapper.queryItemValueByParams(params27);
        for (String lastSecondValue : lastSecondYearFnc27) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto27.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto27.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto27);

        logger.info("资产负债表字段-负债科目中占 总负债 比重大于5%的科目(比率可配置) 处理-----------start--------------");
        Map<String, String> params28 = new HashMap<>();
        if (month == 12) {
            params28.put("dataField", "STAT_ITEM_ID as itemId,STAT_END_AMT_Y as data1");
        } else {
            params28.put("dataField", "STAT_ITEM_ID as itemId,STAT_END_AMT".concat(String.valueOf(month)).concat(" as data1"));
        }
        params28.put("cusId", cusId);
        params28.put("statStyle", "1");
        params28.put("tableName", "FNC_STAT_BS");
        params28.put("rptYear", String.valueOf(year));
        params28.put("itemId", "BS010221,XFZ0060,Z02000001,ACC090213,GC0000015");
        List<FncStatRptItemDTO> totalLiabilityList = fncStatBsMapper.queryItemValueByParams2(params28);
        logger.info("资产负债表字段-资产科目中总负债:", JSON.toJSONString(totalAssetList));
        BigDecimal totalLiability = new BigDecimal(0);
        for (FncStatRptItemDTO fncStatRptItemDTO : totalLiabilityList) {
            if (new BigDecimal(fncStatRptItemDTO.getData1()) != null && new BigDecimal(fncStatRptItemDTO.getData1()).compareTo(new BigDecimal(0)) >= 0) {
                totalLiability = new BigDecimal(fncStatRptItemDTO.getData1());
                break;
            }
        }
        totalLiability = totalLiability.multiply(new BigDecimal(percent));
        logger.info("资产负债表字段-资产科目中 总负债*比重 之后:" + JSON.toJSONString(totalLiability));
        params28.put("itemId", "Z02020001,ACC090212,Z02010001,ACC090207,XFZ0050,BS0102011,BS010202,BS010203,BS010204,XFZ0037,XFZ0038,XFZ0039,XFZ0041,XFZ0042,XFZ0043,XFZ0044,XFZ0045,XFZ0047,XFZ0048,XFZ0049,XFZ0052,XFZ0053,XFZ0054,XFZ0055,XFZ0056,XFZ0057,XFZ0058,Z02010100,Z02010200,Z02010400,Z02010500,Z02010600,Z02010700,Z02010800,Z02010900,Z02011100,Z02011200,Z02011300,Z02011400,Z02020100,Z02020200,Z02020300,Z02020400,Z02020500,ACC090203,ACC090205,ACC090206,ACC090209,ACC090210,ACC090211,GC0000016,B31211133,B31231222,Z04010000,Z04020000,XFZ0063,Z04030000,XFZ0065,Z04050000,XFZ0066,Z02010001,ACC090207,XFZ0050,Z02010300,XFZ0040,Z02011000,XFZ0046,BS010205");
        if (month == 12) {
            params28.put("dataField", "A.STAT_ITEM_ID as itemId,A.STAT_END_AMT_Y as data1,B.item_name as data2");
        } else {
            params28.put("dataField", "A.STAT_ITEM_ID as itemId,A.STAT_END_AMT".concat(String.valueOf(month)).concat(" as data1,B.item_name as data2"));
        }
        params28.put("tableName", "FNC_STAT_BS A");
        List<FncStatRptItemDTO> willShowItemList1 = fncStatBsMapper.queryItemValueByParams3(params28);
        for (FncStatRptItemDTO fncStatRptItemDTO : willShowItemList1) {
            logger.info("当前正在处理项目编号{[]},项目名称{[]}.", fncStatRptItemDTO.getItemId(), fncStatRptItemDTO.getData2());
            if (new BigDecimal(fncStatRptItemDTO.getData1()) != null &&  BigDecimal.valueOf(Double.parseDouble(fncStatRptItemDTO.getData1())).compareTo(totalAsset) > 0) {
                logger.info("当前正在处理的需要展示的当前年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO.getItemId(), fncStatRptItemDTO.getData1());
                FinanIndicAnalyDto finanIndicAnalyDtoShow = new FinanIndicAnalyDto();
                finanIndicAnalyDtoShow.setItemId(fncStatRptItemDTO.getItemId());
                finanIndicAnalyDtoShow.setItemName(fncStatRptItemDTO.getData2());
                if (month == 11 || month == 12) {
                    finanIndicAnalyDtoShow.setInputYear(rptYear);
                } else {
                    finanIndicAnalyDtoShow.setInputYear(rptYear);
                }
                finanIndicAnalyDtoShow.setCurYmValue(new BigDecimal(fncStatRptItemDTO.getData1()));
                params28.put("dataField", "STAT_END_AMT_Y  as data1");
                params28.put("rptYear", String.valueOf(year - 1));
                params28.put("itemId", fncStatRptItemDTO.getItemId());
                List<FncStatRptItemDTO> lastTwoYearValueList = fncStatBsMapper.queryItemValueByParams2(params28);
                for (FncStatRptItemDTO fncStatRptItemDTO1 : lastTwoYearValueList) {
                    if (new BigDecimal(fncStatRptItemDTO1.getData1()) != null && new BigDecimal(0).compareTo(new BigDecimal(fncStatRptItemDTO1.getData1())) < 0) {
                        logger.info("当前正在处理的需要展示的上一年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO1.getItemId(), fncStatRptItemDTO1.getData1());
                        finanIndicAnalyDtoShow.setNearFirstValue(new BigDecimal(fncStatRptItemDTO1.getData1()));
                        break;
                    }
                }
                params28.put("rptYear", String.valueOf(year - 2));
                List<FncStatRptItemDTO> lastTwoYearValueList1 = fncStatBsMapper.queryItemValueByParams2(params28);
                for (FncStatRptItemDTO fncStatRptItemDTO1 : lastTwoYearValueList1) {
                    if (new BigDecimal(fncStatRptItemDTO1.getData1()) != null && new BigDecimal(0).compareTo(new BigDecimal(fncStatRptItemDTO1.getData1())) < 0) {
                        logger.info("当前正在处理的需要展示的前两年项目编号{[]},项目对应数值{[]}.", fncStatRptItemDTO1.getItemId(), fncStatRptItemDTO1.getData1());
                        finanIndicAnalyDtoShow.setNearSecondValue(new BigDecimal(fncStatRptItemDTO1.getData1()));
                        break;
                    }
                }
                finanIndicAnalyDtoShow.setFinanIndicGroup(CmisCommonConstants.STD_SX_FINAN_INDIC_GROUP_02);
                result.add(finanIndicAnalyDtoShow);
            }
        }
        //资产总计
        Map<String, String> params30 = new HashMap<>();
        params30.put("cusId", cusId);
        params30.put("tableName", "FNC_STAT_BS");
        params30.put("statStyle", "1");
        params30.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto30 = new FinanIndicAnalyDto();
        if (month == 12) {
            params30.put("dataField", "STAT_END_AMT_Y");
        } else {
            params30.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params30.put("itemId", "Z01000001,ACC090123,GC0000014,XFZ0035,BS010120");
        //获取资产总计
        List<String> currYearFnc30 = fncStatBsMapper.queryItemValueByParams(params30);
        finanIndicAnalyDto30.setItemName("资产总计");
        finanIndicAnalyDto30.setInputYear(rptYear);
        for (String currYearValue : currYearFnc30) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto30.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto30.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params30.put("dataField", "STAT_END_AMT_Y");
        params30.put("rptYear", String.valueOf(lastYear));
        //取去年资产总计
        List<String> lastYearFnc30 = fncStatBsMapper.queryItemValueByParams(params30);
        for (String lastYearValue : lastYearFnc30) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto30.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto30.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年资产总计
        params30.put("dataField", "STAT_END_AMT_Y");
        params30.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc30 = fncStatBsMapper.queryItemValueByParams(params30);
        for (String lastSecondValue : lastSecondYearFnc30) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto30.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto30.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto30);
        //负债总额
        Map<String, String> params31 = new HashMap<>();
        params31.put("cusId", cusId);
        params31.put("tableName", "FNC_STAT_BS");
        params31.put("statStyle", "1");
        params31.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto31 = new FinanIndicAnalyDto();
        if (month == 12) {
            params31.put("dataField", "STAT_END_AMT_Y");
        } else {
            params31.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params31.put("itemId", "Z02000001,ACC090213,GC0000015,XFZ0060,BS010221");
        //获取负债总额
        List<String> currYearFnc31 = fncStatBsMapper.queryItemValueByParams(params31);
        finanIndicAnalyDto31.setItemName("负债总额");
        finanIndicAnalyDto31.setInputYear(rptYear);
        for (String currYearValue : currYearFnc31) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto31.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto31.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params31.put("dataField", "STAT_END_AMT_Y");
        params31.put("rptYear", String.valueOf(lastYear));
        //取去年负债总额
        List<String> lastYearFnc31 = fncStatBsMapper.queryItemValueByParams(params31);
        for (String lastYearValue : lastYearFnc31) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto31.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto31.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年负债总额
        params31.put("dataField", "STAT_END_AMT_Y");
        params31.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc31 = fncStatBsMapper.queryItemValueByParams(params31);
        for (String lastSecondValue : lastSecondYearFnc31) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto31.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto31.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto31);
        //销售收入
        Map<String, String> params32 = new HashMap<>();
        params32.put("cusId", cusId);
        params32.put("tableName", "FNC_STAT_BS");
        params32.put("statStyle", "1");
        params32.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto32 = new FinanIndicAnalyDto();
        if (month == 12) {
            params32.put("dataField", "STAT_END_AMT_Y");
        } else {
            params32.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params32.put("itemId", "GC0000030");
        //获取销售收入
        List<String> currYearFnc32 = fncStatBsMapper.queryItemValueByParams(params32);
        finanIndicAnalyDto32.setItemName("年度销售收入");
        finanIndicAnalyDto32.setInputYear(rptYear);
        for (String currYearValue : currYearFnc32) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto32.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto32.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params32.put("dataField", "STAT_END_AMT_Y");
        params32.put("rptYear", String.valueOf(lastYear));
        //取去年销售收入
        List<String> lastYearFnc32 = fncStatBsMapper.queryItemValueByParams(params32);
        for (String lastYearValue : lastYearFnc32) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto32.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto32.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年销售收入
        params32.put("dataField", "STAT_END_AMT_Y");
        params32.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc32 = fncStatBsMapper.queryItemValueByParams(params32);
        for (String lastSecondValue : lastSecondYearFnc32) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto32.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto32.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto32);
        // 年度利润
        Map<String, String> params33 = new HashMap<>();
        params33.put("cusId", cusId);
        params33.put("tableName", "FNC_STAT_BS");
        params33.put("statStyle", "1");
        params33.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto33 = new FinanIndicAnalyDto();
        if (month == 12) {
            params33.put("dataField", "STAT_END_AMT_Y");
        } else {
            params33.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params33.put("itemId", "GC0000032");
        //获取年度利润
        List<String> currYearFnc33 = fncStatBsMapper.queryItemValueByParams(params33);
        finanIndicAnalyDto33.setItemName("年度利润");
        finanIndicAnalyDto33.setInputYear(rptYear);
        for (String currYearValue : currYearFnc33) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto33.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto33.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params33.put("dataField", "STAT_END_AMT_Y");
        params33.put("rptYear", String.valueOf(lastYear));
        //取去年年度利润
        List<String> lastYearFnc33 = fncStatBsMapper.queryItemValueByParams(params33);
        for (String lastYearValue : lastYearFnc33) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto33.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto33.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年年度利润
        params33.put("dataField", "STAT_END_AMT_Y");
        params33.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc33 = fncStatBsMapper.queryItemValueByParams(params33);
        for (String lastSecondValue : lastSecondYearFnc33) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto33.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto33.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto33);
        //所有者权益
        Map<String, String> params29 = new HashMap<>();
        params29.put("cusId", cusId);
        params29.put("tableName", "FNC_STAT_BS");
        params29.put("statStyle", "1");
        params29.put("rptYear", String.valueOf(year));
        FinanIndicAnalyDto finanIndicAnalyDto29 = new FinanIndicAnalyDto();
        if (month == 12) {
            params29.put("dataField", "STAT_END_AMT_Y");
        } else {
            params29.put("dataField", "STAT_END_AMT".concat(String.valueOf(month)));
        }
        params29.put("itemId", "Z04000001,ACC090218,GC0000025,XFZ0069");
        //获取所有者权益
        List<String> currYearFnc29 = fncStatBsMapper.queryItemValueByParams(params29);
        finanIndicAnalyDto29.setItemName("所有者权益");
        finanIndicAnalyDto29.setInputYear(rptYear);
        for (String currYearValue : currYearFnc29) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto29.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto29.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        params29.put("dataField", "STAT_END_AMT_Y");
        params29.put("rptYear", String.valueOf(lastYear));
        //取去年所有者权益
        List<String> lastYearFnc29 = fncStatBsMapper.queryItemValueByParams(params29);
        for (String lastYearValue : lastYearFnc29) {
            if ("0".equals(lastYearValue) || "".equals(lastYearValue) || lastYearValue == null) {
                finanIndicAnalyDto29.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto29.setNearFirstValue(BigDecimal.valueOf(Double.parseDouble(lastYearValue)));
                break;
            }
        }
        //取前一年所有者权益
        params29.put("dataField", "STAT_END_AMT_Y");
        params29.put("rptYear", String.valueOf(lastSecond));
        List<String> lastSecondYearFnc29 = fncStatBsMapper.queryItemValueByParams(params29);
        for (String lastSecondValue : lastSecondYearFnc29) {
            if ("0".equals(lastSecondValue) || "".equals(lastSecondValue) || lastSecondValue == null) {
                finanIndicAnalyDto29.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto29.setNearSecondValue(BigDecimal.valueOf(Double.parseDouble(lastSecondValue)));
                break;
            }
        }
        result.add(finanIndicAnalyDto29);
        return result;
    }

    /**
     * 销售收入、销售成本、应收账款、预收账款、存货、预付账款、应付账款
     *
     * @param cusId
     * @return
     */
    public Map<String, FinanIndicAnalyDto> getRptFncTotalProfitForLmtHighCurfundEval(String cusId) {
        Map<String, FinanIndicAnalyDto> resultMap = new HashMap<>();
        String rptYear = fncStatBaseMapper.getNewRptYear(cusId);
        logger.info("当前客户最新一期{["+rptYear+"]}财报信息");
        int year = Integer.parseInt(rptYear.substring(0, 4));
        int month = Integer.parseInt(rptYear.substring(4));
        // 获取上年度销售收入
        resultMap.put("salesIncome", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_is", "XSL0001,PL010110,L01000000", "销售收入","STAT_END_AMT_Y"));
        // 销售利润率
        resultMap.put("saleProfitRate", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_index_rpt", "C01010000,XSC0022", "销售利润率","STAT_INIT_AMT_Y"));
        // 获取销售成本
        resultMap.put("salesCost", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_is", "XSL0002,PL010211,L02020000", "销售成本","STAT_END_AMT_Y"));
        // 应收账款年初数
        resultMap.put("rcvTurnovYearBegin", getFinanIndicAnalyDto(cusId, rptYear, year - 1, 12, "fnc_stat_bs", "Z01010610,ACC090105,XFZ0005,GC0000006", "应收账款年初数","STAT_INIT_AMT_Y"));
        // 应收账款年末数
        resultMap.put("rcvTurnovYearEnd", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_bs", "Z01010610,ACC090105,XFZ0005,GC0000006", "应收账款年末数","STAT_END_AMT_Y"));
        // 预收账款年初数 Z02010400 XFZ0041
        resultMap.put("ppmTurnoYearBegin", getFinanIndicAnalyDto(cusId, rptYear, year - 1, 12, "fnc_stat_bs", "Z02010400,XFZ0041", "预收账款年初数","STAT_INIT_AMT_Y"));
        // 预收账款年末数
        resultMap.put("ppmTurnoYearEnd", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_bs", "Z02010400,XFZ0041", "预收账款年末数","STAT_END_AMT_Y"));
        // 存货年初数 Z01011210 ACC090106 GC0000005 XFZ0010
        resultMap.put("ivtTurnovYearBegin", getFinanIndicAnalyDto(cusId, rptYear, year - 1, 12, "fnc_stat_bs", "Z01011210,ACC090106,XFZ0010,GC0000005", "存货年初数","STAT_INIT_AMT_Y"));
        // 存货年末数
        resultMap.put("ivtTurnovYearEnd", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_bs", "Z01011210,ACC090106,XFZ0010,GC0000005", "存货年末数","STAT_END_AMT_Y"));
        // 预付账款年初数 Z01010700 XFZ0006
        resultMap.put("advanceTurnovYearBegin", getFinanIndicAnalyDto(cusId, rptYear, year - 1, 12, "fnc_stat_bs", "Z01010700,XFZ0006", "预付账款年初数","STAT_INIT_AMT_Y"));
        // 预付账款年末数
        resultMap.put("advanceTurnovYearEnd", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_bs", "Z01010700,XFZ0006", "预付账款年末数","STAT_END_AMT_Y"));
        // 应付账款年初数 XFZ0040 GC0000021 ACC090204 Z02010300
        resultMap.put("acpTurnovYearBegin", getFinanIndicAnalyDto(cusId, rptYear, year - 1, 12, "fnc_stat_bs", "Z02010300,ACC090204,XFZ0040,GC0000021", "应付账款年初数","STAT_INIT_AMT_Y"));
        // 应付账款年末数
        resultMap.put("acpTurnovYearEnd", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_bs", "Z02010300,ACC090204,XFZ0040,GC0000021", "应付账款年末数","STAT_END_AMT_Y"));
        // 借款人自有资金 = 流动资产 - 流动负债
        try {
            FinanIndicAnalyDto flowAsset = getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_bs", "Z01010001,ACC090107,XFZ0014", "流动资产合计","STAT_END_AMT_Y");
            FinanIndicAnalyDto flowLiability = getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_bs", "Z02010001,ACC090207,XFZ0050", "流动负债合计","STAT_END_AMT_Y");
            FinanIndicAnalyDto loanerSelfAmtDto = new FinanIndicAnalyDto();
            loanerSelfAmtDto.setItemId("JKRZYZJ01");
            loanerSelfAmtDto.setItemName("借款人自有资金");
            loanerSelfAmtDto.setInputYear(String.valueOf(year).concat(String.valueOf(month)));
            loanerSelfAmtDto.setCurYmValue(flowAsset.getCurYmValue().subtract(flowLiability.getCurYmValue()));
            resultMap.put("loanerSelfAmt",loanerSelfAmtDto);
        }catch (Exception e){
            logger.info("借款人自有资金获取错误!");
        }
        return resultMap;
    }

    /**
     * 财报科目净利润连续两年取值，上一年度或当期资产负债表的资产负债率取值
     * @创建者：zhangliang15
     * @param cusId
     * @return
     */
    public Map<String, FinanIndicAnalyDto> getFinRepRetProAndRatOfLia(String cusId) {
        Map<String, FinanIndicAnalyDto> resultMap = new HashMap<>();
        String rptYear = fncStatBaseMapper.getNewRptYear(cusId);
        logger.info("当前客户最新一期{["+rptYear+"]}财报信息");
        int year = Integer.parseInt(rptYear.substring(0, 4));
        int month = Integer.parseInt(rptYear.substring(4));
        String monthItem = String.valueOf(month);
        if (month == 12) {
            monthItem = "_Y";
        }
        // 获取今年度净利润
        resultMap.put("retainedProfits", getFinanIndicAnalyDto(cusId, rptYear, year, month, "fnc_stat_is", "XSL0017,L06010000", "今年度净利润","STAT_END_AMT".concat(monthItem)));
        // 获取上年度净利润
        resultMap.put("lastYearRetainedProfits", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_is", "XSL0017,L06010000", "去年度净利润","STAT_END_AMT_Y"));
        // 获取前年度净利润
        resultMap.put("lastBeforeYearRetainedProfits", getFinanIndicAnalyDto(cusId, rptYear, year-2, 12, "fnc_stat_is", "XSL0017,L06010000", "前年度净利润","STAT_END_AMT_Y"));
        // 获取今年资产负债率取值
        resultMap.put("saleProfitRate", getFinanIndicAnalyDto(cusId, rptYear, year, month, "fnc_index_rpt", "XSC0003,C03010000", "今年资产负债率","STAT_INIT_AMT".concat(monthItem)));
        // 获取上年资产负债率取值
        resultMap.put("lastYearSaleProfitRate", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_index_rpt", "XSC0003,C03010000", "去年资产负债率","STAT_INIT_AMT_Y"));
        // 获取上年度销售收入
        resultMap.put("lastYearSalesIncome", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_is", "XSL0001,PL010110,L01000000", "上年度销售收入","STAT_END_AMT_Y"));
        // 获取当期销售收入,当期财务报表中损益表的主营业务收入
        resultMap.put("salesIncome", getFinanIndicAnalyDto(cusId, rptYear, year, month, "fnc_stat_is", "XSL0001,PL010110,L01000000", "当期销售收入","STAT_END_AMT".concat(monthItem)));
        // 期末所有者权益合计
        resultMap.put("statEndAmt", getFinanIndicAnalyDto(cusId, rptYear, year, month, "fnc_stat_bs", "Z04000001,ACC090218,GC0000025,XFZ0069", "期末所有者权益","STAT_END_AMT".concat(monthItem)));
        // 期初所有者权益合计
        resultMap.put("statInitAmt", getFinanIndicAnalyDto(cusId, rptYear, year, month, "fnc_stat_bs", "Z04000001,ACC090218,GC0000025,XFZ0069", "期初所有者权益","STAT_INIT_AMT".concat(monthItem)));
        // 获取当期财务报表中损益表的主营业务收入
        //resultMap.put("salesIncome", getFinanIndicAnalyDto(cusId, rptYear, year, month, "fnc_stat_is", "XSL0001,L01000000", "当期主营业务收入","STAT_END_AMT".concat(monthItem)));
        // 获取上年财务报表中损益表的主营业务收入
        //resultMap.put("lastYearsalesIncome", getFinanIndicAnalyDto(cusId, rptYear, year-1, 12, "fnc_stat_is", "XSL0001,L01000000", "去年主营业务收入","STAT_INIT_AMT_Y"));
        return resultMap;
    }

    /**
     * @param cusId
     * @param rptYear
     * @param year
     * @param month
     * @param itemName
     * @return
     */
    public FinanIndicAnalyDto getFinanIndicAnalyDto(String cusId, String rptYear, Integer year, Integer month, String tableName, String itemId, String itemName,String dataField) {
        FinanIndicAnalyDto finanIndicAnalyDto = new FinanIndicAnalyDto();
        // 设置项目名称
        finanIndicAnalyDto.setItemName(itemName);
        // 录入年月
        finanIndicAnalyDto.setInputYear(rptYear);
        Map<String, String> params2 = new HashMap<>();
        params2.put("cusId", cusId);
        params2.put("tableName", tableName);
        params2.put("statStyle", "1");
        params2.put("rptYear", String.valueOf(year));
        // 正常判断当前年的查询字段
        params2.put("dataField", dataField);
        params2.put("itemId", itemId);
        List<String> currYearFnc2 = fncStatBsMapper.queryItemValueByParams(params2);
        for (String currYearValue : currYearFnc2) {
            if ("0".equals(currYearValue) || "".equals(currYearValue) || currYearValue == null) {
                finanIndicAnalyDto.setCurYmValue(new BigDecimal(0));
                continue;
            } else {
                finanIndicAnalyDto.setCurYmValue(BigDecimal.valueOf(Double.parseDouble(currYearValue)));
                break;
            }
        }
        return finanIndicAnalyDto;
    }
}
