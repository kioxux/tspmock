package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusManaTask;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10ReqDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import cn.com.yusys.yusp.util.PUBUtilTools;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * KHGL01--客户信息录入集中作业流程（含创建、维护、转正）申请
 * --公司正式客户创建（集中作业）
 * --公司正式客户客观信息维护（集中作业）
 * --公司客户转正（集中作业）
 * --公司客户财务报表新增（集中作业）
 * --公司客户财务报表修改-非年度（集中作业）
 * --个人客户财务报表新增（集中作业）
 * --个人客户财务报表维护（集中作业）
 *
 * @author liucheng
 */
@Service
public class KHGL01BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(KHGL01BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CusManaTaskService cusManaTaskService;

    @Autowired
    private CusCorpService cusCorpService;

    @Autowired
    private CusBaseService cusBaseService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;
    @Autowired
    private Dscms2IrsClientService dscms2IrsClientService;

    @Autowired
    private CmisBizClientService cmisBizClientService;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageSendService messageSendService;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String flowBizType = resultInstanceDto.getBizType();
        log.info("后业务处理类型{}", currentOpType);
        try {
            CusManaTask cusManaTask = cusManaTaskService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程发起操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                cusManaTask.setTaskStatus("2");
                cusManaTaskService.update(cusManaTask);
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程提交操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                cusManaTask.setTaskStatus("2");
                cusManaTaskService.update(cusManaTask);
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程跳转操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                // 1、更新base表
                CusBase record = new CusBase();
                record.setCusId(cusManaTask.getCusId());
                record.setCusState("2"); //生效
                if ("KH005".equals(flowBizType)) { // 公司客户转正（集中作业） 更新公司用户数据及任务表状态
                    record.setCusRankCls("01"); // 正式客户
                }
                /**     KH001 公司正式客户创建（集中作业）	KHGL01
                        KH002 公司正式客户客观信息维护（集中作业）
                        KH003 公司客户财务报表新增（集中作业）
                        KH004 公司客户财务报表修改（集中作业）
                        KH023 公司客户（年报）财务报表修改（集中作业）	KHGL02**/
                //  调非零内评同步用户数据
                if ("KH001".equals(flowBizType) || "KH002".equals(flowBizType) || "KH003".equals(flowBizType) || "KH004".equals(flowBizType) || "KH005".equals(flowBizType)) {
                    Xirs10ReqDto reqDto = new Xirs10ReqDto();
                    CusBase cusBase = cusBaseService.selectByPrimaryKey(cusManaTask.getCusId());
                    CusCorp cusCorp = cusCorpService.selectByPrimaryKey(cusManaTask.getCusId());
                    reqDto.setCustid(cusBase.getCusId());
                    reqDto.setCustname(cusBase.getCusName());
                    reqDto.setCertid(cusBase.getCertCode());
                    reqDto.setCerttype(PUBUtilTools.changeCertTypeFromEcif(cusBase.getCertType()));
                    reqDto.setCusttype(cusCorp.getCusType());
                    reqDto.setIndustrycode(cusCorp.getTradeClass());
                    reqDto.setInputorgid(cusBase.getManagerBrId());
                    reqDto.setInputuserid(cusBase.getManagerId());
                    dscms2IrsClientService.xirs10(reqDto);
                    log.info("A06类型，开始同步ecif系统：{}", cusManaTask.getCusId());
                    cusCorpService.updateEcifInfo(cusManaTask.getCusId());
                }
                cusBaseService.updateSelective(record);
                // 2、更新任务表
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                cusManaTask.setTaskStatus("3");
                cusManaTaskService.update(cusManaTask);

                //3、新增临时档案任务
                //判断是否是异步机构
                ResultDto<AdminSmOrgDto> resultDto = adminSmOrgService.getByOrgCode(cusManaTask.getInputBrId());
                AdminSmOrgDto adminSmOrgDto = resultDto.getData();
                if ("1".equals(adminSmOrgDto.getOrgType()) || "2".equals(adminSmOrgDto.getOrgType()) || "3".equals(adminSmOrgDto.getOrgType())) {
                    log.info("异地机构无法生成档案池任务");
                } else {
                    CentralFileTaskDto centralFileTaskdto = new CentralFileTaskDto();
                    centralFileTaskdto.setSerno(cusManaTask.getSerno());
                    centralFileTaskdto.setCusId(cusManaTask.getCusId());
                    centralFileTaskdto.setCusName(cusManaTask.getCusName());
                    centralFileTaskdto.setBizType(flowBizType); // 客户资料
                    centralFileTaskdto.setInstanceId(resultInstanceDto.getInstanceId());
                    centralFileTaskdto.setNodeId(resultInstanceDto.getNextNodeInfos().get(0).getNextNodeId());
                    centralFileTaskdto.setInputId(cusManaTask.getInputId());
                    centralFileTaskdto.setInputBrId(cusManaTask.getInputBrId());
                    centralFileTaskdto.setOptType("01"); // 纯指令
                    centralFileTaskdto.setTaskType("01"); // 档案接收
                    centralFileTaskdto.setTaskUrgentFlag("9"); // 不加急
                    cmisBizClientService.createCentralFileTask(centralFileTaskdto);
                }

                // 4.1、公司客户转正（集中作业）KH005 或 公司正式客户创建（集中作业）KH001  新增档案归档任务
                // 4.2、公司客户维护KH002、KH003、KH004/个人客户维护KH006 新增档案归档任务
                if ("KH001".equals(flowBizType) || "KH002".equals(flowBizType) || "KH003".equals(flowBizType) || "KH004".equals(flowBizType)
                        || "KH005".equals(flowBizType) || "KH006".equals(flowBizType)) {
                    // CusCorp cusCorp = cusCorpService.selectByPrimaryKey(cusManaTask.getCusId());
                    CusBase cusBase = cusBaseService.selectByPrimaryKey(cusManaTask.getCusId());
                    DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                    docArchiveClientDto.setArchiveMode("02");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
                    docArchiveClientDto.setDocClass("01");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                    if ("KH006".equals(flowBizType)) {
                        docArchiveClientDto.setDocType("01");//01:个人客户资料
                    } else {
                        docArchiveClientDto.setDocType("02");//02:对公客户资料
                    }
                    docArchiveClientDto.setBizSerno(serno);
                    docArchiveClientDto.setCusId(cusBase.getCusId());
                    docArchiveClientDto.setCusName(cusBase.getCusName());
                    docArchiveClientDto.setCertType(cusBase.getCertType());
                    docArchiveClientDto.setCertCode(cusBase.getCertCode());
                    docArchiveClientDto.setManagerId(cusManaTask.getInputId());
                    docArchiveClientDto.setManagerBrId(cusManaTask.getInputBrId());
                    docArchiveClientDto.setInputId(cusManaTask.getInputId());
                    docArchiveClientDto.setInputBrId(cusManaTask.getInputBrId());
                    ResultDto<Integer> docArchiveBySys = cmisBizClientService.createDocArchiveBySys(docArchiveClientDto);
                    if (!docArchiveBySys.getCode().equals("0")) {
                        log.info("客户信息录入集中作业流程申请【{}】系统生成档案归档信息失败{}", serno, docArchiveBySys.getMessage());
                    }
                }
                String flowName = "";
                switch (flowBizType){
                    case "KH001": flowName = "公司正式客户创建（集中作业）";break;
                    case "KH002": flowName = "公司正式客户客观信息维护（集中作业）";break;
                    case "KH003": flowName = "公司客户财务报表新增（集中作业）";break;
                    case "KH004": flowName = "公司客户财务报表修改（集中作业）";break;
                    case "KH005": flowName = "公司客户转正（集中作业）";break;
                    case "KH006": flowName = "个人客户财务报表新增（集中作业）";break;
                    case "KH007": flowName = "个人客户财务报表维护（集中作业）";break;
                    default: flowName = "";
                }
                if(StringUtils.isNotEmpty(flowName)){
                    CusBase cusBase = cusBaseService.selectByPrimaryKey(cusManaTask.getCusId());
                    MessageSendDto messageSendDto = new MessageSendDto();
                    Map<String, String> map = new HashMap<>();
                    map.put("cusId", cusBase.getCusName());
                    map.put("flowName", flowName);
                    map.put("result", "通过");
                    List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                    ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                    receivedUserDto.setReceivedUserType("1");// 发送人员类型
                    receivedUserDto.setUserId(cusBase.getManagerId());// 客户经理工号
                    receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                    receivedUserList.add(receivedUserDto);
                    messageSendDto.setMessageType("MSG_KH_M_0003");
                    messageSendDto.setParams(map);
                    messageSendDto.setReceivedUserList(receivedUserList);
                    messageSendService.sendMessage(messageSendDto);
                }

                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程结束操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                String flowName = "";
                switch (flowBizType){
                    case "KH001": flowName = "公司正式客户创建（集中作业）";break;
                    case "KH002": flowName = "公司正式客户客观信息维护（集中作业）";break;
                    case "KH003": flowName = "公司客户财务报表新增（集中作业）";break;
                    case "KH004": flowName = "公司客户财务报表修改（集中作业）";break;
                    case "KH005": flowName = "公司客户转正（集中作业）";break;
                    case "KH006": flowName = "个人客户财务报表新增（集中作业）";break;
                    case "KH007": flowName = "个人客户财务报表维护（集中作业）";break;
                    default: flowName = "";
                }
                if(StringUtils.isNotEmpty(flowName)) {
                    CusBase cusBase = cusBaseService.selectByPrimaryKey(cusManaTask.getCusId());
                    MessageSendDto messageSendDto = new MessageSendDto();
                    Map<String, String> map = new HashMap<>();
                    map.put("cusId", cusBase.getCusName());
                    map.put("flowName", flowName);
                    map.put("result", "退回");
                    List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                    ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                    receivedUserDto.setReceivedUserType("1");// 发送人员类型
                    receivedUserDto.setUserId(cusBase.getManagerId());// 客户经理工号
                    receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                    receivedUserList.add(receivedUserDto);
                    messageSendDto.setMessageType("MSG_KH_M_0003");
                    messageSendDto.setParams(map);
                    messageSendDto.setReceivedUserList(receivedUserList);
                    messageSendService.sendMessage(messageSendDto);
                }
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程退回操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 判断是否打回发起人节点
                if (BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    cusManaTaskService.update(cusManaTask);
                }
                String flowName = "";
                switch (flowBizType){
                    case "KH001": flowName = "公司正式客户创建（集中作业）";break;
                    case "KH002": flowName = "公司正式客户客观信息维护（集中作业）";break;
                    case "KH003": flowName = "公司客户财务报表新增（集中作业）";break;
                    case "KH004": flowName = "公司客户财务报表修改（集中作业）";break;
                    case "KH005": flowName = "公司客户转正（集中作业）";break;
                    case "KH006": flowName = "个人客户财务报表新增（集中作业）";break;
                    case "KH007": flowName = "个人客户财务报表维护（集中作业）";break;
                    default: flowName = "";
                }
                if(StringUtils.isNotEmpty(flowName)) {
                    CusBase cusBase = cusBaseService.selectByPrimaryKey(cusManaTask.getCusId());
                    MessageSendDto messageSendDto = new MessageSendDto();
                    Map<String, String> map = new HashMap<>();
                    map.put("cusId", cusBase.getCusName());
                    map.put("flowName", flowName);
                    map.put("result", "退回");
                    List<ReceivedUserDto> receivedUserList = new ArrayList<>();
                    ReceivedUserDto receivedUserDto = new ReceivedUserDto();
                    receivedUserDto.setReceivedUserType("1");// 发送人员类型
                    receivedUserDto.setUserId(cusBase.getManagerId());// 客户经理工号
                    receivedUserDto.setMobilePhone("");// 电话号码/邮箱/微信号 借款人必输
                    receivedUserList.add(receivedUserDto);
                    messageSendDto.setMessageType("MSG_KH_M_0003");
                    messageSendDto.setParams(map);
                    messageSendDto.setReceivedUserList(receivedUserList);
                    messageSendService.sendMessage(messageSendDto);
                }
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程打回操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程拿回操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                cusManaTaskService.update(cusManaTask);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程拿回初始节点操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                cusManaTaskService.update(cusManaTask);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，流程拿回初始节点操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                cusManaTask.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                cusManaTaskService.update(cusManaTask);
            } else {
                log.info("客户信息录入集中作业流程申请【{}】，场景【{}】，未知操作，流程参数【{}】", serno, flowBizType, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("客户信息录入集中作业流程申请【{}】，场景【{}】，后业务处理失败{}", serno, flowBizType, e.getMessage());
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "KHGL01".equals(flowCode);
    }
}
