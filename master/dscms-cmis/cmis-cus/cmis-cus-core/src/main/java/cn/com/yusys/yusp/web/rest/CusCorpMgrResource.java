/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.CusCorpMgrDto;
import cn.com.yusys.yusp.dto.CusIndivDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusCorpMgr;
import cn.com.yusys.yusp.service.CusCorpMgrService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpMgrResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-03-22 14:57:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuscorpmgr")
public class CusCorpMgrResource {
    @Autowired
    private CusCorpMgrService cusCorpMgrService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusCorpMgr>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusCorpMgr> list = cusCorpMgrService.selectAll(queryModel);
        return new ResultDto<List<CusCorpMgr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusCorpMgr>> index(QueryModel queryModel) {
        List<CusCorpMgr> list = cusCorpMgrService.selectByModel(queryModel);
        return new ResultDto<List<CusCorpMgr>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusCorpMgr>> query(@RequestBody QueryModel queryModel) {
        List<CusCorpMgr> list = cusCorpMgrService.selectByModel(queryModel);
        return new ResultDto<List<CusCorpMgr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusCorpMgr> show(@PathVariable("pkId") String pkId) {
        CusCorpMgr cusCorpMgr = cusCorpMgrService.selectByPrimaryKey(pkId);
        return new ResultDto<CusCorpMgr>(cusCorpMgr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusCorpMgr> create(@RequestBody CusCorpMgr cusCorpMgr) throws URISyntaxException {
        cusCorpMgr.setOprType("01");
        cusCorpMgrService.insert(cusCorpMgr);
        return new ResultDto<CusCorpMgr>(cusCorpMgr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusCorpMgr cusCorpMgr) throws URISyntaxException {
        int result = cusCorpMgrService.update(cusCorpMgr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusCorpMgrService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusCorpMgrService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:batchdelete
     * @函数描述:获取高管信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getCert")
    protected ResultDto<CusCorpMgr> getCert(@RequestBody CusCorpMgr cusCorpMgr) {
        QueryModel queryModel = new QueryModel();
        CusCorpMgr cusMgr = cusCorpMgrService.selectCusCorpMgrByCodeAndType(cusCorpMgr.getMrgType(),cusCorpMgr.getMrgCertCode());
        return new ResultDto<CusCorpMgr>(cusMgr);
    }

    /**
     * @return
     * @函数名称:sendEcif
     * @函数描述: 个人客户创建向导
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 徐鑫
     */
    @ApiOperation(value = "个人客户开户")
    @PostMapping("/sendEcif")
    protected ResultDto<CusCorpMgr> sendEcif(@Validated @RequestBody CusCorpMgr cusCorpMgr) {
        CusIndivDto cusIndivDto = new CusIndivDto();
        cusIndivDto.setCertType(cusCorpMgr.getMrgCertType());
        cusIndivDto.setCertCode(cusCorpMgr.getMrgCertCode());
        cusIndivDto.setCusName(cusCorpMgr.getMrgName());
        CusIndivDto cusIndivDtos = cusCorpMgrService.createCusIndiv(cusIndivDto);
        if (cusIndivDtos != null) {
            cusCorpMgr.setCusId(cusIndivDtos.getCusId());
            cusCorpMgr.setMrgName(cusIndivDtos.getCusName());
            cusCorpMgr.setCountry(cusIndivDtos.getNation());
            cusCorpMgr.setMrgSex(cusIndivDtos.getSex());
            cusCorpMgr.setCertIdate(cusIndivDtos.getCertEndDt());
        } else {
            cusCorpMgr = null;
        }
        return new ResultDto<CusCorpMgr>(cusCorpMgr);
    }

    @PostMapping("/getCusCorpMgrByMrgType")
    protected ResultDto<CusCorpMgrDto> getCusCorpMgrByMrgType(@RequestBody Map<String,Object> map){
        String mrgType = map.get("mrgType").toString();
        String cusId = map.get("cusId").toString();
        CusCorpMgrDto cusCorpMgrDto = cusCorpMgrService.getCusCorpMgrByMrgType(mrgType,cusId);
        return new ResultDto<CusCorpMgrDto>(cusCorpMgrDto);
    }

    @PostMapping("/getCusCorpMgrByParams")
    protected ResultDto<CusCorpMgrDto> getCusCorpMgrByParams(@RequestBody Map<String,Object> map){
        String mrgType = map.get("mrgType").toString();
        String cusIdRel = map.get("cusIdRel").toString();
        CusCorpMgrDto cusCorpMgrDto = cusCorpMgrService.getCusCorpMgrByParams(mrgType,cusIdRel);
        return new ResultDto<CusCorpMgrDto>(cusCorpMgrDto);
    }

    /**
     * @函数名称:queryCusCorpMgr
     * @函数描述:查询高管信息
     * @参数与返回说明:
     * @param queryModel
     */
    @PostMapping("/querycuscorpmgr")
    protected ResultDto<List<CusCorpMgrDto>> queryCusCorpMgr(@RequestBody QueryModel queryModel) {
        List<CusCorpMgrDto> list = cusCorpMgrService.queryCusCorpMgr(queryModel);
        return new ResultDto<List<CusCorpMgrDto>>(list);
    }
}
