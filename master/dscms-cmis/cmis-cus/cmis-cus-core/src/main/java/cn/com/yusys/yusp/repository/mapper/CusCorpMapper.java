/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.CusCorpSingUpholdDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.resp.Xdkh0002DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-04-10 10:48:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusCorpMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusCorp selectByPrimaryKey(@Param("cusId") String cusId);

    /**
     * @方法名称: selectCropAndBaseByPrimarKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    CusCorp selectCropAndBaseByPrimarKey(@Param("cusId") String cusId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusCorp> selectByModel(QueryModel model);

    /**
     * 交易描述：对公客户信息查询
     *
     * @param map
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdkh0030.resp.CusInfoList> selectCusCorpList(Map map);


    /**
     * @方法名称: queryCuscrop
     * @方法描述: 国控类
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String,Object>> queryCuscrop(QueryModel model);


    /**
     * @方法名称: queryCuscropBank
     * @方法描述: 银企合作信息列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String,Object>> queryCuscropBank(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusCorp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusCorp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusCorp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusCorp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("cusId") String cusId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 对公客户基本信息查询
     *
     * @param req
     * @return
     */
    Xdkh0002DataRespDto selectCusIdByOrgCode(@Param("req") Map<String, Object> req);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String, Object>> selectByModelXp1(QueryModel model);


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    Map<String, Object> queryCorpEvalInfoXp(String cusId);

    /**
     * @方法名称: selectCusCorpSignUpholdByModel
     * @方法描述: 条件查询国控类标识维护
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusCorpSingUpholdDto> selectCusCorpSignUpholdByModel(QueryModel model);

    /**
     * 优企贷、优农贷客户信息查询
     * @param map
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.cus.xdkh0025.resp.Xdkh0025RespDto4Null> getXdkh0025(Map map);
	/**
     * 根据客户编号查询对公客户基本信息
     * @param cusId
     * @return
     */

    CusCorpDto queryCusCropDtoByCusId(@Param("cusId") String cusId);
	/**
     * @方法名称: updateLockStatusBycusId
     * @方法描述: 根据客户号更新是否锁定
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateLockStatusBycusId(Map map);

    /**
     * @方法名称: updateBhjqxydjAndPdBycusId
     * @方法描述: 根据客户号更新对公客户本行即期信用等级及平均违约概率
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateBhjqxydjAndPdBycusId(Map map);

    /**
     * @Description:根据客户号删除客户在CusCorp表中的信息(逻辑删除)
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:22
     * @param cusId: 客户号
     * @return: boolean
     **/
    int deleteCusCorpByCusId(String cusId);

    /**
     * 根据客户编号查询对公客户基本信息
     * @param cusId
     * @return
     */

    CusCorp selectCropAndBaseByCusId(@Param("cusId") String cusId);

    /**
     * 查询该客户其所在集团成员的外部评级
     * @param grpNo
     * @return
     */
    String selectCreditLevelOuterByGrpNo(@Param("grpNo") String grpNo);
}