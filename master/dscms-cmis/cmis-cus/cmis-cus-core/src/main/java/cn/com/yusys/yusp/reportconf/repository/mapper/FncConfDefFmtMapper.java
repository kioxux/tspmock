/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.reportconf.repository.mapper;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.com.yusys.yusp.reportconf.domain.FncConfDefFmt;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfDefFmtMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 11:26:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface FncConfDefFmtMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    FncConfDefFmt selectByPrimaryKey(@Param("styleId") String styleId, @Param("itemId") String itemId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<FncConfDefFmt> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insert(FncConfDefFmt record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int insertSelective(FncConfDefFmt record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int updateByPrimaryKey(FncConfDefFmt record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    int updateByPrimaryKeySelective(FncConfDefFmt record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    int deleteByPrimaryKey(@Param("styleId") String styleId, @Param("itemId") String itemId);
    /**
     * 读取数据库中的报表配置定义表(FNC_CONF_DEF_FMT)中的属于"styleID"的报表的所有item的配置信息
	 * 取出来的item在list中的排列顺序是栏位、顺序号
	 * 将结果数据放到list中并返回
     * @param styleId
     * @return
     */
    List<FncConfDefFmt> getFncConfDefFormatFromDB(@Param("styleId") String styleId);
    /**
     * 财务报表信息  资产负债、损益、现金流量、财务指标调用
     * @param record
     * @return
     */
    List<FncConfDefFmt> queryFncConfDefFmtList1(FncConfDefFmt record);
    /**
     * 整体拼接动态sql直接执行
     * @param preSql
     * @return
     */
    List<FncConfDefFmt> queryFncConfDefFmtList2(@Param("preSql") String preSql);

    List<FncConfDefFmt> queryForSoe(@Param("postfix")String postfix,@Param("cusId")String cusId,
                              @Param("tableName")String tableName,@Param("year")String year,
                              @Param("statStyle")String statStyle,@Param("styleId")String styleId);

    List<FncConfDefFmt> querySqlNew2(@Param("tableName")String tableName, @Param("fncConfTyp")String fncConfTyp,
                               @Param("postfix")String postfix, @Param("cusId")String cusId,
                               @Param("year")String year, @Param("lastYear")String lastYear,
                               @Param("statStyle")String statStyle, @Param("styleId")String styleId);

    List<FncConfDefFmt> querySqlNew(@Param("tableName")String tableName, @Param("fncConfTyp")String fncConfTyp,
                              @Param("postfix")String postfix, @Param("cusId")String cusId,
                              @Param("year")String year, @Param("statStyle")String statStyle,
                              @Param("styleId")String styleId);
}