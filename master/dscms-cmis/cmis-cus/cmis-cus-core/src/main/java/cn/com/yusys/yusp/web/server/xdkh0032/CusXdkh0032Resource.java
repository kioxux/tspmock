package cn.com.yusys.yusp.web.server.xdkh0032;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0032.req.Xdkh0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0032.resp.Xdkh0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0032.Xdkh0032Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:信息锁定标志同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0032:信息锁定标志同步")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0032Resource.class);

    @Autowired
    private Xdkh0032Service xdkh0032Service;

    /**
     * 交易码：xdkh0032
     * 交易描述：信息锁定标志同步
     *
     * @param xdkh0032DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信息锁定标志同步")
    @PostMapping("/xdkh0032")
    protected @ResponseBody
    ResultDto<Xdkh0032DataRespDto> xdkh0032(@Validated @RequestBody Xdkh0032DataReqDto xdkh0032DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, JSON.toJSONString(xdkh0032DataReqDto));
        Xdkh0032DataRespDto xdkh0032DataRespDto = new Xdkh0032DataRespDto();// 响应Dto:信息锁定标志同步
        ResultDto<Xdkh0032DataRespDto> xdkh0032DataResultDto = new ResultDto<>();
        try {
            // 从xdkh0032DataReqDto获取业务值进行业务逻辑处理
            String cusId = xdkh0032DataReqDto.getCusId();//客户编号
            String lockStatus = xdkh0032DataReqDto.getLockStatus();//锁定状态
            String dtghFlag = xdkh0032DataReqDto.getDtghFlag();//区分标识
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0032DataReqDto));
            xdkh0032DataRespDto=xdkh0032Service.xdkh0032(xdkh0032DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0032DataRespDto));
            // 封装xdkh0032DataResultDto中正确的返回码和返回信息
            xdkh0032DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0032DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, e.getMessage());
            // 封装xdkh0032DataResultDto中异常返回码和返回信息
            xdkh0032DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0032DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0032DataRespDto到xdkh0032DataResultDto中
        xdkh0032DataResultDto.setData(xdkh0032DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, JSON.toJSONString(xdkh0032DataResultDto));
        return xdkh0032DataResultDto;
    }
}