/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.dto.CusLstGlfGljyyjedDto;
import cn.com.yusys.yusp.vo.CusLstGlfGljyyjedVo;
import cn.com.yusys.yusp.vo.CusLstGlfVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstGlfGljyyjed;
import cn.com.yusys.yusp.service.CusLstGlfGljyyjedService;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstGlfGljyyjedResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:29
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstglfgljyyjed")
public class CusLstGlfGljyyjedResource {
    @Autowired
    private CusLstGlfGljyyjedService cusLstGlfGljyyjedService;
    private final Logger logger = LoggerFactory.getLogger(CusLstDedkkhResource.class);

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstGlfGljyyjed>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstGlfGljyyjed> list = cusLstGlfGljyyjedService.selectAll(queryModel);
        return new ResultDto<List<CusLstGlfGljyyjed>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstGlfGljyyjed>> index(QueryModel queryModel) {
        List<CusLstGlfGljyyjed> list = cusLstGlfGljyyjedService.selectByModel(queryModel);
        return new ResultDto<List<CusLstGlfGljyyjed>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:query
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstGlfGljyyjed>> query(@RequestBody QueryModel queryModel) {
        List<CusLstGlfGljyyjed> list = cusLstGlfGljyyjedService.selectByModel(queryModel);
        return new ResultDto<List<CusLstGlfGljyyjed>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstGlfGljyyjed> show(@PathVariable("serno") String serno) {
        CusLstGlfGljyyjed cusLstGlfGljyyjed = cusLstGlfGljyyjedService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstGlfGljyyjed>(cusLstGlfGljyyjed);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstGlfGljyyjed> create(@RequestBody CusLstGlfGljyyjed cusLstGlfGljyyjed) throws URISyntaxException {
        ResultDto<CusLstGlfGljyyjed> dto = cusLstGlfGljyyjedService.insert(cusLstGlfGljyyjed);
        return dto;
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstGlfGljyyjed cusLstGlfGljyyjed) throws URISyntaxException {
        int result = cusLstGlfGljyyjedService.update(cusLstGlfGljyyjed);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstGlfGljyyjedService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstGlfGljyyjedService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = cusLstGlfGljyyjedService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        // TODO  修改业务流水号
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库
        ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(CusLstGlfGljyyjedVo.class)
                // 批量操作需要将batch设置为true
                .batch(true)
                .file(tempFile)
                .dataStorage(ExcelUtils.batchConsumer(cusLstGlfGljyyjedService::insertInBatch)));
        logger.info("开始执行异步导出，导入taskId为[{}];", progressDto.getTaskId());
        return ResultDto.success(progressDto);
    }


    /**
     * 根据客户号查询关联额度信息
     *
     * @param cusId 客户编号
     * @return
     * @throws IOException
     */
    @PostMapping("/querycuslstglfgljyyjeddtobycusid")
    public ResultDto<CusLstGlfGljyyjedDto> queryCusLstGlfGljyyjedDtoByCusId(@RequestBody String cusId){
        return ResultDto.success(cusLstGlfGljyyjedService.queryCusLstGlfGljyyjedDtoByCusId(cusId));
    }
    

}
