/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.dto.CusLstGlfDto;
import cn.com.yusys.yusp.dto.server.xdkh0026.req.Xdkh0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0027.req.Xdkh0027DataReqDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.CusLstGlf;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstGlfMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-13 20:17:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusLstGlfMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusLstGlf selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusLstGlf> selectByModel(QueryModel model);

    /**
     * @方法名称: selectByCusIdIsNull
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusLstGlf> selectCusIdIsNull();
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusLstGlf record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusLstGlf record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusLstGlf record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusLstGlf record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);
    /**
     * @方法名称: selectCount
     * @方法描述: 查询记录数
     * @参数与返回说明:
     * @算法描述: 无
     */

    int selectCount(@Param("cert_code") String certCode);

    int selectRelationCount(String certNo);

    /**
     * 乐悠金卡关联人查询
     * @param parebtRelatedPartyCertNo
     * @return
     */
    int selectCountByparebtRelatedPartyCertNo(@Param("certCode") String certCode,@Param("parebtRelatedPartyCertNo") String parebtRelatedPartyCertNo);

    /**
     * 优企贷、优农贷行内关联人基本信息查询-个人查询
     * @param xdkh0026DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdkh0026.resp.List> getIndivRel(Xdkh0026DataReqDto xdkh0026DataReqDto);

    /**
     * 优企贷、优农贷行内关联人基本信息查询-对公查询
     * @param xdkh0026DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdkh0026.resp.List> getComRel(Xdkh0026DataReqDto xdkh0026DataReqDto);

    /**
     * 优企贷、优农贷行内关联自然人基本信息查询
     * @param xdkh0027DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdkh0027.resp.List> getXdkh0027(Xdkh0027DataReqDto xdkh0027DataReqDto);

    /**
     * 根据客户号查询生效的记录数
     * @param cusId
     * @return
     */
    int queryCountsByCusId(@Param("cusId") String cusId);

    /**
     * 根据客户号查询问题关联名单信息
     * @param cusId
     * @return
     */
    CusLstGlfDto queryCusLstGlfByCusId(@Param("cusId") String cusId);
    /**
     * 查询生效的自然人关联方
     * @return
     */
    List<CusLstGlfDto> queryCusLstGlfForIndiv();
}