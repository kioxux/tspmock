package cn.com.yusys.yusp.web.server.xdkh0027;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0027.req.Xdkh0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0027.resp.Xdkh0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0027.Xdkh0027Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:优企贷、优农贷行内关联自然人基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0027:优企贷、优农贷行内关联自然人基本信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0027Resource.class);

    @Resource
    private Xdkh0027Service xdkh0027Service;

    /**
     * 交易码：xdkh0027
     * 交易描述：优企贷、优农贷行内关联自然人基本信息查询
     *
     * @param xdkh0027DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷行内关联自然人基本信息查询")
    @PostMapping("/xdkh0027")
    protected @ResponseBody
    ResultDto<Xdkh0027DataRespDto> xdkh0027(@Validated @RequestBody Xdkh0027DataReqDto xdkh0027DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027DataReqDto));
        Xdkh0027DataRespDto xdkh0027DataRespDto = new Xdkh0027DataRespDto();// 响应Dto:优企贷、优农贷行内关联自然人基本信息查询
        ResultDto<Xdkh0027DataRespDto> xdkh0027DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027DataReqDto));
            xdkh0027DataRespDto = xdkh0027Service.xdkh0027(xdkh0027DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027DataRespDto));
            // 封装xdkh0027DataResultDto中正确的返回码和返回信息
            xdkh0027DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0027DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, e.getMessage());
            // 封装xdkh0027DataResultDto中异常返回码和返回信息
            xdkh0027DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0027DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0027DataRespDto到xdkh0027DataResultDto中
        xdkh0027DataResultDto.setData(xdkh0027DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027DataResultDto));
        return xdkh0027DataResultDto;
    }
}