/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndiv
 * @类描述: cus_indiv数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-09 13:36:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_indiv")
public class CusIndiv extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CUS_ID")
	private String cusId;
	
	/** 别称 **/
	@Column(name = "NAME", unique = false, nullable = true, length = 80)
	private String name;
	
	/** 国籍 **/
	@Column(name = "NATION", unique = false, nullable = true, length = 5)
	private String nation;
	
	/** 性别 STD_ZB_SEX **/
	@Column(name = "SEX", unique = false, nullable = true, length = 5)
	private String sex;
	
	/** 是否为长期证件 STD_ZB_YES_NO **/
	@Column(name = "IS_LONG_INDIV", unique = false, nullable = true, length = 5)
	private String isLongIndiv;
	
	/** 证件签发日期 **/
	@Column(name = "CERT_START_DT", unique = false, nullable = true, length = 10)
	private String certStartDt;
	
	/** 证件到期日期 **/
	@Column(name = "CERT_END_DT", unique = false, nullable = true, length = 10)
	private String certEndDt;
	
	/** 是否为农户 STD_ZB_YES_NO **/
	@Column(name = "AGRI_FLG", unique = false, nullable = true, length = 5)
	private String agriFlg;
	
	/** 民族 STD_ZB_INDIV_FOLK **/
	@Column(name = "INDIV_FOLK", unique = false, nullable = true, length = 5)
	private String indivFolk;
	
	/** 籍贯 **/
	@Column(name = "INDIV_BRT_PLACE", unique = false, nullable = true, length = 80)
	private String indivBrtPlace;
	
	/** 户籍地址 **/
	@Column(name = "INDIV_HOUH_REG_ADD", unique = false, nullable = true, length = 200)
	private String indivHouhRegAdd;
	
	/** 街道 **/
	@Column(name = "VISIT_STREET", unique = false, nullable = true, length = 200)
	private String visitStreet;
	
	/** 出生日期 **/
	@Column(name = "INDIV_DT_OF_BIRTH", unique = false, nullable = true, length = 10)
	private String indivDtOfBirth;
	
	/** 政治面貌 STD_ZB_POLITICAL **/
	@Column(name = "INDIV_POL_ST", unique = false, nullable = true, length = 5)
	private String indivPolSt;
	
	/** 最高学历 STD_ZB_EDU **/
	@Column(name = "INDIV_EDT", unique = false, nullable = true, length = 5)
	private String indivEdt;
	
	/** 最高学位 STD_ZB_DEGREE **/
	@Column(name = "INDIV_DGR", unique = false, nullable = true, length = 5)
	private String indivDgr;
	
	/** 是否有子女 STD_ZB_YES_NO **/
	@Column(name = "IS_HAVE_CHILDREN", unique = false, nullable = true, length = 5)
	private String isHaveChildren;
	
	/** 婚姻状况 STD_ZB_MAR_ST **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
	private String marStatus;
	
	/** 健康状况 **/
	@Column(name = "HEALTH_STATUS", unique = false, nullable = true, length = 5)
	private String healthStatus;
	
	/** 建立信贷关系时间 **/
	@Column(name = "INIT_LOAN_DATE", unique = false, nullable = true, length = 10)
	private String initLoanDate;
	
	/** 在我行建立业务情况 STD_ZB_INV_HL_ACN **/
	@Column(name = "INDIV_HLD_ACNT", unique = false, nullable = true, length = 5)
	private String indivHldAcnt;
	
	/** 是否我行员工 STD_ZB_YES_NO **/
	@Column(name = "IS_BANK_EMPLOYEE", unique = false, nullable = true, length = 5)
	private String isBankEmployee;
	
	/** 在我行职务 STD_ZB_BANK_DUTY **/
	@Column(name = "BANK_DUTY", unique = false, nullable = true, length = 5)
	private String bankDuty;
	
	/** 是否我行股东 STD_ZB_YES_NO **/
	@Column(name = "IS_BANK_SHAREHD", unique = false, nullable = true, length = 5)
	private String isBankSharehd;
	
	/** 用工形式 **/
	@Column(name = "UTIL_EMPLOYEE_MODE", unique = false, nullable = true, length = 5)
	private String utilEmployeeMode;
	
	/** 人员类别 **/
	@Column(name = "EMPLOYEE_CLS", unique = false, nullable = true, length = 5)
	private String employeeCls;
	
	/** 是否有中征码 STD_ZB_YES_NO **/
	@Column(name = "LOAN_CARD_FLG", unique = false, nullable = true, length = 5)
	private String loanCardFlg;
	
	/** 中征码 **/
	@Column(name = "LOAN_CARD_ID", unique = false, nullable = true, length = 16)
	private String loanCardId;
	
	/** 是否重点客户 STD_ZB_YES_NO **/
	@Column(name = "IS_MAIN_CUS", unique = false, nullable = true, length = 5)
	private String isMainCus;

	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;

	/** 影像编号 **/
	@Column(name = "IMAGE_NO", unique = false, nullable = true, length = 80)
	private String imageNo;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;

	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 10)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 是否五日还款 **/
	@Column(name = "CUS_GETFIVE", unique = false, nullable = true, length = 5)
	private String cusGetfive;
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
    /**
     * @return name
     */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @param nation
	 */
	public void setNation(String nation) {
		this.nation = nation;
	}
	
    /**
     * @return nation
     */
	public String getNation() {
		return this.nation;
	}
	
	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex;
	}
	
    /**
     * @return sex
     */
	public String getSex() {
		return this.sex;
	}
	
	/**
	 * @param isLongIndiv
	 */
	public void setIsLongIndiv(String isLongIndiv) {
		this.isLongIndiv = isLongIndiv;
	}
	
    /**
     * @return isLongIndiv
     */
	public String getIsLongIndiv() {
		return this.isLongIndiv;
	}
	
	/**
	 * @param certStartDt
	 */
	public void setCertStartDt(String certStartDt) {
		this.certStartDt = certStartDt;
	}
	
    /**
     * @return certStartDt
     */
	public String getCertStartDt() {
		return this.certStartDt;
	}
	
	/**
	 * @param certEndDt
	 */
	public void setCertEndDt(String certEndDt) {
		this.certEndDt = certEndDt;
	}
	
    /**
     * @return certEndDt
     */
	public String getCertEndDt() {
		return this.certEndDt;
	}
	
	/**
	 * @param agriFlg
	 */
	public void setAgriFlg(String agriFlg) {
		this.agriFlg = agriFlg;
	}
	
    /**
     * @return agriFlg
     */
	public String getAgriFlg() {
		return this.agriFlg;
	}
	
	/**
	 * @param indivFolk
	 */
	public void setIndivFolk(String indivFolk) {
		this.indivFolk = indivFolk;
	}
	
    /**
     * @return indivFolk
     */
	public String getIndivFolk() {
		return this.indivFolk;
	}
	
	/**
	 * @param indivBrtPlace
	 */
	public void setIndivBrtPlace(String indivBrtPlace) {
		this.indivBrtPlace = indivBrtPlace;
	}
	
    /**
     * @return indivBrtPlace
     */
	public String getIndivBrtPlace() {
		return this.indivBrtPlace;
	}
	
	/**
	 * @param indivHouhRegAdd
	 */
	public void setIndivHouhRegAdd(String indivHouhRegAdd) {
		this.indivHouhRegAdd = indivHouhRegAdd;
	}
	
    /**
     * @return indivHouhRegAdd
     */
	public String getIndivHouhRegAdd() {
		return this.indivHouhRegAdd;
	}
	
	/**
	 * @param visitStreet
	 */
	public void setVisitStreet(String visitStreet) {
		this.visitStreet = visitStreet;
	}
	
    /**
     * @return visitStreet
     */
	public String getVisitStreet() {
		return this.visitStreet;
	}
	
	/**
	 * @param indivDtOfBirth
	 */
	public void setIndivDtOfBirth(String indivDtOfBirth) {
		this.indivDtOfBirth = indivDtOfBirth;
	}
	
    /**
     * @return indivDtOfBirth
     */
	public String getIndivDtOfBirth() {
		return this.indivDtOfBirth;
	}
	
	/**
	 * @param indivPolSt
	 */
	public void setIndivPolSt(String indivPolSt) {
		this.indivPolSt = indivPolSt;
	}
	
    /**
     * @return indivPolSt
     */
	public String getIndivPolSt() {
		return this.indivPolSt;
	}
	
	/**
	 * @param indivEdt
	 */
	public void setIndivEdt(String indivEdt) {
		this.indivEdt = indivEdt;
	}
	
    /**
     * @return indivEdt
     */
	public String getIndivEdt() {
		return this.indivEdt;
	}
	
	/**
	 * @param indivDgr
	 */
	public void setIndivDgr(String indivDgr) {
		this.indivDgr = indivDgr;
	}
	
    /**
     * @return indivDgr
     */
	public String getIndivDgr() {
		return this.indivDgr;
	}
	
	/**
	 * @param isHaveChildren
	 */
	public void setIsHaveChildren(String isHaveChildren) {
		this.isHaveChildren = isHaveChildren;
	}
	
    /**
     * @return isHaveChildren
     */
	public String getIsHaveChildren() {
		return this.isHaveChildren;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}
	
    /**
     * @return marStatus
     */
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param healthStatus
	 */
	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}
	
    /**
     * @return healthStatus
     */
	public String getHealthStatus() {
		return this.healthStatus;
	}
	
	/**
	 * @param initLoanDate
	 */
	public void setInitLoanDate(String initLoanDate) {
		this.initLoanDate = initLoanDate;
	}
	
    /**
     * @return initLoanDate
     */
	public String getInitLoanDate() {
		return this.initLoanDate;
	}
	
	/**
	 * @param indivHldAcnt
	 */
	public void setIndivHldAcnt(String indivHldAcnt) {
		this.indivHldAcnt = indivHldAcnt;
	}
	
    /**
     * @return indivHldAcnt
     */
	public String getIndivHldAcnt() {
		return this.indivHldAcnt;
	}
	
	/**
	 * @param isBankEmployee
	 */
	public void setIsBankEmployee(String isBankEmployee) {
		this.isBankEmployee = isBankEmployee;
	}
	
    /**
     * @return isBankEmployee
     */
	public String getIsBankEmployee() {
		return this.isBankEmployee;
	}
	
	/**
	 * @param bankDuty
	 */
	public void setBankDuty(String bankDuty) {
		this.bankDuty = bankDuty;
	}
	
    /**
     * @return bankDuty
     */
	public String getBankDuty() {
		return this.bankDuty;
	}
	
	/**
	 * @param isBankSharehd
	 */
	public void setIsBankSharehd(String isBankSharehd) {
		this.isBankSharehd = isBankSharehd;
	}
	
    /**
     * @return isBankSharehd
     */
	public String getIsBankSharehd() {
		return this.isBankSharehd;
	}
	
	/**
	 * @param utilEmployeeMode
	 */
	public void setUtilEmployeeMode(String utilEmployeeMode) {
		this.utilEmployeeMode = utilEmployeeMode;
	}
	
    /**
     * @return utilEmployeeMode
     */
	public String getUtilEmployeeMode() {
		return this.utilEmployeeMode;
	}
	
	/**
	 * @param employeeCls
	 */
	public void setEmployeeCls(String employeeCls) {
		this.employeeCls = employeeCls;
	}
	
    /**
     * @return employeeCls
     */
	public String getEmployeeCls() {
		return this.employeeCls;
	}
	
	/**
	 * @param loanCardFlg
	 */
	public void setLoanCardFlg(String loanCardFlg) {
		this.loanCardFlg = loanCardFlg;
	}
	
    /**
     * @return loanCardFlg
     */
	public String getLoanCardFlg() {
		return this.loanCardFlg;
	}
	
	/**
	 * @param loanCardId
	 */
	public void setLoanCardId(String loanCardId) {
		this.loanCardId = loanCardId;
	}
	
    /**
     * @return loanCardId
     */
	public String getLoanCardId() {
		return this.loanCardId;
	}
	
	/**
	 * @param isMainCus
	 */
	public void setIsMainCus(String isMainCus) {
		this.isMainCus = isMainCus;
	}
	
    /**
     * @return isMainCus
     */
	public String getIsMainCus() {
		return this.isMainCus;
	}

	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}

	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}

    /**
     * @return imageNo
     */
	public String getImageNo() {
		return this.imageNo;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}
}