package cn.com.yusys.yusp.fncstat.domain;

/**
 * OCR返回的科目信息
 * @author qiantj
 * 
 *
 */
public class OcrItem {

	private String subjectId;//子科目id
	
	private String subjectName;//子科目名称
	
	private String subjectCode;//科目编码
	
	private String data1;//第一列数据
	
	private String data2;//第二列数据

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}
}
