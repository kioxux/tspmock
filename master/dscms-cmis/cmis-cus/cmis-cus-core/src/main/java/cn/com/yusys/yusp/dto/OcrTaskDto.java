package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: OcrTask
 * @类描述: ocr_task数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-18 19:39:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class OcrTaskDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 报表期间 **/
	private String statPrd;
	
	/** 任务编号 **/
	private String taskId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param statPrd
	 */
	public void setStatPrd(String statPrd) {
		this.statPrd = statPrd == null ? null : statPrd.trim();
	}
	
    /**
     * @return StatPrd
     */	
	public String getStatPrd() {
		return this.statPrd;
	}
	
	/**
	 * @param taskId
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId == null ? null : taskId.trim();
	}
	
    /**
     * @return TaskId
     */	
	public String getTaskId() {
		return this.taskId;
	}


}