package cn.com.yusys.yusp.service.server.xdkh0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.req.XxdrelReqDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.List1;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.XxdrelRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0007.resp.Xdkh0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import cn.com.yusys.yusp.service.Dscms2RlzyxtClientService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0005Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-28 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0007Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0007Service.class);

    @Resource
    private CusBaseMapper cusBaseMapper;
    @Resource
    private CusIndivMapper cusIndivMapper;
    @Autowired
    private Dscms2RlzyxtClientService dscms2RlzyxtClientService;// BSP 人力资源

    /**
     * 查询是否我行员工及其直系亲属
     *
     * @param cusId
     * @return
     */
    @Transactional
    public Xdkh0007DataRespDto getXdkh0007(String cusId) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, "请求参数cus_id：" + cusId);
        Xdkh0007DataRespDto xdkh0007DataRespDto = new Xdkh0007DataRespDto();
        try {
            //查询是否本行员工c
            CusBase cusBase = cusBaseMapper.selectByPrimaryKey(cusId);

            if (cusBase == null) {//客户信息不存在
                xdkh0007DataRespDto.setIsBankStaff(CmisCommonConstants.STD_IS_FLAG_N);
                xdkh0007DataRespDto.setIsBankStaffFamily(CmisCommonConstants.STD_IS_FLAG_N);
                return xdkh0007DataRespDto;
            }
            //调用人力资源系统的接口,查询客户是否为我行员工
            String certCode = cusBase.getCertCode();
            if (StringUtils.isNotEmpty(certCode)) {
                XxdrelReqDto xxdrelReqDto = new XxdrelReqDto();
                xxdrelReqDto.setCardid(certCode);
                logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDREL.key, EsbEnum.TRADE_CODE_XXDREL.value, JSON.toJSONString(xxdrelReqDto));
                ResultDto resultDto = dscms2RlzyxtClientService.xxdrel(xxdrelReqDto);
                logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDREL.key, EsbEnum.TRADE_CODE_XXDREL.value, JSON.toJSONString(resultDto));
                XxdrelRespDto xxdrelRespDto = (XxdrelRespDto) resultDto.getData();
                if (xxdrelRespDto != null) {
                    if (CollectionUtils.nonEmpty(xxdrelRespDto.getList1())) {//员工信息不为空,则为本行员工
                        List<List1> list = xxdrelRespDto.getList1();
                        boolean isbank = false;//判断本人是否本行员
                        for (List1 l : list) {
                            if (certCode.equals(l.getIdtfno())) {//证件号是否和入参匹配
                                isbank = true;
                            }
                        }
                        //分三种情况1、本人是行员2、本人和亲属都是行员3、只有亲属是行员
                        if (isbank && list.size() == 1) {//只有一条记录且和本人证件号匹配
                            xdkh0007DataRespDto.setIsBankStaff(CmisCommonConstants.STD_IS_FLAG_Y);
                            xdkh0007DataRespDto.setIsBankStaffFamily(CmisCommonConstants.STD_IS_FLAG_N);
                        } else if (isbank && list.size() > 1) {//存在多条记录且存在和本人证件号匹配
                            xdkh0007DataRespDto.setIsBankStaff(CmisCommonConstants.STD_IS_FLAG_Y);
                            xdkh0007DataRespDto.setIsBankStaffFamily(CmisCommonConstants.STD_IS_FLAG_Y);
                        } else {//是否本行员工直属亲属（父母、子女、配偶）
                            xdkh0007DataRespDto.setIsBankStaff(CmisCommonConstants.STD_IS_FLAG_N);
                            xdkh0007DataRespDto.setIsBankStaffFamily(CmisCommonConstants.STD_IS_FLAG_Y);
                        }
                    } else {
                        xdkh0007DataRespDto.setIsBankStaff(CmisCommonConstants.STD_IS_FLAG_N);
                        xdkh0007DataRespDto.setIsBankStaffFamily(CmisCommonConstants.STD_IS_FLAG_N);
                    }
                } else {
                    xdkh0007DataRespDto.setIsBankStaff(CmisCommonConstants.STD_IS_FLAG_N);
                    xdkh0007DataRespDto.setIsBankStaffFamily(CmisCommonConstants.STD_IS_FLAG_N);
                }
            } else {
                xdkh0007DataRespDto.setIsBankStaff(CmisCommonConstants.STD_IS_FLAG_N);
                xdkh0007DataRespDto.setIsBankStaffFamily(CmisCommonConstants.STD_IS_FLAG_N);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, e.getMessage());
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, JSON.toJSONString(xdkh0007DataRespDto));
        return xdkh0007DataRespDto;
    }
}
