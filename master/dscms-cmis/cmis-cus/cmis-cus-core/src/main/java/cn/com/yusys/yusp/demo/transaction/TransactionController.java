package cn.com.yusys.yusp.demo.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {
    @Autowired
    private  TransactionService transactionService;

    @GetMapping("/test")
    public void test() throws Exception {
        transactionService.process();
    }
}
