/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CusBizAcquUserlstDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusBizAcquUserLst;
import cn.com.yusys.yusp.service.CusBizAcquUserLstService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBizAcquUserLstResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 18301
 * @创建时间: 2021-05-05 09:33:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusbizacquuserlst")
public class CusBizAcquUserLstResource {
    @Autowired
    private CusBizAcquUserLstService cusBizAcquUserLstService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusBizAcquUserLst>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusBizAcquUserLst> list = cusBizAcquUserLstService.selectAll(queryModel);
        return new ResultDto<List<CusBizAcquUserLst>>(list);
    }

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query")
    protected ResultDto<List<CusBizAcquUserLst>> queryAll(@RequestBody QueryModel queryModel) {
        List<CusBizAcquUserLst> list = cusBizAcquUserLstService.selectAll(queryModel);
        return new ResultDto<List<CusBizAcquUserLst>>(list);
    }



    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusBizAcquUserLst>> index(QueryModel queryModel) {
        List<CusBizAcquUserLst> list = cusBizAcquUserLstService.selectByModel(queryModel);
        return new ResultDto<List<CusBizAcquUserLst>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusBizAcquUserLst> show(@PathVariable("pkId") String pkId) {
        CusBizAcquUserLst cusBizAcquUserLst = cusBizAcquUserLstService.selectByPrimaryKey(pkId);
        return new ResultDto<CusBizAcquUserLst>(cusBizAcquUserLst);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusBizAcquUserLst> create(@RequestBody CusBizAcquUserLst cusBizAcquUserLst) throws URISyntaxException {
        cusBizAcquUserLstService.insert(cusBizAcquUserLst);
        return new ResultDto<CusBizAcquUserLst>(cusBizAcquUserLst);
    }


    /**
     * @函数名称:insertMore
     * @函数描述:插入多条数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertmore")
    protected ResultDto<CusBizAcquUserLst> insertMore(@RequestBody CusBizAcquUserlstDto cusBizAcquUserlstDto) {
        cusBizAcquUserLstService.insertApp(cusBizAcquUserlstDto);
        return ResultDto.success();
    }

    /**
     * @函数名称:insertMore
     * @函数描述:插入多条数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatemore")
    protected ResultDto<CusBizAcquUserLst> updatemore(@RequestBody CusBizAcquUserlstDto cusBizAcquUserlstDto) {
        cusBizAcquUserLstService.updateApp(cusBizAcquUserlstDto);
        return ResultDto.success();
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusBizAcquUserLst cusBizAcquUserLst) throws URISyntaxException {
        int result = cusBizAcquUserLstService.update(cusBizAcquUserLst);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusBizAcquUserLstService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusBizAcquUserLstService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
