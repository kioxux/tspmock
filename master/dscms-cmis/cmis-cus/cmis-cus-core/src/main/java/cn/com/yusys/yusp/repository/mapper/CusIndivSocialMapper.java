/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusIndivSocial;
import cn.com.yusys.yusp.dto.CusIndivSocialDto;
import cn.com.yusys.yusp.dto.CusIndivSocialResp;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.req.Xdkh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.resp.Xdkh0004DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivSocialMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-10 14:15:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusIndivSocialMapper {
    /**
     * 
     * @param cusId
     * @return
     */
    CusIndivSocial selectCusIndivSocialByCusId(@Param("cusId") String cusId);
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusIndivSocial selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusIndivSocial> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusIndivSocial record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusIndivSocial record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusIndivSocial record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusIndivSocial record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectCount
     * @方法描述: 根据客户号查配偶
     * @参数与返回说明:
     * @算法描述: 无
     * @author 周茂伟
     */
    int selectCount(@Param("cusIdRel") String cusIdRel);

    /**
     * 查询客户配偶信息
     * @param xdkh0004DataReqDto
     * @return
     */
    Xdkh0004DataRespDto getSpouseBaseData(Xdkh0004DataReqDto xdkh0004DataReqDto);

    /**
     * 根据实际控制人列表查询其配偶、父母、子女
     * @param cusIdRelList
     * @return
     */
    List<Map<String,String>> selectSocialInfoByCusIdRelList(@Param("cusIdRelList") List<String> cusIdRelList);

    /**
     * 根据证件编号查询配偶客户编号
     * @param certCode
     * @return
     */
    CmisCus0013RespDto selectSocialByCertCode(String certCode);

    /**
     * 根据证件类型，证件编号查询信贷客户信息
     * @param cusIndivSocialDto
     * @return
     */
    CusIndivSocialDto getCusMsg(CusIndivSocialDto cusIndivSocialDto);

    /**
     * @方法名称: queryCusSocial
     * @方法描述: 查询社会关系人信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusIndivSocialResp> queryCusSocial(QueryModel model);
}