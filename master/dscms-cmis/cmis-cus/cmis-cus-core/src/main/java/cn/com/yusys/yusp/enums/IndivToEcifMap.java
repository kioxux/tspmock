package cn.com.yusys.yusp.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * 个人客户  与 ECIF对接 字段映射关系
 */
public class IndivToEcifMap implements BeanMapInterface{

    private static Map<String, String> beanMap;

    static {
        beanMap = new HashMap<>();
        beanMap.put("custno","cusId");
        beanMap.put("custna","cusName");
        beanMap.put("idtftp","certType");
        beanMap.put("idtfno","certCode");
        //beanMap.put("efctdt","certStartDt");
        beanMap.put("inefdt","certEndDt");
        beanMap.put("prpsex","sex");
        beanMap.put("natvpc","indivBrtPlace");
        beanMap.put("ethnic","indivFolk");
        beanMap.put("nation","nation");
        beanMap.put("borndt","indivDtOfBirth");
        beanMap.put("polist","indivPolSt");
        beanMap.put("marist","marStatus");
        beanMap.put("havetg","isHaveChildren");
        beanMap.put("educlv","indivEdt");
        beanMap.put("indidp","indivDgr");
        beanMap.put("projob","occ");
        beanMap.put("poston","jobTtl");
        beanMap.put("posttl","indivCrtftn");
        beanMap.put("wkutna","unitName");
        beanMap.put("unquty","indivComTyp");
        beanMap.put("compad","unitAddr");
        beanMap.put("workdt","workDate");
        beanMap.put("idwkdt","tradeDate");
        beanMap.put("utwkdt","unitDate");
        beanMap.put("corppr","indivComTrade");
        beanMap.put("currcy","earningCurType");
        beanMap.put("pryric","indivYearn");
        beanMap.put("empltg","isBankEmployee");
        beanMap.put("farmtg","agriFlg");
        beanMap.put("sthdfg","isBankSharehe");
        beanMap.put("healst","healthStatus");
        beanMap.put("mobitl","mobileNo");
        beanMap.put("offctl","mobile");
        beanMap.put("qqnumb","qq");
        beanMap.put("wechat","wechatNo");
        beanMap.put("emailx","email");
        beanMap.put("uphone","unitPhn");
        beanMap.put("cusstp","cusType");
        beanMap.put("resare","indivRsdAddr");
        beanMap.put("resadd","streetRsd");
    }

    @Override
    public String getSourceFieldByTarget(String datatargetField) {
        String sourceField = null;
//        for (IndivToEcifEnum enumData : IndivToEcifEnum.values()) {
//            if (enumData.targetField.equals(datatargetField)) {
//                sourceField = enumData.sourceField;
//                break;
//            }
//        }
        for(Map.Entry<String, String> val : beanMap.entrySet()){
            if(datatargetField.equals(val.getValue())){
                sourceField = val.getKey();
            }
        }
        return sourceField;
    }

    @Override
    public String getTargetFieldBySource(String datasourceField) {
        String targetField = null;
//        for (IndivToEcifEnum enumData : IndivToEcifEnum.values()) {
//            if (enumData.sourceField.equals(datasourceField)) {
//                targetField = enumData.targetField;
//                break;
//            }
//        }
        if(beanMap.containsKey(datasourceField)){
            targetField =  beanMap.get(datasourceField);
        }

        return targetField;
    }


}
