/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.*;

import cn.com.yusys.yusp.domain.CusLstDedkkh;
import cn.com.yusys.yusp.dto.CusLstDedkkhYjsxDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstDedkkhYjsx;
import cn.com.yusys.yusp.service.CusLstDedkkhYjsxService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhYjsxResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstdedkkhyjsx")
public class CusLstDedkkhYjsxResource {
    @Autowired
    private CusLstDedkkhYjsxService cusLstDedkkhYjsxService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstDedkkhYjsx>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstDedkkhYjsx> list = cusLstDedkkhYjsxService.selectAll(queryModel);
        return new ResultDto<List<CusLstDedkkhYjsx>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param
     * @算法描述:
     */
    @PostMapping("/querylistdatabylistserno")
    protected ResultDto<List<CusLstDedkkhYjsx>> index(@RequestBody QueryModel queryModel) {
        List<CusLstDedkkhYjsx> list = cusLstDedkkhYjsxService.selectByModel(queryModel);
        return new ResultDto<List<CusLstDedkkhYjsx>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstDedkkhYjsx> show(@PathVariable("serno") String serno) {
        CusLstDedkkhYjsx cusLstDedkkhYjsx = cusLstDedkkhYjsxService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstDedkkhYjsx>(cusLstDedkkhYjsx);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstDedkkhYjsx> create(@RequestBody CusLstDedkkhYjsx cusLstDedkkhYjsx) throws URISyntaxException {
        cusLstDedkkhYjsxService.insert(cusLstDedkkhYjsx);
        return new ResultDto<CusLstDedkkhYjsx>(cusLstDedkkhYjsx);
    }
    /**
     * @函数名称:insertMore
     * @函数描述:插入多条数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertmore")
    protected ResultDto<CusLstDedkkhYjsx> insertMore(@RequestBody CusLstDedkkhYjsxDto dto) {
        int message = cusLstDedkkhYjsxService.insertMore(dto);
        return ResultDto.success(message);
    }
    /**
     * @函数名称:insertMore
     * @函数描述:插入多条数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatemore")
    protected ResultDto<CusLstDedkkhYjsx> updateMore(@RequestBody CusLstDedkkhYjsxDto dto) {
        int message = cusLstDedkkhYjsxService.updateMore(dto);
        return ResultDto.success(message);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstDedkkhYjsx cusLstDedkkhYjsx) throws URISyntaxException {
        int result = cusLstDedkkhYjsxService.updateSelective(cusLstDedkkhYjsx);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstDedkkhYjsxService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstDedkkhYjsxService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
