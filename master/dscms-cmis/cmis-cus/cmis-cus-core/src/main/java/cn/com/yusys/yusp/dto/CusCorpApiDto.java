package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 对外投资关系
 */
public class CusCorpApiDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //上一级客户ID
    private String upperCusId;
    private List<CusCorpRelationsDto> cusCorpRelationsDtos;

    public String getUpperCusId() {
        return upperCusId;
    }

    public void setUpperCusId(String upperCusId) {
        this.upperCusId = upperCusId;
    }

    public List<CusCorpRelationsDto> getCusCorpRelationsDtos() {
        return cusCorpRelationsDtos;
    }

    public void setCusCorpRelationsDtos(List<CusCorpRelationsDto> cusCorpRelationsDtos) {
        this.cusCorpRelationsDtos = cusCorpRelationsDtos;
    }
}
