package cn.com.yusys.yusp.service.server.xdkh0033;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusComGrade;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusIntbank;
import cn.com.yusys.yusp.dto.server.xdkh0033.req.Xdkh0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0033.resp.Xdkh0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusComGradeMapper;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static cn.com.yusys.yusp.enums.returncode.EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0033Service
 * @类描述: #服务类
 * @功能描述: 客户评级结果同步
 * @创建人: xs
 * @创建时间: 2021-5-11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */

@Service
public class Xdkh0033Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdkh0033.Xdkh0033Service.class);

    @Resource
    private CusComGradeMapper cusComGradeMapper;
    @Resource
    private CusCorpMapper cusCorpMapper;
    @Resource
    private CusIntbankMapper cusIntbankMapper;
    @Resource
    private CusBaseMapper cusBaseMapper;
    /**
     * 客户评级结果同步
     *
     * @param xdkh0033DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0033DataRespDto xdkh0033(Xdkh0033DataReqDto xdkh0033DataReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, JSON.toJSONString(xdkh0033DataReqDto));

        Xdkh0033DataRespDto xdkh0033DataRespDto = new Xdkh0033DataRespDto();
        /**
         * 区分标识
         * 字典项
         * 1公司客户
         * 2同业客户
         **/
        String dthFlag = xdkh0033DataReqDto.getDtghFlag();
        CusComGrade cusComGrade = new CusComGrade();
        String comSerno = xdkh0033DataReqDto.getEvalApplySerno();
        cusComGrade.setSerno(comSerno);//评级申请流水号
        cusComGrade.setCusId(xdkh0033DataReqDto.getCusId());//客户编号
        cusComGrade.setCusName(xdkh0033DataReqDto.getCusName());//客户名称
        cusComGrade.setGradeYear(xdkh0033DataReqDto.getEvalYear());//评级年度
        cusComGrade.setGradeType(xdkh0033DataReqDto.getEvalHppType());//评级发生类型
        cusComGrade.setGradeMode(xdkh0033DataReqDto.getEvalModel());//评级模型
        cusComGrade.setSysRank(xdkh0033DataReqDto.getSysStartLvl());//系统初始等级
        cusComGrade.setSuggestRank(xdkh0033DataReqDto.getAdviceLvl());//建议后等级
        cusComGrade.setAdjRank(xdkh0033DataReqDto.getAdjdLvl());//调整后等级
        cusComGrade.setFinalRank(xdkh0033DataReqDto.getFinalLvl());//最终认定等级
        cusComGrade.setEffDt(xdkh0033DataReqDto.getEvalStartDate());//评级生效日
        cusComGrade.setDueDt(xdkh0033DataReqDto.getEvalEndDate());//评级到期日
        cusComGrade.setPd(xdkh0033DataReqDto.getBreachProbly().toString());//违约概率
        cusComGrade.setBigriskclass(xdkh0033DataReqDto.getRiskTypeMax());//风险大类
        cusComGrade.setMidriskclass(xdkh0033DataReqDto.getRiskType());//风险中类
        cusComGrade.setFewriskclass(xdkh0033DataReqDto.getRiskTypeMin());//风险小类
        cusComGrade.setRiskdividedate(xdkh0033DataReqDto.getRiskDtghDate());//风险划分日期
        CusBase cusBase = cusBaseMapper.selectByPrimaryKey(xdkh0033DataReqDto.getCusId());
        if("1".equals(dthFlag) && ObjectUtils.isEmpty(cusBase)){
            throw BizException.error(null, E_CUS_CLIENT_CUSNULL_EXCEPTION.key, E_CUS_CLIENT_CUSNULL_EXCEPTION.value);
        }else if(!ObjectUtils.isEmpty(cusBase)){
            cusComGrade.setCertCode(cusBase.getCertCode());
            cusComGrade.setCertType(cusBase.getCertType());
        }

        int count = cusComGradeMapper.isExistBySerno(comSerno);
        if(count>0){
            // 存在数据，更新
            if(cusComGradeMapper.updateByPrimaryKeySelective(cusComGrade)>0){
                xdkh0033DataRespDto.setOpFlag("S");
                xdkh0033DataRespDto.setOpMsg("更新成功");
            }else{
                xdkh0033DataRespDto.setOpFlag("F");
                xdkh0033DataRespDto.setOpMsg("更新失败");
            }
        }else{
            // 不存在 新增
            if(cusComGradeMapper.insertSelective(cusComGrade)>0){
                xdkh0033DataRespDto.setOpFlag("S");
                xdkh0033DataRespDto.setOpMsg("新增成功");
            }else{
                xdkh0033DataRespDto.setOpFlag("F");
                xdkh0033DataRespDto.setOpMsg("新增失败");
            }
        }
        // 更新客户信息表中的评级信息
        if("1".equals(dthFlag)){
            CusCorp cusCorp= new CusCorp();
            cusCorp.setCusId(xdkh0033DataReqDto.getCusId());
            cusCorp.setPd(xdkh0033DataReqDto.getBreachProbly().toString());
            cusCorp.setCusCcrModelId(xdkh0033DataReqDto.getEvalModel());
            cusCorp.setBankLoanLevel(xdkh0033DataReqDto.getFinalLvl());
            cusCorpMapper.updateByPrimaryKeySelective(cusCorp);
        }else if("2".equals(dthFlag)){
            CusIntbank cusIntbank = new CusIntbank();
            cusIntbank.setCusId(xdkh0033DataReqDto.getCusId());
            cusIntbank.setPd(xdkh0033DataReqDto.getBreachProbly().toString());
            cusIntbank.setIntbankLevel(xdkh0033DataReqDto.getFinalLvl());
            cusIntbankMapper.updateByPrimaryKeySelective(cusIntbank);

        }
        // 从xdkh0033DataReqDto获取业务值进行业务逻辑处理
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, JSON.toJSONString(xdkh0033DataRespDto));
        return xdkh0033DataRespDto;
    }
}
