/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpMgr
 * @类描述: cus_corp_mgr数据实体类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-03-22 14:57:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_corp_mgr")
public class CusCorpMgr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
	private String cusId;
	
	/** 关联客户编号 **/
	@Column(name = "CUS_ID_REL", unique = false, nullable = false, length = 30)
	private String cusIdRel;
	
	/** 高管类型 **/
	@Column(name = "MRG_TYPE", unique = false, nullable = true, length = 5)
	private String mrgType;

	/** 高管证件类型 STD_ZB_CERT_TYP **/
	@Column(name = "MRG_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String mrgCertType;
	
	/** 高管证件号码 **/
	@Column(name = "MRG_CERT_CODE", unique = false, nullable = true, length = 20)
	private String mrgCertCode;
	
	/** 高管姓名 **/
	@Column(name = "MRG_NAME", unique = false, nullable = true, length = 80)
	private String mrgName;
	
	/** 性别 STD_ZB_SEX **/
	@Column(name = "MRG_SEX", unique = false, nullable = true, length = 5)
	private String mrgSex;
	
	/** 出生日期 **/
	@Column(name = "MRG_BDAY", unique = false, nullable = true, length = 10)
	private String mrgBday;
	
	/** 高管职业 STD_ZB_OCC **/
	@Column(name = "MRG_OCC", unique = false, nullable = true, length = 5)
	private String mrgOcc;

	/** 高管职业 STD_ZB_OCC **/
	@Column(name = "CLATM_ORG", unique = false, nullable = true, length = 5)
	private String clatmOrg;
	
	/** 高管职务 STD_ZB_JOB_TTL **/
	@Column(name = "MRG_DUTY", unique = false, nullable = true, length = 5)
	private String mrgDuty;
	
	/** 高管职称 STD_ZB_TITLE **/
	@Column(name = "MRG_CRTF", unique = false, nullable = true, length = 5)
	private String mrgCrtf;
	
	/** 高管学历 STD_ZB_EDU **/
	@Column(name = "MRG_EDT", unique = false, nullable = true, length = 5)
	private String mrgEdt;
	
	/** 高管学位 STD_ZB_DEGREE **/
	@Column(name = "MRG_DGR", unique = false, nullable = true, length = 5)
	private String mrgDgr;
	
	/** 联系电话 **/
	@Column(name = "MRG_PHONE", unique = false, nullable = true, length = 35)
	private String mrgPhone;
	
	/** 签字样本开始日期 **/
	@Column(name = "SIGN_INIT_DATE", unique = false, nullable = true, length = 10)
	private String signInitDate;
	
	/** 签字样本到期日期 **/
	@Column(name = "SIGN_END_DATE", unique = false, nullable = true, length = 10)
	private String signEndDate;
	
	/** 授权书开始日期 **/
	@Column(name = "ACCREDIT_INIT_DATE", unique = false, nullable = true, length = 10)
	private String accreditInitDate;
	
	/** 授权书到期日期 **/
	@Column(name = "ACCREDIT_END_DATE", unique = false, nullable = true, length = 10)
	private String accreditEndDate;
	
	/** 工作简历 **/
	@Column(name = "RESUME", unique = false, nullable = true, length = 1000)
	private String resume;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 国别 STD_ZB_STATE_TYPE **/
	@Column(name = "COUNTRY", unique = false, nullable = true, length = 5)
	private String country;
	
	/** 证件到期日 **/
	@Column(name = "CERT_IDATE", unique = false, nullable = true, length = 10)
	private String certIdate;
	
	/** 从业日期 **/
	@Column(name = "FJOB_DATE", unique = false, nullable = true, length = 10)
	private String fjobDate;
	
	/** 个人净资产 **/
	@Column(name = "INDIV_NAS", unique = false, nullable = true, length = 5)
	private String indivNas;
	
	/** 本地居住状况 STD_ZB_LOCAL_RS **/
	@Column(name = "LOCAL_RESI_STATUS", unique = false, nullable = true, length = 5)
	private String localResiStatus;
	
	/** 居住地址 **/
	@Column(name = "RESI_ADDR", unique = false, nullable = true, length = 300)
	private String resiAddr;

	/** 持股比例 **/
	@Column(name = "SHD_PERC", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal shdPerc;

	/** 控股类型 **/
	@Column(name = "HOLD_TYPE", unique = false, nullable = true, length = 5)
	private String holdType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}

	public String getCusIdRel() {
		return cusIdRel;
	}

	public void setCusIdRel(String cusIdRel) {
		this.cusIdRel = cusIdRel;
	}

	/**
	 * @param mrgType
	 */
	public void setMrgType(String mrgType) {
		this.mrgType = mrgType;
	}
	
    /**
     * @return mrgType
     */
	public String getMrgType() {
		return this.mrgType;
	}

	/**
	 * @param mrgCertType
	 */
	public void setMrgCertType(String mrgCertType) {
		this.mrgCertType = mrgCertType;
	}

	/**
	 * @return mrgCertType
	 */
	public String getMrgCertType() {
		return mrgCertType;
	}

	/**
	 * @param mrgCertCode
	 */
	public void setMrgCertCode(String mrgCertCode) {
		this.mrgCertCode = mrgCertCode;
	}
	
    /**
     * @return mrgCertCode
     */
	public String getMrgCertCode() {
		return this.mrgCertCode;
	}
	
	/**
	 * @param mrgName
	 */
	public void setMrgName(String mrgName) {
		this.mrgName = mrgName;
	}
	
    /**
     * @return mrgName
     */
	public String getMrgName() {
		return this.mrgName;
	}
	
	/**
	 * @param mrgSex
	 */
	public void setMrgSex(String mrgSex) {
		this.mrgSex = mrgSex;
	}
	
    /**
     * @return mrgSex
     */
	public String getMrgSex() {
		return this.mrgSex;
	}
	
	/**
	 * @param mrgBday
	 */
	public void setMrgBday(String mrgBday) {
		this.mrgBday = mrgBday;
	}
	
    /**
     * @return mrgBday
     */
	public String getMrgBday() {
		return this.mrgBday;
	}
	
	/**
	 * @param mrgOcc
	 */
	public void setMrgOcc(String mrgOcc) {
		this.mrgOcc = mrgOcc;
	}
	
    /**
     * @return mrgOcc
     */
	public String getMrgOcc() {
		return this.mrgOcc;
	}

	public String getClatmOrg() {
		return clatmOrg;
	}

	public void setClatmOrg(String clatmOrg) {
		this.clatmOrg = clatmOrg;
	}

	/**
	 * @param mrgDuty
	 */
	public void setMrgDuty(String mrgDuty) {
		this.mrgDuty = mrgDuty;
	}
	
    /**
     * @return mrgDuty
     */
	public String getMrgDuty() {
		return this.mrgDuty;
	}
	
	/**
	 * @param mrgCrtf
	 */
	public void setMrgCrtf(String mrgCrtf) {
		this.mrgCrtf = mrgCrtf;
	}
	
    /**
     * @return mrgCrtf
     */
	public String getMrgCrtf() {
		return this.mrgCrtf;
	}
	
	/**
	 * @param mrgEdt
	 */
	public void setMrgEdt(String mrgEdt) {
		this.mrgEdt = mrgEdt;
	}
	
    /**
     * @return mrgEdt
     */
	public String getMrgEdt() {
		return this.mrgEdt;
	}
	
	/**
	 * @param mrgDgr
	 */
	public void setMrgDgr(String mrgDgr) {
		this.mrgDgr = mrgDgr;
	}
	
    /**
     * @return mrgDgr
     */
	public String getMrgDgr() {
		return this.mrgDgr;
	}
	
	/**
	 * @param mrgPhone
	 */
	public void setMrgPhone(String mrgPhone) {
		this.mrgPhone = mrgPhone;
	}
	
    /**
     * @return mrgPhone
     */
	public String getMrgPhone() {
		return this.mrgPhone;
	}
	
	/**
	 * @param signInitDate
	 */
	public void setSignInitDate(String signInitDate) {
		this.signInitDate = signInitDate;
	}
	
    /**
     * @return signInitDate
     */
	public String getSignInitDate() {
		return this.signInitDate;
	}
	
	/**
	 * @param signEndDate
	 */
	public void setSignEndDate(String signEndDate) {
		this.signEndDate = signEndDate;
	}
	
    /**
     * @return signEndDate
     */
	public String getSignEndDate() {
		return this.signEndDate;
	}
	
	/**
	 * @param accreditInitDate
	 */
	public void setAccreditInitDate(String accreditInitDate) {
		this.accreditInitDate = accreditInitDate;
	}
	
    /**
     * @return accreditInitDate
     */
	public String getAccreditInitDate() {
		return this.accreditInitDate;
	}
	
	/**
	 * @param accreditEndDate
	 */
	public void setAccreditEndDate(String accreditEndDate) {
		this.accreditEndDate = accreditEndDate;
	}
	
    /**
     * @return accreditEndDate
     */
	public String getAccreditEndDate() {
		return this.accreditEndDate;
	}
	
	/**
	 * @param resume
	 */
	public void setResume(String resume) {
		this.resume = resume;
	}
	
    /**
     * @return resume
     */
	public String getResume() {
		return this.resume;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	
    /**
     * @return country
     */
	public String getCountry() {
		return this.country;
	}
	
	/**
	 * @param certIdate
	 */
	public void setCertIdate(String certIdate) {
		this.certIdate = certIdate;
	}
	
    /**
     * @return certIdate
     */
	public String getCertIdate() {
		return this.certIdate;
	}
	
	/**
	 * @param fjobDate
	 */
	public void setFjobDate(String fjobDate) {
		this.fjobDate = fjobDate;
	}
	
    /**
     * @return fjobDate
     */
	public String getFjobDate() {
		return this.fjobDate;
	}
	
	/**
	 * @param indivNas
	 */
	public void setIndivNas(String indivNas) {
		this.indivNas = indivNas;
	}
	
    /**
     * @return indivNas
     */
	public String getIndivNas() {
		return this.indivNas;
	}
	
	/**
	 * @param localResiStatus
	 */
	public void setLocalResiStatus(String localResiStatus) {
		this.localResiStatus = localResiStatus;
	}
	
    /**
     * @return localResiStatus
     */
	public String getLocalResiStatus() {
		return this.localResiStatus;
	}
	
	/**
	 * @param resiAddr
	 */
	public void setResiAddr(String resiAddr) {
		this.resiAddr = resiAddr;
	}
	
    /**
     * @return resiAddr
     */
	public String getResiAddr() {
		return this.resiAddr;
	}

	public java.math.BigDecimal getShdPerc() {
		return shdPerc;
	}

	public void setShdPerc( java.math.BigDecimal shdPerc) {
		this.shdPerc = shdPerc;
	}

	public String getHoldType() {
		return holdType;
	}

	public void setHoldType(String holdType) {
		this.holdType = holdType;
	}

}