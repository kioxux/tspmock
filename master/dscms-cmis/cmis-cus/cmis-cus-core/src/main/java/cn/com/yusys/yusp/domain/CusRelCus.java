/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusRelCus
 * @类描述: cus_rel_cus数据实体类
 * @功能描述: 
 * @创建人: liqiang
 * @创建时间: 2021-05-07 21:29:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_rel_cus")
public class CusRelCus extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 关联编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CORRE_NO")
	private String correNo;
	
	/** 关联客户名称（主） **/
	@Column(name = "CORRE_CUS_NAME", unique = false, nullable = false, length = 40)
	private String correCusName;
	
	/** 关联客户编号（主） **/
	@Column(name = "CORRE_CUS_ID", unique = false, nullable = false, length = 40)
	private String correCusId;
	
	/** 认定说明 **/
	@Column(name = "IDENTY_MEMO", unique = false, nullable = false, length = 500)
	private String identyMemo;
	
	/** 解散原因 **/
	@Column(name = "DISMISS_RESN", unique = false, nullable = false, length = 20)
	private String dismissResn;
	
	/** 管户客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = false, length = 20)
	private String managerId;
	
	/** 所属机构 **/
	@Column(name = "BELG_ORG", unique = false, nullable = true, length = 20)
	private String belgOrg;
	
	/** 认定日期 **/
	@Column(name = "IDENTY_DATE", unique = false, nullable = false, length = 10)
	private String identyDate;
	
	/** 解散日期 **/
	@Column(name = "DISMISS_DATE", unique = false, nullable = false, length = 10)
	private String dismissDate;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = false, length = 5)
	private String status;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = false, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = false, length = 20)
	private String updId;
	
	/** 最后更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = false, length = 20)
	private String updBrId;
	
	/** 最后更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = false, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param correNo
	 */
	public void setCorreNo(String correNo) {
		this.correNo = correNo;
	}
	
    /**
     * @return correNo
     */
	public String getCorreNo() {
		return this.correNo;
	}
	
	/**
	 * @param correCusName
	 */
	public void setCorreCusName(String correCusName) {
		this.correCusName = correCusName;
	}
	
    /**
     * @return correCusName
     */
	public String getCorreCusName() {
		return this.correCusName;
	}
	
	/**
	 * @param correCusId
	 */
	public void setCorreCusId(String correCusId) {
		this.correCusId = correCusId;
	}
	
    /**
     * @return correCusId
     */
	public String getCorreCusId() {
		return this.correCusId;
	}
	
	/**
	 * @param identyMemo
	 */
	public void setIdentyMemo(String identyMemo) {
		this.identyMemo = identyMemo;
	}
	
    /**
     * @return identyMemo
     */
	public String getIdentyMemo() {
		return this.identyMemo;
	}
	
	/**
	 * @param dismissResn
	 */
	public void setDismissResn(String dismissResn) {
		this.dismissResn = dismissResn;
	}
	
    /**
     * @return dismissResn
     */
	public String getDismissResn() {
		return this.dismissResn;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg;
	}
	
    /**
     * @return belgOrg
     */
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param identyDate
	 */
	public void setIdentyDate(String identyDate) {
		this.identyDate = identyDate;
	}
	
    /**
     * @return identyDate
     */
	public String getIdentyDate() {
		return this.identyDate;
	}
	
	/**
	 * @param dismissDate
	 */
	public void setDismissDate(String dismissDate) {
		this.dismissDate = dismissDate;
	}
	
    /**
     * @return dismissDate
     */
	public String getDismissDate() {
		return this.dismissDate;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}