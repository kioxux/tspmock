package cn.com.yusys.yusp.service.server.xdqt0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdqt0008.req.Xdqt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0008.resp.Xdqt0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:乐悠金卡关联人查询
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdqt0008Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdqt0008Service.class);

    @Autowired
    private CusLstGlfMapper cusLstGlfMapper;

    /**
     * 乐悠金卡关联人查询
     *
     * @param xdqt0008DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdqt0008DataRespDto xdqt0008(Xdqt0008DataReqDto xdqt0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value);
        //返回对象
        Xdqt0008DataRespDto xdqt0008DataRespDto = new Xdqt0008DataRespDto();
        try {
            String certCode = xdqt0008DataReqDto.getCertCode();
            //证件号,上一级关联方证件号码查询
            int count = cusLstGlfMapper.selectCountByparebtRelatedPartyCertNo(certCode, certCode);

            if (count > 0) {
                xdqt0008DataRespDto.setOpFlag("1");
                xdqt0008DataRespDto.setOpMsg("此证件号为信贷关联人");
            } else {
                xdqt0008DataRespDto.setOpFlag("2");
                xdqt0008DataRespDto.setOpMsg("此证件号不为信贷关联人");
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value);
        return xdqt0008DataRespDto;
    }
}
