/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.vo.CusLstZlkhExportVo;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstZlkh;
import cn.com.yusys.yusp.repository.mapper.CusLstZlkhMapper;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstZlkhService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:17:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstZlkhService {

    @Autowired
    private CusLstZlkhMapper cusLstZlkhMapper;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private AdminSmOrgService adminSmOrgService;
    @Autowired
    private CusBaseMapper cusBaseMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstZlkh selectByPrimaryKey(String serno) {
        return cusLstZlkhMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstZlkh> selectAll(QueryModel model) {
        List<CusLstZlkh> records = (List<CusLstZlkh>) cusLstZlkhMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstZlkh> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstZlkh> list = cusLstZlkhMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstZlkh record) {
        return cusLstZlkhMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstZlkh record) {
        return cusLstZlkhMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstZlkh record) {
        return cusLstZlkhMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstZlkh record) {
        return cusLstZlkhMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstZlkhMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstZlkhMapper.deleteByIds(ids);
    }

    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusLstZlkhExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param perCustInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> perCustInfoList) {
        List<CusLstZlkh> cusLstZlkhList = (List<CusLstZlkh>) BeanUtils.beansCopy(perCustInfoList, CusLstZlkh.class);
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CusLstZlkhMapper cusLstZlkhMapper = sqlSession.getMapper(CusLstZlkhMapper.class);
            for (CusLstZlkh cusLstZlkh : cusLstZlkhList) {
                if (StringUtil.isEmpty(cusLstZlkh.getCusId())) {
                    throw BizException.error(null, "9999", "客户编号不能为空");
                }
                if (StringUtil.isEmpty(cusLstZlkh.getCusName())) {
                    throw BizException.error(null, "9999", "客户名称不能为空");
                }
                if (StringUtil.isEmpty(cusLstZlkh.getManagerId())) {
                    throw BizException.error(null, "9999", "管户客户经理不能为空");
                }
                if (StringUtil.isEmpty(cusLstZlkh.getBelgOrg())) {
                    throw BizException.error(null, "9999", "所属机构不能为空");
                }
                ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(cusLstZlkh.getManagerId());
                if (manager.getData().getLoginCode() == null) {
                    throw BizException.error(null, "9999", "管户客户经理号不存在");
                }
                ResultDto<AdminSmOrgDto> Sorg = adminSmOrgService.getByOrgCode(cusLstZlkh.getBelgOrg());
                if (Sorg.getData().getOrgId() == null) {
                    throw BizException.error(null, "9999", "所属机构号不存在");
                }
                QueryModel cqm = new QueryModel();
                cqm.getCondition().put("cusId", cusLstZlkh.getCusId());
                List<CusBase> exists = cusBaseMapper.selectByModel(cqm);
                if(exists.isEmpty()){
                    throw BizException.error(null, "9999", cusLstZlkh.getCusId()+"客户编号不存在");
                }else {
                    for (int i = 0; i<exists.size();i++) {
                        if(!exists.get(i).getCusName().equals(cusLstZlkh.getCusName())){
                            throw BizException.error(null, "9999", "导入的客户编号:"+cusLstZlkh.getCusId()+"对应的客户姓名"
                                    +cusLstZlkh.getCusName()+"与客户信息的名称"+exists.get(i).getCusName()+"不一致");
                        }
                    }
                }
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                cusLstZlkh.setSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>()));
                cusLstZlkh.setInputBrId(userInfo.getOrg().getCode());
                cusLstZlkh.setInputId(userInfo.getLoginCode());
                cusLstZlkh.setInputDate(date);
                cusLstZlkh.setCreateTime(date1);
                cusLstZlkh.setUpdateTime(date1);
                cusLstZlkh.setUpdId(userInfo.getLoginCode());
                cusLstZlkh.setStatus("1");
                cusLstZlkh.setUpdBrId(userInfo.getOrg().getCode());
                cusLstZlkhMapper.insertSelective(cusLstZlkh);
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perCustInfoList.size();
    }
}
