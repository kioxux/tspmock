package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class IndivCusQueryDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String cusId;  // 客户编号
    private String name;  // 别称
    private String nation;  // 国籍
    private String sex;  // 性别
    private String isLongIndiv;  // 是否为长期证件
    private String certStartDt;  // 证件签发日期
    private String certEndDt;  // 证件到期日期
    private String agriFlg;  // 是否为农户
    private String indivFolk;  // 民族
    private String indivBrtPlace;  // 籍贯
    private String indivHouhRegAdd;  // 户籍地址
    private String visitStreet;  // 街道
    private String indivDtOfBirth;  // 出生日期
    private String indivPolSt;  // 政治面貌
    private String indivEdt;  // 最高学历
    private String indivDgr;  // 最高学位
    private String isHaveChildren;  // 是否有子女
    private String marStatus;  // 婚姻状况
    private String healthStatus;  // 健康状况
    private String initLoanDate;  // 建立信贷关系时间
    private String indivHldAcnt;  // 在我行建立业务情况
    private String isBankEmployee;  // 是否我行员工
    private String bankDuty;  // 在我行职务
    private String isBankSharehd;  // 是否我行股东
    private String utilEmployeeMode;  // 用工形式
    private String employeeCls;  // 人员类别
    private String loanCardFlg;  // 是否有中征码
    private String loanCardId;  // 中征码
    private String isMainCus;  // 是否重点客户
    private String cusType;  // 客户类型
    private String imageNo;  // 影像编号
    private String inputId;  // 登记人
    private String inputBrId;  // 登记机构
    private String inputDate;  // 登记日期
    private String updId;  // 更新人
    private String updBrId;  // 更新机构
    private String updDate;  // 更新日期
    private java.util.Date createTime;  // 创建时间
    private java.util.Date updateTime;  // 修改时间
    private String saveStatus;  // 保存状态
    private String cusName;   // 客户名称
    private String certType;  // 证件类型
    private String certCode;  // 证件号码

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getIsLongIndiv() {
        return isLongIndiv;
    }

    public void setIsLongIndiv(String isLongIndiv) {
        this.isLongIndiv = isLongIndiv;
    }

    public String getCertStartDt() {
        return certStartDt;
    }

    public void setCertStartDt(String certStartDt) {
        this.certStartDt = certStartDt;
    }

    public String getCertEndDt() {
        return certEndDt;
    }

    public void setCertEndDt(String certEndDt) {
        this.certEndDt = certEndDt;
    }

    public String getAgriFlg() {
        return agriFlg;
    }

    public void setAgriFlg(String agriFlg) {
        this.agriFlg = agriFlg;
    }

    public String getIndivFolk() {
        return indivFolk;
    }

    public void setIndivFolk(String indivFolk) {
        this.indivFolk = indivFolk;
    }

    public String getIndivBrtPlace() {
        return indivBrtPlace;
    }

    public void setIndivBrtPlace(String indivBrtPlace) {
        this.indivBrtPlace = indivBrtPlace;
    }

    public String getIndivHouhRegAdd() {
        return indivHouhRegAdd;
    }

    public void setIndivHouhRegAdd(String indivHouhRegAdd) {
        this.indivHouhRegAdd = indivHouhRegAdd;
    }

    public String getVisitStreet() {
        return visitStreet;
    }

    public void setVisitStreet(String visitStreet) {
        this.visitStreet = visitStreet;
    }

    public String getIndivDtOfBirth() {
        return indivDtOfBirth;
    }

    public void setIndivDtOfBirth(String indivDtOfBirth) {
        this.indivDtOfBirth = indivDtOfBirth;
    }

    public String getIndivPolSt() {
        return indivPolSt;
    }

    public void setIndivPolSt(String indivPolSt) {
        this.indivPolSt = indivPolSt;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public String getIndivDgr() {
        return indivDgr;
    }

    public void setIndivDgr(String indivDgr) {
        this.indivDgr = indivDgr;
    }

    public String getIsHaveChildren() {
        return isHaveChildren;
    }

    public void setIsHaveChildren(String isHaveChildren) {
        this.isHaveChildren = isHaveChildren;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public String getInitLoanDate() {
        return initLoanDate;
    }

    public void setInitLoanDate(String initLoanDate) {
        this.initLoanDate = initLoanDate;
    }

    public String getIndivHldAcnt() {
        return indivHldAcnt;
    }

    public void setIndivHldAcnt(String indivHldAcnt) {
        this.indivHldAcnt = indivHldAcnt;
    }

    public String getIsBankEmployee() {
        return isBankEmployee;
    }

    public void setIsBankEmployee(String isBankEmployee) {
        this.isBankEmployee = isBankEmployee;
    }

    public String getBankDuty() {
        return bankDuty;
    }

    public void setBankDuty(String bankDuty) {
        this.bankDuty = bankDuty;
    }

    public String getIsBankSharehd() {
        return isBankSharehd;
    }

    public void setIsBankSharehd(String isBankSharehd) {
        this.isBankSharehd = isBankSharehd;
    }

    public String getUtilEmployeeMode() {
        return utilEmployeeMode;
    }

    public void setUtilEmployeeMode(String utilEmployeeMode) {
        this.utilEmployeeMode = utilEmployeeMode;
    }

    public String getEmployeeCls() {
        return employeeCls;
    }

    public void setEmployeeCls(String employeeCls) {
        this.employeeCls = employeeCls;
    }

    public String getLoanCardFlg() {
        return loanCardFlg;
    }

    public void setLoanCardFlg(String loanCardFlg) {
        this.loanCardFlg = loanCardFlg;
    }

    public String getLoanCardId() {
        return loanCardId;
    }

    public void setLoanCardId(String loanCardId) {
        this.loanCardId = loanCardId;
    }

    public String getIsMainCus() {
        return isMainCus;
    }

    public void setIsMainCus(String isMainCus) {
        this.isMainCus = isMainCus;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSaveStatus() {
        return saveStatus;
    }

    public void setSaveStatus(String saveStatus) {
        this.saveStatus = saveStatus;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }
}
