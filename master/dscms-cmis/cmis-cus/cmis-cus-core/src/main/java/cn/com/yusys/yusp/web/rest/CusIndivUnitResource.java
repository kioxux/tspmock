/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusIndivUnit;
import cn.com.yusys.yusp.service.CusIndivUnitService;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusIndivUnitResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-11-16 11:44:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags= "个人客户工作属性")
@RestController
@RequestMapping("/api/cusindivunit")
public class CusIndivUnitResource {
    @Autowired
    private CusIndivUnitService cusIndivUnitService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusIndivUnit>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusIndivUnit> list = cusIndivUnitService.selectAll(queryModel);
        return new ResultDto<List<CusIndivUnit>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusIndivUnit>> index(QueryModel queryModel) {
        List<CusIndivUnit> list = cusIndivUnitService.selectByModel(queryModel);
        return new ResultDto<List<CusIndivUnit>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusIndivUnit> show(@PathVariable("pkId") String pkId) {
        CusIndivUnit cusIndivUnit = cusIndivUnitService.selectByPrimaryKey(pkId);
        return new ResultDto<CusIndivUnit>(cusIndivUnit);
    }

    /**
     * @函数名称:show
     * @函数描述:通过客户号查询工作信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "个人客户工作查询")
    @PostMapping("/queryByCusId")
    protected ResultDto<CusIndivUnit> queryByCusId(@Validated @RequestBody CusIndivUnit cusIndivUnit) {
        CusIndivUnit cusIndivUnits = cusIndivUnitService.selectBycusId(cusIndivUnit.getCusId());
        return new ResultDto<CusIndivUnit>(cusIndivUnits);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusIndivUnit> create(@RequestBody CusIndivUnit cusIndivUnit) throws URISyntaxException {
        cusIndivUnitService.insert(cusIndivUnit);
        return new ResultDto<CusIndivUnit>(cusIndivUnit);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusIndivUnit cusIndivUnit) throws URISyntaxException {
        int result = cusIndivUnitService.update(cusIndivUnit);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusIndivUnitService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusIndivUnitService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @函数名称:save
     * @函数描述: 客户工作信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "个人客户工作信息保存")
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody CusIndivUnit cusIndivUnit) throws URISyntaxException {
        int result =cusIndivUnitService.save(cusIndivUnit);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:通过客户号查询工作信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbycusid")
    protected ResultDto<CusIndivUnit> selectBycusId(@RequestBody String  cusId) {
        CusIndivUnit cusIndivUnits = cusIndivUnitService.selectBycusId(cusId);
        return new ResultDto<CusIndivUnit>(cusIndivUnits);
    }

}
