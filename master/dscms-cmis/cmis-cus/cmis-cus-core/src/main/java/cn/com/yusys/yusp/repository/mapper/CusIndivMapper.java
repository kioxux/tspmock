/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-cus模块
 * @类名称: CusIndivMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: www58
 * @创建时间: 2020-11-14 14:41:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CusIndivMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CusIndiv selectByPrimaryKey(@Param("cusId") String cusId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CusIndiv> selectByModel(QueryModel model);

    /**
     * @方法名称: queryindivCus
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<Map<String,Object>> queryindivCus(QueryModel model);

	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CusIndiv record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CusIndiv record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CusIndiv record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CusIndiv record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("cusId") String cusId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 个人客户引入查询
     * @param indivCusCreateDto
     * @return
     */
    IndivCusCreateDto selectCusIndivDto(IndivCusCreateDto indivCusCreateDto);

    /**
     * 通过客户号查询评级信息
     * @param cusId
     * @return
     */
    CusIndivRankScoreDto selectRankScoreByCusId(@Param("cusId") String cusId);

    /**
     * 通过客户号修改建立信贷关系
     * @param cusIndivDto
     * @return
     */
    int updateComInitLoanDateBycusId(CusIndivReqClientDto cusIndivDto);

    /**
     * 通过客户号查询个人客户信息
     * @param
     * @author zhoumw
     * @return IndivCusQueryDto
     */
    IndivCusQueryDto queryCusIndivInfo(@Param("cusId") String cusId);


    List<Map<String,Object>> selectByModelxp(QueryModel model);



    /**
     * 个人客户基本信息查询接口
     * @param map
     * @return
     */
    Xdkh0001DataRespDto getIndivCusBaseInfo(Map map);

    /**
     * 查询直系亲属是否为本行员工
     * @param cusId
     * @return
     */
    int isImmeFamily(@Param("cusId") String cusId);

    /**
     * @方法名称: queryCusIndivByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    List<CusIndivDto> queryCusIndivByModel(QueryModel model);

    /**
     * @方法名称: queryAllCusIndivByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：周茂伟
     */
    List<CusIndivAllDto> queryAllCusIndivByModel(QueryModel model);

    /**
     * @方法名称: queryCusIndivByCertcode
     * @方法描述: 查看该身份证号码是否已经被开户
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人： ouyangjie
     */
    String queryCusBaseByCertcode(@Param("certCode") String certCode);
    /**
     * @方法名称: queryCusIndivByCertcodeAndcusState
     * @方法描述: 查看该身份证号码的客户的状态是否不是正式客户
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人： ouyangjie
     */
    String queryCusBaseByCertcodeAndcusState(@Param("certCode") String certCode);
    /**
     * @param model
     * @return
     * @方法描述:更新客户号、日期和客户状态为正式客户
     * @创建人:ouyangjie
     */
    int updateCusStatesFromCusBase(QueryModel model);


    /**
     * @param model
     * @return
     * @方法描述:更新客户号、日期和客户状态为正式客户
     * @创建人:ouyangjie
     */
    int updateCusStatesFromCusindiv(QueryModel model);
    /**
     * @方法名称: queryCusIndivByCusId
     * @方法描述: 查看该身份证号码是否已经被开户
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人： ouyangjie
     */
    String queryCusIndivByCusId(@Param("cusId") String cusId);

    /**
     * @param model
     * @return
     * @方法描述: 插入cusIndiv一些信息
     * @创建人:ouyangjie
     */
    int inertCusIndivSomeInfo(QueryModel model);

    /**
     * @param model
     * @return
     * @方法描述:更新从东方微银获取的数据至cusIndiv
     * @创建人:ouyangjie
     */
    int updateZxInfoByCusid(QueryModel model);

    /**
     * @方法名称: selectCusInDivByModel
     * @方法描述: 根据高管证件类型，证件号查询高管信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    CusIndivDto getCertByCodeAndType(CusBase cusBase);

    /**
     * @方法名称: getCusIndivDtoList
     * @方法描述: 查询cusIndiv关联cusBase
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CusIndivDto> getCusIndivDtoList(QueryModel queryModel);


    /**
     * @方法名称: selectByCondition
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusIndiv> selectByCondition(QueryModel model);

    /**
     * @方法名称: selectByCusIds
     * @方法描述: 通过CusId集合查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CusIndivAllDto> selectByCusIds(List<String> ids);

    /**
     * @函数名称: selectCusAllInfo
     * @函数描述: 查询页面的基本信息
     * @参数与返回说明:
     * @算法描述:
     */
    Map<String, String> selectCusAllInfo(@Param("cusId") String cusId);
}