/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBatPubHandoverLst
 * @类描述: cus_bat_pub_handover_lst数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-05-05 10:09:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_bat_pub_handover_lst")
public class CusBatPubHandoverLst extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "CBPHA_SERNO", unique = false, nullable = false, length = 40)
	private String cbphaSerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 30)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 移出人工号 **/
	@Column(name = "HANDOVER_ID", unique = false, nullable = true, length = 20)
	private String handoverId;
	
	/** 移出人姓名 **/
	@Column(name = "HANDOVER_NAME", unique = false, nullable = true, length = 255)
	private String handoverName;
	
	/** 接收人工号 **/
	@Column(name = "RECEIVER_ID", unique = false, nullable = true, length = 20)
	private String receiverId;
	
	/** 接收人姓名 **/
	@Column(name = "RECEIVER_NAME", unique = false, nullable = true, length = 255)
	private String receiverName;
	
	/** 处理结果 **/
	@Column(name = "PRC_RST", unique = false, nullable = true, length = 255)
	private String prcRst;
	
	/** 结果描述 **/
	@Column(name = "RST_DEC", unique = false, nullable = true, length = 255)
	private String rstDec;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cbphaSerno
	 */
	public void setCbphaSerno(String cbphaSerno) {
		this.cbphaSerno = cbphaSerno;
	}
	
    /**
     * @return cbphaSerno
     */
	public String getCbphaSerno() {
		return this.cbphaSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param handoverId
	 */
	public void setHandoverId(String handoverId) {
		this.handoverId = handoverId;
	}
	
    /**
     * @return handoverId
     */
	public String getHandoverId() {
		return this.handoverId;
	}
	
	/**
	 * @param handoverName
	 */
	public void setHandoverName(String handoverName) {
		this.handoverName = handoverName;
	}
	
    /**
     * @return handoverName
     */
	public String getHandoverName() {
		return this.handoverName;
	}
	
	/**
	 * @param receiverId
	 */
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	
    /**
     * @return receiverId
     */
	public String getReceiverId() {
		return this.receiverId;
	}
	
	/**
	 * @param receiverName
	 */
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	
    /**
     * @return receiverName
     */
	public String getReceiverName() {
		return this.receiverName;
	}
	
	/**
	 * @param prcRst
	 */
	public void setPrcRst(String prcRst) {
		this.prcRst = prcRst;
	}
	
    /**
     * @return prcRst
     */
	public String getPrcRst() {
		return this.prcRst;
	}
	
	/**
	 * @param rstDec
	 */
	public void setRstDec(String rstDec) {
		this.rstDec = rstDec;
	}
	
    /**
     * @return rstDec
     */
	public String getRstDec() {
		return this.rstDec;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}