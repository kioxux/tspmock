package cn.com.yusys.yusp.service.server.cmiscus0024;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusIntbank;
import cn.com.yusys.yusp.dto.server.cmiscus0024.req.CmisCus0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0024.resp.CmisCus0024RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusIntbankService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CmisCus0024Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0024Service.class);

    @Autowired
    private CusIntbankService cusIntbankService;

    /**
     * 根据行号或BICCODE获取同业客户号
     * @param reqDto
     * @return
     * @throws YuspException
     */
    public CmisCus0024RespDto execute(CmisCus0024ReqDto reqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0024.key, DscmsEnum.TRADE_CODE_CMISCUS0024.value);

        CmisCus0024RespDto cmisCus0024RespDto = new CmisCus0024RespDto();
        String bankNo = reqDto.getBankNo();
        String bicCode = reqDto.getBicCode();
        try {
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("largeBankNo",bankNo);
            queryModel.addCondition("swiftCde",bicCode);
            List<CusIntbank> cusIntbanks = cusIntbankService.selectByModel(queryModel);
            if (CollectionUtils.nonEmpty(cusIntbanks)){
                cmisCus0024RespDto.setCusId(cusIntbanks.get(0).getCusId());
            }
            cmisCus0024RespDto.setErrorCode(SuccessEnum.SUCCESS.key);
            cmisCus0024RespDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsEnum.TRADE_CODE_CMISCUS0024.key+"报错：",e);
            cmisCus0024RespDto.setErrorCode(e.getCode());
            cmisCus0024RespDto.setErrorMsg(e.getMsg());
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0024.key, DscmsEnum.TRADE_CODE_CMISCUS0024.value);

        return cmisCus0024RespDto;
    }
}
