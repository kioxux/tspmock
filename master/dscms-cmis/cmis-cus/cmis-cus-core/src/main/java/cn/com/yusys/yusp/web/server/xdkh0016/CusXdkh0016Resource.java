package cn.com.yusys.yusp.web.server.xdkh0016;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0016.req.Xdkh0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0016.resp.Xdkh0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0016.Xdkh0016Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询是否我行关系人
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0016:查询是否我行关系人")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0016Resource.class);

    @Autowired
    private Xdkh0016Service xdkh0016Service;

    /**
     * 交易码：xdkh0016
     * 交易描述：查询是否我行关系人
     *
     * @param xdkh0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询是否我行关系人")
    @PostMapping("/xdkh0016")
    protected @ResponseBody
    ResultDto<Xdkh0016DataRespDto> xdkh0016(@Validated @RequestBody Xdkh0016DataReqDto xdkh0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, JSON.toJSONString(xdkh0016DataReqDto));
        Xdkh0016DataRespDto xdkh0016DataRespDto = new Xdkh0016DataRespDto();// 响应Dto:查询是否我行关系人
        ResultDto<Xdkh0016DataRespDto> xdkh0016DataResultDto = new ResultDto<>();
        // 从xdkh0016DataReqDto获取业务值进行业务逻辑处理
        String certNo = xdkh0016DataReqDto.getCertNo();//证件号码
        try {
            // 调用xdkh0016Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, certNo);
            int count = xdkh0016Service.selectRelationCount(certNo);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, count);
            if (count > 0) {
                xdkh0016DataRespDto.setOpFlg("Y");// 是否在我行关系人名单中
            } else {
                xdkh0016DataRespDto.setOpFlg("N");// 是否在我行关系人名单中
            }
            xdkh0016DataRespDto.setQnt(count);// 数量
            // 封装xdkh0016DataResultDto中正确的返回码和返回信息
            xdkh0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, e.getMessage());
            // 封装xdkh0016DataResultDto中异常返回码和返回信息
            xdkh0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0016DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0016DataRespDto到xdkh0016DataResultDto中
        xdkh0016DataResultDto.setData(xdkh0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, JSON.toJSONString(xdkh0016DataRespDto));
        return xdkh0016DataResultDto;
    }
}
