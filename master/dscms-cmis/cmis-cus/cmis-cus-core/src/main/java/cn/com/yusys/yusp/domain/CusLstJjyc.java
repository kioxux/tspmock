/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.annotation.SeqId;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstJjyc
 * @类描述: cus_lst_jjyc数据实体类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_jjyc")
public class CusLstJjyc extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@SeqId("SERNO")
	@Column(name = "SERNO")
	private String serno;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 20)
	private String cusName;

	/** 依存客户编号 **/
	@Column(name = "DEP_CUS_NO", unique = false, nullable = true, length = 20)
	private String depCusNo;

	/** 依存客户名称 **/
	@Column(name = "DEP_CUS_NAME", unique = false, nullable = true, length = 40)
	private String depCusName;

	/** 经济依存客户识别因素 **/
	@Column(name = "ECO_DEP_CUS_DIST_FACTOR", unique = false, nullable = true, length = 40)
	private String ecoDepCusDistFactor;

	/** 情况说明 **/
	@Column(name = "CASE_MOME", unique = false, nullable = true, length = 40)
	private String caseMome;

	/** 管户客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 所属机构 **/
	@Column(name = "BELG_ORG", unique = false, nullable = true, length = 20)
	private String belgOrg;

	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 5)
	private String status;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param depCusNo
	 */
	public void setDepCusNo(String depCusNo) {
		this.depCusNo = depCusNo;
	}

    /**
     * @return depCusNo
     */
	public String getDepCusNo() {
		return this.depCusNo;
	}
	
	/**
	 * @param depCusName
	 */
	public void setDepCusName(String depCusName) {
		this.depCusName = depCusName;
	}
	
    /**
     * @return depCusName
     */
	public String getDepCusName() {
		return this.depCusName;
	}
	
	/**
	 * @param ecoDepCusDistFactor
	 */
	public void setEcoDepCusDistFactor(String ecoDepCusDistFactor) {
		this.ecoDepCusDistFactor = ecoDepCusDistFactor;
	}
	
    /**
     * @return ecoDepCusDistFactor
     */
	public String getEcoDepCusDistFactor() {
		return this.ecoDepCusDistFactor;
	}
	
	/**
	 * @param caseMome
	 */
	public void setCaseMome(String caseMome) {
		this.caseMome = caseMome;
	}
	
    /**
     * @return caseMome
     */
	public String getCaseMome() {
		return this.caseMome;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg;
	}
	
    /**
     * @return belgOrg
     */
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}