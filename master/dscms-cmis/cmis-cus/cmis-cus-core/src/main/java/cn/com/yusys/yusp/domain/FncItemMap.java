/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncItemMap
 * @类描述: fnc_item_map数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 14:19:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "fnc_item_map")
public class FncItemMap extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** serno **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "serno")
	private String serno;
	
	/** 信贷科目号 **/
	@Column(name = "fnc_item_id", unique = false, nullable = true, length = 40)
	private String fncItemId;
	
	/** 信贷科目名 **/
	@Column(name = "fnc_item_name", unique = false, nullable = true, length = 40)
	private String fncItemName;
	
	/** OCR科目编码 **/
	@Column(name = "ocr_subject_id", unique = true, nullable = true, length = 40)
	private String ocrSubjectId;
	
	/** OCR科目名 **/
	@Column(name = "ocr_subject_name", unique = false, nullable = true, length = 40)
	private String ocrSubjectName;
	
	/** 报表样式编号 **/
	@Column(name = "style_id", unique = false, nullable = false, length = 6)
	private String styleId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param fncItemId
	 */
	public void setFncItemId(String fncItemId) {
		this.fncItemId = fncItemId;
	}
	
    /**
     * @return fncItemId
     */
	public String getFncItemId() {
		return this.fncItemId;
	}
	
	/**
	 * @param fncItemName
	 */
	public void setFncItemName(String fncItemName) {
		this.fncItemName = fncItemName;
	}
	
    /**
     * @return fncItemName
     */
	public String getFncItemName() {
		return this.fncItemName;
	}
	
	/**
	 * @param ocrSubjectId
	 */
	public void setOcrSubjectId(String ocrSubjectId) {
		this.ocrSubjectId = ocrSubjectId;
	}
	
    /**
     * @return ocrSubjectId
     */
	public String getOcrSubjectId() {
		return this.ocrSubjectId;
	}
	
	/**
	 * @param ocrSubjectName
	 */
	public void setOcrSubjectName(String ocrSubjectName) {
		this.ocrSubjectName = ocrSubjectName;
	}
	
    /**
     * @return ocrSubjectName
     */
	public String getOcrSubjectName() {
		return this.ocrSubjectName;
	}
	
	/**
	 * @param styleId
	 */
	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}
	
    /**
     * @return styleId
     */
	public String getStyleId() {
		return this.styleId;
	}


}