package cn.com.yusys.yusp.fncstat.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: FinanIndicAnaly
 * @类描述: finan_indic_analy数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-31 16:12:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class FinanIndicAnalyDto implements Serializable{
	private static final long serialVersionUID = 1L;

	
	/** 项目编号 **/
	private String itemId;
	
	/** 项目名称 **/
	private String itemName;
	
	/** 录入年月 **/
	private String inputYear;
	
	/** 当前年月数值 **/
	private java.math.BigDecimal curYmValue;
	
	/** 前一年数值 **/
	private java.math.BigDecimal nearFirstValue;
	
	/** 前二年数值 **/
	private java.math.BigDecimal nearSecondValue;

	/** 财务指标分组序列 **/
	private String finanIndicGroup;
	
	/**
	 * @param itemId
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId == null ? null : itemId.trim();
	}
	
    /**
     * @return ItemId
     */	
	public String getItemId() {
		return this.itemId;
	}
	
	/**
	 * @param itemName
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName == null ? null : itemName.trim();
	}
	
    /**
     * @return ItemName
     */	
	public String getItemName() {
		return this.itemName;
	}

	/**
	 * @param inputYear
	 */
	public void setInputYear(String inputYear) {
		this.inputYear = inputYear == null ? null : inputYear.trim();
	}
	
    /**
     * @return InputYear
     */	
	public String getInputYear() {
		return this.inputYear;
	}
	
	/**
	 * @param curYmValue
	 */
	public void setCurYmValue(java.math.BigDecimal curYmValue) {
		this.curYmValue = curYmValue;
	}
	
    /**
     * @return CurYmValue
     */	
	public java.math.BigDecimal getCurYmValue() {
		return this.curYmValue;
	}
	
	/**
	 * @param nearFirstValue
	 */
	public void setNearFirstValue(java.math.BigDecimal nearFirstValue) {
		this.nearFirstValue = nearFirstValue;
	}
	
    /**
     * @return NearFirstValue
     */	
	public java.math.BigDecimal getNearFirstValue() {
		return this.nearFirstValue;
	}
	
	/**
	 * @param nearSecondValue
	 */
	public void setNearSecondValue(java.math.BigDecimal nearSecondValue) {
		this.nearSecondValue = nearSecondValue;
	}
	
    /**
     * @return NearSecondValue
     */	
	public java.math.BigDecimal getNearSecondValue() {
		return this.nearSecondValue;
	}
	
	/**
	 * @param finanIndicGroup
	 */
	public void setFinanIndicGroup(String finanIndicGroup) {
		this.finanIndicGroup = finanIndicGroup == null ? null : finanIndicGroup.trim();
	}
	
    /**
     * @return FinanIndicGroup
     */	
	public String getFinanIndicGroup() {
		return this.finanIndicGroup;
	}


}