/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusPubHandoverApp;
import cn.com.yusys.yusp.domain.CusPubHandoverLst;
import cn.com.yusys.yusp.dto.CusPubHandoverAppDto;
import cn.com.yusys.yusp.repository.mapper.CusPubHandoverAppMapper;
import cn.com.yusys.yusp.repository.mapper.CusPubHandoverLstMapper;
import cn.com.yusys.yusp.web.rest.CusPubHandoverAppResource;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusPubHandoverAppService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: pc
 * @创建时间: 2021-05-10 14:12:48
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusPubHandoverAppService {
    private static final Logger log = LoggerFactory.getLogger(CusPubHandoverAppResource.class);
    @Autowired
    private CusPubHandoverAppMapper cusPubHandoverAppMapper;

    @Autowired
    private CusPubHandoverLstMapper cusPubHandoverLstMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusPubHandoverApp selectByPrimaryKey(String serno) {
        return cusPubHandoverAppMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusPubHandoverApp> selectAll(QueryModel model) {
        List<CusPubHandoverApp> records = (List<CusPubHandoverApp>) cusPubHandoverAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusPubHandoverApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusPubHandoverApp> list = cusPubHandoverAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusPubHandoverApp record) {
        return cusPubHandoverAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusPubHandoverApp record) {
        return cusPubHandoverAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusPubHandoverApp record) {
        return cusPubHandoverAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusPubHandoverApp record) {
        return cusPubHandoverAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusPubHandoverAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusPubHandoverAppMapper.deleteByIds(ids);
    }

    /**
     * 数据插入或者更新
     *
     * @param cusPubHandoverAppDto
     */
    @Transactional(rollbackFor = Exception.class)
    public void insertHandoverApp(CusPubHandoverAppDto cusPubHandoverAppDto) {
        CusPubHandoverApp app = cusPubHandoverAppDto.getCusPubHandoverApp();
        CusPubHandoverApp appIsSaved = cusPubHandoverAppMapper.selectByPrimaryKey(app.getSerno());
        if (appIsSaved != null) {
            cusPubHandoverAppMapper.updateByPrimaryKey(app);
        } else {
            cusPubHandoverAppMapper.insert(app);
        }
        List<CusPubHandoverLst> list = cusPubHandoverAppDto.getList();
        list.stream().forEach(lst -> {
            if (org.apache.commons.lang.StringUtils.isNotBlank(lst.getPkId())) {
                cusPubHandoverLstMapper.updateByPrimaryKey(lst);
            } else {
                lst.setPkId(UUID.randomUUID().toString().replaceAll("-", ""));
                cusPubHandoverLstMapper.insert(lst);
            }
        });
    }
}
