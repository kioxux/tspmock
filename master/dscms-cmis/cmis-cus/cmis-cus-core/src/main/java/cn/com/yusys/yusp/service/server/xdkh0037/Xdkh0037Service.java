package cn.com.yusys.yusp.service.server.xdkh0037;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0037.req.Xdkh0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0037.resp.Xdkh0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMapper;
import cn.com.yusys.yusp.service.CusCorpService;
import cn.com.yusys.yusp.service.Dscms2EcifClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 接口处理类:微业贷新开户信息入库
 *
 * @author zdl
 * @version 1.0
 */
@Service
public class Xdkh0037Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0037Service.class);

    private static final String[] citys = {"常熟高新技术产业开发区",
            "常熟经济技术开发区",
            "常州高新技术产业开发区",
            "海安经济技术开发区",
            "海门经济技术开发区",
            "淮安高新技术产业开发区",
            "淮安经济技术开发区",
            "江宁经济技术开发区",
            "江苏宝应经济开发区",
            "江苏滨海经济开发区",
            "江苏常州滨江经济开发区",
            "江苏常州经济开发区",
            "江苏常州天宁经济开发区",
            "江苏常州钟楼经济开发区",
            "江苏大丰港经济开发区",
            "江苏大丰经济开发区",
            "江苏丹徒经济开发区",
            "江苏丹阳经济开发区",
            "江苏东海经济开发区",
            "江苏东台经济开发区",
            "江苏丰县经济开发区",
            "江苏阜宁经济开发区",
            "江苏赣榆海洋经济开发区",
            "江苏赣榆经济开发区",
            "江苏高淳经济开发区",
            "江苏高邮经济开发区",
            "江苏灌南经济开发区",
            "江苏灌云经济开发区",
            "江苏海门工业园区",
            "江苏海州经济开发区",
            "江苏洪泽经济开发区",
            "江苏淮安工业园区",
            "江苏淮安经济开发区",
            "江苏淮安清河经济开发区",
            "江苏建湖经济开发区",
            "江苏姜堰经济开发区",
            "江苏江都经济开发区",
            "江苏江阴-靖江工业园区",
            "江苏江阴临港经济开发区",
            "江苏金湖经济开发区",
            "江苏金坛经济开发区",
            "江苏句容经济开发区",
            "江苏昆山花桥经济开发区",
            "江苏连云港化学工业园区",
            "江苏连云港徐圩经济开发区",
            "江苏连云经济开发区",
            "江苏涟水经济开发区",
            "江苏南京生态科技岛经济开发区",
            "江苏南通崇川经济开发区",
            "江苏南通港闸经济开发区",
            "江苏南通苏通科技产业园区",
            "江苏南通通州湾经济开发区",
            "江苏沛北经济开发区",
            "江苏沛县经济开发区",
            "江苏启东经济开发区",
            "江苏启东吕四港经济开发区",
            "江苏如东经济开发区",
            "江苏如东洋口港经济开发区",
            "江苏射阳港经济开发区",
            "江苏射阳经济开发区",
            "江苏省丹阳高新技术产业开发区",
            "江苏省东海高新技术产业开发区",
            "江苏省汾湖高新技术产业开发区",
            "江苏省高淳高新技术产业开发区",
            "江苏省海安高新技术产业开发区",
            "江苏省杭集高新技术产业开发区",
            "江苏省建湖高新技术产业开发区",
            "江苏省南京白马高新技术产业开发区",
            "江苏省南京徐庄高新技术产业开发区",
            "江苏省南通市北高新技术产业开发区",
            "江苏省如皋高新技术产业开发区",
            "江苏省太仓高新技术产业开发区",
            "江苏省锡沂高新技术产业开发区",
            "江苏省相城高新技术产业开发区",
            "江苏省盐城环保高新技术产业开发区",
            "江苏省盐南高新技术产业开发区",
            "江苏省扬中高新技术产业开发区",
            "江苏省张家港高新技术产业开发区",
            "江苏省中关村高新技术产业开发区",
            "江苏省邳州高新技术产业开发区",
            "江苏苏州宿迁工业园区",
            "江苏宿城经济开发区",
            "江苏泰兴黄桥经济开发区",
            "江苏泰兴经济开发区",
            "江苏泰州港经济开发区",
            "江苏泰州海陵工业园区",
            "江苏亭湖经济开发区",
            "江苏铜山经济开发区",
            "江苏无锡惠山经济开发区",
            "江苏无锡经济开发区",
            "江苏无锡空港经济开发区",
            "江苏无锡蠡园经济开发区",
            "江苏武进经济开发区",
            "江苏响水经济开发区",
            "江苏新沂经济开发区",
            "江苏兴化经济开发区",
            "江苏徐州工业园区",
            "江苏徐州空港经济开发区",
            "江苏徐州泉山经济开发区",
            "江苏徐州云龙经济开发区",
            "江苏扬中经济开发区",
            "江苏扬州广陵经济开发区",
            "江苏扬州化学工业园区",
            "江苏扬州维扬经济开发区",
            "江苏仪征经济开发区",
            "江苏宜兴陶瓷产业园区",
            "江苏镇江京口工业园区",
            "江苏邳州经济开发区",
            "江苏泗洪经济开发区",
            "江苏泗阳经济开发区",
            "江苏溧水经济开发区",
            "江苏溧阳经济开发区",
            "江苏盱眙经济开发区",
            "江苏睢宁经济开发区",
            "江阴高新技术产业开发区",
            "靖江经济技术开发区",
            "昆山高新技术产业开发区",
            "昆山经济技术开发区",
            "连云港高新技术产业开发区",
            "连云港经济技术开发区",
            "南京白下高新技术产业园区",
            "南京高新技术产业开发区",
            "南京化学工业园区",
            "南京江宁滨江经济开发区",
            "南京经济技术开发区",
            "南京六合经济开发区",
            "南京浦口经济开发区",
            "南京雨花经济开发区",
            "南通高新技术产业开发区",
            "南通经济技术开发区",
            "如皋经济技术开发区",
            "苏州高新技术产业开发区",
            "苏州工业园区",
            "苏州太湖国家旅游度假区",
            "苏州浒墅关经济技术开发区",
            "宿迁高新技术产业开发区",
            "宿迁经济技术开发区",
            "泰州医药高新技术产业开发区",
            "太仓港经济技术开发区",
            "无锡高新技术产业开发区",
            "无锡太湖国家旅游度假区",
            "吴江经济技术开发区",
            "吴中经济技术开发区",
            "武进高新技术产业开发区",
            "锡山经济技术开发区",
            "相城经济技术开发区",
            "徐州高新技术产业开发区",
            "徐州经济技术开发区",
            "盐城高新技术产业开发区",
            "盐城经济技术开发区",
            "扬州高新技术产业开发区",
            "扬州经济技术开发区",
            "宜兴经济技术开发区",
            "张家港保税港区",
            "张家港经济技术开发区",
            "镇江高新技术产业开发区",
            "镇江经济技术开发区",
            "沭阳经济技术开发区"};

    @Resource
    private CusBaseMapper cusBaseMapper;
    @Resource
    private CusCorpMapper cusCorpMapper;
    @Resource
    private CusCorpService cusCorpService;
    @Resource
    private Dscms2EcifClientService dscms2EcifClientService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    @Transactional
    public Xdkh0037DataRespDto xdkh0037(Xdkh0037DataReqDto xdkh0037DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, JSON.toJSONString(xdkh0037DataReqDto));
        Xdkh0037DataRespDto xdkh0037DataRespDto = new Xdkh0037DataRespDto();
        String cusId = xdkh0037DataReqDto.getCus_id();//客户编号

        G00101ReqDto g00101ReqDto = new G00101ReqDto();
        g00101ReqDto.setCustno(cusId);
        g00101ReqDto.setResotp("1");

        logger.info("客户【"+cusId+"】，对公客户综合信息查询开始："+ g00101ReqDto);
        ResultDto<G00101RespDto> g00101RespDto = dscms2EcifClientService.g00101(g00101ReqDto);
        logger.info("客户【"+cusId+"】，对公客户综合信息查询结束："+ g00101RespDto);
        if(SuccessEnum.CMIS_SUCCSESS.key.equals(g00101RespDto.getCode())){
           int exists = cusBaseMapper.isExistCusID(cusId);
           if(exists>0){
               //throw BizException.error("",EpbEnum.EPB099999.key,"信贷已存在该客户，客户号："+cusId);
               CusBase cusBase = cusBaseMapper.selectCusBaseByCusId(cusId);

               G00101RespDto result = g00101RespDto.getData();
               //cusbase表数据插入
               //cusBase.setCusId(cusId);//客户号
               cusBase.setCusName(result.getCustna());//客户名
               cusBase.setCertType(result.getIdtftp());//证件类型
               cusBase.setCertCode(result.getIdtfno());//证件号码
               cusBase.setOpenType("");//开户类型
               cusBase.setCusRankCls("01");
               cusBase.setCusCatalog("2");
               cusBase.setCusState("2");
               cusBaseMapper.updateByPrimaryKeySelective(cusBase);

               CusCorp cusCorp = cusCorpMapper.selectCropAndBaseByCusId(cusId);
               //cusCorp.setCusId(cusId);//客户号
               cusCorp.setCusName(result.getCustna());//客户名
               cusCorp.setCusType("211");//客户类型
               cusCorp.setCertCode(result.getIdtfno());//证件号码
               cusCorp.setCountry(result.getRegicd());//国籍
               cusCorp.setInvestMbody(result.getInvesj());//投资主体
               cusCorp.setHoldType(result.getFinctp());//控股类型
               cusCorp.setTradeClass(result.getCorppr());//行业分类
               cusCorp.setBuildDate(result.getFoundt());//成立日期
               cusCorp.setCorpScale(result.getCorpsz());//企业规模
               cusCorp.setRegiAreaCode(result.getRgctry());//注册行政区划
               cusCorp.setCityType(checkAddress(result.getRegadr()));
               cusCorp.setIsSmconCus(CmisCommonConstants.YES_NO_1);//是否小企业客户 STD_ZB_YES_NO
               cusCorpMapper.updateByPrimaryKeySelective(cusCorp);

               this.sendEcifModifyCusInfo(cusCorp);

           }else {
               /**************参照的cn.com.yusys.yusp.service.server.xdkh0018.Xdkh0018Service.xdkh0018*/
               String inputId = CmisBizConstants.WYD_MANAGER_ID;
               String inputBrId =CmisBizConstants.WYD_MANAGER_BR_ID;
               G00101RespDto result = g00101RespDto.getData();
               String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前系统日期
               CusBase cusBase = new CusBase();
               //cusbase表数据插入
               cusBase.setCusId(cusId);//客户号
               cusBase.setCusName(result.getCustna());//客户名
               cusBase.setCertType(result.getIdtftp());//证件类型
               cusBase.setCertCode(result.getIdtfno());//证件号码
               cusBase.setOpenType("");//开户类型
               cusBase.setCusRankCls("01");
               cusBase.setCusCatalog("2");
               cusBase.setCusState("2");
               cusBase.setOpenDate(openDay);//开户日期
               cusBase.setInputId(inputId);//登记人
               cusBase.setInputBrId(inputBrId);//登记机构
               cusBase.setInputDate(openDay);//登记日期
               cusBase.setManagerId(inputId);//责任人
               cusBase.setManagerBrId(inputBrId);//责任机构
               cusBaseMapper.insertSelective(cusBase);
               //CusCorp 表数据插入
               CusCorp cusCorp = new CusCorp();
               cusCorp.setCusId(cusId);//客户号
               cusCorp.setCusName(result.getCustna());//客户名
               cusCorp.setCusType("211");//客户类型
               cusCorp.setCertCode(result.getIdtfno());//证件号码
               cusCorp.setInputId(inputId);//登记人
               cusCorp.setInputBrId(inputBrId);//登记机构
               cusCorp.setInputDate(openDay);//登记日期
               cusCorp.setCountry(result.getRegicd());//国籍
               cusCorp.setInvestMbody(result.getInvesj());//投资主体
               cusCorp.setHoldType(result.getFinctp());//控股类型
               cusCorp.setTradeClass(result.getCorppr());//行业分类
               cusCorp.setBuildDate(result.getFoundt());//成立日期
               cusCorp.setCorpScale(result.getCorpsz());//企业规模
               cusCorp.setRegiAreaCode(result.getRgctry());//注册行政区划
               cusCorp.setCityType(checkAddress(result.getRegadr()));
               cusCorp.setInitLoanDate(openDay);//建立信贷关系时间
               cusCorp.setIsSmconCus(CmisCommonConstants.YES_NO_1);//是否小企业客户 STD_ZB_YES_NO
               cusCorpMapper.insertSelective(cusCorp);

               this.sendEcifModifyCusInfo(cusCorp);
           }
        }else{
            throw BizException.error("",EpbEnum.EPB099999.key,g00101RespDto.getMessage());
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, JSON.toJSONString(xdkh0037DataReqDto));
        return xdkh0037DataRespDto;
    }

    /**
     * 发送ECIF更新客户信息
     * @param cusCorp
     */
    private void sendEcifModifyCusInfo(CusCorp cusCorp){

        /*************参照的 cn.com.yusys.yusp.service.CusCorpService.ecifCreateCus()*/
        G00102ReqDto g00102ReqDto = new G00102ReqDto();
        //正式客户开户
        g00102ReqDto.setCustst("1");//客户状态
        /**
         * 01	对公客户
         * 02	同业客户
         */
        g00102ReqDto.setCusttp("01");//客户类型               ,
        g00102ReqDto.setCustsb(StringUtils.EMPTY);//对公客户类型细分           ,
        g00102ReqDto.setIdtftp(cusCorp.getCertType());//证件类型   TODO 修改码值            ,
        g00102ReqDto.setIdtfno(cusCorp.getCertCode());//证件号码               ,
        g00102ReqDto.setEfctdt(StringUtils.EMPTY);//证件生效日期             ,
        g00102ReqDto.setBusisp(StringUtils.EMPTY);//经营范围  提交之后更新
        g00102ReqDto.setRgprov(StringUtils.EMPTY);//注册地址所在省
        g00102ReqDto.setRgcity(StringUtils.EMPTY);//注册地址所在市
        g00102ReqDto.setRgerea(StringUtils.EMPTY);//注册地址所在地区
        g00102ReqDto.setRegadr(StringUtils.EMPTY);//注册地址
        g00102ReqDto.setCustna(cusCorp.getCusName());//客户名称
        g00102ReqDto.setJgxyno(StringUtils.EMPTY);//社会信用代码
        g00102ReqDto.setCustno(cusCorp.getCusId());	//客户编号
        g00102ReqDto.setCustna(cusCorp.getCusName());	//客户名称
        g00102ReqDto.setCsstna(cusCorp.getCusShortName());	//客户简称
        g00102ReqDto.setCsenna(cusCorp.getCusNameEn());	//客户英文名称
        //g00102ReqDto.setCustst();	//客户状态
        //g00102ReqDto.setCusttp("01");	//客户类型
        g00102ReqDto.setCustsb(cusCorp.getCusType());	//对公客户类型细分
        //g00102ReqDto.setIdtftp(cusCorpDto.getCertType());	//证件类型
        //g00102ReqDto.setEfctdt();	//证件生效日期
        //g00102ReqDto.setInefdt();	//证件失效日期
        //g00102ReqDto.setAreaad();	//发证机关国家或地区
        //g00102ReqDto.setDepart();	//发证机关
        //g00102ReqDto.setGvrgno();	//营业执照号码
        //g00102ReqDto.setGvbgdt();	//营业执照生效日期
        //g00102ReqDto.setGveddt();	//营业执照失效日期
        //g00102ReqDto.setGvtion();	//营业执照发证机关
        //g00102ReqDto.setCropcd(cusCorpDto.getInsCode());	//组织机构代码
        //g00102ReqDto.setOreddt();	//组织机构代码失效日期
        //g00102ReqDto.setNttxno();	//税务证件号
        //g00102ReqDto.setSwbgdt();	//税务证件生效日期
        //g00102ReqDto.setSweddt();	//税务证件失效日期
        //g00102ReqDto.setSwtion();	//税务证件发证机关
        //g00102ReqDto.setJgxyno();	//机构信用代码证
        //g00102ReqDto.setCropdt();	//机构信用代码证失效日期
        //g00102ReqDto.setOpcfno();	//开户许可证
        //g00102ReqDto.setFoundt();	//成立日期
        g00102ReqDto.setCorppr(cusCorp.getTradeClass());//行业类别
        //g00102ReqDto.setCoppbc();	//人行行业类别
        //g00102ReqDto.setEntetp();	//产业分类
        g00102ReqDto.setCorpnu(cusCorp.getCorpQlty());	//企业性质
        g00102ReqDto.setBlogto(cusCorp.getSubTyp());	//隶属关系
        //g00102ReqDto.setPropts();	//居民性质
        g00102ReqDto.setFinctp(cusCorp.getHoldType());	//控股类型（出资人经济类型）
        g00102ReqDto.setInvesj(cusCorp.getInvestMbody());	//投资主体
        g00102ReqDto.setRegitp(cusCorp.getRegiType());	//登记注册类型
        g00102ReqDto.setRegidt(cusCorp.getRegiStartDate());	//注册日期
        //g00102ReqDto.setRegicd();	//注册地国家代码
        //g00102ReqDto.setRegchn();	//人民银行注册地区代码
        g00102ReqDto.setRgctry(cusCorp.getRegiAreaCode());	//注册地行政区划
        //g00102ReqDto.setRgcrcy();	//注册资本币别
        g00102ReqDto.setRgcpam(cusCorp.getRegiCapAmt());	//注册资本金额
        //g00102ReqDto.setCacrcy();	//实收资本币别
        g00102ReqDto.setCacpam(cusCorp.getPaidCapAmt());	//实收资本金额
        //g00102ReqDto.setOrgatp();	//组织机构类型
        //g00102ReqDto.setSpectp();	//特殊经济区类型
        //g00102ReqDto.setBusstp();	//企业经济类型
        //g00102ReqDto.setNaticd();	//国民经济部门
        //g00102ReqDto.setDebttp();	//境内主体类型
        //g00102ReqDto.setEnvirt();	//企业环保级别
        //g00102ReqDto.setFinacd();	//金融机构类型代码
        //g00102ReqDto.setSwifcd();	//SWIFT编号
        //g00102ReqDto.setSpntcd();	//该SPV或壳机构所属国家/地区
        //g00102ReqDto.setAdmnti();	//经营期限
        //g00102ReqDto.setAdmnst();	//经营状态
        g00102ReqDto.setCorpsz(cusCorp.getCorpScale());	//企业规模
        //g00102ReqDto.setBusisp();	//经营范围
        //g00102ReqDto.setAcesbs();	//客户兼营业务
        //g00102ReqDto.setEmplnm();	//员工总数
        g00102ReqDto.setComput(cusCorp.getAdminOrg());	//主管单位
        //g00102ReqDto.setUpcrps();	//主管单位法人名称
        //g00102ReqDto.setUpidtp();	//主管单位法人证件类型
        //g00102ReqDto.setUpidno();	//主管单位法人证件号码
        //g00102ReqDto.setLoanno();	//贷款卡编码
        //g00102ReqDto.setWkflar(cusCorpDto.getOperPlaceSqu());	//经营场地面积
        g00102ReqDto.setWkflfe(cusCorp.getOperPlaceOwnshp());	//经营场地所有权
        //g00102ReqDto.setBaseno();	//基本账户核准号
        //g00102ReqDto.setAcctnm();	//基本账户开户行名称
        //g00102ReqDto.setAcctbr();	//基本账户开户行号
        //g00102ReqDto.setAcctno();	//基本账户账号
        g00102ReqDto.setAcctdt(cusCorp.getBasicAccNoOpenDate());	//基本账户开户日期
        //g00102ReqDto.setDepotp();	//存款人类别
        //g00102ReqDto.setRhtype();	//人行统计分类
        //g00102ReqDto.setSumast();	//公司总资产
        //g00102ReqDto.setNetast();	//公司净资产
        //g00102ReqDto.setSthdfg();	//是否我行股东
        //g00102ReqDto.setSharhd();	//拥有我行股份金额
        //g00102ReqDto.setListtg();	//是否上市企业
        //g00102ReqDto.setListld();	//上市地
        //g00102ReqDto.setStoctp();	//股票类别
        //g00102ReqDto.setStoccd();	//股票代码
        //g00102ReqDto.setOnacnm();	//一类户数量
        //g00102ReqDto.setTotlnm();	//总户数
        //g00102ReqDto.setGrpctg();	//是否集团客户
        //g00102ReqDto.setReletg();	//是否是我行关联客户
        //g00102ReqDto.setFamltg();	//是否家族企业
        //g00102ReqDto.setHightg();	//是否高新技术企业
        //g00102ReqDto.setIsbdcp();	//是否是担保公司
        //g00102ReqDto.setSpcltg();	//是否为特殊经济区内企业
        //g00102ReqDto.setArimtg();	//是否地区重点企业
        //g00102ReqDto.setFinatg();	//是否融资类企业
        //g00102ReqDto.setResutg();	//是否国资委所属企业
        //g00102ReqDto.setSctlpr();	//是否国家宏观调控限控行业
        //g00102ReqDto.setFarmtg();	//是否农户
        //g00102ReqDto.setHgegtg();	//是否高能耗企业
        //g00102ReqDto.setExegtg();	//是否产能过剩企业
        //g00102ReqDto.setElmntg();	//是否属于淘汰产能目录
        //g00102ReqDto.setEnvifg();	//是否环保企业
        //g00102ReqDto.setHgplcp();	//是否高污染企业
        //g00102ReqDto.setIstdcs();	//是否是我行贸易融资客户
        g00102ReqDto.setSpclfg(cusCorp.getSpOperFlag());	//特种经营标识
        g00102ReqDto.setSteltg(cusCorp.getIsSteelCus());	//是否钢贸企业
        //g00102ReqDto.setDomitg();	//是否优势企业
        //g00102ReqDto.setStrutp();	//产业结构调整类型
        //g00102ReqDto.setStratp();	//战略新兴产业类型
        //g00102ReqDto.setIndutg();	//工业转型升级标识
        //g00102ReqDto.setLeadtg();	//是否龙头企业
        //g00102ReqDto.setCncatg();	//中资标志
        //g00102ReqDto.setImpotg();	//进出口权标志
        //g00102ReqDto.setClostg();	//企业关停标志
        g00102ReqDto.setPlattg(cusCorp.getIisSzjrfwCrop());	//是否苏州综合平台企业
        //g00102ReqDto.setFibrtg();	//是否为工业园区、经济开发区等行政管理区
        //g00102ReqDto.setGvfntg();	//是否政府投融资平台
        //g00102ReqDto.setGvlnlv();	//地方政府融资平台隶属关系
        //g00102ReqDto.setGvlwtp();	//地方政府融资平台法律性质
        //g00102ReqDto.setGvlntp();	//政府融资贷款类型
        //g00102ReqDto.setPetytg();	//是否小额贷款公司
        //g00102ReqDto.setFscddt();	//首次与我行建立信贷关系时间
        //g00102ReqDto.setSupptg();	//是否列入重点扶持产业
        //g00102ReqDto.setLicetg();	//是否一照一码
        //g00102ReqDto.setLandtg();	//是否土地整治机构
        //g00102ReqDto.setPasvfg();	//是否消极非金融机构
        //g00102ReqDto.setImagno();	//影像编号
        //g00102ReqDto.setTaxptp();	//纳税人身份
        //g00102ReqDto.setNeedfp();	//是否需要开具增值税专用发票
        //g00102ReqDto.setTaxpna();	//纳税人全称
        //g00102ReqDto.setTaxpad();	//纳税人地址
        //g00102ReqDto.setTaxptl();	//纳税人电话
        //g00102ReqDto.setTxbkna();	//纳税人行名
        //g00102ReqDto.setTaxpac();	//纳税人账号
        //g00102ReqDto.setTxpstp();	//税收居民类型
        //g00102ReqDto.setTaxadr();	//税收单位英文地址
        //g00102ReqDto.setTxnion();	//税收居民国（地区）
        //g00102ReqDto.setTxennm();	//税收英文姓名
        //g00102ReqDto.setTxbcty();	//税收出生国家或地区
        //g00102ReqDto.setTaxnum();	//纳税人识别号
        //g00102ReqDto.setNotxrs();	//不能提供纳税人识别号原因
        //g00102ReqDto.setCtigno();	//客户经理号
        //g00102ReqDto.setCtigna();	//客户经理姓名
        //g00102ReqDto.setCtigph();	//客户经理手机号
        //g00102ReqDto.setCorptl();	//公司电话
        //g00102ReqDto.setHometl();	//联系电话
        //g00102ReqDto.setRgpost();	//注册地址邮编
        //g00102ReqDto.setRgcuty();	//注册地址所在国家
        //g00102ReqDto.setRgprov();	//注册地址所在省
        //g00102ReqDto.setRgcity();	//注册地址所在市
        //g00102ReqDto.setRgerea();	//注册地址所在区
        //g00102ReqDto.setRegadr();	//注册地址
        //g00102ReqDto.setBspost();	//经营地址邮编
        //g00102ReqDto.setBscuty();	//经营地址所在国家
        //g00102ReqDto.setBsprov();	//经营地址所在省
        //g00102ReqDto.setBscity();	//经营地址所在市
        //g00102ReqDto.setBserea();	//经营地址所在区
        //g00102ReqDto.setBusiad();	//经营地址
        //g00102ReqDto.setEnaddr();	//英文地址
        //g00102ReqDto.setWebsit();	//公司网址
        //g00102ReqDto.setFaxfax();	//传真
        //g00102ReqDto.setEmailx();	//邮箱
        //g00102ReqDto.setSorgno();	//同业机构(行)号
        //g00102ReqDto.setSorgtp();	//同业机构(行)类型
        //g00102ReqDto.setOrgsit();	//同业机构(行)网址
        //g00102ReqDto.setBkplic();	//同业客户金融业务许可证
        //g00102ReqDto.setReguno();	//同业非现场监管统计机构编码
        //g00102ReqDto.setPdcpat();	//同业客户实际到位资金(万元)
        //g00102ReqDto.setSorglv();	//同业授信评估等级
        //g00102ReqDto.setEvalda();	//同业客户评级到期日期
        //g00102ReqDto.setReldgr();	//同业客户与我行合作关系
        //g00102ReqDto.setLimind();	//同业客户是否授信
        //g00102ReqDto.setMabrid();	//同业客户主管机构(同业)
        //g00102ReqDto.setManmgr();	//同业客户主管客户经理(同业)
        //g00102ReqDto.setBriskc();	//同业客户风险大类
        //g00102ReqDto.setMriskc();	//同业客户风险种类
        //g00102ReqDto.setFriskc();	//同业客户风险小类
        //g00102ReqDto.setRiskda();	//同业客户风险划分日期
        //g00102ReqDto.setChflag();	//境内标识
        //g00102ReqDto.setNnjytp();	//交易类型
        //g00102ReqDto.setNnjyna();	//交易名称
        ResultDto<G00102RespDto> g00102RespDto = dscms2EcifClientService.g00102(g00102ReqDto);

    }


    /**
     * 城乡类型转换
     * @param regadr
     * @return
     */
    private String checkAddress(String regadr){

        String com_city_flg = "10";

        if(!"".equals(regadr)){

            if(regadr.contains("区")){
                com_city_flg="10";
            }else{
                int x = 0;
                int j = 0;
                int a = regadr.indexOf("经济技术开发区");
                int b = regadr.indexOf("工业园区");
                int c = regadr.indexOf("高新技术产业开发区");
                int d = regadr.indexOf("国家旅游度假区");
                int e = regadr.indexOf("保税港区");
                int f = regadr.indexOf("经济开发区");
                int g = regadr.indexOf("产业园区");
                int h = regadr.indexOf("科技产业园区");
                int i = regadr.indexOf("高新技术产业园区");
                if(a!=-1||b!=-1||c!=-1||d!=-1||e!=-1||f!=-1||g!=-1||h!=-1||i!=-1){
                    x = regadr.indexOf("市");
                    String city = regadr.substring(x+1,regadr.length()-1);
                    for(j= 0 ; j < citys.length ; j++)
                    {
                        x = -1;
                        x = citys[j].indexOf(city);
                        if(x!=-1){
                            break;
                        }
                    }
                    if(j >= citys.length){
                        com_city_flg= "20";
                    }else
                    {
                        com_city_flg="10";
                    }
                }
                else {
                    com_city_flg="20";
                }
            }


        }else{
            com_city_flg ="20";
        }


        return com_city_flg;
    }

}
