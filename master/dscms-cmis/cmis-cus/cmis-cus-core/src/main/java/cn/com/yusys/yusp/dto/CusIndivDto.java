package cn.com.yusys.yusp.dto;

import javax.validation.constraints.NotBlank;

public class CusIndivDto {
    private static final long serialVersionUID = 1L;
    // 客户编号
    private String cusId;
    // 客户名
    @NotBlank(message = "客户名称不能为空")
    private String cusName;
    // 证件类
    @NotBlank(message = "证件类型不能为空")
    private String certType;
    // 证件号
    @NotBlank(message = "证件号码不能为空")
    private String certCode;
    // 国籍
    private String nation;
    // 性别 std_zb_sex
    private String sex;
    // 是否为长期证件 std_zb_yes_no
    private String isLongIndiv;
    // 证件到期日期
    private String certEndDt;
    // 民族 std_zb_indiv_folk
    private String indivFolk;
    // 籍贯
    private String indivBrtPlace;
    // 出生日期
    private String indivDtOfBirth;
    // 政治面貌 std_zb_political
    private String indivPolSt;
    // 最高学历 std_zb_edu
    private String indivEdt;
    // 最高学位 std_zb_degree;
    private String indivDgr;
    // 婚姻状况 std_zb_mar_st
    private String marStatus;
    // 健康状况
    private String healthStatus;
    // 建立信贷关系时间
    private String initLoanDate;
    // 在我行建立业务情况 std_zb_inv_hl_acn
    private String indivHldAcnt;
    // 评级得分
    private String cusCrdGrade;
    // 是否我行股东 std_zb_yes_no
    private String isBankSharehd;
    // 客户类型
    private String cusType;
    // 影像编号
    private String imageNo;
    // 开户日期
    private String openDate;
    // 开户类型 std_zb_open_typ
    private String openType;
    // 客户大类
    private String cusCatalog;
    // 客户分类
    private String cusRankCls;
    // 管户客户经理
    private String managerId;
    // 客户状态 std_zb_cus_st
    private String cusState;
    // 主管机构
    private String managerBrId;
    // 操作类型  std_zb_opr_type
    private String oprType;
    // 登记人
    private String inputId;
    // 登记机构
    private String inputBrId;
    // 登记日期
    private String inputDate;
    // 更新人
    private String updId;
    // 更新机构
    private String updBrId;
    // 更新日期
    private String updDate;
    // 创建时间
    private String createTime;
    // 修改时间
    private String updateTime;
    // 保存状态
    private String saveStatus;

    private String isHaveChildren;

    // 是否五日还款
    private String cusGetfive;
    //流水号
    private String serno;
    /** 业务类型**/
    private String bizType;
    /** 区分是否是存量客户 **/
    private String isEcif;
    /** 单位电话 **/
    private String unitPhn;
    /** 居住地地址 **/
    private String indivRsdAddr;

    /** 居住地地址街道 **/
    private String streetRsd;
    /** 证件起始日 **/
    private String certStartDt;
    /** 职业**/
    private String occ;
    /** 职务**/
    private String jobTtl;
    /** 职称**/
    private String indivCrtftn;

    /**个人客户信息主表的是否农户**/
    private String agriFlg;

    public String getIndivCrtftn() {
        return indivCrtftn;
    }

    public void setIndivCrtftn(String indivCrtftn) {
        this.indivCrtftn = indivCrtftn;
    }

    public String getJobTtl() {
        return jobTtl;
    }

    public void setJobTtl(String jobTtl) {
        this.jobTtl = jobTtl;
    }

    public String getOcc() {
        return occ;
    }

    public void setOcc(String occ) {
        this.occ = occ;
    }

    public String getCertStartDt() {
        return certStartDt;
    }

    public void setCertStartDt(String certStartDt) {
        this.certStartDt = certStartDt;
    }

    public String getStreetRsd() {
        return streetRsd;
    }

    public void setStreetRsd(String streetRsd) {
        this.streetRsd = streetRsd;
    }

    public String getIndivRsdAddr() {
        return indivRsdAddr;
    }

    public void setIndivRsdAddr(String indivRsdAddr) {
        this.indivRsdAddr = indivRsdAddr;
    }

    public String getUnitPhn() {
        return unitPhn;
    }

    public void setUnitPhn(String unitPhn) {
        this.unitPhn = unitPhn;
    }

    public String getIsEcif() {
        return isEcif;
    }

    public void setIsEcif(String isEcif) {
        this.isEcif = isEcif;
    }

    public String getCusId() {
        return cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public String getCertType() {
        return certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public String getNation() {
        return nation;
    }

    public String getSex() {
        return sex;
    }

    public String getIsLongIndiv() {
        return isLongIndiv;
    }

    public String getCertEndDt() {
        return certEndDt;
    }

    public String getIndivFolk() {
        return indivFolk;
    }

    public String getIndivBrtPlace() {
        return indivBrtPlace;
    }

    public String getIndivDtOfBirth() {
        return indivDtOfBirth;
    }

    public String getIndivPolSt() {
        return indivPolSt;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public String getIndivDgr() {
        return indivDgr;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public String getInitLoanDate() {
        return initLoanDate;
    }

    public String getIndivHldAcnt() {
        return indivHldAcnt;
    }

    public String getCusCrdGrade() {
        return cusCrdGrade;
    }

    public String getIsBankSharehd() {
        return isBankSharehd;
    }

    public String getCusType() {
        return cusType;
    }

    public String getImageNo() {
        return imageNo;
    }

    public String getOpenDate() {
        return openDate;
    }

    public String getOpenType() {
        return openType;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public String getCusRankCls() {
        return cusRankCls;
    }

    public String getManagerId() {
        return managerId;
    }

    public String getCusState() {
        return cusState;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public String getOprType() {
        return oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public String getSaveStatus() {
        return saveStatus;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setIsLongIndiv(String isLongIndiv) {
        this.isLongIndiv = isLongIndiv;
    }

    public void setCertEndDt(String certEndDt) {
        this.certEndDt = certEndDt;
    }

    public void setIndivFolk(String indivFolk) {
        this.indivFolk = indivFolk;
    }

    public void setIndivBrtPlace(String indivBrtPlace) {
        this.indivBrtPlace = indivBrtPlace;
    }

    public void setIndivDtOfBirth(String indivDtOfBirth) {
        this.indivDtOfBirth = indivDtOfBirth;
    }

    public void setIndivPolSt(String indivPolSt) {
        this.indivPolSt = indivPolSt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public void setIndivDgr(String indivDgr) {
        this.indivDgr = indivDgr;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public void setInitLoanDate(String initLoanDate) {
        this.initLoanDate = initLoanDate;
    }

    public void setIndivHldAcnt(String indivHldAcnt) {
        this.indivHldAcnt = indivHldAcnt;
    }

    public void setCusCrdGrade(String cusCrdGrade) {
        this.cusCrdGrade = cusCrdGrade;
    }

    public void setIsBankSharehd(String isBankSharehd) {
        this.isBankSharehd = isBankSharehd;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    public void setCusRankCls(String cusRankCls) {
        this.cusRankCls = cusRankCls;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setSaveStatus(String saveStatus) {
        this.saveStatus = saveStatus;
    }

    public void setCusGetfive(String cusGetfive) { this.cusGetfive = cusGetfive; }

    public String getCusGetfive() { return cusGetfive; }

    public void setSerno(String serno) {  this.serno = serno; }

    public String getSerno() { return serno; }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getIsHaveChildren() {
        return isHaveChildren;
    }

    public void setIsHaveChildren(String isHaveChildren) {
        this.isHaveChildren = isHaveChildren;
    }

    public String getAgriFlg() {
        return agriFlg;
    }

    public void setAgriFlg(String agriFlg) {
        this.agriFlg = agriFlg;
    }
}
