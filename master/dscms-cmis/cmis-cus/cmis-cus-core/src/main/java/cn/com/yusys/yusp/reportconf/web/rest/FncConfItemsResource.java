package cn.com.yusys.yusp.reportconf.web.rest;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.web.rest.CommonResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.reportconf.domain.FncConfItems;
import cn.com.yusys.yusp.reportconf.service.FncConfItemsService;
//import cn.com.yusys.yusp.trace.annotation.TraceBaggage;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.service.CommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfItemsResource
 * @类描述: #资源类
 * @功能描述: 报表配置项目
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 10:05:50
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/nrcs-cms/fncconfitems")
@Api(tags = "FncConfItemsResource", description = "财务报表科目配置")
public class FncConfItemsResource extends CommonResource<FncConfItems, String> {
	@Autowired
	private FncConfItemsService fncConfItemsService;

	@Override
	protected CommonService getCommonService() {
		return fncConfItemsService;
	}

	/**
	 * 财务报表科目配置查询
	 * 
	 * @param model
	 *            条件封装
	 * @return
	 */
	@GetMapping("/q/fncconfitems/list")
	@ApiOperation("NRCS603211财务报表科目配置查询")
//	@TraceBaggage(functionCode = "NRCS603211")
	public ResultDto<List<FncConfItems>> queryFncConfItemsList(QueryModel model) {
		return new ResultDto<List<FncConfItems>>(fncConfItemsService.queryFncConfItemsList(model));
	}

	/**
	 * 财务报表科目配置全量查询,或fncConfTyp条件查询
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping(value = "/q/fncconfitems/all/list")
	@ApiOperation("NRCS603221财务报表科目配置全量查询,或fncConfTyp条件查询")
//	@TraceBaggage(functionCode = "NRCS603221")
	public ResultDto<List<FncConfItems>> queryFncConfItemsAllListWithFncConfType(QueryModel model) {
		return new ResultDto<List<FncConfItems>>(fncConfItemsService.queryFncConfItemsAllListWithFncConfTyp(model));
	}

	/**
	 * 财务报表科目配置新增
	 * 
	 * @param fncConfItems
	 *            实体类
	 * @return
	 */
	@PostMapping("/s/fncconfitems/add")
	@ApiOperation("NRCS603233财务报表科目配置新增")
//	@TraceBaggage(functionCode = "NRCS603233")
	public ResultDto<Integer> addFncConfItems(@RequestBody FncConfItems fncConfItems) {
		return new ResultDto<Integer>(fncConfItemsService.addFncConfItems(fncConfItems));
	}

	/**
	 * 财务报表科目配置删除
	 * 
	 * @param map
	 *            主键
	 * @return
	 */
	@PostMapping("/s/fncconfitems/delete")
	@ApiOperation("NRCS603254财务报表科目配置删除")
//	@TraceBaggage(functionCode = "NRCS603254")
	public ResultDto<Integer> deleteFncConfItems(@RequestBody HashMap<String, String> map) {
		return new ResultDto<Integer>(fncConfItemsService.deleteFncConfItems(map.get("itemId")));
	}

	/**
	 * 财务报表科目配置修改
	 * 
	 * @param fncConfItems
	 *            实体类
	 * @return
	 */
	@PostMapping("/s/fncconfitems/update")
	@ApiOperation("NRCS603245财务报表科目配置修改")
//	@TraceBaggage(functionCode = "NRCS603245")
	public ResultDto<Integer> updateFncConfItems(@RequestBody FncConfItems fncConfItems) {
		return new ResultDto<Integer>(fncConfItemsService.updateSelective(fncConfItems));
	}

	/**
	 * 报表项目配置查看 暂时不使用，后期delete
	 * 
	 * @param itemId
	 *            主键
	 * @return
	 */
	/*
	 * @GetMapping("/q/fncconfitems/detail")
	 * 
	 * @ApiOperation("财务报表科目配置查看")
	 * 
	 * @TraceBaggage(functionCode = "NRCS603262") public ResultDto<FncConfItems>
	 * queryFncConfItemsByKey(@RequestBody HashMap<String, String> map) { return new
	 * ResultDto<FncConfItems>(fncConfItemsService.queryFncConfItemsByKey(map.get(
	 * "itemId"))); }
	 */
}
