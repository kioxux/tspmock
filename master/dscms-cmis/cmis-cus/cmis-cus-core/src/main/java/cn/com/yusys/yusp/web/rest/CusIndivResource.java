/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.service.CusIndivService;
import cn.com.yusys.yusp.service.CusManaTaskService;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: CusIndivResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: yangx
 * @创建时间: 2020-11-14 14:41:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags= "个人客户信息")
@RestController
@RequestMapping("/api/cusindiv")
public class CusIndivResource {
    @Autowired
    private CusIndivService cusIndivService;
    @Autowired
    private CusManaTaskService cusManaTaskService;

    private static final Logger log = LoggerFactory.getLogger(CusIndivResource.class);

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusIndiv>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusIndiv> list = cusIndivService.selectAll(queryModel);
        return new ResultDto<List<CusIndiv>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusIndiv>> index(QueryModel queryModel) {
        List<CusIndiv> list = cusIndivService.selectByModel(queryModel);
        return new ResultDto<List<CusIndiv>>(list);
    }

    @GetMapping("/xp")
    protected ResultDto<List<Map<String, Object>>> indexYuxp(QueryModel queryModel) {
        List<Map<String, Object>> list = cusIndivService.selectByModelxp(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel 个人客户信息查询
     * @函数名称: queryindivCus
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @author 周茂伟
     */
    @GetMapping("/queryindivcus")
    protected ResultDto<List<Map<String, Object>>> queryindivCus(QueryModel queryModel) {
        List<Map<String, Object>> list = cusIndivService.queryindivCus(queryModel);
        return new ResultDto<List<Map<String, Object>>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cusId}")
    protected ResultDto<CusIndiv> show(@PathVariable("cusId") String cusId) {
        CusIndiv cusIndiv = cusIndivService.selectByPrimaryKey(cusId);
        return new ResultDto<CusIndiv>(cusIndiv);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusIndiv> create(@RequestBody CusIndiv cusIndiv) throws URISyntaxException {
        cusIndivService.insert(cusIndiv);
        return new ResultDto<CusIndiv>(cusIndiv);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusIndiv cusIndiv) throws URISyntaxException {
        int result = cusIndivService.update(cusIndiv);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateSelective
     * @函数描述:只更新非空字段
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CusIndiv cusIndiv) throws URISyntaxException {
        int result = cusIndivService.updateSelective(cusIndiv);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cusId}")
    protected ResultDto<Integer> delete(@PathVariable("cusId") String cusId) {
        int result = cusIndivService.deleteByPrimaryKey(cusId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusIndivService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 个人客户引用引导
     *
     * @param indivCusCreateDto
     * @return
     */
    @PostMapping("/queryCusIndivImport")
    protected ResultDto<IndivCusCreateDto> queryCusIndivImport(@RequestBody IndivCusCreateDto indivCusCreateDto) {
        log.info("个人客户引用引导信息【{}】", JSONObject.toJSON(indivCusCreateDto));
        IndivCusCreateDto rsIndivCusCreateDto = cusIndivService.queryCusIndivImport(indivCusCreateDto);
        return new ResultDto<IndivCusCreateDto>(rsIndivCusCreateDto);
    }

    /**
     * 获取客户关系
     *
     * @param cusRelationsRequestDto
     * @return
     */
    @PostMapping("/cusRelations")
    protected ResultDto<CusRelationsDto> cusRelations(@RequestBody CusRelationsRequestDto cusRelationsRequestDto) {
        log.info("客户关系请求信息：【{}】", JSONObject.toJSONString(cusRelationsRequestDto));
        CusRelationsDto rsCusRelationsDto = cusIndivService.cusRelations(cusRelationsRequestDto, null);
        log.info("客户关系响应信息【{}】", JSONObject.toJSON(rsCusRelationsDto));
        return new ResultDto<CusRelationsDto>(rsCusRelationsDto);
    }

    /**
     * @return
     * @函数名称:saveCusIndiv
     * @函数描述:保存个人客户信息
     * @参数与返回说明:
     * @算法描述:
     * @修改人 周茂伟
     */
    @ApiOperation(value = "保存个人客户信息")
    @PostMapping("/saveCusIndiv")
    protected ResultDto<Integer> saveCusIndiv(@RequestBody CusIndivDto cusIndivDto) throws Exception {
        int result = cusIndivService.saveCusIndiv(cusIndivDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @return
     * @函数名称:sendEcif
     * @函数描述: 个人客户创建向导
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 周茂伟
     */
    @ApiOperation(value = "个人客户开户")
    @PostMapping("/sendEcif")
    protected ResultDto<CusIndivDto> sendEcif(@Validated @RequestBody CusIndivDto cusIndivDto) throws URISyntaxException {
        CusIndivDto cusIndivDtos = cusIndivService.createCusIndiv(cusIndivDto);
        return new ResultDto<CusIndivDto>(cusIndivDtos);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryRankScore")
    protected ResultDto<CusIndivRankScoreDto> queryRankScore(@RequestBody CusIndivDto cusIndivDto) {
        CusIndivRankScoreDto cusIndivRankScoreDto = cusIndivService.selectRankScoreByCusId(cusIndivDto);
        return new ResultDto<CusIndivRankScoreDto>(cusIndivRankScoreDto);
    }

    /**
     * 个人客户信息查看
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/tradecode")
    protected ResultDto<CusIndivQueryRespDto> cusIndivQuery(@RequestBody CusIndivQueryReqDto reqDto) {
        // TODO tradecode 值待定
        // TODO 交易的包名待确定
        return null;
    }

    /**
     * 根据客户号修改建立信贷时间
     *
     * @param cusIndivDto
     * @return
     */
    @RequestMapping("/updateComInitLoanDate")
    protected ResultDto<Integer> updateComInitLoanDate(@RequestBody CusIndivReqClientDto cusIndivDto) {
        int result = cusIndivService.updateComInitLoanDate(cusIndivDto);
        return new ResultDto<Integer>(result);
    }

    /**
     *
     *
     * @param :cusIndivDto.cusId
     * @方法描述: 个人客户详细信息查询
     * @return IndivCusQueryDto
     * @创建人：周茂伟
     */
    @ApiOperation(value = "个人客户详细信息查看")
    @PostMapping("/queryCusIndivInfo")
    protected ResultDto<IndivCusQueryDto> queryCusIndivInfo(@RequestBody CusIndivDto cusIndivDto) {
        IndivCusQueryDto indivCusQueryDto = cusIndivService.queryCusIndivInfo(cusIndivDto);
        return new ResultDto<IndivCusQueryDto>(indivCusQueryDto);
    }

    /**
     * @函数名称:getCertByCodeAndType
     * @函数描述:根据高管证件类型，证件号获取高管信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/getCertByCodeAndType")
    protected ResultDto<CusIndivDto> getCertByCodeAndType(@RequestBody CusBase cusBase) {
        CusIndivDto cusIndivDto = cusIndivService.getCertByCodeAndType(cusBase);
        return new ResultDto<CusIndivDto>(cusIndivDto);
    }
    
//    /**
//     * 获取个人客户额度视图中的客户关系
//     * @param relationsDto
//     * @return
//     */
//    @PostMapping("/cusRelationship")
//    protected ResultDto<CusRelationsDto> cusRelationship(@RequestBody CusRelationsDto relationsDto){
//        CusRelationsDto rsCusRelationsDto = cusIndivService.loadCusRelations(relationsDto.getCusId());
//        log.info("客户关系响应信息【{}】", JSONObject.toJSON(rsCusRelationsDto));
//        return ResultDto.success(rsCusRelationsDto);
//    }

    /**
     * @return
     * @函数名称:saveCusIndiv
     * @函数描述:保存个人客户信息所有
     * @参数与返回说明:同时保存4个tab
     * @算法描述:
     * @修改人 周茂伟
     */
    @ApiOperation(value = "保存个人客户信息（全部）")
    @PostMapping("/saveAllCusIndiv")
    protected ResultDto<Integer> saveAllCusIndiv(@RequestBody CusIndivAllDto cusIndivAllDto){
        int result = cusIndivService.saveAllCusIndiv(cusIndivAllDto);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "个人客户列表查询")
    @PostMapping("/queryCusIndiv")
    protected ResultDto<List<CusIndivDto>> queryCusIndiv(@RequestBody QueryModel queryModel) {
        List<CusIndivDto> list = cusIndivService.queryCusIndivByModel(queryModel);
        return new ResultDto<List<CusIndivDto>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:客户信息列表查询-权限控制
     */
    @ApiOperation(value = "客户信息列表查询-权限控制")
    @PostMapping("/queryCusIndivByManageId")
    protected ResultDto<List<CusIndivDto>> queryCusIndivByManageId(@RequestBody QueryModel queryModel) {
        List<CusIndivDto> list = cusIndivService.queryCusIndivByModel(queryModel);
        return new ResultDto<List<CusIndivDto>>(list);
    }

    /***
     * @param cusId
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.CusIndivDto>
     * @author 王玉坤
     * @date 2021/5/17 15:57
     * @version 1.0.0
     * @desc  根据客户号查询客户信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/querycusindivbycusid")
    protected ResultDto<CusIndivDto> queryCusindivByCusid(@RequestBody String cusId) {
        CusIndiv cusIndiv = cusIndivService.selectByPrimaryKey(cusId);
        CusIndivDto cusIndivDto = null;
        if (!Objects.isNull(cusIndiv)) {
            cusIndivDto = new CusIndivDto();
            BeanUtils.beanCopy(cusIndiv, cusIndivDto);
        }
        return new ResultDto<CusIndivDto>(cusIndivDto);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "个人客户列表查询（全信息）")
    @PostMapping("/queryAllCusIndiv")
    protected ResultDto<List<CusIndivAllDto>> queryAllCusIndiv(@RequestBody QueryModel queryModel) {
        List<CusIndivAllDto> list = cusIndivService.queryAllCusIndivByModel(queryModel);
        return new ResultDto<List<CusIndivAllDto>>(list);
    }

    /**
     * @param  cusIndivDto
     * @return
     * @author wzy
     * @date 2021/6/18 10:52
     * @version 1.0.0
     * @desc 信用卡客户信息开户
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "信用卡客户信息开户")
    @PostMapping("/createcusindivbycard")
    protected ResultDto<CusIndivDto> createCusIndivByCard(@RequestBody CusIndivDto cusIndivDto) {
        CusIndivDto cusIndivDto2 = cusIndivService.createCusIndivByCard(cusIndivDto);
        return new ResultDto<CusIndivDto>(cusIndivDto2);
    }

    /**
     * @param  creditCardAppInfoDto
     * @return
     * @author wzy
     * @date 2021/6/18 10:52
     * @version 1.0.0
     * @desc 信用卡客户信息开户
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "信用卡客户信息修改")
    @PostMapping("/createcreditcardcusinfo")
    protected ResultDto<CreditCardAppInfoDto> createCreditCardCusInfo(@RequestBody CreditCardAppInfoDto creditCardAppInfoDto) {
        CreditCardAppInfoDto creditCardAppInfoDto2 = cusIndivService.createCreditCardCusInfo(creditCardAppInfoDto);
        return new ResultDto<>(creditCardAppInfoDto2);
    }

    /**
     * @param  queryModel
     * @return
     * @author shenli
     * @date 2021-7-12 19:54:46
     * @version 1.0.0
     * @desc 个人客户查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "个人客户查询")
    @PostMapping("/selectbycondition")
    protected ResultDto<List<CusIndiv>> selectByCondition(@RequestBody QueryModel queryModel) {
        List<CusIndiv> list = cusIndivService.selectByCondition(queryModel);
        return new ResultDto<List<CusIndiv>>(list);
    }

    /**
     * @param  queryModel
     * @return
     * @author shenli
     * @date 2021-7-12 19:54:46
     * @version 1.0.0
     * @desc 个人客户查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation(value = "个人客户DTO查询")
    @PostMapping("/selectDtoByCondition")
    protected ResultDto<List<CusIndivDto>> selectDtoByCondition(@RequestBody QueryModel queryModel) {
        List<CusIndivDto> list = cusIndivService.selectDtoByCondition(queryModel);
        return new ResultDto<List<CusIndivDto>>(list);
    }
    /**
     * @函数名称:fastIndivCreate
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述: 快捷个人客户开户
     */
    @PostMapping("/fastindivcreate")
    protected ResultDto<String> fastIndivCreate(@Validated @RequestBody CusIndivDto cusIndivDto) throws Exception {
        return ResultDto.success(cusIndivService.fastIndivCreate(cusIndivDto));
    }

    /**
     * @函数名称:cusInit
     * @函数描述: 新增页面初始化，反显客户基本信息
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增页面初始化，反显客户基本信息")
    @PostMapping("/cusinit/{cusId}")
    protected ResultDto<Map<String, String>> cusInit(@PathVariable String cusId) {
        Map<String, String> result = cusIndivService.cusInit(cusId);
        return new ResultDto<Map<String, String>>(result);
    }

}
