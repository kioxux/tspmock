package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBase
 * @类描述: cus_base数据实体类
 * @功能描述: 
 * @创建人: wh
 * @创建时间: 2021年8月4日15:22:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusForZxdDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 是否农户 **/
	private String isAgri;
	/** QQ **/
	private String qq;
	/** 邮箱 **/
	private String email;
	/** 微信 **/
	private String wechatNo;
	/** 工作单位 **/
	private String indivComTrade;


	@Override
	public String toString() {
		return "CusForZxdDto{" +
				"isAgri='" + isAgri + '\'' +
				", qq='" + qq + '\'' +
				", email='" + email + '\'' +
				", wechatNo='" + wechatNo + '\'' +
				", indivComTrade='" + indivComTrade + '\'' +
				'}';
	}

	public String getIsAgri() {
		return isAgri;
	}

	public void setIsAgri(String isAgri) {
		this.isAgri = isAgri;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWechatNo() {
		return wechatNo;
	}

	public void setWechatNo(String wechatNo) {
		this.wechatNo = wechatNo;
	}

	public String getIndivComTrade() {
		return indivComTrade;
	}

	public void setIndivComTrade(String indivComTrade) {
		this.indivComTrade = indivComTrade;
	}
}