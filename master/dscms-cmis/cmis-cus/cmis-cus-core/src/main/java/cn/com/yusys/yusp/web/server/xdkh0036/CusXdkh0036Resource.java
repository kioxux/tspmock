package cn.com.yusys.yusp.web.server.xdkh0036;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0036.req.Xdkh0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0036.resp.Xdkh0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0036.Xdkh0036Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:网银对公客户查询接口
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0036:网银对公客户查询接口")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0036Resource.class);

    @Autowired
    private Xdkh0036Service xdkh0036Service;
    /**
     * 交易码：xdkh0036
     * 交易描述：网银对公客户查询接口
     *
     * @param xdkh0036DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("网银对公客户查询接口")
    @PostMapping("/xdkh0036")
    protected @ResponseBody
    ResultDto<Xdkh0036DataRespDto> xdkh0036(@Validated @RequestBody Xdkh0036DataReqDto xdkh0036DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, JSON.toJSONString(xdkh0036DataReqDto));
        Xdkh0036DataRespDto xdkh0036DataRespDto = new Xdkh0036DataRespDto();// 响应Dto:网银对公客户查询接口
        ResultDto<Xdkh0036DataRespDto> xdkh0036DataResultDto = new ResultDto<>();
        try {

            xdkh0036DataRespDto = xdkh0036Service.xdkh0036(xdkh0036DataReqDto);

            xdkh0036DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0036DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, e.getMessage());
            // 封装xdkh0036DataResultDto中异常返回码和返回信息
            xdkh0036DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0036DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0036DataRespDto到xdkh0036DataResultDto中
        xdkh0036DataResultDto.setData(xdkh0036DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, JSON.toJSONString(xdkh0036DataResultDto));
        return xdkh0036DataResultDto;
    }
}
