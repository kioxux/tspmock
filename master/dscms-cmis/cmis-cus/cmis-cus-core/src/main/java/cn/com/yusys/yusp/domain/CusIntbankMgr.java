/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIntbankMgr
 * @类描述: cus_intbank_mgr数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-25 17:50:14
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_intbank_mgr")
public class CusIntbankMgr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 同业客户编号 **/
	@Column(name = "INTBANK_CUS_ID", unique = false, nullable = true, length = 32)
	private String intbankCusId;
	
	/** 高管类别 **/
	@Column(name = "MRG_CLS", unique = false, nullable = true, length = 2)
	private String mrgCls;
	
	/** 高管人员名称 **/
	@Column(name = "MRG_NAME", unique = false, nullable = true, length = 35)
	private String mrgName;
	
	/** 高管证件类型 **/
	@Column(name = "MRG_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String mrgCertType;
	
	/** 高管证件号码 **/
	@Column(name = "MRG_CERT_CODE", unique = false, nullable = true, length = 20)
	private String mrgCertCode;
	
	/** 高管证件失效日期 **/
	@Column(name = "EXP_DATE_MANAGE_CERT", unique = false, nullable = true, length = 20)
	private String expDateManageCert;
	
	/** 主联系人电话 **/
	@Column(name = "LINKMAN_PHONE", unique = false, nullable = true, length = 20)
	private String linkmanPhone;
	
	/** 居住地址 **/
	@Column(name = "RESI_ADDR", unique = false, nullable = true, length = 80)
	private String resiAddr;
	
	/** 控制人类型 **/
	@Column(name = "TYPE_CONTROLLER", unique = false, nullable = true, length = 2)
	private String typeController;
	
	/** 资料管理 **/
	@Column(name = "DATA_MANAGEMENT", unique = false, nullable = true, length = 40)
	private String dataManagement;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 持股比例 **/
	@Column(name = "SHD_PERC", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal shdPerc;

	/** 控股类型 **/
	@Column(name = "HOLD_TYPE", unique = false, nullable = true, length = 5)
	private String holdType;



	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param intbankCusId
	 */
	public void setIntbankCusId(String intbankCusId) {
		this.intbankCusId = intbankCusId;
	}
	
    /**
     * @return intbankCusId
     */
	public String getIntbankCusId() {
		return this.intbankCusId;
	}
	
	/**
	 * @param mrgCls
	 */
	public void setMrgCls(String mrgCls) {
		this.mrgCls = mrgCls;
	}
	
    /**
     * @return mrgCls
     */
	public String getMrgCls() {
		return this.mrgCls;
	}
	
	/**
	 * @param mrgName
	 */
	public void setMrgName(String mrgName) {
		this.mrgName = mrgName;
	}
	
    /**
     * @return mrgName
     */
	public String getMrgName() {
		return this.mrgName;
	}
	
	/**
	 * @param mrgCertType
	 */
	public void setMrgCertType(String mrgCertType) {
		this.mrgCertType = mrgCertType;
	}
	
    /**
     * @return mrgCertType
     */
	public String getMrgCertType() {
		return this.mrgCertType;
	}
	
	/**
	 * @param mrgCertCode
	 */
	public void setMrgCertCode(String mrgCertCode) {
		this.mrgCertCode = mrgCertCode;
	}
	
    /**
     * @return mrgCertCode
     */
	public String getMrgCertCode() {
		return this.mrgCertCode;
	}
	
	/**
	 * @param expDateManageCert
	 */
	public void setExpDateManageCert(String expDateManageCert) {
		this.expDateManageCert = expDateManageCert;
	}
	
    /**
     * @return expDateManageCert
     */
	public String getExpDateManageCert() {
		return this.expDateManageCert;
	}
	
	/**
	 * @param linkmanPhone
	 */
	public void setLinkmanPhone(String linkmanPhone) {
		this.linkmanPhone = linkmanPhone;
	}
	
    /**
     * @return linkmanPhone
     */
	public String getLinkmanPhone() {
		return this.linkmanPhone;
	}
	
	/**
	 * @param resiAddr
	 */
	public void setResiAddr(String resiAddr) {
		this.resiAddr = resiAddr;
	}
	
    /**
     * @return resiAddr
     */
	public String getResiAddr() {
		return this.resiAddr;
	}
	
	/**
	 * @param typeController
	 */
	public void setTypeController(String typeController) {
		this.typeController = typeController;
	}
	
    /**
     * @return typeController
     */
	public String getTypeController() {
		return this.typeController;
	}
	
	/**
	 * @param dataManagement
	 */
	public void setDataManagement(String dataManagement) {
		this.dataManagement = dataManagement;
	}
	
    /**
     * @return dataManagement
     */
	public String getDataManagement() {
		return this.dataManagement;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


	public java.math.BigDecimal getShdPerc() {
		return shdPerc;
	}

	public void setShdPerc( java.math.BigDecimal shdPerc) {
		this.shdPerc = shdPerc;
	}

	public String getHoldType() {
		return holdType;
	}

	public void setHoldType(String holdType) {
		this.holdType = holdType;
	}

}