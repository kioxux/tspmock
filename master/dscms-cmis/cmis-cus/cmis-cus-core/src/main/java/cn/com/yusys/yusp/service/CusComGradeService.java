/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.domain.CusGrpMemberRel;
import cn.com.yusys.yusp.dto.CusComGradeDto2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusComGrade;
import cn.com.yusys.yusp.repository.mapper.CusComGradeMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusComGradeService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 20:41:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusComGradeService {

    @Autowired
    private CusComGradeMapper cusComGradeMapper;

    @Autowired
    private CusGrpMemberRelService cusGrpMemberRelService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusComGrade selectByPrimaryKey(String serno) {
        return cusComGradeMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusComGrade> selectAll(QueryModel model) {
        List<CusComGrade> records = (List<CusComGrade>) cusComGradeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusComGrade> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusComGrade> list = cusComGradeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusComGrade record) {
        return cusComGradeMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusComGrade record) {
        return cusComGradeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusComGrade record) {
        return cusComGradeMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusComGrade record) {
        return cusComGradeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusComGradeMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusComGradeMapper.deleteByIds(ids);
    }

    /**
     * 根据客户号查询最新的客户评级信息
     * @param cusId
     * @return
     */
    public Map<String,String> selectGradeInfoByCusId(String cusId){
        return cusComGradeMapper.selectGradeInfoByCusId(cusId);
    }

    /**
     * 根据客户号查询客户评级信息
     * @param cusId
     * @return
     */
    public Map<String,String> getGradeInfoByCusId(String cusId){
        return cusComGradeMapper.getGradeInfoByCusId(cusId);
    }

    /**
     * 根据客户号查询客户所有评级信息
     * @param cusId
     * @return
     */
    public List<CusComGrade> queryGradeInfoByCusId(String cusId){
        return cusComGradeMapper.queryGradeInfoByCusId(cusId);
    }

    /**
     * 根据客户号查询客户所有评级信息
     * @param cusId
     * @return
     */
    public List<CusComGradeDto2> queryCusComGrade(String cusId){
        if (StringUtils.isEmpty(cusId)){
            throw new BizException(null,"",null,"客户编号cusId不能为空！");
        }

        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusId);
        //查询该客户的所在集团编号
        Map<String, String> grpNoMap = cusGrpMemberRelService.queryGrpNoByQueryModel(queryModel);

        if (CollectionUtils.isEmpty(grpNoMap)){
            throw new BizException(null,"",null,"根据客户编号【"+cusId+"】未查到集团客户编号！");
        }
        //集团编号
        String grpNo = grpNoMap.get("grpNo");

        List<CusComGradeDto2> resultList = new ArrayList<>();

        QueryModel model = new QueryModel();
        model.addCondition("grpNo",grpNo);
        model.addCondition("availableInd", CmisCommonConstants.STD_ZB_YES_NO_1);
        model.addCondition("cusCatalog", CmisCusConstants.STD_ZB_CUS_CATALOG_2);
        model.addCondition("notCusId",cusId);

        //查询集团成员的信用评级信息
        List<CusGrpMemberRel> cusGrpMemberRels = cusGrpMemberRelService.selectAll(model);

        if (CollectionUtils.nonEmpty(cusGrpMemberRels)){
            for (CusGrpMemberRel cusGrpMemberRel : cusGrpMemberRels) {
                CusComGradeDto2 comGradeDto2 = new CusComGradeDto2();
                comGradeDto2.setRelCusId(cusGrpMemberRel.getCusId());
                comGradeDto2.setRelCusName(cusGrpMemberRel.getCusName());
                comGradeDto2.setRelCusType(cusGrpMemberRel.getGrpCorreType());

                //查询该客户最近两年的信用评级信息
                List<CusComGrade> cusComGrades = cusComGradeMapper.queryCusComGradeByCusId(cusGrpMemberRel.getCusId());

                if (CollectionUtils.nonEmpty(cusComGrades)){
                    comGradeDto2.setCurCusCrdGrade(cusComGrades.get(0).getFinalRank());

                    if (cusComGrades.size()==2){
                        comGradeDto2.setLastCusCrdGrade(cusComGrades.get(1).getFinalRank());
                    }
                }
                resultList.add(comGradeDto2);
            }
        }
        return resultList;
    }
}