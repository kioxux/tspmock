package cn.com.yusys.yusp.service.server.xdkh0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusIndivMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0001Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-04-25 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */

@Service
public class Xdkh0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0001Service.class);

    @Resource
    private CusIndivMapper cusIndivMapper;

    @Autowired
    private AdminSmUserService adminSmUserService;
    /**
     * 个人客户基本信息查询
     *
     * @param xdkh0001DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0001DataRespDto getIndivCusBaseInfo(Xdkh0001DataReqDto xdkh0001DataReqDto) throws BizException,Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
        Map map = new HashMap<>();
        Xdkh0001DataRespDto xdkh0001DataRespDto = new Xdkh0001DataRespDto();
        //查询类型
        //01-按客户号查询；
        //02-按证件类型和证件号码查询；
        //03-按客户姓名和证件号查询；
        //04-证件号
        try {
            String queryType = "";
            if (StringUtils.isEmpty(xdkh0001DataReqDto.getQueryType())) {
                throw BizException.error(null,EcsEnum.ECS040001.key, EcsEnum.ECS040001.value());
            } else {
                queryType = xdkh0001DataReqDto.getQueryType();
                if (CmisCusConstants.INDIV_QUERYTYPE_01.equals(queryType)) {
                    if (StringUtils.isEmpty(xdkh0001DataReqDto.getCusId())) {
                        throw BizException.error(null,EcsEnum.ECS040002.key, EcsEnum.ECS040002.value());
                    } else {
                        map.put("cusId", xdkh0001DataReqDto.getCusId());
                    }
                } else if (CmisCusConstants.INDIV_QUERYTYPE_02.equals(queryType)) {
                    if (StringUtils.isEmpty(xdkh0001DataReqDto.getCertType())) {
                        throw BizException.error(null,EcsEnum.ECS040003.key, EcsEnum.ECS040003.value());
                    }
                    if (StringUtils.isEmpty(xdkh0001DataReqDto.getCertCode())) {
                        throw BizException.error(null,EcsEnum.ECS040004.key, EcsEnum.ECS040004.value());
                    }
                    map.put("certType", xdkh0001DataReqDto.getCertType());
                    map.put("certCode", xdkh0001DataReqDto.getCertCode());
                } else if (CmisCusConstants.INDIV_QUERYTYPE_03.equals(queryType)) {
                    if (StringUtils.isEmpty(xdkh0001DataReqDto.getCertCode())) {
                        throw BizException.error(null,EcsEnum.ECS040004.key, EcsEnum.ECS040004.value());
                    }
                    if (StringUtils.isEmpty(xdkh0001DataReqDto.getCusName())) {
                        throw BizException.error(null,EcsEnum.ECS040005.key, EcsEnum.ECS040005.value());
                    }
                    map.put("cusName", xdkh0001DataReqDto.getCusName());
                    map.put("certCode", xdkh0001DataReqDto.getCertCode());
                } else if (CmisCusConstants.INDIV_QUERYTYPE_04.equals(queryType)) {
                    if (StringUtils.isEmpty(xdkh0001DataReqDto.getCertCode())) {
                        throw BizException.error(null,EcsEnum.ECS040004.key, EcsEnum.ECS040004.value());
                    } else {
                        map.put("certCode", xdkh0001DataReqDto.getCertCode());
                    }
                }
                if (!map.isEmpty()) {
                    xdkh0001DataRespDto = cusIndivMapper.getIndivCusBaseInfo(map);
                    //客户状态映射 TODO

                    if(!Objects.isNull(xdkh0001DataRespDto)){
                        //客户名称翻译
                        String managerId = xdkh0001DataRespDto.getManagerId();
                        String managerName = "";
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        if(ResultDto.success().getCode().equals(resultDto.getCode())){
                            AdminSmUserDto adminSmUserDto = resultDto.getData();
                            if(!Objects.isNull(adminSmUserDto)){
                                managerName = adminSmUserDto.getUserName();
                            }
                        }
                        xdkh0001DataRespDto.setManagerName(managerName);
                        //客户状态字典转换
                        if ("1".equals(xdkh0001DataRespDto.getCusStatus())) {
                            xdkh0001DataRespDto.setCusStatus("04");
                        } else if ("2".equals(xdkh0001DataRespDto.getCusStatus())) {
                            xdkh0001DataRespDto.setCusStatus("20");
                        }
                    }else{
                        xdkh0001DataRespDto = new Xdkh0001DataRespDto();
                    }
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataRespDto));
        return xdkh0001DataRespDto;
    }
}
