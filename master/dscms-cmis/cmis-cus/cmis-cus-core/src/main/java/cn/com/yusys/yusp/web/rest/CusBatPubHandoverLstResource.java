/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.CusBatPubHandoverLstVo;
import cn.com.yusys.yusp.vo.CusLstDedkkhVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusBatPubHandoverLst;
import cn.com.yusys.yusp.service.CusBatPubHandoverLstService;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBatPubHandoverLstResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 18301
 * @创建时间: 2021-05-05 10:09:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusbatpubhandoverlst")
public class CusBatPubHandoverLstResource {
    private final Logger logger = LoggerFactory.getLogger(CusBatPubHandoverLstResource.class);

    @Autowired
    private CusBatPubHandoverLstService cusBatPubHandoverLstService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusBatPubHandoverLst>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusBatPubHandoverLst> list = cusBatPubHandoverLstService.selectAll(queryModel);
        return new ResultDto<List<CusBatPubHandoverLst>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusBatPubHandoverLst>> index(QueryModel queryModel) {
        List<CusBatPubHandoverLst> list = cusBatPubHandoverLstService.selectByModel(queryModel);
        return new ResultDto<List<CusBatPubHandoverLst>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusBatPubHandoverLst> show(@PathVariable("pkId") String pkId) {
        CusBatPubHandoverLst cusBatPubHandoverLst = cusBatPubHandoverLstService.selectByPrimaryKey(pkId);
        return new ResultDto<CusBatPubHandoverLst>(cusBatPubHandoverLst);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusBatPubHandoverLst> create(@RequestBody CusBatPubHandoverLst cusBatPubHandoverLst) throws URISyntaxException {
        cusBatPubHandoverLstService.insert(cusBatPubHandoverLst);
        return new ResultDto<CusBatPubHandoverLst>(cusBatPubHandoverLst);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusBatPubHandoverLst cusBatPubHandoverLst) throws URISyntaxException {
        int result = cusBatPubHandoverLstService.update(cusBatPubHandoverLst);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusBatPubHandoverLstService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusBatPubHandoverLstService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = cusBatPubHandoverLstService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId, @RequestBody Map map) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        // TODO  修改业务流水号
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库，StudentScore为导入数据的类
        try {
            ExcelUtils.syncImport(CusBatPubHandoverLstVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return cusBatPubHandoverLstService.insertInBatch(dataList,String.valueOf(map.get("cbphaSerno")));
            }), true);
        } catch (Exception e) {
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }

}
