package cn.com.yusys.yusp.web.server.cmiscus0020;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0020.req.CmisCus0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.resp.CmisCus0020RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscus0020.CmisCus0020Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 根据客户号查询关联方名单信息记录数
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0020:根据客户号查询关联方名单信息记录数")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0020Resource.class);

    @Autowired
    private CmisCus0020Service cmisCus0020Service;

    /**
     * 交易码：cmiscus0020
     * 交易描述：法人关联客户列表查询
     *
     * @param cmisCus0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("法人关联客户列表查询")
    @PostMapping("/cmiscus0020")
    protected @ResponseBody
    ResultDto<CmisCus0020RespDto> cmiscus0020(@Validated @RequestBody CmisCus0020ReqDto cmisCus0020ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0020.key, DscmsEnum.TRADE_CODE_CMISCUS0020.value, JSON.toJSONString(cmisCus0020ReqDto));
        CmisCus0020RespDto cmisCus0020RespDto = new CmisCus0020RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0020RespDto> cmisCus0020ResultDto = new ResultDto<>();
        try {
            // 封装cmisCus0020ResultDto中正确的返回码和返回信息
            cmisCus0020RespDto = cmisCus0020Service.execute(cmisCus0020ReqDto);
            cmisCus0020ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0020ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0020.key, DscmsEnum.TRADE_CODE_CMISCUS0020.value, e.getMessage());
            // 封装cmisCus0020ResultDto中异常返回码和返回信息
            cmisCus0020ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0020ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0020RespDto到cmisCus0020ResultDto中
        cmisCus0020ResultDto.setData(cmisCus0020RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0020.key, DscmsEnum.TRADE_CODE_CMISCUS0020.value, JSON.toJSONString(cmisCus0020ResultDto));
        return cmisCus0020ResultDto;
    }
}
