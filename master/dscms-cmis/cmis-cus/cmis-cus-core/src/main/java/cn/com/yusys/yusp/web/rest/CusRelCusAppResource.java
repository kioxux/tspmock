/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusRelCusApp;
import cn.com.yusys.yusp.service.CusRelCusAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusRelCusAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: liqiang
 * @创建时间: 2021-05-07 21:29:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusrelcusapp")
public class CusRelCusAppResource {
    @Autowired
    private CusRelCusAppService cusRelCusAppService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusRelCusApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusRelCusApp> list = cusRelCusAppService.selectAll(queryModel);
        return new ResultDto<List<CusRelCusApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusRelCusApp>> index(QueryModel queryModel) {
        List<CusRelCusApp> list = cusRelCusAppService.selectByModel(queryModel);
        return new ResultDto<List<CusRelCusApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusRelCusApp> show(@PathVariable("serno") String serno) {
        CusRelCusApp cusRelCusApp = cusRelCusAppService.selectByPrimaryKey(serno);
        return new ResultDto<CusRelCusApp>(cusRelCusApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusRelCusApp> create(@RequestBody CusRelCusApp cusRelCusApp) throws URISyntaxException {
        CusRelCusApp cusRelCusApps = cusRelCusAppService.selectByPrimaryKey(cusRelCusApp.getSerno());
        if(cusRelCusApps!=null){
            cusRelCusAppService.update(cusRelCusApp);
        }else{
            cusRelCusAppService.insert(cusRelCusApp);
        }
        return new ResultDto<CusRelCusApp>(cusRelCusApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusRelCusApp cusRelCusApp) throws URISyntaxException {
        int result = cusRelCusAppService.update(cusRelCusApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusRelCusAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusRelCusAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectCusRelCusAppList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel 分页查询类
     * @创建人：周茂伟
     * @算法描述:
     */
    @PostMapping("/selectCusRelCusAppList")
    protected ResultDto<List<CusRelCusApp>> selectCusRelCusAppList(@RequestBody QueryModel queryModel) {
        List<CusRelCusApp> list = cusRelCusAppService.selectByModel(queryModel);
        return new ResultDto<List<CusRelCusApp>>(list);
    }

    /**
     * @函数名称:selectCusRelCusApp
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：周茂伟
     */
    @PostMapping("/selectCusRelCusApp")
    protected ResultDto<CusRelCusApp> selectCusRelCusApp(@RequestBody CusRelCusApp cusRelCusApp) {
        CusRelCusApp cusRelCusApps = cusRelCusAppService.selectByPrimaryKey(cusRelCusApp.getSerno());
        return new ResultDto<CusRelCusApp>(cusRelCusApp);
    }

    /**
     * @函数名称:doCusRelCusAppComit
     * @函数描述:关联客户认定提交
     * @参数与返回说明:
     * @算法描述:
     * @创建人：liqiang
     */
    @PostMapping("/doCusRelCusAppComit")
    protected ResultDto<Void> doCusRelCusAppComit(@RequestBody CusRelCusApp cusRelCusApp) {
        cusRelCusAppService.doCusRelCusAppComit(cusRelCusApp);
        return new ResultDto<>();
    }

    /**
     * @函数名称:getMemAppFromRel
     * @函数描述:关联客户变更下一步逻辑
     * @参数与返回说明:
            * @算法描述:
            * @创建人：liqiang
     */
    @PostMapping("/getMemAppFromRel")
    protected ResultDto<Void> getMemAppFromRel(@RequestBody Map map) {
        cusRelCusAppService.getMemAppFromRel(map);
        return new ResultDto<>();
    }
}
