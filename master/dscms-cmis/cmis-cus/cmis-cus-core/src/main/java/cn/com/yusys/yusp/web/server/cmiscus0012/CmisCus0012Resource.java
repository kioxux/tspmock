package cn.com.yusys.yusp.web.server.cmiscus0012;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0012.req.CmisCus0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.CmisCus0012RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusCorpMgrService;
import cn.com.yusys.yusp.web.server.cmiscus0007.CmisCus0007Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;


/**
 * 接口处理类: 对公高管信息查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "CmisCus0012:对公高管信息查询")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0007Resource.class);

    @Autowired
    private CusCorpMgrService cusCorpMgrService;

    /**
     * 交易码：cmiscus0012
     * 交易描述：对公高管信息查询
     *
     * @param cmisCus0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对公高管信息查询")
    @PostMapping("/cmiscus0012")
    protected @ResponseBody
    ResultDto<CmisCus0012RespDto> cmiscus0012(@Validated @RequestBody CmisCus0012ReqDto cmisCus0012ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0012.key, DscmsEnum.TRADE_CODE_CMISCUS0012.value, JSON.toJSONString(cmisCus0012ReqDto));
        CmisCus0012RespDto cmisCus0012RespDto = new CmisCus0012RespDto();// 响应Dto:查询优农贷名单信息
        ResultDto<CmisCus0012RespDto> cmisCus0012ResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0012.key, DscmsEnum.TRADE_CODE_CMISCUS0012.value, JSON.toJSONString(cmisCus0012ReqDto));
            String certCode = cmisCus0012ReqDto.getCertCode();//高管证件号码
            String cusId = cmisCus0012ReqDto.getCusId();//客户号
            String mrgType = cmisCus0012ReqDto.getMrgType();//高管类别
            String mrgCertType = cmisCus0012ReqDto.getMrgCertType();//高管证件类型
            String queryType = cmisCus0012ReqDto.getQueryType();// 1-根据多个高管类型查询

            java.util.List<cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List> cusCorpMgrlist = null;
            if (StringUtil.isNotEmpty(cusId) && !"1".equals(queryType)) {//根据客户号查询
                Map map = new HashMap();
                map.put("cusId", cusId);
                map.put("certCode", certCode);
                map.put("mrgType", mrgType);
                map.put("mrgCertType", mrgCertType);
                cusCorpMgrlist = cusCorpMgrService.getCusCorpMgrMessageByCusId(map);
            } else if (StringUtils.isNotEmpty(mrgCertType) && StringUtils.isNotEmpty(certCode) && !"1".equals(queryType)) {
                Map map = new HashMap();
                map.put("cusId", cusId);
                map.put("certCode", certCode);
                map.put("mrgType", mrgType);
                map.put("mrgCertType", mrgCertType);
                cusCorpMgrlist = cusCorpMgrService.getCusCorpMgrMessageByCusId(map);
            } else if (!"1".equals(queryType)) {
                cusCorpMgrlist = cusCorpMgrService.getCusCorpMgrMessageByCertCode(certCode);
            } else {//根据多个高管类型查询
                Map map = new HashMap();
                map.put("cusId", cusId);
                map.put("certCode", certCode);
                map.put("mrgType", mrgType);
                cusCorpMgrlist = cusCorpMgrService.getCusCorpMgrMessageBymrgCertType(map);
            }
            cmisCus0012RespDto.setList(cusCorpMgrlist);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0012.key, DscmsEnum.TRADE_CODE_CMISCUS0012.value, JSON.toJSONString(cmisCus0012RespDto));
            // 封装cmisCus0012ResultDto中正确的返回码和返回信息
            cmisCus0012ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0012ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0012.key, DscmsEnum.TRADE_CODE_CMISCUS0012.value, e.getMessage());
            // 封装cmisCus0012ResultDto中异常返回码和返回信息
            cmisCus0012ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0012ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0012RespDto到cmisCus0007ResultDto中
        cmisCus0012ResultDto.setData(cmisCus0012RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0012.key, DscmsEnum.TRADE_CODE_CMISCUS0012.value, JSON.toJSONString(cmisCus0012ResultDto));
        return cmisCus0012ResultDto;
    }
}
