/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndJyxxApp
 * @类描述: CUS_LST_YND_JYXX_APP数据实体类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:18:21
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "CUS_LST_YND_JYXX_APP")
public class CusLstYndJyxxApp extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;

	/** 一级行业编号 **/
	@Column(name = "ONE_LEVEL_TRADE_CODE", unique = false, nullable = true, length = 40)
	private String oneLevelTradeCode;

	/** 一级行业名称 **/
	@Column(name = "ONE_LEVEL_TRADE_NAME", unique = false, nullable = true, length = 40)
	private String oneLevelTradeName;

	/** 细分行业编号 **/
	@Column(name = "DETAILS_TRADE_CODE", unique = false, nullable = true, length = 40)
	private String detailsTradeCode;

	/** 细分行业名称 **/
	@Column(name = "DETAILS_TRADE_NAME", unique = false, nullable = true, length = 40)
	private String detailsTradeName;

	/** 经营规模 **/
	@Column(name = "OPER_SCALE", unique = false, nullable = true, length = 20)
	private String operScale;

	/** 经营规模单位 **/
	@Column(name = "OPER_SCALE_UNIT", unique = false, nullable = true, length = 20)
	private String operScaleUnit;

	/** 年销售收入 **/
	@Column(name = "YEAR_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearSaleIncome;

	/** 年利润 **/
	@Column(name = "YEAR_PROFIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yearProfit;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

	/**
	 * @return serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param oneLevelTradeCode
	 */
	public void setOneLevelTradeCode(String oneLevelTradeCode) {
		this.oneLevelTradeCode = oneLevelTradeCode;
	}

	/**
	 * @return oneLevelTradeCode
	 */
	public String getOneLevelTradeCode() {
		return this.oneLevelTradeCode;
	}

	/**
	 * @param oneLevelTradeName
	 */
	public void setOneLevelTradeName(String oneLevelTradeName) {
		this.oneLevelTradeName = oneLevelTradeName;
	}

	/**
	 * @return oneLevelTradeName
	 */
	public String getOneLevelTradeName() {
		return this.oneLevelTradeName;
	}

	/**
	 * @param detailsTradeCode
	 */
	public void setDetailsTradeCode(String detailsTradeCode) {
		this.detailsTradeCode = detailsTradeCode;
	}

	/**
	 * @return detailsTradeCode
	 */
	public String getDetailsTradeCode() {
		return this.detailsTradeCode;
	}

	/**
	 * @param detailsTradeName
	 */
	public void setDetailsTradeName(String detailsTradeName) {
		this.detailsTradeName = detailsTradeName;
	}

	/**
	 * @return detailsTradeName
	 */
	public String getDetailsTradeName() {
		return this.detailsTradeName;
	}

	/**
	 * @param operScale
	 */
	public void setOperScale(String operScale) {
		this.operScale = operScale;
	}

	/**
	 * @return operScale
	 */
	public String getOperScale() {
		return this.operScale;
	}

	/**
	 * @param operScaleUnit
	 */
	public void setOperScaleUnit(String operScaleUnit) {
		this.operScaleUnit = operScaleUnit;
	}

	/**
	 * @return operScaleUnit
	 */
	public String getOperScaleUnit() {
		return this.operScaleUnit;
	}

	/**
	 * @param yearSaleIncome
	 */
	public void setYearSaleIncome(java.math.BigDecimal yearSaleIncome) {
		this.yearSaleIncome = yearSaleIncome;
	}

	/**
	 * @return yearSaleIncome
	 */
	public java.math.BigDecimal getYearSaleIncome() {
		return this.yearSaleIncome;
	}

	/**
	 * @param yearProfit
	 */
	public void setYearProfit(java.math.BigDecimal yearProfit) {
		this.yearProfit = yearProfit;
	}

	/**
	 * @return yearProfit
	 */
	public java.math.BigDecimal getYearProfit() {
		return this.yearProfit;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}
