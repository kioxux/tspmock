package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBizAarApp
 * @类描述: cus_biz_aar_app数据实体类
 * @功能描述: 
 * @创建人: pc
 * @创建时间: 2021-05-07 22:13:20
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusBizAarAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务权申领流水号 **/
	private String aarAppSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 申请日期 **/
	private String appDate;
	
	/** 管户客户经理 **/
	private String managerId;
	
	/** 所属机构 **/
	private String belgOrg;
	
	/** 业务权申领人机构 **/
	private String bizRightsAarBrIdOrg;
	
	/** 业务权申领人 **/
	private String bizRightsAarBrId;
	
	/** 管户机构与申领机构关系 **/
	private String orgRela;
	
	/** 业务权申领理由 **/
	private String bizRightsArrResn;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param aarAppSerno
	 */
	public void setAarAppSerno(String aarAppSerno) {
		this.aarAppSerno = aarAppSerno == null ? null : aarAppSerno.trim();
	}
	
    /**
     * @return AarAppSerno
     */	
	public String getAarAppSerno() {
		return this.aarAppSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}
	
    /**
     * @return AppDate
     */	
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg == null ? null : belgOrg.trim();
	}
	
    /**
     * @return BelgOrg
     */	
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param bizRightsAarBrIdOrg
	 */
	public void setBizRightsAarBrIdOrg(String bizRightsAarBrIdOrg) {
		this.bizRightsAarBrIdOrg = bizRightsAarBrIdOrg == null ? null : bizRightsAarBrIdOrg.trim();
	}
	
    /**
     * @return BizRightsAarBrIdOrg
     */	
	public String getBizRightsAarBrIdOrg() {
		return this.bizRightsAarBrIdOrg;
	}
	
	/**
	 * @param bizRightsAarBrId
	 */
	public void setBizRightsAarBrId(String bizRightsAarBrId) {
		this.bizRightsAarBrId = bizRightsAarBrId == null ? null : bizRightsAarBrId.trim();
	}
	
    /**
     * @return BizRightsAarBrId
     */	
	public String getBizRightsAarBrId() {
		return this.bizRightsAarBrId;
	}
	
	/**
	 * @param orgRela
	 */
	public void setOrgRela(String orgRela) {
		this.orgRela = orgRela == null ? null : orgRela.trim();
	}
	
    /**
     * @return OrgRela
     */	
	public String getOrgRela() {
		return this.orgRela;
	}
	
	/**
	 * @param bizRightsArrResn
	 */
	public void setBizRightsArrResn(String bizRightsArrResn) {
		this.bizRightsArrResn = bizRightsArrResn == null ? null : bizRightsArrResn.trim();
	}
	
    /**
     * @return BizRightsArrResn
     */	
	public String getBizRightsArrResn() {
		return this.bizRightsArrResn;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}