/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstYndDcjlApp;
import cn.com.yusys.yusp.repository.mapper.CusLstYndDcjlAppMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndDcjlAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:17:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYndDcjlAppService {

    @Autowired
    private CusLstYndDcjlAppMapper cusLstYndDcjlAppMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstYndDcjlApp selectByPrimaryKey(String serno) {
        return cusLstYndDcjlAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYndDcjlApp> selectAll(QueryModel model) {
        List<CusLstYndDcjlApp> records = (List<CusLstYndDcjlApp>) cusLstYndDcjlAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstYndDcjlApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYndDcjlApp> list = cusLstYndDcjlAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstYndDcjlApp record) {
        return cusLstYndDcjlAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstYndDcjlApp record) {
        return cusLstYndDcjlAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstYndDcjlApp record) {
        return cusLstYndDcjlAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstYndDcjlApp record) {
        return cusLstYndDcjlAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstYndDcjlAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYndDcjlAppMapper.deleteByIds(ids);
    }

    /**
     * @param cusLstYndDcjlApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndDcjlApp>
     * @author hubp
     * @date 2021/7/20 10:25
     * @version 1.0.0
     * @desc    调查结论新增或修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto addCusLstYndDcjlApp(CusLstYndDcjlApp cusLstYndDcjlApp) {
        CusLstYndDcjlApp cusLstYndDcjl = cusLstYndDcjlAppMapper.selectByPrimaryKey(cusLstYndDcjlApp.getSerno());
        if (null != cusLstYndDcjl) {
            cusLstYndDcjlAppMapper.updateByPrimaryKey(cusLstYndDcjlApp);
        } else {
            cusLstYndDcjlApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            cusLstYndDcjlAppMapper.insert(cusLstYndDcjlApp);
        }
        return new ResultDto<CusLstYndDcjlApp>(cusLstYndDcjlApp);
    }
}
