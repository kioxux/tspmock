package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 客户关系网
 */
public class CusRelationsDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 客户编号 **/
    private String cusId;
    /** 客户名称 **/
    private String cusName;
    /** 客户类型 **/
    private String cusType;
    /** 证件类型 **/
    private String certType;
    
    /** 客户大类 **/
    private String cusCatalog;
    
    /** 证件号码 **/
    private String certCode;
    /** 关系（指客户与亲友的关系，客户与企业的关系，客户与担保的关系） **/
    private String relations;
    /** 企业关系 **/
    private CusIndivOperComDto cusIndivOperComDto;
    /** 亲友关系 **/
    private CusIndivFamilyInfoDto cusIndivFamilyInfoDto;
    /** 担保关系 **/
    private CusIndivObisAssureDto cusIndivObisAssureDto;

    public String getRelations() {
        return relations;
    }

    public void setRelations(String relations) {
        this.relations = relations;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public CusIndivOperComDto getCusIndivOperComDto() {
        return cusIndivOperComDto;
    }

    public void setCusIndivOperComDto(CusIndivOperComDto cusIndivOperComDto) {
        this.cusIndivOperComDto = cusIndivOperComDto;
    }

    public CusIndivFamilyInfoDto getCusIndivFamilyInfoDto() {
        return cusIndivFamilyInfoDto;
    }

    public void setCusIndivFamilyInfoDto(CusIndivFamilyInfoDto cusIndivFamilyInfoDto) {
        this.cusIndivFamilyInfoDto = cusIndivFamilyInfoDto;
    }

    public CusIndivObisAssureDto getCusIndivObisAssureDto() {
        return cusIndivObisAssureDto;
    }

    public void setCusIndivObisAssureDto(CusIndivObisAssureDto cusIndivObisAssureDto) {
        this.cusIndivObisAssureDto = cusIndivObisAssureDto;
    }

	public String getCusCatalog() {
		return cusCatalog;
	}

	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog;
	}
    
}
