package cn.com.yusys.yusp.service.server.cmiscus0020;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0020.req.CmisCus0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.resp.CmisCus0020RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusLstGlfService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 业务逻辑处理类：根据客户号查询关联方名单信息记录数
 *
 * @author dumd
 * @version 1.0
 * @since 2021年6月24日
 */
@Service
public class CmisCus0020Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0020Service.class);

    @Autowired
    private CusLstGlfService cusLstGlfService;

    @Transactional
    public CmisCus0020RespDto execute(CmisCus0020ReqDto reqDto) throws YuspException {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0020.key, DscmsEnum.TRADE_CODE_CMISCUS0020.value);

        CmisCus0020RespDto respDto = new CmisCus0020RespDto();
        //客户编号
        String cusId = reqDto.getCusId();

        try{
            int count = cusLstGlfService.queryCountsByCusId(cusId);

            respDto.setCount(count);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsEnum.TRADE_CODE_CMISCUS0020.key+"报错：",e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0020.key, DscmsEnum.TRADE_CODE_CMISCUS0020.value);
        return respDto;
    }
}