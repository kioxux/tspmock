package cn.com.yusys.yusp.service.server.xdkh0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0005.req.Xdkh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0005.resp.Xdkh0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0005Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-28 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0005Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0005Service.class);

    @Resource
    private CusIntbankMapper cusIntbankMapper;

    /**
     * 同业客户信息查询
     *
     * @param xdkh0005DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0005DataRespDto getXdkh0005(Xdkh0005DataReqDto xdkh0005DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005DataReqDto));
        Xdkh0005DataRespDto xdkh0005DataRespDto = new Xdkh0005DataRespDto();
        try {
            //金融同业
            List<cn.com.yusys.yusp.dto.server.xdkh0005.resp.List> list = cusIntbankMapper.getBankInfo(xdkh0005DataReqDto);
            xdkh0005DataRespDto.setList(list);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, e.getMessage());
            throw BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005DataRespDto));
        return xdkh0005DataRespDto;
    }
}
