package cn.com.yusys.yusp.vo;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import cn.com.yusys.yusp.reportconf.domain.FncConfStyles;

public class CellExtendVo {

	private FncConfStyles fncConfStyles;
	
	private CellVO cellVO;
	
	private HSSFSheet sheet;

	public FncConfStyles getFncConfStyles() {
		return fncConfStyles;
	}

	public void setFncConfStyles(FncConfStyles fncConfStyles) {
		this.fncConfStyles = fncConfStyles;
	}

	public CellVO getCellVO() {
		return cellVO;
	}

	public void setCellVO(CellVO cellVO) {
		this.cellVO = cellVO;
	}

	public HSSFSheet getSheet() {
		return sheet;
	}

	public void setSheet(HSSFSheet sheet) {
		this.sheet = sheet;
	}
	
}
