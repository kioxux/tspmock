package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusLstYnd;
import cn.com.yusys.yusp.domain.CusLstYndApp;
import cn.com.yusys.yusp.domain.CusLstYxd;
import cn.com.yusys.yusp.domain.CusLstYxdJbxxApp;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.MessageSendDto;
import cn.com.yusys.yusp.dto.ReceivedUserDto;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 优享贷、优农贷准入名单申请流程
 * @author liucheng
 * @version 1.0.0
 */
@Service
public class KHGL10BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(KHGL10BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CusLstYxdJbxxAppService yxdService;

    @Autowired
    private CusLstYndAppService yndService;

    @Autowired
    private CusLstYndService cusLstYndService;

    @Autowired
    private CusLstYxdService cusLstYxdService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private CusLstYndJyxxAppService cusLstYndJyxxAppService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private MessageSendService messageSendService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        if("KH019".equals(bizType)){
            kh019Process(resultInstanceDto);
        }else if("KH020".equals(bizType)){
            kh020Process(resultInstanceDto);
        }else{
            log.error("业务类型错误：{}", bizType);
        }
    }

    private void kh020Process(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("后业务处理类型" + currentOpType);
        try {
            CusLstYxdJbxxApp domain = yxdService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
//                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
//                yxdService.update(domain);
                log.info("优享贷准入名单申请流程【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                yxdService.update(domain);
                log.info("优享贷准入名单申请流程【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                yxdService.update(domain);
                log.info("优享贷准入名单申请流程【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                // 1、插入cus_lst_yxd 表
                CusLstYxd cusLstYxd = new CusLstYxd();
                BeanUtils.copyProperties(domain, cusLstYxd);
                cusLstYxd.setInureDate(DateUtils.getCurrDateStr()); // 生效时间设置为审批通过时间
                cusLstYxd.setManagerId(domain.getHuser());
                cusLstYxd.setBelgOrg(domain.getHandOrg());
                cusLstYxdService.insert(cusLstYxd);
                // 2、更新申请表
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                yxdService.update(domain);
                log.info("优享贷准入名单申请流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("优享贷准入名单申请流程【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 判断是否打回发起人节点
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                    domain.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    yxdService.update(domain);
                }
                log.info("优享贷准入名单申请流程【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("优享贷准入名单申请流程【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                yxdService.update(domain);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("优享贷准入名单申请流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                yxdService.update(domain);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("优享贷准入名单申请流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                yxdService.update(domain);
            } else {
                log.info("优享贷准入名单申请流程【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("优享贷准入名单申请流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    private void kh019Process(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        log.info("后业务处理类型" + currentOpType);
        try {
            CusLstYndApp domain = yndService.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                yndService.update(domain);
                log.info("优农贷名单准入审批流程【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                yndService.update(domain);
                log.info("优农贷名单准入审批流程【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                yndService.update(domain);
                log.info("优农贷名单准入审批流程【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                // 1、插入白名单信息
                endDo(domain);
                // 2、更新申请表
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                yndService.update(domain);
                sendMessage(domain);
                log.info("优农贷名单准入审批流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("优农贷名单准入审批流程【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                yndService.update(domain);
                log.info("优农贷名单准入审批流程【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("优农贷名单准入审批流程【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                yndService.update(domain);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("优农贷名单准入审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                yndService.update(domain);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("优农贷名单准入审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                yndService.update(domain);
            } else {
                log.info("优农贷名单准入审批流程【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("优农贷名单准入审批流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    /**
     * @param cusLstYndApp
     * @return void
     * @author hubp
     * @date 2021/9/24 10:33
     * @version 1.0.0
     * @desc  生成白名单信息 --BUG12206 --生成白名单信息之前，先行排查改客户是否存在于白名单信息，若存在。则将信息覆盖，保留
     *      原有编号，若不存在，则直接生成
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void endDo (CusLstYndApp cusLstYndApp) {
        // 1.将之前通过的申请全部作废
        QueryModel model = new QueryModel();
        model.addCondition("cusId",cusLstYndApp.getCusId());
        List<CusLstYnd>  cusLstYndlist = cusLstYndService.selectByModel(model);
        model.addCondition("approveStatus","997");
        List<CusLstYndApp> cusLstYndAppList = yndService.selectByModel(model);
        for(CusLstYndApp cusLstYndAppTemp : cusLstYndAppList){
            if(!cusLstYndApp.getSerno().equals(cusLstYndAppTemp.getSerno())){
                cusLstYndAppTemp.setApproveStatus("996");
                yndService.updateSelective(cusLstYndAppTemp);
                cusLstYndJyxxAppService.updateTypeBySerno(cusLstYndAppTemp.getSerno());
            }
        }
        if(cusLstYndlist.size() != 0){
            // 2.不等于0直接认定为已存在,将之前白名单软删
            CusLstYnd cusLstYndTemp = cusLstYndlist.get(0);
            CusLstYnd cusLstYnd = new CusLstYnd();
            cusLstYndTemp.setOprType(CmisBizConstants.OPR_TYPE_02);
            cusLstYndService.updateSelective(cusLstYndTemp);
            // 3.开始插入新数据
            BeanUtils.copyProperties(cusLstYndApp, cusLstYnd);
            cusLstYnd.setIdcardNo(cusLstYndApp.getCertCode());
            cusLstYnd.setManagerId(cusLstYndApp.getInputId());
            cusLstYnd.setBelgOrg(cusLstYndApp.getInputBrId());
            cusLstYnd.setListStatus("03");
            cusLstYnd.setOprType(CmisBizConstants.OPR_TYPE_01);
            cusLstYndService.insert(cusLstYnd);
        } else{
            // 不存在，直接新增
            CusLstYnd cusLstYnd = new CusLstYnd();
            BeanUtils.copyProperties(cusLstYndApp, cusLstYnd);
            cusLstYnd.setIdcardNo(cusLstYndApp.getCertCode());
            cusLstYnd.setManagerId(cusLstYndApp.getInputId());
            cusLstYnd.setBelgOrg(cusLstYndApp.getInputBrId());
            cusLstYnd.setOprType(CmisBizConstants.OPR_TYPE_01);
            cusLstYnd.setListStatus("03"); //[{"key":"01","value":"灰名单"},{"key":"02","value":"黑名单"},{"key":"03","value":"白名单"}]
            cusLstYndService.insert(cusLstYnd);
        }
    }
    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "KHGL10".equals(flowCode);
    }

    /**
     * @param cusLstYndApp
     * @return void
     * @author hubp
     * @date 2021/10/22 21:31
     * @version 1.0.0
     * @desc  优农贷白名单任务短信通知
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void sendMessage(CusLstYndApp cusLstYndApp) {
        log.info("*************优农贷白名单任务短信通知开始，流水号：【{}】", cusLstYndApp.getSerno());
        String serno = cusLstYndApp.getSerno();
        String managerId = cusLstYndApp.getHuser();
        String mgrTel = StringUtils.EMPTY;
        // 短信发送对象

        try {
             /** 先发送客户经理 */
            log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            AdminSmUserDto adminSmUserDto = resultDto.getData();
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                adminSmUserDto = resultDto.getData();
                mgrTel = adminSmUserDto.getUserMobilephone();
            }
            MessageSendDto messageSendDto = new MessageSendDto();
            String messageType = "MSG_XW_M_0006";// 优农贷
            String receivedUserType = "1";// 1--客户经理 2--借款人
            Map paramMap = new HashMap();//短信填充参数
            paramMap.put("cusName", cusLstYndApp.getCusName());
            paramMap.put("prdName", "优农贷");
            //执行发送借款人操作
            messageSendDto.setMessageType(messageType);
            messageSendDto.setParams(paramMap);
            List<ReceivedUserDto> receivedUserList = new ArrayList<>();
            ReceivedUserDto receivedUserDto = new ReceivedUserDto();
            receivedUserDto.setReceivedUserType(receivedUserType);
            receivedUserDto.setUserId(managerId);
            receivedUserDto.setMobilePhone(mgrTel);
            receivedUserList.add(receivedUserDto);
            messageSendDto.setReceivedUserList(receivedUserList);
            log.info("*************优农贷白名单任务短信通知客户经理开始，调查流水号：【{}】，发送报文：【{}】", serno, JSON.toJSONString(paramMap));
            ResultDto<Integer> result = messageSendService.sendMessage(messageSendDto);
            log.info("*************小微调查分配任务短信通知客户经理调用开始，调查流水号：【{}】，返回报文：【{}】", serno, JSON.toJSONString(result));


            /** 再发送借款人 */
            MessageSendDto messageSendDtoC = new MessageSendDto();
            String messageTypeC = "MSG_XW_C_0006";// 优农贷
            String receivedUserTypeC = "2";// 1--客户经理 2--借款人
            Map paramMapC = new HashMap();//短信填充参数
            paramMapC.put("cusName", cusLstYndApp.getCusName());
            paramMapC.put("prdName", "优农贷");
            //执行发送借款人操作
            messageSendDtoC.setMessageType(messageTypeC);
            messageSendDtoC.setParams(paramMapC);
            List<ReceivedUserDto> receivedUserListC = new ArrayList<>();
            ReceivedUserDto receivedUserDtoC = new ReceivedUserDto();
            receivedUserDtoC.setReceivedUserType(receivedUserTypeC);
            receivedUserDtoC.setUserId(managerId);
            receivedUserDtoC.setMobilePhone(cusLstYndApp.getMobileNo());
            receivedUserListC.add(receivedUserDtoC);
            messageSendDtoC.setReceivedUserList(receivedUserListC);
            log.info("*************优农贷白名单任务短信通知借款人开始，调查流水号：【{}】，发送报文：【{}】", serno, JSON.toJSONString(paramMapC));
            ResultDto<Integer> resultC = messageSendService.sendMessage(messageSendDtoC);
            log.info("*************优农贷白名单任务短信通知借款人调用结束，调查流水号：【{}】，返回报文：【{}】", serno, JSON.toJSONString(resultC));
        } catch (Exception e) {
            log.error("*************小微调查分配任务短信通知异常，调查流水号：【{}】，异常信息：【{}】", serno, JSON.toJSONString(e));
        }
        log.info("*************小微调查分配任务短信通知结束，调查流水号：【{}】", serno);
    }
}
