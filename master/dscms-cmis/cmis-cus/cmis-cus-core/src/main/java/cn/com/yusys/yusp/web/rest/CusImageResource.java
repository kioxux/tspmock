/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.Cbocr1ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.resp.Cbocr1RespDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.req.Cbocr2ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.resp.Cbocr2RespDto;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.req.YxljcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.resp.YxljcxRespDto;
import cn.com.yusys.yusp.dto.client.http.image.callimage.CallImageReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenRespDto;
import cn.com.yusys.yusp.dto.client.http.ocr.req.Cbocr3ReqDto;
import cn.com.yusys.yusp.dto.client.http.ocr.resp.Cbocr3RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2ImageClientService;
import cn.com.yusys.yusp.service.Dscms2OcrClientService;
import cn.com.yusys.yusp.service.Dscms2OuterdataClientService;
import cn.com.yusys.yusp.service.Dscms2YxxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusImageResource
 * @类描述: #影像
 * @功能描述:
 * @创建人: zhoumw
 * @创建时间: 2021年4月24日20:46:52
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/image")
public class CusImageResource {
    private static final Logger log = LoggerFactory.getLogger(CusImageResource.class);

    @Autowired
    private Dscms2ImageClientService dscms2ImageClientService;

    @Autowired
    private Dscms2YxxtClientService dscms2YxxtClientService;

    @Autowired
    private Dscms2OcrClientService dscms2OcrClientService;

    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

	/**
     * 影像url获取
     *
     * @param
     * @return
     */
    @PostMapping ("/getImageSrc")
    protected ResultDto<String> queryImageUrl(@RequestBody CallImageReqDto callImageReqDto) {
        return dscms2ImageClientService.imageBizUrl(callImageReqDto);
    }


    /**
     * @函数名称:getImageSysToken
     * @函数描述:获取影像系统token
     * @参数与返回说明:
     * @算法描述:
     */
    @Deprecated
    @PostMapping("/getImageSysToken")
    protected ResultDto<ImageTokenRespDto> getImageSysToken(@RequestBody ImageTokenReqDto imageTokenReqDto){
//        ImageTokenReqDto imageTokenReqDto = new ImageTokenReqDto();
//        imageTokenReqDto.setUsername(useName);
//        imageTokenReqDto.setPassword(passWord);
        return dscms2ImageClientService.imagetoken(imageTokenReqDto);
    }

    /**
     * 影像图像路径查询
     * @param reqDto
     * @return
     */
    @PostMapping("/getImageFilePath")
    protected ResultDto<YxljcxRespDto> getImageFilePath(@RequestBody YxljcxReqDto reqDto){
        return dscms2YxxtClientService.yxljcx(reqDto);
    }

    /**
     * 新增批次报表数据
     * @param reqDto
     * @return
     */
    @PostMapping("/cbocr1")
    protected ResultDto<Cbocr1RespDto> getOcr1(@RequestBody Cbocr1ReqDto reqDto){
        return dscms2OcrClientService.cbocr1(reqDto);
    }

    /**
     *识别任务的模板匹配结果列表查询
     * @param reqDto
     * @return
     */
    @PostMapping("/cbocr2")
    protected ResultDto<Cbocr2RespDto> getOcr2(@RequestBody Cbocr2ReqDto reqDto){
        return dscms2OcrClientService.cbocr2(reqDto);
    }


    /**
     * 报表核对统计总览列表查询
     * @param reqDto
     * @return
     */
    @PostMapping("/cbocr3")
    protected ResultDto<Cbocr3RespDto> cbocr3(@RequestBody Cbocr3ReqDto reqDto){
        return dscms2OuterdataClientService.cbocr3(reqDto);
    }



}
