package cn.com.yusys.yusp.fncstat.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
//import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * 财务简表暂存/完成参数值对象
 */
@ApiModel("财务简表暂存/完成参数值对象")
public class FncStatRptBriefDTO implements Serializable {

    private static final long serialVersionUID = 8465100476088471609L;
    @ApiModelProperty("简表详细信息")
    private List<FncStatRptDTO> reportInfo;

    public List<FncStatRptDTO> getReportInfo() {
        return reportInfo;
    }

    public void setReportInfo(List<FncStatRptDTO> reportInfo) {
        this.reportInfo = reportInfo;
    }

//    @Override
//    public String toString() {
//        return ToStringBuilder.reflectionToString(this);
//    }
    @Override
    public String toString() {
        return "FncStatRptBriefDTO{" +
                "reportInfo=" + reportInfo +
                '}';
    }
}
