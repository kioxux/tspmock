/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusPubRelInvest
 * @类描述: cus_pub_rel_invest数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-05-04 22:21:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_pub_rel_invest")
public class CusPubRelInvest extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 被投资人客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 投资性质 STD_ZB_COM_INV_TYP **/
	@Column(name = "COM_INV_TYP", unique = false, nullable = true, length = 5)
	private String comInvTyp;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 30)
	private String certCode;
	
	/** 关联客户编号 **/
	@Column(name = "CUS_ID_REL", unique = false, nullable = true, length = 30)
	private String cusIdRel;
	
	/** 被投资企业名称(全称) **/
	@Column(name = "CUS_NAME_REL", unique = false, nullable = true, length = 80)
	private String cusNameRel;
	
	/** 注册登记号 **/
	@Column(name = "REGI_NO", unique = false, nullable = true, length = 40)
	private String regiNo;
	
	/** 贷款卡号 **/
	@Column(name = "LN_CARD_NO", unique = false, nullable = true, length = 40)
	private String lnCardNo;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 投资金额(万元) **/
	@Column(name = "INV_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal invAmt;
	
	/** 出资方式 STD_ZB_INVT_TYP **/
	@Column(name = "INV_APP", unique = false, nullable = true, length = 5)
	private String invApp;
	
	/** 投资时间 **/
	@Column(name = "INV_DT", unique = false, nullable = true, length = 10)
	private String invDt;
	
	/** 所占比例 **/
	@Column(name = "INV_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal invPerc;
	
	/** 投资说明 **/
	@Column(name = "INV_DESC", unique = false, nullable = true, length = 500)
	private String invDesc;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param comInvTyp
	 */
	public void setComInvTyp(String comInvTyp) {
		this.comInvTyp = comInvTyp;
	}
	
    /**
     * @return comInvTyp
     */
	public String getComInvTyp() {
		return this.comInvTyp;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param cusIdRel
	 */
	public void setCusIdRel(String cusIdRel) {
		this.cusIdRel = cusIdRel;
	}
	
    /**
     * @return cusIdRel
     */
	public String getCusIdRel() {
		return this.cusIdRel;
	}
	
	/**
	 * @param cusNameRel
	 */
	public void setCusNameRel(String cusNameRel) {
		this.cusNameRel = cusNameRel;
	}
	
    /**
     * @return cusNameRel
     */
	public String getCusNameRel() {
		return this.cusNameRel;
	}
	
	/**
	 * @param regiNo
	 */
	public void setRegiNo(String regiNo) {
		this.regiNo = regiNo;
	}
	
    /**
     * @return regiNo
     */
	public String getRegiNo() {
		return this.regiNo;
	}
	
	/**
	 * @param lnCardNo
	 */
	public void setLnCardNo(String lnCardNo) {
		this.lnCardNo = lnCardNo;
	}
	
    /**
     * @return lnCardNo
     */
	public String getLnCardNo() {
		return this.lnCardNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param invAmt
	 */
	public void setInvAmt(java.math.BigDecimal invAmt) {
		this.invAmt = invAmt;
	}
	
    /**
     * @return invAmt
     */
	public java.math.BigDecimal getInvAmt() {
		return this.invAmt;
	}
	
	/**
	 * @param invApp
	 */
	public void setInvApp(String invApp) {
		this.invApp = invApp;
	}
	
    /**
     * @return invApp
     */
	public String getInvApp() {
		return this.invApp;
	}
	
	/**
	 * @param invDt
	 */
	public void setInvDt(String invDt) {
		this.invDt = invDt;
	}
	
    /**
     * @return invDt
     */
	public String getInvDt() {
		return this.invDt;
	}
	
	/**
	 * @param invPerc
	 */
	public void setInvPerc(java.math.BigDecimal invPerc) {
		this.invPerc = invPerc;
	}
	
    /**
     * @return invPerc
     */
	public java.math.BigDecimal getInvPerc() {
		return this.invPerc;
	}
	
	/**
	 * @param invDesc
	 */
	public void setInvDesc(String invDesc) {
		this.invDesc = invDesc;
	}
	
    /**
     * @return invDesc
     */
	public String getInvDesc() {
		return this.invDesc;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
}