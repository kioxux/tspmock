package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivEdu
 * @类描述: cus_indiv_edu数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-03-13 14:16:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusIndivEduDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 开始日期 **/
	private String beginDate;
	
	/** 结束日期 **/
	private String endDate;
	
	/** 所在学校 **/
	private String ubietySchool;
	
	/** 所在院系 **/
	private String ubietyDepartment;
	
	/** 专业 **/
	private String major;
	
	/** 最高学历 STD_ZB_EDU **/
	private String indivEdt;
	
	/** 最高学位 STD_ZB_DEGREE **/
	private String indivDgr;
	
	/** 学历证书号 **/
	private String eduCode;
	
	/** 学位证书号 **/
	private String dgrCode;
	
	/** 备注 **/
	private String remark;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param beginDate
	 */
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate == null ? null : beginDate.trim();
	}
	
    /**
     * @return BeginDate
     */	
	public String getBeginDate() {
		return this.beginDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param ubietySchool
	 */
	public void setUbietySchool(String ubietySchool) {
		this.ubietySchool = ubietySchool == null ? null : ubietySchool.trim();
	}
	
    /**
     * @return UbietySchool
     */	
	public String getUbietySchool() {
		return this.ubietySchool;
	}
	
	/**
	 * @param ubietyDepartment
	 */
	public void setUbietyDepartment(String ubietyDepartment) {
		this.ubietyDepartment = ubietyDepartment == null ? null : ubietyDepartment.trim();
	}
	
    /**
     * @return UbietyDepartment
     */	
	public String getUbietyDepartment() {
		return this.ubietyDepartment;
	}
	
	/**
	 * @param major
	 */
	public void setMajor(String major) {
		this.major = major == null ? null : major.trim();
	}
	
    /**
     * @return Major
     */	
	public String getMajor() {
		return this.major;
	}
	
	/**
	 * @param indivEdt
	 */
	public void setIndivEdt(String indivEdt) {
		this.indivEdt = indivEdt == null ? null : indivEdt.trim();
	}
	
    /**
     * @return IndivEdt
     */	
	public String getIndivEdt() {
		return this.indivEdt;
	}
	
	/**
	 * @param indivDgr
	 */
	public void setIndivDgr(String indivDgr) {
		this.indivDgr = indivDgr == null ? null : indivDgr.trim();
	}
	
    /**
     * @return IndivDgr
     */	
	public String getIndivDgr() {
		return this.indivDgr;
	}
	
	/**
	 * @param eduCode
	 */
	public void setEduCode(String eduCode) {
		this.eduCode = eduCode == null ? null : eduCode.trim();
	}
	
    /**
     * @return EduCode
     */	
	public String getEduCode() {
		return this.eduCode;
	}
	
	/**
	 * @param dgrCode
	 */
	public void setDgrCode(String dgrCode) {
		this.dgrCode = dgrCode == null ? null : dgrCode.trim();
	}
	
    /**
     * @return DgrCode
     */	
	public String getDgrCode() {
		return this.dgrCode;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}