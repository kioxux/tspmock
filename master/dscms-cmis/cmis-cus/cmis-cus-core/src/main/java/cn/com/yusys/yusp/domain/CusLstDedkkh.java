/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.annotation.SeqId;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkh
 * @类描述: cus_lst_dedkkh数据实体类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 14:36:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_dedkkh")
public class CusLstDedkkh extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 名单流水号 **/
	@Id
	@SeqId("LIST_SERNO")
	@Column(name = "LIST_SERNO")
	private String listSerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 20)
	private String cusName;
	
	/** 管户客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 所属机构 **/
	@Column(name = "BELG_ORG", unique = false, nullable = true, length = 10)
	private String belgOrg;
	
	/** 本年度计划总压降金额（万元） **/
	@Column(name = "PLAN_AMT", unique = false, nullable = true, length = 16)
	private String planAmt;
	
	/** 压降年度 **/
	@Column(name = "PRESSURE_DROP_YEARLY", unique = false, nullable = true, length = 10)
	private String pressureDropYearly;
	
	/** 风险缓释措施 **/
	@Column(name = "RISK_RELIEVE_STEP", unique = false, nullable = true, length = 500)
	private String riskRelieveStep;
	
	/** 其他说明 **/
	@Column(name = "OTHER_MEMO", unique = false, nullable = true, length = 500)
	private String otherMemo;
	
	/** 进入名单标识 **/
	@Column(name = "ENTER_LIST_FLAG", unique = false, nullable = true, length = 10)
	private String enterListFlag;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 5)
	private String status;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 10)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 10)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param listSerno
	 */
	public void setListSerno(String listSerno) {
		this.listSerno = listSerno;
	}
	
    /**
     * @return listSerno
     */
	public String getListSerno() {
		return this.listSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg;
	}
	
    /**
     * @return belgOrg
     */
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param planAmt
	 */
	public void setPlanAmt(String planAmt) {
		this.planAmt = planAmt;
	}
	
    /**
     * @return planAmt
     */
	public String getPlanAmt() {
		return this.planAmt;
	}
	
	/**
	 * @param pressureDropYearly
	 */
	public void setPressureDropYearly(String pressureDropYearly) {
		this.pressureDropYearly = pressureDropYearly;
	}
	
    /**
     * @return pressureDropYearly
     */
	public String getPressureDropYearly() {
		return this.pressureDropYearly;
	}
	
	/**
	 * @param riskRelieveStep
	 */
	public void setRiskRelieveStep(String riskRelieveStep) {
		this.riskRelieveStep = riskRelieveStep;
	}
	
    /**
     * @return riskRelieveStep
     */
	public String getRiskRelieveStep() {
		return this.riskRelieveStep;
	}
	
	/**
	 * @param otherMemo
	 */
	public void setOtherMemo(String otherMemo) {
		this.otherMemo = otherMemo;
	}
	
    /**
     * @return otherMemo
     */
	public String getOtherMemo() {
		return this.otherMemo;
	}
	
	/**
	 * @param enterListFlag
	 */
	public void setEnterListFlag(String enterListFlag) {
		this.enterListFlag = enterListFlag;
	}
	
    /**
     * @return enterListFlag
     */
	public String getEnterListFlag() {
		return this.enterListFlag;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}