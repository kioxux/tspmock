package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

/**
 * 网金客户明细信息导入导出Vo
 *
 * @author xuchi
 * @since 2021/6/7
 **/
@ExcelCsv(namePrefix = "网金客户明细信息导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CusLstWjkhMxxxExportVo {



    /**
     * 业务流水号
     **/
    @ExcelField(title = "业务流水号", viewLength = 40)
    private String serno;

    /**
     * 客户编号
     **/
    @ExcelField(title = "客户编号", viewLength = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    @ExcelField(title = "客户名称", viewLength = 80)
    private String cusName;


    /**
     * 证件类型
     **/
    @ExcelField(title = "证件类型", viewLength = 5)
    private String certType;

    /**
     * 证件号码
     **/
    @ExcelField(title = "证件号码", viewLength = 40)
    private String certCode;

    /**
     * 产品名称
     **/
    @ExcelField(title = "产品名称", viewLength = 40)
    private String prdName;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }
}