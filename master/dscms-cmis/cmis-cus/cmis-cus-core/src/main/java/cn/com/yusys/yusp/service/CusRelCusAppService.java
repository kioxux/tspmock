/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.repository.mapper.CusRelCusAppMapper;
import cn.com.yusys.yusp.repository.mapper.CusRelCusMapper;
import cn.com.yusys.yusp.repository.mapper.CusRelCusMemAppMapper;
import cn.com.yusys.yusp.repository.mapper.CusRelCusMemberRelMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusRelCusAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: liqiang
 * @创建时间: 2021-05-07 21:29:39
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusRelCusAppService {

    @Autowired
    private CusRelCusAppMapper cusRelCusAppMapper;
    @Autowired
    private CusRelCusMemAppMapper cusRelCusMemAppMapper;
    @Autowired
    private CusRelCusMemberRelMapper cusRelCusMemberRelMapper;
    @Autowired
    private CusRelCusMapper cusRelCusMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusRelCusApp selectByPrimaryKey(String serno) {
        return cusRelCusAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusRelCusApp> selectAll(QueryModel model) {
        List<CusRelCusApp> records = (List<CusRelCusApp>) cusRelCusAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusRelCusApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusRelCusApp> list = cusRelCusAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusRelCusApp record) {
        return cusRelCusAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusRelCusApp record) {
        return cusRelCusAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusRelCusApp record) {
        return cusRelCusAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusRelCusApp record) {
        return cusRelCusAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusRelCusAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusRelCusAppMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:doCusRelCusAppComit
     * @函数描述:关联客户认定提交
     * @参数与返回说明:
     * @算法描述:
     * @创建人：liqiang
     */
    @Transactional
    public void doCusRelCusAppComit(CusRelCusApp cusRelCusApp){
        CusRelCusApp oldCusRelCusApp = cusRelCusAppMapper.selectByPrimaryKey(cusRelCusApp.getSerno());
        cusRelCusApp.setApproveStatus("997");
        if(oldCusRelCusApp == null){
            cusRelCusAppMapper.insert(cusRelCusApp);
        }else{
            cusRelCusAppMapper.updateByPrimaryKey(cusRelCusApp);
        }
        CusRelCus cusRelCus = new CusRelCus();
        BeanUtils.beanCopy(cusRelCusApp,cusRelCus);
        cusRelCus.setStatus("1");
        cusRelCus.setDismissResn(cusRelCusApp.getResnMemo());
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        cusRelCus.setIdentyDate(dateFormat.format(date));
        cusRelCusMapper.insert(cusRelCus);

        QueryModel model = new QueryModel();
        model.addCondition("serno",cusRelCusApp.getSerno());
        List<CusRelCusMemApp> listCusRelCusMemApp = cusRelCusMemAppMapper.selectByModel(model);
        if(listCusRelCusMemApp != null){
            for (int i = 0 ; i<listCusRelCusMemApp.size(); i++){
                model.addCondition("correMemCusNo", listCusRelCusMemApp.get(i).getCorreMemCusNo());
                model.addCondition("oprType", "01");
                List<CusRelCusMemberRel> listCusRelCusMemberRel = cusRelCusMemberRelMapper.selectByModel(model);
                if (listCusRelCusMemberRel.size() > 0) {
                    throw BizException.error(null, "9999", "关联客户选择已经被关联");
                }
                CusRelCusMemberRel cusRelCusMemberRel = new CusRelCusMemberRel();
                BeanUtils.beanCopy(listCusRelCusMemApp.get(i),cusRelCusMemberRel);
                cusRelCusMemberRelMapper.insert(cusRelCusMemberRel);
            }
        }
    }

    /**
     * @函数名称:getMemAppFromRel
     * @函数描述:关联客户变更下一步逻辑
     * @参数与返回说明:
     * @算法描述:
     * @创建人：liqiang
     */
    @Transactional
    public void getMemAppFromRel(Map map){
        String correNo = (String) map.get("correNo");
        String serno = (String) map.get("serno");
        CusRelCus cusRelCus = cusRelCusMapper.selectByPrimaryKey(correNo);
        List<CusRelCusMemberRel> listCusRelCusMemberRel = new ArrayList<CusRelCusMemberRel>();
        if(cusRelCus!=null){
            QueryModel model = new QueryModel();
            model.addCondition("correNo",correNo);
            listCusRelCusMemberRel = cusRelCusMemberRelMapper.selectByModel(model);
            if(listCusRelCusMemberRel != null){
                for (int i = 0 ; i<listCusRelCusMemberRel.size(); i++){
                    CusRelCusMemApp cusRelCusMemApp = new CusRelCusMemApp();
                    BeanUtils.beanCopy(listCusRelCusMemberRel.get(i),cusRelCusMemApp);
                    cusRelCusMemApp.setPkId(null);
                    cusRelCusMemApp.setSerno(serno);
                    cusRelCusMemApp.setModifyType("01");
                    cusRelCusMemApp.setOprType("01");
                    cusRelCusMemAppMapper.insert(cusRelCusMemApp);
                }
            }
        }
    }


}
