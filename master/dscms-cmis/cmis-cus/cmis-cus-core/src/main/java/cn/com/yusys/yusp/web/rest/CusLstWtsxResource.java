/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.dto.CusIndivDto;
import cn.com.yusys.yusp.dto.CusLstWtsxDto;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.CusLstDedkkhVo;
import cn.com.yusys.yusp.vo.CusLstWtsxExportVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstWtsx;
import cn.com.yusys.yusp.service.CusLstWtsxService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWtsxResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:17:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstwtsx")
public class CusLstWtsxResource {
    @Autowired
    private CusLstWtsxService cusLstWtsxService;
    private final Logger logger = LoggerFactory.getLogger(CusLstJjycResource.class);
	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstWtsx>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstWtsx> list = cusLstWtsxService.selectAll(queryModel);
        return new ResultDto<List<CusLstWtsx>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstWtsx>> index(QueryModel queryModel) {
        List<CusLstWtsx> list = cusLstWtsxService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWtsx>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/queryList")
    protected ResultDto<List<CusLstWtsx>> queryListData(@RequestBody QueryModel queryModel) {
        List<CusLstWtsx> list = cusLstWtsxService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWtsx>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstWtsx> show(@PathVariable("serno") String serno) {
        CusLstWtsx cusLstWtsx = cusLstWtsxService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstWtsx>(cusLstWtsx);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstWtsx> create(@RequestBody CusLstWtsx cusLstWtsx) throws URISyntaxException {
        cusLstWtsxService.insert(cusLstWtsx);
        return new ResultDto<CusLstWtsx>(cusLstWtsx);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstWtsx cusLstWtsx) throws URISyntaxException {
        int result = cusLstWtsxService.update(cusLstWtsx);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstWtsxService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstWtsxService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateSelective
     * @函数描述:启用/停用
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody CusLstWtsx cusLstWtsx) {
        //int result = cusLstWtxsService.update(cusLstWtxs);
        int result = cusLstWtsxService.updateSelective(cusLstWtsx);
        return ResultDto.success(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = cusLstWtsxService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库，StudentScore为导入数据的类
        try {
            ExcelUtils.syncImport(CusLstWtsxExportVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return cusLstWtsxService.insertInBatch(dataList);
            }), true);
        } catch (Exception e) {
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }

    /**
     *
     *根据客户号查询是否存在授信名单且导入模式为人工维护
     * @param cusId
     * @return
     * @throws IOException
     */
    @PostMapping("/querycuslstwtsxbycusId")
    public ResultDto<CusLstWtsxDto> queryCuslstwtsxByCusId(@RequestBody String cusId){
        CusLstWtsx cusLstWtsx = cusLstWtsxService.queryCuslstwtsxByCusId(cusId);
        CusLstWtsxDto cusLstWtsxDto = new CusLstWtsxDto();
        BeanUtils.beanCopy(cusLstWtsx, cusLstWtsxDto);
        return new ResultDto<CusLstWtsxDto>(cusLstWtsxDto);
    }
}
