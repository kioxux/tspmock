package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncItemMap
 * @类描述: fnc_item_map数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 14:19:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class FncItemMapDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** serno **/
	private String serno;
	
	/** 信贷科目号 **/
	private String fncItemId;
	
	/** 信贷科目名 **/
	private String fncItemName;
	
	/** OCR科目编码 **/
	private String ocrSubjectId;
	
	/** OCR科目名 **/
	private String ocrSubjectName;
	
	/** 报表样式编号 **/
	private String styleId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param fncItemId
	 */
	public void setFncItemId(String fncItemId) {
		this.fncItemId = fncItemId == null ? null : fncItemId.trim();
	}
	
    /**
     * @return FncItemId
     */	
	public String getFncItemId() {
		return this.fncItemId;
	}
	
	/**
	 * @param fncItemName
	 */
	public void setFncItemName(String fncItemName) {
		this.fncItemName = fncItemName == null ? null : fncItemName.trim();
	}
	
    /**
     * @return FncItemName
     */	
	public String getFncItemName() {
		return this.fncItemName;
	}
	
	/**
	 * @param ocrSubjectId
	 */
	public void setOcrSubjectId(String ocrSubjectId) {
		this.ocrSubjectId = ocrSubjectId == null ? null : ocrSubjectId.trim();
	}
	
    /**
     * @return OcrSubjectId
     */	
	public String getOcrSubjectId() {
		return this.ocrSubjectId;
	}
	
	/**
	 * @param ocrSubjectName
	 */
	public void setOcrSubjectName(String ocrSubjectName) {
		this.ocrSubjectName = ocrSubjectName == null ? null : ocrSubjectName.trim();
	}
	
    /**
     * @return OcrSubjectName
     */	
	public String getOcrSubjectName() {
		return this.ocrSubjectName;
	}
	
	/**
	 * @param styleId
	 */
	public void setStyleId(String styleId) {
		this.styleId = styleId == null ? null : styleId.trim();
	}
	
    /**
     * @return StyleId
     */	
	public String getStyleId() {
		return this.styleId;
	}


}