package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYpqfWhite
 * @类描述: cus_lst_ypqf_white数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:19:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusLstYpqfWhiteDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 申请理由 **/
	private String appResn;
	
	/** 申请有效期 **/
	private String appIdate;
	
	/** 管户客户经理 **/
	private String managerId;
	
	/** 所属机构 **/
	private String belgOrg;
	
	/** 状态 **/
	private String status;
	
	/** 录入人 **/
	private String regiId;
	
	/** 录入机构 **/
	private String regiBrId;
	
	/** 录入日期 **/
	private String regiDate;
	
	/** 最近修改人 **/
	private String lastUpdateId;
	
	/** 最近修改机构 **/
	private String lastUpdateBrId;
	
	/** 最近修改日期 **/
	private String lastUpdateDate;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新时间 **/
	private String updDate;
	
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn == null ? null : appResn.trim();
	}
	
    /**
     * @return AppResn
     */	
	public String getAppResn() {
		return this.appResn;
	}
	
	/**
	 * @param appIdate
	 */
	public void setAppIdate(String appIdate) {
		this.appIdate = appIdate == null ? null : appIdate.trim();
	}
	
    /**
     * @return AppIdate
     */	
	public String getAppIdate() {
		return this.appIdate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg == null ? null : belgOrg.trim();
	}
	
    /**
     * @return BelgOrg
     */	
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}
	
    /**
     * @return Status
     */	
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param regiId
	 */
	public void setRegiId(String regiId) {
		this.regiId = regiId == null ? null : regiId.trim();
	}
	
    /**
     * @return RegiId
     */	
	public String getRegiId() {
		return this.regiId;
	}
	
	/**
	 * @param regiBrId
	 */
	public void setRegiBrId(String regiBrId) {
		this.regiBrId = regiBrId == null ? null : regiBrId.trim();
	}
	
    /**
     * @return RegiBrId
     */	
	public String getRegiBrId() {
		return this.regiBrId;
	}
	
	/**
	 * @param regiDate
	 */
	public void setRegiDate(String regiDate) {
		this.regiDate = regiDate == null ? null : regiDate.trim();
	}
	
    /**
     * @return RegiDate
     */	
	public String getRegiDate() {
		return this.regiDate;
	}
	
	/**
	 * @param lastUpdateId
	 */
	public void setLastUpdateId(String lastUpdateId) {
		this.lastUpdateId = lastUpdateId == null ? null : lastUpdateId.trim();
	}
	
    /**
     * @return LastUpdateId
     */	
	public String getLastUpdateId() {
		return this.lastUpdateId;
	}
	
	/**
	 * @param lastUpdateBrId
	 */
	public void setLastUpdateBrId(String lastUpdateBrId) {
		this.lastUpdateBrId = lastUpdateBrId == null ? null : lastUpdateBrId.trim();
	}
	
    /**
     * @return LastUpdateBrId
     */	
	public String getLastUpdateBrId() {
		return this.lastUpdateBrId;
	}
	
	/**
	 * @param lastUpdateDate
	 */
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate == null ? null : lastUpdateDate.trim();
	}
	
    /**
     * @return LastUpdateDate
     */	
	public String getLastUpdateDate() {
		return this.lastUpdateDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}


}