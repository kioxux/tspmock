/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.vo.CusLstGlfVo;
import cn.com.yusys.yusp.vo.CusLstYndVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYnd;
import cn.com.yusys.yusp.service.CusLstYndService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 09:51:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstynd")
public class CusLstYndResource {
    @Autowired
    private CusLstYndService cusLstYndService;
    private final Logger logger = LoggerFactory.getLogger(CusLstYndResource.class);
	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstYnd>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstYnd> list = cusLstYndService.selectAll(queryModel);
        return new ResultDto<List<CusLstYnd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstYnd>> index(QueryModel queryModel) {
        List<CusLstYnd> list = cusLstYndService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYnd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusLstYnd> show(@PathVariable("serno") String serno) {
        CusLstYnd cusLstYnd = cusLstYndService.selectByPrimaryKey(serno);
        return new ResultDto<CusLstYnd>(cusLstYnd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstYnd> create(@RequestBody CusLstYnd cusLstYnd) throws URISyntaxException {
        cusLstYndService.insert(cusLstYnd);
        return new ResultDto<CusLstYnd>(cusLstYnd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstYnd cusLstYnd) throws URISyntaxException {
        int result = cusLstYndService.updateSelective(cusLstYnd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusLstYndService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstYndService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = cusLstYndService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:asyncExportCreditList
     * @函数描述:异步导出优农贷信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/asyncExportList")
    protected ResultDto<ProgressDto> asyncExportCreditList(@RequestBody QueryModel queryModel) {
        ProgressDto progressDto = cusLstYndService.asyncExportList(queryModel);
        return ResultDto.success(progressDto);
    }
    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        // TODO  修改业务流水号
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库
        ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(CusLstYndVo.class)
                // 批量操作需要将batch设置为true
                .batch(true)
                .file(tempFile)
                .dataStorage(ExcelUtils.batchConsumer(cusLstYndService::insertInBatch)));
        logger.info("开始执行异步导出，导入taskId为[{}];", progressDto.getTaskId());
        return ResultDto.success(progressDto);
    }

    /**
     * @param cusLstYnd
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYnd>
     * @author hubp
     * @date 2021/8/23 11:21
     * @version 1.0.0
     * @desc    新增一个优农贷名单
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addcuslstynd")
    protected ResultDto<CusLstYnd> addCusLstYnd(@RequestBody CusLstYnd cusLstYnd) throws URISyntaxException {
        return cusLstYndService.addCusLstYnd(cusLstYnd);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.CusLstYnd>>
     * @author hubp
     * @date 2021/8/23 13:44
     * @version 1.0.0
     * @desc  查找优农贷列表
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CusLstYnd>> selectByModel(@RequestBody QueryModel queryModel) {
        List<CusLstYnd> list = cusLstYndService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYnd>>(list);
    }

    /**
     * @param cusLstYnd
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List<cn.com.yusys.yusp.domain.CusLstYnd>>
     * @author hubp
     * @date 2021/8/23 13:45
     * @version 1.0.0
     * @desc    通过流水号查找单条数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<CusLstYnd> selectBySerno(@RequestBody CusLstYnd cusLstYnd) {
        CusLstYnd result = cusLstYndService.selectByPrimaryKey(cusLstYnd.getSerno());
        return new ResultDto<CusLstYnd>(result);
    }
}
