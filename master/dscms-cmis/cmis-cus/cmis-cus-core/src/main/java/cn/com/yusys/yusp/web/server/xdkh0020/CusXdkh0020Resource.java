package cn.com.yusys.yusp.web.server.xdkh0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdkh0020.req.Xdkh0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0020.resp.Xdkh0020DataRespDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0020.Xdkh0020Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:小微平台请求信贷线上开户
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDKH0020:客户查询并开户")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0020Resource.class);

    @Autowired
    private Xdkh0020Service xdkh0020Service;

    /**
     * 交易码：xdkh0020
     * 交易描述：小微平台请求信贷线上开户
     *
     * @param xdkh0020DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdkh0020:客户查询并开户")
    @PostMapping("/xdkh0020")
    protected @ResponseBody
    ResultDto<Xdkh0020DataRespDto> xdkh0020(@Validated @RequestBody Xdkh0020DataReqDto xdkh0020DataReqDto) throws Exception {
        Xdkh0020DataRespDto xdkh0020DataRespDto = new Xdkh0020DataRespDto();// 响应Dto:小微平台请求信贷线上开户
        ResultDto<Xdkh0020DataRespDto> xdkh0020DataResultDto = new ResultDto<>();
        // 从xdkh0020DataReqDto获取业务值进行业务逻辑处理
        try {
            xdkh0020DataRespDto = xdkh0020Service.xdkh0020(xdkh0020DataReqDto);
            // 封装xdkh0020DataResultDto中正确的返回码和返回信息
            xdkh0020DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0020DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            // 封装xdkh0020DataResultDto中异常返回码和返回信息
            xdkh0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0020DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            // 封装xdkh0020DataResultDto中异常返回码和返回信息
            xdkh0020DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0020DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0020DataRespDto到xdkh0020DataResultDto中
        xdkh0020DataResultDto.setData(xdkh0020DataRespDto);
        return xdkh0020DataResultDto;
    }
}
