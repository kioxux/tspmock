package cn.com.yusys.yusp.dto;

import cn.com.yusys.yusp.domain.CusLstDedkkh;
import cn.com.yusys.yusp.domain.CusLstDedkkhYjsx;

import java.util.List;

public class CusLstDedkkhYjsxDto {

    private List<CusLstDedkkhYjsx> list;
    private CusLstDedkkh cusLstDedkkh;

    public List<CusLstDedkkhYjsx> getList() {
        return list;
    }

    public void setList(List<CusLstDedkkhYjsx> list) {
        this.list = list;
    }

    public CusLstDedkkh getCusLstDedkkh() {
        return cusLstDedkkh;
    }

    public void setCusLstDedkkh(CusLstDedkkh cusLstDedkkh) {
        this.cusLstDedkkh = cusLstDedkkh;
    }
}
