package cn.com.yusys.yusp.service.server.xdkh0024;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0024.req.Xdkh0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0024.resp.List;
import cn.com.yusys.yusp.dto.server.xdkh0024.resp.Xdkh0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0024Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0024Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0024Service.class);

    @Resource
    private CusBaseMapper cusBaseMapper;

    /**
     * 优企贷、优农贷客户基本信息查询
     *
     * @param xdkh0024DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0024DataRespDto xdkh0024(Xdkh0024DataReqDto xdkh0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024DataReqDto));
        Xdkh0024DataRespDto xdkh0024DataRespDto = new Xdkh0024DataRespDto();
        try {
            java.util.List certCodes = Arrays.asList(xdkh0024DataReqDto.getCert_code().split(","));
            java.util.List<List> list = cusBaseMapper.getCusListByCusIds(certCodes);
            xdkh0024DataRespDto.setList(list);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024DataRespDto));
        return xdkh0024DataRespDto;
    }
}
