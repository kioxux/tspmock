/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusGrpApp;
import cn.com.yusys.yusp.service.CusGrpAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusGrpAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: fangzhen
 * @创建时间: 2021-03-05 14:24:03
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusgrpapp")
public class CusGrpAppResource {
    @Autowired
    private CusGrpAppService cusGrpAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusGrpApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusGrpApp> list = cusGrpAppService.selectAll(queryModel);
        return new ResultDto<List<CusGrpApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusGrpApp>> index(QueryModel queryModel) {
        List<CusGrpApp> list = cusGrpAppService.selectByModel(queryModel);
        return new ResultDto<List<CusGrpApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusGrpApp>> query(@RequestBody QueryModel queryModel) {
        List<CusGrpApp> list = cusGrpAppService.selectByModel(queryModel);
        return new ResultDto<List<CusGrpApp>>(list);
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CusGrpApp> show(@PathVariable("serno") String serno) {
        CusGrpApp cusGrpApp = cusGrpAppService.selectByPrimaryKey(serno);
        return new ResultDto<CusGrpApp>(cusGrpApp);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{serno}")
    protected ResultDto<CusGrpApp> showpost(@PathVariable("serno") String serno) {
        CusGrpApp cusGrpApp = cusGrpAppService.selectByPrimaryKey(serno);
        return new ResultDto<CusGrpApp>(cusGrpApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusGrpApp> create(@RequestBody CusGrpApp cusGrpApp) throws URISyntaxException {
        cusGrpAppService.insert(cusGrpApp);
        return new ResultDto<CusGrpApp>(cusGrpApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusGrpApp cusGrpApp) throws URISyntaxException {
        int result = cusGrpAppService.updateSelective(cusGrpApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = cusGrpAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusGrpAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 通过申请流水号查询集团客户申请
     * @param cusGrpApp
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<CusGrpApp> selectBySerno(@RequestBody CusGrpApp cusGrpApp) {
        CusGrpApp cusGrpAppNew = cusGrpAppService.selectByPrimaryKey(cusGrpApp.getSerno());
        return new ResultDto<CusGrpApp>(cusGrpAppNew);
    }

    /**
     * 通过申请流水号查询集团客户申请
     * @param cusGrpApp
     * @return
     */
    @PostMapping("/selectbycusid")
    protected ResultDto<Map> selectByCusId(@RequestBody CusGrpApp cusGrpApp) {
        Map result = cusGrpAppService.selectByCusId(cusGrpApp);
        return new ResultDto<Map>(result);
    }

    /**
     * 集团修改页面，根据集团类型，确定成员关系类型
     * @param cusGrpApp
     * @return
     */
    @PostMapping("/updatecusgrpmemInfo")
    protected ResultDto<Map> updateCusGrpMemInfo(@RequestBody CusGrpApp cusGrpApp) {
        Map result = cusGrpAppService.updateCusGrpMemInfo(cusGrpApp);
        return new ResultDto<Map>(result);
    }

    /**
     * @函数名称:flowApprovePass
     * @函数描述:流程审批通过，生成正式集团数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/flowapprovepass")
    protected ResultDto<Integer> flowApprovePass(@RequestBody CusGrpApp cusGrpApp) {
        cusGrpAppService.flowApprovePass(cusGrpApp);
        return new ResultDto<Integer>(0);
    }


    /**
     * @函数名称:checkCusGrpAppInWay
     * @函数描述:校验集团在途
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkcusgrpappinway")
    protected ResultDto<Map> checkCusGrpAppInWay(@RequestBody CusGrpApp cusGrpApp) {
        Map resultMap = cusGrpAppService.checkCusGrpAppInWay(cusGrpApp);
        return new ResultDto<Map>(resultMap);
    }

    /**
     * @函数名称:commit
     * @函数描述:集团客户提交引导
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/commit")
    protected ResultDto<Integer> commit(@RequestBody CusGrpApp cusGrpApp) throws URISyntaxException {
        int result = cusGrpAppService.commit(cusGrpApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:cusgrpapp4town
     * @函数描述:
     * @param map
     * @return
     */
    @ApiOperation("村镇银行集团客户认定变更解散处理")
    @PostMapping("/cusgrpapp4town")
    protected ResultDto<Integer> cusGrpApp4Town(@RequestBody Map map) {
        String serno = map.get("serno").toString();
        String bizType = map.get("bizType").toString();
        cusGrpAppService.handleBusinessAfterEnd(serno, bizType, null);
        return new ResultDto<>(0);
    }

}
