/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.CusMgrDividePercDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusMgrDividePerc;
import cn.com.yusys.yusp.service.CusMgrDividePercService;

import javax.servlet.http.HttpServletRequest;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: esource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 14:22:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusmgrdivideperc")
public class CusMgrDividePercResource {
    @Autowired
    private CusMgrDividePercService cusMgrDividePercService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusMgrDividePerc>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusMgrDividePerc> list = cusMgrDividePercService.selectAll(queryModel);
        return new ResultDto<List<CusMgrDividePerc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusMgrDividePerc>> index(QueryModel queryModel) {
        List<CusMgrDividePerc> list = cusMgrDividePercService.selectByModel(queryModel);
        return new ResultDto<List<CusMgrDividePerc>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CusMgrDividePerc> create(@RequestBody CusMgrDividePerc cusMgrDividePerc) throws URISyntaxException {
        cusMgrDividePercService.insert(cusMgrDividePerc);
        return new ResultDto<CusMgrDividePerc>(cusMgrDividePerc);
    }

    /**
     * @函数名称:selectByPrimaryKey
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectByPrimaryKey")
    protected ResultDto<List<CusMgrDividePerc>> selectByPkId(@RequestBody Map<String,String> map) throws URISyntaxException {
        List<CusMgrDividePerc> cusMgrDividePercs = cusMgrDividePercService.selectByPkId(map.get("pkId"));
        return new ResultDto<List<CusMgrDividePerc>>(cusMgrDividePercs);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusMgrDividePerc cusMgrDividePerc) throws URISyntaxException {
        int result = cusMgrDividePercService.update(cusMgrDividePerc);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String cusId, String managerId) {
        int result = cusMgrDividePercService.deleteByPrimaryKey(cusId, managerId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:query
     * @函数描述:根据客户编号查询列表信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryListInfoByCusId")
    protected ResultDto<List<CusMgrDividePerc>> query(@RequestBody QueryModel queryModel) {
        List<CusMgrDividePerc> list = cusMgrDividePercService.selectByCusId(queryModel);
        return new ResultDto<List<CusMgrDividePerc>>(list);
    }

    /**
     * @函数名称:queryBeforeCreate
     * @函数描述:新增表记录,根据客户号、客户经理号、客户尽经理性质校验存在性,不在在则新增
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryBeforeCreate")
    protected ResultDto<Map<String, Object>> queryBeforeCreate(@RequestBody List<CusMgrDividePerc> cusMgrDividePerc) throws Exception {
        String pkId = cusMgrDividePercService.queryBeforeCreate(cusMgrDividePerc);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("pkId", pkId);
        return new ResultDto<Map<String, Object>>(result);
    }

    /**
     * @函数名称:deleteByParam
     * @函数描述:单个对象删除根据传入参数删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteByParam")
    protected ResultDto<Integer> deleteByParam(@RequestBody CusMgrDividePerc cusMgrDividePerc) {
        int result = cusMgrDividePercService.deleteByParam(cusMgrDividePerc);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateAll
     * @函数描述:整条记录更新
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateAll")
    protected ResultDto<Map<String, Object>> updateAll(@RequestBody List<CusMgrDividePerc> cusMgrDividePerc) throws URISyntaxException {
        String pkId = cusMgrDividePercService.updateAll(cusMgrDividePerc);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("pkId", pkId);
        return new ResultDto<Map<String, Object>>(result);
    }

    /**
     * @函数名称:
     * @函数描述:检查主办与协办客户经理所属机构是否一致
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkIsSameOrg")
    protected ResultDto<Map<String, Object>> checkIsSameOrg(@RequestBody Map<String,Object> map) throws URISyntaxException{
        String managerId = (String) map.get("managerId");
        String managerProp = (String) map.get("managerProp");
        Map<String, Object> result = cusMgrDividePercService.checkIsSameOrg(managerId, managerProp);
        return new ResultDto<Map<String, Object>>(result);
    }

    /**
     * @函数名称:selectEffectListByCusId
     * @函数描述:根据客户编号查询已生效的列表信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectEffectListByCusId")
    protected ResultDto<List<CusMgrDividePerc>> selectEffectListByCusId(@RequestBody QueryModel queryModel) {
        List<CusMgrDividePerc> list = cusMgrDividePercService.selectEffectListByCusId(queryModel);
        return new ResultDto<List<CusMgrDividePerc>>(list);
    }

    /**
     * @函数名称:orgAccessCheck
     * @函数描述:查询当前机构是否可做分成比例
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/orgAccessCheck")
    public ResultDto<Map<String, Object>> orgAccessCheck() {
        Boolean flag = cusMgrDividePercService.orgAccessCheck();
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("flag", flag);
        return new ResultDto<Map<String, Object>>(result);
    }

    /**
     * @函数名称:orgAccessCheck
     * @函数描述:查询客户经理的协办客户经理
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectXBManagerId")
    public ResultDto<CusMgrDividePercDto> selectXBManagerId(@RequestBody String cusId) {
        return ResultDto.success(cusMgrDividePercService.selectXBManagerId(cusId));
    }


    /**
     * @函数名称:isVirtualCusManager
     * @函数描述:查询是否为虚拟客户经理
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/isVirtualCusManager")
    public ResultDto<Map<String, Object>> isVirtualCusManager(@RequestBody Map<String,Object> map) {
        String managerId = (String) map.get("managerId");
        String managerProp = (String) map.get("managerProp");
        Boolean flag = cusMgrDividePercService.isVirtualCusManager(managerId, managerProp);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("flag", flag);
        return new ResultDto<Map<String, Object>>(result);
    }
}
