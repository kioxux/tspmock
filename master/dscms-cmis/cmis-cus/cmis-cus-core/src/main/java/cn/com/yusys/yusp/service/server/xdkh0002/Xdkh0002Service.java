package cn.com.yusys.yusp.service.server.xdkh0002;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0002.req.Xdkh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.resp.Xdkh0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import com.alibaba.fastjson.JSON;
import org.apache.ibatis.annotations.Param;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0002Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: lihh
 * @创建时间: 2021-04-24 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class Xdkh0002Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0002Service.class);
    @Resource
    private CusCorpMapper cusCorpMapper;

    /**
     * 对公客户基本信息查询
     *
     * @param xdkh0002DataReqDto
     * @return
     */
    public Xdkh0002DataRespDto selectCusIdByOrgCode(Xdkh0002DataReqDto xdkh0002DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value);
        String queryType = xdkh0002DataReqDto.getQueryType();//查询类型
        String cusId = xdkh0002DataReqDto.getCusId();//客户号
        String cusName = xdkh0002DataReqDto.getCusName();//客户名称
        String certCode = xdkh0002DataReqDto.getCertCode();//开户证件号码
        String loanCardId = xdkh0002DataReqDto.getLoanCardId();//贷款卡编号（中证码）
        String cmonNo = xdkh0002DataReqDto.getCmonNo();//组织机构代码
        Map<String, Object> req = new HashMap<>();
        if (CmisCusConstants.QUERY_TYPE_01.equals(queryType)) {
            req.put(CmisCusConstants.QUERY_MAP_CUS_ID, cusId);
        } else if (CmisCusConstants.QUERY_TYPE_02.equals(queryType)) {
            req.put(CmisCusConstants.QUERY_MAP_CUS_NAME, cusName);
            if(StringUtils.nonEmpty(certCode)){
                List<String> certCodes = Arrays.asList(certCode.split(","));
                req.put(CmisCusConstants.QUERY_MAP_CERT_CODE, certCodes);
            }
        } else if (CmisCusConstants.QUERY_TYPE_03.equals(queryType)) {
            req.put(CmisCusConstants.QUERY_MAP_LOAN_CARD_ID, loanCardId);
        } else {
            req.put(CmisCusConstants.QUERY_MAP_CMON_NO, cmonNo);
        }
        // managerBrId 最后一位为1 参考老信贷代码 在sql中做掉
        Xdkh0002DataRespDto xdkh0002DataRespDto = cusCorpMapper.selectCusIdByOrgCode(req);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002DataRespDto));
        return xdkh0002DataRespDto;
    }
}
