/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusRelCusMemApp
 * @类描述: cus_rel_cus_mem_app数据实体类
 * @功能描述: 
 * @创建人: liqiang
 * @创建时间: 2021-05-07 21:29:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_rel_cus_mem_app")
public class CusRelCusMemApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 关联编号 **/
	@Column(name = "CORRE_NO", unique = false, nullable = true, length = 20)
	private String correNo;
	
	/** 关联成员客户编号 **/
	@Column(name = "CORRE_MEM_CUS_NO", unique = false, nullable = true, length = 40)
	private String correMemCusNo;
	
	/** 关联成员客户名称 **/
	@Column(name = "CORRE_MEM_CUS_NAME", unique = false, nullable = true, length = 40)
	private String correMemCusName;
	
	/** 关联成员证件类型 **/
	@Column(name = "CORRE_MEM_CERT_TYPE", unique = false, nullable = true, length = 10)
	private String correMemCertType;
	
	/** 关联成员证件号码 **/
	@Column(name = "CORRE_MEM_CERT_NO", unique = false, nullable = true, length = 20)
	private String correMemCertNo;
	
	/** 关联关系类型 **/
	@Column(name = "CORRE_RELA_TYPE", unique = false, nullable = true, length = 5)
	private String correRelaType;
	
	/** 数据来源 **/
	@Column(name = "DATA_SOUR", unique = false, nullable = true, length = 10)
	private String dataSour;
	
	/** 关联关系说明 **/
	@Column(name = "CORRE_RELA_EXPL", unique = false, nullable = true, length = 500)
	private String correRelaExpl;
	
	/** 变更类型 **/
	@Column(name = "MODIFY_TYPE", unique = false, nullable = true, length = 5)
	private String modifyType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param correNo
	 */
	public void setCorreNo(String correNo) {
		this.correNo = correNo;
	}
	
    /**
     * @return correNo
     */
	public String getCorreNo() {
		return this.correNo;
	}
	
	/**
	 * @param correMemCusNo
	 */
	public void setCorreMemCusNo(String correMemCusNo) {
		this.correMemCusNo = correMemCusNo;
	}
	
    /**
     * @return correMemCusNo
     */
	public String getCorreMemCusNo() {
		return this.correMemCusNo;
	}
	
	/**
	 * @param correMemCusName
	 */
	public void setCorreMemCusName(String correMemCusName) {
		this.correMemCusName = correMemCusName;
	}
	
    /**
     * @return correMemCusName
     */
	public String getCorreMemCusName() {
		return this.correMemCusName;
	}
	
	/**
	 * @param correMemCertType
	 */
	public void setCorreMemCertType(String correMemCertType) {
		this.correMemCertType = correMemCertType;
	}
	
    /**
     * @return correMemCertType
     */
	public String getCorreMemCertType() {
		return this.correMemCertType;
	}
	
	/**
	 * @param correMemCertNo
	 */
	public void setCorreMemCertNo(String correMemCertNo) {
		this.correMemCertNo = correMemCertNo;
	}
	
    /**
     * @return correMemCertNo
     */
	public String getCorreMemCertNo() {
		return this.correMemCertNo;
	}
	
	/**
	 * @param correRelaType
	 */
	public void setCorreRelaType(String correRelaType) {
		this.correRelaType = correRelaType;
	}
	
    /**
     * @return correRelaType
     */
	public String getCorreRelaType() {
		return this.correRelaType;
	}
	
	/**
	 * @param dataSour
	 */
	public void setDataSour(String dataSour) {
		this.dataSour = dataSour;
	}
	
    /**
     * @return dataSour
     */
	public String getDataSour() {
		return this.dataSour;
	}
	
	/**
	 * @param correRelaExpl
	 */
	public void setCorreRelaExpl(String correRelaExpl) {
		this.correRelaExpl = correRelaExpl;
	}
	
    /**
     * @return correRelaExpl
     */
	public String getCorreRelaExpl() {
		return this.correRelaExpl;
	}
	
	/**
	 * @param modifyType
	 */
	public void setModifyType(String modifyType) {
		this.modifyType = modifyType;
	}
	
    /**
     * @return modifyType
     */
	public String getModifyType() {
		return this.modifyType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}