/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusLstJjyc;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusLstJjycMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.vo.CusLstJjycExportVo;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstJjycService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:45
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstJjycService {

    @Autowired
    private CusLstJjycMapper cusLstJjycMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CusBaseMapper cusBaseMapper;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private AdminSmOrgService adminSmOrgService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusLstJjyc selectByPrimaryKey(String serno) {
        return cusLstJjycMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusLstJjyc> selectAll(QueryModel model) {
        List<CusLstJjyc> records = (List<CusLstJjyc>) cusLstJjycMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusLstJjyc> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstJjyc> list = cusLstJjycMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusLstJjyc record) {
        return cusLstJjycMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusLstJjyc record) {
        return cusLstJjycMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusLstJjyc record) {
        return cusLstJjycMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusLstJjyc record) {
        return cusLstJjycMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return cusLstJjycMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstJjycMapper.deleteByIds(ids);
    }

    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusLstJjycExportVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param perCustInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> perCustInfoList) {
        List<CusLstJjyc> cusLstJjycList = (List<CusLstJjyc>) BeanUtils.beansCopy(perCustInfoList, CusLstJjyc.class);
        User userInfo = SessionUtils.getUserInformation();
        try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
            CusLstJjycMapper cusLstJjycMapper = sqlSession.getMapper(CusLstJjycMapper.class);
            for (CusLstJjyc cusLstJjyc : cusLstJjycList) {
                if (StringUtil.isEmpty(cusLstJjyc.getCusId())) {
                    throw BizException.error(null, "9999", "客户编号不能为空");
                }
                if (StringUtil.isEmpty(cusLstJjyc.getCusName())) {
                    throw BizException.error(null, "9999", "客户名称不能为空");
                }
                if (StringUtil.isEmpty(cusLstJjyc.getDepCusNo())) {
                    throw BizException.error(null, "9999", "依存客户编号不能为空");
                }
                if (StringUtil.isEmpty(cusLstJjyc.getDepCusName())) {
                    throw BizException.error(null, "9999", "依存客户名称不能为空");
                }
                if (StringUtil.isEmpty(cusLstJjyc.getManagerId())) {
                    throw BizException.error(null, "9999", "管户客户经理不能为空");
                }

                if (StringUtil.isEmpty(cusLstJjyc.getBelgOrg())) {
                    throw BizException.error(null, "9999", "所属机构不能为空");
                }
                ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(cusLstJjyc.getManagerId());
                if (manager.getData().getLoginCode() == null) {
                    throw BizException.error(null, "9999", "管户客户经理号不存在");
                }
                ResultDto<AdminSmOrgDto> Sorg = adminSmOrgService.getByOrgCode(cusLstJjyc.getBelgOrg());
                if (Sorg.getData().getOrgId() == null) {
                    throw BizException.error(null, "9999", "所属机构号不存在");
                }
                QueryModel cqm = new QueryModel();
                cqm.getCondition().put("cusId", cusLstJjyc.getCusId());
                List<CusBase> exists = cusBaseMapper.selectByModel(cqm);
                if(exists.isEmpty()){
                    throw BizException.error(null, "9999", cusLstJjyc.getCusId()+"客户编号不存在");
                }else {
                    for (int i = 0; i<exists.size();i++) {
                        if(!exists.get(i).getCusName().equals(cusLstJjyc.getCusName())){
                            throw BizException.error(null, "9999", "导入的客户编号:"+cusLstJjyc.getCusId()+"对应的客户姓名"
                                    +cusLstJjyc.getCusName()+"与客户信息的名称"+exists.get(i).getCusName()+"不一致");
                        }
                    }
                }
                QueryModel cqmDep = new QueryModel();
                cqmDep.getCondition().put("cusId", cusLstJjyc.getDepCusNo());
                List<CusBase> existsdep = cusBaseMapper.selectByModel(cqmDep);
                if(existsdep.isEmpty()){
                    throw BizException.error(null, "9999", cusLstJjyc.getDepCusNo()+"依存客户编号不存在");
                }else {
                    for (int i = 0; i<existsdep.size();i++) {
                        if(!existsdep.get(i).getCusName().equals(cusLstJjyc.getDepCusName())){
                            throw BizException.error(null, "9999", "导入的依存客户编号:"+cusLstJjyc.getDepCusNo()+"对应的依存客户姓名"
                                    +cusLstJjyc.getDepCusName()+"与客户信息的名称"+existsdep.get(i).getCusName()+"不一致");
                        }
                    }
                }
                String date = DateUtils.getCurrDateTimeStr();
                Date date1 = new Date();
                cusLstJjyc.setSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>()));
                cusLstJjyc.setInputBrId(userInfo.getOrg().getCode());
                cusLstJjyc.setInputId(userInfo.getLoginCode());
                cusLstJjyc.setInputDate(date);
                cusLstJjyc.setCreateTime(date1);
                cusLstJjyc.setUpdateTime(date1);
                cusLstJjyc.setUpdId(userInfo.getLoginCode());
                cusLstJjyc.setStatus("1");
                cusLstJjyc.setUpdBrId(userInfo.getOrg().getCode());
                cusLstJjycMapper.insertSelective(cusLstJjyc);
            }
            sqlSession.flushStatements();
            sqlSession.commit();
        }
        return perCustInfoList.size();
    }

}