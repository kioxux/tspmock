package cn.com.yusys.yusp.demo.workflow.biz;

import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service  //将实现类注入spring容器管理
public class ClientBizDemo implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(ClientBizDemo.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                log.info("提交操作:" + instanceInfo);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + instanceInfo);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                WFException exception = new WFException();
                exception.setBizId(instanceInfo.getBizId());
                exception.setBizType(instanceInfo.getBizType());
                exception.setFlowName(instanceInfo.getFlowName());
                exception.setInstanceId(instanceInfo.getInstanceId());
                exception.setNodeId(instanceInfo.getNodeId());
                exception.setNodeName(instanceInfo.getNodeName());
                exception.setUserId(instanceInfo.getCurrentUserId());
                exception.setOpType(currentOpType);
                // 后业务处理失败时，将异常信息保存到异常表中
                amqpTemplate.convertAndSend(ClientCons.queue_exception, exception);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto instanceInfo) {
        String flowCode = instanceInfo.getFlowCode();
        return "TESTGAOLQ".equals(flowCode);
    }
}

