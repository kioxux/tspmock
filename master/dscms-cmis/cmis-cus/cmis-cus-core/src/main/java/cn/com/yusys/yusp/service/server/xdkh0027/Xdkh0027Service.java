package cn.com.yusys.yusp.service.server.xdkh0027;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0027.req.Xdkh0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0027.resp.List;
import cn.com.yusys.yusp.dto.server.xdkh0027.resp.Xdkh0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0027Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-04-29 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0027Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0027Service.class);

    @Resource
    private CusLstGlfMapper cusLstGlfMapper;

    /**
     * 优企贷、优农贷行内关联自然人基本信息查询
     *
     * @return
     */
    @Transactional
    public Xdkh0027DataRespDto xdkh0027(Xdkh0027DataReqDto xdkh0027DataReqDto) throws Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027DataReqDto));
        Xdkh0027DataRespDto xdkh0027DataRespDto = null;
        try {
            xdkh0027DataRespDto = new Xdkh0027DataRespDto();
            java.util.List<List> list = new ArrayList<>();
            if (!StringUtils.isEmpty(xdkh0027DataReqDto.getCert_code())) {
                list = cusLstGlfMapper.getXdkh0027(xdkh0027DataReqDto);
            }
            xdkh0027DataRespDto.setList(list);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027DataRespDto));
        return xdkh0027DataRespDto;
    }
}
