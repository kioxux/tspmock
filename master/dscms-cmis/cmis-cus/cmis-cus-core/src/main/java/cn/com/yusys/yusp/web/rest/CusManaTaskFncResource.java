/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusManaTaskFnc;
import cn.com.yusys.yusp.service.CusManaTaskFncService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusManaTaskFncResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-23 21:08:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusmanataskfnc")
public class CusManaTaskFncResource {
    @Autowired
    private CusManaTaskFncService cusManaTaskFncService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusManaTaskFnc>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusManaTaskFnc> list = cusManaTaskFncService.selectAll(queryModel);
        return new ResultDto<List<CusManaTaskFnc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusManaTaskFnc>> index(QueryModel queryModel) {
        List<CusManaTaskFnc> list = cusManaTaskFncService.selectByModel(queryModel);
        return new ResultDto<List<CusManaTaskFnc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusManaTaskFnc> show(@PathVariable("pkId") String pkId) {
        CusManaTaskFnc cusManaTaskFnc = cusManaTaskFncService.selectByPrimaryKey(pkId);
        return new ResultDto<CusManaTaskFnc>(cusManaTaskFnc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusManaTaskFnc> create(@RequestBody CusManaTaskFnc cusManaTaskFnc) throws URISyntaxException {
        cusManaTaskFncService.insert(cusManaTaskFnc);
        return new ResultDto<CusManaTaskFnc>(cusManaTaskFnc);
    }
    /**
     * @函数名称:insert
     * @函数描述:实体类批量创建
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<List<CusManaTaskFnc>> insert(@RequestBody List<CusManaTaskFnc> cusManaTaskFnc) throws URISyntaxException {
        for (CusManaTaskFnc cmtf:cusManaTaskFnc) {
            cusManaTaskFncService.insert(cmtf);
        }
        return new ResultDto<List<CusManaTaskFnc>>(cusManaTaskFnc);
    }
    /**
     * @函数名称:insert
     * @函数描述:实体类批量创建
     * @参数与返回说明:
     * @算法描述:解决退回重新提交冲突
     */
    @PostMapping("/insertafterdelete")
    protected ResultDto<List<CusManaTaskFnc>> insertafterdelete(@RequestBody List<CusManaTaskFnc> cusManaTaskFnc) throws URISyntaxException {
        QueryModel qm = new QueryModel();
        qm.addCondition("serno",cusManaTaskFnc.get(0).getSerno());
        qm.addCondition("cusId",cusManaTaskFnc.get(0).getCusId());
        List<CusManaTaskFnc> list = cusManaTaskFncService.selectByModel(qm);
        if(list.size()>0){
            for (CusManaTaskFnc cmtf:list) {
                cusManaTaskFncService.deleteByPrimaryKey(cmtf.getPkId());
            }
        }
        for (CusManaTaskFnc cmtf:cusManaTaskFnc) {
            cusManaTaskFncService.insert(cmtf);
        }
        return new ResultDto<List<CusManaTaskFnc>>(cusManaTaskFnc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusManaTaskFnc cusManaTaskFnc) throws URISyntaxException {
        int result = cusManaTaskFncService.update(cusManaTaskFnc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusManaTaskFncService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusManaTaskFncService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
