package cn.com.yusys.yusp.web.server.xdxw0067;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0067.req.Xdxw0067DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0067.resp.Xdxw0067DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0067.Xdxw0067Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;


/**
 * 接口处理类:调查基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0067:调查基本信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class BizXdxw0067Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0067Resource.class);

    @Autowired
    private Xdxw0067Service xdxw0067Service;

    /**
     * 交易码：xdxw0067
     * 交易描述：调查基本信息查询
     *
     * @param xdxw0067DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调查基本信息查询")
    @PostMapping("/xdxw0067")
    protected @ResponseBody
    ResultDto<Xdxw0067DataRespDto> xdxw0067(@Validated @RequestBody Xdxw0067DataReqDto xdxw0067DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067DataReqDto));
        Xdxw0067DataRespDto xdxw0067DataRespDto = new Xdxw0067DataRespDto();// 响应Dto:调查基本信息查询
        ResultDto<Xdxw0067DataRespDto> xdxw0067DataResultDto = new ResultDto<>();
        String cert_code = xdxw0067DataReqDto.getCert_code();//证件号
        try {
            if (StringUtil.isEmpty(cert_code)) {
                xdxw0067DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdxw0067DataResultDto.setMessage("证件号不能为空");
                return xdxw0067DataResultDto;
            }
            // 从xdxw0067DataReqDto获取业务值进行业务逻辑处理
            // 调用xdxw0067Service层开始
            logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067DataReqDto));
            xdxw0067DataRespDto = xdxw0067Service.xdxw0067(xdxw0067DataReqDto);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067DataRespDto));
            // 封装xdxw0067DataResultDto中正确的返回码和返回信息
            xdxw0067DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0067DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, e.getMessage());
            // 封装xdxw0067DataResultDto中异常返回码和返回信息
            xdxw0067DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0067DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0067DataRespDto到xdxw0067DataResultDto中
        xdxw0067DataResultDto.setData(xdxw0067DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067DataResultDto));
        return xdxw0067DataResultDto;
    }
}
