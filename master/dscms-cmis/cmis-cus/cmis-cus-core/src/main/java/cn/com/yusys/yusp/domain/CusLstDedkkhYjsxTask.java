/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkhYjsxTask
 * @类描述: cus_lst_dedkkh_yjsx_task数据实体类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_dedkkh_yjsx_task")
public class CusLstDedkkhYjsxTask extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "TASK_NO")
	private String taskNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 20)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 20)
	private String cusName;
	
	/** 压降事项 **/
	@Column(name = "PRESSURE_DROP_ITEM", unique = false, nullable = true, length = 20)
	private String pressureDropItem;
	
	/** 计划压降金额（万元） **/
	@Column(name = "PLAN_PDO_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal planPdoAmt;
	
	/** 压降计划完成时间 **/
	@Column(name = "PLAN_PDO_FINISH_TIME", unique = false, nullable = true, length = 20)
	private String planPdoFinishTime;
	
	/** 实际压降金额（万元） **/
	@Column(name = "ACT_PDO_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal actPdoAmt;
	
	/** 压降实际完成时间 **/
	@Column(name = "ACT_PDO_FINISH_DATE", unique = false, nullable = true, length = 20)
	private String actPdoFinishDate;
	
	/** 任务生成日期 **/
	@Column(name = "TASK_CREATE_DATE", unique = false, nullable = true, length = 20)
	private String taskCreateDate;
	
	/** 任务到期日期 **/
	@Column(name = "TASK_END_DATE", unique = false, nullable = true, length = 20)
	private String taskEndDate;
	
	/** 压降落实情况说明 **/
	@Column(name = "PDO_ACT_CASE_MEMO", unique = false, nullable = true, length = 20)
	private String pdoActCaseMemo;
	
	/** 任务终止说明 **/
	@Column(name = "TASK_END_MEMO", unique = false, nullable = true, length = 20)
	private String taskEndMemo;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 20)
	private String approveStatus;
	
	/** 管户客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 所属机构 **/
	@Column(name = "BELG_ORG", unique = false, nullable = true, length = 20)
	private String belgOrg;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param pressureDropItem
	 */
	public void setPressureDropItem(String pressureDropItem) {
		this.pressureDropItem = pressureDropItem;
	}
	
    /**
     * @return pressureDropItem
     */
	public String getPressureDropItem() {
		return this.pressureDropItem;
	}
	
	/**
	 * @param planPdoAmt
	 */
	public void setPlanPdoAmt(java.math.BigDecimal planPdoAmt) {
		this.planPdoAmt = planPdoAmt;
	}
	
    /**
     * @return planPdoAmt
     */
	public java.math.BigDecimal getPlanPdoAmt() {
		return this.planPdoAmt;
	}
	
	/**
	 * @param planPdoFinishTime
	 */
	public void setPlanPdoFinishTime(String planPdoFinishTime) {
		this.planPdoFinishTime = planPdoFinishTime;
	}
	
    /**
     * @return planPdoFinishTime
     */
	public String getPlanPdoFinishTime() {
		return this.planPdoFinishTime;
	}
	
	/**
	 * @param actPdoAmt
	 */
	public void setActPdoAmt(java.math.BigDecimal actPdoAmt) {
		this.actPdoAmt = actPdoAmt;
	}
	
    /**
     * @return actPdoAmt
     */
	public java.math.BigDecimal getActPdoAmt() {
		return this.actPdoAmt;
	}
	
	/**
	 * @param actPdoFinishDate
	 */
	public void setActPdoFinishDate(String actPdoFinishDate) {
		this.actPdoFinishDate = actPdoFinishDate;
	}
	
    /**
     * @return actPdoFinishDate
     */
	public String getActPdoFinishDate() {
		return this.actPdoFinishDate;
	}
	
	/**
	 * @param taskCreateDate
	 */
	public void setTaskCreateDate(String taskCreateDate) {
		this.taskCreateDate = taskCreateDate;
	}
	
    /**
     * @return taskCreateDate
     */
	public String getTaskCreateDate() {
		return this.taskCreateDate;
	}
	
	/**
	 * @param taskEndDate
	 */
	public void setTaskEndDate(String taskEndDate) {
		this.taskEndDate = taskEndDate;
	}
	
    /**
     * @return taskEndDate
     */
	public String getTaskEndDate() {
		return this.taskEndDate;
	}
	
	/**
	 * @param pdoActCaseMemo
	 */
	public void setPdoActCaseMemo(String pdoActCaseMemo) {
		this.pdoActCaseMemo = pdoActCaseMemo;
	}
	
    /**
     * @return pdoActCaseMemo
     */
	public String getPdoActCaseMemo() {
		return this.pdoActCaseMemo;
	}
	
	/**
	 * @param taskEndMemo
	 */
	public void setTaskEndMemo(String taskEndMemo) {
		this.taskEndMemo = taskEndMemo;
	}
	
    /**
     * @return taskEndMemo
     */
	public String getTaskEndMemo() {
		return this.taskEndMemo;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg;
	}
	
    /**
     * @return belgOrg
     */
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}