package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIntbank
 * @类描述: cus_intbank数据实体类
 * @功能描述:
 * @创建人: 乐友先生
 * @创建时间: 2021-04-08 11:10:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusIntbankDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 客户编号 **/
	private String cusId;

	/** 同业机构(行)名称 **/
	private String cusName;

	/** 同业机构(行)英文名称 **/
	private String cusEnName;

	/** 同业机构类型 STD_ZB_INTB_TYPE **/
	private String intbankType;

	/** 同业机构子类型 STD_ZB_INTB_SUB_TYPE **/
	private String intbankSubType;

	/** 大额行号 **/
	private String largeBankNo;

	/** Swift代码 **/
	private String swiftCde;

	/** 证件类型 **/
	private String certType;

	/** 证件号码 **/
	private String certCode;

	/** 国民经济部门编号 **/
	private String natEcoSec;

	/** 控股类型 STD_ZB_HOLD_TYPE **/
	private String holdType;

	/** 客户规模 **/
	private String cusScale;

	/** 金融业企业分类 **/
	private String fnaCllTyp;

	/** 中证码/贷款卡号 **/
	private String loanCardId;

	/** 总行组织机构代码 **/
	private String headInsCde;

	/** 国别 **/
	private String country;

	/** 法人姓名 **/
	private String legalName;

	/** 法人证件类型 STD_ZB_CERT_TYP **/
	private String legalCertType;

	/** 法人证件号码 **/
	private String legalCertCode;

	/** 法人性别 STD_ZB_SEX **/
	private String legalSex;

	/** 法人出生日期 **/
	private String legalBirth;

	/** 税务登记证号（国税） **/
	private String natTaxRegCde;

	/** 税务登记证号（地税） **/
	private String locTaxRegCde;

	/** 营业执照号码 **/
	private String regCde;

	/** 总资产(万元) **/
	private java.math.BigDecimal ressetAmt;

	/** 注册/开办资金币种 STD_ZB_CUR_TYP **/
	private String regCapCurType;

	/** 注册/开办资金 **/
	private java.math.BigDecimal regCapAmt;

	/** 实收资本金额 **/
	private java.math.BigDecimal paidCapAmt;

	/** 所有者权益(万元) **/
	private java.math.BigDecimal owersEquity;

	/** 基本账户开户行 **/
	private String basAccBank;

	/** 基本账户账号 **/
	private String basAccNo;

	/** 是否上市/上市公司标志 STD_ZB_YES_NO **/
	private String mrkFlg;

	/** 上市地1 STD_ZB_MRK_AREA **/
	private String mrkAreaOne;

	/** 上市地2 STD_ZB_MRK_AREA **/
	private String mrkAreaTwo;

	/** 股票代码 **/
	private String stkCode;

	/** 同业机构(行)成立日 **/
	private String intbankBuildDate;

	/** 金融业务许可证 **/
	private String bankProLic;

	/** 同业机构行网址 **/
	private String intbankSite;

	/** 通讯地址省市区/县 **/
	private String contArea;

	/** 通讯地址村 **/
	private String contCountry;

	/** 通讯地址乡/镇 **/
	private String contTown;

	/** 通讯地址 **/
	private String contAddr;

	/** 邮箱 **/
	private String email;

	/** 监管评级 STD_ZB_SUPE_EVAL **/
	private String supeEval;

	/** 外部评级 STD_ZB_EVAL_RST **/
	private String intbankEval;

	/** 评级到期日期 **/
	private String evalEndDt;

	/** 拥有资质名称 **/
	private String owmAptName;

	/** 与我行合作关系 **/
	private String relDrg;

	/** 客户状态 STD_ZB_CUS_ST **/
	private String cusState;

	/** 主管机构 **/
	private String managerBrId;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最近修改人 **/
	private String updId;

	/** 最近修改机构 **/
	private String updBrId;

	/** 最近修改日期 **/
	private String updDate;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/** 社会信用代码 **/
	private String socialCreditCode;

	/** 机构简称 **/
	private String orgForShort;

	/** 评定机构外部STD_ZB_OUT_APPR_ORG   **/
	private String outApprOrg;

	/** 同业非现场监管统计机构编码 **/
	private String intbankOsssOrgCode;

	/** 是否长期STD_ZB_YES_NO **/
	private String isLt;

	/** 经营地区 **/
	private String operArea;

	/** 机构类型说明 **/
	private String orgTypeMemo;

	/** 管户客户经理 **/
	private String managerId;

	/** 行业分类 **/
	private String tradeClass;

	/** 营业执照到期日 **/
	private String expDateEnd;

	/** 注册地行政区划 **/
	private String regiAreaCode;

	/** 注册地址 **/
	private String comRegAdd;

	/** 经营范围 **/
	private String natBusi;

	/** 主联系人电话 **/
	private String linkmanPhone;

	/** 锁定状态 **/
	private String islocked;

	/** 业务类型 **/
	private String bizType;

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

	/**
	 * @return CusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}

	/**
	 * @return CusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param cusEnName
	 */
	public void setCusEnName(String cusEnName) {
		this.cusEnName = cusEnName == null ? null : cusEnName.trim();
	}

	/**
	 * @return CusEnName
	 */
	public String getCusEnName() {
		return this.cusEnName;
	}

	/**
	 * @param intbankType
	 */
	public void setIntbankType(String intbankType) {
		this.intbankType = intbankType == null ? null : intbankType.trim();
	}

	/**
	 * @return IntbankType
	 */
	public String getIntbankType() {
		return this.intbankType;
	}

	/**
	 * @param intbankSubType
	 */
	public void setIntbankSubType(String intbankSubType) {
		this.intbankSubType = intbankSubType == null ? null : intbankSubType.trim();
	}

	/**
	 * @return IntbankSubType
	 */
	public String getIntbankSubType() {
		return this.intbankSubType;
	}

	/**
	 * @param largeBankNo
	 */
	public void setLargeBankNo(String largeBankNo) {
		this.largeBankNo = largeBankNo == null ? null : largeBankNo.trim();
	}

	/**
	 * @return LargeBankNo
	 */
	public String getLargeBankNo() {
		return this.largeBankNo;
	}

	/**
	 * @param swiftCde
	 */
	public void setSwiftCde(String swiftCde) {
		this.swiftCde = swiftCde == null ? null : swiftCde.trim();
	}

	/**
	 * @return SwiftCde
	 */
	public String getSwiftCde() {
		return this.swiftCde;
	}

	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}

	/**
	 * @return CertType
	 */
	public String getCertType() {
		return this.certType;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}

	/**
	 * @return CertCode
	 */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param natEcoSec
	 */
	public void setNatEcoSec(String natEcoSec) {
		this.natEcoSec = natEcoSec == null ? null : natEcoSec.trim();
	}

	/**
	 * @return NatEcoSec
	 */
	public String getNatEcoSec() {
		return this.natEcoSec;
	}

	/**
	 * @param holdType
	 */
	public void setHoldType(String holdType) {
		this.holdType = holdType == null ? null : holdType.trim();
	}

	/**
	 * @return HoldType
	 */
	public String getHoldType() {
		return this.holdType;
	}

	/**
	 * @param cusScale
	 */
	public void setCusScale(String cusScale) {
		this.cusScale = cusScale == null ? null : cusScale.trim();
	}

	/**
	 * @return CusScale
	 */
	public String getCusScale() {
		return this.cusScale;
	}

	/**
	 * @param fnaCllTyp
	 */
	public void setFnaCllTyp(String fnaCllTyp) {
		this.fnaCllTyp = fnaCllTyp == null ? null : fnaCllTyp.trim();
	}

	/**
	 * @return FnaCllTyp
	 */
	public String getFnaCllTyp() {
		return this.fnaCllTyp;
	}

	/**
	 * @param loanCardId
	 */
	public void setLoanCardId(String loanCardId) {
		this.loanCardId = loanCardId == null ? null : loanCardId.trim();
	}

	/**
	 * @return LoanCardId
	 */
	public String getLoanCardId() {
		return this.loanCardId;
	}

	/**
	 * @param headInsCde
	 */
	public void setHeadInsCde(String headInsCde) {
		this.headInsCde = headInsCde == null ? null : headInsCde.trim();
	}

	/**
	 * @return HeadInsCde
	 */
	public String getHeadInsCde() {
		return this.headInsCde;
	}

	/**
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country == null ? null : country.trim();
	}

	/**
	 * @return Country
	 */
	public String getCountry() {
		return this.country;
	}

	/**
	 * @param legalName
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName == null ? null : legalName.trim();
	}

	/**
	 * @return LegalName
	 */
	public String getLegalName() {
		return this.legalName;
	}

	/**
	 * @param legalCertType
	 */
	public void setLegalCertType(String legalCertType) {
		this.legalCertType = legalCertType == null ? null : legalCertType.trim();
	}

	/**
	 * @return LegalCertType
	 */
	public String getLegalCertType() {
		return this.legalCertType;
	}

	/**
	 * @param legalCertCode
	 */
	public void setLegalCertCode(String legalCertCode) {
		this.legalCertCode = legalCertCode == null ? null : legalCertCode.trim();
	}

	/**
	 * @return LegalCertCode
	 */
	public String getLegalCertCode() {
		return this.legalCertCode;
	}

	/**
	 * @param legalSex
	 */
	public void setLegalSex(String legalSex) {
		this.legalSex = legalSex == null ? null : legalSex.trim();
	}

	/**
	 * @return LegalSex
	 */
	public String getLegalSex() {
		return this.legalSex;
	}

	/**
	 * @param legalBirth
	 */
	public void setLegalBirth(String legalBirth) {
		this.legalBirth = legalBirth == null ? null : legalBirth.trim();
	}

	/**
	 * @return LegalBirth
	 */
	public String getLegalBirth() {
		return this.legalBirth;
	}

	/**
	 * @param natTaxRegCde
	 */
	public void setNatTaxRegCde(String natTaxRegCde) {
		this.natTaxRegCde = natTaxRegCde == null ? null : natTaxRegCde.trim();
	}

	/**
	 * @return NatTaxRegCde
	 */
	public String getNatTaxRegCde() {
		return this.natTaxRegCde;
	}

	/**
	 * @param locTaxRegCde
	 */
	public void setLocTaxRegCde(String locTaxRegCde) {
		this.locTaxRegCde = locTaxRegCde == null ? null : locTaxRegCde.trim();
	}

	/**
	 * @return LocTaxRegCde
	 */
	public String getLocTaxRegCde() {
		return this.locTaxRegCde;
	}

	/**
	 * @param regCde
	 */
	public void setRegCde(String regCde) {
		this.regCde = regCde == null ? null : regCde.trim();
	}

	/**
	 * @return RegCde
	 */
	public String getRegCde() {
		return this.regCde;
	}

	/**
	 * @param ressetAmt
	 */
	public void setRessetAmt(java.math.BigDecimal ressetAmt) {
		this.ressetAmt = ressetAmt;
	}

	/**
	 * @return RessetAmt
	 */
	public java.math.BigDecimal getRessetAmt() {
		return this.ressetAmt;
	}

	/**
	 * @param regCapCurType
	 */
	public void setRegCapCurType(String regCapCurType) {
		this.regCapCurType = regCapCurType == null ? null : regCapCurType.trim();
	}

	/**
	 * @return RegCapCurType
	 */
	public String getRegCapCurType() {
		return this.regCapCurType;
	}

	/**
	 * @param regCapAmt
	 */
	public void setRegCapAmt(java.math.BigDecimal regCapAmt) {
		this.regCapAmt = regCapAmt;
	}

	/**
	 * @return RegCapAmt
	 */
	public java.math.BigDecimal getRegCapAmt() {
		return this.regCapAmt;
	}

	/**
	 * @param paidCapAmt
	 */
	public void setPaidCapAmt(java.math.BigDecimal paidCapAmt) {
		this.paidCapAmt = paidCapAmt;
	}

	/**
	 * @return PaidCapAmt
	 */
	public java.math.BigDecimal getPaidCapAmt() {
		return this.paidCapAmt;
	}

	/**
	 * @param owersEquity
	 */
	public void setOwersEquity(java.math.BigDecimal owersEquity) {
		this.owersEquity = owersEquity;
	}

	/**
	 * @return OwersEquity
	 */
	public java.math.BigDecimal getOwersEquity() {
		return this.owersEquity;
	}

	/**
	 * @param basAccBank
	 */
	public void setBasAccBank(String basAccBank) {
		this.basAccBank = basAccBank == null ? null : basAccBank.trim();
	}

	/**
	 * @return BasAccBank
	 */
	public String getBasAccBank() {
		return this.basAccBank;
	}

	/**
	 * @param basAccNo
	 */
	public void setBasAccNo(String basAccNo) {
		this.basAccNo = basAccNo == null ? null : basAccNo.trim();
	}

	/**
	 * @return BasAccNo
	 */
	public String getBasAccNo() {
		return this.basAccNo;
	}

	/**
	 * @param mrkFlg
	 */
	public void setMrkFlg(String mrkFlg) {
		this.mrkFlg = mrkFlg == null ? null : mrkFlg.trim();
	}

	/**
	 * @return MrkFlg
	 */
	public String getMrkFlg() {
		return this.mrkFlg;
	}

	/**
	 * @param mrkAreaOne
	 */
	public void setMrkAreaOne(String mrkAreaOne) {
		this.mrkAreaOne = mrkAreaOne == null ? null : mrkAreaOne.trim();
	}

	/**
	 * @return MrkAreaOne
	 */
	public String getMrkAreaOne() {
		return this.mrkAreaOne;
	}

	/**
	 * @param mrkAreaTwo
	 */
	public void setMrkAreaTwo(String mrkAreaTwo) {
		this.mrkAreaTwo = mrkAreaTwo == null ? null : mrkAreaTwo.trim();
	}

	/**
	 * @return MrkAreaTwo
	 */
	public String getMrkAreaTwo() {
		return this.mrkAreaTwo;
	}

	/**
	 * @param stkCode
	 */
	public void setStkCode(String stkCode) {
		this.stkCode = stkCode == null ? null : stkCode.trim();
	}

	/**
	 * @return StkCode
	 */
	public String getStkCode() {
		return this.stkCode;
	}

	/**
	 * @param intbankBuildDate
	 */
	public void setIntbankBuildDate(String intbankBuildDate) {
		this.intbankBuildDate = intbankBuildDate == null ? null : intbankBuildDate.trim();
	}

	/**
	 * @return IntbankBuildDate
	 */
	public String getIntbankBuildDate() {
		return this.intbankBuildDate;
	}

	/**
	 * @param bankProLic
	 */
	public void setBankProLic(String bankProLic) {
		this.bankProLic = bankProLic == null ? null : bankProLic.trim();
	}

	/**
	 * @return BankProLic
	 */
	public String getBankProLic() {
		return this.bankProLic;
	}

	/**
	 * @param intbankSite
	 */
	public void setIntbankSite(String intbankSite) {
		this.intbankSite = intbankSite == null ? null : intbankSite.trim();
	}

	/**
	 * @return IntbankSite
	 */
	public String getIntbankSite() {
		return this.intbankSite;
	}

	/**
	 * @param contArea
	 */
	public void setContArea(String contArea) {
		this.contArea = contArea == null ? null : contArea.trim();
	}

	/**
	 * @return ContArea
	 */
	public String getContArea() {
		return this.contArea;
	}

	/**
	 * @param contCountry
	 */
	public void setContCountry(String contCountry) {
		this.contCountry = contCountry == null ? null : contCountry.trim();
	}

	/**
	 * @return ContCountry
	 */
	public String getContCountry() {
		return this.contCountry;
	}

	/**
	 * @param contTown
	 */
	public void setContTown(String contTown) {
		this.contTown = contTown == null ? null : contTown.trim();
	}

	/**
	 * @return ContTown
	 */
	public String getContTown() {
		return this.contTown;
	}

	/**
	 * @param contAddr
	 */
	public void setContAddr(String contAddr) {
		this.contAddr = contAddr == null ? null : contAddr.trim();
	}

	/**
	 * @return ContAddr
	 */
	public String getContAddr() {
		return this.contAddr;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * @return Email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @param supeEval
	 */
	public void setSupeEval(String supeEval) {
		this.supeEval = supeEval == null ? null : supeEval.trim();
	}

	/**
	 * @return SupeEval
	 */
	public String getSupeEval() {
		return this.supeEval;
	}

	/**
	 * @param intbankEval
	 */
	public void setIntbankEval(String intbankEval) {
		this.intbankEval = intbankEval == null ? null : intbankEval.trim();
	}

	/**
	 * @return IntbankEval
	 */
	public String getIntbankEval() {
		return this.intbankEval;
	}

	/**
	 * @param evalEndDt
	 */
	public void setEvalEndDt(String evalEndDt) {
		this.evalEndDt = evalEndDt == null ? null : evalEndDt.trim();
	}

	/**
	 * @return EvalEndDt
	 */
	public String getEvalEndDt() {
		return this.evalEndDt;
	}

	/**
	 * @param owmAptName
	 */
	public void setOwmAptName(String owmAptName) {
		this.owmAptName = owmAptName == null ? null : owmAptName.trim();
	}

	/**
	 * @return OwmAptName
	 */
	public String getOwmAptName() {
		return this.owmAptName;
	}

	/**
	 * @param relDrg
	 */
	public void setRelDrg(String relDrg) {
		this.relDrg = relDrg == null ? null : relDrg.trim();
	}

	/**
	 * @return RelDrg
	 */
	public String getRelDrg() {
		return this.relDrg;
	}

	/**
	 * @param cusState
	 */
	public void setCusState(String cusState) {
		this.cusState = cusState == null ? null : cusState.trim();
	}

	/**
	 * @return CusState
	 */
	public String getCusState() {
		return this.cusState;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}

	/**
	 * @return ManagerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

	/**
	 * @return UpdId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

	/**
	 * @return UpdBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

	/**
	 * @return UpdDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param socialCreditCode
	 */
	public void setSocialCreditCode(String socialCreditCode) {
		this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
	}

	/**
	 * @return SocialCreditCode
	 */
	public String getSocialCreditCode() {
		return this.socialCreditCode;
	}

	/**
	 * @param orgForShort
	 */
	public void setOrgForShort(String orgForShort) {
		this.orgForShort = orgForShort == null ? null : orgForShort.trim();
	}

	/**
	 * @return OrgForShort
	 */
	public String getOrgForShort() {
		return this.orgForShort;
	}

	/**
	 * @param outApprOrg
	 */
	public void setOutApprOrg(String outApprOrg) {
		this.outApprOrg = outApprOrg == null ? null : outApprOrg.trim();
	}

	/**
	 * @return OutApprOrg
	 */
	public String getOutApprOrg() {
		return this.outApprOrg;
	}

	/**
	 * @param intbankOsssOrgCode
	 */
	public void setIntbankOsssOrgCode(String intbankOsssOrgCode) {
		this.intbankOsssOrgCode = intbankOsssOrgCode == null ? null : intbankOsssOrgCode.trim();
	}

	/**
	 * @return IntbankOsssOrgCode
	 */
	public String getIntbankOsssOrgCode() {
		return this.intbankOsssOrgCode;
	}

	/**
	 * @param isLt
	 */
	public void setIsLt(String isLt) {
		this.isLt = isLt == null ? null : isLt.trim();
	}

	/**
	 * @return IsLt
	 */
	public String getIsLt() {
		return this.isLt;
	}

	/**
	 * @param operArea
	 */
	public void setOperArea(String operArea) {
		this.operArea = operArea == null ? null : operArea.trim();
	}

	/**
	 * @return OperArea
	 */
	public String getOperArea() {
		return this.operArea;
	}

	/**
	 * @param orgTypeMemo
	 */
	public void setOrgTypeMemo(String orgTypeMemo) {
		this.orgTypeMemo = orgTypeMemo == null ? null : orgTypeMemo.trim();
	}

	/**
	 * @return OrgTypeMemo
	 */
	public String getOrgTypeMemo() {
		return this.orgTypeMemo;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

	/**
	 * @return ManagerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param expDateEnd
	 */
	public void setExpDateEnd(String expDateEnd) {
		this.expDateEnd = expDateEnd == null ? null : expDateEnd.trim();
	}

	/**
	 * @return ExpDateEnd
	 */
	public String getExpDateEnd() {
		return this.expDateEnd;
	}

	/**
	 * @param regiAreaCode
	 */
	public void setRegiAreaCode(String regiAreaCode) {
		this.regiAreaCode = regiAreaCode == null ? null : regiAreaCode.trim();
	}

	/**
	 * @return RegiAreaCode
	 */
	public String getRegiAreaCode() {
		return this.regiAreaCode;
	}

	/**
	 * @param comRegAdd
	 */
	public void setComRegAdd(String comRegAdd) {
		this.comRegAdd = comRegAdd == null ? null : comRegAdd.trim();
	}

	/**
	 * @return ComRegAdd
	 */
	public String getComRegAdd() {
		return this.comRegAdd;
	}

	/**
	 * @param natBusi
	 */
	public void setNatBusi(String natBusi) {
		this.natBusi = natBusi == null ? null : natBusi.trim();
	}

	/**
	 * @return NatBusi
	 */
	public String getNatBusi() {
		return this.natBusi;
	}


}
