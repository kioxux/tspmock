/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivContact
 * @类描述: cus_indiv_contact数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-09 17:17:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_indiv_contact")
public class CusIndivContact extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CUS_ID")
	@NotBlank(message = "客户号不能为空")
	private String cusId;
	
	/** 身份证地址 **/
	@Column(name = "CERT_ADDR", unique = false, nullable = true, length = 80)
	private String certAddr;
	
	/** 身份证地址街道/路 **/
	@Column(name = "CERT_STREET", unique = false, nullable = true, length = 80)
	private String certStreet;
	
	/** 居住地地址 **/
	@Column(name = "INDIV_RSD_ADDR", unique = false, nullable = true, length = 80)
	private String indivRsdAddr;
	
	/** 居住地址街道/路 **/
	@Column(name = "STREET_RSD", unique = false, nullable = true, length = 80)
	private String streetRsd;
	
	/** 居住地邮政编码 **/
	@Column(name = "INDIV_ZIP_CODE", unique = false, nullable = true, length = 200)
	private String indivZipCode;
	
	/** 居住状况 STD_ZB_RSD_ST **/
	@Column(name = "INDIV_RSD_ST", unique = false, nullable = true, length = 5)
	private String indivRsdSt;
	
	/** 住宅电话 **/
	@Column(name = "MOBILE", unique = false, nullable = true, length = 35)
	private String mobile;
	
	/** 居住区域编号 **/
	@Column(name = "REGIONALISM", unique = false, nullable = true, length = 10)
	private String regionalism;
	
	/** 居住区域名称 **/
	@Column(name = "REGION_NAME", unique = false, nullable = true, length = 30)
	private String regionName;
	
	/** 手机号码1 **/
	@Column(name = "MOBILE_A", unique = false, nullable = true, length = 35)
	private String mobileA;
	
	/** 号码是否验证1 STD_ZB_YES_NO **/
	@Column(name = "IS_CHECK_A", unique = false, nullable = true, length = 5)
	private String isCheckA;
	
	/** 号码是否本人1 STD_ZB_YES_NO **/
	@Column(name = "IS_ONESELF_A", unique = false, nullable = true, length = 5)
	private String isOneselfA;
	
	/** 手机号码2 **/
	@Column(name = "MOBILE_B", unique = false, nullable = true, length = 35)
	private String mobileB;
	
	/** 号码是否验证2 STD_ZB_YES_NO **/
	@Column(name = "IS_CHECK_B", unique = false, nullable = true, length = 5)
	private String isCheckB;
	
	/** 号码是否本人2 STD_ZB_YES_NO **/
	@Column(name = "IS_ONESELF_B", unique = false, nullable = true, length = 5)
	private String isOneselfB;
	
	/** 传真 **/
	@Column(name = "FAX_CODE", unique = false, nullable = true, length = 35)
	private String faxCode;
	
	/** Email地址 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 80)
	private String email;
	
	/** 手机号码1来源 **/
	@Column(name = "MOBILE1_SOUR", unique = false, nullable = true, length = 5)
	private String mobile1Sour;
	
	/** 手机号码2来源 **/
	@Column(name = "MOBILE2_SOUR", unique = false, nullable = true, length = 5)
	private String mobile2Sour;
	
	/** 微信号 **/
	@Column(name = "WECHAT_NO", unique = false, nullable = true, length = 40)
	private String wechatNo;
	
	/** 手机号码
 **/
	@Column(name = "MOBILE_NO", unique = false, nullable = true, length = 35)
	private String mobileNo;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** QQ号 **/
	@Column(name = "QQ", unique = false, nullable = true, length = 11)
	private String qq;
	
	/** 户籍地址 **/
	@Column(name = "INDIV_HOUH_REG_ADD", unique = false, nullable = true, length = 80)
	private String indivHouhRegAdd;
	
	/** 户籍地址街道/路 **/
	@Column(name = "REGION_STREET", unique = false, nullable = true, length = 80)
	private String regionStreet;
	
	/** 送达地址 **/
	@Column(name = "SEND_ADDR", unique = false, nullable = true, length = 80)
	private String sendAddr;
	
	/** 送达地址街道/路 **/
	@Column(name = "DELIVERY_STREET", unique = false, nullable = true, length = 80)
	private String deliveryStreet;
	
	/** 邮政编码 **/
	@Column(name = "POSTCODE", unique = false, nullable = true, length = 8)
	private String postcode;
	
	/** 是否农户 **/
	@Column(name = "IS_AGRI", unique = false, nullable = true, length = 10)
	private String isAgri;
	
	/** 居住时间（年） **/
	@Column(name = "RESI_TIME", unique = false, nullable = true, length = 10)
	private String resiTime;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 10)
	private String updDate;
	
	/** 创建日期 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 更新日期 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param certAddr
	 */
	public void setCertAddr(String certAddr) {
		this.certAddr = certAddr;
	}
	
    /**
     * @return certAddr
     */
	public String getCertAddr() {
		return this.certAddr;
	}
	
	/**
	 * @param certStreet
	 */
	public void setCertStreet(String certStreet) {
		this.certStreet = certStreet;
	}
	
    /**
     * @return certStreet
     */
	public String getCertStreet() {
		return this.certStreet;
	}
	
	/**
	 * @param indivRsdAddr
	 */
	public void setIndivRsdAddr(String indivRsdAddr) {
		this.indivRsdAddr = indivRsdAddr;
	}
	
    /**
     * @return indivRsdAddr
     */
	public String getIndivRsdAddr() {
		return this.indivRsdAddr;
	}
	
	/**
	 * @param streetRsd
	 */
	public void setStreetRsd(String streetRsd) {
		this.streetRsd = streetRsd;
	}
	
    /**
     * @return streetRsd
     */
	public String getStreetRsd() {
		return this.streetRsd;
	}
	
	/**
	 * @param indivZipCode
	 */
	public void setIndivZipCode(String indivZipCode) {
		this.indivZipCode = indivZipCode;
	}
	
    /**
     * @return indivZipCode
     */
	public String getIndivZipCode() {
		return this.indivZipCode;
	}
	
	/**
	 * @param indivRsdSt
	 */
	public void setIndivRsdSt(String indivRsdSt) {
		this.indivRsdSt = indivRsdSt;
	}
	
    /**
     * @return indivRsdSt
     */
	public String getIndivRsdSt() {
		return this.indivRsdSt;
	}
	
	/**
	 * @param mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
    /**
     * @return mobile
     */
	public String getMobile() {
		return this.mobile;
	}
	
	/**
	 * @param regionalism
	 */
	public void setRegionalism(String regionalism) {
		this.regionalism = regionalism;
	}
	
    /**
     * @return regionalism
     */
	public String getRegionalism() {
		return this.regionalism;
	}
	
	/**
	 * @param regionName
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	
    /**
     * @return regionName
     */
	public String getRegionName() {
		return this.regionName;
	}
	
	/**
	 * @param mobileA
	 */
	public void setMobileA(String mobileA) {
		this.mobileA = mobileA;
	}
	
    /**
     * @return mobileA
     */
	public String getMobileA() {
		return this.mobileA;
	}
	
	/**
	 * @param isCheckA
	 */
	public void setIsCheckA(String isCheckA) {
		this.isCheckA = isCheckA;
	}
	
    /**
     * @return isCheckA
     */
	public String getIsCheckA() {
		return this.isCheckA;
	}
	
	/**
	 * @param isOneselfA
	 */
	public void setIsOneselfA(String isOneselfA) {
		this.isOneselfA = isOneselfA;
	}
	
    /**
     * @return isOneselfA
     */
	public String getIsOneselfA() {
		return this.isOneselfA;
	}
	
	/**
	 * @param mobileB
	 */
	public void setMobileB(String mobileB) {
		this.mobileB = mobileB;
	}
	
    /**
     * @return mobileB
     */
	public String getMobileB() {
		return this.mobileB;
	}
	
	/**
	 * @param isCheckB
	 */
	public void setIsCheckB(String isCheckB) {
		this.isCheckB = isCheckB;
	}
	
    /**
     * @return isCheckB
     */
	public String getIsCheckB() {
		return this.isCheckB;
	}
	
	/**
	 * @param isOneselfB
	 */
	public void setIsOneselfB(String isOneselfB) {
		this.isOneselfB = isOneselfB;
	}
	
    /**
     * @return isOneselfB
     */
	public String getIsOneselfB() {
		return this.isOneselfB;
	}
	
	/**
	 * @param faxCode
	 */
	public void setFaxCode(String faxCode) {
		this.faxCode = faxCode;
	}
	
    /**
     * @return faxCode
     */
	public String getFaxCode() {
		return this.faxCode;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param mobile1Sour
	 */
	public void setMobile1Sour(String mobile1Sour) {
		this.mobile1Sour = mobile1Sour;
	}
	
    /**
     * @return mobile1Sour
     */
	public String getMobile1Sour() {
		return this.mobile1Sour;
	}
	
	/**
	 * @param mobile2Sour
	 */
	public void setMobile2Sour(String mobile2Sour) {
		this.mobile2Sour = mobile2Sour;
	}
	
    /**
     * @return mobile2Sour
     */
	public String getMobile2Sour() {
		return this.mobile2Sour;
	}
	
	/**
	 * @param wechatNo
	 */
	public void setWechatNo(String wechatNo) {
		this.wechatNo = wechatNo;
	}
	
    /**
     * @return wechatNo
     */
	public String getWechatNo() {
		return this.wechatNo;
	}
	
	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
    /**
     * @return mobileNo
     */
	public String getMobileNo() {
		return this.mobileNo;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}
	
    /**
     * @return qq
     */
	public String getQq() {
		return this.qq;
	}
	
	/**
	 * @param indivHouhRegAdd
	 */
	public void setIndivHouhRegAdd(String indivHouhRegAdd) {
		this.indivHouhRegAdd = indivHouhRegAdd;
	}
	
    /**
     * @return indivHouhRegAdd
     */
	public String getIndivHouhRegAdd() {
		return this.indivHouhRegAdd;
	}
	
	/**
	 * @param regionStreet
	 */
	public void setRegionStreet(String regionStreet) {
		this.regionStreet = regionStreet;
	}
	
    /**
     * @return regionStreet
     */
	public String getRegionStreet() {
		return this.regionStreet;
	}
	
	/**
	 * @param sendAddr
	 */
	public void setSendAddr(String sendAddr) {
		this.sendAddr = sendAddr;
	}
	
    /**
     * @return sendAddr
     */
	public String getSendAddr() {
		return this.sendAddr;
	}
	
	/**
	 * @param deliveryStreet
	 */
	public void setDeliveryStreet(String deliveryStreet) {
		this.deliveryStreet = deliveryStreet;
	}
	
    /**
     * @return deliveryStreet
     */
	public String getDeliveryStreet() {
		return this.deliveryStreet;
	}
	
	/**
	 * @param postcode
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
    /**
     * @return postcode
     */
	public String getPostcode() {
		return this.postcode;
	}
	
	/**
	 * @param isAgri
	 */
	public void setIsAgri(String isAgri) {
		this.isAgri = isAgri;
	}
	
    /**
     * @return isAgri
     */
	public String getIsAgri() {
		return this.isAgri;
	}
	
	/**
	 * @param resiTime
	 */
	public void setResiTime(String resiTime) {
		this.resiTime = resiTime;
	}
	
    /**
     * @return resiTime
     */
	public String getResiTime() {
		return this.resiTime;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}