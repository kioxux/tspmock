/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.CusLstYndApp;
import cn.com.yusys.yusp.domain.CusLstYndJyxxApp;
import cn.com.yusys.yusp.domain.CusLstYndLinkman;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYndCmdcApp;
import cn.com.yusys.yusp.service.CusLstYndCmdcAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYndCmdcAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-26 00:19:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstyndcmdcapp")
public class CusLstYndCmdcAppResource {
    @Autowired
    private CusLstYndCmdcAppService cusLstYndCmdcAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstYndCmdcApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstYndCmdcApp> list = cusLstYndCmdcAppService.selectAll(queryModel);
        return new ResultDto<List<CusLstYndCmdcApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstYndCmdcApp>> index(QueryModel queryModel) {
        List<CusLstYndCmdcApp> list = cusLstYndCmdcAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYndCmdcApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusLstYndCmdcApp> show(@PathVariable("pkId") String pkId) {
        CusLstYndCmdcApp cusLstYndCmdcApp = cusLstYndCmdcAppService.selectByPrimaryKey(pkId);
        return new ResultDto<CusLstYndCmdcApp>(cusLstYndCmdcApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstYndCmdcApp> create(@RequestBody CusLstYndCmdcApp cusLstYndCmdcApp) throws URISyntaxException {
        cusLstYndCmdcAppService.insert(cusLstYndCmdcApp);
        return new ResultDto<CusLstYndCmdcApp>(cusLstYndCmdcApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstYndCmdcApp cusLstYndCmdcApp) throws URISyntaxException {
        int result = cusLstYndCmdcAppService.update(cusLstYndCmdcApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusLstYndCmdcAppService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstYndCmdcAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param cusLstYndCmdcApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/7/15 15:16
     * @version 1.0.0
     * @desc    新增侧面调查信息-----移动OA
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/addcuslstyndcmdcapp")
    protected ResultDto<Integer> addCusLstYndCmdcApp(@RequestBody CusLstYndCmdcApp cusLstYndCmdcApp) throws URISyntaxException {
        return new ResultDto<Integer>(cusLstYndCmdcAppService.addCusLstYndCmdcApp(cusLstYndCmdcApp));
    }

    /**
     * @param cusLstYndCmdcApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndCmdcApp>
     * @author hubp
     * @date 2021/7/16 10:32
     * @version 1.0.0
     * @desc    通过流水号查询侧面调查信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbyserno")
    public ResultDto<List<CusLstYndCmdcApp>> selectBySerno(@RequestBody CusLstYndCmdcApp cusLstYndCmdcApp) {
        List<CusLstYndCmdcApp> list = cusLstYndCmdcAppService.selectBySerno(cusLstYndCmdcApp.getSerno());;
        return new ResultDto<List<CusLstYndCmdcApp>>(list);
    }

    /**
     * @param cusLstYndCmdcApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.domain.CusLstYndJyxxApp>
     * @author hubp
     * @date 2021/7/16 16:09
     * @version 1.0.0
     * @desc    通过主键查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectbypkid")
    protected ResultDto<CusLstYndCmdcApp> selectByPkId(@RequestBody CusLstYndCmdcApp cusLstYndCmdcApp) {
        CusLstYndCmdcApp result = cusLstYndCmdcAppService.selectByPrimaryKey(cusLstYndCmdcApp.getPkId());
        return new ResultDto<CusLstYndCmdcApp>(result);
    }

    /**
     * @param cusLstYndCmdcApp
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/7/16 16:35
     * @version 1.0.0
     * @desc    通过主键删除
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deletecuslstyndcmdcapp")
    protected ResultDto<Integer> deleteCusLstYndCmdcApp(@RequestBody CusLstYndCmdcApp cusLstYndCmdcApp) {
        int result = cusLstYndCmdcAppService.deleteByPrimaryKey(cusLstYndCmdcApp.getPkId());
        return new ResultDto<Integer>(result);
    }
}
