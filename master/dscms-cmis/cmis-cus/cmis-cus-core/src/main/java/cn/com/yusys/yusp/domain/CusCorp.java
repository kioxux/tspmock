/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorp
 * @类描述: cus_corp数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-25 23:58:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_corp")
public class CusCorp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	/** 新增客户经理名称 管护机构**/
	@Column(name = "MANAGER_BR_ID")
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="managerBrIdName" )
	private String managerBrId;

	@Column(name = "MANAGER_ID")
	@RedisCacheTranslator(redisCacheKey = "userName" ,refFieldName="managerIdName")
	private String managerId;

	/** 客户编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "CUS_ID")
	private String cusId;
	
	/** 外文名称 **/
	@Column(name = "CUS_NAME_EN", unique = false, nullable = true, length = 80)
	private String cusNameEn;
	
	/** 国别 **/
	@Column(name = "COUNTRY", unique = false, nullable = true, length = 5)
	private String country;
	
	/** 企业性质 **/
	@Column(name = "CORP_QLTY", unique = false, nullable = true, length = 20)
	private String corpQlty;
	
	/** 城乡类型 STD_ZB_CITY_TYP **/
	@Column(name = "CITY_TYPE", unique = false, nullable = true, length = 5)
	private String cityType;
	
	/** 资产总额（万元） **/
	@Column(name = "ASS_TOTAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assTotal;
	
	/** 行政隶属关系 STD_ZB_SUB_TYP **/
	@Column(name = "ADMIN_SUB_REL", unique = false, nullable = true, length = 5)
	private String adminSubRel;
	
	/** 隶属关系 STD_ZB_SUB_TYP **/
	@Column(name = "SUB_TYP", unique = false, nullable = true, length = 5)
	private String subTyp;
	
	/** 投资主体 STD_ZB_INVEST_TYP **/
	@Column(name = "INVEST_MBODY", unique = false, nullable = true, length = 5)
	private String investMbody;
	
	/** 控股类型 STD_ZB_HOLD_TYPE **/
	@Column(name = "HOLD_TYPE", unique = false, nullable = true, length = 5)
	private String holdType;
	
	/** 行业分类 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = true, length = 5)
	private String tradeClass;
	
	/** 评级模型ID **/
	@Column(name = "CUS_CCR_MODEL_ID", unique = false, nullable = true, length = 5)
	private String cusCcrModelId;
	
	/** 评级模型名称 **/
	@Column(name = "CUS_CCR_MODEL_NAME", unique = false, nullable = true, length = 200)
	private String cusCcrModelName;
	
	/** 行业分类2 **/
	@Column(name = "CLL_TYPE2", unique = false, nullable = true, length = 5)
	private String cllType2;
	
	/** 成立日期 **/
	@Column(name = "BUILD_DATE", unique = false, nullable = true, length = 10)
	private String buildDate;
	
	/** 从业人数 **/
	@Column(name = "FJOB_NUM", unique = false, nullable = true, length = 10)
	private Integer fjobNum;
	
	/** 销售额（万元） **/
	@Column(name = "SAL_VOLUME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal salVolume;
	
	/** 营业收入（万元） **/
	@Column(name = "OPER_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal operIncome;
	
	/** 企业规模 STD_ZB_CUS_SCALE **/
	@Column(name = "CORP_SCALE", unique = false, nullable = true, length = 5)
	private String corpScale;
	
	/** 企业规模（报表） STD_ZB_CUS_SCALE **/
	@Column(name = "CUS_SCALE_FORM", unique = false, nullable = true, length = 5)
	private String cusScaleForm;
	
	/** 国民经济部门编号 **/
	@Column(name = "NAT_ECO_SEC", unique = false, nullable = true, length = 5)
	private String natEcoSec;
	
	/** 产业扶贫单位标志 STD_ZB_YES_NO **/
	@Column(name = "UNIT_ALLE_IND", unique = false, nullable = true, length = 5)
	private String unitAlleInd;
	
	/** 扶贫带动人数 **/
	@Column(name = "ALLE_BUNB", unique = false, nullable = true, length = 10)
	private Integer alleBunb;
	
	/** 农场标志 **/
	@Column(name = "FRAM_IND", unique = false, nullable = true, length = 5)
	private String framInd;
	
	/** 农民专业合作社标志 STD_ZB_YES_NO **/
	@Column(name = "FARMER_COP_IND", unique = false, nullable = true, length = 5)
	private String farmerCopInd;
	
	/** 是否涉农 STD_ZB_YES_NO **/
	@Column(name = "AGR_IND", unique = false, nullable = true, length = 5)
	private String agrInd;
	
	/** 普惠金融统计口径 STD_ZB_YES_NO **/
	@Column(name = "INCLUSIVE_FINANCE_STATISTICS", unique = false, nullable = true, length = 5)
	private String inclusiveFinanceStatistics;
	
	/** 组织机构代码 **/
	@Column(name = "INS_CODE", unique = false, nullable = true, length = 10)
	private String insCode;
	
	/** 组织机构登记日期 **/
	@Column(name = "INS_REG_DATE", unique = false, nullable = true, length = 10)
	private String insRegDate;
	
	/** 组织机构有效日期 **/
	@Column(name = "INS_END_DATE", unique = false, nullable = true, length = 10)
	private String insEndDate;
	
	/** 组织机构代码证颁发机关 **/
	@Column(name = "INS_ORG", unique = false, nullable = true, length = 60)
	private String insOrg;
	
	/** 组织机构代码证年检到期日 **/
	@Column(name = "INS_ANN_DATE", unique = false, nullable = true, length = 10)
	private String insAnnDate;
	
	/** 注册登记号类型 STD_ZB_LICENSE_TYPE **/
	@Column(name = "LICENSE_TYPE", unique = false, nullable = true, length = 5)
	private String licenseType;
	
	/** 登记注册号 **/
	@Column(name = "REGI_CODE", unique = false, nullable = true, length = 100)
	private String regiCode;
	
	/** 注册登记类型 STD_ZB_REG_TYPE **/
	@Column(name = "REGI_TYPE", unique = false, nullable = true, length = 3)
	private String regiType;
	
	/** 资质等级 STD_ZB_QUAL_LEVEL **/
	@Column(name = "QULI_GARADE", unique = false, nullable = true, length = 5)
	private String quliGarade;
	
	/** 主管单位 **/
	@Column(name = "ADMIN_ORG", unique = false, nullable = true, length = 80)
	private String adminOrg;
	
	/** 审批机关 **/
	@Column(name = "APPR_ORG", unique = false, nullable = true, length = 80)
	private String apprOrg;
	
	/** 批准文号 **/
	@Column(name = "APPR_DOC_NO", unique = false, nullable = true, length = 80)
	private String apprDocNo;
	
	/** 注册地行政区划 **/
	@Column(name = "REGI_AREA_CODE", unique = false, nullable = true, length = 12)
	private String regiAreaCode;
	
	/** 注册登记地址 **/
	@Column(name = "REGI_ADDR", unique = false, nullable = true, length = 80)
	private String regiAddr;
	
	/** 外文注册登记地址 **/
	@Column(name = "REGI_ADDR_EN", unique = false, nullable = true, length = 80)
	private String regiAddrEn;
	
	/** 实际经营地行政区划 **/
	@Column(name = "ACU_STATE_CODE", unique = false, nullable = true, length = 12)
	private String acuStateCode;
	
	/** 实际经营地址 **/
	@Column(name = "OPER_ADDR_ACT", unique = false, nullable = true, length = 80)
	private String operAddrAct;
	
	/** 主营业务范围 **/
	@Column(name = "MAIN_OPT_SCP", unique = false, nullable = true, length = 500)
	private String mainOptScp;
	
	/** 兼营业务范围 **/
	@Column(name = "PART_OPT_SCP", unique = false, nullable = true, length = 250)
	private String partOptScp;
	
	/** 注册资本/开办资金币种 **/
	@Column(name = "REGI_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String regiCurType;
	
	/** 注册资本金额 **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 实收资本币种 **/
	@Column(name = "PAID_CAP_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String paidCapCurType;
	
	/** 实收资本金额 **/
	@Column(name = "PAID_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal paidCapAmt;
	
	/** 注册登记日期 **/
	@Column(name = "REGI_START_DATE", unique = false, nullable = true, length = 10)
	private String regiStartDate;
	
	/** 注册登记到期日期 **/
	@Column(name = "REGI_END_DATE", unique = false, nullable = true, length = 10)
	private String regiEndDate;
	
	/** 注册登记年审结论 **/
	@Column(name = "REG_AUDIT", unique = false, nullable = true, length = 200)
	private String regAudit;
	
	/** 注册登记年审日期 **/
	@Column(name = "REG_AUDIT_DATE", unique = false, nullable = true, length = 10)
	private String regAuditDate;
	
	/** 注册登记年检到期日 **/
	@Column(name = "REG_AUDIT_END_DATE", unique = false, nullable = true, length = 10)
	private String regAuditEndDate;
	
	/** 国税税务登记代码 **/
	@Column(name = "NAT_TAX_REG_CODE", unique = false, nullable = true, length = 40)
	private String natTaxRegCode;
	
	/** 国税税务登记机关 **/
	@Column(name = "NAT_TAX_REG_ORG", unique = false, nullable = true, length = 80)
	private String natTaxRegOrg;
	
	/** 国税税务登记日期 **/
	@Column(name = "NAT_TAX_REG_DT", unique = false, nullable = true, length = 10)
	private String natTaxRegDt;
	
	/** 国税登记有效期 **/
	@Column(name = "NAT_TAX_REG_END_DT", unique = false, nullable = true, length = 10)
	private String natTaxRegEndDt;
	
	/** 国税登记证年检到期日 **/
	@Column(name = "NAT_TAX_ANN_DATE", unique = false, nullable = true, length = 10)
	private String natTaxAnnDate;
	
	/** 地税税务登记代码 **/
	@Column(name = "LOC_TAX_REG_CODE", unique = false, nullable = true, length = 40)
	private String locTaxRegCode;
	
	/** 地税税务登记机构 **/
	@Column(name = "LOC_TAX_REG_ORG", unique = false, nullable = true, length = 80)
	private String locTaxRegOrg;
	
	/** 地税税务登记日期 **/
	@Column(name = "LOC_TAX_REG_DT", unique = false, nullable = true, length = 10)
	private String locTaxRegDt;
	
	/** 地税登记有效期 **/
	@Column(name = "LOC_TAX_REG_END_DT", unique = false, nullable = true, length = 10)
	private String locTaxRegEndDt;
	
	/** 地税登记证年检到期日 **/
	@Column(name = "LOC_TAX_ANN_DATE", unique = false, nullable = true, length = 10)
	private String locTaxAnnDate;
	
	/** 有无贷款卡 **/
	@Column(name = "LOAN_CARD_FLG", unique = false, nullable = true, length = 5)
	private String loanCardFlg;
	
	/** 企业中征码 **/
	@Column(name = "LOAN_CARD_ID", unique = false, nullable = true, length = 30)
	private String loanCardId;
	
	/** 贷款卡密码 **/
	@Column(name = "LOAN_CARD_PWD", unique = false, nullable = true, length = 30)
	private String loanCardPwd;
	
	/** 贷款卡状态 **/
	@Column(name = "LOAN_CARD_EFF_FLG", unique = false, nullable = true, length = 5)
	private String loanCardEffFlg;
	
	/** 贷款卡年检日期 **/
	@Column(name = "LOAN_CARD_ANN_DATE", unique = false, nullable = true, length = 10)
	private String loanCardAnnDate;
	
	/** 证件有效期 **/
	@Column(name = "CERT_IDATE", unique = false, nullable = true, length = 10)
	private String certIdate;
	
	/** 企业类型  STD_ZB_CORP_TYPE **/
	@Column(name = "CON_TYPE", unique = false, nullable = true, length = 5)
	private String conType;
	
	/** 企业所有制  STD_ZB_CORP_OWNERS **/
	@Column(name = "CORP_OWNERS_TYPE", unique = false, nullable = true, length = 5)
	private String corpOwnersType;
	
	/** 是否本行股东 STD_ZB_YES_NO **/
	@Column(name = "IS_BANK_SHD", unique = false, nullable = true, length = 5)
	private String isBankShd;
	
	/** 是否小企业客户 STD_ZB_YES_NO **/
	@Column(name = "IS_SMCON_CUS", unique = false, nullable = true, length = 5)
	private String isSmconCus;
	
	/** 详细地址 **/
	@Column(name = "DETAIL_ADDR", unique = false, nullable = true, length = 300)
	private String detailAddr;
	
	/** 年生产设备 **/
	@Column(name = "PRODUCE_EQUIP_YEAR", unique = false, nullable = true, length = 300)
	private String produceEquipYear;
	
	/** QQ **/
	@Column(name = "QQ", unique = false, nullable = true, length = 15)
	private String qq;
	
	/** 年生产能力 **/
	@Column(name = "PRODUCE_ABI_YEAR", unique = false, nullable = true, length = 300)
	private String produceAbiYear;
	
	/** 是否国控 **/
	@Column(name = "IS_NATCTL", unique = false, nullable = true, length = 10)
	private String isNatctl;
	
	/** 常用联系人 **/
	@Column(name = "FREQ_LINKMAN", unique = false, nullable = true, length = 100)
	private String freqLinkman;
	
	/** 国控层级 **/
	@Column(name = "NATCTL_LEVEL", unique = false, nullable = true, length = 10)
	private String natctlLevel;
	
	/** 许可经营项目 **/
	@Column(name = "LIC_OPER_PRO", unique = false, nullable = true, length = 255)
	private String licOperPro;
	
	/** 常用联系人手机 **/
	@Column(name = "FREQ_LINKMAN_TEL", unique = false, nullable = true, length = 20)
	private String freqLinkmanTel;
	
	/** 一般经营项目 **/
	@Column(name = "COMMON_OPER_PRO", unique = false, nullable = true, length = 500)
	private String commonOperPro;
	
	/** 经营状况 **/
	@Column(name = "OPER_STATUS", unique = false, nullable = true, length = 5)
	private String operStatus;
	
	/** 是否长期有效 **/
	@Column(name = "IS_LONG_VLD", unique = false, nullable = true, length = 5)
	private String isLongVld;
	
	/** 基本存款账户开户许可证（核准号） **/
	@Column(name = "BASIC_DEP_ACC_NO_OPEN_LIC", unique = false, nullable = true, length = 20)
	private String basicDepAccNoOpenLic;
	
	/** 基本存款账户账号 **/
	@Column(name = "BASIC_DEP_ACC_NO", unique = false, nullable = true, length = 32)
	private String basicDepAccNo;
	
	/** 基本存款账户是否在本机构 **/
	@Column(name = "IS_BANK_BASIC_DEP_ACC_NO", unique = false, nullable = true, length = 5)
	private String isBankBasicDepAccNo;
	
	/** 基本存款账户开户行 **/
	@Column(name = "BASIC_DEP_ACCOB", unique = false, nullable = true, length = 80)
	private String basicDepAccob;
	
	/** 基本账户开户日期 **/
	@Column(name = "BASIC_ACC_NO_OPEN_DATE", unique = false, nullable = true, length = 20)
	private String basicAccNoOpenDate;
	
	/** 一般账户开户日期 **/
	@Column(name = "COMMON_ACC_NO_OPEN_DATE", unique = false, nullable = true, length = 10)
	private String commonAccNoOpenDate;
	
	/** 主要产品情况 **/
	@Column(name = "MAIN_PRD_DESC", unique = false, nullable = true, length = 250)
	private String mainPrdDesc;
	
	/** 信用等级（外部） **/
	@Column(name = "CREDIT_LEVEL_OUTER", unique = false, nullable = true, length = 5)
	private String creditLevelOuter;
	
	/** 评定日期（外部） **/
	@Column(name = "EVAL_DATE", unique = false, nullable = true, length = 10)
	private String evalDate;
	
	/** 传真 **/
	@Column(name = "FAX", unique = false, nullable = true, length = 35)
	private String fax;
	
	/** 评定机构（外部） **/
	@Column(name = "EVAL_ORG_ID", unique = false, nullable = true, length = 60)
	private String evalOrgId;
	
	/** 电子邮箱 **/
	@Column(name = "LINKMAN_EMAIL", unique = false, nullable = true, length = 80)
	private String linkmanEmail;
	
	/** 送达地址 **/
	@Column(name = "SEND_ADDR", unique = false, nullable = true, length = 60)
	private String sendAddr;
	
	/** 建立信贷关系时间 **/
	@Column(name = "INIT_LOAN_DATE", unique = false, nullable = true, length = 10)
	private String initLoanDate;
	
	/** 微信 **/
	@Column(name = "WECHAT_NO", unique = false, nullable = true, length = 40)
	private String wechatNo;
	
	/** 是否战略客户 **/
	@Column(name = "IS_STRGC_CUS", unique = false, nullable = true, length = 5)
	private String isStrgcCus;
	
	/** 财务报表类型 **/
	@Column(name = "FINA_REPORT_TYPE", unique = false, nullable = true, length = 6)
	private String finaReportType;
	
	/** 地区重点企业 **/
	@Column(name = "AREA_PRIOR_CORP", unique = false, nullable = true, length = 5)
	private String areaPriorCorp;
	
	/** 特种经营标识 **/
	@Column(name = "SP_OPER_FLAG", unique = false, nullable = true, length = 5)
	private String spOperFlag;
	
	/** 是否新建企业 **/
	@Column(name = "IS_NEW_BUILD_CORP", unique = false, nullable = true, length = 5)
	private String isNewBuildCorp;
	
	/** 集团客户类型 **/
	@Column(name = "GRP_CUS_TYPE", unique = false, nullable = true, length = 5)
	private String grpCusType;
	
	/** 注册登记机关 **/
	@Column(name = "REGI_ORG", unique = false, nullable = true, length = 80)
	private String regiOrg;
	
	/** 主营业务所在国家 **/
	@Column(name = "MAIN_BUS_NATION", unique = false, nullable = true, length = 80)
	private String mainBusNation;
	
	/** 信用证垫款账号 **/
	@Column(name = "CREDIT_PAD_ACC_NO", unique = false, nullable = true, length = 32)
	private String creditPadAccNo;
	
	/** 贷款类型 **/
	@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 10)
	private String loanType;
	
	/** 经营场地所有权 **/
	@Column(name = "OPER_PLACE_OWNSHP", unique = false, nullable = true, length = 5)
	private String operPlaceOwnshp;
	
	/** 经营场地面积 **/
	@Column(name = "OPER_PLACE_SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal operPlaceSqu;
	
	/** 主要生产设备 **/
	@Column(name = "MAIN_PRODUCE_EQUIP", unique = false, nullable = true, length = 250)
	private String mainProduceEquip;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 更新时间 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/**
	 * 操作类型
	 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/**
	 * 创建时间
	 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/**
	 * 修改时间
	 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/**
	 * 是否苏州综合平台企业
	 **/
	@Column(name = "IIS_SZJRFW_CROP", unique = false, nullable = true, length = 10)
	private String iisSzjrfwCrop;

	/**
	 * 是否钢贸企业
	 **/
	@Column(name = "IS_STEEL_CUS", unique = false, nullable = true, length = 10)
	private String isSteelCus;
	
	/** 是否上市公司 **/
	@Column(name = "IS_STOCK_CORP", unique = false, nullable = true, length = 10)
	private String isStockCorp;
	
	/** 是否城投 **/
	@Column(name = "IS_CTINVE", unique = false, nullable = true, length = 10)
	private String isCtinve;
	
	/** 城投层级 **/
	@Column(name = "CTINVE_LEVEL", unique = false, nullable = true, length = 10)
	private String ctinveLevel;
	
	/** 政府投资平台 **/
	@Column(name = "GOVER_INVEST_PLAT", unique = false, nullable = true, length = 10)
	private String goverInvestPlat;
	
	/** 进出口标识 **/
	@Column(name = "IMPEXP_FLAG", unique = false, nullable = true, length = 10)
	private String impexpFlag;
	
	/** 再贴现业务中申请单位行业分类 **/
	@Column(name = "REDCBIZ_UNIT_TRADE_CLASS", unique = false, nullable = true, length = 20)
	private String redcbizUnitTradeClass;
	
	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 10)
	private String cusType;

	/** 客户简称 **/
	@Column(name = "CUS_SHORT_NAME", unique = false, nullable = true, length = 80)
	private String cusShortName;

	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 3)
	private String certType;

	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 20)
	private String certCode;

	/** 锁定状态 **/
	@Column(name = "ISLOCKED", unique = false, nullable = true, length = 5)
	private String islocked;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;

	/** 本行即期用信等级 **/
	@Column(name = "BANK_LOAN_LEVEL", unique = false, nullable = true, length = 40)
	private String bankLoanLevel;

	/** 违约概率 **/
	@Column(name = "PD", unique = false, nullable = true, length = 10)
	private String pd;

	/**
	 * @param cusShortName
	 */
	public void setCusShortName(String cusShortName) { this.cusShortName = cusShortName; }

	/**
	 * @return cusShortName
	 */
	public String getCusShortName() { return cusShortName; }

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) { this.certCode = certCode; }

	/**
	 * @return certCode
	 */
	public String getCertCode() { return certCode; }

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusNameEn
	 */
	public void setCusNameEn(String cusNameEn) {
		this.cusNameEn = cusNameEn;
	}
	
    /**
     * @return cusNameEn
     */
	public String getCusNameEn() {
		return this.cusNameEn;
	}
	
	/**
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	
    /**
     * @return country
     */
	public String getCountry() {
		return this.country;
	}
	
	/**
	 * @param corpQlty
	 */
	public void setCorpQlty(String corpQlty) {
		this.corpQlty = corpQlty;
	}
	
    /**
     * @return corpQlty
     */
	public String getCorpQlty() {
		return this.corpQlty;
	}
	
	/**
	 * @param cityType
	 */
	public void setCityType(String cityType) {
		this.cityType = cityType;
	}
	
    /**
     * @return cityType
     */
	public String getCityType() {
		return this.cityType;
	}
	
	/**
	 * @param assTotal
	 */
	public void setAssTotal(java.math.BigDecimal assTotal) {
		this.assTotal = assTotal;
	}
	
    /**
     * @return assTotal
     */
	public java.math.BigDecimal getAssTotal() {
		return this.assTotal;
	}
	
	/**
	 * @param adminSubRel
	 */
	public void setAdminSubRel(String adminSubRel) {
		this.adminSubRel = adminSubRel;
	}
	
    /**
     * @return adminSubRel
     */
	public String getAdminSubRel() {
		return this.adminSubRel;
	}
	
	/**
	 * @param subTyp
	 */
	public void setSubTyp(String subTyp) {
		this.subTyp = subTyp;
	}
	
    /**
     * @return subTyp
     */
	public String getSubTyp() {
		return this.subTyp;
	}
	
	/**
	 * @param investMbody
	 */
	public void setInvestMbody(String investMbody) {
		this.investMbody = investMbody;
	}
	
    /**
     * @return investMbody
     */
	public String getInvestMbody() {
		return this.investMbody;
	}
	
	/**
	 * @param holdType
	 */
	public void setHoldType(String holdType) {
		this.holdType = holdType;
	}
	
    /**
     * @return holdType
     */
	public String getHoldType() {
		return this.holdType;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}
	
    /**
     * @return tradeClass
     */
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param cusCcrModelId
	 */
	public void setCusCcrModelId(String cusCcrModelId) {
		this.cusCcrModelId = cusCcrModelId;
	}
	
    /**
     * @return cusCcrModelId
     */
	public String getCusCcrModelId() {
		return this.cusCcrModelId;
	}
	
	/**
	 * @param cusCcrModelName
	 */
	public void setCusCcrModelName(String cusCcrModelName) {
		this.cusCcrModelName = cusCcrModelName;
	}
	
    /**
     * @return cusCcrModelName
     */
	public String getCusCcrModelName() {
		return this.cusCcrModelName;
	}
	
	/**
	 * @param cllType2
	 */
	public void setCllType2(String cllType2) {
		this.cllType2 = cllType2;
	}
	
    /**
     * @return cllType2
     */
	public String getCllType2() {
		return this.cllType2;
	}
	
	/**
	 * @param buildDate
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}
	
    /**
     * @return buildDate
     */
	public String getBuildDate() {
		return this.buildDate;
	}
	
	/**
	 * @param fjobNum
	 */
	public void setFjobNum(Integer fjobNum) {
		this.fjobNum = fjobNum;
	}
	
    /**
     * @return fjobNum
     */
	public Integer getFjobNum() {
		return this.fjobNum;
	}
	
	/**
	 * @param salVolume
	 */
	public void setSalVolume(java.math.BigDecimal salVolume) {
		this.salVolume = salVolume;
	}
	
    /**
     * @return salVolume
     */
	public java.math.BigDecimal getSalVolume() {
		return this.salVolume;
	}
	
	/**
	 * @param operIncome
	 */
	public void setOperIncome(java.math.BigDecimal operIncome) {
		this.operIncome = operIncome;
	}
	
    /**
     * @return operIncome
     */
	public java.math.BigDecimal getOperIncome() {
		return this.operIncome;
	}
	
	/**
	 * @param corpScale
	 */
	public void setCorpScale(String corpScale) {
		this.corpScale = corpScale;
	}
	
    /**
     * @return corpScale
     */
	public String getCorpScale() {
		return this.corpScale;
	}
	
	/**
	 * @param cusScaleForm
	 */
	public void setCusScaleForm(String cusScaleForm) {
		this.cusScaleForm = cusScaleForm;
	}
	
    /**
     * @return cusScaleForm
     */
	public String getCusScaleForm() {
		return this.cusScaleForm;
	}
	
	/**
	 * @param natEcoSec
	 */
	public void setNatEcoSec(String natEcoSec) {
		this.natEcoSec = natEcoSec;
	}
	
    /**
     * @return natEcoSec
     */
	public String getNatEcoSec() {
		return this.natEcoSec;
	}
	
	/**
	 * @param unitAlleInd
	 */
	public void setUnitAlleInd(String unitAlleInd) {
		this.unitAlleInd = unitAlleInd;
	}
	
    /**
     * @return unitAlleInd
     */
	public String getUnitAlleInd() {
		return this.unitAlleInd;
	}
	
	/**
	 * @param alleBunb
	 */
	public void setAlleBunb(Integer alleBunb) {
		this.alleBunb = alleBunb;
	}
	
    /**
     * @return alleBunb
     */
	public Integer getAlleBunb() {
		return this.alleBunb;
	}
	
	/**
	 * @param framInd
	 */
	public void setFramInd(String framInd) {
		this.framInd = framInd;
	}
	
    /**
     * @return framInd
     */
	public String getFramInd() {
		return this.framInd;
	}
	
	/**
	 * @param farmerCopInd
	 */
	public void setFarmerCopInd(String farmerCopInd) {
		this.farmerCopInd = farmerCopInd;
	}
	
    /**
     * @return farmerCopInd
     */
	public String getFarmerCopInd() {
		return this.farmerCopInd;
	}
	
	/**
	 * @param agrInd
	 */
	public void setAgrInd(String agrInd) {
		this.agrInd = agrInd;
	}
	
    /**
     * @return agrInd
     */
	public String getAgrInd() {
		return this.agrInd;
	}
	
	/**
	 * @param inclusiveFinanceStatistics
	 */
	public void setInclusiveFinanceStatistics(String inclusiveFinanceStatistics) {
		this.inclusiveFinanceStatistics = inclusiveFinanceStatistics;
	}
	
    /**
     * @return inclusiveFinanceStatistics
     */
	public String getInclusiveFinanceStatistics() {
		return this.inclusiveFinanceStatistics;
	}
	
	/**
	 * @param insCode
	 */
	public void setInsCode(String insCode) {
		this.insCode = insCode;
	}
	
    /**
     * @return insCode
     */
	public String getInsCode() {
		return this.insCode;
	}
	
	/**
	 * @param insRegDate
	 */
	public void setInsRegDate(String insRegDate) {
		this.insRegDate = insRegDate;
	}
	
    /**
     * @return insRegDate
     */
	public String getInsRegDate() {
		return this.insRegDate;
	}
	
	/**
	 * @param insEndDate
	 */
	public void setInsEndDate(String insEndDate) {
		this.insEndDate = insEndDate;
	}
	
    /**
     * @return insEndDate
     */
	public String getInsEndDate() {
		return this.insEndDate;
	}
	
	/**
	 * @param insOrg
	 */
	public void setInsOrg(String insOrg) {
		this.insOrg = insOrg;
	}
	
    /**
     * @return insOrg
     */
	public String getInsOrg() {
		return this.insOrg;
	}
	
	/**
	 * @param insAnnDate
	 */
	public void setInsAnnDate(String insAnnDate) {
		this.insAnnDate = insAnnDate;
	}
	
    /**
     * @return insAnnDate
     */
	public String getInsAnnDate() {
		return this.insAnnDate;
	}
	
	/**
	 * @param licenseType
	 */
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	
    /**
     * @return licenseType
     */
	public String getLicenseType() {
		return this.licenseType;
	}
	
	/**
	 * @param regiCode
	 */
	public void setRegiCode(String regiCode) {
		this.regiCode = regiCode;
	}
	
    /**
     * @return regiCode
     */
	public String getRegiCode() {
		return this.regiCode;
	}
	
	/**
	 * @param regiType
	 */
	public void setRegiType(String regiType) {
		this.regiType = regiType;
	}
	
    /**
     * @return regiType
     */
	public String getRegiType() {
		return this.regiType;
	}
	
	/**
	 * @param quliGarade
	 */
	public void setQuliGarade(String quliGarade) {
		this.quliGarade = quliGarade;
	}
	
    /**
     * @return quliGarade
     */
	public String getQuliGarade() {
		return this.quliGarade;
	}
	
	/**
	 * @param adminOrg
	 */
	public void setAdminOrg(String adminOrg) {
		this.adminOrg = adminOrg;
	}
	
    /**
     * @return adminOrg
     */
	public String getAdminOrg() {
		return this.adminOrg;
	}
	
	/**
	 * @param apprOrg
	 */
	public void setApprOrg(String apprOrg) {
		this.apprOrg = apprOrg;
	}
	
    /**
     * @return apprOrg
     */
	public String getApprOrg() {
		return this.apprOrg;
	}
	
	/**
	 * @param apprDocNo
	 */
	public void setApprDocNo(String apprDocNo) {
		this.apprDocNo = apprDocNo;
	}
	
    /**
     * @return apprDocNo
     */
	public String getApprDocNo() {
		return this.apprDocNo;
	}
	
	/**
	 * @param regiAreaCode
	 */
	public void setRegiAreaCode(String regiAreaCode) {
		this.regiAreaCode = regiAreaCode;
	}
	
    /**
     * @return regiAreaCode
     */
	public String getRegiAreaCode() {
		return this.regiAreaCode;
	}
	
	/**
	 * @param regiAddr
	 */
	public void setRegiAddr(String regiAddr) {
		this.regiAddr = regiAddr;
	}
	
    /**
     * @return regiAddr
     */
	public String getRegiAddr() {
		return this.regiAddr;
	}
	
	/**
	 * @param regiAddrEn
	 */
	public void setRegiAddrEn(String regiAddrEn) {
		this.regiAddrEn = regiAddrEn;
	}
	
    /**
     * @return regiAddrEn
     */
	public String getRegiAddrEn() {
		return this.regiAddrEn;
	}
	
	/**
	 * @param acuStateCode
	 */
	public void setAcuStateCode(String acuStateCode) {
		this.acuStateCode = acuStateCode;
	}
	
    /**
     * @return acuStateCode
     */
	public String getAcuStateCode() {
		return this.acuStateCode;
	}
	
	/**
	 * @param operAddrAct
	 */
	public void setOperAddrAct(String operAddrAct) {
		this.operAddrAct = operAddrAct;
	}
	
    /**
     * @return operAddrAct
     */
	public String getOperAddrAct() {
		return this.operAddrAct;
	}
	
	/**
	 * @param mainOptScp
	 */
	public void setMainOptScp(String mainOptScp) {
		this.mainOptScp = mainOptScp;
	}
	
    /**
     * @return mainOptScp
     */
	public String getMainOptScp() {
		return this.mainOptScp;
	}
	
	/**
	 * @param partOptScp
	 */
	public void setPartOptScp(String partOptScp) {
		this.partOptScp = partOptScp;
	}
	
    /**
     * @return partOptScp
     */
	public String getPartOptScp() {
		return this.partOptScp;
	}
	
	/**
	 * @param regiCurType
	 */
	public void setRegiCurType(String regiCurType) {
		this.regiCurType = regiCurType;
	}
	
    /**
     * @return regiCurType
     */
	public String getRegiCurType() {
		return this.regiCurType;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param paidCapCurType
	 */
	public void setPaidCapCurType(String paidCapCurType) {
		this.paidCapCurType = paidCapCurType;
	}
	
    /**
     * @return paidCapCurType
     */
	public String getPaidCapCurType() {
		return this.paidCapCurType;
	}
	
	/**
	 * @param paidCapAmt
	 */
	public void setPaidCapAmt(java.math.BigDecimal paidCapAmt) {
		this.paidCapAmt = paidCapAmt;
	}
	
    /**
     * @return paidCapAmt
     */
	public java.math.BigDecimal getPaidCapAmt() {
		return this.paidCapAmt;
	}
	
	/**
	 * @param regiStartDate
	 */
	public void setRegiStartDate(String regiStartDate) {
		this.regiStartDate = regiStartDate;
	}
	
    /**
     * @return regiStartDate
     */
	public String getRegiStartDate() {
		return this.regiStartDate;
	}
	
	/**
	 * @param regiEndDate
	 */
	public void setRegiEndDate(String regiEndDate) {
		this.regiEndDate = regiEndDate;
	}
	
    /**
     * @return regiEndDate
     */
	public String getRegiEndDate() {
		return this.regiEndDate;
	}
	
	/**
	 * @param regAudit
	 */
	public void setRegAudit(String regAudit) {
		this.regAudit = regAudit;
	}
	
    /**
     * @return regAudit
     */
	public String getRegAudit() {
		return this.regAudit;
	}
	
	/**
	 * @param regAuditDate
	 */
	public void setRegAuditDate(String regAuditDate) {
		this.regAuditDate = regAuditDate;
	}
	
    /**
     * @return regAuditDate
     */
	public String getRegAuditDate() {
		return this.regAuditDate;
	}
	
	/**
	 * @param regAuditEndDate
	 */
	public void setRegAuditEndDate(String regAuditEndDate) {
		this.regAuditEndDate = regAuditEndDate;
	}
	
    /**
     * @return regAuditEndDate
     */
	public String getRegAuditEndDate() {
		return this.regAuditEndDate;
	}
	
	/**
	 * @param natTaxRegCode
	 */
	public void setNatTaxRegCode(String natTaxRegCode) {
		this.natTaxRegCode = natTaxRegCode;
	}
	
    /**
     * @return natTaxRegCode
     */
	public String getNatTaxRegCode() {
		return this.natTaxRegCode;
	}
	
	/**
	 * @param natTaxRegOrg
	 */
	public void setNatTaxRegOrg(String natTaxRegOrg) {
		this.natTaxRegOrg = natTaxRegOrg;
	}
	
    /**
     * @return natTaxRegOrg
     */
	public String getNatTaxRegOrg() {
		return this.natTaxRegOrg;
	}
	
	/**
	 * @param natTaxRegDt
	 */
	public void setNatTaxRegDt(String natTaxRegDt) {
		this.natTaxRegDt = natTaxRegDt;
	}
	
    /**
     * @return natTaxRegDt
     */
	public String getNatTaxRegDt() {
		return this.natTaxRegDt;
	}
	
	/**
	 * @param natTaxRegEndDt
	 */
	public void setNatTaxRegEndDt(String natTaxRegEndDt) {
		this.natTaxRegEndDt = natTaxRegEndDt;
	}
	
    /**
     * @return natTaxRegEndDt
     */
	public String getNatTaxRegEndDt() {
		return this.natTaxRegEndDt;
	}
	
	/**
	 * @param natTaxAnnDate
	 */
	public void setNatTaxAnnDate(String natTaxAnnDate) {
		this.natTaxAnnDate = natTaxAnnDate;
	}
	
    /**
     * @return natTaxAnnDate
     */
	public String getNatTaxAnnDate() {
		return this.natTaxAnnDate;
	}
	
	/**
	 * @param locTaxRegCode
	 */
	public void setLocTaxRegCode(String locTaxRegCode) {
		this.locTaxRegCode = locTaxRegCode;
	}
	
    /**
     * @return locTaxRegCode
     */
	public String getLocTaxRegCode() {
		return this.locTaxRegCode;
	}
	
	/**
	 * @param locTaxRegOrg
	 */
	public void setLocTaxRegOrg(String locTaxRegOrg) {
		this.locTaxRegOrg = locTaxRegOrg;
	}
	
    /**
     * @return locTaxRegOrg
     */
	public String getLocTaxRegOrg() {
		return this.locTaxRegOrg;
	}
	
	/**
	 * @param locTaxRegDt
	 */
	public void setLocTaxRegDt(String locTaxRegDt) {
		this.locTaxRegDt = locTaxRegDt;
	}
	
    /**
     * @return locTaxRegDt
     */
	public String getLocTaxRegDt() {
		return this.locTaxRegDt;
	}
	
	/**
	 * @param locTaxRegEndDt
	 */
	public void setLocTaxRegEndDt(String locTaxRegEndDt) {
		this.locTaxRegEndDt = locTaxRegEndDt;
	}
	
    /**
     * @return locTaxRegEndDt
     */
	public String getLocTaxRegEndDt() {
		return this.locTaxRegEndDt;
	}
	
	/**
	 * @param locTaxAnnDate
	 */
	public void setLocTaxAnnDate(String locTaxAnnDate) {
		this.locTaxAnnDate = locTaxAnnDate;
	}
	
    /**
     * @return locTaxAnnDate
     */
	public String getLocTaxAnnDate() {
		return this.locTaxAnnDate;
	}
	
	/**
	 * @param loanCardFlg
	 */
	public void setLoanCardFlg(String loanCardFlg) {
		this.loanCardFlg = loanCardFlg;
	}
	
    /**
     * @return loanCardFlg
     */
	public String getLoanCardFlg() {
		return this.loanCardFlg;
	}
	
	/**
	 * @param loanCardId
	 */
	public void setLoanCardId(String loanCardId) {
		this.loanCardId = loanCardId;
	}
	
    /**
     * @return loanCardId
     */
	public String getLoanCardId() {
		return this.loanCardId;
	}
	
	/**
	 * @param loanCardPwd
	 */
	public void setLoanCardPwd(String loanCardPwd) {
		this.loanCardPwd = loanCardPwd;
	}
	
    /**
     * @return loanCardPwd
     */
	public String getLoanCardPwd() {
		return this.loanCardPwd;
	}
	
	/**
	 * @param loanCardEffFlg
	 */
	public void setLoanCardEffFlg(String loanCardEffFlg) {
		this.loanCardEffFlg = loanCardEffFlg;
	}
	
    /**
     * @return loanCardEffFlg
     */
	public String getLoanCardEffFlg() {
		return this.loanCardEffFlg;
	}
	
	/**
	 * @param loanCardAnnDate
	 */
	public void setLoanCardAnnDate(String loanCardAnnDate) {
		this.loanCardAnnDate = loanCardAnnDate;
	}
	
    /**
     * @return loanCardAnnDate
     */
	public String getLoanCardAnnDate() {
		return this.loanCardAnnDate;
	}
	
	/**
	 * @param certIdate
	 */
	public void setCertIdate(String certIdate) {
		this.certIdate = certIdate;
	}
	
    /**
     * @return certIdate
     */
	public String getCertIdate() {
		return this.certIdate;
	}
	
	/**
	 * @param conType
	 */
	public void setConType(String conType) {
		this.conType = conType;
	}
	
    /**
     * @return conType
     */
	public String getConType() {
		return this.conType;
	}
	
	/**
	 * @param corpOwnersType
	 */
	public void setCorpOwnersType(String corpOwnersType) {
		this.corpOwnersType = corpOwnersType;
	}
	
    /**
     * @return corpOwnersType
     */
	public String getCorpOwnersType() {
		return this.corpOwnersType;
	}
	
	/**
	 * @param isBankShd
	 */
	public void setIsBankShd(String isBankShd) {
		this.isBankShd = isBankShd;
	}
	
    /**
     * @return isBankShd
     */
	public String getIsBankShd() {
		return this.isBankShd;
	}
	
	/**
	 * @param isSmconCus
	 */
	public void setIsSmconCus(String isSmconCus) {
		this.isSmconCus = isSmconCus;
	}
	
    /**
     * @return isSmconCus
     */
	public String getIsSmconCus() {
		return this.isSmconCus;
	}
	
	/**
	 * @param detailAddr
	 */
	public void setDetailAddr(String detailAddr) {
		this.detailAddr = detailAddr;
	}
	
    /**
     * @return detailAddr
     */
	public String getDetailAddr() {
		return this.detailAddr;
	}
	
	/**
	 * @param produceEquipYear
	 */
	public void setProduceEquipYear(String produceEquipYear) {
		this.produceEquipYear = produceEquipYear;
	}
	
    /**
     * @return produceEquipYear
     */
	public String getProduceEquipYear() {
		return this.produceEquipYear;
	}
	
	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}
	
    /**
     * @return qq
     */
	public String getQq() {
		return this.qq;
	}
	
	/**
	 * @param produceAbiYear
	 */
	public void setProduceAbiYear(String produceAbiYear) {
		this.produceAbiYear = produceAbiYear;
	}
	
    /**
     * @return produceAbiYear
     */
	public String getProduceAbiYear() {
		return this.produceAbiYear;
	}
	
	/**
	 * @param isNatctl
	 */
	public void setIsNatctl(String isNatctl) {
		this.isNatctl = isNatctl;
	}
	
    /**
     * @return isNatctl
     */
	public String getIsNatctl() {
		return this.isNatctl;
	}
	
	/**
	 * @param freqLinkman
	 */
	public void setFreqLinkman(String freqLinkman) {
		this.freqLinkman = freqLinkman;
	}
	
    /**
     * @return freqLinkman
     */
	public String getFreqLinkman() {
		return this.freqLinkman;
	}
	
	/**
	 * @param natctlLevel
	 */
	public void setNatctlLevel(String natctlLevel) {
		this.natctlLevel = natctlLevel;
	}
	
    /**
     * @return natctlLevel
     */
	public String getNatctlLevel() {
		return this.natctlLevel;
	}
	
	/**
	 * @param licOperPro
	 */
	public void setLicOperPro(String licOperPro) {
		this.licOperPro = licOperPro;
	}
	
    /**
     * @return licOperPro
     */
	public String getLicOperPro() {
		return this.licOperPro;
	}
	
	/**
	 * @param freqLinkmanTel
	 */
	public void setFreqLinkmanTel(String freqLinkmanTel) {
		this.freqLinkmanTel = freqLinkmanTel;
	}
	
    /**
     * @return freqLinkmanTel
     */
	public String getFreqLinkmanTel() {
		return this.freqLinkmanTel;
	}
	
	/**
	 * @param commonOperPro
	 */
	public void setCommonOperPro(String commonOperPro) {
		this.commonOperPro = commonOperPro;
	}
	
    /**
     * @return commonOperPro
     */
	public String getCommonOperPro() {
		return this.commonOperPro;
	}
	
	/**
	 * @param operStatus
	 */
	public void setOperStatus(String operStatus) {
		this.operStatus = operStatus;
	}
	
    /**
     * @return operStatus
     */
	public String getOperStatus() {
		return this.operStatus;
	}
	
	/**
	 * @param isLongVld
	 */
	public void setIsLongVld(String isLongVld) {
		this.isLongVld = isLongVld;
	}
	
    /**
     * @return isLongVld
     */
	public String getIsLongVld() {
		return this.isLongVld;
	}
	
	/**
	 * @param basicDepAccNoOpenLic
	 */
	public void setBasicDepAccNoOpenLic(String basicDepAccNoOpenLic) {
		this.basicDepAccNoOpenLic = basicDepAccNoOpenLic;
	}
	
    /**
     * @return basicDepAccNoOpenLic
     */
	public String getBasicDepAccNoOpenLic() {
		return this.basicDepAccNoOpenLic;
	}
	
	/**
	 * @param basicDepAccNo
	 */
	public void setBasicDepAccNo(String basicDepAccNo) {
		this.basicDepAccNo = basicDepAccNo;
	}
	
    /**
     * @return basicDepAccNo
     */
	public String getBasicDepAccNo() {
		return this.basicDepAccNo;
	}
	
	/**
	 * @param isBankBasicDepAccNo
	 */
	public void setIsBankBasicDepAccNo(String isBankBasicDepAccNo) {
		this.isBankBasicDepAccNo = isBankBasicDepAccNo;
	}
	
    /**
     * @return isBankBasicDepAccNo
     */
	public String getIsBankBasicDepAccNo() {
		return this.isBankBasicDepAccNo;
	}
	
	/**
	 * @param basicDepAccob
	 */
	public void setBasicDepAccob(String basicDepAccob) {
		this.basicDepAccob = basicDepAccob;
	}
	
    /**
     * @return basicDepAccob
     */
	public String getBasicDepAccob() {
		return this.basicDepAccob;
	}
	
	/**
	 * @param basicAccNoOpenDate
	 */
	public void setBasicAccNoOpenDate(String basicAccNoOpenDate) {
		this.basicAccNoOpenDate = basicAccNoOpenDate;
	}
	
    /**
     * @return basicAccNoOpenDate
     */
	public String getBasicAccNoOpenDate() {
		return this.basicAccNoOpenDate;
	}
	
	/**
	 * @param commonAccNoOpenDate
	 */
	public void setCommonAccNoOpenDate(String commonAccNoOpenDate) {
		this.commonAccNoOpenDate = commonAccNoOpenDate;
	}
	
    /**
     * @return commonAccNoOpenDate
     */
	public String getCommonAccNoOpenDate() {
		return this.commonAccNoOpenDate;
	}
	
	/**
	 * @param mainPrdDesc
	 */
	public void setMainPrdDesc(String mainPrdDesc) {
		this.mainPrdDesc = mainPrdDesc;
	}
	
    /**
     * @return mainPrdDesc
     */
	public String getMainPrdDesc() {
		return this.mainPrdDesc;
	}
	
	/**
	 * @param creditLevelOuter
	 */
	public void setCreditLevelOuter(String creditLevelOuter) {
		this.creditLevelOuter = creditLevelOuter;
	}
	
    /**
     * @return creditLevelOuter
     */
	public String getCreditLevelOuter() {
		return this.creditLevelOuter;
	}
	
	/**
	 * @param evalDate
	 */
	public void setEvalDate(String evalDate) {
		this.evalDate = evalDate;
	}
	
    /**
     * @return evalDate
     */
	public String getEvalDate() {
		return this.evalDate;
	}
	
	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	
    /**
     * @return fax
     */
	public String getFax() {
		return this.fax;
	}
	
	/**
	 * @param evalOrgId
	 */
	public void setEvalOrgId(String evalOrgId) {
		this.evalOrgId = evalOrgId;
	}
	
    /**
     * @return evalOrgId
     */
	public String getEvalOrgId() {
		return this.evalOrgId;
	}
	
	/**
	 * @param linkmanEmail
	 */
	public void setLinkmanEmail(String linkmanEmail) {
		this.linkmanEmail = linkmanEmail;
	}
	
    /**
     * @return linkmanEmail
     */
	public String getLinkmanEmail() {
		return this.linkmanEmail;
	}
	
	/**
	 * @param sendAddr
	 */
	public void setSendAddr(String sendAddr) {
		this.sendAddr = sendAddr;
	}
	
    /**
     * @return sendAddr
     */
	public String getSendAddr() {
		return this.sendAddr;
	}
	
	/**
	 * @param initLoanDate
	 */
	public void setInitLoanDate(String initLoanDate) {
		this.initLoanDate = initLoanDate;
	}
	
    /**
     * @return initLoanDate
     */
	public String getInitLoanDate() {
		return this.initLoanDate;
	}
	
	/**
	 * @param wechatNo
	 */
	public void setWechatNo(String wechatNo) {
		this.wechatNo = wechatNo;
	}
	
    /**
     * @return wechatNo
     */
	public String getWechatNo() {
		return this.wechatNo;
	}
	
	/**
	 * @param isStrgcCus
	 */
	public void setIsStrgcCus(String isStrgcCus) {
		this.isStrgcCus = isStrgcCus;
	}
	
    /**
     * @return isStrgcCus
     */
	public String getIsStrgcCus() {
		return this.isStrgcCus;
	}
	
	/**
	 * @param finaReportType
	 */
	public void setFinaReportType(String finaReportType) {
		this.finaReportType = finaReportType;
	}
	
    /**
     * @return finaReportType
     */
	public String getFinaReportType() {
		return this.finaReportType;
	}
	
	/**
	 * @param areaPriorCorp
	 */
	public void setAreaPriorCorp(String areaPriorCorp) {
		this.areaPriorCorp = areaPriorCorp;
	}
	
    /**
     * @return areaPriorCorp
     */
	public String getAreaPriorCorp() {
		return this.areaPriorCorp;
	}
	
	/**
	 * @param spOperFlag
	 */
	public void setSpOperFlag(String spOperFlag) {
		this.spOperFlag = spOperFlag;
	}
	
    /**
     * @return spOperFlag
     */
	public String getSpOperFlag() {
		return this.spOperFlag;
	}
	
	/**
	 * @param isNewBuildCorp
	 */
	public void setIsNewBuildCorp(String isNewBuildCorp) {
		this.isNewBuildCorp = isNewBuildCorp;
	}
	
    /**
     * @return isNewBuildCorp
     */
	public String getIsNewBuildCorp() {
		return this.isNewBuildCorp;
	}
	
	/**
	 * @param grpCusType
	 */
	public void setGrpCusType(String grpCusType) {
		this.grpCusType = grpCusType;
	}
	
    /**
     * @return grpCusType
     */
	public String getGrpCusType() {
		return this.grpCusType;
	}
	
	/**
	 * @param regiOrg
	 */
	public void setRegiOrg(String regiOrg) {
		this.regiOrg = regiOrg;
	}
	
    /**
     * @return regiOrg
     */
	public String getRegiOrg() {
		return this.regiOrg;
	}
	
	/**
	 * @param mainBusNation
	 */
	public void setMainBusNation(String mainBusNation) {
		this.mainBusNation = mainBusNation;
	}
	
    /**
     * @return mainBusNation
     */
	public String getMainBusNation() {
		return this.mainBusNation;
	}
	
	/**
	 * @param creditPadAccNo
	 */
	public void setCreditPadAccNo(String creditPadAccNo) {
		this.creditPadAccNo = creditPadAccNo;
	}
	
    /**
     * @return creditPadAccNo
     */
	public String getCreditPadAccNo() {
		return this.creditPadAccNo;
	}
	
	/**
	 * @param loanType
	 */
	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
    /**
     * @return loanType
     */
	public String getLoanType() {
		return this.loanType;
	}
	
	/**
	 * @param operPlaceOwnshp
	 */
	public void setOperPlaceOwnshp(String operPlaceOwnshp) {
		this.operPlaceOwnshp = operPlaceOwnshp;
	}
	
    /**
     * @return operPlaceOwnshp
     */
	public String getOperPlaceOwnshp() {
		return this.operPlaceOwnshp;
	}
	
	/**
	 * @param operPlaceSqu
	 */
	public void setOperPlaceSqu(java.math.BigDecimal operPlaceSqu) {
		this.operPlaceSqu = operPlaceSqu;
	}
	
    /**
     * @return operPlaceSqu
     */
	public java.math.BigDecimal getOperPlaceSqu() {
		return this.operPlaceSqu;
	}
	
	/**
	 * @param mainProduceEquip
	 */
	public void setMainProduceEquip(String mainProduceEquip) {
		this.mainProduceEquip = mainProduceEquip;
	}
	
    /**
     * @return mainProduceEquip
     */
	public String getMainProduceEquip() {
		return this.mainProduceEquip;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param iisSzjrfwCrop
	 */
	public void setIisSzjrfwCrop(String iisSzjrfwCrop) {
		this.iisSzjrfwCrop = iisSzjrfwCrop;
	}
	
    /**
     * @return iisSzjrfwCrop
     */
	public String getIisSzjrfwCrop() {
		return this.iisSzjrfwCrop;
	}
	
	/**
	 * @param isSteelCus
	 */
	public void setIsSteelCus(String isSteelCus) {
		this.isSteelCus = isSteelCus;
	}
	
    /**
     * @return isSteelCus
     */
	public String getIsSteelCus() {
		return this.isSteelCus;
	}
	
	/**
	 * @param isStockCorp
	 */
	public void setIsStockCorp(String isStockCorp) {
		this.isStockCorp = isStockCorp;
	}
	
    /**
     * @return isStockCorp
     */
	public String getIsStockCorp() {
		return this.isStockCorp;
	}
	
	/**
	 * @param isCtinve
	 */
	public void setIsCtinve(String isCtinve) {
		this.isCtinve = isCtinve;
	}
	
    /**
     * @return isCtinve
     */
	public String getIsCtinve() {
		return this.isCtinve;
	}
	
	/**
	 * @param ctinveLevel
	 */
	public void setCtinveLevel(String ctinveLevel) {
		this.ctinveLevel = ctinveLevel;
	}
	
    /**
     * @return ctinveLevel
     */
	public String getCtinveLevel() {
		return this.ctinveLevel;
	}
	
	/**
	 * @param goverInvestPlat
	 */
	public void setGoverInvestPlat(String goverInvestPlat) {
		this.goverInvestPlat = goverInvestPlat;
	}
	
    /**
     * @return goverInvestPlat
     */
	public String getGoverInvestPlat() {
		return this.goverInvestPlat;
	}
	
	/**
	 * @param impexpFlag
	 */
	public void setImpexpFlag(String impexpFlag) {
		this.impexpFlag = impexpFlag;
	}
	
    /**
     * @return impexpFlag
     */
	public String getImpexpFlag() {
		return this.impexpFlag;
	}
	
	/**
	 * @param redcbizUnitTradeClass
	 */
	public void setRedcbizUnitTradeClass(String redcbizUnitTradeClass) {
		this.redcbizUnitTradeClass = redcbizUnitTradeClass;
	}
	
    /**
     * @return redcbizUnitTradeClass
     */
	public String getRedcbizUnitTradeClass() {
		return this.redcbizUnitTradeClass;
	}
	
	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}
	
    /**
     * @return cusType
     */
	public String getCusType() {
		return this.cusType;
	}

	/**
	 * @param islocked
	 */
	public void setIslocked(String islocked) {
		this.islocked = islocked;
	}

	/**
	 * @return islocked
	 */
	public String getIslocked() {
		return this.islocked;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @param bankLoanLevel
	 */
	public void setBankLoanLevel(String bankLoanLevel) {
		this.bankLoanLevel = bankLoanLevel;
	}

	/**
	 * @return bankLoanLevel
	 */
	public String getBankLoanLevel() {
		return this.bankLoanLevel;
	}

	/**
	 * @param pd
	 */
	public void setPd(String pd) {
		this.pd = pd;
	}

	/**
	 * @return pd
	 */
	public String getPd() {
		return this.pd;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
}