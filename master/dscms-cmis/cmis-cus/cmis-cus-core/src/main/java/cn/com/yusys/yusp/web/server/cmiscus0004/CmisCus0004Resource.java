package cn.com.yusys.yusp.web.server.cmiscus0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0004.req.CmisCus0004ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0004.resp.CmisCus0004RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscus0004.CmisCus0004Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 个人关联客户列表查询
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0004:查询个人关联客户列表")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0004Resource.class);

    @Autowired
    private CmisCus0004Service cmisCus0004Service;

    /**
     * 交易码：cmiscus0004
     * 交易描述：个人关联客户列表查询
     *
     * @param cmisCus0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("个人关联客户列表查询")
    @PostMapping("/cmiscus0004")
    protected @ResponseBody
    ResultDto<CmisCus0004RespDto> cmiscus0004(@Validated @RequestBody CmisCus0004ReqDto cmisCus0004ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0004.key, DscmsEnum.TRADE_CODE_CMISCUS0004.value, JSON.toJSONString(cmisCus0004ReqDto));
        CmisCus0004RespDto cmisCus0004RespDto = new CmisCus0004RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0004RespDto> cmisCus0004ResultDto = new ResultDto<>();
        try {
            // 封装cmisCus0004ResultDto中正确的返回码和返回信息
            cmisCus0004RespDto = cmisCus0004Service.execute(cmisCus0004ReqDto);
            cmisCus0004ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0004ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.error(DscmsEnum.TRADE_CODE_CMISCUS0004.key+"报错：", e);
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0004.key, DscmsEnum.TRADE_CODE_CMISCUS0004.value, e.getMessage());
            // 封装cmisCus0004ResultDto中异常返回码和返回信息
            cmisCus0004ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0004ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0004RespDto到cmisCus0004ResultDto中
        cmisCus0004ResultDto.setData(cmisCus0004RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0004.key, DscmsEnum.TRADE_CODE_CMISCUS0004.value, JSON.toJSONString(cmisCus0004ResultDto));
        return cmisCus0004ResultDto;
    }
}