/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusAccountInfo
 * @类描述: cus_account_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 10:18:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_account_info")
public class CusAccountInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户编号 **/
	@Id
	@Column(name = "CUS_ID")
	private String cusId;
	
	/** 结算账户账号 **/
	@Column(name = "SETTL_ACCT_NUM", unique = false, nullable = true, length = 40)
	private String settlAcctNum;
	
	/** 账户名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 80)
	private String acctName;
	
	/** 账户子序号 **/
	@Column(name = "ACCT_SUB_NUM", unique = false, nullable = true, length = 40)
	private String acctSubNum;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 开户机构 **/
	@Column(name = "OPEN_ORG_ID", unique = false, nullable = true, length = 40)
	private String openOrgId;
	
	/** 是否使用 **/
	@Column(name = "IS_EFFECT", unique = false, nullable = true, length = 2)
	private String isEffect;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param settlAcctNum
	 */
	public void setSettlAcctNum(String settlAcctNum) {
		this.settlAcctNum = settlAcctNum;
	}
	
    /**
     * @return settlAcctNum
     */
	public String getSettlAcctNum() {
		return this.settlAcctNum;
	}
	
	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	
    /**
     * @return acctName
     */
	public String getAcctName() {
		return this.acctName;
	}
	
	/**
	 * @param acctSubNum
	 */
	public void setAcctSubNum(String acctSubNum) {
		this.acctSubNum = acctSubNum;
	}
	
    /**
     * @return acctSubNum
     */
	public String getAcctSubNum() {
		return this.acctSubNum;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param openOrgId
	 */
	public void setOpenOrgId(String openOrgId) {
		this.openOrgId = openOrgId;
	}
	
    /**
     * @return openOrgId
     */
	public String getOpenOrgId() {
		return this.openOrgId;
	}
	
	/**
	 * @param isEffect
	 */
	public void setIsEffect(String isEffect) {
		this.isEffect = isEffect;
	}
	
    /**
     * @return isEffect
     */
	public String getIsEffect() {
		return this.isEffect;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}