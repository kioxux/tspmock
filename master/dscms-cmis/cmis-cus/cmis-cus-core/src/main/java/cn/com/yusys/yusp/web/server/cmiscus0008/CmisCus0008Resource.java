package cn.com.yusys.yusp.web.server.cmiscus0008;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusIndiv;
import cn.com.yusys.yusp.dto.server.cmiscus0007.req.CmisCus0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.req.CmisCus0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.resp.CmisCus0008RespDto;
import cn.com.yusys.yusp.enums.cache.CacheKeyEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.service.CusBaseService;
import cn.com.yusys.yusp.web.server.cmiscus0007.CmisCus0007Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * 接口处理类: 查询根据客户编号查询法人名称,名称，证件类型，证件号，电话
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "CmisCus0008:查询根据客户编号查询法人名称,名称，证件类型，证件号，电话")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0008Resource.class);

    @Autowired
    private CusBaseService cusBaseService;
    /**
     * 交易码：cmiscus0007
     * 交易描述：查询个人客户基本信息
     *
     * @param cmisCus0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户编号查询法人名称,名称，证件类型，证件号，电话")
    @PostMapping("/cmiscus0008")
    protected @ResponseBody
    ResultDto<CmisCus0008RespDto> cmiscus0008(@Validated @RequestBody CmisCus0008ReqDto cmisCus0008ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0008.key, DscmsEnum.TRADE_CODE_CMISCUS0008.value, JSON.toJSONString(cmisCus0008ReqDto));
        CmisCus0008RespDto cmisCus0008RespDto = new CmisCus0008RespDto();// 响应Dto:查询个人客户基本信息
        ResultDto<CmisCus0008RespDto> cmisCus0008ResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0008.key, DscmsEnum.TRADE_CODE_CMISCUS0008.value, JSON.toJSONString(cmisCus0008ReqDto));
            cmisCus0008RespDto = cusBaseService.cmiscus0008(cmisCus0008ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0008.key, DscmsEnum.TRADE_CODE_CMISCUS0008.value, JSON.toJSONString(cmisCus0008RespDto));
            // 封装cmisCus0007ResultDto中正确的返回码和返回信息
            cmisCus0008ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0008ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, e.getMessage());
            // 封装cmisCus0007ResultDto中异常返回码和返回信息
            cmisCus0008ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0008ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0007RespDto到cmisCus0007ResultDto中
        cmisCus0008ResultDto.setData(cmisCus0008RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(cmisCus0008ResultDto));
        return cmisCus0008ResultDto;
    }
}
