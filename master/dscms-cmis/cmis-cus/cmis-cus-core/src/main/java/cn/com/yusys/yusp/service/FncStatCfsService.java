/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.FncStatCfs;
import cn.com.yusys.yusp.repository.mapper.FncStatCfsMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncStatCfsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-03 14:38:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class FncStatCfsService {

    @Autowired
    private FncStatCfsMapper fncStatCfsMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public FncStatCfs selectByPrimaryKey(String cusId, String statStyle, String statYear, String statItemId) {
        return fncStatCfsMapper.selectByPrimaryKey(cusId, statStyle, statYear, statItemId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<FncStatCfs> selectAll(QueryModel model) {
        List<FncStatCfs> records = (List<FncStatCfs>) fncStatCfsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<FncStatCfs> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FncStatCfs> list = fncStatCfsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(FncStatCfs record) {
        return fncStatCfsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(FncStatCfs record) {
        return fncStatCfsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(FncStatCfs record) {
        return fncStatCfsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(FncStatCfs record) {
        return fncStatCfsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId, String statStyle, String statYear, String statItemId) {
        return fncStatCfsMapper.deleteByPrimaryKey(cusId, statStyle, statYear, statItemId);
    }

    /**
     * 根据客户号、报表年、项目编号查询财报现金流量明细信息
     * @param queryModel
     * @return
     */
    public List<FncStatCfs> selectByQueryModel(QueryModel queryModel){
        return fncStatCfsMapper.selectByQueryModel(queryModel);
    }



}
