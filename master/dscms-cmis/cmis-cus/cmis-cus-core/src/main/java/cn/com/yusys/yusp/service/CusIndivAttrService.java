/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Date;
import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.dto.CusIndivAttrDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusIndivAttr;
import cn.com.yusys.yusp.repository.mapper.CusIndivAttrMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivAttrService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-09 17:41:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusIndivAttrService {

    @Autowired
    private CusIndivAttrMapper cusIndivAttrMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusIndivAttr selectByPrimaryKey(String cusId) {
        return cusIndivAttrMapper.selectByPrimaryKey(cusId);
    }
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据客户号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusIndivAttr selectBycusId(String cusId) {
        return cusIndivAttrMapper.selectBycusId(cusId);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusIndivAttr> selectAll(QueryModel model) {
        List<CusIndivAttr> records = (List<CusIndivAttr>) cusIndivAttrMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusIndivAttr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusIndivAttr> list = cusIndivAttrMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusIndivAttr record) {
        return cusIndivAttrMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusIndivAttr record) {
        return cusIndivAttrMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusIndivAttr record) {
        return cusIndivAttrMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusIndivAttr record) {
        return cusIndivAttrMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId) {
        return cusIndivAttrMapper.deleteByPrimaryKey(cusId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusIndivAttrMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryCusIndivAttrByCusId
     * @方法描述: 根据客户编号查询个人客户属性信息表
     * @创建人：zhangliang15
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CusIndivAttrDto queryCusIndivAttrByCusId(String cusId) {
        return cusIndivAttrMapper.queryCusIndivAttrByCusId(cusId);

    }

    /**
     * @方法名称: save
     * @方法描述: 客户属性保存，有则更新，无责保存
     * @参数与返回说明:
     * @算法描述: 无
     * @修改人 周茂伟
     */
    public int save(CusIndivAttr cusIndivAttr) {
        int  result=0;
        if(cusIndivAttr != null){
            // 申请人
            String inputId = "";
            // 申请机构
            String inputBrId = "";
            // 申请时间
            String inputDate = "";
            // 创建时间
            Date createTime = DateUtils.getCurrTimestamp();
            // 获取用户信息
            User userInfo = SessionUtils.getUserInformation();
            if(userInfo != null){
                // 申请人
                inputId = userInfo.getLoginCode();
                // 申请机构
                inputBrId = userInfo.getOrg().getCode();
                // 申请时间
                inputDate = DateUtils.getCurrDateStr();
            }
            String cusId="";
            if(StringUtils.nonEmpty(cusIndivAttr.getCusId())){
                cusId=cusIndivAttr.getCusId();
                CusIndivAttr cusIndivAttr1 = selectByPrimaryKey(cusId);
                if (cusIndivAttr1 != null){
                    // 修改人
                    cusIndivAttr.setUpdId(inputId);
                    // 修改机构
                    cusIndivAttr.setUpdBrId(inputBrId);
                    // 修改日期
                    cusIndivAttr.setUpdDate(inputDate);
                    // 修改时间
                    cusIndivAttr.setUpdateTime(createTime);
                    //说明执行得是更新得操作
                    result = this.cusIndivAttrMapper.updateByPrimaryKeySelective(cusIndivAttr);
                }else{
                    // 申请人
                    cusIndivAttr.setInputId(inputId);
                    // 申请机构
                    cusIndivAttr.setInputBrId(inputBrId);
                    // 申请时间
                    cusIndivAttr.setInputDate(inputDate);
                    // 创建时间
                    cusIndivAttr.setCreateTime(createTime);
                    // 修改人
                    cusIndivAttr.setUpdId(inputId);
                    // 修改机构
                    cusIndivAttr.setUpdBrId(inputBrId);
                    // 修改日期
                    cusIndivAttr.setUpdDate(inputDate);
                    // 修改时间
                    cusIndivAttr.setUpdateTime(createTime);
                    result = this.cusIndivAttrMapper.insertSelective(cusIndivAttr);
                }
            }
        }
        return result;
    }
}
