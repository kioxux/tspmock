package cn.com.yusys.yusp.service.server.xdkh0016;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:查询是否我行关系人
 *
 * @author lihh
 * @version 1.0
 */
@Service

public class Xdkh0016Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdkh0016Service.class);

    @Autowired
    private CusLstGlfMapper cusLstGlfMapper;

    /**
     * 查询是否我行关系人
     * @param certNo
     * @return
     */
    @Transactional
    public int selectRelationCount(String certNo) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, certNo);
        int count = cusLstGlfMapper.selectRelationCount(certNo);
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, count);
        return count;
    }


}
