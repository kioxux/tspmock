/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.CusCorpCert;
import cn.com.yusys.yusp.dto.CusCorpCertDto;
import cn.com.yusys.yusp.service.CusCorpCertService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpCertResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: 茂茂
 * @创建时间: 2021-04-25 16:21:23
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuscorpcert")
public class CusCorpCertResource {
    @Autowired
    private CusCorpCertService cusCorpCertService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusCorpCert>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusCorpCert> list = cusCorpCertService.selectAll(queryModel);
        return new ResultDto<List<CusCorpCert>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusCorpCert>> index(QueryModel queryModel) {
        List<CusCorpCert> list = cusCorpCertService.selectByModel(queryModel);
        return new ResultDto<List<CusCorpCert>>(list);
    }
    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询企业证件信息")
    @PostMapping("/query")
    protected ResultDto<List<CusCorpCert>> query(@RequestBody QueryModel queryModel) {
        List<CusCorpCert> list = cusCorpCertService.selectByModel(queryModel);
        return new ResultDto<List<CusCorpCert>>(list);
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusCorpCert> show(@PathVariable("pkId") String pkId) {
        CusCorpCert cusCorpCert = cusCorpCertService.selectByPrimaryKey(pkId);
        return new ResultDto<CusCorpCert>(cusCorpCert);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusCorpCert> create(@RequestBody CusCorpCert cusCorpCert) throws URISyntaxException {
        cusCorpCertService.insert(cusCorpCert);
        return new ResultDto<CusCorpCert>(cusCorpCert);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusCorpCert cusCorpCert) throws URISyntaxException {
        int result = cusCorpCertService.update(cusCorpCert);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusCorpCertService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusCorpCertService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据客户号查询
     * @param cusId
     * @return
     */
    @PostMapping("/selectByCusId")
    protected ResultDto<List<CusCorpCertDto>> selectByCusId(@RequestBody String cusId){
        return new ResultDto<List<CusCorpCertDto>>(cusCorpCertService.selectByCusId(cusId));
    }

    /**
     * 根据客户号、证件类型（企业统一社会信用代码）查询客户证件信息
     * @param map
     * @return
     */
    @PostMapping("/queryCusCorpCertDataByParams")
    protected List<CusCorpCertDto> queryCusCorpCertDataByParams(@RequestBody Map map){
        return cusCorpCertService.queryCusCorpCertDataByParams(map);
    }

    /**
     * 根据证件号查询客户ID
     * @param certCode
     * @return
     */
    @PostMapping("/queryCusIdByCertCode")
    protected String queryCusIdByCertCode(@RequestBody String certCode){
        return cusCorpCertService.queryCusIdByCertCode(certCode);
    }
}
