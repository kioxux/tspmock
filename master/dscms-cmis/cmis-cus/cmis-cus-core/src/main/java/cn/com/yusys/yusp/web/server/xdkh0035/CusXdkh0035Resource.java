package cn.com.yusys.yusp.web.server.xdkh0035;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0035.req.Xdkh0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0035.resp.Xdkh0035DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0035.Xdkh0035Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:违约认定、重生认定评级信息同步
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0035:违约认定、重生认定评级信息同步")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0035Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0035Resource.class);

    @Autowired
    private Xdkh0035Service xdkh0035Service;
    /**
     * 交易码：xdkh0035
     * 交易描述：违约认定、重生认定评级信息同步
     *
     * @param xdkh0035DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("违约认定、重生认定评级信息同步")
    @PostMapping("/xdkh0035")
    protected @ResponseBody
    ResultDto<Xdkh0035DataRespDto> xdkh0035(@Validated @RequestBody Xdkh0035DataReqDto xdkh0035DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, JSON.toJSONString(xdkh0035DataReqDto));
        Xdkh0035DataRespDto xdkh0035DataRespDto = new Xdkh0035DataRespDto();// 响应Dto:违约认定、重生认定评级信息同步
        ResultDto<Xdkh0035DataRespDto> xdkh0035DataResultDto = new ResultDto<>();
        try {
            // 从xdkh0035DataReqDto获取业务值进行业务逻辑处理

            // TODO 调用XXXXXService层开始
            xdkh0035DataRespDto = xdkh0035Service.xdkh0035(xdkh0035DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xdkh0035DataRespDto对象开始
            xdkh0035DataRespDto.setOpFlag(xdkh0035DataRespDto.getOpFlag());
            xdkh0035DataRespDto.setOpMsg(xdkh0035DataRespDto.getOpMsg());
            // TODO 封装xdkh0035DataRespDto对象结束
            // 封装xdkh0035DataResultDto中正确的返回码和返回信息
            xdkh0035DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0035DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, e.getMessage());
            // 封装xdkh0035DataResultDto中异常返回码和返回信息
            xdkh0035DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0035DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0035DataRespDto到xdkh0035DataResultDto中
        xdkh0035DataResultDto.setData(xdkh0035DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, JSON.toJSONString(xdkh0035DataResultDto));
        return xdkh0035DataResultDto;
    }
}
