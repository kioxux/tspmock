/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusPubHandoverLst;
import cn.com.yusys.yusp.repository.mapper.CusPubHandoverLstMapper;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusPubHandoverLstService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: pc
 * @创建时间: 2021-05-10 16:17:13
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusPubHandoverLstService {
    private final Logger log = LoggerFactory.getLogger(CusPubHandoverAppService.class);

    @Autowired
    private CusPubHandoverLstMapper cusPubHandoverLstMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusPubHandoverLst selectByPrimaryKey(String pkId) {
        return cusPubHandoverLstMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusPubHandoverLst> selectAll(QueryModel model) {
        List<CusPubHandoverLst> records = (List<CusPubHandoverLst>) cusPubHandoverLstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusPubHandoverLst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusPubHandoverLst> list = cusPubHandoverLstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusPubHandoverLst record) {
        return cusPubHandoverLstMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusPubHandoverLst record) {
        return cusPubHandoverLstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusPubHandoverLst record) {
        return cusPubHandoverLstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusPubHandoverLst record) {
        return cusPubHandoverLstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusPubHandoverLstMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusPubHandoverLstMapper.deleteByIds(ids);
    }

    public List<CusPubHandoverLst> selectByModeList(CusPubHandoverLst cusPubHandoverLst) {
        return cusPubHandoverLstMapper.selectByModeList(cusPubHandoverLst);
    }


    /**
     * @方法名称: deleteByParam
     * @方法描述: 根据map 中参数删除数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    public void deleteByParam(CusPubHandoverLst cusPubHandoverLst) {
        Map param = new HashMap();
        param.put("serno", cusPubHandoverLst.getSerno());
        cusPubHandoverLstMapper.deleteByParam(param);
    }


    /**
     * @方法名称: selectHandvoerInWayApp
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int selectHandvoerInWayApp(CusPubHandoverLst cusPubHandoverLst) {

        if (cusPubHandoverLst == null) {
            throw new YuspException(EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.key, EcsEnum.E_CUS_CLIENT_CUSNULL_EXCEPTION.value);
        }
        String serno = null;
        if(null != cusPubHandoverLst.getSerno()){
             serno = cusPubHandoverLst.getSerno();
        }
        log.info("流程发起-获取业务申请" + cusPubHandoverLst.getSerno() + "申请主表信息");

        Map map = new HashMap();
        String handoverMode = cusPubHandoverLst.getHandoverMode();
        String cusId = cusPubHandoverLst.getCusId();
        String contNo = cusPubHandoverLst.getContNo();

        Map paramMap = new HashMap();
        //客户移交
        paramMap.put("serno", serno);
        paramMap.put("cusId", cusId);
        //paramMap.put("handover_mode",handoverMode) ;
        if (CmisCusConstants.STD_ZB_HAND_TYP_2.equals(handoverMode)) {
            paramMap.put("cont_no", contNo);
        }
        return cusPubHandoverLstMapper.selectHandvoerInWayApp(paramMap);
    }

}
