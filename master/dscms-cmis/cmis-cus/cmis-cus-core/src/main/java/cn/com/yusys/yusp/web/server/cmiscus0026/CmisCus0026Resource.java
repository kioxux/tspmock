package cn.com.yusys.yusp.web.server.cmiscus0026;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusIndivContact;
import cn.com.yusys.yusp.dto.server.cmiscus0026.req.CmisCus0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0026.resp.CmisCus0026RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusIndivContactService;
import cn.com.yusys.yusp.web.server.cmiscus0007.CmisCus0007Resource;
import com.alibaba.fastjson.JSON;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 客户联系信息维护
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0026:客户联系信息维护")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0026Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisCus0007Resource.class);

    @Autowired
    private CusIndivContactService cusIndivContactService;

    /**
     * 交易码：cmiscus0026
     * 交易描述：客户联系信息维护
     *
     * @param cmisCus0026ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户联系信息维护")
    @PostMapping("/cmiscus0026")
    protected @ResponseBody
    ResultDto<CmisCus0026RespDto> cmiscus0026(@Validated @RequestBody CmisCus0026ReqDto cmisCus0026ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0026.key, DscmsEnum.TRADE_CODE_CMISCUS0026.value, JSON.toJSONString(cmisCus0026ReqDto));
        CmisCus0026RespDto cmisCus0026RespDto = new CmisCus0026RespDto();// 响应Dto:查询优农贷名单信息
        ResultDto<CmisCus0026RespDto> cmisCus0026ResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0026.key, DscmsEnum.TRADE_CODE_CMISCUS0026.value, JSON.toJSONString(cmisCus0026ReqDto));
            String cusId = cmisCus0026ReqDto.getCusId();
            if (StringUtils.isNotEmpty(cusId)) {
                CusIndivContact cusIndivContact = new CusIndivContact();
                BeanUtils.copyProperties(cmisCus0026ReqDto, cusIndivContact);// 将 cmisCus0026ReqDto 拷贝到 cusIndivContact
                cusIndivContact.setCusId(cusId);
                int result = cusIndivContactService.updateSelective(cusIndivContact);
                cmisCus0026RespDto.setResult(result);
            }
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0026.key, DscmsEnum.TRADE_CODE_CMISCUS0026.value, JSON.toJSONString(cmisCus0026RespDto));
            // 封装cmisCus0026ResultDto中正确的返回码和返回信息
            cmisCus0026ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0026ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0026.key, DscmsEnum.TRADE_CODE_CMISCUS0026.value, e.getMessage());
            // 封装cmisCus0026ResultDto中异常返回码和返回信息
            cmisCus0026ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0026ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0026RespDto到cmisCus0007ResultDto中
        cmisCus0026ResultDto.setData(cmisCus0026RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0026.key, DscmsEnum.TRADE_CODE_CMISCUS0026.value, JSON.toJSONString(cmisCus0026ResultDto));
        return cmisCus0026ResultDto;
    }

}
