/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

/**
 * 关联交易预计额度信息导入模板
 */
@ExcelCsv(namePrefix = "关联交易预计额度信息导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CusLstGlfGljyyjedVo {


    /**
     * 关联方名称
     **/
    @ExcelField(title = "关联方名称", viewLength = 80)
    private String relatedPartyName;

    /**
     * 关联方客户编号
     **/
    @ExcelField(title = "关联方客户编号", viewLength = 40)
    private String relatedPartyCusId;


    /**
     * 关联方证件类型
     **/
    @ExcelField(title = "关联方证件类型", viewLength = 5)
    private String relatedPartyCertType;

    /**
     * 关联方证件号码
     **/
    @ExcelField(title = "关联方证件号码", viewLength = 40)
    private String relatedPartyCertNo;

    /**
     * 关联方预计额度
     **/
    @ExcelField(title = "关联方预计额度", viewLength = 40)
    private String relatedPartyForeLmt;

    /**
     * 关联方类型
     **/
    @ExcelField(title = "关联方类型", viewLength = 10)
    private String relatedPartyType;

    /**
     * 归属组别
     **/
    @ExcelField(title = "归属组别", viewLength = 10)
    private String belongGroup;

    /**
     * 组别预计总额度
     **/
    @ExcelField(title = "组别预计总额度", viewLength = 40)
    private String groupForeTotlAmt;

    public String getRelatedPartyName() {
        return relatedPartyName;
    }

    public void setRelatedPartyName(String relatedPartyName) {
        this.relatedPartyName = relatedPartyName;
    }

    public String getRelatedPartyCusId() {
        return relatedPartyCusId;
    }

    public void setRelatedPartyCusId(String relatedPartyCusId) {
        this.relatedPartyCusId = relatedPartyCusId;
    }

    public String getRelatedPartyCertType() {
        return relatedPartyCertType;
    }

    public void setRelatedPartyCertType(String relatedPartyCertType) {
        this.relatedPartyCertType = relatedPartyCertType;
    }

    public String getRelatedPartyCertNo() {
        return relatedPartyCertNo;
    }

    public void setRelatedPartyCertNo(String relatedPartyCertNo) {
        this.relatedPartyCertNo = relatedPartyCertNo;
    }

    public String getRelatedPartyForeLmt() {
        return relatedPartyForeLmt;
    }

    public void setRelatedPartyForeLmt(String relatedPartyForeLmt) {
        this.relatedPartyForeLmt = relatedPartyForeLmt;
    }

    public String getRelatedPartyType() {
        return relatedPartyType;
    }

    public void setRelatedPartyType(String relatedPartyType) {
        this.relatedPartyType = relatedPartyType;
    }

    public String getBelongGroup() {
        return belongGroup;
    }

    public void setBelongGroup(String belongGroup) {
        this.belongGroup = belongGroup;
    }

    public String getGroupForeTotlAmt() {
        return groupForeTotlAmt;
    }

    public void setGroupForeTotlAmt(String groupForeTotlAmt) {
        this.groupForeTotlAmt = groupForeTotlAmt;
    }
}