package cn.com.yusys.yusp.reportconf.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfItems
 * @类描述: FNC_CONF_ITEMS数据实体类
 * @功能描述:
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 10:05:50
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Entity
@Table(name = "FNC_CONF_ITEMS")
public class FncConfItems extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 项目编号 **/
	@Id
	@Column(name = "ITEM_ID", unique = true, nullable = false, length = 20)
	private String itemId;

	/** 项目名称 **/
	@Column(name = "ITEM_NAME", unique = false, nullable = true, length = 200)
	private String itemName;

	/** 所属报表种类STD_ZB_FNC_TYP **/
	@Column(name = "FNC_CONF_TYP", unique = false, nullable = true, length = 2)
	private String fncConfTyp;

	/** 新旧报表标志STD_ZB_FNC_ON_TYP **/
	@Column(name = "FNC_NO_FLG", unique = false, nullable = true, length = 1)
	private String fncNoFlg;

	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 250)
	private String remark;

	/** 用于展现科目的公式财务分析上使用 **/
	@Column(name = "FORMULA", unique = false, nullable = true, length = 500)
	private String formula;

	/** ITEM_UNIT **/
	@Column(name = "ITEM_UNIT", unique = false, nullable = true, length = 60)
	private String itemUnit;

	/**
	 * @param itemId
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId == null ? null : itemId.trim();
	}

	/**
	 * @return ItemId
	 */
	public String getItemId() {
		return this.itemId;
	}

	/**
	 * @param itemName
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName == null ? null : itemName.trim();
	}

	/**
	 * @return ItemName
	 */
	public String getItemName() {
		return this.itemName;
	}

	/**
	 * @param fncConfTyp
	 */
	public void setFncConfTyp(String fncConfTyp) {
		this.fncConfTyp = fncConfTyp == null ? null : fncConfTyp.trim();
	}

	/**
	 * @return FncConfTyp
	 */
	public String getFncConfTyp() {
		return this.fncConfTyp;
	}

	/**
	 * @param fncNoFlg
	 */
	public void setFncNoFlg(String fncNoFlg) {
		this.fncNoFlg = fncNoFlg == null ? null : fncNoFlg.trim();
	}

	/**
	 * @return FncNoFlg
	 */
	public String getFncNoFlg() {
		return this.fncNoFlg;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	/**
	 * @return Remark
	 */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param formula
	 */
	public void setFormula(String formula) {
		this.formula = formula == null ? null : formula.trim();
	}

	/**
	 * @return Formula
	 */
	public String getFormula() {
		return this.formula;
	}

	/**
	 * @param itemUnit
	 */
	public void setItemUnit(String itemUnit) {
		this.itemUnit = itemUnit == null ? null : itemUnit.trim();
	}

	/**
	 * @return ItemUnit
	 */
	public String getItemUnit() {
		return this.itemUnit;
	}

}