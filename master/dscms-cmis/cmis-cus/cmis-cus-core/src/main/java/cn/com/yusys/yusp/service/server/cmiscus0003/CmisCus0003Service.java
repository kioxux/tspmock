package cn.com.yusys.yusp.service.server.cmiscus0003;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.dto.server.cmiscus0003.req.CmisCus0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0003.resp.CmisCus0003ComCusRelListRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0003.resp.CmisCus0003RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 业务逻辑处理类：法人关联客户列表查询
 *
 * @author dumd
 * @version 1.0
 * @since 2021年5月18日
 */
@Service
public class CmisCus0003Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0003Service.class);

    @Autowired
    private CusCorpMgrService cusCorpMgrService;

    @Autowired
    private CusIndivSocialService cusIndivSocialService;

    @Autowired
    private CusPubRelInvestService cusPubRelInvestService;

    @Autowired
    private CusCorpApitalService cusCorpApitalService;

    @Autowired
    private CusBaseService cusBaseService;

    @Transactional
    public CmisCus0003RespDto execute(CmisCus0003ReqDto reqDto) throws YuspException {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0003.key, DscmsEnum.TRADE_CODE_CMISCUS0003.value);

        CmisCus0003RespDto respDto = new CmisCus0003RespDto();
        List<CmisCus0003ComCusRelListRespDto> cusRelList = new ArrayList<>();

        try{
            String cusId = reqDto.getCusId();
            //查询法人关联高管信息
            List<Map<String, String>> comMgrList = cusCorpMgrService.selectCusInfoByCusIdRel(cusId);

            //用于保存实际控制人列表
            List<String> ctrlCusList = new ArrayList<>();

            //key：与客户关系字典项enname，value：与客户关系字典项cnname
            Map<String,String> dictMap = new HashMap<>();

            if (comMgrList!=null && comMgrList.size()>0){
                //将高管类别字典项放入dictMap里
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200300,"高管人员");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200400,"法人代表");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200500,"公司董事长");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200600,"公司总经理/厂长/CEO");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200700,"财务主管/CFO");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200800,"授权经办人");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_200900,"部门经理");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201000,"董事");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201100,"监事");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201200,"实际控制人");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201300,"受益人");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201400,"公司副总经理级的管理人员");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201500,"合伙制企业的合伙人");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201600,"中层管理人员");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201700,"股东");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201800,"顾问");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_201900,"联系人");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_202000,"执行董事");
                dictMap.put(CmisCusConstants.STD_CROP_MRG_TYPE_209900,"其他");

                for (Map<String, String> comMgr : comMgrList) {
                    CmisCus0003ComCusRelListRespDto comCusRelListRespDto = new CmisCus0003ComCusRelListRespDto();
                    //关联客户号
                    String cusIdRel = (String) comMgr.get("cusId");
                    //关联客户名称
                    String cusNameRel = (String) comMgr.get("mrgName");
                    //客户大类
                    String cusCatalog = (String) comMgr.get("cusCatalog");
                    //高管类别
                    String mrgType = (String) comMgr.get("mrgType");

                    if (CmisCusConstants.STD_CROP_MRG_TYPE_201200.equals(mrgType)){
                        //将实际控制人客户编号放入ctrlCusList里
                        ctrlCusList.add(cusIdRel);
                    }

                    comCusRelListRespDto.setCusIdRel(cusIdRel);
                    comCusRelListRespDto.setCusNameRel(cusNameRel);
                    comCusRelListRespDto.setCusTypeRel(cusCatalog);
                    //从dictMap里获取对应的码值
                    comCusRelListRespDto.setRelType(dictMap.get(mrgType));
                    cusRelList.add(comCusRelListRespDto);
                }
            }

            if (!ctrlCusList.isEmpty()){
                //实际控制人不为空，查询实际控制人配偶、父母、子女信息
                List<Map<String, String>> cusSocialList = cusIndivSocialService.selectSocialInfoByCusIdRelList(ctrlCusList);

                if (cusSocialList!=null && cusSocialList.size()>0){

                    for (Map<String, String> cusSocial : cusSocialList) {
                        //客户id
                        String cusIdRel = (String) cusSocial.get("cusId");
                        //与客户关系
                        String indivCusRel = (String) cusSocial.get("indivCusRel");
                        //客户姓名
                        String name = (String) cusSocial.get("name");

                        CmisCus0003ComCusRelListRespDto comCusRelListRespDto = new CmisCus0003ComCusRelListRespDto();
                        comCusRelListRespDto.setCusIdRel(cusIdRel);
                        comCusRelListRespDto.setCusNameRel(name);
                        comCusRelListRespDto.setCusTypeRel(CmisCusConstants.STD_ZB_CUS_CATALOG_1);//客户大类默认对私客户

                        if (CmisCusConstants.STD_ZB_INDIV_CUS_1.equals(indivCusRel)){
                            comCusRelListRespDto.setRelType("实际控制人配偶");
                        }else if (CmisCusConstants.STD_ZB_INDIV_CUS_2.equals(indivCusRel)){
                            comCusRelListRespDto.setRelType("实际控制人父母");
                        }else if (CmisCusConstants.STD_ZB_INDIV_CUS_3.equals(indivCusRel)){
                            comCusRelListRespDto.setRelType("实际控制人子女");
                        }

                        cusRelList.add(comCusRelListRespDto);
                    }
                }
            }

            //查询对外投资信息表里关联客户的信息
//            List<Map<String, String>> relInvestList = cusPubRelInvestService.queryCusInfoByCusId(cusId);
//
//            for (Map<String, String> relInvest : relInvestList) {
//                CmisCus0003ComCusRelListRespDto comCusRelListRespDto = new CmisCus0003ComCusRelListRespDto();
//                comCusRelListRespDto.setCusIdRel((String) relInvest.get("cusIdRel"));
//                comCusRelListRespDto.setCusNameRel((String) relInvest.get("cusName"));
//                comCusRelListRespDto.setCusTypeRel((String) relInvest.get("cusCatalog"));
//                comCusRelListRespDto.setRelType("股东");
//                cusRelList.add(comCusRelListRespDto);
//            }

            //查询客户资本构成信息表里关联法人的出资人信息
            List<Map<String, String>> corpApitalList = cusCorpApitalService.queryCusInfoByCusIdRel(cusId);

            if (corpApitalList!=null && corpApitalList.size()>0){
                for (Map<String, String> corpApital : corpApitalList) {
                    CmisCus0003ComCusRelListRespDto comCusRelListRespDto = new CmisCus0003ComCusRelListRespDto();

                    String cusIdRel = (String) corpApital.get("cusId");
                    comCusRelListRespDto.setCusIdRel(cusIdRel);

                    //判断出资人是否为个人客户，如果不是，则不作为关联客户返回
                    CusBase cusBase = cusBaseService.selectByPrimaryKey(cusIdRel);
                    if(cusBase!=null  && !CmisBizConstants.STD_ZB_CUS_CATALOG_1.equals(cusBase.getCusCatalog())){
                        continue;
                    }

                    comCusRelListRespDto.setCusNameRel((String) corpApital.get("invtName"));
                    comCusRelListRespDto.setCusTypeRel((String) corpApital.get("cusCatalog"));
                    comCusRelListRespDto.setRelType("股东");
                    cusRelList.add(comCusRelListRespDto);
                }
            }

            //key:cusId，value：CmisCus0003ComCusRelListRespDto对象
            Map<String,CmisCus0003ComCusRelListRespDto> respDtoMap = new HashMap<>();

            for (CmisCus0003ComCusRelListRespDto comCusRelListRespDto : cusRelList) {

                String cus_id = comCusRelListRespDto.getCusIdRel();
                if (respDtoMap.containsKey(cus_id)){
                    CmisCus0003ComCusRelListRespDto comCusRelDto = respDtoMap.get(cus_id);
                    String relType = comCusRelDto.getRelType();
                    String relType2 = comCusRelListRespDto.getRelType();

                    //如果一个客户有多个客户类型，则合并为一条记录，关联类型用“/”连接
                    if (!relType.contains(relType2)){
                        relType = relType+"/"+relType2;
                        comCusRelDto.setRelType(relType);
                        respDtoMap.put(cus_id,comCusRelDto);
                    }
                }else{
                    respDtoMap.put(cus_id,comCusRelListRespDto);
                }
            }

            //清空cusRelList，将respDtoMap中的value放入cusRelList里

            cusRelList.clear();

            Set<Map.Entry<String, CmisCus0003ComCusRelListRespDto>> entries = respDtoMap.entrySet();

            for (Map.Entry<String, CmisCus0003ComCusRelListRespDto> entry : entries) {
                cusRelList.add(entry.getValue());
            }

            respDto.setComCusRelList(cusRelList);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            logger.error(DscmsEnum.TRADE_CODE_CMISCUS0003.key+"报错：",e);
            respDto.setErrorCode(e.getCode());
            respDto.setErrorMsg(e.getMsg());
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0003.key, DscmsEnum.TRADE_CODE_CMISCUS0003.value);
        return respDto;
    }
}