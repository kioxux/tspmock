package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivUnit
 * @类描述: cus_indiv_unit数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-05-03 21:19:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusIndivUnitDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 是否当前单位 STD_ZB_YES_NO **/
	private String isCurtUnit;
	
	/** 职业 STD_ZB_OCC **/
	private String occ;
	
	/** 自由职业说明 **/
	private String occDesc;
	
	/** 经营企业名称/工作单位 **/
	private String unitName;
	
	/** 所在部门 **/
	private String ubietyDept;
	
	/** 单位性质 **/
	private String indivComTyp;
	
	/** 单位所属行业 **/
	private String indivComTrade;
	
	/** 职务 STD_ZB_JOB_TTL **/
	private String jobTtl;
	
	/** 职称 STD_ZB_TITLE **/
	private String indivCrtfctn;
	
	/** 年收入(元) **/
	private java.math.BigDecimal yScore;
	
	/** 单位地址 **/
	private String unitAddr;
	
	/** 单位地址街道/路 **/
	private String unitStreet;
	
	/** 单位邮政编码 **/
	private String unitZipCode;
	
	/** 单位电话 **/
	private String unitPhn;
	
	/** 单位传真 **/
	private String unitFax;
	
	/** 单位联系人 **/
	private String unitCntName;
	
	/** 参加工作年份 **/
	private String workDate;
	
	/** 本单位参加工作日期 **/
	private String unitDate;
	
	/** 本单位工作结束日期 **/
	private String unitEndDate;
	
	/** 本行业参加工作日期  **/
	private String tradeDate;
	
	/** 备注 **/
	private String remark;
	
	/** 个人工作履历标识 **/
	private String prsnWorkId;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 雇佣状态 **/
	private String employeeStatus;
	
	/** 营业执照号码 **/
	private String blicNo;
	
	/** 经营状况 **/
	private String operStatus;
	
	/** 经营企业统一社会信用代码 **/
	private String unifyCreditCode;
	
	/** 收入币种 **/
	private String earningCurType;
	
	/** 家庭年收入(元) **/
	private java.math.BigDecimal familyYScore;
	
	/** 个人年收入 **/
	private String indivYearn;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;
	
	/** 创建日期 **/
	private java.util.Date createTime;
	
	/** 更新日期 **/
	private java.util.Date updateTime;
	
	/** 保存状态 **/
	private String saveStatus;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param isCurtUnit
	 */
	public void setIsCurtUnit(String isCurtUnit) {
		this.isCurtUnit = isCurtUnit == null ? null : isCurtUnit.trim();
	}
	
    /**
     * @return IsCurtUnit
     */	
	public String getIsCurtUnit() {
		return this.isCurtUnit;
	}
	
	/**
	 * @param occ
	 */
	public void setOcc(String occ) {
		this.occ = occ == null ? null : occ.trim();
	}
	
    /**
     * @return Occ
     */	
	public String getOcc() {
		return this.occ;
	}
	
	/**
	 * @param occDesc
	 */
	public void setOccDesc(String occDesc) {
		this.occDesc = occDesc == null ? null : occDesc.trim();
	}
	
    /**
     * @return OccDesc
     */	
	public String getOccDesc() {
		return this.occDesc;
	}
	
	/**
	 * @param unitName
	 */
	public void setUnitName(String unitName) {
		this.unitName = unitName == null ? null : unitName.trim();
	}
	
    /**
     * @return UnitName
     */	
	public String getUnitName() {
		return this.unitName;
	}
	
	/**
	 * @param ubietyDept
	 */
	public void setUbietyDept(String ubietyDept) {
		this.ubietyDept = ubietyDept == null ? null : ubietyDept.trim();
	}
	
    /**
     * @return UbietyDept
     */	
	public String getUbietyDept() {
		return this.ubietyDept;
	}
	
	/**
	 * @param indivComTyp
	 */
	public void setIndivComTyp(String indivComTyp) {
		this.indivComTyp = indivComTyp == null ? null : indivComTyp.trim();
	}
	
    /**
     * @return IndivComTyp
     */	
	public String getIndivComTyp() {
		return this.indivComTyp;
	}
	
	/**
	 * @param indivComTrade
	 */
	public void setIndivComTrade(String indivComTrade) {
		this.indivComTrade = indivComTrade == null ? null : indivComTrade.trim();
	}
	
    /**
     * @return IndivComTrade
     */	
	public String getIndivComTrade() {
		return this.indivComTrade;
	}
	
	/**
	 * @param jobTtl
	 */
	public void setJobTtl(String jobTtl) {
		this.jobTtl = jobTtl == null ? null : jobTtl.trim();
	}
	
    /**
     * @return JobTtl
     */	
	public String getJobTtl() {
		return this.jobTtl;
	}
	
	/**
	 * @param indivCrtfctn
	 */
	public void setIndivCrtfctn(String indivCrtfctn) {
		this.indivCrtfctn = indivCrtfctn == null ? null : indivCrtfctn.trim();
	}
	
    /**
     * @return IndivCrtfctn
     */	
	public String getIndivCrtfctn() {
		return this.indivCrtfctn;
	}
	
	/**
	 * @param yScore
	 */
	public void setYScore(java.math.BigDecimal yScore) {
		this.yScore = yScore;
	}
	
    /**
     * @return YScore
     */	
	public java.math.BigDecimal getYScore() {
		return this.yScore;
	}
	
	/**
	 * @param unitAddr
	 */
	public void setUnitAddr(String unitAddr) {
		this.unitAddr = unitAddr == null ? null : unitAddr.trim();
	}
	
    /**
     * @return UnitAddr
     */	
	public String getUnitAddr() {
		return this.unitAddr;
	}
	
	/**
	 * @param unitStreet
	 */
	public void setUnitStreet(String unitStreet) {
		this.unitStreet = unitStreet == null ? null : unitStreet.trim();
	}
	
    /**
     * @return UnitStreet
     */	
	public String getUnitStreet() {
		return this.unitStreet;
	}
	
	/**
	 * @param unitZipCode
	 */
	public void setUnitZipCode(String unitZipCode) {
		this.unitZipCode = unitZipCode == null ? null : unitZipCode.trim();
	}
	
    /**
     * @return UnitZipCode
     */	
	public String getUnitZipCode() {
		return this.unitZipCode;
	}
	
	/**
	 * @param unitPhn
	 */
	public void setUnitPhn(String unitPhn) {
		this.unitPhn = unitPhn == null ? null : unitPhn.trim();
	}
	
    /**
     * @return UnitPhn
     */	
	public String getUnitPhn() {
		return this.unitPhn;
	}
	
	/**
	 * @param unitFax
	 */
	public void setUnitFax(String unitFax) {
		this.unitFax = unitFax == null ? null : unitFax.trim();
	}
	
    /**
     * @return UnitFax
     */	
	public String getUnitFax() {
		return this.unitFax;
	}
	
	/**
	 * @param unitCntName
	 */
	public void setUnitCntName(String unitCntName) {
		this.unitCntName = unitCntName == null ? null : unitCntName.trim();
	}
	
    /**
     * @return UnitCntName
     */	
	public String getUnitCntName() {
		return this.unitCntName;
	}
	
	/**
	 * @param workDate
	 */
	public void setWorkDate(String workDate) {
		this.workDate = workDate == null ? null : workDate.trim();
	}
	
    /**
     * @return WorkDate
     */	
	public String getWorkDate() {
		return this.workDate;
	}
	
	/**
	 * @param unitDate
	 */
	public void setUnitDate(String unitDate) {
		this.unitDate = unitDate == null ? null : unitDate.trim();
	}
	
    /**
     * @return UnitDate
     */	
	public String getUnitDate() {
		return this.unitDate;
	}
	
	/**
	 * @param unitEndDate
	 */
	public void setUnitEndDate(String unitEndDate) {
		this.unitEndDate = unitEndDate == null ? null : unitEndDate.trim();
	}
	
    /**
     * @return UnitEndDate
     */	
	public String getUnitEndDate() {
		return this.unitEndDate;
	}
	
	/**
	 * @param tradeDate
	 */
	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate == null ? null : tradeDate.trim();
	}
	
    /**
     * @return TradeDate
     */	
	public String getTradeDate() {
		return this.tradeDate;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}
	
    /**
     * @return Remark
     */	
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param prsnWorkId
	 */
	public void setPrsnWorkId(String prsnWorkId) {
		this.prsnWorkId = prsnWorkId == null ? null : prsnWorkId.trim();
	}
	
    /**
     * @return PrsnWorkId
     */	
	public String getPrsnWorkId() {
		return this.prsnWorkId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param employeeStatus
	 */
	public void setEmployeeStatus(String employeeStatus) {
		this.employeeStatus = employeeStatus == null ? null : employeeStatus.trim();
	}
	
    /**
     * @return EmployeeStatus
     */	
	public String getEmployeeStatus() {
		return this.employeeStatus;
	}
	
	/**
	 * @param blicNo
	 */
	public void setBlicNo(String blicNo) {
		this.blicNo = blicNo == null ? null : blicNo.trim();
	}
	
    /**
     * @return BlicNo
     */	
	public String getBlicNo() {
		return this.blicNo;
	}
	
	/**
	 * @param operStatus
	 */
	public void setOperStatus(String operStatus) {
		this.operStatus = operStatus == null ? null : operStatus.trim();
	}
	
    /**
     * @return OperStatus
     */	
	public String getOperStatus() {
		return this.operStatus;
	}
	
	/**
	 * @param unifyCreditCode
	 */
	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode == null ? null : unifyCreditCode.trim();
	}
	
    /**
     * @return UnifyCreditCode
     */	
	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}
	
	/**
	 * @param earningCurType
	 */
	public void setEarningCurType(String earningCurType) {
		this.earningCurType = earningCurType == null ? null : earningCurType.trim();
	}
	
    /**
     * @return EarningCurType
     */	
	public String getEarningCurType() {
		return this.earningCurType;
	}
	
	/**
	 * @param familyYScore
	 */
	public void setFamilyYScore(java.math.BigDecimal familyYScore) {
		this.familyYScore = familyYScore;
	}
	
    /**
     * @return FamilyYScore
     */	
	public java.math.BigDecimal getFamilyYScore() {
		return this.familyYScore;
	}
	
	/**
	 * @param indivYearn
	 */
	public void setIndivYearn(String indivYearn) {
		this.indivYearn = indivYearn == null ? null : indivYearn.trim();
	}
	
    /**
     * @return IndivYearn
     */	
	public String getIndivYearn() {
		return this.indivYearn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param saveStatus
	 */
	public void setSaveStatus(String saveStatus) {
		this.saveStatus = saveStatus == null ? null : saveStatus.trim();
	}
	
    /**
     * @return SaveStatus
     */	
	public String getSaveStatus() {
		return this.saveStatus;
	}


}