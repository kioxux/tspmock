/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusLstYpqfWhite;
import cn.com.yusys.yusp.repository.mapper.CusLstYpqfWhiteMapper;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYpqfWhiteService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 18:19:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusLstYpqfWhiteService {

    @Autowired
    private CusLstYpqfWhiteMapper cusLstYpqfWhiteMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusLstYpqfWhite selectByPrimaryKey(String cusId) {
        return cusLstYpqfWhiteMapper.selectByPrimaryKey(cusId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusLstYpqfWhite> selectAll(QueryModel model) {
        List<CusLstYpqfWhite> records = (List<CusLstYpqfWhite>) cusLstYpqfWhiteMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusLstYpqfWhite> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusLstYpqfWhite> list = cusLstYpqfWhiteMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusLstYpqfWhite record) {
        return cusLstYpqfWhiteMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusLstYpqfWhite record) {
        return cusLstYpqfWhiteMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusLstYpqfWhite record) {
        return cusLstYpqfWhiteMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusLstYpqfWhite record) {
        return cusLstYpqfWhiteMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String cusId) {
        return cusLstYpqfWhiteMapper.deleteByPrimaryKey(cusId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusLstYpqfWhiteMapper.deleteByIds(ids);
    }
}
