/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorpBond
 * @类描述: cus_corp_bond数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-26 00:04:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_corp_bond")
public class CusCorpBond extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = false, length = 30)
	private String cusId;
	
	/** 债券名称 **/
	@Column(name = "BOND_NAME", unique = false, nullable = true, length = 100)
	private String bondName;
	
	/** 债券类型 STD_ZB_BOND_TYP **/
	@Column(name = "BOND_TYP", unique = false, nullable = true, length = 50)
	private String bondTyp;
	
	/** 发行时间 **/
	@Column(name = "BOND_PUB_DT", unique = false, nullable = true, length = 10)
	private String bondPubDt;
	
	/** 债券期限 **/
	@Column(name = "BOND_TRM", unique = false, nullable = true, length = 10)
	private Integer bondTrm;
	
	/** 债券币种 STD_ZB_CUR_TYP **/
	@Column(name = "BOND_CUR_TYP", unique = false, nullable = true, length = 5)
	private String bondCurTyp;
	
	/** 总金额(元) **/
	@Column(name = "BOND_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bondAmt;
	
	/** 年利率(%) **/
	@Column(name = "BOND_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bondRate;
	
	/** 是否上市 STD_ZB_YES_NO **/
	@Column(name = "BOND_MRK_FLG", unique = false, nullable = true, length = 5)
	private String bondMrkFlg;
	
	/** 上市地 STD_ZB_LIST_CITY **/
	@Column(name = "BOND_MRK_PLACE", unique = false, nullable = true, length = 5)
	private String bondMrkPlace;
	
	/** 交易所名称 **/
	@Column(name = "BOND_MRK_BRS", unique = false, nullable = true, length = 100)
	private String bondMrkBrs;
	
	/** 债券等级 **/
	@Column(name = "BOND_CRT_INFO", unique = false, nullable = true, length = 60)
	private String bondCrtInfo;
	
	/** 评级机构 **/
	@Column(name = "BOND_CRT_ORG", unique = false, nullable = true, length = 100)
	private String bondCrtOrg;
	
	/** 债券评估价 **/
	@Column(name = "BOND_EVA_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bondEvaAmt;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 500)
	private String remark;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param bondName
	 */
	public void setBondName(String bondName) {
		this.bondName = bondName;
	}
	
    /**
     * @return bondName
     */
	public String getBondName() {
		return this.bondName;
	}
	
	/**
	 * @param bondTyp
	 */
	public void setBondTyp(String bondTyp) {
		this.bondTyp = bondTyp;
	}
	
    /**
     * @return bondTyp
     */
	public String getBondTyp() {
		return this.bondTyp;
	}
	
	/**
	 * @param bondPubDt
	 */
	public void setBondPubDt(String bondPubDt) {
		this.bondPubDt = bondPubDt;
	}
	
    /**
     * @return bondPubDt
     */
	public String getBondPubDt() {
		return this.bondPubDt;
	}
	
	/**
	 * @param bondTrm
	 */
	public void setBondTrm(Integer bondTrm) {
		this.bondTrm = bondTrm;
	}
	
    /**
     * @return bondTrm
     */
	public Integer getBondTrm() {
		return this.bondTrm;
	}
	
	/**
	 * @param bondCurTyp
	 */
	public void setBondCurTyp(String bondCurTyp) {
		this.bondCurTyp = bondCurTyp;
	}
	
    /**
     * @return bondCurTyp
     */
	public String getBondCurTyp() {
		return this.bondCurTyp;
	}
	
	/**
	 * @param bondAmt
	 */
	public void setBondAmt(java.math.BigDecimal bondAmt) {
		this.bondAmt = bondAmt;
	}
	
    /**
     * @return bondAmt
     */
	public java.math.BigDecimal getBondAmt() {
		return this.bondAmt;
	}
	
	/**
	 * @param bondRate
	 */
	public void setBondRate(java.math.BigDecimal bondRate) {
		this.bondRate = bondRate;
	}
	
    /**
     * @return bondRate
     */
	public java.math.BigDecimal getBondRate() {
		return this.bondRate;
	}
	
	/**
	 * @param bondMrkFlg
	 */
	public void setBondMrkFlg(String bondMrkFlg) {
		this.bondMrkFlg = bondMrkFlg;
	}
	
    /**
     * @return bondMrkFlg
     */
	public String getBondMrkFlg() {
		return this.bondMrkFlg;
	}
	
	/**
	 * @param bondMrkPlace
	 */
	public void setBondMrkPlace(String bondMrkPlace) {
		this.bondMrkPlace = bondMrkPlace;
	}
	
    /**
     * @return bondMrkPlace
     */
	public String getBondMrkPlace() {
		return this.bondMrkPlace;
	}
	
	/**
	 * @param bondMrkBrs
	 */
	public void setBondMrkBrs(String bondMrkBrs) {
		this.bondMrkBrs = bondMrkBrs;
	}
	
    /**
     * @return bondMrkBrs
     */
	public String getBondMrkBrs() {
		return this.bondMrkBrs;
	}
	
	/**
	 * @param bondCrtInfo
	 */
	public void setBondCrtInfo(String bondCrtInfo) {
		this.bondCrtInfo = bondCrtInfo;
	}
	
    /**
     * @return bondCrtInfo
     */
	public String getBondCrtInfo() {
		return this.bondCrtInfo;
	}
	
	/**
	 * @param bondCrtOrg
	 */
	public void setBondCrtOrg(String bondCrtOrg) {
		this.bondCrtOrg = bondCrtOrg;
	}
	
    /**
     * @return bondCrtOrg
     */
	public String getBondCrtOrg() {
		return this.bondCrtOrg;
	}
	
	/**
	 * @param bondEvaAmt
	 */
	public void setBondEvaAmt(java.math.BigDecimal bondEvaAmt) {
		this.bondEvaAmt = bondEvaAmt;
	}
	
    /**
     * @return bondEvaAmt
     */
	public java.math.BigDecimal getBondEvaAmt() {
		return this.bondEvaAmt;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}