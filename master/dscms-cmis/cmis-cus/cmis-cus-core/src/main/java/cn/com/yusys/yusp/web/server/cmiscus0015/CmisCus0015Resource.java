package cn.com.yusys.yusp.web.server.cmiscus0015;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusIndivUnit;
import cn.com.yusys.yusp.dto.server.cmiscus0015.req.CmisCus0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0015.resp.CmisCus0015RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusIndivUnitService;
import cn.com.yusys.yusp.web.server.cmiscus0007.CmisCus0007Resource;
import com.alibaba.fastjson.JSON;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 更新客户单位信息表
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0015:更新客户单位信息表")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0015Resource {

    private static final Logger logger = LoggerFactory.getLogger(CmisCus0007Resource.class);

    @Autowired
    private CusIndivUnitService cusIndivUnitService;

    /**
     * 交易码：cmiscus0015
     * 交易描述：更新客户单位信息表
     *
     * @param cmisCus0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("更新客户单位信息表")
    @PostMapping("/cmiscus0015")
    protected @ResponseBody
    ResultDto<CmisCus0015RespDto> cmiscus0015(@Validated @RequestBody CmisCus0015ReqDto cmisCus0015ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0015.key, DscmsEnum.TRADE_CODE_CMISCUS0015.value, JSON.toJSONString(cmisCus0015ReqDto));
        CmisCus0015RespDto cmisCus0015RespDto = new CmisCus0015RespDto();// 响应Dto:查询优农贷名单信息
        ResultDto<CmisCus0015RespDto> cmisCus0015ResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0015.key, DscmsEnum.TRADE_CODE_CMISCUS0015.value, JSON.toJSONString(cmisCus0015ReqDto));
            String cusId = cmisCus0015ReqDto.getCusId();
            String indivComTrade = cmisCus0015ReqDto.getIndivComTrade();
            if (StringUtils.isNotEmpty(cusId) && StringUtils.isNotEmpty(indivComTrade)) {
                CusIndivUnit cusIndivUnit = new CusIndivUnit();
                cusIndivUnit.setCusId(cusId);
                cusIndivUnit.setIndivComTrade(indivComTrade);
                int result = cusIndivUnitService.updateCusIndivUnitByCusId(cusIndivUnit);
                cmisCus0015RespDto.setResult(result);
            }
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0015.key, DscmsEnum.TRADE_CODE_CMISCUS0015.value, JSON.toJSONString(cmisCus0015RespDto));
            // 封装cmisCus0015ResultDto中正确的返回码和返回信息
            cmisCus0015ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0015ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0015.key, DscmsEnum.TRADE_CODE_CMISCUS0015.value, e.getMessage());
            // 封装cmisCus0015ResultDto中异常返回码和返回信息
            cmisCus0015ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0015ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0015RespDto到cmisCus0007ResultDto中
        cmisCus0015ResultDto.setData(cmisCus0015RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0015.key, DscmsEnum.TRADE_CODE_CMISCUS0015.value, JSON.toJSONString(cmisCus0015ResultDto));
        return cmisCus0015ResultDto;
    }

}
