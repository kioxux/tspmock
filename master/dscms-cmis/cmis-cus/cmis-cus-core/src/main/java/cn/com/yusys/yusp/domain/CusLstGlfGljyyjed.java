/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstGlfGljyyjed
 * @类描述: cus_lst_glf_gljyyjed数据实体类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 15:16:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_glf_gljyyjed")
public class CusLstGlfGljyyjed extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 关联方名称 **/
	@Column(name = "RELATED_PARTY_NAME", unique = false, nullable = true, length = 20)
	private String relatedPartyName;
	
	/** 关联方客户编号 **/
	@Column(name = "RELATED_PARTY_CUS_ID", unique = false, nullable = true, length = 20)
	private String relatedPartyCusId;
	
	/** 关联方证件类型 **/
	@Column(name = "RELATED_PARTY_CERT_TYPE", unique = false, nullable = true, length = 20)
	private String relatedPartyCertType;
	
	/** 关联方证件号码 **/
	@Column(name = "RELATED_PARTY_CERT_NO", unique = false, nullable = true, length = 20)
	private String relatedPartyCertNo;
	
	/** 关联方预计额度 **/
	@Column(name = "RELATED_PARTY_FORE_LMT", unique = false, nullable = true, length = 20)
	private String relatedPartyForeLmt;
	
	/** 关联方类型 **/
	@Column(name = "RELATED_PARTY_TYPE", unique = false, nullable = true, length = 20)
	private String relatedPartyType;
	
	/** 归属组别 **/
	@Column(name = "BELONG_GROUP", unique = false, nullable = true, length = 20)
	private String belongGroup;
	
	/** 组别预计总额度 **/
	@Column(name = "GROUP_FORE_TOTL_AMT", unique = false, nullable = true, length = 20)
	private String groupForeTotlAmt;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 10)
	private String updDate;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param relatedPartyName
	 */
	public void setRelatedPartyName(String relatedPartyName) {
		this.relatedPartyName = relatedPartyName;
	}
	
    /**
     * @return relatedPartyName
     */
	public String getRelatedPartyName() {
		return this.relatedPartyName;
	}
	
	/**
	 * @param relatedPartyCusId
	 */
	public void setRelatedPartyCusId(String relatedPartyCusId) {
		this.relatedPartyCusId = relatedPartyCusId;
	}
	
    /**
     * @return relatedPartyCusId
     */
	public String getRelatedPartyCusId() {
		return this.relatedPartyCusId;
	}
	
	/**
	 * @param relatedPartyCertType
	 */
	public void setRelatedPartyCertType(String relatedPartyCertType) {
		this.relatedPartyCertType = relatedPartyCertType;
	}
	
    /**
     * @return relatedPartyCertType
     */
	public String getRelatedPartyCertType() {
		return this.relatedPartyCertType;
	}
	
	/**
	 * @param relatedPartyCertNo
	 */
	public void setRelatedPartyCertNo(String relatedPartyCertNo) {
		this.relatedPartyCertNo = relatedPartyCertNo;
	}
	
    /**
     * @return relatedPartyCertNo
     */
	public String getRelatedPartyCertNo() {
		return this.relatedPartyCertNo;
	}
	
	/**
	 * @param relatedPartyForeLmt
	 */
	public void setRelatedPartyForeLmt(String relatedPartyForeLmt) {
		this.relatedPartyForeLmt = relatedPartyForeLmt;
	}
	
    /**
     * @return relatedPartyForeLmt
     */
	public String getRelatedPartyForeLmt() {
		return this.relatedPartyForeLmt;
	}
	
	/**
	 * @param relatedPartyType
	 */
	public void setRelatedPartyType(String relatedPartyType) {
		this.relatedPartyType = relatedPartyType;
	}
	
    /**
     * @return relatedPartyType
     */
	public String getRelatedPartyType() {
		return this.relatedPartyType;
	}
	
	/**
	 * @param belongGroup
	 */
	public void setBelongGroup(String belongGroup) {
		this.belongGroup = belongGroup;
	}
	
    /**
     * @return belongGroup
     */
	public String getBelongGroup() {
		return this.belongGroup;
	}
	
	/**
	 * @param groupForeTotlAmt
	 */
	public void setGroupForeTotlAmt(String groupForeTotlAmt) {
		this.groupForeTotlAmt = groupForeTotlAmt;
	}
	
    /**
     * @return groupForeTotlAmt
     */
	public String getGroupForeTotlAmt() {
		return this.groupForeTotlAmt;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}


}