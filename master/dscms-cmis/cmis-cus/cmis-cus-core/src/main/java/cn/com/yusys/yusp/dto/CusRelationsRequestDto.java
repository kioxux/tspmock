package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * 客户关系网
 */
public class CusRelationsRequestDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 层级 **/
    private String cusLevel;
    /** 客户编号 **/
    private String cusId;
    /** 企业关系 **/
    private String comRelations;
    /** 亲友关系 **/
    private String famRelations;
    /** 担保关系 **/
    private String assureRelations;

    public String getCusLevel() {
        return cusLevel;
    }

    public void setCusLevel(String cusLevel) {
        this.cusLevel = cusLevel;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getComRelations() {
        return comRelations;
    }

    public void setComRelations(String comRelations) {
        this.comRelations = comRelations;
    }

    public String getFamRelations() {
        return famRelations;
    }

    public void setFamRelations(String famRelations) {
        this.famRelations = famRelations;
    }

    public String getAssureRelations() {
        return assureRelations;
    }

    public void setAssureRelations(String assureRelations) {
        this.assureRelations = assureRelations;
    }
}
