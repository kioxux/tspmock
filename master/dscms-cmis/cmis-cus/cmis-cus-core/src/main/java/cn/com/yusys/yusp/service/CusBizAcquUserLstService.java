/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.domain.CusBizAssignApp;
import cn.com.yusys.yusp.domain.CusPrivCorreRel;
import cn.com.yusys.yusp.dto.CusBizAcquUserlstDto;
import cn.com.yusys.yusp.repository.mapper.CusBizAssignAppMapper;
import cn.com.yusys.yusp.repository.mapper.CusPrivCorreRelMapper;
import org.apache.commons.codec.net.QCodec;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusBizAcquUserLst;
import cn.com.yusys.yusp.repository.mapper.CusBizAcquUserLstMapper;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBizAcquUserLstService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 18301
 * @创建时间: 2021-05-05 09:33:12
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusBizAcquUserLstService {

    @Autowired
    private CusBizAcquUserLstMapper cusBizAcquUserLstMapper;
    @Autowired
    private CusPrivCorreRelMapper cusPrivCorreRelMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CusBizAcquUserLst selectByPrimaryKey(String pkId) {
        return cusBizAcquUserLstMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<CusBizAcquUserLst> selectAll(QueryModel model) {
        List<CusBizAcquUserLst> records = (List<CusBizAcquUserLst>) cusBizAcquUserLstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CusBizAcquUserLst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBizAcquUserLst> list = cusBizAcquUserLstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(CusBizAcquUserLst record) {
        return cusBizAcquUserLstMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CusBizAcquUserLst record) {
        return cusBizAcquUserLstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(CusBizAcquUserLst record) {
        return cusBizAcquUserLstMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CusBizAcquUserLst record) {
        return cusBizAcquUserLstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusBizAcquUserLstMapper.deleteByPrimaryKey(pkId);
    }

    @Resource
    private CusBizAssignAppMapper cusBizAssignAppMapper;

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusBizAcquUserLstMapper.deleteByIds(ids);
    }

    /***
     * @param cusBizAcquUserlstDto
     * @return int
     * @author tangxun
     * @date 2021/4/26 4:49 下午
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertApp(CusBizAcquUserlstDto cusBizAcquUserlstDto) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("cusId",cusBizAcquUserlstDto.getCusBizAssignApp().getCusId());
        queryModel.addCondition("managerId",cusBizAcquUserlstDto.getCusBizAssignApp().getManagerId());
        int result = cusBizAssignAppMapper.insertSelective(cusBizAcquUserlstDto.getCusBizAssignApp());
        List<CusBizAcquUserLst> list = cusBizAcquUserlstDto.getList();
        for (CusBizAcquUserLst cusBizAcquUserLst : list) {
            queryModel.addCondition("bizRightsRecipientUser",cusBizAcquUserLst.getBizRightsRecipientUser());
            int cnt = cusBizAssignAppMapper.validateAssignCnt(queryModel);
            if(cnt > 0){
                throw BizException.error(null, null, "用户【" + cusBizAcquUserLst.getBizRightsRecipientUser() + "】已拥有该客户业务权，禁止重复分配！");
            }
            CusPrivCorreRel record = new CusPrivCorreRel();
            record.setCusId(cusBizAcquUserlstDto.getCusBizAssignApp().getCusId());
            record.setShareUser(cusBizAcquUserLst.getBizRightsRecipientUser());
            record.setStatus("01");
            cusBizAcquUserLstMapper.insertSelective(cusBizAcquUserLst);
            cusPrivCorreRelMapper.insertSelective(record);
        }
        return result;
    }

    /***
     * @param cusBizAcquUserlstDto
     * @return int
     * @author tangxun
     * @date 2021/4/26 4:49 下午
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int updateApp(CusBizAcquUserlstDto cusBizAcquUserlstDto) {
        int result = cusBizAssignAppMapper.updateByPrimaryKey(cusBizAcquUserlstDto.getCusBizAssignApp());
        List<CusBizAcquUserLst> list = cusBizAcquUserlstDto.getList();
        for (CusBizAcquUserLst cusBizAcquUserLst : list) {
            if (StringUtils.isNotBlank(cusBizAcquUserLst.getPkId())) {
                cusBizAcquUserLstMapper.updateByPrimaryKey(cusBizAcquUserLst);
            } else {
                cusBizAcquUserLstMapper.insertSelective(cusBizAcquUserLst);
            }
        }
        return result;
    }


}
