package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

/**
 * 问题授信客户名单导入导出Vo
 *
 * @author xuchi
 * @since 2021/6/7
 **/
@ExcelCsv(namePrefix = "问题授信名单导入模板", fileType = ExcelCsv.ExportFileType.XLS)
public class CusLstWtsxExportVo {



    /**
     * 客户类型
     **/
    @ExcelField(title = "客户类型", viewLength = 20)
    private String cusType;

    /**
     * 客户编号
     **/
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /**
     * 客户名称
     **/
    @ExcelField(title = "客户名称", viewLength = 80)
    private String cusName;


    /**
     * 导入原因
     **/
    @ExcelField(title = "导入原因", viewLength = 80)
    private String importResn;


    /**
     * 管户客户经理
     **/
    @ExcelField(title = "管户客户经理", viewLength = 20)
    private String managerId;

    /**
     * 所属机构
     **/
    @ExcelField(title = "所属机构", viewLength = 20)
    private String belgOrg;

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getImportResn() {
        return importResn;
    }

    public void setImportResn(String importResn) {
        this.importResn = importResn;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getBelgOrg() {
        return belgOrg;
    }

    public void setBelgOrg(String belgOrg) {
        this.belgOrg = belgOrg;
    }
}