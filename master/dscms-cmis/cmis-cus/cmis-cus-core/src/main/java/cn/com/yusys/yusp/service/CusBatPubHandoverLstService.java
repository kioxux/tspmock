/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.*;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.CusBase;
import cn.com.yusys.yusp.domain.CusLstGlf;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.repository.mapper.CusBaseMapper;
import cn.com.yusys.yusp.repository.mapper.CusLstGlfMapper;
import cn.com.yusys.yusp.vo.CusBatPubHandoverLstVo;
import cn.com.yusys.yusp.vo.CusLstGlfVo;
import cn.com.yusys.yusp.vo.CusLstJjycExportVo;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusBatPubHandoverLst;
import cn.com.yusys.yusp.repository.mapper.CusBatPubHandoverLstMapper;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBatPubHandoverLstService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-05-05 10:09:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CusBatPubHandoverLstService {

    @Autowired
    private CusBatPubHandoverLstMapper cusBatPubHandoverLstMapper;

    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;

    @Resource
    private AdminSmUserService adminSmUserService;

    @Autowired
    private CusBaseMapper cusBaseMapper;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusBatPubHandoverLst selectByPrimaryKey(String pkId) {
        return cusBatPubHandoverLstMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusBatPubHandoverLst> selectAll(QueryModel model) {
        List<CusBatPubHandoverLst> records = (List<CusBatPubHandoverLst>) cusBatPubHandoverLstMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusBatPubHandoverLst> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusBatPubHandoverLst> list = cusBatPubHandoverLstMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusBatPubHandoverLst record) {
        return cusBatPubHandoverLstMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusBatPubHandoverLst record) {
        return cusBatPubHandoverLstMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusBatPubHandoverLst record) {
        return cusBatPubHandoverLstMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusBatPubHandoverLst record) {
        return cusBatPubHandoverLstMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusBatPubHandoverLstMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusBatPubHandoverLstMapper.deleteByIds(ids);
    }


    /**
     * 异步导出模板
     *
     * @return 导出进度信息
     */
    public ProgressDto asyncExportTemplate() {
        // 数据检索规则——模板时返回空数据
        DataAcquisition dataAcquisition = new DataAcquisition() {
            @Override
            public Collection<?> getData(int i, int i1, Object object) {
                return new ArrayList<>();
            }
        };
        ExportContext exportContext = ExportContext.of(CusBatPubHandoverLstVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, null);
        return ExcelUtils.asyncExport(exportContext);
    }

    /**
     * 批量插入
     *
     * @param perCustInfoList 解析出的Excel数据
     * @return 本次批量插入数据量
     */
    @Transactional(rollbackFor = Exception.class)
    public int insertInBatch(List<Object> perCustInfoList, String serno) {
            User userInfo = SessionUtils.getUserInformation();
            List<CusBatPubHandoverLstVo> cusBatPubHandoverLstVoList = (List<CusBatPubHandoverLstVo>) BeanUtils.beansCopy(perCustInfoList, CusBatPubHandoverLstVo.class);
            try (SqlSession sqlSession = sqlSessionTemplate.getSqlSessionFactory().openSession(ExecutorType.BATCH)) {
                CusBatPubHandoverLstMapper cusBatPubHandoverLstMapper = sqlSession.getMapper(CusBatPubHandoverLstMapper.class);
                for (CusBatPubHandoverLstVo cusBatPubHandoverLstVo : cusBatPubHandoverLstVoList) {
                    if (StringUtil.isEmpty(cusBatPubHandoverLstVo.getCusId())) {
                        throw BizException.error(null, "9999", "客户编号不能为空");
                    }
                    if (StringUtil.isEmpty(cusBatPubHandoverLstVo.getCusName())) {
                        throw BizException.error(null, "9999", "客户名称不能为空");
                    }
                    if (StringUtil.isEmpty(cusBatPubHandoverLstVo.getHandoverId())) {
                        throw BizException.error(null, "9999", "移出人工号不能为空");
                    }
                    if (StringUtil.isEmpty(cusBatPubHandoverLstVo.getHandoverName())) {
                        throw BizException.error(null, "9999", "移出人姓名不能为空");
                    }
                    if (StringUtil.isEmpty(cusBatPubHandoverLstVo.getReceiverId())) {
                        throw BizException.error(null, "9999", "接收人工号不能为空");
                    }
                    if (StringUtil.isEmpty(cusBatPubHandoverLstVo.getReceiverName())) {
                        throw BizException.error(null, "9999", "接收人姓名不能为空");
                    }
                    ResultDto<AdminSmUserDto> handoverId = adminSmUserService.getByLoginCode(cusBatPubHandoverLstVo.getHandoverId());
                    if (handoverId.getData().getLoginCode() == null) {
                        throw BizException.error(null, "9999", "移出人工号"+cusBatPubHandoverLstVo.getHandoverId()+"系统不存在");
                    }
                    ResultDto<AdminSmUserDto> receiverId = adminSmUserService.getByLoginCode(cusBatPubHandoverLstVo.getReceiverId());
                    if (receiverId.getData().getLoginCode() == null) {
                        throw BizException.error(null, "9999", "接收人工号"+cusBatPubHandoverLstVo.getReceiverId()+"系统不存在");
                    }
                    //客户编号，客户经理必须存在，
                    QueryModel cqm = new QueryModel();
                    cqm.getCondition().put("cusId", cusBatPubHandoverLstVo.getCusId());
                    List<CusBase> exists = cusBaseMapper.selectByModel(cqm);
                    if(exists.isEmpty()){
                        throw BizException.error(null, "9999", cusBatPubHandoverLstVo.getCusId()+"客户编号不存在");
                    }
                    ResultDto<Integer> ct = adminSmUserService.queryCusFlowCount(cusBatPubHandoverLstVo.getCusId());
                    if(ct.getData()>0){
                        throw BizException.error(null, "9999", cusBatPubHandoverLstVo.getCusName()+"存在在途任务，不能移交");
                    }
                    CusBatPubHandoverLst cbp = new CusBatPubHandoverLst();
                    BeanUtils.beanCopy(cusBatPubHandoverLstVo, cbp);
                    String date = DateUtils.getCurrDateTimeStr();
                    Date date1 = new Date();
                    cbp.setInputBrId(userInfo.getOrg().getCode());
                    cbp.setInputId(userInfo.getLoginCode());
                    cbp.setInputDate(date);
                    cbp.setCreateTime(date1);
                    cbp.setUpdateTime(date1);
                    cbp.setUpdId(userInfo.getLoginCode());
                    cbp.setUpdBrId(userInfo.getOrg().getCode());
                    cbp.setPkId(UUID.randomUUID().toString());
                    cbp.setCbphaSerno(serno);
                    cusBatPubHandoverLstMapper.insertSelective(cbp);
                    }
                    sqlSession.flushStatements();
                    sqlSession.commit();
                }
                return cusBatPubHandoverLstVoList == null ? 0 : cusBatPubHandoverLstVoList.size();
            }


}
