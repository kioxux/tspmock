/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstYpqfWhiteApp;
import cn.com.yusys.yusp.service.CusLstYpqfWhiteAppService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstYpqfWhiteAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-11 17:24:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstypqfwhiteapp")
public class CusLstYpqfWhiteAppResource {
    @Autowired
    private CusLstYpqfWhiteAppService cusLstYpqfWhiteAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstYpqfWhiteApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstYpqfWhiteApp> list = cusLstYpqfWhiteAppService.selectAll(queryModel);
        return new ResultDto<List<CusLstYpqfWhiteApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstYpqfWhiteApp>> index(QueryModel queryModel) {
        List<CusLstYpqfWhiteApp> list = cusLstYpqfWhiteAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYpqfWhiteApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CusLstYpqfWhiteApp>> query(@RequestBody  QueryModel queryModel) {
        List<CusLstYpqfWhiteApp> list = cusLstYpqfWhiteAppService.selectByModel(queryModel);
        return new ResultDto<List<CusLstYpqfWhiteApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{lywaSerno}")
    protected ResultDto<CusLstYpqfWhiteApp> show(@PathVariable("lywaSerno") String lywaSerno) {
        CusLstYpqfWhiteApp cusLstYpqfWhiteApp = cusLstYpqfWhiteAppService.selectByPrimaryKey(lywaSerno);
        return new ResultDto<CusLstYpqfWhiteApp>(cusLstYpqfWhiteApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstYpqfWhiteApp> create(@RequestBody CusLstYpqfWhiteApp cusLstYpqfWhiteApp) throws URISyntaxException {
        cusLstYpqfWhiteAppService.insert(cusLstYpqfWhiteApp);
        return new ResultDto<CusLstYpqfWhiteApp>(cusLstYpqfWhiteApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstYpqfWhiteApp cusLstYpqfWhiteApp) throws URISyntaxException {
        int result = cusLstYpqfWhiteAppService.update(cusLstYpqfWhiteApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{lywaSerno}")
    protected ResultDto<Integer> delete(@PathVariable("lywaSerno") String lywaSerno) {
        int result = cusLstYpqfWhiteAppService.deleteByPrimaryKey(lywaSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstYpqfWhiteAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
