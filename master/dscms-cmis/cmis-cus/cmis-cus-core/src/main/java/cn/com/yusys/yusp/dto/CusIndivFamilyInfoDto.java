package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

public class CusIndivFamilyInfoDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //上一级客户ID
    private String upperCusId;
    private List<CusRelationsDto> cusRelationsDto;

    public String getUpperCusId() {
        return upperCusId;
    }

    public void setUpperCusId(String upperCusId) {
        this.upperCusId = upperCusId;
    }

    public List<CusRelationsDto> getCusRelationsDto() {
        return cusRelationsDto;
    }

    public void setCusRelationsDto(List<CusRelationsDto> cusRelationsDto) {
        this.cusRelationsDto = cusRelationsDto;
    }
}
