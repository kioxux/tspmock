/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatSofp
 * @类描述: FNC_STAT_SOFP数据实体类
 * @功能描述: 
 * @创建人: zzbankwb369
 * @创建时间: 2019-10-08 09:45:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "FNC_STAT_SOFP")
public class FncStatSofp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 客户代码 **/
	@Id
	@Column(name = "CUS_ID")
	private String cusId;
	
	/** 报表口径STD_ZB_FNC_STYLE **/
	@Id
	@Column(name = "STAT_STYLE")
	private String statStyle;
	
	/** 报表年 **/
	@Id
	@Column(name = "STAT_YEAR")
	private String statYear;
	
	/** 项目编号 **/
	@Id
	@Column(name = "STAT_ITEM_ID")
	private String statItemId;
	
	/** 一月期数值 **/
	@Column(name = "STAT_INIT_AMT1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt1;
	
	/** 二月期数值 **/
	@Column(name = "STAT_INIT_AMT2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt2;
	
	/** 三月期数值 **/
	@Column(name = "STAT_INIT_AMT3", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt3;
	
	/** 四月期数值 **/
	@Column(name = "STAT_INIT_AMT4", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt4;
	
	/** 五月期数值 **/
	@Column(name = "STAT_INIT_AMT5", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt5;
	
	/** 六月期数值 **/
	@Column(name = "STAT_INIT_AMT6", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt6;
	
	/** 七月期数值 **/
	@Column(name = "STAT_INIT_AMT7", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt7;
	
	/** 八月期数值 **/
	@Column(name = "STAT_INIT_AMT8", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt8;
	
	/** 九月期数值 **/
	@Column(name = "STAT_INIT_AMT9", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt9;
	
	/** 十月期数值 **/
	@Column(name = "STAT_INIT_AMT10", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt10;
	
	/** 十一月期数值 **/
	@Column(name = "STAT_INIT_AMT11", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt11;
	
	/** 十二月期数值 **/
	@Column(name = "STAT_INIT_AMT12", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmt12;
	
	/** 一季度期数值 **/
	@Column(name = "STAT_INIT_AMT_Q1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtQ1;
	
	/** 二季度期数值 **/
	@Column(name = "STAT_INIT_AMT_Q2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtQ2;
	
	/** 三季度期数值 **/
	@Column(name = "STAT_INIT_AMT_Q3", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtQ3;
	
	/** 四季度期数值 **/
	@Column(name = "STAT_INIT_AMT_Q4", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtQ4;
	
	/** 上半年期数值 **/
	@Column(name = "STAT_INIT_AMT_Y1", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtY1;
	
	/** 下半年期数值 **/
	@Column(name = "STAT_INIT_AMT_Y2", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtY2;
	
	/** 年期数值 **/
	@Column(name = "STAT_INIT_AMT_Y", unique = false, nullable = true, length = 20)
	private java.math.BigDecimal statInitAmtY;
	
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param statStyle
	 */
	public void setStatStyle(String statStyle) {
		this.statStyle = statStyle;
	}
	
    /**
     * @return statStyle
     */
	public String getStatStyle() {
		return this.statStyle;
	}
	
	/**
	 * @param statYear
	 */
	public void setStatYear(String statYear) {
		this.statYear = statYear;
	}
	
    /**
     * @return statYear
     */
	public String getStatYear() {
		return this.statYear;
	}
	
	/**
	 * @param statItemId
	 */
	public void setStatItemId(String statItemId) {
		this.statItemId = statItemId;
	}
	
    /**
     * @return statItemId
     */
	public String getStatItemId() {
		return this.statItemId;
	}
	
	/**
	 * @param statInitAmt1
	 */
	public void setStatInitAmt1(java.math.BigDecimal statInitAmt1) {
		this.statInitAmt1 = statInitAmt1;
	}
	
    /**
     * @return statInitAmt1
     */
	public java.math.BigDecimal getStatInitAmt1() {
		return this.statInitAmt1;
	}
	
	/**
	 * @param statInitAmt2
	 */
	public void setStatInitAmt2(java.math.BigDecimal statInitAmt2) {
		this.statInitAmt2 = statInitAmt2;
	}
	
    /**
     * @return statInitAmt2
     */
	public java.math.BigDecimal getStatInitAmt2() {
		return this.statInitAmt2;
	}
	
	/**
	 * @param statInitAmt3
	 */
	public void setStatInitAmt3(java.math.BigDecimal statInitAmt3) {
		this.statInitAmt3 = statInitAmt3;
	}
	
    /**
     * @return statInitAmt3
     */
	public java.math.BigDecimal getStatInitAmt3() {
		return this.statInitAmt3;
	}
	
	/**
	 * @param statInitAmt4
	 */
	public void setStatInitAmt4(java.math.BigDecimal statInitAmt4) {
		this.statInitAmt4 = statInitAmt4;
	}
	
    /**
     * @return statInitAmt4
     */
	public java.math.BigDecimal getStatInitAmt4() {
		return this.statInitAmt4;
	}
	
	/**
	 * @param statInitAmt5
	 */
	public void setStatInitAmt5(java.math.BigDecimal statInitAmt5) {
		this.statInitAmt5 = statInitAmt5;
	}
	
    /**
     * @return statInitAmt5
     */
	public java.math.BigDecimal getStatInitAmt5() {
		return this.statInitAmt5;
	}
	
	/**
	 * @param statInitAmt6
	 */
	public void setStatInitAmt6(java.math.BigDecimal statInitAmt6) {
		this.statInitAmt6 = statInitAmt6;
	}
	
    /**
     * @return statInitAmt6
     */
	public java.math.BigDecimal getStatInitAmt6() {
		return this.statInitAmt6;
	}
	
	/**
	 * @param statInitAmt7
	 */
	public void setStatInitAmt7(java.math.BigDecimal statInitAmt7) {
		this.statInitAmt7 = statInitAmt7;
	}
	
    /**
     * @return statInitAmt7
     */
	public java.math.BigDecimal getStatInitAmt7() {
		return this.statInitAmt7;
	}
	
	/**
	 * @param statInitAmt8
	 */
	public void setStatInitAmt8(java.math.BigDecimal statInitAmt8) {
		this.statInitAmt8 = statInitAmt8;
	}
	
    /**
     * @return statInitAmt8
     */
	public java.math.BigDecimal getStatInitAmt8() {
		return this.statInitAmt8;
	}
	
	/**
	 * @param statInitAmt9
	 */
	public void setStatInitAmt9(java.math.BigDecimal statInitAmt9) {
		this.statInitAmt9 = statInitAmt9;
	}
	
    /**
     * @return statInitAmt9
     */
	public java.math.BigDecimal getStatInitAmt9() {
		return this.statInitAmt9;
	}
	
	/**
	 * @param statInitAmt10
	 */
	public void setStatInitAmt10(java.math.BigDecimal statInitAmt10) {
		this.statInitAmt10 = statInitAmt10;
	}
	
    /**
     * @return statInitAmt10
     */
	public java.math.BigDecimal getStatInitAmt10() {
		return this.statInitAmt10;
	}
	
	/**
	 * @param statInitAmt11
	 */
	public void setStatInitAmt11(java.math.BigDecimal statInitAmt11) {
		this.statInitAmt11 = statInitAmt11;
	}
	
    /**
     * @return statInitAmt11
     */
	public java.math.BigDecimal getStatInitAmt11() {
		return this.statInitAmt11;
	}
	
	/**
	 * @param statInitAmt12
	 */
	public void setStatInitAmt12(java.math.BigDecimal statInitAmt12) {
		this.statInitAmt12 = statInitAmt12;
	}
	
    /**
     * @return statInitAmt12
     */
	public java.math.BigDecimal getStatInitAmt12() {
		return this.statInitAmt12;
	}
	
	/**
	 * @param statInitAmtQ1
	 */
	public void setStatInitAmtQ1(java.math.BigDecimal statInitAmtQ1) {
		this.statInitAmtQ1 = statInitAmtQ1;
	}
	
    /**
     * @return statInitAmtQ1
     */
	public java.math.BigDecimal getStatInitAmtQ1() {
		return this.statInitAmtQ1;
	}
	
	/**
	 * @param statInitAmtQ2
	 */
	public void setStatInitAmtQ2(java.math.BigDecimal statInitAmtQ2) {
		this.statInitAmtQ2 = statInitAmtQ2;
	}
	
    /**
     * @return statInitAmtQ2
     */
	public java.math.BigDecimal getStatInitAmtQ2() {
		return this.statInitAmtQ2;
	}
	
	/**
	 * @param statInitAmtQ3
	 */
	public void setStatInitAmtQ3(java.math.BigDecimal statInitAmtQ3) {
		this.statInitAmtQ3 = statInitAmtQ3;
	}
	
    /**
     * @return statInitAmtQ3
     */
	public java.math.BigDecimal getStatInitAmtQ3() {
		return this.statInitAmtQ3;
	}
	
	/**
	 * @param statInitAmtQ4
	 */
	public void setStatInitAmtQ4(java.math.BigDecimal statInitAmtQ4) {
		this.statInitAmtQ4 = statInitAmtQ4;
	}
	
    /**
     * @return statInitAmtQ4
     */
	public java.math.BigDecimal getStatInitAmtQ4() {
		return this.statInitAmtQ4;
	}
	
	/**
	 * @param statInitAmtY1
	 */
	public void setStatInitAmtY1(java.math.BigDecimal statInitAmtY1) {
		this.statInitAmtY1 = statInitAmtY1;
	}
	
    /**
     * @return statInitAmtY1
     */
	public java.math.BigDecimal getStatInitAmtY1() {
		return this.statInitAmtY1;
	}
	
	/**
	 * @param statInitAmtY2
	 */
	public void setStatInitAmtY2(java.math.BigDecimal statInitAmtY2) {
		this.statInitAmtY2 = statInitAmtY2;
	}
	
    /**
     * @return statInitAmtY2
     */
	public java.math.BigDecimal getStatInitAmtY2() {
		return this.statInitAmtY2;
	}
	
	/**
	 * @param statInitAmtY
	 */
	public void setStatInitAmtY(java.math.BigDecimal statInitAmtY) {
		this.statInitAmtY = statInitAmtY;
	}
	
    /**
     * @return statInitAmtY
     */
	public java.math.BigDecimal getStatInitAmtY() {
		return this.statInitAmtY;
	}


}