package cn.com.yusys.yusp.service.server.cmiscus0017;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0017.req.Cmiscus0017ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0017.resp.Cmiscus0017RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.FncStatIsMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 业务逻辑处理类：查询客户去年12期财报中最大的主营业务收入
 *
 * @author xuchao
 * @version 1.0
 * @since 2021年6月9日
 */
@Service
public class Cmiscus0017Service {
    private static final Logger logger = LoggerFactory.getLogger(Cmiscus0017Service.class);
    @Autowired
    private FncStatIsMapper fncStatIsMapper;

    /**
     * 查询客户去年12期财报中最大的主营业务收入
     * @param cmisCus0017ReqDto
     * @return
     */
    @Transactional
    public Cmiscus0017RespDto cmiscus0017(Cmiscus0017ReqDto cmisCus0017ReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0016.key, DscmsEnum.TRADE_CODE_CMISCUS0016.value, JSON.toJSONString(cmisCus0017ReqDto));
        Cmiscus0017RespDto cmiscus0017RespDto = new Cmiscus0017RespDto();
        try {
            //查询客户去年12期财报中最大的主营业务收入
            BigDecimal amt = fncStatIsMapper.getMaxAmt(cmisCus0017ReqDto);
            cmiscus0017RespDto.setAmt(amt);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0016.key, DscmsEnum.TRADE_CODE_CMISCUS0016.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0016.key, DscmsEnum.TRADE_CODE_CMISCUS0016.value, JSON.toJSONString(cmisCus0017ReqDto));
        return cmiscus0017RespDto;
    }
}
