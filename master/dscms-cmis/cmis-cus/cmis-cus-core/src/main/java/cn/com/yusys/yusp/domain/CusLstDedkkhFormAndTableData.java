/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstDedkkh
 * @类描述: cus_lst_dedkkh数据实体类
 * @功能描述: 
 * @创建人: bryan
 * @创建时间: 2021-04-03 14:36:13
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_lst_dedkkh")
public class CusLstDedkkhFormAndTableData extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 表单 **/
	@Column(name = "CUS_LST_DEDKKH_DATA")
	private CusLstDedkkh cusLstDedkkhData;
	
	/** 表格 **/
	@Column(name = "CUS_LST_DEDKKH_YJSX_DATA")
	private List<CusLstDedkkhYjsx> cusLstDedkkhYjsxData;
	
	/**
	 * @param cusLstDedkkhData
	 */
	public void setCusLstDedkkhData(CusLstDedkkh cusLstDedkkhData) {
		this.cusLstDedkkhData = cusLstDedkkhData;
	}
	
    /**
     * @return cusLstDedkkhData
     */
	public CusLstDedkkh getCusLstDedkkhData() {
		return this.cusLstDedkkhData;
	}

	/**
	 * @param cusLstDedkkhYjsxData
	 */
	public void setCusLstDedkkhYjsxData(List<CusLstDedkkhYjsx> cusLstDedkkhYjsxData) {
		this.cusLstDedkkhYjsxData = cusLstDedkkhYjsxData;
	}

	/**
	 * @return cusLstDedkkhYjsxData
	 */
	public List<CusLstDedkkhYjsx> getCusLstDedkkhYjsxData() {
		return this.cusLstDedkkhYjsxData;
	}


}