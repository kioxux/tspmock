/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.FncItemMap;
import cn.com.yusys.yusp.service.FncItemMapService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncItemMapResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 14:19:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/fncitemmap")
public class FncItemMapResource {
    @Autowired
    private FncItemMapService fncItemMapService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<FncItemMap>> query() {
        QueryModel queryModel = new QueryModel();
        List<FncItemMap> list = fncItemMapService.selectAll(queryModel);
        return new ResultDto<List<FncItemMap>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<FncItemMap>> index(QueryModel queryModel) {
        List<FncItemMap> list = fncItemMapService.selectByModel(queryModel);
        return new ResultDto<List<FncItemMap>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<FncItemMap> show(@PathVariable("serno") String serno) {
        FncItemMap fncItemMap = fncItemMapService.selectByPrimaryKey(serno);
        return new ResultDto<FncItemMap>(fncItemMap);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<FncItemMap> create(@RequestBody FncItemMap fncItemMap) throws URISyntaxException {
        fncItemMapService.insert(fncItemMap);
        return new ResultDto<FncItemMap>(fncItemMap);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody FncItemMap fncItemMap) throws URISyntaxException {
        int result = fncItemMapService.update(fncItemMap);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = fncItemMapService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = fncItemMapService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
