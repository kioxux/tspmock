package cn.com.yusys.yusp.web.server.xdkh0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0012.req.Xdkh0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0012.resp.Xdkh0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0012.Xdkh0012Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:对公客户信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0012:对公客户信息同步")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0012Resource.class);

    @Autowired
    private Xdkh0012Service xdkh0012Service;

    /**
     * 交易码：xdkh0012
     * 交易描述：对公客户信息同步
     *
     * @param xdkh0012DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对公客户信息同步")
    @PostMapping("/xdkh0012")
    protected @ResponseBody
    ResultDto<Xdkh0012DataRespDto> xdkh0012(@Validated @RequestBody Xdkh0012DataReqDto xdkh0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012DataReqDto));
        Xdkh0012DataRespDto xdkh0012DataRespDto = new Xdkh0012DataRespDto();// 响应Dto:对公客户信息同步
        ResultDto<Xdkh0012DataRespDto> xdkh0012DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012DataReqDto));
            xdkh0012DataRespDto = xdkh0012Service.getXdkh0012(xdkh0012DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012DataRespDto));
            // 封装xdkh0012DataResultDto中正确的返回码和返回信息
            xdkh0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, e.getMessage());
            // 封装xdkh0012DataResultDto中异常返回码和返回信息
            xdkh0012DataResultDto.setCode(e.getErrorCode());
            xdkh0012DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, e.getMessage());
            // 封装xdkh0012DataResultDto中异常返回码和返回信息
            xdkh0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0012DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0012DataRespDto到xdkh0012DataResultDto中
        xdkh0012DataResultDto.setData(xdkh0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012DataResultDto));
        return xdkh0012DataResultDto;
    }
}