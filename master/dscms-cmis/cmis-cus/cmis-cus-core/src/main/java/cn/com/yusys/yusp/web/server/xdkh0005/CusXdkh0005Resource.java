package cn.com.yusys.yusp.web.server.xdkh0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0005.req.Xdkh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0005.resp.Xdkh0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0005.Xdkh0005Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:同业客户信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0005:同业客户信息查询")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0005Resource.class);

    @Autowired
    private Xdkh0005Service xdkh0005Service;

    /**
     * 交易码：xdkh0005
     * 交易描述：同业客户信息查询
     *
     * @param xdkh0005DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("同业客户信息查询")
    @PostMapping("/xdkh0005")
    protected @ResponseBody
    ResultDto<Xdkh0005DataRespDto> xdkh0005(@Validated @RequestBody Xdkh0005DataReqDto xdkh0005DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005DataReqDto));
        Xdkh0005DataRespDto xdkh0005DataRespDto = new Xdkh0005DataRespDto();// 响应Dto:同业客户信息查询
        ResultDto<Xdkh0005DataRespDto> xdkh0005DataResultDto = new ResultDto<Xdkh0005DataRespDto>(); //响应Data:同业客户信息查询
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005DataReqDto));
            xdkh0005DataRespDto = xdkh0005Service.getXdkh0005(xdkh0005DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005DataRespDto));
            // 封装xdkh0005DataResultDto中正确的返回码和返回信息
            xdkh0005DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0005DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, e.getMessage());
            // 封装xdkh0005DataResultDto中异常返回码和返回信息
            xdkh0005DataResultDto.setCode(e.getErrorCode());
            xdkh0005DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, e.getMessage());
            // 封装xdkh0005DataResultDto中异常返回码和返回信息
            xdkh0005DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0005DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0005DataRespDto到xdkh0005DataResultDto中
        xdkh0005DataResultDto.setData(xdkh0005DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005DataResultDto));
        return xdkh0005DataResultDto;
    }
}
