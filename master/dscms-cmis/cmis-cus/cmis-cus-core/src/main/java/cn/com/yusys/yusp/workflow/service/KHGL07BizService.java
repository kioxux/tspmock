package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusLstDedkkhYjsx;
import cn.com.yusys.yusp.domain.CusLstDedkkhYjsxTask;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CusLstDedkkhYjsxService;
import cn.com.yusys.yusp.service.CusLstDedkkhYjsxTaskService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 大额贷款客户压降任务审批流程
 *
 * @author liucheng
 * @version 1.0.0
 *
 */
@Service
public class KHGL07BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(KHGL07BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CusLstDedkkhYjsxTaskService service;

    @Autowired
    private CusLstDedkkhYjsxService cusLstDedkkhYjsxService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        String floeBizType = resultInstanceDto.getBizType();
        log.info("后业务处理类型" + currentOpType);
        try {
            CusLstDedkkhYjsxTask domain = service.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
//                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
//                service.update(domain);
                log.info("大额贷款客户压降任务审批流程【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("大额贷款客户压降任务审批流程【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("大额贷款客户压降任务审批流程【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                if("KH015".equals(floeBizType)){ //压降任务情况落实
                    // 更新主表【实际压降金额（万元）】【压降实际完成时间】【压降落实情况说明】
                    CusLstDedkkhYjsx cusLstDedkkhYjsx = cusLstDedkkhYjsxService.selectByPrimaryKey(domain.getTaskNo());
                    if(cusLstDedkkhYjsx!=null) {
                        cusLstDedkkhYjsx.setActPdoAmt(domain.getActPdoAmt()); // 实际压降金额（万元）
                        cusLstDedkkhYjsx.setActPdoFinishDate(domain.getActPdoFinishDate()); // 压降实际完成时间
                        cusLstDedkkhYjsx.setPdoActCaseMemo(domain.getPdoActCaseMemo()); // 压降落实情况说明
                        cusLstDedkkhYjsxService.update(cusLstDedkkhYjsx);
                    }
                }else if("KH016".equals(floeBizType)){ // 压降任务终止申请
                    // 不做任何操作
                }
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                service.update(domain);
                log.info("大额贷款客户压降任务审批流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("大额贷款客户压降任务审批流程【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 判断是否打回发起人节点
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                    domain.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    service.update(domain);
                }
                log.info("大额贷款客户压降任务审批流程【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("大额贷款客户压降任务审批流程【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("大额贷款客户压降任务审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("大额贷款客户压降任务审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                service.update(domain);
            } else {
                log.info("大额贷款客户压降任务审批流程【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("大额贷款客户压降任务审批流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "KHGL07".equals(flowCode);
    }
}
