package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusMgrDividePerc;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CusMgrDividePercService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 对公客户分成比例申请审批流程
 *
 * @author cp
 * @version 1.0.0
 */
@Service
public class KHGL13BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(KHGL13BizService.class);

    @Autowired
    private CusMgrDividePercService cusMgrDividePercService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String cusId = resultInstanceDto.getBizUserId();
        log.info("后业务处理类型" + currentOpType);
        String approveStatus = null;
        String isEffect = null;
        Date updateTime = DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue());
        try {
            List<CusMgrDividePerc> list = cusMgrDividePercService.selectAllByCusId(cusId);
            String pkId = list.get(0).getPkId();
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("对公客户分成比例申请审批流程【{}】，流程发起操作，流程参数【{}】", cusId, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                approveStatus = CmisBizConstants.APPLY_STATE_APP;
                cusMgrDividePercService.updateApproveStatus(pkId, approveStatus, updateTime);
                log.info("对公客户分成比例申请审批流程【{}】，流程提交操作，流程参数【{}】", cusId, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                approveStatus = CmisBizConstants.APPLY_STATE_CALL_BACK;
                cusMgrDividePercService.updateApproveStatus(pkId, approveStatus, updateTime);
                log.info("对公客户分成比例申请审批流程【{}】，流程结束操作，流程参数【{}】", cusId, resultInstanceDto);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                approveStatus = CmisBizConstants.APPLY_STATE_REFUSE;
                isEffect = "0";
                cusMgrDividePercService.updateApproveStatusAndEffect(pkId, approveStatus, isEffect, updateTime);
                log.info("对公客户分成比例申请审批流程【{}】，流程打回操作，流程参数【{}】", cusId, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                approveStatus = CmisBizConstants.APPLY_STATE_PASS;
                isEffect = "1";
                cusMgrDividePercService.updatecusMgrDividePerc(pkId,cusId,approveStatus, isEffect, updateTime);
                log.info("对公客户分成比例申请审批流程【{}】，流程结束操作，流程参数【{}】", cusId, resultInstanceDto);
            } else {
                log.info("对公客户分成比例申请审批流程【{}】，未知操作，流程参数【{}】", cusId, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("对公客户分成比例申请审批流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "KHGL13".equals(flowCode);
    }
}

