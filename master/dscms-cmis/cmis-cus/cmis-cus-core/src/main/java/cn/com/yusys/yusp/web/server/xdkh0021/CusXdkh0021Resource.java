package cn.com.yusys.yusp.web.server.xdkh0021;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdkh0021.req.Xdkh0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0021.resp.Xdkh0021DataRespDto;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0021.Xdkh0021Service;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:在信贷系统中生成用户信息
 *
 * @author code-generator
 * @version 1.0
 */
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0021Resource.class);
    @Autowired
    private Xdkh0021Service xdkh0021Service;

    /**
     * 交易码：xdkh0021
     * 交易描述：在信贷系统中生成用户信息
     *
     * @param xdkh0021DataReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdkh0021")
    protected @ResponseBody
    ResultDto<Xdkh0021DataRespDto> xdkh0021(@Validated @RequestBody Xdkh0021DataReqDto xdkh0021DataReqDto) throws Exception {
        Xdkh0021DataRespDto xdkh0021DataRespDto = new Xdkh0021DataRespDto();// 响应Dto:在信贷系统中生成用户信息
        ResultDto<Xdkh0021DataRespDto> xdkh0021DataResultDto = new ResultDto<>();
        // 从xdkh0021DataReqDto获取业务值进行业务逻辑处理
        String cus_id = xdkh0021DataReqDto.getCus_id();//用户ID
        String cus_name = xdkh0021DataReqDto.getCus_name();//用户名
        String cert_type = xdkh0021DataReqDto.getCert_type();//证件类型
        String cert_code = xdkh0021DataReqDto.getCert_code();//身份证号码
        String orgid = xdkh0021DataReqDto.getOrgid();//机构号
        String currentuserid = xdkh0021DataReqDto.getCurrentuserid();//当前客户经理
        try {
            // TODO 调用XXXXXService层开始
            xdkh0021DataRespDto = xdkh0021Service.xdkh0021(xdkh0021DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xdkh0021DataRespDto对象开始
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
//            xdkh0021DataRespDto.setOpFlag(StringUtils.EMPTY);// 能否关闭
//            xdkh0021DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
            // TODO 封装xdkh0021DataRespDto对象结束
            // 封装xdkh0021DataResultDto中正确的返回码和返回信息
            xdkh0021DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0021DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            // 封装xdkh0021DataResultDto中异常返回码和返回信息
            xdkh0021DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0021DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0021DataRespDto到xdkh0021DataResultDto中
        xdkh0021DataResultDto.setData(xdkh0021DataRespDto);
        return xdkh0021DataResultDto;
    }
}
