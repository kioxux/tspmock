/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.ImportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.vo.CusLstJjycExportVo;
import cn.com.yusys.yusp.vo.CusLstWjkhMxxxExportVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusLstWjkhMxxx;
import cn.com.yusys.yusp.service.CusLstWjkhMxxxService;

/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusLstWjkhMxxxResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-05-05 09:06:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuslstwjkhmxxx")
public class CusLstWjkhMxxxResource {
    @Autowired
    private CusLstWjkhMxxxService cusLstWjkhMxxxService;

    private final Logger logger = LoggerFactory.getLogger(CusLstJjycResource.class);
	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusLstWjkhMxxx>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusLstWjkhMxxx> list = cusLstWjkhMxxxService.selectAll(queryModel);
        return new ResultDto<List<CusLstWjkhMxxx>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusLstWjkhMxxx>> index(QueryModel queryModel) {
        List<CusLstWjkhMxxx> list = cusLstWjkhMxxxService.selectByModel(queryModel);
        return new ResultDto<List<CusLstWjkhMxxx>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusLstWjkhMxxx> show(@PathVariable("pkId") String pkId) {
        CusLstWjkhMxxx cusLstWjkhMxxx = cusLstWjkhMxxxService.selectByPrimaryKey(pkId);
        return new ResultDto<CusLstWjkhMxxx>(cusLstWjkhMxxx);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusLstWjkhMxxx> create(@RequestBody CusLstWjkhMxxx cusLstWjkhMxxx) throws URISyntaxException {
        cusLstWjkhMxxxService.insert(cusLstWjkhMxxx);
        return new ResultDto<CusLstWjkhMxxx>(cusLstWjkhMxxx);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusLstWjkhMxxx cusLstWjkhMxxx) throws URISyntaxException {
        int result = cusLstWjkhMxxxService.update(cusLstWjkhMxxx);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusLstWjkhMxxxService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusLstWjkhMxxxService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 导出模板
     */
    @PostMapping("/exporttemplate")
    public ResultDto<ProgressDto> asyncExportTemplate() {
        ProgressDto progressDto = cusLstWjkhMxxxService.asyncExportTemplate();
        return ResultDto.success(progressDto);
    }

    /**
     * Excel数据导入
     *
     * @param fileId Excel文件信息ID
     * @return
     * @throws IOException
     */
    @PostMapping("/excelimport")
    public ResultDto<ProgressDto> asyncImportDataBatch(@RequestParam("fileId") String fileId) throws IOException {
        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
        FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
        // 将文件内容导入数据库，StudentScore为导入数据的类
        ProgressDto progressDto = ExcelUtils.asyncImport(ImportContext.of(CusLstWjkhMxxxExportVo.class)
                // 批量操作需要将batch设置为true
                .batch(true)
                .file(tempFile)
                .dataStorage(ExcelUtils.batchConsumer(cusLstWjkhMxxxService::insertInBatch)));
        logger.info("开始执行异步导出，导入taskId为[{}];", progressDto.getTaskId());
        return ResultDto.success(progressDto);
    }
}
