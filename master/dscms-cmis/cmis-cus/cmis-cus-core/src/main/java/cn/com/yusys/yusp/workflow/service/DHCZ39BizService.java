package cn.com.yusys.yusp.workflow.service;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.domain.CusLstWhbxd;
import cn.com.yusys.yusp.domain.CusLstWhbxdApp;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.CusLstWhbxdAppService;
import cn.com.yusys.yusp.service.CusLstWhbxdService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 无还本续贷白名单申请审批流程
 * @author liucheng
 * @version 1.0.0
 * @date 2021/4/12 下午12:42
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DHCZ39BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(DHCZ39BizService.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private CusLstWhbxdAppService service;
    @Autowired
    private CusLstWhbxdService cusLstWhbxdService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
//        String floeBizType = resultInstanceDto.getBizType();
        log.info("后业务处理类型" + currentOpType);
        try {
            CusLstWhbxdApp domain = service.selectByPrimaryKey(serno);
            if (OpType.STRAT.equals(currentOpType)) {
//                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
//                service.update(domain);
                log.info("无还本续贷白名单申请审批流程【{}】，流程发起操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RUN.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("无还本续贷白名单申请审批流程【{}】，流程提交操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.JUMP.equals(currentOpType)) {
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
                service.update(domain);
                log.info("无还本续贷白名单申请审批流程【{}】，流程跳转操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                // 1、插入cus_lst_whbxd 表
                CusLstWhbxd cusLstWhbxd = new CusLstWhbxd();
                BeanUtils.beanCopy(domain, cusLstWhbxd);
                // 有则更新、无则插入
                CusLstWhbxd temp = cusLstWhbxdService.selectByPrimaryKey(domain.getCusId());
                if(temp == null){
                    cusLstWhbxdService.insert(cusLstWhbxd);
                }else{
                    cusLstWhbxdService.updateSelective(cusLstWhbxd);
                }

                // 2、更新申请表
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
                service.update(domain);
                log.info("无还本续贷白名单申请审批流程【{}】，流程结束操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("无还本续贷白名单申请审批流程【{}】，流程退回操作，流程参数【{}】", serno, resultInstanceDto);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 判断是否打回发起人节点
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)){
                    domain.setApproveStatus(CmisBizConstants.APPLY_STATE_CALL_BACK);
                    service.update(domain);
                }
                log.info("无还本续贷白名单申请审批流程【{}】，流程打回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("无还本续贷白名单申请审批流程【{}】，流程拿回操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("无还本续贷白名单申请审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_TACK_BACK);
                service.update(domain);
                //针对流程到办结节点，进行以下处理
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("无还本续贷白名单申请审批流程【{}】，流程拿回初始节点操作，流程参数【{}】", serno, resultInstanceDto);
                //针对流程到办结节点，进行以下处理
                domain.setApproveStatus(CmisBizConstants.APPLY_STATE_REFUSE);
                service.update(domain);
            } else {
                log.info("无还本续贷白名单申请审批流程【{}】，未知操作，流程参数【{}】", serno, resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("无还本续贷白名单申请审批流程后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
            // 抛出此指定异常，仅为将消息消费掉，避免重试进入死循环
            throw new MessageConversionException("消息消费失败，触发事务回滚，添加异常队列，并将此消息移除队列！！！！！！");
        }
    }

    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "DHCZ39".equals(flowCode);
    }
}
