package cn.com.yusys.yusp.web.server.cmiscus0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0014.req.CmisCus0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.cmiscus0014.CmisCus0014Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类: 法人客户及股东信息查询
 *
 * @author dumd
 * @version 1.0
 */
@Api(tags = "CmisCus0014:法人客户及股东信息查询")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0014Resource.class);

    @Autowired
    private CmisCus0014Service cmisCus0014Service;

    /**
     * 交易码：cmiscus0014
     * 交易描述：法人客户及股东信息查询
     *
     * @param cmisCus0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("法人客户及股东信息查询")
    @PostMapping("/cmiscus0014")
    protected @ResponseBody
    ResultDto<CmisCus0014RespDto> cmiscus0014(@Validated @RequestBody CmisCus0014ReqDto cmisCus0014ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0014.key, DscmsEnum.TRADE_CODE_CMISCUS0014.value, JSON.toJSONString(cmisCus0014ReqDto));
        CmisCus0014RespDto cmisCus0014RespDto = new CmisCus0014RespDto();// 响应Dto:查询客户基本信息
        ResultDto<CmisCus0014RespDto> cmisCus0014ResultDto = new ResultDto<>();
        try {
            // 封装cmisCus0014ResultDto中正确的返回码和返回信息
            cmisCus0014RespDto = cmisCus0014Service.execute(cmisCus0014ReqDto);
            cmisCus0014ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0014ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0014.key, DscmsEnum.TRADE_CODE_CMISCUS0014.value, e.getMessage());
            // 封装cmisCus0014ResultDto中异常返回码和返回信息
            cmisCus0014ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0014ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0014RespDto到cmisCus0014ResultDto中
        cmisCus0014ResultDto.setData(cmisCus0014RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0014.key, DscmsEnum.TRADE_CODE_CMISCUS0014.value, JSON.toJSONString(cmisCus0014ResultDto));
        return cmisCus0014ResultDto;
    }
}
