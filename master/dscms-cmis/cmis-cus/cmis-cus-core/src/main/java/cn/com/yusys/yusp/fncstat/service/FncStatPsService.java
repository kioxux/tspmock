/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.fncstat.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.fncstat.domain.FncStatPs;
import cn.com.yusys.yusp.fncstat.repository.mapper.FncStatPsMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncStatPsService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zzbankwb369
 * @创建时间: 2019-10-08 09:46:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class FncStatPsService {

    @Autowired
    private FncStatPsMapper fncStatPsMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public FncStatPs selectByPrimaryKey(String cusId, String statStyle, String statYear, String statItemId) {
        return fncStatPsMapper.selectByPrimaryKey(cusId, statStyle, statYear, statItemId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional(readOnly=true)
    public List<FncStatPs> selectAll(QueryModel model) {
        List<FncStatPs> records = (List<FncStatPs>) fncStatPsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public List<FncStatPs> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<FncStatPs> list = fncStatPsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insert(FncStatPs record) {
        return fncStatPsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int insertSelective(FncStatPs record) {
        return fncStatPsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int update(FncStatPs record) {
        return fncStatPsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int updateSelective(FncStatPs record) {
        return fncStatPsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String cusId, String statStyle, String statYear, String statItemId) {
        return fncStatPsMapper.deleteByPrimaryKey(cusId, statStyle, statYear, statItemId);
    }

}
