package cn.com.yusys.yusp.repository.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface OrderDownloadMapper {

	List<Map<String, Object>> queryResultBySql(@Param("querySql") String querySql);
}
