package cn.com.yusys.yusp.web.server.xdkh0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdkh0007.req.Xdkh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0007.resp.Xdkh0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdkh0007.Xdkh0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:查询客户是否本行员工及直属亲属
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0007:查询客户是否本行员工及直属亲属")
@RestController
@RequestMapping("/api/cus4bsp")
public class CusXdkh0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(CusXdkh0007Resource.class);
    @Autowired
    private Xdkh0007Service xdkh0007Service;

    /**
     * 交易码：xdkh0007
     * 交易描述：查询客户是否本行员工及直属亲属
     *
     * @param xdkh0007DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("同业客户信息查询")
    @PostMapping("/xdkh0007")
    protected @ResponseBody
    ResultDto<Xdkh0007DataRespDto> xdkh0007(@Validated @RequestBody Xdkh0007DataReqDto xdkh0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, JSON.toJSONString(xdkh0007DataReqDto));
        Xdkh0007DataRespDto xdkh0007DataRespDto = new Xdkh0007DataRespDto();// 响应Dto:查询客户是否本行员工及直属亲属
        ResultDto<Xdkh0007DataRespDto> xdkh0007DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, JSON.toJSONString(xdkh0007DataReqDto));
            xdkh0007DataRespDto = xdkh0007Service.getXdkh0007(xdkh0007DataReqDto.getCusId());
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, JSON.toJSONString(xdkh0007DataRespDto));
            // 封装xdkh0007DataResultDto中正确的返回码和返回信息
            xdkh0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdkh0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, e.getMessage());
            // 封装xdkh0007DataResultDto中异常返回码和返回信息
            xdkh0007DataResultDto.setCode(e.getErrorCode());
            xdkh0007DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, e.getMessage());
            // 封装xdkh0007DataResultDto中异常返回码和返回信息
            xdkh0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdkh0007DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdkh0007DataRespDto到xdkh0007DataResultDto中
        xdkh0007DataResultDto.setData(xdkh0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, JSON.toJSONString(xdkh0007DataReqDto));
        return xdkh0007DataResultDto;
    }
}