package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 客户关系网
 */
public class CusCorpRelationsDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 客户编号 **/
    private String cusId;
    /** 客户名称 **/
    private String cusName;
    /** 客户类型 **/
    private String cusType;
    /** 证件类型 **/
    private String certType;
    /** 证件号码 **/
    private String certCode;
    /** 关系（指客户与亲友的关系，客户与企业的关系，客户与担保的关系） **/
    private String relations;
    /** 高管关系 **/
    private CusCorpMgrComDto cusCorpMgrComDto;
    /** 法人家庭关系 **/
    private CusCorpFamilyInfoDto corpFamilyInfoDto;
    /** 对外投资关系 **/
    private CusCorpPubDto cusCorpPubDto;
    /** 股东信息**/
    private CusCorpApiDto cusCorpApiDto;

    /**所在关联集团信息**/
    private CusGrpMemberDto cusGrpMemberDto;

    public String getRelations() {
        return relations;
    }

    public void setRelations(String relations) {
        this.relations = relations;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public CusCorpMgrComDto getCusCorpMgrComDto() {
        return cusCorpMgrComDto;
    }

    public void setCusCorpMgrComDto(CusCorpMgrComDto cusCorpMgrComDto) {
        this.cusCorpMgrComDto = cusCorpMgrComDto;
    }

    public CusCorpFamilyInfoDto getCorpFamilyInfoDto() {
        return corpFamilyInfoDto;
    }

    public void setCorpFamilyInfoDto(CusCorpFamilyInfoDto corpFamilyInfoDto) {
        this.corpFamilyInfoDto = corpFamilyInfoDto;
    }

    public CusCorpPubDto getCusCorpPubDto() {
        return cusCorpPubDto;
    }

    public void setCusCorpPubDto(CusCorpPubDto cusCorpPubDto) {
        this.cusCorpPubDto = cusCorpPubDto;
    }

    public CusCorpApiDto getCusCorpApiDto() {
        return cusCorpApiDto;
    }

    public void setCusCorpApiDto(CusCorpApiDto cusCorpApiDto) {
        this.cusCorpApiDto = cusCorpApiDto;
    }

    public CusGrpMemberDto getCusGrpMemberDto() {
        return cusGrpMemberDto;
    }

    public void setCusGrpMemberDto(CusGrpMemberDto cusGrpMemberDto) {
        this.cusGrpMemberDto = cusGrpMemberDto;
    }
}
