/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.CusCorpMgr;
import cn.com.yusys.yusp.dto.CusCorpDto;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusPubRelInvest;
import cn.com.yusys.yusp.service.CusPubRelInvestService;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusPubRelInvestResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-22 21:11:16
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cuspubrelinvest")
public class CusPubRelInvestResource {
    @Autowired
    private CusPubRelInvestService cusPubRelInvestService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusPubRelInvest>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusPubRelInvest> list = cusPubRelInvestService.selectAll(queryModel);
        return new ResultDto<List<CusPubRelInvest>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CusPubRelInvest>> index(QueryModel queryModel) {
        List<CusPubRelInvest> list = cusPubRelInvestService.selectByModel(queryModel);
        return new ResultDto<List<CusPubRelInvest>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CusPubRelInvest> show(@PathVariable("pkId") String pkId) {
        CusPubRelInvest cusPubRelInvest = cusPubRelInvestService.selectByPrimaryKey(pkId);
        return new ResultDto<CusPubRelInvest>(cusPubRelInvest);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人zhoumw
     */
    @PostMapping("/queryBycusId")
    protected ResultDto<List<CusPubRelInvest>> queryBycusId(@RequestBody QueryModel queryModel) {
        List<CusPubRelInvest> list = cusPubRelInvestService.queryBycusId(queryModel);
        return new ResultDto<List<CusPubRelInvest>>(list);
    }
    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusPubRelInvest> create(@RequestBody CusPubRelInvest cusPubRelInvest) throws URISyntaxException {
        cusPubRelInvestService.insert(cusPubRelInvest);
        return new ResultDto<CusPubRelInvest>(cusPubRelInvest);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusPubRelInvest cusPubRelInvest) throws URISyntaxException {
        int result = cusPubRelInvestService.update(cusPubRelInvest);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = cusPubRelInvestService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusPubRelInvestService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 获取对外投资信息
     */
    @PostMapping("/getRel")
    protected ResultDto<CusPubRelInvest> getRel(@RequestBody CusPubRelInvest cusPubRelInvest) {
        QueryModel queryModel = new QueryModel();
        CusPubRelInvest cusRel = cusPubRelInvestService.selectCusCorpRelByCodeAndType(cusPubRelInvest.getCertType(),cusPubRelInvest.getCertCode(),cusPubRelInvest.getCusId());
        return new ResultDto<CusPubRelInvest>(cusRel);
    }

    /**
     * @函数名称:save
     * @函数描述:新增保存对外投资
     * @参数与返回说明:
     * @算法描述:
     * @创建人： 周茂伟
     */
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody CusPubRelInvest cusPubRelInvest) throws URISyntaxException {
        int count=cusPubRelInvestService.save(cusPubRelInvest);
        return new ResultDto<Integer>(count);
    }

    /**
     * @函数名称:save
     * @函数描述:对公客户开户引导
     * @参数与返回说明:
     * @算法描述:
     * @创建人： xuxin
     */
    @PostMapping("/createCrop")
    protected ResultDto<CusPubRelInvest> createCrop(@Validated @RequestBody CusPubRelInvest cusPubRelInvest) throws Exception {
        CusCorpDto cusCorpDto = new CusCorpDto();
        BeanUtils.copyProperties(cusPubRelInvest,cusCorpDto);
        cusCorpDto.setCusName(cusPubRelInvest.getCusNameRel());
        cusCorpDto.setBizType("A02");
        return new ResultDto<CusPubRelInvest>(cusPubRelInvestService.openAccount(cusCorpDto));
    }
}
