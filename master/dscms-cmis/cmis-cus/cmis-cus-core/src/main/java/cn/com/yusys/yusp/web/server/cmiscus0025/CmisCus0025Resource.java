package cn.com.yusys.yusp.web.server.cmiscus0025;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0025.req.CmisCus0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.resp.CmisCus0025RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusCorpMgrService;
import cn.com.yusys.yusp.service.CusLstYndAppService;
import cn.com.yusys.yusp.web.server.cmiscus0007.CmisCus0007Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;


/**
 * 接口处理类: 优农贷名单详细信息查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "CmisCus0025:优农贷名单详细信息查询")
@RestController
@RequestMapping("/api/cus4inner")
public class CmisCus0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0007Resource.class);

    @Autowired
    private CusLstYndAppService cusLstYndAppService;

    /**
     * 交易码：cmiscus0025
     * 交易描述：优农贷名单详细信息查询
     *
     * @param cmisCus0025ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷名单详细信息查询")
    @PostMapping("/cmiscus0025")
    protected @ResponseBody
    ResultDto<CmisCus0025RespDto> cmiscus0025(@Validated @RequestBody CmisCus0025ReqDto cmisCus0025ReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, JSON.toJSONString(cmisCus0025ReqDto));
        CmisCus0025RespDto cmisCus0025RespDto = new CmisCus0025RespDto();// 响应Dto:查询优农贷名单信息
        ResultDto<CmisCus0025RespDto> cmisCus0025ResultDto = new ResultDto<>();
        try {
            String certCode = cmisCus0025ReqDto.getCertCode();//高管证件号码
            String cusId = cmisCus0025ReqDto.getCusId();//客户号
            String serno = cmisCus0025ReqDto.getSerno();//流水
            String cusName = cmisCus0025ReqDto.getCusName();
            //查询条件不能全为空
            if (StringUtil.isEmpty(certCode) && StringUtil.isEmpty(cusId) && StringUtil.isEmpty(serno) && StringUtil.isEmpty(cusName)) {
                return cmisCus0025ResultDto;
            }
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, JSON.toJSONString(cmisCus0025ReqDto));
            cmisCus0025RespDto = cusLstYndAppService.selectdeatilYndCountByCertCode(cmisCus0025ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, JSON.toJSONString(cmisCus0025RespDto));
            // 封装cmisCus0025ResultDto中正确的返回码和返回信息
            cmisCus0025ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            cmisCus0025ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, e.getMessage());
            // 封装cmisCus0025ResultDto中异常返回码和返回信息
            cmisCus0025ResultDto.setCode(EpbEnum.EPB099999.key);
            cmisCus0025ResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装cmisCus0025RespDto到cmisCus0007ResultDto中
        cmisCus0025ResultDto.setData(cmisCus0025RespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0025.key, DscmsEnum.TRADE_CODE_CMISCUS0025.value, JSON.toJSONString(cmisCus0025ResultDto));
        return cmisCus0025ResultDto;
    }
}
