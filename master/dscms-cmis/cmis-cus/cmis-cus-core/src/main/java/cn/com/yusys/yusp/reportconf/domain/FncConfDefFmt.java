/*
 * 代码生成器自动生成的
 * Since 2008 - 2019
 *
 */
package cn.com.yusys.yusp.reportconf.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @项目名称: nrcs-busi-cms-core模块
 * @类名称: FncConfDefFmt
 * @类描述: FNC_CONF_DEF_FMT数据实体类
 * @功能描述:
 * @创建人: zzbankwb369
 * @创建时间: 2019-08-21 11:26:38
 * @修改备注:
 * @修改记录: 修改时间 修改人员 修改原因
 *        -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "FNC_CONF_DEF_FMT")
@ApiModel("报表配置定义")
public class FncConfDefFmt extends BaseDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	/** 报表样式编号 **/
	@Id
	@Column(name = "STYLE_ID",unique = true, nullable = false, length = 6)
	@ApiModelProperty("报表样式编号")
	private String styleId;

	/** 项目编号 **/
	@Id
	@Column(name = "ITEM_ID",unique = true, nullable = false, length = 20)
	@ApiModelProperty("项目编号")
	private String itemId;

	/** 顺序编号 **/
	@Column(name = "FNC_CONF_ORDER", unique = false, nullable = true, length = 0)
	@ApiModelProperty("顺序编号")
	private java.math.BigDecimal fncConfOrder;

	/** 栏位 **/
	@Column(name = "FNC_CONF_COTES", unique = false, nullable = true, length = 0)
	@ApiModelProperty("栏位")
	private Integer fncConfCotes;

	/** 行次标识 **/
	@Column(name = "FNC_CONF_ROW_FLG", unique = false, nullable = true, length = 1)
	@ApiModelProperty("行次标识")
	private String fncConfRowFlg;

	/** 层次 **/
	@Column(name = "FNC_CONF_INDENT", unique = false, nullable = true, length = 0)
	@ApiModelProperty("层次")
	private java.math.BigDecimal fncConfIndent;

	/** 前缀 **/
	@Column(name = "FNC_CONF_PREFIX", unique = false, nullable = true, length = 200)
	@ApiModelProperty("前缀 ")
	private String fncConfPrefix;

	/** 项目编辑方式 **/
	@Column(name = "FNC_ITEM_EDIT_TYP", unique = false, nullable = true, length = 1)
	@ApiModelProperty("项目编辑方式")
	private String fncItemEditTyp;

	/** 显示数值 **/
	@Column(name = "FNC_CONF_DISP_AMT", unique = false, nullable = true, length = 20)
	@ApiModelProperty("显示数值")
	private java.math.BigDecimal fncConfDispAmt;

	/** 检查公式 **/
	@Column(name = "FNC_CONF_CHK_FRM", unique = false, nullable = true, length = 2000)
	@ApiModelProperty("检查公式")
	private String fncConfChkFrm;

	/** 计算公式 **/
	@Column(name = "FNC_CONF_CAL_FRM", unique = false, nullable = true, length = 2000)
	@ApiModelProperty("计算公式")
	private String fncConfCalFrm;

	/** 追加行数 **/
	@Column(name = "FNC_CNF_APP_ROW", unique = false, nullable = true, length = 0)
	@ApiModelProperty("追加行数")
	private java.math.BigDecimal fncCnfAppRow;

	/** 默认现实类型 **/
	@Column(name = "FNC_CONF_DISP_TPY", unique = false, nullable = true, length = 2)
	@ApiModelProperty("默认现实类型")
	private String fncConfDispTpy;
	/*********** 辅助字段 ************/
	@Transient
	private String itemName;
	@Transient
	private String data1;
	@Transient
	private String data2;
	@Transient
	private String data3;
	@Transient
	private String tableName;
	@Transient
	private String postfix;
	@Transient
	private String cusId;
	@Transient
	private String statYear;
	@Transient
	private String statStyle;
	@Transient
	private String preSql;
	@Transient
	private Integer orderNum;
	@Transient
	private String data4;
	@Transient
	private String data5;
	@Transient
	private String data6;
	@Transient
	private String data7;
	@Transient
	private String data8;
	@Transient
	private String data9;
	@Transient
	private String data10;
	@Transient
	private String data11;
	@Transient
	private String data12;
	@Transient
	private String data13;
	@Transient
	private String data14;
	@Transient
	private String data15;
	@Transient
	private String data16;

	public FncConfDefFmt() {
		super();
	}

	/**
	 * @param styleId
	 */
	public void setStyleId(String styleId) {
		this.styleId = styleId;
	}

	/**
	 * @return styleId
	 */
	public String getStyleId() {
		return this.styleId;
	}

	/**
	 * @param itemId
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return itemId
	 */
	public String getItemId() {
		return this.itemId;
	}

	/**
	 * @param fncConfOrder
	 */
	public void setFncConfOrder(java.math.BigDecimal fncConfOrder) {
		this.fncConfOrder = fncConfOrder;
	}

	/**
	 * @return fncConfOrder
	 */
	public java.math.BigDecimal getFncConfOrder() {
		return this.fncConfOrder;
	}

	public Integer getFncConfCotes() {
		return fncConfCotes;
	}

	public void setFncConfCotes(Integer fncConfCotes) {
		this.fncConfCotes = fncConfCotes;
	}

	/**
	 * @param fncConfRowFlg
	 */
	public void setFncConfRowFlg(String fncConfRowFlg) {
		this.fncConfRowFlg = fncConfRowFlg;
	}

	/**
	 * @return fncConfRowFlg
	 */
	public String getFncConfRowFlg() {
		return this.fncConfRowFlg;
	}

	/**
	 * @param fncConfIndent
	 */
	public void setFncConfIndent(java.math.BigDecimal fncConfIndent) {
		this.fncConfIndent = fncConfIndent;
	}

	/**
	 * @return fncConfIndent
	 */
	public java.math.BigDecimal getFncConfIndent() {
		return this.fncConfIndent;
	}

	/**
	 * @param fncConfPrefix
	 */
	public void setFncConfPrefix(String fncConfPrefix) {
		this.fncConfPrefix = fncConfPrefix;
	}

	/**
	 * @return fncConfPrefix
	 */
	public String getFncConfPrefix() {
		return this.fncConfPrefix;
	}

	/**
	 * @param fncItemEditTyp
	 */
	public void setFncItemEditTyp(String fncItemEditTyp) {
		this.fncItemEditTyp = fncItemEditTyp;
	}

	/**
	 * @return fncItemEditTyp
	 */
	public String getFncItemEditTyp() {
		return this.fncItemEditTyp;
	}

	/**
	 * @param fncConfDispAmt
	 */
	public void setFncConfDispAmt(java.math.BigDecimal fncConfDispAmt) {
		this.fncConfDispAmt = fncConfDispAmt;
	}

	/**
	 * @return fncConfDispAmt
	 */
	public java.math.BigDecimal getFncConfDispAmt() {
		return this.fncConfDispAmt;
	}

	/**
	 * @param fncConfChkFrm
	 */
	public void setFncConfChkFrm(String fncConfChkFrm) {
		this.fncConfChkFrm = fncConfChkFrm;
	}

	/**
	 * @return fncConfChkFrm
	 */
	public String getFncConfChkFrm() {
		return this.fncConfChkFrm;
	}

	/**
	 * @param fncConfCalFrm
	 */
	public void setFncConfCalFrm(String fncConfCalFrm) {
		this.fncConfCalFrm = fncConfCalFrm;
	}

	/**
	 * @return fncConfCalFrm
	 */
	public String getFncConfCalFrm() {
		return this.fncConfCalFrm;
	}

	/**
	 * @param fncCnfAppRow
	 */
	public void setFncCnfAppRow(java.math.BigDecimal fncCnfAppRow) {
		this.fncCnfAppRow = fncCnfAppRow;
	}

	/**
	 * @return fncCnfAppRow
	 */
	public java.math.BigDecimal getFncCnfAppRow() {
		return this.fncCnfAppRow;
	}

	/**
	 * @param fncConfDispTpy
	 */
	public void setFncConfDispTpy(String fncConfDispTpy) {
		this.fncConfDispTpy = fncConfDispTpy;
	}

	/**
	 * @return fncConfDispTpy
	 */
	public String getFncConfDispTpy() {
		return this.fncConfDispTpy;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getStatYear() {
		return statYear;
	}

	public void setStatYear(String statYear) {
		this.statYear = statYear;
	}

	public String getStatStyle() {
		return statStyle;
	}

	public void setStatStyle(String statStyle) {
		this.statStyle = statStyle;
	}

	public String getPreSql() {
		return preSql;
	}

	public void setPreSql(String preSql) {
		this.preSql = preSql;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public String getData3() {
		return data3;
	}

	public void setData3(String data3) {
		this.data3 = data3;
	}

	public String getData4() {
		return data4;
	}

	public void setData4(String data4) {
		this.data4 = data4;
	}

	public String getData5() {
		return data5;
	}

	public void setData5(String data5) {
		this.data5 = data5;
	}

	public String getData6() {
		return data6;
	}

	public void setData6(String data6) {
		this.data6 = data6;
	}

	public String getData7() {
		return data7;
	}

	public void setData7(String data7) {
		this.data7 = data7;
	}

	public String getData8() {
		return data8;
	}

	public void setData8(String data8) {
		this.data8 = data8;
	}

	public String getData9() {
		return data9;
	}

	public void setData9(String data9) {
		this.data9 = data9;
	}

	public String getData10() {
		return data10;
	}

	public void setData10(String data10) {
		this.data10 = data10;
	}

	public String getData11() {
		return data11;
	}

	public void setData11(String data11) {
		this.data11 = data11;
	}

	public String getData12() {
		return data12;
	}

	public void setData12(String data12) {
		this.data12 = data12;
	}

	public String getData13() {
		return data13;
	}

	public void setData13(String data13) {
		this.data13 = data13;
	}

	public String getData14() {
		return data14;
	}

	public void setData14(String data14) {
		this.data14 = data14;
	}

	public String getData15() {
		return data15;
	}

	public void setData15(String data15) {
		this.data15 = data15;
	}

	public String getData16() {
		return data16;
	}

	public void setData16(String data16) {
		this.data16 = data16;
	}

}