package cn.com.yusys.yusp.service.server.xdkh0035;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.CusIntbank;
import cn.com.yusys.yusp.dto.server.xdkh0035.req.Xdkh0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0035.resp.Xdkh0035DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsCusEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CusCorpMapper;
import cn.com.yusys.yusp.repository.mapper.CusIntbankMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus模块
 * @类名称: Xdkh0035Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xll
 * @创建时间: 2021-05-11 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdkh0035Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdkh0035.Xdkh0035Service.class);

    @Resource
    private CusCorpMapper cusCorpMapper;
    @Resource
    private CusIntbankMapper cusIntbankMapper;

    /**
     * 信息锁定标志同步
     *
     * @param xdkh0035DataReqDto
     * @return
     */
    @Transactional
    public Xdkh0035DataRespDto xdkh0035(Xdkh0035DataReqDto xdkh0035DataReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, JSON.toJSONString(xdkh0035DataReqDto));
        Xdkh0035DataRespDto xdkh0035DataRespDto = new Xdkh0035DataRespDto();
        //对公客户表
        CusCorp cusCorp = new CusCorp();
        //同业客户信息表
        CusIntbank cusIntbank = new CusIntbank();
        String cusId = xdkh0035DataReqDto.getCusId();//客户编号
        String cusName = "";//客户名称
        String bankCreditLvl = xdkh0035DataReqDto.getBankCreditLvl();//本行即期信用等级
        BigDecimal avgBreachProblyPD = xdkh0035DataReqDto.getAvgBreachProblyPD();//平均违约概率PD
        String evalStartDate = xdkh0035DataReqDto.getEvalStartDate();//评级生效日
        String evalEndDate = xdkh0035DataReqDto.getEvalEndDate();//评级到期日
        String dtghFlag = xdkh0035DataReqDto.getDtghFlag();//区分标识
        //默认成功
        xdkh0035DataRespDto.setOpFlag(DscmsCusEnum.RETURN_SUCCESS.key);
        xdkh0035DataRespDto.setOpMsg(DscmsCusEnum.RETURN_SUCCESS.value);
        try {
            if("1".equals(dtghFlag)){
                //公司客户,查询cus_com表的客户名称
                CusCorp cusCorpInfo = cusCorpMapper.selectByPrimaryKey(cusId);
                if (cusCorpInfo != null && null != cusCorpInfo.getCusName() && !"".equals(cusCorpInfo.getCusName())){
                    cusName = (String)cusCorpInfo.getCusName();
                }
            }else if("2".equals(dtghFlag)){
                //同业客户，查询同业客户Cus_Intbank的客户名称
                CusIntbank cusIntbankInfo = cusIntbankMapper.selectByPrimaryKey(cusId);
                if (cusIntbankInfo != null && null != cusIntbankInfo.getCusName() && !"".equals(cusIntbankInfo.getCusName())){
                    cusName = (String)cusIntbankInfo.getCusName();
                }
            }else{
                xdkh0035DataRespDto.setOpFlag(DscmsCusEnum.RETURN_FAIL.key);
                xdkh0035DataRespDto.setOpMsg(DscmsCusEnum.RETURN_FAIL.value);
            }
            //返回结果
            int result = 0;
            //查询条件
            Map map = new HashMap<>();
            map.put("cusId", cusId);//客户号
            map.put("bankCreditLvl", bankCreditLvl);//本行即期信用等级
            map.put("avgBreachProblyPD", avgBreachProblyPD);//平均违约概率PD

            if ("1".equals(dtghFlag)) {//1 公司客户
                result = cusCorpMapper.updateBhjqxydjAndPdBycusId(map);
            } else if ("2".equals(dtghFlag)) {//2同业客户
                result = cusIntbankMapper.updateGradeRankAndPdBycusId(map);
            } else{
                xdkh0035DataRespDto.setOpFlag(DscmsCusEnum.RETURN_FAIL.key);
                xdkh0035DataRespDto.setOpMsg(DscmsCusEnum.RETURN_FAIL.value);
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, e.getMessage());
            throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, JSON.toJSONString(xdkh0035DataReqDto));
        return xdkh0035DataRespDto;
    }
}
