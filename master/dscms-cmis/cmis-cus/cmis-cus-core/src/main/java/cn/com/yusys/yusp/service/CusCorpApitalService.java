/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CusCorpApital;
import cn.com.yusys.yusp.repository.mapper.CusCorpApitalMapper;

/**
 * @项目名称: cmis-cus模块
 * @类名称: CusCorpApitalService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-23 09:49:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class CusCorpApitalService {

    @Autowired
    private CusCorpApitalMapper cusCorpApitalMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CusCorpApital selectByPrimaryKey(String pkId) {
        return cusCorpApitalMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CusCorpApital> selectAll(QueryModel model) {
        List<CusCorpApital> records = (List<CusCorpApital>) cusCorpApitalMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CusCorpApital> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CusCorpApital> list = cusCorpApitalMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CusCorpApital record) {
        return cusCorpApitalMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CusCorpApital record) {
        return cusCorpApitalMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CusCorpApital record) {
        return cusCorpApitalMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CusCorpApital record) {
        return cusCorpApitalMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return cusCorpApitalMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cusCorpApitalMapper.deleteByIds(ids);
    }

    /**
     * 获取股东信息
     * @param certTyp
     * @param certCode
     * @return
     */
    public CusCorpApital selectCusCorpApiByCodeAndType(String certTyp, String certCode) {
        return cusCorpApitalMapper.selectCusCorpApiByCodeAndType(certTyp,certCode);
    }

    /**
     * 获取股东信息
     * @param cusCorpRelationsRequestDto
     * @return
     */
    public CusCorpApiDto cusCorpApiRelations(CusCorpRelationsRequestDto cusCorpRelationsRequestDto) {
        List<CusCorpRelationsDto> cusCorpRelationsDtos = cusCorpApitalMapper.selectCusCorpApiComDto(cusCorpRelationsRequestDto);
        if(CollectionUtils.isEmpty(cusCorpRelationsDtos) && cusCorpRelationsDtos.size() <= 0){
            return null;
        }

        CusCorpApiDto cusCorpApiDto = new CusCorpApiDto();
        cusCorpApiDto.setUpperCusId(cusCorpRelationsRequestDto.getCusId());
        cusCorpApiDto.setCusCorpRelationsDtos(cusCorpRelationsDtos);
        return cusCorpApiDto;
    }

    /**
     * 根据关联客户号查询出资人信息
     * @param cusIdRel
     * @return
     */
    public List<Map<String,String>> queryCusInfoByCusIdRel(String cusIdRel) {
        return cusCorpApitalMapper.queryCusInfoByCusIdRel(cusIdRel);
    }

    /**
     * 根据关联客户号查询
     * @param cusIdRel
     * @return
     */
    public List<CusCorpApitalDto> selectByCusIdRel(String cusIdRel){
        return  cusCorpApitalMapper.selectByCusIdRel(cusIdRel);
    }
}
