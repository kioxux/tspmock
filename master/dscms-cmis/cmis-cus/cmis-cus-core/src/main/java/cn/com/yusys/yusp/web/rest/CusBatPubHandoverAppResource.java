/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiParam;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CusBatPubHandoverApp;
import cn.com.yusys.yusp.service.CusBatPubHandoverAppService;

import javax.servlet.http.HttpServletResponse;

/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBatPubHandoverAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-24 21:53:25
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cusbatpubhandoverapp")
public class CusBatPubHandoverAppResource {
    private static final String FILE_PATH_SUFFIX = ".xlsx";
    @Autowired
    private CusBatPubHandoverAppService cusBatPubHandoverAppService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CusBatPubHandoverApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CusBatPubHandoverApp> list = cusBatPubHandoverAppService.selectAll(queryModel);
        return new ResultDto<List<CusBatPubHandoverApp>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryByModel")
    protected ResultDto<List<CusBatPubHandoverApp>> index(@RequestBody QueryModel queryModel) {
        queryModel.setSort("inputDate desc");
        List<CusBatPubHandoverApp> list = cusBatPubHandoverAppService.selectByModel(queryModel);
        return new ResultDto<List<CusBatPubHandoverApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{cbphaSerno}")
    protected ResultDto<CusBatPubHandoverApp> show(@PathVariable("cbphaSerno") String cbphaSerno) {
        CusBatPubHandoverApp cusBatPubHandoverApp = cusBatPubHandoverAppService.selectByPrimaryKey(cbphaSerno);
        return new ResultDto<CusBatPubHandoverApp>(cusBatPubHandoverApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CusBatPubHandoverApp> create(@RequestBody CusBatPubHandoverApp cusBatPubHandoverApp) throws URISyntaxException {
        cusBatPubHandoverAppService.insert(cusBatPubHandoverApp);
        return new ResultDto<CusBatPubHandoverApp>(cusBatPubHandoverApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CusBatPubHandoverApp cusBatPubHandoverApp) throws URISyntaxException {
        int result = cusBatPubHandoverAppService.update(cusBatPubHandoverApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{cbphaSerno}")
    protected ResultDto<Integer> delete(@PathVariable("cbphaSerno") String cbphaSerno) {
        int result = cusBatPubHandoverAppService.deleteByPrimaryKey(cbphaSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cusBatPubHandoverAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 下单批量导入清单
     *
     * @return void
     */
    @GetMapping("/download")
    protected void downloadExcel(@ApiParam(value = "excel template file download", required = true) HttpServletResponse response) throws Exception {
        InputStream inputStream = cusBatPubHandoverAppService.getExcelTemp();
        OutputStream outputStream = response.getOutputStream();
        response.setContentType("application/x-download");
        response.addHeader("Content-Disposition", "attachment;filename=" + "客户批量移交清单" + FILE_PATH_SUFFIX);
        IOUtils.copy(inputStream, outputStream);
        outputStream.flush();
        outputStream.close();
    }
}
