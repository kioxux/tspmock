package cn.com.yusys.yusp.service.server.cmiscus0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CusCorp;
import cn.com.yusys.yusp.domain.FncStatCfs;
import cn.com.yusys.yusp.domain.FncStatIs;
import cn.com.yusys.yusp.dto.server.cmiscus0005.req.CmisCus0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0005.resp.CmisCus0005RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CusCorpService;
import cn.com.yusys.yusp.service.FncStatCfsService;
import cn.com.yusys.yusp.service.FncStatIsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

import static cn.com.yusys.yusp.enums.returncode.EpbEnum.EPB099999;


/**
 * 业务逻辑处理类：客户销售总额查询
 *
 * @author dumdrepoproconfig
 * @version 1.0
 * @since 2021年6月3日
 */
@Service
public class CmisCus0005Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisCus0005Service.class);

    @Autowired
    private FncStatIsService fncStatIsService;
    @Autowired
    private CusCorpService cusCorpService;


    @Transactional
    public CmisCus0005RespDto execute(CmisCus0005ReqDto reqDto) throws YuspException {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0005.key, DscmsEnum.TRADE_CODE_CMISCUS0005.value);

        CmisCus0005RespDto respDto = new CmisCus0005RespDto();
        String cusId = reqDto.getCusId();
        if(Objects.isNull(cusId)){
            throw BizException.error(null, EpbEnum.EPB090009.key, "客户代码custid不能为空");
        }
        //上年度销售总额
        BigDecimal statLastYearAmt = BigDecimal.ZERO;
        //当年销售总额
        BigDecimal statCurYearAmt = BigDecimal.ZERO;
        //上年度销售总额和当年销售总额较大值
        BigDecimal statYearAmtMax = BigDecimal.ZERO;

        try{
            CusCorp cusCorp = cusCorpService.selectByPrimaryKey(cusId);
            Calendar cal = Calendar.getInstance();
            int month = cal.get(Calendar.MONTH)+1;
            int year = cal.get(Calendar.YEAR);
            int lastYear = year-1;
            //老会计准则科目:L01000000-主营业务收入 新会计准则科目:XSL0001-营业收入
            //老会计准则科目:PB0001 新会计准则PB0005
            String statItemId  ="L01000000";
            if(Objects.nonNull(cusCorp) && "PB0005".equals(cusCorp.getFinaReportType())){
                statItemId = "XSL0001";
            }
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("cusId",cusId);
            //FNC_STAT_IS 损益表
            queryModel.addCondition("statItemId",statItemId);
            //报表年
            queryModel.addCondition("statYear",String.valueOf(lastYear));

            List<FncStatIs> fncStatIsList = fncStatIsService.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(fncStatIsList)){
                statLastYearAmt = fncStatIsList.get(0).getStatEndAmtY(); //期末数
            }

            QueryModel queryModeThisYear = new QueryModel();
            queryModel.addCondition("cusId",cusId);
            //FNC_STAT_IS 损益表
            queryModel.addCondition("statItemId",statItemId); //老会计准则科目:L01000000-主营业务收入 新会计准则客户:XSL0001-营业收入
            //报表年
            queryModel.addCondition("statYear",String.valueOf(year));
            List<FncStatIs> fncStatIsListThisYear = fncStatIsService.selectByModel(queryModel);
            if (!CollectionUtils.isEmpty(fncStatIsListThisYear)){
                FncStatIs fncStatIs =fncStatIsListThisYear.get(0);
                Map<String,BigDecimal> map = new HashMap<>();
                map.put("1", fncStatIs.getStatInitAmt1());
                map.put("2", fncStatIs.getStatInitAmt2());
                map.put("3", fncStatIs.getStatInitAmt3());
                map.put("4", fncStatIs.getStatInitAmt4());
                map.put("5", fncStatIs.getStatInitAmt5());
                map.put("6", fncStatIs.getStatInitAmt6());
                map.put("7", fncStatIs.getStatInitAmt7());
                map.put("8", fncStatIs.getStatInitAmt8());
                map.put("9", fncStatIs.getStatInitAmt9());
                map.put("10",fncStatIs.getStatInitAmt10());
                map.put("11",fncStatIs.getStatInitAmt11());
                map.put("12",fncStatIs.getStatInitAmt12());
                statCurYearAmt = getStatCurYearAmt(map,month);
                //如果一月期初数没有值，则当年销售总额取上年度销售总额
                if (statCurYearAmt==null){
                    statCurYearAmt = statLastYearAmt;
                }
            }

            if (statLastYearAmt==null){
                statLastYearAmt = BigDecimal.ZERO;
            }

            if (statCurYearAmt==null){
                statCurYearAmt = BigDecimal.ZERO;
            }

            if (statLastYearAmt.compareTo(statCurYearAmt)>0){
                statYearAmtMax = statLastYearAmt;
            }else{
                statYearAmtMax = statCurYearAmt;
            }

            respDto.setStatCurYearAmt(statCurYearAmt);
            respDto.setStatLastYearAmt(statLastYearAmt);
            respDto.setStatYearAmtMax(statYearAmtMax);
            respDto.setErrorCode(SuccessEnum.SUCCESS.key);
            respDto.setErrorMsg(SuccessEnum.SUCCESS.value);
        } catch (YuspException e) {
            throw BizException.error(null, EPB099999.key, EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0005.key, DscmsEnum.TRADE_CODE_CMISCUS0005.value);
        return respDto;
    }

    /**
     * 如果当前月的月期初数为null，则递归获取上个月的月期初数，直到一月的月期初数
     * @param map
     * @param month
     * @return
     */
    private BigDecimal getStatCurYearAmt(Map<String,BigDecimal> map,int month){

        BigDecimal statInitAmt = map.get(String.valueOf(month));

        if (statInitAmt==null || statInitAmt.compareTo(BigDecimal.ZERO)==0){
             if (month==1){
                 return null;
             }else{
                 return getStatCurYearAmt(map, month-1);
             }
        }else{
            return statInitAmt;
        }
    }
}