package cn.com.yusys.yusp.dto.server.cmiscus0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：查询个人客户基本信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0007RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusIndivCacheKey")
    private String cusIndivCacheKey;//个人客户基本信息对应的缓存Key
    @JsonProperty(value = "cusIndivList")
    private List<CusIndivDto> cusIndivList;

    public String getCusIndivCacheKey() {
        return cusIndivCacheKey;
    }

    public void setCusIndivCacheKey(String cusIndivCacheKey) {
        this.cusIndivCacheKey = cusIndivCacheKey;
    }

    public List<CusIndivDto> getCusIndivList() {
        return cusIndivList;
    }

    public void setCusIndivList(List<CusIndivDto> cusIndivList) {
        this.cusIndivList = cusIndivList;
    }

    @Override
    public String toString() {
        return "CmisCus0007RespDto{" +
                "cusIndivCacheKey='" + cusIndivCacheKey + '\'' +
                ", cusIndivList=" + cusIndivList +
                '}';
    }
}
