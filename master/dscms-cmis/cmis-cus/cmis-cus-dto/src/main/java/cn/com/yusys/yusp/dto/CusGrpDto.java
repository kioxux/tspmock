package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-lmt-core模块
 * @类名称: CusGrp
 * @类描述: cus_grp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-29 20:42:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusGrpDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 集团编号 **/
	private String grpNo;
	
	/** 集团名称 **/
	private String grpName;
	
	/** 核心客户编号 **/
	private String coreCusId;
	
	/** 核心客户中征码 **/
	private String coreCusLoanCardId;
	
	/** 核心客户名称 **/
	private String coreCusName;
	
	/** 核心客户证件号码 **/
	private String coreCusCertNo;
	
	/** 更新办公地址日期 **/
	private String updateOfficeAddrDate;
	
	/** 办公地址 **/
	private String officeAddr;
	
	/** 社会信用代码 **/
	private String socialCreditCode;
	
	/** 工商登记注册号 **/
	private String businessCirclesRegiNo;
	
	/** 办公地址行政区划 **/
	private String officeAddrAdminDiv;
	
	/** 集团紧密程度 **/
	private String grpCloselyDegree;
	
	/** 集团客户情况说明 **/
	private String grpCaseMemo;
	
	/** 管户客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 集团客户状态 **/
	private String groupCusStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记时间 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;


	/**
	 * @param grpNo
	 */
	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo == null ? null : grpNo.trim();
	}

    /**
     * @return GrpNo
     */
	public String getGrpNo() {
		return this.grpNo;
	}

	/**
	 * @param grpName
	 */
	public void setGrpName(String grpName) {
		this.grpName = grpName == null ? null : grpName.trim();
	}

    /**
     * @return GrpName
     */
	public String getGrpName() {
		return this.grpName;
	}

	/**
	 * @param coreCusId
	 */
	public void setCoreCusId(String coreCusId) {
		this.coreCusId = coreCusId == null ? null : coreCusId.trim();
	}

    /**
     * @return CoreCusId
     */
	public String getCoreCusId() {
		return this.coreCusId;
	}

	/**
	 * @param coreCusLoanCardId
	 */
	public void setCoreCusLoanCardId(String coreCusLoanCardId) {
		this.coreCusLoanCardId = coreCusLoanCardId == null ? null : coreCusLoanCardId.trim();
	}

    /**
     * @return CoreCusLoanCardId
     */
	public String getCoreCusLoanCardId() {
		return this.coreCusLoanCardId;
	}

	/**
	 * @param coreCusName
	 */
	public void setCoreCusName(String coreCusName) {
		this.coreCusName = coreCusName == null ? null : coreCusName.trim();
	}

    /**
     * @return CoreCusName
     */
	public String getCoreCusName() {
		return this.coreCusName;
	}

	/**
	 * @param coreCusCertNo
	 */
	public void setCoreCusCertNo(String coreCusCertNo) {
		this.coreCusCertNo = coreCusCertNo == null ? null : coreCusCertNo.trim();
	}

    /**
     * @return CoreCusCertNo
     */
	public String getCoreCusCertNo() {
		return this.coreCusCertNo;
	}

	/**
	 * @param updateOfficeAddrDate
	 */
	public void setUpdateOfficeAddrDate(String updateOfficeAddrDate) {
		this.updateOfficeAddrDate = updateOfficeAddrDate == null ? null : updateOfficeAddrDate.trim();
	}

    /**
     * @return UpdateOfficeAddrDate
     */
	public String getUpdateOfficeAddrDate() {
		return this.updateOfficeAddrDate;
	}

	/**
	 * @param officeAddr
	 */
	public void setOfficeAddr(String officeAddr) {
		this.officeAddr = officeAddr == null ? null : officeAddr.trim();
	}

    /**
     * @return OfficeAddr
     */
	public String getOfficeAddr() {
		return this.officeAddr;
	}

	/**
	 * @param socialCreditCode
	 */
	public void setSocialCreditCode(String socialCreditCode) {
		this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
	}

    /**
     * @return SocialCreditCode
     */
	public String getSocialCreditCode() {
		return this.socialCreditCode;
	}

	/**
	 * @param businessCirclesRegiNo
	 */
	public void setBusinessCirclesRegiNo(String businessCirclesRegiNo) {
		this.businessCirclesRegiNo = businessCirclesRegiNo == null ? null : businessCirclesRegiNo.trim();
	}

    /**
     * @return BusinessCirclesRegiNo
     */
	public String getBusinessCirclesRegiNo() {
		return this.businessCirclesRegiNo;
	}

	/**
	 * @param officeAddrAdminDiv
	 */
	public void setOfficeAddrAdminDiv(String officeAddrAdminDiv) {
		this.officeAddrAdminDiv = officeAddrAdminDiv == null ? null : officeAddrAdminDiv.trim();
	}

    /**
     * @return OfficeAddrAdminDiv
     */
	public String getOfficeAddrAdminDiv() {
		return this.officeAddrAdminDiv;
	}

	/**
	 * @param grpCloselyDegree
	 */
	public void setGrpCloselyDegree(String grpCloselyDegree) {
		this.grpCloselyDegree = grpCloselyDegree == null ? null : grpCloselyDegree.trim();
	}

    /**
     * @return GrpCloselyDegree
     */
	public String getGrpCloselyDegree() {
		return this.grpCloselyDegree;
	}

	/**
	 * @param grpCaseMemo
	 */
	public void setGrpCaseMemo(String grpCaseMemo) {
		this.grpCaseMemo = grpCaseMemo == null ? null : grpCaseMemo.trim();
	}

    /**
     * @return GrpCaseMemo
     */
	public String getGrpCaseMemo() {
		return this.grpCaseMemo;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}

    /**
     * @return ManagerId
     */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}

    /**
     * @return ManagerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param groupCusStatus
	 */
	public void setGroupCusStatus(String groupCusStatus) {
		this.groupCusStatus = groupCusStatus == null ? null : groupCusStatus.trim();
	}

    /**
     * @return GroupCusStatus
     */
	public String getGroupCusStatus() {
		return this.groupCusStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

    /**
     * @return InputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

    /**
     * @return InputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

    /**
     * @return InputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

    /**
     * @return UpdId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

    /**
     * @return UpdBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

    /**
     * @return UpdDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return CreateTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return UpdateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}