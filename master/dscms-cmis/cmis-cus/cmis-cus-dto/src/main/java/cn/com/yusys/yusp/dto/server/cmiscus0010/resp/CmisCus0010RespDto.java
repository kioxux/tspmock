package cn.com.yusys.yusp.dto.server.cmiscus0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：根据客户编号,证件类型，证件号码查询同业客户基本信息
 *
 * @author 蒋云龙
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0010RespDto {
    private static final long serialVersionUID = 1L;

    /** 客户编号 **/
    @JsonProperty(value = "cusId")
    private String cusId;

    /** 同业机构(行)名称 **/
    @JsonProperty(value = "cusName")
    private String cusName;

    /** 同业机构(行)英文名称 **/
    @JsonProperty(value = "cusEnName")
    private String cusEnName;

    /** 同业机构类型 STD_ZB_INTB_TYPE **/
    @JsonProperty(value = "intbankType")
    private String intbankType;

    /** 同业机构子类型 STD_ZB_INTB_SUB_TYPE **/
    @JsonProperty(value = "intbankSubType")
    private String intbankSubType;

    /** 大额行号 **/
    @JsonProperty(value = "largeBankNo")
    private String largeBankNo;

    /** Swift代码 **/
    @JsonProperty(value = "swiftCde")
    private String swiftCde;

    /** 证件类型 **/
    @JsonProperty(value = "certType")
    private String certType;

    /** 证件号码 **/
    @JsonProperty(value = "certCode")
    private String certCode;

    /** 国民经济部门编号 **/
    @JsonProperty(value = "natEcoSec")
    private String natEcoSec;

    /** 控股类型 STD_ZB_HOLD_TYPE **/
    @JsonProperty(value = "holdType")
    private String holdType;

    /** 客户规模 **/
    @JsonProperty(value = "cusScale")
    private String cusScale;

    /** 金融业企业分类 **/
    @JsonProperty(value = "fnaCllTyp")
    private String fnaCllTyp;

    /** 中证码/贷款卡号 **/
    @JsonProperty(value = "loanCardId")
    private String loanCardId;

    /** 总行组织机构代码 **/
    @JsonProperty(value = "headInsCde")
    private String headInsCde;

    /** 国别 **/
    @JsonProperty(value = "country")
    private String country;

    /** 法人姓名 **/
    @JsonProperty(value = "legalName")
    private String legalName;

    /** 法人证件类型 STD_ZB_CERT_TYP **/
    @JsonProperty(value = "legalCertType")
    private String legalCertType;

    /** 法人证件号码 **/
    @JsonProperty(value = "legalCertCode")
    private String legalCertCode;

    /** 法人性别 STD_ZB_SEX **/
    @JsonProperty(value = "legalSex")
    private String legalSex;

    /** 法人出生日期 **/
    @JsonProperty(value = "legalBirth")
    private String legalBirth;

    /** 税务登记证号（国税） **/
    @JsonProperty(value = "natTaxRegCde")
    private String natTaxRegCde;

    /** 税务登记证号（地税） **/
    @JsonProperty(value = "locTaxRegCde")
    private String locTaxRegCde;

    /** 营业执照号码 **/
    @JsonProperty(value = "regCde")
    private String regCde;

    /** 总资产(万元) **/
    @JsonProperty(value = "ressetAmt")
    private java.math.BigDecimal ressetAmt;

    /** 注册/开办资金币种 STD_ZB_CUR_TYP **/
    @JsonProperty(value = "regCapCurType")
    private String regCapCurType;

    /** 注册/开办资金 **/
    @JsonProperty(value = "regCapAmt")
    private java.math.BigDecimal regCapAmt;

    /** 实收资本金额 **/
    @JsonProperty(value = "paidCapAmt")
    private java.math.BigDecimal paidCapAmt;

    /** 所有者权益(万元) **/
    @JsonProperty(value = "owersEquity")
    private java.math.BigDecimal owersEquity;

    /** 基本账户开户行 **/
    @JsonProperty(value = "basAccBank")
    private String basAccBank;

    /** 基本账户账号 **/
    @JsonProperty(value = "basAccNo")
    private String basAccNo;

    /** 是否上市/上市公司标志 STD_ZB_YES_NO **/
    @JsonProperty(value = "mrkFlg")
    private String mrkFlg;

    /** 上市地1 STD_ZB_MRK_AREA **/
    @JsonProperty(value = "mrkAreaOne")
    private String mrkAreaOne;

    /** 上市地2 STD_ZB_MRK_AREA **/
    @JsonProperty(value = "mrkAreaTwo")
    private String mrkAreaTwo;

    /** 股票代码 **/
    @JsonProperty(value = "stkCode")
    private String stkCode;

    /** 同业机构(行)成立日 **/
    @JsonProperty(value = "intbankBuildDate")
    private String intbankBuildDate;

    /** 金融业务许可证 **/
    @JsonProperty(value = "bankProLic")
    private String bankProLic;

    /** 同业机构行网址 **/
    @JsonProperty(value = "intbankSite")
    private String intbankSite;

    /** 通讯地址省市区/县 **/
    @JsonProperty(value = "contArea")
    private String contArea;

    /** 通讯地址村 **/
    @JsonProperty(value = "contCountry")
    private String contCountry;

    /** 通讯地址乡/镇 **/
    @JsonProperty(value = "contTown")
    private String contTown;

    /** 通讯地址 **/
    @JsonProperty(value = "contAddr")
    private String contAddr;

    /** 邮箱 **/
    @JsonProperty(value = "email")
    private String email;

    /** 监管评级 STD_ZB_SUPE_EVAL **/
    @JsonProperty(value = "supeEval")
    private String supeEval;

    /** 外部评级 STD_ZB_EVAL_RST **/
    @JsonProperty(value = "intbankEval")
    private String intbankEval;

    /** 评级到期日期 **/
    @JsonProperty(value = "evalEndDt")
    private String evalEndDt;

    /** 拥有资质名称 **/
    @JsonProperty(value = "owmAptName")
    private String owmAptName;

    /** 与我行合作关系 **/
    @JsonProperty(value = "relDrg")
    private String relDrg;

    /** 客户状态 STD_ZB_CUS_ST **/
    @JsonProperty(value = "cusState")
    private String cusState;

    /** 主管机构 **/
    @JsonProperty(value = "managerBrId")
    private String managerBrId;

    /** 操作类型  STD_ZB_OPR_TYPE **/
    @JsonProperty(value = "oprType")
    private String oprType;

    /** 社会信用代码 **/
    @JsonProperty(value = "socialCreditCode")
    private String socialCreditCode;

    /** 机构简称 **/
    @JsonProperty(value = "orgForShort")
    private String orgForShort;

    /** 评定机构外部STD_ZB_OUT_APPR_ORG   **/
    @JsonProperty(value = "outApprOrg")
    private String outApprOrg;

    /** 同业非现场监管统计机构编码 **/
    @JsonProperty(value = "intbankOsssOrgCode")
    private String intbankOsssOrgCode;

    /** 是否长期STD_ZB_YES_NO **/
    @JsonProperty(value = "isLt")
    private String isLt;

    /** 经营地区 **/
    @JsonProperty(value = "operArea")
    private String operArea;

    /** 机构类型说明 **/
    @JsonProperty(value = "orgTypeMemo")
    private String orgTypeMemo;

    /** 管户客户经理 **/
    @JsonProperty(value = "managerId")
    private String managerId;

    /** 行业分类 **/
    @JsonProperty(value = "tradeClass")
    private String tradeClass;

    /** 营业执照到期日 **/
    @JsonProperty(value = "expDateEnd")
    private String expDateEnd;

    /** 注册地行政区划 **/
    @JsonProperty(value = "regiAreaCode")
    private String regiAreaCode;

    /** 注册地址 **/
    @JsonProperty(value = "comRegAdd")
    private String comRegAdd;

    /** 经营范围 **/
    @JsonProperty(value = "natBusi")
    private String natBusi;

    /** 主联系人电话 **/
    @JsonProperty(value = "linkmanPhone")
    private String linkmanPhone;

    /** 锁定状态 **/
    @JsonProperty(value = "islocked")
    private String islocked;

    @JsonProperty(value = "finalRank")
    private String  finalRank;//信用评级最终等级
    @JsonProperty(value = "dueDt")
    private String  dueDt;//非零内评到期日期
    @JsonProperty(value = "effDt")
    private String  effDt;//非零内评到期日期

    @JsonProperty(value = "cusIntbankMgrList")
    private List<CusIntbankMgrDto> cusIntbankMgrList;

    @JsonProperty(value = "cusCorpApitalDtoList")
    private List<CusCorpApitalDto> cusCorpApitalDtoList;

    public List<CusIntbankMgrDto> getCusIntbankMgrList() {
        return cusIntbankMgrList;
    }

    public void setCusIntbankMgrList(List<CusIntbankMgrDto> cusIntbankMgrList) {
        this.cusIntbankMgrList = cusIntbankMgrList;
    }

    public List<CusCorpApitalDto> getCusCorpApitalDtoList() {
        return cusCorpApitalDtoList;
    }

    public void setCusCorpApitalDtoList(List<CusCorpApitalDto> cusCorpApitalDtoList) {
        this.cusCorpApitalDtoList = cusCorpApitalDtoList;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusEnName() {
        return cusEnName;
    }

    public void setCusEnName(String cusEnName) {
        this.cusEnName = cusEnName;
    }

    public String getIntbankType() {
        return intbankType;
    }

    public void setIntbankType(String intbankType) {
        this.intbankType = intbankType;
    }

    public String getIntbankSubType() {
        return intbankSubType;
    }

    public void setIntbankSubType(String intbankSubType) {
        this.intbankSubType = intbankSubType;
    }

    public String getLargeBankNo() {
        return largeBankNo;
    }

    public void setLargeBankNo(String largeBankNo) {
        this.largeBankNo = largeBankNo;
    }

    public String getSwiftCde() {
        return swiftCde;
    }

    public void setSwiftCde(String swiftCde) {
        this.swiftCde = swiftCde;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getNatEcoSec() {
        return natEcoSec;
    }

    public void setNatEcoSec(String natEcoSec) {
        this.natEcoSec = natEcoSec;
    }

    public String getHoldType() {
        return holdType;
    }

    public void setHoldType(String holdType) {
        this.holdType = holdType;
    }

    public String getCusScale() {
        return cusScale;
    }

    public void setCusScale(String cusScale) {
        this.cusScale = cusScale;
    }

    public String getFnaCllTyp() {
        return fnaCllTyp;
    }

    public void setFnaCllTyp(String fnaCllTyp) {
        this.fnaCllTyp = fnaCllTyp;
    }

    public String getLoanCardId() {
        return loanCardId;
    }

    public void setLoanCardId(String loanCardId) {
        this.loanCardId = loanCardId;
    }

    public String getHeadInsCde() {
        return headInsCde;
    }

    public void setHeadInsCde(String headInsCde) {
        this.headInsCde = headInsCde;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getLegalCertType() {
        return legalCertType;
    }

    public void setLegalCertType(String legalCertType) {
        this.legalCertType = legalCertType;
    }

    public String getLegalCertCode() {
        return legalCertCode;
    }

    public void setLegalCertCode(String legalCertCode) {
        this.legalCertCode = legalCertCode;
    }

    public String getLegalSex() {
        return legalSex;
    }

    public void setLegalSex(String legalSex) {
        this.legalSex = legalSex;
    }

    public String getLegalBirth() {
        return legalBirth;
    }

    public void setLegalBirth(String legalBirth) {
        this.legalBirth = legalBirth;
    }

    public String getNatTaxRegCde() {
        return natTaxRegCde;
    }

    public void setNatTaxRegCde(String natTaxRegCde) {
        this.natTaxRegCde = natTaxRegCde;
    }

    public String getLocTaxRegCde() {
        return locTaxRegCde;
    }

    public void setLocTaxRegCde(String locTaxRegCde) {
        this.locTaxRegCde = locTaxRegCde;
    }

    public String getRegCde() {
        return regCde;
    }

    public void setRegCde(String regCde) {
        this.regCde = regCde;
    }

    public BigDecimal getRessetAmt() {
        return ressetAmt;
    }

    public void setRessetAmt(BigDecimal ressetAmt) {
        this.ressetAmt = ressetAmt;
    }

    public String getRegCapCurType() {
        return regCapCurType;
    }

    public void setRegCapCurType(String regCapCurType) {
        this.regCapCurType = regCapCurType;
    }

    public BigDecimal getRegCapAmt() {
        return regCapAmt;
    }

    public void setRegCapAmt(BigDecimal regCapAmt) {
        this.regCapAmt = regCapAmt;
    }

    public BigDecimal getPaidCapAmt() {
        return paidCapAmt;
    }

    public void setPaidCapAmt(BigDecimal paidCapAmt) {
        this.paidCapAmt = paidCapAmt;
    }

    public BigDecimal getOwersEquity() {
        return owersEquity;
    }

    public void setOwersEquity(BigDecimal owersEquity) {
        this.owersEquity = owersEquity;
    }

    public String getBasAccBank() {
        return basAccBank;
    }

    public void setBasAccBank(String basAccBank) {
        this.basAccBank = basAccBank;
    }

    public String getBasAccNo() {
        return basAccNo;
    }

    public void setBasAccNo(String basAccNo) {
        this.basAccNo = basAccNo;
    }

    public String getMrkFlg() {
        return mrkFlg;
    }

    public void setMrkFlg(String mrkFlg) {
        this.mrkFlg = mrkFlg;
    }

    public String getMrkAreaOne() {
        return mrkAreaOne;
    }

    public void setMrkAreaOne(String mrkAreaOne) {
        this.mrkAreaOne = mrkAreaOne;
    }

    public String getMrkAreaTwo() {
        return mrkAreaTwo;
    }

    public void setMrkAreaTwo(String mrkAreaTwo) {
        this.mrkAreaTwo = mrkAreaTwo;
    }

    public String getStkCode() {
        return stkCode;
    }

    public void setStkCode(String stkCode) {
        this.stkCode = stkCode;
    }

    public String getIntbankBuildDate() {
        return intbankBuildDate;
    }

    public void setIntbankBuildDate(String intbankBuildDate) {
        this.intbankBuildDate = intbankBuildDate;
    }

    public String getBankProLic() {
        return bankProLic;
    }

    public void setBankProLic(String bankProLic) {
        this.bankProLic = bankProLic;
    }

    public String getIntbankSite() {
        return intbankSite;
    }

    public void setIntbankSite(String intbankSite) {
        this.intbankSite = intbankSite;
    }

    public String getContArea() {
        return contArea;
    }

    public void setContArea(String contArea) {
        this.contArea = contArea;
    }

    public String getContCountry() {
        return contCountry;
    }

    public void setContCountry(String contCountry) {
        this.contCountry = contCountry;
    }

    public String getContTown() {
        return contTown;
    }

    public void setContTown(String contTown) {
        this.contTown = contTown;
    }

    public String getContAddr() {
        return contAddr;
    }

    public void setContAddr(String contAddr) {
        this.contAddr = contAddr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSupeEval() {
        return supeEval;
    }

    public void setSupeEval(String supeEval) {
        this.supeEval = supeEval;
    }

    public String getIntbankEval() {
        return intbankEval;
    }

    public void setIntbankEval(String intbankEval) {
        this.intbankEval = intbankEval;
    }

    public String getEvalEndDt() {
        return evalEndDt;
    }

    public void setEvalEndDt(String evalEndDt) {
        this.evalEndDt = evalEndDt;
    }

    public String getOwmAptName() {
        return owmAptName;
    }

    public void setOwmAptName(String owmAptName) {
        this.owmAptName = owmAptName;
    }

    public String getRelDrg() {
        return relDrg;
    }

    public void setRelDrg(String relDrg) {
        this.relDrg = relDrg;
    }

    public String getCusState() {
        return cusState;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode;
    }

    public String getOrgForShort() {
        return orgForShort;
    }

    public void setOrgForShort(String orgForShort) {
        this.orgForShort = orgForShort;
    }

    public String getOutApprOrg() {
        return outApprOrg;
    }

    public void setOutApprOrg(String outApprOrg) {
        this.outApprOrg = outApprOrg;
    }

    public String getIntbankOsssOrgCode() {
        return intbankOsssOrgCode;
    }

    public void setIntbankOsssOrgCode(String intbankOsssOrgCode) {
        this.intbankOsssOrgCode = intbankOsssOrgCode;
    }

    public String getIsLt() {
        return isLt;
    }

    public void setIsLt(String isLt) {
        this.isLt = isLt;
    }

    public String getOperArea() {
        return operArea;
    }

    public void setOperArea(String operArea) {
        this.operArea = operArea;
    }

    public String getOrgTypeMemo() {
        return orgTypeMemo;
    }

    public void setOrgTypeMemo(String orgTypeMemo) {
        this.orgTypeMemo = orgTypeMemo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getTradeClass() {
        return tradeClass;
    }

    public void setTradeClass(String tradeClass) {
        this.tradeClass = tradeClass;
    }

    public String getExpDateEnd() {
        return expDateEnd;
    }

    public void setExpDateEnd(String expDateEnd) {
        this.expDateEnd = expDateEnd;
    }

    public String getRegiAreaCode() {
        return regiAreaCode;
    }

    public void setRegiAreaCode(String regiAreaCode) {
        this.regiAreaCode = regiAreaCode;
    }

    public String getComRegAdd() {
        return comRegAdd;
    }

    public void setComRegAdd(String comRegAdd) {
        this.comRegAdd = comRegAdd;
    }

    public String getNatBusi() {
        return natBusi;
    }

    public void setNatBusi(String natBusi) {
        this.natBusi = natBusi;
    }

    public String getLinkmanPhone() {
        return linkmanPhone;
    }

    public void setLinkmanPhone(String linkmanPhone) {
        this.linkmanPhone = linkmanPhone;
    }

    public String getIslocked() {
        return islocked;
    }

    public void setIslocked(String islocked) {
        this.islocked = islocked;
    }

    public String getFinalRank() {
        return finalRank;
    }

    public void setFinalRank(String finalRank) {
        this.finalRank = finalRank;
    }

    public String getDueDt() {
        return dueDt;
    }

    public void setDueDt(String dueDt) {
        this.dueDt = dueDt;
    }

    public String getEffDt() {
        return effDt;
    }

    public void setEffDt(String effDt) {
        this.effDt = effDt;
    }

    @Override
    public String toString() {
        return "CmisCus0010RespDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", cusEnName='" + cusEnName + '\'' +
                ", intbankType='" + intbankType + '\'' +
                ", intbankSubType='" + intbankSubType + '\'' +
                ", largeBankNo='" + largeBankNo + '\'' +
                ", swiftCde='" + swiftCde + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", natEcoSec='" + natEcoSec + '\'' +
                ", holdType='" + holdType + '\'' +
                ", cusScale='" + cusScale + '\'' +
                ", fnaCllTyp='" + fnaCllTyp + '\'' +
                ", loanCardId='" + loanCardId + '\'' +
                ", headInsCde='" + headInsCde + '\'' +
                ", country='" + country + '\'' +
                ", legalName='" + legalName + '\'' +
                ", legalCertType='" + legalCertType + '\'' +
                ", legalCertCode='" + legalCertCode + '\'' +
                ", legalSex='" + legalSex + '\'' +
                ", legalBirth='" + legalBirth + '\'' +
                ", natTaxRegCde='" + natTaxRegCde + '\'' +
                ", locTaxRegCde='" + locTaxRegCde + '\'' +
                ", regCde='" + regCde + '\'' +
                ", ressetAmt=" + ressetAmt +
                ", regCapCurType='" + regCapCurType + '\'' +
                ", regCapAmt=" + regCapAmt +
                ", paidCapAmt=" + paidCapAmt +
                ", owersEquity=" + owersEquity +
                ", basAccBank='" + basAccBank + '\'' +
                ", basAccNo='" + basAccNo + '\'' +
                ", mrkFlg='" + mrkFlg + '\'' +
                ", mrkAreaOne='" + mrkAreaOne + '\'' +
                ", mrkAreaTwo='" + mrkAreaTwo + '\'' +
                ", stkCode='" + stkCode + '\'' +
                ", intbankBuildDate='" + intbankBuildDate + '\'' +
                ", bankProLic='" + bankProLic + '\'' +
                ", intbankSite='" + intbankSite + '\'' +
                ", contArea='" + contArea + '\'' +
                ", contCountry='" + contCountry + '\'' +
                ", contTown='" + contTown + '\'' +
                ", contAddr='" + contAddr + '\'' +
                ", email='" + email + '\'' +
                ", supeEval='" + supeEval + '\'' +
                ", intbankEval='" + intbankEval + '\'' +
                ", evalEndDt='" + evalEndDt + '\'' +
                ", owmAptName='" + owmAptName + '\'' +
                ", relDrg='" + relDrg + '\'' +
                ", cusState='" + cusState + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", oprType='" + oprType + '\'' +
                ", socialCreditCode='" + socialCreditCode + '\'' +
                ", orgForShort='" + orgForShort + '\'' +
                ", outApprOrg='" + outApprOrg + '\'' +
                ", intbankOsssOrgCode='" + intbankOsssOrgCode + '\'' +
                ", isLt='" + isLt + '\'' +
                ", operArea='" + operArea + '\'' +
                ", orgTypeMemo='" + orgTypeMemo + '\'' +
                ", managerId='" + managerId + '\'' +
                ", tradeClass='" + tradeClass + '\'' +
                ", expDateEnd='" + expDateEnd + '\'' +
                ", regiAreaCode='" + regiAreaCode + '\'' +
                ", comRegAdd='" + comRegAdd + '\'' +
                ", natBusi='" + natBusi + '\'' +
                ", linkmanPhone='" + linkmanPhone + '\'' +
                ", islocked='" + islocked + '\'' +
                ", finalRank='" + finalRank + '\'' +
                ", dueDt='" + dueDt + '\'' +
                ", effDt='" + effDt + '\'' +
                ", cusIntbankMgrList=" + cusIntbankMgrList +
                ", cusCorpApitalDtoList=" + cusCorpApitalDtoList +
                '}';
    }
}
