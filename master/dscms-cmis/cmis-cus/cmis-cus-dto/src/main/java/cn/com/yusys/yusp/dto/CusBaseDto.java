/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.dto;

public class CusBaseDto {

    /**
     * 客户编号
     **/
    private String cusId;

    /**
     * 客户名称
     **/
    private String cusName;

    /**
     * 客户简称
     **/
    private String cusShortName;

    /**
     * 客户类型
     **/
    private String cusType;

    /**
     * 证件类型 STD_ZB_CERT_TYP
     **/
    private String certType;

    /**
     * 证件号码
     **/
    private String certCode;

    /**
     * 开户日期
     **/
    private String openDate;

    /**
     * 开户类型 STD_ZB_OPEN_TYP
     **/
    private String openType;

    /**
     * 客户大类
     **/
    private String cusCatalog;

    /**
     * 所属条线 STD_ZB_BIZ_BELG
     **/
    private String belgLine;

    /**
     * 信用等级 STD_ZB_CREDIT_GRADE
     **/
    private String cusCrdGrade;

    /**
     * 信用评定到期日期
     **/
    private String cusCrdDt;

    /**
     * 客户状态 STD_ZB_CUS_ST
     **/
    private String cusState;

    /**
     * 客户分类 CUS_RANK_CLS
     **/
    private String cusRankCls;

    /**
     * 居住地址
     **/
    private String resiAddr;

    public String getCusRankCls() {
        return cusRankCls;
    }

    public void setCusRankCls(String cusRankCls) {
        this.cusRankCls = cusRankCls;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusShortName() {
        return cusShortName;
    }

    public void setCusShortName(String cusShortName) {
        this.cusShortName = cusShortName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getCusCrdGrade() {
        return cusCrdGrade;
    }

    public void setCusCrdGrade(String cusCrdGrade) {
        this.cusCrdGrade = cusCrdGrade;
    }

    public String getCusCrdDt() {
        return cusCrdDt;
    }

    public void setCusCrdDt(String cusCrdDt) {
        this.cusCrdDt = cusCrdDt;
    }

    public String getCusState() {
        return cusState;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    public String getResiAddr() {
        return resiAddr;
    }

    public void setResiAddr(String resiAddr) {
        this.resiAddr = resiAddr;
    }
}