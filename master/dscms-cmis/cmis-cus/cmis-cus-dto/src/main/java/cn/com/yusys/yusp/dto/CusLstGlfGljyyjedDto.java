package cn.com.yusys.yusp.dto;

import java.util.Date;

public class CusLstGlfGljyyjedDto {

    private static final long serialVersionUID = 1L;

    /** 业务流水号 **/
    private String serno;

    /** 关联方名称 **/
    private String relatedPartyName;

    /** 关联方客户编号 **/
    private String relatedPartyCusId;

    /** 关联方证件类型 **/
    private String relatedPartyCertType;

    /** 关联方证件号码 **/
    private String relatedPartyCertNo;

    /** 关联方预计额度 **/
    private String relatedPartyForeLmt;

    /** 关联方类型 **/
    private String relatedPartyType;

    /** 归属组别 **/
    private String belongGroup;

    /** 组别预计总额度 **/
    private String groupForeTotlAmt;

    /** 登记日期 **/
    private String inputDate;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRelatedPartyName() {
        return relatedPartyName;
    }

    public void setRelatedPartyName(String relatedPartyName) {
        this.relatedPartyName = relatedPartyName;
    }

    public String getRelatedPartyCusId() {
        return relatedPartyCusId;
    }

    public void setRelatedPartyCusId(String relatedPartyCusId) {
        this.relatedPartyCusId = relatedPartyCusId;
    }

    public String getRelatedPartyCertType() {
        return relatedPartyCertType;
    }

    public void setRelatedPartyCertType(String relatedPartyCertType) {
        this.relatedPartyCertType = relatedPartyCertType;
    }

    public String getRelatedPartyCertNo() {
        return relatedPartyCertNo;
    }

    public void setRelatedPartyCertNo(String relatedPartyCertNo) {
        this.relatedPartyCertNo = relatedPartyCertNo;
    }

    public String getRelatedPartyForeLmt() {
        return relatedPartyForeLmt;
    }

    public void setRelatedPartyForeLmt(String relatedPartyForeLmt) {
        this.relatedPartyForeLmt = relatedPartyForeLmt;
    }

    public String getRelatedPartyType() {
        return relatedPartyType;
    }

    public void setRelatedPartyType(String relatedPartyType) {
        this.relatedPartyType = relatedPartyType;
    }

    public String getBelongGroup() {
        return belongGroup;
    }

    public void setBelongGroup(String belongGroup) {
        this.belongGroup = belongGroup;
    }

    public String getGroupForeTotlAmt() {
        return groupForeTotlAmt;
    }

    public void setGroupForeTotlAmt(String groupForeTotlAmt) {
        this.groupForeTotlAmt = groupForeTotlAmt;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    @Override
    public String toString() {
        return "CusLstGlfGljyyjedDto{" +
                "serno='" + serno + '\'' +
                ", relatedPartyName='" + relatedPartyName + '\'' +
                ", relatedPartyCusId='" + relatedPartyCusId + '\'' +
                ", relatedPartyCertType='" + relatedPartyCertType + '\'' +
                ", relatedPartyCertNo='" + relatedPartyCertNo + '\'' +
                ", relatedPartyForeLmt='" + relatedPartyForeLmt + '\'' +
                ", relatedPartyType='" + relatedPartyType + '\'' +
                ", belongGroup='" + belongGroup + '\'' +
                ", groupForeTotlAmt='" + groupForeTotlAmt + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                '}';
    }
}
