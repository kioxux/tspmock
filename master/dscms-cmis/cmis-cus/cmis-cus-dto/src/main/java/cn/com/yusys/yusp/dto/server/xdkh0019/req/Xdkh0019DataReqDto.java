package cn.com.yusys.yusp.dto.server.xdkh0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：客户查询并开户
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0019DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certType")
    private String certType;//借款人证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//借款人证件号码
    @JsonProperty(value = "cusName")
    private String cusName;//借款人名称
    @JsonProperty(value = "cusType")
    private String cusType;//客户分类 01 正式客户 02 临时客户
    @JsonProperty(value = "managerId")
    private String managerId;//管户经理号
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//所属机构号
    @JsonProperty(value = "opType")
    private String opType;//操作类型

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    @Override
    public String toString() {
        return "Xdkh0019DataReqDto{" +
                "certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", cusName='" + cusName + '\'' +
                ", cusType='" + cusType + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", opType='" + opType + '\'' +
                '}';
    }
}