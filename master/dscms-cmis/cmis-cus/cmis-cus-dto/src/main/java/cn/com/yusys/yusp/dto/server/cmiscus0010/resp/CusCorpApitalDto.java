package cn.com.yusys.yusp.dto.server.cmiscus0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class CusCorpApitalDto {

    private static final long serialVersionUID = 1L;

    /** 主键 **/
    @JsonProperty(value = "pkId")
    private String pkId;

    /** 客户编号 **/
    @JsonProperty(value = "cusId")
    private String cusId;

    /** 出资人性质 STD_ZB_INVY_TYP **/
    @JsonProperty(value = "invtTyp")
    private String invtTyp;

    /** 出资人证件类型  STD_ZB_CERT_TYP **/
    @JsonProperty(value = "certTyp")
    private String certTyp;

    /** 出资人证件号码  **/
    @JsonProperty(value = "certCode")
    private String certCode;

    /** 出资人客户编号 **/
    @JsonProperty(value = "cusIdRel")
    private String cusIdRel;

    /** 出资人名称 **/
    @JsonProperty(value = "invtName")
    private String invtName;

    /** 币种 STD_ZB_CUR_TYP **/
    @JsonProperty(value = "curType")
    private String curType;

    /** 出资金额  **/
    @JsonProperty(value = "invtAmt")
    private BigDecimal invtAmt;

    /** 出资比例 **/
    @JsonProperty(value = "invtPerc")
    private BigDecimal invtPerc;

    /** 出资时间 **/
    @JsonProperty(value = "invDate")
    private String invDate;

    /** 出资说明 **/
    @JsonProperty(value = "invtDesc")
    private String invtDesc;

    /** 备注 **/
    @JsonProperty(value = "remark")
    private String remark;

    /** 国别  STD_ZB_COUNTRY **/
    @JsonProperty(value = "country")
    private String country;

    /** 出资方式  STD_ZB_INVT_TYPE **/
    @JsonProperty(value = "invtType")
    private String invtType;


    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getInvtTyp() {
        return invtTyp;
    }

    public void setInvtTyp(String invtTyp) {
        this.invtTyp = invtTyp;
    }

    public String getCertTyp() {
        return certTyp;
    }

    public void setCertTyp(String certTyp) {
        this.certTyp = certTyp;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCusIdRel() {
        return cusIdRel;
    }

    public void setCusIdRel(String cusIdRel) {
        this.cusIdRel = cusIdRel;
    }

    public String getInvtName() {
        return invtName;
    }

    public void setInvtName(String invtName) {
        this.invtName = invtName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getInvtAmt() {
        return invtAmt;
    }

    public void setInvtAmt(BigDecimal invtAmt) {
        this.invtAmt = invtAmt;
    }

    public BigDecimal getInvtPerc() {
        return invtPerc;
    }

    public void setInvtPerc(BigDecimal invtPerc) {
        this.invtPerc = invtPerc;
    }

    public String getInvDate() {
        return invDate;
    }

    public void setInvDate(String invDate) {
        this.invDate = invDate;
    }

    public String getInvtDesc() {
        return invtDesc;
    }

    public void setInvtDesc(String invtDesc) {
        this.invtDesc = invtDesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getInvtType() {
        return invtType;
    }

    public void setInvtType(String invtType) {
        this.invtType = invtType;
    }

    @Override
    public String toString() {
        return "CusIntbankMgrDto{" +
                "pkId='" + pkId + '\'' +
                ", cusId='" + cusId + '\'' +
                ", invtTyp='" + invtTyp + '\'' +
                ", certTyp='" + certTyp + '\'' +
                ", certCode='" + certCode + '\'' +
                ", cusIdRel='" + cusIdRel + '\'' +
                ", invtName='" + invtName + '\'' +
                ", curType='" + curType + '\'' +
                ", invtAmt=" + invtAmt +
                ", invtPerc=" + invtPerc +
                ", invDate='" + invDate + '\'' +
                ", invtDesc='" + invtDesc + '\'' +
                ", remark='" + remark + '\'' +
                ", country='" + country + '\'' +
                ", invtType='" + invtType + '\'' +
                '}';
    }
}
