package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;

public class CusIndivAllDto {
    private static final long serialVersionUID = 1L;
    // 客户编号
    private String cusId;
    // 客户名
    private String cusName;
    // 证件类
    private String certType;
    // 证件号
    private String certCode;
    // 国籍
    private String nation;
    // 性别 std_zb_sex
    private String sex;
    // 是否为长期证件 std_zb_yes_no
    private String isLongIndiv;
    // 证件到期日期
    private String certEndDt;
    // 民族 std_zb_indiv_folk
    private String indivFolk;
    // 籍贯
    private String indivBrtPlace;
    // 出生日期
    private String indivDtOfBirth;
    // 政治面貌 std_zb_political
    private String indivPolSt;
    // 最高学历 std_zb_edu
    private String indivEdt;
    // 最高学位 std_zb_degree;
    private String indivDgr;
    // 婚姻状况 std_zb_mar_st
    private String marStatus;
    // 健康状况
    private String healthStatus;
    // 建立信贷关系时间
    private String initLoanDate;
    // 在我行建立业务情况 std_zb_inv_hl_acn
    private String indivHldAcnt;
    // 评级得分
    private String cusCrdGrade;
    // 是否我行股东 std_zb_yes_no
    private String isBankSharehd;
    // 客户类型
    private String cusType;
    // 影像编号
    private String imageNo;
    // 开户日期
    private String openDate;
    // 开户类型 std_zb_open_typ
    private String openType;
    // 客户大类
    private String cusCatalog;
    // 客户分类
    private String cusRankCls;
    // 管户客户经理
    private String managerId;
    // 客户状态 std_zb_cus_st
    private String cusState;
    // 主管机构
    private String managerBrId;
    // 操作类型  std_zb_opr_type
    private String oprType;
    /** 是否强村富民贷款 **/
    private String isLoan;

    /** 是否小企业客户 **/
    private String isSmconCus;
    /** 所属机构 **/
    private String belgOrg;

    /** 保存状态 **/
    private String saveStatus;

    /** 身份证地址 **/
    private String certAddr;

    /** 身份证地址街道 **/
    private String certStreet;

    /** 居住地地址 **/
    private String indivRsdAddr;

    /** 居住地址街道 **/
    private String streetRsd;

    /** 居住地邮政编码 **/
    private String indivZipCode;

    /** 居住状况 STD_ZB_RSD_ST **/
    private String indivRsdSt;

    /** 住宅电话 **/
    private String mobile;

    /** 居住区域编号 **/
    private String regionalism;

    /** 居住区域名称 **/
    private String regionName;

    /** 手机号码1 **/
    private String mobileA;

    /** 号码是否验证1 STD_ZB_YES_NO **/
    private String isCheckA;

    /** 号码是否本人1 STD_ZB_YES_NO **/
    private String isOneselfA;

    /** 手机号码2 **/
    private String mobileB;

    /** 号码是否验证2 STD_ZB_YES_NO **/
    private String isCheckB;

    /** 号码是否本人2 STD_ZB_YES_NO **/
    private String isOneselfB;

    /** 传真 **/
    private String faxCode;

    /** Email地址 **/
    private String email;

    /** 手机号码1来源 **/
    private String mobile1Sour;

    /** 手机号码2来源 **/
    private String mobile2Sour;

    /** 微信号 **/
    private String wechatNo;

    /** 手机号码 **/
    private String mobileNo;

    /** QQ号 **/
    private String qq;

    /** 户籍地址 **/
    private String regionAddr;

    /** 户籍地址 **/
    private String indivHouhRegAdd;

    /** 户籍地址街道 **/
    private String regionStreet;

    /** 送达地址 **/
    private String sendAddr;

    /** 送达地址街道 **/
    private String deliveryStreet;

    /** 邮政编码 **/
    private String postcode;

    /** 是否农户 **/
    private String isAgri;

    /** 居住时间-年 **/
    private String resiTime;
    /** 主键 **/
    private String pkId;

    /** 是否当前单位 STD_ZB_YES_NO **/
    private String isCurtUnit;

    /** 职业 STD_ZB_OCC **/
    private String occ;

    /** 自由职业说明 **/
    private String occDesc;

    /** 经营企业名称/工作单位 **/
    private String unitName;

    /** 所在部门 **/
    private String ubietyDept;

    /** 单位性质 **/
    private String indivComTyp;

    /** 单位所属行业 **/
    private String indivComTrade;

    /** 职务 STD_ZB_JOB_TTL **/
    private String jobTtl;

    /** 职称 STD_ZB_TITLE **/
    private String indivCrtfctn;

    /** 年收入(元) **/
    private java.math.BigDecimal yScore;

    /** 单位地址 **/
    private String unitAddr;

    /** 单位地址街道/路 **/
    private String unitStreet;

    /** 单位邮政编码 **/
    private String unitZipCode;

    /** 单位电话 **/
    private String unitPhn;

    /** 单位传真 **/
    private String unitFax;

    /** 单位联系人 **/
    private String unitCntName;

    /** 参加工作年份 **/
    private String workDate;

    /** 本单位参加工作日期 **/
    private String unitDate;

    /** 本单位工作结束日期 **/
    private String unitEndDate;

    /** 本行业参加工作日期  **/
    private String tradeDate;

    /** 备注 **/
    private String remark;

    /** 个人工作履历标识 **/
    private String prsnWorkId;

    /** 雇佣状态 **/
    private String employeeStatus;

    /** 营业执照号码 **/
    private String blicNo;

    /** 经营状况 **/
    private String operStatus;

    /** 经营企业统一社会信用代码 **/
    private String unifyCreditCode;

    /** 收入币种 **/
    private String earningCurType;

    /** 家庭年收入(元) **/
    private java.math.BigDecimal familyYScore;

    /** 个人年收入 **/
    private String indivYearn;

    /**提交状态**/
    private String taskStatus;

    /**业务类型**/
    private String bizType;

    /**流水号**/
    private String serno;
    // 是否五日还款
    private String cusGetfive;

    /** 营业执照号码 **/
    private String regCde;
    /** 是否有子女 **/
    private String isHaveChildren;
    /** 职称**/
    private String indivCrtftn;
    /** 是否我行员工**/
    private String isBankEmployee;
    /** 登记日期**/
    private String inputDate;

    @Override
    public String toString() {
        return "CusIndivAllDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", nation='" + nation + '\'' +
                ", sex='" + sex + '\'' +
                ", isLongIndiv='" + isLongIndiv + '\'' +
                ", certEndDt='" + certEndDt + '\'' +
                ", indivFolk='" + indivFolk + '\'' +
                ", indivBrtPlace='" + indivBrtPlace + '\'' +
                ", indivDtOfBirth='" + indivDtOfBirth + '\'' +
                ", indivPolSt='" + indivPolSt + '\'' +
                ", indivEdt='" + indivEdt + '\'' +
                ", indivDgr='" + indivDgr + '\'' +
                ", marStatus='" + marStatus + '\'' +
                ", healthStatus='" + healthStatus + '\'' +
                ", initLoanDate='" + initLoanDate + '\'' +
                ", indivHldAcnt='" + indivHldAcnt + '\'' +
                ", cusCrdGrade='" + cusCrdGrade + '\'' +
                ", isBankSharehd='" + isBankSharehd + '\'' +
                ", cusType='" + cusType + '\'' +
                ", imageNo='" + imageNo + '\'' +
                ", openDate='" + openDate + '\'' +
                ", openType='" + openType + '\'' +
                ", cusCatalog='" + cusCatalog + '\'' +
                ", cusRankCls='" + cusRankCls + '\'' +
                ", managerId='" + managerId + '\'' +
                ", cusState='" + cusState + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", oprType='" + oprType + '\'' +
                ", isLoan='" + isLoan + '\'' +
                ", isSmconCus='" + isSmconCus + '\'' +
                ", belgOrg='" + belgOrg + '\'' +
                ", saveStatus='" + saveStatus + '\'' +
                ", certAddr='" + certAddr + '\'' +
                ", certStreet='" + certStreet + '\'' +
                ", indivRsdAddr='" + indivRsdAddr + '\'' +
                ", streetRsd='" + streetRsd + '\'' +
                ", indivZipCode='" + indivZipCode + '\'' +
                ", indivRsdSt='" + indivRsdSt + '\'' +
                ", mobile='" + mobile + '\'' +
                ", regionalism='" + regionalism + '\'' +
                ", regionName='" + regionName + '\'' +
                ", mobileA='" + mobileA + '\'' +
                ", isCheckA='" + isCheckA + '\'' +
                ", isOneselfA='" + isOneselfA + '\'' +
                ", mobileB='" + mobileB + '\'' +
                ", isCheckB='" + isCheckB + '\'' +
                ", isOneselfB='" + isOneselfB + '\'' +
                ", faxCode='" + faxCode + '\'' +
                ", email='" + email + '\'' +
                ", mobile1Sour='" + mobile1Sour + '\'' +
                ", mobile2Sour='" + mobile2Sour + '\'' +
                ", wechatNo='" + wechatNo + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", qq='" + qq + '\'' +
                ", regionAddr='" + regionAddr + '\'' +
                ", indivHouhRegAdd='" + indivHouhRegAdd + '\'' +
                ", regionStreet='" + regionStreet + '\'' +
                ", sendAddr='" + sendAddr + '\'' +
                ", deliveryStreet='" + deliveryStreet + '\'' +
                ", postcode='" + postcode + '\'' +
                ", isAgri='" + isAgri + '\'' +
                ", resiTime='" + resiTime + '\'' +
                ", pkId='" + pkId + '\'' +
                ", isCurtUnit='" + isCurtUnit + '\'' +
                ", occ='" + occ + '\'' +
                ", occDesc='" + occDesc + '\'' +
                ", unitName='" + unitName + '\'' +
                ", ubietyDept='" + ubietyDept + '\'' +
                ", indivComTyp='" + indivComTyp + '\'' +
                ", indivComTrade='" + indivComTrade + '\'' +
                ", jobTtl='" + jobTtl + '\'' +
                ", indivCrtfctn='" + indivCrtfctn + '\'' +
                ", yScore=" + yScore +
                ", unitAddr='" + unitAddr + '\'' +
                ", unitStreet='" + unitStreet + '\'' +
                ", unitZipCode='" + unitZipCode + '\'' +
                ", unitPhn='" + unitPhn + '\'' +
                ", unitFax='" + unitFax + '\'' +
                ", unitCntName='" + unitCntName + '\'' +
                ", workDate='" + workDate + '\'' +
                ", unitDate='" + unitDate + '\'' +
                ", unitEndDate='" + unitEndDate + '\'' +
                ", tradeDate='" + tradeDate + '\'' +
                ", remark='" + remark + '\'' +
                ", prsnWorkId='" + prsnWorkId + '\'' +
                ", employeeStatus='" + employeeStatus + '\'' +
                ", blicNo='" + blicNo + '\'' +
                ", operStatus='" + operStatus + '\'' +
                ", unifyCreditCode='" + unifyCreditCode + '\'' +
                ", earningCurType='" + earningCurType + '\'' +
                ", familyYScore=" + familyYScore +
                ", indivYearn='" + indivYearn + '\'' +
                ", taskStatus='" + taskStatus + '\'' +
                ", bizType='" + bizType + '\'' +
                ", serno='" + serno + '\'' +
                ", cusGetfive='" + cusGetfive + '\'' +
                ", regCde='" + regCde + '\'' +
                ", isHaveChildren='" + isHaveChildren + '\'' +
                ", indivCrtftn='" + indivCrtftn + '\'' +
                ", isBankEmployee='" + isBankEmployee + '\'' +
                ", agriFlg='" + agriFlg + '\'' +
                ", isBankSharehe='" + isBankSharehe + '\'' +
                ", inputDate='" + inputDate + '\'' +
                '}';
    }

    /** 是否农户**/
    private String agriFlg;
    /** 是否我行股东**/
    private String isBankSharehe;

    public String getIsBankSharehe() {
        return isBankSharehe;
    }

    public void setIsBankSharehe(String isBankSharehe) {
        this.isBankSharehe = isBankSharehe;
    }

    public String getAgriFlg() {
        return agriFlg;
    }

    public void setAgriFlg(String agriFlg) {
        this.agriFlg = agriFlg;
    }

    public String getIsBankEmployee() {
        return isBankEmployee;
    }

    public void setIsBankEmployee(String isBankEmployee) {
        this.isBankEmployee = isBankEmployee;
    }

    public String getIndivCrtftn() {
        return indivCrtftn;
    }

    public void setIndivCrtftn(String indivCrtftn) {
        this.indivCrtftn = indivCrtftn;
    }
    public String getIsHaveChildren() {
        return isHaveChildren;
    }

    public void setIsHaveChildren(String isHaveChildren) {
        this.isHaveChildren = isHaveChildren;
    }

    public String getRegCde() {
        return regCde;
    }

    public void setRegCde(String regCde) {
        this.regCde = regCde;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setIsLongIndiv(String isLongIndiv) {
        this.isLongIndiv = isLongIndiv;
    }

    public void setCertEndDt(String certEndDt) {
        this.certEndDt = certEndDt;
    }

    public void setIndivFolk(String indivFolk) {
        this.indivFolk = indivFolk;
    }

    public void setIndivBrtPlace(String indivBrtPlace) {
        this.indivBrtPlace = indivBrtPlace;
    }

    public void setIndivDtOfBirth(String indivDtOfBirth) {
        this.indivDtOfBirth = indivDtOfBirth;
    }

    public void setIndivPolSt(String indivPolSt) {
        this.indivPolSt = indivPolSt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public void setIndivDgr(String indivDgr) {
        this.indivDgr = indivDgr;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public void setInitLoanDate(String initLoanDate) {
        this.initLoanDate = initLoanDate;
    }

    public void setIndivHldAcnt(String indivHldAcnt) {
        this.indivHldAcnt = indivHldAcnt;
    }

    public void setCusCrdGrade(String cusCrdGrade) {
        this.cusCrdGrade = cusCrdGrade;
    }

    public void setIsBankSharehd(String isBankSharehd) {
        this.isBankSharehd = isBankSharehd;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    public void setCusRankCls(String cusRankCls) {
        this.cusRankCls = cusRankCls;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public void setIsLoan(String isLoan) {
        this.isLoan = isLoan;
    }

    public void setIsSmconCus(String isSmconCus) {
        this.isSmconCus = isSmconCus;
    }

    public void setBelgOrg(String belgOrg) {
        this.belgOrg = belgOrg;
    }

    public void setSaveStatus(String saveStatus) {
        this.saveStatus = saveStatus;
    }

    public void setCertAddr(String certAddr) {
        this.certAddr = certAddr;
    }

    public void setCertStreet(String certStreet) {
        this.certStreet = certStreet;
    }

    public void setIndivRsdAddr(String indivRsdAddr) {
        this.indivRsdAddr = indivRsdAddr;
    }

    public void setStreetRsd(String streetRsd) {
        this.streetRsd = streetRsd;
    }

    public void setIndivZipCode(String indivZipCode) {
        this.indivZipCode = indivZipCode;
    }

    public void setIndivRsdSt(String indivRsdSt) {
        this.indivRsdSt = indivRsdSt;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setRegionalism(String regionalism) {
        this.regionalism = regionalism;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void setMobileA(String mobileA) {
        this.mobileA = mobileA;
    }

    public void setIsCheckA(String isCheckA) {
        this.isCheckA = isCheckA;
    }

    public void setIsOneselfA(String isOneselfA) {
        this.isOneselfA = isOneselfA;
    }

    public void setMobileB(String mobileB) {
        this.mobileB = mobileB;
    }

    public void setIsCheckB(String isCheckB) {
        this.isCheckB = isCheckB;
    }

    public void setIsOneselfB(String isOneselfB) {
        this.isOneselfB = isOneselfB;
    }

    public void setFaxCode(String faxCode) {
        this.faxCode = faxCode;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobile1Sour(String mobile1Sour) {
        this.mobile1Sour = mobile1Sour;
    }

    public void setMobile2Sour(String mobile2Sour) {
        this.mobile2Sour = mobile2Sour;
    }

    public void setWechatNo(String wechatNo) {
        this.wechatNo = wechatNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public void setRegionAddr(String regionAddr) {
        this.regionAddr = regionAddr;
    }

    public void setRegionStreet(String regionStreet) {
        this.regionStreet = regionStreet;
    }

    public void setSendAddr(String sendAddr) {
        this.sendAddr = sendAddr;
    }

    public void setDeliveryStreet(String deliveryStreet) {
        this.deliveryStreet = deliveryStreet;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setIsAgri(String isAgri) {
        this.isAgri = isAgri;
    }

    public void setResiTime(String resiTime) {
        this.resiTime = resiTime;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public void setIsCurtUnit(String isCurtUnit) {
        this.isCurtUnit = isCurtUnit;
    }

    public void setOcc(String occ) {
        this.occ = occ;
    }

    public void setOccDesc(String occDesc) {
        this.occDesc = occDesc;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public void setUbietyDept(String ubietyDept) {
        this.ubietyDept = ubietyDept;
    }

    public void setIndivComTyp(String indivComTyp) {
        this.indivComTyp = indivComTyp;
    }

    public void setIndivComTrade(String indivComTrade) {
        this.indivComTrade = indivComTrade;
    }

    public void setJobTtl(String jobTtl) {
        this.jobTtl = jobTtl;
    }

    public void setIndivCrtfctn(String indivCrtfctn) {
        this.indivCrtfctn = indivCrtfctn;
    }

    public void setyScore(BigDecimal yScore) {
        this.yScore = yScore;
    }

    public void setUnitAddr(String unitAddr) {
        this.unitAddr = unitAddr;
    }

    public void setUnitStreet(String unitStreet) {
        this.unitStreet = unitStreet;
    }

    public void setUnitZipCode(String unitZipCode) {
        this.unitZipCode = unitZipCode;
    }

    public void setUnitPhn(String unitPhn) {
        this.unitPhn = unitPhn;
    }

    public void setUnitFax(String unitFax) {
        this.unitFax = unitFax;
    }

    public void setUnitCntName(String unitCntName) {
        this.unitCntName = unitCntName;
    }

    public void setWorkDate(String workDate) {
        this.workDate = workDate;
    }

    public void setUnitDate(String unitDate) {
        this.unitDate = unitDate;
    }

    public void setUnitEndDate(String unitEndDate) {
        this.unitEndDate = unitEndDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setPrsnWorkId(String prsnWorkId) {
        this.prsnWorkId = prsnWorkId;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public void setBlicNo(String blicNo) {
        this.blicNo = blicNo;
    }

    public void setOperStatus(String operStatus) {
        this.operStatus = operStatus;
    }

    public void setUnifyCreditCode(String unifyCreditCode) {
        this.unifyCreditCode = unifyCreditCode;
    }

    public void setEarningCurType(String earningCurType) {
        this.earningCurType = earningCurType;
    }

    public void setFamilyYScore(BigDecimal familyYScore) {
        this.familyYScore = familyYScore;
    }

    public void setIndivYearn(String indivYearn) {
        this.indivYearn = indivYearn;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getCusId() {
        return cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public String getCertType() {
        return certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public String getNation() {
        return nation;
    }

    public String getSex() {
        return sex;
    }

    public String getIsLongIndiv() {
        return isLongIndiv;
    }

    public String getCertEndDt() {
        return certEndDt;
    }

    public String getIndivFolk() {
        return indivFolk;
    }

    public String getIndivBrtPlace() {
        return indivBrtPlace;
    }

    public String getIndivDtOfBirth() {
        return indivDtOfBirth;
    }

    public String getIndivPolSt() {
        return indivPolSt;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public String getIndivDgr() {
        return indivDgr;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public String getInitLoanDate() {
        return initLoanDate;
    }

    public String getIndivHldAcnt() {
        return indivHldAcnt;
    }

    public String getCusCrdGrade() {
        return cusCrdGrade;
    }

    public String getIsBankSharehd() {
        return isBankSharehd;
    }

    public String getCusType() {
        return cusType;
    }

    public String getImageNo() {
        return imageNo;
    }

    public String getOpenDate() {
        return openDate;
    }

    public String getOpenType() {
        return openType;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public String getCusRankCls() {
        return cusRankCls;
    }

    public String getManagerId() {
        return managerId;
    }

    public String getCusState() {
        return cusState;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public String getOprType() {
        return oprType;
    }

    public String getIsLoan() {
        return isLoan;
    }

    public String getIsSmconCus() {
        return isSmconCus;
    }

    public String getBelgOrg() {
        return belgOrg;
    }

    public String getSaveStatus() {
        return saveStatus;
    }

    public String getCertAddr() {
        return certAddr;
    }

    public String getCertStreet() {
        return certStreet;
    }

    public String getIndivRsdAddr() {
        return indivRsdAddr;
    }

    public String getStreetRsd() {
        return streetRsd;
    }

    public String getIndivZipCode() {
        return indivZipCode;
    }

    public String getIndivRsdSt() {
        return indivRsdSt;
    }

    public String getMobile() {
        return mobile;
    }

    public String getRegionalism() {
        return regionalism;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getMobileA() {
        return mobileA;
    }

    public String getIsCheckA() {
        return isCheckA;
    }

    public String getIsOneselfA() {
        return isOneselfA;
    }

    public String getMobileB() {
        return mobileB;
    }

    public String getIsCheckB() {
        return isCheckB;
    }

    public String getIsOneselfB() {
        return isOneselfB;
    }

    public String getFaxCode() {
        return faxCode;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile1Sour() {
        return mobile1Sour;
    }

    public String getMobile2Sour() {
        return mobile2Sour;
    }

    public String getWechatNo() {
        return wechatNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getQq() {
        return qq;
    }

    public String getRegionAddr() {
        return regionAddr;
    }

    public String getRegionStreet() {
        return regionStreet;
    }

    public String getSendAddr() {
        return sendAddr;
    }

    public String getDeliveryStreet() {
        return deliveryStreet;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getIsAgri() {
        return isAgri;
    }

    public String getResiTime() {
        return resiTime;
    }

    public String getPkId() {
        return pkId;
    }

    public String getIsCurtUnit() {
        return isCurtUnit;
    }

    public String getOcc() {
        return occ;
    }

    public String getOccDesc() {
        return occDesc;
    }

    public String getUnitName() {
        return unitName;
    }

    public String getUbietyDept() {
        return ubietyDept;
    }

    public String getIndivComTyp() {
        return indivComTyp;
    }

    public String getIndivComTrade() {
        return indivComTrade;
    }

    public String getJobTtl() {
        return jobTtl;
    }

    public String getIndivCrtfctn() {
        return indivCrtfctn;
    }

    public BigDecimal getyScore() {
        return yScore;
    }

    public String getUnitAddr() {
        return unitAddr;
    }

    public String getUnitStreet() {
        return unitStreet;
    }

    public String getUnitZipCode() {
        return unitZipCode;
    }

    public String getUnitPhn() {
        return unitPhn;
    }

    public String getUnitFax() {
        return unitFax;
    }

    public String getUnitCntName() {
        return unitCntName;
    }

    public String getWorkDate() {
        return workDate;
    }

    public String getUnitDate() {
        return unitDate;
    }

    public String getUnitEndDate() {
        return unitEndDate;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public String getRemark() {
        return remark;
    }

    public String getPrsnWorkId() {
        return prsnWorkId;
    }

    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public String getBlicNo() {
        return blicNo;
    }

    public String getOperStatus() {
        return operStatus;
    }

    public String getUnifyCreditCode() {
        return unifyCreditCode;
    }

    public String getEarningCurType() {
        return earningCurType;
    }

    public BigDecimal getFamilyYScore() {
        return familyYScore;
    }

    public String getIndivYearn() {
        return indivYearn;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public String getBizType() {
        return bizType;
    }

    public String getSerno() {
        return serno;
    }
    public void setCusGetfive(String cusGetfive) {
        this.cusGetfive = cusGetfive;
    }

    public String getCusGetfive() {
        return cusGetfive;
    }

    public String getIndivHouhRegAdd() {
        return indivHouhRegAdd;
    }

    public void setIndivHouhRegAdd(String indivHouhRegAdd) {
        this.indivHouhRegAdd = indivHouhRegAdd;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }
}
