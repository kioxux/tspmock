package cn.com.yusys.yusp.dto.server.cmiscus0008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询根据客户编号查询法人名称,名称，证件类型，证件号，电话
 *
 * @author 徐超
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0008ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisCus0008ReqDto{" +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
