package cn.com.yusys.yusp.dto;

import java.util.List;

/**
 * 用户基本信息查询参数对象
 * @since 2021-04-6
 */
public class CusInfoQueryDto {

    private List<String> cusIds;

    public List<String> getCusIds() {
        return cusIds;
    }

    public void setCusIds(List<String> cusIds) {
        this.cusIds = cusIds;
    }

}