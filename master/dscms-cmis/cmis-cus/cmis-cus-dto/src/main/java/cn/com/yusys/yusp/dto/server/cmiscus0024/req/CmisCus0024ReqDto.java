package cn.com.yusys.yusp.dto.server.cmiscus0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 *  请求Dto：根据行号或BICCODE获取同业客户号
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0024ReqDto implements Serializable {

    private static final long serialVersionUID = 3211355567892051016L;
    /**
     * 行号
     */
    @JsonProperty(value = "bankNo",required = true)
    private String bankNo;

    /**
     * bicCode
     */
    @JsonProperty(value = "bicCode")
    private String bicCode;


    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getBicCode() {
        return bicCode;
    }

    public void setBicCode(String bicCode) {
        this.bicCode = bicCode;
    }

    @Override
    public String toString() {
        return "CmisCus0024ReqDto{" +
                "bankNo='" + bankNo + '\'' +
                ", bicCode='" + bicCode + '\'' +
                '}';
    }
}
