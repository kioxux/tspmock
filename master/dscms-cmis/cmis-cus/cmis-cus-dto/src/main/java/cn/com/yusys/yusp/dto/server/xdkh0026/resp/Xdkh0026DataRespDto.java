package cn.com.yusys.yusp.dto.server.xdkh0026.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：优企贷、优农贷行内关联人基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0026DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdkh0026DataRespDto{" +
                "list=" + list +
                '}';
    }
}