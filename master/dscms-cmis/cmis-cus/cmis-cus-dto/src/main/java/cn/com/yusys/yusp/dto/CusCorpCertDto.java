package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-dto模块
 * @类名称: CusCorpCert
 * @类描述: cus_corp_cert数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-21 16:40:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusCorpCertDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 客户编号 **/
	private String cusId;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 注册登记日期 **/
	private String regiDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 颁证机关 **/
	private String issuDep;
	
	/** 资质名称 **/
	private String aptiName;
	
	/** 资质等级 **/
	private String aptiLvl;
	
	/** 证件说明 **/
	private String certExplain;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param regiDate
	 */
	public void setRegiDate(String regiDate) {
		this.regiDate = regiDate == null ? null : regiDate.trim();
	}
	
    /**
     * @return RegiDate
     */	
	public String getRegiDate() {
		return this.regiDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param issuDep
	 */
	public void setIssuDep(String issuDep) {
		this.issuDep = issuDep == null ? null : issuDep.trim();
	}
	
    /**
     * @return IssuDep
     */	
	public String getIssuDep() {
		return this.issuDep;
	}
	
	/**
	 * @param aptiName
	 */
	public void setAptiName(String aptiName) {
		this.aptiName = aptiName == null ? null : aptiName.trim();
	}
	
    /**
     * @return AptiName
     */	
	public String getAptiName() {
		return this.aptiName;
	}
	
	/**
	 * @param aptiLvl
	 */
	public void setAptiLvl(String aptiLvl) {
		this.aptiLvl = aptiLvl == null ? null : aptiLvl.trim();
	}
	
    /**
     * @return AptiLvl
     */	
	public String getAptiLvl() {
		return this.aptiLvl;
	}
	
	/**
	 * @param certExplain
	 */
	public void setCertExplain(String certExplain) {
		this.certExplain = certExplain == null ? null : certExplain.trim();
	}
	
    /**
     * @return CertExplain
     */	
	public String getCertExplain() {
		return this.certExplain;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}