package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * DocAccSearchDto
 * @author
 * @date 2021/9/28 0:00
 **/
public class CusLstDedkkhYjsxTaskDto implements Serializable{
	private static final long serialVersionUID = 1L;
	/** 任务编号 **/
	private String taskNo;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 压降事项 **/
	private String pressureDropItem;

	/** 计划压降金额（万元） **/
	private java.math.BigDecimal planPdoAmt;

	/** 压降计划完成时间 **/
	private String planPdoFinishTime;

	/** 实际压降金额（万元） **/
	private java.math.BigDecimal actPdoAmt;

	/** 压降实际完成时间 **/
	private String actPdoFinishDate;

	/** 任务生成日期 **/
	private String taskCreateDate;

	/** 任务到期日期 **/
	private String taskEndDate;

	/** 压降落实情况说明 **/
	private String pdoActCaseMemo;

	/** 任务终止说明 **/
	private String taskEndMemo;

	/** 审批状态 **/
	private String approveStatus;

	/** 管户客户经理 **/
	private String managerId;

	/** 所属机构 **/
	private String belgOrg;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 更新人 **/
	private String updId;

	/** 更新机构 **/
	private String updBrId;

	/** 更新日期 **/
	private String updDate;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;


	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}

	/**
	 * @return taskNo
	 */
	public String getTaskNo() {
		return this.taskNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param pressureDropItem
	 */
	public void setPressureDropItem(String pressureDropItem) {
		this.pressureDropItem = pressureDropItem;
	}

	/**
	 * @return pressureDropItem
	 */
	public String getPressureDropItem() {
		return this.pressureDropItem;
	}

	/**
	 * @param planPdoAmt
	 */
	public void setPlanPdoAmt(java.math.BigDecimal planPdoAmt) {
		this.planPdoAmt = planPdoAmt;
	}

	/**
	 * @return planPdoAmt
	 */
	public java.math.BigDecimal getPlanPdoAmt() {
		return this.planPdoAmt;
	}

	/**
	 * @param planPdoFinishTime
	 */
	public void setPlanPdoFinishTime(String planPdoFinishTime) {
		this.planPdoFinishTime = planPdoFinishTime;
	}

	/**
	 * @return planPdoFinishTime
	 */
	public String getPlanPdoFinishTime() {
		return this.planPdoFinishTime;
	}

	/**
	 * @param taskCreateDate
	 */
	public void setTaskCreateDate(String taskCreateDate) {
		this.taskCreateDate = taskCreateDate;
	}

	/**
	 * @return taskCreateDate
	 */
	public String getTaskCreateDate() {
		return this.taskCreateDate;
	}

	/**
	 * @param taskEndDate
	 */
	public void setTaskEndDate(String taskEndDate) {
		this.taskEndDate = taskEndDate;
	}

	/**
	 * @return taskEndDate
	 */
	public String getTaskEndDate() {
		return this.taskEndDate;
	}


	/**
	 * @param taskEndMemo
	 */
	public void setTaskEndMemo(String taskEndMemo) {
		this.taskEndMemo = taskEndMemo;
	}

	/**
	 * @return taskEndMemo
	 */
	public String getTaskEndMemo() {
		return this.taskEndMemo;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg;
	}

	/**
	 * @return belgOrg
	 */
	public String getBelgOrg() {
		return this.belgOrg;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
}