package cn.com.yusys.yusp.dto.server.cmiscus0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：查询客户基本信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0006RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusBaseCacheKey")
    private String cusBaseCacheKey;//客户基本信息对应的缓存Key
    @JsonProperty(value = "cusBaseList")
    private java.util.List<CusBaseDto> cusBaseList;

    public String getCusBaseCacheKey() {
        return cusBaseCacheKey;
    }

    public void setCusBaseCacheKey(String cusBaseCacheKey) {
        this.cusBaseCacheKey = cusBaseCacheKey;
    }

    public List<CusBaseDto> getCusBaseList() {
        return cusBaseList;
    }

    public void setCusBaseList(List<CusBaseDto> cusBaseList) {
        this.cusBaseList = cusBaseList;
    }

    @Override
    public String toString() {
        return "CmisCus0006RespDto{" +
                "cusBaseCacheKey='" + cusBaseCacheKey + '\'' +
                ", cusBaseList=" + cusBaseList +
                '}';
    }
}
