package cn.com.yusys.yusp.dto.server.xdkh0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
/**
 * 响应Data：同业客户信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusno")
    private String cusno;//客户号
    @JsonProperty(value = "regno")
    private String regno;//登记注册号
    @JsonProperty(value = "orgno")
    private String orgno;//同业机构(行)号
    @JsonProperty(value = "orgna")
    private String orgna;//同业机构(行)名称
    @JsonProperty(value = "regamt")
    private String regamt;//注册/开办资金(万元)
    @JsonProperty(value = "rank")
    private String rank;//评级等级


    public String getCusno() {
        return cusno;
    }

    public void setCusno(String cusno) {
        this.cusno = cusno;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getOrgno() {
        return orgno;
    }

    public void setOrgno(String orgno) {
        this.orgno = orgno;
    }

    public String getOrgna() {
        return orgna;
    }

    public void setOrgna(String orgna) {
        this.orgna = orgna;
    }

    public String getRegamt() {
        return regamt;
    }

    public void setRegamt(String regamt) {
        this.regamt = regamt;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }


    @Override
    public String toString() {
        return "Xdkh0005DataRespDto{" +
                "cusno='" + cusno + '\'' +
                ", regno='" + regno + '\'' +
                ", orgno='" + orgno + '\'' +
                ", orgna='" + orgna + '\'' +
                ", regamt='" + regamt + '\'' +
                '}';
    }
}
