package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusAccountInfo
 * @类描述: cus_account_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 10:18:18
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusAccountInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 客户编号 **/
	private String cusId;
	
	/** 结算账户账号 **/
	private String settlAcctNum;
	
	/** 账户名称 **/
	private String acctName;
	
	/** 账户子序号 **/
	private String acctSubNum;
	
	/** 币种 **/
	private String curType;
	
	/** 开户机构 **/
	private String openOrgId;
	
	/** 是否使用 **/
	private String isEffect;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}

    /**
     * @return PkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

    /**
     * @return CusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param settlAcctNum
	 */
	public void setSettlAcctNum(String settlAcctNum) {
		this.settlAcctNum = settlAcctNum == null ? null : settlAcctNum.trim();
	}

    /**
     * @return SettlAcctNum
     */
	public String getSettlAcctNum() {
		return this.settlAcctNum;
	}

	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName == null ? null : acctName.trim();
	}

    /**
     * @return AcctName
     */
	public String getAcctName() {
		return this.acctName;
	}

	/**
	 * @param acctSubNum
	 */
	public void setAcctSubNum(String acctSubNum) {
		this.acctSubNum = acctSubNum == null ? null : acctSubNum.trim();
	}

    /**
     * @return AcctSubNum
     */
	public String getAcctSubNum() {
		return this.acctSubNum;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}

    /**
     * @return CurType
     */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param openOrgId
	 */
	public void setOpenOrgId(String openOrgId) {
		this.openOrgId = openOrgId == null ? null : openOrgId.trim();
	}

    /**
     * @return OpenOrgId
     */
	public String getOpenOrgId() {
		return this.openOrgId;
	}

	/**
	 * @param isEffect
	 */
	public void setIsEffect(String isEffect) {
		this.isEffect = isEffect == null ? null : isEffect.trim();
	}

    /**
     * @return IsEffect
     */
	public String getIsEffect() {
		return this.isEffect;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

    /**
     * @return InputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

    /**
     * @return InputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

    /**
     * @return InputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

    /**
     * @return UpdId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

    /**
     * @return UpdBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

    /**
     * @return UpdDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return CreateTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return UpdateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}