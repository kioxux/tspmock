package cn.com.yusys.yusp.dto.server.cmiscus0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询法人关联客户列表
 *
 * @author dumd
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0003ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//客户编号




    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisCus0003ReqDto{" +
                "cusId='" + cusId + '\'' +
                '}';
    }
}

