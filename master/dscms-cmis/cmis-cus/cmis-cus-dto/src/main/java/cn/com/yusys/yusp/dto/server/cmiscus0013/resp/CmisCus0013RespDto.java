package cn.com.yusys.yusp.dto.server.cmiscus0013.resp;

import java.io.Serializable;
import java.util.Date;

public class CmisCus0013RespDto implements Serializable {
    private static final long serialVersionUID =1L;
    private String pkId;

    /** 客户编号 **/
    private String cusId;

    private String isFamilyMem;
    private String indivCusRel;
    private String certType;
    private String certCode;
    private String name;
    private String cusIdRel;
    private String sex;
    private String yearn;
    private String occu;
    private String duty;
    private String oprType;
    private String remark;
    private String inputId;
    private String inputBrId;
    private String inputDate;
    private String updId;
    private String updBrId;
    private String updDate;
    private String createTime;
    private String updateTime;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsFamilyMem() {
        return isFamilyMem;
    }

    public void setIsFamilyMem(String isFamilyMem) {
        this.isFamilyMem = isFamilyMem;
    }

    public String getIndivCusRel() {
        return indivCusRel;
    }

    public void setIndivCusRel(String indivCusRel) {
        this.indivCusRel = indivCusRel;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCusIdRel() {
        return cusIdRel;
    }

    public void setCusIdRel(String cusIdRel) {
        this.cusIdRel = cusIdRel;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getYearn() {
        return yearn;
    }

    public void setYearn(String yearn) {
        this.yearn = yearn;
    }

    public String getOccu() {
        return occu;
    }

    public void setOccu(String occu) {
        this.occu = occu;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }
}
