package cn.com.yusys.yusp.dto.server.cmiscus0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询客户基本信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CusBaseDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 客户编号
     **/
    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     * 客户名称
     **/
    @JsonProperty(value = "cusName")
    private String cusName;

    /**
     * 客户简称
     **/
    @JsonProperty(value = "cusShortName")
    private String cusShortName;


    /**
     * 证件类型
     **/
    @JsonProperty(value = "certType")
    private String certType;

    /**
     * 证件号码
     **/
    @JsonProperty(value = "certCode")
    private String certCode;

    /**
     * 开户日期
     **/
    @JsonProperty(value = "openDate")
    private String openDate;

    /**
     * 开户类型 STD_ZB_OPEN_TYP
     **/
    @JsonProperty(value = "openType")
    private String openType;

    /**
     * 客户大类
     **/
    @JsonProperty(value = "cusCatalog")
    private String cusCatalog;

    /**
     * 所属条线 STD_ZB_BIZ_BELG
     **/
    @JsonProperty(value = "belgLine")
    private String belgLine;

    /**
     * 信用等级 STD_ZB_CREDIT_GRADE
     **/
    @JsonProperty(value = "cusCrdGrade")
    private String cusCrdGrade;

    /**
     * 客户分类
     **/
    @JsonProperty(value = "cusRankCls")
    private String cusRankCls;

    /**
     * 信用评定到期日期
     **/
    @JsonProperty(value = "cusCrdDt")
    private String cusCrdDt;

    /**
     * 管户客户经理
     **/
    @JsonProperty(value = "managerId")
    private String managerId;

    /**
     * 客户状态 STD_ZB_CUS_ST
     **/
    @JsonProperty(value = "cusState")
    private String cusState;

    /**
     * 主管机构
     **/
    @JsonProperty(value = "mainBrId")
    private String mainBrId;

    /**
     * 登记人
     **/
    @JsonProperty(value = "inputId")
    private String inputId;

    /**
     * 登记机构
     **/
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    /**
     * 登记日期
     **/
    @JsonProperty(value = "inputDate")
    private String inputDate;

    /**
     * 更新人
     **/
    @JsonProperty(value = "updId")
    private String updId;

    /**
     * 更新机构
     **/
    @JsonProperty(value = "updBrId")
    private String updBrId;

    /**
     * 更新日期
     **/
    @JsonProperty(value = "updDate")
    private String updDate;

    /**
     * 操作类型  STD_ZB_OPR_TYPE
     **/
    @JsonProperty(value = "oprType")
    private String oprType;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusShortName() {
        return cusShortName;
    }

    public void setCusShortName(String cusShortName) {
        this.cusShortName = cusShortName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getCusCrdGrade() {
        return cusCrdGrade;
    }

    public void setCusCrdGrade(String cusCrdGrade) {
        this.cusCrdGrade = cusCrdGrade;
    }

    public String getCusRankCls() {
        return cusRankCls;
    }

    public void setCusRankCls(String cusRankCls) {
        this.cusRankCls = cusRankCls;
    }

    public String getCusCrdDt() {
        return cusCrdDt;
    }

    public void setCusCrdDt(String cusCrdDt) {
        this.cusCrdDt = cusCrdDt;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getCusState() {
        return cusState;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    public String getMainBrId() {
        return mainBrId;
    }

    public void setMainBrId(String mainBrId) {
        this.mainBrId = mainBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    @Override
    public String toString() {
        return "CusBaseDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", cusShortName='" + cusShortName + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", openDate='" + openDate + '\'' +
                ", openType='" + openType + '\'' +
                ", cusCatalog='" + cusCatalog + '\'' +
                ", belgLine='" + belgLine + '\'' +
                ", cusCrdGrade='" + cusCrdGrade + '\'' +
                ", cusRankCls='" + cusRankCls + '\'' +
                ", cusCrdDt='" + cusCrdDt + '\'' +
                ", managerId='" + managerId + '\'' +
                ", cusState='" + cusState + '\'' +
                ", mainBrId='" + mainBrId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", oprType='" + oprType + '\'' +
                '}';
    }
}
