package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: FncConfTemplate
 * @类描述: fnc_conf_template数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-03-01 15:11:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class FncConfTemplateDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 报表编号 **/
	private String fncId;
	
	/** 报表名称 **/
	private String fncName;
	
	/** 资产负债表样式编号 **/
	private String fncBsStyleId;
	
	/** 损益表样式编号 **/
	private String fncPlStyleId;
	
	/** 现金流量样式编号 **/
	private String fncCfStyleId;
	
	/** 财务指标样式编号 **/
	private String fncFiStyleId;
	
	/** 所有者权益变动 **/
	private String fncRiStyleId;
	
	/** 财务简表 **/
	private String fncSmpStyleId;
	
	/** 保留字段1 **/
	private String fncStyleId1;
	
	/** 保留字段2 **/
	private String fncStyleId2;
	
	/** 新旧报表标志 STD_ZB_FNC_ON_TYP **/
	private String noInd;
	
	/** 企事业报表标志 STD_ZB_FNC_COMIND **/
	private String comInd;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	
	/**
	 * @param fncId
	 */
	public void setFncId(String fncId) {
		this.fncId = fncId == null ? null : fncId.trim();
	}
	
    /**
     * @return FncId
     */	
	public String getFncId() {
		return this.fncId;
	}
	
	/**
	 * @param fncName
	 */
	public void setFncName(String fncName) {
		this.fncName = fncName == null ? null : fncName.trim();
	}
	
    /**
     * @return FncName
     */	
	public String getFncName() {
		return this.fncName;
	}
	
	/**
	 * @param fncBsStyleId
	 */
	public void setFncBsStyleId(String fncBsStyleId) {
		this.fncBsStyleId = fncBsStyleId == null ? null : fncBsStyleId.trim();
	}
	
    /**
     * @return FncBsStyleId
     */	
	public String getFncBsStyleId() {
		return this.fncBsStyleId;
	}
	
	/**
	 * @param fncPlStyleId
	 */
	public void setFncPlStyleId(String fncPlStyleId) {
		this.fncPlStyleId = fncPlStyleId == null ? null : fncPlStyleId.trim();
	}
	
    /**
     * @return FncPlStyleId
     */	
	public String getFncPlStyleId() {
		return this.fncPlStyleId;
	}
	
	/**
	 * @param fncCfStyleId
	 */
	public void setFncCfStyleId(String fncCfStyleId) {
		this.fncCfStyleId = fncCfStyleId == null ? null : fncCfStyleId.trim();
	}
	
    /**
     * @return FncCfStyleId
     */	
	public String getFncCfStyleId() {
		return this.fncCfStyleId;
	}
	
	/**
	 * @param fncFiStyleId
	 */
	public void setFncFiStyleId(String fncFiStyleId) {
		this.fncFiStyleId = fncFiStyleId == null ? null : fncFiStyleId.trim();
	}
	
    /**
     * @return FncFiStyleId
     */	
	public String getFncFiStyleId() {
		return this.fncFiStyleId;
	}
	
	/**
	 * @param fncRiStyleId
	 */
	public void setFncRiStyleId(String fncRiStyleId) {
		this.fncRiStyleId = fncRiStyleId == null ? null : fncRiStyleId.trim();
	}
	
    /**
     * @return FncRiStyleId
     */	
	public String getFncRiStyleId() {
		return this.fncRiStyleId;
	}
	
	/**
	 * @param fncSmpStyleId
	 */
	public void setFncSmpStyleId(String fncSmpStyleId) {
		this.fncSmpStyleId = fncSmpStyleId == null ? null : fncSmpStyleId.trim();
	}
	
    /**
     * @return FncSmpStyleId
     */	
	public String getFncSmpStyleId() {
		return this.fncSmpStyleId;
	}
	
	/**
	 * @param fncStyleId1
	 */
	public void setFncStyleId1(String fncStyleId1) {
		this.fncStyleId1 = fncStyleId1 == null ? null : fncStyleId1.trim();
	}
	
    /**
     * @return FncStyleId1
     */	
	public String getFncStyleId1() {
		return this.fncStyleId1;
	}
	
	/**
	 * @param fncStyleId2
	 */
	public void setFncStyleId2(String fncStyleId2) {
		this.fncStyleId2 = fncStyleId2 == null ? null : fncStyleId2.trim();
	}
	
    /**
     * @return FncStyleId2
     */	
	public String getFncStyleId2() {
		return this.fncStyleId2;
	}
	
	/**
	 * @param noInd
	 */
	public void setNoInd(String noInd) {
		this.noInd = noInd == null ? null : noInd.trim();
	}
	
    /**
     * @return NoInd
     */	
	public String getNoInd() {
		return this.noInd;
	}
	
	/**
	 * @param comInd
	 */
	public void setComInd(String comInd) {
		this.comInd = comInd == null ? null : comInd.trim();
	}
	
    /**
     * @return ComInd
     */	
	public String getComInd() {
		return this.comInd;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}


}