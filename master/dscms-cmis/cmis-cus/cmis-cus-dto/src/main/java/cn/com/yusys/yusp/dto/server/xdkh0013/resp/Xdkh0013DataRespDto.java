package cn.com.yusys.yusp.dto.server.xdkh0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：优享贷客户白名单信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0013DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "rate")
    private BigDecimal rate;//利率
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批状态

    public Xdkh0013DataRespDto(BigDecimal applyAmt, BigDecimal rate, String approveStatus) {
        this.applyAmt = applyAmt;
        this.rate = rate;
        this.approveStatus = approveStatus;
    }

    public Xdkh0013DataRespDto() {
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    @Override
    public String toString() {
        return "Xdkh0013DataRespDto{" +
                "applyAmt='" + applyAmt + '\'' +
                ", rate=" + rate +
                ", approveStatus='" + approveStatus + '\'' +
                '}';
    }
}