package cn.com.yusys.yusp.dto.server.xdkh0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询客户配偶信息
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0004DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "correCusName")
    private String correCusName;//配偶姓名
    @JsonProperty(value = "correCertNo")
    private String correCertNo;//配偶证件号码
    @JsonProperty(value = "cusIdRel")
    private String cusIdRel;//配偶客户编号
    @JsonProperty(value = "indivCusRel")
    private String indivCusRel;//与客户关系
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "mobile")
    private String mobile;//配偶电话
    @JsonProperty(value = "correManagerId")
    private String correManagerId;//配偶管户客户经理工号
    @JsonProperty(value = "correManagerName")
    private String correManagerName;//配偶管户客户经理姓名
    @JsonProperty(value = "correManagerBrId")
    private String correManagerBrId;//配偶管户客户经理所在机构编号
    @JsonProperty(value = "correManagerBrName")
    private String correManagerBrName;//配偶管户客户经理所在机构名称

    public String getCorreCusName() {
        return correCusName;
    }

    public void setCorreCusName(String correCusName) {
        this.correCusName = correCusName;
    }

    public String getCorreCertNo() {
        return correCertNo;
    }

    public void setCorreCertNo(String correCertNo) {
        this.correCertNo = correCertNo;
    }

    public String getCusIdRel() {
        return cusIdRel;
    }

    public void setCusIdRel(String cusIdRel) {
        this.cusIdRel = cusIdRel;
    }

    public String getIndivCusRel() {
        return indivCusRel;
    }

    public void setIndivCusRel(String indivCusRel) {
        this.indivCusRel = indivCusRel;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCorreManagerId() {
        return correManagerId;
    }

    public void setCorreManagerId(String correManagerId) {
        this.correManagerId = correManagerId;
    }

    public String getCorreManagerName() {
        return correManagerName;
    }

    public void setCorreManagerName(String correManagerName) {
        this.correManagerName = correManagerName;
    }

    public String getCorreManagerBrId() {
        return correManagerBrId;
    }

    public void setCorreManagerBrId(String correManagerBrId) {
        this.correManagerBrId = correManagerBrId;
    }

    public String getCorreManagerBrName() {
        return correManagerBrName;
    }

    public void setCorreManagerBrName(String correManagerBrName) {
        this.correManagerBrName = correManagerBrName;
    }

    @Override
    public String toString() {
        return "Xdkh0004DataRespDto{" +
                "correCusName='" + correCusName + '\'' +
                ", correCertNo='" + correCertNo + '\'' +
                ", cusIdRel='" + cusIdRel + '\'' +
                ", indivCusRel='" + indivCusRel + '\'' +
                ", cusId='" + cusId + '\'' +
                ", mobile='" + mobile + '\'' +
                ", correManagerId='" + correManagerId + '\'' +
                ", correManagerName='" + correManagerName + '\'' +
                ", correManagerBrId='" + correManagerBrId + '\'' +
                ", correManagerBrName='" + correManagerBrName + '\'' +
                '}';
    }
}