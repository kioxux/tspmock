package cn.com.yusys.yusp.dto.server.xdkh0037.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 接口处理类:微业贷新开户信息入库
 *
 * @author zdl
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0037DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户编号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cert_code")
    private String cert_code;//客户名称
    @JsonProperty(value = "cert_type")
    private String cert_type;//客户名称


    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    @Override
    public String toString() {
        return "Xdkh0037DataReqDto{" +
                "cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cert_type='" + cert_type + '\'' +
                '}';
    }
}
