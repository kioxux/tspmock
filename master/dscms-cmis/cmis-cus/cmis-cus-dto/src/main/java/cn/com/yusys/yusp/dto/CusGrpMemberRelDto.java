package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @方法名称: CusGrpMemberRelDto
 * @方法描述: 集团成员列表
 * @参数与返回说明:
 * @算法描述:
 * @创建人: mashun
 * @创建时间: 2021-05-15 14:44:44
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusGrpMemberRelDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 集团编号 **/
	private String grpNo;
	/** 集团编号 **/
	private String grpName;
	/** 成员客户编号 **/
	private String cusId;
	/** 成员客户编号 **/
	private String cusName;
	/** 成员客户类型 **/
	private String cusType;
	/** 成员主管客户经理 **/
	private String managerId;
	/** 成员主管机构 **/
	private String managerBrId;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getGrpNo() {
		return grpNo;
	}

	public void setGrpNo(String grpNo) {
		this.grpNo = grpNo;
	}

	public String getGrpName() {
		return grpName;
	}

	public void setGrpName(String grpName) {
		this.grpName = grpName;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	@Override
	public String toString() {
		return "CusGrpMemberRelDto{" +
				"grpNo='" + grpNo + '\'' +
				", grpName='" + grpName + '\'' +
				", cusId='" + cusId + '\'' +
				", cusName='" + cusName + '\'' +
				", cusType='" + cusType + '\'' +
				", managerId='" + managerId + '\'' +
				", managerBrId='" + managerBrId + '\'' +
				'}';
	}
}