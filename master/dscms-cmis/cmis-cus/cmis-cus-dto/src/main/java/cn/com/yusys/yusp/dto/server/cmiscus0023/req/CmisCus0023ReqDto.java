package cn.com.yusys.yusp.dto.server.cmiscus0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据同业机构行号查找同业客户号
 *
 * @author dumd
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0023ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 业务流水号 **/
    @JsonProperty(value = "bankNo")
    private String bankNo;

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }



    @Override
    public String toString() {
        return "CmisCus0023ReqDto{" +
                "bankNo='" + bankNo + '\'' +
                '}';
    }
}
