package cn.com.yusys.yusp.dto.server.cmiscus0017.resp;

import java.io.Serializable;
import java.math.BigDecimal;

public class Cmiscus0017RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //收入
    private BigDecimal amt;

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    @Override
    public String toString() {
        return "Cmiscus0017RespDto{" +
                "amt=" + amt +
                '}';
    }
}
