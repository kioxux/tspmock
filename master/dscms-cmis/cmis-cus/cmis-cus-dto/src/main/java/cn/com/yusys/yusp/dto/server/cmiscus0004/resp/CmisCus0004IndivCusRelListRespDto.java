package cn.com.yusys.yusp.dto.server.cmiscus0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询法人关联客户列表
 *
 * @author dumd
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0004IndivCusRelListRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusIdRel")
    private String cusIdRel;//关联客户号

    @JsonProperty(value = "cusNameRel")
    private String cusNameRel;//关联客户名称

    @JsonProperty(value = "cusTypeRel")
    private String cusTypeRel;//关联客户大类

    @JsonProperty(value = "relType")
    private String relType;//关联类型

    public String getCusIdRel() {
        return cusIdRel;
    }

    public void setCusIdRel(String cusIdRel) {
        this.cusIdRel = cusIdRel;
    }

    public String getCusNameRel() {
        return cusNameRel;
    }

    public void setCusNameRel(String cusNameRel) {
        this.cusNameRel = cusNameRel;
    }

    public String getCusTypeRel() {
        return cusTypeRel;
    }

    public void setCusTypeRel(String cusTypeRel) {
        this.cusTypeRel = cusTypeRel;
    }

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    @Override
    public String toString() {
        return "CmisCus0004IndivCusRelListRespDto{" +
                "cusIdRel='" + cusIdRel + '\'' +
                ", cusNameRel='" + cusNameRel + '\'' +
                ", cusTypeRel='" + cusTypeRel + '\'' +
                ", relType='" + relType + '\'' +
                '}';
    }
}