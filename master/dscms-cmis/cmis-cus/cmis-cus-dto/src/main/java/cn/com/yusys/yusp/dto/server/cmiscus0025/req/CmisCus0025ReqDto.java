package cn.com.yusys.yusp.dto.server.cmiscus0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：优农贷名单信息查询
 *
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0025ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")//客户编号
    private String cusId;
    @JsonProperty(value = "cusName")//客户编号
    private String cusName;
    @JsonProperty(value = "certCode")//证件编号
    private String certCode;
    @JsonProperty(value = "serno")//流水号
    private String serno;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }
}
