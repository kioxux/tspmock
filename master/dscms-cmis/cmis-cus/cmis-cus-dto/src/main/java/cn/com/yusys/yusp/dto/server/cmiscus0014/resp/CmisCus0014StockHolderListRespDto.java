package cn.com.yusys.yusp.dto.server.cmiscus0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：股东列表
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0014StockHolderListRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String  cusId;//出资人客户编号
    @JsonProperty(value = "invtTyp")
    private String  invtTyp;//出资人性质
    @JsonProperty(value = "certTyp")
    private String  certTyp;//出资人证件类型
    @JsonProperty(value = "certCode")
    private String  certCode;//出资人证件号码
    @JsonProperty(value = "invtName")
    private String  invtName;//出资人名称
    @JsonProperty(value = "cusIdRel")
    private String  cusIdRel;//关联客户编号
    @JsonProperty(value = "country")
    private String  country;//国别
    @JsonProperty(value = "invtType")
    private String  invtType;//出资方式
    @JsonProperty(value = "curType")
    private String  curType;//币种
    @JsonProperty(value = "invtAmt")
    private BigDecimal  invtAmt;//出资金额
    @JsonProperty(value = "invtPerc")
    private BigDecimal  invtPerc;//出资比例
    @JsonProperty(value = "invDate")
    private String  invDate;//出资时间
    @JsonProperty(value = "invtDesc")
    private String  invtDesc;//出资说明
    @JsonProperty(value = "remark")
    private String  remark;//备注

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getInvtTyp() {
        return invtTyp;
    }

    public void setInvtTyp(String invtTyp) {
        this.invtTyp = invtTyp;
    }

    public String getCertTyp() {
        return certTyp;
    }

    public void setCertTyp(String certTyp) {
        this.certTyp = certTyp;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getInvtName() {
        return invtName;
    }

    public void setInvtName(String invtName) {
        this.invtName = invtName;
    }

    public String getCusIdRel() {
        return cusIdRel;
    }

    public void setCusIdRel(String cusIdRel) {
        this.cusIdRel = cusIdRel;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getInvtType() {
        return invtType;
    }

    public void setInvtType(String invtType) {
        this.invtType = invtType;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getInvtAmt() {
        return invtAmt;
    }

    public void setInvtAmt(BigDecimal invtAmt) {
        this.invtAmt = invtAmt;
    }

    public BigDecimal getInvtPerc() {
        return invtPerc;
    }

    public void setInvtPerc(BigDecimal invtPerc) {
        this.invtPerc = invtPerc;
    }

    public String getInvDate() {
        return invDate;
    }

    public void setInvDate(String invDate) {
        this.invDate = invDate;
    }

    public String getInvtDesc() {
        return invtDesc;
    }

    public void setInvtDesc(String invtDesc) {
        this.invtDesc = invtDesc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "CmisCus0014StockHolderListRespDto{" +
                "cusId='" + cusId + '\'' +
                ", invtTyp='" + invtTyp + '\'' +
                ", certTyp='" + certTyp + '\'' +
                ", certCode='" + certCode + '\'' +
                ", invtName='" + invtName + '\'' +
                ", cusIdRel='" + cusIdRel + '\'' +
                ", country='" + country + '\'' +
                ", invtType='" + invtType + '\'' +
                ", curType='" + curType + '\'' +
                ", invtAmt=" + invtAmt +
                ", invtPerc=" + invtPerc +
                ", invDate='" + invDate + '\'' +
                ", invtDesc='" + invtDesc + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}


