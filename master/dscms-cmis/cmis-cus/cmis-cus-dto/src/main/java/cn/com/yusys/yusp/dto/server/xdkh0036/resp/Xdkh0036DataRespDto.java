package cn.com.yusys.yusp.dto.server.xdkh0036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：违约认定、重生认定评级信息同步
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0036DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "cusName")
	private String cusName;// 客户名称

	@JsonProperty(value = "address")
	private String address;// 居住地址

	@JsonProperty(value = "mobile")
	private String mobile;// 手机

	@JsonProperty(value = "qq")
	private String qq;// qq

	@JsonProperty(value = "wechat")
	private String wechat;// 微信

	@JsonProperty(value = "otherContract")
	private String otherContract;// 其他通讯地址

	@JsonProperty(value = "email")
	private String email;// 邮件

	@JsonProperty(value = "faxCode")
	private String faxCode;// 传真

	@JsonProperty(value = "freqLinkman")
	private String freqLinkman;// 常用联系人

	public String getFreqLinkman() {
		return freqLinkman;
	}

	public void setFreqLinkman(String freqLinkman) {
		this.freqLinkman = freqLinkman;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getOtherContract() {
		return otherContract;
	}

	public void setOtherContract(String otherContract) {
		this.otherContract = otherContract;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFaxCode() {
		return faxCode;
	}

	public void setFaxCode(String faxCode) {
		this.faxCode = faxCode;
	}

	@Override
	public String toString() {
		return "Xdkh0036DataRespDto{" +
				"cusName='" + cusName + '\'' +
				", address='" + address + '\'' +
				", mobile='" + mobile + '\'' +
				", qq='" + qq + '\'' +
				", wechat='" + wechat + '\'' +
				", otherContract='" + otherContract + '\'' +
				", email='" + email + '\'' +
				", faxCode='" + faxCode + '\'' +
				", freqLinkman='" + freqLinkman + '\'' +
				'}';
	}
}
