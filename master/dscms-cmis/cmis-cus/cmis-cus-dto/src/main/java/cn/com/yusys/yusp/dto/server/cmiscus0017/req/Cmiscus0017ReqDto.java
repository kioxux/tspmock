package cn.com.yusys.yusp.dto.server.cmiscus0017.req;

import java.io.Serializable;

public class Cmiscus0017ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //客户编号
    private String cusId;
    //财报年份
    private String year;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Cmiscus0017ReqDto{" +
                "cusId='" + cusId + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
