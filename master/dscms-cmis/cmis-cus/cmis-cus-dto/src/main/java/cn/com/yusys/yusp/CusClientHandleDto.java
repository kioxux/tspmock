package cn.com.yusys.yusp;

/**
 * 处理客户信息交互类
 */
public class CusClientHandleDto {
    //入参信息
    private String cusId;//客户编号
    private String serno;//业务流水号

    //出参信息
    private String rtnCode;//返回码值，成功默认000000，异常默认999999
    private String rtnMsg;//返回信息

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusId() {
        return cusId;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
}
