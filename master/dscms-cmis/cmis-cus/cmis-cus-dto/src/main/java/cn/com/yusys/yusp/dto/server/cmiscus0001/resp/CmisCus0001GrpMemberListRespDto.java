package cn.com.yusys.yusp.dto.server.cmiscus0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：集团成员列表查询
 *
 * @author dumd
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0001GrpMemberListRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//成员客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//成员客户名称
    @JsonProperty(value = "cusCatalog")
    private String cusCatalog;//成员客户大类
    @JsonProperty(value = "cusType")
    private String cusType;//成员客户类型
    @JsonProperty(value = "managerId")
    private String managerId;//成员管护客户经理ID
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//成员管护机构
    @JsonProperty(value = "grpCorreType")
    private String grpCorreType;//集团关联关系类型

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getGrpCorreType() {
        return grpCorreType;
    }

    public void setGrpCorreType(String grpCorreType) {
        this.grpCorreType = grpCorreType;
    }

    @Override
    public String toString() {
        return "CmisCus0001GrpMemberListRespDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", cusCatalog='" + cusCatalog + '\'' +
                ", cusType='" + cusType + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", grpCorreType='" + grpCorreType + '\'' +
                '}';
    }
}

