package cn.com.yusys.yusp.dto.server.cmiscus0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 请求Dto：查询根据客户编号,证件类型，证件号码查询同业客户基本信息
 *
 * @author 蒋云龙
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CusIntbankMgrDto {
    private static final long serialVersionUID = 1L;

    /** 同业客户编号 **/
    @JsonProperty(value = "intbankCusId")
    private String intbankCusId;

    /** 高管类别 **/
    @JsonProperty(value = "mrgCls")
    private String mrgCls;

    /** 高管人员名称 **/
    @JsonProperty(value = "mrgName")
    private String mrgName;

    /** 高管证件类型 **/
    @JsonProperty(value = "mrgCertType")
    private String mrgCertType;

    /** 高管证件号码 **/
    @JsonProperty(value = "mrgCertCode")
    private String mrgCertCode;

    /** 高管证件失效日期 **/
    @JsonProperty(value = "expDateManageCert")
    private String expDateManageCert;

    /** 主联系人电话 **/
    @JsonProperty(value = "linkmanPhone")
    private String linkmanPhone;

    /** 居住地址 **/
    @JsonProperty(value = "resiAddr")
    private String resiAddr;

    /** 控制人类型 **/
    @JsonProperty(value = "typeController")
    private String typeController;

    /** 资料管理 **/
    @JsonProperty(value = "dataManagement")
    private String dataManagement;

    public String getIntbankCusId() {
        return intbankCusId;
    }

    public void setIntbankCusId(String intbankCusId) {
        this.intbankCusId = intbankCusId;
    }

    public String getMrgCls() {
        return mrgCls;
    }

    public void setMrgCls(String mrgCls) {
        this.mrgCls = mrgCls;
    }

    public String getMrgName() {
        return mrgName;
    }

    public void setMrgName(String mrgName) {
        this.mrgName = mrgName;
    }

    public String getMrgCertType() {
        return mrgCertType;
    }

    public void setMrgCertType(String mrgCertType) {
        this.mrgCertType = mrgCertType;
    }

    public String getMrgCertCode() {
        return mrgCertCode;
    }

    public void setMrgCertCode(String mrgCertCode) {
        this.mrgCertCode = mrgCertCode;
    }

    public String getExpDateManageCert() {
        return expDateManageCert;
    }

    public void setExpDateManageCert(String expDateManageCert) {
        this.expDateManageCert = expDateManageCert;
    }

    public String getLinkmanPhone() {
        return linkmanPhone;
    }

    public void setLinkmanPhone(String linkmanPhone) {
        this.linkmanPhone = linkmanPhone;
    }

    public String getResiAddr() {
        return resiAddr;
    }

    public void setResiAddr(String resiAddr) {
        this.resiAddr = resiAddr;
    }

    public String getTypeController() {
        return typeController;
    }

    public void setTypeController(String typeController) {
        this.typeController = typeController;
    }

    public String getDataManagement() {
        return dataManagement;
    }

    public void setDataManagement(String dataManagement) {
        this.dataManagement = dataManagement;
    }

    @Override
    public String toString() {
        return "CusIntbankMgrDto{" +
                "intbankCusId='" + intbankCusId + '\'' +
                ", mrgCls='" + mrgCls + '\'' +
                ", mrgName='" + mrgName + '\'' +
                ", mrgCertType='" + mrgCertType + '\'' +
                ", mrgCertCode='" + mrgCertCode + '\'' +
                ", expDateManageCert='" + expDateManageCert + '\'' +
                ", linkmanPhone='" + linkmanPhone + '\'' +
                ", resiAddr='" + resiAddr + '\'' +
                ", typeController='" + typeController + '\'' +
                ", dataManagement='" + dataManagement + '\'' +
                '}';
    }
}
