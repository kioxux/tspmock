package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * 请求DTO：个人客户信息查看
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
//@JsonPropertyOrder(alphabetic = true)
public class CusIndivQueryReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //    @JsonProperty(value = "cusId")
//    @Size(max = 10)
//    @NotBlank(message = "交易日期不能为空")
    private String cusId;//客户编号
    private String cusName;//客户名称
    private String certType;//证件类型
    private String certNo;//证件号码

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "CusIndivQueryReqDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                '}';
    }
}
