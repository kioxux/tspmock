package cn.com.yusys.yusp.dto.server.xdkh0030.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：公司客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0030DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusInfoList")
    private java.util.List<CusInfoList> cusInfoList;
    @JsonProperty(value = "mainPersonInfoList")
    private java.util.List<MainPersonInfoList> mainPersonInfoList;
    @JsonProperty(value = "cprtFncInfo")
    private String cprtFncInfo;//公司客户财报信息

    public List<CusInfoList> getCusInfoList() {
        return cusInfoList;
    }

    public void setCusInfoList(List<CusInfoList> cusInfoList) {
        this.cusInfoList = cusInfoList;
    }

    public List<MainPersonInfoList> getMainPersonInfoList() {
        return mainPersonInfoList;
    }

    public void setMainPersonInfoList(List<MainPersonInfoList> mainPersonInfoList) {
        this.mainPersonInfoList = mainPersonInfoList;
    }

    public String getCprtFncInfo() {
        return cprtFncInfo;
    }

    public void setCprtFncInfo(String cprtFncInfo) {
        this.cprtFncInfo = cprtFncInfo;
    }

    @Override
    public String toString() {
        return "Xdkh0030DataRespDto{" +
                "cusInfoList=" + cusInfoList +
                ", mainPersonInfoList=" + mainPersonInfoList +
                ", cprtFncInfo='" + cprtFncInfo + '\'' +
                '}';
    }
}