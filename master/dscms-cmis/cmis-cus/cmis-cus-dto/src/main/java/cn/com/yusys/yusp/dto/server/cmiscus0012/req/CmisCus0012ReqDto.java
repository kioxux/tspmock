package cn.com.yusys.yusp.dto.server.cmiscus0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：对公高管信息查询
 *
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0012ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")//客户编号
    private String cusId;
    @JsonProperty(value = "certCode")
    private String certCode;//证件编号
    @JsonProperty(value = "mrgType")
    private String mrgType;//高管类型
    @JsonProperty(value = "mrgCertType")
    private String mrgCertType;//高管类型
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getMrgCertType() {
        return mrgCertType;
    }

    public void setMrgCertType(String mrgCertType) {
        this.mrgCertType = mrgCertType;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusId() {
        return this.cusId;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getMrgType() {
        return mrgType;
    }

    public void setMrgType(String mrgType) {
        this.mrgType = mrgType;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    @Override
    public String toString() {
        return "CmisCus0012ReqDto{" +
                "cusId='" + cusId + '\'' +
                ", certCode='" + certCode + '\'' +
                ", mrgType='" + mrgType + '\'' +
                ", mrgCertType='" + mrgCertType + '\'' +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}
