package cn.com.yusys.yusp.dto.server.xdkh0037.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 接口处理类:微业贷新开户信息入库
 *
 * @author zdl
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0037DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
}
