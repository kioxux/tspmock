package cn.com.yusys.yusp.dto;

public class OperationRespDto {
    //操作表示S成功F失败
    private String opFlag;
    private String opMsg;

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }
}
