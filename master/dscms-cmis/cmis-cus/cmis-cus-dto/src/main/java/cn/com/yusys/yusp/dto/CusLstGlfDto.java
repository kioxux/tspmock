package cn.com.yusys.yusp.dto;

import java.util.Date;

public class CusLstGlfDto {
    private static final long serialVersionUID = 1L;

    /** 业务流水号 **/
    private String serno;

    /** 关联方类型 **/
    private String relatedPartyType;

    /** 关联方名称 **/
    private String relatedPartyName;

    /** 证件类型 **/
    private String certType;

    /** 证件号码 **/
    private String certCode;

    /** 客户编号 **/
    private String cusId;

    /** 境内/境外 **/
    private String lcaos;

    /** 归属组别 **/
    private String belongGroup;

    /** 层级标识 **/
    private String levels;

    /** 上一级关联方名称 **/
    private String parebtRelatedPartyName;

    /** 上一级关联方证件号码 **/
    private String parebtRelatedPartyCertNo;

    /** 与上一级关联方关系 **/
    private String parebtRelatedPartyRela;

    /** 与上一级之间是否有权益份额 **/
    private String parebtBetweenIsRightsShare;

    /** 持有比例 **/
    private String holdPerc;

    /** 关联关系说明 **/
    private String correMemo;

    /** 登记日期 **/
    private String inputDate;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 状态 **/
    private String status;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRelatedPartyType() {
        return relatedPartyType;
    }

    public void setRelatedPartyType(String relatedPartyType) {
        this.relatedPartyType = relatedPartyType;
    }

    public String getRelatedPartyName() {
        return relatedPartyName;
    }

    public void setRelatedPartyName(String relatedPartyName) {
        this.relatedPartyName = relatedPartyName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLcaos() {
        return lcaos;
    }

    public void setLcaos(String lcaos) {
        this.lcaos = lcaos;
    }

    public String getBelongGroup() {
        return belongGroup;
    }

    public void setBelongGroup(String belongGroup) {
        this.belongGroup = belongGroup;
    }

    public String getLevels() {
        return levels;
    }

    public void setLevels(String levels) {
        this.levels = levels;
    }

    public String getParebtRelatedPartyName() {
        return parebtRelatedPartyName;
    }

    public void setParebtRelatedPartyName(String parebtRelatedPartyName) {
        this.parebtRelatedPartyName = parebtRelatedPartyName;
    }

    public String getParebtRelatedPartyCertNo() {
        return parebtRelatedPartyCertNo;
    }

    public void setParebtRelatedPartyCertNo(String parebtRelatedPartyCertNo) {
        this.parebtRelatedPartyCertNo = parebtRelatedPartyCertNo;
    }

    public String getParebtRelatedPartyRela() {
        return parebtRelatedPartyRela;
    }

    public void setParebtRelatedPartyRela(String parebtRelatedPartyRela) {
        this.parebtRelatedPartyRela = parebtRelatedPartyRela;
    }

    public String getParebtBetweenIsRightsShare() {
        return parebtBetweenIsRightsShare;
    }

    public void setParebtBetweenIsRightsShare(String parebtBetweenIsRightsShare) {
        this.parebtBetweenIsRightsShare = parebtBetweenIsRightsShare;
    }

    public String getHoldPerc() {
        return holdPerc;
    }

    public void setHoldPerc(String holdPerc) {
        this.holdPerc = holdPerc;
    }

    public String getCorreMemo() {
        return correMemo;
    }

    public void setCorreMemo(String correMemo) {
        this.correMemo = correMemo;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CusLstGlfDto{" +
                "serno='" + serno + '\'' +
                ", relatedPartyType='" + relatedPartyType + '\'' +
                ", relatedPartyName='" + relatedPartyName + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", cusId='" + cusId + '\'' +
                ", lcaos='" + lcaos + '\'' +
                ", belongGroup='" + belongGroup + '\'' +
                ", levels='" + levels + '\'' +
                ", parebtRelatedPartyName='" + parebtRelatedPartyName + '\'' +
                ", parebtRelatedPartyCertNo='" + parebtRelatedPartyCertNo + '\'' +
                ", parebtRelatedPartyRela='" + parebtRelatedPartyRela + '\'' +
                ", parebtBetweenIsRightsShare='" + parebtBetweenIsRightsShare + '\'' +
                ", holdPerc='" + holdPerc + '\'' +
                ", correMemo='" + correMemo + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", status='" + status + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
