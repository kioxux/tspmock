package cn.com.yusys.yusp.dto.server.xdkh0032.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：信息锁定标志同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0032DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "lockStatus")
    private String lockStatus;//锁定状态
    @JsonProperty(value = "dtghFlag")
    private String dtghFlag;//区分标识

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }

    public String getDtghFlag() {
        return dtghFlag;
    }

    public void setDtghFlag(String dtghFlag) {
        this.dtghFlag = dtghFlag;
    }

    @Override
    public String toString() {
        return "Xdkh0032DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", lockStatus='" + lockStatus + '\'' +
                ", dtghFlag='" + dtghFlag + '\'' +
                '}';
    }
}