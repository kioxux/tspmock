package cn.com.yusys.yusp.dto.server.xdkh0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：对私客户信息同步
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ReqList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "relttp")
    private String relttp;//关系类型
    @JsonProperty(value = "realna")
    private String realna;//关系人姓名
    @JsonProperty(value = "cstno1")
    private String cstno1;//关系人客户编号
    @JsonProperty(value = "rlennm")
    private String rlennm;//关系人英文名称
    @JsonProperty(value = "idtftp")
    private String idtftp;//证件类型
    @JsonProperty(value = "idtfno")
    private String idtfno;//证件号码
    @JsonProperty(value = "efctdt")
    private String efctdt;//证件生效日期
    @JsonProperty(value = "inefdt")
    private String inefdt;//证件失效日期
    @JsonProperty(value = "invswy")
    private String invswy;//出资方式
    @JsonProperty(value = "currcy")
    private String currcy;//出资币种
    @JsonProperty(value = "invsam")
    private BigDecimal invsam;//出资金额
    @JsonProperty(value = "invsrt")
    private BigDecimal invsrt;//出资占比
    @JsonProperty(value = "sthdtg")
    private String sthdtg;//是否控股股东
    @JsonProperty(value = "lrattr")
    private String lrattr;//法人属性
    @JsonProperty(value = "prpsex")
    private String prpsex;//性别
    @JsonProperty(value = "borndt")
    private String borndt;//出生日期
    @JsonProperty(value = "nation")
    private String nation;//国籍
    @JsonProperty(value = "cutycd")
    private String cutycd;//民族
    @JsonProperty(value = "propts")
    private String propts;//居民性质
    @JsonProperty(value = "educlv")
    private String educlv;//教育水平（学历）
    @JsonProperty(value = "wkutna")
    private String wkutna;//工作单位
    @JsonProperty(value = "projob")
    private String projob;//职业
    @JsonProperty(value = "poston")
    private String poston;//职务
    @JsonProperty(value = "income")
    private BigDecimal income;//月收入
    @JsonProperty(value = "tbcome")
    private BigDecimal tbcome;//年收入
    @JsonProperty(value = "provce")
    private String provce;//省/直辖市/自治区
    @JsonProperty(value = "cityna")
    private String cityna;//州市(地市或城市)
    @JsonProperty(value = "county")
    private String county;//县/区
    @JsonProperty(value = "homead")
    private String homead;//联系地址
    @JsonProperty(value = "hometl")
    private String hometl;//联系电话
    @JsonProperty(value = "emailx")
    private String emailx;//邮箱
    @JsonProperty(value = "txpstp")
    private String txpstp;//税收居民类型
    @JsonProperty(value = "addttr")
    private String addttr;//税收现居英文地址
    @JsonProperty(value = "txbcty")
    private String txbcty;//税收出生国家或地区
    @JsonProperty(value = "txnion")
    private String txnion;//税收居民国（地区）
    @JsonProperty(value = "taxnum")
    private String taxnum;//纳税人识别号
    @JsonProperty(value = "notxrs")
    private String notxrs;//不能提供纳税人识别号原因
    @JsonProperty(value = "etenna")
    private String etenna;//企业英文名称
    @JsonProperty(value = "econtp")
    private String econtp;//经济类型
    @JsonProperty(value = "foundt")
    private String foundt;//成立日期
    @JsonProperty(value = "busisp")
    private String busisp;//经营范围
    @JsonProperty(value = "emplnm")
    private Integer emplnm;//员工总数
    @JsonProperty(value = "corppr")
    private String corppr;//所属行业
    @JsonProperty(value = "lawcna")
    private String lawcna;//法人代表名称
    @JsonProperty(value = "lwidtf")
    private String lwidtf;//法人代表证件类型
    @JsonProperty(value = "lwidno")
    private String lwidno;//法人代表证件号码
    @JsonProperty(value = "lwidph")
    private String lwidph;//法人代表联系电话

    public String getRelttp() {
        return relttp;
    }

    public void setRelttp(String relttp) {
        this.relttp = relttp;
    }

    public String getRealna() {
        return realna;
    }

    public void setRealna(String realna) {
        this.realna = realna;
    }

    public String getCstno1() {
        return cstno1;
    }

    public void setCstno1(String cstno1) {
        this.cstno1 = cstno1;
    }

    public String getRlennm() {
        return rlennm;
    }

    public void setRlennm(String rlennm) {
        this.rlennm = rlennm;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getEfctdt() {
        return efctdt;
    }

    public void setEfctdt(String efctdt) {
        this.efctdt = efctdt;
    }

    public String getInefdt() {
        return inefdt;
    }

    public void setInefdt(String inefdt) {
        this.inefdt = inefdt;
    }

    public String getInvswy() {
        return invswy;
    }

    public void setInvswy(String invswy) {
        this.invswy = invswy;
    }

    public String getCurrcy() {
        return currcy;
    }

    public void setCurrcy(String currcy) {
        this.currcy = currcy;
    }

    public BigDecimal getInvsam() {
        return invsam;
    }

    public void setInvsam(BigDecimal invsam) {
        this.invsam = invsam;
    }

    public BigDecimal getInvsrt() {
        return invsrt;
    }

    public void setInvsrt(BigDecimal invsrt) {
        this.invsrt = invsrt;
    }

    public String getSthdtg() {
        return sthdtg;
    }

    public void setSthdtg(String sthdtg) {
        this.sthdtg = sthdtg;
    }

    public String getLrattr() {
        return lrattr;
    }

    public void setLrattr(String lrattr) {
        this.lrattr = lrattr;
    }

    public String getPrpsex() {
        return prpsex;
    }

    public void setPrpsex(String prpsex) {
        this.prpsex = prpsex;
    }

    public String getBorndt() {
        return borndt;
    }

    public void setBorndt(String borndt) {
        this.borndt = borndt;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getCutycd() {
        return cutycd;
    }

    public void setCutycd(String cutycd) {
        this.cutycd = cutycd;
    }

    public String getPropts() {
        return propts;
    }

    public void setPropts(String propts) {
        this.propts = propts;
    }

    public String getEduclv() {
        return educlv;
    }

    public void setEduclv(String educlv) {
        this.educlv = educlv;
    }

    public String getWkutna() {
        return wkutna;
    }

    public void setWkutna(String wkutna) {
        this.wkutna = wkutna;
    }

    public String getProjob() {
        return projob;
    }

    public void setProjob(String projob) {
        this.projob = projob;
    }

    public String getPoston() {
        return poston;
    }

    public void setPoston(String poston) {
        this.poston = poston;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getTbcome() {
        return tbcome;
    }

    public void setTbcome(BigDecimal tbcome) {
        this.tbcome = tbcome;
    }

    public String getProvce() {
        return provce;
    }

    public void setProvce(String provce) {
        this.provce = provce;
    }

    public String getCityna() {
        return cityna;
    }

    public void setCityna(String cityna) {
        this.cityna = cityna;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getHometl() {
        return hometl;
    }

    public void setHometl(String hometl) {
        this.hometl = hometl;
    }

    public String getEmailx() {
        return emailx;
    }

    public void setEmailx(String emailx) {
        this.emailx = emailx;
    }

    public String getTxpstp() {
        return txpstp;
    }

    public void setTxpstp(String txpstp) {
        this.txpstp = txpstp;
    }

    public String getAddttr() {
        return addttr;
    }

    public void setAddttr(String addttr) {
        this.addttr = addttr;
    }

    public String getTxbcty() {
        return txbcty;
    }

    public void setTxbcty(String txbcty) {
        this.txbcty = txbcty;
    }

    public String getTxnion() {
        return txnion;
    }

    public void setTxnion(String txnion) {
        this.txnion = txnion;
    }

    public String getTaxnum() {
        return taxnum;
    }

    public void setTaxnum(String taxnum) {
        this.taxnum = taxnum;
    }

    public String getNotxrs() {
        return notxrs;
    }

    public void setNotxrs(String notxrs) {
        this.notxrs = notxrs;
    }

    public String getEtenna() {
        return etenna;
    }

    public void setEtenna(String etenna) {
        this.etenna = etenna;
    }

    public String getEcontp() {
        return econtp;
    }

    public void setEcontp(String econtp) {
        this.econtp = econtp;
    }

    public String getFoundt() {
        return foundt;
    }

    public void setFoundt(String foundt) {
        this.foundt = foundt;
    }

    public String getBusisp() {
        return busisp;
    }

    public void setBusisp(String busisp) {
        this.busisp = busisp;
    }

    public Integer getEmplnm() {
        return emplnm;
    }

    public void setEmplnm(Integer emplnm) {
        this.emplnm = emplnm;
    }

    public String getCorppr() {
        return corppr;
    }

    public void setCorppr(String corppr) {
        this.corppr = corppr;
    }

    public String getLawcna() {
        return lawcna;
    }

    public void setLawcna(String lawcna) {
        this.lawcna = lawcna;
    }

    public String getLwidtf() {
        return lwidtf;
    }

    public void setLwidtf(String lwidtf) {
        this.lwidtf = lwidtf;
    }

    public String getLwidno() {
        return lwidno;
    }

    public void setLwidno(String lwidno) {
        this.lwidno = lwidno;
    }

    public String getLwidph() {
        return lwidph;
    }

    public void setLwidph(String lwidph) {
        this.lwidph = lwidph;
    }

    @Override
    public String toString() {
        return "ReqList{" +
                "relttp='" + relttp + '\'' +
                ", realna='" + realna + '\'' +
                ", cstno1='" + cstno1 + '\'' +
                ", rlennm='" + rlennm + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", efctdt='" + efctdt + '\'' +
                ", inefdt='" + inefdt + '\'' +
                ", invswy='" + invswy + '\'' +
                ", currcy='" + currcy + '\'' +
                ", invsam=" + invsam +
                ", invsrt=" + invsrt +
                ", sthdtg='" + sthdtg + '\'' +
                ", lrattr='" + lrattr + '\'' +
                ", prpsex='" + prpsex + '\'' +
                ", borndt='" + borndt + '\'' +
                ", nation='" + nation + '\'' +
                ", cutycd='" + cutycd + '\'' +
                ", propts='" + propts + '\'' +
                ", educlv='" + educlv + '\'' +
                ", wkutna='" + wkutna + '\'' +
                ", projob='" + projob + '\'' +
                ", poston='" + poston + '\'' +
                ", income=" + income +
                ", tbcome=" + tbcome +
                ", provce='" + provce + '\'' +
                ", cityna='" + cityna + '\'' +
                ", county='" + county + '\'' +
                ", homead='" + homead + '\'' +
                ", hometl='" + hometl + '\'' +
                ", emailx='" + emailx + '\'' +
                ", txpstp='" + txpstp + '\'' +
                ", addttr='" + addttr + '\'' +
                ", txbcty='" + txbcty + '\'' +
                ", txnion='" + txnion + '\'' +
                ", taxnum='" + taxnum + '\'' +
                ", notxrs='" + notxrs + '\'' +
                ", etenna='" + etenna + '\'' +
                ", econtp='" + econtp + '\'' +
                ", foundt='" + foundt + '\'' +
                ", busisp='" + busisp + '\'' +
                ", emplnm=" + emplnm +
                ", corppr='" + corppr + '\'' +
                ", lawcna='" + lawcna + '\'' +
                ", lwidtf='" + lwidtf + '\'' +
                ", lwidno='" + lwidno + '\'' +
                ", lwidph='" + lwidph + '\'' +
                '}';
    }
}
