package cn.com.yusys.yusp.dto.server.cmiscus0023.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据同业机构行号查找同业客户号
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0023RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 客户编号 **/
    @JsonProperty(value = "cusId")
    private String cusId;

    @JsonProperty(value = "errorCode")
    private String errorCode;

    @JsonProperty(value = "errorMsg")
    private String errorMsg;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "CmisCus0023RespDto{" +
                "cusId='" + cusId + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
