package cn.com.yusys.yusp.dto.server.cmiscus0021.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：优农贷（经营信息）记录生成
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0021RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "result")//返回结果
    private Integer result;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CmisCus0021RespDto{" +
                "result='" + result + '\'' +
                '}';
    }

}
