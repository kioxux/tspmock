package cn.com.yusys.yusp.dto;

/**
 * 接口查询不宜贷款户-交互dto
 */
public class QueryCusPubBlacklistRsDto {
    //入参数据
    private String cusId;//客户编号

    //出参数据
    private String rtnCode;//返回码值，成功默认"000000"
    private String rtnMsg;//返回信息
    //不宜贷款户对象
    private CusPubBlacklistRsClientDto cusPubBlacklistRsClientDto;

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusId() {
        return cusId;
    }

    public CusPubBlacklistRsClientDto getCusPubBlacklistRsClientDto() {
        return cusPubBlacklistRsClientDto;
    }

    public void setCusPubBlacklistRsClientDto(CusPubBlacklistRsClientDto cusPubBlacklistRsClientDto) {
        this.cusPubBlacklistRsClientDto = cusPubBlacklistRsClientDto;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
}
