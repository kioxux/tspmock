package cn.com.yusys.yusp.dto.server.cmiscus0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：集团成员列表查询
 *
 * @author dumd
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0001ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "grpNo")
    private String grpNo;//集团客户编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号

    public String getGrpNo() {
        return grpNo;
    }

    public void setGrpNo(String grpNo) {
        this.grpNo = grpNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisCus0004ReqDto{" +
                "grpNo='" + grpNo + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}

