package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-dto模块
 * @类名称: CusMgrDividePercDto
 * @类描述: cus_mgr_divide_perc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 14:22:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusMgrDividePercDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 主键 **/
	private String pkId;
	/** 客户编号 **/
	private String cusId;
	/** 客户经理工号 **/
	private String managerId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 客户经理名称 **/
	private String managerName;
	
	/** 客户经理机构 **/
	private String managerBrId;
	
	/** 分成比例 **/
	private java.math.BigDecimal dividePerc;
	
	/** 客户经理性质 **/
	private String managerProp;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 创建时间 **/
	private Date createTime;
	
	/** 修改时间 **/
	private Date updateTime;

	/** 是否生效 **/
	private String isEffect;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}

	/**
	 * @return PkId
	 */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName == null ? null : managerName.trim();
	}
	
    /**
     * @return ManagerName
     */	
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param dividePerc
	 */
	public void setDividePerc(java.math.BigDecimal dividePerc) {
		this.dividePerc = dividePerc;
	}
	
    /**
     * @return DividePerc
     */	
	public java.math.BigDecimal getDividePerc() {
		return this.dividePerc;
	}
	
	/**
	 * @param managerProp
	 */
	public void setManagerProp(String managerProp) {
		this.managerProp = managerProp == null ? null : managerProp.trim();
	}
	
    /**
     * @return ManagerProp
     */	
	public String getManagerProp() {
		return this.managerProp;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param isEffect
	 */
	public void setIsEffect(String isEffect) {
		this.isEffect = isEffect == null ? null : isEffect.trim();
	}

	/**
	 * @return IsEffect
	 */
	public String getIsEffect() {
		return this.isEffect;
	}

}