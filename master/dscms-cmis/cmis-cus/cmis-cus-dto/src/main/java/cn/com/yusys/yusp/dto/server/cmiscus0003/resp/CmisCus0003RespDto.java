package cn.com.yusys.yusp.dto.server.cmiscus0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：法人关联客户列表查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0003RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    /**
     * 法人关联台账列表
     */
    private List<CmisCus0003ComCusRelListRespDto> comCusRelList;

    public List<CmisCus0003ComCusRelListRespDto> getComCusRelList() {
        return comCusRelList;
    }

    public void setComCusRelList(List<CmisCus0003ComCusRelListRespDto> comCusRelList) {
        this.comCusRelList = comCusRelList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "Cmiscus0003RespDto{" +
                "errorCode='" + errorCode + '\'' +
                "errorMsg='" + errorMsg + '\'' +
                '}';
    }
}


