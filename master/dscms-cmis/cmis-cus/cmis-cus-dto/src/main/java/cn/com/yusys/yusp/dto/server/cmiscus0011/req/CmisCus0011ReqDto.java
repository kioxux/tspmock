package cn.com.yusys.yusp.dto.server.cmiscus0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询根据证件编号查询优农贷名单信息
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0011ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")//客户编号
    private String cusId;
    @JsonProperty(value = "certCode")
    private String certCode;//证件编号
    @JsonProperty(value = "spouseIdcardNo")
    private String spouseIdcardNo;//配偶证件编号

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusId() {
        return this.cusId;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public void setSpouseIdcardNo(String spouseIdcardNo) {
        this.spouseIdcardNo = spouseIdcardNo;
    }

    public String getSpouseIdcardNo() {
        return this.spouseIdcardNo;
    }

    @Override
    public String toString() {
        return "CmisCus0011ReqDto{" +
                "cusId='" + cusId + '\'' +
                "certCode='" + certCode + '\'' +
                "spouseIdcardNo='" + spouseIdcardNo + '\'' +
                '}';
    }
}
