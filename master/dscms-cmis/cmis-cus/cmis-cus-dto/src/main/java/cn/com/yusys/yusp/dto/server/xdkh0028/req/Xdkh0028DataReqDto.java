package cn.com.yusys.yusp.dto.server.xdkh0028.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：优农贷黑名单查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0028DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "prd_name")
    private String prd_name;//产品名称
    @JsonProperty(value = "prd_code")
    private String prd_code;//产品编号
    @JsonProperty(value = "approve_status")
    private String approve_status;//审批状态
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//流水号

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getApprove_status() {
        return approve_status;
    }

    public void setApprove_status(String approve_status) {
        this.approve_status = approve_status;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    @Override
    public String toString() {
        return "Xdkh0028DataReqDto{" +
                "cert_code='" + cert_code + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", prd_code='" + prd_code + '\'' +
                ", approve_status='" + approve_status + '\'' +
                ", survey_serno='" + survey_serno + '\'' +
                '}';
    }
}