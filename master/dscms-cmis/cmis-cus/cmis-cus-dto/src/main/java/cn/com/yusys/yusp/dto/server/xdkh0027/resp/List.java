package cn.com.yusys.yusp.dto.server.xdkh0027.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：优企贷、优农贷行内关联自然人基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "rel_name")
    private String rel_name;//关联人姓名
    @JsonProperty(value = "rel_cert_code")
    private String rel_cert_code;//关联人证件号
    @JsonProperty(value = "relative_type")
    private String relative_type;//关联人类型
    @JsonProperty(value = "used_ind")
    private String used_ind;//有效标志
    @JsonProperty(value = "update_date")
    private String update_date;//最后修改日期
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "relation_description")
    private String relation_description;//与本行的关系描述

    public String getRel_name() {
        return rel_name;
    }

    public void setRel_name(String rel_name) {
        this.rel_name = rel_name;
    }

    public String getRel_cert_code() {
        return rel_cert_code;
    }

    public void setRel_cert_code(String rel_cert_code) {
        this.rel_cert_code = rel_cert_code;
    }

    public String getRelative_type() {
        return relative_type;
    }

    public void setRelative_type(String relative_type) {
        this.relative_type = relative_type;
    }

    public String getUsed_ind() {
        return used_ind;
    }

    public void setUsed_ind(String used_ind) {
        this.used_ind = used_ind;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRelation_description() {
        return relation_description;
    }

    public void setRelation_description(String relation_description) {
        this.relation_description = relation_description;
    }

    @Override
    public String toString() {
        return "List{" +
                "rel_name='" + rel_name + '\'' +
                ", rel_cert_code='" + rel_cert_code + '\'' +
                ", relative_type='" + relative_type + '\'' +
                ", used_ind='" + used_ind + '\'' +
                ", update_date='" + update_date + '\'' +
                ", serno='" + serno + '\'' +
                ", relation_description='" + relation_description + '\'' +
                '}';
    }
}