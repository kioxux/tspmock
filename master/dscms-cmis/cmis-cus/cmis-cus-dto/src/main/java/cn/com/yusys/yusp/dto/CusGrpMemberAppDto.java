package cn.com.yusys.yusp.dto;

public class CusGrpMemberAppDto {

    /** 主键 **/
    private String pkId;

    /** 申请流水号 **/
    private String serno;

    /** 集团编号 **/
    private String grpNo;

    /** 成员客户编号 **/
    private String cusId;

    /** 集团关联关系类型 STD_ZB_GRP_CO_TYPE **/
    private String grpCorreType;

    /** 集团关联关系描述 **/
    private String grpCorreDetail;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 确认状态 STD_ZB_CONFIRM_STS **/
    private String conStatus;

    /** 操作类型  STD_ZB_OPR_TYPE **/
    private String oprType;

    /** 主管经理 **/
    private String managerId;

    /** 主管部门 **/
    private String managerBrId;

    /** 成员客户类型 **/
    private String cusType;

    /** 成员客户名称 **/
    private String cusName;

    /** 变更类型 **/
    private String modifyType;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    /** 客户大类 **/
    private String cusCatalog;

    /** 是否保留集团额度**/
    private String reserveGrpLimitInd;
    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno;
    }

    /**
     * @return serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param grpNo
     */
    public void setGrpNo(String grpNo) {
        this.grpNo = grpNo;
    }

    /**
     * @return grpNo
     */
    public String getGrpNo() {
        return this.grpNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param grpCorreType
     */
    public void setGrpCorreType(String grpCorreType) {
        this.grpCorreType = grpCorreType;
    }

    /**
     * @return grpCorreType
     */
    public String getGrpCorreType() {
        return this.grpCorreType;
    }

    /**
     * @param grpCorreDetail
     */
    public void setGrpCorreDetail(String grpCorreDetail) {
        this.grpCorreDetail = grpCorreDetail;
    }

    /**
     * @return grpCorreDetail
     */
    public String getGrpCorreDetail() {
        return this.grpCorreDetail;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param conStatus
     */
    public void setConStatus(String conStatus) {
        this.conStatus = conStatus;
    }

    /**
     * @return conStatus
     */
    public String getConStatus() {
        return this.conStatus;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param cusType
     */
    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    /**
     * @return cusType
     */
    public String getCusType() {
        return this.cusType;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    /**
     * @return cusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param modifyType
     */
    public void setModifyType(String modifyType) {
        this.modifyType = modifyType;
    }

    /**
     * @return modifyType
     */
    public String getModifyType() {
        return this.modifyType;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getCusCatalog() {
        return cusCatalog;
    }

    public void setCusCatalog(String cusCatalog) {
        this.cusCatalog = cusCatalog;
    }

    public String getReserveGrpLimitInd() {
        return reserveGrpLimitInd;
    }

    public void setReserveGrpLimitInd(String reserveGrpLimitInd) {
        this.reserveGrpLimitInd = reserveGrpLimitInd;
    }

}