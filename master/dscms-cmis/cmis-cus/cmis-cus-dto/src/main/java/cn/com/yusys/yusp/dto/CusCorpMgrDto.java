package cn.com.yusys.yusp.dto;


import java.io.Serializable;

/**
 * 高管信息Dto
 */
public class CusCorpMgrDto implements Serializable {
    /** 主键 **/
    private String pkId;
    /** 客户编号 **/
    private String cusId;
    /** 关联客户编号 **/
    private String cusIdRel;
    /** 高管证件类型 STD_ZB_CERT_TYP **/
    private String mrgType;
    /** 高管证件号码 **/
    private String mrgCertCode;
    /** 高管姓名 **/
    private String mrgName;
    /** 性别 STD_ZB_SEX **/
    private String mrgSex;
    /** 出生日期 **/
    private String mrgBday;
    /** 高管职业 STD_ZB_OCC **/
    private String mrgOcc;
    /** 高管职务 STD_ZB_JOB_TTL **/
    private String mrgDuty;
    /** 高管职称 STD_ZB_TITLE **/
    private String mrgCrtf;
    /** 高管学历 STD_ZB_EDU **/
    private String mrgEdt;
    /** 高管学位 STD_ZB_DEGREE **/
    private String mrgDgr;
    /** 联系电话 **/
    private String mrgPhone;
    /** 签字样本开始日期 **/
    private String signInitDate;
    /** 签字样本到期日期 **/
    private String signEndDate;
    /** 授权书开始日期 **/
    private String accreditInitDate;
    /** 授权书到期日期 **/
    private String accreditEndDate;
    /** 工作简历 **/
    private String resume;
    /** 备注 **/
    private String remark;
    /** 登记人 **/
    private String inputId;
    /** 登记机构 **/
    private String inputBrId;
    /** 登记日期 **/
    private String inputDate;
    /** 最后修改人 **/
    private String updId;
    /** 最后修改机构 **/
    private String updBrId;
    /** 最后修改日期 **/
    private String updDate;
    /** 操作类型  STD_ZB_OPR_TYPE **/
    private String oprType;
    /** 国别 STD_ZB_STATE_TYPE **/
    private String country;
    /** 证件到期日 **/
    private String certIdate;
    /** 从业日期 **/
    private String fjobDate;
    /** 个人净资产 **/
    private Double indivNas;
    /** 本地居住状况 STD_ZB_LOCAL_RS **/
    private String localResiStatus;
    /** 居住地址 **/
    private String resiAddr;


    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    /**
     * @return cusId
     */
    public String getCusId() {
        return this.cusId;
    }

    public String getCusIdRel() {
        return cusIdRel;
    }

    public void setCusIdRel(String cusIdRel) {
        this.cusIdRel = cusIdRel;
    }

    /**
     * @param mrgType
     */
    public void setMrgType(String mrgType) {
        this.mrgType = mrgType;
    }

    /**
     * @return mrgType
     */
    public String getMrgType() {
        return this.mrgType;
    }

    /**
     * @param mrgCertCode
     */
    public void setMrgCertCode(String mrgCertCode) {
        this.mrgCertCode = mrgCertCode;
    }

    /**
     * @return mrgCertCode
     */
    public String getMrgCertCode() {
        return this.mrgCertCode;
    }

    /**
     * @param mrgName
     */
    public void setMrgName(String mrgName) {
        this.mrgName = mrgName;
    }

    /**
     * @return mrgName
     */
    public String getMrgName() {
        return this.mrgName;
    }

    /**
     * @param mrgSex
     */
    public void setMrgSex(String mrgSex) {
        this.mrgSex = mrgSex;
    }

    /**
     * @return mrgSex
     */
    public String getMrgSex() {
        return this.mrgSex;
    }

    /**
     * @param mrgBday
     */
    public void setMrgBday(String mrgBday) {
        this.mrgBday = mrgBday;
    }

    /**
     * @return mrgBday
     */
    public String getMrgBday() {
        return this.mrgBday;
    }

    /**
     * @param mrgOcc
     */
    public void setMrgOcc(String mrgOcc) {
        this.mrgOcc = mrgOcc;
    }

    /**
     * @return mrgOcc
     */
    public String getMrgOcc() {
        return this.mrgOcc;
    }

    /**
     * @param mrgDuty
     */
    public void setMrgDuty(String mrgDuty) {
        this.mrgDuty = mrgDuty;
    }

    /**
     * @return mrgDuty
     */
    public String getMrgDuty() {
        return this.mrgDuty;
    }

    /**
     * @param mrgCrtf
     */
    public void setMrgCrtf(String mrgCrtf) {
        this.mrgCrtf = mrgCrtf;
    }

    /**
     * @return mrgCrtf
     */
    public String getMrgCrtf() {
        return this.mrgCrtf;
    }

    /**
     * @param mrgEdt
     */
    public void setMrgEdt(String mrgEdt) {
        this.mrgEdt = mrgEdt;
    }

    /**
     * @return mrgEdt
     */
    public String getMrgEdt() {
        return this.mrgEdt;
    }

    /**
     * @param mrgDgr
     */
    public void setMrgDgr(String mrgDgr) {
        this.mrgDgr = mrgDgr;
    }

    /**
     * @return mrgDgr
     */
    public String getMrgDgr() {
        return this.mrgDgr;
    }

    /**
     * @param mrgPhone
     */
    public void setMrgPhone(String mrgPhone) {
        this.mrgPhone = mrgPhone;
    }

    /**
     * @return mrgPhone
     */
    public String getMrgPhone() {
        return this.mrgPhone;
    }

    /**
     * @param signInitDate
     */
    public void setSignInitDate(String signInitDate) {
        this.signInitDate = signInitDate;
    }

    /**
     * @return signInitDate
     */
    public String getSignInitDate() {
        return this.signInitDate;
    }

    /**
     * @param signEndDate
     */
    public void setSignEndDate(String signEndDate) {
        this.signEndDate = signEndDate;
    }

    /**
     * @return signEndDate
     */
    public String getSignEndDate() {
        return this.signEndDate;
    }

    /**
     * @param accreditInitDate
     */
    public void setAccreditInitDate(String accreditInitDate) {
        this.accreditInitDate = accreditInitDate;
    }

    /**
     * @return accreditInitDate
     */
    public String getAccreditInitDate() {
        return this.accreditInitDate;
    }

    /**
     * @param accreditEndDate
     */
    public void setAccreditEndDate(String accreditEndDate) {
        this.accreditEndDate = accreditEndDate;
    }

    /**
     * @return accreditEndDate
     */
    public String getAccreditEndDate() {
        return this.accreditEndDate;
    }

    /**
     * @param resume
     */
    public void setResume(String resume) {
        this.resume = resume;
    }

    /**
     * @return resume
     */
    public String getResume() {
        return this.resume;
    }

    /**
     * @param remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return remark
     */
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return country
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * @param certIdate
     */
    public void setCertIdate(String certIdate) {
        this.certIdate = certIdate;
    }

    /**
     * @return certIdate
     */
    public String getCertIdate() {
        return this.certIdate;
    }

    /**
     * @param fjobDate
     */
    public void setFjobDate(String fjobDate) {
        this.fjobDate = fjobDate;
    }

    /**
     * @return fjobDate
     */
    public String getFjobDate() {
        return this.fjobDate;
    }

    /**
     * @param indivNas
     */
    public void setIndivNas(Double indivNas) {
        this.indivNas = indivNas;
    }

    /**
     * @return indivNas
     */
    public Double getIndivNas() {
        return this.indivNas;
    }

    /**
     * @param localResiStatus
     */
    public void setLocalResiStatus(String localResiStatus) {
        this.localResiStatus = localResiStatus;
    }

    /**
     * @return localResiStatus
     */
    public String getLocalResiStatus() {
        return this.localResiStatus;
    }

    /**
     * @param resiAddr
     */
    public void setResiAddr(String resiAddr) {
        this.resiAddr = resiAddr;
    }

    /**
     * @return resiAddr
     */
    public String getResiAddr() {
        return this.resiAddr;
    }
}
