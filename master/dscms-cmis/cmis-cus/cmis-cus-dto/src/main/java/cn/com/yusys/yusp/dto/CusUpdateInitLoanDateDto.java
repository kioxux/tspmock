package cn.com.yusys.yusp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 更新个人客户基本信息 -建立信贷关系时间
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CusUpdateInitLoanDateDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 客户编号
     **/

    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     * 建立信贷关系时间
     **/
    @JsonProperty(value = "initLoanDate")
    private String initLoanDate;

    /**
     * 更新人
     **/
    @JsonProperty(value = "updId")
    private String updId;

    /**
     * 更新机构
     **/
    @JsonProperty(value = "updBrId")
    private String updBrId;

    /**
     * 更新日期
     **/
    @JsonProperty(value = "updDate")
    private String updDate;

    /**
     * 修改时间
     **/
    @JsonProperty(value = "updateTime")
    private Date updateTime;


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getInitLoanDate() {
        return initLoanDate;
    }

    public void setInitLoanDate(String initLoanDate) {
        this.initLoanDate = initLoanDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CusUpdateInitLoanDateDto{" +
                "cusId='" + cusId + '\'' +
                ", initLoanDate='" + initLoanDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", updateTime=" + updateTime +
                '}';
    }
}

