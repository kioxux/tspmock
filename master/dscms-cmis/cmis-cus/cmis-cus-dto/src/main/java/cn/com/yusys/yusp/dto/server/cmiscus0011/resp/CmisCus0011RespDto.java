package cn.com.yusys.yusp.dto.server.cmiscus0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询根据证件编号查询优农贷名单信息
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0011RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")//流水号
    private String serno;
    @JsonProperty(value = "cusId")//客户编号
    private String cusId;
    @JsonProperty(value = "cusName")//客户姓名
    private String cusName;
    @JsonProperty(value = "certCode")//证件号码
    private String certCode;
    @JsonProperty(value = "mobileNo")//手机号码
    private String mobileNo;
    @JsonProperty(value = "sex")//性别
    private String sex;
    @JsonProperty(value = "edu")//学历
    private String edu;
    @JsonProperty(value = "isHaveChildren")//有无子女
    private String isHaveChildren;
    @JsonProperty(value = "resiType")//居住场所类型
    private String resiType;
    @JsonProperty(value = "familyAddr")//家庭地址
    private String familyAddr;
    @JsonProperty(value = "localResiLmt")//本地居住年限
    private String localResiLmt;
    @JsonProperty(value = "localRegist")//本地户口
    private String localRegist;
    @JsonProperty(value = "operAddr")//经营地址
    private String operAddr;
    @JsonProperty(value = "operLmt")//经营年限
    private String operLmt;
    @JsonProperty(value = "marStatus")//婚姻状况
    private String marStatus;
    @JsonProperty(value = "spouseName")//配偶姓名
    private String spouseName;
    @JsonProperty(value = "spouseIdcardNo")//配偶身份证号码
    private String spouseIdcardNo;
    @JsonProperty(value = "spouseMobileNo")//配偶手机号码
    private String spouseMobileNo;
    @JsonProperty(value = "approveStatus")//审批状态
    private String approveStatus;
    @JsonProperty(value = "imageNo")//影像编号
    private String imageNo;
    @JsonProperty(value = "huser")//经办人
    private String huser;
    @JsonProperty(value = "handOrg")//经办机构
    private String handOrg;
    @JsonProperty(value = "listStatus")//名单状态
    private String listStatus;

    public String getSerno() {return serno; }

    public void setSerno(String serno) { this.serno = serno; }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusId() {
        return this.cusId;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusName() {
        return this.cusName;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCertCode() {
        return this.certCode;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return this.sex;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getEdu() {
        return this.edu;
    }

    public void setIsHaveChildren(String isHaveChildren) {
        this.isHaveChildren = isHaveChildren;
    }

    public String getIsHaveChildren() {
        return this.isHaveChildren;
    }

    public void setResiType(String resiType) {
        this.resiType = resiType;
    }

    public String getResiType() {
        return this.resiType;
    }

    public void setFamilyAddr(String familyAddr) {
        this.familyAddr = familyAddr;
    }

    public String getFamilyAddr() {
        return this.familyAddr;
    }

    public void setLocalResiLmt(String localResiLmt) {
        this.localResiLmt = localResiLmt;
    }

    public String getLocalResiLmt() {
        return this.localResiLmt;
    }

    public void setLocalRegist(String localRegist) {
        this.localRegist = localRegist;
    }

    public String getLocalRegist() {
        return this.localRegist;
    }

    public void setOperAddr(String operAddr) {
        this.operAddr = operAddr;
    }

    public String getOperAddr() {
        return this.operAddr;
    }

    public void setOperLmt(String operLmt) {
        this.operLmt = operLmt;
    }

    public String getOperLmt() {
        return this.operLmt;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getMarStatus() {
        return this.marStatus;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getSpouseName() {
        return this.spouseName;
    }

    public void setSpouseIdcardNo(String spouseIdcardNo) {
        this.spouseIdcardNo = spouseIdcardNo;
    }

    public String getSpouseIdcardNo() {
        return this.spouseIdcardNo;
    }

    public void setSpouseMobileNo(String spouseMobileNo) {
        this.spouseMobileNo = spouseMobileNo;
    }

    public String getSpouseMobileNo() {
        return this.spouseMobileNo;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getApproveStatus() {
        return this.approveStatus;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public String getImageNo() {
        return this.imageNo;
    }

    public void setHuser(String huser) {
        this.huser = huser;
    }

    public String getHuser() {
        return this.huser;
    }

    public void setHandOrg(String handOrg) {
        this.handOrg = handOrg;
    }

    public String getHandOrg() {
        return this.handOrg;
    }

    public void setListStatus(String listStatus) {
        this.listStatus = listStatus;
    }

    public String getListStatus() {
        return this.listStatus;
    }

    @Override
    public String toString() {
        return "CmisCus0011RespDto{" +
                "serno='" + serno + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certCode='" + certCode + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", edu='" + edu + '\'' +
                ", isHaveChildren='" + isHaveChildren + '\'' +
                ", resiType='" + resiType + '\'' +
                ", familyAddr='" + familyAddr + '\'' +
                ", localResiLmt='" + localResiLmt + '\'' +
                ", localRegist='" + localRegist + '\'' +
                ", operAddr='" + operAddr + '\'' +
                ", operLmt='" + operLmt + '\'' +
                ", marStatus='" + marStatus + '\'' +
                ", spouseName='" + spouseName + '\'' +
                ", spouseIdcardNo='" + spouseIdcardNo + '\'' +
                ", spouseMobileNo='" + spouseMobileNo + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", imageNo='" + imageNo + '\'' +
                ", huser='" + huser + '\'' +
                ", handOrg='" + handOrg + '\'' +
                ", listStatus='" + listStatus + '\'' +
                '}';
    }


}
