package cn.com.yusys.yusp.dto.server.cmiscus0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据客户号查询关联方名单信息记录数
 *
 * @author dumd
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0020ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")//客户编号
    private String cusId;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "CmisCus0020ReqDto{" +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
