package cn.com.yusys.yusp.dto.server.xdkh0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Data：对私客户信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0011DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "syccde")
    private String syccde;//同步系统编码
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "CertCode")
    private String CertCode;//证件号码
    @JsonProperty(value = "certStartDate")
    private String certStartDate;//证件生效日期
    @JsonProperty(value = "certEndDate")
    private String certEndDate;//证件失效日期
    @JsonProperty(value = "auxidtftp")
    private String auxidtftp;//辅证件类型
    @JsonProperty(value = "auxidtfno")
    private String auxidtfno;//辅证件号码
    @JsonProperty(value = "auxinefdt")
    private String auxinefdt;//辅证件到期日
    @JsonProperty(value = "areaad")
    private String areaad;//发证机关国家或地区
    @JsonProperty(value = "depart")
    private String depart;//发证机关
    @JsonProperty(value = "cusRankCls")
    private String cusRankCls;//客户状态
    @JsonProperty(value = "csspna")
    private String csspna;//拼音名称
    @JsonProperty(value = "name")
    private String name;//客户英文名
    @JsonProperty(value = "indivSex")
    private String indivSex;//性别
    @JsonProperty(value = "indivBrtPlace")
    private String indivBrtPlace;//籍贯
    @JsonProperty(value = "indivFolk")
    private String indivFolk;//民族
    @JsonProperty(value = "nation")
    private String nation;//国籍
    @JsonProperty(value = "indivDtOfBirth")
    private String indivDtOfBirth;//出生日期
    @JsonProperty(value = "indivPolSt")
    private String indivPolSt;//政治面貌
    @JsonProperty(value = "marStatus")
    private String marStatus;//婚姻状态
    @JsonProperty(value = "isHaveChildren")
    private String isHaveChildren;//是否有子女
    @JsonProperty(value = "brinnm")
    private BigDecimal brinnm;//抚养人数
    @JsonProperty(value = "indivEdt")
    private String indivEdt;//教育水平（学历）
    @JsonProperty(value = "indivDgr")
    private String indivDgr;//学位
    @JsonProperty(value = "gradna")
    private String gradna;//毕业院校
    @JsonProperty(value = "graddt")
    private String graddt;//毕业日期
    @JsonProperty(value = "occu")
    private String occu;//职业
    @JsonProperty(value = "duty")
    private String duty;//职务
    @JsonProperty(value = "indivCrtfctn")
    private String indivCrtfctn;//职称
    @JsonProperty(value = "offibr")
    private String offibr;//任职部门
    @JsonProperty(value = "unitName")
    private String unitName;//工作单位
    @JsonProperty(value = "indivComType")
    private String indivComType;//单位性质
    @JsonProperty(value = "unitAddr")
    private String unitAddr;//单位地址
    @JsonProperty(value = "unitnm")
    private BigDecimal unitnm;//工作单位雇员人数
    @JsonProperty(value = "workdt")
    private String workdt;//参加工作日期
    @JsonProperty(value = "tradeDate")
    private String tradeDate;//参加本行业工作日期
    @JsonProperty(value = "unitDate")
    private String unitDate;//参加本单位工作日期
    @JsonProperty(value = "indivComTrade")
    private String indivComTrade;//单位所属行业
    @JsonProperty(value = "expryr")
    private Integer expryr;//行业经验年限
    @JsonProperty(value = "utwkyr")
    private Integer utwkyr;//现单位工作年限
    @JsonProperty(value = "ctcktg")
    private String ctcktg;//是否联网核查
    @JsonProperty(value = "result")
    private String result;//联网核查结果
    @JsonProperty(value = "indivRsdSt")
    private String indivRsdSt;//居住状况
    @JsonProperty(value = "lvbgdt")
    private String lvbgdt;//本城市居住起始日期
    @JsonProperty(value = "famisz")
    private Integer famisz;//家庭人口
    @JsonProperty(value = "mainic")
    private String mainic;//主要收入来源
    @JsonProperty(value = "earningCurType")
    private String earningCurType;//收入币种
    @JsonProperty(value = "indivMonn")
    private BigDecimal indivMonn;//个人月收入
    @JsonProperty(value = "indivYearn")
    private BigDecimal indivYearn;//个人年收入
    @JsonProperty(value = "familyMonn")
    private BigDecimal familyMonn;//家庭月收入
    @JsonProperty(value = "familyYearn")
    private BigDecimal familyYearn;//家庭年收入
    @JsonProperty(value = "isBankEmployee")
    private String isBankEmployee;//本行员工标志
    @JsonProperty(value = "bankEmployeeId")
    private String bankEmployeeId;//本行员工号
    @JsonProperty(value = "isAgri")
    private String isAgri;//是否农户
    @JsonProperty(value = "isBankSharehd")
    private String isBankSharehd;//是否我行股东
    @JsonProperty(value = "relgon")
    private String relgon;//宗教信仰
    @JsonProperty(value = "proptg")
    private String proptg;//居民标志
    @JsonProperty(value = "onacnm")
    private Integer onacnm;//一类户数量
    @JsonProperty(value = "totlnm")
    private Integer totlnm;//总户数
    @JsonProperty(value = "amlrat")
    private String amlrat;//反洗钱评级
    @JsonProperty(value = "resiYears")
    private String resiYears;//居住年限
    @JsonProperty(value = "healthStatus")
    private String healthStatus;//健康状况
    @JsonProperty(value = "socist")
    private String socist;//社会保障情况
    @JsonProperty(value = "hobyds")
    private String hobyds;//爱好
    @JsonProperty(value = "psrttp")
    private String psrttp;//个人评级类型
    @JsonProperty(value = "isLoan")
    private String isLoan;//是否强村富民贷款
    @JsonProperty(value = "vipptg")
    private String vipptg;//是否VIP客户
    @JsonProperty(value = "credlv")
    private String credlv;//信用等级
    @JsonProperty(value = "comInitLoanDate")
    private String comInitLoanDate;//首次建立信贷关系年月
    @JsonProperty(value = "cnbgdt")
    private String cnbgdt;//往来起始日期
    @JsonProperty(value = "cneddt")
    private String cneddt;//往来终止日期
    @JsonProperty(value = "imagno")
    private String imagno;//影像编号
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理姓名
    @JsonProperty(value = "managerPhone")
    private String managerPhone;//客户经理手机号
    @JsonProperty(value = "txpstp")
    private String txpstp;//税收居民类型
    @JsonProperty(value = "addttr")
    private String addttr;//税收现居英文地址
    @JsonProperty(value = "brctry")
    private String brctry;//税收出生国家或地区
    @JsonProperty(value = "txnion")
    private String txnion;//税收居民国（地区）
    @JsonProperty(value = "taxnum")
    private String taxnum;//纳税人识别号
    @JsonProperty(value = "notxrs")
    private String notxrs;//不能提供纳税人识别号原因
    @JsonProperty(value = "mobileNo")
    private String mobileNo;//移动电话
    @JsonProperty(value = "mobile")
    private String mobile;//固定电话
    @JsonProperty(value = "cncktg")
    private String cncktg;//联系方式验证标识
    @JsonProperty(value = "postCode")
    private String postCode;//联系地址邮编
    @JsonProperty(value = "cncuty")
    private String cncuty;//联系地址所在国家
    @JsonProperty(value = "cnprov")
    private String cnprov;//联系地址所在省
    @JsonProperty(value = "cncity")
    private String cncity;//联系地址所在市
    @JsonProperty(value = "cnarea")
    private String cnarea;//联系地址所在区
    @JsonProperty(value = "homead")
    private String homead;//联系地址信息
    @JsonProperty(value = "QQ")
    private String QQ;//QQ
    @JsonProperty(value = "whchatNo")
    private String whchatNo;//微信
    @JsonProperty(value = "emailx")
    private String emailx;//邮箱
    @JsonProperty(value = "listnm")
    private String listnm;//循环列表记录数
    @JsonProperty(value = "chflag")
    private String chflag;//境内标识
    @JsonProperty(value = "comadd")
    private String comadd;//常用居住地址
    @JsonProperty(value = "nnjytp")
    private String nnjytp;//交易类型
    @JsonProperty(value = "nnjyna")
    private String nnjyna;//交易名称
    @JsonProperty(value = "fullfg")
    private String fullfg;//信息完整性标识
    @JsonProperty(value = "chsdsc")
    private String chsdsc;//校验结果登记说明
    @JsonProperty(value = "prelttp")
    private String prelttp;//电话关系人
    @JsonProperty(value = "passno")
    private String passno;//通行证号码
    @JsonProperty(value = "uphone")
    private String uphone;//单位电话
    @JsonProperty(value = "cusstp")
    private String cusstp;//对私客户类型
    @JsonProperty(value = "resare")
    private String resare;//居住区域编号
    @JsonProperty(value = "resvue")
    private String resvue;//居住区域名称
    @JsonProperty(value = "resadd")
    private String resadd;//居住地址
    @JsonProperty(value = "lastti")
    private String lastti;//最新更新时间
    @JsonProperty(value = "relInfoList")
    private java.util.List<RelInfoList> relInfoList;

    @Override
    public String toString() {
        return "Xdkh0011DataReqDto{" +
                "syccde='" + syccde + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certType='" + certType + '\'' +
                ", CertCode='" + CertCode + '\'' +
                ", certStartDate='" + certStartDate + '\'' +
                ", certEndDate='" + certEndDate + '\'' +
                ", auxidtftp='" + auxidtftp + '\'' +
                ", auxidtfno='" + auxidtfno + '\'' +
                ", auxinefdt='" + auxinefdt + '\'' +
                ", areaad='" + areaad + '\'' +
                ", depart='" + depart + '\'' +
                ", cusRankCls='" + cusRankCls + '\'' +
                ", csspna='" + csspna + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + indivSex + '\'' +
                ", indivBrtPlace='" + indivBrtPlace + '\'' +
                ", indivFolk='" + indivFolk + '\'' +
                ", nation='" + nation + '\'' +
                ", indivDtOfBirth='" + indivDtOfBirth + '\'' +
                ", indivPolSt='" + indivPolSt + '\'' +
                ", marStatus='" + marStatus + '\'' +
                ", isHaveChildren='" + isHaveChildren + '\'' +
                ", brinnm=" + brinnm +
                ", indivEdt='" + indivEdt + '\'' +
                ", indivDgr='" + indivDgr + '\'' +
                ", gradna='" + gradna + '\'' +
                ", graddt='" + graddt + '\'' +
                ", occu='" + occu + '\'' +
                ", duty='" + duty + '\'' +
                ", indivCrtfctn='" + indivCrtfctn + '\'' +
                ", offibr='" + offibr + '\'' +
                ", unitName='" + unitName + '\'' +
                ", indivComType='" + indivComType + '\'' +
                ", unitAddr='" + unitAddr + '\'' +
                ", unitnm=" + unitnm +
                ", workdt='" + workdt + '\'' +
                ", tradeDate='" + tradeDate + '\'' +
                ", unitDate='" + unitDate + '\'' +
                ", indivComTrade='" + indivComTrade + '\'' +
                ", expryr=" + expryr +
                ", utwkyr=" + utwkyr +
                ", ctcktg='" + ctcktg + '\'' +
                ", result='" + result + '\'' +
                ", indivRsdSt='" + indivRsdSt + '\'' +
                ", lvbgdt='" + lvbgdt + '\'' +
                ", famisz=" + famisz +
                ", mainic='" + mainic + '\'' +
                ", earningCurType='" + earningCurType + '\'' +
                ", indivMonn=" + indivMonn +
                ", indivYearn=" + indivYearn +
                ", familyMonn=" + familyMonn +
                ", familyYearn=" + familyYearn +
                ", isBankEmployee='" + isBankEmployee + '\'' +
                ", bankEmployeeId='" + bankEmployeeId + '\'' +
                ", isAgri='" + isAgri + '\'' +
                ", isBankSharehd='" + isBankSharehd + '\'' +
                ", relgon='" + relgon + '\'' +
                ", proptg='" + proptg + '\'' +
                ", onacnm=" + onacnm +
                ", totlnm=" + totlnm +
                ", amlrat='" + amlrat + '\'' +
                ", resiYears='" + resiYears + '\'' +
                ", healthStatus='" + healthStatus + '\'' +
                ", socist='" + socist + '\'' +
                ", hobyds='" + hobyds + '\'' +
                ", psrttp='" + psrttp + '\'' +
                ", isLoan='" + isLoan + '\'' +
                ", vipptg='" + vipptg + '\'' +
                ", credlv='" + credlv + '\'' +
                ", initLoanDate='" + comInitLoanDate + '\'' +
                ", cnbgdt='" + cnbgdt + '\'' +
                ", cneddt='" + cneddt + '\'' +
                ", imagno='" + imagno + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", managerPhone='" + managerPhone + '\'' +
                ", txpstp='" + txpstp + '\'' +
                ", addttr='" + addttr + '\'' +
                ", brctry='" + brctry + '\'' +
                ", txnion='" + txnion + '\'' +
                ", taxnum='" + taxnum + '\'' +
                ", notxrs='" + notxrs + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", mobile='" + mobile + '\'' +
                ", cncktg='" + cncktg + '\'' +
                ", postCode='" + postCode + '\'' +
                ", cncuty='" + cncuty + '\'' +
                ", cnprov='" + cnprov + '\'' +
                ", cncity='" + cncity + '\'' +
                ", cnarea='" + cnarea + '\'' +
                ", homead='" + homead + '\'' +
                ", QQ='" + QQ + '\'' +
                ", whchatNo='" + whchatNo + '\'' +
                ", emailx='" + emailx + '\'' +
                ", listnm='" + listnm + '\'' +
                ", chflag='" + chflag + '\'' +
                ", comadd='" + comadd + '\'' +
                ", nnjytp='" + nnjytp + '\'' +
                ", nnjyna='" + nnjyna + '\'' +
                ", fullfg='" + fullfg + '\'' +
                ", chsdsc='" + chsdsc + '\'' +
                ", prelttp='" + prelttp + '\'' +
                ", passno='" + passno + '\'' +
                ", uphone='" + uphone + '\'' +
                ", cusstp='" + cusstp + '\'' +
                ", resare='" + resare + '\'' +
                ", resvue='" + resvue + '\'' +
                ", resadd='" + resadd + '\'' +
                ", lastti='" + lastti + '\'' +
                ", relInfoList=" + relInfoList +
                '}';
    }

    public String getEmailx() {
        return emailx;
    }

    public void setEmailx(String emailx) {
        this.emailx = emailx;
    }

    public String getListnm() {
        return listnm;
    }

    public void setListnm(String listnm) {
        this.listnm = listnm;
    }

    public String getChflag() {
        return chflag;
    }

    public void setChflag(String chflag) {
        this.chflag = chflag;
    }

    public String getComadd() {
        return comadd;
    }

    public void setComadd(String comadd) {
        this.comadd = comadd;
    }

    public String getNnjytp() {
        return nnjytp;
    }

    public void setNnjytp(String nnjytp) {
        this.nnjytp = nnjytp;
    }

    public String getNnjyna() {
        return nnjyna;
    }

    public void setNnjyna(String nnjyna) {
        this.nnjyna = nnjyna;
    }

    public String getFullfg() {
        return fullfg;
    }

    public void setFullfg(String fullfg) {
        this.fullfg = fullfg;
    }

    public String getChsdsc() {
        return chsdsc;
    }

    public void setChsdsc(String chsdsc) {
        this.chsdsc = chsdsc;
    }

    public String getPrelttp() {
        return prelttp;
    }

    public void setPrelttp(String prelttp) {
        this.prelttp = prelttp;
    }

    public String getPassno() {
        return passno;
    }

    public void setPassno(String passno) {
        this.passno = passno;
    }

    public String getUphone() {
        return uphone;
    }

    public void setUphone(String uphone) {
        this.uphone = uphone;
    }

    public String getCusstp() {
        return cusstp;
    }

    public void setCusstp(String cusstp) {
        this.cusstp = cusstp;
    }

    public String getResare() {
        return resare;
    }

    public void setResare(String resare) {
        this.resare = resare;
    }

    public String getResvue() {
        return resvue;
    }

    public void setResvue(String resvue) {
        this.resvue = resvue;
    }

    public String getResadd() {
        return resadd;
    }

    public void setResadd(String resadd) {
        this.resadd = resadd;
    }

    public String getLastti() {
        return lastti;
    }

    public void setLastti(String lastti) {
        this.lastti = lastti;
    }

    public String getSyccde() {
        return syccde;
    }

    public void setSyccde(String syccde) {
        this.syccde = syccde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return CertCode;
    }

    public void setCertCode(String certCode) {
        CertCode = certCode;
    }

    public String getCertStartDate() {
        return certStartDate;
    }

    public void setCertStartDate(String certStartDate) {
        this.certStartDate = certStartDate;
    }

    public String getCertEndDate() {
        return certEndDate;
    }

    public void setCertEndDate(String certEndDate) {
        this.certEndDate = certEndDate;
    }

    public String getAuxidtftp() {
        return auxidtftp;
    }

    public void setAuxidtftp(String auxidtftp) {
        this.auxidtftp = auxidtftp;
    }

    public String getAuxidtfno() {
        return auxidtfno;
    }

    public void setAuxidtfno(String auxidtfno) {
        this.auxidtfno = auxidtfno;
    }

    public String getAuxinefdt() {
        return auxinefdt;
    }

    public void setAuxinefdt(String auxinefdt) {
        this.auxinefdt = auxinefdt;
    }

    public String getAreaad() {
        return areaad;
    }

    public void setAreaad(String areaad) {
        this.areaad = areaad;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getCusRankCls() {
        return cusRankCls;
    }

    public void setCusRankCls(String cusRankCls) {
        this.cusRankCls = cusRankCls;
    }

    public String getCsspna() {
        return csspna;
    }

    public void setCsspna(String csspna) {
        this.csspna = csspna;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndivSex() {
        return indivSex;
    }

    public void setIndivSex(String sex) {
        this.indivSex = sex;
    }

    public String getIndivBrtPlace() {
        return indivBrtPlace;
    }

    public void setIndivBrtPlace(String indivBrtPlace) {
        this.indivBrtPlace = indivBrtPlace;
    }

    public String getIndivFolk() {
        return indivFolk;
    }

    public void setIndivFolk(String indivFolk) {
        this.indivFolk = indivFolk;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getIndivDtOfBirth() {
        return indivDtOfBirth;
    }

    public void setIndivDtOfBirth(String indivDtOfBirth) {
        this.indivDtOfBirth = indivDtOfBirth;
    }

    public String getIndivPolSt() {
        return indivPolSt;
    }

    public void setIndivPolSt(String indivPolSt) {
        this.indivPolSt = indivPolSt;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getIsHaveChildren() {
        return isHaveChildren;
    }

    public void setIsHaveChildren(String isHaveChildren) {
        this.isHaveChildren = isHaveChildren;
    }

    public BigDecimal getBrinnm() {
        return brinnm;
    }

    public void setBrinnm(BigDecimal brinnm) {
        this.brinnm = brinnm;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public String getIndivDgr() {
        return indivDgr;
    }

    public void setIndivDgr(String indivDgr) {
        this.indivDgr = indivDgr;
    }

    public String getGradna() {
        return gradna;
    }

    public void setGradna(String gradna) {
        this.gradna = gradna;
    }

    public String getGraddt() {
        return graddt;
    }

    public void setGraddt(String graddt) {
        this.graddt = graddt;
    }

    public String getOccu() {
        return occu;
    }

    public void setOccu(String occu) {
        this.occu = occu;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getIndivCrtfctn() {
        return indivCrtfctn;
    }

    public void setIndivCrtfctn(String indivCrtfctn) {
        this.indivCrtfctn = indivCrtfctn;
    }

    public String getOffibr() {
        return offibr;
    }

    public void setOffibr(String offibr) {
        this.offibr = offibr;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getIndivComType() {
        return indivComType;
    }

    public void setIndivComType(String indivComType) {
        this.indivComType = indivComType;
    }

    public String getUnitAddr() {
        return unitAddr;
    }

    public void setUnitAddr(String unitAddr) {
        this.unitAddr = unitAddr;
    }

    public BigDecimal getUnitnm() {
        return unitnm;
    }

    public void setUnitnm(BigDecimal unitnm) {
        this.unitnm = unitnm;
    }

    public String getWorkdt() {
        return workdt;
    }

    public void setWorkdt(String workdt) {
        this.workdt = workdt;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getUnitDate() {
        return unitDate;
    }

    public void setUnitDate(String unitDate) {
        this.unitDate = unitDate;
    }

    public String getIndivComTrade() {
        return indivComTrade;
    }

    public void setIndivComTrade(String indivComTrade) {
        this.indivComTrade = indivComTrade;
    }

    public Integer getExpryr() {
        return expryr;
    }

    public void setExpryr(Integer expryr) {
        this.expryr = expryr;
    }

    public Integer getUtwkyr() {
        return utwkyr;
    }

    public void setUtwkyr(Integer utwkyr) {
        this.utwkyr = utwkyr;
    }

    public String getCtcktg() {
        return ctcktg;
    }

    public void setCtcktg(String ctcktg) {
        this.ctcktg = ctcktg;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getIndivRsdSt() {
        return indivRsdSt;
    }

    public void setIndivRsdSt(String indivRsdSt) {
        this.indivRsdSt = indivRsdSt;
    }

    public String getLvbgdt() {
        return lvbgdt;
    }

    public void setLvbgdt(String lvbgdt) {
        this.lvbgdt = lvbgdt;
    }

    public Integer getFamisz() {
        return famisz;
    }

    public void setFamisz(Integer famisz) {
        this.famisz = famisz;
    }

    public String getMainic() {
        return mainic;
    }

    public void setMainic(String mainic) {
        this.mainic = mainic;
    }

    public String getEarningCurType() {
        return earningCurType;
    }

    public void setEarningCurType(String earningCurType) {
        this.earningCurType = earningCurType;
    }

    public BigDecimal getIndivMonn() {
        return indivMonn;
    }

    public void setIndivMonn(BigDecimal indivMonn) {
        this.indivMonn = indivMonn;
    }

    public BigDecimal getIndivYearn() {
        return indivYearn;
    }

    public void setIndivYearn(BigDecimal indivYearn) {
        this.indivYearn = indivYearn;
    }

    public BigDecimal getFamilyMonn() {
        return familyMonn;
    }

    public void setFamilyMonn(BigDecimal familyMonn) {
        this.familyMonn = familyMonn;
    }

    public BigDecimal getFamilyYearn() {
        return familyYearn;
    }

    public void setFamilyYearn(BigDecimal familyYearn) {
        this.familyYearn = familyYearn;
    }

    public String getIsBankEmployee() {
        return isBankEmployee;
    }

    public void setIsBankEmployee(String isBankEmployee) {
        this.isBankEmployee = isBankEmployee;
    }

    public String getBankEmployeeId() {
        return bankEmployeeId;
    }

    public void setBankEmployeeId(String bankEmployeeId) {
        this.bankEmployeeId = bankEmployeeId;
    }

    public String getIsAgri() {
        return isAgri;
    }

    public void setIsAgri(String isAgri) {
        this.isAgri = isAgri;
    }

    public String getIsBankSharehd() {
        return isBankSharehd;
    }

    public void setIsBankSharehd(String isBankSharehd) {
        this.isBankSharehd = isBankSharehd;
    }

    public String getRelgon() {
        return relgon;
    }

    public void setRelgon(String relgon) {
        this.relgon = relgon;
    }

    public String getProptg() {
        return proptg;
    }

    public void setProptg(String proptg) {
        this.proptg = proptg;
    }

    public Integer getOnacnm() {
        return onacnm;
    }

    public void setOnacnm(Integer onacnm) {
        this.onacnm = onacnm;
    }

    public Integer getTotlnm() {
        return totlnm;
    }

    public void setTotlnm(Integer totlnm) {
        this.totlnm = totlnm;
    }

    public String getAmlrat() {
        return amlrat;
    }

    public void setAmlrat(String amlrat) {
        this.amlrat = amlrat;
    }

    public String getResiYears() {
        return resiYears;
    }

    public void setResiYears(String resiYears) {
        this.resiYears = resiYears;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public String getSocist() {
        return socist;
    }

    public void setSocist(String socist) {
        this.socist = socist;
    }

    public String getHobyds() {
        return hobyds;
    }

    public void setHobyds(String hobyds) {
        this.hobyds = hobyds;
    }

    public String getPsrttp() {
        return psrttp;
    }

    public void setPsrttp(String psrttp) {
        this.psrttp = psrttp;
    }

    public String getIsLoan() {
        return isLoan;
    }

    public void setIsLoan(String isLoan) {
        this.isLoan = isLoan;
    }

    public String getVipptg() {
        return vipptg;
    }

    public void setVipptg(String vipptg) {
        this.vipptg = vipptg;
    }

    public String getCredlv() {
        return credlv;
    }

    public void setCredlv(String credlv) {
        this.credlv = credlv;
    }

    public String getComInitLoanDate() {
        return comInitLoanDate;
    }

    public void setComInitLoanDate(String initLoanDate) {
        this.comInitLoanDate = comInitLoanDate;
    }

    public String getCnbgdt() {
        return cnbgdt;
    }

    public void setCnbgdt(String cnbgdt) {
        this.cnbgdt = cnbgdt;
    }

    public String getCneddt() {
        return cneddt;
    }

    public void setCneddt(String cneddt) {
        this.cneddt = cneddt;
    }

    public String getImagno() {
        return imagno;
    }

    public void setImagno(String imagno) {
        this.imagno = imagno;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public String getTxpstp() {
        return txpstp;
    }

    public void setTxpstp(String txpstp) {
        this.txpstp = txpstp;
    }

    public String getAddttr() {
        return addttr;
    }

    public void setAddttr(String addttr) {
        this.addttr = addttr;
    }

    public String getBrctry() {
        return brctry;
    }

    public void setBrctry(String brctry) {
        this.brctry = brctry;
    }

    public String getTxnion() {
        return txnion;
    }

    public void setTxnion(String txnion) {
        this.txnion = txnion;
    }

    public String getTaxnum() {
        return taxnum;
    }

    public void setTaxnum(String taxnum) {
        this.taxnum = taxnum;
    }

    public String getNotxrs() {
        return notxrs;
    }

    public void setNotxrs(String notxrs) {
        this.notxrs = notxrs;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCncktg() {
        return cncktg;
    }

    public void setCncktg(String cncktg) {
        this.cncktg = cncktg;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCncuty() {
        return cncuty;
    }

    public void setCncuty(String cncuty) {
        this.cncuty = cncuty;
    }

    public String getCnprov() {
        return cnprov;
    }

    public void setCnprov(String cnprov) {
        this.cnprov = cnprov;
    }

    public String getCncity() {
        return cncity;
    }

    public void setCncity(String cncity) {
        this.cncity = cncity;
    }

    public String getCnarea() {
        return cnarea;
    }

    public void setCnarea(String cnarea) {
        this.cnarea = cnarea;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getQQ() {
        return QQ;
    }

    public void setQQ(String QQ) {
        this.QQ = QQ;
    }

    public String getWhchatNo() {
        return whchatNo;
    }

    public void setWhchatNo(String whchatNo) {
        this.whchatNo = whchatNo;
    }

    public List<RelInfoList> getRelInfoList() {
        return relInfoList;
    }

    public void setRelInfoList(List<RelInfoList> relInfoList) {
        this.relInfoList = relInfoList;
    }

}