package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusPubBlacklistRs
 * @类描述: cus_pub_blacklist_rs数据实体类
 * @功能描述:
 * @创建人: monchi
 * @创建时间: 2021-01-16 17:14:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusPubBlacklistRsClientDto implements Serializable{
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 证件类型  **/
    private String certType;

    /** 证件号码 **/
    private String certCode;

    /** 不宜贷款客户程度 **/
    private String blackDeg;

    /** 不宜贷款客户类型 **/
    private String blackType;

    /** 不宜贷款客户来源 **/
    private String dataSource;

    /** 列入日期 **/
    private String blackDate;

    /** 注销日期 **/
    private String logoutDate;

    /** 列入原因 **/
    private String blackReason;

    /** 注销原因 **/
    private String logoutReason;

    /** 主办人 **/
    private String managerId;

    /** 主办机构 **/
    private String managerBrId;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 更新人 **/
    private String updId;

    /** 更新机构 **/
    private String updBrId;

    /** 更新日期 **/
    private String updDate;

    /** 不宜贷款户状态  **/
    private String status;

    /** 操作类型 **/
    private String oprType;


    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId == null ? null : cusId.trim();
    }

    /**
     * @return CusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName == null ? null : cusName.trim();
    }

    /**
     * @return CusName
     */
    public String getCusName() {
        return this.cusName;
    }

    /**
     * @param certType
     */
    public void setCertType(String certType) {
        this.certType = certType == null ? null : certType.trim();
    }

    /**
     * @return CertType
     */
    public String getCertType() {
        return this.certType;
    }

    /**
     * @param certCode
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode == null ? null : certCode.trim();
    }

    /**
     * @return CertCode
     */
    public String getCertCode() {
        return this.certCode;
    }

    /**
     * @param blackDeg
     */
    public void setBlackDeg(String blackDeg) {
        this.blackDeg = blackDeg == null ? null : blackDeg.trim();
    }

    /**
     * @return BlackDeg
     */
    public String getBlackDeg() {
        return this.blackDeg;
    }

    /**
     * @param blackType
     */
    public void setBlackType(String blackType) {
        this.blackType = blackType == null ? null : blackType.trim();
    }

    /**
     * @return BlackType
     */
    public String getBlackType() {
        return this.blackType;
    }

    /**
     * @param dataSource
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource == null ? null : dataSource.trim();
    }

    /**
     * @return DataSource
     */
    public String getDataSource() {
        return this.dataSource;
    }

    /**
     * @param blackDate
     */
    public void setBlackDate(String blackDate) {
        this.blackDate = blackDate == null ? null : blackDate.trim();
    }

    /**
     * @return BlackDate
     */
    public String getBlackDate() {
        return this.blackDate;
    }

    /**
     * @param logoutDate
     */
    public void setLogoutDate(String logoutDate) {
        this.logoutDate = logoutDate == null ? null : logoutDate.trim();
    }

    /**
     * @return LogoutDate
     */
    public String getLogoutDate() {
        return this.logoutDate;
    }

    /**
     * @param blackReason
     */
    public void setBlackReason(String blackReason) {
        this.blackReason = blackReason == null ? null : blackReason.trim();
    }

    /**
     * @return BlackReason
     */
    public String getBlackReason() {
        return this.blackReason;
    }

    /**
     * @param logoutReason
     */
    public void setLogoutReason(String logoutReason) {
        this.logoutReason = logoutReason == null ? null : logoutReason.trim();
    }

    /**
     * @return LogoutReason
     */
    public String getLogoutReason() {
        return this.logoutReason;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId == null ? null : managerId.trim();
    }

    /**
     * @return ManagerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId == null ? null : managerBrId.trim();
    }

    /**
     * @return ManagerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param status
     */
    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    /**
     * @return Status
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

}