package cn.com.yusys.yusp.dto.server.cmiscus0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户联系信息维护
 *
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0026ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")//客户编号
    private String cusId;

    @JsonProperty(value = "certAddr")//身份证地址
    private String certAddr;

    @JsonProperty(value = "certStreet")//身份证地址街道
    private String certStreet;

    @JsonProperty(value = "indivRsdAddr")//居住地地址
    private String indivRsdAddr;

    @JsonProperty(value = "streetRsd")//居住地址街道
    private String streetRsd;

    @JsonProperty(value = "indivZipCode")//居住地邮政编码
    private String indivZipCode;

    @JsonProperty(value = "indivRsdSt")//居住状况 STD_ZB_RSD_ST
    private String indivRsdSt;

    @JsonProperty(value = "mobile")//住宅电话
    private String mobile;

    @JsonProperty(value = "regionalism")//居住区域编号
    private String regionalism;

    @JsonProperty(value = "regionName")//居住区域名称
    private String regionName;

    @JsonProperty(value = "mobileA")//手机号码1
    private String mobileA;

    @JsonProperty(value = "isCheckA")//号码是否验证1
    private String isCheckA;

    @JsonProperty(value = "isOneselfA")//号码是否本人1
    private String isOneselfA;

    @JsonProperty(value = "mobileB")//手机号码2
    private String mobileB;

    @JsonProperty(value = "isCheckB")//号码是否验证2
    private String isCheckB;

    @JsonProperty(value = "isOneselfB")//号码是否本人2
    private String isOneselfB;

    @JsonProperty(value = "faxCode")//传真
    private String faxCode;

    @JsonProperty(value = "email")//Email地址
    private String email;

    @JsonProperty(value = "mobile1Sour")//手机号码1来源
    private String mobile1Sour;

    @JsonProperty(value = "mobile2Sour")//手机号码2来源
    private String mobile2Sour;

    @JsonProperty(value = "wechatNo")//微信号
    private String wechatNo;

    @JsonProperty(value = "mobileNo")//手机号码
    private String mobileNo;

    @JsonProperty(value = "oprType")//操作类型
    private String oprType;

    @JsonProperty(value = "qq")//QQ号
    private String qq;

    @JsonProperty(value = "indivHouhRegAdd")//户籍地址
    private String indivHouhRegAdd;

    @JsonProperty(value = "regionStreet")//户籍地址街道
    private String regionStreet;

    @JsonProperty(value = "sendAddr")//送达地址
    private String sendAddr;

    @JsonProperty(value = "deliveryStreet")//送达地址街道
    private String deliveryStreet;

    @JsonProperty(value = "postcode")//邮政编码
    private String postcode;

    @JsonProperty(value = "isAgri")//是否农户
    private String isAgri;

    @JsonProperty(value = "resiTime")//居住时间-年
    private String resiTime;

    /** 保存状态 **/
    @JsonProperty(value = "saveStatus")//保存状态
    private String saveStatus;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertAddr() {
        return certAddr;
    }

    public void setCertAddr(String certAddr) {
        this.certAddr = certAddr;
    }

    public String getCertStreet() {
        return certStreet;
    }

    public void setCertStreet(String certStreet) {
        this.certStreet = certStreet;
    }

    public String getIndivRsdAddr() {
        return indivRsdAddr;
    }

    public void setIndivRsdAddr(String indivRsdAddr) {
        this.indivRsdAddr = indivRsdAddr;
    }

    public String getStreetRsd() {
        return streetRsd;
    }

    public void setStreetRsd(String streetRsd) {
        this.streetRsd = streetRsd;
    }

    public String getIndivZipCode() {
        return indivZipCode;
    }

    public void setIndivZipCode(String indivZipCode) {
        this.indivZipCode = indivZipCode;
    }

    public String getIndivRsdSt() {
        return indivRsdSt;
    }

    public void setIndivRsdSt(String indivRsdSt) {
        this.indivRsdSt = indivRsdSt;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRegionalism() {
        return regionalism;
    }

    public void setRegionalism(String regionalism) {
        this.regionalism = regionalism;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getMobileA() {
        return mobileA;
    }

    public void setMobileA(String mobileA) {
        this.mobileA = mobileA;
    }

    public String getIsCheckA() {
        return isCheckA;
    }

    public void setIsCheckA(String isCheckA) {
        this.isCheckA = isCheckA;
    }

    public String getIsOneselfA() {
        return isOneselfA;
    }

    public void setIsOneselfA(String isOneselfA) {
        this.isOneselfA = isOneselfA;
    }

    public String getMobileB() {
        return mobileB;
    }

    public void setMobileB(String mobileB) {
        this.mobileB = mobileB;
    }

    public String getIsCheckB() {
        return isCheckB;
    }

    public void setIsCheckB(String isCheckB) {
        this.isCheckB = isCheckB;
    }

    public String getIsOneselfB() {
        return isOneselfB;
    }

    public void setIsOneselfB(String isOneselfB) {
        this.isOneselfB = isOneselfB;
    }

    public String getFaxCode() {
        return faxCode;
    }

    public void setFaxCode(String faxCode) {
        this.faxCode = faxCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile1Sour() {
        return mobile1Sour;
    }

    public void setMobile1Sour(String mobile1Sour) {
        this.mobile1Sour = mobile1Sour;
    }

    public String getMobile2Sour() {
        return mobile2Sour;
    }

    public void setMobile2Sour(String mobile2Sour) {
        this.mobile2Sour = mobile2Sour;
    }

    public String getWechatNo() {
        return wechatNo;
    }

    public void setWechatNo(String wechatNo) {
        this.wechatNo = wechatNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getIndivHouhRegAdd() {
        return indivHouhRegAdd;
    }

    public void setIndivHouhRegAdd(String indivHouhRegAdd) {
        this.indivHouhRegAdd = indivHouhRegAdd;
    }

    public String getRegionStreet() {
        return regionStreet;
    }

    public void setRegionStreet(String regionStreet) {
        this.regionStreet = regionStreet;
    }

    public String getSendAddr() {
        return sendAddr;
    }

    public void setSendAddr(String sendAddr) {
        this.sendAddr = sendAddr;
    }

    public String getDeliveryStreet() {
        return deliveryStreet;
    }

    public void setDeliveryStreet(String deliveryStreet) {
        this.deliveryStreet = deliveryStreet;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getIsAgri() {
        return isAgri;
    }

    public void setIsAgri(String isAgri) {
        this.isAgri = isAgri;
    }

    public String getResiTime() {
        return resiTime;
    }

    public void setResiTime(String resiTime) {
        this.resiTime = resiTime;
    }

    public String getSaveStatus() {
        return saveStatus;
    }

    public void setSaveStatus(String saveStatus) {
        this.saveStatus = saveStatus;
    }
}
