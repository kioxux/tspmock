package cn.com.yusys.yusp.dto.server.cmiscus0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：个人关联客户列表查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0004RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    /**
     * 个人关联台账列表
     */
    private List<CmisCus0004IndivCusRelListRespDto> indivCusRelList;

    public List<CmisCus0004IndivCusRelListRespDto> getIndivCusRelList() {
        return indivCusRelList;
    }

    public void setIndivCusRelList(List<CmisCus0004IndivCusRelListRespDto> indivCusRelList) {
        this.indivCusRelList = indivCusRelList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "Cmiscus0004RespDto{" +
                "errorCode='" + errorCode + '\'' +
                "errorMsg='" + errorMsg + '\'' +
                '}';
    }
}