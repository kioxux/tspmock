package cn.com.yusys.yusp.dto.server.cmiscus0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询根据客户编号查询法人名称,名称，证件类型，证件号，电话
 *
 * @author 徐超
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0008RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;
    @JsonProperty(value = "mrgName")
    private String mrgName;
    @JsonProperty(value = "certCode")
    private String certCode;
    @JsonProperty(value = "certType")
    private String certType;
    @JsonProperty(value = "phone")
    private String phone;

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getMrgName() {
        return mrgName;
    }

    public void setMrgName(String mrgName) {
        this.mrgName = mrgName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "CmisCus0008RespDto{" +
                "cusName='" + cusName + '\'' +
                ", mrgName='" + mrgName + '\'' +
                ", certCode='" + certCode + '\'' +
                ", certType='" + certType + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
