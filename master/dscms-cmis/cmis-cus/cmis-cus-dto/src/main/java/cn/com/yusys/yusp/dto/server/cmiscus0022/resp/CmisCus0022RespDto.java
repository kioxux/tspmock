package cn.com.yusys.yusp.dto.server.cmiscus0022.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 响应Dto：优农贷（经营信息）查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0022RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    @JsonProperty(value = "pkId")
    private String pkId;

    /** 业务流水号 **/
    @JsonProperty(value = "serno")
    private String serno;

    /** 一级行业编号 **/
    @JsonProperty(value = "oneLevelTradeCode")
    private String oneLevelTradeCode;

    /** 一级行业名称 **/
    @JsonProperty(value = "oneLevelTradeName")
    private String oneLevelTradeName;

    /** 细分行业编号 **/
    @JsonProperty(value = "detailsTradeCode")
    private String detailsTradeCode;

    /** 细分行业名称 **/
    @JsonProperty(value = "detailsTradeName")
    private String detailsTradeName;

    /** 经营规模 **/
    @JsonProperty(value = "operScale")
    private String operScale;

    /** 经营规模单位 **/
    @JsonProperty(value = "operScaleUnit")
    private String operScaleUnit;

    /** 年销售收入 **/
    @JsonProperty(value = "yearSaleIncome")
    private BigDecimal yearSaleIncome;

    /** 年利润 **/
    @JsonProperty(value = "yearProfit")
    private BigDecimal yearProfit;

    /** 操作类型 **/
    @JsonProperty(value = "oprType")
    private String oprType;

    /** 登记人 **/
    @JsonProperty(value = "inputId")
    private String inputId;

    /** 登记机构 **/
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    /** 登记日期 **/
    @JsonProperty(value = "inputDate")
    private String inputDate;

    /** 最后修改人 **/
    @JsonProperty(value = "updId")
    private String updId;

    /** 最近修改机构 **/
    @JsonProperty(value = "updBrId")
    private String updBrId;

    /** 最近修改日期 **/
    @JsonProperty(value = "updDate")
    private String updDate;

    /** 创建时间 **/
    @JsonProperty(value = "createTime")
    private Date createTime;

    /** 修改时间 **/
    @JsonProperty(value = "updateTime")
    private Date updateTime;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getOneLevelTradeCode() {
        return oneLevelTradeCode;
    }

    public void setOneLevelTradeCode(String oneLevelTradeCode) {
        this.oneLevelTradeCode = oneLevelTradeCode;
    }

    public String getOneLevelTradeName() {
        return oneLevelTradeName;
    }

    public void setOneLevelTradeName(String oneLevelTradeName) {
        this.oneLevelTradeName = oneLevelTradeName;
    }

    public String getDetailsTradeCode() {
        return detailsTradeCode;
    }

    public void setDetailsTradeCode(String detailsTradeCode) {
        this.detailsTradeCode = detailsTradeCode;
    }

    public String getDetailsTradeName() {
        return detailsTradeName;
    }

    public void setDetailsTradeName(String detailsTradeName) {
        this.detailsTradeName = detailsTradeName;
    }

    public String getOperScale() {
        return operScale;
    }

    public void setOperScale(String operScale) {
        this.operScale = operScale;
    }

    public String getOperScaleUnit() {
        return operScaleUnit;
    }

    public void setOperScaleUnit(String operScaleUnit) {
        this.operScaleUnit = operScaleUnit;
    }

    public BigDecimal getYearSaleIncome() {
        return yearSaleIncome;
    }

    public void setYearSaleIncome(BigDecimal yearSaleIncome) {
        this.yearSaleIncome = yearSaleIncome;
    }

    public BigDecimal getYearProfit() {
        return yearProfit;
    }

    public void setYearProfit(BigDecimal yearProfit) {
        this.yearProfit = yearProfit;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CmisCus0022RespDto{" +
                "pkId='" + pkId + '\'' +
                ", serno='" + serno + '\'' +
                ", oneLevelTradeCode='" + oneLevelTradeCode + '\'' +
                ", oneLevelTradeName='" + oneLevelTradeName + '\'' +
                ", detailsTradeCode='" + detailsTradeCode + '\'' +
                ", detailsTradeName='" + detailsTradeName + '\'' +
                ", operScale='" + operScale + '\'' +
                ", operScaleUnit='" + operScaleUnit + '\'' +
                ", yearSaleIncome=" + yearSaleIncome +
                ", yearProfit=" + yearProfit +
                ", oprType='" + oprType + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }

}
