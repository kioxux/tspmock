package cn.com.yusys.yusp.dto.server.cmiscus0026.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：客户联系信息维护
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0026RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "result")//返回结果
    private Integer result;

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CmisCus0026RespDto{" +
                "result='" + result + '\'' +
                '}';
    }

}
