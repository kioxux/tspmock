package cn.com.yusys.yusp.dto.server.cmiscus0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 请求Dto：优农贷（经营信息）查询
 *
 * @author dumd
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0022ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 业务流水号 **/
    @JsonProperty(value = "serno")
    private String serno;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }



    @Override
    public String toString() {
        return "CmisCus0022ReqDto{" +
                "serno='" + serno + '\'' +
                '}';
    }
}
