package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusComGrade
 * @类描述: cus_com_grade数据实体类
 * @功能描述: 
 * @创建人: dumd
 * @创建时间: 2021-10-14 20:41:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusComGradeDto2 implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 客户编号 **/
	private String relCusId;

	/** 关联类型 **/
	private String relCusType;
	
	/** 客户名称 **/
	private String relCusName;

	/** 上期客户评级 */
	private String lastCusCrdGrade;

	/** 本期客户评级 **/
	private String curCusCrdGrade;

	public String getRelCusId() {
		return relCusId;
	}

	public void setRelCusId(String relCusId) {
		this.relCusId = relCusId;
	}

	public String getRelCusType() {
		return relCusType;
	}

	public void setRelCusType(String relCusType) {
		this.relCusType = relCusType;
	}

	public String getRelCusName() {
		return relCusName;
	}

	public void setRelCusName(String relCusName) {
		this.relCusName = relCusName;
	}

	public String getLastCusCrdGrade() {
		return lastCusCrdGrade;
	}

	public void setLastCusCrdGrade(String lastCusCrdGrade) {
		this.lastCusCrdGrade = lastCusCrdGrade;
	}

	public String getCurCusCrdGrade() {
		return curCusCrdGrade;
	}

	public void setCurCusCrdGrade(String curCusCrdGrade) {
		this.curCusCrdGrade = curCusCrdGrade;
	}
}