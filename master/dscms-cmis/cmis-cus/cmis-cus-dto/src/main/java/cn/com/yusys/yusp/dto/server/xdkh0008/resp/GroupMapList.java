package cn.com.yusys.yusp.dto.server.xdkh0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：集团下所有成员信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class GroupMapList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//成员客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//成员客户名称
    @JsonProperty(value = "certType")
    private String certType;//成员证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//成员证件号码
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理编号
    @JsonProperty(value = "managerOrgId")
    private String managerOrgId;//主管客户经理所属机构编号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerOrgId() {
        return managerOrgId;
    }

    public void setManagerOrgId(String managerOrgId) {
        this.managerOrgId = managerOrgId;
    }

    @Override
    public String toString() {
        return "GroupMapList{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerOrgId='" + managerOrgId + '\'' +
                '}';
    }
}
