package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndiv
 * @类描述: cus_indiv数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-15 10:33:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusIndivReqClientDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	private String cusId;

	/** 建立信贷关系时间 **/
	private String initLoanDate;
	
	/** 在我行建立业务情况 STD_ZB_INV_HL_ACN **/
	private String indivHldAcnt;

	public String getCusId() {
		return cusId;
	}

	public String getInitLoanDate() {
		return initLoanDate;
	}

	public String getIndivHldAcnt() {
		return indivHldAcnt;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public void setInitLoanDate(String initLoanDate) {
		this.initLoanDate = initLoanDate;
	}

	public void setIndivHldAcnt(String indivHldAcnt) {
		this.indivHldAcnt = indivHldAcnt;
	}

	@Override
	public String toString() {
		return "CusIndivReqClientDto{" +
				"cusId='" + cusId + '\'' +
				", comInitLoanDate='" + initLoanDate + '\'' +
				", indivHldAcnt='" + indivHldAcnt + '\'' +
				'}';
	}
}