package cn.com.yusys.yusp.dto.server.cmiscus0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 响应Dto：查询个人客户基本信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CusIndivDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 客户编号
     **/

    @JsonProperty(value = "cusId")
    private String cusId;
    /**
     * 客户名称
     **/
    @JsonProperty(value = "cusName")
    private String cusName;

    /**
     * 别称
     **/
    @JsonProperty(value = "name")
    private String name;
    /**
     * 证件类型
     **/
    @JsonProperty(value = "certType")
    private String certType;//新增

    /**
     * 证件号码
     **/
    @JsonProperty(value = "certCode")
    private String certCode;//新增

    /**
     * 国籍
     **/
    @JsonProperty(value = "nation")
    private String nation;

    /**
     * 性别 STD_ZB_SEX
     **/
    @JsonProperty(value = "sex")
    private String sex;

    /**
     * 是否为长期证件 STD_ZB_YES_NO
     **/
    @JsonProperty(value = "isLongIndiv")
    private String isLongIndiv;

    /**
     * 证件签发日期
     **/
    @JsonProperty(value = "certStartDt")
    private String certStartDt;

    /**
     * 证件到期日期
     **/
    @JsonProperty(value = "certEndDt")
    private String certEndDt;

    /**
     * 是否为农户 STD_ZB_YES_NO
     **/
    @JsonProperty(value = "agriFlg")
    private String agriFlg;

    /**
     * 民族 STD_ZB_INDIV_FOLK
     **/
    @JsonProperty(value = "indivFolk")
    private String indivFolk;

    /**
     * 籍贯
     **/
    @JsonProperty(value = "indivBrtPlace")
    private String indivBrtPlace;

    /**
     * 户籍地址
     **/
    @JsonProperty(value = "indivHouhRegAdd")
    private String indivHouhRegAdd;

    /**
     * 街道
     **/
    @JsonProperty(value = "visitStreet")
    private String visitStreet;

    /**
     * 出生日期
     **/
    @JsonProperty(value = "indivDtOfBirth")
    private String indivDtOfBirth;

    /**
     * 政治面貌 STD_ZB_POLITICAL
     **/
    @JsonProperty(value = "indivPolSt")
    private String indivPolSt;

    /**
     * 最高学历 STD_ZB_EDU
     **/
    @JsonProperty(value = "indivEdt")
    private String indivEdt;

    /**
     * 最高学位 STD_ZB_DEGREE
     **/
    @JsonProperty(value = "indivDgr")
    private String indivDgr;

    /**
     * 是否有子女 STD_ZB_YES_NO
     **/
    @JsonProperty(value = "isHaveChildren")
    private String isHaveChildren;

    /**
     * 婚姻状况 STD_ZB_MAR_ST
     **/
    @JsonProperty(value = "marStatus")
    private String marStatus;

    /**
     * 健康状况
     **/
    @JsonProperty(value = "healthStatus")
    private String healthStatus;

    /**
     * 建立信贷关系时间
     **/
    @JsonProperty(value = "initLoanDate")
    private String initLoanDate;

    /**
     * 在我行建立业务情况 STD_ZB_INV_HL_ACN
     **/
    @JsonProperty(value = "indivHldAcnt")
    private String indivHldAcnt;

    /**
     * 评级得分
     **/
    @JsonProperty(value = "cusCrdGrade")
    private String cusCrdGrade;//新增

    /**
     * 最近评级日期
     **/
    @JsonProperty(value = "cusCrdDt")
    private String cusCrdDt;//新增

    /**
     * 是否我行员工 STD_ZB_YES_NO
     **/
    @JsonProperty(value = "isBankEmployee")
    private String isBankEmployee;

    /**
     * 在我行职务 STD_ZB_BANK_DUTY
     **/
    @JsonProperty(value = "bankDuty")
    private String bankDuty;

    /**
     * 是否我行股东 STD_ZB_YES_NO
     **/
    @JsonProperty(value = "isBankSharehd")
    private String isBankSharehd;

    /**
     * 用工形式
     **/
    @JsonProperty(value = "utilEmployeeMode")
    private String utilEmployeeMode;

    /**
     * 人员类别
     **/
    @JsonProperty(value = "employeeCls")
    private String employeeCls;

    /**
     * 是否有中征码 STD_ZB_YES_NO
     **/
    @JsonProperty(value = "loanCardFlg")
    private String loanCardFlg;

    /**
     * 中征码
     **/
    @JsonProperty(value = "loanCardId")
    private String loanCardId;

    /**
     * 是否重点客户 STD_ZB_YES_NO
     **/
    @JsonProperty(value = "isMainCus")
    private String isMainCus;

    /**
     * 客户类型
     **/
    @JsonProperty(value = "cusType")
    private String cusType;

    /**
     * 影像编号
     **/
    @JsonProperty(value = "imageNo")
    private String imageNo;

    /**
     * 登记人
     **/
    @JsonProperty(value = "inputId")
    private String inputId;

    /**
     * 登记机构
     **/
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    /**
     * 登记日期
     **/
    @JsonProperty(value = "inputDate")
    private String inputDate;

    /**
     * 更新人
     **/
    @JsonProperty(value = "updId")
    private String updId;

    /**
     * 更新机构
     **/
    @JsonProperty(value = "updBrId")
    private String updBrId;

    /**
     * 更新日期
     **/
    @JsonProperty(value = "updDate")
    private String updDate;

    /**
     * 创建时间
     **/
    @JsonProperty(value = "createTime")
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @JsonProperty(value = "updateTime")
    private java.util.Date updateTime;

    /**
     * 是否五日还款
     **/
    @JsonProperty(value = "cusGetfive")
    private String cusGetfive;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIsLongIndiv() {
        return isLongIndiv;
    }

    public void setIsLongIndiv(String isLongIndiv) {
        this.isLongIndiv = isLongIndiv;
    }

    public String getCertStartDt() {
        return certStartDt;
    }

    public void setCertStartDt(String certStartDt) {
        this.certStartDt = certStartDt;
    }

    public String getCertEndDt() {
        return certEndDt;
    }

    public void setCertEndDt(String certEndDt) {
        this.certEndDt = certEndDt;
    }

    public String getAgriFlg() {
        return agriFlg;
    }

    public void setAgriFlg(String agriFlg) {
        this.agriFlg = agriFlg;
    }

    public String getIndivFolk() {
        return indivFolk;
    }

    public void setIndivFolk(String indivFolk) {
        this.indivFolk = indivFolk;
    }

    public String getIndivBrtPlace() {
        return indivBrtPlace;
    }

    public void setIndivBrtPlace(String indivBrtPlace) {
        this.indivBrtPlace = indivBrtPlace;
    }

    public String getIndivHouhRegAdd() {
        return indivHouhRegAdd;
    }

    public void setIndivHouhRegAdd(String indivHouhRegAdd) {
        this.indivHouhRegAdd = indivHouhRegAdd;
    }

    public String getVisitStreet() {
        return visitStreet;
    }

    public void setVisitStreet(String visitStreet) {
        this.visitStreet = visitStreet;
    }

    public String getIndivDtOfBirth() {
        return indivDtOfBirth;
    }

    public void setIndivDtOfBirth(String indivDtOfBirth) {
        this.indivDtOfBirth = indivDtOfBirth;
    }

    public String getIndivPolSt() {
        return indivPolSt;
    }

    public void setIndivPolSt(String indivPolSt) {
        this.indivPolSt = indivPolSt;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public String getIndivDgr() {
        return indivDgr;
    }

    public void setIndivDgr(String indivDgr) {
        this.indivDgr = indivDgr;
    }

    public String getIsHaveChildren() {
        return isHaveChildren;
    }

    public void setIsHaveChildren(String isHaveChildren) {
        this.isHaveChildren = isHaveChildren;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public String getInitLoanDate() {
        return initLoanDate;
    }

    public void setInitLoanDate(String initLoanDate) {
        this.initLoanDate = initLoanDate;
    }

    public String getIndivHldAcnt() {
        return indivHldAcnt;
    }

    public void setIndivHldAcnt(String indivHldAcnt) {
        this.indivHldAcnt = indivHldAcnt;
    }

    public String getCusCrdGrade() {
        return cusCrdGrade;
    }

    public void setCusCrdGrade(String cusCrdGrade) {
        this.cusCrdGrade = cusCrdGrade;
    }

    public String getCusCrdDt() {
        return cusCrdDt;
    }

    public void setCusCrdDt(String cusCrdDt) {
        this.cusCrdDt = cusCrdDt;
    }

    public String getIsBankEmployee() {
        return isBankEmployee;
    }

    public void setIsBankEmployee(String isBankEmployee) {
        this.isBankEmployee = isBankEmployee;
    }

    public String getBankDuty() {
        return bankDuty;
    }

    public void setBankDuty(String bankDuty) {
        this.bankDuty = bankDuty;
    }

    public String getIsBankSharehd() {
        return isBankSharehd;
    }

    public void setIsBankSharehd(String isBankSharehd) {
        this.isBankSharehd = isBankSharehd;
    }

    public String getUtilEmployeeMode() {
        return utilEmployeeMode;
    }

    public void setUtilEmployeeMode(String utilEmployeeMode) {
        this.utilEmployeeMode = utilEmployeeMode;
    }

    public String getEmployeeCls() {
        return employeeCls;
    }

    public void setEmployeeCls(String employeeCls) {
        this.employeeCls = employeeCls;
    }

    public String getLoanCardFlg() {
        return loanCardFlg;
    }

    public void setLoanCardFlg(String loanCardFlg) {
        this.loanCardFlg = loanCardFlg;
    }

    public String getLoanCardId() {
        return loanCardId;
    }

    public void setLoanCardId(String loanCardId) {
        this.loanCardId = loanCardId;
    }

    public String getIsMainCus() {
        return isMainCus;
    }

    public void setIsMainCus(String isMainCus) {
        this.isMainCus = isMainCus;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCusGetfive() {
        return cusGetfive;
    }

    public void setCusGetfive(String cusGetfive) {
        this.cusGetfive = cusGetfive;
    }

    @Override
    public String toString() {
        return "CusIndivDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", name='" + name + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", nation='" + nation + '\'' +
                ", sex='" + sex + '\'' +
                ", isLongIndiv='" + isLongIndiv + '\'' +
                ", certStartDt='" + certStartDt + '\'' +
                ", certEndDt='" + certEndDt + '\'' +
                ", agriFlg='" + agriFlg + '\'' +
                ", indivFolk='" + indivFolk + '\'' +
                ", indivBrtPlace='" + indivBrtPlace + '\'' +
                ", indivHouhRegAdd='" + indivHouhRegAdd + '\'' +
                ", visitStreet='" + visitStreet + '\'' +
                ", indivDtOfBirth='" + indivDtOfBirth + '\'' +
                ", indivPolSt='" + indivPolSt + '\'' +
                ", indivEdt='" + indivEdt + '\'' +
                ", indivDgr='" + indivDgr + '\'' +
                ", isHaveChildren='" + isHaveChildren + '\'' +
                ", marStatus='" + marStatus + '\'' +
                ", healthStatus='" + healthStatus + '\'' +
                ", initLoanDate='" + initLoanDate + '\'' +
                ", indivHldAcnt='" + indivHldAcnt + '\'' +
                ", cusCrdGrade=" + cusCrdGrade +
                ", cusCrdDt='" + cusCrdDt + '\'' +
                ", isBankEmployee='" + isBankEmployee + '\'' +
                ", bankDuty='" + bankDuty + '\'' +
                ", isBankSharehd='" + isBankSharehd + '\'' +
                ", utilEmployeeMode='" + utilEmployeeMode + '\'' +
                ", employeeCls='" + employeeCls + '\'' +
                ", loanCardFlg='" + loanCardFlg + '\'' +
                ", loanCardId='" + loanCardId + '\'' +
                ", isMainCus='" + isMainCus + '\'' +
                ", cusType='" + cusType + '\'' +
                ", imageNo='" + imageNo + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", cusGetfive=" + cusGetfive +
                '}';
    }
}

