package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @version 1.0.0
 * @项目名称: cmis-cus-core模块
 * @类名称: CusCorp
 * @类描述: cus_corp数据实体类
 * @功能描述:
 * @创建人: AbsonZ
 * @创建时间: 2021-04-13 10:06:34
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusCorpDto implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 客户编号
     **/
    private String cusId;

    /**
     * 客户编号
     **/
    private String bizType;

    /**
     * 客户名称
     **/
    private String cusName;

    /**
     * 外文名称
     **/
    private String cusNameEn;

    /**
     * 国别
     **/
    private String country;

    /**
     * 企业性质
     **/
    private String corpQlty;

    /**
     * 城乡类型 STD_ZB_CITY_TYP
     **/
    private String cityType;

    /**
     * 资产总额（万元）
     **/
    private java.math.BigDecimal assTotal;

    /**
     * 行政隶属关系 STD_ZB_SUB_TYP
     **/
    private String adminSubRel;

    /**
     * 隶属关系 STD_ZB_SUB_TYP
     **/
    private String subTyp;

    /**
     * 投资主体 STD_ZB_INVEST_TYP
     **/
    private String investMbody;

    /**
     * 控股类型 STD_ZB_HOLD_TYPE
     **/
    private String holdType;

    /**
     * 行业分类
     **/
    private String tradeClass;

    /**
     * 评级模型ID
     **/
    private String cusCcrModelId;

    /**
     * 评级模型名称
     **/
    private String cusCcrModelName;

    /**
     * 行业分类2
     **/
    private String cllType2;

    /**
     * 成立日期
     **/
    private String buildDate;

    /**
     * 从业人数
     **/
    private Integer fjobNum;

    /**
     * 销售额（万元）
     **/
    private java.math.BigDecimal salVolume;

    /**
     * 营业收入（万元）
     **/
    private java.math.BigDecimal operIncome;

    /**
     * 企业规模 STD_ZB_CUS_SCALE
     **/
    private String corpScale;

    /**
     * 企业规模（报表） STD_ZB_CUS_SCALE
     **/
    private String cusScaleForm;

    /**
     * 国民经济部门编号
     **/
    private String natEcoSec;

    /**
     * 产业扶贫单位标志 STD_ZB_YES_NO
     **/
    private String unitAlleInd;

    /**
     * 扶贫带动人数
     **/
    private Integer alleBunb;

    /**
     * 农场标志
     **/
    private String framInd;

    /**
     * 农民专业合作社标志 STD_ZB_YES_NO
     **/
    private String farmerCopInd;

    /**
     * 是否涉农 STD_ZB_YES_NO
     **/
    private String agrInd;

    /**
     * 普惠金融统计口径 STD_ZB_YES_NO
     **/
    private String inclusiveFinanceStatistics;

    /**
     * 组织机构代码
     **/
    private String insCode;

    /**
     * 组织机构登记日期
     **/
    private String insRegDate;

    /**
     * 组织机构有效日期
     **/
    private String insEndDate;

    /**
     * 组织机构代码证颁发机关
     **/
    private String insOrg;

    /**
     * 组织机构代码证年检到期日
     **/
    private String insAnnDate;

    /**
     * 注册登记号类型 STD_ZB_LICENSE_TYPE
     **/
    private String licenseType;

    /**
     * 登记注册号
     **/
    private String regiCode;

    /**
     * 注册登记类型 STD_ZB_REG_TYPE
     **/
    private String regiType;

    /**
     * 资质等级 STD_ZB_QUAL_LEVEL
     **/
    private String quliGarade;

    /**
     * 主管单位
     **/
    private String adminOrg;

    /**
     * 审批机关
     **/
    private String apprOrg;

    /**
     * 批准文号
     **/
    private String apprDocNo;

    /**
     * 注册地行政区划
     **/
    private String regiAreaCode;

    /**
     * 注册登记地址
     **/
    private String regiAddr;

    /**
     * 外文注册登记地址
     **/
    private String regiAddrEn;

    /**
     * 实际经营地行政区划
     **/
    private String acuStateCode;

    /**
     * 实际经营地址
     **/
    private String operAddrAct;

    /**
     * 主营业务范围
     **/
    private String mainOptScp;

    /**
     * 兼营业务范围
     **/
    private String partOptScp;

    /**
     * 注册资本/开办资金币种
     **/
    private String regiCurType;

    /**
     * 注册资本金额
     **/
    private java.math.BigDecimal regiCapAmt;

    /**
     * 实收资本币种
     **/
    private String paidCapCurType;

    /**
     * 实收资本金额
     **/
    private java.math.BigDecimal paidCapAmt;

    /**
     * 许可经营项目
     **/
    private String comMainOptScp;

    /**
     * 注册登记日期
     **/
    private String regiStartDate;

    /**
     * 注册登记到期日期
     **/
    private String regiEndDate;

    /**
     * 注册登记年审结论
     **/
    private String regAudit;

    /**
     * 注册登记年审日期
     **/
    private String regAuditDate;

    /**
     * 注册登记年检到期日
     **/
    private String regAuditEndDate;

    /**
     * 国税税务登记代码
     **/
    private String natTaxRegCode;

    /**
     * 国税税务登记机关
     **/
    private String natTaxRegOrg;

    /**
     * 国税税务登记日期
     **/
    private String natTaxRegDt;

    /**
     * 国税登记有效期
     **/
    private String natTaxRegEndDt;

    /**
     * 国税登记证年检到期日
     **/
    private String natTaxAnnDate;

    /**
     * 地税税务登记代码
     **/
    private String locTaxRegCode;

    /**
     * 地税税务登记机构
     **/
    private String locTaxRegOrg;

    /**
     * 地税税务登记日期
     **/
    private String locTaxRegDt;

    /**
     * 地税登记有效期
     **/
    private String locTaxRegEndDt;

    /**
     * 地税登记证年检到期日
     **/
    private String locTaxAnnDate;

    /**
     * 有无贷款卡
     **/
    private String loanCardFlg;

    /**
     * 企业中征码
     **/
    private String loanCardId;

    /**
     * 贷款卡密码
     **/
    private String loanCardPwd;

    /**
     * 贷款卡状态
     **/
    private String loanCardEffFlg;

    /**
     * 贷款卡年检日期
     **/
    private String loanCardAnnDate;

    /**
     * 证件类型
     **/
    private String certType;

    /**
     * 证件号码
     **/
    private String certCode;

    /**
     * 证件有效期
     **/
    private String certIdate;

    /**
     * 企业类型  STD_ZB_CORP_TYPE
     **/
    private String conType;

    /**
     * 企业所有制  STD_ZB_CORP_OWNERS
     **/
    private String corpOwnersType;

    /**
     * 是否本行股东 STD_ZB_YES_NO
     **/
    private String isBankShd;

    /**
     * 是否小企业客户 STD_ZB_YES_NO
     **/
    private String isSmconCus;

    /**
     * 详细地址
     **/
    private String detailAddr;

    /**
     * 年生产设备
     **/
    private String produceEquipYear;

    /**
     * QQ
     **/
    private String qq;

    /**
     * 年生产能力
     **/
    private String produceAbiYear;

    /**
     * 是否国控
     **/
    private String isNatctl;

    /**
     * 常用联系人
     **/
    private String freqLinkman;

    /**
     * 国控层级
     **/
    private String natctlLevel;

    /**
     * 许可经营项目
     **/
    private String licOperPro;

    /**
     * 常用联系人手机
     **/
    private String freqLinkmanTel;

    /**
     * 一般经营项目
     **/
    private String commonOperPro;

    /**
     * 经营状况
     **/
    private String operStatus;

    /**
     * 是否长期有效
     **/
    private String isLongVld;

    /**
     * 基本存款账户开户许可证（核准号）
     **/
    private String basicDepAccNoOpenLic;

    /**
     * 基本存款账户账号
     **/
    private String basicDepAccNo;

    /**
     * 基本存款账户是否在本机构
     **/
    private String isBankBasicDepAccNo;

    /**
     * 基本存款账户开户行
     **/
    private String basicDepAccob;

    /**
     * 基本账户开户日期
     **/
    private String basicAccNoOpenDate;

    /**
     * 一般账户开户日期
     **/
    private String commonAccNoOpenDate;

    /**
     * 主要产品情况
     **/
    private String mainPrdDesc;

    /**
     * 信用等级（外部）
     **/
    private String creditLevelOuter;

    /**
     * 评定日期（外部）
     **/
    private String evalDate;

    /**
     * 传真
     **/
    private String fax;

    /**
     * 评定机构（外部）
     **/
    private String evalOrgId;

    /**
     * 电子邮箱
     **/
    private String linkmanEmail;

    /**
     * 送达地址
     **/
    private String sendAddr;

    /**
     * 建立信贷关系时间
     **/
    private String initLoanDate;

    /**
     * 微信
     **/
    private String wechatNo;

    /**
     * 是否战略客户
     **/
    private String isStrgcCus;

    /**
     * 财务报表类型
     **/
    private String finaReportType;

    /**
     * 地区重点企业
     **/
    private String areaPriorCorp;

    /**
     * 特种经营标识
     **/
    private String spOperFlag;

    /**
     * 是否新建企业
     **/
    private String isNewBuildCorp;

    /**
     * 集团客户类型
     **/
    private String grpCusType;

    /**
     * 注册登记机关
     **/
    private String regiOrg;

    /**
     * 主营业务所在国家
     **/
    private String mainBusNation;

    /**
     * 信用证垫款账号
     **/
    private String creditPadAccNo;

    /**
     * 注册地址
     **/
    private String registerAddress;

    /**
     * 贷款类型
     **/
    private String loanType;

    /**
     * 经营场地所有权
     **/
    private String operPlaceOwnshp;

    /**
     * 经营场地面积
     **/
    private java.math.BigDecimal operPlaceSqu;

    /**
     * 主要生产设备
     **/
    private String mainProduceEquip;

    /**
     * 登记人
     **/
    private String inputId;

    /**
     * 登记机构
     **/
    private String inputBrId;

    /**
     * 登记人
     **/
    private String managerId;

    /**
     * 登记机构
     **/
    private String managerBrId;

    /**
     * 登记时间
     **/
    private String inputDate;

    /**
     * 更新人
     **/
    private String updId;

    /**
     * 更新机构
     **/
    private String updBrId;

    /**
     * 更新时间
     **/
    private String updDate;

    /**
     * 操作类型
     **/
    private String oprType;

    /**
     * 创建时间
     **/
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    private java.util.Date updateTime;

    /**
     * 是否苏州综合平台企业
     **/
    private String iisSzjrfwCrop;

    /**
     * 是否钢贸企业
     **/
    private String isSteelCus;

    /**
     * 是否上市公司
     **/
    private String isStockCorp;

    /**
     * 是否城投
     **/
    private String isCtinve;

    /**
     * 城投层级
     **/
    private String ctinveLevel;

    /**
     * 政府投资平台
     **/
    private String goverInvestPlat;

    /**
     * 进出口标识
     **/
    private String impexpFlag;

    /**
     * 再贴现业务中申请单位行业分类
     **/
    private String redcbizUnitTradeClass;

    /**
     * 客户类型
     **/
    private String cusType;

    /**
     * 客户简称
     **/
    private String cusShortName;

    /**
     * 任务状态
     **/
    private String taskStatus;

    /**
     * 任务流水号
     **/
    private String taskSerno;

    /**
     * 客户分类 01-正式客户 02-临时客户
     */
    private String cusRankCls;
    /**
     * 客户状态 STD_CUS_STATE 2-生效 1-暂存
     **/
    private String cusState;

    /**
     * 本行即期用信等级
     **/
    private String bankLoanLevel;

    public String getBankLoanLevel() {
        return bankLoanLevel;
    }

    public void setBankLoanLevel(String bankLoanLevel) {
        this.bankLoanLevel = bankLoanLevel;
    }

    public String getCusRankCls() {
        return cusRankCls;
    }

    public void setCusRankCls(String cusRankCls) {
        this.cusRankCls = cusRankCls;
    }

    public String getCusState() {
        return cusState;
    }

    public void setCusState(String cusState) {
        this.cusState = cusState;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskSerno() {
        return taskSerno;
    }

    public void setTaskSerno(String taskSerno) {
        this.taskSerno = taskSerno;
    }

    public String getCusShortName() {
        return cusShortName;
    }

    public void setCusShortName(String cusShortName) {
        this.cusShortName = cusShortName;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId == null ? null : cusId.trim();
    }

    /**
     * @return CusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param cusNameEn
     */
    public void setCusNameEn(String cusNameEn) {
        this.cusNameEn = cusNameEn == null ? null : cusNameEn.trim();
    }

    /**
     * @return CusNameEn
     */
    public String getCusNameEn() {
        return this.cusNameEn;
    }

    /**
     * @param country
     */
    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    /**
     * @return Country
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * @param corpQlty
     */
    public void setCorpQlty(String corpQlty) {
        this.corpQlty = corpQlty == null ? null : corpQlty.trim();
    }

    /**
     * @return CorpQlty
     */
    public String getCorpQlty() {
        return this.corpQlty;
    }

    /**
     * @param cityType
     */
    public void setCityType(String cityType) {
        this.cityType = cityType == null ? null : cityType.trim();
    }

    /**
     * @return CityType
     */
    public String getCityType() {
        return this.cityType;
    }

    /**
     * @param assTotal
     */
    public void setAssTotal(java.math.BigDecimal assTotal) {
        this.assTotal = assTotal;
    }

    /**
     * @return AssTotal
     */
    public java.math.BigDecimal getAssTotal() {
        return this.assTotal;
    }

    /**
     * @param adminSubRel
     */
    public void setAdminSubRel(String adminSubRel) {
        this.adminSubRel = adminSubRel == null ? null : adminSubRel.trim();
    }

    /**
     * @return AdminSubRel
     */
    public String getAdminSubRel() {
        return this.adminSubRel;
    }

    /**
     * @param subTyp
     */
    public void setSubTyp(String subTyp) {
        this.subTyp = subTyp == null ? null : subTyp.trim();
    }

    /**
     * @return SubTyp
     */
    public String getSubTyp() {
        return this.subTyp;
    }

    /**
     * @param investMbody
     */
    public void setInvestMbody(String investMbody) {
        this.investMbody = investMbody == null ? null : investMbody.trim();
    }

    /**
     * @return InvestMbody
     */
    public String getInvestMbody() {
        return this.investMbody;
    }

    /**
     * @param holdType
     */
    public void setHoldType(String holdType) {
        this.holdType = holdType == null ? null : holdType.trim();
    }

    /**
     * @return HoldType
     */
    public String getHoldType() {
        return this.holdType;
    }

    /**
     * @param tradeClass
     */
    public void setTradeClass(String tradeClass) {
        this.tradeClass = tradeClass == null ? null : tradeClass.trim();
    }

    /**
     * @return TradeClass
     */
    public String getTradeClass() {
        return this.tradeClass;
    }

    /**
     * @param cusCcrModelId
     */
    public void setCusCcrModelId(String cusCcrModelId) {
        this.cusCcrModelId = cusCcrModelId == null ? null : cusCcrModelId.trim();
    }

    /**
     * @return CusCcrModelId
     */
    public String getCusCcrModelId() {
        return this.cusCcrModelId;
    }

    /**
     * @param cusCcrModelName
     */
    public void setCusCcrModelName(String cusCcrModelName) {
        this.cusCcrModelName = cusCcrModelName == null ? null : cusCcrModelName.trim();
    }

    /**
     * @return CusCcrModelName
     */
    public String getCusCcrModelName() {
        return this.cusCcrModelName;
    }

    /**
     * @param cllType2
     */
    public void setCllType2(String cllType2) {
        this.cllType2 = cllType2 == null ? null : cllType2.trim();
    }

    /**
     * @return CllType2
     */
    public String getCllType2() {
        return this.cllType2;
    }

    /**
     * @param buildDate
     */
    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate == null ? null : buildDate.trim();
    }

    /**
     * @return BuildDate
     */
    public String getBuildDate() {
        return this.buildDate;
    }

    /**
     * @param fjobNum
     */
    public void setFjobNum(Integer fjobNum) {
        this.fjobNum = fjobNum;
    }

    /**
     * @return FjobNum
     */
    public Integer getFjobNum() {
        return this.fjobNum;
    }

    /**
     * @param salVolume
     */
    public void setSalVolume(java.math.BigDecimal salVolume) {
        this.salVolume = salVolume;
    }

    /**
     * @return SalVolume
     */
    public java.math.BigDecimal getSalVolume() {
        return this.salVolume;
    }

    /**
     * @param operIncome
     */
    public void setOperIncome(java.math.BigDecimal operIncome) {
        this.operIncome = operIncome;
    }

    /**
     * @return OperIncome
     */
    public java.math.BigDecimal getOperIncome() {
        return this.operIncome;
    }

    /**
     * @param corpScale
     */
    public void setCorpScale(String corpScale) {
        this.corpScale = corpScale == null ? null : corpScale.trim();
    }

    /**
     * @return CorpScale
     */
    public String getCorpScale() {
        return this.corpScale;
    }

    /**
     * @param cusScaleForm
     */
    public void setCusScaleForm(String cusScaleForm) {
        this.cusScaleForm = cusScaleForm == null ? null : cusScaleForm.trim();
    }

    /**
     * @return CusScaleForm
     */
    public String getCusScaleForm() {
        return this.cusScaleForm;
    }

    /**
     * @param natEcoSec
     */
    public void setNatEcoSec(String natEcoSec) {
        this.natEcoSec = natEcoSec == null ? null : natEcoSec.trim();
    }

    /**
     * @return NatEcoSec
     */
    public String getNatEcoSec() {
        return this.natEcoSec;
    }

    /**
     * @param unitAlleInd
     */
    public void setUnitAlleInd(String unitAlleInd) {
        this.unitAlleInd = unitAlleInd == null ? null : unitAlleInd.trim();
    }

    /**
     * @return UnitAlleInd
     */
    public String getUnitAlleInd() {
        return this.unitAlleInd;
    }

    /**
     * @param alleBunb
     */
    public void setAlleBunb(Integer alleBunb) {
        this.alleBunb = alleBunb;
    }

    /**
     * @return AlleBunb
     */
    public Integer getAlleBunb() {
        return this.alleBunb;
    }

    /**
     * @param framInd
     */
    public void setFramInd(String framInd) {
        this.framInd = framInd == null ? null : framInd.trim();
    }

    /**
     * @return FramInd
     */
    public String getFramInd() {
        return this.framInd;
    }

    /**
     * @param farmerCopInd
     */
    public void setFarmerCopInd(String farmerCopInd) {
        this.farmerCopInd = farmerCopInd == null ? null : farmerCopInd.trim();
    }

    /**
     * @return FarmerCopInd
     */
    public String getFarmerCopInd() {
        return this.farmerCopInd;
    }

    /**
     * @param agrInd
     */
    public void setAgrInd(String agrInd) {
        this.agrInd = agrInd == null ? null : agrInd.trim();
    }

    /**
     * @return AgrInd
     */
    public String getAgrInd() {
        return this.agrInd;
    }

    /**
     * @param inclusiveFinanceStatistics
     */
    public void setInclusiveFinanceStatistics(String inclusiveFinanceStatistics) {
        this.inclusiveFinanceStatistics = inclusiveFinanceStatistics == null ? null : inclusiveFinanceStatistics.trim();
    }

    /**
     * @return InclusiveFinanceStatistics
     */
    public String getInclusiveFinanceStatistics() {
        return this.inclusiveFinanceStatistics;
    }

    /**
     * @param insCode
     */
    public void setInsCode(String insCode) {
        this.insCode = insCode == null ? null : insCode.trim();
    }

    /**
     * @return InsCode
     */
    public String getInsCode() {
        return this.insCode;
    }

    /**
     * @param insRegDate
     */
    public void setInsRegDate(String insRegDate) {
        this.insRegDate = insRegDate == null ? null : insRegDate.trim();
    }

    /**
     * @return InsRegDate
     */
    public String getInsRegDate() {
        return this.insRegDate;
    }

    /**
     * @param insEndDate
     */
    public void setInsEndDate(String insEndDate) {
        this.insEndDate = insEndDate == null ? null : insEndDate.trim();
    }

    /**
     * @return InsEndDate
     */
    public String getInsEndDate() {
        return this.insEndDate;
    }

    /**
     * @param insOrg
     */
    public void setInsOrg(String insOrg) {
        this.insOrg = insOrg == null ? null : insOrg.trim();
    }

    /**
     * @return InsOrg
     */
    public String getInsOrg() {
        return this.insOrg;
    }

    /**
     * @param insAnnDate
     */
    public void setInsAnnDate(String insAnnDate) {
        this.insAnnDate = insAnnDate == null ? null : insAnnDate.trim();
    }

    /**
     * @return InsAnnDate
     */
    public String getInsAnnDate() {
        return this.insAnnDate;
    }

    /**
     * @param licenseType
     */
    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType == null ? null : licenseType.trim();
    }

    /**
     * @return LicenseType
     */
    public String getLicenseType() {
        return this.licenseType;
    }

    /**
     * @param regiCode
     */
    public void setRegiCode(String regiCode) {
        this.regiCode = regiCode == null ? null : regiCode.trim();
    }

    /**
     * @return RegiCode
     */
    public String getRegiCode() {
        return this.regiCode;
    }

    /**
     * @param regiType
     */
    public void setRegiType(String regiType) {
        this.regiType = regiType == null ? null : regiType.trim();
    }

    /**
     * @return RegiType
     */
    public String getRegiType() {
        return this.regiType;
    }

    /**
     * @param quliGarade
     */
    public void setQuliGarade(String quliGarade) {
        this.quliGarade = quliGarade == null ? null : quliGarade.trim();
    }

    /**
     * @return QuliGarade
     */
    public String getQuliGarade() {
        return this.quliGarade;
    }

    /**
     * @param adminOrg
     */
    public void setAdminOrg(String adminOrg) {
        this.adminOrg = adminOrg == null ? null : adminOrg.trim();
    }

    /**
     * @return AdminOrg
     */
    public String getAdminOrg() {
        return this.adminOrg;
    }

    /**
     * @param apprOrg
     */
    public void setApprOrg(String apprOrg) {
        this.apprOrg = apprOrg == null ? null : apprOrg.trim();
    }

    /**
     * @return ApprOrg
     */
    public String getApprOrg() {
        return this.apprOrg;
    }

    /**
     * @param apprDocNo
     */
    public void setApprDocNo(String apprDocNo) {
        this.apprDocNo = apprDocNo == null ? null : apprDocNo.trim();
    }

    /**
     * @return ApprDocNo
     */
    public String getApprDocNo() {
        return this.apprDocNo;
    }

    /**
     * @param regiAreaCode
     */
    public void setRegiAreaCode(String regiAreaCode) {
        this.regiAreaCode = regiAreaCode == null ? null : regiAreaCode.trim();
    }

    /**
     * @return RegiAreaCode
     */
    public String getRegiAreaCode() {
        return this.regiAreaCode;
    }

    /**
     * @param regiAddr
     */
    public void setRegiAddr(String regiAddr) {
        this.regiAddr = regiAddr == null ? null : regiAddr.trim();
    }

    /**
     * @return RegiAddr
     */
    public String getRegiAddr() {
        return this.regiAddr;
    }

    /**
     * @param regiAddrEn
     */
    public void setRegiAddrEn(String regiAddrEn) {
        this.regiAddrEn = regiAddrEn == null ? null : regiAddrEn.trim();
    }

    /**
     * @return RegiAddrEn
     */
    public String getRegiAddrEn() {
        return this.regiAddrEn;
    }

    /**
     * @param acuStateCode
     */
    public void setAcuStateCode(String acuStateCode) {
        this.acuStateCode = acuStateCode == null ? null : acuStateCode.trim();
    }

    /**
     * @return AcuStateCode
     */
    public String getAcuStateCode() {
        return this.acuStateCode;
    }

    /**
     * @param operAddrAct
     */
    public void setOperAddrAct(String operAddrAct) {
        this.operAddrAct = operAddrAct == null ? null : operAddrAct.trim();
    }

    /**
     * @return OperAddrAct
     */
    public String getOperAddrAct() {
        return this.operAddrAct;
    }

    /**
     * @param mainOptScp
     */
    public void setMainOptScp(String mainOptScp) {
        this.mainOptScp = mainOptScp == null ? null : mainOptScp.trim();
    }

    /**
     * @return MainOptScp
     */
    public String getMainOptScp() {
        return this.mainOptScp;
    }

    /**
     * @param partOptScp
     */
    public void setPartOptScp(String partOptScp) {
        this.partOptScp = partOptScp == null ? null : partOptScp.trim();
    }

    /**
     * @return PartOptScp
     */
    public String getPartOptScp() {
        return this.partOptScp;
    }

    /**
     * @param regiCurType
     */
    public void setRegiCurType(String regiCurType) {
        this.regiCurType = regiCurType == null ? null : regiCurType.trim();
    }

    /**
     * @return RegiCurType
     */
    public String getRegiCurType() {
        return this.regiCurType;
    }

    /**
     * @param regiCapAmt
     */
    public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
        this.regiCapAmt = regiCapAmt;
    }

    /**
     * @return RegiCapAmt
     */
    public java.math.BigDecimal getRegiCapAmt() {
        return this.regiCapAmt;
    }

    /**
     * @param paidCapCurType
     */
    public void setPaidCapCurType(String paidCapCurType) {
        this.paidCapCurType = paidCapCurType == null ? null : paidCapCurType.trim();
    }

    /**
     * @return PaidCapCurType
     */
    public String getPaidCapCurType() {
        return this.paidCapCurType;
    }

    /**
     * @param paidCapAmt
     */
    public void setPaidCapAmt(java.math.BigDecimal paidCapAmt) {
        this.paidCapAmt = paidCapAmt;
    }

    /**
     * @return PaidCapAmt
     */
    public java.math.BigDecimal getPaidCapAmt() {
        return this.paidCapAmt;
    }

    /**
     * @param comMainOptScp
     */
    public void setComMainOptScp(String comMainOptScp) {
        this.comMainOptScp = comMainOptScp == null ? null : comMainOptScp.trim();
    }

    /**
     * @return ComMainOptScp
     */
    public String getComMainOptScp() {
        return this.comMainOptScp;
    }

    /**
     * @param regiStartDate
     */
    public void setRegiStartDate(String regiStartDate) {
        this.regiStartDate = regiStartDate == null ? null : regiStartDate.trim();
    }

    /**
     * @return RegiStartDate
     */
    public String getRegiStartDate() {
        return this.regiStartDate;
    }

    /**
     * @param regiEndDate
     */
    public void setRegiEndDate(String regiEndDate) {
        this.regiEndDate = regiEndDate == null ? null : regiEndDate.trim();
    }

    /**
     * @return RegiEndDate
     */
    public String getRegiEndDate() {
        return this.regiEndDate;
    }

    /**
     * @param regAudit
     */
    public void setRegAudit(String regAudit) {
        this.regAudit = regAudit == null ? null : regAudit.trim();
    }

    /**
     * @return RegAudit
     */
    public String getRegAudit() {
        return this.regAudit;
    }

    /**
     * @param regAuditDate
     */
    public void setRegAuditDate(String regAuditDate) {
        this.regAuditDate = regAuditDate == null ? null : regAuditDate.trim();
    }

    /**
     * @return RegAuditDate
     */
    public String getRegAuditDate() {
        return this.regAuditDate;
    }

    /**
     * @param regAuditEndDate
     */
    public void setRegAuditEndDate(String regAuditEndDate) {
        this.regAuditEndDate = regAuditEndDate == null ? null : regAuditEndDate.trim();
    }

    /**
     * @return RegAuditEndDate
     */
    public String getRegAuditEndDate() {
        return this.regAuditEndDate;
    }

    /**
     * @param natTaxRegCode
     */
    public void setNatTaxRegCode(String natTaxRegCode) {
        this.natTaxRegCode = natTaxRegCode == null ? null : natTaxRegCode.trim();
    }

    /**
     * @return NatTaxRegCode
     */
    public String getNatTaxRegCode() {
        return this.natTaxRegCode;
    }

    /**
     * @param natTaxRegOrg
     */
    public void setNatTaxRegOrg(String natTaxRegOrg) {
        this.natTaxRegOrg = natTaxRegOrg == null ? null : natTaxRegOrg.trim();
    }

    /**
     * @return NatTaxRegOrg
     */
    public String getNatTaxRegOrg() {
        return this.natTaxRegOrg;
    }

    /**
     * @param natTaxRegDt
     */
    public void setNatTaxRegDt(String natTaxRegDt) {
        this.natTaxRegDt = natTaxRegDt == null ? null : natTaxRegDt.trim();
    }

    /**
     * @return NatTaxRegDt
     */
    public String getNatTaxRegDt() {
        return this.natTaxRegDt;
    }

    /**
     * @param natTaxRegEndDt
     */
    public void setNatTaxRegEndDt(String natTaxRegEndDt) {
        this.natTaxRegEndDt = natTaxRegEndDt == null ? null : natTaxRegEndDt.trim();
    }

    /**
     * @return NatTaxRegEndDt
     */
    public String getNatTaxRegEndDt() {
        return this.natTaxRegEndDt;
    }

    /**
     * @param natTaxAnnDate
     */
    public void setNatTaxAnnDate(String natTaxAnnDate) {
        this.natTaxAnnDate = natTaxAnnDate == null ? null : natTaxAnnDate.trim();
    }

    /**
     * @return NatTaxAnnDate
     */
    public String getNatTaxAnnDate() {
        return this.natTaxAnnDate;
    }

    /**
     * @param locTaxRegCode
     */
    public void setLocTaxRegCode(String locTaxRegCode) {
        this.locTaxRegCode = locTaxRegCode == null ? null : locTaxRegCode.trim();
    }

    /**
     * @return LocTaxRegCode
     */
    public String getLocTaxRegCode() {
        return this.locTaxRegCode;
    }

    /**
     * @param locTaxRegOrg
     */
    public void setLocTaxRegOrg(String locTaxRegOrg) {
        this.locTaxRegOrg = locTaxRegOrg == null ? null : locTaxRegOrg.trim();
    }

    /**
     * @return LocTaxRegOrg
     */
    public String getLocTaxRegOrg() {
        return this.locTaxRegOrg;
    }

    /**
     * @param locTaxRegDt
     */
    public void setLocTaxRegDt(String locTaxRegDt) {
        this.locTaxRegDt = locTaxRegDt == null ? null : locTaxRegDt.trim();
    }

    /**
     * @return LocTaxRegDt
     */
    public String getLocTaxRegDt() {
        return this.locTaxRegDt;
    }

    /**
     * @param locTaxRegEndDt
     */
    public void setLocTaxRegEndDt(String locTaxRegEndDt) {
        this.locTaxRegEndDt = locTaxRegEndDt == null ? null : locTaxRegEndDt.trim();
    }

    /**
     * @return LocTaxRegEndDt
     */
    public String getLocTaxRegEndDt() {
        return this.locTaxRegEndDt;
    }

    /**
     * @param locTaxAnnDate
     */
    public void setLocTaxAnnDate(String locTaxAnnDate) {
        this.locTaxAnnDate = locTaxAnnDate == null ? null : locTaxAnnDate.trim();
    }

    /**
     * @return LocTaxAnnDate
     */
    public String getLocTaxAnnDate() {
        return this.locTaxAnnDate;
    }

    /**
     * @param loanCardFlg
     */
    public void setLoanCardFlg(String loanCardFlg) {
        this.loanCardFlg = loanCardFlg == null ? null : loanCardFlg.trim();
    }

    /**
     * @return LoanCardFlg
     */
    public String getLoanCardFlg() {
        return this.loanCardFlg;
    }

    /**
     * @param loanCardId
     */
    public void setLoanCardId(String loanCardId) {
        this.loanCardId = loanCardId == null ? null : loanCardId.trim();
    }

    /**
     * @return LoanCardId
     */
    public String getLoanCardId() {
        return this.loanCardId;
    }

    /**
     * @param loanCardPwd
     */
    public void setLoanCardPwd(String loanCardPwd) {
        this.loanCardPwd = loanCardPwd == null ? null : loanCardPwd.trim();
    }

    /**
     * @return LoanCardPwd
     */
    public String getLoanCardPwd() {
        return this.loanCardPwd;
    }

    /**
     * @param loanCardEffFlg
     */
    public void setLoanCardEffFlg(String loanCardEffFlg) {
        this.loanCardEffFlg = loanCardEffFlg == null ? null : loanCardEffFlg.trim();
    }

    /**
     * @return LoanCardEffFlg
     */
    public String getLoanCardEffFlg() {
        return this.loanCardEffFlg;
    }

    /**
     * @param loanCardAnnDate
     */
    public void setLoanCardAnnDate(String loanCardAnnDate) {
        this.loanCardAnnDate = loanCardAnnDate == null ? null : loanCardAnnDate.trim();
    }

    /**
     * @return LoanCardAnnDate
     */
    public String getLoanCardAnnDate() {
        return this.loanCardAnnDate;
    }

    /**
     * @param certType
     */
    public void setCertType(String certType) {
        this.certType = certType == null ? null : certType.trim();
    }

    /**
     * @return CertType
     */
    public String getCertType() {
        return this.certType;
    }

    /**
     * @param certCode
     */
    public void setCertCode(String certCode) {
        this.certCode = certCode == null ? null : certCode.trim();
    }

    /**
     * @return CertCode
     */
    public String getCertCode() {
        return this.certCode;
    }

    /**
     * @param certIdate
     */
    public void setCertIdate(String certIdate) {
        this.certIdate = certIdate == null ? null : certIdate.trim();
    }

    /**
     * @return CertIdate
     */
    public String getCertIdate() {
        return this.certIdate;
    }

    /**
     * @param conType
     */
    public void setConType(String conType) {
        this.conType = conType == null ? null : conType.trim();
    }

    /**
     * @return ConType
     */
    public String getConType() {
        return this.conType;
    }

    /**
     * @param corpOwnersType
     */
    public void setCorpOwnersType(String corpOwnersType) {
        this.corpOwnersType = corpOwnersType == null ? null : corpOwnersType.trim();
    }

    /**
     * @return CorpOwnersType
     */
    public String getCorpOwnersType() {
        return this.corpOwnersType;
    }

    /**
     * @param isBankShd
     */
    public void setIsBankShd(String isBankShd) {
        this.isBankShd = isBankShd == null ? null : isBankShd.trim();
    }

    /**
     * @return IsBankShd
     */
    public String getIsBankShd() {
        return this.isBankShd;
    }

    /**
     * @param isSmconCus
     */
    public void setIsSmconCus(String isSmconCus) {
        this.isSmconCus = isSmconCus == null ? null : isSmconCus.trim();
    }

    /**
     * @return IsSmconCus
     */
    public String getIsSmconCus() {
        return this.isSmconCus;
    }

    /**
     * @param detailAddr
     */
    public void setDetailAddr(String detailAddr) {
        this.detailAddr = detailAddr == null ? null : detailAddr.trim();
    }

    /**
     * @return DetailAddr
     */
    public String getDetailAddr() {
        return this.detailAddr;
    }

    /**
     * @param produceEquipYear
     */
    public void setProduceEquipYear(String produceEquipYear) {
        this.produceEquipYear = produceEquipYear == null ? null : produceEquipYear.trim();
    }

    /**
     * @return ProduceEquipYear
     */
    public String getProduceEquipYear() {
        return this.produceEquipYear;
    }

    /**
     * @param qq
     */
    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    /**
     * @return Qq
     */
    public String getQq() {
        return this.qq;
    }

    /**
     * @param produceAbiYear
     */
    public void setProduceAbiYear(String produceAbiYear) {
        this.produceAbiYear = produceAbiYear == null ? null : produceAbiYear.trim();
    }

    /**
     * @return ProduceAbiYear
     */
    public String getProduceAbiYear() {
        return this.produceAbiYear;
    }

    /**
     * @param isNatctl
     */
    public void setIsNatctl(String isNatctl) {
        this.isNatctl = isNatctl == null ? null : isNatctl.trim();
    }

    /**
     * @return IsNatctl
     */
    public String getIsNatctl() {
        return this.isNatctl;
    }

    /**
     * @param freqLinkman
     */
    public void setFreqLinkman(String freqLinkman) {
        this.freqLinkman = freqLinkman == null ? null : freqLinkman.trim();
    }

    /**
     * @return FreqLinkman
     */
    public String getFreqLinkman() {
        return this.freqLinkman;
    }

    /**
     * @param natctlLevel
     */
    public void setNatctlLevel(String natctlLevel) {
        this.natctlLevel = natctlLevel == null ? null : natctlLevel.trim();
    }

    /**
     * @return NatctlLevel
     */
    public String getNatctlLevel() {
        return this.natctlLevel;
    }

    /**
     * @param licOperPro
     */
    public void setLicOperPro(String licOperPro) {
        this.licOperPro = licOperPro == null ? null : licOperPro.trim();
    }

    /**
     * @return LicOperPro
     */
    public String getLicOperPro() {
        return this.licOperPro;
    }

    /**
     * @param freqLinkmanTel
     */
    public void setFreqLinkmanTel(String freqLinkmanTel) {
        this.freqLinkmanTel = freqLinkmanTel == null ? null : freqLinkmanTel.trim();
    }

    /**
     * @return FreqLinkmanTel
     */
    public String getFreqLinkmanTel() {
        return this.freqLinkmanTel;
    }

    /**
     * @param commonOperPro
     */
    public void setCommonOperPro(String commonOperPro) {
        this.commonOperPro = commonOperPro == null ? null : commonOperPro.trim();
    }

    /**
     * @return CommonOperPro
     */
    public String getCommonOperPro() {
        return this.commonOperPro;
    }

    /**
     * @param operStatus
     */
    public void setOperStatus(String operStatus) {
        this.operStatus = operStatus == null ? null : operStatus.trim();
    }

    /**
     * @return OperStatus
     */
    public String getOperStatus() {
        return this.operStatus;
    }

    /**
     * @param isLongVld
     */
    public void setIsLongVld(String isLongVld) {
        this.isLongVld = isLongVld == null ? null : isLongVld.trim();
    }

    /**
     * @return IsLongVld
     */
    public String getIsLongVld() {
        return this.isLongVld;
    }

    /**
     * @param basicDepAccNoOpenLic
     */
    public void setBasicDepAccNoOpenLic(String basicDepAccNoOpenLic) {
        this.basicDepAccNoOpenLic = basicDepAccNoOpenLic == null ? null : basicDepAccNoOpenLic.trim();
    }

    /**
     * @return BasicDepAccNoOpenLic
     */
    public String getBasicDepAccNoOpenLic() {
        return this.basicDepAccNoOpenLic;
    }

    /**
     * @param basicDepAccNo
     */
    public void setBasicDepAccNo(String basicDepAccNo) {
        this.basicDepAccNo = basicDepAccNo == null ? null : basicDepAccNo.trim();
    }

    /**
     * @return BasicDepAccNo
     */
    public String getBasicDepAccNo() {
        return this.basicDepAccNo;
    }

    /**
     * @param isBankBasicDepAccNo
     */
    public void setIsBankBasicDepAccNo(String isBankBasicDepAccNo) {
        this.isBankBasicDepAccNo = isBankBasicDepAccNo == null ? null : isBankBasicDepAccNo.trim();
    }

    /**
     * @return IsBankBasicDepAccNo
     */
    public String getIsBankBasicDepAccNo() {
        return this.isBankBasicDepAccNo;
    }

    /**
     * @param basicDepAccob
     */
    public void setBasicDepAccob(String basicDepAccob) {
        this.basicDepAccob = basicDepAccob == null ? null : basicDepAccob.trim();
    }

    /**
     * @return BasicDepAccob
     */
    public String getBasicDepAccob() {
        return this.basicDepAccob;
    }

    /**
     * @param basicAccNoOpenDate
     */
    public void setBasicAccNoOpenDate(String basicAccNoOpenDate) {
        this.basicAccNoOpenDate = basicAccNoOpenDate == null ? null : basicAccNoOpenDate.trim();
    }

    /**
     * @return BasicAccNoOpenDate
     */
    public String getBasicAccNoOpenDate() {
        return this.basicAccNoOpenDate;
    }

    /**
     * @param commonAccNoOpenDate
     */
    public void setCommonAccNoOpenDate(String commonAccNoOpenDate) {
        this.commonAccNoOpenDate = commonAccNoOpenDate == null ? null : commonAccNoOpenDate.trim();
    }

    /**
     * @return CommonAccNoOpenDate
     */
    public String getCommonAccNoOpenDate() {
        return this.commonAccNoOpenDate;
    }

    /**
     * @param mainPrdDesc
     */
    public void setMainPrdDesc(String mainPrdDesc) {
        this.mainPrdDesc = mainPrdDesc == null ? null : mainPrdDesc.trim();
    }

    /**
     * @return MainPrdDesc
     */
    public String getMainPrdDesc() {
        return this.mainPrdDesc;
    }

    /**
     * @param creditLevelOuter
     */
    public void setCreditLevelOuter(String creditLevelOuter) {
        this.creditLevelOuter = creditLevelOuter == null ? null : creditLevelOuter.trim();
    }

    /**
     * @return CreditLevelOuter
     */
    public String getCreditLevelOuter() {
        return this.creditLevelOuter;
    }

    /**
     * @param evalDate
     */
    public void setEvalDate(String evalDate) {
        this.evalDate = evalDate == null ? null : evalDate.trim();
    }

    /**
     * @return EvalDate
     */
    public String getEvalDate() {
        return this.evalDate;
    }

    /**
     * @param fax
     */
    public void setFax(String fax) {
        this.fax = fax == null ? null : fax.trim();
    }

    /**
     * @return Fax
     */
    public String getFax() {
        return this.fax;
    }

    /**
     * @param evalOrgId
     */
    public void setEvalOrgId(String evalOrgId) {
        this.evalOrgId = evalOrgId == null ? null : evalOrgId.trim();
    }

    /**
     * @return EvalOrgId
     */
    public String getEvalOrgId() {
        return this.evalOrgId;
    }

    /**
     * @param linkmanEmail
     */
    public void setLinkmanEmail(String linkmanEmail) {
        this.linkmanEmail = linkmanEmail == null ? null : linkmanEmail.trim();
    }

    /**
     * @return LinkmanEmail
     */
    public String getLinkmanEmail() {
        return this.linkmanEmail;
    }

    /**
     * @param sendAddr
     */
    public void setSendAddr(String sendAddr) {
        this.sendAddr = sendAddr == null ? null : sendAddr.trim();
    }

    /**
     * @return SendAddr
     */
    public String getSendAddr() {
        return this.sendAddr;
    }

    /**
     * @param initLoanDate
     */
    public void setInitLoanDate(String initLoanDate) {
        this.initLoanDate = initLoanDate == null ? null : initLoanDate.trim();
    }

    /**
     * @return InitLoanDate
     */
    public String getInitLoanDate() {
        return this.initLoanDate;
    }

    /**
     * @param wechatNo
     */
    public void setWechatNo(String wechatNo) {
        this.wechatNo = wechatNo == null ? null : wechatNo.trim();
    }

    /**
     * @return WechatNo
     */
    public String getWechatNo() {
        return this.wechatNo;
    }

    /**
     * @param isStrgcCus
     */
    public void setIsStrgcCus(String isStrgcCus) {
        this.isStrgcCus = isStrgcCus == null ? null : isStrgcCus.trim();
    }

    /**
     * @return IsStrgcCus
     */
    public String getIsStrgcCus() {
        return this.isStrgcCus;
    }

    /**
     * @param finaReportType
     */
    public void setFinaReportType(String finaReportType) {
        this.finaReportType = finaReportType == null ? null : finaReportType.trim();
    }

    /**
     * @return FinaReportType
     */
    public String getFinaReportType() {
        return this.finaReportType;
    }

    /**
     * @param areaPriorCorp
     */
    public void setAreaPriorCorp(String areaPriorCorp) {
        this.areaPriorCorp = areaPriorCorp == null ? null : areaPriorCorp.trim();
    }

    /**
     * @return AreaPriorCorp
     */
    public String getAreaPriorCorp() {
        return this.areaPriorCorp;
    }

    /**
     * @param spOperFlag
     */
    public void setSpOperFlag(String spOperFlag) {
        this.spOperFlag = spOperFlag == null ? null : spOperFlag.trim();
    }

    /**
     * @return SpOperFlag
     */
    public String getSpOperFlag() {
        return this.spOperFlag;
    }

    /**
     * @param isNewBuildCorp
     */
    public void setIsNewBuildCorp(String isNewBuildCorp) {
        this.isNewBuildCorp = isNewBuildCorp == null ? null : isNewBuildCorp.trim();
    }

    /**
     * @return IsNewBuildCorp
     */
    public String getIsNewBuildCorp() {
        return this.isNewBuildCorp;
    }

    /**
     * @param grpCusType
     */
    public void setGrpCusType(String grpCusType) {
        this.grpCusType = grpCusType == null ? null : grpCusType.trim();
    }

    /**
     * @return GrpCusType
     */
    public String getGrpCusType() {
        return this.grpCusType;
    }

    /**
     * @param regiOrg
     */
    public void setRegiOrg(String regiOrg) {
        this.regiOrg = regiOrg == null ? null : regiOrg.trim();
    }

    /**
     * @return RegiOrg
     */
    public String getRegiOrg() {
        return this.regiOrg;
    }

    /**
     * @param mainBusNation
     */
    public void setMainBusNation(String mainBusNation) {
        this.mainBusNation = mainBusNation == null ? null : mainBusNation.trim();
    }

    /**
     * @return MainBusNation
     */
    public String getMainBusNation() {
        return this.mainBusNation;
    }

    /**
     * @param creditPadAccNo
     */
    public void setCreditPadAccNo(String creditPadAccNo) {
        this.creditPadAccNo = creditPadAccNo == null ? null : creditPadAccNo.trim();
    }

    /**
     * @return CreditPadAccNo
     */
    public String getCreditPadAccNo() {
        return this.creditPadAccNo;
    }

    /**
     * @param registerAddress
     */
    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress == null ? null : registerAddress.trim();
    }

    /**
     * @return RegisterAddress
     */
    public String getRegisterAddress() {
        return this.registerAddress;
    }

    /**
     * @param loanType
     */
    public void setLoanType(String loanType) {
        this.loanType = loanType == null ? null : loanType.trim();
    }

    /**
     * @return LoanType
     */
    public String getLoanType() {
        return this.loanType;
    }

    /**
     * @param operPlaceOwnshp
     */
    public void setOperPlaceOwnshp(String operPlaceOwnshp) {
        this.operPlaceOwnshp = operPlaceOwnshp == null ? null : operPlaceOwnshp.trim();
    }

    /**
     * @return OperPlaceOwnshp
     */
    public String getOperPlaceOwnshp() {
        return this.operPlaceOwnshp;
    }

    /**
     * @param operPlaceSqu
     */
    public void setOperPlaceSqu(java.math.BigDecimal operPlaceSqu) {
        this.operPlaceSqu = operPlaceSqu;
    }

    /**
     * @return OperPlaceSqu
     */
    public java.math.BigDecimal getOperPlaceSqu() {
        return this.operPlaceSqu;
    }

    /**
     * @param mainProduceEquip
     */
    public void setMainProduceEquip(String mainProduceEquip) {
        this.mainProduceEquip = mainProduceEquip == null ? null : mainProduceEquip.trim();
    }

    /**
     * @return MainProduceEquip
     */
    public String getMainProduceEquip() {
        return this.mainProduceEquip;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId == null ? null : managerId.trim();
    }

    /**
     * @return managerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId == null ? null : managerBrId.trim();
    }

    /**
     * @return managerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return CreateTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return UpdateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    /**
     * @param iisSzjrfwCrop
     */
    public void setIisSzjrfwCrop(String iisSzjrfwCrop) {
        this.iisSzjrfwCrop = iisSzjrfwCrop == null ? null : iisSzjrfwCrop.trim();
    }

    /**
     * @return IisSzjrfwCrop
     */
    public String getIisSzjrfwCrop() {
        return this.iisSzjrfwCrop;
    }

    /**
     * @param isSteelCus
     */
    public void setIsSteelCus(String isSteelCus) {
        this.isSteelCus = isSteelCus == null ? null : isSteelCus.trim();
    }

    /**
     * @return IsSteelCus
     */
    public String getIsSteelCus() {
        return this.isSteelCus;
    }

    /**
     * @param isStockCorp
     */
    public void setIsStockCorp(String isStockCorp) {
        this.isStockCorp = isStockCorp == null ? null : isStockCorp.trim();
    }

    /**
     * @return IsStockCorp
     */
    public String getIsStockCorp() {
        return this.isStockCorp;
    }

    /**
     * @param isCtinve
     */
    public void setIsCtinve(String isCtinve) {
        this.isCtinve = isCtinve == null ? null : isCtinve.trim();
    }

    /**
     * @return IsCtinve
     */
    public String getIsCtinve() {
        return this.isCtinve;
    }

    /**
     * @param ctinveLevel
     */
    public void setCtinveLevel(String ctinveLevel) {
        this.ctinveLevel = ctinveLevel == null ? null : ctinveLevel.trim();
    }

    /**
     * @return CtinveLevel
     */
    public String getCtinveLevel() {
        return this.ctinveLevel;
    }

    /**
     * @param goverInvestPlat
     */
    public void setGoverInvestPlat(String goverInvestPlat) {
        this.goverInvestPlat = goverInvestPlat == null ? null : goverInvestPlat.trim();
    }

    /**
     * @return GoverInvestPlat
     */
    public String getGoverInvestPlat() {
        return this.goverInvestPlat;
    }

    /**
     * @param impexpFlag
     */
    public void setImpexpFlag(String impexpFlag) {
        this.impexpFlag = impexpFlag == null ? null : impexpFlag.trim();
    }

    /**
     * @return ImpexpFlag
     */
    public String getImpexpFlag() {
        return this.impexpFlag;
    }

    /**
     * @param redcbizUnitTradeClass
     */
    public void setRedcbizUnitTradeClass(String redcbizUnitTradeClass) {
        this.redcbizUnitTradeClass = redcbizUnitTradeClass == null ? null : redcbizUnitTradeClass.trim();
    }

    /**
     * @return RedcbizUnitTradeClass
     */
    public String getRedcbizUnitTradeClass() {
        return this.redcbizUnitTradeClass;
    }

    /**
     * @param cusType
     */
    public void setCusType(String cusType) {
        this.cusType = cusType == null ? null : cusType.trim();
    }

    /**
     * @return CusType
     */
    public String getCusType() {
        return this.cusType;
    }

    /**
     * @param cusName
     */
    public void setCusName(String cusName) {
        this.cusName = cusName == null ? null : cusName.trim();
    }

    /**
     * @return CusName
     */
    public String getCusName() {
        return this.cusName;
    }


}