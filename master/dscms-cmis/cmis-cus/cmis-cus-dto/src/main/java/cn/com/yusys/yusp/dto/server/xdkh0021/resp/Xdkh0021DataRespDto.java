package cn.com.yusys.yusp.dto.server.xdkh0021.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：在信贷系统中生成用户信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0021DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "opFlag")
    private String opFlag;//能否关闭
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xdkh0021DataRespDto{" +
                "opFlag='" + opFlag + '\'' +
                ", opMsg='" + opMsg + '\'' +
                '}';
    }
}
