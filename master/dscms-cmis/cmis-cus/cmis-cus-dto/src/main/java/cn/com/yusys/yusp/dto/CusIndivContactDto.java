package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivContact
 * @类描述: cus_indiv_contact数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-05-03 21:17:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusIndivContactDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 身份证地址 **/
	private String certAddr;
	
	/** 身份证地址街道 **/
	private String certStreet;
	
	/** 居住地地址 **/
	private String indivRsdAddr;
	
	/** 居住地址街道 **/
	private String streetRsd;
	
	/** 居住地邮政编码 **/
	private String indivZipCode;
	
	/** 居住状况 STD_ZB_RSD_ST **/
	private String indivRsdSt;
	
	/** 住宅电话 **/
	private String mobile;
	
	/** 居住区域编号 **/
	private String regionalism;
	
	/** 居住区域名称 **/
	private String regionName;
	
	/** 手机号码1 **/
	private String mobileA;
	
	/** 号码是否验证1 STD_ZB_YES_NO **/
	private String isCheckA;
	
	/** 号码是否本人1 STD_ZB_YES_NO **/
	private String isOneselfA;
	
	/** 手机号码2 **/
	private String mobileB;
	
	/** 号码是否验证2 STD_ZB_YES_NO **/
	private String isCheckB;
	
	/** 号码是否本人2 STD_ZB_YES_NO **/
	private String isOneselfB;
	
	/** 传真 **/
	private String faxCode;
	
	/** Email地址 **/
	private String email;
	
	/** 手机号码1来源 **/
	private String mobile1Sour;
	
	/** 手机号码2来源 **/
	private String mobile2Sour;
	
	/** 微信号 **/
	private String wechatNo;
	
	/** 手机号码 **/
	private String mobileNo;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** QQ号 **/
	private String qq;
	
	/** 户籍地址 **/
	private String indivHouhRegAdd;
	
	/** 户籍地址街道 **/
	private String regionStreet;
	
	/** 送达地址 **/
	private String sendAddr;
	
	/** 送达地址街道 **/
	private String deliveryStreet;
	
	/** 邮政编码 **/
	private String postcode;
	
	/** 是否农户 **/
	private String isAgri;
	
	/** 居住时间-年 **/
	private String resiTime;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;
	
	/** 创建日期 **/
	private Date createTime;

	/** 更新日期 **/
	private Date updateTime;

	/** 保存状态 **/
	private String saveStatus;


	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

    /**
     * @return CusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param certAddr
	 */
	public void setCertAddr(String certAddr) {
		this.certAddr = certAddr == null ? null : certAddr.trim();
	}

    /**
     * @return CertAddr
     */
	public String getCertAddr() {
		return this.certAddr;
	}

	/**
	 * @param certStreet
	 */
	public void setCertStreet(String certStreet) {
		this.certStreet = certStreet == null ? null : certStreet.trim();
	}

    /**
     * @return CertStreet
     */
	public String getCertStreet() {
		return this.certStreet;
	}

	/**
	 * @param indivRsdAddr
	 */
	public void setIndivRsdAddr(String indivRsdAddr) {
		this.indivRsdAddr = indivRsdAddr == null ? null : indivRsdAddr.trim();
	}

    /**
     * @return IndivRsdAddr
     */
	public String getIndivRsdAddr() {
		return this.indivRsdAddr;
	}

	/**
	 * @param streetRsd
	 */
	public void setStreetRsd(String streetRsd) {
		this.streetRsd = streetRsd == null ? null : streetRsd.trim();
	}

    /**
     * @return StreetRsd
     */
	public String getStreetRsd() {
		return this.streetRsd;
	}

	/**
	 * @param indivZipCode
	 */
	public void setIndivZipCode(String indivZipCode) {
		this.indivZipCode = indivZipCode == null ? null : indivZipCode.trim();
	}

    /**
     * @return IndivZipCode
     */
	public String getIndivZipCode() {
		return this.indivZipCode;
	}

	/**
	 * @param indivRsdSt
	 */
	public void setIndivRsdSt(String indivRsdSt) {
		this.indivRsdSt = indivRsdSt == null ? null : indivRsdSt.trim();
	}

    /**
     * @return IndivRsdSt
     */
	public String getIndivRsdSt() {
		return this.indivRsdSt;
	}

	/**
	 * @param mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile == null ? null : mobile.trim();
	}

    /**
     * @return Mobile
     */
	public String getMobile() {
		return this.mobile;
	}

	/**
	 * @param regionalism
	 */
	public void setRegionalism(String regionalism) {
		this.regionalism = regionalism == null ? null : regionalism.trim();
	}

    /**
     * @return Regionalism
     */
	public String getRegionalism() {
		return this.regionalism;
	}

	/**
	 * @param regionName
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName == null ? null : regionName.trim();
	}

    /**
     * @return RegionName
     */
	public String getRegionName() {
		return this.regionName;
	}

	/**
	 * @param mobileA
	 */
	public void setMobileA(String mobileA) {
		this.mobileA = mobileA == null ? null : mobileA.trim();
	}

    /**
     * @return MobileA
     */
	public String getMobileA() {
		return this.mobileA;
	}

	/**
	 * @param isCheckA
	 */
	public void setIsCheckA(String isCheckA) {
		this.isCheckA = isCheckA == null ? null : isCheckA.trim();
	}

    /**
     * @return IsCheckA
     */
	public String getIsCheckA() {
		return this.isCheckA;
	}

	/**
	 * @param isOneselfA
	 */
	public void setIsOneselfA(String isOneselfA) {
		this.isOneselfA = isOneselfA == null ? null : isOneselfA.trim();
	}

    /**
     * @return IsOneselfA
     */
	public String getIsOneselfA() {
		return this.isOneselfA;
	}

	/**
	 * @param mobileB
	 */
	public void setMobileB(String mobileB) {
		this.mobileB = mobileB == null ? null : mobileB.trim();
	}

    /**
     * @return MobileB
     */
	public String getMobileB() {
		return this.mobileB;
	}

	/**
	 * @param isCheckB
	 */
	public void setIsCheckB(String isCheckB) {
		this.isCheckB = isCheckB == null ? null : isCheckB.trim();
	}

    /**
     * @return IsCheckB
     */
	public String getIsCheckB() {
		return this.isCheckB;
	}

	/**
	 * @param isOneselfB
	 */
	public void setIsOneselfB(String isOneselfB) {
		this.isOneselfB = isOneselfB == null ? null : isOneselfB.trim();
	}

    /**
     * @return IsOneselfB
     */
	public String getIsOneselfB() {
		return this.isOneselfB;
	}

	/**
	 * @param faxCode
	 */
	public void setFaxCode(String faxCode) {
		this.faxCode = faxCode == null ? null : faxCode.trim();
	}

    /**
     * @return FaxCode
     */
	public String getFaxCode() {
		return this.faxCode;
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

    /**
     * @return Email
     */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @param mobile1Sour
	 */
	public void setMobile1Sour(String mobile1Sour) {
		this.mobile1Sour = mobile1Sour == null ? null : mobile1Sour.trim();
	}

    /**
     * @return Mobile1Sour
     */
	public String getMobile1Sour() {
		return this.mobile1Sour;
	}

	/**
	 * @param mobile2Sour
	 */
	public void setMobile2Sour(String mobile2Sour) {
		this.mobile2Sour = mobile2Sour == null ? null : mobile2Sour.trim();
	}

    /**
     * @return Mobile2Sour
     */
	public String getMobile2Sour() {
		return this.mobile2Sour;
	}

	/**
	 * @param wechatNo
	 */
	public void setWechatNo(String wechatNo) {
		this.wechatNo = wechatNo == null ? null : wechatNo.trim();
	}

    /**
     * @return WechatNo
     */
	public String getWechatNo() {
		return this.wechatNo;
	}

	/**
	 * @param mobileNo
	 */
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo == null ? null : mobileNo.trim();
	}

    /**
     * @return MobileNo
     */
	public String getMobileNo() {
		return this.mobileNo;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq == null ? null : qq.trim();
	}

    /**
     * @return Qq
     */
	public String getQq() {
		return this.qq;
	}

	/**
	 * @param indivHouhRegAdd
	 */
	public void setIndivHouhRegAdd(String indivHouhRegAdd) {
		this.indivHouhRegAdd = indivHouhRegAdd == null ? null : indivHouhRegAdd.trim();
	}

    /**
     * @return indivHouhRegAdd
     */
	public String getIndivHouhRegAdd() {
		return this.indivHouhRegAdd;
	}

	/**
	 * @param regionStreet
	 */
	public void setRegionStreet(String regionStreet) {
		this.regionStreet = regionStreet == null ? null : regionStreet.trim();
	}

    /**
     * @return RegionStreet
     */
	public String getRegionStreet() {
		return this.regionStreet;
	}

	/**
	 * @param sendAddr
	 */
	public void setSendAddr(String sendAddr) {
		this.sendAddr = sendAddr == null ? null : sendAddr.trim();
	}

    /**
     * @return SendAddr
     */
	public String getSendAddr() {
		return this.sendAddr;
	}

	/**
	 * @param deliveryStreet
	 */
	public void setDeliveryStreet(String deliveryStreet) {
		this.deliveryStreet = deliveryStreet == null ? null : deliveryStreet.trim();
	}

    /**
     * @return DeliveryStreet
     */
	public String getDeliveryStreet() {
		return this.deliveryStreet;
	}

	/**
	 * @param postcode
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode == null ? null : postcode.trim();
	}

    /**
     * @return Postcode
     */
	public String getPostcode() {
		return this.postcode;
	}

	/**
	 * @param isAgri
	 */
	public void setIsAgri(String isAgri) {
		this.isAgri = isAgri == null ? null : isAgri.trim();
	}

    /**
     * @return IsAgri
     */
	public String getIsAgri() {
		return this.isAgri;
	}

	/**
	 * @param resiTime
	 */
	public void setResiTime(String resiTime) {
		this.resiTime = resiTime == null ? null : resiTime.trim();
	}

    /**
     * @return ResiTime
     */
	public String getResiTime() {
		return this.resiTime;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

    /**
     * @return InputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

    /**
     * @return InputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

    /**
     * @return InputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

    /**
     * @return UpdId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

    /**
     * @return UpdBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

    /**
     * @return UpdDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return CreateTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

    /**
     * @return UpdateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param saveStatus
	 */
	public void setSaveStatus(String saveStatus) {
		this.saveStatus = saveStatus == null ? null : saveStatus.trim();
	}
	
    /**
     * @return SaveStatus
     */	
	public String getSaveStatus() {
		return this.saveStatus;
	}


}