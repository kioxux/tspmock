package cn.com.yusys.yusp.dto.server.xdkh0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：优企贷、优农贷行内关联人基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0026DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "query_type")
    private String query_type;//查询类型
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称

    public String getQuery_type() {
        return query_type;
    }

    public void setQuery_type(String query_type) {
        this.query_type = query_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    @Override
    public String toString() {
        return "Xdkh0026DataReqDto{" +
                "query_type='" + query_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cus_name='" + cus_name + '\'' +
                '}';
    }
}