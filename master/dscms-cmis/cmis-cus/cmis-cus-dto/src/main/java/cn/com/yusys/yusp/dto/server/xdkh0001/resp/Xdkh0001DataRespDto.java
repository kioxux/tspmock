package cn.com.yusys.yusp.dto.server.xdkh0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：个人客户基本信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0001DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "indivSex")
    private String indivSex;//性别
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码
    @JsonProperty(value = "certEndDt")
    private String certEndDt;//证件到期日
    @JsonProperty(value = "isAgri")
    private String isAgri;//是否农户
    @JsonProperty(value = "cusCrdType")
    private String cusCrdType;//个人评级类型
    @JsonProperty(value = "isLoan")
    private String isLoan;//是否强村富民贷款
    @JsonProperty(value = "nation")
    private String nation;//国别
    @JsonProperty(value = "indivFolk")
    private String indivFolk;//民族
    @JsonProperty(value = "indivBrtPlace")
    private String indivBrtPlace;//籍贯
    @JsonProperty(value = "indivHouhRegAdd")
    private String indivHouhRegAdd;//户籍地址
    @JsonProperty(value = "indivDtOfBirth")
    private String indivDtOfBirth;//出生年月日
    @JsonProperty(value = "indivPolSt")
    private String indivPolSt;//政治面貌
    @JsonProperty(value = "indivEdt")
    private String indivEdt;//最高学历
    @JsonProperty(value = "indivDgr")
    private String indivDgr;//最高学位
    @JsonProperty(value = "marStatus")
    private String marStatus;//婚姻状况
    @JsonProperty(value = "healthStatus")
    private String healthStatus;//健康状况
    @JsonProperty(value = "comInitLoanDate")
    private String comInitLoanDate;//建立信贷关系时间
    @JsonProperty(value = "cdtLvl")
    private String cdtLvl;//信用等级
    @JsonProperty(value = "pcode")
    private String pcode;//邮政编码
    @JsonProperty(value = "regionalism")
    private String regionalism;//区域编码
    @JsonProperty(value = "regionName")
    private String regionName;//区域名称
    @JsonProperty(value = "mobileNo")
    private String mobileNo;//手机
    @JsonProperty(value = "faxCode")
    private String faxCode;//传真
    @JsonProperty(value = "email")
    private String email;//Email地址
    @JsonProperty(value = "indivRsdAddr")
    private String indivRsdAddr;//常驻居住地址
    @JsonProperty(value = "indivZipCode")
    private String indivZipCode;//常驻居住地邮政编码
    @JsonProperty(value = "indivRsdSt")
    private String indivRsdSt;//居住状况
    @JsonProperty(value = "resiTime")
    private String resiTime;//居住时间
    @JsonProperty(value = "employeeStatus")
    private String employeeStatus;//雇佣状态
    @JsonProperty(value = "occu")
    private String occu;//从事职业
    @JsonProperty(value = "unitName")
    private String unitName;//工作单位
    @JsonProperty(value = "indivComTyp")
    private String indivComTyp;//单位性质
    @JsonProperty(value = "indivComTrade")
    private String indivComTrade;//单位所属行业
    @JsonProperty(value = "unitAddr")
    private String unitAddr;//单位地址
    @JsonProperty(value = "unitPhn")
    private String unitPhn;//单位电话
    @JsonProperty(value = "unitDate")
    private String unitDate;//单位工作起始年
    @JsonProperty(value = "jobTtl")
    private String jobTtl;//职务
    @JsonProperty(value = "indivCrtfctn")
    private String indivCrtfctn;//职称
    @JsonProperty(value = "earningCurType")
    private String earningCurType;//收入币种
    @JsonProperty(value = "indivYearn")
    private String indivYearn;//年收入情况
    @JsonProperty(value = "indivMarBaby")
    private String indivMarBaby;//有无子女
    @JsonProperty(value = "familyYearn")
    private BigDecimal familyYearn;//家庭年收入
    @JsonProperty(value = "familyPhone")
    private String familyPhone;//家庭电话
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "cusStatus")
    private String cusStatus;//客户状态
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称
    @JsonProperty(value = "legalAddr")
    private String legalAddr;//通讯地址
    @JsonProperty(value = "indivSocScr")
    private String indivSocScr;//社会保障情况
    @JsonProperty(value = "indivComFldName")
    private String indivComFldName;//所属行业名称
    @JsonProperty(value = "cusType")
    private String cusType;//客户类型
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//管户机构

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public void setComInitLoanDate(String comInitLoanDate) {
        this.comInitLoanDate = comInitLoanDate;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getIndivSex() {
        return indivSex;
    }

    public void setIndivSex(String indivSex) {
        this.indivSex = indivSex;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCertEndDt() {
        return certEndDt;
    }

    public void setCertEndDt(String certEndDt) {
        this.certEndDt = certEndDt;
    }

    public String getIsAgri() {
        return isAgri;
    }

    public void setIsAgri(String isAgri) {
        this.isAgri = isAgri;
    }

    public String getCusCrdType() {
        return cusCrdType;
    }

    public void setCusCrdType(String cusCrdType) {
        this.cusCrdType = cusCrdType;
    }

    public String getIsLoan() {
        return isLoan;
    }

    public void setIsLoan(String isLoan) {
        this.isLoan = isLoan;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getIndivFolk() {
        return indivFolk;
    }

    public void setIndivFolk(String indivFolk) {
        this.indivFolk = indivFolk;
    }

    public String getIndivBrtPlace() {
        return indivBrtPlace;
    }

    public void setIndivBrtPlace(String indivBrtPlace) {
        this.indivBrtPlace = indivBrtPlace;
    }

    public String getIndivHouhRegAdd() {
        return indivHouhRegAdd;
    }

    public void setIndivHouhRegAdd(String indivHouhRegAdd) {
        this.indivHouhRegAdd = indivHouhRegAdd;
    }

    public String getIndivDtOfBirth() {
        return indivDtOfBirth;
    }

    public void setIndivDtOfBirth(String indivDtOfBirth) {
        this.indivDtOfBirth = indivDtOfBirth;
    }

    public String getIndivPolSt() {
        return indivPolSt;
    }

    public void setIndivPolSt(String indivPolSt) {
        this.indivPolSt = indivPolSt;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public String getIndivDgr() {
        return indivDgr;
    }

    public void setIndivDgr(String indivDgr) {
        this.indivDgr = indivDgr;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public String getComInitLoanDate() {
        return comInitLoanDate;
    }

    public String getCdtLvl() {
        return cdtLvl;
    }

    public void setCdtLvl(String cdtLvl) {
        this.cdtLvl = cdtLvl;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getRegionalism() {
        return regionalism;
    }

    public void setRegionalism(String regionalism) {
        this.regionalism = regionalism;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getFaxCode() {
        return faxCode;
    }

    public void setFaxCode(String faxCode) {
        this.faxCode = faxCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIndivRsdAddr() {
        return indivRsdAddr;
    }

    public void setIndivRsdAddr(String indivRsdAddr) {
        this.indivRsdAddr = indivRsdAddr;
    }

    public String getIndivZipCode() {
        return indivZipCode;
    }

    public void setIndivZipCode(String indivZipCode) {
        this.indivZipCode = indivZipCode;
    }

    public String getIndivRsdSt() {
        return indivRsdSt;
    }

    public void setIndivRsdSt(String indivRsdSt) {
        this.indivRsdSt = indivRsdSt;
    }

    public String getResiTime() {
        return resiTime;
    }

    public void setResiTime(String resiTime) {
        this.resiTime = resiTime;
    }

    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public String getOccu() {
        return occu;
    }

    public void setOccu(String occu) {
        this.occu = occu;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getIndivComTyp() {
        return indivComTyp;
    }

    public void setIndivComTyp(String indivComTyp) {
        this.indivComTyp = indivComTyp;
    }

    public String getIndivComTrade() {
        return indivComTrade;
    }

    public void setIndivComTrade(String indivComTrade) {
        this.indivComTrade = indivComTrade;
    }

    public String getUnitAddr() {
        return unitAddr;
    }

    public void setUnitAddr(String unitAddr) {
        this.unitAddr = unitAddr;
    }

    public String getUnitPhn() {
        return unitPhn;
    }

    public void setUnitPhn(String unitPhn) {
        this.unitPhn = unitPhn;
    }

    public String getUnitDate() {
        return unitDate;
    }

    public void setUnitDate(String unitDate) {
        this.unitDate = unitDate;
    }

    public String getJobTtl() {
        return jobTtl;
    }

    public void setJobTtl(String jobTtl) {
        this.jobTtl = jobTtl;
    }

    public String getIndivCrtfctn() {
        return indivCrtfctn;
    }

    public void setIndivCrtfctn(String indivCrtfctn) {
        this.indivCrtfctn = indivCrtfctn;
    }

    public String getEarningCurType() {
        return earningCurType;
    }

    public void setEarningCurType(String earningCurType) {
        this.earningCurType = earningCurType;
    }

    public String getIndivYearn() {
        return indivYearn;
    }

    public void setIndivYearn(String indivYearn) {
        this.indivYearn = indivYearn;
    }

    public String getIndivMarBaby() {
        return indivMarBaby;
    }

    public void setIndivMarBaby(String indivMarBaby) {
        this.indivMarBaby = indivMarBaby;
    }

    public BigDecimal getFamilyYearn() {
        return familyYearn;
    }

    public void setFamilyYearn(BigDecimal familyYearn) {
        this.familyYearn = familyYearn;
    }

    public String getFamilyPhone() {
        return familyPhone;
    }

    public void setFamilyPhone(String familyPhone) {
        this.familyPhone = familyPhone;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getCusStatus() {
        return cusStatus;
    }

    public void setCusStatus(String cusStatus) {
        this.cusStatus = cusStatus;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getLegalAddr() {
        return legalAddr;
    }

    public void setLegalAddr(String legalAddr) {
        this.legalAddr = legalAddr;
    }

    public String getIndivSocScr() {
        return indivSocScr;
    }

    public void setIndivSocScr(String indivSocScr) {
        this.indivSocScr = indivSocScr;
    }

    public String getIndivComFldName() {
        return indivComFldName;
    }

    public void setIndivComFldName(String indivComFldName) {
        this.indivComFldName = indivComFldName;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    @Override
    public String toString() {
        return "Xdkh0001DataRespDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", indivSex='" + indivSex + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", certEndDt='" + certEndDt + '\'' +
                ", isAgri='" + isAgri + '\'' +
                ", cusCrdType='" + cusCrdType + '\'' +
                ", isLoan='" + isLoan + '\'' +
                ", nation='" + nation + '\'' +
                ", indivFolk='" + indivFolk + '\'' +
                ", indivBrtPlace='" + indivBrtPlace + '\'' +
                ", indivHouhRegAdd='" + indivHouhRegAdd + '\'' +
                ", indivDtOfBirth='" + indivDtOfBirth + '\'' +
                ", indivPolSt='" + indivPolSt + '\'' +
                ", indivEdt='" + indivEdt + '\'' +
                ", indivDgr='" + indivDgr + '\'' +
                ", marStatus='" + marStatus + '\'' +
                ", healthStatus='" + healthStatus + '\'' +
                ", comInitLoanDate='" + comInitLoanDate + '\'' +
                ", cdtLvl='" + cdtLvl + '\'' +
                ", pcode='" + pcode + '\'' +
                ", regionalism='" + regionalism + '\'' +
                ", regionName='" + regionName + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", faxCode='" + faxCode + '\'' +
                ", email='" + email + '\'' +
                ", indivRsdAddr='" + indivRsdAddr + '\'' +
                ", indivZipCode='" + indivZipCode + '\'' +
                ", indivRsdSt='" + indivRsdSt + '\'' +
                ", resiTime='" + resiTime + '\'' +
                ", employeeStatus='" + employeeStatus + '\'' +
                ", occu='" + occu + '\'' +
                ", unitName='" + unitName + '\'' +
                ", indivComTyp='" + indivComTyp + '\'' +
                ", indivComTrade='" + indivComTrade + '\'' +
                ", unitAddr='" + unitAddr + '\'' +
                ", unitPhn='" + unitPhn + '\'' +
                ", unitDate='" + unitDate + '\'' +
                ", jobTtl='" + jobTtl + '\'' +
                ", indivCrtfctn='" + indivCrtfctn + '\'' +
                ", earningCurType='" + earningCurType + '\'' +
                ", indivYearn='" + indivYearn + '\'' +
                ", indivMarBaby='" + indivMarBaby + '\'' +
                ", familyYearn=" + familyYearn +
                ", familyPhone='" + familyPhone + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", cusStatus='" + cusStatus + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", legalAddr='" + legalAddr + '\'' +
                ", indivSocScr='" + indivSocScr + '\'' +
                ", indivComFldName='" + indivComFldName + '\'' +
                ", cusType='" + cusType + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                '}';
    }
}
