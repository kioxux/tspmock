package cn.com.yusys.yusp.dto.server.cmiscus0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：客户销售总额查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0005RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    @JsonProperty(value = "statLastYearAmt")
    private BigDecimal statLastYearAmt;//上年度销售总额
    @JsonProperty(value = "statCurYearAmt")
    private BigDecimal statCurYearAmt;//当年销售总额
    @JsonProperty(value = "statYearAmtMax")
    private BigDecimal statYearAmtMax;//上年度销售总额和当年销售总额较大值

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BigDecimal getStatLastYearAmt() {
        return statLastYearAmt;
    }

    public void setStatLastYearAmt(BigDecimal statLastYearAmt) {
        this.statLastYearAmt = statLastYearAmt;
    }

    public BigDecimal getStatCurYearAmt() {
        return statCurYearAmt;
    }

    public void setStatCurYearAmt(BigDecimal statCurYearAmt) {
        this.statCurYearAmt = statCurYearAmt;
    }

    public BigDecimal getStatYearAmtMax() {
        return statYearAmtMax;
    }

    public void setStatYearAmtMax(BigDecimal statYearAmtMax) {
        this.statYearAmtMax = statYearAmtMax;
    }

    @Override
    public String toString() {
        return "CmisCus0005RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", statLastYearAmt=" + statLastYearAmt +
                ", statCurYearAmt=" + statCurYearAmt +
                ", statYearAmtMax=" + statYearAmtMax +
                '}';
    }
}