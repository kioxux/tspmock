package cn.com.yusys.yusp.dto.server.xdkh0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：农拍档接受白名单维护
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0015DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号
    @JsonProperty(value = "actOperAddr")
    private String actOperAddr;//经营地址
    @JsonProperty(value = "mobile")
    private String mobile;//手机号码
    @JsonProperty(value = "sex")
    private String sex;//性别
    @JsonProperty(value = "indivEdt")
    private String indivEdt;//学历
    @JsonProperty(value = "isHavingChild")
    private String isHavingChild;//是否有子女
    @JsonProperty(value = "indivMarSt")
    private String indivMarSt;//婚姻状况
    @JsonProperty(value = "spouseName")
    private String spouseName;//配偶姓名
    @JsonProperty(value = "spouseMobile")
    private String spouseMobile;//配偶手机
    @JsonProperty(value = "spouseCertNo")
    private String spouseCertNo;//配偶身份证号码
    @JsonProperty(value = "operYear")
    private String operYear;//经营年限
    @JsonProperty(value = "supTradeCode")
    private String supTradeCode;//一级行业代码
    @JsonProperty(value = "supTradeName")
    private String supTradeName;//一级行业名称
    @JsonProperty(value = "detailTradeCode")
    private String detailTradeCode;//细分行业代码
    @JsonProperty(value = "detailTradeName")
    private String detailTradeName;//细分行业名称
    @JsonProperty(value = "curtBreedingScale")
    private String curtBreedingScale;//当前种养殖规模
    @JsonProperty(value = "saleAmtYear")
    private String saleAmtYear;//年销售收入
    @JsonProperty(value = "profitYear")
    private String profitYear;//年利润
    @JsonProperty(value = "recomOrgName")
    private String recomOrgName;//推荐机构名称
    @JsonProperty(value = "urgentLinkmanName")
    private String urgentLinkmanName;//紧急联系人姓名
    @JsonProperty(value = "urgentLinkmanPhone")
    private String urgentLinkmanPhone;//紧急联系人电话
    @JsonProperty(value = "sideIndgtChnl")
    private String sideIndgtChnl;//侧面调查渠道
    @JsonProperty(value = "sideIndgtPhone")
    private String sideIndgtPhone;//侧面调查电话
    @JsonProperty(value = "sideIndgtMemo")
    private String sideIndgtMemo;//侧面调查备注
    @JsonProperty(value = "isAgri")
    private String isAgri;//是否农户
    @JsonProperty(value = "isOperNormal")
    private String isOperNormal;//是否经营正常
    @JsonProperty(value = "coopYearLmt")
    private String coopYearLmt;//合作年限
    @JsonProperty(value = "tranAmtYear")
    private String tranAmtYear;//年交易流水
    @JsonProperty(value = "chnlSuggestLmt")
    private String chnlSuggestLmt;//渠道建议额度（元）
    @JsonProperty(value = "cusCdtEvlu")
    private String cusCdtEvlu;//客户信用评价
    @JsonProperty(value = "recomTime")
    private String recomTime;//推荐时间
    @JsonProperty(value = "recomMemo")
    private String recomMemo;//推荐备注

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getActOperAddr() {
        return actOperAddr;
    }

    public void setActOperAddr(String actOperAddr) {
        this.actOperAddr = actOperAddr;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public String getIsHavingChild() {
        return isHavingChild;
    }

    public void setIsHavingChild(String isHavingChild) {
        this.isHavingChild = isHavingChild;
    }

    public String getIndivMarSt() {
        return indivMarSt;
    }

    public void setIndivMarSt(String indivMarSt) {
        this.indivMarSt = indivMarSt;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getSpouseMobile() {
        return spouseMobile;
    }

    public void setSpouseMobile(String spouseMobile) {
        this.spouseMobile = spouseMobile;
    }

    public String getSpouseCertNo() {
        return spouseCertNo;
    }

    public void setSpouseCertNo(String spouseCertNo) {
        this.spouseCertNo = spouseCertNo;
    }

    public String getOperYear() {
        return operYear;
    }

    public void setOperYear(String operYear) {
        this.operYear = operYear;
    }

    public String getSupTradeCode() {
        return supTradeCode;
    }

    public void setSupTradeCode(String supTradeCode) {
        this.supTradeCode = supTradeCode;
    }

    public String getSupTradeName() {
        return supTradeName;
    }

    public void setSupTradeName(String supTradeName) {
        this.supTradeName = supTradeName;
    }

    public String getDetailTradeCode() {
        return detailTradeCode;
    }

    public void setDetailTradeCode(String detailTradeCode) {
        this.detailTradeCode = detailTradeCode;
    }

    public String getDetailTradeName() {
        return detailTradeName;
    }

    public void setDetailTradeName(String detailTradeName) {
        this.detailTradeName = detailTradeName;
    }

    public String getCurtBreedingScale() {
        return curtBreedingScale;
    }

    public void setCurtBreedingScale(String curtBreedingScale) {
        this.curtBreedingScale = curtBreedingScale;
    }

    public String getSaleAmtYear() {
        return saleAmtYear;
    }

    public void setSaleAmtYear(String saleAmtYear) {
        this.saleAmtYear = saleAmtYear;
    }

    public String getProfitYear() {
        return profitYear;
    }

    public void setProfitYear(String profitYear) {
        this.profitYear = profitYear;
    }

    public String getRecomOrgName() {
        return recomOrgName;
    }

    public void setRecomOrgName(String recomOrgName) {
        this.recomOrgName = recomOrgName;
    }

    public String getUrgentLinkmanName() {
        return urgentLinkmanName;
    }

    public void setUrgentLinkmanName(String urgentLinkmanName) {
        this.urgentLinkmanName = urgentLinkmanName;
    }

    public String getUrgentLinkmanPhone() {
        return urgentLinkmanPhone;
    }

    public void setUrgentLinkmanPhone(String urgentLinkmanPhone) {
        this.urgentLinkmanPhone = urgentLinkmanPhone;
    }

    public String getSideIndgtChnl() {
        return sideIndgtChnl;
    }

    public void setSideIndgtChnl(String sideIndgtChnl) {
        this.sideIndgtChnl = sideIndgtChnl;
    }

    public String getSideIndgtPhone() {
        return sideIndgtPhone;
    }

    public void setSideIndgtPhone(String sideIndgtPhone) {
        this.sideIndgtPhone = sideIndgtPhone;
    }

    public String getSideIndgtMemo() {
        return sideIndgtMemo;
    }

    public void setSideIndgtMemo(String sideIndgtMemo) {
        this.sideIndgtMemo = sideIndgtMemo;
    }

    public String getIsAgri() {
        return isAgri;
    }

    public void setIsAgri(String isAgri) {
        this.isAgri = isAgri;
    }

    public String getIsOperNormal() {
        return isOperNormal;
    }

    public void setIsOperNormal(String isOperNormal) {
        this.isOperNormal = isOperNormal;
    }

    public String getCoopYearLmt() {
        return coopYearLmt;
    }

    public void setCoopYearLmt(String coopYearLmt) {
        this.coopYearLmt = coopYearLmt;
    }

    public String getTranAmtYear() {
        return tranAmtYear;
    }

    public void setTranAmtYear(String tranAmtYear) {
        this.tranAmtYear = tranAmtYear;
    }

    public String getChnlSuggestLmt() {
        return chnlSuggestLmt;
    }

    public void setChnlSuggestLmt(String chnlSuggestLmt) {
        this.chnlSuggestLmt = chnlSuggestLmt;
    }

    public String getCusCdtEvlu() {
        return cusCdtEvlu;
    }

    public void setCusCdtEvlu(String cusCdtEvlu) {
        this.cusCdtEvlu = cusCdtEvlu;
    }

    public String getRecomTime() {
        return recomTime;
    }

    public void setRecomTime(String recomTime) {
        this.recomTime = recomTime;
    }

    public String getRecomMemo() {
        return recomMemo;
    }

    public void setRecomMemo(String recomMemo) {
        this.recomMemo = recomMemo;
    }

    @Override
    public String toString() {
        return "Xdkh0015DataReqDto{" +
                "cusName='" + cusName + '\'' +
                ", certNo='" + certNo + '\'' +
                ", actOperAddr='" + actOperAddr + '\'' +
                ", mobile='" + mobile + '\'' +
                ", sex='" + sex + '\'' +
                ", indivEdt='" + indivEdt + '\'' +
                ", isHavingChild='" + isHavingChild + '\'' +
                ", indivMarSt='" + indivMarSt + '\'' +
                ", spouseName='" + spouseName + '\'' +
                ", spouseMobile='" + spouseMobile + '\'' +
                ", spouseCertNo='" + spouseCertNo + '\'' +
                ", operYear='" + operYear + '\'' +
                ", supTradeCode='" + supTradeCode + '\'' +
                ", supTradeName='" + supTradeName + '\'' +
                ", detailTradeCode='" + detailTradeCode + '\'' +
                ", detailTradeName='" + detailTradeName + '\'' +
                ", curtBreedingScale='" + curtBreedingScale + '\'' +
                ", saleAmtYear='" + saleAmtYear + '\'' +
                ", profitYear='" + profitYear + '\'' +
                ", recomOrgName='" + recomOrgName + '\'' +
                ", urgentLinkmanName='" + urgentLinkmanName + '\'' +
                ", urgentLinkmanPhone='" + urgentLinkmanPhone + '\'' +
                ", sideIndgtChnl='" + sideIndgtChnl + '\'' +
                ", sideIndgtPhone='" + sideIndgtPhone + '\'' +
                ", sideIndgtMemo='" + sideIndgtMemo + '\'' +
                ", isAgri='" + isAgri + '\'' +
                ", isOperNormal='" + isOperNormal + '\'' +
                ", coopYearLmt='" + coopYearLmt + '\'' +
                ", tranAmtYear='" + tranAmtYear + '\'' +
                ", chnlSuggestLmt='" + chnlSuggestLmt + '\'' +
                ", cusCdtEvlu='" + cusCdtEvlu + '\'' +
                ", recomTime='" + recomTime + '\'' +
                ", recomMemo='" + recomMemo + '\'' +
                '}';
    }
}
