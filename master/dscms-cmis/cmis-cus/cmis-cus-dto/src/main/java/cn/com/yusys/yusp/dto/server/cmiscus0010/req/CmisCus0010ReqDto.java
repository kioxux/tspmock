package cn.com.yusys.yusp.dto.server.cmiscus0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据客户编号,证件类型，证件号码查询同业客户基本信息
 *
 * @author 蒋云龙
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0010ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号

    @JsonProperty(value = "certType")
    private String certType;//证件类型

    @JsonProperty(value = "certCode")
    private String certCode;//证件号码

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    @Override
    public String toString() {
        return "CmisCus0010ReqDto{" +
                "cusId='" + cusId + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                '}';
    }
}
