package cn.com.yusys.yusp.dto.server.cmiscus0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：更新客户单位信息表
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0015ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")//客户编号
    private String cusId;
    @JsonProperty(value = "indivComTrade")//客户编号
    private String indivComTrade;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIndivComTrade() {
        return indivComTrade;
    }

    public void setIndivComTrade(String indivComTrade) {
        this.indivComTrade = indivComTrade;
    }

    @Override
    public String toString() {
        return "CmisCus0015ReqDto{" +
                "cusId='" + cusId + '\'' +
                "indivComTrade='" + indivComTrade + '\'' +
                '}';
    }
}
