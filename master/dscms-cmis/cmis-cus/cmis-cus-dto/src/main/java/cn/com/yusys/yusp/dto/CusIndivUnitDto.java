package cn.com.yusys.yusp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 响应Dto：查询个人客户基本信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CusIndivUnitDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 客户编号 **/
    private String cusId;

    /** 是否当前单位 STD_ZB_YES_NO **/
    private String isCurtUnit;

    /** 职业 STD_ZB_OCC **/
    private String occ;

    /** 自由职业说明 **/
    private String occDesc;

    /** 经营企业名称/工作单位 **/
    private String unitName;

    /** 所在部门 **/
    private String ubietyDept;

    /** 单位性质 **/
    private String indivComTyp;

    /** 单位所属行业 **/
    private String indivComTrade;

    /** 职务 STD_ZB_JOB_TTL **/
    private String jobTtl;

    /** 职称 STD_ZB_TITLE **/
    private String indivCrtfctn;

    /** 年收入(元) **/
    private java.math.BigDecimal yScore;

    /** 单位地址 **/
    private String unitAddr;

    /** 单位地址街道/路 **/
    private String unitStreet;

    /** 单位邮政编码 **/
    private String unitZipCode;

    /** 单位电话 **/
    private String unitPhn;

    /** 单位传真 **/
    private String unitFax;

    /** 单位联系人 **/
    private String unitCntName;

    /** 参加工作年份 **/
    private String workDate;

    /** 本单位参加工作日期 **/
    private String unitDate;

    /** 本单位工作结束日期 **/
    private String unitEndDate;

    /** 本行业参加工作日期  **/
    private String tradeDate;

    /** 备注 **/
    private String remark;

    /** 个人工作履历标识 **/
    private String prsnWorkId;

    /** 操作类型  STD_ZB_OPR_TYPE **/
    private String oprType;

    /** 雇佣状态 **/
    private String employeeStatus;

    /** 营业执照号码 **/
    private String regCde;

    /** 经营状况 **/
    private String operStatus;

    /** 经营企业统一社会信用代码 **/
    private String unifyCreditCode;

    /** 收入币种 **/
    private String earningCurType;

    /** 家庭年收入(元) **/
    private java.math.BigDecimal familyYScore;

    /** 个人年收入 **/
    private String indivYearn;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 更新人 **/
    private String updId;

    /** 更新机构 **/
    private String updBrId;

    /** 更新日期 **/
    private String updDate;

    /** 创建日期 **/
    private Date createTime;

    /** 更新日期 **/
    private Date updateTime;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getIsCurtUnit() {
        return isCurtUnit;
    }

    public void setIsCurtUnit(String isCurtUnit) {
        this.isCurtUnit = isCurtUnit;
    }

    public String getOcc() {
        return occ;
    }

    public void setOcc(String occ) {
        this.occ = occ;
    }

    public String getOccDesc() {
        return occDesc;
    }

    public void setOccDesc(String occDesc) {
        this.occDesc = occDesc;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUbietyDept() {
        return ubietyDept;
    }

    public void setUbietyDept(String ubietyDept) {
        this.ubietyDept = ubietyDept;
    }

    public String getIndivComTyp() {
        return indivComTyp;
    }

    public void setIndivComTyp(String indivComTyp) {
        this.indivComTyp = indivComTyp;
    }

    public String getIndivComTrade() {
        return indivComTrade;
    }

    public void setIndivComTrade(String indivComTrade) {
        this.indivComTrade = indivComTrade;
    }

    public String getJobTtl() {
        return jobTtl;
    }

    public void setJobTtl(String jobTtl) {
        this.jobTtl = jobTtl;
    }

    public String getIndivCrtfctn() {
        return indivCrtfctn;
    }

    public void setIndivCrtfctn(String indivCrtfctn) {
        this.indivCrtfctn = indivCrtfctn;
    }

    public BigDecimal getyScore() {
        return yScore;
    }

    public void setyScore(BigDecimal yScore) {
        this.yScore = yScore;
    }

    public String getUnitAddr() {
        return unitAddr;
    }

    public void setUnitAddr(String unitAddr) {
        this.unitAddr = unitAddr;
    }

    public String getUnitStreet() {
        return unitStreet;
    }

    public void setUnitStreet(String unitStreet) {
        this.unitStreet = unitStreet;
    }

    public String getUnitZipCode() {
        return unitZipCode;
    }

    public void setUnitZipCode(String unitZipCode) {
        this.unitZipCode = unitZipCode;
    }

    public String getUnitPhn() {
        return unitPhn;
    }

    public void setUnitPhn(String unitPhn) {
        this.unitPhn = unitPhn;
    }

    public String getUnitFax() {
        return unitFax;
    }

    public void setUnitFax(String unitFax) {
        this.unitFax = unitFax;
    }

    public String getUnitCntName() {
        return unitCntName;
    }

    public void setUnitCntName(String unitCntName) {
        this.unitCntName = unitCntName;
    }

    public String getWorkDate() {
        return workDate;
    }

    public void setWorkDate(String workDate) {
        this.workDate = workDate;
    }

    public String getUnitDate() {
        return unitDate;
    }

    public void setUnitDate(String unitDate) {
        this.unitDate = unitDate;
    }

    public String getUnitEndDate() {
        return unitEndDate;
    }

    public void setUnitEndDate(String unitEndDate) {
        this.unitEndDate = unitEndDate;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPrsnWorkId() {
        return prsnWorkId;
    }

    public void setPrsnWorkId(String prsnWorkId) {
        this.prsnWorkId = prsnWorkId;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(String employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public String getRegCde() {
        return regCde;
    }

    public void setRegCde(String regCde) {
        this.regCde = regCde;
    }

    public String getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(String operStatus) {
        this.operStatus = operStatus;
    }

    public String getUnifyCreditCode() {
        return unifyCreditCode;
    }

    public void setUnifyCreditCode(String unifyCreditCode) {
        this.unifyCreditCode = unifyCreditCode;
    }

    public String getEarningCurType() {
        return earningCurType;
    }

    public void setEarningCurType(String earningCurType) {
        this.earningCurType = earningCurType;
    }

    public BigDecimal getFamilyYScore() {
        return familyYScore;
    }

    public void setFamilyYScore(BigDecimal familyYScore) {
        this.familyYScore = familyYScore;
    }

    public String getIndivYearn() {
        return indivYearn;
    }

    public void setIndivYearn(String indivYearn) {
        this.indivYearn = indivYearn;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "CusIndivUnitDto{" +
                "pkId='" + pkId + '\'' +
                ", cusId='" + cusId + '\'' +
                ", isCurtUnit='" + isCurtUnit + '\'' +
                ", occ='" + occ + '\'' +
                ", occDesc='" + occDesc + '\'' +
                ", unitName='" + unitName + '\'' +
                ", ubietyDept='" + ubietyDept + '\'' +
                ", indivComTyp='" + indivComTyp + '\'' +
                ", indivComTrade='" + indivComTrade + '\'' +
                ", jobTtl='" + jobTtl + '\'' +
                ", indivCrtfctn='" + indivCrtfctn + '\'' +
                ", yScore=" + yScore +
                ", unitAddr='" + unitAddr + '\'' +
                ", unitStreet='" + unitStreet + '\'' +
                ", unitZipCode='" + unitZipCode + '\'' +
                ", unitPhn='" + unitPhn + '\'' +
                ", unitFax='" + unitFax + '\'' +
                ", unitCntName='" + unitCntName + '\'' +
                ", workDate='" + workDate + '\'' +
                ", unitDate='" + unitDate + '\'' +
                ", unitEndDate='" + unitEndDate + '\'' +
                ", tradeDate='" + tradeDate + '\'' +
                ", remark='" + remark + '\'' +
                ", prsnWorkId='" + prsnWorkId + '\'' +
                ", oprType='" + oprType + '\'' +
                ", employeeStatus='" + employeeStatus + '\'' +
                ", regCde='" + regCde + '\'' +
                ", operStatus='" + operStatus + '\'' +
                ", unifyCreditCode='" + unifyCreditCode + '\'' +
                ", earningCurType='" + earningCurType + '\'' +
                ", familyYScore=" + familyYScore +
                ", indivYearn='" + indivYearn + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}

