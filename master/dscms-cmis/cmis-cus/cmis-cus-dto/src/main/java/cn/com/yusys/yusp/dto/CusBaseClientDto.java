package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusBase
 * @类描述: cus_base数据实体类
 * @功能描述: 
 * @创建人: LQC
 * @创建时间: 2021-04-21 14:55:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusBaseClientDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 客户简称 **/
	private String cusShortName;
	
	/** 证件类型 STD_ZB_CERT_TYP **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 开户日期 **/
	private String openDate;
	
	/** 开户类型 STD_ZB_OPEN_TYP **/
	private String openType;
	
	/** 客户大类 **/
	private String cusCatalog;
	
	/** 所属条线 STD_ZB_BIZ_BELG **/
	private String belgLine;
	
	/** 信用等级 STD_ZB_CREDIT_GRADE **/
	private String cusCrdGrade;
	
	/** 信用评定到期日期 **/
	private String cusCrdDt;
	
	/** 客户状态 STD_ZB_CUS_ST **/
	private String cusState;

    /** 客户管护经理 **/
    private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/** 客户管护经理名称 **/
	private String managerIdName;

	/** 主管机构名称 **/
	private String managerBrIdName;

	/** 客户分类 **/
	private String cusRankCls;

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusShortName
	 */
	public void setCusShortName(String cusShortName) {
		this.cusShortName = cusShortName == null ? null : cusShortName.trim();
	}
	
    /**
     * @return CusShortName
     */	
	public String getCusShortName() {
		return this.cusShortName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param openDate
	 */
	public void setOpenDate(String openDate) {
		this.openDate = openDate == null ? null : openDate.trim();
	}
	
    /**
     * @return OpenDate
     */	
	public String getOpenDate() {
		return this.openDate;
	}
	
	/**
	 * @param openType
	 */
	public void setOpenType(String openType) {
		this.openType = openType == null ? null : openType.trim();
	}
	
    /**
     * @return OpenType
     */	
	public String getOpenType() {
		return this.openType;
	}
	
	/**
	 * @param cusCatalog
	 */
	public void setCusCatalog(String cusCatalog) {
		this.cusCatalog = cusCatalog == null ? null : cusCatalog.trim();
	}
	
    /**
     * @return CusCatalog
     */	
	public String getCusCatalog() {
		return this.cusCatalog;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine == null ? null : belgLine.trim();
	}
	
    /**
     * @return BelgLine
     */	
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param cusCrdGrade
	 */
	public void setCusCrdGrade(String cusCrdGrade) {
		this.cusCrdGrade = cusCrdGrade == null ? null : cusCrdGrade.trim();
	}
	
    /**
     * @return CusCrdGrade
     */	
	public String getCusCrdGrade() {
		return this.cusCrdGrade;
	}
	
	/**
	 * @param cusCrdDt
	 */
	public void setCusCrdDt(String cusCrdDt) {
		this.cusCrdDt = cusCrdDt == null ? null : cusCrdDt.trim();
	}
	
    /**
     * @return CusCrdDt
     */	
	public String getCusCrdDt() {
		return this.cusCrdDt;
	}
	
	/**
	 * @param cusState
	 */
	public void setCusState(String cusState) {
		this.cusState = cusState == null ? null : cusState.trim();
	}
	
    /**
     * @return CusState
     */	
	public String getCusState() {
		return this.cusState;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return managerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId == null ? null : managerId.trim();;
    }

    /**
     * @return ManagerId
     */
    public String getManagerId() {
        return this.managerId;
    }

	public String getManagerIdName() {
		return managerIdName;
	}

	public void setManagerIdName(String managerIdName) {
		this.managerIdName = managerIdName;
	}

	public String getManagerBrIdName() {
		return managerBrIdName;
	}

	public void setManagerBrIdName(String managerBrIdName) {
		this.managerBrIdName = managerBrIdName;
	}

	public String getCusRankCls() {
		return cusRankCls;
	}

	public void setCusRankCls(String cusRankCls) {
		this.cusRankCls = cusRankCls;
	}

	@Override
	public String toString() {
		return "CusBaseClientDto{" +
				"cusId='" + cusId + '\'' +
				", cusName='" + cusName + '\'' +
				", cusShortName='" + cusShortName + '\'' +
				", certType='" + certType + '\'' +
				", certCode='" + certCode + '\'' +
				", openDate='" + openDate + '\'' +
				", openType='" + openType + '\'' +
				", cusCatalog='" + cusCatalog + '\'' +
				", belgLine='" + belgLine + '\'' +
				", cusCrdGrade='" + cusCrdGrade + '\'' +
				", cusCrdDt='" + cusCrdDt + '\'' +
				", cusState='" + cusState + '\'' +
				", managerId='" + managerId + '\'' +
				", managerBrId='" + managerBrId + '\'' +
				", inputId='" + inputId + '\'' +
				", inputBrId='" + inputBrId + '\'' +
				", inputDate='" + inputDate + '\'' +
				", updId='" + updId + '\'' +
				", updBrId='" + updBrId + '\'' +
				", updDate='" + updDate + '\'' +
				", oprType='" + oprType + '\'' +
				", managerIdName='" + managerIdName + '\'' +
				", managerBrIdName='" + managerBrIdName + '\'' +
				", cusRankCls='" + cusRankCls + '\'' +
				'}';
	}
}