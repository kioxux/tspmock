package cn.com.yusys.yusp.dto.server.xdkh0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：优企贷、优农贷客户基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0024DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号


    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    @Override
    public String toString() {
        return "Xdkh0024DataReqDto{" +
                ", cert_code='" + cert_code + '\'' +
                '}';
    }
}
