package cn.com.yusys.yusp.dto.server.cmiscus0012.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pkId")//主键
    private String pkId;
    @JsonProperty(value = "cusId")//客户编号
    private String cusId;
    @JsonProperty(value = "cusIdRel")//关联客户编号
    private String cusIdRel;
    @JsonProperty(value = "mrgType")//高管证件类型
    private String mrgType;
    @JsonProperty(value = "mrgCertCode")//高管证件号码
    private String mrgCertCode;
    @JsonProperty(value = "mrgName")//高管姓名
    private String mrgName;
    @JsonProperty(value = "mrgSex")//性别
    private String mrgSex;
    @JsonProperty(value = "mrgBday")//出生日期
    private String mrgBday;
    @JsonProperty(value = "mrgOcc")//高管职业
    private String mrgOcc;
    @JsonProperty(value = "mrgDuty")//高管职务
    private String mrgDuty;
    @JsonProperty(value = "mrgCrtf")//高管职称
    private String mrgCrtf;
    @JsonProperty(value = "mrgEdt")//高管学历
    private String mrgEdt;
    @JsonProperty(value = "mrgDgr")//高管学位
    private String mrgDgr;
    @JsonProperty(value = "mrgPhone")//联系电话
    private String mrgPhone;
    @JsonProperty(value = "signInitDate")//签字样本开始日期
    private String signInitDate;
    @JsonProperty(value = "signEndDate")//签字样本到期日期
    private String signEndDate;
    @JsonProperty(value = "accreditInitDate")//授权书开始日期
    private String accreditInitDate;
    @JsonProperty(value = "accreditEndDate")//授权书到期日期
    private String accreditEndDate;
    @JsonProperty(value = "resume")//工作简历
    private String resume;
    @JsonProperty(value = "remark")//备注
    private String remark;
    @JsonProperty(value = "inputId")//登记人
    private String inputId;
    @JsonProperty(value = "inputBrId")//登记机构
    private String inputBrId;
    @JsonProperty(value = "inputDate")//登记日期
    private String inputDate;
    @JsonProperty(value = "updId")//最后修改人
    private String updId;
    @JsonProperty(value = "updBrId")//最后修改机构
    private String updBrId;
    @JsonProperty(value = "updDate")//最后修改日期
    private String updDate;
    @JsonProperty(value = "oprType")//操作类型
    private String oprType;
    @JsonProperty(value = "country")//国别
    private String country;
    @JsonProperty(value = "certIdate")//证件到期日
    private String certIdate;
    @JsonProperty(value = "fjobDate")//从业日期
    private String fjobDate;
    @JsonProperty(value = "indivNas")//个人净资产
    private Double indivNas;
    @JsonProperty(value = "localResiStatus")//本地居住状况
    private String localResiStatus;
    @JsonProperty(value = "resiAddr")//居住地址
    private String resiAddr;


    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getPkId() {
        return this.pkId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusId() {
        return this.cusId;
    }

    public String getCusIdRel() {
        return cusIdRel;
    }

    public void setCusIdRel(String cusIdRel) {
        this.cusIdRel = cusIdRel;
    }

    public void setMrgType(String mrgType) {
        this.mrgType = mrgType;
    }

    public String getMrgType() {
        return this.mrgType;
    }

    public void setMrgCertCode(String mrgCertCode) {
        this.mrgCertCode = mrgCertCode;
    }

    public String getMrgCertCode() {
        return this.mrgCertCode;
    }

    public void setMrgName(String mrgName) {
        this.mrgName = mrgName;
    }

    public String getMrgName() {
        return this.mrgName;
    }

    public void setMrgSex(String mrgSex) {
        this.mrgSex = mrgSex;
    }

    public String getMrgSex() {
        return this.mrgSex;
    }

    public void setMrgBday(String mrgBday) {
        this.mrgBday = mrgBday;
    }

    public String getMrgBday() {
        return this.mrgBday;
    }

    public void setMrgOcc(String mrgOcc) {
        this.mrgOcc = mrgOcc;
    }

    public String getMrgOcc() {
        return this.mrgOcc;
    }

    public void setMrgDuty(String mrgDuty) {
        this.mrgDuty = mrgDuty;
    }

    public String getMrgDuty() {
        return this.mrgDuty;
    }

    public void setMrgCrtf(String mrgCrtf) {
        this.mrgCrtf = mrgCrtf;
    }

    public String getMrgCrtf() {
        return this.mrgCrtf;
    }

    public void setMrgEdt(String mrgEdt) {
        this.mrgEdt = mrgEdt;
    }

    public String getMrgEdt() {
        return this.mrgEdt;
    }

    public void setMrgDgr(String mrgDgr) {
        this.mrgDgr = mrgDgr;
    }

    public String getMrgDgr() {
        return this.mrgDgr;
    }

    public void setMrgPhone(String mrgPhone) {
        this.mrgPhone = mrgPhone;
    }

    public String getMrgPhone() {
        return this.mrgPhone;
    }

    public void setSignInitDate(String signInitDate) {
        this.signInitDate = signInitDate;
    }

    public String getSignInitDate() {
        return this.signInitDate;
    }

    public void setSignEndDate(String signEndDate) {
        this.signEndDate = signEndDate;
    }

    public String getSignEndDate() {
        return this.signEndDate;
    }

    public void setAccreditInitDate(String accreditInitDate) {
        this.accreditInitDate = accreditInitDate;
    }

    public String getAccreditInitDate() {
        return this.accreditInitDate;
    }

    public void setAccreditEndDate(String accreditEndDate) {
        this.accreditEndDate = accreditEndDate;
    }

    public String getAccreditEndDate() {
        return this.accreditEndDate;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getResume() {
        return this.resume;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputId() {
        return this.inputId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getOprType() {
        return this.oprType;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCertIdate(String certIdate) {
        this.certIdate = certIdate;
    }

    public String getCertIdate() {
        return this.certIdate;
    }

    public void setFjobDate(String fjobDate) {
        this.fjobDate = fjobDate;
    }

    public String getFjobDate() {
        return this.fjobDate;
    }

    public void setIndivNas(Double indivNas) {
        this.indivNas = indivNas;
    }

    public Double getIndivNas() {
        return this.indivNas;
    }

    public void setLocalResiStatus(String localResiStatus) {
        this.localResiStatus = localResiStatus;
    }

    public String getLocalResiStatus() {
        return this.localResiStatus;
    }

    public void setResiAddr(String resiAddr) {
        this.resiAddr = resiAddr;
    }

    public String getResiAddr() {
        return this.resiAddr;
    }

    @Override
    public String toString() {
        return "List{" +
                "pkId='" + pkId + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusIdRel='" + cusIdRel + '\'' +
                ", mrgType='" + mrgType + '\'' +
                ", mrgCertCode='" + mrgCertCode + '\'' +
                ", mrgName='" + mrgName + '\'' +
                ", mrgSex='" + mrgSex + '\'' +
                ", mrgBday='" + mrgBday + '\'' +
                ", mrgOcc='" + mrgOcc + '\'' +
                ", mrgDuty='" + mrgDuty + '\'' +
                ", mrgCrtf='" + mrgCrtf + '\'' +
                ", mrgEdt='" + mrgEdt + '\'' +
                ", mrgDgr='" + mrgDgr + '\'' +
                ", mrgPhone='" + mrgPhone + '\'' +
                ", signInitDate='" + signInitDate + '\'' +
                ", signEndDate='" + signEndDate + '\'' +
                ", accreditInitDate='" + accreditInitDate + '\'' +
                ", accreditEndDate='" + accreditEndDate + '\'' +
                ", resume='" + resume + '\'' +
                ", remark='" + remark + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", oprType='" + oprType + '\'' +
                ", country='" + country + '\'' +
                ", certIdate='" + certIdate + '\'' +
                ", fjobDate='" + fjobDate + '\'' +
                ", indivNas=" + indivNas +
                ", localResiStatus='" + localResiStatus + '\'' +
                ", resiAddr='" + resiAddr + '\'' +
                '}';
    }
}
