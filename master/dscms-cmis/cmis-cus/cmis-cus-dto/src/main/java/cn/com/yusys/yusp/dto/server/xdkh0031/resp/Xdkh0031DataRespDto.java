package cn.com.yusys.yusp.dto.server.xdkh0031.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：同业客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0031DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusIdCore")
    private String cusIdCore;//核心客户号
    @JsonProperty(value = "sameOrgNo")
    private String sameOrgNo;//同业机构（行）号
    @JsonProperty(value = "sameOrgCnname")
    private String sameOrgCnname;//同业机构（行）号名称
    @JsonProperty(value = "sameOrgType")
    private String sameOrgType;//同业机构（行）类型
    @JsonProperty(value = "regStateCode")
    private String regStateCode;//注册地行政区划代码
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理编号
    @JsonProperty(value = "managerorgId")
    private String managerorgId;//主管客户经理所属机构编号

    public String getCusIdCore() {
        return cusIdCore;
    }

    public void setCusIdCore(String cusIdCore) {
        this.cusIdCore = cusIdCore;
    }

    public String getSameOrgNo() {
        return sameOrgNo;
    }

    public void setSameOrgNo(String sameOrgNo) {
        this.sameOrgNo = sameOrgNo;
    }

    public String getSameOrgCnname() {
        return sameOrgCnname;
    }

    public void setSameOrgCnname(String sameOrgCnname) {
        this.sameOrgCnname = sameOrgCnname;
    }

    public String getSameOrgType() {
        return sameOrgType;
    }

    public void setSameOrgType(String sameOrgType) {
        this.sameOrgType = sameOrgType;
    }

    public String getRegStateCode() {
        return regStateCode;
    }

    public void setRegStateCode(String regStateCode) {
        this.regStateCode = regStateCode;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerorgId() {
        return managerorgId;
    }

    public void setManagerorgId(String managerorgId) {
        this.managerorgId = managerorgId;
    }

    @Override
    public String toString() {
        return "Xdkh0031DataRespDto{" +
                "cusIdCore='" + cusIdCore + '\'' +
                ", sameOrgNo='" + sameOrgNo + '\'' +
                ", sameOrgCnname='" + sameOrgCnname + '\'' +
                ", sameOrgType='" + sameOrgType + '\'' +
                ", regStateCode='" + regStateCode + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerorgId='" + managerorgId + '\'' +
                '}';
    }
}