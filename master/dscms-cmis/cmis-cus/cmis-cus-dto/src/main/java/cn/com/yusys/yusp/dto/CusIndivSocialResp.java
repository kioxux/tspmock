package cn.com.yusys.yusp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-cus-core模块
 * @类名称: CusIndivSocial
 * @类描述: cus_indiv_social数据实体类
 * @功能描述: 
 * @创建人: 茂茂
 * @创建时间: 2021-04-12 19:10:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@JsonPropertyOrder(alphabetic = true)
public class CusIndivSocialResp implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@JsonProperty(value = "pkId")
	private String pkId;
	
	/** 客户编号 **/
	@JsonProperty(value = "cusId")
	private String cusId;
	
	/** 是否家庭成员 **/
	@JsonProperty(value = "isFamilyMem")
	private String isFamilyMem;
	
	/** 与客户关系 **/
	@JsonProperty(value = "indivCusRel")
	private String indivCusRel;
	
	/** 证件类型 **/
	@JsonProperty(value = "certType")
	private String certType;
	
	/** 证件号码 **/
	@JsonProperty(value = "certCode")
	private String certCode;
	
	/** 姓名 **/
	@JsonProperty(value = "name")
	private String name;
	
	/** 关联客户编号 **/
	@JsonProperty(value = "correCusId")
	private String correCusId;
	
	/** 性别 **/
	@JsonProperty(value = "sex")
	private String sex;
	
	/** 年收入（元） **/
	@JsonProperty(value = "yearn")
	private java.math.BigDecimal yearn;
	
	/** 职业 **/
	@JsonProperty(value = "occu")
	private String occu;
	
	/** 职务 **/
	@JsonProperty(value = "duty")
	private String duty;
	
	/** 操作类型 **/
	@JsonProperty(value = "oprType")
	private String oprType;
	
	/** 家庭成员编号 **/
	@JsonProperty(value = "famCusId")
	private String famCusId;
	
	/** 备注 **/
	@JsonProperty(value = "remark")
	private String remark;
	
	/** 登记人 **/
	@JsonProperty(value = "inputId")
	private String inputId;
	
	/** 登记机构 **/
	@JsonProperty(value = "inputBrId")
	private String inputBrId;
	
	/** 登记日期 **/
	@JsonProperty(value = "inputDate")
	private String inputDate;
	
	/** 更新人 **/
	@JsonProperty(value = "updId")
	private String updId;
	
	/** 更新机构 **/
	@JsonProperty(value = "updBrId")
	private String updBrId;
	
	/** 更新日期 **/
	@JsonProperty(value = "updDate")
	private String updDate;
	
	/** 创建时间 **/
	@JsonProperty(value = "createTime")
	private Date createTime;

	/** 修改时间 **/
	@JsonProperty(value = "upddateTime")
	private Date upddateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}

    /**
     * @return PkId
     */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

    /**
     * @return CusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param isFamilyMem
	 */
	public void setIsFamilyMem(String isFamilyMem) {
		this.isFamilyMem = isFamilyMem == null ? null : isFamilyMem.trim();
	}

    /**
     * @return IsFamilyMem
     */
	public String getIsFamilyMem() {
		return this.isFamilyMem;
	}

	/**
	 * @param indivCusRel
	 */
	public void setIndivCusRel(String indivCusRel) {
		this.indivCusRel = indivCusRel == null ? null : indivCusRel.trim();
	}

    /**
     * @return IndivCusRel
     */
	public String getIndivCusRel() {
		return this.indivCusRel;
	}

	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}

    /**
     * @return CertType
     */
	public String getCertType() {
		return this.certType;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}

    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

    /**
     * @return Name
     */
	public String getName() {
		return this.name;
	}

	/**
	 * @param correCusId
	 */
	public void setCorreCusId(String correCusId) {
		this.correCusId = correCusId == null ? null : correCusId.trim();
	}

    /**
     * @return CorreCusId
     */
	public String getCorreCusId() {
		return this.correCusId;
	}

	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex == null ? null : sex.trim();
	}

    /**
     * @return Sex
     */
	public String getSex() {
		return this.sex;
	}

	/**
	 * @param yearn
	 */
	public void setYearn(java.math.BigDecimal yearn) {
		this.yearn = yearn;
	}

    /**
     * @return Yearn
     */
	public java.math.BigDecimal getYearn() {
		return this.yearn;
	}

	/**
	 * @param occu
	 */
	public void setOccu(String occu) {
		this.occu = occu == null ? null : occu.trim();
	}

    /**
     * @return Occu
     */
	public String getOccu() {
		return this.occu;
	}

	/**
	 * @param duty
	 */
	public void setDuty(String duty) {
		this.duty = duty == null ? null : duty.trim();
	}

    /**
     * @return Duty
     */
	public String getDuty() {
		return this.duty;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param famCusId
	 */
	public void setFamCusId(String famCusId) {
		this.famCusId = famCusId == null ? null : famCusId.trim();
	}

    /**
     * @return FamCusId
     */
	public String getFamCusId() {
		return this.famCusId;
	}

	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

    /**
     * @return Remark
     */
	public String getRemark() {
		return this.remark;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

    /**
     * @return InputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

    /**
     * @return InputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

    /**
     * @return InputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

    /**
     * @return UpdId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

    /**
     * @return UpdBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

    /**
     * @return UpdDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

    /**
     * @return CreateTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param upddateTime
	 */
	public void setUpddateTime(Date upddateTime) {
		this.upddateTime = upddateTime;
	}

    /**
     * @return UpddateTime
     */
	public Date getUpddateTime() {
		return this.upddateTime;
	}


}