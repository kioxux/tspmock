package cn.com.yusys.yusp.dto.server.cmiscus0025.resp;

import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：优农贷名单信息查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0025RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")//流水号
    private String serno;
    @JsonProperty(value = "appDate")//申请日期
    private String appDate;
    @JsonProperty(value = "cusId")//客户编号
    private String cusId;
    @JsonProperty(value = "cusName")//客户编号
    private String cusName;
    @JsonProperty(value = "certCode")//证件编号
    private String certCode;
    @JsonProperty(value = "mobileNo")//手机号码
    private String mobileNo;
    @JsonProperty(value = "sex")//性别
    private String sex;
    @JsonProperty(value = "edu")//学历
    private String edu;
    @JsonProperty(value = "isHaveChildren")//有无子女
    private String isHaveChildren;
    @JsonProperty(value = "resiType")//居住场所类型
    private String resiType;
    @JsonProperty(value = "localResiLmt")//本地居住年限
    private String localResiLmt;
    @JsonProperty(value = "localRegist")//本地户口
    private String localRegist;
    @JsonProperty(value = "operAddr")//经营地址
    private String operAddr;
    @JsonProperty(value = "operLmt")//经营年限
    private String operLmt;
    @JsonProperty(value = "marStatus")//婚姻状况
    private String marStatus;
    @JsonProperty(value = "spouseName")//配偶姓名
    private String spouseName;
    @JsonProperty(value = "spouseIdcardNo")//配偶身份证号码
    private String spouseIdcardNo;
    @JsonProperty(value = "spouseMobileNo")//配偶手机号码
    private String spouseMobileNo;
    @JsonProperty(value = "approveStatus")//审批状态
    private String approveStatus;
    @JsonProperty(value = "imageNo")//影像编号
    private String imageNo;
    @JsonProperty(value = "agriflag")//农户标示
    private String agriflag;
    @JsonProperty(value = "cusOperIsNormal")//客户经营是否正常
    private String cusOperIsNormal;
    @JsonProperty(value = "oneLevelTradeCode")//一级行业编号
    private String oneLevelTradeCode;
    @JsonProperty(value = "oneLevelTradeName")//一级行业名称
    private String oneLevelTradeName;
    @JsonProperty(value = "detailsTradeCode")//细分行业编号
    private String detailsTradeCode;
    @JsonProperty(value = "detailsTradeName")//细分行业名称
    private String detailsTradeName;
    @JsonProperty(value = "operScale")//经营规模
    private String operScale;
    @JsonProperty(value = "operScaleUnit")//经营规模单位
    private String operScaleUnit;
    @JsonProperty(value = "yaerSaleIncome")//年销售收入
    private String yaerSaleIncome;
    @JsonProperty(value = "yearProfit")//年利润
    private String yearProfit;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getIsHaveChildren() {
        return isHaveChildren;
    }

    public void setIsHaveChildren(String isHaveChildren) {
        this.isHaveChildren = isHaveChildren;
    }

    public String getResiType() {
        return resiType;
    }

    public void setResiType(String resiType) {
        this.resiType = resiType;
    }

    public String getLocalResiLmt() {
        return localResiLmt;
    }

    public void setLocalResiLmt(String localResiLmt) {
        this.localResiLmt = localResiLmt;
    }

    public String getLocalRegist() {
        return localRegist;
    }

    public void setLocalRegist(String localRegist) {
        this.localRegist = localRegist;
    }

    public String getOperAddr() {
        return operAddr;
    }

    public void setOperAddr(String operAddr) {
        this.operAddr = operAddr;
    }

    public String getOperLmt() {
        return operLmt;
    }

    public void setOperLmt(String operLmt) {
        this.operLmt = operLmt;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getSpouseIdcardNo() {
        return spouseIdcardNo;
    }

    public void setSpouseIdcardNo(String spouseIdcardNo) {
        this.spouseIdcardNo = spouseIdcardNo;
    }

    public String getSpouseMobileNo() {
        return spouseMobileNo;
    }

    public void setSpouseMobileNo(String spouseMobileNo) {
        this.spouseMobileNo = spouseMobileNo;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public String getAgriflag() {
        return agriflag;
    }

    public void setAgriflag(String agriflag) {
        this.agriflag = agriflag;
    }

    public String getCusOperIsNormal() {
        return cusOperIsNormal;
    }

    public void setCusOperIsNormal(String cusOperIsNormal) {
        this.cusOperIsNormal = cusOperIsNormal;
    }

    public String getOneLevelTradeCode() {
        return oneLevelTradeCode;
    }

    public void setOneLevelTradeCode(String oneLevelTradeCode) {
        this.oneLevelTradeCode = oneLevelTradeCode;
    }

    public String getOneLevelTradeName() {
        return oneLevelTradeName;
    }

    public void setOneLevelTradeName(String oneLevelTradeName) {
        this.oneLevelTradeName = oneLevelTradeName;
    }

    public String getDetailsTradeCode() {
        return detailsTradeCode;
    }

    public void setDetailsTradeCode(String detailsTradeCode) {
        this.detailsTradeCode = detailsTradeCode;
    }

    public String getDetailsTradeName() {
        return detailsTradeName;
    }

    public void setDetailsTradeName(String detailsTradeName) {
        this.detailsTradeName = detailsTradeName;
    }

    public String getOperScale() {
        return operScale;
    }

    public void setOperScale(String operScale) {
        this.operScale = operScale;
    }

    public String getOperScaleUnit() {
        return operScaleUnit;
    }

    public void setOperScaleUnit(String operScaleUnit) {
        this.operScaleUnit = operScaleUnit;
    }

    public String getYaerSaleIncome() {
        return yaerSaleIncome;
    }

    public void setYaerSaleIncome(String yaerSaleIncome) {
        this.yaerSaleIncome = yaerSaleIncome;
    }

    public String getYearProfit() {
        return yearProfit;
    }

    public void setYearProfit(String yearProfit) {
        this.yearProfit = yearProfit;
    }
}
