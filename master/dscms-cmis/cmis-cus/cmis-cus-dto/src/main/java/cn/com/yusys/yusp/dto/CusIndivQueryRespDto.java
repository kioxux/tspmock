package cn.com.yusys.yusp.dto;

import java.io.Serializable;
/**
 * 响应DTO：个人客户信息查看
 *
 * @author leehuang
 * @since 2021年4月10日 下午1:22:06
 * @version 1.0
 */
//@JsonPropertyOrder(alphabetic = true)
public class CusIndivQueryRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
//    @JsonProperty(value = "cusId")
    private String cusId                                     ;//客户编号
    private String cusName                                   ;//客户名称
    private String certType                                  ;//证件类型
    private String certNo                                    ;//证件号码
    private String idateIsIong                               ;//有效期是否长期
    private String certDueDate                               ;//证件到期日
    private String country                                   ;//国别
    private String sex                                       ;//性别
    private String cusType                                   ;//客户类型
    private String folk                                      ;//民族
    private String native1                                    ;//籍贯 TODO native是Java关键字
    private String birday                                    ;//出生日期
    private String politicalStatus                           ;//政治面貌
    private String edu                                       ;//最高学历
    private String degree                                    ;//最高学位
    private String marStatus                                 ;//婚姻状况
    private String healthStatus                              ;//健康情况
    private String isBankShd                                 ;//是否本行股东
    private String isBuildCretCorreTime                      ;//建立信贷关系时间
    private String mobileNo                                  ;//手机号码
    private String wechat                                    ;//微信
    private String resiPhone                                 ;//住宅电话
    private String qq                                        ;//QQ
    private String fax                                       ;//传真
    private String email                                     ;//Email地址
    private String region                                    ;//户籍地址
    private String postAddress                               ;//送达地址
    private String pcode                                     ;//邮政编码
    private String resiStatus                                ;//居住状况
    private String resiAreaName                              ;//居住区域名称
    private String resiAreaNo                                ;//居住编号区域
    private String resiAddr                                  ;//居住地址
    private String resiAddrPcode                             ;//居住地邮政编码
    private String resiTime                                  ;//居住时间（年）
    private String isAgri                                    ;//是否农户
    private String employStatus                              ;//雇佣状态
    private String occu                                      ;//职业
    private String freeOccuMemo                              ;//自由职业说明
    private String blic                                      ;//营业执照号码
    private String operStatus                                ;//经营状况
    private String operConName                               ;//经营企业名称/工作单位
    private String operConUnifiedCreditCode                  ;//经营企业统一社会信用代码
    private String unitCha                                   ;//单位性质
    private String unitPhone                                 ;//单位电话
    private String unitTrade                                 ;//单位所属行业
    private String unitAddr                                  ;//单位地址
    private String duty                                      ;//职务
    private String title                                     ;//职称
    private String cprtStartYear                             ;//单位工作起始年
    private String curType                                   ;//收入币种
    private String familyYearn                               ;//家庭年收入
    private String indivYearn                                ;//个人年收入
    private String isSmconCus                                ;//是否小企业客户
    private String isRichLoan                                ;//是否强村富民贷款
    private String inputId                                   ;//登记人
    private String inputDate                                 ;//登记日期
    private String managerId                                 ;//管户客户经理
    private String managerBrId                               ;//所属机构

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getIdateIsIong() {
        return idateIsIong;
    }

    public void setIdateIsIong(String idateIsIong) {
        this.idateIsIong = idateIsIong;
    }

    public String getCertDueDate() {
        return certDueDate;
    }

    public void setCertDueDate(String certDueDate) {
        this.certDueDate = certDueDate;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getFolk() {
        return folk;
    }

    public void setFolk(String folk) {
        this.folk = folk;
    }

    public String getBirday() {
        return birday;
    }

    public void setBirday(String birday) {
        this.birday = birday;
    }

    public String getPoliticalStatus() {
        return politicalStatus;
    }

    public void setPoliticalStatus(String politicalStatus) {
        this.politicalStatus = politicalStatus;
    }

    public String getEdu() {
        return edu;
    }

    public void setEdu(String edu) {
        this.edu = edu;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getMarStatus() {
        return marStatus;
    }

    public void setMarStatus(String marStatus) {
        this.marStatus = marStatus;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public String getIsBankShd() {
        return isBankShd;
    }

    public void setIsBankShd(String isBankShd) {
        this.isBankShd = isBankShd;
    }

    public String getIsBuildCretCorreTime() {
        return isBuildCretCorreTime;
    }

    public void setIsBuildCretCorreTime(String isBuildCretCorreTime) {
        this.isBuildCretCorreTime = isBuildCretCorreTime;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getResiPhone() {
        return resiPhone;
    }

    public void setResiPhone(String resiPhone) {
        this.resiPhone = resiPhone;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostAddress() {
        return postAddress;
    }

    public void setPostAddress(String postAddress) {
        this.postAddress = postAddress;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {
        this.pcode = pcode;
    }

    public String getResiStatus() {
        return resiStatus;
    }

    public void setResiStatus(String resiStatus) {
        this.resiStatus = resiStatus;
    }

    public String getResiAreaName() {
        return resiAreaName;
    }

    public void setResiAreaName(String resiAreaName) {
        this.resiAreaName = resiAreaName;
    }

    public String getResiAreaNo() {
        return resiAreaNo;
    }

    public void setResiAreaNo(String resiAreaNo) {
        this.resiAreaNo = resiAreaNo;
    }

    public String getResiAddr() {
        return resiAddr;
    }

    public void setResiAddr(String resiAddr) {
        this.resiAddr = resiAddr;
    }

    public String getResiAddrPcode() {
        return resiAddrPcode;
    }

    public void setResiAddrPcode(String resiAddrPcode) {
        this.resiAddrPcode = resiAddrPcode;
    }

    public String getResiTime() {
        return resiTime;
    }

    public void setResiTime(String resiTime) {
        this.resiTime = resiTime;
    }

    public String getIsAgri() {
        return isAgri;
    }

    public void setIsAgri(String isAgri) {
        this.isAgri = isAgri;
    }

    public String getEmployStatus() {
        return employStatus;
    }

    public void setEmployStatus(String employStatus) {
        this.employStatus = employStatus;
    }

    public String getOccu() {
        return occu;
    }

    public void setOccu(String occu) {
        this.occu = occu;
    }

    public String getFreeOccuMemo() {
        return freeOccuMemo;
    }

    public void setFreeOccuMemo(String freeOccuMemo) {
        this.freeOccuMemo = freeOccuMemo;
    }

    public String getBlic() {
        return blic;
    }

    public void setBlic(String blic) {
        this.blic = blic;
    }

    public String getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(String operStatus) {
        this.operStatus = operStatus;
    }

    public String getOperConName() {
        return operConName;
    }

    public void setOperConName(String operConName) {
        this.operConName = operConName;
    }

    public String getOperConUnifiedCreditCode() {
        return operConUnifiedCreditCode;
    }

    public void setOperConUnifiedCreditCode(String operConUnifiedCreditCode) {
        this.operConUnifiedCreditCode = operConUnifiedCreditCode;
    }

    public String getUnitCha() {
        return unitCha;
    }

    public void setUnitCha(String unitCha) {
        this.unitCha = unitCha;
    }

    public String getUnitPhone() {
        return unitPhone;
    }

    public void setUnitPhone(String unitPhone) {
        this.unitPhone = unitPhone;
    }

    public String getUnitTrade() {
        return unitTrade;
    }

    public void setUnitTrade(String unitTrade) {
        this.unitTrade = unitTrade;
    }

    public String getUnitAddr() {
        return unitAddr;
    }

    public void setUnitAddr(String unitAddr) {
        this.unitAddr = unitAddr;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCprtStartYear() {
        return cprtStartYear;
    }

    public void setCprtStartYear(String cprtStartYear) {
        this.cprtStartYear = cprtStartYear;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getFamilyYearn() {
        return familyYearn;
    }

    public void setFamilyYearn(String familyYearn) {
        this.familyYearn = familyYearn;
    }

    public String getIndivYearn() {
        return indivYearn;
    }

    public void setIndivYearn(String indivYearn) {
        this.indivYearn = indivYearn;
    }

    public String getIsSmconCus() {
        return isSmconCus;
    }

    public void setIsSmconCus(String isSmconCus) {
        this.isSmconCus = isSmconCus;
    }

    public String getIsRichLoan() {
        return isRichLoan;
    }

    public void setIsRichLoan(String isRichLoan) {
        this.isRichLoan = isRichLoan;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    @Override
    public String toString() {
        return "CusIndivQueryRespDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", idateIsIong='" + idateIsIong + '\'' +
                ", certDueDate='" + certDueDate + '\'' +
                ", country='" + country + '\'' +
                ", sex='" + sex + '\'' +
                ", cusType='" + cusType + '\'' +
                ", folk='" + folk + '\'' +
                ", birday='" + birday + '\'' +
                ", politicalStatus='" + politicalStatus + '\'' +
                ", edu='" + edu + '\'' +
                ", degree='" + degree + '\'' +
                ", marStatus='" + marStatus + '\'' +
                ", healthStatus='" + healthStatus + '\'' +
                ", isBankShd='" + isBankShd + '\'' +
                ", isBuildCretCorreTime='" + isBuildCretCorreTime + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", wechat='" + wechat + '\'' +
                ", resiPhone='" + resiPhone + '\'' +
                ", qq='" + qq + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", region='" + region + '\'' +
                ", postAddress='" + postAddress + '\'' +
                ", pcode='" + pcode + '\'' +
                ", resiStatus='" + resiStatus + '\'' +
                ", resiAreaName='" + resiAreaName + '\'' +
                ", resiAreaNo='" + resiAreaNo + '\'' +
                ", resiAddr='" + resiAddr + '\'' +
                ", resiAddrPcode='" + resiAddrPcode + '\'' +
                ", resiTime='" + resiTime + '\'' +
                ", isAgri='" + isAgri + '\'' +
                ", employStatus='" + employStatus + '\'' +
                ", occu='" + occu + '\'' +
                ", freeOccuMemo='" + freeOccuMemo + '\'' +
                ", blic='" + blic + '\'' +
                ", operStatus='" + operStatus + '\'' +
                ", operConName='" + operConName + '\'' +
                ", operConUnifiedCreditCode='" + operConUnifiedCreditCode + '\'' +
                ", unitCha='" + unitCha + '\'' +
                ", unitPhone='" + unitPhone + '\'' +
                ", unitTrade='" + unitTrade + '\'' +
                ", unitAddr='" + unitAddr + '\'' +
                ", duty='" + duty + '\'' +
                ", title='" + title + '\'' +
                ", cprtStartYear='" + cprtStartYear + '\'' +
                ", curType='" + curType + '\'' +
                ", familyYearn='" + familyYearn + '\'' +
                ", indivYearn='" + indivYearn + '\'' +
                ", isSmconCus='" + isSmconCus + '\'' +
                ", isRichLoan='" + isRichLoan + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                '}';
    }
}
