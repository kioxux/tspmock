package cn.com.yusys.yusp.dto.server.xdkh0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询客户配偶信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0004DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户代码
    @JsonProperty(value = "certType")
    private String certType;//借款人证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//借款人证件号码

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xdkh0004DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                '}';
    }
}
