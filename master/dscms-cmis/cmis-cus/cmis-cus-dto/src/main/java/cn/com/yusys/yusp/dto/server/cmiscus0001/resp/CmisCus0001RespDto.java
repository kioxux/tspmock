package cn.com.yusys.yusp.dto.server.cmiscus0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：集团成员列表查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0001RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息
    @JsonProperty(value = "grpNo")
    private String grpNo;//集团客户编号
    @JsonProperty(value = "grpName")
    private String grpName;//集团客户名称

    /**
     * 集团成员列表
     */
    private List<CmisCus0001GrpMemberListRespDto> grpMemberList;

    public List<CmisCus0001GrpMemberListRespDto> getGrpMemberList() {
        return grpMemberList;
    }

    public void setGrpMemberList(List<CmisCus0001GrpMemberListRespDto> grpMemberList) {
        this.grpMemberList = grpMemberList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getGrpNo() {
        return grpNo;
    }

    public void setGrpNo(String grpNo) {
        this.grpNo = grpNo;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    @Override
    public String toString() {
        return "CmisCus0001RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", grpNo='" + grpNo + '\'' +
                ", grpName='" + grpName + '\'' +
                ", grpMemberList=" + grpMemberList +
                '}';
    }
}


