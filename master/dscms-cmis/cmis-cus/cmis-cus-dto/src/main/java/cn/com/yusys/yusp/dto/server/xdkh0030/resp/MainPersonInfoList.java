package cn.com.yusys.yusp.dto.server.xdkh0030.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：公司客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class MainPersonInfoList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "mainCtrlorNo")
    private String mainCtrlorNo;//主要控制人编号
    @JsonProperty(value = "mainCtrlorName")
    private String mainCtrlorName;//主要控制人名称
    @JsonProperty(value = "mrgFjobDate")
    private String mrgFjobDate;//高管从业日期
    @JsonProperty(value = "mrgNetAsset")
    private String mrgNetAsset;//高管个人净资产
    @JsonProperty(value = "mrgBirday")
    private String mrgBirday;//高管出生日期
    @JsonProperty(value = "mrgEdt")
    private String mrgEdt;//高管最高学历
    @JsonProperty(value = "mrgIndivRsdSt")
    private String mrgIndivRsdSt;//高管本地居住情况

    public String getMainCtrlorNo() {
        return mainCtrlorNo;
    }

    public void setMainCtrlorNo(String mainCtrlorNo) {
        this.mainCtrlorNo = mainCtrlorNo;
    }

    public String getMainCtrlorName() {
        return mainCtrlorName;
    }

    public void setMainCtrlorName(String mainCtrlorName) {
        this.mainCtrlorName = mainCtrlorName;
    }

    public String getMrgFjobDate() {
        return mrgFjobDate;
    }

    public void setMrgFjobDate(String mrgFjobDate) {
        this.mrgFjobDate = mrgFjobDate;
    }

    public String getMrgNetAsset() {
        return mrgNetAsset;
    }

    public void setMrgNetAsset(String mrgNetAsset) {
        this.mrgNetAsset = mrgNetAsset;
    }

    public String getMrgBirday() {
        return mrgBirday;
    }

    public void setMrgBirday(String mrgBirday) {
        this.mrgBirday = mrgBirday;
    }

    public String getMrgEdt() {
        return mrgEdt;
    }

    public void setMrgEdt(String mrgEdt) {
        this.mrgEdt = mrgEdt;
    }

    public String getMrgIndivRsdSt() {
        return mrgIndivRsdSt;
    }

    public void setMrgIndivRsdSt(String mrgIndivRsdSt) {
        this.mrgIndivRsdSt = mrgIndivRsdSt;
    }

    @Override
    public String toString() {
        return "MainPersonInfoList{" +
                "mainCtrlorNo='" + mainCtrlorNo + '\'' +
                ", mainCtrlorName='" + mainCtrlorName + '\'' +
                ", mrgFjobDate='" + mrgFjobDate + '\'' +
                ", mrgNetAsset='" + mrgNetAsset + '\'' +
                ", mrgBirday='" + mrgBirday + '\'' +
                ", mrgEdt='" + mrgEdt + '\'' +
                ", mrgIndivRsdSt='" + mrgIndivRsdSt + '\'' +
                '}';
    }
}