package cn.com.yusys.yusp.dto.server.cmiscus0014.resp;

import cn.com.yusys.yusp.dto.CusCorpMgrDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusIntbankMgrDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：法人客户及股东信息查询
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisCus0014RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    @JsonProperty(value = "cusId")
    private String  cusId;//客户编号
    @JsonProperty(value = "cusNameEn")
    private String  cusNameEn;//外文名称
    @JsonProperty(value = "country")
    private String  country;//国别
    @JsonProperty(value = "corpQlty")
    private String  corpQlty;//企业性质
    @JsonProperty(value = "cityType")
    private String  cityType;//城乡类型
    @JsonProperty(value = "assTotal")
    private BigDecimal  assTotal;//资产总额（万元)
    @JsonProperty(value = "adminSubRel")
    private String  adminSubRel;//行政隶属关系
    @JsonProperty(value = "subTyp")
    private String  subTyp;//隶属关系
    @JsonProperty(value = "investMbody")
    private String  investMbody;//投资主体
    @JsonProperty(value = "holdType")
    private String  holdType;//控股类型
    @JsonProperty(value = "tradeClass")
    private String  tradeClass;//行业分类
    @JsonProperty(value = "cusCcrModelId")
    private String  cusCcrModelId;//评级模型ID
    @JsonProperty(value = "cusCcrModelName")
    private String  cusCcrModelName;//评级模型名称
    @JsonProperty(value = "cllType2")
    private String  cllType2;//行业分类2
    @JsonProperty(value = "buildDate")
    private String  buildDate;//成立日期
    @JsonProperty(value = "fjobNum")
    private Integer  fjobNum;//从业人数
    @JsonProperty(value = "salVolume")
    private BigDecimal  salVolume;//销售额（万元)
    @JsonProperty(value = "operIncome")
    private BigDecimal  operIncome;//营业收入（万元)
    @JsonProperty(value = "corpScale")
    private String  corpScale;//企业规模
    @JsonProperty(value = "cusScaleForm")
    private String  cusScaleForm;//企业规模（报表)
    @JsonProperty(value = "natEcoSec")
    private String  natEcoSec;//国民经济部门编号
    @JsonProperty(value = "unitAlleInd")
    private String  unitAlleInd;//产业扶贫单位标志
    @JsonProperty(value = "alleBunb")
    private Integer alleBunb;//扶贫带动人数
    @JsonProperty(value = "framInd")
    private String  framInd;//农场标志
    @JsonProperty(value = "farmerCopInd")
    private String  farmerCopInd;//农民专业合作社标志
    @JsonProperty(value = "agrInd")
    private String  agrInd;//是否涉农
    @JsonProperty(value = "inclusiveFinanceStatistics")
    private String  inclusiveFinanceStatistics;//普惠金融统计口径 STD_ZB_YES_NO
    @JsonProperty(value = "insCode")
    private String  insCode;//组织机构代码
    @JsonProperty(value = "insRegDate")
    private String  insRegDate;//组织机构登记日期
    @JsonProperty(value = "insEndDate")
    private String  insEndDate;//组织机构有效日期
    @JsonProperty(value = "insOrg")
    private String  insOrg;//组织机构代码证颁发机关
    @JsonProperty(value = "insAnnDate")
    private String  insAnnDate;//组织机构代码证年检到期日
    @JsonProperty(value = "licenseType")
    private String  licenseType;//注册登记号类型 STD_ZB_LICENSE_TYPE
    @JsonProperty(value = "regiCode")
    private String  regiCode;//登记注册号
    @JsonProperty(value = "regiType")
    private String  regiType;//注册登记类型
    @JsonProperty(value = "quliGarade")
    private String  quliGarade;//资质等级 STD_ZB_QUAL_LEVEL
    @JsonProperty(value = "adminOrg")
    private String  adminOrg;//主管单位
    @JsonProperty(value = "apprOrg")
    private String  apprOrg;//审批机关
    @JsonProperty(value = "apprDocNo")
    private String  apprDocNo;//批准文号
    @JsonProperty(value = "regiAreaCode")
    private String  regiAreaCode;//注册地行政区划
    @JsonProperty(value = "regiAddr")
    private String  regiAddr;//注册登记地址
    @JsonProperty(value = "regiAddrEn")
    private String  regiAddrEn;//外文注册登记地址
    @JsonProperty(value = "acuStateCode")
    private String  acuStateCode;//实际经营地行政区划
    @JsonProperty(value = "operAddrAct")
    private String  operAddrAct;//实际经营地址
    @JsonProperty(value = "mainOptScp")
    private String  mainOptScp;//主营业务范围
    @JsonProperty(value = "partOptScp")
    private String  partOptScp;//兼营业务范围
    @JsonProperty(value = "regiCurType")
    private String  regiCurType;//注册资本/开办资金币种
    @JsonProperty(value = "regiCapAmt")
    private BigDecimal  regiCapAmt;//注册资本金额
    @JsonProperty(value = "paidCapCurType")
    private String  paidCapCurType;//实收资本币种
    @JsonProperty(value = "paidCapAmt")
    private BigDecimal  paidCapAmt;//实收资本金额
    @JsonProperty(value = "regiStartDate")
    private String  regiStartDate;//注册登记起始日期
    @JsonProperty(value = "regiEndDate")
    private String  regiEndDate;//注册登记到期日期
    @JsonProperty(value = "regAudit")
    private String  regAudit;//注册登记年审结论
    @JsonProperty(value = "regAuditDate")
    private String  regAuditDate;//注册登记年审日期
    @JsonProperty(value = "regAuditEndDate")
    private String  regAuditEndDate;//注册登记年检到期日
    @JsonProperty(value = "natTaxRegCode")
    private String  natTaxRegCode;//国税税务登记代码
    @JsonProperty(value = "natTaxRegOrg")
    private String  natTaxRegOrg;//国税税务登记机关
    @JsonProperty(value = "natTaxRegDt")
    private String  natTaxRegDt;//国税税务登记日期
    @JsonProperty(value = "natTaxRegEndDt")
    private String  natTaxRegEndDt;//国税登记有效期
    @JsonProperty(value = "natTaxAnnDate")
    private String  natTaxAnnDate;//国税登记证年检到期日
    @JsonProperty(value = "locTaxRegCode")
    private String  locTaxRegCode;//地税税务登记代码
    @JsonProperty(value = "locTaxRegOrg")
    private String  locTaxRegOrg;//地税税务登记机构
    @JsonProperty(value = "locTaxRegDt")
    private String  locTaxRegDt;//地税税务登记日期
    @JsonProperty(value = "locTaxRegEndDt")
    private String  locTaxRegEndDt;//地税登记有效期
    @JsonProperty(value = "locTaxAnnDate")
    private String  locTaxAnnDate;//地税登记证年检到期日
    @JsonProperty(value = "loanCardFlg")
    private String  loanCardFlg;//有无贷款卡
    @JsonProperty(value = "loanCardId")
    private String  loanCardId;//中证码/贷款卡号
    @JsonProperty(value = "loanCardPwd")
    private String  loanCardPwd;//贷款卡密码
    @JsonProperty(value = "loanCardEffFlg")
    private String  loanCardEffFlg;//贷款卡状态
    @JsonProperty(value = "loanCardAnnDate")
    private String  loanCardAnnDate;//贷款卡年检日期
    @JsonProperty(value = "certIdate")
    private String  certIdate;//证件有效期
    @JsonProperty(value = "conType")
    private String  conType;//企业类型
    @JsonProperty(value = "corpOwnersType")
    private String  corpOwnersType;//企业所有制
    @JsonProperty(value = "isBankShd")
    private String  isBankShd;//是否本行股东
    @JsonProperty(value = "isSmconCus")
    private String  isSmconCus;//是否小企业客户
    @JsonProperty(value = "detailAddr")
    private String  detailAddr;//详细地址
    @JsonProperty(value = "produceEquipYear")
    private String  produceEquipYear;//年生产设备
    @JsonProperty(value = "qq")
    private String  qq;//QQ
    @JsonProperty(value = "produceAbiYear")
    private String  produceAbiYear;//年生产能力
    @JsonProperty(value = "isNatctl")
    private String  isNatctl;//是否国控
    @JsonProperty(value = "freqLinkman")
    private String  freqLinkman;//常用联系人
    @JsonProperty(value = "natctlLevel")
    private String  natctlLevel;//国控层级
    @JsonProperty(value = "licOperPro")
    private String  licOperPro;//许可经营项目
    @JsonProperty(value = "freqLinkmanTel")
    private String  freqLinkmanTel;//常用联系人手机
    @JsonProperty(value = "commonOperPro")
    private String  commonOperPro;//一般经营项目
    @JsonProperty(value = "operStatus")
    private String  operStatus;//经营状况
    @JsonProperty(value = "isLongVld")
    private String  isLongVld;//是否长期有效
    @JsonProperty(value = "basicDepAccNoOpenLic")
    private String  basicDepAccNoOpenLic;//基本存款账户开户许可证（核准号)
    @JsonProperty(value = "basicDepAccNo")
    private String  basicDepAccNo;//基本存款账户账号
    @JsonProperty(value = "isBankBasicDepAccNo")
    private String  isBankBasicDepAccNo;//基本存款账户是否在本机构
    @JsonProperty(value = "basicDepAccob")
    private String  basicDepAccob;//基本存款账户开户行
    @JsonProperty(value = "basicAccNoOpenDate")
    private String  basicAccNoOpenDate;//基本账户开户日期
    @JsonProperty(value = "commonAccNoOpenDate")
    private String  commonAccNoOpenDate;//一般账户开户日期
    @JsonProperty(value = "mainPrdDesc")
    private String  mainPrdDesc;//主要产品情况
    @JsonProperty(value = "creditLevelOuter")
    private String  creditLevelOuter;//信用等级（外部)
    @JsonProperty(value = "evalDate")
    private String  evalDate;//评定日期（外部)
    @JsonProperty(value = "fax")
    private String  fax;//传真
    @JsonProperty(value = "evalOrgId")
    private String  evalOrgId;//评定机构（外部)
    @JsonProperty(value = "linkmanEmail")
    private String  linkmanEmail;//联系人电子邮箱
    @JsonProperty(value = "sendAddr")
    private String  sendAddr;//送达地址
    @JsonProperty(value = "initLoanDate")
    private String  initLoanDate;//建立信贷关系时间
    @JsonProperty(value = "wechatNo")
    private String  wechatNo;//微信号
    @JsonProperty(value = "isStrgcCus")
    private String  isStrgcCus;//是否战略客户
    @JsonProperty(value = "finaReportType")
    private String  finaReportType;//财务报表类型
    @JsonProperty(value = "areaPriorCorp")
    private String  areaPriorCorp;//地区重点企业
    @JsonProperty(value = "spOperFlag")
    private String  spOperFlag;//特种经营标识
    @JsonProperty(value = "isNewBuildCorp")
    private String  isNewBuildCorp;//是否新建企业
    @JsonProperty(value = "grpCusType")
    private String  grpCusType;//集团客户类型
    @JsonProperty(value = "regiOrg")
    private String  regiOrg;//注册登记机关
    @JsonProperty(value = "mainBusNation")
    private String  mainBusNation;//主营业务所在国家
    @JsonProperty(value = "creditPadAccNo")
    private String  creditPadAccNo;//信用证垫款账号
    @JsonProperty(value = "loanType")
    private String  loanType;//贷款类型
    @JsonProperty(value = "operPlaceOwnshp")
    private String  operPlaceOwnshp;//经营场地所有权
    @JsonProperty(value = "operPlaceSqu")
    private BigDecimal  operPlaceSqu;//经营场地面积
    @JsonProperty(value = "mainProduceEquip")
    private String  mainProduceEquip;//主要生产设备
    @JsonProperty(value = "inputId")
    private String  inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String  inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String  inputDate;//登记时间
    @JsonProperty(value = "oprType")
    private String  oprType;//操作类型
    @JsonProperty(value = "iisSzjrfwCrop")
    private String  iisSzjrfwCrop;//是否苏州综合平台企业
    @JsonProperty(value = "isSteelCus")
    private String  isSteelCus;//是否钢贸企业
    @JsonProperty(value = "isStockCorp")
    private String  isStockCorp;//是否上市公司
    @JsonProperty(value = "isCtinve")
    private String  isCtinve;//是否城投
    @JsonProperty(value = "ctinveLevel")
    private String  ctinveLevel;//城投层级
    @JsonProperty(value = "goverInvestPlat")
    private String  goverInvestPlat;//政府投资平台
    @JsonProperty(value = "impexpFlag")
    private String  impexpFlag;//进出口权标识
    @JsonProperty(value = "redcbizUnitTradeClass")
    private String  redcbizUnitTradeClass;//再贴现业务中申请单位行业分类
    @JsonProperty(value = "cusType")
    private String  cusType;//客户类型
    @JsonProperty(value = "cusName")
    private String  cusName;//客户名称
    @JsonProperty(value = "islocked")
    private String  islocked;//锁定状态
    @JsonProperty(value = "finalRank")
    private String  finalRank;//信用评级最终等级
    @JsonProperty(value = "dueDt")
    private String  dueDt;//非零内评到期日期

    /**
     * 股东列表
     */
    private List<CmisCus0014StockHolderListRespDto> stockHolderList;

    public List<CmisCus0014StockHolderListRespDto> getStockHolderList(){
        return stockHolderList;
    }

    public void setStockHolderList(List<CmisCus0014StockHolderListRespDto> stockHolderList){
        this.stockHolderList = stockHolderList;
    }

    /**
     * 控股股东
     */
    private List<CusCorpMgrDto> cusCorpMgrDtoList;

    public List<CusCorpMgrDto> getCusCorpMgrDtoList() {
        return cusCorpMgrDtoList;
    }

    public void setCusCorpMgrDtoList(List<CusCorpMgrDto> cusCorpMgrDtoList) {
        this.cusCorpMgrDtoList = cusCorpMgrDtoList;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusNameEn() {
        return cusNameEn;
    }

    public void setCusNameEn(String cusNameEn) {
        this.cusNameEn = cusNameEn;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCorpQlty() {
        return corpQlty;
    }

    public void setCorpQlty(String corpQlty) {
        this.corpQlty = corpQlty;
    }

    public String getCityType() {
        return cityType;
    }

    public void setCityType(String cityType) {
        this.cityType = cityType;
    }

    public BigDecimal getAssTotal() {
        return assTotal;
    }

    public void setAssTotal(BigDecimal assTotal) {
        this.assTotal = assTotal;
    }

    public String getAdminSubRel() {
        return adminSubRel;
    }

    public void setAdminSubRel(String adminSubRel) {
        this.adminSubRel = adminSubRel;
    }

    public String getSubTyp() {
        return subTyp;
    }

    public void setSubTyp(String subTyp) {
        this.subTyp = subTyp;
    }

    public String getInvestMbody() {
        return investMbody;
    }

    public void setInvestMbody(String investMbody) {
        this.investMbody = investMbody;
    }

    public String getHoldType() {
        return holdType;
    }

    public void setHoldType(String holdType) {
        this.holdType = holdType;
    }

    public String getTradeClass() {
        return tradeClass;
    }

    public void setTradeClass(String tradeClass) {
        this.tradeClass = tradeClass;
    }

    public String getCusCcrModelId() {
        return cusCcrModelId;
    }

    public void setCusCcrModelId(String cusCcrModelId) {
        this.cusCcrModelId = cusCcrModelId;
    }

    public String getCusCcrModelName() {
        return cusCcrModelName;
    }

    public void setCusCcrModelName(String cusCcrModelName) {
        this.cusCcrModelName = cusCcrModelName;
    }

    public String getCllType2() {
        return cllType2;
    }

    public void setCllType2(String cllType2) {
        this.cllType2 = cllType2;
    }

    public String getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(String buildDate) {
        this.buildDate = buildDate;
    }

    public Integer getFjobNum() {
        return fjobNum;
    }

    public void setFjobNum(Integer fjobNum) {
        this.fjobNum = fjobNum;
    }

    public BigDecimal getSalVolume() {
        return salVolume;
    }

    public void setSalVolume(BigDecimal salVolume) {
        this.salVolume = salVolume;
    }

    public BigDecimal getOperIncome() {
        return operIncome;
    }

    public void setOperIncome(BigDecimal operIncome) {
        this.operIncome = operIncome;
    }

    public String getCorpScale() {
        return corpScale;
    }

    public void setCorpScale(String corpScale) {
        this.corpScale = corpScale;
    }

    public String getCusScaleForm() {
        return cusScaleForm;
    }

    public void setCusScaleForm(String cusScaleForm) {
        this.cusScaleForm = cusScaleForm;
    }

    public String getNatEcoSec() {
        return natEcoSec;
    }

    public void setNatEcoSec(String natEcoSec) {
        this.natEcoSec = natEcoSec;
    }

    public String getUnitAlleInd() {
        return unitAlleInd;
    }

    public void setUnitAlleInd(String unitAlleInd) {
        this.unitAlleInd = unitAlleInd;
    }

    public Integer getAlleBunb() {
        return alleBunb;
    }

    public void setAlleBunb(Integer alleBunb) {
        this.alleBunb = alleBunb;
    }

    public String getFramInd() {
        return framInd;
    }

    public void setFramInd(String framInd) {
        this.framInd = framInd;
    }

    public String getFarmerCopInd() {
        return farmerCopInd;
    }

    public void setFarmerCopInd(String farmerCopInd) {
        this.farmerCopInd = farmerCopInd;
    }

    public String getAgrInd() {
        return agrInd;
    }

    public void setAgrInd(String agrInd) {
        this.agrInd = agrInd;
    }

    public String getInclusiveFinanceStatistics() {
        return inclusiveFinanceStatistics;
    }

    public void setInclusiveFinanceStatistics(String inclusiveFinanceStatistics) {
        this.inclusiveFinanceStatistics = inclusiveFinanceStatistics;
    }

    public String getInsCode() {
        return insCode;
    }

    public void setInsCode(String insCode) {
        this.insCode = insCode;
    }

    public String getInsRegDate() {
        return insRegDate;
    }

    public void setInsRegDate(String insRegDate) {
        this.insRegDate = insRegDate;
    }

    public String getInsEndDate() {
        return insEndDate;
    }

    public void setInsEndDate(String insEndDate) {
        this.insEndDate = insEndDate;
    }

    public String getInsOrg() {
        return insOrg;
    }

    public void setInsOrg(String insOrg) {
        this.insOrg = insOrg;
    }

    public String getInsAnnDate() {
        return insAnnDate;
    }

    public void setInsAnnDate(String insAnnDate) {
        this.insAnnDate = insAnnDate;
    }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getRegiCode() {
        return regiCode;
    }

    public void setRegiCode(String regiCode) {
        this.regiCode = regiCode;
    }

    public String getRegiType() {
        return regiType;
    }

    public void setRegiType(String regiType) {
        this.regiType = regiType;
    }

    public String getQuliGarade() {
        return quliGarade;
    }

    public void setQuliGarade(String quliGarade) {
        this.quliGarade = quliGarade;
    }

    public String getAdminOrg() {
        return adminOrg;
    }

    public void setAdminOrg(String adminOrg) {
        this.adminOrg = adminOrg;
    }

    public String getApprOrg() {
        return apprOrg;
    }

    public void setApprOrg(String apprOrg) {
        this.apprOrg = apprOrg;
    }

    public String getApprDocNo() {
        return apprDocNo;
    }

    public void setApprDocNo(String apprDocNo) {
        this.apprDocNo = apprDocNo;
    }

    public String getRegiAreaCode() {
        return regiAreaCode;
    }

    public void setRegiAreaCode(String regiAreaCode) {
        this.regiAreaCode = regiAreaCode;
    }

    public String getRegiAddr() {
        return regiAddr;
    }

    public void setRegiAddr(String regiAddr) {
        this.regiAddr = regiAddr;
    }

    public String getRegiAddrEn() {
        return regiAddrEn;
    }

    public void setRegiAddrEn(String regiAddrEn) {
        this.regiAddrEn = regiAddrEn;
    }

    public String getAcuStateCode() {
        return acuStateCode;
    }

    public void setAcuStateCode(String acuStateCode) {
        this.acuStateCode = acuStateCode;
    }

    public String getOperAddrAct() {
        return operAddrAct;
    }

    public void setOperAddrAct(String operAddrAct) {
        this.operAddrAct = operAddrAct;
    }

    public String getMainOptScp() {
        return mainOptScp;
    }

    public void setMainOptScp(String mainOptScp) {
        this.mainOptScp = mainOptScp;
    }

    public String getPartOptScp() {
        return partOptScp;
    }

    public void setPartOptScp(String partOptScp) {
        this.partOptScp = partOptScp;
    }

    public String getRegiCurType() {
        return regiCurType;
    }

    public void setRegiCurType(String regiCurType) {
        this.regiCurType = regiCurType;
    }

    public BigDecimal getRegiCapAmt() {
        return regiCapAmt;
    }

    public void setRegiCapAmt(BigDecimal regiCapAmt) {
        this.regiCapAmt = regiCapAmt;
    }

    public String getPaidCapCurType() {
        return paidCapCurType;
    }

    public void setPaidCapCurType(String paidCapCurType) {
        this.paidCapCurType = paidCapCurType;
    }

    public BigDecimal getPaidCapAmt() {
        return paidCapAmt;
    }

    public void setPaidCapAmt(BigDecimal paidCapAmt) {
        this.paidCapAmt = paidCapAmt;
    }

    public String getRegiStartDate() {
        return regiStartDate;
    }

    public void setRegiStartDate(String regiStartDate) {
        this.regiStartDate = regiStartDate;
    }

    public String getRegiEndDate() {
        return regiEndDate;
    }

    public void setRegiEndDate(String regiEndDate) {
        this.regiEndDate = regiEndDate;
    }

    public String getRegAudit() {
        return regAudit;
    }

    public void setRegAudit(String regAudit) {
        this.regAudit = regAudit;
    }

    public String getRegAuditDate() {
        return regAuditDate;
    }

    public void setRegAuditDate(String regAuditDate) {
        this.regAuditDate = regAuditDate;
    }

    public String getRegAuditEndDate() {
        return regAuditEndDate;
    }

    public void setRegAuditEndDate(String regAuditEndDate) {
        this.regAuditEndDate = regAuditEndDate;
    }

    public String getNatTaxRegCode() {
        return natTaxRegCode;
    }

    public void setNatTaxRegCode(String natTaxRegCode) {
        this.natTaxRegCode = natTaxRegCode;
    }

    public String getNatTaxRegOrg() {
        return natTaxRegOrg;
    }

    public void setNatTaxRegOrg(String natTaxRegOrg) {
        this.natTaxRegOrg = natTaxRegOrg;
    }

    public String getNatTaxRegDt() {
        return natTaxRegDt;
    }

    public void setNatTaxRegDt(String natTaxRegDt) {
        this.natTaxRegDt = natTaxRegDt;
    }

    public String getNatTaxRegEndDt() {
        return natTaxRegEndDt;
    }

    public void setNatTaxRegEndDt(String natTaxRegEndDt) {
        this.natTaxRegEndDt = natTaxRegEndDt;
    }

    public String getNatTaxAnnDate() {
        return natTaxAnnDate;
    }

    public void setNatTaxAnnDate(String natTaxAnnDate) {
        this.natTaxAnnDate = natTaxAnnDate;
    }

    public String getLocTaxRegCode() {
        return locTaxRegCode;
    }

    public void setLocTaxRegCode(String locTaxRegCode) {
        this.locTaxRegCode = locTaxRegCode;
    }

    public String getLocTaxRegOrg() {
        return locTaxRegOrg;
    }

    public void setLocTaxRegOrg(String locTaxRegOrg) {
        this.locTaxRegOrg = locTaxRegOrg;
    }

    public String getLocTaxRegDt() {
        return locTaxRegDt;
    }

    public void setLocTaxRegDt(String locTaxRegDt) {
        this.locTaxRegDt = locTaxRegDt;
    }

    public String getLocTaxRegEndDt() {
        return locTaxRegEndDt;
    }

    public void setLocTaxRegEndDt(String locTaxRegEndDt) {
        this.locTaxRegEndDt = locTaxRegEndDt;
    }

    public String getLocTaxAnnDate() {
        return locTaxAnnDate;
    }

    public void setLocTaxAnnDate(String locTaxAnnDate) {
        this.locTaxAnnDate = locTaxAnnDate;
    }

    public String getLoanCardFlg() {
        return loanCardFlg;
    }

    public void setLoanCardFlg(String loanCardFlg) {
        this.loanCardFlg = loanCardFlg;
    }

    public String getLoanCardId() {
        return loanCardId;
    }

    public void setLoanCardId(String loanCardId) {
        this.loanCardId = loanCardId;
    }

    public String getLoanCardPwd() {
        return loanCardPwd;
    }

    public void setLoanCardPwd(String loanCardPwd) {
        this.loanCardPwd = loanCardPwd;
    }

    public String getLoanCardEffFlg() {
        return loanCardEffFlg;
    }

    public void setLoanCardEffFlg(String loanCardEffFlg) {
        this.loanCardEffFlg = loanCardEffFlg;
    }

    public String getLoanCardAnnDate() {
        return loanCardAnnDate;
    }

    public void setLoanCardAnnDate(String loanCardAnnDate) {
        this.loanCardAnnDate = loanCardAnnDate;
    }

    public String getCertIdate() {
        return certIdate;
    }

    public void setCertIdate(String certIdate) {
        this.certIdate = certIdate;
    }

    public String getConType() {
        return conType;
    }

    public void setConType(String conType) {
        this.conType = conType;
    }

    public String getCorpOwnersType() {
        return corpOwnersType;
    }

    public void setCorpOwnersType(String corpOwnersType) {
        this.corpOwnersType = corpOwnersType;
    }

    public String getIsBankShd() {
        return isBankShd;
    }

    public void setIsBankShd(String isBankShd) {
        this.isBankShd = isBankShd;
    }

    public String getIsSmconCus() {
        return isSmconCus;
    }

    public void setIsSmconCus(String isSmconCus) {
        this.isSmconCus = isSmconCus;
    }

    public String getDetailAddr() {
        return detailAddr;
    }

    public void setDetailAddr(String detailAddr) {
        this.detailAddr = detailAddr;
    }

    public String getProduceEquipYear() {
        return produceEquipYear;
    }

    public void setProduceEquipYear(String produceEquipYear) {
        this.produceEquipYear = produceEquipYear;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getProduceAbiYear() {
        return produceAbiYear;
    }

    public void setProduceAbiYear(String produceAbiYear) {
        this.produceAbiYear = produceAbiYear;
    }

    public String getIsNatctl() {
        return isNatctl;
    }

    public void setIsNatctl(String isNatctl) {
        this.isNatctl = isNatctl;
    }

    public String getFreqLinkman() {
        return freqLinkman;
    }

    public void setFreqLinkman(String freqLinkman) {
        this.freqLinkman = freqLinkman;
    }

    public String getNatctlLevel() {
        return natctlLevel;
    }

    public void setNatctlLevel(String natctlLevel) {
        this.natctlLevel = natctlLevel;
    }

    public String getLicOperPro() {
        return licOperPro;
    }

    public void setLicOperPro(String licOperPro) {
        this.licOperPro = licOperPro;
    }

    public String getFreqLinkmanTel() {
        return freqLinkmanTel;
    }

    public void setFreqLinkmanTel(String freqLinkmanTel) {
        this.freqLinkmanTel = freqLinkmanTel;
    }

    public String getCommonOperPro() {
        return commonOperPro;
    }

    public void setCommonOperPro(String commonOperPro) {
        this.commonOperPro = commonOperPro;
    }

    public String getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(String operStatus) {
        this.operStatus = operStatus;
    }

    public String getIsLongVld() {
        return isLongVld;
    }

    public void setIsLongVld(String isLongVld) {
        this.isLongVld = isLongVld;
    }

    public String getBasicDepAccNoOpenLic() {
        return basicDepAccNoOpenLic;
    }

    public void setBasicDepAccNoOpenLic(String basicDepAccNoOpenLic) {
        this.basicDepAccNoOpenLic = basicDepAccNoOpenLic;
    }

    public String getBasicDepAccNo() {
        return basicDepAccNo;
    }

    public void setBasicDepAccNo(String basicDepAccNo) {
        this.basicDepAccNo = basicDepAccNo;
    }

    public String getIsBankBasicDepAccNo() {
        return isBankBasicDepAccNo;
    }

    public void setIsBankBasicDepAccNo(String isBankBasicDepAccNo) {
        this.isBankBasicDepAccNo = isBankBasicDepAccNo;
    }

    public String getBasicDepAccob() {
        return basicDepAccob;
    }

    public void setBasicDepAccob(String basicDepAccob) {
        this.basicDepAccob = basicDepAccob;
    }

    public String getBasicAccNoOpenDate() {
        return basicAccNoOpenDate;
    }

    public void setBasicAccNoOpenDate(String basicAccNoOpenDate) {
        this.basicAccNoOpenDate = basicAccNoOpenDate;
    }

    public String getCommonAccNoOpenDate() {
        return commonAccNoOpenDate;
    }

    public void setCommonAccNoOpenDate(String commonAccNoOpenDate) {
        this.commonAccNoOpenDate = commonAccNoOpenDate;
    }

    public String getMainPrdDesc() {
        return mainPrdDesc;
    }

    public void setMainPrdDesc(String mainPrdDesc) {
        this.mainPrdDesc = mainPrdDesc;
    }

    public String getCreditLevelOuter() {
        return creditLevelOuter;
    }

    public void setCreditLevelOuter(String creditLevelOuter) {
        this.creditLevelOuter = creditLevelOuter;
    }

    public String getEvalDate() {
        return evalDate;
    }

    public void setEvalDate(String evalDate) {
        this.evalDate = evalDate;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEvalOrgId() {
        return evalOrgId;
    }

    public void setEvalOrgId(String evalOrgId) {
        this.evalOrgId = evalOrgId;
    }

    public String getLinkmanEmail() {
        return linkmanEmail;
    }

    public void setLinkmanEmail(String linkmanEmail) {
        this.linkmanEmail = linkmanEmail;
    }

    public String getSendAddr() {
        return sendAddr;
    }

    public void setSendAddr(String sendAddr) {
        this.sendAddr = sendAddr;
    }

    public String getInitLoanDate() {
        return initLoanDate;
    }

    public void setInitLoanDate(String initLoanDate) {
        this.initLoanDate = initLoanDate;
    }

    public String getWechatNo() {
        return wechatNo;
    }

    public void setWechatNo(String wechatNo) {
        this.wechatNo = wechatNo;
    }

    public String getIsStrgcCus() {
        return isStrgcCus;
    }

    public void setIsStrgcCus(String isStrgcCus) {
        this.isStrgcCus = isStrgcCus;
    }

    public String getFinaReportType() {
        return finaReportType;
    }

    public void setFinaReportType(String finaReportType) {
        this.finaReportType = finaReportType;
    }

    public String getAreaPriorCorp() {
        return areaPriorCorp;
    }

    public void setAreaPriorCorp(String areaPriorCorp) {
        this.areaPriorCorp = areaPriorCorp;
    }

    public String getSpOperFlag() {
        return spOperFlag;
    }

    public void setSpOperFlag(String spOperFlag) {
        this.spOperFlag = spOperFlag;
    }

    public String getIsNewBuildCorp() {
        return isNewBuildCorp;
    }

    public void setIsNewBuildCorp(String isNewBuildCorp) {
        this.isNewBuildCorp = isNewBuildCorp;
    }

    public String getGrpCusType() {
        return grpCusType;
    }

    public void setGrpCusType(String grpCusType) {
        this.grpCusType = grpCusType;
    }

    public String getRegiOrg() {
        return regiOrg;
    }

    public void setRegiOrg(String regiOrg) {
        this.regiOrg = regiOrg;
    }

    public String getMainBusNation() {
        return mainBusNation;
    }

    public void setMainBusNation(String mainBusNation) {
        this.mainBusNation = mainBusNation;
    }

    public String getCreditPadAccNo() {
        return creditPadAccNo;
    }

    public void setCreditPadAccNo(String creditPadAccNo) {
        this.creditPadAccNo = creditPadAccNo;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getOperPlaceOwnshp() {
        return operPlaceOwnshp;
    }

    public void setOperPlaceOwnshp(String operPlaceOwnshp) {
        this.operPlaceOwnshp = operPlaceOwnshp;
    }

    public BigDecimal getOperPlaceSqu() {
        return operPlaceSqu;
    }

    public void setOperPlaceSqu(BigDecimal operPlaceSqu) {
        this.operPlaceSqu = operPlaceSqu;
    }

    public String getMainProduceEquip() {
        return mainProduceEquip;
    }

    public void setMainProduceEquip(String mainProduceEquip) {
        this.mainProduceEquip = mainProduceEquip;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getIisSzjrfwCrop() {
        return iisSzjrfwCrop;
    }

    public void setIisSzjrfwCrop(String iisSzjrfwCrop) {
        this.iisSzjrfwCrop = iisSzjrfwCrop;
    }

    public String getIsSteelCus() {
        return isSteelCus;
    }

    public void setIsSteelCus(String isSteelCus) {
        this.isSteelCus = isSteelCus;
    }

    public String getIsStockCorp() {
        return isStockCorp;
    }

    public void setIsStockCorp(String isStockCorp) {
        this.isStockCorp = isStockCorp;
    }

    public String getIsCtinve() {
        return isCtinve;
    }

    public void setIsCtinve(String isCtinve) {
        this.isCtinve = isCtinve;
    }

    public String getCtinveLevel() {
        return ctinveLevel;
    }

    public void setCtinveLevel(String ctinveLevel) {
        this.ctinveLevel = ctinveLevel;
    }

    public String getGoverInvestPlat() {
        return goverInvestPlat;
    }

    public void setGoverInvestPlat(String goverInvestPlat) {
        this.goverInvestPlat = goverInvestPlat;
    }

    public String getImpexpFlag() {
        return impexpFlag;
    }

    public void setImpexpFlag(String impexpFlag) {
        this.impexpFlag = impexpFlag;
    }

    public String getRedcbizUnitTradeClass() {
        return redcbizUnitTradeClass;
    }

    public void setRedcbizUnitTradeClass(String redcbizUnitTradeClass) {
        this.redcbizUnitTradeClass = redcbizUnitTradeClass;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getIslocked() {
        return islocked;
    }

    public void setIslocked(String islocked) {
        this.islocked = islocked;
    }

    public String getFinalRank() {
        return finalRank;
    }

    public void setFinalRank(String finalRank) {
        this.finalRank = finalRank;
    }

    public String getDueDt() {
        return dueDt;
    }

    public void setDueDt(String dueDt) {
        this.dueDt = dueDt;
    }

    @Override
    public String toString() {
        return "CmisCus0014RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusNameEn='" + cusNameEn + '\'' +
                ", country='" + country + '\'' +
                ", corpQlty='" + corpQlty + '\'' +
                ", cityType='" + cityType + '\'' +
                ", assTotal=" + assTotal +
                ", adminSubRel='" + adminSubRel + '\'' +
                ", subTyp='" + subTyp + '\'' +
                ", investMbody='" + investMbody + '\'' +
                ", holdType='" + holdType + '\'' +
                ", tradeClass='" + tradeClass + '\'' +
                ", cusCcrModelId='" + cusCcrModelId + '\'' +
                ", cusCcrModelName='" + cusCcrModelName + '\'' +
                ", cllType2='" + cllType2 + '\'' +
                ", buildDate='" + buildDate + '\'' +
                ", fjobNum=" + fjobNum +
                ", salVolume=" + salVolume +
                ", operIncome=" + operIncome +
                ", corpScale='" + corpScale + '\'' +
                ", cusScaleForm='" + cusScaleForm + '\'' +
                ", natEcoSec='" + natEcoSec + '\'' +
                ", unitAlleInd='" + unitAlleInd + '\'' +
                ", alleBunb=" + alleBunb +
                ", framInd='" + framInd + '\'' +
                ", farmerCopInd='" + farmerCopInd + '\'' +
                ", agrInd='" + agrInd + '\'' +
                ", inclusiveFinanceStatistics='" + inclusiveFinanceStatistics + '\'' +
                ", insCode='" + insCode + '\'' +
                ", insRegDate='" + insRegDate + '\'' +
                ", insEndDate='" + insEndDate + '\'' +
                ", insOrg='" + insOrg + '\'' +
                ", insAnnDate='" + insAnnDate + '\'' +
                ", licenseType='" + licenseType + '\'' +
                ", regiCode='" + regiCode + '\'' +
                ", regiType='" + regiType + '\'' +
                ", quliGarade='" + quliGarade + '\'' +
                ", adminOrg='" + adminOrg + '\'' +
                ", apprOrg='" + apprOrg + '\'' +
                ", apprDocNo='" + apprDocNo + '\'' +
                ", regiAreaCode='" + regiAreaCode + '\'' +
                ", regiAddr='" + regiAddr + '\'' +
                ", regiAddrEn='" + regiAddrEn + '\'' +
                ", acuStateCode='" + acuStateCode + '\'' +
                ", operAddrAct='" + operAddrAct + '\'' +
                ", mainOptScp='" + mainOptScp + '\'' +
                ", partOptScp='" + partOptScp + '\'' +
                ", regiCurType='" + regiCurType + '\'' +
                ", regiCapAmt=" + regiCapAmt +
                ", paidCapCurType='" + paidCapCurType + '\'' +
                ", paidCapAmt=" + paidCapAmt +
                ", regiStartDate='" + regiStartDate + '\'' +
                ", regiEndDate='" + regiEndDate + '\'' +
                ", regAudit='" + regAudit + '\'' +
                ", regAuditDate='" + regAuditDate + '\'' +
                ", regAuditEndDate='" + regAuditEndDate + '\'' +
                ", natTaxRegCode='" + natTaxRegCode + '\'' +
                ", natTaxRegOrg='" + natTaxRegOrg + '\'' +
                ", natTaxRegDt='" + natTaxRegDt + '\'' +
                ", natTaxRegEndDt='" + natTaxRegEndDt + '\'' +
                ", natTaxAnnDate='" + natTaxAnnDate + '\'' +
                ", locTaxRegCode='" + locTaxRegCode + '\'' +
                ", locTaxRegOrg='" + locTaxRegOrg + '\'' +
                ", locTaxRegDt='" + locTaxRegDt + '\'' +
                ", locTaxRegEndDt='" + locTaxRegEndDt + '\'' +
                ", locTaxAnnDate='" + locTaxAnnDate + '\'' +
                ", loanCardFlg='" + loanCardFlg + '\'' +
                ", loanCardId='" + loanCardId + '\'' +
                ", loanCardPwd='" + loanCardPwd + '\'' +
                ", loanCardEffFlg='" + loanCardEffFlg + '\'' +
                ", loanCardAnnDate='" + loanCardAnnDate + '\'' +
                ", certIdate='" + certIdate + '\'' +
                ", conType='" + conType + '\'' +
                ", corpOwnersType='" + corpOwnersType + '\'' +
                ", isBankShd='" + isBankShd + '\'' +
                ", isSmconCus='" + isSmconCus + '\'' +
                ", detailAddr='" + detailAddr + '\'' +
                ", produceEquipYear='" + produceEquipYear + '\'' +
                ", qq='" + qq + '\'' +
                ", produceAbiYear='" + produceAbiYear + '\'' +
                ", isNatctl='" + isNatctl + '\'' +
                ", freqLinkman='" + freqLinkman + '\'' +
                ", natctlLevel='" + natctlLevel + '\'' +
                ", licOperPro='" + licOperPro + '\'' +
                ", freqLinkmanTel='" + freqLinkmanTel + '\'' +
                ", commonOperPro='" + commonOperPro + '\'' +
                ", operStatus='" + operStatus + '\'' +
                ", isLongVld='" + isLongVld + '\'' +
                ", basicDepAccNoOpenLic='" + basicDepAccNoOpenLic + '\'' +
                ", basicDepAccNo='" + basicDepAccNo + '\'' +
                ", isBankBasicDepAccNo='" + isBankBasicDepAccNo + '\'' +
                ", basicDepAccob='" + basicDepAccob + '\'' +
                ", basicAccNoOpenDate='" + basicAccNoOpenDate + '\'' +
                ", commonAccNoOpenDate='" + commonAccNoOpenDate + '\'' +
                ", mainPrdDesc='" + mainPrdDesc + '\'' +
                ", creditLevelOuter='" + creditLevelOuter + '\'' +
                ", evalDate='" + evalDate + '\'' +
                ", fax='" + fax + '\'' +
                ", evalOrgId='" + evalOrgId + '\'' +
                ", linkmanEmail='" + linkmanEmail + '\'' +
                ", sendAddr='" + sendAddr + '\'' +
                ", initLoanDate='" + initLoanDate + '\'' +
                ", wechatNo='" + wechatNo + '\'' +
                ", isStrgcCus='" + isStrgcCus + '\'' +
                ", finaReportType='" + finaReportType + '\'' +
                ", areaPriorCorp='" + areaPriorCorp + '\'' +
                ", spOperFlag='" + spOperFlag + '\'' +
                ", isNewBuildCorp='" + isNewBuildCorp + '\'' +
                ", grpCusType='" + grpCusType + '\'' +
                ", regiOrg='" + regiOrg + '\'' +
                ", mainBusNation='" + mainBusNation + '\'' +
                ", creditPadAccNo='" + creditPadAccNo + '\'' +
                ", loanType='" + loanType + '\'' +
                ", operPlaceOwnshp='" + operPlaceOwnshp + '\'' +
                ", operPlaceSqu=" + operPlaceSqu +
                ", mainProduceEquip='" + mainProduceEquip + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", oprType='" + oprType + '\'' +
                ", iisSzjrfwCrop='" + iisSzjrfwCrop + '\'' +
                ", isSteelCus='" + isSteelCus + '\'' +
                ", isStockCorp='" + isStockCorp + '\'' +
                ", isCtinve='" + isCtinve + '\'' +
                ", ctinveLevel='" + ctinveLevel + '\'' +
                ", goverInvestPlat='" + goverInvestPlat + '\'' +
                ", impexpFlag='" + impexpFlag + '\'' +
                ", redcbizUnitTradeClass='" + redcbizUnitTradeClass + '\'' +
                ", cusType='" + cusType + '\'' +
                ", cusName='" + cusName + '\'' +
                ", dueDt='" + dueDt + '\'' +
                ", finalRank='" + finalRank + '\'' +
                ", islocked='" + islocked + '\'' +
                '}';
    }
}