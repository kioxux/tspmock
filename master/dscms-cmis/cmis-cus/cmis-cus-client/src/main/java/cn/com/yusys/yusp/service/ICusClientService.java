package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.CusClientHandleDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * 客户服务对外提供服务接口
 */
@FeignClient(name = "cmis-cus", path = "/api", fallback = CusClientServiceImpl.class)
public interface ICusClientService {
    /**
     * 不宜贷款客户查询接口
     *
     * @param queryCusPubBlacklistRsDto
     * @return
     */
    @PostMapping("/cuspubblacklistrs/queryCusPubBlacklistRsClientDto")
    public QueryCusPubBlacklistRsDto queryCusPubBlacklistRs(QueryCusPubBlacklistRsDto queryCusPubBlacklistRsDto);

    /**
     * 外部服务调用删除接口
     *
     * @param cusClientHandleDto
     * @return
     */
    @PostMapping("/cusclienthandle/handleDeleteBySerno")
    public CusClientHandleDto handleDeleteBySerno(CusClientHandleDto cusClientHandleDto);

    /**
     * @方法名称: queryCusGrpMemberRelDtoList
     * @方法描述: 根据集团客户编号查询成员列表信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-15 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cusgrpmemberrel/querycusgrpmemberreldtolist")
    public ResultDto<List<CusGrpMemberRelDto>> queryCusGrpMemberRelDtoList(@RequestBody CusGrpMemberRelDto cusGrpMemberRelDto);

    /**
     * 客户查询接口
     */
    @PostMapping("/cusbase/queryCusBase")
    public CusBaseQueryRespDto queryCusBase(CusBaseQueryRespDto cusBaseQueryRespDto);

    /**
     * 个人客户建立信贷关系时间修改接口
     *
     * @param cusIndivReqClientDto
     * @return
     */
    @PostMapping("/cusindiv/updateComInitLoanDate")
    public int updateComInitLoanDate(@RequestBody CusIndivReqClientDto cusIndivReqClientDto);


    /**
     * 获取集团客户树
     *
     * @param cusGrpMemberTreeClientDto
     * @return List<CusGrpMemberTreeClientDto>
     */

    @PostMapping("/cusgrpmember/getCusGrpMemberTree")
    public List<CusGrpMemberTreeClientDto> getCusGrpMemberTree(CusGrpMemberTreeClientDto cusGrpMemberTreeClientDto);

    /**
     * 获取客户信息
     *
     * @param cusId
     * @return CusBaseClientDto
     */

    @PostMapping("/cusbase/queryCus")
    public CusBaseClientDto queryCus(@RequestBody String cusId);

    /**
     * 根据证件号获取客户信息
     *
     * @param certCode
     * @return CusBaseClientDto
     */

    @PostMapping("/cusbase/queryCusInfo")
    public CusBaseClientDto queryCusByCertCode(@RequestBody String certCode);

    /**
     * 根据客户编号获取对公客户基本信息
     *
     * @param cusId
     * @return CusCorpDto
     */

    @PostMapping("/cuscorp/queryCusCropDtoByCusId")
    public ResultDto<CusCorpDto> queryCusCropDtoByCusId(@RequestBody String cusId);

    /**
     * @Description:根据客户号删除客户在cusBase表中的信息(逻辑删除),返回删除成功标志
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:15
     * @param cusId:客户号
     * @return: cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     **/
    @PostMapping("/cusbase/deleteCusBaseByCusId")
    public  ResultDto<Boolean> deleteCusBaseByCusId(@RequestBody String cusId);

    /**
     * @Description:根据客户号删除客户在CusCorp表中的信息(逻辑删除),返回删除成功标志
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:15
     * @param cusId:客户号
     * @return: cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     **/
    @PostMapping("/cuscorp/deleteCusCorpByCusId")
    public  ResultDto<Boolean> deleteCusCorpByCusId(@RequestBody String cusId);

    /**
     * 交易码：cusUpdateInitLoanDateDto
     * 交易描述：首笔授信申请审批通过，更新个人客户信息表建立信贷关系时间
     * @param cusUpdateInitLoanDateDto
     * @return
     */
    @PostMapping("/cusbase/updateinitloandate")
    public ResultDto<Integer> updateInitLoanDate(CusUpdateInitLoanDateDto cusUpdateInitLoanDateDto);

    @PostMapping("/cusindivcontact/selectByPrimaryKey")
    public CusIndivAllDto queryCusContactByCusId(@RequestBody String cusId);

    @PostMapping("/cusbase/updateSelective")
    public ResultDto<Integer> update(@RequestBody CusBaseClientDto cusBase);

    @PostMapping("/cusbase/updateSelectivenew")
    public ResultDto<Integer> updateCusbase(@RequestBody CusBaseClientDto cusBase);

    @PostMapping("/cusindivcontact/queryCusIndivByCusId")
    public CusIndivContactDto queryCusIndivByCusId(@RequestBody String cusId);
    /**
     * 交易码：/cuscomgrade/query
     * 交易描述：根据流水号查询客户评级信息
     * @param serno
     * @return
     */
    @PostMapping("/cuscomgrade/query")
    public ResultDto<CusComGradeDto> query(@RequestBody String serno) ;
    /**
     * 根据客户号查询最新的客户评级信息
     * @param cusId
     * @return
     */
    @PostMapping("/cuscomgrade/selectGradeInfoByCusId")
    public ResultDto<Map<String,String>> selectGradeInfoByCusId(@RequestBody String cusId);

    /**
     * 根据客户号查询问题授信名单信息
     * @param cusId
     * @return
     */
    @PostMapping("/cuslstwtsx/querycuslstwtsxbycusId")
    public ResultDto<CusLstWtsxDto> queryCuslstwtsxByCusId(@RequestBody String cusId);


    /**
     * 根据客户号查询问题关联名单信息
     * @param cusId
     * @return
     */
    @PostMapping("/cuslstglf/querycuslstglfbycusid")
    public ResultDto<CusLstGlfDto> queryCusLstGlfByCusId(@RequestBody String cusId);

    /**
     * 根据客户号查询关联额度信息
     * @param cusId
     * @return
     */
    @PostMapping("/cuslstglfgljyyjed/querycuslstglfgljyyjeddtobycusid")
    public ResultDto<CusLstGlfGljyyjedDto> queryCusLstGlfGljyyjedDtoByCusId(@RequestBody String cusId);

    /**
     * @创建人 WH
     * @创建时间 2021/8/4 15:33
     * @注释 根据客户号查询增享贷额外参数
     */
    @PostMapping("/cusbase/selectbycusidforzxd")
    public ResultDto<CusForZxdDto> selectbycusidforzxd(@RequestBody String cusId);

    /**
     * 根据集团号查询集团信息
     * @param grpNo
     * @return
     */
    @PostMapping("/cusgrp/selectCusGrpDtoByGrpNo")
    public ResultDto<CusGrpDto> selectCusGrpDtoByGrpNo(@RequestBody String grpNo);

    /**
     * 根据客户关联号查询出资人信息
     * @param cusIdRel
     * @return
     */
    @PostMapping("/cuscorpapital/selectByCusIdRel")
    public ResultDto<List<CusCorpApitalDto>> selectByCusIdRel(@RequestBody String cusIdRel);

    @PostMapping("/cuscorpcert/selectByCusId")
    public ResultDto<List<CusCorpCertDto>> selectByCusId(@RequestBody String cusId);

    /**
     * 根据客户编号查询协办客户经理
     * @param
     * @return
     */
    @PostMapping("/cusmgrdivideperc/selectXBManagerId")
    public ResultDto<CusMgrDividePercDto> selectXBManagerId(@RequestBody String cusId);

    /**
     * 更新生效状态
     * @param cusId
     * @return
     */
    @PostMapping("/cuslstdedkkh/updateStatusPass")
    public ResultDto<Integer> updateStatusPass(@RequestBody String cusId);

    /**
     * 更新失效状态
     * @param listSerno
     * @return
     */
    @PostMapping("/cuslstdedkkh/updateStatusRefuse")
    public ResultDto<Integer> updateStatusRefuse(@RequestBody String listSerno);

    @PostMapping("/cuslstdedkkh/queryNewDekhByCusId")
    public Map queryNewDekhByCusId(@RequestBody String cusId);

    @PostMapping("/cuscomgrade/getGradeInfoByCusId")
    ResultDto<Map<String, String>> getGradeInfoByCusId(String cusId);

    /**
     * 根据客户编号查询信息插入被告人信息表
     * @param
     * @return
     */
    @PostMapping("/cusbase/queryCusBaseByCusId")
    public ResultDto<CusBaseDto> queryCusBaseByCusId(@RequestBody String cusId);

    /**
     * 根据客户编号查询证件信息
     * @param
     * @return
     */
    @PostMapping("/cuscorpcert/queryCusCorpCertDataByParams")
    public List<CusCorpCertDto> queryCusCorpCertDataByParams(@RequestBody Map map);

    /**
     * 获取客户号对应的财务指标分析所需内容
     * @param
     * @return
     */
    @PostMapping("/nrcs-cms/fncstatbase/selectfinanindicanalysfncitemdatabycusid")
    public ResultDto<List<FinanIndicAnalyDto>> selectFinanindicAnalysFncItemDataByCusId(@RequestBody String cusId);

    /**
     * 调查报告去财报利润总额字段
     * @param map
     * @return
     */
    @PostMapping("/nrcs-cms/fncstatbase/getRptFncTotalProfit")
    public ResultDto<List<FinanIndicAnalyDto>> getRptFncTotalProfit(@RequestBody Map map);

    /**
     * 根据客户编号查询信息插入被告人信息表
     * @param
     * @return
     */
    @PostMapping("/nrcs-cms/fncstatbase/getRptFncTotalProfitForLmtHighCurfundEval")
    public ResultDto<Map<String,FinanIndicAnalyDto>> getRptFncTotalProfitForLmtHighCurfundEval(@RequestBody String cusId);

    /**
     * 财报科目净利润连续两年取值，上一年度或当期资产负债表的资产负债率取值
     * @创建者：zhangliang15
     * @param
     * @return
     */
    @PostMapping("/nrcs-cms/fncstatbase/getFinRepRetProAndRatOfLia")
    public ResultDto<Map<String,FinanIndicAnalyDto>> getFinRepRetProAndRatOfLia(@RequestBody String cusId);

    /**
     * @方法名称：queryCusBaseCropByCusId
     * @方法描述：
     * @创建人：liuquan
     * 根据客户编号查询信息插入被告人信息表
     * @周茂伟
     */
    @PostMapping("/cusbase/queryCusBaseCropByCusId")
    public ResultDto<CusBaseDto> queryCusBaseCropByCusId(@RequestBody String cusId);

    /**
     * @方法名称：queryCusBaseByCusId
     * @方法描述：
     * @创建人：liuquan
     * 根据客户编号查询信息插入被告人信息表
     * @周茂伟
     */
    @PostMapping("/cusbase/queryCusBaseIndivByCusId")
    public ResultDto<CusBaseDto> queryCusBaseIndivByCusId(@RequestBody String cusId);


    /**
     * @方法名称：queryCusIndivAttrByCusId
     * @方法描述：
     * @创建人：zhangliang15
     * 根据客户编号查询个人客户属性信息表
     */
    @PostMapping("/cusindivattr/queryCusIndivAttrByCusId")
    public ResultDto<CusIndivAttrDto> queryCusIndivAttrByCusId(@RequestBody String cusId);

    @PostMapping("/nrcs-cms/fncstatbase/selectFncByCusIdRptYear")
    public ResultDto<Map> selectFncByCusIdRptYear(@RequestBody Map map);
}
