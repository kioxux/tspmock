package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.req.Xdkh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.resp.Xdkh0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.req.Xdkh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.resp.Xdkh0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0005.req.Xdkh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0005.resp.Xdkh0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0007.req.Xdkh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0007.resp.Xdkh0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0008.req.Xdkh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0008.resp.Xdkh0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0011.req.Xdkh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0011.resp.Xdkh0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0012.req.Xdkh0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0012.resp.Xdkh0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0013.req.Xdkh0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0013.resp.Xdkh0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0014.req.Xdkh0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0014.resp.Xdkh0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0015.req.Xdkh0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0015.resp.Xdkh0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0016.req.Xdkh0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0016.resp.Xdkh0016DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0018.req.Xdkh0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0018.resp.Xdkh0018DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.req.Xdkh0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.resp.Xdkh0019DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0020.req.Xdkh0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0020.resp.Xdkh0020DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0021.req.Xdkh0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0021.resp.Xdkh0021DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0024.req.Xdkh0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0024.resp.Xdkh0024DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0025.req.Xdkh0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0025.resp.Xdkh0025DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0026.req.Xdkh0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0026.resp.Xdkh0026DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0027.req.Xdkh0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0027.resp.Xdkh0027DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0028.req.Xdkh0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0028.resp.Xdkh0028DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0030.req.Xdkh0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0030.resp.Xdkh0030DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0031.req.Xdkh0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0031.resp.Xdkh0031DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0032.req.Xdkh0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0032.resp.Xdkh0032DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0033.req.Xdkh0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0033.resp.Xdkh0033DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0034.req.Xdkh0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0034.resp.Xdkh0034DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0035.req.Xdkh0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0035.resp.Xdkh0035DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0036.req.Xdkh0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0036.resp.Xdkh0036DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0037.req.Xdkh0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0037.resp.Xdkh0037DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0008.req.Xdqt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0008.resp.Xdqt0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0067.req.Xdxw0067DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0067.resp.Xdxw0067DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsCusClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:客户管理模块
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-cus", path = "/api", fallback = DscmsCusClientServiceImpl.class)
public interface DscmsCusClientService {

    /**
     * 交易码：xdkh0001
     * 交易描述：个人客户基本信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0001")
    public ResultDto<Xdkh0001DataRespDto> xdkh0001(Xdkh0001DataReqDto reqDto);

    /**
     * 交易码：xdkh0005
     * 交易描述：同业客户信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0005")
    public ResultDto<Xdkh0005DataRespDto> xdkh0005(Xdkh0005DataReqDto reqDto);

    /**
     * 交易码：xdkh0008
     * 交易描述：集团关联信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0008")
    public ResultDto<Xdkh0008DataRespDto> xdkh0008(Xdkh0008DataReqDto reqDto);

    /**
     * 交易码：xdkh0015
     * 交易描述：农拍档接受白名单维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0015")
    public ResultDto<Xdkh0015DataRespDto> xdkh0015(Xdkh0015DataReqDto reqDto);

    /**
     * 交易码：xdkh0020
     * 交易描述：小微平台请求信贷线上开户
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0020")
    public ResultDto<Xdkh0020DataRespDto> xdkh0020(Xdkh0020DataReqDto reqDto);

    /**
     * 交易码：xdkh0021
     * 交易描述：在信贷系统中生成用户信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0021")
    public ResultDto<Xdkh0021DataRespDto> xdkh0021(Xdkh0021DataReqDto reqDto);

    /**
     * 交易码：xdkh0014
     * 交易描述：省心E付白名单信息维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0014")
    public ResultDto<Xdkh0014DataRespDto> xdkh0014(Xdkh0014DataReqDto reqDto);

    /**
     * 交易码：xdkh0002
     * 交易描述：对公客户基本信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0002")
    public ResultDto<Xdkh0002DataRespDto> xdkh0002(Xdkh0002DataReqDto reqDto);

    /**
     * 交易码：xdkh0004
     * 交易描述：查询客户配偶信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0004")
    public ResultDto<Xdkh0004DataRespDto> xdkh0004(Xdkh0004DataReqDto reqDto);

    /**
         * 交易码：xdkh0007
     * 交易描述：查询客户是否本行员工及直属亲属
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0007")
    public ResultDto<Xdkh0007DataRespDto> xdkh0007(Xdkh0007DataReqDto reqDto);

    /**
     * 交易码：xdkh0013
     * 交易描述：优享贷客户白名单信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0013")
    public ResultDto<Xdkh0013DataRespDto> xdkh0013(Xdkh0013DataReqDto reqDto);

    /**
     * 交易码：xdkh0016
     * 交易描述：查询是否我行关系人
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0016")
    public ResultDto<Xdkh0016DataRespDto> xdkh0016(Xdkh0016DataReqDto reqDto);

    /**
     * 交易码：xdkh0018
     * 交易描述：临时客户信息维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0018")
    public ResultDto<Xdkh0018DataRespDto> xdkh0018(Xdkh0018DataReqDto reqDto);

    /**
     * 交易码：xdkh0019
     * 交易描述：客户查询并开户
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0019")
    public ResultDto<Xdkh0019DataRespDto> xdkh0019(Xdkh0019DataReqDto reqDto);

    /**
     * 交易码：xdkh0024
     * 交易描述：优企贷、优农贷客户基本信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0024")
    public ResultDto<Xdkh0024DataRespDto> xdkh0024(Xdkh0024DataReqDto reqDto);

    /**
     * 交易码：xdkh0025
     * 交易描述：优企贷、优农贷客户信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0025")
    public ResultDto<Xdkh0025DataRespDto> xdkh0025(Xdkh0025DataReqDto reqDto);

    /**
     * 交易码：xdkh0026
     * 交易描述：优企贷、优农贷行内关联人基本信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0026")
    public ResultDto<Xdkh0026DataRespDto> xdkh0026(Xdkh0026DataReqDto reqDto);

    /**
     * 交易码：xdkh0027
     * 交易描述：优企贷、优农贷行内关联自然人基本信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0027")
    public ResultDto<Xdkh0027DataRespDto> xdkh0027(Xdkh0027DataReqDto reqDto);

    /**
     * 交易码：xdkh0011
     * 交易描述：对私客户信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0011")
    public ResultDto<Xdkh0011DataRespDto> xdkh0011(Xdkh0011DataReqDto reqDto);

    /**
     * 交易码：xdkh0012
     * 交易描述：对公客户信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0012")
    public ResultDto<Xdkh0012DataRespDto> xdkh0012(Xdkh0012DataReqDto reqDto);

    /**
     * 交易码：xdkh0028
     * 交易描述：优农贷黑名单查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0028")
    public ResultDto<Xdkh0028DataRespDto> xdkh0028(Xdkh0028DataReqDto reqDto);

    /**
     * 交易码：xdkh0030
     * 交易描述：公司客户评级相关信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0030")
    public ResultDto<Xdkh0030DataRespDto> xdkh0030(Xdkh0030DataReqDto reqDto);

    /**
     * 交易码：xdkh0031
     * 交易描述：同业客户评级相关信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0031")
    public ResultDto<Xdkh0031DataRespDto> xdkh0031(Xdkh0031DataReqDto reqDto);

    /**
     * 交易码：xdkh0032
     * 交易描述：信息锁定标志同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0032")
    public ResultDto<Xdkh0032DataRespDto> xdkh0032(Xdkh0032DataReqDto reqDto);

    /**
     * 交易码：xdkh0033
     * 交易描述：客户评级结果同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0033")
    public ResultDto<Xdkh0033DataRespDto> xdkh0033(Xdkh0033DataReqDto reqDto);

    /**
     * 交易码：xdkh0034
     * 交易描述：查询企业在我行客户评级信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0034")
    public ResultDto<Xdkh0034DataRespDto> xdkh0034(Xdkh0034DataReqDto reqDto);

//    /**
//     * 交易码：xdkh0023
//     * 交易描述：还款试算计划查询日期
//     *
//     * @param reqDto
//     * @return
//     */
//    @PostMapping("/cus4bsp/xdkh0023")
//    public ResultDto<Xdkh0023DataRespDto> xdkh0023(Xdkh0023DataReqDto reqDto);

    /**
     * 交易码：xdkh0035
     * 交易描述：违约认定、重生认定评级信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0035")
    public ResultDto<Xdkh0035DataRespDto>  xdkh0035(Xdkh0035DataReqDto reqDto);

    /**
     * 交易码：xdkh0036
     * 交易描述： 对公客户接口查询接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0036")
    ResultDto<Xdkh0036DataRespDto> xdkh0036(Xdkh0036DataReqDto reqDto);

    /**
     * 交易码：xdkh0037
     * 交易描述： 微业贷新开户信息入库
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdkh0037")
    ResultDto<Xdkh0037DataRespDto> xdkh0037(Xdkh0037DataReqDto reqDto);

    /**
     * 交易码：xdqt0008
     * 交易描述：乐悠金卡关联人查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizqt4bsp/xdqt0008")
    public ResultDto<Xdqt0008DataRespDto>  xdqt0008(Xdqt0008DataReqDto reqDto);

    /**
     * 交易码：xdxw0066
     * 交易描述：调查基本信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4bsp/xdxw0067")
    public ResultDto<Xdxw0067DataRespDto> xdxw0067(Xdxw0067DataReqDto reqDto);


}
