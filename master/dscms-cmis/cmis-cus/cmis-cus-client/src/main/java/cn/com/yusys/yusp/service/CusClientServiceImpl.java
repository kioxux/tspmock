package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.CusClientHandleDto;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CusCorpApitalDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * 客户服务对外提供服务接口实现类
 */
@Component
public class CusClientServiceImpl implements ICusClientService {
    private static final Logger logger = LoggerFactory.getLogger(CusClientServiceImpl.class);

    /**
     * 不宜贷款客户查询接口
     *
     * @param queryCusPubBlacklistRsDto
     * @return
     */
    @Override
    public QueryCusPubBlacklistRsDto queryCusPubBlacklistRs(QueryCusPubBlacklistRsDto queryCusPubBlacklistRsDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 外部服务调用删除接口
     *
     * @param cusClientHandleDto
     * @return
     */
    @Override
    public CusClientHandleDto handleDeleteBySerno(CusClientHandleDto cusClientHandleDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }


    /**
     * @方法名称: queryCusGrpMemberRelDtoList
     * @方法描述: 根据集团客户编号查询成员列表信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-15 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Override
    public ResultDto<List<CusGrpMemberRelDto>> queryCusGrpMemberRelDtoList(CusGrpMemberRelDto cusGrpMemberRelDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 查询客户信息
     *
     * @param cusBaseQueryRespDto
     * @return
     */
    @Override
    public CusBaseQueryRespDto queryCusBase(CusBaseQueryRespDto cusBaseQueryRespDto) {
        return null;
    }

    /**
     * 个人客户建立信贷关系时间修改接口
     *
     * @param cusIndivDto
     * @return
     */
    @Override
    public int updateComInitLoanDate(CusIndivReqClientDto cusIndivDto) {
        logger.error("访问失败，触发熔断。");
        return 0;
    }


    /**
     * 获取集团客户树
     *
     * @param cusGrpMemberTreeClientDto
     * @return List<CusGrpMemberTreeDto>
     */
    @Override
    public List<CusGrpMemberTreeClientDto> getCusGrpMemberTree(CusGrpMemberTreeClientDto cusGrpMemberTreeClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 获取客户信息
     *
     * @param cusId
     * @return CusBaseClientDto
     */

    @Override
    public CusBaseClientDto queryCus(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 根据证件获取客户信息
     *
     * @param certCode
     * @return CusBaseClientDto
     */
    @Override
    public CusBaseClientDto queryCusByCertCode(String certCode) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 根据客户编号获取对公客户基本信息
     *
     * @param cusId
     * @return CusCorpDto
     */
    @Override
    public ResultDto<CusCorpDto> queryCusCropDtoByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * @Description:根据客户号删除客户在cusBase表中的信息(逻辑删除),返回删除成功标志
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:15
     * @param cusId:客户号
     * @return: cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     **/
    @Override
    public ResultDto<Boolean> deleteCusBaseByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * @Description:根据客户号删除客户在CusCorp表中的信息(逻辑删除),返回删除成功标志
     * @Author: YX-WJ
     * @Date: 2021/6/10 20:15
     * @param cusId:客户号
     * @return: cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     **/
    @Override
    public ResultDto<Boolean> deleteCusCorpByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * @Description:首笔授信申请审批通过，更新个人客户信息表建立信贷关系时间
     * @Author: xuchi
     * @Date:
     * @param cusUpdateInitLoanDateDto
     * @return:cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     **/
    @Override
    public ResultDto<Integer> updateInitLoanDate(CusUpdateInitLoanDateDto cusUpdateInitLoanDateDto){
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public CusIndivAllDto queryCusContactByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> update(CusBaseClientDto cusBase){
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public CusIndivContactDto queryCusIndivByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusComGradeDto> query(String serno) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Map<String, String>> selectGradeInfoByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Map<String, String>> getGradeInfoByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusLstWtsxDto> queryCuslstwtsxByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusLstGlfDto> queryCusLstGlfByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }
    
    @Override
    public ResultDto<CusLstGlfGljyyjedDto> queryCusLstGlfGljyyjedDtoByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusForZxdDto> selectbycusidforzxd(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusGrpDto> selectCusGrpDtoByGrpNo(String grpNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CusCorpApitalDto>> selectByCusIdRel(String cusIdRel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<CusCorpCertDto>> selectByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusMgrDividePercDto> selectXBManagerId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> updateStatusPass(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> updateStatusRefuse(String listSerno) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public Map queryNewDekhByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<FinanIndicAnalyDto>> selectFinanindicAnalysFncItemDataByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<FinanIndicAnalyDto>> getRptFncTotalProfit(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Map<String, FinanIndicAnalyDto>> getRptFncTotalProfitForLmtHighCurfundEval(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Map<String, FinanIndicAnalyDto>> getFinRepRetProAndRatOfLia(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusBaseDto> queryCusBaseCropByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusBaseDto> queryCusBaseIndivByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusIndivAttrDto> queryCusIndivAttrByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Map> selectFncByCusIdRptYear(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<CusBaseDto> queryCusBaseByCusId(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public List<CusCorpCertDto> queryCusCorpCertDataByParams(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> updateCusbase(@RequestBody CusBaseClientDto cusBase) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

}
