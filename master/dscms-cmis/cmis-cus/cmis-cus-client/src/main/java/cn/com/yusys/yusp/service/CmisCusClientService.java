package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmiscus0001.req.CmisCus0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0003.req.CmisCus0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0003.resp.CmisCus0003RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0004.req.CmisCus0004ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0004.resp.CmisCus0004RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0005.req.CmisCus0005ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0005.resp.CmisCus0005RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.req.CmisCus0007ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0007.resp.CmisCus0007RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.req.CmisCus0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0008.resp.CmisCus0008RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.req.CmisCus0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0010.resp.CmisCus0010RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.req.CmisCus0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0011.resp.CmisCus0011RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.req.CmisCus0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0012.resp.CmisCus0012RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0013.resp.CmisCus0013RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.req.CmisCus0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0014.resp.CmisCus0014RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0015.req.CmisCus0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0015.resp.CmisCus0015RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0017.req.Cmiscus0017ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0017.resp.Cmiscus0017RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.req.CmisCus0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0019.resp.CmisCus0019RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.req.CmisCus0020ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0020.resp.CmisCus0020RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0021.req.CmisCus0021ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0021.resp.CmisCus0021RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0022.req.CmisCus0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0022.resp.CmisCus0022RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0023.req.CmisCus0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0023.resp.CmisCus0023RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0024.req.CmisCus0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0024.resp.CmisCus0024RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.req.CmisCus0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0025.resp.CmisCus0025RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0026.req.CmisCus0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0026.resp.CmisCus0026RespDto;
import cn.com.yusys.yusp.service.impl.CmisCusClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 封装的接口类:客户管理模块
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-cus", path = "/api", fallback = CmisCusClientServiceImpl.class)
public interface CmisCusClientService {

    /**
     * 交易码：cmiscus0001
     * 交易描述：集团成员列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0001")
    public ResultDto<CmisCus0001RespDto> cmiscus0001(CmisCus0001ReqDto reqDto);

    /**
     * 交易码：cmiscus0003
     * 交易描述：法人关联客户列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0003")
    public ResultDto<CmisCus0003RespDto> cmiscus0003(CmisCus0003ReqDto reqDto);

    /**
     * 交易码：cmiscus0004
     * 交易描述：个人关联客户列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0004")
    public ResultDto<CmisCus0004RespDto> cmiscus0004(CmisCus0004ReqDto reqDto);

    /**
     * 交易码：cmiscus0005
     * 交易描述：客户销售总额查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0005")
    public ResultDto<CmisCus0005RespDto> cmiscus0005(CmisCus0005ReqDto reqDto);

    /**
     * 交易码：cmiscus0006
     * 交易描述：查询客户基本信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0006")
    public ResultDto<CmisCus0006RespDto> cmiscus0006(CmisCus0006ReqDto reqDto);

    /**
     * 交易码：cmiscus0007
     * 交易描述：查询个人客户基本信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0007")
    public ResultDto<CmisCus0007RespDto> cmiscus0007(CmisCus0007ReqDto reqDto);

    /**
     * 交易码：cmiscus0011
     * 交易描述：查询优农贷名单信息
     * H
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0011")
    public ResultDto<CmisCus0011RespDto> cmiscus0011(CmisCus0011ReqDto reqDto);

    /**
     * 交易码：cmiscus0012
     * 交易描述：对公高管信息查询
     * H
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0012")
    public ResultDto<CmisCus0012RespDto> cmiscus0012(CmisCus0012ReqDto reqDto);

    /**
     * 交易码：cmiscus0025
     * 交易描述：优农贷名单信息查询
     * H
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0025")
    public ResultDto<CmisCus0025RespDto> cmiscus0025(CmisCus0025ReqDto reqDto);

    /**
     * @author zlf
     * @date 2021/5/4 16:41
     * @version 1.0.0
     * @desc 根据证件号码查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cusbase/queryCusInfo")
    public CusBaseClientDto queryCusByCertCode(String certCode);


    /**
     * @author zlf
     * @date 2021/5/4 16:41
     * @version 1.0.0
     * @desc 根据证件号码查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/queryCus")
    public CusBaseClientDto queryCus(String cusId);

    /**
     * 交易码：cmiscus0008
     * 交易描述：查询根据客户编号查询法人名称,名称，证件类型，证件号，电话
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0008")
    public ResultDto<CmisCus0008RespDto> cmiscus0008(CmisCus0008ReqDto reqDto);

    /**
     * 交易码：cmiscus0010
     * 交易描述：根据客户编号,证件类型，证件号码查询同业客户基本信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0010")
    public ResultDto<CmisCus0010RespDto> cmiscus0010(CmisCus0010ReqDto reqDto);

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectCusCorp
     * @函数描述:查询对公客户对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     * @创建人：xs
     */
    @PostMapping("/cuscorp/selectCusCorpDtoList")
    public ResultDto<List<CusCorpDto>> selectCusCorpDtoList(@RequestBody QueryModel queryModel);

    /**
     * @param cusId
     * @return
     */
    @PostMapping("/cusindivsocial/selectCusIndivSocialDtoList")
    public ResultDto<List<CusIndivSocialResp>> selectCusIndivSocialDtoList(@RequestParam("cusId") String cusId);

    /***
     * @param cusId
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<IndivCusQueryDto>
     * @author 王玉坤
     * @date 2021/5/17 15:39
     * @version 1.0.0
     * @desc 个人客户信息查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cusindiv/querycusindivbycusid")
    public ResultDto<CusIndivDto> queryCusindivByCusid(String cusId);

    /**
     * 查询个人客户基本信息
     *
     * @param queryModel
     * @return
     */
    @PostMapping("/cusindiv/queryAllCusIndiv")
    public ResultDto<List<CusIndivAllDto>> queryAllCusIndiv(@RequestBody QueryModel queryModel);

    /***
     * @param cusId
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<CusBaseDto>
     */
    @PostMapping("/cusbase/cusBaseInfo")
    public ResultDto<CusBaseDto> cusBaseInfo(@RequestBody String cusId);

    /**
     * 交易码：cmiscus0013
     * 交易描述：根据证件编号查询是否有配偶，有就返回配偶客户编号
     *
     * @param certCode
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0013")
    public ResultDto<CmisCus0013RespDto> cmiscus0013(String certCode);

    /**
     * 交易码：cmiscus0014
     * 交易描述：法人客户及股东信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0014")
    public ResultDto<CmisCus0014RespDto> cmiscus0014(CmisCus0014ReqDto reqDto);


    /**
     * 交易码：cmiscus0015
     * 交易描述：根据客户号更新行业信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0015")
    public ResultDto<CmisCus0015RespDto> cmiscus0015(CmisCus0015ReqDto reqDto);


    /**
     * 查询增享贷白名单
     *
     * @param serno
     * @return
     */
    @PostMapping("/cus4inner/cmiscuslstzxd")
    public ResultDto<CusLstZxdDto> cmiscuslstzxd(String serno);

    /**
     * 交易码：cmiscus0017
     * 交易描述：查询客户去年12期财报中最大的主营业务收入
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0017")
    public ResultDto<Cmiscus0017RespDto> cmiscus0017(Cmiscus0017ReqDto reqDto);

    /**
     * 交易码：cmiscus0018
     * 交易描述：更新增享贷白名单
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0018")
    public ResultDto<OperationRespDto> cmiscus0018(CusLstZxdDto reqDto);


    /**
     * 交易码：cmiscus0019
     * 交易描述：查询优农贷名单信息
     * H
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0019")
    public ResultDto<CmisCus0019RespDto> cmiscus0019(CmisCus0019ReqDto reqDto);

    /**
     * 交易码：cmiscus0018
     * 交易描述：更新增享贷白名单
     *
     * @param cusId
     * @return
     */
    @PostMapping("/cuslstwtsx/querycuslstwtsxbybusId")
    public ResultDto<CusLstWtsxDto> queryCuslstwtsxByCusId(String cusId);

    /**
     * 交易码：cmiscus0020
     * 交易描述：根据客户号查询关联方名单信息记录数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0020")
    public ResultDto<CmisCus0020RespDto> cmiscus0020(CmisCus0020ReqDto reqDto);

    /**
     * 交易码：cmiscus0021
     * 交易描述：优农贷（经营信息）记录生成
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0021")
    public ResultDto<CmisCus0021RespDto> cmiscus0021(CmisCus0021ReqDto reqDto);

    /**
     * 交易码：cmiscus0022
     * 交易描述：优农贷（经营信息）查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0022")
    public ResultDto<CmisCus0022RespDto> cmiscus0022(CmisCus0022ReqDto reqDto);

    /**
     * 交易码：cmiscus0023
     * 交易描述：根据大额行号查询客户号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0023")
    public ResultDto<CmisCus0023RespDto> cmiscus0023(CmisCus0023ReqDto reqDto);

    /**
     * 交易码：cmiscus0024
     * 交易描述：根据行号或BICCODE获取同业客户号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0024")
    public ResultDto<CmisCus0024RespDto> cmiscus0024(CmisCus0024ReqDto reqDto);

    /**
     * 交易码：cmiscus0026
     * 交易描述：客户联系信息维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/cus4inner/cmiscus0026")
    public ResultDto<CmisCus0026RespDto> cmiscus0026(CmisCus0026ReqDto reqDto);

    /**
     * 查询法人代表或实际控制人
     */
    @PostMapping("/cuscorpmgr/getCusCorpMgrByMrgType")
    public ResultDto<CusCorpMgrDto> getCusCorpMgrByMrgType(@RequestBody Map<String, Object> map);

    /***
     * @param cusId
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<IndivCusQueryDto>
     * @author shenli
     * @date 2021-7-3 14:19:14
     * @version 1.0.0
     * @desc 个人客户工作信息查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cusindivunit/selectbycusid")
    public ResultDto<CusIndivUnitDto> queryCusindivUnitByCusid(String cusId);

    /***
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<IndivCusQueryDto>
     * @author shenli
     * @date 2021-7-12 19:32:58
     * @version 1.0.0
     * @desc 个人客户查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cusindiv/selectbycondition")
    public ResultDto<List<CusIndivDto>> selectByCondition(QueryModel queryModel);

    @PostMapping("/cusindiv/queryCusIndiv")
    public ResultDto<List<CusIndivDto>> queryCusIndiv(QueryModel queryModel);


    /***
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<IndivCusQueryDto>
     * @author shenli
     * @date 2021-7-12 19:32:58
     * @version 1.0.0
     * @desc 个人客户查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cusindiv/selectDtoByCondition")
    public ResultDto<List<CusIndivDto>> selectDtoByCondition(QueryModel queryModel);

    /**
     * @创建人 WH
     * @创建时间 2021/8/5 13:55
     * @注释 根据客户编号和白名单状态
     */
    @PostMapping("/cuslstzxd/selectzxdbycusid")
    public ResultDto<CusLstZxdDto> selectzxdbycusid(@RequestBody CusLstZxdDto cusLstZxdDto);

    /**
     * 对公客户信息查询
     */
    @PostMapping("/cusbase/baseCrop")
    public ResultDto<List<CusBaseDto>> selectBaseCrop(@RequestBody QueryModel queryModel);


    @PostMapping("/cusgrpmemberapp/query")
    public ResultDto<List<CusGrpMemberAppDto>> queryCusGrpMemberApp(@RequestBody QueryModel queryModel);

    /**
     * 根据客户经理号获取其管户客户号
     **/
    @PostMapping("/cusbase/queryCusIdByManagerId")
    public ResultDto<List<String>> queryCusIdByManagerId(@RequestBody String managerId);

    /**
     * 追加无还本续贷名单
     **/
    @PostMapping("/cuslstwhbxd/addCusLstWhbxd")
    public ResultDto<Integer> addCusLstWhbxd(@RequestBody CusLstWhbxdDto cusLstWhbxdDto);

    /**
     * 追加无还本续贷名单
     **/
    @PostMapping("/cusaccountinfo/queryAccount")
    public ResultDto<List<CusAccountInfoDto>> queryAccount(String cusId);

    /**
     * 根据企业客户编号查询企业法人代表信息
     */
    @PostMapping("/cuscorpmgr/getCusCorpMgrByParams")
    public ResultDto<CusCorpMgrDto> getCusCorpMgrByParams(@RequestBody Map<String, Object> map);

    /**
     * 查询高管信息
     */
    @PostMapping("/cuscorpmgr/querycuscorpmgr")
    public ResultDto<List<CusCorpMgrDto>> queryCusCorpMgr(@RequestBody QueryModel queryModel);

    /**
     * 获取该客户及其所在集团成员客户最低的外部评级
     */
    @PostMapping("/cuscorp/queryminbankloanlevel")
    public ResultDto<String> queryMinBankLoanLevel(@RequestBody Map<String, String> map);

    /**
     * 获取该客户及其所在集团成员客户的信用评级信息
     */
    @PostMapping("/cuscomgrade/querycuscomgrade")
    public ResultDto<List<CusComGradeDto2>> queryCusComGrade(@RequestBody String cusId);

    /**
     * 查询社会关系人信息
     */
    @PostMapping("/cusindivsocial/querycussocial")
    public ResultDto<List<CusIndivSocialResp>> queryCusSocial(@RequestBody QueryModel queryModel);


    /**
     * @param cusLstYndAppDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.util.List < cn.com.yusys.yusp.dto.CusIndivSocialResp>>
     * @author hubp
     * @date 2021/9/15 19:21
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/cuslstyndapp/insertyndapp")
    public ResultDto<Integer> insertYndApp(@RequestBody CusLstYndAppDto cusLstYndAppDto);

    /**
     * 查询生效的自然人关联方
     *
     * @return
     */
    @PostMapping("/cuslstglf/querycuslstglfforindiv")
    public List<CusLstGlfDto> queryCusLstGlfForIndiv();

    /**
     * 根据客户号获取年份集合
     *
     * @author jijian_yx
     * @date 2021/9/27 15:33
     **/
    @PostMapping("/cusbase/getacctimelist")
    public ResultDto<List<String>> getAccTimeList(@RequestBody String cusId);

    /**
     * 根据档案目录和年份获取档案台账信息（分页）
     *
     * @author jijian_yx
     * @date 2021/9/28 0:03
     **/
    @PostMapping("/cusbase/queryacclistbydoctypeandyear")
    public ResultDto<List<DocAccSearchDto>> queryAccListByDocTypeAndYear(@RequestBody QueryModel queryModel);

    /**
     * 根据客户号查找出该客户存在到期未完成的压降任务
     * @param queryModel
     * @return
     */
    @PostMapping("/cuslstdedkkhyjsxtask/selectcusLstDedkkhYjsxTaskDataByParams")
    public ResultDto<List<CusLstDedkkhYjsxTaskDto>> selectcusLstDedkkhYjsxTaskDataByParams(@RequestBody QueryModel queryModel);

    @PostMapping("/cuscorpcert/queryCusIdByCertCode")
    public String queryCusIdByCertCode(@RequestBody String certCode);
}
