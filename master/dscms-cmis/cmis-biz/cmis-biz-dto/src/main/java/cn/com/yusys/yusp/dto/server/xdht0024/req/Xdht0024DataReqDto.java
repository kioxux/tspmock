package cn.com.yusys.yusp.dto.server.xdht0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 请求Data：合同详情查看
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0024DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotBlank(message = "certid不能为空")
    @JsonProperty(value = "certid")
    private String certid;//证件号
    @NotBlank(message = "biz_type不能为空")
    @JsonProperty(value = "biz_type")
    private String biz_type;//贷款类型
    @JsonProperty(value = "cont_state")
    private String cont_state;//合同状态

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    @Override
    public String toString() {
        return "Xdht0024DataReqDto{" +
                "certid='" + certid + '\'' +
                ", biz_type='" + biz_type + '\'' +
                ", cont_state='" + cont_state + '\'' +
                '}';
    }
}