package cn.com.yusys.yusp.dto;

import java.math.BigDecimal;

/**
 * @author xll
 * @version 1.0.0
 * @date 2021/8/30 19:27
 * @desc 优企贷续贷申请名单DTO
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class ComLoanInfoDto {

    // 查询类型
    private String type;
    // 业务流水
    private String serno;
    //客户编号
    private String cusId;
    //客户名称
    private String cusName;
    //申请人证件号
    private String certCode;
    //合同编号
    private String contNo;
    //原合同编号
    private String oldContNo;
    //合同类型
    private String contType;
    //业务类型
    private String bizType;
    //合同金额
    private BigDecimal contAmt;
    //借据余额
    private BigDecimal loanBalance;
    //申请人手机号
    private String telPhone;
    //企业名称
    private String conName;
    //统一社会信用代码
    private String unifyCreditCode;
    //名单生成日期
    private String inputDate;
    //续贷申请日期
    private String appDate;
    //名单处理状态
    private String status;
    //责任人
    private String managerId;
    //责任人名称
    private String managerName;
    //责任机构
    private String managerBrId;
    //责任机构名称
    private String managerBrName;
    //开始页码
    private String startPageNum;
    //每页记录数
    private String pageSize;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getOldContNo() {
        return oldContNo;
    }

    public void setOldContNo(String oldContNo) {
        this.oldContNo = oldContNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getUnifyCreditCode() {
        return unifyCreditCode;
    }

    public void setUnifyCreditCode(String unifyCreditCode) {
        this.unifyCreditCode = unifyCreditCode;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerBrName() {
        return managerBrName;
    }

    public void setManagerBrName(String managerBrName) {
        this.managerBrName = managerBrName;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
