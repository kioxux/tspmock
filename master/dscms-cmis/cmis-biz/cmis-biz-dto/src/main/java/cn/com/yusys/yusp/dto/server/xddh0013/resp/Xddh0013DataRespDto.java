package cn.com.yusys.yusp.dto.server.xddh0013.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询优抵贷抵质押品信息
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0013DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String totalQnt;
    private java.util.List<cn.com.yusys.yusp.dto.server.xddh0013.resp.List> list;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTotalQnt() {
        return totalQnt;
    }

    public void setTotalQnt(String totalQnt) {
        this.totalQnt = totalQnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xddh0013DataRespDto{" +
                "totalQnt='" + totalQnt + '\'' +
                ", list=" + list +
                '}';
    }
}
