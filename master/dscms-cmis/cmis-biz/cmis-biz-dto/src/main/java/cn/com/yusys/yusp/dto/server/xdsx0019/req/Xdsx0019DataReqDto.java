package cn.com.yusys.yusp.dto.server.xdsx0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：风控发送信贷审核受托信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0019DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同编号
    @JsonProperty(value = "cont_cn_no")
    private String cont_cn_no;//中文合同编号
    @JsonProperty(value = "manager_id")
    private String manager_id;//客户经理号
    @JsonProperty(value = "manager_name")
    private String manager_name;//客户经理名
    @JsonProperty(value = "bill_no")
    private String bill_no;//借据编号
    @JsonProperty(value = "bill_amount")
    private BigDecimal bill_amount;//借据金额
    @JsonProperty(value = "trade_partner_acc")
    private String trade_partner_acc;//受托支付账户户名
    @JsonProperty(value = "trade_partner_name")
    private String trade_partner_name;//受托支付账户账号
    @JsonProperty(value = "khh_no")
    private String khh_no;//受托支付开户行号
    @JsonProperty(value = "khh_name")
    private String khh_name;//受托支付开户行名
    @JsonProperty(value = "bill_date")
    private String bill_date;//借据期限
    @JsonProperty(value = "chack_rate")
    private BigDecimal chack_rate;//审批利率
    @JsonProperty(value = "refund_card")
    private String refund_card;//放款/还款卡号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getCont_cn_no() {
        return cont_cn_no;
    }

    public void setCont_cn_no(String cont_cn_no) {
        this.cont_cn_no = cont_cn_no;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public BigDecimal getBill_amount() {
        return bill_amount;
    }

    public void setBill_amount(BigDecimal bill_amount) {
        this.bill_amount = bill_amount;
    }

    public String getTrade_partner_acc() {
        return trade_partner_acc;
    }

    public void setTrade_partner_acc(String trade_partner_acc) {
        this.trade_partner_acc = trade_partner_acc;
    }

    public String getTrade_partner_name() {
        return trade_partner_name;
    }

    public void setTrade_partner_name(String trade_partner_name) {
        this.trade_partner_name = trade_partner_name;
    }

    public String getKhh_no() {
        return khh_no;
    }

    public void setKhh_no(String khh_no) {
        this.khh_no = khh_no;
    }

    public String getKhh_name() {
        return khh_name;
    }

    public void setKhh_name(String khh_name) {
        this.khh_name = khh_name;
    }

    public String getBill_date() {
        return bill_date;
    }

    public void setBill_date(String bill_date) {
        this.bill_date = bill_date;
    }

    public BigDecimal getChack_rate() {
        return chack_rate;
    }

    public void setChack_rate(BigDecimal chack_rate) {
        this.chack_rate = chack_rate;
    }

    public String getRefund_card() {
        return refund_card;
    }

    public void setRefund_card(String refund_card) {
        this.refund_card = refund_card;
    }

    @Override
    public String toString() {
        return "Xdsx0019DataReqDto{" +
                "serno='" + serno + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", cont_cn_no='" + cont_cn_no + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", bill_no='" + bill_no + '\'' +
                ", bill_amount=" + bill_amount +
                ", trade_partner_acc='" + trade_partner_acc + '\'' +
                ", trade_partner_name='" + trade_partner_name + '\'' +
                ", khh_no='" + khh_no + '\'' +
                ", khh_name='" + khh_name + '\'' +
                ", bill_date='" + bill_date + '\'' +
                ", chack_rate=" + chack_rate +
                ", refund_card='" + refund_card + '\'' +
                '}';
    }
}
