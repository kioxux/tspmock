package cn.com.yusys.yusp.dto.server.xdxw0081.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：优企贷、优农贷授信调查信息查询
 *
 * @author zdl
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0081DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cert_code")
    @NotNull(message = "证件号cert_code不能为空")
    @NotEmpty(message = "证件号cert_code不能为空")
    private String cert_code;//身份证号

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    @Override
    public String toString() {
        return "Xdxw0081DataReqDto{" +
                "cert_code='" + cert_code + '\'' +
                '}';
    }
}
