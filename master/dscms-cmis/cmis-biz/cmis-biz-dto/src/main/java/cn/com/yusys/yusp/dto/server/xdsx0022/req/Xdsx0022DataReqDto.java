package cn.com.yusys.yusp.dto.server.xdsx0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：推送苏州地方征信给信贷
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0022DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "connRecordId")
    private String connRecordId;//对接记录ID
    @JsonProperty(value = "conName")
    private String conName;//企业名称
    @JsonProperty(value = "conCode")
    private String conCode;//企业代码
    @JsonProperty(value = "initBrId")
    private String initBrId;//具体发起机构
    @JsonProperty(value = "expectCrdLmt")
    private BigDecimal expectCrdLmt;//借款人预期借款额度
    @JsonProperty(value = "bizType")
    private String bizType;//具体品种
    @JsonProperty(value = "crdTerm")
    private String crdTerm;//授信期限
    @JsonProperty(value = "taxEval")
    private String taxEval;//纳税评级
    @JsonProperty(value = "creditScroe")
    private String creditScroe;//征信报告信用评分
    @JsonProperty(value = "szObankLoanBal")
    private BigDecimal szObankLoanBal;//信用贷款用信余额
    @JsonProperty(value = "isBlklsOrGreyls")
    private String isBlklsOrGreyls;//是否黑灰名单客户
    @JsonProperty(value = "isSzTaxDisplay")
    private String isSzTaxDisplay;//苏州地方征信是否显示纳税信息
    @JsonProperty(value = "poiorTaxAmt")
    private BigDecimal poiorTaxAmt;//上年度纳税金额
    @JsonProperty(value = "isRent")
    private String isRent;//是否租用
    @JsonProperty(value = "isOwn")
    private String isOwn;//是否自有
    @JsonProperty(value = "isHavingProp")
    private String isHavingProp;//是否具备自有产权证
    @JsonProperty(value = "szCreditScore")
    private String szCreditScore;//企业苏州地方征信信用情况
    @JsonProperty(value = "bsRate")
    private BigDecimal bsRate;//资产负债率
    @JsonProperty(value = "depositSaleRate")
    private BigDecimal depositSaleRate;//企业存量贷款与销售收入比率
    @JsonProperty(value = "patentInfo")
    private String patentInfo;//专利信息情况
    @JsonProperty(value = "profitGrowInfo")
    private String profitGrowInfo;//利润增长情况
    @JsonProperty(value = "controlerOpYear")
    private String controlerOpYear;//实际控制人从事本行业年限
    @JsonProperty(value = "conTrade")
    private String conTrade;//企业所属行业
    @JsonProperty(value = "providSsPayInfo")
    private String providSsPayInfo;//公积金、社保缴纳情况
    @JsonProperty(value = "utilitiesPayInfo")
    private String utilitiesPayInfo;//水电气费缴纳情况
    @JsonProperty(value = "reprSettleInfo")
    private String reprSettleInfo;//法人或实际控制人落户情况
    @JsonProperty(value = "totalScore")
    private BigDecimal totalScore;//总分值
    @JsonProperty(value = "isAdmit")
    private String isAdmit;//是否准入（自动测算）
    @JsonProperty(value = "maxPrecrdAmt")
    private BigDecimal maxPrecrdAmt;//最大预授信额度

    public String getConnRecordId() {
        return connRecordId;
    }

    public void setConnRecordId(String connRecordId) {
        this.connRecordId = connRecordId;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getConCode() {
        return conCode;
    }

    public void setConCode(String conCode) {
        this.conCode = conCode;
    }

    public String getInitBrId() {
        return initBrId;
    }

    public void setInitBrId(String initBrId) {
        this.initBrId = initBrId;
    }

    public BigDecimal getExpectCrdLmt() {
        return expectCrdLmt;
    }

    public void setExpectCrdLmt(BigDecimal expectCrdLmt) {
        this.expectCrdLmt = expectCrdLmt;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getCrdTerm() {
        return crdTerm;
    }

    public void setCrdTerm(String crdTerm) {
        this.crdTerm = crdTerm;
    }

    public String getTaxEval() {
        return taxEval;
    }

    public void setTaxEval(String taxEval) {
        this.taxEval = taxEval;
    }

    public String getCreditScroe() {
        return creditScroe;
    }

    public void setCreditScroe(String creditScroe) {
        this.creditScroe = creditScroe;
    }

    public BigDecimal getSzObankLoanBal() {
        return szObankLoanBal;
    }

    public void setSzObankLoanBal(BigDecimal szObankLoanBal) {
        this.szObankLoanBal = szObankLoanBal;
    }

    public String getIsBlklsOrGreyls() {
        return isBlklsOrGreyls;
    }

    public void setIsBlklsOrGreyls(String isBlklsOrGreyls) {
        this.isBlklsOrGreyls = isBlklsOrGreyls;
    }

    public String getIsSzTaxDisplay() {
        return isSzTaxDisplay;
    }

    public void setIsSzTaxDisplay(String isSzTaxDisplay) {
        this.isSzTaxDisplay = isSzTaxDisplay;
    }

    public BigDecimal getPoiorTaxAmt() {
        return poiorTaxAmt;
    }

    public void setPoiorTaxAmt(BigDecimal poiorTaxAmt) {
        this.poiorTaxAmt = poiorTaxAmt;
    }

    public String getIsRent() {
        return isRent;
    }

    public void setIsRent(String isRent) {
        this.isRent = isRent;
    }

    public String getIsOwn() {
        return isOwn;
    }

    public void setIsOwn(String isOwn) {
        this.isOwn = isOwn;
    }

    public String getIsHavingProp() {
        return isHavingProp;
    }

    public void setIsHavingProp(String isHavingProp) {
        this.isHavingProp = isHavingProp;
    }

    public String getSzCreditScore() {
        return szCreditScore;
    }

    public void setSzCreditScore(String szCreditScore) {
        this.szCreditScore = szCreditScore;
    }

    public BigDecimal getBsRate() {
        return bsRate;
    }

    public void setBsRate(BigDecimal bsRate) {
        this.bsRate = bsRate;
    }

    public BigDecimal getDepositSaleRate() {
        return depositSaleRate;
    }

    public void setDepositSaleRate(BigDecimal depositSaleRate) {
        this.depositSaleRate = depositSaleRate;
    }

    public String getPatentInfo() {
        return patentInfo;
    }

    public void setPatentInfo(String patentInfo) {
        this.patentInfo = patentInfo;
    }

    public String getProfitGrowInfo() {
        return profitGrowInfo;
    }

    public void setProfitGrowInfo(String profitGrowInfo) {
        this.profitGrowInfo = profitGrowInfo;
    }

    public String getControlerOpYear() {
        return controlerOpYear;
    }

    public void setControlerOpYear(String controlerOpYear) {
        this.controlerOpYear = controlerOpYear;
    }

    public String getConTrade() {
        return conTrade;
    }

    public void setConTrade(String conTrade) {
        this.conTrade = conTrade;
    }

    public String getProvidSsPayInfo() {
        return providSsPayInfo;
    }

    public void setProvidSsPayInfo(String providSsPayInfo) {
        this.providSsPayInfo = providSsPayInfo;
    }

    public String getUtilitiesPayInfo() {
        return utilitiesPayInfo;
    }

    public void setUtilitiesPayInfo(String utilitiesPayInfo) {
        this.utilitiesPayInfo = utilitiesPayInfo;
    }

    public String getReprSettleInfo() {
        return reprSettleInfo;
    }

    public void setReprSettleInfo(String reprSettleInfo) {
        this.reprSettleInfo = reprSettleInfo;
    }

    public BigDecimal getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(BigDecimal totalScore) {
        this.totalScore = totalScore;
    }

    public String getIsAdmit() {
        return isAdmit;
    }

    public void setIsAdmit(String isAdmit) {
        this.isAdmit = isAdmit;
    }

    public BigDecimal getMaxPrecrdAmt() {
        return maxPrecrdAmt;
    }

    public void setMaxPrecrdAmt(BigDecimal maxPrecrdAmt) {
        this.maxPrecrdAmt = maxPrecrdAmt;
    }

    @Override
    public String toString() {
        return "Xdsx0022DataReqDto{" +
                "connRecordId='" + connRecordId + '\'' +
                "conName='" + conName + '\'' +
                "conCode='" + conCode + '\'' +
                "initBrId='" + initBrId + '\'' +
                "expectCrdLmt='" + expectCrdLmt + '\'' +
                "bizType='" + bizType + '\'' +
                "crdTerm='" + crdTerm + '\'' +
                "taxEval='" + taxEval + '\'' +
                "creditScroe='" + creditScroe + '\'' +
                "szObankLoanBal='" + szObankLoanBal + '\'' +
                "isBlklsOrGreyls='" + isBlklsOrGreyls + '\'' +
                "isSzTaxDisplay='" + isSzTaxDisplay + '\'' +
                "poiorTaxAmt='" + poiorTaxAmt + '\'' +
                "isRent='" + isRent + '\'' +
                "isOwn='" + isOwn + '\'' +
                "isHavingProp='" + isHavingProp + '\'' +
                "szCreditScore='" + szCreditScore + '\'' +
                "bsRate='" + bsRate + '\'' +
                "depositSaleRate='" + depositSaleRate + '\'' +
                "patentInfo='" + patentInfo + '\'' +
                "profitGrowInfo='" + profitGrowInfo + '\'' +
                "controlerOpYear='" + controlerOpYear + '\'' +
                "conTrade='" + conTrade + '\'' +
                "providSsPayInfo='" + providSsPayInfo + '\'' +
                "utilitiesPayInfo='" + utilitiesPayInfo + '\'' +
                "reprSettleInfo='" + reprSettleInfo + '\'' +
                "totalScore='" + totalScore + '\'' +
                "isAdmit='" + isAdmit + '\'' +
                "maxPrecrdAmt='" + maxPrecrdAmt + '\'' +
                '}';
    }
}
