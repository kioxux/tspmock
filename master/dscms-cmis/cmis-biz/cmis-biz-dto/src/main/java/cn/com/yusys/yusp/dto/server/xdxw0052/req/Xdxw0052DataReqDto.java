package cn.com.yusys.yusp.dto.server.xdxw0052.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询优抵贷客户调查表
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0052DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//客户调查表编号
    @JsonProperty(value = "applySerno")
    private String applySerno;//业务唯一编号

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getApplySerno() {
        return applySerno;
    }

    public void setApplySerno(String applySerno) {
        this.applySerno = applySerno;
    }

    @Override
    public String toString() {
        return "Xdxw0052DataReqDto{" +
                "queryType='" + queryType + '\'' +
                ", indgtSerno='" + indgtSerno + '\'' +
                ", applySerno='" + applySerno + '\'' +
                '}';
    }
}
