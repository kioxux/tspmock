package cn.com.yusys.yusp.dto.server.xdtz0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Data：查询小微借据余额
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusNo")
    private String cusNo;//客户号

    @JsonProperty(value = "assureMeans")
    private String assureMeans;

    @JsonProperty(value = "bizType")
    private String bizType;

    public String getCusNo() {
        return cusNo;
    }

    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    @Override
    public String toString() {
        return "Xdtz0003DataReqDto{" +
                "cusNo='" + cusNo + '\'' +
                ", assureMeans=" + assureMeans +
                ", bizType=" + bizType +
                '}';
    }
}
