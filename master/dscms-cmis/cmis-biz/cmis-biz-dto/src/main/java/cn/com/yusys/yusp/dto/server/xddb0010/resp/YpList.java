package cn.com.yusys.yusp.dto.server.xddb0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 17:01
 * @Version 1.0
 */
public class YpList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "_ypserno")
    private String _ypserno;//押品统一编号
    @JsonProperty(value = "_ypstatus")
    private String _ypstatus;//入库状态
    @JsonProperty(value = "areacode")
    private String areacode;//所在区域编号

    public String get_ypserno() {
        return _ypserno;
    }

    public void set_ypserno(String _ypserno) {
        this._ypserno = _ypserno;
    }

    public String get_ypstatus() {
        return _ypstatus;
    }

    public void set_ypstatus(String _ypstatus) {
        this._ypstatus = _ypstatus;
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode;
    }

    @Override
    public String toString() {
        return "YpList{" +
                "_ypserno='" + _ypserno + '\'' +
                ", _ypstatus='" + _ypstatus + '\'' +
                ", areacode='" + areacode + '\'' +
                '}';
    }
}
