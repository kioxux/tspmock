package cn.com.yusys.yusp.dto.server.xdxw0055.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：经营地址查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0055DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "operAddr")
	private String operAddr;//经营地址


	public String getOperAddr() {
		return operAddr;
	}

	public void setOperAddr(String operAddr) {
		this.operAddr = operAddr;
	}

	@Override
	public String toString() {
		return "Xdxw0055DataRespDto{" +
				"operAddr='" + operAddr + '\'' +
				'}';
	}
}
