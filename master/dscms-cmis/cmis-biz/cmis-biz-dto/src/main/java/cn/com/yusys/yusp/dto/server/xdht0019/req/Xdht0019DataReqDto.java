package cn.com.yusys.yusp.dto.server.xdht0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：合同生成
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0019DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "certNo")
    private String certNo;//客户证件号
    @JsonProperty(value = "loanType")
    private String loanType;//贷款类型
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "startDate")
    private String startDate;//起始日
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "rate")
    private String rate;//利率
    @JsonProperty(value = "cusId")
    private String cusId;//所在公司客户号
    @JsonProperty(value = "preferCode")
    private String preferCode;//优惠券号

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getPreferCode() {
        return preferCode;
    }

    public void setPreferCode(String preferCode) {
        this.preferCode = preferCode;
    }

	@Override
	public String toString() {
		return "Xdht0019DataReqDto{" +
				"certNo='" + certNo + '\'' +
				", loanType='" + loanType + '\'' +
				", applyAmt=" + applyAmt +
				", startDate='" + startDate + '\'' +
				", endDate='" + endDate + '\'' +
				", rate=" + rate +
				", cusId='" + cusId + '\'' +
				", preferCode='" + preferCode + '\'' +
				'}';
	}
}
