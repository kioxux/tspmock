package cn.com.yusys.yusp.dto.server.xdcz0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：企业网银查询影像补录批次
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0014DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "startPageNum")
    private String startPageNum;//起始页数
    @JsonProperty(value = "pageSize")
    private String pageSize;//分页大小
    @JsonProperty(value = "batchNo")
    private String batchNo;//批次号


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

	@Override
	public String toString() {
		return "Xdcz0014DataReqDto{" +
				", cusId='" + cusId + '\'' +
				", startPageNum='" + startPageNum + '\'' +
				", pageSize='" + pageSize + '\'' +
				", batchNo='" + batchNo + '\'' +
				'}';
	}
}
