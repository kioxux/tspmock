package cn.com.yusys.yusp.dto.server.xdls0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/18 11:02:<br>
 *
 * @author qianxin
 * @version 0.1
 * @date 2021/5/18 11:02
 * @since 2021/5/18 11:02
 */
@JsonPropertyOrder(alphabetic = true)
public class GuarList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarNo")
    private String guarNo;//押品编号

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    @Override
    public String toString() {
        return "GuarList{" +
                "guarNo='" + guarNo + '\'' +
                '}';
    }
}
