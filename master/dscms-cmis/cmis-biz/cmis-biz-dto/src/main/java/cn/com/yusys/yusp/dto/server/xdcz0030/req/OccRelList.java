package cn.com.yusys.yusp.dto.server.xdcz0030.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/7/16 17:06:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/7/16 17:06
 * @since 2021/7/16 17:06
 */
@JsonPropertyOrder(alphabetic = true)
public class OccRelList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dealBizNo")
    private String dealBizNo;//台账编号
    @JsonProperty(value = "lmtCusId")
    private String lmtCusId;//同业客户号
    @JsonProperty(value = "bizSpacAmtCny")
    private BigDecimal bizSpacAmtCny;//占用敞口

    public String getDealBizNo() {
        return dealBizNo;
    }

    public void setDealBizNo(String dealBizNo) {
        this.dealBizNo = dealBizNo;
    }

    public String getLmtCusId() {
        return lmtCusId;
    }

    public void setLmtCusId(String lmtCusId) {
        this.lmtCusId = lmtCusId;
    }

    public BigDecimal getBizSpacAmtCny() {
        return bizSpacAmtCny;
    }

    public void setBizSpacAmtCny(BigDecimal bizSpacAmtCny) {
        this.bizSpacAmtCny = bizSpacAmtCny;
    }

    @Override
    public String toString() {
        return "OccRelList{" +
                "dealBizNo='" + dealBizNo + '\'' +
                ", lmtCusId='" + lmtCusId + '\'' +
                ", bizSpacAmtCny=" + bizSpacAmtCny +
                '}';
    }
}
