package cn.com.yusys.yusp.dto.server.xdxt0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：客户经理信息通用查
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0011DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称
    @JsonProperty(value = "certCode")
    private String certCode;//客户经理身份证号
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "cusCertNo")
    private String cusCertNo;//客户身份证号

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusCertNo() {
        return cusCertNo;
    }

    public void setCusCertNo(String cusCertNo) {
        this.cusCertNo = cusCertNo;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Xdxt0011DataReqDto{" +
                "queryType='" + queryType + '\'' +
                ", managerName='" + managerName + '\'' +
                ", certCode='" + certCode + '\'' +
                ", managerId='" + managerId + '\'' +
                ", billNo='" + billNo + '\'' +
                ", cusCertNo='" + cusCertNo + '\'' +
                '}';
    }
}
