package cn.com.yusys.yusp.dto.server.xddb0022.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：据押品编号查询核心及信贷系统有无押品数据
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0022DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "flag")
    private String flag;//是否存在押品数据 0 否 1 是

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Xddb0022DataRespDto{" +
                "flag='" + flag + '\'' +
                '}';
    }
}
