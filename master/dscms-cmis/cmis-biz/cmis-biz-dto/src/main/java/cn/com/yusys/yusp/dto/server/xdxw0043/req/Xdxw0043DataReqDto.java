package cn.com.yusys.yusp.dto.server.xdxw0043.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：无还本续贷待办接收
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0043DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "apply_no")
    private String apply_no;//业务流水
    @JsonProperty(value = "flag")
    private String flag;//客户分层
    @JsonProperty(value = "zb_manager_id")
    private String zb_manager_id;//客户经理号

    public String getApply_no() {
        return apply_no;
    }

    public void setApply_no(String apply_no) {
        this.apply_no = apply_no;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getZb_manager_id() {
        return zb_manager_id;
    }

    public void setZb_manager_id(String zb_manager_id) {
        this.zb_manager_id = zb_manager_id;
    }

    @Override
    public String toString() {
        return "Xdxw0043DataReqDto{" +
                "apply_no='" + apply_no + '\'' +
                "flag='" + flag + '\'' +
                "zb_manager_id='" + zb_manager_id + '\'' +
                '}';
    }
}
