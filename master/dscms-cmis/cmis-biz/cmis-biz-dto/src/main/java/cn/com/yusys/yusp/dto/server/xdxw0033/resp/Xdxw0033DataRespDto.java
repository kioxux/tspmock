package cn.com.yusys.yusp.dto.server.xdxw0033.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据流水号查询征信报告关联信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0033DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "list")
	private java.util.List<List> list;

	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Xdxw0033DataRespDto{" +
				"list=" + list +
				'}';
	}
}
