package cn.com.yusys.yusp.dto.server.xdtz0046.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据借据号获取共同借款人信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0046DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "level")
    private String level;//层级
    @JsonProperty(value = "debit")
    private String debit;//借款人
    @JsonProperty(value = "debitCertNo")
    private BigDecimal debitCertNo;//借款人身份证号
    @JsonProperty(value = "contNo")
    private BigDecimal contNo;//合同号
    @JsonProperty(value = "LPRInterzone")
    private String LPRInterzone;//lpr区间
    @JsonProperty(value = "LPRRate")
    private String LPRRate;//lpr利率
    @JsonProperty(value = "addDeclFlag")
    private BigDecimal addDeclFlag;//加减标志
    @JsonProperty(value = "irFloatNum")
    private BigDecimal irFloatNum;//浮动点数
    @JsonProperty(value = "isHouseLoan")
    private String isHouseLoan;//是否为房贷
    @JsonProperty(value = "realityIrY")
    private String realityIrY;//执行利率年
    @JsonProperty(value = "irAdjustType")
    private BigDecimal irAdjustType;//利率调整方式
    @JsonProperty(value = "isCommHouse")
    private BigDecimal isCommHouse;//是否为商用房
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public BigDecimal getDebitCertNo() {
        return debitCertNo;
    }

    public void setDebitCertNo(BigDecimal debitCertNo) {
        this.debitCertNo = debitCertNo;
    }

    public BigDecimal getContNo() {
        return contNo;
    }

    public void setContNo(BigDecimal contNo) {
        this.contNo = contNo;
    }

    public String getPRInterzone() {
        return LPRInterzone;
    }

    public void setPRInterzone(
            String LPRInterzone) {
        this.LPRInterzone = LPRInterzone;
    }

    public String getPRRate() {
        return LPRRate;
    }

    public void setPRRate(
            String LPRRate) {
        this.LPRRate = LPRRate;
    }

    public BigDecimal getAddDeclFlag() {
        return addDeclFlag;
    }

    public void setAddDeclFlag(BigDecimal addDeclFlag) {
        this.addDeclFlag = addDeclFlag;
    }

    public BigDecimal getIrFloatNum() {
        return irFloatNum;
    }

    public void setIrFloatNum(BigDecimal irFloatNum) {
        this.irFloatNum = irFloatNum;
    }

    public String getIsHouseLoan() {
        return isHouseLoan;
    }

    public void setIsHouseLoan(String isHouseLoan) {
        this.isHouseLoan = isHouseLoan;
    }

    public String getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(String realityIrY) {
        this.realityIrY = realityIrY;
    }

    public BigDecimal getIrAdjustType() {
        return irAdjustType;
    }

    public void setIrAdjustType(BigDecimal irAdjustType) {
        this.irAdjustType = irAdjustType;
    }

    public BigDecimal getIsCommHouse() {
        return isCommHouse;
    }

    public void setIsCommHouse(BigDecimal isCommHouse) {
        this.isCommHouse = isCommHouse;
    }

    public String getLPRInterzone() {
        return LPRInterzone;
    }

    public void setLPRInterzone(String LPRInterzone) {
        this.LPRInterzone = LPRInterzone;
    }

    public String getLPRRate() {
        return LPRRate;
    }

    public void setLPRRate(String LPRRate) {
        this.LPRRate = LPRRate;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdtz0046DataRespDto{" +
                "level='" + level + '\'' +
                ", debit='" + debit + '\'' +
                ", debitCertNo=" + debitCertNo +
                ", contNo=" + contNo +
                ", LPRInterzone='" + LPRInterzone + '\'' +
                ", LPRRate='" + LPRRate + '\'' +
                ", addDeclFlag=" + addDeclFlag +
                ", irFloatNum=" + irFloatNum +
                ", isHouseLoan='" + isHouseLoan + '\'' +
                ", realityIrY='" + realityIrY + '\'' +
                ", irAdjustType=" + irAdjustType +
                ", isCommHouse=" + isCommHouse +
                ", list=" + list +
                '}';
    }
}
