package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DocArchiveClientDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 归档模式 STD_ARCHIVE_MODE
     * 01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
     **/
    private String archiveMode;

    /**
     * 档案分类 STD_DOC_CLASS
     * 01:基础资料档案,02:授信资料档案,03:重要信息档案
     **/
    private String docClass;

    /**
     * 档案类型 STD_DOC_TYPE
     * 01:个人客户资料
     * 02:对公客户资料
     * 03:个人授信资料
     * 04:企业授信资料
     * 05:对公及个人经营性贷款
     * 06:委托贷款
     * 07:特殊担保协议
     * 08:票据池业务
     * 09:贴现协议/贴现额度协议
     * 10:合作方协议
     * 11:委托贷款展期
     * 12:贷款展期
     * 13:保函
     * 14:承兑业务
     * 15:国际业务
     * 16:白领易贷通业务
     * 17:零售消费类业务
     * 18:公积金
     * 19:省心快贷合同
     * 20:信用卡
     * 21:小微贷款展期
     * 22:小微贷款业务
     * 23:业务变更-展期业务
     * 24:业务变更-担保变更
     * 25:业务变更-利率变更
     * 26:业务变更-还款计划变更
     * 27:业务变更-延期还款
     **/
    private String docType;

    /** 业务流水号 **/
    private String bizSerno;

    /** 客户号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 证件类型 **/
    private String certType;

    /** 证件号 **/
    private String certCode;

    /** 责任人 **/
    private String managerId;

    /** 责任机构 **/
    private String managerBrId;

    /** 入库操作人 **/
    private String optUsr;

    /** 入库操作机构 **/
    private String optOrg;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 合同编号 **/
    private String contNo;

    /** 借据编号 **/
    private String billNo;

    /** 档案业务品种 **/
    private String docBizType;

    /** 贷款金额 **/
    private BigDecimal loanAmt;

    /** 起始时间 **/
    private String startDate;

    /** 到期日期 **/
    private String endDate;

    /** 产品编号 **/
    private String prdId;

    /** 产品名称 **/
    private String prdName;

    public String getArchiveMode() {
        return archiveMode;
    }

    public void setArchiveMode(String archiveMode) {
        this.archiveMode = archiveMode;
    }

    public String getDocClass() {
        return docClass;
    }

    public void setDocClass(String docClass) {
        this.docClass = docClass;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getBizSerno() {
        return bizSerno;
    }

    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getOptUsr() {
        return optUsr;
    }

    public void setOptUsr(String optUsr) {
        this.optUsr = optUsr;
    }

    public String getOptOrg() {
        return optOrg;
    }

    public void setOptOrg(String optOrg) {
        this.optOrg = optOrg;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getDocBizType() {
        return docBizType;
    }

    public void setDocBizType(String docBizType) {
        this.docBizType = docBizType;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }
}
