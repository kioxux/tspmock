package cn.com.yusys.yusp.dto.server.xdtz0057.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据流水号查询客户调查的放款信息（在途需求）
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0057DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;//列表

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    public java.util.List<List> getList() {
        return list;
    }

    @Override
    public String toString() {
        return "Xdtz0057DataRespDto{" +
                "list=" + list +
                '}';
    }
}
