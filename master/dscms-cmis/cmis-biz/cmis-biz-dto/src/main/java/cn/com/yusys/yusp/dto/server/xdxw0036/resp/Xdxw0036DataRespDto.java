package cn.com.yusys.yusp.dto.server.xdxw0036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询优抵贷调查结论
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0036DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "indgtSerno")
	private String indgtSerno;//客户调查表编号
	@JsonProperty(value = "isRealOperPer")
	private String isRealOperPer;//客户是否为实际经营人
	@JsonProperty(value = "isOperNormal")
	private String isOperNormal;//客户经营是否正常
	@JsonProperty(value = "loanAmt")
	private BigDecimal loanAmt;//贷款金额
	@JsonProperty(value = "loanTerm")
	private BigDecimal loanTerm;//贷款期限
	@JsonProperty(value = "loanRate")
	private BigDecimal loanRate;//贷款利率
	@JsonProperty(value = "assureMeans")
	private String assureMeans;//担保方式
	@JsonProperty(value = "assureMeansName")
	private String assureMeansName;//担保方式名称
	@JsonProperty(value = "repayType")
	private String repayType;//还款方式（调查结论）
	@JsonProperty(value = "isAdmit")
	private String isAdmit;//是否准入
	@JsonProperty(value = "cusCha")
	private String cusCha;//客户性质
	@JsonProperty(value = "isOldPld")
	private String isOldPld;//是否原抵押物
	@JsonProperty(value = "turnovAmt")
	private BigDecimal turnovAmt;//周转额度
	@JsonProperty(value = "newAmt")
	private BigDecimal newAmt;//新增额度

	public String getIndgtSerno() {
		return indgtSerno;
	}

	public void setIndgtSerno(String indgtSerno) {
		this.indgtSerno = indgtSerno;
	}

	public String getIsRealOperPer() {
		return isRealOperPer;
	}

	public void setIsRealOperPer(String isRealOperPer) {
		this.isRealOperPer = isRealOperPer;
	}

	public String getIsOperNormal() {
		return isOperNormal;
	}

	public void setIsOperNormal(String isOperNormal) {
		this.isOperNormal = isOperNormal;
	}

	public BigDecimal getLoanAmt() {
		return loanAmt;
	}

	public void setLoanAmt(BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	public BigDecimal getLoanTerm() {
		return loanTerm;
	}

	public void setLoanTerm(BigDecimal loanTerm) {
		this.loanTerm = loanTerm;
	}

	public BigDecimal getLoanRate() {
		return loanRate;
	}

	public void setLoanRate(BigDecimal loanRate) {
		this.loanRate = loanRate;
	}

	public String getAssureMeans() {
		return assureMeans;
	}

	public void setAssureMeans(String assureMeans) {
		this.assureMeans = assureMeans;
	}

	public String getAssureMeansName() {
		return assureMeansName;
	}

	public void setAssureMeansName(String assureMeansName) {
		this.assureMeansName = assureMeansName;
	}

	public String getRepayType() {
		return repayType;
	}

	public void setRepayType(String repayType) {
		this.repayType = repayType;
	}

	public String getIsAdmit() {
		return isAdmit;
	}

	public void setIsAdmit(String isAdmit) {
		this.isAdmit = isAdmit;
	}

	public String getCusCha() {
		return cusCha;
	}

	public void setCusCha(String cusCha) {
		this.cusCha = cusCha;
	}

	public String getIsOldPld() {
		return isOldPld;
	}

	public void setIsOldPld(String isOldPld) {
		this.isOldPld = isOldPld;
	}

	public BigDecimal getTurnovAmt() {
		return turnovAmt;
	}

	public void setTurnovAmt(BigDecimal turnovAmt) {
		this.turnovAmt = turnovAmt;
	}

	public BigDecimal getNewAmt() {
		return newAmt;
	}

	public void setNewAmt(BigDecimal newAmt) {
		this.newAmt = newAmt;
	}

	@Override
	public String toString() {
		return "Xdxw0036DataRespDto{" +
				"indgtSerno='" + indgtSerno + '\'' +
				", isRealOperPer='" + isRealOperPer + '\'' +
				", isOperNormal='" + isOperNormal + '\'' +
				", loanAmt=" + loanAmt +
				", loanTerm=" + loanTerm +
				", loanRate=" + loanRate +
				", assureMeans='" + assureMeans + '\'' +
				", assureMeansName='" + assureMeansName + '\'' +
				", repayType='" + repayType + '\'' +
				", isAdmit='" + isAdmit + '\'' +
				", cusCha='" + cusCha + '\'' +
				", isOldPld='" + isOldPld + '\'' +
				", turnovAmt=" + turnovAmt +
				", newAmt=" + newAmt +
				'}';
	}
}
