package cn.com.yusys.yusp.dto.server.xdca0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：大额分期合同列表查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdca0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码
    @JsonProperty(value = "startNo")
    private String startNo;//起始记录号
    @JsonProperty(value = "queryNum")
    private String queryNum;//查询记录数

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getStartNo() {
        return startNo;
    }

    public void setStartNo(String startNo) {
        this.startNo = startNo;
    }

    public String getQueryNum() {
        return queryNum;
    }

    public void setQueryNum(String queryNum) {
        this.queryNum = queryNum;
    }

    @Override
    public String toString() {
        return "Xdca0003DataReqDto{" +
                "cusName='" + cusName + '\'' +
                "certType='" + certType + '\'' +
                "certCode='" + certCode + '\'' +
                "startNo='" + startNo + '\'' +
                "queryNum='" + queryNum + '\'' +
                '}';
    }
}
