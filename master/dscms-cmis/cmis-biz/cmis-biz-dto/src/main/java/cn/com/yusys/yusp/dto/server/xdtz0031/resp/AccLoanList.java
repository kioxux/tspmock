package cn.com.yusys.yusp.dto.server.xdtz0031.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据合同号获取借据信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class AccLoanList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bill_no")
    private String bill_no;//借据号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号(身份证号码)
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//借据开始日期
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//借据结束日期
    @JsonProperty(value = "account_status")
    private String account_status;//借据状态
    @JsonProperty(value = "assure_means_main")
    private String assure_means_main;//担保方式
    @JsonProperty(value = "loan_amount")
    private BigDecimal loan_amount;//借据金额
    @JsonProperty(value = "loan_balance")
    private BigDecimal loan_balance;//贷款余额
    @JsonProperty(value = "enter_account")
    private String enter_account;//放款账号
    @JsonProperty(value = "repayment_account")
    private String repayment_account;//还款账号
    @JsonProperty(value = "loan_paym_mtd")
    private String loan_paym_mtd;//还款方式
    @JsonProperty(value = "reality_ir_y")
    private BigDecimal reality_ir_y;//执行利率(年)
    @JsonProperty(value = "loan_od_int_rate1")
    private BigDecimal loan_od_int_rate1;//逾期利率
    @JsonProperty(value = "default_ir")
    private BigDecimal default_ir;//罚息利率
    @JsonProperty(value = "loan_direction")
    private String loan_direction;//贷款投向
    @JsonProperty(value = "biz_type")
    private String biz_type;//业务品种代码
    @JsonProperty(value = "prd_name")
    private String prd_name;//业务品种名称
    @JsonProperty(value = "account_class")
    private String account_class;//贷款科目

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public String getAssure_means_main() {
        return assure_means_main;
    }

    public void setAssure_means_main(String assure_means_main) {
        this.assure_means_main = assure_means_main;
    }

    public BigDecimal getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(BigDecimal loan_amount) {
        this.loan_amount = loan_amount;
    }

    public BigDecimal getLoan_balance() {
        return loan_balance;
    }

    public void setLoan_balance(BigDecimal loan_balance) {
        this.loan_balance = loan_balance;
    }

    public String getEnter_account() {
        return enter_account;
    }

    public void setEnter_account(String enter_account) {
        this.enter_account = enter_account;
    }

    public String getRepayment_account() {
        return repayment_account;
    }

    public void setRepayment_account(String repayment_account) {
        this.repayment_account = repayment_account;
    }

    public String getLoan_paym_mtd() {
        return loan_paym_mtd;
    }

    public void setLoan_paym_mtd(String loan_paym_mtd) {
        this.loan_paym_mtd = loan_paym_mtd;
    }

    public BigDecimal getReality_ir_y() {
        return reality_ir_y;
    }

    public void setReality_ir_y(BigDecimal reality_ir_y) {
        this.reality_ir_y = reality_ir_y;
    }

    public BigDecimal getLoan_od_int_rate1() {
        return loan_od_int_rate1;
    }

    public void setLoan_od_int_rate1(BigDecimal loan_od_int_rate1) {
        this.loan_od_int_rate1 = loan_od_int_rate1;
    }

    public BigDecimal getDefault_ir() {
        return default_ir;
    }

    public void setDefault_ir(BigDecimal default_ir) {
        this.default_ir = default_ir;
    }

    public String getLoan_direction() {
        return loan_direction;
    }

    public void setLoan_direction(String loan_direction) {
        this.loan_direction = loan_direction;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getAccount_class() {
        return account_class;
    }

    public void setAccount_class(String account_class) {
        this.account_class = account_class;
    }

    @Override
    public String toString() {
        return "AccLoanList{" +
                "bill_no='" + bill_no + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", account_status='" + account_status + '\'' +
                ", assure_means_main='" + assure_means_main + '\'' +
                ", loan_amount=" + loan_amount +
                ", loan_balance=" + loan_balance +
                ", enter_account='" + enter_account + '\'' +
                ", repayment_account='" + repayment_account + '\'' +
                ", loan_paym_mtd='" + loan_paym_mtd + '\'' +
                ", reality_ir_y=" + reality_ir_y +
                ", loan_od_int_rate1=" + loan_od_int_rate1 +
                ", default_ir=" + default_ir +
                ", loan_direction='" + loan_direction + '\'' +
                ", biz_type='" + biz_type + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", account_class='" + account_class + '\'' +
                '}';
    }
}
