package cn.com.yusys.yusp.dto.server.xdht0031.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据核心客户号查询经营性贷款合同额度
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0031DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applyAmt")
    private String applyAmt;//申请金额

    public Xdht0031DataRespDto(){

    }

    public Xdht0031DataRespDto(String applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(String applyAmt) {
        this.applyAmt = applyAmt;
    }

    @Override
    public String toString() {
        return "Xdht0031DataRespDto{" +
                "applyAmt='" + applyAmt + '\'' +
                '}';
    }
}