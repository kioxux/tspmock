package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class DocCreditArchiveClientDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 关联业务流水号 **/
    private String bizSerno;

    /** 主借款人名称 **/
    private String borrowerCusName;

    /** 主借款人证件号码 **/
    private String borrowerCertCode;

    /** 征信查询对象名称 **/
    private String cusName;

    /** 征信查询对象证件号码 **/
    private String certCode;

    /** 授权书日期 **/
    private String authbookDate;

    /** 查询人 **/
    private String qryUser;

    /** 查询机构 **/
    private String qryOrg;

    /** 接收人 **/
    private String receiverId;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    public String getBizSerno() {
        return bizSerno;
    }

    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    public String getBorrowerCusName() {
        return borrowerCusName;
    }

    public void setBorrowerCusName(String borrowerCusName) {
        this.borrowerCusName = borrowerCusName;
    }

    public String getBorrowerCertCode() {
        return borrowerCertCode;
    }

    public void setBorrowerCertCode(String borrowerCertCode) {
        this.borrowerCertCode = borrowerCertCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getAuthbookDate() {
        return authbookDate;
    }

    public void setAuthbookDate(String authbookDate) {
        this.authbookDate = authbookDate;
    }

    public String getQryUser() {
        return qryUser;
    }

    public void setQryUser(String qryUser) {
        this.qryUser = qryUser;
    }

    public String getQryOrg() {
        return qryOrg;
    }

    public void setQryOrg(String qryOrg) {
        this.qryOrg = qryOrg;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }
}
