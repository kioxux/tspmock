package cn.com.yusys.yusp.dto.server.xddb0008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：查询在线抵押信息
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0008DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pldManName")
    private String pldManName;//抵押人名称
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "startPageNum")
    @NotNull(message = "开始页码startPageNum不能为空")
    @NotEmpty(message = "开始页码startPageNum不能为空")
    private String startPageNum;//开始页码
    @JsonProperty(value = "pageSize")
    @NotNull(message = "每页记录数pageSize不能为空")
    @NotEmpty(message = "每页记录数pageSize不能为空")
    private String pageSize;//每页记录数



    public String getPldManName() {
        return pldManName;
    }

    public void setPldManName(String pldManName) {
        this.pldManName = pldManName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

	@Override
	public String toString() {
		return "Xddb0008DataReqDto{" +
				"pldManName='" + pldManName + '\'' +
				", managerId='" + managerId + '\'' +
				", startPageNum='" + startPageNum + '\'' +
				", pageSize='" + pageSize + '\'' +
				'}';
	}
}
