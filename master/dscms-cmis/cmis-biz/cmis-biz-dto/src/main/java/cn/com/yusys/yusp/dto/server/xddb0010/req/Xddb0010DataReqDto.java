package cn.com.yusys.yusp.dto.server.xddb0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：信贷押品列表查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0010DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

	@Override
	public String toString() {
		return "Xddb0010DataReqDto{" +
				"cont_no='" + cont_no + '\'' +
				'}';
	}
}
