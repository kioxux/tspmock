package cn.com.yusys.yusp.dto.server.xdzx0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：授权结果反馈接口
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzx0004DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批状态

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

	@Override
	public String toString() {
		return "Xdzx0004DataRespDto{" +
				"approveStatus='" + approveStatus + '\'' +
				'}';
	}
}
