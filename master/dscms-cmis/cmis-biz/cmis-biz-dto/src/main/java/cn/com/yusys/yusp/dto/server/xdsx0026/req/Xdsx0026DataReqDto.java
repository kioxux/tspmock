package cn.com.yusys.yusp.dto.server.xdsx0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;
import java.util.List;

/**
 * 请求Data：风控推送面签信息至信贷系统
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0026DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "credit_type")
    private String credit_type;//证件类型
    @JsonProperty(value = "credit_id")
    private String credit_id;//证件号码
    @JsonProperty(value = "phone_no")
    private String phone_no;//客户手机号码
    @JsonProperty(value = "manager_id")
    private String manager_id;//管户经理编号
    @JsonProperty(value = "manager_name")
    private String manager_name;//管户经理名称
    @JsonProperty(value = "listEntl")
    private List<ListEntl> listEntl;
    @JsonProperty(value = "listHouse")
    private List<ListHouse> listHouse;

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCredit_type() {
        return credit_type;
    }

    public void setCredit_type(String credit_type) {
        this.credit_type = credit_type;
    }

    public String getCredit_id() {
        return credit_id;
    }

    public void setCredit_id(String credit_id) {
        this.credit_id = credit_id;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public List<ListEntl> getListEntl() {
        return listEntl;
    }

    public void setListEntl(List<ListEntl> listEntl) {
        this.listEntl = listEntl;
    }

    public List<ListHouse> getListHouse() {
        return listHouse;
    }

    public void setListHouse(List<ListHouse> listHouse) {
        this.listHouse = listHouse;
    }

    @Override
    public String toString() {
        return "Xdsx0026DataReqDto{" +
                "cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", credit_type='" + credit_type + '\'' +
                ", credit_id='" + credit_id + '\'' +
                ", phone_no='" + phone_no + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", listEntl=" + listEntl +
                ", listHouse=" + listHouse +
                '}';
    }
}
