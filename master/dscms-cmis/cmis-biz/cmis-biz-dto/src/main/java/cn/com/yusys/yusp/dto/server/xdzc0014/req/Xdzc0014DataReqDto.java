package cn.com.yusys.yusp.dto.server.xdzc0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：资产池台账列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0014DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty("cusId")
    private String cusId;// 客户号
    @JsonProperty("loanType")
    private String loanType;// 台账种类
    @JsonProperty("loanStatus")
    private String loanStatus;// 台账状态
    @JsonProperty("pvpSerno")
    private String pvpSerno;// 出账流水号
    @JsonProperty("loanDateStartS")
    private String loanDateStartS;// 台账起始日start
    @JsonProperty("loanDateStartE")
    private String loanDateStartE;// 台账起始日end
    @JsonProperty("loanDateEndS")
    private String loanDateEndS;// 台账到期日start
    @JsonProperty("loanDateEndE")
    private String loanDateEndE;// 台账到期日end
    @JsonProperty("startPageNum")
    private String startPageNum;// 开始页数
    @JsonProperty("pageSize")
    private String pageSize;// 条数

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getLoanDateStartS() {
        return loanDateStartS;
    }

    public void setLoanDateStartS(String loanDateStartS) {
        this.loanDateStartS = loanDateStartS;
    }

    public String getLoanDateStartE() {
        return loanDateStartE;
    }

    public void setLoanDateStartE(String loanDateStartE) {
        this.loanDateStartE = loanDateStartE;
    }

    public String getLoanDateEndS() {
        return loanDateEndS;
    }

    public void setLoanDateEndS(String loanDateEndS) {
        this.loanDateEndS = loanDateEndS;
    }

    public String getLoanDateEndE() {
        return loanDateEndE;
    }

    public void setLoanDateEndE(String loanDateEndE) {
        this.loanDateEndE = loanDateEndE;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Xdzc0014DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", loanType='" + loanType + '\'' +
                ", loanStatus='" + loanStatus + '\'' +
                ", pvpSerno='" + pvpSerno + '\'' +
                ", loanDateStartS='" + loanDateStartS + '\'' +
                ", loanDateStartE='" + loanDateStartE + '\'' +
                ", loanDateEndS='" + loanDateEndS + '\'' +
                ", loanDateEndE='" + loanDateEndE + '\'' +
                ", startPageNum='" + startPageNum + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
