package cn.com.yusys.yusp.dto.server.xdxw0076.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/11 10:27
 * @since 2021/6/11 10:27
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prdNo")
    private String prdNo;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "serno")
    private String serno;//业务流水号
    @JsonProperty(value = "applyNo")
    private String applyNo;//申请金额
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//授信审批状态
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getApplyNo() {
        return applyNo;
    }

    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdxw0076DataRespDto{" +
                "prdNo='" + prdNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", serno='" + serno + '\'' +
                ", applyNo='" + applyNo + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
