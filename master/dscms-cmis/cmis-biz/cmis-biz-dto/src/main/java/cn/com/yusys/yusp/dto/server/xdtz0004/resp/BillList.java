package cn.com.yusys.yusp.dto.server.xdtz0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：在查询经营性贷款借据信息
 *
 * @author chenyong
 * @version 1.0
 */
public class BillList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//客户证件号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//贷款金额
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款起始日
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款截止日
    @JsonProperty(value = "guarType")
    private String guarType;//担保类型
    @JsonProperty(value = "disbMode")
    private String disbMode;//放款方式
    @JsonProperty(value = "realityIrY")
    private String realityIrY;//执行利率


    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getGuarType() {
        return guarType;
    }

    public void setGuarType(String guarType) {
        this.guarType = guarType;
    }

    public String getDisbMode() {
        return disbMode;
    }

    public void setDisbMode(String disbMode) {
        this.disbMode = disbMode;
    }

    public String getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(String realityIrY) {
        this.realityIrY = realityIrY;
    }

    @Override
    public String toString() {
        return "BillList{" +
                ", billNo='" + billNo + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certNo='" + certNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", loanAmt=" + loanAmt +
                ", loanStartDate='" + loanStartDate + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", guarType='" + guarType + '\'' +
                ", disbMode='" + disbMode + '\'' +
                ", realityIrY='" + realityIrY + '\'' +
                '}';
    }
}
