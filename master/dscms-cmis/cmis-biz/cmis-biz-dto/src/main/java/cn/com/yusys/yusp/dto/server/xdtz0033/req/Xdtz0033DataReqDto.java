package cn.com.yusys.yusp.dto.server.xdtz0033.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：查询客户所担保的行内贷款五级分类非正常状态件数
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0033DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    @NotNull(message = "客户编号cusId不能为空")
    @NotEmpty(message = "客户编号cusId不能为空")
    private String cusId;//客户编号


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }


}
