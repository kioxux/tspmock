package cn.com.yusys.yusp.dto.server.xdtz0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：客户信息查询(贷款信息)
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0001DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isOverdue")
    private String isOverdue;//是否存在逾期
    @JsonProperty(value = "billBal")
    private BigDecimal billBal;//存量余额
    @JsonProperty(value = "isOperLoan")
    private String isOperLoan;//是否存在经营性贷款
    @JsonProperty(value = "loanTimes")
    private String loanTimes;//惠享贷贷款次数
    @JsonProperty(value = "operLoanBal")
    private BigDecimal operLoanBal;//经营性贷款借据余额

    public String getIsOverdue() {
        return isOverdue;
    }

    public void setIsOverdue(String isOverdue) {
        this.isOverdue = isOverdue;
    }

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    public String getIsOperLoan() {
        return isOperLoan;
    }

    public void setIsOperLoan(String isOperLoan) {
        this.isOperLoan = isOperLoan;
    }

    public String getLoanTimes() {
        return loanTimes;
    }

    public void setLoanTimes(String loanTimes) {
        this.loanTimes = loanTimes;
    }

    public BigDecimal getOperLoanBal() {
        return operLoanBal;
    }

    public void setOperLoanBal(BigDecimal operLoanBal) {
        this.operLoanBal = operLoanBal;
    }

    @Override
    public String toString() {
        return "Xdtz0001DataRespDto{" +
                "isOverdue='" + isOverdue + '\'' +
                ", billBal='" + billBal + '\'' +
                ", isOperLoan='" + isOperLoan + '\'' +
                ", loanTimes='" + loanTimes + '\'' +
                ", operLoanBal='" + operLoanBal + '\'' +
                '}';
    }
}