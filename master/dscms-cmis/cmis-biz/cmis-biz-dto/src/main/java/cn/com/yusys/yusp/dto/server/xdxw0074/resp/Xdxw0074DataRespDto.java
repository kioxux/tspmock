package cn.com.yusys.yusp.dto.server.xdxw0074.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据客户身份证号查询线上产品申请记录
 * @author xll
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0074DataRespDto implements Serializable {
	@JsonProperty(value = "list")
	private java.util.List<List> list;

	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Xdxw0074DataRespDto{" +
				"list=" + list +
				'}';
	}
}  
