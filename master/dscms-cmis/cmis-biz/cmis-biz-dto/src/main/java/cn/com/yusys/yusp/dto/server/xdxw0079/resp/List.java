package cn.com.yusys.yusp.dto.server.xdxw0079.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：小微续贷白名单查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "status")
    private String status;//名单处理状态
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "certCode")
    private String certCode;//申请人证件号

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    @Override
    public String toString() {
        return "List{" +
                "status='" + status + '\'' +
                "serno='" + serno + '\'' +
                "certCode='" + certCode + '\'' +
                '}';
    }
}
