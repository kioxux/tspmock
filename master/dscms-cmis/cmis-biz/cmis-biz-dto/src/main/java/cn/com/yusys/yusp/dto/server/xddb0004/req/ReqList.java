package cn.com.yusys.yusp.dto.server.xddb0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 20:13
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ReqList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "newcod")
    private String newcod;//担保三级分类
    @JsonProperty(value = "guarid")
    private String guarid;//押品统一编号
    @JsonProperty(value = "type")
    private String type;//任务类型
    @JsonProperty(value = "result")
    private String result;//处理结果
    @JsonProperty(value = "mangid")
    private String mangid;//处理人工号
    @JsonProperty(value = "mabrid")
    private String mabrid;//处理人所属机构ID
    @JsonProperty(value = "evamt")
    private BigDecimal evamt;//评估价值
    @JsonProperty(value = "time")
    private String time;//下次估值到期日

    public String getNewcod() {
        return newcod;
    }

    public void setNewcod(String newcod) {
        this.newcod = newcod;
    }

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMangid() {
        return mangid;
    }

    public void setMangid(String mangid) {
        this.mangid = mangid;
    }

    public String getMabrid() {
        return mabrid;
    }

    public void setMabrid(String mabrid) {
        this.mabrid = mabrid;
    }

    public BigDecimal getEvamt() {
        return evamt;
    }

    public void setEvamt(BigDecimal evamt) {
        this.evamt = evamt;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "ReqList{" +
                "newcod='" + newcod + '\'' +
                ", guarid='" + guarid + '\'' +
                ", type='" + type + '\'' +
                ", result='" + result + '\'' +
                ", mangid='" + mangid + '\'' +
                ", mabrid='" + mabrid + '\'' +
                ", evamt=" + evamt +
                ", time='" + time + '\'' +
                '}';
    }
}
