package cn.com.yusys.yusp.dto.server.xdxw0039.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：提交决议
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0039DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFlag")
    private String opFlag;//操作成功标志位
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xdxw0039DataRespDto{" +
                "opFlag='" + opFlag + '\'' +
                "opMsg='" + opMsg + '\'' +
                '}';
    }
}  
