package cn.com.yusys.yusp.dto.server.xdzc0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：资产池主动还款接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0026DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "tradeCode")
    private String tradeCode;//交易流水号
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "repayMode1")
    private String repayMode1;//一级还款模式
    @JsonProperty(value = "repayMode2")
    private String repayMode2;//二级还款模式
    @JsonProperty(value = "repayAmt")
    private BigDecimal repayAmt;//还款金额
    @JsonProperty(value = "loanRecoverType")
    private String loanRecoverType;//贷款回收方式
    @JsonProperty(value = "repayAccNO")
    private String repayAccNO;//还款账户
    @JsonProperty(value = "repayAccName")
    private String repayAccName;//还款账户名称
    @JsonProperty(value = "repayAccSubNo")
    private String repayAccSubNo;//还款账户子序号
    @JsonProperty(value = "Magerid")
    private String Magerid;//主管客户经理

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRepayMode1() {
        return repayMode1;
    }

    public void setRepayMode1(String repayMode1) {
        this.repayMode1 = repayMode1;
    }

    public String getRepayMode2() {
        return repayMode2;
    }

    public void setRepayMode2(String repayMode2) {
        this.repayMode2 = repayMode2;
    }

    public BigDecimal getRepayAmt() {
        return repayAmt;
    }

    public void setRepayAmt(BigDecimal repayAmt) {
        this.repayAmt = repayAmt;
    }

    public String getLoanRecoverType() {
        return loanRecoverType;
    }

    public void setLoanRecoverType(String loanRecoverType) {
        this.loanRecoverType = loanRecoverType;
    }

    public String getRepayAccNO() {
        return repayAccNO;
    }

    public void setRepayAccNO(String repayAccNO) {
        this.repayAccNO = repayAccNO;
    }

    public String getRepayAccName() {
        return repayAccName;
    }

    public void setRepayAccName(String repayAccName) {
        this.repayAccName = repayAccName;
    }

    public String getRepayAccSubNo() {
        return repayAccSubNo;
    }

    public void setRepayAccSubNo(String repayAccSubNo) {
        this.repayAccSubNo = repayAccSubNo;
    }

    public String getMagerid() {
        return Magerid;
    }

    public void setMagerid(String magerid) {
        Magerid = magerid;
    }

    @Override
    public String toString() {
        return "Xdzc0026DataReqDto{" +
                "tradeCode='" + tradeCode + '\'' +
                ", billNo='" + billNo + '\'' +
                ", repayMode1='" + repayMode1 + '\'' +
                ", repayMode2='" + repayMode2 + '\'' +
                ", repayAmt=" + repayAmt +
                ", loanRecoverType='" + loanRecoverType + '\'' +
                ", repayAccNO='" + repayAccNO + '\'' +
                ", repayAccName='" + repayAccName + '\'' +
                ", repayAccSubNo='" + repayAccSubNo + '\'' +
                ", Magerid='" + Magerid + '\'' +
                '}';
    }
}
