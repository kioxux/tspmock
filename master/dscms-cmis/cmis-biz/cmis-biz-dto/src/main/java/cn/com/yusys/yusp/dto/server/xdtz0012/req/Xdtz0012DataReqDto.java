package cn.com.yusys.yusp.dto.server.xdtz0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据借款人证件号，判断配偶经营性贷款是否存在余额
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0012DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "certType")
    private String certType;//借款人证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//借款人证件号码

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xdtz0012DataReqDto{" +
                "certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                '}';
    }
}
