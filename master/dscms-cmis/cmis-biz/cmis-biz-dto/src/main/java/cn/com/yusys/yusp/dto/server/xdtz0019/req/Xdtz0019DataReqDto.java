package cn.com.yusys.yusp.dto.server.xdtz0019.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：台账入账
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0019DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bizType")
    private String bizType;//业务类型
    @JsonProperty(value = "oprtype")
    private String oprtype;//操作类型
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "bizVariet")
    private String bizVariet;//业务品种
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "creditIdate")
    private String creditIdate;//信用证有效期
    @JsonProperty(value = "creditNo")
    private String creditNo;//信用证号码
    @JsonProperty(value = "issuingCurType")
    private String issuingCurType;//开证币种
    @JsonProperty(value = "issuingContAmt")
    private String issuingContAmt;//开证合同金额
    @JsonProperty(value = "billAmt")
    private String billAmt;//借据金额
    @JsonProperty(value = "overdueDays")
    private String overdueDays;//逾期天数
    @JsonProperty(value = "floodactPerc")
    private String floodactPerc;//溢装比例
    @JsonProperty(value = "exchgRate")
    private String exchgRate;//汇率
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "accStatus")
    private String accStatus;//台账状态
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//责任机构
    @JsonProperty(value = "cretQuotationExchgRate")
    private String cretQuotationExchgRate;//信贷牌价汇率
    @JsonProperty(value = "pvpSerno")
    private String pvpSerno;//交易编号
    @JsonProperty(value = "startDate")
    private String startDate;//生效日期
    @JsonProperty(value = "endDate")
    private String endDate;//失效日期
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "bailAmt")
    private String bailAmt;//保证金金额（人民币）

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getOprtype() {
        return oprtype;
    }

    public void setOprtype(String oprtype) {
        this.oprtype = oprtype;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBizVariet() {
        return bizVariet;
    }

    public void setBizVariet(String bizVariet) {
        this.bizVariet = bizVariet;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCreditIdate() {
        return creditIdate;
    }

    public void setCreditIdate(String creditIdate) {
        this.creditIdate = creditIdate;
    }

    public String getCreditNo() {
        return creditNo;
    }

    public void setCreditNo(String creditNo) {
        this.creditNo = creditNo;
    }

    public String getIssuingCurType() {
        return issuingCurType;
    }

    public void setIssuingCurType(String issuingCurType) {
        this.issuingCurType = issuingCurType;
    }

    public String getIssuingContAmt() {
        return issuingContAmt;
    }

    public void setIssuingContAmt(String issuingContAmt) {
        this.issuingContAmt = issuingContAmt;
    }

    public String getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(String billAmt) {
        this.billAmt = billAmt;
    }

    public String getOverdueDays() {
        return overdueDays;
    }

    public void setOverdueDays(String overdueDays) {
        this.overdueDays = overdueDays;
    }

    public String getFloodactPerc() {
        return floodactPerc;
    }

    public void setFloodactPerc(String floodactPerc) {
        this.floodactPerc = floodactPerc;
    }

    public String getExchgRate() {
        return exchgRate;
    }

    public void setExchgRate(String exchgRate) {
        this.exchgRate = exchgRate;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getCretQuotationExchgRate() {
        return cretQuotationExchgRate;
    }

    public void setCretQuotationExchgRate(String cretQuotationExchgRate) {
        this.cretQuotationExchgRate = cretQuotationExchgRate;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(String bailAmt) {
        this.bailAmt = bailAmt;
    }

    @Override
    public String toString() {
        return "Xdtz0019DataReqDto{" +
                "bizType='" + bizType + '\'' +
                ", oprtype='" + oprtype + '\'' +
                ", billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", bizVariet='" + bizVariet + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", creditIdate='" + creditIdate + '\'' +
                ", creditNo='" + creditNo + '\'' +
                ", issuingCurType='" + issuingCurType + '\'' +
                ", issuingContAmt='" + issuingContAmt + '\'' +
                ", billAmt='" + billAmt + '\'' +
                ", overdueDays='" + overdueDays + '\'' +
                ", floodactPerc='" + floodactPerc + '\'' +
                ", exchgRate='" + exchgRate + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", accStatus='" + accStatus + '\'' +
                ", inputId='" + inputId + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", cretQuotationExchgRate='" + cretQuotationExchgRate + '\'' +
                ", pvpSerno='" + pvpSerno + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", bailAmt='" + bailAmt + '\'' +
                '}';
    }
}
