package cn.com.yusys.yusp.dto.server.xdtz0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：保证金补交/冲补交结果通知服务
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0014DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "seqNo")
    private String seqNo;//批次号
    @JsonProperty(value = "oprId")
    private String oprId;//操作码
    @JsonProperty(value = "coreBailPaySerNo")
    private String coreBailPaySerNo;//核心保证金缴纳流水号
    @JsonProperty(value = "contNo")
    private String contNo;//银承合同编号
    @JsonProperty(value = "bailType")
    private String bailType;//保证金类型
    @JsonProperty(value = "bailAcctNo")
    private String bailAcctNo;//保证金账号
    @JsonProperty(value = "bailCurType")
    private String bailCurType;//保证金币种
    @JsonProperty(value = "bailAmt")
    private String bailAmt;//保证金金额
    @JsonProperty(value = "intType")
    private String intType;//计息方式
    @JsonProperty(value = "bailIntAcctNo")
    private String bailIntAcctNo;//保证金利息存入账号
    @JsonProperty(value = "rqstrAcctName")
    private String rqstrAcctName;//申请人户名
    @JsonProperty(value = "rqstrAcctNo")
    private String rqstrAcctNo;//申请人账号
    @JsonProperty(value = "endDate")
    private String endDate;//到期日期
    @JsonProperty(value = "drfpoIsseMk")
    private String drfpoIsseMk;//票据池出票标记
    @JsonProperty(value = "drftNo")
    private String drftNo;//票号
    @JsonProperty(value = "drftAmt")
    private BigDecimal drftAmt;//票面金额

    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getOprId() {
        return oprId;
    }

    public void setOprId(String oprId) {
        this.oprId = oprId;
    }

    public String getCoreBailPaySerNo() {
        return coreBailPaySerNo;
    }

    public void setCoreBailPaySerNo(String coreBailPaySerNo) {
        this.coreBailPaySerNo = coreBailPaySerNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBailType() {
        return bailType;
    }

    public void setBailType(String bailType) {
        this.bailType = bailType;
    }

    public String getBailAcctNo() {
        return bailAcctNo;
    }

    public void setBailAcctNo(String bailAcctNo) {
        this.bailAcctNo = bailAcctNo;
    }

    public String getBailCurType() {
        return bailCurType;
    }

    public void setBailCurType(String bailCurType) {
        this.bailCurType = bailCurType;
    }

    public String getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(String bailAmt) {
        this.bailAmt = bailAmt;
    }

    public String getIntType() {
        return intType;
    }

    public void setIntType(String intType) {
        this.intType = intType;
    }

    public String getBailIntAcctNo() {
        return bailIntAcctNo;
    }

    public void setBailIntAcctNo(String bailIntAcctNo) {
        this.bailIntAcctNo = bailIntAcctNo;
    }

    public String getRqstrAcctName() {
        return rqstrAcctName;
    }

    public void setRqstrAcctName(String rqstrAcctName) {
        this.rqstrAcctName = rqstrAcctName;
    }

    public String getRqstrAcctNo() {
        return rqstrAcctNo;
    }

    public void setRqstrAcctNo(String rqstrAcctNo) {
        this.rqstrAcctNo = rqstrAcctNo;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDrfpoIsseMk() {
        return drfpoIsseMk;
    }

    public void setDrfpoIsseMk(String drfpoIsseMk) {
        this.drfpoIsseMk = drfpoIsseMk;
    }

    public String getDrftNo() {
        return drftNo;
    }

    public void setDrftNo(String drftNo) {
        this.drftNo = drftNo;
    }

    public BigDecimal getDrftAmt() {
        return drftAmt;
    }

    public void setDrftAmt(BigDecimal drftAmt) {
        this.drftAmt = drftAmt;
    }

    @Override
    public String toString() {
        return "Xdtz0014DataReqDto{" +
                "seqNo='" + seqNo + '\'' +
                ", oprId='" + oprId + '\'' +
                ", coreBailPaySerNo='" + coreBailPaySerNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", bailType='" + bailType + '\'' +
                ", bailAcctNo='" + bailAcctNo + '\'' +
                ", bailCurType='" + bailCurType + '\'' +
                ", bailAmt='" + bailAmt + '\'' +
                ", intType='" + intType + '\'' +
                ", bailIntAcctNo='" + bailIntAcctNo + '\'' +
                ", rqstrAcctName='" + rqstrAcctName + '\'' +
                ", rqstrAcctNo='" + rqstrAcctNo + '\'' +
                ", endDate='" + endDate + '\'' +
                ", drfpoIsseMk='" + drfpoIsseMk + '\'' +
                ", drftNo='" + drftNo + '\'' +
                ", drftAmt=" + drftAmt +
                '}';
    }
}
