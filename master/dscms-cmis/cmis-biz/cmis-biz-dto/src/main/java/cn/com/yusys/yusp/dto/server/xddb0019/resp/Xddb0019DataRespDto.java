package cn.com.yusys.yusp.dto.server.xddb0019.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：解质押押品校验
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0019DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarStatus")
    private String guarStatus;//押品状态
    @JsonProperty(value = "opFlag")
    private String opFlag;//操作成功标志位
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getGuarStatus() {
        return guarStatus;
    }

    public void setGuarStatus(String guarStatus) {
        this.guarStatus = guarStatus;
    }

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xddb0019DataRespDto{" +
                "guarStatus='" + guarStatus + '\'' +
                ", opFlag='" + opFlag + '\'' +
                ", opMsg='" + opMsg + '\'' +
                '}';
    }
}
