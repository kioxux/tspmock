package cn.com.yusys.yusp.dto;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/5/11 5:23 下午
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class CentralFileTaskDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @NotBlank
    private String serno;
    /**
     * 资料类型 STD_BIZ_SUB_TYPE
     *[{"key":"KH001","value":"公司正式客户创建（集中作业）"},{"key":"KH002","value":"公司正式客户客观信息维护（集中作业）"},{"key":"KH003","value":"公司客户财务报表新增（集中作业）"},{"key":"KH004","value":"公司客户财务报表修改（集中作业）"},{"key":"KH023","value":"公司客户（年报）财务报表修改（集中作业）"},{"key":"KH005","value":"公司客户转正（集中作业）"},{"key":"KH006","value":"个人客户财务报表新增（集中作业）"},{"key":"KH007","value":"个人客户财务报表维护（集中作业）"},{"key":"DB001","value":"抵押物信息创建（对公）"},{"key":"DB002","value":"抵押登记（本地机构集中办理模式）"},{"key":"DB003","value":"抵押登记（本地机构放款后抵押模式）"},{"key":"DB009","value":"抵押注销（本地机构）"},{"key":"LS004","value":"零售空白合同签订"},{"key":"LS005","value":"零售放款申请（空白合同模式）"},{"key":"LS006","value":"零售放款申请（生成打印模式）"},{"key":"LS007","value":"零售线上提款启用"},{"key":"YX001","value":"最高额授信协议申请"},{"key":"YX002","value":"普通贷款合同申请"},{"key":"YX003","value":"贸易融资合同申请"},{"key":"YX004","value":"福费廷合同申请"},{"key":"YX005","value":"开证合同申请"},{"key":"YX006","value":"银承合同申请"},{"key":"YX007","value":"保函合同申请"},{"key":"YX008","value":"委托贷款合同申请"},{"key":"YX010","value":"合同影像审核"},{"key":"YX011","value":"对公贷款出账申请"},{"key":"YX012","value":"银承出账申请"},{"key":"YX013","value":"委托贷款出账申请"},{"key":"ZX003","value":"苏州征信查询"}]
     **/
    @NotBlank
    private String bizType;

    /**
     * 任务类型  STD_TASK_TYPE
     * 01	档案接收
     * 02	档案暂存
     * 03	档案派发
     * 04	档案暂存及派发
     **/
    @NotBlank
    private String taskType;

    /**
     * 操作类型  STD_OPT_TYPE
     * 01	纯指令
     * 02	非纯指令
     **/
    @NotBlank
    private String optType;

    /**
     * 客户编号
     **/
    @NotBlank
    private String cusId;

    /**
     * 客户名称
     **/
    @NotBlank
    private String cusName;

    /**
     * 任务加急标识
     * 9：不加急
     * 3：系统加急
     **/
    @NotBlank
    private String taskUrgentFlag;

    /**
     * 登记人
     **/
    @NotBlank
    private String inputId;

    /**
     * 登记机构
     **/
    @NotBlank
    private String inputBrId;

    /**
     * 流程实例号 涉及到流程的场景必输 从流程instanceDto中取
     **/
    private String instanceId;

    /**
     * 流程节点号 涉及到流程的场景必输 从流程instanceDto中取
     **/
    private String nodeId;
    /**
     * 全局流水号
     **/
    @NotBlank
    private String traceId;


    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getTaskUrgentFlag() {
        return taskUrgentFlag;
    }

    public void setTaskUrgentFlag(String taskUrgentFlag) {
        this.taskUrgentFlag = taskUrgentFlag;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }
}
