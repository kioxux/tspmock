package cn.com.yusys.yusp.dto.server.xdxt0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询客户经理所在分部编号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0005DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Xdxt0005DataReqDto{" +
                "managerId='" + managerId + '\'' +
                '}';
    }
}
