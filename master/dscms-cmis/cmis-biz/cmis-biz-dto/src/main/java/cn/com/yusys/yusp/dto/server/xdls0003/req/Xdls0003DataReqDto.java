package cn.com.yusys.yusp.dto.server.xdls0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：信贷授信协议合同金额查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdls0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//核心客户号
    @JsonProperty(value = "curtDate")
    private String curtDate;//当前日期


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCurtDate() {
        return curtDate;
    }

    public void setCurtDate(String curtDate) {
        this.curtDate = curtDate;
    }

	@Override
	public String toString() {
		return "Xdls0003DataReqDto{" +
				"cusId='" + cusId + '\'' +
				", curtDate='" + curtDate + '\'' +
				'}';
	}
}
