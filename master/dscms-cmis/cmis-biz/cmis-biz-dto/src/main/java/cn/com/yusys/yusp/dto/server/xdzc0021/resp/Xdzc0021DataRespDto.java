package cn.com.yusys.yusp.dto.server.xdzc0021.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：融资汇总查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0021DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "assetSumAmt")
    private BigDecimal assetSumAmt;//资产池总额
    @JsonProperty(value = "supshAgrAmt")
    private BigDecimal supshAgrAmt;//超短贷用信金额
    @JsonProperty(value = "eBillAmt")
    private BigDecimal eBillAmt;//电票用信金额
    @JsonProperty(value = "sxepyxje")
    private BigDecimal sxepyxje;//省心e票用信金额
    @JsonProperty(value = "sxefyxje")
    private BigDecimal sxefyxje;//省心e付用信金额
    @JsonProperty(value = "nowAmt")
    private BigDecimal nowAmt;//剩余额度
    @JsonProperty(value = "supshAgrPer")
    private BigDecimal supshAgrPer;//超短贷用信金额百分比
    @JsonProperty(value = "eBillPer")
    private BigDecimal eBillPer;//电票用信金额百分比
    @JsonProperty(value = "sxepyxjePer")
    private BigDecimal sxepyxjePer;//省心e票用信金额百分比
    @JsonProperty(value = "sxefyxjePer")
    private BigDecimal sxefyxjePer;//省心e付用信金额百分比
    @JsonProperty(value = "nowAmtPer")
    private BigDecimal nowAmtPer;//剩余额度百分比

    public BigDecimal getAssetSumAmt() {
        return assetSumAmt;
    }

    public void setAssetSumAmt(BigDecimal assetSumAmt) {
        this.assetSumAmt = assetSumAmt;
    }

    public BigDecimal getSupshAgrAmt() {
        return supshAgrAmt;
    }

    public void setSupshAgrAmt(BigDecimal supshAgrAmt) {
        this.supshAgrAmt = supshAgrAmt;
    }

    public BigDecimal getEBillAmt() {
        return eBillAmt;
    }

    public void setEBillAmt(BigDecimal eBillAmt) {
        this.eBillAmt = eBillAmt;
    }

    public BigDecimal getSxepyxje() {
        return sxepyxje;
    }

    public void setSxepyxje(BigDecimal sxepyxje) {
        this.sxepyxje = sxepyxje;
    }

    public BigDecimal getSxefyxje() {
        return sxefyxje;
    }

    public void setSxefyxje(BigDecimal sxefyxje) {
        this.sxefyxje = sxefyxje;
    }

    public BigDecimal getNowAmt() {
        return nowAmt;
    }

    public void setNowAmt(BigDecimal nowAmt) {
        this.nowAmt = nowAmt;
    }

    public BigDecimal getSupshAgrPer() {
        return supshAgrPer;
    }

    public void setSupshAgrPer(BigDecimal supshAgrPer) {
        this.supshAgrPer = supshAgrPer;
    }

    public BigDecimal getEBillPer() {
        return eBillPer;
    }

    public void setEBillPer(BigDecimal eBillPer) {
        this.eBillPer = eBillPer;
    }

    public BigDecimal getSxepyxjePer() {
        return sxepyxjePer;
    }

    public void setSxepyxjePer(BigDecimal sxepyxjePer) {
        this.sxepyxjePer = sxepyxjePer;
    }

    public BigDecimal getSxefyxjePer() {
        return sxefyxjePer;
    }

    public void setSxefyxjePer(BigDecimal sxefyxjePer) {
        this.sxefyxjePer = sxefyxjePer;
    }

    public BigDecimal getNowAmtPer() {
        return nowAmtPer;
    }

    public void setNowAmtPer(BigDecimal nowAmtPer) {
        this.nowAmtPer = nowAmtPer;
    }

    @Override
    public String toString() {
        return "Xdzc0021DataRespDto{" +
                "assetSumAmt=" + assetSumAmt +
                ", supshAgrAmt=" + supshAgrAmt +
                ", eBillAmt=" + eBillAmt +
                ", sxepyxje=" + sxepyxje +
                ", sxefyxje=" + sxefyxje +
                ", nowAmt=" + nowAmt +
                ", supshAgrPer=" + supshAgrPer +
                ", eBillPer=" + eBillPer +
                ", sxepyxjePer=" + sxepyxjePer +
                ", sxefyxjePer=" + sxefyxjePer +
                ", nowAmtPer=" + nowAmtPer +
                '}';
    }
}
