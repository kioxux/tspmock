package cn.com.yusys.yusp.dto.server.xdzx0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/11 11:32
 * @since 2021/6/11 11:32
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//征信查询对象名
    @JsonProperty(value = "certCode")
    private String certCode;//查询对象证件号
    @JsonProperty(value = "certType")
    private String certType;//查询对象证件类型
    @JsonProperty(value = "qryResn")
    private String qryResn;//征信查询原因
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批状态
    @JsonProperty(value = "crqlSerno")
    private String crqlSerno;//征信查询申请流水号
    @JsonProperty(value = "authbookNo")
    private String authbookNo;//授权书编号
    @JsonProperty(value = "authbookContent")
    private String authbookContent;//授权书日期
    @JsonProperty(value = "borrowRel")
    private String borrowRel;//与主借款人关系
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getQryResn() {
        return qryResn;
    }

    public void setQryResn(String qryResn) {
        this.qryResn = qryResn;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getCrqlSerno() {
        return crqlSerno;
    }

    public void setCrqlSerno(String crqlSerno) {
        this.crqlSerno = crqlSerno;
    }

    public String getAuthbookNo() {
        return authbookNo;
    }

    public void setAuthbookNo(String authbookNo) {
        this.authbookNo = authbookNo;
    }

    public String getAuthbookContent() {
        return authbookContent;
    }

    public void setAuthbookContent(String authbookContent) {
        this.authbookContent = authbookContent;
    }

    public String getBorrowRel() {
        return borrowRel;
    }

    public void setBorrowRel(String borrowRel) {
        this.borrowRel = borrowRel;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    @Override
    public String toString() {
        return "Xdzx0002DataRespDto{" +
                "cusId='" + cusId + '\'' +
                ", certCode='" + certCode + '\'' +
                ", certType='" + certType + '\'' +
                ", qryResn='" + qryResn + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", crqlSerno='" + crqlSerno + '\'' +
                ", authbookNo='" + authbookNo + '\'' +
                ", authbookContent='" + authbookContent + '\'' +
                ", borrowRel='" + borrowRel + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                '}';
    }
}
