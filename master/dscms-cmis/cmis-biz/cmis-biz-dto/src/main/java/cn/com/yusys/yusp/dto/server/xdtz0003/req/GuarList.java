package cn.com.yusys.yusp.dto.server.xdtz0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @Author zhangpeng
 * @Date 2021/5/7 10:03
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class GuarList {

    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    @Override
    public String toString() {
        return "GuarList{" +
                "assureMeans='" + assureMeans + '\'' +
                '}';
    }
}
