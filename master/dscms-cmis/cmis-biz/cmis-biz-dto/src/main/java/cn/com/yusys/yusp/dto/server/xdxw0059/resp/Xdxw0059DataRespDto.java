package cn.com.yusys.yusp.dto.server.xdxw0059.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据业务唯一编号查询在信贷系统中的抵押率
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0059DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pldRate")
    private BigDecimal pldRate;//抵押率


    public BigDecimal getPldRate() {
        return pldRate;
    }

    public void setPldRate(BigDecimal pldRate) {
        this.pldRate = pldRate;
    }

    @Override
    public String toString() {
        return "Xdxw0059DataRespDto{" +
                "pldRate=" + pldRate +
                '}';
    }
}
