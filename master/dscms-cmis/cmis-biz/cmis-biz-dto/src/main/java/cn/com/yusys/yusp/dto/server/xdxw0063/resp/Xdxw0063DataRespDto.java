package cn.com.yusys.yusp.dto.server.xdxw0063.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：调查基本信息查询
 *
 * @author chenyog
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0063DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "spouse_name")
    private String spouse_name;//配偶名称
    @JsonProperty(value = "spouse_cert_code")
    private String spouse_cert_code;//配偶身份证
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//授信调查流水号
    @JsonProperty(value = "borrower")
    private String borrower;//借款人名称
    @JsonProperty(value = "borrower_cert_code")
    private String borrower_cert_code;//借款人身份证号码
    @JsonProperty(value = "applyer")
    private String applyer;//申请人

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_cert_code() {
        return spouse_cert_code;
    }

    public void setSpouse_cert_code(String spouse_cert_code) {
        this.spouse_cert_code = spouse_cert_code;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getBorrower() {
        return borrower;
    }

    public void setBorrower(String borrower) {
        this.borrower = borrower;
    }

    public String getBorrower_cert_code() {
        return borrower_cert_code;
    }

    public void setBorrower_cert_code(String borrower_cert_code) {
        this.borrower_cert_code = borrower_cert_code;
    }

    public String getApplyer() {
        return applyer;
    }

    public void setApplyer(String applyer) {
        this.applyer = applyer;
    }

    @Override
    public String toString() {
        return "Xdxw0063DataRespDto{" +
                "spouse_name='" + spouse_name + '\'' +
                ", spouse_cert_code='" + spouse_cert_code + '\'' +
                ", survey_serno='" + survey_serno + '\'' +
                ", borrower='" + borrower + '\'' +
                ", borrower_cert_code='" + borrower_cert_code + '\'' +
                ", applyer='" + applyer + '\'' +
                '}';
    }
}
