package cn.com.yusys.yusp.dto.server.xddh0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：贷后检查任务
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class PspTaskListRep implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 主键 **/
    @JsonProperty(value = "pkId")
    private String pkId;

    /** 任务编号 **/
    @JsonProperty(value = "taskNo")
    private String taskNo;

    /** 任务类型 **/
    @JsonProperty(value = "taskType")
    private String taskType;

    /** 检查类型 **/
    @JsonProperty(value = "checkType")
    private String checkType;

    /** 客户编号 **/
    @JsonProperty(value = "cusId")
    private String cusId;

    /** 客户名称 **/
    @JsonProperty(value = "cusName")
    private String cusName;

    /** 借据编号 **/
    @JsonProperty(value = "billNo")
    private String billNo;

    /** 合同编号 **/
    @JsonProperty(value = "contNo")
    private String contNo;

    /** 产品名称 **/
    @JsonProperty(value = "prdName")
    private String prdName;

    /** 任务生成日期 **/
    @JsonProperty(value = "taskStartDt")
    private String taskStartDt;

    /** 任务要求完成日期 **/
    @JsonProperty(value = "taskEndDt")
    private String taskEndDt;

    /** 任务执行人 **/
    @JsonProperty(value = "execId")
    private String execId;

    /** 任务执行机构 **/
    @JsonProperty(value = "execBrId")
    private String execBrId;

    /** 检查状态 **/
    @JsonProperty(value = "checkStatus")
    private String checkStatus;

    /** 审批状态 **/
    @JsonProperty(value = "approveStatus")
    private String approveStatus;

    /** 报告类型 **/
    @JsonProperty(value = "rptType")
    private String rptType;

    /** 检查日期 **/
    @JsonProperty(value = "checkDate")
    private String checkDate;

    /** 借据起始日 **/
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;

    /** 借据到期日 **/
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;

    /** 风险等级 **/
    @JsonProperty(value = "riskLvl")
    private String riskLvl;

    /** 担保方式 **/
    @JsonProperty(value = "guarMode")
    private String guarMode;

    /** 任务派发人员 **/
    @JsonProperty(value = "issueId")
    private String issueId;

    /** 任务派发人员所属机构 **/
    @JsonProperty(value = "issueBrId")
    private String issueBrId;

    /** 任务下发日期 **/
    @JsonProperty(value = "issueDate")
    private String issueDate;

    /** 贷款金额（元）  **/
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;
    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getTaskStartDt() {
        return taskStartDt;
    }

    public void setTaskStartDt(String taskStartDt) {
        this.taskStartDt = taskStartDt;
    }

    public String getTaskEndDt() {
        return taskEndDt;
    }

    public void setTaskEndDt(String taskEndDt) {
        this.taskEndDt = taskEndDt;
    }

    public String getExecId() {
        return execId;
    }

    public void setExecId(String execId) {
        this.execId = execId;
    }

    public String getExecBrId() {
        return execBrId;
    }

    public void setExecBrId(String execBrId) {
        this.execBrId = execBrId;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getRptType() {
        return rptType;
    }

    public void setRptType(String rptType) {
        this.rptType = rptType;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getRiskLvl() {
        return riskLvl;
    }

    public void setRiskLvl(String riskLvl) {
        this.riskLvl = riskLvl;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getIssueBrId() {
        return issueBrId;
    }

    public void setIssueBrId(String issueBrId) {
        this.issueBrId = issueBrId;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }


    @Override
    public String toString() {
        return "PspTaskListRep{" +
                "pkId='" + pkId + '\'' +
                "taskNo='" + taskNo + '\'' +
                "taskType='" + taskType + '\'' +
                "checkType='" + checkType + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "billNo='" + billNo + '\'' +
                "contNo='" + contNo + '\'' +
                "prdName='" + prdName + '\'' +
                "taskStartDt='" + taskStartDt + '\'' +
                "taskEndDt='" + taskEndDt + '\'' +
                "execId='" + execId + '\'' +
                "execBrId='" + execBrId + '\'' +
                "checkStatus='" + checkStatus + '\'' +
                "approveStatus='" + approveStatus + '\'' +
                "checkDate='" + checkDate + '\'' +
                "loanStartDate='" + loanStartDate + '\'' +
                "loanEndDate='" + loanEndDate + '\'' +
                "riskLvl='" + riskLvl + '\'' +
                "guarMode='" + guarMode + '\'' +
                "issueId='" + issueId + '\'' +
                "issueBrId='" + issueBrId + '\'' +
                "issueDate='" + issueDate + '\'' +
                "loanAmt='" + loanAmt + '\'' +
                '}';
    }
}
