package cn.com.yusys.yusp.dto.server.xdxw0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：小微贷前调查信息维护
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理编号
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "surveyType")
    private String surveyType;//表格类型
    @JsonProperty(value = "prdType")
    private String prdType;//产品细分
    @JsonProperty(value = "apprStatus")
    private String apprStatus;//客户名
    @JsonProperty(value = "cusId")
    private String cusId;//身份证号
    @JsonProperty(value = "certNo")
    private String certNo;//经营地址
    @JsonProperty(value = "operAddr")
    private String operAddr;//经营地址（定位）
    @JsonProperty(value = "mainBusi")
    private String mainBusi;//主营业务
    @JsonProperty(value = "applyAmt")
    private String applyAmt;//申请金额
    @JsonProperty(value = "applyTerm")
    private String applyTerm;//申请期限
    @JsonProperty(value = "loanResn")
    private String loanResn;//贷款原因
    @JsonProperty(value = "loanUse")
    private String loanUse;//贷款用途
    @JsonProperty(value = "repayMode")
    private String repayMode;//还款方式
    @JsonProperty(value = "grtMode")
    private String grtMode;//担保方式
    @JsonProperty(value = "chrem")
    private String chrem;//现金/银行存款/理财
    @JsonProperty(value = "actrec")
    private String actrec;//应收账款
    @JsonProperty(value = "depositAmt")
    private String depositAmt;//存货
    @JsonProperty(value = "equip")
    private String equip;//设备（净值）
    @JsonProperty(value = "ownRealpro")
    private String ownRealpro;//自有房产
    @JsonProperty(value = "ownCar")
    private String ownCar;//自有车辆
    @JsonProperty(value = "debtAmt")
    private String debtAmt;//负债总额
    @JsonProperty(value = "bankLoan")
    private String bankLoan;//银行借款
    @JsonProperty(value = "otherDebt")
    private String otherDebt;//其他负债
    @JsonProperty(value = "outguar")
    private String outguar;//对外担保
    @JsonProperty(value = "salesAmt")
    private String salesAmt;//销售收入-总额
    @JsonProperty(value = "costAmt")
    private String costAmt;//可变成本-总额
    @JsonProperty(value = "expendAmt")
    private String expendAmt;//固定支出-总额
    @JsonProperty(value = "profit")
    private String profit;//净利润
    @JsonProperty(value = "mearn")
    private BigDecimal mearn;//月均收入
    @JsonProperty(value = "cash")
    private BigDecimal cash;//现金
    @JsonProperty(value = "dep")
    private BigDecimal dep;//存款
    @JsonProperty(value = "realpro")
    private BigDecimal realpro;//房产
    @JsonProperty(value = "car")
    private BigDecimal car;//汽车
    @JsonProperty(value = "indivLoan")
    private BigDecimal indivLoan;//私人借款
    @JsonProperty(value = "outguaramt")
    private BigDecimal outguaramt;//对外担保金额
    @JsonProperty(value = "totalIncome")
    private BigDecimal totalIncome;//总收入
    @JsonProperty(value = "paybill")
    private BigDecimal paybill;//工资收入
    @JsonProperty(value = "operProfit")
    private BigDecimal operProfit;//经营利润
    @JsonProperty(value = "yearHomeSpend")
    private BigDecimal yearHomeSpend;//年家庭开支
    @JsonProperty(value = "workUnitName")
    private String workUnitName;//工作单位名称
    @JsonProperty(value = "list")
    private List list;//列表

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getOutguar() {
        return outguar;
    }

    public BigDecimal getOutguaramt() {
        return outguaramt;
    }

    public void setOutguaramt(BigDecimal outguaramt) {
        this.outguaramt = outguaramt;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    public String getPrdType() {
        return prdType;
    }

    public void setPrdType(String prdType) {
        this.prdType = prdType;
    }

    public String getApprStatus() {
        return apprStatus;
    }

    public void setApprStatus(String apprStatus) {
        this.apprStatus = apprStatus;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getOperAddr() {
        return operAddr;
    }

    public void setOperAddr(String operAddr) {
        this.operAddr = operAddr;
    }

    public String getMainBusi() {
        return mainBusi;
    }

    public void setMainBusi(String mainBusi) {
        this.mainBusi = mainBusi;
    }

    public String getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(String applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getApplyTerm() {
        return applyTerm;
    }

    public void setApplyTerm(String applyTerm) {
        this.applyTerm = applyTerm;
    }

    public String getLoanResn() {
        return loanResn;
    }

    public void setLoanResn(String loanResn) {
        this.loanResn = loanResn;
    }

    public String getLoanUse() {
        return loanUse;
    }

    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getGrtMode() {
        return grtMode;
    }

    public void setGrtMode(String grtMode) {
        this.grtMode = grtMode;
    }

    public String getChrem() {
        return chrem;
    }

    public void setChrem(String chrem) {
        this.chrem = chrem;
    }

    public String getActrec() {
        return actrec;
    }

    public void setActrec(String actrec) {
        this.actrec = actrec;
    }

    public String getDepositAmt() {
        return depositAmt;
    }

    public void setDepositAmt(String depositAmt) {
        this.depositAmt = depositAmt;
    }

    public String getEquip() {
        return equip;
    }

    public void setEquip(String equip) {
        this.equip = equip;
    }

    public String getOwnRealpro() {
        return ownRealpro;
    }

    public void setOwnRealpro(String ownRealpro) {
        this.ownRealpro = ownRealpro;
    }

    public String getOwnCar() {
        return ownCar;
    }

    public void setOwnCar(String ownCar) {
        this.ownCar = ownCar;
    }

    public String getDebtAmt() {
        return debtAmt;
    }

    public void setDebtAmt(String debtAmt) {
        this.debtAmt = debtAmt;
    }

    public String getBankLoan() {
        return bankLoan;
    }

    public void setBankLoan(String bankLoan) {
        this.bankLoan = bankLoan;
    }

    public String getOtherDebt() {
        return otherDebt;
    }

    public void setOtherDebt(String otherDebt) {
        this.otherDebt = otherDebt;
    }


    public void setOutguar(String outguar) {
        this.outguar = outguar;
    }

    public String getSalesAmt() {
        return salesAmt;
    }

    public void setSalesAmt(String salesAmt) {
        this.salesAmt = salesAmt;
    }

    public String getCostAmt() {
        return costAmt;
    }

    public void setCostAmt(String costAmt) {
        this.costAmt = costAmt;
    }

    public String getExpendAmt() {
        return expendAmt;
    }

    public void setExpendAmt(String expendAmt) {
        this.expendAmt = expendAmt;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }

    public BigDecimal getMearn() {
        return mearn;
    }

    public void setMearn(BigDecimal mearn) {
        this.mearn = mearn;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getDep() {
        return dep;
    }

    public void setDep(BigDecimal dep) {
        this.dep = dep;
    }

    public BigDecimal getRealpro() {
        return realpro;
    }

    public void setRealpro(BigDecimal realpro) {
        this.realpro = realpro;
    }

    public BigDecimal getCar() {
        return car;
    }

    public void setCar(BigDecimal car) {
        this.car = car;
    }

    public BigDecimal getIndivLoan() {
        return indivLoan;
    }

    public void setIndivLoan(BigDecimal indivLoan) {
        this.indivLoan = indivLoan;
    }



    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public BigDecimal getPaybill() {
        return paybill;
    }

    public void setPaybill(BigDecimal paybill) {
        this.paybill = paybill;
    }

    public BigDecimal getOperProfit() {
        return operProfit;
    }

    public void setOperProfit(BigDecimal operProfit) {
        this.operProfit = operProfit;
    }

    public BigDecimal getYearHomeSpend() {
        return yearHomeSpend;
    }

    public void setYearHomeSpend(BigDecimal yearHomeSpend) {
        this.yearHomeSpend = yearHomeSpend;
    }

    public String getWorkUnitName() {
        return workUnitName;
    }

    public void setWorkUnitName(String workUnitName) {
        this.workUnitName = workUnitName;
    }

    @Override
    public String toString() {
        return "Xdxw0003DataReqDto{" +
                "cusName='" + cusName + '\'' +
                ", managerId='" + managerId + '\'' +
                ", serno='" + serno + '\'' +
                ", surveyType='" + surveyType + '\'' +
                ", prdType='" + prdType + '\'' +
                ", apprStatus='" + apprStatus + '\'' +
                ", cusId='" + cusId + '\'' +
                ", certNo='" + certNo + '\'' +
                ", operAddr='" + operAddr + '\'' +
                ", mainBusi='" + mainBusi + '\'' +
                ", applyAmt='" + applyAmt + '\'' +
                ", applyTerm='" + applyTerm + '\'' +
                ", loanResn='" + loanResn + '\'' +
                ", loanUse='" + loanUse + '\'' +
                ", repayMode='" + repayMode + '\'' +
                ", grtMode='" + grtMode + '\'' +
                ", chrem='" + chrem + '\'' +
                ", actrec='" + actrec + '\'' +
                ", depositAmt='" + depositAmt + '\'' +
                ", equip='" + equip + '\'' +
                ", ownRealpro='" + ownRealpro + '\'' +
                ", ownCar='" + ownCar + '\'' +
                ", debtAmt='" + debtAmt + '\'' +
                ", bankLoan='" + bankLoan + '\'' +
                ", otherDebt='" + otherDebt + '\'' +
                ", outguar='" + outguar + '\'' +
                ", salesAmt='" + salesAmt + '\'' +
                ", costAmt='" + costAmt + '\'' +
                ", expendAmt='" + expendAmt + '\'' +
                ", profit='" + profit + '\'' +
                ", mearn=" + mearn +
                ", cash=" + cash +
                ", dep=" + dep +
                ", realpro=" + realpro +
                ", car=" + car +
                ", indivLoan=" + indivLoan +
                ", outguaramt=" + outguaramt +
                ", totalIncome=" + totalIncome +
                ", paybill=" + paybill +
                ", operProfit=" + operProfit +
                ", yearHomeSpend=" + yearHomeSpend +
                ", workUnitName='" + workUnitName + '\'' +
                ", list=" + list +
                '}';
    }
}
