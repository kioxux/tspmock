package cn.com.yusys.yusp.dto.server.xdsx0020.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：担保公司合作协议查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0020DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "custid")
	private String custid;//合作方客户号
	@JsonProperty(value = "custna")
	private String custna;//合作客户名称
	@JsonProperty(value = "contno")
	private String contno;//合作协议编号
	@JsonProperty(value = "cnhtno")
	private String cnhtno;//中文合同编号
	@JsonProperty(value = "dbhstp")
	private String dbhstp;//担保公司类型
	@JsonProperty(value = "conamt")
	private BigDecimal conamt;//协议金额
	@JsonProperty(value = "stdate")
	private String stdate;//合作协议起始日期
	@JsonProperty(value = "eddate")
	private String eddate;//合作协议到期日期
	@JsonProperty(value = "sxxybh")
	private String sxxybh;//授信协议编号
	@JsonProperty(value = "sxqsrq")
	private String sxqsrq;//授信起始日期
	@JsonProperty(value = "sxdqrq")
	private String sxdqrq;//授信到期日期
	@JsonProperty(value = "hzzeed")
	private BigDecimal hzzeed;//合作总额度
	@JsonProperty(value = "sigamt")
	private BigDecimal sigamt;//单笔业务合作额度
	@JsonProperty(value = "zxbhed")
	private BigDecimal zxbhed;//在线保函额度
	@JsonProperty(value = "zdseam")
	private BigDecimal zdseam;//最低保证金金额
	@JsonProperty(value = "sepert")
	private BigDecimal sepert;//保证金比例
	@JsonProperty(value = "dhxeee")
	private BigDecimal dhxeee;//单户限额
	@JsonProperty(value = "magtim")
	private BigDecimal magtim;//批复放大倍数
	@JsonProperty(value = "xtcsbs")
	private BigDecimal xtcsbs;//系统测算倍数
	@JsonProperty(value = "bzjdcd")
	private BigDecimal bzjdcd;//保证金代偿宽限期
	@JsonProperty(value = "costat")
	private String costat;//协议状态
	@JsonProperty(value = "bzjacc")
	private String bzjacc;//保证金账号
	@JsonProperty(value = "inptid")
	private String inptid;//登记人
	@JsonProperty(value = "mangid")
	private String mangid;//责任人
	@JsonProperty(value = "inbrid")
	private String inbrid;//登记人机构
	@JsonProperty(value = "mabrid")
	private String mabrid;//责任人机构
	@JsonProperty(value = "indate")
	private String indate;//登记日期

	public String getCustid() {
		return custid;
	}

	public void setCustid(String custid) {
		this.custid = custid;
	}

	public String getCustna() {
		return custna;
	}

	public void setCustna(String custna) {
		this.custna = custna;
	}

	public String getContno() {
		return contno;
	}

	public void setContno(String contno) {
		this.contno = contno;
	}

	public String getCnhtno() {
		return cnhtno;
	}

	public void setCnhtno(String cnhtno) {
		this.cnhtno = cnhtno;
	}

	public String getDbhstp() {
		return dbhstp;
	}

	public void setDbhstp(String dbhstp) {
		this.dbhstp = dbhstp;
	}

	public BigDecimal getConamt() {
		return conamt;
	}

	public void setConamt(BigDecimal conamt) {
		this.conamt = conamt;
	}

	public String getStdate() {
		return stdate;
	}

	public void setStdate(String stdate) {
		this.stdate = stdate;
	}

	public String getEddate() {
		return eddate;
	}

	public void setEddate(String eddate) {
		this.eddate = eddate;
	}

	public String getSxxybh() {
		return sxxybh;
	}

	public void setSxxybh(String sxxybh) {
		this.sxxybh = sxxybh;
	}

	public String getSxqsrq() {
		return sxqsrq;
	}

	public void setSxqsrq(String sxqsrq) {
		this.sxqsrq = sxqsrq;
	}

	public String getSxdqrq() {
		return sxdqrq;
	}

	public void setSxdqrq(String sxdqrq) {
		this.sxdqrq = sxdqrq;
	}

	public BigDecimal getHzzeed() {
		return hzzeed;
	}

	public void setHzzeed(BigDecimal hzzeed) {
		this.hzzeed = hzzeed;
	}

	public BigDecimal getSigamt() {
		return sigamt;
	}

	public void setSigamt(BigDecimal sigamt) {
		this.sigamt = sigamt;
	}

	public BigDecimal getZxbhed() {
		return zxbhed;
	}

	public void setZxbhed(BigDecimal zxbhed) {
		this.zxbhed = zxbhed;
	}

	public BigDecimal getZdseam() {
		return zdseam;
	}

	public void setZdseam(BigDecimal zdseam) {
		this.zdseam = zdseam;
	}

	public BigDecimal getSepert() {
		return sepert;
	}

	public void setSepert(BigDecimal sepert) {
		this.sepert = sepert;
	}

	public BigDecimal getDhxeee() {
		return dhxeee;
	}

	public void setDhxeee(BigDecimal dhxeee) {
		this.dhxeee = dhxeee;
	}

	public BigDecimal getMagtim() {
		return magtim;
	}

	public void setMagtim(BigDecimal magtim) {
		this.magtim = magtim;
	}

	public BigDecimal getXtcsbs() {
		return xtcsbs;
	}

	public void setXtcsbs(BigDecimal xtcsbs) {
		this.xtcsbs = xtcsbs;
	}

	public BigDecimal getBzjdcd() {
		return bzjdcd;
	}

	public void setBzjdcd(BigDecimal bzjdcd) {
		this.bzjdcd = bzjdcd;
	}

	public String getCostat() {
		return costat;
	}

	public void setCostat(String costat) {
		this.costat = costat;
	}

	public String getBzjacc() {
		return bzjacc;
	}

	public void setBzjacc(String bzjacc) {
		this.bzjacc = bzjacc;
	}

	public String getInptid() {
		return inptid;
	}

	public void setInptid(String inptid) {
		this.inptid = inptid;
	}

	public String getMangid() {
		return mangid;
	}

	public void setMangid(String mangid) {
		this.mangid = mangid;
	}

	public String getInbrid() {
		return inbrid;
	}

	public void setInbrid(String inbrid) {
		this.inbrid = inbrid;
	}

	public String getMabrid() {
		return mabrid;
	}

	public void setMabrid(String mabrid) {
		this.mabrid = mabrid;
	}

	public String getIndate() {
		return indate;
	}

	public void setIndate(String indate) {
		this.indate = indate;
	}

	@Override
	public String toString() {
		return "Xdsx0020DataRespDto{" +
				"custid='" + custid + '\'' +
				", custna='" + custna + '\'' +
				", contno='" + contno + '\'' +
				", cnhtno='" + cnhtno + '\'' +
				", dbhstp='" + dbhstp + '\'' +
				", conamt=" + conamt +
				", stdate='" + stdate + '\'' +
				", eddate='" + eddate + '\'' +
				", sxxybh='" + sxxybh + '\'' +
				", sxqsrq='" + sxqsrq + '\'' +
				", sxdqrq='" + sxdqrq + '\'' +
				", hzzeed=" + hzzeed +
				", sigamt=" + sigamt +
				", zxbhed=" + zxbhed +
				", zdseam=" + zdseam +
				", sepert=" + sepert +
				", dhxeee=" + dhxeee +
				", magtim=" + magtim +
				", xtcsbs=" + xtcsbs +
				", bzjdcd=" + bzjdcd +
				", costat='" + costat + '\'' +
				", bzjacc='" + bzjacc + '\'' +
				", inptid='" + inptid + '\'' +
				", mangid='" + mangid + '\'' +
				", inbrid='" + inbrid + '\'' +
				", mabrid='" + mabrid + '\'' +
				", indate='" + indate + '\'' +
				'}';
	}
}
