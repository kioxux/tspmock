package cn.com.yusys.yusp.dto.server.xdtz0033.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询客户所担保的行内贷款五级分类非正常状态件数
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0033DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "grtLoanNotNormalCnt")
    private String grtLoanNotNormalCnt;//所担保的行内贷款五级分类非正常状态件数


    public String getGrtLoanNotNormalCnt() {
        return grtLoanNotNormalCnt;
    }

    public void setGrtLoanNotNormalCnt(String grtLoanNotNormalCnt) {
        this.grtLoanNotNormalCnt = grtLoanNotNormalCnt;
    }

    @Override
    public String toString() {
        return "Xdtz0033DataRespDto{" +
                "grtLoanNotNormalCnt='" + grtLoanNotNormalCnt + '\'' +
                '}';
    }
}
