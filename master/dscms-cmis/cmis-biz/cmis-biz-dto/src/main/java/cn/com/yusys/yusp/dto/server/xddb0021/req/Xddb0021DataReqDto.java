package cn.com.yusys.yusp.dto.server.xddb0021.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：根据客户名查询抵押物类型
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0021DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guaranty_id")
    @NotNull(message = "押品编号guaranty_id不能为空")
    @NotEmpty(message = "押品编号guaranty_id不能为空")
    private String guaranty_id;//押品编号

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    @Override
    public String toString() {
        return "Xddb0021DataReqDto{" +
                "guaranty_id='" + guaranty_id + '\'' +
                '}';
    }
}
