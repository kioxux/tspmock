package cn.com.yusys.yusp.dto.server.xdxw0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    private String serno;//业务流水号
    @JsonProperty(value = "loanType")
    private String loanType;//贷款类型
    @JsonProperty(value = "isHavingBillBal")
    private String isHavingBillBal;//借据余额是否大于0
    @JsonProperty(value = "isMicroDept")
    private String isMicroDept;//是否属于小微部门标志
    @JsonProperty(value = "isCrdApprStatusPending")
    private String isCrdApprStatusPending;//授信审批状态是否“审批中”
    @JsonProperty(value = "isApprVaild")
    private String isApprVaild;//批复是否有效
    @JsonProperty(value = "isContVaild")
    private String isContVaild;//合同是否有效
    @JsonProperty(value = "managerId")
    private String managerId;//管户客户经理工号
    @JsonProperty(value = "managerName")
    private String managerName;//管户客户经理姓名
    @JsonProperty(value = "orgNo")
    private String orgNo;//客户经理所在机构编号
    @JsonProperty(value = "orgName")
    private String orgName;//客户经理所在机构名称
    @JsonProperty(value = "teamType")
    private String teamType;//直营团队类型
    @JsonProperty(value = "surveyType")
    private String surveyType;//调查类型

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getIsHavingBillBal() {
        return isHavingBillBal;
    }

    public void setIsHavingBillBal(String isHavingBillBal) {
        this.isHavingBillBal = isHavingBillBal;
    }

    public String getIsMicroDept() {
        return isMicroDept;
    }

    public void setIsMicroDept(String isMicroDept) {
        this.isMicroDept = isMicroDept;
    }

    public String getIsCrdApprStatusPending() {
        return isCrdApprStatusPending;
    }

    public void setIsCrdApprStatusPending(String isCrdApprStatusPending) {
        this.isCrdApprStatusPending = isCrdApprStatusPending;
    }

    public String getIsApprVaild() {
        return isApprVaild;
    }

    public void setIsApprVaild(String isApprVaild) {
        this.isApprVaild = isApprVaild;
    }

    public String getIsContVaild() {
        return isContVaild;
    }

    public void setIsContVaild(String isContVaild) {
        this.isContVaild = isContVaild;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                ", loanType='" + loanType + '\'' +
                ", isHavingBillBal='" + isHavingBillBal + '\'' +
                ", isMicroDept='" + isMicroDept + '\'' +
                ", isCrdApprStatusPending='" + isCrdApprStatusPending + '\'' +
                ", isApprVaild='" + isApprVaild + '\'' +
                ", isContVaild='" + isContVaild + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", orgName='" + orgName + '\'' +
                ", teamType='" + teamType + '\'' +
                ", surveyType='" + surveyType + '\'' +
                '}';
    }
}
