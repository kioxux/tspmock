package cn.com.yusys.yusp.dto.server.xdxt0009.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：客户经理是否为小微客户经理
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0009DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isMicroManager")
    private String isMicroManager;//是否小微客户经理标志
    @JsonProperty(value = "isNewManager")
    private String isNewManager;//是否新客户经理标志
    @JsonProperty(value = "deptChiefId")
    private String deptChiefId;//分中心负责人工号
    @JsonProperty(value = "teamType")
    private String teamType;//直营团队类型
    @JsonProperty(value = "surveyType")
    private String surveyType;//调查类型
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//客户经理机构号
    @JsonProperty(value = "managerBrName")
    private String managerBrName;//客户经理机构名称
    @JsonProperty(value = "managerDept")
    private String managerDept;//客户经理所属分中心
    @JsonProperty(value = "isDispatch")
    private String isDispatch;//是否派遣员工
    @JsonProperty(value = "isLargeCredit")
    private String isLargeCredit;//大额信用权限

    public String getIsDispatch() {
        return isDispatch;
    }

    public void setIsDispatch(String isDispatch) {
        this.isDispatch = isDispatch;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerBrName() {
        return managerBrName;
    }

    public void setManagerBrName(String managerBrName) {
        this.managerBrName = managerBrName;
    }

    public String getManagerDept() {
        return managerDept;
    }

    public void setManagerDept(String managerDept) {
        this.managerDept = managerDept;
    }

    public String getIsMicroManager() {
        return isMicroManager;
    }

    public void setIsMicroManager(String isMicroManager) {
        this.isMicroManager = isMicroManager;
    }

    public String getIsNewManager() {
        return isNewManager;
    }

    public void setIsNewManager(String isNewManager) {
        this.isNewManager = isNewManager;
    }

    public String getDeptChiefId() {
        return deptChiefId;
    }

    public void setDeptChiefId(String deptChiefId) {
        this.deptChiefId = deptChiefId;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    public String getIsLargeCredit() {
        return isLargeCredit;
    }

    public void setIsLargeCredit(String isLargeCredit) {
        this.isLargeCredit = isLargeCredit;
    }

    @Override
    public String toString() {
        return "Xdxt0009DataRespDto{" +
                "isMicroManager='" + isMicroManager + '\'' +
                ", isNewManager='" + isNewManager + '\'' +
                ", deptChiefId='" + deptChiefId + '\'' +
                ", teamType='" + teamType + '\'' +
                ", surveyType='" + surveyType + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", managerBrName='" + managerBrName + '\'' +
                ", managerDept='" + managerDept + '\'' +
                ", isDispatch='" + isDispatch + '\'' +
                ", isLargeCredit='" + isLargeCredit + '\'' +
                '}';
    }
}
