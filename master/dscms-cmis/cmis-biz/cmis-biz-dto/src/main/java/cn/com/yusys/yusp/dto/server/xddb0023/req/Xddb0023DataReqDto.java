package cn.com.yusys.yusp.dto.server.xddb0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 响应Dto：更新抵质押品状态
 *
 * @author zdl
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0023DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ypseno")
    @NotNull(message = "核心担保编号ypseno不能为空")
    @NotEmpty(message = "核心担保编号ypseno不能为空")
    private String ypseno;//核心担保编号
    @JsonProperty(value = "status")
    @NotNull(message = "出入库状态status不能为空")
    @NotEmpty(message = "出入库状态status不能为空")
    private String status;//出入库状态

    public String getYpseno() {
        return ypseno;
    }

    public void setYpseno(String ypseno) {
        this.ypseno = ypseno;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Xddb0023DataReqDto{" +
                "ypseno='" + ypseno + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
