package cn.com.yusys.yusp.dto.server.cmisbiz0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class CurrentMonthAllowLoanBlanceDto implements Serializable {

    private static final long serialVersionUID = 1239950604132596713L;

    @JsonProperty("orgId")
    private String orgId;

    @JsonProperty("currentMonthAllowLoanBlance")
    private BigDecimal currentMonthAllowLoanBlance;

    @JsonProperty("errorCode")
    private String errorCode;

    @JsonProperty("errorMsg")
    private String errorMsg;


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public BigDecimal getCurrentMonthAllowLoanBlance() {
        return currentMonthAllowLoanBlance;
    }

    public void setCurrentMonthAllowLoanBlance(BigDecimal currentMonthAllowLoanBlance) {
        this.currentMonthAllowLoanBlance = currentMonthAllowLoanBlance;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        return "CurrentMonthAllowLoanBlanceDto{" +
                "orgId='" + orgId + '\'' +
                ", currentMonthAllowLoanBlance=" + currentMonthAllowLoanBlance +
                ", errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
