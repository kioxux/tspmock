package cn.com.yusys.yusp.dto.server.xdxw0086.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：涉税保密信息查询委托授权书文本生产PDF
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0086DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "legalName")
    private String legalName;//法定代表人
    @JsonProperty(value = "companyName")
    private String companyName;//公司名称
    @JsonProperty(value = "idNo")
    private String idNo;//法人身份证号码
    @JsonProperty(value = "signDate")
    private String signDate;//申请日期

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    @Override
    public String toString() {
        return "Xdxw0086DataReqDto{" +
                "legalName='" + legalName + '\'' +
                "companyName='" + companyName + '\'' +
                "idNo='" + idNo + '\'' +
                "signDate='" + signDate + '\'' +
                '}';
    }
}
