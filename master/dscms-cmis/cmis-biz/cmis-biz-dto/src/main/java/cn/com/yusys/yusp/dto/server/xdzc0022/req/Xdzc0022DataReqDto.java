package cn.com.yusys.yusp.dto.server.xdzc0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：发票补录
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0022DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//出账流水号
    @JsonProperty(value = "yxSerno")
    private String yxSerno;//发票影像ID

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getYxSerno() {
        return yxSerno;
    }

    public void setYxSerno(String yxSerno) {
        this.yxSerno = yxSerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billNo='" + billNo + '\'' +
                ", yxSerno='" + yxSerno + '\'' +
                '}';
    }
}
