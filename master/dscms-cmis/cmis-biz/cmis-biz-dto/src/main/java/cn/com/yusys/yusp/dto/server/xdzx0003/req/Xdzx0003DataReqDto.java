package cn.com.yusys.yusp.dto.server.xdzx0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：征信查询授权状态同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzx0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//状态
    @JsonProperty(value = "iamgeNo")
    private String iamgeNo;//影像流水号
    @JsonProperty(value = "crqlSerno")
    private String crqlSerno;//征信查询申请流水号

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getIamgeNo() {
        return iamgeNo;
    }

    public void setIamgeNo(String iamgeNo) {
        this.iamgeNo = iamgeNo;
    }

    public String getCrqlSerno() {
        return crqlSerno;
    }

    public void setCrqlSerno(String crqlSerno) {
        this.crqlSerno = crqlSerno;
    }

    @Override
    public String toString() {
        return "Xdzx0003DataReqDto{" +
                "approveStatus='" + approveStatus + '\'' +
                ", iamgeNo='" + iamgeNo + '\'' +
                ", crqlSerno='" + crqlSerno + '\'' +
                '}';
    }
}
