package cn.com.yusys.yusp.dto.server.xdht0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：合同信息列表查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "bailAcct")
    private String bailAcct;//保证金账户
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "bizType")
    private String bizType;//业务品种
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "avlBal")
    private BigDecimal avlBal;//可用余额
    @JsonProperty(value = "bailRate")
    private BigDecimal bailRate;//保证金比例
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "exchgRate")
    private BigDecimal exchgRate;//汇率
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同到期日
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "loanForm")
    private String loanForm;//贷款形式

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBailAcct() {
        return bailAcct;
    }

    public void setBailAcct(String bailAcct) {
        this.bailAcct = bailAcct;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getAvlBal() {
        return avlBal;
    }

    public void setAvlBal(BigDecimal avlBal) {
        this.avlBal = avlBal;
    }

    public BigDecimal getBailRate() {
        return bailRate;
    }

    public void setBailRate(BigDecimal bailRate) {
        this.bailRate = bailRate;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public BigDecimal getExchgRate() {
        return exchgRate;
    }

    public void setExchgRate(BigDecimal exchgRate) {
        this.exchgRate = exchgRate;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getLoanForm() {
        return loanForm;
    }

    public void setLoanForm(String loanForm) {
        this.loanForm = loanForm;
    }

    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                ", serno='" + serno + '\'' +
                ", bailAcct='" + bailAcct + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", contType='" + contType + '\'' +
                ", bizType='" + bizType + '\'' +
                ", cusName='" + cusName + '\'' +
                ", cusId='" + cusId + '\'' +
                ", curType='" + curType + '\'' +
                ", contAmt='" + contAmt + '\'' +
                ", avlBal='" + avlBal + '\'' +
                ", bailRate=" + bailRate +
                ", assureMeans='" + assureMeans + '\'' +
                ", exchgRate=" + exchgRate +
                ", contStartDate='" + contStartDate + '\'' +
                ", contEndDate='" + contEndDate + '\'' +
                ", inputId='" + inputId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", loanForm='" + loanForm + '\'' +
                '}';
    }
}
