package cn.com.yusys.yusp.dto.server.xdzx0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：征信授权查看
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzx0002DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    @Override
    public String toString() {
        return "Xdzx0002DataReqDto{" +
                "certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                '}';
    }
}
