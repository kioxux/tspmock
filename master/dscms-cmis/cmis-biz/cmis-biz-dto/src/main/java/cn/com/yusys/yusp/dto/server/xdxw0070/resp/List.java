package cn.com.yusys.yusp.dto.server.xdxw0070.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：授信侧面调查信息查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//主键
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//外键
    @JsonProperty(value = "channel")
    private String channel;//渠道
    @JsonProperty(value = "tel")
    private String tel;//电话
    @JsonProperty(value = "cus_list_serno")
    private String cus_list_serno;//名单表流水号
    @JsonProperty(value = "remark")
    private String remark;//备注

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCus_list_serno() {
        return cus_list_serno;
    }

    public void setCus_list_serno(String cus_list_serno) {
        this.cus_list_serno = cus_list_serno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                "survey_serno='" + survey_serno + '\'' +
                "channel='" + channel + '\'' +
                "tel='" + tel + '\'' +
                "cus_list_serno='" + cus_list_serno + '\'' +
                "remark='" + remark + '\'' +
                '}';
    }
}
