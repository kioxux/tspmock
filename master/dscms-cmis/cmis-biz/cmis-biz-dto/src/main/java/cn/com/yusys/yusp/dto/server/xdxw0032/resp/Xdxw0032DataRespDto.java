package cn.com.yusys.yusp.dto.server.xdxw0032.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：查询信贷系统的审批历史信息
 * @author xuchao
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0032DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "apprHisList")
	private List<ApprHisList> apprHisLists;//start

	public List<ApprHisList> getApprHisLists() {
		return apprHisLists;
	}

	public void setApprHisLists(List<ApprHisList> apprHisLists) {
		this.apprHisLists = apprHisLists;
	}

	@Override
	public String toString() {
		return "Data{" +
				"apprHisLists=" + apprHisLists +
				'}';
	}
}  
