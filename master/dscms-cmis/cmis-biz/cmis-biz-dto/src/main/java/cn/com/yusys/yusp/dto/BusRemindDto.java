package cn.com.yusys.yusp.dto;

import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BusRemindDto
 * @类描述: 首页提醒打回/否决数据实体类
 * @功能描述:
 * @创建人:
 * @创建时间: 2021-02-05 16:49:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class BusRemindDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 业务流水号 **/
    private String bizId;

    /** 流程名称 **/
    private String flowName;

    /** 客户名称 **/
    private String bizUserName;

    /** 审批节点 **/
    private String nodeName;

    /** 审批结果 **/
    private String userComment;

    /** 审批人 **/
    private String userName;

    /** 流程发起时间 **/
    private String startTime;

    /** 流程状态码值 **/
    private String flowState;

    /** 流程状态 **/
    private String lookupItemName;

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }

    public String getFlowName() {
        return flowName;
    }

    public void setFlowName(String flowName) {
        this.flowName = flowName;
    }

    public String getBizUserName() {
        return bizUserName;
    }

    public void setBizUserName(String bizUserName) {
        this.bizUserName = bizUserName;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFlowState() {
        return flowState;
    }

    public void setFlowState(String flowState) {
        this.flowState = flowState;
    }

    public String getLookupItemName() {
        return lookupItemName;
    }

    public void setLookupItemName(String lookupItemName) {
        this.lookupItemName = lookupItemName;
    }

}
