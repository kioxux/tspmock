package cn.com.yusys.yusp.dto.server.xdxw0021.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0021DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "disbStatus")
    private String disbStatus;//放款状态
    @JsonProperty(value = "billStatus")
    private String billStatus;//借据状态

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getDisbStatus() {
        return disbStatus;
    }

    public void setDisbStatus(String disbStatus) {
        this.disbStatus = disbStatus;
    }

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    @Override
    public String toString() {
        return "Xdxw0021DataRespDto{" +
                "contStatus='" + contStatus + '\'' +
                ", disbStatus='" + disbStatus + '\'' +
                ", billStatus='" + billStatus + '\'' +
                '}';
    }
}
