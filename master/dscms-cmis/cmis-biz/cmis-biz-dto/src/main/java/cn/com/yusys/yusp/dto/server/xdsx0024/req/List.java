package cn.com.yusys.yusp.dto.server.xdsx0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：省心快贷plus授信，风控自动审批结果推送信贷
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "rule_code")
    private String rule_code;//规则编号
    @JsonProperty(value = "rule_name")
    private String rule_name;//规则名称
    @JsonProperty(value = "execute_result_code")
    private String execute_result_code;//规则执行结果代码
    @JsonProperty(value = "execute_result_remark")
    private String execute_result_remark;//规则执行结果描述

    public String getRule_code() {
        return rule_code;
    }

    public void setRule_code(String rule_code) {
        this.rule_code = rule_code;
    }

    public String getRule_name() {
        return rule_name;
    }

    public void setRule_name(String rule_name) {
        this.rule_name = rule_name;
    }

    public String getExecute_result_code() {
        return execute_result_code;
    }

    public void setExecute_result_code(String execute_result_code) {
        this.execute_result_code = execute_result_code;
    }

    public String getExecute_result_remark() {
        return execute_result_remark;
    }

    public void setExecute_result_remark(String execute_result_remark) {
        this.execute_result_remark = execute_result_remark;
    }

    @Override
    public String toString() {
        return "List{" +
                "rule_code='" + rule_code + '\'' +
                "rule_name='" + rule_name + '\'' +
                "execute_result_code='" + execute_result_code + '\'' +
                "execute_result_remark='" + execute_result_remark + '\'' +
                '}';
    }
}
