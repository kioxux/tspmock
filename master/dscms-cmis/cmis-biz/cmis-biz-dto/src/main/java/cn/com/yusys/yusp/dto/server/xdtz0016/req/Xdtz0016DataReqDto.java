package cn.com.yusys.yusp.dto.server.xdtz0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：通知信贷系统更新贴现台账状态
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0016DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "drftNo")
    @NotNull(message = "票号drftNo不能为空")
    @NotEmpty(message = "票号drftNo不能为空")
    private String drftNo;//票号
    @JsonProperty(value = "drftStatus")
    @NotNull(message = "票据状态drftStatus不能为空")
    @NotEmpty(message = "票据状态drftStatus不能为空")
    private String drftStatus;//票据状态

    public String getDrftNo() {
        return drftNo;
    }

    public void setDrftNo(String drftNo) {
        this.drftNo = drftNo;
    }

    public String getDrftStatus() {
        return drftStatus;
    }

    public void setDrftStatus(String drftStatus) {
        this.drftStatus = drftStatus;
    }

}
