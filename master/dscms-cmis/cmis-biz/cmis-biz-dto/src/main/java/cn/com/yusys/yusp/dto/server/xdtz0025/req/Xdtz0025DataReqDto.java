package cn.com.yusys.yusp.dto.server.xdtz0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：查询指定贷款开始日的优企贷客户贷款余额合计
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0025DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "loanStartDate")
    @NotNull(message = "贷款开始日loanStartDate不能为空")
    @NotEmpty(message = "贷款开始日loanStartDate不能为空")
    private String loanStartDate;//贷款开始日

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    @Override
    public String toString() {
        return "Xdtz0025DataReqDto{" +
                "loanStartDate='" + loanStartDate + '\'' +
                '}';
    }
}
