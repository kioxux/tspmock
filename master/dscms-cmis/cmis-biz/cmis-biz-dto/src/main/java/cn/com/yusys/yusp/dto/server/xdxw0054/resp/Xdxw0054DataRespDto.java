package cn.com.yusys.yusp.dto.server.xdxw0054.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询优抵贷损益表明细
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0054DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "nearlyYear")
    private BigDecimal nearlyYear;//最近一个完整年度

    public BigDecimal getNearlyYear() {
        return nearlyYear;
    }

    public void setNearlyYear(BigDecimal nearlyYear) {
        this.nearlyYear = nearlyYear;
    }

    @Override
    public String toString() {
        return "Xdxw0054DataRespDto{" +
                "nearlyYear='" + nearlyYear + '\'' +
                '}';
    }
}
