package cn.com.yusys.yusp.dto.server.xdht0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据zheng获取信贷合同信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0026DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号(身份证号)
    @JsonProperty(value = "cont_state")
    private String cont_state;//合同状态


    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    @Override
    public String toString() {
        return "Xdht0026DataReqDto{" +
                ", cert_code='" + cert_code + '\'' +
                ", cont_state='" + cont_state + '\'' +
                '}';
    }
}
