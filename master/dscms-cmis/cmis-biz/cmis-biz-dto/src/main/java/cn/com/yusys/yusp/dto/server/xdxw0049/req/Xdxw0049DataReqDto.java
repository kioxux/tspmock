package cn.com.yusys.yusp.dto.server.xdxw0049.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：智能风控删除通知
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0049DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//业务流水号

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    @Override
    public String toString() {
        return "Xdxw0049DataReqDto{" +
                "survey_serno='" + survey_serno + '\'' +
                '}';
    }
}
