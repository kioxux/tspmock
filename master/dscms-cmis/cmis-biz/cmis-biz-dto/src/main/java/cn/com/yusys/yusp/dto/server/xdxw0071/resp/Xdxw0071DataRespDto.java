package cn.com.yusys.yusp.dto.server.xdxw0071.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询优抵贷抵质押品信息
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0071DataRespDto implements Serializable {
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdxw0071.resp.List> list;

    public java.util.List<cn.com.yusys.yusp.dto.server.xdxw0071.resp.List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdxw0071DataRespDto{" +
                "list=" + list +
                '}';
    }
}
