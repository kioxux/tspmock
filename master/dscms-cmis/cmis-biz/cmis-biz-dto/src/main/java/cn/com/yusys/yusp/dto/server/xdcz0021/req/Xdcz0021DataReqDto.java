package cn.com.yusys.yusp.dto.server.xdcz0021.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：支用申请
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0021DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pvpAmt")
    private BigDecimal pvpAmt;//出账金额
    @JsonProperty(value = "billStartDate")
    private String billStartDate;//借据起始日期
    @JsonProperty(value = "billTerm")
    private String billTerm;//借据期限
    @JsonProperty(value = "billEndDate")
    private String billEndDate;//借据到期日
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "debitAcctNo")
    private String debitAcctNo;//借款人账号
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//贷款用途
    @JsonProperty(value = "recomManagerId")
    private String recomManagerId;//推荐客户经理号
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号
    @JsonProperty(value = "loanSour")
    private String loanSour;//贷款来源
    @JsonProperty(value = "isIntFree")
    private String isIntFree;//是否免息
    @JsonProperty(value = "intFreeDay")
    private String intFreeDay;//免息天数
    @JsonProperty(value = "preferCode")
    private String preferCode;//优惠券编号
    @JsonProperty(value = "billAmount")
    private String billAmount;//无还本借据金额
    @JsonProperty(value = "oldBillNo")
    private String oldBillNo;//无还本原借据


    public BigDecimal getPvpAmt() {
        return pvpAmt;
    }

    public void setPvpAmt(BigDecimal pvpAmt) {
        this.pvpAmt = pvpAmt;
    }

    public String getBillStartDate() {
        return billStartDate;
    }

    public void setBillStartDate(String billStartDate) {
        this.billStartDate = billStartDate;
    }

    public String getBillTerm() {
        return billTerm;
    }

    public void setBillTerm(String billTerm) {
        this.billTerm = billTerm;
    }

    public String getBillEndDate() {
        return billEndDate;
    }

    public void setBillEndDate(String billEndDate) {
        this.billEndDate = billEndDate;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getDebitAcctNo() {
        return debitAcctNo;
    }

    public void setDebitAcctNo(String debitAcctNo) {
        this.debitAcctNo = debitAcctNo;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getRecomManagerId() {
        return recomManagerId;
    }

    public void setRecomManagerId(String recomManagerId) {
        this.recomManagerId = recomManagerId;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getLoanSour() {
        return loanSour;
    }

    public void setLoanSour(String loanSour) {
        this.loanSour = loanSour;
    }

    public String getIsIntFree() {
        return isIntFree;
    }

    public void setIsIntFree(String isIntFree) {
        this.isIntFree = isIntFree;
    }

    public String getIntFreeDay() {
        return intFreeDay;
    }

    public void setIntFreeDay(String intFreeDay) {
        this.intFreeDay = intFreeDay;
    }

    public String getPreferCode() {
        return preferCode;
    }

    public void setPreferCode(String preferCode) {
        this.preferCode = preferCode;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getOldBillNo() {
        return oldBillNo;
    }

    public void setOldBillNo(String oldBillNo) {
        this.oldBillNo = oldBillNo;
    }

    @Override
    public String toString() {
        return "Xdcz0021ReqDto{" +
                "pvpAmt='" + pvpAmt + '\'' +
                "billStartDate='" + billStartDate + '\'' +
                "billTerm='" + billTerm + '\'' +
                "billEndDate='" + billEndDate + '\'' +
                "repayType='" + repayType + '\'' +
                "debitAcctNo='" + debitAcctNo + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "recomManagerId='" + recomManagerId + '\'' +
                "certNo='" + certNo + '\'' +
                "loanContNo='" + loanContNo + '\'' +
                "loanSour='" + loanSour + '\'' +
                "isIntFree ='" + isIntFree + '\'' +
                "intFreeDay='" + intFreeDay + '\'' +
                "preferCode='" + preferCode + '\'' +
                "billAmount='" + billAmount + '\'' +
                "oldBillNo='" + oldBillNo + '\'' +
                '}';
    }
}
