package cn.com.yusys.yusp.dto;

public class LmtOtherAppRelDto {
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 授信申请流水号 **/
    private String lmtSerno;

    /** 其他申报事项类型 **/
    private String lmtOtherAppType;

    /** 申请申报事项流水号 **/
    private String lmtOtherAppSerno;

    /** 审批状态 **/
    private String approveStatus;

    /** 操作类型 **/
    private String oprType;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最近修改人 **/
    private String updId;

    /** 最近修改机构 **/
    private String updBrId;

    /** 最近修改日期 **/
    private String updDate;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    // 归属成员名称
    private String cusId;


    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    /**
     * @return pkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param lmtSerno
     */
    public void setLmtSerno(String lmtSerno) {
        this.lmtSerno = lmtSerno;
    }

    /**
     * @return lmtSerno
     */
    public String getLmtSerno() {
        return this.lmtSerno;
    }

    /**
     * @param lmtOtherAppType
     */
    public void setLmtOtherAppType(String lmtOtherAppType) {
        this.lmtOtherAppType = lmtOtherAppType;
    }

    /**
     * @return lmtOtherAppType
     */
    public String getLmtOtherAppType() {
        return this.lmtOtherAppType;
    }

    /**
     * @param lmtOtherAppSerno
     */
    public void setLmtOtherAppSerno(String lmtOtherAppSerno) {
        this.lmtOtherAppSerno = lmtOtherAppSerno;
    }

    /**
     * @return lmtOtherAppSerno
     */
    public String getLmtOtherAppSerno() {
        return this.lmtOtherAppSerno;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    /**
     * @return oprType
     */
    public String getOprType() {
        return this.oprType;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    /**
     * @return inputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    /**
     * @return inputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    /**
     * @return inputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId;
    }

    /**
     * @return updId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    /**
     * @return updBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    /**
     * @return updDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param createTime
     */
    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    /**
     * @return createTime
     */
    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    /**
     * @param updateTime
     */
    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return updateTime
     */
    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }
}
