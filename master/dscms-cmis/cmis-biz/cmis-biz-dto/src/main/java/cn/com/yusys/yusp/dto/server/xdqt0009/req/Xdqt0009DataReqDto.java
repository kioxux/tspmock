package cn.com.yusys.yusp.dto.server.xdqt0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求dto：微业贷信贷文件处理通知
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdqt0009DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opflag")
    private String opflag;//是否可处理标志 01：可处理 02：不可处理
    @JsonProperty(value = "buzdate")
    private String buzdate;//业务日期 yyyy-MM-dd

    public String getOpflag() {
        return opflag;
    }

    public void setOpflag(String opflag) {
        this.opflag = opflag;
    }

    public String getBuzdate() {
        return buzdate;
    }

    public void setBuzdate(String buzdate) {
        this.buzdate = buzdate;
    }

    @Override
    public String toString() {
        return "Xdqt0009DataReqDto{" +
                "opflag='" + opflag + '\'' +
                ", buzdate='" + buzdate + '\'' +
                '}';
    }

}
