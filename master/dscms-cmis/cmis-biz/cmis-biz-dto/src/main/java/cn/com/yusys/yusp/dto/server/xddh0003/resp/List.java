package cn.com.yusys.yusp.dto.server.xddh0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/22 14:10:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 14:10
 * @since 2021/5/22 14:10
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "billBal")
    private BigDecimal billBal;//借据余额
    @JsonProperty(value = "startDate")
    private String startDate;//发生日期
    @JsonProperty(value = "endDate")
    private String endDate;//到期日期
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构
    @JsonProperty(value = "accountMgr")
    private String accountMgr;//管户人
    @JsonProperty(value = "bizType")
    private String bizType;//业务品种
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批状态

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getAccountMgr() {
        return accountMgr;
    }

    public void setAccountMgr(String accountMgr) {
        this.accountMgr = accountMgr;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    @Override
    public String toString() {
        return "Xddh0003DataRespDto{" +
                "cusName='" + cusName + '\'' +
                ", contNo='" + contNo + '\'' +
                ", curType='" + curType + '\'' +
                ", billBal=" + billBal +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", cusId='" + cusId + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", accountMgr='" + accountMgr + '\'' +
                ", bizType='" + bizType + '\'' +
                ", serno='" + serno + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                '}';
    }
}
