package cn.com.yusys.yusp.dto.server.xdcz0030.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Data：校验额度是否足额，合同是否足额
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0030DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysNo")
    private String sysNo;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "bizNo")
    private String bizNo;//合同编号
    @JsonProperty(value = "dealBizList")
    private java.util.List<DealBizList> dealBizList;
    @JsonProperty(value = "occRelList")
    private java.util.List<OccRelList> occRelList;

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public List<DealBizList> getDealBizList() {
        return dealBizList;
    }

    public void setDealBizList(List<DealBizList> dealBizList) {
        this.dealBizList = dealBizList;
    }

    public List<OccRelList> getOccRelList() {
        return occRelList;
    }

    public void setOccRelList(List<OccRelList> occRelList) {
        this.occRelList = occRelList;
    }

    @Override
    public String toString() {
        return "Xdcz0030DataReqDto{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", serno='" + serno + '\'' +
                ", bizNo='" + bizNo + '\'' +
                ", dealBizList=" + dealBizList +
                ", occRelList=" + occRelList +
                '}';
    }
}
