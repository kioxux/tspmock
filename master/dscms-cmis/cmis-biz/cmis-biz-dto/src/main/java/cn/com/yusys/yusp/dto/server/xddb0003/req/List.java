package cn.com.yusys.yusp.dto.server.xddb0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

/**
 * 请求Dto：押品信息同步
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List {
    @JsonProperty(value = "dyorzy")
    private String dyorzy;//抵质或质押
    @JsonProperty(value = "newcd")
    private String newcd;//新押品编码
    @JsonProperty(value = "newla")
    private String newla;//新押品编码名称
    @JsonProperty(value = "guarid")
    private String guarid;//抵质押物编码
    @JsonProperty(value = "bzsern")
    private String bzsern;//保证人子项流水号
    @JsonProperty(value = "gagtyp")
    private String gagtyp;//抵质押物类型
    @JsonProperty(value = "ganame")
    private String ganame;//抵押物名称
    @JsonProperty(value = "evamt")
    private BigDecimal evamt;//评估价值（元）
    @JsonProperty(value = "evdate")
    private String evdate;//评估日期
    @JsonProperty(value = "arealo")
    private String arealo;//抵押物存放地点
    @JsonProperty(value = "tenci")
    private String tenci;//租赁情况  STD_ZB_TENANCY_CIRCE
    @JsonProperty(value = "ifexho")
    private String ifexho;//是否成品房 STD_ZX_YES_NO
    @JsonProperty(value = "area")
    private String area;//面积
    @JsonProperty(value = "riceno")
    private String riceno;//权属证件号

    public String getDyorzy() {
        return dyorzy;
    }

    public void setDyorzy(String dyorzy) {
        this.dyorzy = dyorzy;
    }

    public String getNewcd() {
        return newcd;
    }

    public void setNewcd(String newcd) {
        this.newcd = newcd;
    }

    public String getNewla() {
        return newla;
    }

    public void setNewla(String newla) {
        this.newla = newla;
    }

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public String getBzsern() {
        return bzsern;
    }

    public void setBzsern(String bzsern) {
        this.bzsern = bzsern;
    }

    public String getGagtyp() {
        return gagtyp;
    }

    public void setGagtyp(String gagtyp) {
        this.gagtyp = gagtyp;
    }

    public String getGaname() {
        return ganame;
    }

    public void setGaname(String ganame) {
        this.ganame = ganame;
    }

    public BigDecimal getEvamt() {
        return evamt;
    }

    public void setEvamt(BigDecimal evamt) {
        this.evamt = evamt;
    }

    public String getEvdate() {
        return evdate;
    }

    public void setEvdate(String evdate) {
        this.evdate = evdate;
    }

    public String getArealo() {
        return arealo;
    }

    public void setArealo(String arealo) {
        this.arealo = arealo;
    }

    public String getTenci() {
        return tenci;
    }

    public void setTenci(String tenci) {
        this.tenci = tenci;
    }

    public String getIfexho() {
        return ifexho;
    }

    public void setIfexho(String ifexho) {
        this.ifexho = ifexho;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRiceno() {
        return riceno;
    }

    public void setRiceno(String riceno) {
        this.riceno = riceno;
    }

    @Override
    public String toString() {
        return "List{" +
                "dyorzy='" + dyorzy + '\'' +
                ", newcd='" + newcd + '\'' +
                ", newla='" + newla + '\'' +
                ", guarid='" + guarid + '\'' +
                ", bzsern='" + bzsern + '\'' +
                ", gagtyp='" + gagtyp + '\'' +
                ", ganame='" + ganame + '\'' +
                ", evamt=" + evamt +
                ", evdate='" + evdate + '\'' +
                ", arealo='" + arealo + '\'' +
                ", tenci='" + tenci + '\'' +
                ", ifexho='" + ifexho + '\'' +
                ", area='" + area + '\'' +
                ", riceno='" + riceno + '\'' +
                '}';
    }
}
