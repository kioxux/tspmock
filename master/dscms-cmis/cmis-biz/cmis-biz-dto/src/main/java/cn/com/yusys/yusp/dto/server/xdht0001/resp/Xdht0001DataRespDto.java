package cn.com.yusys.yusp.dto.server.xdht0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询省心E付合同信息
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0001DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalQnt")
    private String totalQnt;//总数
    @JsonProperty(value = "list")
    private java.util.List<List> list;


    public String getTotalQnt() {
        return totalQnt;
    }

    public void setTotalQnt(String totalQnt) {
        this.totalQnt = totalQnt;
    }


    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }


    @Override
    public String toString() {
        return "Xdht0001RespDto{" +
                "totalQnt='" + totalQnt + '\'' +
                "list='" + list + '\'' +
                '}';
    }
}  
