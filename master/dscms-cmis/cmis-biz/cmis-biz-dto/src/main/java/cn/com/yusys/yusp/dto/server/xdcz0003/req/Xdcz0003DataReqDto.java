package cn.com.yusys.yusp.dto.server.xdcz0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：电子银行承兑汇票出账申请
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//银承协议编号
    @JsonProperty(value = "prcCode")
    private String prcCode;//处理码
    @JsonProperty(value = "seq")
    private String seq;//批次号
    @JsonProperty(value = "appPorderQnt")
    private Integer appPorderQnt;//申请汇票数量
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "drftSignDate")
    private String drftSignDate;//票据签订日期
    @JsonProperty(value = "drftEndDate")
    private String drftEndDate;//票据到期日期
    @JsonProperty(value = "managerId")
    private String managerId;//责任人客户经理号
    @JsonProperty(value = "bailAcct")
    private String bailAcct;//保证金账号
    @JsonProperty(value = "bailRate")
    private BigDecimal bailRate;//保证金比例
    @JsonProperty(value = "bailIntType")
    private String bailIntType;//保证金计息方式
    @JsonProperty(value = "bailAcctName")
    private String bailAcctName;//保证金户名
    @JsonProperty(value = "chrgRate")
    private BigDecimal chrgRate;//手续费比例
    @JsonProperty(value = "chrgAmt")
    private BigDecimal chrgAmt;//手续费金额
    @JsonProperty(value = "drfpoIsseMk")
    private String drfpoIsseMk;//票据池出票标记
    @JsonProperty(value = "drftMedia")
    private String drftMedia;//票据介质
    @JsonProperty(value = "isOtherSign")
    private String isOtherSign;//是否他行代签
    @JsonProperty(value = "isOnlineMk")
    private String isOnlineMk;//是否线上化标记（是否省心E票）
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "imageNo")
    private String imageNo;//影像编号
    @JsonProperty(value = "eContNo")
    private String eContNo;//电子合同编号
    @JsonProperty(value = "pvpImgSerno")
    private String pvpImgSerno;// 银承出账影像流水号
    @JsonProperty(value = "tContNoImgs")
    private String tContNoImgs;//购销合同影像流水


    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getPrcCode() {
        return prcCode;
    }

    public void setPrcCode(String prcCode) {
        this.prcCode = prcCode;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public Integer getAppPorderQnt() {
        return appPorderQnt;
    }

    public void setAppPorderQnt(Integer appPorderQnt) {
        this.appPorderQnt = appPorderQnt;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getDrftSignDate() {
        return drftSignDate;
    }

    public void setDrftSignDate(String drftSignDate) {
        this.drftSignDate = drftSignDate;
    }

    public String getDrftEndDate() {
        return drftEndDate;
    }

    public void setDrftEndDate(String drftEndDate) {
        this.drftEndDate = drftEndDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getBailAcct() {
        return bailAcct;
    }

    public void setBailAcct(String bailAcct) {
        this.bailAcct = bailAcct;
    }

    public BigDecimal getBailRate() {
        return bailRate;
    }

    public void setBailRate(BigDecimal bailRate) {
        this.bailRate = bailRate;
    }

    public String getBailIntType() {
        return bailIntType;
    }

    public void setBailIntType(String bailIntType) {
        this.bailIntType = bailIntType;
    }

    public String getBailAcctName() {
        return bailAcctName;
    }

    public void setBailAcctName(String bailAcctName) {
        this.bailAcctName = bailAcctName;
    }

    public BigDecimal getChrgRate() {
        return chrgRate;
    }

    public void setChrgRate(BigDecimal chrgRate) {
        this.chrgRate = chrgRate;
    }

    public BigDecimal getChrgAmt() {
        return chrgAmt;
    }

    public void setChrgAmt(BigDecimal chrgAmt) {
        this.chrgAmt = chrgAmt;
    }

    public String getDrfpoIsseMk() {
        return drfpoIsseMk;
    }

    public void setDrfpoIsseMk(String drfpoIsseMk) {
        this.drfpoIsseMk = drfpoIsseMk;
    }

    public String getDrftMedia() {
        return drftMedia;
    }

    public void setDrftMedia(String drftMedia) {
        this.drftMedia = drftMedia;
    }

    public String getIsOtherSign() {
        return isOtherSign;
    }

    public void setIsOtherSign(String isOtherSign) {
        this.isOtherSign = isOtherSign;
    }

    public String getIsOnlineMk() {
        return isOnlineMk;
    }

    public void setIsOnlineMk(String isOnlineMk) {
        this.isOnlineMk = isOnlineMk;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public String geteContNo() {
        return eContNo;
    }

    public void seteContNo(String eContNo) {
        this.eContNo = eContNo;
    }

    public String getPvpImgSerno() {
        return pvpImgSerno;
    }

    public void setPvpImgSerno(String pvpImgSerno) {
        this.pvpImgSerno = pvpImgSerno;
    }

    public String gettContNoImgs() {
        return tContNoImgs;
    }

    public void settContNoImgs(String tContNoImgs) {
        this.tContNoImgs = tContNoImgs;
    }

    @Override
    public String toString() {
        return "Xdcz0003DataReqDto{" +
                "contNo='" + contNo + '\'' +
                ", prcCode='" + prcCode + '\'' +
                ", seq='" + seq + '\'' +
                ", appPorderQnt=" + appPorderQnt +
                ", applyAmt=" + applyAmt +
                ", drftSignDate='" + drftSignDate + '\'' +
                ", drftEndDate='" + drftEndDate + '\'' +
                ", managerId='" + managerId + '\'' +
                ", bailAcct='" + bailAcct + '\'' +
                ", bailRate=" + bailRate +
                ", bailIntType='" + bailIntType + '\'' +
                ", bailAcctName='" + bailAcctName + '\'' +
                ", chrgRate=" + chrgRate +
                ", chrgAmt=" + chrgAmt +
                ", drfpoIsseMk='" + drfpoIsseMk + '\'' +
                ", drftMedia='" + drftMedia + '\'' +
                ", isOtherSign='" + isOtherSign + '\'' +
                ", isOnlineMk='" + isOnlineMk + '\'' +
                ", cusId='" + cusId + '\'' +
                ", imageNo='" + imageNo + '\'' +
                ", eContNo='" + eContNo + '\'' +
                ", pvpImgSerno='" + pvpImgSerno + '\'' +
                ", tContNoImgs='" + tContNoImgs + '\'' +
                '}';
    }
}
