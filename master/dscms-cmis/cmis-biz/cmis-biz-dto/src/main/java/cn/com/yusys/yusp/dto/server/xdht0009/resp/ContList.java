package cn.com.yusys.yusp.dto.server.xdht0009.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/5/6 13:56
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ContList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同到期日
    @JsonProperty(value = "bailRate")
    private BigDecimal bailRate;//保证金比例
    @JsonProperty(value = "chrgRate")
    private BigDecimal chrgRate;//手续费比例
    @JsonProperty(value = "drwrAcctNo")
    private String drwrAcctNo;//出票人账号
    @JsonProperty(value = "drwrAcctName")
    private String drwrAcctName;//出票人户名
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称
    @JsonProperty(value = "bailAcctNo")
    private String bailAcctNo;//保证金账号
    @JsonProperty(value = "bailIntMode")
    private String bailIntMode;//保证金计息方式

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public BigDecimal getBailRate() {
        return bailRate;
    }

    public void setBailRate(BigDecimal bailRate) {
        this.bailRate = bailRate;
    }

    public BigDecimal getChrgRate() {
        return chrgRate;
    }

    public void setChrgRate(BigDecimal chrgRate) {
        this.chrgRate = chrgRate;
    }

    public String getDrwrAcctNo() {
        return drwrAcctNo;
    }

    public void setDrwrAcctNo(String drwrAcctNo) {
        this.drwrAcctNo = drwrAcctNo;
    }

    public String getDrwrAcctName() {
        return drwrAcctName;
    }

    public void setDrwrAcctName(String drwrAcctName) {
        this.drwrAcctName = drwrAcctName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getBailAcctNo() {
        return bailAcctNo;
    }

    public void setBailAcctNo(String bailAcctNo) {
        this.bailAcctNo = bailAcctNo;
    }

    public String getBailIntMode() {
        return bailIntMode;
    }

    public void setBailIntMode(String bailIntMode) {
        this.bailIntMode = bailIntMode;
    }

    @Override
    public String toString() {
        return "ContList{" +
                "contNo='" + contNo + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", contType='" + contType + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", contAmt=" + contAmt +
                ", contStartDate='" + contStartDate + '\'' +
                ", contEndDate='" + contEndDate + '\'' +
                ", bailRate=" + bailRate +
                ", chrgRate=" + chrgRate +
                ", drwrAcctNo='" + drwrAcctNo + '\'' +
                ", drwrAcctName='" + drwrAcctName + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", bailAcctNo='" + bailAcctNo + '\'' +
                ", bailIntMode='" + bailIntMode + '\'' +
                '}';
    }
}
