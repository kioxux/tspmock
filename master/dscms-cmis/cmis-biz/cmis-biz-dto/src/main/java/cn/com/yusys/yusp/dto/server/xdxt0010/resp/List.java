package cn.com.yusys.yusp.dto.server.xdxt0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 17:27:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 17:27
 * @since 2021/5/24 17:27
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "orgNo")
    private String orgNo;//机构号
    @JsonProperty(value = "orgName")
    private String orgName;//机构名称
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理姓名

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    @Override
    public String toString() {
        return "List{" +
                "orgNo='" + orgNo + '\'' +
                ", orgName='" + orgName + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerName='" + managerName + '\'' +
                '}';
    }
}
