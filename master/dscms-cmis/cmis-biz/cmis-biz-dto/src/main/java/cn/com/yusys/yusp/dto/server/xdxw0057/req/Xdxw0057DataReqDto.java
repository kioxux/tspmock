package cn.com.yusys.yusp.dto.server.xdxw0057.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据核心客户号查询经营性贷款批复额度
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0057DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户号


    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdxw0057DataReqDto{" +
                "queryType='" + queryType + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
