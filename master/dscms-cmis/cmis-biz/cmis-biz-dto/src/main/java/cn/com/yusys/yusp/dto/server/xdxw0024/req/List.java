package cn.com.yusys.yusp.dto.server.xdxw0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询是否有信贷记录
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    @JsonProperty(value = "certNo")
    private String certNo;//证件号

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "List{" +
                "certNo='" + certNo + '\'' +
                '}';
    }
}
