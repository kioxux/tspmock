package cn.com.yusys.yusp.dto.server.xdls0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：信贷授信协议合同金额查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdls0003DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "crdLmtTotal")
    private String crdLmtTotal;//授信协议合同金额合计

    public String getCrdLmtTotal() {
        return crdLmtTotal;
    }

    public void setCrdLmtTotal(String crdLmtTotal) {
        this.crdLmtTotal = crdLmtTotal;
    }

	@Override
	public String toString() {
		return "Xdls0003DataRespDto{" +
				"crdLmtTotal='" + crdLmtTotal + '\'' +
				'}';
	}
}
