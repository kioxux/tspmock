package cn.com.yusys.yusp.dto.server.xdht0020.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：额度查看
 *
 * @author Qianxin
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0020LmtDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "lmt")
    private String lmt;//额度
    @JsonProperty(value = "approvalEndDate")
    private String approvalEndDate;
    @JsonProperty(value = "repayMode")
    private String repayMode;

    public String getLmt() {
        return lmt;
    }

    public void setLmt(String lmt) {
        this.lmt = lmt;
    }

    public String getApprovalEndDate() {
        return approvalEndDate;
    }

    public void setApprovalEndDate(String approvalEndDate) {
        this.approvalEndDate = approvalEndDate;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    @Override
    public String toString() {
        return "Xdht0020LmtDto{" +
                "lmt='" + lmt + '\'' +
                ", approvalEndDate='" + approvalEndDate + '\'' +
                ", repayMode='" + repayMode + '\'' +
                '}';
    }

}
