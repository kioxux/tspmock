package cn.com.yusys.yusp.dto.server.xdxw0027.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：客户调查信息详情查看
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0027DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "indgtSerno")
	private String indgtSerno;//客户调查主键
	@JsonProperty(value = "loanType")
	private String loanType;//贷款类型
	@JsonProperty(value = "mainManagerId")
	private String mainManagerId;//主办客户经理号
	@JsonProperty(value = "subManagerId")
	private String subManagerId;//协办客户经理
	@JsonProperty(value = "checkDate")
	private String checkDate;//检查日期
	@JsonProperty(value = "approveStatus")
	private String approveStatus;//审批状态
	@JsonProperty(value = "bizSour")
	private String bizSour;//业务来源
	@JsonProperty(value = "adviceMan")
	private String adviceMan;//推荐人
	@JsonProperty(value = "bizType")
	private String bizType;//业务类型
	@JsonProperty(value = "copyFrom")
	private String copyFrom;//从哪复制
	@JsonProperty(value = "lastApprTime")
	private String lastApprTime;//最后一次审批时间
	@JsonProperty(value = "payType")
	private String payType;//支付方式
	@JsonProperty(value = "contType")
	private String contType;//合同类型
	@JsonProperty(value = "curtLmtAmt")
	private BigDecimal curtLmtAmt;//本次授信金额
	@JsonProperty(value = "curtLoanAmt")
	private BigDecimal curtLoanAmt;//本次用信金额
	@JsonProperty(value = "isBorrowReturn")
	private String isBorrowReturn;//是否随借随还
	@JsonProperty(value = "cancelFlag")
	private String cancelFlag;//撤销标志
	@JsonProperty(value = "xfOrgId")
	private String xfOrgId;//取的s_user表中的xf_orgid
	@JsonProperty(value = "returnCapY")
	private BigDecimal returnCapY;//每年归还本金（元）
	@JsonProperty(value = "isNewEmployee")
	private String isNewEmployee;//是否新员工
	@JsonProperty(value = "newEmployeeName")
	private String newEmployeeName;//新员工名称
	@JsonProperty(value = "newEmployeePhone")
	private String newEmployeePhone;//新员工电话
	@JsonProperty(value = "apprType")
	private String apprType;//审批类型
	@JsonProperty(value = "isSubManagerId")
	private String isSubManagerId;//是否存在协办客户经理
	@JsonProperty(value = "cusType")
	private String cusType;//客户类型

	public String getIndgtSerno() {
		return indgtSerno;
	}

	public void setIndgtSerno(String indgtSerno) {
		this.indgtSerno = indgtSerno;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public String getMainManagerId() {
		return mainManagerId;
	}

	public void setMainManagerId(String mainManagerId) {
		this.mainManagerId = mainManagerId;
	}

	public String getSubManagerId() {
		return subManagerId;
	}

	public void setSubManagerId(String subManagerId) {
		this.subManagerId = subManagerId;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getBizSour() {
		return bizSour;
	}

	public void setBizSour(String bizSour) {
		this.bizSour = bizSour;
	}

	public String getAdviceMan() {
		return adviceMan;
	}

	public void setAdviceMan(String adviceMan) {
		this.adviceMan = adviceMan;
	}

	public String getBizType() {
		return bizType;
	}

	public void setBizType(String bizType) {
		this.bizType = bizType;
	}

	public String getCopyFrom() {
		return copyFrom;
	}

	public void setCopyFrom(String copyFrom) {
		this.copyFrom = copyFrom;
	}

	public String getLastApprTime() {
		return lastApprTime;
	}

	public void setLastApprTime(String lastApprTime) {
		this.lastApprTime = lastApprTime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getContType() {
		return contType;
	}

	public void setContType(String contType) {
		this.contType = contType;
	}

	public BigDecimal getCurtLmtAmt() {
		return curtLmtAmt;
	}

	public void setCurtLmtAmt(BigDecimal curtLmtAmt) {
		this.curtLmtAmt = curtLmtAmt;
	}

	public BigDecimal getCurtLoanAmt() {
		return curtLoanAmt;
	}

	public void setCurtLoanAmt(BigDecimal curtLoanAmt) {
		this.curtLoanAmt = curtLoanAmt;
	}

	public String getIsBorrowReturn() {
		return isBorrowReturn;
	}

	public void setIsBorrowReturn(String isBorrowReturn) {
		this.isBorrowReturn = isBorrowReturn;
	}

	public String getCancelFlag() {
		return cancelFlag;
	}

	public void setCancelFlag(String cancelFlag) {
		this.cancelFlag = cancelFlag;
	}

	public String getXfOrgId() {
		return xfOrgId;
	}

	public void setXfOrgId(String xfOrgId) {
		this.xfOrgId = xfOrgId;
	}

	public BigDecimal getReturnCapY() {
		return returnCapY;
	}

	public void setReturnCapY(BigDecimal returnCapY) {
		this.returnCapY = returnCapY;
	}

	public String getIsNewEmployee() {
		return isNewEmployee;
	}

	public void setIsNewEmployee(String isNewEmployee) {
		this.isNewEmployee = isNewEmployee;
	}

	public String getNewEmployeeName() {
		return newEmployeeName;
	}

	public void setNewEmployeeName(String newEmployeeName) {
		this.newEmployeeName = newEmployeeName;
	}

	public String getNewEmployeePhone() {
		return newEmployeePhone;
	}

	public void setNewEmployeePhone(String newEmployeePhone) {
		this.newEmployeePhone = newEmployeePhone;
	}

	public String getApprType() {
		return apprType;
	}

	public void setApprType(String apprType) {
		this.apprType = apprType;
	}

	public String getIsSubManagerId() {
		return isSubManagerId;
	}

	public void setIsSubManagerId(String isSubManagerId) {
		this.isSubManagerId = isSubManagerId;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	@Override
	public String toString() {
		return "Xdxw0027DataRespDto{" +
				"indgtSerno='" + indgtSerno + '\'' +
				", loanType='" + loanType + '\'' +
				", mainManagerId='" + mainManagerId + '\'' +
				", subManagerId='" + subManagerId + '\'' +
				", checkDate='" + checkDate + '\'' +
				", approveStatus='" + approveStatus + '\'' +
				", bizSour='" + bizSour + '\'' +
				", adviceMan='" + adviceMan + '\'' +
				", bizType='" + bizType + '\'' +
				", copyFrom='" + copyFrom + '\'' +
				", lastApprTime='" + lastApprTime + '\'' +
				", payType='" + payType + '\'' +
				", contType='" + contType + '\'' +
				", curtLmtAmt=" + curtLmtAmt +
				", curtLoanAmt=" + curtLoanAmt +
				", isBorrowReturn='" + isBorrowReturn + '\'' +
				", cancelFlag='" + cancelFlag + '\'' +
				", xfOrgId='" + xfOrgId + '\'' +
				", returnCapY=" + returnCapY +
				", isNewEmployee='" + isNewEmployee + '\'' +
				", newEmployeeName='" + newEmployeeName + '\'' +
				", newEmployeePhone='" + newEmployeePhone + '\'' +
				", apprType='" + apprType + '\'' +
				", isSubManagerId='" + isSubManagerId + '\'' +
				", cusType='" + cusType + '\'' +
				'}';
	}
}
