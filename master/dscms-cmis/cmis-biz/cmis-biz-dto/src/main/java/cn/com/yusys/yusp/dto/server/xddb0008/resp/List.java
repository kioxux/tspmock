package cn.com.yusys.yusp.dto.server.xddb0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询在线抵押信息
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pldManCusId")
    private String pldManCusId;//抵押人客户码
    @JsonProperty(value = "pldManName")
    private String pldManName;//抵押人名称
    @JsonProperty(value = "pldManCertNo")
    private String pldManCertNo;//抵押人证件号码
    @JsonProperty(value = "guarNo")
    private String guarNo;//抵押物编号
    @JsonProperty(value = "guarName")
    private String guarName;//抵押物名称
    @JsonProperty(value = "guarPlace")
    private String guarPlace;//抵押物位置
    @JsonProperty(value = "guarValue")
    private BigDecimal guarValue;//抵押物价值


    public String getPldManCusId() {
        return pldManCusId;
    }

    public void setPldManCusId(String pldManCusId) {
        this.pldManCusId = pldManCusId;
    }

    public String getPldManName() {
        return pldManName;
    }

    public void setPldManName(String pldManName) {
        this.pldManName = pldManName;
    }

    public String getPldManCertNo() {
        return pldManCertNo;
    }

    public void setPldManCertNo(String pldManCertNo) {
        this.pldManCertNo = pldManCertNo;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getGuarName() {
        return guarName;
    }

    public void setGuarName(String guarName) {
        this.guarName = guarName;
    }

    public String getGuarPlace() {
        return guarPlace;
    }

    public void setGuarPlace(String guarPlace) {
        this.guarPlace = guarPlace;
    }

    public BigDecimal getGuarValue() {
        return guarValue;
    }

    public void setGuarValue(BigDecimal guarValue) {
        this.guarValue = guarValue;
    }

    @Override
    public String toString() {
        return "List{" +
                "pldManCusId='" + pldManCusId + '\'' +
                ", pldManName='" + pldManName + '\'' +
                ", pldManCertNo='" + pldManCertNo + '\'' +
                ", guarNo='" + guarNo + '\'' +
                ", guarName='" + guarName + '\'' +
                ", guarPlace='" + guarPlace + '\'' +
                ", guarValue=" + guarValue +
                '}';
    }
}
