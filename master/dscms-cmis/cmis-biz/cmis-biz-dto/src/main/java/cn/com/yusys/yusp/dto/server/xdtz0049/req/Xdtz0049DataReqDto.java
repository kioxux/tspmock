package cn.com.yusys.yusp.dto.server.xdtz0049.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：客户贷款信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0049DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "certNo")
    private String certNo;//证件号

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xdtz0049DataReqDto{" +
                "certNo='" + certNo + '\'' +
                '}';
    }
}
