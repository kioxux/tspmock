package cn.com.yusys.yusp.dto.server.xdht0033.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0033DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applyAmt")
    private String applyAmt;//申请金额

    public String getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(String applyAmt) {
        this.applyAmt = applyAmt;
    }

    @Override
    public String toString() {
        return "Xdht0033DataRespDto{" +
                "applyAmt='" + applyAmt + '\'' +
                '}';
    }
}