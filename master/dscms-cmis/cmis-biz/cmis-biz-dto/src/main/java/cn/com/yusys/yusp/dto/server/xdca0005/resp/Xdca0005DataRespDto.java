package cn.com.yusys.yusp.dto.server.xdca0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：大额分期合同签订接口
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdca0005DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "contSignDate")
    private String contSignDate;//合同签订日期

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getContSignDate() {
        return contSignDate;
    }

    public void setContSignDate(String contSignDate) {
        this.contSignDate = contSignDate;
    }

    @Override
    public String toString() {
        return "Xdca0005DataRespDto{" +
                "contNo='" + contNo + '\'' +
                "contStatus='" + contStatus + '\'' +
                "contSignDate='" + contSignDate + '\'' +
                '}';
    }
}
