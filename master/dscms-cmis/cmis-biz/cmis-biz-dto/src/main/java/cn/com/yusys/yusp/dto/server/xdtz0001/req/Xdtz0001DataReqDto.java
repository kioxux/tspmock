package cn.com.yusys.yusp.dto.server.xdtz0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：客户信息查询(贷款信息)
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0001DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户代码

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdtz0001DataReqDto{" +
                "queryType='" + queryType + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}