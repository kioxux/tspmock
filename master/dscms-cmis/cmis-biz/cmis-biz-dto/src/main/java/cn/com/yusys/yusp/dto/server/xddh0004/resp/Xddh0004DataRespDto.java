package cn.com.yusys.yusp.dto.server.xddh0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询还款是否在途
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0004DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "onTheWayAmt")
    private BigDecimal onTheWayAmt;//在途金额
    @JsonProperty(value = "isOnTheWay")
    private String isOnTheWay;//是否在途
    @JsonProperty(value = "onTheWayContNo")
    private String onTheWayContNo;//在途合同号

    public BigDecimal getOnTheWayAmt() {
        return onTheWayAmt;
    }

    public void setOnTheWayAmt(BigDecimal onTheWayAmt) {
        this.onTheWayAmt = onTheWayAmt;
    }

    public String getIsOnTheWay() {
        return isOnTheWay;
    }

    public void setIsOnTheWay(String isOnTheWay) {
        this.isOnTheWay = isOnTheWay;
    }

    public String getOnTheWayContNo() {
        return onTheWayContNo;
    }

    public void setOnTheWayContNo(String onTheWayContNo) {
        this.onTheWayContNo = onTheWayContNo;
    }

    @Override
    public String toString() {
        return "Xddh0004DataRespDto{" +
                "onTheWayAmt=" + onTheWayAmt +
                ", isOnTheWay='" + isOnTheWay + '\'' +
                ", onTheWayContNo='" + onTheWayContNo + '\'' +
                '}';
    }
}
