package cn.com.yusys.yusp.dto.server.xddh0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询贷后任务是否完成现场检查
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0001DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//任务编号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    @Override
    public String toString() {
        return "Xddh0001DataRespDto{" +
                "serno='" + serno + '\'' +
                '}';
    }
}
