package cn.com.yusys.yusp.dto.server.xdxw0066.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：调查基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//主键流水号
    @JsonProperty(value = "cus_name")
    private String cus_name;//申请人姓名
    @JsonProperty(value = "cert_code")
    private String cert_code;//身份证号码
    @JsonProperty(value = "marry_status")
    private String marry_status;//婚姻状况STD_XD_MAR
    @JsonProperty(value = "house_prop")
    private String house_prop;//居住场所类型 XD_LIVECS_TYPE
    @JsonProperty(value = "addr")
    private String addr;//家庭地址
    @JsonProperty(value = "live_year")
    private String live_year;//本地居住年限
    @JsonProperty(value = "local_resident")
    private String local_resident;//本地户口 XD_LOCAL_RESIDENT
    @JsonProperty(value = "tel")
    private String tel;//手机
    @JsonProperty(value = "spouse_name")
    private String spouse_name;//配偶姓名
    @JsonProperty(value = "spouse_cert_code")
    private String spouse_cert_code;//配偶身份证
    @JsonProperty(value = "spouse_work_type")
    private String spouse_work_type;//配偶工作情况 XD_WORK_TYPE
    @JsonProperty(value = "spouse_tel")
    private String spouse_tel;//配偶电话
    @JsonProperty(value = "per_zx")
    private String per_zx;//央行个人征信系统（含配偶）XD_RE_GEL
    @JsonProperty(value = "per_remark")
    private String per_remark;//备注（个人）
    @JsonProperty(value = "com_zx")
    private String com_zx;//央行企业征信系统XD_RE_GEL
    @JsonProperty(value = "com_remark")
    private String com_remark;//备注（企业）
    @JsonProperty(value = "main_business")
    private String main_business;//主营业务
    @JsonProperty(value = "years_operations")
    private String years_operations;//经营年限
    @JsonProperty(value = "business_addres")
    private String business_addres;//经营地址
    @JsonProperty(value = "education")
    private String education;//学历 STD_ZX_EDU
    @JsonProperty(value = "sex")
    private String sex;//性别 STD_ZX_SEX
    @JsonProperty(value = "cus_list_serno")
    private String cus_list_serno;//名单表流水号
    @JsonProperty(value = "app_amt")
    private BigDecimal app_amt;//申请金额
    @JsonProperty(value = "app_limit")
    private String app_limit;//申请期限
    @JsonProperty(value = "loan_reason")
    private String loan_reason;//贷款原因
    @JsonProperty(value = "loan_used")
    private String loan_used;//贷款用途


    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getMarry_status() {
        return marry_status;
    }

    public void setMarry_status(String marry_status) {
        this.marry_status = marry_status;
    }

    public String getHouse_prop() {
        return house_prop;
    }

    public void setHouse_prop(String house_prop) {
        this.house_prop = house_prop;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getLive_year() {
        return live_year;
    }

    public void setLive_year(String live_year) {
        this.live_year = live_year;
    }

    public String getLocal_resident() {
        return local_resident;
    }

    public void setLocal_resident(String local_resident) {
        this.local_resident = local_resident;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_cert_code() {
        return spouse_cert_code;
    }

    public void setSpouse_cert_code(String spouse_cert_code) {
        this.spouse_cert_code = spouse_cert_code;
    }

    public String getSpouse_work_type() {
        return spouse_work_type;
    }

    public void setSpouse_work_type(String spouse_work_type) {
        this.spouse_work_type = spouse_work_type;
    }

    public String getSpouse_tel() {
        return spouse_tel;
    }

    public void setSpouse_tel(String spouse_tel) {
        this.spouse_tel = spouse_tel;
    }

    public String getPer_zx() {
        return per_zx;
    }

    public void setPer_zx(String per_zx) {
        this.per_zx = per_zx;
    }

    public String getPer_remark() {
        return per_remark;
    }

    public void setPer_remark(String per_remark) {
        this.per_remark = per_remark;
    }

    public String getCom_zx() {
        return com_zx;
    }

    public void setCom_zx(String com_zx) {
        this.com_zx = com_zx;
    }

    public String getCom_remark() {
        return com_remark;
    }

    public void setCom_remark(String com_remark) {
        this.com_remark = com_remark;
    }

    public String getMain_business() {
        return main_business;
    }

    public void setMain_business(String main_business) {
        this.main_business = main_business;
    }

    public String getYears_operations() {
        return years_operations;
    }

    public void setYears_operations(String years_operations) {
        this.years_operations = years_operations;
    }

    public String getBusiness_addres() {
        return business_addres;
    }

    public void setBusiness_addres(String business_addres) {
        this.business_addres = business_addres;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCus_list_serno() {
        return cus_list_serno;
    }

    public void setCus_list_serno(String cus_list_serno) {
        this.cus_list_serno = cus_list_serno;
    }

    public BigDecimal getApp_amt() {
        return app_amt;
    }

    public void setApp_amt(BigDecimal app_amt) {
        this.app_amt = app_amt;
    }

    public String getApp_limit() {
        return app_limit;
    }

    public void setApp_limit(String app_limit) {
        this.app_limit = app_limit;
    }

    public String getLoan_reason() {
        return loan_reason;
    }

    public void setLoan_reason(String loan_reason) {
        this.loan_reason = loan_reason;
    }

    public String getLoan_used() {
        return loan_used;
    }

    public void setLoan_used(String loan_used) {
        this.loan_used = loan_used;
    }

    @Override
    public String toString() {
        return "List{" +
                "survey_serno='" + survey_serno + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", marry_status='" + marry_status + '\'' +
                ", house_prop='" + house_prop + '\'' +
                ", addr='" + addr + '\'' +
                ", live_year='" + live_year + '\'' +
                ", local_resident='" + local_resident + '\'' +
                ", tel='" + tel + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", spouse_cert_code='" + spouse_cert_code + '\'' +
                ", spouse_work_type='" + spouse_work_type + '\'' +
                ", spouse_tel='" + spouse_tel + '\'' +
                ", per_zx='" + per_zx + '\'' +
                ", per_remark='" + per_remark + '\'' +
                ", com_zx='" + com_zx + '\'' +
                ", com_remark='" + com_remark + '\'' +
                ", main_business='" + main_business + '\'' +
                ", years_operations='" + years_operations + '\'' +
                ", business_addres='" + business_addres + '\'' +
                ", education='" + education + '\'' +
                ", sex='" + sex + '\'' +
                ", cus_list_serno='" + cus_list_serno + '\'' +
                ", app_amt=" + app_amt +
                ", app_limit='" + app_limit + '\'' +
                ", loan_reason='" + loan_reason + '\'' +
                ", loan_used='" + loan_used + '\'' +
                '}';
    }
}
