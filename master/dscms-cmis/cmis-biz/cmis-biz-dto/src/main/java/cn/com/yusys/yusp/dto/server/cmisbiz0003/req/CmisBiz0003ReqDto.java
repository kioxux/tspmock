package cn.com.yusys.yusp.dto.server.cmisbiz0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class CmisBiz0003ReqDto implements Serializable {
    private static final long serialVersionUID = -1667942819080971898L;

    @JsonProperty(value = "orgIdList", required = true)
    private List<String> orgIdList;

    @JsonProperty(value = "belgLine", required = true)
    private String belgLine;

    /**
     * selectType==1  业务条线
     */
    @JsonProperty(value = "selectType")
    private Integer selectType;

    public List<String> getOrgIdList() {
        return orgIdList;
    }

    public void setOrgIdList(List<String> orgIdList) {
        this.orgIdList = orgIdList;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public Integer getSelectType() {
        return selectType;
    }

    public void setSelectType(Integer selectType) {
        this.selectType = selectType;
    }

    @Override
    public String toString() {
        return "CmisBiz0003ReqDto{" +
                "orgIdList=" + orgIdList +
                ", belgLine='" + belgLine + '\'' +
                ", selectType=" + selectType +
                '}';
    }
}
