package cn.com.yusys.yusp.dto.server.xdtz0035.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0035DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHavingOverdueRecord24")
    private String isHavingOverdueRecord24;//征信业务流水号

    public String getIsHavingOverdueRecord24() {
        return isHavingOverdueRecord24;
    }

    public void setIsHavingOverdueRecord24(String isHavingOverdueRecord24) {
        this.isHavingOverdueRecord24 = isHavingOverdueRecord24;
    }

    @Override
    public String toString() {
        return "Xdtz0035DataRespDto{" +
                "isHavingOverdueRecord24='" + isHavingOverdueRecord24 + '\'' +
                '}';
    }
}
