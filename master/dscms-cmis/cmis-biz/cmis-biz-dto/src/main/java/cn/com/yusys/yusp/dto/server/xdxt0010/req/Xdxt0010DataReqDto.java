package cn.com.yusys.yusp.dto.server.xdxt0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询客户经理名单，包括工号、姓名
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0010DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "deptChiefId")
    private String deptChiefId;//分中心负责人工号
    @JsonProperty(value = "startPageNum")
    private Integer startPageNum;//开始页数
    @JsonProperty(value = "pageSize")
    private Integer pageSize;//条数

    public String getDeptChiefId() {
        return deptChiefId;
    }

    public void setDeptChiefId(String deptChiefId) {
        this.deptChiefId = deptChiefId;
    }

    public Integer getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(Integer startPageNum) {
        this.startPageNum = startPageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Xdxt0010DataReqDto{" +
                "deptChiefId='" + deptChiefId + '\'' +
                ", startPageNum=" + startPageNum +
                ", pageSize=" + pageSize +
                '}';
    }
}
