package cn.com.yusys.yusp.dto.server.xdxt0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据工号获取所辖区域
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0004DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "areaList")
    private java.util.List<AreaList> areaList;

    public java.util.List<AreaList> getAreaList() {
        return areaList;
    }

    public void setAreaList(java.util.List<AreaList> areaList) {
        this.areaList = areaList;
    }

    @Override
    public String toString() {
        return "Xdxt0004DataRespDto{" +
                "areaList=" + areaList +
                '}';
    }
}
