package cn.com.yusys.yusp.dto.server.xdxw0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0015DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "listApply")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListApply> listApply;
    @JsonProperty(value = "listCont")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListCont> listCont;
    @JsonProperty(value = "listLmt")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListLmt> listLmt;
    @JsonProperty(value = "listLoan")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListLoan> listLoan;

    public List<ListApply> getListApply() {
        return listApply;
    }

    public void setListApply(List<ListApply> listApply) {
        this.listApply = listApply;
    }

    public List<ListCont> getListCont() {
        return listCont;
    }

    public void setListCont(List<ListCont> listCont) {
        this.listCont = listCont;
    }

    public List<ListLmt> getListLmt() {
        return listLmt;
    }

    public void setListLmt(List<ListLmt> listLmt) {
        this.listLmt = listLmt;
    }

    public List<ListLoan> getListLoan() {
        return listLoan;
    }

    public void setListLoan(List<ListLoan> listLoan) {
        this.listLoan = listLoan;
    }

    @Override
    public String toString() {
        return "Xdxw0015DataRespDto{" +
                "listApply=" + listApply +
                ", listCont=" + listCont +
                ", listLmt=" + listLmt +
                ", listLoan=" + listLoan +
                '}';
    }
}
