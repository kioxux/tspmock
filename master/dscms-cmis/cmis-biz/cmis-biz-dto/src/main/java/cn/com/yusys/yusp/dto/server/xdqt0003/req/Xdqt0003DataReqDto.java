package cn.com.yusys.yusp.dto.server.xdqt0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：企业网银推送预约信息
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdqt0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "unifiedCreditCode")
    private String unifiedCreditCode;//统一社会信用代码
    @JsonProperty(value = "cusLinkman")
    private String cusLinkman;//客户联系人
    @JsonProperty(value = "phone")
    private String phone;//联系电话
    @JsonProperty(value = "appointTime")
    private String appointTime;//预约时间
    @JsonProperty(value = "appointType")
    private String appointType;//预约类型


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getUnifiedCreditCode() {
        return unifiedCreditCode;
    }

    public void setUnifiedCreditCode(String unifiedCreditCode) {
        this.unifiedCreditCode = unifiedCreditCode;
    }

    public String getCusLinkman() {
        return cusLinkman;
    }

    public void setCusLinkman(String cusLinkman) {
        this.cusLinkman = cusLinkman;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAppointTime() {
        return appointTime;
    }

    public void setAppointTime(String appointTime) {
        this.appointTime = appointTime;
    }

    public String getAppointType() {
        return appointType;
    }

    public void setAppointType(String appointType) {
        this.appointType = appointType;
    }

    @Override
    public String toString() {
        return "Xdqt0003DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", unifiedCreditCode='" + unifiedCreditCode + '\'' +
                ", cusLinkman='" + cusLinkman + '\'' +
                ", phone='" + phone + '\'' +
                ", appointTime='" + appointTime + '\'' +
                ", appointType='" + appointType + '\'' +
                '}';
    }
}
