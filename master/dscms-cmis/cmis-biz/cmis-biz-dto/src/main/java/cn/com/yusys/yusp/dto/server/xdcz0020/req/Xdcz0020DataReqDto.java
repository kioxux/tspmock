package cn.com.yusys.yusp.dto.server.xdcz0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：小贷用途承诺书文本生成pdf
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0020DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户
    @JsonProperty(value = "certCode")
    private String certCode;//客户编号
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//借款用途
    @JsonProperty(value = "applyDate")
    private String applyDate;//申请日期
    @JsonProperty(value = "dkType")
    private String dkType;//产品类型

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getDkType() {
        return dkType;
    }

    public void setDkType(String dkType) {
        this.dkType = dkType;
    }

    @Override
    public String toString() {
        return "Xdcz0020ReqDto{" +
                "cusName='" + cusName + '\'' +
                "certCode='" + certCode + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "applyDate='" + applyDate + '\'' +
                "dkType='" + dkType + '\'' +
                '}';
    }
}
