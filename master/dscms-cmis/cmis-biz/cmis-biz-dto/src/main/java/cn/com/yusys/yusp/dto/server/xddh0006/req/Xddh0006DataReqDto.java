package cn.com.yusys.yusp.dto.server.xddh0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：提前还款试算查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0006DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "repayType")
    private String repayType;//还款模式
    @JsonProperty(value = "advRepaymentAmt")
    private BigDecimal advRepaymentAmt;//提前还本金额

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public BigDecimal getAdvRepaymentAmt() {
        return advRepaymentAmt;
    }

    public void setAdvRepaymentAmt(BigDecimal advRepaymentAmt) {
        this.advRepaymentAmt = advRepaymentAmt;
    }

    @Override
    public String toString() {
        return "Xddh0006DataReqDto{" +
                "billNo='" + billNo + '\'' +
                ", repayType='" + repayType + '\'' +
                ", advRepaymentAmt=" + advRepaymentAmt +
                '}';
    }
}
