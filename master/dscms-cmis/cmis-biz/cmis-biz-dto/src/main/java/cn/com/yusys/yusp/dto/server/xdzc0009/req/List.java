package cn.com.yusys.yusp.dto.server.xdzc0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author xs
 * @version 0.1
 * @date 2021/9/17 15:12
 * @since 2021/9/17 15:12
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "aorgNo")
    private String aorgNo;//承兑行行号（总行）
    @JsonProperty(value = "aorgName")
    private String aorgName;//承兑行名称
    @JsonProperty(value = "creditLevel")
    private String creditLevel;//信用等级（承兑人分类）
    @JsonProperty(value = "status")
    private String status;//状态（是否启用）
    @JsonProperty(value = "inputId")
    private String inputId;//操作人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//操作人机构

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getAorgName() {
        return aorgName;
    }

    public void setAorgName(String aorgName) {
        this.aorgName = aorgName;
    }

    public String getCreditLevel() {
        return creditLevel;
    }

    public void setCreditLevel(String creditLevel) {
        this.creditLevel = creditLevel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    @Override
    public String toString() {
        return "List{" +
                "aorgNo='" + aorgNo + '\'' +
                ", aorgName='" + aorgName + '\'' +
                ", creditLevel='" + creditLevel + '\'' +
                ", status='" + status + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                '}';
    }
}
