package cn.com.yusys.yusp.dto;

import java.util.Map;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/7/19 19:27
 * @desc cfca签章参数dto
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class CfcaArgsDto {
    // 文件名称
    private String pdfFileName;
    // 用户
    private String managerId;
    // 用户所属机构
    private String managerBrId;
    // 印章编码
    private String sealCode;
    // 印章密码
    private String sealPassword;
    // 流水号
    private String sealserno;
    // 骑缝章标识
    private boolean crossSealFlag;
    // 专用章标识
    private boolean keySealFlag;
    // 报表标识
    private String templateId;
    // 参数
    private Map<String, Object> map;
    // 合同标识 01--借款合同 02--担保合同
    private String contType;

    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getSealCode() {
        return sealCode;
    }

    public void setSealCode(String sealCode) {
        this.sealCode = sealCode;
    }

    public String getSealPassword() {
        return sealPassword;
    }

    public void setSealPassword(String sealPassword) {
        this.sealPassword = sealPassword;
    }

    public String getSealserno() {
        return sealserno;
    }

    public void setSealserno(String sealserno) {
        this.sealserno = sealserno;
    }

    public boolean getCrossSealFlag() {
        return crossSealFlag;
    }

    public void setCrossSealFlag(boolean crossSealFlag) {
        this.crossSealFlag = crossSealFlag;
    }

    public boolean getKeySealFlag() {
        return keySealFlag;
    }

    public void setKeySealFlag(boolean keySealFlag) {
        this.keySealFlag = keySealFlag;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }
}
