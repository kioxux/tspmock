package cn.com.yusys.yusp.dto.server.xdxw0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：小微营业额信息维护
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0005DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "surveyNo")
    private String surveyNo;//调查流水号
    @JsonProperty(value = "surveyType")
    private String surveyType;//调查表类型
    @JsonProperty(value = "list")
    private List list;//start

    public String getSurveyNo() {
        return surveyNo;
    }

    public void setSurveyNo(String surveyNo) {
        this.surveyNo = surveyNo;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

	@Override
	public String toString() {
		return "Xdxw0005DataReqDto{" +
				"surveyNo='" + surveyNo + '\'' +
				", surveyType='" + surveyType + '\'' +
				", list=" + list +
				'}';
	}
}

