package cn.com.yusys.yusp.dto.server.xdzc0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户资产清单查询接口
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0004DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "total")
    private long total;//总数

    @JsonProperty(value = "list")
    private java.util.List<List> list;//start

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "total=" + total +
                ", list=" + list +
                '}';
    }
}
