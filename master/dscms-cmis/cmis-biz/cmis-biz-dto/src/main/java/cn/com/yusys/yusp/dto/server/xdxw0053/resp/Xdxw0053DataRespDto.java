package cn.com.yusys.yusp.dto.server.xdxw0053.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询经营性贷款客户基本信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0053DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indivMarSt")
    private String indivMarSt;//婚姻状况
    @JsonProperty(value = "indivEdt")
    private String indivEdt;//学历
    @JsonProperty(value = "liveYear")
    private BigDecimal liveYear;//居住年限
    @JsonProperty(value = "reprName")
    private String reprName;//法人代表
    @JsonProperty(value = "operAddr")
    private String operAddr;//经营地址
    @JsonProperty(value = "bsinsLicYear")
    private String bsinsLicYear;//营业执照年限
    @JsonProperty(value = "actOperAddr")
    private String actOperAddr;//实际经营地址
    @JsonProperty(value = "actTrade")
    private String actTrade;//实际行业
    @JsonProperty(value = "spouseName")
    private String spouseName;//配偶姓名
    @JsonProperty(value = "spouseCertNo")
    private String spouseCertNo;//配偶证件号
    @JsonProperty(value = "spousePhone")
    private String spousePhone;//配偶电话


    public String getIndivMarSt() {
        return indivMarSt;
    }

    public void setIndivMarSt(String indivMarSt) {
        this.indivMarSt = indivMarSt;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public BigDecimal getLiveYear() {
        return liveYear;
    }

    public void setLiveYear(BigDecimal liveYear) {
        this.liveYear = liveYear;
    }

    public String getReprName() {
        return reprName;
    }

    public void setReprName(String reprName) {
        this.reprName = reprName;
    }

    public String getOperAddr() {
        return operAddr;
    }

    public void setOperAddr(String operAddr) {
        this.operAddr = operAddr;
    }

    public String getBsinsLicYear() {
        return bsinsLicYear;
    }

    public void setBsinsLicYear(String bsinsLicYear) {
        this.bsinsLicYear = bsinsLicYear;
    }

    public String getActOperAddr() {
        return actOperAddr;
    }

    public void setActOperAddr(String actOperAddr) {
        this.actOperAddr = actOperAddr;
    }

    public String getActTrade() {
        return actTrade;
    }

    public void setActTrade(String actTrade) {
        this.actTrade = actTrade;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getSpouseCertNo() {
        return spouseCertNo;
    }

    public void setSpouseCertNo(String spouseCertNo) {
        this.spouseCertNo = spouseCertNo;
    }

    public String getSpousePhone() {
        return spousePhone;
    }

    public void setSpousePhone(String spousePhone) {
        this.spousePhone = spousePhone;
    }

	@Override
	public String toString() {
		return "Xdxw0053DataRespDto{" +
				"indivMarSt='" + indivMarSt + '\'' +
				", indivEdt='" + indivEdt + '\'' +
				", liveYear=" + liveYear +
				", reprName='" + reprName + '\'' +
				", operAddr='" + operAddr + '\'' +
				", bsinsLicYear=" + bsinsLicYear +
				", actOperAddr='" + actOperAddr + '\'' +
				", actTrade='" + actTrade + '\'' +
				", spouseName='" + spouseName + '\'' +
				", spouseCertNo='" + spouseCertNo + '\'' +
				", spousePhone='" + spousePhone + '\'' +
				'}';
	}
}
