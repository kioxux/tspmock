package cn.com.yusys.yusp.dto.server.xdxw0060.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：客户及配偶信用类小微业务贷款授信金额
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0060DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "totlApplyAmount")
    private BigDecimal totlApplyAmount;//授信总金额


    public BigDecimal getTotlApplyAmount() {
        return totlApplyAmount;
    }

    public void setTotlApplyAmount(BigDecimal totlApplyAmount) {
        this.totlApplyAmount = totlApplyAmount;
    }

    @Override
    public String toString() {
        return "Xdxw0060DataRespDto{" +
                "totlApplyAmount=" + totlApplyAmount +
                '}';
    }
}
