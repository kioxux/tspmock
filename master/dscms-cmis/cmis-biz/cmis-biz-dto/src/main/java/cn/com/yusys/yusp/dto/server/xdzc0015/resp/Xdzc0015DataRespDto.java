package cn.com.yusys.yusp.dto.server.xdzc0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：保证金查询接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0015DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bailAccAmt")
    private BigDecimal bailAccAmt;//保证金账户金额
    @JsonProperty(value = "bailAvaAmt")
    private BigDecimal bailAvaAmt;//可提取的保证金账户金额
    @JsonProperty(value = "bailAccNo")
    private String bailAccNo;//保证金账户
    @JsonProperty(value = "bailOrgNO")
    private String bailOrgNO;//保证金开户行号
    @JsonProperty(value = "bailOrgName")
    private String bailOrgName;//保证金开户行行名
    @JsonProperty(value = "guarContNo")
    private String guarContNo;//担保合同编号
    @JsonProperty(value = "guarContCnNo")
    private String guarContCnNo;//担保合同中文编号
    @JsonProperty(value = "contNO")
    private String contNO;//资产池协议号
    @JsonProperty(value = "contName")
    private String contName;//资产池协议名称
    @JsonProperty(value = "endDate")
    private String endDate;//合同到期日
    @JsonProperty(value = "retcod")
    private String retcod;//返回码
    @JsonProperty(value = "opMessage")
    private String opMessage;//返回消息

    public BigDecimal getBailAccAmt() {
        return bailAccAmt;
    }

    public void setBailAccAmt(BigDecimal bailAccAmt) {
        this.bailAccAmt = bailAccAmt;
    }

    public BigDecimal getBailAvaAmt() {
        return bailAvaAmt;
    }

    public void setBailAvaAmt(BigDecimal bailAvaAmt) {
        this.bailAvaAmt = bailAvaAmt;
    }

    public String getBailAccNo() {
        return bailAccNo;
    }

    public void setBailAccNo(String bailAccNo) {
        this.bailAccNo = bailAccNo;
    }

    public String getBailOrgNO() {
        return bailOrgNO;
    }

    public void setBailOrgNO(String bailOrgNO) {
        this.bailOrgNO = bailOrgNO;
    }

    public String getBailOrgName() {
        return bailOrgName;
    }

    public void setBailOrgName(String bailOrgName) {
        this.bailOrgName = bailOrgName;
    }

    public String getGuarContNo() {
        return guarContNo;
    }

    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    public String getGuarContCnNo() {
        return guarContCnNo;
    }

    public void setGuarContCnNo(String guarContCnNo) {
        this.guarContCnNo = guarContCnNo;
    }

    public String getContNO() {
        return contNO;
    }

    public void setContNO(String contNO) {
        this.contNO = contNO;
    }

    public String getContName() {
        return contName;
    }

    public void setContName(String contName) {
        this.contName = contName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRetcod() {
        return retcod;
    }

    public void setRetcod(String retcod) {
        this.retcod = retcod;
    }

    public String getOpMessage() {
        return opMessage;
    }

    public void setOpMessage(String opMessage) {
        this.opMessage = opMessage;
    }

    @Override
    public String toString() {
        return "Xdzc0015DataRespDto{" +
                "bailAccAmt=" + bailAccAmt +
                ", bailAvaAmt=" + bailAvaAmt +
                ", bailAccNo='" + bailAccNo + '\'' +
                ", bailOrgNO='" + bailOrgNO + '\'' +
                ", bailOrgName='" + bailOrgName + '\'' +
                ", guarContNo='" + guarContNo + '\'' +
                ", guarContCnNo='" + guarContCnNo + '\'' +
                ", contNO='" + contNO + '\'' +
                ", contName='" + contName + '\'' +
                ", endDate='" + endDate + '\'' +
                ", retcod='" + retcod + '\'' +
                ", opMessage='" + opMessage + '\'' +
                '}';
    }
}
