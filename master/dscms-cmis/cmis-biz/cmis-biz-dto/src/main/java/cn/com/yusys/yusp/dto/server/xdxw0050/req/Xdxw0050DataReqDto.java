package cn.com.yusys.yusp.dto.server.xdxw0050.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：资产负债表信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0050DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//客户调查表编号
    @JsonProperty(value = "applySerno")
    private String applySerno;//业务唯一编号
    @JsonProperty(value = "subject")
    private String subject;//科目

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getApplySerno() {
        return applySerno;
    }

    public void setApplySerno(String applySerno) {
        this.applySerno = applySerno;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Xdxw0050DataReqDto{" +
                "queryType='" + queryType + '\'' +
                ", indgtSerno='" + indgtSerno + '\'' +
                ", applySerno='" + applySerno + '\'' +
                ", subject='" + subject + '\'' +
                '}';
    }
}
