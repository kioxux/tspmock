package cn.com.yusys.yusp.dto.server.xdtz0062.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询客户的个人消费贷款
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0062DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "count")
    private String count;//笔数

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Xdtz0062DataRespDto{" +
                "count='" + count + '\'' +
                '}';
    }
}
