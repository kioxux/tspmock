package cn.com.yusys.yusp.dto.server.xdtz0053.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：个人社会关系查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0053DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "list")
	private java.util.List<List> list;

	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Xdtz0053DataRespDto{" +
				"list=" + list +
				'}';
	}
}
