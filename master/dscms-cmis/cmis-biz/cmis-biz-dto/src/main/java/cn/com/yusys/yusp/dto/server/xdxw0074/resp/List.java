package cn.com.yusys.yusp.dto.server.xdxw0074.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据客户身份证号查询线上产品申请记录
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serNo")
    private String serNo;//调查流水号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "applyDate")
    private String applyDate;//申请日期
    @JsonProperty(value = "managerName")
    private String managerName;//客户经理名称
    @JsonProperty(value = "orgName")
    private String orgName;//所属机构
    @JsonProperty(value = "userNo")
    private String userNo;//客户经理工号
    @JsonProperty(value = "relation")
    private String relation;//关系
    @JsonProperty(value = "guarTypeMainName")
    private String guarTypeMainName;//担保方式
    @JsonProperty(value = "loanStatusName")
    private String loanStatusName;//申请记录状态
    @JsonProperty(value = "loanTypeName")
    private String loanTypeName;//贷款类型
    @JsonProperty(value = "channel")
    private String channel;//渠道

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getGuarTypeMainName() {
        return guarTypeMainName;
    }

    public void setGuarTypeMainName(String guarTypeMainName) {
        this.guarTypeMainName = guarTypeMainName;
    }

    public String getLoanStatusName() {
        return loanStatusName;
    }

    public void setLoanStatusName(String loanStatusName) {
        this.loanStatusName = loanStatusName;
    }

    public String getLoanTypeName() {
        return loanTypeName;
    }

    public void setLoanTypeName(String loanTypeName) {
        this.loanTypeName = loanTypeName;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Override
    public String toString() {
        return "List{" +
                "serNo='" + serNo + '\'' +
                "cusName='" + cusName + '\'' +
                "applyAmt='" + applyAmt + '\'' +
                "applyDate='" + applyDate + '\'' +
                "managerName='" + managerName + '\'' +
                "orgName='" + orgName + '\'' +
                "userNo='" + userNo + '\'' +
                "relation='" + relation + '\'' +
                "guarTypeMainName='" + guarTypeMainName + '\'' +
                "loanStatusName='" + loanStatusName + '\'' +
                "loanTypeName='" + loanTypeName + '\'' +
                "channel='" + channel + '\'' +
                '}';
    }
}
