package cn.com.yusys.yusp.dto.server.xddh0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：提前还款
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0005DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "accotTranSerno")
    private String accotTranSerno;//核心交易流水
    @JsonProperty(value = "repayInt")
    private BigDecimal repayInt;//还款利息
    @JsonProperty(value = "nextRepayDate")
    private String nextRepayDate;//下次还款日
    @JsonProperty(value = "repayCap")
    private BigDecimal repayCap;//还款本金

    public String getAccotTranSerno() {
        return accotTranSerno;
    }

    public void setAccotTranSerno(String accotTranSerno) {
        this.accotTranSerno = accotTranSerno;
    }

    public BigDecimal getRepayInt() {
        return repayInt;
    }

    public void setRepayInt(BigDecimal repayInt) {
        this.repayInt = repayInt;
    }

    public String getNextRepayDate() {
        return nextRepayDate;
    }

    public void setNextRepayDate(String nextRepayDate) {
        this.nextRepayDate = nextRepayDate;
    }

    public BigDecimal getRepayCap() {
        return repayCap;
    }

    public void setRepayCap(BigDecimal repayCap) {
        this.repayCap = repayCap;
    }

    @Override
    public String toString() {
        return "Xddh0005DataRespDto{" +
                "accotTranSerno='" + accotTranSerno + '\'' +
                ", repayInt=" + repayInt +
                ", nextRepayDate='" + nextRepayDate + '\'' +
                ", repayCap=" + repayCap +
                '}';
    }
}
