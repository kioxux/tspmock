package cn.com.yusys.yusp.dto.server.xdxw0035.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Data：根据流水号查询无还本续贷抵押信息
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0035DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pldList")
    private List<PldList> pldList;

    public List<PldList> getPldList() {
        return pldList;
    }

    public void setPldList(List<PldList> pldList) {
        this.pldList = pldList;
    }

    @Override
    public String toString() {
        return "Xdxw0035DataRespDto{" +
                "pldList=" + pldList +
                '}';
    }
}