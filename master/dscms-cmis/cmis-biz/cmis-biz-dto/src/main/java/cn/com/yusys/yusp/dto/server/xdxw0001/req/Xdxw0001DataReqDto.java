package cn.com.yusys.yusp.dto.server.xdxw0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：房屋估价信息同步
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0001DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "tranDate")
    private String tranDate;//日期
    @JsonProperty(value = "totalAmt")
    private BigDecimal totalAmt;//总价
    @JsonProperty(value = "buildings")
    private String buildings;//楼盘
    @JsonProperty(value = "building")
    private String building;//楼栋
    @JsonProperty(value = "floor")
    private String floor;//楼层
    @JsonProperty(value = "roomNo")
    private String roomNo;//房间号
    @JsonProperty(value = "assetAmt")
    private BigDecimal assetAmt;//估价
    @JsonProperty(value = "squ")
    private Integer squ;//面积
    @JsonProperty(value = "addr")
    private String addr;//地址
    @JsonProperty(value = "qryName")
    private String qryName;//查询人员
    @JsonProperty(value = "qryDate")
    private String qryDate;//查询日期
    @JsonProperty(value = "qryId")
    private String qryId;//查询人ID
    @JsonProperty(value = "phone")
    private String phone;//电话

    public String getTranDate() {
        return tranDate;
    }

    public void setTranDate(String tranDate) {
        this.tranDate = tranDate;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getBuildings() {
        return buildings;
    }

    public void setBuildings(String buildings) {
        this.buildings = buildings;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public BigDecimal getAssetAmt() {
        return assetAmt;
    }

    public void setAssetAmt(BigDecimal assetAmt) {
        this.assetAmt = assetAmt;
    }

    public Integer getSqu() {
        return squ;
    }

    public void setSqu(Integer squ) {
        this.squ = squ;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getQryName() {
        return qryName;
    }

    public void setQryName(String qryName) {
        this.qryName = qryName;
    }

    public String getQryDate() {
        return qryDate;
    }

    public void setQryDate(String qryDate) {
        this.qryDate = qryDate;
    }

    public String getQryId() {
        return qryId;
    }

    public void setQryId(String qryId) {
        this.qryId = qryId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Xdxw0001DataReqDto{" +
                "tranDate='" + tranDate + '\'' +
                ", totalAmt=" + totalAmt +
                ", buildings='" + buildings + '\'' +
                ", building='" + building + '\'' +
                ", floor='" + floor + '\'' +
                ", roomNo='" + roomNo + '\'' +
                ", assetAmt=" + assetAmt +
                ", squ=" + squ +
                ", addr='" + addr + '\'' +
                ", qryName='" + qryName + '\'' +
                ", qryDate='" + qryDate + '\'' +
                ", qryId='" + qryId + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
