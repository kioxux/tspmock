package cn.com.yusys.yusp.dto.server.xdxw0077.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：渠道端查询我的贷款（流程监控）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0077DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//业务流水号
    @JsonProperty(value = "prdNo")
    private String prdNo;//产品编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdxw0077DataReqDto{" +
                "serno='" + serno + '\'' +
                ", prdNo='" + prdNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}
