package cn.com.yusys.yusp.dto.server.xdzc0024.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：票据池资料补全查询
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0024DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "startPage")
    private int startPage;//起始页数
    @JsonProperty(value = "pageSize")
    private int pageSize;//分页大小
    @JsonProperty(value = "taskCreateDateS")
    private String taskCreateDateS;//任务生成日期开始
    @JsonProperty(value = "taskCreateDateE")
    private String taskCreateDateE;//任务生成日期截止
    @JsonProperty(value = "needFinishDateS")
    private String needFinishDateS;//要求完成日期开始
    @JsonProperty(value = "needFinishDateE")
    private String needFinishDateE;//要求完成日期截止
    @JsonProperty(value = "collectDateS")
    private String collectDateS;//收集日期开始
    @JsonProperty(value = "collectDateE")
    private String collectDateE;//收集日期截止
    @JsonProperty(value = "batchDrftAmtS")
    private BigDecimal batchDrftAmtS;//票面总金额下限
    @JsonProperty(value = "batchDrftAmtE")
    private BigDecimal batchDrftAmtE;//票面总金额上限
    @JsonProperty(value = "pvpSerno")
    private String pvpSerno;//票面总金额上限
    @JsonProperty(value = "taskId")
    private String taskId;// 任务编号(补录流水）
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public int getStartPage() {
        return startPage;
    }

    public void setStartPage(int startPage) {
        this.startPage = startPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getTaskCreateDateS() {
        return taskCreateDateS;
    }

    public void setTaskCreateDateS(String taskCreateDateS) {
        this.taskCreateDateS = taskCreateDateS;
    }

    public String getTaskCreateDateE() {
        return taskCreateDateE;
    }

    public void setTaskCreateDateE(String taskCreateDateE) {
        this.taskCreateDateE = taskCreateDateE;
    }

    public String getNeedFinishDateS() {
        return needFinishDateS;
    }

    public void setNeedFinishDateS(String needFinishDateS) {
        this.needFinishDateS = needFinishDateS;
    }

    public String getNeedFinishDateE() {
        return needFinishDateE;
    }

    public void setNeedFinishDateE(String needFinishDateE) {
        this.needFinishDateE = needFinishDateE;
    }

    public String getCollectDateS() {
        return collectDateS;
    }

    public void setCollectDateS(String collectDateS) {
        this.collectDateS = collectDateS;
    }

    public String getCollectDateE() {
        return collectDateE;
    }

    public void setCollectDateE(String collectDateE) {
        this.collectDateE = collectDateE;
    }

    public BigDecimal getBatchDrftAmtS() {
        return batchDrftAmtS;
    }

    public void setBatchDrftAmtS(BigDecimal batchDrftAmtS) {
        this.batchDrftAmtS = batchDrftAmtS;
    }

    public BigDecimal getBatchDrftAmtE() {
        return batchDrftAmtE;
    }

    public void setBatchDrftAmtE(BigDecimal batchDrftAmtE) {
        this.batchDrftAmtE = batchDrftAmtE;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    @Override
    public String toString() {
        return "Xdzc0024DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", startPage=" + startPage +
                ", pageSize=" + pageSize +
                ", taskCreateDateS='" + taskCreateDateS + '\'' +
                ", taskCreateDateE='" + taskCreateDateE + '\'' +
                ", needFinishDateS='" + needFinishDateS + '\'' +
                ", needFinishDateE='" + needFinishDateE + '\'' +
                ", collectDateS='" + collectDateS + '\'' +
                ", collectDateE='" + collectDateE + '\'' +
                ", batchDrftAmtS=" + batchDrftAmtS +
                ", batchDrftAmtE=" + batchDrftAmtE +
                ", pvpSerno='" + pvpSerno + '\'' +
                ", taskId='" + taskId + '\'' +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}
