package cn.com.yusys.yusp.dto.server.xddb0020.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据客户名查询抵押物类型
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "status_name")
    private String status_name;//抵质押状态名
    @JsonProperty(value = "cus_name")
    private String cus_name;//担保人名称
    @JsonProperty(value = "gage_type_name")
    private String gage_type_name;//抵押物类型名
    @JsonProperty(value = "gage_name")
    private String gage_name;//抵押物名称
    @JsonProperty(value = "right_cert_type_name")
    private String right_cert_type_name;//抵押物权属证件类型名
    @JsonProperty(value = "right_cert_no")
    private String right_cert_no;//权属证件号
    @JsonProperty(value = "right_org")
    private String right_org;//权属登记机关
    @JsonProperty(value = "eval_type_name")
    private String eval_type_name;//评估方式名
    @JsonProperty(value = "eval_org")
    private String eval_org;//评估机构
    @JsonProperty(value = "eval_amt")
    private String eval_amt;//评估价值（元）
    @JsonProperty(value = "eval_date")
    private String eval_date;//评估日期
    @JsonProperty(value = "book_amt")
    private BigDecimal book_amt;//内部评估确认金额（元）
    @JsonProperty(value = "mortagage_max_rate")
    private BigDecimal mortagage_max_rate;//最高抵押率
    @JsonProperty(value = "mortagage_rate")
    private BigDecimal mortagage_rate;//设定抵押率（%）
    @JsonProperty(value = "max_mortagage_amt")
    private BigDecimal max_mortagage_amt;//最高可抵押金额（元）
    @JsonProperty(value = "area_location")
    private String area_location;//抵押物存放地点
    @JsonProperty(value = "book_No")
    private String book_No;//抵押登记编号
    @JsonProperty(value = "book_Org")
    private String book_Org;//抵押登记机关
    @JsonProperty(value = "book_Date")
    private String book_Date;//抵押登记日期
    @JsonProperty(value = "lmt_eval_amt")
    private BigDecimal lmt_eval_amt;//授信评估金额
    @JsonProperty(value = "confirm_eval")
    private BigDecimal confirm_eval;//我行认定价值
    @JsonProperty(value = "confirm_date")
    private String confirm_date;//认定日期

    public String getStatus_name() {
        return status_name;
    }

    public void setStatus_name(String status_name) {
        this.status_name = status_name;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getGage_type_name() {
        return gage_type_name;
    }

    public void setGage_type_name(String gage_type_name) {
        this.gage_type_name = gage_type_name;
    }

    public String getGage_name() {
        return gage_name;
    }

    public void setGage_name(String gage_name) {
        this.gage_name = gage_name;
    }

    public String getRight_cert_type_name() {
        return right_cert_type_name;
    }

    public void setRight_cert_type_name(String right_cert_type_name) {
        this.right_cert_type_name = right_cert_type_name;
    }

    public String getRight_cert_no() {
        return right_cert_no;
    }

    public void setRight_cert_no(String right_cert_no) {
        this.right_cert_no = right_cert_no;
    }

    public String getRight_org() {
        return right_org;
    }

    public void setRight_org(String right_org) {
        this.right_org = right_org;
    }

    public String getEval_type_name() {
        return eval_type_name;
    }

    public void setEval_type_name(String eval_type_name) {
        this.eval_type_name = eval_type_name;
    }

    public String getEval_org() {
        return eval_org;
    }

    public void setEval_org(String eval_org) {
        this.eval_org = eval_org;
    }

    public String getEval_amt() {
        return eval_amt;
    }

    public void setEval_amt(String eval_amt) {
        this.eval_amt = eval_amt;
    }

    public String getEval_date() {
        return eval_date;
    }

    public void setEval_date(String eval_date) {
        this.eval_date = eval_date;
    }

    public BigDecimal getBook_amt() {
        return book_amt;
    }

    public void setBook_amt(BigDecimal book_amt) {
        this.book_amt = book_amt;
    }

    public BigDecimal getMortagage_max_rate() {
        return mortagage_max_rate;
    }

    public void setMortagage_max_rate(BigDecimal mortagage_max_rate) {
        this.mortagage_max_rate = mortagage_max_rate;
    }

    public BigDecimal getMortagage_rate() {
        return mortagage_rate;
    }

    public void setMortagage_rate(BigDecimal mortagage_rate) {
        this.mortagage_rate = mortagage_rate;
    }

    public BigDecimal getMax_mortagage_amt() {
        return max_mortagage_amt;
    }

    public void setMax_mortagage_amt(BigDecimal max_mortagage_amt) {
        this.max_mortagage_amt = max_mortagage_amt;
    }

    public String getArea_location() {
        return area_location;
    }

    public void setArea_location(String area_location) {
        this.area_location = area_location;
    }

    public String getBook_No() {
        return book_No;
    }

    public void setBook_No(String book_No) {
        this.book_No = book_No;
    }

    public String getBook_Org() {
        return book_Org;
    }

    public void setBook_Org(String book_Org) {
        this.book_Org = book_Org;
    }

    public String getBook_Date() {
        return book_Date;
    }

    public void setBook_Date(String book_Date) {
        this.book_Date = book_Date;
    }

    public BigDecimal getLmt_eval_amt() {
        return lmt_eval_amt;
    }

    public void setLmt_eval_amt(BigDecimal lmt_eval_amt) {
        this.lmt_eval_amt = lmt_eval_amt;
    }

    public BigDecimal getConfirm_eval() {
        return confirm_eval;
    }

    public void setConfirm_eval(BigDecimal confirm_eval) {
        this.confirm_eval = confirm_eval;
    }

    public String getConfirm_date() {
        return confirm_date;
    }

    public void setConfirm_date(String confirm_date) {
        this.confirm_date = confirm_date;
    }

    @Override
    public String toString() {
        return "List{" +
                "status_name='" + status_name + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", gage_type_name='" + gage_type_name + '\'' +
                ", gage_name='" + gage_name + '\'' +
                ", right_cert_type_name='" + right_cert_type_name + '\'' +
                ", right_cert_no='" + right_cert_no + '\'' +
                ", right_org='" + right_org + '\'' +
                ", eval_type_name='" + eval_type_name + '\'' +
                ", eval_org='" + eval_org + '\'' +
                ", eval_amt='" + eval_amt + '\'' +
                ", eval_date='" + eval_date + '\'' +
                ", book_amt=" + book_amt +
                ", mortagage_max_rate=" + mortagage_max_rate +
                ", mortagage_rate=" + mortagage_rate +
                ", max_mortagage_amt=" + max_mortagage_amt +
                ", area_location='" + area_location + '\'' +
                ", book_No='" + book_No + '\'' +
                ", book_Org='" + book_Org + '\'' +
                ", book_Date='" + book_Date + '\'' +
                ", lmt_eval_amt=" + lmt_eval_amt +
                ", confirm_eval=" + confirm_eval +
                ", confirm_date='" + confirm_date + '\'' +
                '}';
    }
}
