package cn.com.yusys.yusp.dto.server.xdtz0038.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangpeng
 * @Date 2021/5/7 13:54
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pkId")
    private String pkId;//主键
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "guarMode")
    private String guarMode;//担保方式
    @JsonProperty(value = "loanModal")
    private String loanModal;//贷款形式
    @JsonProperty(value = "contCurType")
    private String contCurType;//贷款发放币种
    @JsonProperty(value = "exchangeRate")
    private BigDecimal exchangeRate;//汇率
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//贷款金额
    @JsonProperty(value = "loanBal")
    private BigDecimal loanBal;//贷款余额
    @JsonProperty(value = "exchangeRmbAmt")
    private BigDecimal exchangeRmbAmt;//折合人民币金额
    @JsonProperty(value = "exchangeRmbBal")
    private BigDecimal exchangeRmbBal;//折合人民币余额
    @JsonProperty(value = "innerDebitInterest")
    private BigDecimal innerDebitInterest;//表内欠息
    @JsonProperty(value = "offDebitInterest")
    private BigDecimal offDebitInterest;//表外欠息
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款起始日
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款到期日
    @JsonProperty(value = "loanTerm")
    private String loanTerm;//贷款期限
    @JsonProperty(value = "loanTermUnit")
    private String loanTermUnit;//贷款期限单位
    @JsonProperty(value = "extTimes")
    private String extTimes;//展期次数
    @JsonProperty(value = "overdueDay")
    private String overdueDay;//逾期天数
    @JsonProperty(value = "overdueTimes")
    private String overdueTimes;//逾期期数
    @JsonProperty(value = "settlDate")
    private String settlDate;//结清日期
    @JsonProperty(value = "rateAdjMode")
    private String rateAdjMode;//利率调整方式
    @JsonProperty(value = "isSegInterest")
    private String isSegInterest;//是否分段计息
    @JsonProperty(value = "lprRateIntval")
    private String lprRateIntval;//LPR授信利率区间
    @JsonProperty(value = "curtLprRate")
    private BigDecimal curtLprRate;//当前LPR利率
    @JsonProperty(value = "rateFloatPoint")
    private BigDecimal rateFloatPoint;//浮动点数
    @JsonProperty(value = "execRateYear")
    private BigDecimal execRateYear;//执行利率(年)
    @JsonProperty(value = "overdueRatePefloat")
    private BigDecimal overdueRatePefloat;//逾期利率浮动比
    @JsonProperty(value = "overdueExecRate")
    private BigDecimal overdueExecRate;//逾期执行利率(年利率)
    @JsonProperty(value = "ciRatePefloat")
    private BigDecimal ciRatePefloat;//复息利率浮动比
    @JsonProperty(value = "ciExecRate")
    private BigDecimal ciExecRate;//复息执行利率(年利率)
    @JsonProperty(value = "rateAdjType")
    private String rateAdjType;//利率调整选项
    @JsonProperty(value = "nextRateAdjInterval")
    private String nextRateAdjInterval;//下一次利率调整间隔
    @JsonProperty(value = "nextRateAdjUnit")
    private String nextRateAdjUnit;//下一次利率调整间隔单位
    @JsonProperty(value = "firstAdjDate")
    private String firstAdjDate;//第一次调整日
    @JsonProperty(value = "repayMode")
    private String repayMode;//还款方式
    @JsonProperty(value = "eiIntervalCycle")
    private String eiIntervalCycle;//结息间隔周期
    @JsonProperty(value = "eiIntervalUnit")
    private String eiIntervalUnit;//结息间隔周期单位
    @JsonProperty(value = "deductType")
    private String deductType;//扣款方式
    @JsonProperty(value = "deductDay")
    private String deductDay;//扣款日
    @JsonProperty(value = "loanPayoutAccno")
    private String loanPayoutAccno;//贷款发放账号
    @JsonProperty(value = "loanPayoutSubNo")
    private String loanPayoutSubNo;//贷款发放账号子序号
    @JsonProperty(value = "payoutAcctName")
    private String payoutAcctName;//发放账号名称
    @JsonProperty(value = "isBeEntrustedPay")
    private String isBeEntrustedPay;//是否受托支付
    @JsonProperty(value = "repayAccno")
    private String repayAccno;//贷款还款账号
    @JsonProperty(value = "repaySubAccno")
    private String repaySubAccno;//贷款还款账户子序号
    @JsonProperty(value = "repayAcctName")
    private String repayAcctName;//还款账户名称
    @JsonProperty(value = "loanTer")
    private String loanTer;//贷款投向
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//借款用途类型
    @JsonProperty(value = "subjectNo")
    private String subjectNo;//科目号
    @JsonProperty(value = "agriType")
    private String agriType;//农户类型
    @JsonProperty(value = "agriLoanTer")
    private String agriLoanTer;//涉农贷款投向
    @JsonProperty(value = "loanPromiseFlag")
    private String loanPromiseFlag;//贷款承诺标志
    @JsonProperty(value = "loanPromiseType")
    private String loanPromiseType;//贷款承诺类型
    @JsonProperty(value = "isSbsy")
    private String isSbsy;//是否贴息
    @JsonProperty(value = "sbsyDepAccno")
    private String sbsyDepAccno;//贴息人存款账号
    @JsonProperty(value = "sbsyPerc")
    private BigDecimal sbsyPerc;//贴息比例
    @JsonProperty(value = "sbysEnddate")
    private String sbysEnddate;//贴息到期日
    @JsonProperty(value = "isUseLmtAmt")
    private String isUseLmtAmt;//是否使用授信额度
    @JsonProperty(value = "lmtAccNo")
    private String lmtAccNo;//授信额度编号
    @JsonProperty(value = "replyNo")
    private String replyNo;//批复编号
    @JsonProperty(value = "loanType")
    private String loanType;//贷款类别
    @JsonProperty(value = "isPactLoan")
    private String isPactLoan;//是否落实贷款
    @JsonProperty(value = "isGreenIndustry")
    private String isGreenIndustry;//是否绿色产业
    @JsonProperty(value = "isOperPropertyLoan")
    private String isOperPropertyLoan;//是否经营性物业贷款
    @JsonProperty(value = "isSteelLoan")
    private String isSteelLoan;//是否钢贸行业贷款
    @JsonProperty(value = "isStainlessLoan")
    private String isStainlessLoan;//是否不锈钢行业贷款
    @JsonProperty(value = "isPovertyReliefLoan")
    private String isPovertyReliefLoan;//是否扶贫贴息贷款
    @JsonProperty(value = "isLaborIntenSbsyLoan")
    private String isLaborIntenSbsyLoan;//是否劳动密集型小企业贴息贷款
    @JsonProperty(value = "goverSubszHouseLoan")
    private String goverSubszHouseLoan;//保障性安居工程贷款
    @JsonProperty(value = "engyEnviProteLoan")
    private String engyEnviProteLoan;//项目贷款节能环保
    @JsonProperty(value = "isCphsRurDelpLoan")
    private String isCphsRurDelpLoan;//是否农村综合开发贷款标志
    @JsonProperty(value = "realproLoan")
    private String realproLoan;//房地产贷款
    @JsonProperty(value = "realproLoanRate")
    private String realproLoanRate;//房产开发贷款资本金比例
    @JsonProperty(value = "guarDetailMode")
    private String guarDetailMode;//担保方式细分
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构编号
    @JsonProperty(value = "finaBrIdName")
    private String finaBrIdName;//账务机构名称
    @JsonProperty(value = "disbOrgNo")
    private String disbOrgNo;//放款机构编号
    @JsonProperty(value = "disbOrgName")
    private String disbOrgName;//放款机构名称
    @JsonProperty(value = "fiveClass")
    private String fiveClass;//五级分类
    @JsonProperty(value = "tenClass")
    private String tenClass;//十级分类
    @JsonProperty(value = "classDate")
    private String classDate;//分类日期
    @JsonProperty(value = "accStatus")
    private String accStatus;//台账状态
    @JsonProperty(value = "belgLine")
    private String belgLine;//所属条线
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "updId")
    private String updId;//最近修改人
    @JsonProperty(value = "updBrId")
    private String updBrId;//最近修改机构
    @JsonProperty(value = "updDate")
    private String updDate;//最近修改日期
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//主管机构
    @JsonProperty(value = "createTime")
    private String createTime;//创建时间
    @JsonProperty(value = "updateTime")
    private String updateTime;//修改时间

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getContCurType() {
        return contCurType;
    }

    public void setContCurType(String contCurType) {
        this.contCurType = contCurType;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    public BigDecimal getExchangeRmbAmt() {
        return exchangeRmbAmt;
    }

    public void setExchangeRmbAmt(BigDecimal exchangeRmbAmt) {
        this.exchangeRmbAmt = exchangeRmbAmt;
    }

    public BigDecimal getExchangeRmbBal() {
        return exchangeRmbBal;
    }

    public void setExchangeRmbBal(BigDecimal exchangeRmbBal) {
        this.exchangeRmbBal = exchangeRmbBal;
    }

    public BigDecimal getInnerDebitInterest() {
        return innerDebitInterest;
    }

    public void setInnerDebitInterest(BigDecimal innerDebitInterest) {
        this.innerDebitInterest = innerDebitInterest;
    }

    public BigDecimal getOffDebitInterest() {
        return offDebitInterest;
    }

    public void setOffDebitInterest(BigDecimal offDebitInterest) {
        this.offDebitInterest = offDebitInterest;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getLoanTermUnit() {
        return loanTermUnit;
    }

    public void setLoanTermUnit(String loanTermUnit) {
        this.loanTermUnit = loanTermUnit;
    }

    public String getExtTimes() {
        return extTimes;
    }

    public void setExtTimes(String extTimes) {
        this.extTimes = extTimes;
    }

    public String getOverdueDay() {
        return overdueDay;
    }

    public void setOverdueDay(String overdueDay) {
        this.overdueDay = overdueDay;
    }

    public String getOverdueTimes() {
        return overdueTimes;
    }

    public void setOverdueTimes(String overdueTimes) {
        this.overdueTimes = overdueTimes;
    }

    public String getSettlDate() {
        return settlDate;
    }

    public void setSettlDate(String settlDate) {
        this.settlDate = settlDate;
    }

    public String getRateAdjMode() {
        return rateAdjMode;
    }

    public void setRateAdjMode(String rateAdjMode) {
        this.rateAdjMode = rateAdjMode;
    }

    public String getIsSegInterest() {
        return isSegInterest;
    }

    public void setIsSegInterest(String isSegInterest) {
        this.isSegInterest = isSegInterest;
    }

    public String getLprRateIntval() {
        return lprRateIntval;
    }

    public void setLprRateIntval(String lprRateIntval) {
        this.lprRateIntval = lprRateIntval;
    }

    public BigDecimal getCurtLprRate() {
        return curtLprRate;
    }

    public void setCurtLprRate(BigDecimal curtLprRate) {
        this.curtLprRate = curtLprRate;
    }

    public BigDecimal getRateFloatPoint() {
        return rateFloatPoint;
    }

    public void setRateFloatPoint(BigDecimal rateFloatPoint) {
        this.rateFloatPoint = rateFloatPoint;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public BigDecimal getOverdueRatePefloat() {
        return overdueRatePefloat;
    }

    public void setOverdueRatePefloat(BigDecimal overdueRatePefloat) {
        this.overdueRatePefloat = overdueRatePefloat;
    }

    public BigDecimal getOverdueExecRate() {
        return overdueExecRate;
    }

    public void setOverdueExecRate(BigDecimal overdueExecRate) {
        this.overdueExecRate = overdueExecRate;
    }

    public BigDecimal getCiRatePefloat() {
        return ciRatePefloat;
    }

    public void setCiRatePefloat(BigDecimal ciRatePefloat) {
        this.ciRatePefloat = ciRatePefloat;
    }

    public BigDecimal getCiExecRate() {
        return ciExecRate;
    }

    public void setCiExecRate(BigDecimal ciExecRate) {
        this.ciExecRate = ciExecRate;
    }

    public String getRateAdjType() {
        return rateAdjType;
    }

    public void setRateAdjType(String rateAdjType) {
        this.rateAdjType = rateAdjType;
    }

    public String getNextRateAdjInterval() {
        return nextRateAdjInterval;
    }

    public void setNextRateAdjInterval(String nextRateAdjInterval) {
        this.nextRateAdjInterval = nextRateAdjInterval;
    }

    public String getNextRateAdjUnit() {
        return nextRateAdjUnit;
    }

    public void setNextRateAdjUnit(String nextRateAdjUnit) {
        this.nextRateAdjUnit = nextRateAdjUnit;
    }

    public String getFirstAdjDate() {
        return firstAdjDate;
    }

    public void setFirstAdjDate(String firstAdjDate) {
        this.firstAdjDate = firstAdjDate;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getEiIntervalCycle() {
        return eiIntervalCycle;
    }

    public void setEiIntervalCycle(String eiIntervalCycle) {
        this.eiIntervalCycle = eiIntervalCycle;
    }

    public String getEiIntervalUnit() {
        return eiIntervalUnit;
    }

    public void setEiIntervalUnit(String eiIntervalUnit) {
        this.eiIntervalUnit = eiIntervalUnit;
    }

    public String getDeductType() {
        return deductType;
    }

    public void setDeductType(String deductType) {
        this.deductType = deductType;
    }

    public String getDeductDay() {
        return deductDay;
    }

    public void setDeductDay(String deductDay) {
        this.deductDay = deductDay;
    }

    public String getLoanPayoutAccno() {
        return loanPayoutAccno;
    }

    public void setLoanPayoutAccno(String loanPayoutAccno) {
        this.loanPayoutAccno = loanPayoutAccno;
    }

    public String getLoanPayoutSubNo() {
        return loanPayoutSubNo;
    }

    public void setLoanPayoutSubNo(String loanPayoutSubNo) {
        this.loanPayoutSubNo = loanPayoutSubNo;
    }

    public String getPayoutAcctName() {
        return payoutAcctName;
    }

    public void setPayoutAcctName(String payoutAcctName) {
        this.payoutAcctName = payoutAcctName;
    }

    public String getIsBeEntrustedPay() {
        return isBeEntrustedPay;
    }

    public void setIsBeEntrustedPay(String isBeEntrustedPay) {
        this.isBeEntrustedPay = isBeEntrustedPay;
    }

    public String getRepayAccno() {
        return repayAccno;
    }

    public void setRepayAccno(String repayAccno) {
        this.repayAccno = repayAccno;
    }

    public String getRepaySubAccno() {
        return repaySubAccno;
    }

    public void setRepaySubAccno(String repaySubAccno) {
        this.repaySubAccno = repaySubAccno;
    }

    public String getRepayAcctName() {
        return repayAcctName;
    }

    public void setRepayAcctName(String repayAcctName) {
        this.repayAcctName = repayAcctName;
    }

    public String getLoanTer() {
        return loanTer;
    }

    public void setLoanTer(String loanTer) {
        this.loanTer = loanTer;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getSubjectNo() {
        return subjectNo;
    }

    public void setSubjectNo(String subjectNo) {
        this.subjectNo = subjectNo;
    }

    public String getAgriType() {
        return agriType;
    }

    public void setAgriType(String agriType) {
        this.agriType = agriType;
    }

    public String getAgriLoanTer() {
        return agriLoanTer;
    }

    public void setAgriLoanTer(String agriLoanTer) {
        this.agriLoanTer = agriLoanTer;
    }

    public String getLoanPromiseFlag() {
        return loanPromiseFlag;
    }

    public void setLoanPromiseFlag(String loanPromiseFlag) {
        this.loanPromiseFlag = loanPromiseFlag;
    }

    public String getLoanPromiseType() {
        return loanPromiseType;
    }

    public void setLoanPromiseType(String loanPromiseType) {
        this.loanPromiseType = loanPromiseType;
    }

    public String getIsSbsy() {
        return isSbsy;
    }

    public void setIsSbsy(String isSbsy) {
        this.isSbsy = isSbsy;
    }

    public String getSbsyDepAccno() {
        return sbsyDepAccno;
    }

    public void setSbsyDepAccno(String sbsyDepAccno) {
        this.sbsyDepAccno = sbsyDepAccno;
    }

    public BigDecimal getSbsyPerc() {
        return sbsyPerc;
    }

    public void setSbsyPerc(BigDecimal sbsyPerc) {
        this.sbsyPerc = sbsyPerc;
    }

    public String getSbysEnddate() {
        return sbysEnddate;
    }

    public void setSbysEnddate(String sbysEnddate) {
        this.sbysEnddate = sbysEnddate;
    }

    public String getIsUseLmtAmt() {
        return isUseLmtAmt;
    }

    public void setIsUseLmtAmt(String isUseLmtAmt) {
        this.isUseLmtAmt = isUseLmtAmt;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    public String getReplyNo() {
        return replyNo;
    }

    public void setReplyNo(String replyNo) {
        this.replyNo = replyNo;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getIsPactLoan() {
        return isPactLoan;
    }

    public void setIsPactLoan(String isPactLoan) {
        this.isPactLoan = isPactLoan;
    }

    public String getIsGreenIndustry() {
        return isGreenIndustry;
    }

    public void setIsGreenIndustry(String isGreenIndustry) {
        this.isGreenIndustry = isGreenIndustry;
    }

    public String getIsOperPropertyLoan() {
        return isOperPropertyLoan;
    }

    public void setIsOperPropertyLoan(String isOperPropertyLoan) {
        this.isOperPropertyLoan = isOperPropertyLoan;
    }

    public String getIsSteelLoan() {
        return isSteelLoan;
    }

    public void setIsSteelLoan(String isSteelLoan) {
        this.isSteelLoan = isSteelLoan;
    }

    public String getIsStainlessLoan() {
        return isStainlessLoan;
    }

    public void setIsStainlessLoan(String isStainlessLoan) {
        this.isStainlessLoan = isStainlessLoan;
    }

    public String getIsPovertyReliefLoan() {
        return isPovertyReliefLoan;
    }

    public void setIsPovertyReliefLoan(String isPovertyReliefLoan) {
        this.isPovertyReliefLoan = isPovertyReliefLoan;
    }

    public String getIsLaborIntenSbsyLoan() {
        return isLaborIntenSbsyLoan;
    }

    public void setIsLaborIntenSbsyLoan(String isLaborIntenSbsyLoan) {
        this.isLaborIntenSbsyLoan = isLaborIntenSbsyLoan;
    }

    public String getGoverSubszHouseLoan() {
        return goverSubszHouseLoan;
    }

    public void setGoverSubszHouseLoan(String goverSubszHouseLoan) {
        this.goverSubszHouseLoan = goverSubszHouseLoan;
    }

    public String getEngyEnviProteLoan() {
        return engyEnviProteLoan;
    }

    public void setEngyEnviProteLoan(String engyEnviProteLoan) {
        this.engyEnviProteLoan = engyEnviProteLoan;
    }

    public String getIsCphsRurDelpLoan() {
        return isCphsRurDelpLoan;
    }

    public void setIsCphsRurDelpLoan(String isCphsRurDelpLoan) {
        this.isCphsRurDelpLoan = isCphsRurDelpLoan;
    }

    public String getRealproLoan() {
        return realproLoan;
    }

    public void setRealproLoan(String realproLoan) {
        this.realproLoan = realproLoan;
    }

    public String getRealproLoanRate() {
        return realproLoanRate;
    }

    public void setRealproLoanRate(String realproLoanRate) {
        this.realproLoanRate = realproLoanRate;
    }

    public String getGuarDetailMode() {
        return guarDetailMode;
    }

    public void setGuarDetailMode(String guarDetailMode) {
        this.guarDetailMode = guarDetailMode;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getFinaBrIdName() {
        return finaBrIdName;
    }

    public void setFinaBrIdName(String finaBrIdName) {
        this.finaBrIdName = finaBrIdName;
    }

    public String getDisbOrgNo() {
        return disbOrgNo;
    }

    public void setDisbOrgNo(String disbOrgNo) {
        this.disbOrgNo = disbOrgNo;
    }

    public String getDisbOrgName() {
        return disbOrgName;
    }

    public void setDisbOrgName(String disbOrgName) {
        this.disbOrgName = disbOrgName;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getTenClass() {
        return tenClass;
    }

    public void setTenClass(String tenClass) {
        this.tenClass = tenClass;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "List{" +
                "pkId='" + pkId + '\'' +
                ", billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", guarMode='" + guarMode + '\'' +
                ", loanModal='" + loanModal + '\'' +
                ", contCurType='" + contCurType + '\'' +
                ", exchangeRate=" + exchangeRate +
                ", loanAmt=" + loanAmt +
                ", loanBal=" + loanBal +
                ", exchangeRmbAmt=" + exchangeRmbAmt +
                ", exchangeRmbBal=" + exchangeRmbBal +
                ", innerDebitInterest=" + innerDebitInterest +
                ", offDebitInterest=" + offDebitInterest +
                ", loanStartDate='" + loanStartDate + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", loanTerm='" + loanTerm + '\'' +
                ", loanTermUnit='" + loanTermUnit + '\'' +
                ", extTimes='" + extTimes + '\'' +
                ", overdueDay='" + overdueDay + '\'' +
                ", overdueTimes='" + overdueTimes + '\'' +
                ", settlDate='" + settlDate + '\'' +
                ", rateAdjMode='" + rateAdjMode + '\'' +
                ", isSegInterest='" + isSegInterest + '\'' +
                ", lprRateIntval='" + lprRateIntval + '\'' +
                ", curtLprRate=" + curtLprRate +
                ", rateFloatPoint=" + rateFloatPoint +
                ", execRateYear=" + execRateYear +
                ", overdueRatePefloat=" + overdueRatePefloat +
                ", overdueExecRate=" + overdueExecRate +
                ", ciRatePefloat=" + ciRatePefloat +
                ", ciExecRate=" + ciExecRate +
                ", rateAdjType='" + rateAdjType + '\'' +
                ", nextRateAdjInterval='" + nextRateAdjInterval + '\'' +
                ", nextRateAdjUnit='" + nextRateAdjUnit + '\'' +
                ", firstAdjDate='" + firstAdjDate + '\'' +
                ", repayMode='" + repayMode + '\'' +
                ", eiIntervalCycle='" + eiIntervalCycle + '\'' +
                ", eiIntervalUnit='" + eiIntervalUnit + '\'' +
                ", deductType='" + deductType + '\'' +
                ", deductDay='" + deductDay + '\'' +
                ", loanPayoutAccno='" + loanPayoutAccno + '\'' +
                ", loanPayoutSubNo='" + loanPayoutSubNo + '\'' +
                ", payoutAcctName='" + payoutAcctName + '\'' +
                ", isBeEntrustedPay='" + isBeEntrustedPay + '\'' +
                ", repayAccno='" + repayAccno + '\'' +
                ", repaySubAccno='" + repaySubAccno + '\'' +
                ", repayAcctName='" + repayAcctName + '\'' +
                ", loanTer='" + loanTer + '\'' +
                ", loanUseType='" + loanUseType + '\'' +
                ", subjectNo='" + subjectNo + '\'' +
                ", agriType='" + agriType + '\'' +
                ", agriLoanTer='" + agriLoanTer + '\'' +
                ", loanPromiseFlag='" + loanPromiseFlag + '\'' +
                ", loanPromiseType='" + loanPromiseType + '\'' +
                ", isSbsy='" + isSbsy + '\'' +
                ", sbsyDepAccno='" + sbsyDepAccno + '\'' +
                ", sbsyPerc=" + sbsyPerc +
                ", sbysEnddate='" + sbysEnddate + '\'' +
                ", isUseLmtAmt='" + isUseLmtAmt + '\'' +
                ", lmtAccNo='" + lmtAccNo + '\'' +
                ", replyNo='" + replyNo + '\'' +
                ", loanType='" + loanType + '\'' +
                ", isPactLoan='" + isPactLoan + '\'' +
                ", isGreenIndustry='" + isGreenIndustry + '\'' +
                ", isOperPropertyLoan='" + isOperPropertyLoan + '\'' +
                ", isSteelLoan='" + isSteelLoan + '\'' +
                ", isStainlessLoan='" + isStainlessLoan + '\'' +
                ", isPovertyReliefLoan='" + isPovertyReliefLoan + '\'' +
                ", isLaborIntenSbsyLoan='" + isLaborIntenSbsyLoan + '\'' +
                ", goverSubszHouseLoan='" + goverSubszHouseLoan + '\'' +
                ", engyEnviProteLoan='" + engyEnviProteLoan + '\'' +
                ", isCphsRurDelpLoan='" + isCphsRurDelpLoan + '\'' +
                ", realproLoan='" + realproLoan + '\'' +
                ", realproLoanRate='" + realproLoanRate + '\'' +
                ", guarDetailMode='" + guarDetailMode + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", finaBrIdName='" + finaBrIdName + '\'' +
                ", disbOrgNo='" + disbOrgNo + '\'' +
                ", disbOrgName='" + disbOrgName + '\'' +
                ", fiveClass='" + fiveClass + '\'' +
                ", tenClass='" + tenClass + '\'' +
                ", classDate='" + classDate + '\'' +
                ", accStatus='" + accStatus + '\'' +
                ", belgLine='" + belgLine + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
