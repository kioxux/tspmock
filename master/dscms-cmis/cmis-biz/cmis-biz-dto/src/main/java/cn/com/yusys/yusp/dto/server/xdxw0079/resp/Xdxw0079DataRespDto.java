package cn.com.yusys.yusp.dto.server.xdxw0079.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：小微续贷白名单查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0079DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdxw0079.resp.List> list;

    public java.util.List<cn.com.yusys.yusp.dto.server.xdxw0079.resp.List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdxw0079DataRespDto{" +
                "list='" + list + '\'' +
                '}';
    }
}  
