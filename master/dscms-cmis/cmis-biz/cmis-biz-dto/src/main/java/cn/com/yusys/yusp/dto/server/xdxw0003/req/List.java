package cn.com.yusys.yusp.dto.server.xdxw0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：小微贷前调查信息维护
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List  implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pkId")
    private String pkId;//主键
    @JsonProperty(value = "turnoverAmt")
    private String turnoverAmt;//营业额检验项目
    @JsonProperty(value = "salesRate")
    private BigDecimal salesRate;//偏差率

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTurnoverAmt() {
        return turnoverAmt;
    }

    public void setTurnoverAmt(String turnoverAmt) {
        this.turnoverAmt = turnoverAmt;
    }

    public BigDecimal getSalesRate() {
        return salesRate;
    }

    public void setSalesRate(BigDecimal salesRate) {
        this.salesRate = salesRate;
    }

    @Override
    public String toString() {
        return "List{" +
                "pkId='" + pkId + '\'' +
                ", turnoverAmt='" + turnoverAmt + '\'' +
                ", salesRate=" + salesRate +
                '}';
    }
}
