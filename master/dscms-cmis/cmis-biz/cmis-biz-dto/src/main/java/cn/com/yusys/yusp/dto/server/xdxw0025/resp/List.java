package cn.com.yusys.yusp.dto.server.xdxw0025.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {

    @JsonProperty(value = "pldimnType")
    private String pldimnType;//抵质押类型
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//客户证件号
    @JsonProperty(value = "pldimnValue")
    private BigDecimal pldimnValue;//抵押物价值
    @JsonProperty(value = "pldRate")
    private BigDecimal pldRate;//抵押率
    @JsonProperty(value = "locateAddr")
    private String locateAddr;//坐落地址

    public String getPldimnType() {
        return pldimnType;
    }

    public void setPldimnType(String pldimnType) {
        this.pldimnType = pldimnType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public BigDecimal getPldimnValue() {
        return pldimnValue;
    }

    public void setPldimnValue(BigDecimal pldimnValue) {
        this.pldimnValue = pldimnValue;
    }

    public BigDecimal getPldRate() {
        return pldRate;
    }

    public void setPldRate(BigDecimal pldRate) {
        this.pldRate = pldRate;
    }

    public String getLocateAddr() {
        return locateAddr;
    }

    public void setLocateAddr(String locateAddr) {
        this.locateAddr = locateAddr;
    }

    @Override
    public String toString() {
        return "List{" +
                "pldimnType='" + pldimnType + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certNo='" + certNo + '\'' +
                ", pldimnValue=" + pldimnValue +
                ", pldRate=" + pldRate +
                ", locateAddr='" + locateAddr + '\'' +
                '}';
    }
}
