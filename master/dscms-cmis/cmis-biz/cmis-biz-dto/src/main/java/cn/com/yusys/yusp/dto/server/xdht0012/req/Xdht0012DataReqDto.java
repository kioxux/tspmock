package cn.com.yusys.yusp.dto.server.xdht0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：借款担保合同签订/支用
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0012DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "oprType")
    private String oprType;//操作类型
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "grtContNo")
    private String grtContNo;//担保合同号
    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号
    @JsonProperty(value = "pvpSerno")
    private String pvpSerno;//出账流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "curtPvpAmt")
    private String curtPvpAmt;//本次出账金额
    @JsonProperty(value = "billStartDate")
    private String billStartDate;//借据起始日
    @JsonProperty(value = "billEndDate")
    private String billEndDate;//借据到期日
    @JsonProperty(value = "term")
    private String term;//期限
    @JsonProperty(value = "loanAcctNo")
    private String loanAcctNo;//贷款发放账户
    @JsonProperty(value = "loanAcctName")
    private String loanAcctName;//贷款账户名称
    @JsonProperty(value = "loanSubAcctNo")
    private String loanSubAcctNo;//贷款发放账户子账户序号
    @JsonProperty(value = "pvpStatus")
    private String pvpStatus;//出账状态
    @JsonProperty(value = "chnlSour")
    private String chnlSour;//来源渠道
    @JsonProperty(value = "imageSerno1")
    private String imageSerno1;//影像流水号1
    @JsonProperty(value = "imageSerno2")
    private String imageSerno2;//影像流水号2

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getGrtContNo() {
        return grtContNo;
    }

    public void setGrtContNo(String grtContNo) {
        this.grtContNo = grtContNo;
    }

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCurtPvpAmt() {
        return curtPvpAmt;
    }

    public void setCurtPvpAmt(String curtPvpAmt) {
        this.curtPvpAmt = curtPvpAmt;
    }

    public String getBillStartDate() {
        return billStartDate;
    }

    public void setBillStartDate(String billStartDate) {
        this.billStartDate = billStartDate;
    }

    public String getBillEndDate() {
        return billEndDate;
    }

    public void setBillEndDate(String billEndDate) {
        this.billEndDate = billEndDate;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getLoanAcctNo() {
        return loanAcctNo;
    }

    public void setLoanAcctNo(String loanAcctNo) {
        this.loanAcctNo = loanAcctNo;
    }

    public String getLoanAcctName() {
        return loanAcctName;
    }

    public void setLoanAcctName(String loanAcctName) {
        this.loanAcctName = loanAcctName;
    }

    public String getLoanSubAcctNo() {
        return loanSubAcctNo;
    }

    public void setLoanSubAcctNo(String loanSubAcctNo) {
        this.loanSubAcctNo = loanSubAcctNo;
    }

    public String getPvpStatus() {
        return pvpStatus;
    }

    public void setPvpStatus(String pvpStatus) {
        this.pvpStatus = pvpStatus;
    }

    public String getChnlSour() {
        return chnlSour;
    }

    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour;
    }

    public String getImageSerno1() {
        return imageSerno1;
    }

    public void setImageSerno1(String imageSerno1) {
        this.imageSerno1 = imageSerno1;
    }

    public String getImageSerno2() {
        return imageSerno2;
    }

    public void setImageSerno2(String imageSerno2) {
        this.imageSerno2 = imageSerno2;
    }

    @Override
    public String toString() {
        return "Xdht0012DataReqDto{" +
                "oprType='" + oprType + '\'' +
                "managerId='" + managerId + '\'' +
                "grtContNo='" + grtContNo + '\'' +
                "loanContNo='" + loanContNo + '\'' +
                "pvpSerno='" + pvpSerno + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "certNo='" + certNo + '\'' +
                "curtPvpAmt='" + curtPvpAmt + '\'' +
                "billStartDate='" + billStartDate + '\'' +
                "billEndDate='" + billEndDate + '\'' +
                "term='" + term + '\'' +
                "loanAcctNo='" + loanAcctNo + '\'' +
                "loanAcctName='" + loanAcctName + '\'' +
                "loanSubAcctNo='" + loanSubAcctNo + '\'' +
                "pvpStatus='" + pvpStatus + '\'' +
                "chnlSour='" + chnlSour + '\'' +
                "imageSerno1='" + imageSerno1 + '\'' +
                "imageSerno2='" + imageSerno2 + '\'' +
                '}';
    }
}
