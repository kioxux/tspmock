package cn.com.yusys.yusp.dto.server.xdxw0048.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：增享贷2.0风控模型B生成批复
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0048DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "task_id")
    private String task_id;//办理流水号
    @JsonProperty(value = "model_result")
    private String model_result;//模型结果
    @JsonProperty(value = "model_appr_time")
    private String model_appr_time;//模型审批时间
    @JsonProperty(value = "apply_no")
    private String apply_no;//业务唯一编号
    @JsonProperty(value = "opinion")
    private String opinion;//模型意见
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "loan_term")
    private String loan_term;//贷款期限
    @JsonProperty(value = "final_amount")
    private BigDecimal final_amount;//最终金额
    @JsonProperty(value = "final_rate")
    private BigDecimal final_rate;//最终利率

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getModel_result() {
        return model_result;
    }

    public void setModel_result(String model_result) {
        this.model_result = model_result;
    }

    public String getModel_appr_time() {
        return model_appr_time;
    }

    public void setModel_appr_time(String model_appr_time) {
        this.model_appr_time = model_appr_time;
    }

    public String getApply_no() {
        return apply_no;
    }

    public void setApply_no(String apply_no) {
        this.apply_no = apply_no;
    }

    public String getOpinion() {
        return opinion;
    }

    public void setOpinion(String opinion) {
        this.opinion = opinion;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getLoan_term() {
        return loan_term;
    }

    public void setLoan_term(String loan_term) {
        this.loan_term = loan_term;
    }

    public BigDecimal getFinal_amount() {
        return final_amount;
    }

    public void setFinal_amount(BigDecimal final_amount) {
        this.final_amount = final_amount;
    }

    public BigDecimal getFinal_rate() {
        return final_rate;
    }

    public void setFinal_rate(BigDecimal final_rate) {
        this.final_rate = final_rate;
    }

    @Override
    public String toString() {
        return "Xdxw0048DataReqDto{" +
                "task_id='" + task_id + '\'' +
                "model_result='" + model_result + '\'' +
                "model_appr_time='" + model_appr_time + '\'' +
                "apply_no='" + apply_no + '\'' +
                "opinion='" + opinion + '\'' +
                "cus_id='" + cus_id + '\'' +
                "loan_term='" + loan_term + '\'' +
                "final_amount='" + final_amount + '\'' +
                "final_rate='" + final_rate + '\'' +
                '}';
    }
}
