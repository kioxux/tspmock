package cn.com.yusys.yusp.dto.server.xdcz0031.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：大额分期合同详情查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0031DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalNum")
    private String totalNum;//贷款数

    public String getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(String totalNum) {
        this.totalNum = totalNum;
    }
    @Override
    public String toString() {
        return "Xdcz0031DataRespDto{" +
                "totalNum='" + totalNum + '\'' +
                '}';
    }
}  
