package cn.com.yusys.yusp.dto.server.xdxw0081.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：检查是否有优农贷、优企贷、惠享贷合同
 * @author zdl
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0081DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "if_hxd")
    private String if_hxd;//是否有惠享贷
    @JsonProperty(value = "if_ynd")
    private String if_ynd;//是否有优农贷
    @JsonProperty(value = "if_yqd")
    private String if_yqd;//是否有优企贷

    public String getIf_hxd() {
        return if_hxd;
    }

    public void setIf_hxd(String if_hxd) {
        this.if_hxd = if_hxd;
    }

    public String getIf_ynd() {
        return if_ynd;
    }

    public void setIf_ynd(String if_ynd) {
        this.if_ynd = if_ynd;
    }

    public String getIf_yqd() {
        return if_yqd;
    }

    public void setIf_yqd(String if_yqd) {
        this.if_yqd = if_yqd;
    }

    @Override
    public String toString() {
        return "Xdxw0081DataRespDto{" +
                "if_hxd='" + if_hxd + '\'' +
                ", if_ynd='" + if_ynd + '\'' +
                ", if_yqd='" + if_yqd + '\'' +
                '}';
    }
}
