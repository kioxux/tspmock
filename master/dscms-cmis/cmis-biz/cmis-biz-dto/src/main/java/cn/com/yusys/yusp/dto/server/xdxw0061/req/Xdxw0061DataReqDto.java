package cn.com.yusys.yusp.dto.server.xdxw0061.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：通过无还本续贷调查表编号查询配偶核心客户号
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0061DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//调查流水号



    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

	@Override
	public String toString() {
		return "Xdxw0061DataReqDto{" +
				"indgtSerno='" + indgtSerno + '\'' +
				'}';
	}
}
