package cn.com.yusys.yusp.dto.server.xdht0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：银承协议查询接口
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0002DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//银承协议编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdht0002DataReqDto{" +
                "contNo='" + contNo + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
