package cn.com.yusys.yusp.dto.server.xdtz0029.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询指定票号在信贷台账中是否已贴现
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0029DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "porderNo")
    private String porderNo;//汇票号码

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

	@Override
	public String toString() {
		return "Xdtz0029DataReqDto{" +
				"porderNo='" + porderNo + '\'' +
				'}';
	}
}
