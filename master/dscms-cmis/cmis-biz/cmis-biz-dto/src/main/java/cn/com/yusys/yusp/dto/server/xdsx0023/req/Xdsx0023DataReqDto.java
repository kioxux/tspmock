package cn.com.yusys.yusp.dto.server.xdsx0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 请求Data：保函协议查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0023DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotBlank(message = "certno不能为空")
    @JsonProperty(value = "certno")
    private String certno;//组织机构代码
    @NotBlank(message = "queflg不能为空")
    @JsonProperty(value = "queflg")
    private String queflg;//查询条件

    public String getCertno() {
        return certno;
    }

    public void setCertno(String certno) {
        this.certno = certno;
    }

    public String getQueflg() {
        return queflg;
    }

    public void setQueflg(String queflg) {
        this.queflg = queflg;
    }

    @Override
    public String toString() {
        return "Xdsx0023DataReqDto{" +
                "certno='" + certno + '\'' +
                ", queflg='" + queflg + '\'' +
                '}';
    }
}
