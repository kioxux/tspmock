package cn.com.yusys.yusp.dto.server.xdxw0060.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：客户及配偶信用类小微业务贷款授信金额
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0060DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "certNo")
    private String certNo;//身份证号


    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xdxw0060DataReqDto{" +
                "certNo='" + certNo + '\'' +
                '}';
    }
}
