package cn.com.yusys.yusp.dto.server.xddb0012.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 19:59
 * @Version 1.0
 */
public class MortgagorsList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "mogana")
    private String mogana;//抵押人-姓名/名称
    @JsonProperty(value = "modoty")
    private String modoty;//抵押人-证件类型
    @JsonProperty(value = "modonu")
    private String modonu;//抵押人-证件号
    @JsonProperty(value = "molere")
    private String molere;//抵押人-法人/负责人
    @JsonProperty(value = "moleph")
    private String moleph;//抵押人-电话

    public String getMogana() {
        return mogana;
    }

    public void setMogana(String mogana) {
        this.mogana = mogana;
    }

    public String getModoty() {
        return modoty;
    }

    public void setModoty(String modoty) {
        this.modoty = modoty;
    }

    public String getModonu() {
        return modonu;
    }

    public void setModonu(String modonu) {
        this.modonu = modonu;
    }

    public String getMolere() {
        return molere;
    }

    public void setMolere(String molere) {
        this.molere = molere;
    }

    public String getMoleph() {
        return moleph;
    }

    public void setMoleph(String moleph) {
        this.moleph = moleph;
    }


    @Override
    public String toString() {
        return "MortgagorsList{" +
                "mogana='" + mogana + '\'' +
                ", modoty='" + modoty + '\'' +
                ", modonu='" + modonu + '\'' +
                ", molere='" + molere + '\'' +
                ", moleph='" + moleph + '\'' +
                '}';
    }
}
