package cn.com.yusys.yusp.dto.server.xdls0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：白领贷额度查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdls0006DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    @JsonProperty(value = "guarList")
    private java.util.List<List> guarList;

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    public java.util.List<List> getGuarList() {
        return guarList;
    }

    public void setGuarList(java.util.List<List> guarList) {
        this.guarList = guarList;
    }

    @Override
    public String toString() {
        return "Xdls0006DataRespDto{" +
                "list=" + list +
                "guarList=" + guarList +
                '}';
    }
}
