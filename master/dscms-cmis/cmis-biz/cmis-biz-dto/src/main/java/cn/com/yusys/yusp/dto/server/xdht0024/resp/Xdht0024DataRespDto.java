package cn.com.yusys.yusp.dto.server.xdht0024.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：合同详情查看
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0024DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cncont_no")
    private String cncont_no;//中文合同号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "apply_cur_type")
    private String apply_cur_type;//申请币种
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//合同金额
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//合同开始日
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//合同结束日
    @JsonProperty(value = "reality_ir_y")
    private BigDecimal reality_ir_y;//利率
    @JsonProperty(value = "avg_rate")
    private BigDecimal avg_rate;//日息
    @JsonProperty(value = "lmt")
    private BigDecimal lmt;//可用额度
    @JsonProperty(value = "lpr_base_rate")
    private BigDecimal lpr_base_rate;//lpr基准利率
    @JsonProperty(value = "lpr_bp")
    private BigDecimal lpr_bp;//lprbp计算值
    @JsonProperty(value = "lpr_term")
    private String lpr_term;//lpr期限
    @JsonProperty(value = "lpr_bp_type")
    private String lpr_bp_type;//lpr类型
    @JsonProperty(value = "due_day")
    private String due_day;//还款日
    @JsonProperty(value = "valiDate")
    private String valiDate;//lpr利率生效日期
    @JsonProperty(value = "managerIdName")
    private String managerIdName;//经办人名称

    public String getCncont_no() {
        return cncont_no;
    }

    public void setCncont_no(String cncont_no) {
        this.cncont_no = cncont_no;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getApply_cur_type() {
        return apply_cur_type;
    }

    public void setApply_cur_type(String apply_cur_type) {
        this.apply_cur_type = apply_cur_type;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public BigDecimal getReality_ir_y() {
        return reality_ir_y;
    }

    public void setReality_ir_y(BigDecimal reality_ir_y) {
        this.reality_ir_y = reality_ir_y;
    }

    public BigDecimal getAvg_rate() {
        return avg_rate;
    }

    public void setAvg_rate(BigDecimal avg_rate) {
        this.avg_rate = avg_rate;
    }

    public BigDecimal getLmt() {
        return lmt;
    }

    public void setLmt(BigDecimal lmt) {
        this.lmt = lmt;
    }

    public BigDecimal getLpr_base_rate() {
        return lpr_base_rate;
    }

    public void setLpr_base_rate(BigDecimal lpr_base_rate) {
        this.lpr_base_rate = lpr_base_rate;
    }

    public BigDecimal getLpr_bp() {
        return lpr_bp;
    }

    public void setLpr_bp(BigDecimal lpr_bp) {
        this.lpr_bp = lpr_bp;
    }

    public String getLpr_term() {
        return lpr_term;
    }

    public void setLpr_term(String lpr_term) {
        this.lpr_term = lpr_term;
    }

    public String getLpr_bp_type() {
        return lpr_bp_type;
    }

    public void setLpr_bp_type(String lpr_bp_type) {
        this.lpr_bp_type = lpr_bp_type;
    }

    public String getDue_day() {
        return due_day;
    }

    public void setDue_day(String due_day) {
        this.due_day = due_day;
    }

    public String getValiDate() {
        return valiDate;
    }

    public void setValiDate(String valiDate) {
        this.valiDate = valiDate;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    @Override
    public String toString() {
        return "Xdht0024DataRespDto{" +
                "cncont_no='" + cncont_no + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", apply_cur_type='" + apply_cur_type + '\'' +
                ", apply_amount=" + apply_amount +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", reality_ir_y=" + reality_ir_y +
                ", avg_rate=" + avg_rate +
                ", lmt=" + lmt +
                ", lpr_base_rate=" + lpr_base_rate +
                ", lpr_bp=" + lpr_bp +
                ", lpr_term='" + lpr_term + '\'' +
                ", lpr_bp_type='" + lpr_bp_type + '\'' +
                ", due_day='" + due_day + '\'' +
                ", valiDate='" + valiDate + '\'' +
                ", managerIdName='" + managerIdName + '\'' +
                '}';
    }
}