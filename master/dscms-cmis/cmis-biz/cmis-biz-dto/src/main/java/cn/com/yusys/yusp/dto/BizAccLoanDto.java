package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * 对应cmis-biz-core中的acc_loan
 */
public class BizAccLoanDto implements Serializable{
    private static final long serialVersionUID = 1L;
    /** 业务类型，用于传输查询ACCLOAN的标记 **/
    private String bizType;
    /** 借据编号 **/
    private String billNo;

    /** 全局流水号 **/
    private String serno;

    /** 放款流水号 **/
    private String pvpSerno;

    /** 合同编号 **/
    private String contNo;

    /** 客户编号 **/
    private String cusId;

    /** 产品编号 **/
    private String prdId;

    /** 担保方式 **/
    private String guarWay;

    /** 还款方式 STD_ZB_REPAY_TYP **/
    private String repayWay;

    /** 停本付息期间 STD_ZB_PINT_TERM **/
    private String stopPintTerm;

    /** 还款间隔周期 STD_ZB_REPAY_TERM **/
    private String repayTerm;

    /** 还款间隔 STD_ZB_REPAY_SPACE **/
    private String repaySpace;

    /** 还款日 **/
    private String repayDate;

    /** 期限类型 STD_ZB_CHAGE_TYP **/
    private String termType;

    /** 申请期限 **/
    private java.math.BigDecimal appTerm;

    /** 是否涉及诉讼 **/
    private String isIvlLawsuit;

    /** 诉讼日期 **/
    private String lawsuitDate;

    /** 基准利率 **/
    private java.math.BigDecimal rulingIr;

    /** 执行年利率（年） **/
    private java.math.BigDecimal realityIrY;

    /** 逾期利率 **/
    private java.math.BigDecimal overdueRateY;

    /** 违约利率 **/
    private java.math.BigDecimal defaultRateY;

    /** 币种 STD_ZB_CUR_TYP **/
    private String curType;

    /** 贷款金额 **/
    private java.math.BigDecimal loanAmt;

    /** 贷款余额 **/
    private java.math.BigDecimal loanBalance;

    /** 逾期余额 **/
    private java.math.BigDecimal overdueBalance;

    /** 表内欠息 **/
    private java.math.BigDecimal innerOweInt;

    /** 表外欠息 **/
    private java.math.BigDecimal outOweInt;

    /** 应收利息累计 **/
    private java.math.BigDecimal recIntAccum;

    /** 实收利息累计 **/
    private java.math.BigDecimal recvIntAccum;

    /** 核销本金 **/
    private java.math.BigDecimal writeoffCap;

    /** 核销利息 **/
    private java.math.BigDecimal writeoffInt;

    /** 复利 **/
    private java.math.BigDecimal psCommOdInt;

    /** 罚息 **/
    private java.math.BigDecimal psOdInt;

    /** 展期次数 **/
    private java.math.BigDecimal postCount;

    /** 逾期期数 **/
    private java.math.BigDecimal overdue;

    /** 起始日期 **/
    private String startDate;

    /** 到期日期 **/
    private String endDate;

    /** 原到期日期 **/
    private String oldEndDate;

    /** 逾期日期 **/
    private String overdueDate;

    /** 结清日期 **/
    private String settlDate;

    /** 核销日期 **/
    private String writeoffDate;

    /** 五级分类 STD_ZB_FIVE_SORT **/
    private String fiveClass;

    /** 五级分类时间 **/
    private String fiveClassTime;

    /** 十二级分类 STD_ZB_TWELVE_SORT **/
    private String twelveClass;

    /** 十二级分类时间 **/
    private String twelveClassTime;

    /** 首次逾期日期 **/
    private String fstOverdueDate;

    /** 本金逾期日期 **/
    private String capOverdueDate;

    /** 利息逾期日期 **/
    private String intOverdueDate;

    /** 渠道来源 STD_ZB_CHNL_SOUR **/
    private String chnlSour;

    /** 主办人 **/
    private String managerId;

    /** 主办机构 **/
    private String managerBrId;

    /** 放款机构 **/
    private String acctBrId;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 台账状态 STD_ZB_ACC_TYP **/
    private String accStatus;

    /** 资产状态 STD_ZB_ASSET_STATUS **/
    private String assetStatus;
    /** 还款日确定规则 STD_ZB_REPAY_RULE **/
    private String repayRule;

    /** 还款日类型 STD_ZB_REPAY_DT_TYPE **/
    private String repayDtType;

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getRepayDtType() {
        return repayDtType;
    }

    public void setRepayDtType(String repayDtType) {
        this.repayDtType = repayDtType;
    }

    public String getRepayRule() {
        return repayRule;
    }

    public void setRepayRule(String repayRule) {
        this.repayRule = repayRule;
    }

    /**
     * @param billNo
     */
    public void setBillNo(String billNo) {
        this.billNo = billNo == null ? null : billNo.trim();
    }

    /**
     * @return BillNo
     */
    public String getBillNo() {
        return this.billNo;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno == null ? null : serno.trim();
    }

    /**
     * @return Serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param pvpSerno
     */
    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno == null ? null : pvpSerno.trim();
    }

    /**
     * @return PvpSerno
     */
    public String getPvpSerno() {
        return this.pvpSerno;
    }

    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo == null ? null : contNo.trim();
    }

    /**
     * @return ContNo
     */
    public String getContNo() {
        return this.contNo;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId == null ? null : cusId.trim();
    }

    /**
     * @return CusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param prdId
     */
    public void setPrdId(String prdId) {
        this.prdId = prdId == null ? null : prdId.trim();
    }

    /**
     * @return PrdId
     */
    public String getPrdId() {
        return this.prdId;
    }

    /**
     * @param guarWay
     */
    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay == null ? null : guarWay.trim();
    }

    /**
     * @return GuarWay
     */
    public String getGuarWay() {
        return this.guarWay;
    }

    /**
     * @param repayWay
     */
    public void setRepayWay(String repayWay) {
        this.repayWay = repayWay == null ? null : repayWay.trim();
    }

    /**
     * @return RepayWay
     */
    public String getRepayWay() {
        return this.repayWay;
    }

    /**
     * @param stopPintTerm
     */
    public void setStopPintTerm(String stopPintTerm) {
        this.stopPintTerm = stopPintTerm == null ? null : stopPintTerm.trim();
    }

    /**
     * @return StopPintTerm
     */
    public String getStopPintTerm() {
        return this.stopPintTerm;
    }

    /**
     * @param repayTerm
     */
    public void setRepayTerm(String repayTerm) {
        this.repayTerm = repayTerm == null ? null : repayTerm.trim();
    }

    /**
     * @return RepayTerm
     */
    public String getRepayTerm() {
        return this.repayTerm;
    }

    /**
     * @param repaySpace
     */
    public void setRepaySpace(String repaySpace) {
        this.repaySpace = repaySpace == null ? null : repaySpace.trim();
    }

    /**
     * @return RepaySpace
     */
    public String getRepaySpace() {
        return this.repaySpace;
    }

    /**
     * @param repayDate
     */
    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate == null ? null : repayDate.trim();
    }

    /**
     * @return RepayDate
     */
    public String getRepayDate() {
        return this.repayDate;
    }

    /**
     * @param termType
     */
    public void setTermType(String termType) {
        this.termType = termType == null ? null : termType.trim();
    }

    /**
     * @return TermType
     */
    public String getTermType() {
        return this.termType;
    }

    /**
     * @param appTerm
     */
    public void setAppTerm(java.math.BigDecimal appTerm) {
        this.appTerm = appTerm;
    }

    /**
     * @return AppTerm
     */
    public java.math.BigDecimal getAppTerm() {
        return this.appTerm;
    }

    /**
     * @param isIvlLawsuit
     */
    public void setIsIvlLawsuit(String isIvlLawsuit) {
        this.isIvlLawsuit = isIvlLawsuit == null ? null : isIvlLawsuit.trim();
    }

    /**
     * @return IsIvlLawsuit
     */
    public String getIsIvlLawsuit() {
        return this.isIvlLawsuit;
    }

    /**
     * @param lawsuitDate
     */
    public void setLawsuitDate(String lawsuitDate) {
        this.lawsuitDate = lawsuitDate == null ? null : lawsuitDate.trim();
    }

    /**
     * @return LawsuitDate
     */
    public String getLawsuitDate() {
        return this.lawsuitDate;
    }

    /**
     * @param rulingIr
     */
    public void setRulingIr(java.math.BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    /**
     * @return RulingIr
     */
    public java.math.BigDecimal getRulingIr() {
        return this.rulingIr;
    }

    /**
     * @param realityIrY
     */
    public void setRealityIrY(java.math.BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    /**
     * @return RealityIrY
     */
    public java.math.BigDecimal getRealityIrY() {
        return this.realityIrY;
    }

    /**
     * @param overdueRateY
     */
    public void setOverdueRateY(java.math.BigDecimal overdueRateY) {
        this.overdueRateY = overdueRateY;
    }

    /**
     * @return OverdueRateY
     */
    public java.math.BigDecimal getOverdueRateY() {
        return this.overdueRateY;
    }

    /**
     * @param defaultRateY
     */
    public void setDefaultRateY(java.math.BigDecimal defaultRateY) {
        this.defaultRateY = defaultRateY;
    }

    /**
     * @return DefaultRateY
     */
    public java.math.BigDecimal getDefaultRateY() {
        return this.defaultRateY;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType == null ? null : curType.trim();
    }

    /**
     * @return CurType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param loanAmt
     */
    public void setLoanAmt(java.math.BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    /**
     * @return LoanAmt
     */
    public java.math.BigDecimal getLoanAmt() {
        return this.loanAmt;
    }

    /**
     * @param loanBalance
     */
    public void setLoanBalance(java.math.BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    /**
     * @return LoanBalance
     */
    public java.math.BigDecimal getLoanBalance() {
        return this.loanBalance;
    }

    /**
     * @param overdueBalance
     */
    public void setOverdueBalance(java.math.BigDecimal overdueBalance) {
        this.overdueBalance = overdueBalance;
    }

    /**
     * @return OverdueBalance
     */
    public java.math.BigDecimal getOverdueBalance() {
        return this.overdueBalance;
    }

    /**
     * @param innerOweInt
     */
    public void setInnerOweInt(java.math.BigDecimal innerOweInt) {
        this.innerOweInt = innerOweInt;
    }

    /**
     * @return InnerOweInt
     */
    public java.math.BigDecimal getInnerOweInt() {
        return this.innerOweInt;
    }

    /**
     * @param outOweInt
     */
    public void setOutOweInt(java.math.BigDecimal outOweInt) {
        this.outOweInt = outOweInt;
    }

    /**
     * @return OutOweInt
     */
    public java.math.BigDecimal getOutOweInt() {
        return this.outOweInt;
    }

    /**
     * @param recIntAccum
     */
    public void setRecIntAccum(java.math.BigDecimal recIntAccum) {
        this.recIntAccum = recIntAccum;
    }

    /**
     * @return RecIntAccum
     */
    public java.math.BigDecimal getRecIntAccum() {
        return this.recIntAccum;
    }

    /**
     * @param recvIntAccum
     */
    public void setRecvIntAccum(java.math.BigDecimal recvIntAccum) {
        this.recvIntAccum = recvIntAccum;
    }

    /**
     * @return RecvIntAccum
     */
    public java.math.BigDecimal getRecvIntAccum() {
        return this.recvIntAccum;
    }

    /**
     * @param writeoffCap
     */
    public void setWriteoffCap(java.math.BigDecimal writeoffCap) {
        this.writeoffCap = writeoffCap;
    }

    /**
     * @return WriteoffCap
     */
    public java.math.BigDecimal getWriteoffCap() {
        return this.writeoffCap;
    }

    /**
     * @param writeoffInt
     */
    public void setWriteoffInt(java.math.BigDecimal writeoffInt) {
        this.writeoffInt = writeoffInt;
    }

    /**
     * @return WriteoffInt
     */
    public java.math.BigDecimal getWriteoffInt() {
        return this.writeoffInt;
    }

    /**
     * @param psCommOdInt
     */
    public void setPsCommOdInt(java.math.BigDecimal psCommOdInt) {
        this.psCommOdInt = psCommOdInt;
    }

    /**
     * @return PsCommOdInt
     */
    public java.math.BigDecimal getPsCommOdInt() {
        return this.psCommOdInt;
    }

    /**
     * @param psOdInt
     */
    public void setPsOdInt(java.math.BigDecimal psOdInt) {
        this.psOdInt = psOdInt;
    }

    /**
     * @return PsOdInt
     */
    public java.math.BigDecimal getPsOdInt() {
        return this.psOdInt;
    }

    /**
     * @param postCount
     */
    public void setPostCount(java.math.BigDecimal postCount) {
        this.postCount = postCount;
    }

    /**
     * @return PostCount
     */
    public java.math.BigDecimal getPostCount() {
        return this.postCount;
    }

    /**
     * @param overdue
     */
    public void setOverdue(java.math.BigDecimal overdue) {
        this.overdue = overdue;
    }

    /**
     * @return Overdue
     */
    public java.math.BigDecimal getOverdue() {
        return this.overdue;
    }

    /**
     * @param startDate
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate == null ? null : startDate.trim();
    }

    /**
     * @return StartDate
     */
    public String getStartDate() {
        return this.startDate;
    }

    /**
     * @param endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate == null ? null : endDate.trim();
    }

    /**
     * @return EndDate
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * @param oldEndDate
     */
    public void setOldEndDate(String oldEndDate) {
        this.oldEndDate = oldEndDate == null ? null : oldEndDate.trim();
    }

    /**
     * @return OldEndDate
     */
    public String getOldEndDate() {
        return this.oldEndDate;
    }

    /**
     * @param overdueDate
     */
    public void setOverdueDate(String overdueDate) {
        this.overdueDate = overdueDate == null ? null : overdueDate.trim();
    }

    /**
     * @return OverdueDate
     */
    public String getOverdueDate() {
        return this.overdueDate;
    }

    /**
     * @param settlDate
     */
    public void setSettlDate(String settlDate) {
        this.settlDate = settlDate == null ? null : settlDate.trim();
    }

    /**
     * @return SettlDate
     */
    public String getSettlDate() {
        return this.settlDate;
    }

    /**
     * @param writeoffDate
     */
    public void setWriteoffDate(String writeoffDate) {
        this.writeoffDate = writeoffDate == null ? null : writeoffDate.trim();
    }

    /**
     * @return WriteoffDate
     */
    public String getWriteoffDate() {
        return this.writeoffDate;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass == null ? null : fiveClass.trim();
    }

    /**
     * @return FiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param fiveClassTime
     */
    public void setFiveClassTime(String fiveClassTime) {
        this.fiveClassTime = fiveClassTime == null ? null : fiveClassTime.trim();
    }

    /**
     * @return FiveClassTime
     */
    public String getFiveClassTime() {
        return this.fiveClassTime;
    }

    /**
     * @param twelveClass
     */
    public void setTwelveClass(String twelveClass) {
        this.twelveClass = twelveClass == null ? null : twelveClass.trim();
    }

    /**
     * @return TwelveClass
     */
    public String getTwelveClass() {
        return this.twelveClass;
    }

    /**
     * @param twelveClassTime
     */
    public void setTwelveClassTime(String twelveClassTime) {
        this.twelveClassTime = twelveClassTime == null ? null : twelveClassTime.trim();
    }

    /**
     * @return TwelveClassTime
     */
    public String getTwelveClassTime() {
        return this.twelveClassTime;
    }

    /**
     * @param fstOverdueDate
     */
    public void setFstOverdueDate(String fstOverdueDate) {
        this.fstOverdueDate = fstOverdueDate == null ? null : fstOverdueDate.trim();
    }

    /**
     * @return FstOverdueDate
     */
    public String getFstOverdueDate() {
        return this.fstOverdueDate;
    }

    /**
     * @param capOverdueDate
     */
    public void setCapOverdueDate(String capOverdueDate) {
        this.capOverdueDate = capOverdueDate == null ? null : capOverdueDate.trim();
    }

    /**
     * @return CapOverdueDate
     */
    public String getCapOverdueDate() {
        return this.capOverdueDate;
    }

    /**
     * @param intOverdueDate
     */
    public void setIntOverdueDate(String intOverdueDate) {
        this.intOverdueDate = intOverdueDate == null ? null : intOverdueDate.trim();
    }

    /**
     * @return IntOverdueDate
     */
    public String getIntOverdueDate() {
        return this.intOverdueDate;
    }

    /**
     * @param chnlSour
     */
    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour == null ? null : chnlSour.trim();
    }

    /**
     * @return ChnlSour
     */
    public String getChnlSour() {
        return this.chnlSour;
    }

    /**
     * @param managerId
     */
    public void setManagerId(String managerId) {
        this.managerId = managerId == null ? null : managerId.trim();
    }

    /**
     * @return ManagerId
     */
    public String getManagerId() {
        return this.managerId;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId == null ? null : managerBrId.trim();
    }

    /**
     * @return ManagerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param acctBrId
     */
    public void setAcctBrId(String acctBrId) {
        this.acctBrId = acctBrId == null ? null : acctBrId.trim();
    }

    /**
     * @return AcctBrId
     */
    public String getAcctBrId() {
        return this.acctBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param accStatus
     */
    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus == null ? null : accStatus.trim();
    }

    /**
     * @return AccStatus
     */
    public String getAccStatus() {
        return this.accStatus;
    }

    /**
     * @param assetStatus
     */
    public void setAssetStatus(String assetStatus) {
        this.assetStatus = assetStatus == null ? null : assetStatus.trim();
    }

    /**
     * @return AssetStatus
     */
    public String getAssetStatus() {
        return this.assetStatus;
    }


}