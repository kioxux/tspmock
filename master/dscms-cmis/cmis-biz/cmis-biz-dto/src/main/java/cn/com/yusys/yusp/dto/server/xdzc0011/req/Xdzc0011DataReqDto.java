package cn.com.yusys.yusp.dto.server.xdzc0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：资产池超短贷放款接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0011DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//协议编号
    @JsonProperty(value = "billNo")
    private String billNo;//超短贷借据编号
    @JsonProperty(value = "billCnNo")
    private String billCnNo;//超短贷借据中文编号
    @JsonProperty(value = "repayMode")
    private String repayMode;//还款方式
    @JsonProperty(value = "dkyongtu")
    private String dkyongtu;//贷款用途
    @JsonProperty(value = "appAmt")
    private BigDecimal appAmt;//申请金额
    @JsonProperty(value = "tcontNo")
    private String tcontNo;//贸易合同编号
    @JsonProperty(value = "tcontImgId")
    private String tcontImgId;//贸易合同编号
    @JsonProperty(value = "startDate")
    private String startDate;//借据起始日
    @JsonProperty(value = "endDate")
    private String endDate;//借据到期日
    @JsonProperty(value = "loanTerm")
    private int loanTerm;//贷款期限
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构
    @JsonProperty(value = "cusAccNo")
    private String cusAccNo;//客户账户
    @JsonProperty(value = "billSerno")
    private String billSerno;//批次号
    @JsonProperty(value = "payAccNo")
    private String payAccNo;//受托支付账号
    @JsonProperty(value = "payAccName")
    private String payAccName;//受托支付账号名称
    @JsonProperty(value = "payOrgNo")
    private String payOrgNo;//受托支付开户行号
    @JsonProperty(value = "payOrgName")
    private String payOrgName;//受托支付开户行名称

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getBillCnNo() {
        return billCnNo;
    }

    public void setBillCnNo(String billCnNo) {
        this.billCnNo = billCnNo;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public BigDecimal getAppAmt() {
        return appAmt;
    }

    public void setAppAmt(BigDecimal appAmt) {
        this.appAmt = appAmt;
    }

    public String getTcontNo() {
        return tcontNo;
    }

    public void setTcontNo(String tcontNo) {
        this.tcontNo = tcontNo;
    }

    public String getTcontImgId() {
        return tcontImgId;
    }

    public void setTcontImgId(String tcontImgId) {
        this.tcontImgId = tcontImgId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(int loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getCusAccNo() {
        return cusAccNo;
    }

    public void setCusAccNo(String cusAccNo) {
        this.cusAccNo = cusAccNo;
    }

    public String getBillSerno() {
        return billSerno;
    }

    public void setBillSerno(String billSerno) {
        this.billSerno = billSerno;
    }

    public String getPayAccNo() {
        return payAccNo;
    }

    public void setPayAccNo(String payAccNo) {
        this.payAccNo = payAccNo;
    }

    public String getPayAccName() {
        return payAccName;
    }

    public void setPayAccName(String payAccName) {
        this.payAccName = payAccName;
    }

    public String getPayOrgNo() {
        return payOrgNo;
    }

    public void setPayOrgNo(String payOrgNo) {
        this.payOrgNo = payOrgNo;
    }

    public String getPayOrgName() {
        return payOrgName;
    }

    public void setPayOrgName(String payOrgName) {
        this.payOrgName = payOrgName;
    }

    @Override
    public String toString() {
        return "Xdzc0011DataReqDto{" +
                "contNo='" + contNo + '\'' +
                ", billNo='" + billNo + '\'' +
                ", billCnNo='" + billCnNo + '\'' +
                ", repayMode='" + repayMode + '\'' +
                ", dkyongtu='" + dkyongtu + '\'' +
                ", appAmt=" + appAmt +
                ", tcontNo='" + tcontNo + '\'' +
                ", tcontImgId='" + tcontImgId + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", loanTerm=" + loanTerm +
                ", finaBrId='" + finaBrId + '\'' +
                ", cusAccNo='" + cusAccNo + '\'' +
                ", billSerno='" + billSerno + '\'' +
                ", payAccNo='" + payAccNo + '\'' +
                ", payAccName='" + payAccName + '\'' +
                ", payOrgNo='" + payOrgNo + '\'' +
                ", payOrgName='" + payOrgName + '\'' +
                '}';
    }
}
