package cn.com.yusys.yusp.dto.server.xdxw0027.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：客户调查信息详情查看
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0027DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//调查流水号

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    @Override
    public String toString() {
        return "Xdxw0027DataReqDto{" +
                "indgtSerno='" + indgtSerno + '\'' +
                '}';
    }
}
