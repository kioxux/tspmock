package cn.com.yusys.yusp.dto.server.xdzc0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：资产池主动还款接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0014DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalQnt")
    private long totalQnt;// 总数
    @JsonProperty(value = "list")
    private   java.util.List<List> list;// list

    public long getTotalQnt() {
        return totalQnt;
    }

    public void setTotalQnt(long totalQnt) {
        this.totalQnt = totalQnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdzc0014DataRespDto{" +
                "totalQnt=" + totalQnt +
                ", list=" + list +
                '}';
    }
}
