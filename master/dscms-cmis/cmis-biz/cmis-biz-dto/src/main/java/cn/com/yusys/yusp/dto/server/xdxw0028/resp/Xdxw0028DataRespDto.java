package cn.com.yusys.yusp.dto.server.xdxw0028.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：根据客户号查询我行现有融资情况
 * @author xuchao
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0028DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "loanList")
	private List<LoanList> loanList;//start

	public List<LoanList> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<LoanList> loanList) {
		this.loanList = loanList;
	}

	@Override
	public String toString() {
		return "Xdxw0028DataRespDto{" +
				"loanList=" + loanList +
				'}';
	}
}  
