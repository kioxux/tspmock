package cn.com.yusys.yusp.dto.server.xdht0012.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：借款担保合同签订/支用
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class PvpList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pvpSerno")
    private String pvpSerno;//出账流水号
    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号
    @JsonProperty(value = "cnLoanContNo")
    private String cnLoanContNo;//中文合同编号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "curtPvpAmt")
    private BigDecimal curtPvpAmt;//本次出账金额
    @JsonProperty(value = "billStartDate")
    private String billStartDate;//借据起始日
    @JsonProperty(value = "billEndDate")
    private String billEndDate;//借据到期日
    @JsonProperty(value = "term")
    private String term;//期限
    @JsonProperty(value = "loanAcctNo")
    private String loanAcctNo;//贷款发放账户
    @JsonProperty(value = "loanAcctName")
    private String loanAcctName;//贷款账户名称
    @JsonProperty(value = "loanSubAcctNo")
    private String loanSubAcctNo;//贷款发放账户子账户序号
    @JsonProperty(value = "pvpStatus")
    private String pvpStatus;//出账状态
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//借款用途

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getCnLoanContNo() {
        return cnLoanContNo;
    }

    public void setCnLoanContNo(String cnLoanContNo) {
        this.cnLoanContNo = cnLoanContNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public BigDecimal getCurtPvpAmt() {
        return curtPvpAmt;
    }

    public void setCurtPvpAmt(BigDecimal curtPvpAmt) {
        this.curtPvpAmt = curtPvpAmt;
    }

    public String getBillStartDate() {
        return billStartDate;
    }

    public void setBillStartDate(String billStartDate) {
        this.billStartDate = billStartDate;
    }

    public String getBillEndDate() {
        return billEndDate;
    }

    public void setBillEndDate(String billEndDate) {
        this.billEndDate = billEndDate;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getLoanAcctNo() {
        return loanAcctNo;
    }

    public void setLoanAcctNo(String loanAcctNo) {
        this.loanAcctNo = loanAcctNo;
    }

    public String getLoanAcctName() {
        return loanAcctName;
    }

    public void setLoanAcctName(String loanAcctName) {
        this.loanAcctName = loanAcctName;
    }

    public String getLoanSubAcctNo() {
        return loanSubAcctNo;
    }

    public void setLoanSubAcctNo(String loanSubAcctNo) {
        this.loanSubAcctNo = loanSubAcctNo;
    }

    public String getPvpStatus() {
        return pvpStatus;
    }

    public void setPvpStatus(String pvpStatus) {
        this.pvpStatus = pvpStatus;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    @Override
    public String toString() {
        return "PvpList{" +
                "pvpSerno='" + pvpSerno + '\'' +
                "loanContNo='" + loanContNo + '\'' +
                "cnLoanContNo='" + cnLoanContNo + '\'' +
                "contType='" + contType + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "certNo='" + certNo + '\'' +
                "prdName='" + prdName + '\'' +
                "assureMeans='" + assureMeans + '\'' +
                "repayType='" + repayType + '\'' +
                "curtPvpAmt='" + curtPvpAmt + '\'' +
                "billStartDate='" + billStartDate + '\'' +
                "billEndDate='" + billEndDate + '\'' +
                "term='" + term + '\'' +
                "loanAcctNo='" + loanAcctNo + '\'' +
                "loanAcctName='" + loanAcctName + '\'' +
                "loanSubAcctNo='" + loanSubAcctNo + '\'' +
                "pvpStatus='" + pvpStatus + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                '}';
    }
}
