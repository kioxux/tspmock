package cn.com.yusys.yusp.dto.server.xdzc0019.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 20:49
 * @since 2021/6/8 20:49
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "assetType")
    private String assetType;//资产类别
    @JsonProperty(value = "assetNo")
    private String assetNo;//资产编号
    @JsonProperty(value = "assetAmt")
    private BigDecimal assetAmt;//资产金额
    @JsonProperty(value = "assetEndDate")
    private String assetEndDate;//资产到期日
    @JsonProperty(value = "assetStatus")
    private String assetStatus;//资产状态
    @JsonProperty(value = "tradeType")
    private String tradeType;//交易类型

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public BigDecimal getAssetAmt() {
        return assetAmt;
    }

    public void setAssetAmt(BigDecimal assetAmt) {
        this.assetAmt = assetAmt;
    }

    public String getAssetEndDate() {
        return assetEndDate;
    }

    public void setAssetEndDate(String assetEndDate) {
        this.assetEndDate = assetEndDate;
    }

    public String getAssetStatus() {
        return assetStatus;
    }

    public void setAssetStatus(String assetStatus) {
        this.assetStatus = assetStatus;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    @Override
    public String toString() {
        return "List{" +
                "assetType='" + assetType + '\'' +
                ", assetNo='" + assetNo + '\'' +
                ", assetAmt=" + assetAmt +
                ", assetEndDate='" + assetEndDate + '\'' +
                ", assetStatus='" + assetStatus + '\'' +
                ", tradeType='" + tradeType + '\'' +
                '}';
    }
}
