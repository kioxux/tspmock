package cn.com.yusys.yusp.dto.server.xddh0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：提前还款申请
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0016DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "repayMode")
    private String repayMode;//还款方式
    @JsonProperty(value = "repayModeDesc")
    private String repayModeDesc;//还款模式明细
    @JsonProperty(value = "repayAmt")
    private BigDecimal repayAmt;//还款金额
    @JsonProperty(value = "isPeriod")
    private String isPeriod;//是否缩期
    @JsonProperty(value = "periodTimes")
    private String periodTimes;//缩期期数
    @JsonProperty(value = "appResn")
    private String appResn;//申请理由

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getRepayModeDesc() {
        return repayModeDesc;
    }

    public void setRepayModeDesc(String repayModeDesc) {
        this.repayModeDesc = repayModeDesc;
    }

    public BigDecimal getRepayAmt() {
        return repayAmt;
    }

    public void setRepayAmt(BigDecimal repayAmt) {
        this.repayAmt = repayAmt;
    }

    public String getIsPeriod() {
        return isPeriod;
    }

    public void setIsPeriod(String isPeriod) {
        this.isPeriod = isPeriod;
    }

    public String getPeriodTimes() {
        return periodTimes;
    }

    public void setPeriodTimes(String periodTimes) {
        this.periodTimes = periodTimes;
    }

    public String getAppResn() {
        return appResn;
    }

    public void setAppResn(String appResn) {
        this.appResn = appResn;
    }

    @Override
    public String toString() {
        return "Xddh0016DataReqDto{" +
                "billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", repayMode='" + repayMode + '\'' +
                ", repayModeDesc='" + repayModeDesc + '\'' +
                ", repayAmt=" + repayAmt +
                ", isPeriod='" + isPeriod + '\'' +
                ", periodTimes='" + periodTimes + '\'' +
                ", appResn='" + appResn + '\'' +
                '}';
    }
}
