package cn.com.yusys.yusp.dto.server.xdxw0078.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：勘验任务查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0078DataRespDto implements Serializable {
    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @JsonProperty(value = "opFlag")
    private String opFlag;//操作标志

    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息
    @Override
    public String toString() {
        return "Xdxw0078DataRespDto{" +
                "opFlag=" + opFlag +
                "opMsg=" + opMsg +
                '}';
    }
}  
