package cn.com.yusys.yusp.dto.server.xdht0045.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Xdht0045DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "custId")
    private String custId;//客户号

    @JsonProperty(value = "certTp")
    private String certTp;//客户证件类型（私人）

    @JsonProperty(value = "certNo")
    private String certNo;//客户证件号（私人）

    @JsonProperty(value = "ywType")
    private String ywType;//业务类型

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCertTp() {
        return certTp;
    }

    public void setCertTp(String certTp) {
        this.certTp = certTp;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getYwType() {
        return ywType;
    }

    public void setYwType(String ywType) {
        this.ywType = ywType;
    }


    @Override
    public String toString() {
        return "Xdht0045RespDto{" +
                "custId='" + custId + '\'' +
                "certTp='" + certTp + '\'' +
                "certNo='" + certNo + '\'' +
                "ywType='" + ywType + '\'' +
                '}';
    }
}
