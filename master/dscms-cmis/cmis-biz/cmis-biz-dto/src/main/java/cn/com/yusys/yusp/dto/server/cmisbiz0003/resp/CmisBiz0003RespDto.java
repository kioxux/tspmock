package cn.com.yusys.yusp.dto.server.cmisbiz0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

public class CmisBiz0003RespDto implements Serializable {
    private static final long serialVersionUID = 8838692868807977406L;

    @JsonProperty(value = "errorCode")
    private String errorCode;//结果代码
    @JsonProperty(value = "errorMsg")
    private String errorMsg;//结果信息

    /**
     * 可用余额
     */
    @JsonProperty(value = "currMonthAllowLoanBalanceMap")
    private Map<String,CurrentMonthAllowLoanBlanceDto> currMonthAllowLoanBalanceMap;

    @JsonProperty(value = "totalCurrMonthAllowLoanBalance")
    private BigDecimal totalCurrMonthAllowLoanBalance;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Map<String, CurrentMonthAllowLoanBlanceDto> getCurrMonthAllowLoanBalanceMap() {
        return currMonthAllowLoanBalanceMap;
    }

    public void setCurrMonthAllowLoanBalanceMap(Map<String, CurrentMonthAllowLoanBlanceDto> currMonthAllowLoanBalanceMap) {
        this.currMonthAllowLoanBalanceMap = currMonthAllowLoanBalanceMap;
    }

    public BigDecimal getTotalCurrMonthAllowLoanBalance() {
        return totalCurrMonthAllowLoanBalance;
    }

    public void setTotalCurrMonthAllowLoanBalance(BigDecimal totalCurrMonthAllowLoanBalance) {
        this.totalCurrMonthAllowLoanBalance = totalCurrMonthAllowLoanBalance;
    }

    @Override
    public String toString() {
        return "CmisBiz0003RespDto{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", currMonthAllowLoanBalanceMap=" + currMonthAllowLoanBalanceMap +
                ", totalCurrMonthAllowLoanBalance=" + totalCurrMonthAllowLoanBalance +
                '}';
    }
}
