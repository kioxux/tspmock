package cn.com.yusys.yusp.dto.server.xdwb0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：小微电子签约借据号生成
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdwb0001DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bill_no")
    @NotNull(message = "借据号bill_no不能为空")
    @NotEmpty(message = "借据号bill_no不能为空")
    private String bill_no;//借据号

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    @Override
    public String toString() {
        return "Xdwb0001DataReqDto{" +
                "bill_no='" + bill_no + '\'' +
                '}';
    }
}
