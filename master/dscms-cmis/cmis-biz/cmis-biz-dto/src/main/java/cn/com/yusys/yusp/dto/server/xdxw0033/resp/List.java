package cn.com.yusys.yusp.dto.server.xdxw0033.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据流水号查询征信报告关联信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//客户调查主键
    @JsonProperty(value = "creditReportType")
    private String creditReportType;//征信报告类型
    @JsonProperty(value = "certCode")
    private String certCode;//身份证
    @JsonProperty(value = "creditReportPK")
    private String creditReportPK;//征信报告主键
    @JsonProperty(value = "relaDate")
    private String relaDate;//关联日期
    @JsonProperty(value = "cusName")
    private String cusName;//姓名

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getCreditReportType() {
        return creditReportType;
    }

    public void setCreditReportType(String creditReportType) {
        this.creditReportType = creditReportType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCreditReportPK() {
        return creditReportPK;
    }

    public void setCreditReportPK(String creditReportPK) {
        this.creditReportPK = creditReportPK;
    }

    public String getRelaDate() {
        return relaDate;
    }

    public void setRelaDate(String relaDate) {
        this.relaDate = relaDate;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    @Override
    public String toString() {
        return "List{" +
                "indgtSerno='" + indgtSerno + '\'' +
                ", creditReportType='" + creditReportType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", creditReportPK='" + creditReportPK + '\'' +
                ", relaDate='" + relaDate + '\'' +
                ", cusName='" + cusName + '\'' +
                '}';
    }
}
