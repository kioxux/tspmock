package cn.com.yusys.yusp.dto.server.xdtz0032.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：询客户所担保的行内当前贷款逾期件数
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0032DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "curtOverdueLoanCnt")
    private String curtOverdueLoanCnt;//所担保的行内当前贷款逾期件数

    public String getCurtOverdueLoanCnt() {
        return curtOverdueLoanCnt;
    }

    public void setCurtOverdueLoanCnt(String curtOverdueLoanCnt) {
        this.curtOverdueLoanCnt = curtOverdueLoanCnt;
    }

    @Override
    public String toString() {
        return "Xdtz0032DataRespDto{" +
                "curtOverdueLoanCnt='" + curtOverdueLoanCnt + '\'' +
                '}';
    }
}
