package cn.com.yusys.yusp.dto.server.xdxw0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：优企贷共借人、合同信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    private String contNo;//合同号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同号
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同开始日期
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同结束日期
    @JsonProperty(value = "contSignDate")
    private String contSignDate;//合同签订日期
    @JsonProperty(value = "debitName")
    private String debitName;//借款人姓名
    @JsonProperty(value = "debitCertNo")
    private String debitCertNo;//借款人证件号
    @JsonProperty(value = "loanPrd")
    private String loanPrd;//借款产品
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "contTerm")
    private String contTerm;//合同期限
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "signStatus")
    private String signStatus;//签约状态
    @JsonProperty(value = "taskSerno")
    private String taskSerno;//优企贷待办流水
    @JsonProperty(value = "rela")
    private String rela;//关系
    @JsonProperty(value = "otherRela")
    private String otherRela;//其他关系
    @JsonProperty(value = "commonDebitPhone")
    private String commonDebitPhone;//共借人电话号码
    @JsonProperty(value = "deliveryAddr")
    private String deliveryAddr;//送达地址

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getContSignDate() {
        return contSignDate;
    }

    public void setContSignDate(String contSignDate) {
        this.contSignDate = contSignDate;
    }

    public String getDebitName() {
        return debitName;
    }

    public void setDebitName(String debitName) {
        this.debitName = debitName;
    }

    public String getDebitCertNo() {
        return debitCertNo;
    }

    public void setDebitCertNo(String debitCertNo) {
        this.debitCertNo = debitCertNo;
    }

    public String getLoanPrd() {
        return loanPrd;
    }

    public void setLoanPrd(String loanPrd) {
        this.loanPrd = loanPrd;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getContTerm() {
        return contTerm;
    }

    public void setContTerm(String contTerm) {
        this.contTerm = contTerm;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(String signStatus) {
        this.signStatus = signStatus;
    }

    public String getTaskSerno() {
        return taskSerno;
    }

    public void setTaskSerno(String taskSerno) {
        this.taskSerno = taskSerno;
    }

    public String getRela() {
        return rela;
    }

    public void setRela(String rela) {
        this.rela = rela;
    }

    public String getOtherRela() {
        return otherRela;
    }

    public void setOtherRela(String otherRela) {
        this.otherRela = otherRela;
    }

    public String getCommonDebitPhone() {
        return commonDebitPhone;
    }

    public void setCommonDebitPhone(String commonDebitPhone) {
        this.commonDebitPhone = commonDebitPhone;
    }

    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", contStartDate='" + contStartDate + '\'' +
                ", contEndDate='" + contEndDate + '\'' +
                ", contSignDate='" + contSignDate + '\'' +
                ", debitName='" + debitName + '\'' +
                ", debitCertNo='" + debitCertNo + '\'' +
                ", loanPrd='" + loanPrd + '\'' +
                ", contAmt=" + contAmt +
                ", contTerm='" + contTerm + '\'' +
                ", contStatus='" + contStatus + '\'' +
                ", signStatus='" + signStatus + '\'' +
                ", taskSerno='" + taskSerno + '\'' +
                ", rela='" + rela + '\'' +
                ", otherRela='" + otherRela + '\'' +
                ", commonDebitPhone='" + commonDebitPhone + '\'' +
                ", deliveryAddr='" + deliveryAddr + '\'' +
                '}';
    }
}
