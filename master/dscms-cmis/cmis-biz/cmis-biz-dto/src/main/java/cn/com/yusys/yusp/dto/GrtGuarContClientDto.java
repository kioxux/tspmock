package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class GrtGuarContClientDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 担保合同流水号
     **/
    private String guarPkId;

    /**
     * 客户编号
     **/
    private String cusId;

    /**
     * 担保人名称
     **/
    private String assureName;
    /**
     * 押品统一编号
     **/
    private String guarNo;
    /**
     * 担保合同编号
     **/
    private String guarContNo;
    /**
     * 中文合同编号
     **/
    private String guarContCnNo;
    /**
     * 担保合同类型
     **/
    private String guarContType;
    /**
     * 担保方式
     **/
    private String guarWay;
    /**
     * 保证形式
     **/
    private String assureModal;
    /**
     * 借款人编号
     **/
    private String borrowerId;
    /**
     * 担保方式 - 资产保全
     **/
    private String guarMode;

    /**
     * 担保金额
     **/
    private java.math.BigDecimal guarAmt;

    /**
     * 担保起始日
     **/
    private String guarStartDate;

    /**
     * 担保到期日
     **/
    private String guarEndDate;

    /**
     * 担保状态
     **/
    private String guarContState;

    /**
     * 合同编号
     **/
    private String contNo;

    /**
     * 是否浮动抵押
     **/
    private String isFloatPld;

    /**
     * 质押合同类型
     **/
    private String pldContType;

    public String getGuarPkId() {
        return guarPkId;
    }

	public String getGuarMode() {
		return guarMode;
	}

	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	public void setGuarPkId(String guarPkId) {
        this.guarPkId = guarPkId;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getGuarContNo() {
        return guarContNo;
    }

    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    public String getGuarContCnNo() {
        return guarContCnNo;
    }

    public void setGuarContCnNo(String guarContCnNo) {
        this.guarContCnNo = guarContCnNo;
    }

    public String getGuarContType() {
        return guarContType;
    }

    public void setGuarContType(String guarContType) {
        this.guarContType = guarContType;
    }

    public String getGuarWay() {
        return guarWay;
    }

    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay;
    }

    public String getAssureModal() {
        return assureModal;
    }

    public void setAssureModal(String assureModal) {
        this.assureModal = assureModal;
    }

    public String getBorrowerId() {
        return borrowerId;
    }

    public void setBorrowerId(String borrowerId) {
        this.borrowerId = borrowerId;
    }

    public BigDecimal getGuarAmt() {
        return guarAmt;
    }

    public String getGuarStartDate() {
        return guarStartDate;
    }

    public String getGuarEndDate() {
        return guarEndDate;
    }

    public String getGuarContState() {
        return guarContState;
    }

    public void setGuarAmt(BigDecimal guarAmt) {
        this.guarAmt = guarAmt;
    }

    public void setGuarStartDate(String guarStartDate) {
        this.guarStartDate = guarStartDate;
    }

    public void setGuarEndDate(String guarEndDate) {
        this.guarEndDate = guarEndDate;
    }

    public void setGuarContState(String guarContState) {
        this.guarContState = guarContState;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setIsFloatPld(String isFloatPld) {
        this.isFloatPld = isFloatPld;
    }

    public String getIsFloatPld() {
        return isFloatPld;
    }

    public void setPldContType(String pldContType) {
        this.pldContType = pldContType;
    }

    public String getPldContType() {
        return pldContType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getAssureName() {
        return assureName;
    }

    public void setAssureName(String assureName) {
        this.assureName = assureName;
    }
}