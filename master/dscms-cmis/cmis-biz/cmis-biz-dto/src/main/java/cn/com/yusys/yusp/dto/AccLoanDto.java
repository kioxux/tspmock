package cn.com.yusys.yusp.dto;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoan
 * @类描述: acc_loan数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-19 15:34:59
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */

public class AccLoanDto implements Serializable{
    private static final long serialVersionUID = 1L;

    /** 主键 **/
    private String pkId;

    /** 放款流水号 **/
    private String pvpSerno;

    /** 逾期余额 **/
    private java.math.BigDecimal overdueBalance;

    /** 基准利率 **/
    private java.math.BigDecimal rulingIr;

    /** 借据编号 **/
    private String billNo;

    /** 合同编号 **/
    private String contNo;

    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 产品编号 **/
    private String prdId;

    /** 产品名称 **/
    private String prdName;

    /** 产品类型属性 **/
    private String prdTypeProp;

    /** 担保方式 **/
    private String guarMode;

    /** 贷款形式 **/
    private String loanModal;

    /** 贷款发放币种 **/
    private String contCurType;

    /** 汇率 **/
    private java.math.BigDecimal exchangeRate;

    /** 贷款金额 **/
    private java.math.BigDecimal loanAmt;

    /** 贷款余额 **/
    private java.math.BigDecimal loanBalance;

    /** 折合人民币金额 **/
    private java.math.BigDecimal exchangeRmbAmt;

    /** 折合人民币余额 **/
    private java.math.BigDecimal exchangeRmbBal;

    /** 贷款起始日 **/
    private String loanStartDate;

    /** 贷款到期日 **/
    private String loanEndDate;

    /** 贷款期限 **/
    private String loanTerm;

    /** 贷款期限单位 **/
    private String loanTermUnit;

    /** 展期次数 **/
    private String extTimes;

    /** 逾期天数 **/
    private Integer overdueDay;

    /** 逾期期数 **/
    private String overdueTimes;

    /** 结清日期 **/
    private String settlDate;

    /** 利率调整方式 **/
    private String rateAdjMode;

    /** 是否分段计息 **/
    private String isSegInterest;

    /** LPR授信利率区间 **/
    private String lprRateIntval;

    /** 当前LPR利率 **/
    private java.math.BigDecimal curtLprRate;

    /** 浮动点数 **/
    private java.math.BigDecimal rateFloatPoint;

    /** 执行年利率 **/
    private java.math.BigDecimal execRateYear;

    /** 逾期利率浮动比 **/
    private java.math.BigDecimal overdueRatePefloat;

    /** 逾期执行利率(年利率) **/
    private java.math.BigDecimal overdueExecRate;

    /** 复息利率浮动比 **/
    private java.math.BigDecimal ciRatePefloat;

    /** 复息执行利率(年利率) **/
    private java.math.BigDecimal ciExecRate;

    /** 利率调整选项 **/
    private String rateAdjType;

    /** 下一次利率调整间隔 **/
    private String nextRateAdjInterval;

    /** 下一次利率调整间隔单位 **/
    private String nextRateAdjUnit;

    /** 第一次调整日 **/
    private String firstAdjDate;

    /** 还款方式 **/
    private String repayMode;

    /** 结息间隔周期 **/
    private String eiIntervalCycle;

    /** 结息间隔周期单位 **/
    private String eiIntervalUnit;

    /** 扣款方式 **/
    private String deductType;

    /** 扣款日 **/
    private String deductDay;

    /** 贷款发放账号 **/
    private String loanPayoutAccno;

    /** 贷款发放账号子序号 **/
    private String loanPayoutSubNo;

    /** 发放账号名称 **/
    private String payoutAcctName;

    /** 是否受托支付 **/
    private String isBeEntrustedPay;

    /** 贷款还款账号 **/
    private String repayAccno;

    /** 贷款还款账户子序号 **/
    private String repaySubAccno;

    /** 还款账户名称 **/
    private String repayAcctName;

    /** 贷款投向 **/
    private String loanTer;

    /** 借款用途类型 **/
    private String loanUseType;

    /** 科目号 **/
    private String subjectNo;

    /** 科目名称 **/
    private String subjectName;

    /** 农户类型 **/
    private String agriType;

    /** 涉农贷款投向 **/
    private String agriLoanTer;

    /** 贷款承诺标志 **/
    private String loanPromiseFlag;

    /** 贷款承诺类型 **/
    private String loanPromiseType;

    /** 是否贴息 **/
    private String isSbsy;

    /** 贴息人存款账号 **/
    private String sbsyDepAccno;

    /** 贴息比例 **/
    private java.math.BigDecimal sbsyPerc;

    /** 贴息到期日 **/
    private String sbysEnddate;

    /** 是否使用授信额度 **/
    private String isUtilLmt;

    /** 授信额度编号 **/
    private String lmtAccNo;

    /** 批复编号 **/
    private String replyNo;

    /** 借新还旧标识 **/
    private String refinancingFlag;

    /** 展期类型 **/
    private String extType;

    /** 贷款类别细分 **/
    private String loanTypeDetail;

    /** 是否落实贷款 **/
    private String isPactLoan;

    /** 是否绿色产业 **/
    private String isGreenIndustry;

    /** 是否经营性物业贷款 **/
    private String isOperPropertyLoan;

    /** 是否钢贸行业贷款 **/
    private String isSteelLoan;

    /** 是否不锈钢行业贷款 **/
    private String isStainlessLoan;

    /** 是否扶贫贴息贷款 **/
    private String isPovertyReliefLoan;

    /** 是否劳动密集型小企业贴息贷款 **/
    private String isLaborIntenSbsyLoan;

    /** 保障性安居工程贷款 **/
    private String goverSubszHouseLoan;

    /** 项目贷款节能环保 **/
    private String engyEnviProteLoan;

    /** 是否农村综合开发贷款标志 **/
    private String isCphsRurDelpLoan;

    /** 房地产贷款 **/
    private String realproLoan;

    /** 房产开发贷款资本金比例 **/
    private String realproLoanRate;

    /** 担保方式细分 **/
    private String guarDetailMode;

    /** 账务机构编号 **/
    private String finaBrId;

    /** 账务机构名称 **/
    private String finaBrIdName;

    /** 放款机构编号 **/
    private String disbOrgNo;

    /** 放款机构名称 **/
    private String disbOrgName;

    /** 五级分类 **/
    private String fiveClass;

    /** 十级分类 **/
    private String tenClass;

    /** 分类日期 **/
    private String classDate;

    /** 台账状态 **/
    private String accStatus;

    /** 操作类型 **/
    private String oprType;

    /** 所属条线 **/
    private String belgLine;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最近修改人 **/
    private String updId;

    /** 最近修改机构 **/
    private String updBrId;

    /** 最近修改日期 **/
    private String updDate;

    /** 主管客户经理 **/
    private String managerId;

    /** 主管机构 **/
    private String managerBrId;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    /** 正常本金 **/
    private java.math.BigDecimal zcbjAmt;

    /** 逾期本金 **/
    private java.math.BigDecimal overdueCapAmt;

    /** 欠息 **/
    private java.math.BigDecimal debitInt;

    /** 罚息 **/
    private java.math.BigDecimal penalInt;

    /** 复息 **/
    private java.math.BigDecimal compoundInt;

    /** 核销本金 **/
    private java.math.BigDecimal totalHxbjAmt;

    /** 核销利息 **/
    private java.math.BigDecimal totalHxlxAmt;

    /** 正常利率浮动方式 **/
    private String irFloatType;

    /** 利率浮动百分比 **/
    private java.math.BigDecimal irFloatRate;

    /** 是否省心E付(STD_ZB_YES_NO) **/
    private String isSxef;

    /** 垫款原借据号 **/
    private String advanceBillNo;

    /** 垫款类型 **/
    private String advanceType;

    /** 债项等级 **/
    private String debtLevel;

    /** 违约损失率LGD **/
    private java.math.BigDecimal lgd;

    /** 违约风险暴露EAD **/
    private java.math.BigDecimal ead;

    /** 其他借款用途 **/
    private String otherLoanPurp;

    /** 贷款标识 **/
    private String loanFlag;

    /** 是否资产池 **/
    private String isPool;

    /** 原到期日期 **/
    private String origExpiDate;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public BigDecimal getOverdueBalance() {
        return overdueBalance;
    }

    public void setOverdueBalance(BigDecimal overdueBalance) {
        this.overdueBalance = overdueBalance;
    }

    public BigDecimal getRulingIr() {
        return rulingIr;
    }

    public void setRulingIr(BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getPrdTypeProp() {
        return prdTypeProp;
    }

    public void setPrdTypeProp(String prdTypeProp) {
        this.prdTypeProp = prdTypeProp;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getContCurType() {
        return contCurType;
    }

    public void setContCurType(String contCurType) {
        this.contCurType = contCurType;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getExchangeRmbAmt() {
        return exchangeRmbAmt;
    }

    public void setExchangeRmbAmt(BigDecimal exchangeRmbAmt) {
        this.exchangeRmbAmt = exchangeRmbAmt;
    }

    public BigDecimal getExchangeRmbBal() {
        return exchangeRmbBal;
    }

    public void setExchangeRmbBal(BigDecimal exchangeRmbBal) {
        this.exchangeRmbBal = exchangeRmbBal;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getLoanTermUnit() {
        return loanTermUnit;
    }

    public void setLoanTermUnit(String loanTermUnit) {
        this.loanTermUnit = loanTermUnit;
    }

    public String getExtTimes() {
        return extTimes;
    }

    public void setExtTimes(String extTimes) {
        this.extTimes = extTimes;
    }

    public Integer getOverdueDay() {
        return overdueDay;
    }

    public void setOverdueDay(Integer overdueDay) {
        this.overdueDay = overdueDay;
    }

    public String getOverdueTimes() {
        return overdueTimes;
    }

    public void setOverdueTimes(String overdueTimes) {
        this.overdueTimes = overdueTimes;
    }

    public String getSettlDate() {
        return settlDate;
    }

    public void setSettlDate(String settlDate) {
        this.settlDate = settlDate;
    }

    public String getRateAdjMode() {
        return rateAdjMode;
    }

    public void setRateAdjMode(String rateAdjMode) {
        this.rateAdjMode = rateAdjMode;
    }

    public String getIsSegInterest() {
        return isSegInterest;
    }

    public void setIsSegInterest(String isSegInterest) {
        this.isSegInterest = isSegInterest;
    }

    public String getLprRateIntval() {
        return lprRateIntval;
    }

    public void setLprRateIntval(String lprRateIntval) {
        this.lprRateIntval = lprRateIntval;
    }

    public BigDecimal getCurtLprRate() {
        return curtLprRate;
    }

    public void setCurtLprRate(BigDecimal curtLprRate) {
        this.curtLprRate = curtLprRate;
    }

    public BigDecimal getRateFloatPoint() {
        return rateFloatPoint;
    }

    public void setRateFloatPoint(BigDecimal rateFloatPoint) {
        this.rateFloatPoint = rateFloatPoint;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public BigDecimal getOverdueRatePefloat() {
        return overdueRatePefloat;
    }

    public void setOverdueRatePefloat(BigDecimal overdueRatePefloat) {
        this.overdueRatePefloat = overdueRatePefloat;
    }

    public BigDecimal getOverdueExecRate() {
        return overdueExecRate;
    }

    public void setOverdueExecRate(BigDecimal overdueExecRate) {
        this.overdueExecRate = overdueExecRate;
    }

    public BigDecimal getCiRatePefloat() {
        return ciRatePefloat;
    }

    public void setCiRatePefloat(BigDecimal ciRatePefloat) {
        this.ciRatePefloat = ciRatePefloat;
    }

    public BigDecimal getCiExecRate() {
        return ciExecRate;
    }

    public void setCiExecRate(BigDecimal ciExecRate) {
        this.ciExecRate = ciExecRate;
    }

    public String getRateAdjType() {
        return rateAdjType;
    }

    public void setRateAdjType(String rateAdjType) {
        this.rateAdjType = rateAdjType;
    }

    public String getNextRateAdjInterval() {
        return nextRateAdjInterval;
    }

    public void setNextRateAdjInterval(String nextRateAdjInterval) {
        this.nextRateAdjInterval = nextRateAdjInterval;
    }

    public String getNextRateAdjUnit() {
        return nextRateAdjUnit;
    }

    public void setNextRateAdjUnit(String nextRateAdjUnit) {
        this.nextRateAdjUnit = nextRateAdjUnit;
    }

    public String getFirstAdjDate() {
        return firstAdjDate;
    }

    public void setFirstAdjDate(String firstAdjDate) {
        this.firstAdjDate = firstAdjDate;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getEiIntervalCycle() {
        return eiIntervalCycle;
    }

    public void setEiIntervalCycle(String eiIntervalCycle) {
        this.eiIntervalCycle = eiIntervalCycle;
    }

    public String getEiIntervalUnit() {
        return eiIntervalUnit;
    }

    public void setEiIntervalUnit(String eiIntervalUnit) {
        this.eiIntervalUnit = eiIntervalUnit;
    }

    public String getDeductType() {
        return deductType;
    }

    public void setDeductType(String deductType) {
        this.deductType = deductType;
    }

    public String getDeductDay() {
        return deductDay;
    }

    public void setDeductDay(String deductDay) {
        this.deductDay = deductDay;
    }

    public String getLoanPayoutAccno() {
        return loanPayoutAccno;
    }

    public void setLoanPayoutAccno(String loanPayoutAccno) {
        this.loanPayoutAccno = loanPayoutAccno;
    }

    public String getLoanPayoutSubNo() {
        return loanPayoutSubNo;
    }

    public void setLoanPayoutSubNo(String loanPayoutSubNo) {
        this.loanPayoutSubNo = loanPayoutSubNo;
    }

    public String getPayoutAcctName() {
        return payoutAcctName;
    }

    public void setPayoutAcctName(String payoutAcctName) {
        this.payoutAcctName = payoutAcctName;
    }

    public String getIsBeEntrustedPay() {
        return isBeEntrustedPay;
    }

    public void setIsBeEntrustedPay(String isBeEntrustedPay) {
        this.isBeEntrustedPay = isBeEntrustedPay;
    }

    public String getRepayAccno() {
        return repayAccno;
    }

    public void setRepayAccno(String repayAccno) {
        this.repayAccno = repayAccno;
    }

    public String getRepaySubAccno() {
        return repaySubAccno;
    }

    public void setRepaySubAccno(String repaySubAccno) {
        this.repaySubAccno = repaySubAccno;
    }

    public String getRepayAcctName() {
        return repayAcctName;
    }

    public void setRepayAcctName(String repayAcctName) {
        this.repayAcctName = repayAcctName;
    }

    public String getLoanTer() {
        return loanTer;
    }

    public void setLoanTer(String loanTer) {
        this.loanTer = loanTer;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getSubjectNo() {
        return subjectNo;
    }

    public void setSubjectNo(String subjectNo) {
        this.subjectNo = subjectNo;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getAgriType() {
        return agriType;
    }

    public void setAgriType(String agriType) {
        this.agriType = agriType;
    }

    public String getAgriLoanTer() {
        return agriLoanTer;
    }

    public void setAgriLoanTer(String agriLoanTer) {
        this.agriLoanTer = agriLoanTer;
    }

    public String getLoanPromiseFlag() {
        return loanPromiseFlag;
    }

    public void setLoanPromiseFlag(String loanPromiseFlag) {
        this.loanPromiseFlag = loanPromiseFlag;
    }

    public String getLoanPromiseType() {
        return loanPromiseType;
    }

    public void setLoanPromiseType(String loanPromiseType) {
        this.loanPromiseType = loanPromiseType;
    }

    public String getIsSbsy() {
        return isSbsy;
    }

    public void setIsSbsy(String isSbsy) {
        this.isSbsy = isSbsy;
    }

    public String getSbsyDepAccno() {
        return sbsyDepAccno;
    }

    public void setSbsyDepAccno(String sbsyDepAccno) {
        this.sbsyDepAccno = sbsyDepAccno;
    }

    public BigDecimal getSbsyPerc() {
        return sbsyPerc;
    }

    public void setSbsyPerc(BigDecimal sbsyPerc) {
        this.sbsyPerc = sbsyPerc;
    }

    public String getSbysEnddate() {
        return sbysEnddate;
    }

    public void setSbysEnddate(String sbysEnddate) {
        this.sbysEnddate = sbysEnddate;
    }

    public String getIsUtilLmt() {
        return isUtilLmt;
    }

    public void setIsUtilLmt(String isUtilLmt) {
        this.isUtilLmt = isUtilLmt;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    public String getReplyNo() {
        return replyNo;
    }

    public void setReplyNo(String replyNo) {
        this.replyNo = replyNo;
    }

    public String getRefinancingFlag() {
        return refinancingFlag;
    }

    public void setRefinancingFlag(String refinancingFlag) {
        this.refinancingFlag = refinancingFlag;
    }

    public String getExtType() {
        return extType;
    }

    public void setExtType(String extType) {
        this.extType = extType;
    }

    public String getLoanTypeDetail() {
        return loanTypeDetail;
    }

    public void setLoanTypeDetail(String loanTypeDetail) {
        this.loanTypeDetail = loanTypeDetail;
    }

    public String getIsPactLoan() {
        return isPactLoan;
    }

    public void setIsPactLoan(String isPactLoan) {
        this.isPactLoan = isPactLoan;
    }

    public String getIsGreenIndustry() {
        return isGreenIndustry;
    }

    public void setIsGreenIndustry(String isGreenIndustry) {
        this.isGreenIndustry = isGreenIndustry;
    }

    public String getIsOperPropertyLoan() {
        return isOperPropertyLoan;
    }

    public void setIsOperPropertyLoan(String isOperPropertyLoan) {
        this.isOperPropertyLoan = isOperPropertyLoan;
    }

    public String getIsSteelLoan() {
        return isSteelLoan;
    }

    public void setIsSteelLoan(String isSteelLoan) {
        this.isSteelLoan = isSteelLoan;
    }

    public String getIsStainlessLoan() {
        return isStainlessLoan;
    }

    public void setIsStainlessLoan(String isStainlessLoan) {
        this.isStainlessLoan = isStainlessLoan;
    }

    public String getIsPovertyReliefLoan() {
        return isPovertyReliefLoan;
    }

    public void setIsPovertyReliefLoan(String isPovertyReliefLoan) {
        this.isPovertyReliefLoan = isPovertyReliefLoan;
    }

    public String getIsLaborIntenSbsyLoan() {
        return isLaborIntenSbsyLoan;
    }

    public void setIsLaborIntenSbsyLoan(String isLaborIntenSbsyLoan) {
        this.isLaborIntenSbsyLoan = isLaborIntenSbsyLoan;
    }

    public String getGoverSubszHouseLoan() {
        return goverSubszHouseLoan;
    }

    public void setGoverSubszHouseLoan(String goverSubszHouseLoan) {
        this.goverSubszHouseLoan = goverSubszHouseLoan;
    }

    public String getEngyEnviProteLoan() {
        return engyEnviProteLoan;
    }

    public void setEngyEnviProteLoan(String engyEnviProteLoan) {
        this.engyEnviProteLoan = engyEnviProteLoan;
    }

    public String getIsCphsRurDelpLoan() {
        return isCphsRurDelpLoan;
    }

    public void setIsCphsRurDelpLoan(String isCphsRurDelpLoan) {
        this.isCphsRurDelpLoan = isCphsRurDelpLoan;
    }

    public String getRealproLoan() {
        return realproLoan;
    }

    public void setRealproLoan(String realproLoan) {
        this.realproLoan = realproLoan;
    }

    public String getRealproLoanRate() {
        return realproLoanRate;
    }

    public void setRealproLoanRate(String realproLoanRate) {
        this.realproLoanRate = realproLoanRate;
    }

    public String getGuarDetailMode() {
        return guarDetailMode;
    }

    public void setGuarDetailMode(String guarDetailMode) {
        this.guarDetailMode = guarDetailMode;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getFinaBrIdName() {
        return finaBrIdName;
    }

    public void setFinaBrIdName(String finaBrIdName) {
        this.finaBrIdName = finaBrIdName;
    }

    public String getDisbOrgNo() {
        return disbOrgNo;
    }

    public void setDisbOrgNo(String disbOrgNo) {
        this.disbOrgNo = disbOrgNo;
    }

    public String getDisbOrgName() {
        return disbOrgName;
    }

    public void setDisbOrgName(String disbOrgName) {
        this.disbOrgName = disbOrgName;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getTenClass() {
        return tenClass;
    }

    public void setTenClass(String tenClass) {
        this.tenClass = tenClass;
    }

    public String getClassDate() {
        return classDate;
    }

    public void setClassDate(String classDate) {
        this.classDate = classDate;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public BigDecimal getZcbjAmt() {
        return zcbjAmt;
    }

    public void setZcbjAmt(BigDecimal zcbjAmt) {
        this.zcbjAmt = zcbjAmt;
    }

    public BigDecimal getOverdueCapAmt() {
        return overdueCapAmt;
    }

    public void setOverdueCapAmt(BigDecimal overdueCapAmt) {
        this.overdueCapAmt = overdueCapAmt;
    }

    public BigDecimal getDebitInt() {
        return debitInt;
    }

    public void setDebitInt(BigDecimal debitInt) {
        this.debitInt = debitInt;
    }

    public BigDecimal getPenalInt() {
        return penalInt;
    }

    public void setPenalInt(BigDecimal penalInt) {
        this.penalInt = penalInt;
    }

    public BigDecimal getCompoundInt() {
        return compoundInt;
    }

    public void setCompoundInt(BigDecimal compoundInt) {
        this.compoundInt = compoundInt;
    }

    public BigDecimal getTotalHxbjAmt() {
        return totalHxbjAmt;
    }

    public void setTotalHxbjAmt(BigDecimal totalHxbjAmt) {
        this.totalHxbjAmt = totalHxbjAmt;
    }

    public BigDecimal getTotalHxlxAmt() {
        return totalHxlxAmt;
    }

    public void setTotalHxlxAmt(BigDecimal totalHxlxAmt) {
        this.totalHxlxAmt = totalHxlxAmt;
    }

    public String getIrFloatType() {
        return irFloatType;
    }

    public void setIrFloatType(String irFloatType) {
        this.irFloatType = irFloatType;
    }

    public BigDecimal getIrFloatRate() {
        return irFloatRate;
    }

    public void setIrFloatRate(BigDecimal irFloatRate) {
        this.irFloatRate = irFloatRate;
    }

    public String getIsSxef() {
        return isSxef;
    }

    public void setIsSxef(String isSxef) {
        this.isSxef = isSxef;
    }

    public String getAdvanceBillNo() {
        return advanceBillNo;
    }

    public void setAdvanceBillNo(String advanceBillNo) {
        this.advanceBillNo = advanceBillNo;
    }

    public String getAdvanceType() {
        return advanceType;
    }

    public void setAdvanceType(String advanceType) {
        this.advanceType = advanceType;
    }

    public String getDebtLevel() {
        return debtLevel;
    }

    public void setDebtLevel(String debtLevel) {
        this.debtLevel = debtLevel;
    }

    public BigDecimal getLgd() {
        return lgd;
    }

    public void setLgd(BigDecimal lgd) {
        this.lgd = lgd;
    }

    public BigDecimal getEad() {
        return ead;
    }

    public void setEad(BigDecimal ead) {
        this.ead = ead;
    }

    public String getOtherLoanPurp() {
        return otherLoanPurp;
    }

    public void setOtherLoanPurp(String otherLoanPurp) {
        this.otherLoanPurp = otherLoanPurp;
    }

    public String getLoanFlag() {
        return loanFlag;
    }

    public void setLoanFlag(String loanFlag) {
        this.loanFlag = loanFlag;
    }

    public String getIsPool() {
        return isPool;
    }

    public void setIsPool(String isPool) {
        this.isPool = isPool;
    }

    public String getOrigExpiDate() {
        return origExpiDate;
    }

    public void setOrigExpiDate(String origExpiDate) {
        this.origExpiDate = origExpiDate;
    }
}