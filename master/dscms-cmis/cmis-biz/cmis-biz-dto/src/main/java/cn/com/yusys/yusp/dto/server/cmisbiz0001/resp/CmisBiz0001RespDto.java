package cn.com.yusys.yusp.dto.server.cmisbiz0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 单一客户授信复审
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisBiz0001RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 申请流水号
     **/
    @JsonProperty(value = "serno")
    private String serno;
    /**
     * 返回码
     **/
    @JsonProperty(value = "rtnCode")
    private String rtnCode;

    /**
     * 返回消息
     **/
    @JsonProperty(value = "rtnMsg")
    private String rtnMsg;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    @Override
    public String toString() {
        return "CmisBiz0001RespDto{" +
                "serno='" + serno + '\'' +
                ", rtnCode='" + rtnCode + '\'' +
                ", rtnMsg='" + rtnMsg + '\'' +
                '}';
    }
}
