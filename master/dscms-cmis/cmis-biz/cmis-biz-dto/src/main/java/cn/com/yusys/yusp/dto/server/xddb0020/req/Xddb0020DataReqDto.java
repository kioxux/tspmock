package cn.com.yusys.yusp.dto.server.xddb0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：根据客户名查询抵押物类型
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0020DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_name")
    @NotNull(message = "客户名称cus_name不能为空")
    @NotEmpty(message = "客户名称cus_name不能为空")
    private String cus_name;//客户名称

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    @Override
    public String toString() {
        return "Xddb0020DataReqDto{" +
                "cus_name='" + cus_name + '\'' +
                '}';
    }
}
