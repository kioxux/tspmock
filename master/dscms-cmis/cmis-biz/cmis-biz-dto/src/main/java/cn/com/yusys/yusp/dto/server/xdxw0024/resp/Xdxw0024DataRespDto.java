package cn.com.yusys.yusp.dto.server.xdxw0024.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询是否有信贷记录
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0024DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "num")
	private Integer num;//总数

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	@Override
	public String toString() {
		return "Xdxw0024DataRespDto{" +
				"num=" + num +
				'}';
	}
}
