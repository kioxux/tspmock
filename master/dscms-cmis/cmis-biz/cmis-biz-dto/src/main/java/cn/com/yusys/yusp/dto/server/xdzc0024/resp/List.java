package cn.com.yusys.yusp.dto.server.xdzc0024.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：票据池资料补全查询 List
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "taskId")
    private String taskId;//任务编号
    @JsonProperty(value = "billImgId")
    private String billImgId;//发票影像流水号
    @JsonProperty(value = "contNo")
    private String contNo;//发票影像流水号
    @JsonProperty(value = "pvpSerno")
    private String pvpSerno;//出账流水号
    @JsonProperty(value = "taskCreateDate")
    private String taskCreateDate;//任务生成日期
    @JsonProperty(value = "needFinishDate")
    private String needFinishDate;//要求完成日期
    @JsonProperty(value = "batchDrftAmt")
    private String batchDrftAmt;//票面总金额
    @JsonProperty(value = "batchQnt")
    private String batchQnt;//批次条数
    @JsonProperty(value = "collectDate")
    private String collectDate;//收集日期
    @JsonProperty(value = "isAddBill")
    private String isAddBill;//是否完成补扫
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批状态
    @JsonProperty(value = "remark")
    private String remark;//备注

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getBillImgId() {
        return billImgId;
    }

    public void setBillImgId(String billImgId) {
        this.billImgId = billImgId;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getTaskCreateDate() {
        return taskCreateDate;
    }

    public void setTaskCreateDate(String taskCreateDate) {
        this.taskCreateDate = taskCreateDate;
    }

    public String getNeedFinishDate() {
        return needFinishDate;
    }

    public void setNeedFinishDate(String needFinishDate) {
        this.needFinishDate = needFinishDate;
    }

    public String getBatchDrftAmt() {
        return batchDrftAmt;
    }

    public void setBatchDrftAmt(String batchDrftAmt) {
        this.batchDrftAmt = batchDrftAmt;
    }

    public String getBatchQnt() {
        return batchQnt;
    }

    public void setBatchQnt(String batchQnt) {
        this.batchQnt = batchQnt;
    }

    public String getCollectDate() {
        return collectDate;
    }

    public void setCollectDate(String collectDate) {
        this.collectDate = collectDate;
    }

    public String getIsAddBill() {
        return isAddBill;
    }

    public void setIsAddBill(String isAddBill) {
        this.isAddBill = isAddBill;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "List{" +
                "taskId='" + taskId + '\'' +
                ", billImgId='" + billImgId + '\'' +
                ", contNo='" + contNo + '\'' +
                ", pvpSerno='" + pvpSerno + '\'' +
                ", taskCreateDate='" + taskCreateDate + '\'' +
                ", needFinishDate='" + needFinishDate + '\'' +
                ", batchDrftAmt='" + batchDrftAmt + '\'' +
                ", batchQnt='" + batchQnt + '\'' +
                ", collectDate='" + collectDate + '\'' +
                ", isAddBill='" + isAddBill + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}