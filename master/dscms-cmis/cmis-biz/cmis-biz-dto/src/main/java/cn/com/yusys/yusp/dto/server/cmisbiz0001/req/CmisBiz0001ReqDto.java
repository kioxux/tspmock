package cn.com.yusys.yusp.dto.server.cmisbiz0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 单一客户授信复审
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisBiz0001ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 主键
     **/
    @JsonProperty(value = "pkId")
    private String pkId;

    /**
     * 申请流水号
     **/
    @JsonProperty(value = "serno")
    private String serno;

    /**
     * 授信类型
     **/
    @JsonProperty(value = "lmtType")
    private String lmtType;

    /**
     * 客户编号
     **/
    @JsonProperty(value = "cusId")
    private String cusId;

    /**
     * 客户类型
     **/
    @JsonProperty(value = "cusType")
    private String cusType;

    /**
     * 客户名称
     **/
    @JsonProperty(value = "cusName")
    private String cusName;

    /**
     * 原授信流水号
     **/
    @JsonProperty(value = "ogrigiLmtSerno")
    private String ogrigiLmtSerno;

    /**
     * 原授信批复流水号
     **/
    @JsonProperty(value = "origiLmtReplySerno")
    private String origiLmtReplySerno;

    /**
     * 原授信期限
     **/
    @JsonProperty(value = "origiLmtTerm")
    private Integer origiLmtTerm;

    /**
     * 原授信宽限期
     **/
    @JsonProperty(value = "origiLmtGraperTerm")
    private Integer origiLmtGraperTerm;

    /**
     * 原敞口额度合计
     **/
    @JsonProperty(value = "origiOpenTotalLmtAmt")
    private java.math.BigDecimal origiOpenTotalLmtAmt;

    /**
     * 原低风险额度合计
     **/
    @JsonProperty(value = "origiLowRiskTotalLmtAmt")
    private java.math.BigDecimal origiLowRiskTotalLmtAmt;

    /**
     * 测算最高流动资金贷款额度
     **/
    @JsonProperty(value = "evalHighCurfundLmtAmt")
    private java.math.BigDecimal evalHighCurfundLmtAmt;

    /**
     * 币种
     **/
    @JsonProperty(value = "curType")
    private String curType;

    /**
     * 敞口额度合计
     * **/
    @JsonProperty(value = "openTotalLmtAmt")
    private java.math.BigDecimal openTotalLmtAmt;

    /**
     * 低风险额度合计
     **/
    @JsonProperty(value = "lowRiskTotalLmtAmt")
    private java.math.BigDecimal lowRiskTotalLmtAmt;

    /**
     * 授信期限
     **/
    @JsonProperty(value = "lmtTerm")
    private Integer lmtTerm;

    /**
     * 授信宽限期
     **/
    @JsonProperty(value = "lmtGraperTerm")
    private Integer lmtGraperTerm;

    /**
     * 调查报告类型
     **/
    @JsonProperty(value = "rptType")
    private String rptType;

    /**
     * 是否集团授信
     **/
    @JsonProperty(value = "isGrp")
    private String isGrp;

    /**
     * 操作类型
     **/
    @JsonProperty(value = "oprType")
    private String oprType;

    /**
     * 审批状态
     **/
    @JsonProperty(value = "approveStatus")
    private String approveStatus;

    /**
     * 登记人
     **/
    @JsonProperty(value = "inputId")
    private String inputId;

    /**
     * 登记机构
     **/
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    /**
     * 登记日期
     **/
    @JsonProperty(value = "inputDate")
    private String inputDate;

    /**
     * 最近修改人
     **/
    @JsonProperty(value = "updId")
    private String updId;

    /**
     * 最近修改机构
     **/
    @JsonProperty(value = "updBrId")
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @JsonProperty(value = "updDate")
    private String updDate;

    /**
     * 主管客户经理
     **/
    @JsonProperty(value = "managerId")
    private String managerId;

    /**
     * 主管机构
     **/
    @JsonProperty(value = "managerBrId")
    private String managerBrId;

    /**
     * 创建时间
     **/
    @JsonProperty(value = "createTime")
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @JsonProperty(value = "updateTime")
    private java.util.Date updateTime;

    /**
     * 省心快贷风险拦截结果
     **/
    @JsonProperty(value = "sxkdRiskResult")
    private String sxkdRiskResult;

    /**
     * 是否提交自动化审批*/

    @JsonProperty(value = "isSubAutoAppr")
    private String isSubAutoAppr;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getOgrigiLmtSerno() {
        return ogrigiLmtSerno;
    }

    public void setOgrigiLmtSerno(String ogrigiLmtSerno) {
        this.ogrigiLmtSerno = ogrigiLmtSerno;
    }

    public String getOrigiLmtReplySerno() {
        return origiLmtReplySerno;
    }

    public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
        this.origiLmtReplySerno = origiLmtReplySerno;
    }

    public Integer getOrigiLmtTerm() {
        return origiLmtTerm;
    }

    public void setOrigiLmtTerm(Integer origiLmtTerm) {
        this.origiLmtTerm = origiLmtTerm;
    }

    public Integer getOrigiLmtGraperTerm() {
        return origiLmtGraperTerm;
    }

    public void setOrigiLmtGraperTerm(Integer origiLmtGraperTerm) {
        this.origiLmtGraperTerm = origiLmtGraperTerm;
    }

    public BigDecimal getOrigiOpenTotalLmtAmt() {
        return origiOpenTotalLmtAmt;
    }

    public void setOrigiOpenTotalLmtAmt(BigDecimal origiOpenTotalLmtAmt) {
        this.origiOpenTotalLmtAmt = origiOpenTotalLmtAmt;
    }

    public BigDecimal getOrigiLowRiskTotalLmtAmt() {
        return origiLowRiskTotalLmtAmt;
    }

    public void setOrigiLowRiskTotalLmtAmt(BigDecimal origiLowRiskTotalLmtAmt) {
        this.origiLowRiskTotalLmtAmt = origiLowRiskTotalLmtAmt;
    }

    public BigDecimal getEvalHighCurfundLmtAmt() {
        return evalHighCurfundLmtAmt;
    }

    public void setEvalHighCurfundLmtAmt(BigDecimal evalHighCurfundLmtAmt) {
        this.evalHighCurfundLmtAmt = evalHighCurfundLmtAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getOpenTotalLmtAmt() {
        return openTotalLmtAmt;
    }

    public void setOpenTotalLmtAmt(BigDecimal openTotalLmtAmt) {
        this.openTotalLmtAmt = openTotalLmtAmt;
    }

    public BigDecimal getLowRiskTotalLmtAmt() {
        return lowRiskTotalLmtAmt;
    }

    public void setLowRiskTotalLmtAmt(BigDecimal lowRiskTotalLmtAmt) {
        this.lowRiskTotalLmtAmt = lowRiskTotalLmtAmt;
    }

    public Integer getLmtTerm() {
        return lmtTerm;
    }

    public void setLmtTerm(Integer lmtTerm) {
        this.lmtTerm = lmtTerm;
    }

    public Integer getLmtGraperTerm() {
        return lmtGraperTerm;
    }

    public void setLmtGraperTerm(Integer lmtGraperTerm) {
        this.lmtGraperTerm = lmtGraperTerm;
    }

    public String getRptType() {
        return rptType;
    }

    public void setRptType(String rptType) {
        this.rptType = rptType;
    }

    public String getIsGrp() {
        return isGrp;
    }

    public void setIsGrp(String isGrp) {
        this.isGrp = isGrp;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSxkdRiskResult() {
        return sxkdRiskResult;
    }

    public void setSxkdRiskResult(String sxkdRiskResult) {
        this.sxkdRiskResult = sxkdRiskResult;
    }

    public String getIsSubAutoAppr() {
        return isSubAutoAppr;
    }

    public void setIsSubAutoAppr(String isSubAutoAppr) {
        this.isSubAutoAppr = isSubAutoAppr;
    }

    @Override
    public String toString() {
        return "CmisBiz0001ReqDto{" +
                "pkId='" + pkId + '\'' +
                ", serno='" + serno + '\'' +
                ", lmtType='" + lmtType + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusType='" + cusType + '\'' +
                ", cusName='" + cusName + '\'' +
                ", ogrigiLmtSerno='" + ogrigiLmtSerno + '\'' +
                ", origiLmtReplySerno='" + origiLmtReplySerno + '\'' +
                ", origiLmtTerm=" + origiLmtTerm +
                ", origiLmtGraperTerm=" + origiLmtGraperTerm +
                ", origiOpenTotalLmtAmt=" + origiOpenTotalLmtAmt +
                ", origiLowRiskTotalLmtAmt=" + origiLowRiskTotalLmtAmt +
                ", evalHighCurfundLmtAmt=" + evalHighCurfundLmtAmt +
                ", curType='" + curType + '\'' +
                ", openTotalLmtAmt=" + openTotalLmtAmt +
                ", lowRiskTotalLmtAmt=" + lowRiskTotalLmtAmt +
                ", lmtTerm=" + lmtTerm +
                ", lmtGraperTerm=" + lmtGraperTerm +
                ", rptType='" + rptType + '\'' +
                ", isGrp='" + isGrp + '\'' +
                ", oprType='" + oprType + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", sxkdRiskResult='" + sxkdRiskResult + '\'' +
                ", isSubAutoAppr='" + isSubAutoAppr + '\'' +
                '}';
    }
}
