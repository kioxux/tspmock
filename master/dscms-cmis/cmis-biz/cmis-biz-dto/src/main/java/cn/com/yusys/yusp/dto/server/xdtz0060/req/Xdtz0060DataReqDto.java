package cn.com.yusys.yusp.dto.server.xdtz0060.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 请求Dto：查询我行有未结清贷款
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0060DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotBlank(message = "queryType不能为空")
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型 01-查询企业 02-查询企业实际控制人
    @NotBlank(message = "cusId不能为空")
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdtz0060DataReqDto{" +
                "queryType='" + queryType + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
