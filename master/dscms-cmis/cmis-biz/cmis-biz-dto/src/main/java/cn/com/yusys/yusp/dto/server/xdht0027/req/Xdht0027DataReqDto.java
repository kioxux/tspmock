package cn.com.yusys.yusp.dto.server.xdht0027.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据客户调查表编号取得贷款合同主表的合同状态
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0027DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cert_code")
    private String cert_code;//身份证号码


    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    @Override
    public String toString() {
        return "Xdht0027DataReqDto{" +
                "cert_code='" + cert_code + '\'' +
                '}';
    }
}
