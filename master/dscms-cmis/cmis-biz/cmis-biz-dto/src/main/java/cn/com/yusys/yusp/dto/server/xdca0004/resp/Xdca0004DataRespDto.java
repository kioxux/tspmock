package cn.com.yusys.yusp.dto.server.xdca0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：大额分期合同详情查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdca0004DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "serno")
    private String serno;//业务流水号
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码
    @JsonProperty(value = "cardNo")
    private String cardNo;//卡号
    @JsonProperty(value = "loanPlan")
    private String loanPlan;//分期计划
    @JsonProperty(value = "loanAmount")
    private String loanAmount;//分期金额
    @JsonProperty(value = "loanTerm")
    private BigDecimal loanTerm;//分期期数
    @JsonProperty(value = "sendMode")
    private Integer sendMode;//放款方式
    @JsonProperty(value = "guarMode")
    private String guarMode;//担保方式
    @JsonProperty(value = "loanFeeMethod")
    private String loanFeeMethod;//分期手续费收取方式
    @JsonProperty(value = "loanPrinDistMethod")
    private String loanPrinDistMethod;//分期本金分配方式
    @JsonProperty(value = "loanFeeCalcMethod")
    private String loanFeeCalcMethod;//分期手续费计算方式
    @JsonProperty(value = "loanFeeRate")
    private String loanFeeRate;//分期手续费比例
    @JsonProperty(value = "loanrTarget")
    private BigDecimal loanrTarget;//分期放款账户对公/对私标识
    @JsonProperty(value = "ddBankBranch")
    private String ddBankBranch;//分期放款开户行号
    @JsonProperty(value = "ddBankName")
    private String ddBankName;//分期放款银行名称
    @JsonProperty(value = "ddBankAccNo")
    private String ddBankAccNo;//分期放款账号
    @JsonProperty(value = "ddBankAccName")
    private String ddBankAccName;//分期放款账户姓名
    @JsonProperty(value = "disbAcctPhone")
    private String disbAcctPhone;//放款账户移动电话
    @JsonProperty(value = "disbAcctCertType")
    private String disbAcctCertType;//放款账户证件类型
    @JsonProperty(value = "disbAcctCertCode")
    private String disbAcctCertCode;//放款账户证件号码
    @JsonProperty(value = "paymentPurpose")
    private String paymentPurpose;//资金用途
    @JsonProperty(value = "yearInterestRate")
    private String yearInterestRate;//分期折算近似年化利率
    @JsonProperty(value = "salesManNo")
    private BigDecimal salesManNo;//分期营销客户经理号
    @JsonProperty(value = "salesMan")
    private String salesMan;//分期营销人员姓名
    @JsonProperty(value = "salesManPhone")
    private String salesManPhone;//分期营销人员手机号
    @JsonProperty(value = "salesManOwingBranch")
    private String salesManOwingBranch;//分期营销人员所属支行
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "appDate")
    private String appDate;//申请日期

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getLoanPlan() {
        return loanPlan;
    }

    public void setLoanPlan(String loanPlan) {
        this.loanPlan = loanPlan;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public BigDecimal getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(BigDecimal loanTerm) {
        this.loanTerm = loanTerm;
    }

    public Integer getSendMode() {
        return sendMode;
    }

    public void setSendMode(Integer sendMode) {
        this.sendMode = sendMode;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getLoanFeeMethod() {
        return loanFeeMethod;
    }

    public void setLoanFeeMethod(String loanFeeMethod) {
        this.loanFeeMethod = loanFeeMethod;
    }

    public String getLoanPrinDistMethod() {
        return loanPrinDistMethod;
    }

    public void setLoanPrinDistMethod(String loanPrinDistMethod) {
        this.loanPrinDistMethod = loanPrinDistMethod;
    }

    public String getLoanFeeCalcMethod() {
        return loanFeeCalcMethod;
    }

    public void setLoanFeeCalcMethod(String loanFeeCalcMethod) {
        this.loanFeeCalcMethod = loanFeeCalcMethod;
    }

    public String getLoanFeeRate() {
        return loanFeeRate;
    }

    public void setLoanFeeRate(String loanFeeRate) {
        this.loanFeeRate = loanFeeRate;
    }

    public BigDecimal getLoanrTarget() {
        return loanrTarget;
    }

    public void setLoanrTarget(BigDecimal loanrTarget) {
        this.loanrTarget = loanrTarget;
    }

    public String getDdBankBranch() {
        return ddBankBranch;
    }

    public void setDdBankBranch(String ddBankBranch) {
        this.ddBankBranch = ddBankBranch;
    }

    public String getDdBankName() {
        return ddBankName;
    }

    public void setDdBankName(String ddBankName) {
        this.ddBankName = ddBankName;
    }

    public String getDdBankAccNo() {
        return ddBankAccNo;
    }

    public void setDdBankAccNo(String ddBankAccNo) {
        this.ddBankAccNo = ddBankAccNo;
    }

    public String getDdBankAccName() {
        return ddBankAccName;
    }

    public void setDdBankAccName(String ddBankAccName) {
        this.ddBankAccName = ddBankAccName;
    }

    public String getDisbAcctPhone() {
        return disbAcctPhone;
    }

    public void setDisbAcctPhone(String disbAcctPhone) {
        this.disbAcctPhone = disbAcctPhone;
    }

    public String getDisbAcctCertType() {
        return disbAcctCertType;
    }

    public void setDisbAcctCertType(String disbAcctCertType) {
        this.disbAcctCertType = disbAcctCertType;
    }

    public String getDisbAcctCertCode() {
        return disbAcctCertCode;
    }

    public void setDisbAcctCertCode(String disbAcctCertCode) {
        this.disbAcctCertCode = disbAcctCertCode;
    }

    public String getPaymentPurpose() {
        return paymentPurpose;
    }

    public void setPaymentPurpose(String paymentPurpose) {
        this.paymentPurpose = paymentPurpose;
    }

    public String getYearInterestRate() {
        return yearInterestRate;
    }

    public void setYearInterestRate(String yearInterestRate) {
        this.yearInterestRate = yearInterestRate;
    }

    public BigDecimal getSalesManNo() {
        return salesManNo;
    }

    public void setSalesManNo(BigDecimal salesManNo) {
        this.salesManNo = salesManNo;
    }

    public String getSalesMan() {
        return salesMan;
    }

    public void setSalesMan(String salesMan) {
        this.salesMan = salesMan;
    }

    public String getSalesManPhone() {
        return salesManPhone;
    }

    public void setSalesManPhone(String salesManPhone) {
        this.salesManPhone = salesManPhone;
    }

    public String getSalesManOwingBranch() {
        return salesManOwingBranch;
    }

    public void setSalesManOwingBranch(String salesManOwingBranch) {
        this.salesManOwingBranch = salesManOwingBranch;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    @Override
    public String toString() {
        return "Xdca0004DataRespDto{" +
                "contNo='" + contNo + '\'' +
                "serno='" + serno + '\'' +
                "cusName='" + cusName + '\'' +
                "certType='" + certType + '\'' +
                "certCode='" + certCode + '\'' +
                "cardNo='" + cardNo + '\'' +
                "loanPlan='" + loanPlan + '\'' +
                "loanAmount='" + loanAmount + '\'' +
                "loanTerm='" + loanTerm + '\'' +
                "sendMode='" + sendMode + '\'' +
                "guarMode='" + guarMode + '\'' +
                "loanFeeMethod='" + loanFeeMethod + '\'' +
                "loanPrinDistMethod='" + loanPrinDistMethod + '\'' +
                "loanFeeCalcMethod='" + loanFeeCalcMethod + '\'' +
                "loanFeeRate='" + loanFeeRate + '\'' +
                "loanrTarget='" + loanrTarget + '\'' +
                "ddBankBranch='" + ddBankBranch + '\'' +
                "ddBankName='" + ddBankName + '\'' +
                "ddBankAccNo='" + ddBankAccNo + '\'' +
                "ddBankAccName='" + ddBankAccName + '\'' +
                "disbAcctPhone='" + disbAcctPhone + '\'' +
                "disbAcctCertType='" + disbAcctCertType + '\'' +
                "disbAcctCertCode='" + disbAcctCertCode + '\'' +
                "paymentPurpose='" + paymentPurpose + '\'' +
                "yearInterestRate='" + yearInterestRate + '\'' +
                "salesManNo='" + salesManNo + '\'' +
                "salesMan='" + salesMan + '\'' +
                "salesManPhone='" + salesManPhone + '\'' +
                "salesManOwingBranch='" + salesManOwingBranch + '\'' +
                "contStatus='" + contStatus + '\'' +
                "appDate='" + appDate + '\'' +
                '}';
    }
}  
