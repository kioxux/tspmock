package cn.com.yusys.yusp.dto.server.xddb0017.resp;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/21 17:34
 * @since 2021/6/21 17:34
 */
public class SlSernoQueryDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String yxSerno;
    private String cusName;
    private String guarantyId;
    private String areaLocation;
    private String guarCoutNo;
    private String guarContCnNo;
    private String createTime;
    private String guarContState;

    public String getYxSerno() {
        return yxSerno;
    }

    public void setYxSerno(String yxSerno) {
        this.yxSerno = yxSerno;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGuarantyId() {
        return guarantyId;
    }

    public void setGuarantyId(String guarantyId) {
        this.guarantyId = guarantyId;
    }

    public String getAreaLocation() {
        return areaLocation;
    }

    public void setAreaLocation(String areaLocation) {
        this.areaLocation = areaLocation;
    }

    public String getGuarCoutNo() {
        return guarCoutNo;
    }

    public void setGuarCoutNo(String guarCoutNo) {
        this.guarCoutNo = guarCoutNo;
    }

    public String getGuarContCnNo() {
        return guarContCnNo;
    }

    public void setGuarContCnNo(String guarContCnNo) {
        this.guarContCnNo = guarContCnNo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getGuarContState() {
        return guarContState;
    }

    public void setGuarContState(String guarContState) {
        this.guarContState = guarContState;
    }

    @Override
    public String toString() {
        return "SlSernoQueryDto{" +
                "yxSerno='" + yxSerno + '\'' +
                ", cusName='" + cusName + '\'' +
                ", guarantyId='" + guarantyId + '\'' +
                ", areaLocation='" + areaLocation + '\'' +
                ", guarCoutNo='" + guarCoutNo + '\'' +
                ", guarContCnNo='" + guarContCnNo + '\'' +
                ", createTime='" + createTime + '\'' +
                ", guarContState='" + guarContState + '\'' +
                '}';
    }
}
