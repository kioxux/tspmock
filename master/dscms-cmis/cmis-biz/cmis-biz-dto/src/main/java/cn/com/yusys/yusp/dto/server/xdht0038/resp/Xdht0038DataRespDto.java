package cn.com.yusys.yusp.dto.server.xdht0038.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：乐悠金根据核心客户号查询房贷首付款比例进行额度计算
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0038DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "list")
	private java.util.List<List> list;//list

	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Xdht0038DataRespDto{" +
				"list=" + list +
				'}';
	}
}
