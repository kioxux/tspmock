package cn.com.yusys.yusp.dto.server.xdht0030.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询客户我行合作次数
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0030DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xdht0030DataReqDto{" +
                "certNo='" + certNo + '\'' +
                '}';
    }
}