package cn.com.yusys.yusp.dto.server.xdxw0084.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：共借人送达地址确认书文本生成PDF接口
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0084DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pdfurl")
    private String pdfurl;//pdf地址
    @JsonProperty(value = "pdffilename")
    private String pdffilename;//pdf文件名称
    @JsonProperty(value = "htip")
    private String htip;//IP地址
    @JsonProperty(value = "port")
    private String port;//端口
    @JsonProperty(value = "username")
    private String username;//用户名
    @JsonProperty(value = "password")
    private String password;//密码

    public String getPdfurl() {
        return pdfurl;
    }

    public void setPdfurl(String pdfurl) {
        this.pdfurl = pdfurl;
    }

    public String getPdffilename() {
        return pdffilename;
    }

    public void setPdffilename(String pdffilename) {
        this.pdffilename = pdffilename;
    }

    public String getHtip() {
        return htip;
    }

    public void setHtip(String htip) {
        this.htip = htip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Xdxw0084DataRespDto{" +
                "pdfurl=" + pdfurl +
                "pdffilename=" + pdffilename +
                "htip=" + htip +
                "port=" + port +
                "username=" + username +
                "password=" + password +
                '}';
    }
}
