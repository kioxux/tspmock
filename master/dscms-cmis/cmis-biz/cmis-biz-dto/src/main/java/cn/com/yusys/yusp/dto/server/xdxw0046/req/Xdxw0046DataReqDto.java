package cn.com.yusys.yusp.dto.server.xdxw0046.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：优享贷批复结果反馈
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0046DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "areaName")
    private String areaName;//居住地址
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "applyAmount")
    private BigDecimal applyAmount;//申请金额
    @JsonProperty(value = "reality")
    private BigDecimal reality;//利率
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//贷款开始时间
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款结束时间
    @JsonProperty(value = "applyTerm")
    private String applyTerm;//贷款期限
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "guarWays")
    private String guarWays;//担保方式
    @JsonProperty(value = "limitType")
    private String limitType;//额度类型
    @JsonProperty(value = "curUseAmt")
    private BigDecimal curUseAmt;//用信金额
    @JsonProperty(value = "isBeTrusted")
    private String isBeTrusted;//是否受托支付
    @JsonProperty(value = "entrustType")
    private String entrustType;//受托类型
    @JsonProperty(value = "reqType")
    private String reqType;//请求类型
    @JsonProperty(value = "surveySerno")
    private String surveySerno;//批复流水号
    @JsonProperty(value = "geShuiDiZhi")
    private String geShuiDiZhi;//个税地址
    @JsonProperty(value = "contNo")
    private String contNo;//合同号


    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public BigDecimal getApplyAmount() {
        return applyAmount;
    }

    public void setApplyAmount(BigDecimal applyAmount) {
        this.applyAmount = applyAmount;
    }

    public BigDecimal getReality() {
        return reality;
    }

    public void setReality(BigDecimal reality) {
        this.reality = reality;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getApplyTerm() {
        return applyTerm;
    }

    public void setApplyTerm(String applyTerm) {
        this.applyTerm = applyTerm;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getGuarWays() {
        return guarWays;
    }

    public void setGuarWays(String guarWays) {
        this.guarWays = guarWays;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    public BigDecimal getCurUseAmt() {
        return curUseAmt;
    }

    public void setCurUseAmt(BigDecimal curUseAmt) {
        this.curUseAmt = curUseAmt;
    }

    public String getIsBeTrusted() {
        return isBeTrusted;
    }

    public void setIsBeTrusted(String isBeTrusted) {
        this.isBeTrusted = isBeTrusted;
    }

    public String getEntrustType() {
        return entrustType;
    }

    public void setEntrustType(String entrustType) {
        this.entrustType = entrustType;
    }

    public String getReqType() {
        return reqType;
    }

    public void setReqType(String reqType) {
        this.reqType = reqType;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public String getGeShuiDiZhi() {
        return geShuiDiZhi;
    }

    public void setGeShuiDiZhi(String geShuiDiZhi) {
        this.geShuiDiZhi = geShuiDiZhi;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    @Override
    public String toString() {
        return "Xdxw0046DataReqDto{" +
                "cusName='" + cusName + '\'' +
                "cusId='" + cusId + '\'' +
                "certType='" + certType + '\'' +
                "certNo='" + certNo + '\'' +
                "areaName='" + areaName + '\'' +
                "managerId='" + managerId + '\'' +
                "applyAmount='" + applyAmount + '\'' +
                "reality='" + reality + '\'' +
                "loanStartDate='" + loanStartDate + '\'' +
                "loanEndDate='" + loanEndDate + '\'' +
                "applyTerm='" + applyTerm + '\'' +
                "repayType='" + repayType + '\'' +
                "guarWays='" + guarWays + '\'' +
                "limitType='" + limitType + '\'' +
                "curUseAmt='" + curUseAmt + '\'' +
                "isBeTrusted='" + isBeTrusted + '\'' +
                "entrustType='" + entrustType + '\'' +
                "reqType='" + reqType + '\'' +
                "surveySerno='" + surveySerno + '\'' +
                "geShuiDiZhi='" + geShuiDiZhi + '\'' +
                "contNo='" + contNo + '\'' +
                '}';
    }
}
