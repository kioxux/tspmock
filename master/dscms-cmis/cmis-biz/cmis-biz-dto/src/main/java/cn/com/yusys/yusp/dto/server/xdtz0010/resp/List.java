package cn.com.yusys.yusp.dto.server.xdtz0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据身份证号获取借据信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "isHouseLoan")
    private String isHouseLoan;//是否为房贷 Y-是 N-否
    @JsonProperty(value = "bizType")
    private String bizType;//品种名称
    @JsonProperty(value = "payOutAmt")
    private BigDecimal payOutAmt;//发放金额
    @JsonProperty(value = "surplusAmt")
    private BigDecimal surplusAmt;//剩余金额
    @JsonProperty(value = "startDate")
    private String startDate;//起始日
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "debit")
    private String debit;//借款人
    @JsonProperty(value = "signFlag")
    private String signFlag;//签订标志
    @JsonProperty(value = "irAdjustType")
    private String irAdjustType;//利率调整方式
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行年利率
    @JsonProperty(value = "yearLPRRate")
    private BigDecimal yearLPRRate;//1年期LPR利率
    @JsonProperty(value = "fiveYearLPRRate")
    private BigDecimal fiveYearLPRRate;//5年期LPR利率

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getIsHouseLoan() {
        return isHouseLoan;
    }

    public void setIsHouseLoan(String isHouseLoan) {
        this.isHouseLoan = isHouseLoan;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public BigDecimal getPayOutAmt() {
        return payOutAmt;
    }

    public void setPayOutAmt(BigDecimal payOutAmt) {
        this.payOutAmt = payOutAmt;
    }

    public BigDecimal getSurplusAmt() {
        return surplusAmt;
    }

    public void setSurplusAmt(BigDecimal surplusAmt) {
        this.surplusAmt = surplusAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getSignFlag() {
        return signFlag;
    }

    public void setSignFlag(String signFlag) {
        this.signFlag = signFlag;
    }

    public String getIrAdjustType() {
        return irAdjustType;
    }

    public void setIrAdjustType(String irAdjustType) {
        this.irAdjustType = irAdjustType;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public BigDecimal getYearLPRRate() {
        return yearLPRRate;
    }

    public void setYearLPRRate(BigDecimal yearLPRRate) {
        this.yearLPRRate = yearLPRRate;
    }

    public BigDecimal getFiveYearLPRRate() {
        return fiveYearLPRRate;
    }

    public void setFiveYearLPRRate(BigDecimal fiveYearLPRRate) {
        this.fiveYearLPRRate = fiveYearLPRRate;
    }

    @Override
    public String toString() {
        return "List{" +
                "billNo='" + billNo + '\'' +
                "isHouseLoan='" + isHouseLoan + '\'' +
                "bizType='" + bizType + '\'' +
                "payOutAmt='" + payOutAmt + '\'' +
                "surplusAmt='" + surplusAmt + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                "debit='" + debit + '\'' +
                "signFlag='" + signFlag + '\'' +
                "irAdjustType='" + irAdjustType + '\'' +
                "realityIrY='" + realityIrY + '\'' +
                "yearLPRRate='" + yearLPRRate + '\'' +
                "fiveYearLPRRate='" + fiveYearLPRRate + '\'' +
                '}';
    }
}
