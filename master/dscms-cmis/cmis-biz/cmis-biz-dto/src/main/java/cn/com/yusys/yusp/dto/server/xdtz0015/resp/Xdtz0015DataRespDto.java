package cn.com.yusys.yusp.dto.server.xdtz0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：贴现记账结果通知
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0015DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFlag")
    private String opFlag;//成功失败标志
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xdtz0015DataRespDto{" +
                "opFlag='" + opFlag + '\'' +
                ", opMsg='" + opMsg + '\'' +
                '}';
    }
}
