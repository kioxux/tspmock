package cn.com.yusys.yusp.dto.server.xdxw0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 请求Data：批复信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0007DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotBlank(message = "合同号不能为空")
    @JsonProperty(value = "contNo")
    private String contNo;//合同号

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    @Override
    public String toString() {
        return "Xdxw0007DataReqDto{" +
                "contNo=" + contNo +
                '}';
    }
}
