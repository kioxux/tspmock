package cn.com.yusys.yusp.dto.server.xdtz0031.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：根据合同号获取借据信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0031DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "accLoanList")
    private java.util.List<AccLoanList> accLoanList;

    public List<AccLoanList> getAccLoanList() {
        return accLoanList;
    }

    public void setAccLoanList(List<AccLoanList> accLoanList) {
        this.accLoanList = accLoanList;
    }

    @Override
    public String toString() {
        return "Xdtz0031DataRespDto{" +
                "accLoanList=" + accLoanList +
                '}';
    }
}
