package cn.com.yusys.yusp.dto.server.xdcz0031.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;


@JsonPropertyOrder(alphabetic = true)
public class Xdcz0031DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Override
    public String toString() {
        return "Xdcz0031DataReqDto{" +
                "billNo='" + billNo + '\'' +
                '}';
    }
}
