package cn.com.yusys.yusp.dto.server.xdtz0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：借据明细查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0011DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "qryType")
    private String qryType;//查询类型
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "certNo")
    private String certNo;//证件号

    public String getQryType() {
        return qryType;
    }

    public void setQryType(String qryType) {
        this.qryType = qryType;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xdtz0011DataReqDto{" +
                "qryType='" + qryType + '\'' +
                ", billNo='" + billNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", certNo='" + certNo + '\'' +
                '}';
    }
}
