package cn.com.yusys.yusp.dto.server.xdtz0056.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询还款业务类型
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0056DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "biztype")
    private String biztype;//

    public String getBiztype() {
        return biztype;
    }

    public void setBiztype(String biztype) {
        this.biztype = biztype;
    }

    @Override
    public String toString() {
        return "Xdtz0056DataRespDto{" +
                "biztype='" + biztype + '\'' +
                '}';
    }
}
