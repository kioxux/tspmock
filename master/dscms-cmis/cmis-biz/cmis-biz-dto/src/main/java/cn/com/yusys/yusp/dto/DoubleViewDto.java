package cn.com.yusys.yusp.dto;

/**
 * @author xll
 * @version 1.0.0
 * @date 2021/8/30 19:27
 * @desc 双录信息查询dto
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class DoubleViewDto {
    // 业务流水
    private String bizSerno;

    // 业务阶段
    private String bizStageType;

    //产品编号
    private String prdId;

    //产品名称
    private String prdName;

    //客户编号
    private String cusId;

    //客户名称
    private String cusName;

    //证件号
    private String certCode;

    //手机号
    private String phone;

    //地址
    private String address;

    //责任人
    private String managerId;

    //责任人名称
    private String managerName;

    //责任机构
    private String managerBrId;

    //责任机构名称
    private String managerBrName;

    //登记日期
    private String inputDate;

    private String startPageNum;//开始页码

    private String pageSize;//每页记录数

    public String getBizSerno() {
        return bizSerno;
    }

    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    public String getBizStageType() {
        return bizStageType;
    }

    public void setBizStageType(String bizStageType) {
        this.bizStageType = bizStageType;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerBrName() {
        return managerBrName;
    }

    public void setManagerBrName(String managerBrName) {
        this.managerBrName = managerBrName;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
