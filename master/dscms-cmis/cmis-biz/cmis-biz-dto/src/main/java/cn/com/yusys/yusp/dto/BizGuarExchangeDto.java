package cn.com.yusys.yusp.dto;

import java.util.List;
import java.util.Map;

/**
 * 业务与押品交互dto
 */
public class BizGuarExchangeDto {
    //入参部分
    private String guarContNo;//担保合同编号
    private String guarNo;//押品编号
    private String guarBusistate;//押品所在业务阶段

    //代码逻辑入参部分
    private String oprType;//操作类型，数据字典详见IqpConstant常量类中的  操作标识  模块
    private String status;//状态，数据字典详见GrtGuarContRelDefEnums 常量类中 状态  模块

    //返回部分，返回的码值以及错误信息  详见  GrtGuarContRelDefEnums
    private String rtnCode;//返回码值
    private String rtnMsg;//返回信息
    private boolean existsFlag;//关系存在标识位
    private List<Map> list;

    public String getGuarContNo() {
        return guarContNo;
    }

    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    public String getGuarBusistate() {
        return guarBusistate;
    }

    public void setGuarBusistate(String guarBusistate) {
        this.guarBusistate = guarBusistate;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType){
        this.oprType = oprType;
    }

    public boolean isExistsFlag() {
        return existsFlag;
    }

    public void setExistsFlag(boolean existsFlag) {
        this.existsFlag = existsFlag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public List<Map> getList() {
        return list;
    }
    
    public void setList(List<Map> list) {
        this.list = list;
    }
}
