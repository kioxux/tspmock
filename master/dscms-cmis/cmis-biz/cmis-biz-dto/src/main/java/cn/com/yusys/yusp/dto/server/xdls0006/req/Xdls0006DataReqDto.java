package cn.com.yusys.yusp.dto.server.xdls0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：白领贷额度查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdls0006DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "prdType")
    private String prdType;//产品类型

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getPrdType() {
        return prdType;
    }

    public void setPrdType(String prdType) {
        this.prdType = prdType;
    }

    @Override
    public String toString() {
        return "Xdls0006DataReqDto{" +
                "certNo='" + certNo + '\'' +
                ", prdType='" + prdType + '\'' +
                '}';
    }
}
