package cn.com.yusys.yusp.dto.server.xdtz0047.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：借据信息查询（按证件号）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0047DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "query_type")
    private String query_type;//查询类型
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户编号

    public String getQuery_type() {
        return query_type;
    }

    public void setQuery_type(String query_type) {
        this.query_type = query_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    @Override
    public String toString() {
        return "Xdtz0047DataReqDto{" +
                "query_type='" + query_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cus_id='" + cus_id + '\'' +
                '}';
    }
}
