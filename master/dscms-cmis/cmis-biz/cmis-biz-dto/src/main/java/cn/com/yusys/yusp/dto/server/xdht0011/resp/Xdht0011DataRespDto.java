package cn.com.yusys.yusp.dto.server.xdht0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0011DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "accountList")
    private java.util.List<AccountList> accountList;
    @JsonProperty(value = "guarContList")
    private java.util.List<GuarContList> guarContList;
    @JsonProperty(value = "loanContList")
    private java.util.List<LoanContList> loanContList;
    @JsonProperty(value = "hxdLoanContList")
    private java.util.List<HxdLoanContList> hxdLoanContList;

    public List<AccountList> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<AccountList> accountList) {
        this.accountList = accountList;
    }

    public List<GuarContList> getGuarContList() {
        return guarContList;
    }

    public void setGuarContList(List<GuarContList> guarContList) {
        this.guarContList = guarContList;
    }

    public List<LoanContList> getLoanContList() {
        return loanContList;
    }

    public void setLoanContList(List<LoanContList> loanContList) {
        this.loanContList = loanContList;
    }

    public List<HxdLoanContList> getHxdLoanContList() {
        return hxdLoanContList;
    }

    public void setHxdLoanContList(List<HxdLoanContList> hxdLoanContList) {
        this.hxdLoanContList = hxdLoanContList;
    }

    @Override
    public String toString() {
        return "Xdht0011DataRespDto{" +
                "accountList=" + accountList +
                ", guarContList=" + guarContList +
                ", loanContList=" + loanContList +
                ", hxdLoanContList=" + hxdLoanContList +
                '}';
    }
}
