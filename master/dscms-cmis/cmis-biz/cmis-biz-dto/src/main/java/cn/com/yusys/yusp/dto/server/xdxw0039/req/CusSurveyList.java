package cn.com.yusys.yusp.dto.server.xdxw0039.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：提交决议
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CusSurveyList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//小贷客户调查主表主键
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户编号
    @JsonProperty(value = "prd_id")
    private String prd_id;//产品代码
    @JsonProperty(value = "prd_name")
    private String prd_name;//产品名称
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//合同金额
    @JsonProperty(value = "reality_ir_y")
    private BigDecimal reality_ir_y;//利率
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//合同起始日
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//合同到期日
    @JsonProperty(value = "apply_term")
    private Integer apply_term;//期限
    @JsonProperty(value = "repay_type")
    private String repay_type;//还款方式
    @JsonProperty(value = "guar_ways")
    private String guar_ways;//担保方式
    @JsonProperty(value = "limit_type")
    private String limit_type;//额度类型 01一次性 02循环
    @JsonProperty(value = "cur_use_amt")
    private BigDecimal cur_use_amt;//本次用信金额
    @JsonProperty(value = "is_wxbxd")
    private String is_wxbxd;//无还本续贷标记
    @JsonProperty(value = "is_be_entrusted")
    private String is_be_entrusted;//是否受托支付
    @JsonProperty(value = "is_credit_condition")
    private String is_credit_condition;//是否有用信条件
    @JsonProperty(value = "entrust_type")
    private String entrust_type;//受托类型
    @JsonProperty(value = "approval_type")
    private String approval_type;//审批类型
    @JsonProperty(value = "is_comm_borr")
    private String is_comm_borr;//是否有共借人

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getPrd_id() {
        return prd_id;
    }

    public void setPrd_id(String prd_id) {
        this.prd_id = prd_id;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public BigDecimal getReality_ir_y() {
        return reality_ir_y;
    }

    public void setReality_ir_y(BigDecimal reality_ir_y) {
        this.reality_ir_y = reality_ir_y;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public Integer getApply_term() {
        return apply_term;
    }

    public void setApply_term(Integer apply_term) {
        this.apply_term = apply_term;
    }

    public String getRepay_type() {
        return repay_type;
    }

    public void setRepay_type(String repay_type) {
        this.repay_type = repay_type;
    }

    public String getGuar_ways() {
        return guar_ways;
    }

    public void setGuar_ways(String guar_ways) {
        this.guar_ways = guar_ways;
    }

    public String getLimit_type() {
        return limit_type;
    }

    public void setLimit_type(String limit_type) {
        this.limit_type = limit_type;
    }

    public BigDecimal getCur_use_amt() {
        return cur_use_amt;
    }

    public void setCur_use_amt(BigDecimal cur_use_amt) {
        this.cur_use_amt = cur_use_amt;
    }

    public String getIs_wxbxd() {
        return is_wxbxd;
    }

    public void setIs_wxbxd(String is_wxbxd) {
        this.is_wxbxd = is_wxbxd;
    }

    public String getIs_be_entrusted() {
        return is_be_entrusted;
    }

    public void setIs_be_entrusted(String is_be_entrusted) {
        this.is_be_entrusted = is_be_entrusted;
    }

    public String getIs_credit_condition() {
        return is_credit_condition;
    }

    public void setIs_credit_condition(String is_credit_condition) {
        this.is_credit_condition = is_credit_condition;
    }

    public String getEntrust_type() {
        return entrust_type;
    }

    public void setEntrust_type(String entrust_type) {
        this.entrust_type = entrust_type;
    }

    public String getApproval_type() {
        return approval_type;
    }

    public void setApproval_type(String approval_type) {
        this.approval_type = approval_type;
    }

    public String getIs_comm_borr() {
        return is_comm_borr;
    }

    public void setIs_comm_borr(String is_comm_borr) {
        this.is_comm_borr = is_comm_borr;
    }

    @Override
    public String toString() {
        return "CusSurveyList{" +
                "survey_serno='" + survey_serno + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", prd_id='" + prd_id + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", apply_amount=" + apply_amount +
                ", reality_ir_y=" + reality_ir_y +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", apply_term=" + apply_term +
                ", repay_type='" + repay_type + '\'' +
                ", guar_ways='" + guar_ways + '\'' +
                ", limit_type='" + limit_type + '\'' +
                ", cur_use_amt=" + cur_use_amt +
                ", is_wxbxd='" + is_wxbxd + '\'' +
                ", is_be_entrusted='" + is_be_entrusted + '\'' +
                ", is_credit_condition='" + is_credit_condition + '\'' +
                ", entrust_type='" + entrust_type + '\'' +
                ", approval_type='" + approval_type + '\'' +
                ", is_comm_borr='" + is_comm_borr + '\'' +
                '}';
    }
}
