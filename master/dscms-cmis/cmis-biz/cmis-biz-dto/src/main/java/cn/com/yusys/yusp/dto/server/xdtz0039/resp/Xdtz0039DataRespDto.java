package cn.com.yusys.yusp.dto.server.xdtz0039.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据企业名称查询申请企业在本行是否存在当前逾期贷款
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0039DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billBal")
    private BigDecimal billBal;//借据余额
    @JsonProperty(value = "isOverdue")
    private String isOverdue;//是否存在逾期

    public Xdtz0039DataRespDto() {
    }

    public Xdtz0039DataRespDto(BigDecimal billBal, String isOverdue) {
        this.billBal = billBal;
        this.isOverdue = isOverdue;
    }

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    public String getIsOverdue() {
        return isOverdue;
    }

    public void setIsOverdue(String isOverdue) {
        this.isOverdue = isOverdue;
    }

    @Override
    public String toString() {
        return "Xdtz0039DataRespDto{" +
                "billBal='" + billBal + '\'' +
                ", isOverdue='" + isOverdue + '\'' +
                '}';
    }
}
