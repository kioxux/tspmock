package cn.com.yusys.yusp.dto.server.xdxt0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询信贷用户的特定岗位信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0006DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号
    @JsonProperty(value = "dutyNo")
    private String dutyNo;//岗位编号
    @JsonProperty(value = "dutyName")
    private String dutyName;//岗位名称

    public String getDutyNo() {
        return dutyNo;
    }

    public void setDutyNo(String dutyNo) {
        this.dutyNo = dutyNo;
    }

    public String getDutyName() {
        return dutyName;
    }

    public void setDutyName(String dutyName) {
        this.dutyName = dutyName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Xdxt0006DataReqDto{" +
                "managerId='" + managerId + '\'' +
                ", dutyNo='" + dutyNo + '\'' +
                ", dutyName='" + dutyName + '\'' +
                '}';
    }
}
