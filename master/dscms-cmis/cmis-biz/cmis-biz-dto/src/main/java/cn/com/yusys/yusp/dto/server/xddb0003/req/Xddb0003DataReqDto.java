package cn.com.yusys.yusp.dto.server.xddb0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：押品信息同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "lserno")
    private String lserno;//业务流水号
    @JsonProperty(value = "iftssp")
    private String iftssp;//是否特殊审批
    @JsonProperty(value = "grtflg")
    private String grtflg;//主担保或副担保
    @JsonProperty(value = "iffadd")
    private String iffadd;//增加或者修改
    @JsonProperty(value = "custyp")
    private String custyp;//担保人客户类型
    @JsonProperty(value = "maid")
    private String maid;//创建人用户编号
    @JsonProperty(value = "inbrid")
    private String inbrid;//登记机构
    @JsonProperty(value = "indate")
    private String indate;//登记日期
    @JsonProperty(value = "inid")
    private String inid;//登记ID
    @JsonProperty(value = "mabrid")
    private String mabrid;//主管机构
    @JsonProperty(value = "cusid")
    private String cusid;//担保人客户号
    @JsonProperty(value = "cusna")
    private String cusna;//担保人名称
    @JsonProperty(value = "cetype")
    private String cetype;//担保人证件类型  STD_ZB_CERT_TYP
    @JsonProperty(value = "cerno")
    private String cerno;//担保人证件号码
    @JsonProperty(value = "cfblx")
    private String cfblx;//查封便利性
    @JsonProperty(value = "flyxx")
    private String flyxx;//法律有效性
    @JsonProperty(value = "dzywxg")
    private String dzywxg;//抵质押物与借款人相关性
    @JsonProperty(value = "dzypty")
    private String dzypty;//抵质押品通用性
    @JsonProperty(value = "dzypbx")
    private String dzypbx;//抵质押品变现能力
    @JsonProperty(value = "jgbdx")
    private String jgbdx;//价格波动性
    @JsonProperty(value = "areacd")
    private String areacd;//所在区域编号
    @JsonProperty(value = "areana")
    private String areana;//所在区域名称
    @JsonProperty(value = "dudate")
    private String dudate;//理财产品到期日
    @JsonProperty(value = "rirank")
    private String rirank;//理财风险等级
    @JsonProperty(value = "curren")
    private String curren;//币种 STD_ZX_CUR_TYPE
    @JsonProperty(value = "guramt")
    private BigDecimal guramt;//担保金额
    @JsonProperty(value = "relend")
    private String relend;//与借款人关系  STD_ZB_RELATION
    @JsonProperty(value = "ensure")
    private String ensure;//保证法律有效性
    @JsonProperty(value = "gubore")
    private String gubore;//保证人与借款人关联关系
    @JsonProperty(value = "isguar")
    private String isguar;//保证人是否专业担保公司
    @JsonProperty(value = "guartp")
    private String guartp;//保证方式 STD_ZB_GUARANTEE_TY
    @JsonProperty(value = "billno")
    private String billno;//票据号码
    @JsonProperty(value = "bilamt")
    private BigDecimal bilamt;//票面金额
    @JsonProperty(value = "stdate")
    private String stdate;//出票日期
    @JsonProperty(value = "enddt")
    private String enddt;//到期日期
    @JsonProperty(value = "drname")
    private String drname;//出票人名称
    @JsonProperty(value = "dracid")
    private String dracid;//出票人账户
    @JsonProperty(value = "rename")
    private String rename;//收款人名称
    @JsonProperty(value = "drbank")
    private String drbank;//出票人开户行行名
    @JsonProperty(value = "drbaid")
    private String drbaid;//出票人开户行行号
    @JsonProperty(value = "reacid")
    private String reacid;//收款人账号
    @JsonProperty(value = "acpid")
    private String acpid;//承兑行号
    @JsonProperty(value = "acpna")
    private String acpna;//承兑行名
    @JsonProperty(value = "rate")
    private BigDecimal rate;//汇率
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    @JsonProperty(value = "ownerList")
    private java.util.List<OwnerList> ownerList;//押品共有人列表

    public String getLserno() {
        return lserno;
    }

    public void setLserno(String lserno) {
        this.lserno = lserno;
    }

    public String getIftssp() {
        return iftssp;
    }

    public void setIftssp(String iftssp) {
        this.iftssp = iftssp;
    }

    public String getGrtflg() {
        return grtflg;
    }

    public void setGrtflg(String grtflg) {
        this.grtflg = grtflg;
    }

    public String getIffadd() {
        return iffadd;
    }

    public void setIffadd(String iffadd) {
        this.iffadd = iffadd;
    }

    public String getCustyp() {
        return custyp;
    }

    public void setCustyp(String custyp) {
        this.custyp = custyp;
    }

    public String getMaid() {
        return maid;
    }

    public void setMaid(String maid) {
        this.maid = maid;
    }

    public String getInbrid() {
        return inbrid;
    }

    public void setInbrid(String inbrid) {
        this.inbrid = inbrid;
    }

    public String getIndate() {
        return indate;
    }

    public void setIndate(String indate) {
        this.indate = indate;
    }

    public String getInid() {
        return inid;
    }

    public void setInid(String inid) {
        this.inid = inid;
    }

    public String getMabrid() {
        return mabrid;
    }

    public void setMabrid(String mabrid) {
        this.mabrid = mabrid;
    }

    public String getCusid() {
        return cusid;
    }

    public void setCusid(String cusid) {
        this.cusid = cusid;
    }

    public String getCusna() {
        return cusna;
    }

    public void setCusna(String cusna) {
        this.cusna = cusna;
    }

    public String getCetype() {
        return cetype;
    }

    public void setCetype(String cetype) {
        this.cetype = cetype;
    }

    public String getCerno() {
        return cerno;
    }

    public void setCerno(String cerno) {
        this.cerno = cerno;
    }

    public String getCfblx() {
        return cfblx;
    }

    public void setCfblx(String cfblx) {
        this.cfblx = cfblx;
    }

    public String getFlyxx() {
        return flyxx;
    }

    public void setFlyxx(String flyxx) {
        this.flyxx = flyxx;
    }

    public String getDzywxg() {
        return dzywxg;
    }

    public void setDzywxg(String dzywxg) {
        this.dzywxg = dzywxg;
    }

    public String getDzypty() {
        return dzypty;
    }

    public void setDzypty(String dzypty) {
        this.dzypty = dzypty;
    }

    public String getDzypbx() {
        return dzypbx;
    }

    public void setDzypbx(String dzypbx) {
        this.dzypbx = dzypbx;
    }

    public String getJgbdx() {
        return jgbdx;
    }

    public void setJgbdx(String jgbdx) {
        this.jgbdx = jgbdx;
    }

    public String getAreacd() {
        return areacd;
    }

    public void setAreacd(String areacd) {
        this.areacd = areacd;
    }

    public String getAreana() {
        return areana;
    }

    public void setAreana(String areana) {
        this.areana = areana;
    }

    public String getDudate() {
        return dudate;
    }

    public void setDudate(String dudate) {
        this.dudate = dudate;
    }

    public String getRirank() {
        return rirank;
    }

    public void setRirank(String rirank) {
        this.rirank = rirank;
    }

    public String getCurren() {
        return curren;
    }

    public void setCurren(String curren) {
        this.curren = curren;
    }

    public BigDecimal getGuramt() {
        return guramt;
    }

    public void setGuramt(BigDecimal guramt) {
        this.guramt = guramt;
    }

    public String getRelend() {
        return relend;
    }

    public void setRelend(String relend) {
        this.relend = relend;
    }

    public String getEnsure() {
        return ensure;
    }

    public void setEnsure(String ensure) {
        this.ensure = ensure;
    }

    public String getGubore() {
        return gubore;
    }

    public void setGubore(String gubore) {
        this.gubore = gubore;
    }

    public String getIsguar() {
        return isguar;
    }

    public void setIsguar(String isguar) {
        this.isguar = isguar;
    }

    public String getGuartp() {
        return guartp;
    }

    public void setGuartp(String guartp) {
        this.guartp = guartp;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public BigDecimal getBilamt() {
        return bilamt;
    }

    public void setBilamt(BigDecimal bilamt) {
        this.bilamt = bilamt;
    }

    public String getStdate() {
        return stdate;
    }

    public void setStdate(String stdate) {
        this.stdate = stdate;
    }

    public String getEnddt() {
        return enddt;
    }

    public void setEnddt(String enddt) {
        this.enddt = enddt;
    }

    public String getDrname() {
        return drname;
    }

    public void setDrname(String drname) {
        this.drname = drname;
    }

    public String getDracid() {
        return dracid;
    }

    public void setDracid(String dracid) {
        this.dracid = dracid;
    }

    public String getRename() {
        return rename;
    }

    public void setRename(String rename) {
        this.rename = rename;
    }

    public String getDrbank() {
        return drbank;
    }

    public void setDrbank(String drbank) {
        this.drbank = drbank;
    }

    public String getDrbaid() {
        return drbaid;
    }

    public void setDrbaid(String drbaid) {
        this.drbaid = drbaid;
    }

    public String getReacid() {
        return reacid;
    }

    public void setReacid(String reacid) {
        this.reacid = reacid;
    }

    public String getAcpid() {
        return acpid;
    }

    public void setAcpid(String acpid) {
        this.acpid = acpid;
    }

    public String getAcpna() {
        return acpna;
    }

    public void setAcpna(String acpna) {
        this.acpna = acpna;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    public java.util.List<OwnerList> getOwnerList() {
        return ownerList;
    }

    public void setOwnerList(java.util.List<OwnerList> ownerList) {
        this.ownerList = ownerList;
    }

    @Override
    public String toString() {
        return "Xddb0003DataReqDto{" +
                "lserno='" + lserno + '\'' +
                ", iftssp='" + iftssp + '\'' +
                ", grtflg='" + grtflg + '\'' +
                ", iffadd='" + iffadd + '\'' +
                ", custyp='" + custyp + '\'' +
                ", maid='" + maid + '\'' +
                ", inbrid='" + inbrid + '\'' +
                ", indate='" + indate + '\'' +
                ", inid='" + inid + '\'' +
                ", mabrid='" + mabrid + '\'' +
                ", cusid='" + cusid + '\'' +
                ", cusna='" + cusna + '\'' +
                ", cetype='" + cetype + '\'' +
                ", cerno='" + cerno + '\'' +
                ", cfblx='" + cfblx + '\'' +
                ", flyxx='" + flyxx + '\'' +
                ", dzywxg='" + dzywxg + '\'' +
                ", dzypty='" + dzypty + '\'' +
                ", dzypbx='" + dzypbx + '\'' +
                ", jgbdx='" + jgbdx + '\'' +
                ", areacd='" + areacd + '\'' +
                ", areana='" + areana + '\'' +
                ", dudate='" + dudate + '\'' +
                ", rirank='" + rirank + '\'' +
                ", curren='" + curren + '\'' +
                ", guramt=" + guramt +
                ", relend='" + relend + '\'' +
                ", ensure='" + ensure + '\'' +
                ", gubore='" + gubore + '\'' +
                ", isguar='" + isguar + '\'' +
                ", guartp='" + guartp + '\'' +
                ", billno='" + billno + '\'' +
                ", bilamt=" + bilamt +
                ", stdate='" + stdate + '\'' +
                ", enddt='" + enddt + '\'' +
                ", drname='" + drname + '\'' +
                ", dracid='" + dracid + '\'' +
                ", rename='" + rename + '\'' +
                ", drbank='" + drbank + '\'' +
                ", drbaid='" + drbaid + '\'' +
                ", reacid='" + reacid + '\'' +
                ", acpid='" + acpid + '\'' +
                ", acpna='" + acpna + '\'' +
                ", rate=" + rate +
                ", list=" + list +
                ", ownerlist=" + ownerList +
                '}';
    }
}
