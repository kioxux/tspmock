package cn.com.yusys.yusp.dto.server.xdcz0008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 请求Data：代开保函
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0008DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarantAmt")
    private BigDecimal guarantAmt;//保函金额
    @JsonProperty(value = "startDate")
    private String startDate;//生效日期
    @JsonProperty(value = "endDate")
    private String endDate;//到期日
    @JsonProperty(value = "huser")
    private String huser;//经办人
    @JsonProperty(value = "openGuarantBankNo")
    private String openGuarantBankNo;//开立保函行号
    @JsonProperty(value = "openGuarantBankName")
    private String openGuarantBankName;//开立保函行名称
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户中文名称
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "guarantNo")
    private String guarantNo;//保函号码
    @JsonProperty(value = "oprType")
    private String oprType;//操作类型
    @JsonProperty(value = "limitType")
    private String limitType;//同业间额度类型

    public BigDecimal getGuarantAmt() {
        return guarantAmt;
    }

    public void setGuarantAmt(BigDecimal guarantAmt) {
        this.guarantAmt = guarantAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getHuser() {
        return huser;
    }

    public void setHuser(String huser) {
        this.huser = huser;
    }

    public String getOpenGuarantBankNo() {
        return openGuarantBankNo;
    }

    public void setOpenGuarantBankNo(String openGuarantBankNo) {
        this.openGuarantBankNo = openGuarantBankNo;
    }

    public String getOpenGuarantBankName() {
        return openGuarantBankName;
    }

    public void setOpenGuarantBankName(String openGuarantBankName) {
        this.openGuarantBankName = openGuarantBankName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getGuarantNo() {
        return guarantNo;
    }

    public void setGuarantNo(String guarantNo) {
        this.guarantNo = guarantNo;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getLimitType() {
        return limitType;
    }

    public void setLimitType(String limitType) {
        this.limitType = limitType;
    }

    @Override
    public String toString() {
        return "Xdcz0008DataReqDto{" +
                "guarantAmt='" + guarantAmt + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                "huser='" + huser + '\'' +
                "openGuarantBankNo='" + openGuarantBankNo + '\'' +
                "openGuarantBankName='" + openGuarantBankName + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "serno='" + serno + '\'' +
                "guarantNo='" + guarantNo + '\'' +
                "oprType='" + oprType + '\'' +
                "limitType='" + limitType + '\'' +
                '}';
    }
}
