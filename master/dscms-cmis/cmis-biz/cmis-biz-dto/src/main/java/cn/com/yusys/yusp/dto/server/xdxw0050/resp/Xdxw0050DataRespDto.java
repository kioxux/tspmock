package cn.com.yusys.yusp.dto.server.xdxw0050.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：资产负债表信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0050DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "curtAmtTotal")
	private String curtAmtTotal;//本期金额合计

	public String getCurtAmtTotal() {
		return curtAmtTotal;
	}

	public void setCurtAmtTotal(String curtAmtTotal) {
		this.curtAmtTotal = curtAmtTotal;
	}

	@Override
	public String toString() {
		return "Xdxw0050DataRespDto{" +
				"curtAmtTotal='" + curtAmtTotal + '\'' +
				'}';
	}
}
