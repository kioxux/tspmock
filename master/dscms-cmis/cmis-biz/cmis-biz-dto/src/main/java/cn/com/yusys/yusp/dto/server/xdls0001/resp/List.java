package cn.com.yusys.yusp.dto.server.xdls0001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：房贷要素查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "isFirstHouse")
    private String isFirstHouse;//是否首套住房
    @JsonProperty(value = "firstRepayDate")
    private String firstRepayDate;//首次还款日
    @JsonProperty(value = "term")
    private String term;//期限

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getIsFirstHouse() {
        return isFirstHouse;
    }

    public void setIsFirstHouse(String isFirstHouse) {
        this.isFirstHouse = isFirstHouse;
    }

    public String getFirstRepayDate() {
        return firstRepayDate;
    }

    public void setFirstRepayDate(String firstRepayDate) {
        this.firstRepayDate = firstRepayDate;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                ", isFirstHouse='" + isFirstHouse + '\'' +
                ", firstRepayDate='" + firstRepayDate + '\'' +
                ", term='" + term + '\'' +
                '}';
    }
}
