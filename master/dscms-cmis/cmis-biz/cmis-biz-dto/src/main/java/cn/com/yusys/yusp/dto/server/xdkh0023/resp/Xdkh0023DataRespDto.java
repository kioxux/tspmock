package cn.com.yusys.yusp.dto.server.xdkh0023.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：还款试算计划查询日期
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0023DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "repayDate")
    private String repayDate;//还款日
    @JsonProperty(value = "dueFlag")
    private String dueFlag;// 返回标识


    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(String repayDate) {
        this.repayDate = repayDate;
    }

    public String getDueFlag() {
        return dueFlag;
    }

    public void setDueFlag(String dueFlag) {
        this.dueFlag = dueFlag;
    }

    @Override
    public String toString() {
        return "Xdkh0023DataRespDto{" +
                "cusName='" + cusName + '\'' +
                ", repayDate='" + repayDate + '\'' +
                ", dueFlag='" + repayDate + '\'' +
                '}';
    }
}
