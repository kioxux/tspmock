package cn.com.yusys.yusp.dto.server.xddb0017.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：获取抵押登记双录音视频信息列表
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0017DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarno")
    private String guarno;//抵押物编号

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    @Override
    public String toString() {
        return "Xddb0017DataReqDto{" +
                "guarno='" + guarno + '\'' +
                '}';
    }
}
