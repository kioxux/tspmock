package cn.com.yusys.yusp.dto.server.xddh0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：提前还款
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0005DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "repayType")
    private String repayType;//还款模式
    @JsonProperty(value = "repayAmt")
    private BigDecimal repayAmt;//还款金额
    @JsonProperty(value = "repayAcctNo")
    private String repayAcctNo;//还款账号
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "repayTotlCap")
    private BigDecimal repayTotlCap;//还款总本金
    @JsonProperty(value = "tranSerno")
    private String tranSerno;//交易流水号
    @JsonProperty(value = "fstRepayMode")
    private String fstRepayMode;//一级还款模式
    @JsonProperty(value = "sedRepayMode")
    private String sedRepayMode;//二级还款模式

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public BigDecimal getRepayAmt() {
        return repayAmt;
    }

    public void setRepayAmt(BigDecimal repayAmt) {
        this.repayAmt = repayAmt;
    }

    public String getRepayAcctNo() {
        return repayAcctNo;
    }

    public void setRepayAcctNo(String repayAcctNo) {
        this.repayAcctNo = repayAcctNo;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getRepayTotlCap() {
        return repayTotlCap;
    }

    public void setRepayTotlCap(BigDecimal repayTotlCap) {
        this.repayTotlCap = repayTotlCap;
    }

    public String getTranSerno() {
        return tranSerno;
    }

    public void setTranSerno(String tranSerno) {
        this.tranSerno = tranSerno;
    }

    public String getFstRepayMode() {
        return fstRepayMode;
    }

    public void setFstRepayMode(String fstRepayMode) {
        this.fstRepayMode = fstRepayMode;
    }

    public String getSedRepayMode() {
        return sedRepayMode;
    }

    public void setSedRepayMode(String sedRepayMode) {
        this.sedRepayMode = sedRepayMode;
    }

    @Override
    public String toString() {
        return "Xddh0005DataReqDto{" +
                "billNo='" + billNo + '\'' +
                ", repayType='" + repayType + '\'' +
                ", repayAmt=" + repayAmt +
                ", repayAcctNo='" + repayAcctNo + '\'' +
                ", curType='" + curType + '\'' +
                ", repayTotlCap=" + repayTotlCap +
                ", tranSerno='" + tranSerno + '\'' +
                '}';
    }
}
