package cn.com.yusys.yusp.dto.server.xdht0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：信贷系统信贷合同生成接口
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0023DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certid")
    private String certid;//证件号
    @JsonProperty(value = "biz_type")
    private String biz_type;//贷款类型
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//申请金额
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//合同开始日
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//合同截至日
    @JsonProperty(value = "rate")
    private BigDecimal rate;//利率
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "discount_code")
    private String discount_code;//优惠券号

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getDiscount_code() {
        return discount_code;
    }

    public void setDiscount_code(String discount_code) {
        this.discount_code = discount_code;
    }

    @Override
    public String toString() {
        return "Xdht0023DataReqDto{" +
                "certid='" + certid + '\'' +
                "biz_type='" + biz_type + '\'' +
                "apply_amount='" + apply_amount + '\'' +
                "loan_start_date='" + loan_start_date + '\'' +
                "loan_end_date='" + loan_end_date + '\'' +
                "rate='" + rate + '\'' +
                "cus_id='" + cus_id + '\'' +
                "discount_code='" + discount_code + '\'' +
                '}';
    }
}
