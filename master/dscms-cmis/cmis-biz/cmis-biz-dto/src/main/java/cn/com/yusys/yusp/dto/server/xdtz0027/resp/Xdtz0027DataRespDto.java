package cn.com.yusys.yusp.dto.server.xdtz0027.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0027DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "isHavingLoan")
	private String isHavingLoan;//是否有贷款指标值

	public String getIsHavingLoan() {
		return isHavingLoan;
	}

	public void setIsHavingLoan(String isHavingLoan) {
		this.isHavingLoan = isHavingLoan;
	}

	@Override
	public String toString() {
		return "Xdtz0027DataRespDto{" +
				"isHavingLoan='" + isHavingLoan + '\'' +
				'}';
	}
}
