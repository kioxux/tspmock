package cn.com.yusys.yusp.dto.server.xdtz0056.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询还款业务类型
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0056DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billno")
    private String billno;//借据号
    @JsonProperty(value = "cusid")
    private String cusid;//客户号

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getCusid() {
        return cusid;
    }

    public void setCusid(String cusid) {
        this.cusid = cusid;
    }

    @Override
    public String toString() {
        return "Xdtz0056DataReqDto{" +
                "billno='" + billno + '\'' +
                ", cusid='" + cusid + '\'' +
                '}';
    }
}
