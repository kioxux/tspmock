package cn.com.yusys.yusp.dto.server.xdtz0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @Author zhangpeng
 * @Date 2021/5/7 10:03
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class BizList {

    @JsonProperty(value = "bizType")
    private String bizType;//业务类型

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    @Override
    public String toString() {
        return "BizList{" +
                "bizType='" + bizType + '\'' +
                '}';
    }
}
