package cn.com.yusys.yusp.dto.server.xdsx0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 14:55:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/19 14:55
 * @since 2021/5/19 14:55
 */
@JsonPropertyOrder(alphabetic = true)
public class ListGrt implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "grt_serno")
    private String grt_serno;//抵质押物编号/保证群编号
    @JsonProperty(value = "grt_flag")
    private String grt_flag;//担保标志
    @JsonProperty(value = "flag_serno")
    private String flag_serno;//标志字段

    public String getGrt_serno() {
        return grt_serno;
    }

    public void setGrt_serno(String grt_serno) {
        this.grt_serno = grt_serno;
    }

    public String getGrt_flag() {
        return grt_flag;
    }

    public void setGrt_flag(String grt_flag) {
        this.grt_flag = grt_flag;
    }

    public String getFlag_serno() {
        return flag_serno;
    }

    public void setFlag_serno(String flag_serno) {
        this.flag_serno = flag_serno;
    }

    @Override
    public String toString() {
        return "ListGrt{" +
                "grt_serno='" + grt_serno + '\'' +
                ", grt_flag='" + grt_flag + '\'' +
                ", flag_serno='" + flag_serno + '\'' +
                '}';
    }
}
