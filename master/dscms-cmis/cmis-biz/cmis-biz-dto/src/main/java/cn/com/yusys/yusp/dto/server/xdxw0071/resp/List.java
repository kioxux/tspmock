package cn.com.yusys.yusp.dto.server.xdxw0071.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：授信侧面调查信息查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "Serno")
    private String Serno;//主键
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//客户调查表编号
    @JsonProperty(value = "pldType")
    private String pldType;//抵押物类型
    @JsonProperty(value = "owner")
    private String owner;//所有权人
    @JsonProperty(value = "addr")
    private String addr;//地址
    @JsonProperty(value = "squ")
    private BigDecimal squ;//面积
    @JsonProperty(value = "assetAmt")
    private BigDecimal assetAmt;//评估金额
    @JsonProperty(value = "loanAmount")
    private BigDecimal loanAmount;//该抵押物项下贷款总金额
    @JsonProperty(value = "pldRate")
    private BigDecimal pldRate;//抵押率
    @JsonProperty(value = "creditMoney")
    private BigDecimal creditMoney;//一抵金额
    @JsonProperty(value = "creditBalance")
    private BigDecimal creditBalance;//一抵余额
    @JsonProperty(value = "useCase")
    private String useCase;//使用情况
    @JsonProperty(value = "pldName")
    private String pldName;//抵押品名称
    @JsonProperty(value = "pldCloudAssetSerno")
    private String pldCloudAssetSerno;//抵押物云估价主键
    @JsonProperty(value = "schoolHouse")
    private String schoolHouse;//学区房
    @JsonProperty(value = "landCha")
    private String landCha;//土地性质
    @JsonProperty(value = "houseCha")
    private String houseCha;//房屋性质
    @JsonProperty(value = "estateName")
    private String estateName;//小区名称
    @JsonProperty(value = "isSupSchoolHouse")
    private String isSupSchoolHouse;//是否一级学区房


    public String getSerno() {
        return Serno;
    }

    public void setSerno(String Serno) {
        this.Serno = Serno;
    }

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getPldType() {
        return pldType;
    }

    public void setPldType(String pldType) {
        this.pldType = pldType;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public BigDecimal getSqu() {
        return squ;
    }

    public void setSqu(BigDecimal squ) {
        this.squ = squ;
    }

    public BigDecimal getAssetAmt() {
        return assetAmt;
    }

    public void setAssetAmt(BigDecimal assetAmt) {
        this.assetAmt = assetAmt;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public BigDecimal getPldRate() {
        return pldRate;
    }

    public void setPldRate(BigDecimal pldRate) {
        this.pldRate = pldRate;
    }

    public BigDecimal getCreditMoney() {
        return creditMoney;
    }

    public void setCreditMoney(BigDecimal creditMoney) {
        this.creditMoney = creditMoney;
    }

    public BigDecimal getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(BigDecimal creditBalance) {
        this.creditBalance = creditBalance;
    }

    public String getUseCase() {
        return useCase;
    }

    public void setUseCase(String useCase) {
        this.useCase = useCase;
    }

    public String getPldName() {
        return pldName;
    }

    public void setPldName(String pldName) {
        this.pldName = pldName;
    }

    public String getPldCloudAssetSerno() {
        return pldCloudAssetSerno;
    }

    public void setPldCloudAssetSerno(String pldCloudAssetSerno) {
        this.pldCloudAssetSerno = pldCloudAssetSerno;
    }

    public String getSchoolHouse() {
        return schoolHouse;
    }

    public void setSchoolHouse(String schoolHouse) {
        this.schoolHouse = schoolHouse;
    }

    public String getLandCha() {
        return landCha;
    }

    public void setLandCha(String landCha) {
        this.landCha = landCha;
    }

    public String getHouseCha() {
        return houseCha;
    }

    public void setHouseCha(String houseCha) {
        this.houseCha = houseCha;
    }

    public String getEstateName() {
        return estateName;
    }

    public void setEstateName(String estateName) {
        this.estateName = estateName;
    }

    public String getIsSupSchoolHouse() {
        return isSupSchoolHouse;
    }

    public void setIsSupSchoolHouse(String isSupSchoolHouse) {
        this.isSupSchoolHouse = isSupSchoolHouse;
    }

    @Override
    public String toString() {
        return "List{" +
                "Serno='" + Serno + '\'' +
                ", indgtSerno='" + indgtSerno + '\'' +
                ", pldType='" + pldType + '\'' +
                ", owner='" + owner + '\'' +
                ", addr='" + addr + '\'' +
                ", squ=" + squ +
                ", assetAmt=" + assetAmt +
                ", loanAmount=" + loanAmount +
                ", pldRate=" + pldRate +
                ", creditMoney=" + creditMoney +
                ", creditBalance=" + creditBalance +
                ", useCase='" + useCase + '\'' +
                ", pldName='" + pldName + '\'' +
                ", pldCloudAssetSerno='" + pldCloudAssetSerno + '\'' +
                ", schoolHouse='" + schoolHouse + '\'' +
                ", landCha='" + landCha + '\'' +
                ", houseCha='" + houseCha + '\'' +
                ", estateName='" + estateName + '\'' +
                ", isSupSchoolHouse='" + isSupSchoolHouse + '\'' +
                '}';
    }
}
