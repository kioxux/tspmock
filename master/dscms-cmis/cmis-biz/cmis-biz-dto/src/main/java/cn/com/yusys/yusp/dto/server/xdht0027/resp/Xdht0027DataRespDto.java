package cn.com.yusys.yusp.dto.server.xdht0027.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据客户调查表编号取得贷款合同主表的合同状态
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0027DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态

    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//合同签订利率

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    @Override
    public String toString() {
        return "Xdht0027DataRespDto{" +
                "contStatus='" + contStatus + '\'' +
                "realityIrY='" + realityIrY + '\'' +
                '}';
    }
}
