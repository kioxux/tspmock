package cn.com.yusys.yusp.dto.server.xdxw0065.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：调查报告审批结果信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0065DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//流水号

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    @Override
    public String toString() {
        return "Xdxw0065DataReqDto{" +
                "cert_code='" + cert_code + '\'' +
                ", survey_serno='" + survey_serno + '\'' +
                '}';
    }
}
