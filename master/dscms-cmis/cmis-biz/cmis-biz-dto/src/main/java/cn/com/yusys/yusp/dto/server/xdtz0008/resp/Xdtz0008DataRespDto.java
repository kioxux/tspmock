package cn.com.yusys.yusp.dto.server.xdtz0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据客户号获取正常周转次数
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0008DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "turnovTimes")
    private String turnovTimes;//周转次数

    public String getTurnovTimes() {
        return turnovTimes;
    }

    public void setTurnovTimes(String turnovTimes) {
        this.turnovTimes = turnovTimes;
    }

    @Override
    public String toString() {
        return "Xdtz0008DataRespDto{" +
                "turnovTimes='" + turnovTimes + '\'' +
                '}';
    }
}
