package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class GuarCertiRelaClientDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 主键 **/
    private String pkId;

    /** 押品统一编号 **/
    private String guarNo;

    /** 权证类型 STD_ZB_CERTI_TYPE_CD **/
    private String certiTypeCd;

    /** 权利凭证号 **/
    private String certiRecordId;

    /** 权证类别 STD_ZB_CERTI_CATALOG  **/
    private String certiCatalog;

    /** 权证发证机关名称 STD_ZB_ **/
    private String certiOrgName;

    /** 权证发证日期 **/
    private String certiStartDate;

    /** 权证到期日期 **/
    private String certiEndDate;

    /** 权证入库日期 **/
    private String inDate;

    /** 权证正常出库日期 **/
    private String outDate;

    /** 权证临时借用人名称 **/
    private String tempBorrowerName;

    /** 权证临时出库日期 **/
    private String tempOutDate;

    /** 权证预计归还时间 **/
    private String preBackDate;

    /** 权证实际归还日期 **/
    private String realBackDate;

    /** 权证临时出库原因 **/
    private String tempOutReason;

    /** 权证状态 STD_ZB_CERTI_STATE **/
    private String certiState;

    /** 保管机构 **/
    private String depositOrgNo;

    /** 账务机构 **/
    private String finaBrId;

    /** 权证备注信息 **/
    private String certiComment;

    /** 权证续期原因 **/
    private String extReason;

    /** 权利金额 **/
    private java.math.BigDecimal certiAmt;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 操作类型  **/
    private String oprType;

    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param guarNo
     */
    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo == null ? null : guarNo.trim();
    }

    /**
     * @return GuarNo
     */
    public String getGuarNo() {
        return this.guarNo;
    }

    /**
     * @param certiTypeCd
     */
    public void setCertiTypeCd(String certiTypeCd) {
        this.certiTypeCd = certiTypeCd == null ? null : certiTypeCd.trim();
    }

    /**
     * @return CertiTypeCd
     */
    public String getCertiTypeCd() {
        return this.certiTypeCd;
    }

    /**
     * @param certiRecordId
     */
    public void setCertiRecordId(String certiRecordId) {
        this.certiRecordId = certiRecordId == null ? null : certiRecordId.trim();
    }

    /**
     * @return CertiRecordId
     */
    public String getCertiRecordId() {
        return this.certiRecordId;
    }

    /**
     * @param certiCatalog
     */
    public void setCertiCatalog(String certiCatalog) {
        this.certiCatalog = certiCatalog == null ? null : certiCatalog.trim();
    }

    /**
     * @return CertiCatalog
     */
    public String getCertiCatalog() {
        return this.certiCatalog;
    }

    /**
     * @param certiOrgName
     */
    public void setCertiOrgName(String certiOrgName) {
        this.certiOrgName = certiOrgName == null ? null : certiOrgName.trim();
    }

    /**
     * @return CertiOrgName
     */
    public String getCertiOrgName() {
        return this.certiOrgName;
    }

    /**
     * @param certiStartDate
     */
    public void setCertiStartDate(String certiStartDate) {
        this.certiStartDate = certiStartDate == null ? null : certiStartDate.trim();
    }

    /**
     * @return CertiStartDate
     */
    public String getCertiStartDate() {
        return this.certiStartDate;
    }

    /**
     * @param certiEndDate
     */
    public void setCertiEndDate(String certiEndDate) {
        this.certiEndDate = certiEndDate == null ? null : certiEndDate.trim();
    }

    /**
     * @return CertiEndDate
     */
    public String getCertiEndDate() {
        return this.certiEndDate;
    }

    /**
     * @param inDate
     */
    public void setInDate(String inDate) {
        this.inDate = inDate == null ? null : inDate.trim();
    }

    /**
     * @return InDate
     */
    public String getInDate() {
        return this.inDate;
    }

    /**
     * @param outDate
     */
    public void setOutDate(String outDate) {
        this.outDate = outDate == null ? null : outDate.trim();
    }

    /**
     * @return OutDate
     */
    public String getOutDate() {
        return this.outDate;
    }

    /**
     * @param tempBorrowerName
     */
    public void setTempBorrowerName(String tempBorrowerName) {
        this.tempBorrowerName = tempBorrowerName == null ? null : tempBorrowerName.trim();
    }

    /**
     * @return TempBorrowerName
     */
    public String getTempBorrowerName() {
        return this.tempBorrowerName;
    }

    /**
     * @param tempOutDate
     */
    public void setTempOutDate(String tempOutDate) {
        this.tempOutDate = tempOutDate == null ? null : tempOutDate.trim();
    }

    /**
     * @return TempOutDate
     */
    public String getTempOutDate() {
        return this.tempOutDate;
    }

    /**
     * @param preBackDate
     */
    public void setPreBackDate(String preBackDate) {
        this.preBackDate = preBackDate == null ? null : preBackDate.trim();
    }

    /**
     * @return PreBackDate
     */
    public String getPreBackDate() {
        return this.preBackDate;
    }

    /**
     * @param realBackDate
     */
    public void setRealBackDate(String realBackDate) {
        this.realBackDate = realBackDate == null ? null : realBackDate.trim();
    }

    /**
     * @return RealBackDate
     */
    public String getRealBackDate() {
        return this.realBackDate;
    }

    /**
     * @param tempOutReason
     */
    public void setTempOutReason(String tempOutReason) {
        this.tempOutReason = tempOutReason == null ? null : tempOutReason.trim();
    }

    /**
     * @return TempOutReason
     */
    public String getTempOutReason() {
        return this.tempOutReason;
    }

    /**
     * @param certiState
     */
    public void setCertiState(String certiState) {
        this.certiState = certiState == null ? null : certiState.trim();
    }

    /**
     * @return CertiState
     */
    public String getCertiState() {
        return this.certiState;
    }

    /**
     * @param depositOrgNo
     */
    public void setDepositOrgNo(String depositOrgNo) {
        this.depositOrgNo = depositOrgNo == null ? null : depositOrgNo.trim();
    }

    /**
     * @return DepositOrgNo
     */
    public String getDepositOrgNo() {
        return this.depositOrgNo;
    }

    /**
     * @param finaBrId
     */
    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId == null ? null : finaBrId.trim();
    }

    /**
     * @return FinaBrId
     */
    public String getFinaBrId() {
        return this.finaBrId;
    }

    /**
     * @param certiComment
     */
    public void setCertiComment(String certiComment) {
        this.certiComment = certiComment == null ? null : certiComment.trim();
    }

    /**
     * @return CertiComment
     */
    public String getCertiComment() {
        return this.certiComment;
    }

    /**
     * @param extReason
     */
    public void setExtReason(String extReason) {
        this.extReason = extReason == null ? null : extReason.trim();
    }

    /**
     * @return ExtReason
     */
    public String getExtReason() {
        return this.extReason;
    }

    /**
     * @param certiAmt
     */
    public void setCertiAmt(java.math.BigDecimal certiAmt) {
        this.certiAmt = certiAmt;
    }

    /**
     * @return CertiAmt
     */
    public java.math.BigDecimal getCertiAmt() {
        return this.certiAmt;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param oprType
     */
    public void setOprType(String oprType) {
        this.oprType = oprType == null ? null : oprType.trim();
    }

    /**
     * @return OprType
     */
    public String getOprType() {
        return this.oprType;
    }
    
    
    private List<Map> list;
    
    public List<Map> getList() {
        return list;
    }
    
    public void setList(List<Map> list) {
        this.list = list;
    }
    
    private String opeType;//入参判断权证出入库申请操作类型
    
    public String getOpeType() {
        return opeType;
    }
    
    public void setOpeType(String opeType) {
        this.opeType = opeType;
    }
}
