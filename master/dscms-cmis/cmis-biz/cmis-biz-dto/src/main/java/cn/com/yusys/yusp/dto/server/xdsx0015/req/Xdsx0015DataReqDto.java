package cn.com.yusys.yusp.dto.server.xdsx0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 请求Data：已批复的授信申请信息、押品信息以及合同信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0015DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "op_flag")
    private String op_flag;//类型(非必输)
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户
    // @NotNull(message = "客户号cus_id不能为空")
    //    @NotEmpty(message = "客户号cus_id不能为空")号
    @JsonProperty(value = "cus_name")
//    @NotNull(message = "客户名称cus_name不能为空")
//    @NotEmpty(message = "客户名称cus_name不能为空")
    private String cus_name;//客户名称
    @JsonProperty(value = "cur_type")
//    @NotNull(message = "币种cur_type不能为空")
//    @NotEmpty(message = "币种cur_type不能为空")
    private String cur_type;//币种
    @JsonProperty(value = "app_crd_totl_amt")
//    @NotNull(message = "循环授信敞口额度app_crd_totl_amt不能为空")
//    @NotEmpty(message = "循环授信敞口额度app_crd_totl_amt不能为空")
    private String app_crd_totl_amt;//循环授信敞口额度
    @JsonProperty(value = "app_temp_crd_totl_amt")
//    @NotNull(message = "临时授信敞口额额度app_temp_crd_totl_amt不能为空")
//    @NotEmpty(message = "临时授信敞口额额度app_temp_crd_totl_amt不能为空")
    private String app_temp_crd_totl_amt;//临时授信敞口额额度
    @JsonProperty(value = "crd_totl_sum_amt")
//    @NotNull(message = "授信总额crd_totl_sum_amt不能为空")
//    @NotEmpty(message = "授信总额crd_totl_sum_amt不能为空")
    private String crd_totl_sum_amt;//授信总额
    @JsonProperty(value = "apply_date")
//    @NotNull(message = "授信起始日apply_date不能为空")
//    @NotEmpty(message = "授信起始日apply_date不能为空")
    private String apply_date;//授信起始日
    @JsonProperty(value = "over_date")
//    @NotNull(message = "授信到期日over_date不能为空")
//    @NotEmpty(message = "授信到期日over_date不能为空")
    private String over_date;//授信到期日
    @JsonProperty(value = "delay_months")
//    @NotNull(message = "宽限期（月）delay_months不能为空")
//    @NotEmpty(message = "宽限期（月）delay_months不能为空")
    private String delay_months;//宽限期（月）
    @JsonProperty(value = "inpret_val")
    private String inpret_val;//数字解读值
    @JsonProperty(value = "inpret_val_risk_lvl")
    private String inpret_val_risk_lvl;//数字解读值风险等级
    @JsonProperty(value = "complex_risk_lvl")
//    @NotNull(message = "综合风险等级complex_risk_lvl不能为空")
//    @NotEmpty(message = "综合风险等级complex_risk_lvl不能为空")
    private String complex_risk_lvl;//综合风险等级
    @JsonProperty(value = "lmt_advice")
//    @NotNull(message = "额度建议lmt_advice不能为空")
//    @NotEmpty(message = "额度建议lmt_advice不能为空")
    private String lmt_advice;//额度建议
    @JsonProperty(value = "input_date")
//    @NotNull(message = "录入时间input_date不能为空")
//    @NotEmpty(message = "录入时间input_date不能为空")
    private String input_date;//录入时间
    @JsonProperty(value = "manager_id")
//    @NotNull(message = "管户客户经理号manager_id不能为空")
//    @NotEmpty(message = "管户客户经理号manager_id不能为空")
    private String manager_id;//管户客户经理号
    @JsonProperty(value = "fdydd_reality_ir")
//    @NotNull(message = "省心快贷利率fdydd_reality_ir不能为空")
//    @NotEmpty(message = "省心快贷利率fdydd_reality_ir不能为空")
    private String fdydd_reality_ir;//省心快贷利率
    @JsonProperty(value = "manager_br_id")
//    @NotNull(message = "管户机构manager_br_id不能为空")
//    @NotEmpty(message = "管户机构manager_br_id不能为空")
    private String manager_br_id;//管户机构
    @JsonProperty(value = "assure_means_main")
//    @NotNull(message = "合同担保方式assure_means_main不能为空")
//    @NotEmpty(message = "合同担保方式assure_means_main不能为空")
    private String assure_means_main;//合同担保方式
    @JsonProperty(value = "yx_serno")
//    @NotNull(message = "影像流水号yx_serno不能为空")
//    @NotEmpty(message = "影像流水号yx_serno不能为空")
    private String yx_serno;//影像流水号
    @JsonProperty(value = "fx_serno")
    private String fx_serno;//授信协议分项编号
    @JsonProperty(value = "fx_type")
    private String fx_type;//授信协议分项类型
    @JsonProperty(value = "list_db")
    @NotNull(message = "list_db不能为空")
    @NotEmpty(message = "list_db不能为空")
    private List<ListDb> listDb;
    @JsonProperty(value = "list_dy")
    @NotNull(message = "list_dy不能为空")
    @NotEmpty(message = "list_dy不能为空")
    private List<ListDy> listDy;
    @JsonProperty(value = "list_fx")
    @NotNull(message = "list_fx不能为空")
    @NotEmpty(message = "list_fx不能为空")
    private List<ListFx> listFx;
    @JsonProperty(value = "list_grt")
    @NotNull(message = "list_grt不能为空")
    @NotEmpty(message = "list_grt不能为空")
    private List<ListGrt> listGrt;
    @JsonProperty(value = "list_yw")
    @NotNull(message = "list_yw不能为空")
    @NotEmpty(message = "list_yw不能为空")
    private List<ListYw> listYw;

    public String getOp_flag() {
        return op_flag;
    }

    public void setOp_flag(String op_flag) {
        this.op_flag = op_flag;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public String getApp_crd_totl_amt() {
        return app_crd_totl_amt;
    }

    public void setApp_crd_totl_amt(String app_crd_totl_amt) {
        this.app_crd_totl_amt = app_crd_totl_amt;
    }

    public String getApp_temp_crd_totl_amt() {
        return app_temp_crd_totl_amt;
    }

    public void setApp_temp_crd_totl_amt(String app_temp_crd_totl_amt) {
        this.app_temp_crd_totl_amt = app_temp_crd_totl_amt;
    }

    public String getCrd_totl_sum_amt() {
        return crd_totl_sum_amt;
    }

    public void setCrd_totl_sum_amt(String crd_totl_sum_amt) {
        this.crd_totl_sum_amt = crd_totl_sum_amt;
    }

    public String getApply_date() {
        return apply_date;
    }

    public void setApply_date(String apply_date) {
        this.apply_date = apply_date;
    }

    public String getOver_date() {
        return over_date;
    }

    public void setOver_date(String over_date) {
        this.over_date = over_date;
    }

    public String getDelay_months() {
        return delay_months;
    }

    public void setDelay_months(String delay_months) {
        this.delay_months = delay_months;
    }

    public String getInpret_val() {
        return inpret_val;
    }

    public void setInpret_val(String inpret_val) {
        this.inpret_val = inpret_val;
    }

    public String getInpret_val_risk_lvl() {
        return inpret_val_risk_lvl;
    }

    public void setInpret_val_risk_lvl(String inpret_val_risk_lvl) {
        this.inpret_val_risk_lvl = inpret_val_risk_lvl;
    }

    public String getComplex_risk_lvl() {
        return complex_risk_lvl;
    }

    public void setComplex_risk_lvl(String complex_risk_lvl) {
        this.complex_risk_lvl = complex_risk_lvl;
    }

    public String getLmt_advice() {
        return lmt_advice;
    }

    public void setLmt_advice(String lmt_advice) {
        this.lmt_advice = lmt_advice;
    }

    public String getInput_date() {
        return input_date;
    }

    public void setInput_date(String input_date) {
        this.input_date = input_date;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getFdydd_reality_ir() {
        return fdydd_reality_ir;
    }

    public void setFdydd_reality_ir(String fdydd_reality_ir) {
        this.fdydd_reality_ir = fdydd_reality_ir;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getAssure_means_main() {
        return assure_means_main;
    }

    public void setAssure_means_main(String assure_means_main) {
        this.assure_means_main = assure_means_main;
    }

    public String getYx_serno() {
        return yx_serno;
    }

    public void setYx_serno(String yx_serno) {
        this.yx_serno = yx_serno;
    }

    public String getFx_serno() {
        return fx_serno;
    }

    public void setFx_serno(String fx_serno) {
        this.fx_serno = fx_serno;
    }

    public String getFx_type() {
        return fx_type;
    }

    public void setFx_type(String fx_type) {
        this.fx_type = fx_type;
    }

    public List<ListDb> getListDb() {
        return listDb;
    }

    public void setListDb(List<ListDb> listDb) {
        this.listDb = listDb;
    }

    public List<ListDy> getListDy() {
        return listDy;
    }

    public void setListDy(List<ListDy> listDy) {
        this.listDy = listDy;
    }

    public List<ListFx> getListFx() {
        return listFx;
    }

    public void setListFx(List<ListFx> listFx) {
        this.listFx = listFx;
    }

    public List<ListGrt> getListGrt() {
        return listGrt;
    }

    public void setListGrt(List<ListGrt> listGrt) {
        this.listGrt = listGrt;
    }

    public List<ListYw> getListYw() {
        return listYw;
    }

    public void setListYw(List<ListYw> listYw) {
        this.listYw = listYw;
    }

    @Override
    public String toString() {
        return "Xdsx0015DataReqDto{" +
                "op_flag='" + op_flag + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", app_crd_totl_amt='" + app_crd_totl_amt + '\'' +
                ", app_temp_crd_totl_amt='" + app_temp_crd_totl_amt + '\'' +
                ", crd_totl_sum_amt='" + crd_totl_sum_amt + '\'' +
                ", apply_date='" + apply_date + '\'' +
                ", over_date='" + over_date + '\'' +
                ", delay_months='" + delay_months + '\'' +
                ", inpret_val='" + inpret_val + '\'' +
                ", inpret_val_risk_lvl='" + inpret_val_risk_lvl + '\'' +
                ", complex_risk_lvl='" + complex_risk_lvl + '\'' +
                ", lmt_advice='" + lmt_advice + '\'' +
                ", input_date='" + input_date + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", fdydd_reality_ir='" + fdydd_reality_ir + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", assure_means_main='" + assure_means_main + '\'' +
                ", yx_serno='" + yx_serno + '\'' +
                ", fx_serno='" + fx_serno + '\'' +
                ", fx_type='" + fx_type + '\'' +
                ", listDb=" + listDb +
                ", listDy=" + listDy +
                ", listFx=" + listFx +
                ", listGrt=" + listGrt +
                ", listYw=" + listYw +
                '}';
    }
}
