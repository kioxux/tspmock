package cn.com.yusys.yusp.dto.server.xdtz0043.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：统计客户行内信用类贷款余额
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0043DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cdtLoanBal")
    private BigDecimal cdtLoanBal;//信用类贷款余额

    public Xdtz0043DataRespDto() {
    }

    public Xdtz0043DataRespDto(BigDecimal cdtLoanBal) {
        this.cdtLoanBal = cdtLoanBal;
    }

    public BigDecimal getCdtLoanBal() {
        return cdtLoanBal;
    }

    public void setCdtLoanBal(BigDecimal cdtLoanBal) {
        this.cdtLoanBal = cdtLoanBal;
    }

    @Override
    public String toString() {
        return "Xdtz0043DataRespDto{" +
                "cdtLoanBal=" + cdtLoanBal +
                '}';
    }
}
