package cn.com.yusys.yusp.dto.server.xdxw0030.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据云估计流水号查询房屋信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0030DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "ygjSerno")
    private String ygjSerno;//云估计流水号
    @JsonProperty(value = "qryId")
    private String qryId;//云估计流水号
    @JsonProperty(value = "phone")
    private String phone;//电话

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getYgjSerno() {
        return ygjSerno;
    }

    public void setYgjSerno(String ygjSerno) {
        this.ygjSerno = ygjSerno;
    }

    public String getQryId() {
        return qryId;
    }

    public void setQryId(String qryId) {
        this.qryId = qryId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Xdxw0030DataReqDto{" +
                "queryType='" + queryType + '\'' +
                ", ygjSerno='" + ygjSerno + '\'' +
                ", qryId='" + qryId + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
