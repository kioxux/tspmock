package cn.com.yusys.yusp.dto.server.xdxw0083.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;

/**
 * 请求Data：优企贷还款账号变更
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0083DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "repayAcctNo")
    private String repayAcctNo;//还款账号变更

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRepayAcctNo() {
        return repayAcctNo;
    }

    public void setRepayAcctNo(String repayAcctNo) {
        this.repayAcctNo = repayAcctNo;
    }

    @Override
    public String toString() {
        return "Xdxw0083DataReqDto{" +
                "billNo='" + billNo + '\'' +
                "repayAcctNo='" + repayAcctNo + '\'' +
                '}';
    }
}
