package cn.com.yusys.yusp.dto.server.xdxw0029.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据客户号查询现有融资总额、总余额、担保方式
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0029DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "list")
	private java.util.List<List> list;


	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Xdxw0029DataRespDto{" +
				"list=" + list +
				'}';
	}
}
