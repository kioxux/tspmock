package cn.com.yusys.yusp.dto.server.xdtz0050.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 请求Data：对私客户关联业务检查
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0050DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    @NotBlank(message = "传入字段cusId不得为空")
    private String cusId;//客户号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdtz0050DataReqDto{" +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
