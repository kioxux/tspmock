package cn.com.yusys.yusp.dto.server.xdls0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询信贷有无授信历史
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdls0004DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHavingCrdHis")
    private String isHavingCrdHis;//信贷是否有授信历史

    public String getIsHavingCrdHis() {
        return isHavingCrdHis;
    }

    public void setIsHavingCrdHis(String isHavingCrdHis) {
        this.isHavingCrdHis = isHavingCrdHis;
    }

	@Override
	public String toString() {
		return "Xdls0004DataRespDto{" +
				"isHavingCrdHis='" + isHavingCrdHis + '\'' +
				'}';
	}
}
