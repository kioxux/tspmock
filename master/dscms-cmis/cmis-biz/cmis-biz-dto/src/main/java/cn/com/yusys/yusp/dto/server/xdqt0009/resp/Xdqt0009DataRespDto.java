package cn.com.yusys.yusp.dto.server.xdqt0009.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应dto：微业贷信贷文件处理通知
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdqt0009DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;


	@Override
	public String toString() {
		return "Xdqt0009DataRespDto{}";
	}
}
