package cn.com.yusys.yusp.dto.server.xdtz0050.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：对私客户关联业务检查
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0050DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "deletableFlag")
    private String deletableFlag;//可删除标志
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getDeletableFlag() {
        return deletableFlag;
    }

    public void setDeletableFlag(String deletableFlag) {
        this.deletableFlag = deletableFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xdtz0050DataRespDto{" +
                "deletableFlag='" + deletableFlag + '\'' +
                ", opMsg='" + opMsg + '\'' +
                '}';
    }
}
