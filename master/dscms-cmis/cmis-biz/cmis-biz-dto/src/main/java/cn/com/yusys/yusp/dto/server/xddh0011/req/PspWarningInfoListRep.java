package cn.com.yusys.yusp.dto.server.xddh0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：定期检查借据任务
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class PspWarningInfoListRep implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 主键 **/
    @JsonProperty(value = "pkId")
    private String pkId;

    /** 任务编号 **/
    @JsonProperty(value = "taskNo")
    private String taskNo;

    /** 预警单编号 **/
    @JsonProperty(value = "altSinNo")
    private String altSinNo;

    /** 预警日期 **/
    @JsonProperty(value = "altDate")
    private String altDate;
    /** 信息来源 **/
    @JsonProperty(value = "infoSource")
    private String infoSource;

    /** 预警大类 **/
    @JsonProperty(value = "altTypeMax")
    private String altTypeMax;

    /** 预警种类 **/
    @JsonProperty(value = "altType")
    private String altType;

    /** 预警子项 **/
    @JsonProperty(value = "altSubType")
    private String altSubType;

    /** 预警输出描述 **/
    @JsonProperty(value = "altDesc")
    private String altDesc;

    /** 指标等级 **/
    @JsonProperty(value = "altLvl")
    private String altLvl;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }
    public String getAltSinNo() {
        return altSinNo;
    }

    public void setAltSinNo(String altSinNo) {
        this.altSinNo = altSinNo;
    }

    public String getAltDate() {
        return altDate;
    }

    public void setAltDate(String altDate) {
        this.altDate = altDate;
    }

    public String getInfoSource() {
        return infoSource;
    }

    public void setInfoSource(String infoSource) {
        this.infoSource = infoSource;
    }

    public String getAltTypeMax() {
        return altTypeMax;
    }

    public void setAltTypeMax(String altTypeMax) {
        this.altTypeMax = altTypeMax;
    }

    public String getAltType() {
        return altType;
    }

    public void setAltType(String altType) {
        this.altType = altType;
    }

    public String getAltSubType() {
        return altSubType;
    }

    public void setAltSubType(String altSubType) {
        this.altSubType = altSubType;
    }

    public String getAltDesc() {
        return altDesc;
    }

    public void setAltDesc(String altDesc) {
        this.altDesc = altDesc;
    }

    public String getAltLvl() {
        return altLvl;
    }

    public void setAltLvl(String altLvl) {
        this.altLvl = altLvl;
    }

    @Override
    public String toString() {
        return "PspWarningInfoListRep{" +
                "pkId='" + pkId + '\'' +
                "taskNo='" + taskNo + '\'' +
                "altSinNo='" + altSinNo + '\'' +
                "altDate='" + altDate + '\'' +
                "infoSource='" + infoSource + '\'' +
                "altTypeMax='" + altTypeMax + '\'' +
                "altType='" + altType + '\'' +
                "altSubType='" + altSubType + '\'' +
                "altDesc='" + altDesc + '\'' +
                "altLvl='" + altLvl + '\'' +
                '}';
    }
}
