package cn.com.yusys.yusp.dto.server.xdxw0030.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据云估计流水号查询房屋信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    @JsonProperty(value = "pk")
    private String pk;//主键
    @JsonProperty(value = "buildings")
    private String buildings;//楼盘
    @JsonProperty(value = "building")
    private String building;//楼栋
    @JsonProperty(value = "floor")
    private String floor;//楼层
    @JsonProperty(value = "houseNo")
    private String houseNo;//房间号
    @JsonProperty(value = "totalValue")
    private BigDecimal totalValue;//总价
    @JsonProperty(value = "squ")
    private String squ;//面积
    @JsonProperty(value = "address")
    private String address;//地址
    @JsonProperty(value = "qryName")
    private String qryName;//查询人员
    @JsonProperty(value = "qryDate")
    private String qryDate;//查询日期
    @JsonProperty(value = "qryId")
    private String qryId;//查询人员ID
    @JsonProperty(value = "phone")
    private String phone;//电话

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getBuildings() {
        return buildings;
    }

    public void setBuildings(String buildings) {
        this.buildings = buildings;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public String getSqu() {
        return squ;
    }

    public void setSqu(String squ) {
        this.squ = squ;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getQryName() {
        return qryName;
    }

    public void setQryName(String qryName) {
        this.qryName = qryName;
    }

    public String getQryDate() {
        return qryDate;
    }

    public void setQryDate(String qryDate) {
        this.qryDate = qryDate;
    }

    public String getQryId() {
        return qryId;
    }

    public void setQryId(String qryId) {
        this.qryId = qryId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "List{" +
                "pk='" + pk + '\'' +
                ", buildings='" + buildings + '\'' +
                ", building='" + building + '\'' +
                ", floor='" + floor + '\'' +
                ", houseNo='" + houseNo + '\'' +
                ", totalValue=" + totalValue +
                ", squ=" + squ +
                ", address='" + address + '\'' +
                ", qryName='" + qryName + '\'' +
                ", qryDate='" + qryDate + '\'' +
                ", qryId='" + qryId + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
