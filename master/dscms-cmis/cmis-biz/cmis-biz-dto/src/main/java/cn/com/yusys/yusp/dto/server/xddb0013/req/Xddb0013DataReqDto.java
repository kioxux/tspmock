package cn.com.yusys.yusp.dto.server.xddb0013.req;

//import cn.com.yusys.yusp.dto.server.biz.hyy.common.UpdateCollateralElectronicCertificateForm;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：抵押登记不动产登记证明入库
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0013DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarid")
    private String guarid;//押品编号
    @JsonProperty(value = "UpdateCollateralElectronicCertificateForm")
    private cn.com.yusys.yusp.dto.server.hyy.common.UpdateCollateralElectronicCertificateForm UpdateCollateralElectronicCertificateForm;//证件类型



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public cn.com.yusys.yusp.dto.server.hyy.common.UpdateCollateralElectronicCertificateForm getUpdateCollateralElectronicCertificateForm() {
        return UpdateCollateralElectronicCertificateForm;
    }

    public void setUpdateCollateralElectronicCertificateForm(cn.com.yusys.yusp.dto.server.hyy.common.UpdateCollateralElectronicCertificateForm updateCollateralElectronicCertificateForm) {
        UpdateCollateralElectronicCertificateForm = updateCollateralElectronicCertificateForm;
    }


    @Override
    public String toString() {
        return "Xddb0013DataReqDto{" +
                "guarid='" + guarid + '\'' +
                ", UpdateCollateralElectronicCertificateForm=" + UpdateCollateralElectronicCertificateForm +
                '}';
    }

}
