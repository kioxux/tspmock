package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 押品与担保合同交互-押品信息dto
 */
public class GuarEvalInfoClientDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String guarNo;//押品编号
    private String curType;//币种
    private BigDecimal guarValueAmt;//押品评估价值
    private String serno;//流水号
    private String pkId;//主键
    private BigDecimal evalAmt;//我行认定价值
    private String evalInOutType;//评估方式
    private String evalCurrency;//认定币种
    private String evalType;//评估类型
    private String evalRealDate;//评估日期
    private String evalDate;//认定日期
    private BigDecimal evalInAmt;//内部评估价值
    private String evalInMethodEnname;//内部评估方法
    private String evalMethodReason;//估值方法选择理由
    private String evalOutOrgName;//外部评估机构名称
    private String evalOutOrgNo;//外部评估机构号
    private String outEndDate;//外部评估日期
    private BigDecimal preOutAmt;//外部预评估报告的评估价值
    private String isOutPreReport;//是否已取得正式评估报告
    private BigDecimal evalOutAmt;//外部正式评估报告的评估价值

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getGuarValueAmt() {
        return guarValueAmt;
    }

    public void setGuarValueAmt(BigDecimal guarValueAmt) {
        this.guarValueAmt = guarValueAmt;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {this.serno = serno;}

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {this.pkId = pkId; }

    public BigDecimal getEvalAmt() {
        return evalAmt;
    }

    public void setEvalAmt(BigDecimal evalAmt) {this.evalAmt = evalAmt;}

    public String getEvalInOutType() {
        return evalInOutType;
    }

    public void setEvalInOutType(String evalInOutType) {this.evalInOutType = evalInOutType;}

    public String getEvalCurrency() {
        return evalCurrency;
    }

    public void setEvalCurrency(String evalCurrency) {
        this.evalCurrency = evalCurrency;
    }

    public String getEvalType() {
        return evalType;
    }

    public void setEvalType(String evalType) {
        this.evalType = evalType;
    }

    public String getEvalRealDate() {
        return evalRealDate;
    }

    public void setEvalRealDate(String evalRealDate) {
        this.evalRealDate = evalRealDate;
    }

    public String getEvalDate() {
        return evalDate;
    }

    public void setEvalDate(String evalDate) {
        this.evalDate = evalDate;
    }

    public BigDecimal getEvalInAmt() {
        return evalInAmt;
    }

    public void setEvalInAmt(BigDecimal evalInAmt) {
        this.evalInAmt = evalInAmt;
    }

    /** 内部评估方法 STD_ZB_EVAL_METHOD **/
    public String getEvalInMethodEnname() {
        return evalInMethodEnname;
    }

    public void setEvalInMethodEnname(String evalInMethodEnname) {
        this.evalInMethodEnname = evalInMethodEnname;
    }

    /** 估值方法选择理由 **/
    public String getEvalMethodReason() {
        return evalMethodReason;
    }

    public void setEvalMethodReason(String evalMethodReason) {
        this.evalMethodReason = evalMethodReason;
    }

    /** 外部评估机构名称 **/
    public String getEvalOutOrgName() {
        return evalOutOrgName;
    }

    public void setEvalOutOrgName(String evalOutOrgName) {
        this.evalOutOrgName = evalOutOrgName;
    }

    /** 外部评估机构组织机构代码 **/
    public String getEvalOutOrgNo() {
        return evalOutOrgNo;
    }

    public void setEvalOutOrgNo(String evalOutOrgNo) {
        this.evalOutOrgNo = evalOutOrgNo;
    }

    /** 外部评估价值有效期截止日 **/
    public String getOutEndDate() {
        return outEndDate;
    }

    public void setOutEndDate(String outEndDate) {
        this.outEndDate = outEndDate;
    }

    /** 外部预评估报告的评估价值 **/
    public BigDecimal getPreOutAmt() {
        return preOutAmt;
    }

    public void setPreOutAmt(BigDecimal preOutAmt) {
        this.preOutAmt = preOutAmt;
    }

    /** 是否已取得正式评估报告 **/
    public String getIsOutPreReport() {
        return isOutPreReport;
    }

    public void setIsOutPreReport(String isOutPreReport) {
        this.isOutPreReport = isOutPreReport;
    }

    /** 外部正式评估报告的评估价值 **/
    public BigDecimal getEvalOutAmt() {
        return evalOutAmt;
    }

    public void setEvalOutAmt(BigDecimal evalOutAmt) {
        this.evalOutAmt = evalOutAmt;
    }

}
