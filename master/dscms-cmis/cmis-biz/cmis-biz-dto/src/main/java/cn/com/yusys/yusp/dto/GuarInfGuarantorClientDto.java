package cn.com.yusys.yusp.dto;
import java.io.Serializable;

public class GuarInfGuarantorClientDto implements Serializable {
    private static final long serialVersionUID = 1L;
	/** 押品统一编号 **/
	private String guarNo;
	/** 保证人类型 **/
	private String assureOwnerType;
	/** 保证人编号 **/
	private String guareeNo;

	public String getGuarNo() {
		return guarNo;
	}

	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}

	public String getAssureOwnerType() {
		return assureOwnerType;
	}

	public void setAssureOwnerType(String assureOwnerType) {
		this.assureOwnerType = assureOwnerType;
	}

	public String getGuareeNo() {
		return guareeNo;
	}

	public void setGuareeNo(String guareeNo) {
		this.guareeNo = guareeNo;
	}
}