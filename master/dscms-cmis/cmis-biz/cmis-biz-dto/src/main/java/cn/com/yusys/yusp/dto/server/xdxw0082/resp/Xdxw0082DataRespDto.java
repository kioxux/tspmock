package cn.com.yusys.yusp.dto.server.xdxw0082.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询上一笔抵押贷款的余额接口（增享贷）
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0082DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "loan_balance")
    private BigDecimal loan_balance; //贷款余额

    public BigDecimal getLoan_balance() {
        return loan_balance;
    }

    public void setLoan_balance(BigDecimal loan_balance) {
        this.loan_balance = loan_balance;
    }

    @Override
    public String toString() {
        return "Xdxw0080DataRespDto{" +
                "loan_balance=" + loan_balance +
                '}';
    }
}
