package cn.com.yusys.yusp.dto.server.xdht0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：查询符合条件的省心快贷合同
 * @author xll
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdht0010DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "contList")
	private java.util.List<ContList> contList;

	public List<ContList> getContList() {
		return contList;
	}

	public void setContList(List<ContList> contList) {
		this.contList = contList;
	}

	@Override
	public String toString() {
		return "Xdht0009DataRespDto{" +
				"contList=" + contList +
				'}';
	}
}  
