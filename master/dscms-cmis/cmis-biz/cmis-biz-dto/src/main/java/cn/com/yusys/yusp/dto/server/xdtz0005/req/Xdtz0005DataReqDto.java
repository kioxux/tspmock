package cn.com.yusys.yusp.dto.server.xdtz0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：根据流水号查询是否放款标记
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0005DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "serno")
    @NotNull(message = "业务流水号serno不能为空")
    @NotEmpty(message = "业务流水号serno不能为空")
    private String serno;//业务流水号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    @Override
    public String toString() {
        return "Xdtz0005DataReqDto{" +
                "serno='" + serno + '\'' +
                '}';
    }

}
