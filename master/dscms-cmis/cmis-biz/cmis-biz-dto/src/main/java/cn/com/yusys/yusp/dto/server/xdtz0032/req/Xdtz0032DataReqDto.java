package cn.com.yusys.yusp.dto.server.xdtz0032.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：询客户所担保的行内当前贷款逾期件数
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0032DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    @NotNull(message = "客户编号cusId不能为空")
    @NotEmpty(message = "客户编号cusId不能为空")
	private String cusId;//客户编号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

	@Override
	public String toString() {
		return "Xdtz0032DataReqDto{" +
				"cusId='" + cusId + '\'' +
				'}';
	}
}
