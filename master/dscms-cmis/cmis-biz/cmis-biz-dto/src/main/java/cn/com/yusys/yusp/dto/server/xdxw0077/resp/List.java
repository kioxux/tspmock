package cn.com.yusys.yusp.dto.server.xdxw0077.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/11 11:18
 * @since 2021/6/11 11:18
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prdNo")
    private String prdNo;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "loanAmt")
    private String loanAmt;//贷款金额
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//审批状态

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(String loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    @Override
    public String toString() {
        return "Xdxw0077DataRespDto{" +
                "prdNo='" + prdNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", loanAmt='" + loanAmt + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                '}';
    }
}
