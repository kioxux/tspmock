package cn.com.yusys.yusp.dto.server.xdls0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：市民贷联系人信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdls0002DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//调查流水号


    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    @Override
    public String toString() {
        return "Xdls0002DataReqDto{" +
                "indgtSerno='" + indgtSerno + '\'' +
                '}';
    }
}
