package cn.com.yusys.yusp.dto.server.xddb0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Data：提前出库押品信息维护
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0011DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list_grt")
    private java.util.List<ListGrt> listGrt;

    public List<ListGrt> getListGrt() {
        return listGrt;
    }

    public void setListGrt(List<ListGrt> listGrt) {
        this.listGrt = listGrt;
    }

    @Override
    public String toString() {
        return "Xddb0011DataReqDto{" +
                "listGrt=" + listGrt +
                '}';
    }

}
