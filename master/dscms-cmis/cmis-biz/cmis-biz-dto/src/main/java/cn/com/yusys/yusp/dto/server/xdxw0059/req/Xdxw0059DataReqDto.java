package cn.com.yusys.yusp.dto.server.xdxw0059.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据业务唯一编号查询在信贷系统中的抵押率
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0059DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "applyNo")
    private String applyNo;//无还本续贷申请表主键


    public String getApplyNo() {
        return applyNo;
    }

    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }

    @Override
    public String toString() {
        return "Xdxw0059DataReqDto{" +
                "applyNo='" + applyNo + '\'' +
                '}';
    }
}
