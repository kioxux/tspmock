package cn.com.yusys.yusp.dto.server.xdqt0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：贷款申请预约（企业客户）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdqt0005DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

	@Override
	public String toString() {
		return "Xdqt0005DataRespDto{" +
				"serno='" + serno + '\'' +
				'}';
	}
}
