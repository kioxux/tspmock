package cn.com.yusys.yusp.dto.server.xdxw0078.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：勘验任务查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0078DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "housename")
    @NotNull(message = "证件号housename不能为空")
    @NotEmpty(message = "证件号housename不能为空")
    private  String  housename;//楼盘

    @JsonProperty(value = "hbuild")
    private  String  hbuild;//楼栋

    @JsonProperty(value = "hfloor")
    private  String  hfloor;//楼层

    @JsonProperty(value = "roomid")
    private  String  roomid;//房间号

    @JsonProperty(value = "judge")
    private  String  judge;//估价

    @JsonProperty(value = "total")
    private  String  total;//总价

    @JsonProperty(value = "area")
    private  String  area;//面积

    @JsonProperty(value = "hregion")
    private  String  hregion;//地址

    @JsonProperty(value = "speople")
    private  String  speople;//查询人员

    @JsonProperty(value = "sdate")
    private  String  sdate;//查询日期

    @JsonProperty(value = "managerid")
    private  String  managerid;//查询人ID

    @JsonProperty(value = "tel")
    private  String  tel;//电话号码

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getHousename() {
        return housename;
    }

    public void setHousename(String housename) {
        this.housename = housename;
    }

    public String getHbuild() {
        return hbuild;
    }

    public void setHbuild(String hbuild) {
        this.hbuild = hbuild;
    }

    public String getHfloor() {
        return hfloor;
    }

    public void setHfloor(String hfloor) {
        this.hfloor = hfloor;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid;
    }

    public String getJudge() {
        return judge;
    }

    public void setJudge(String judge) {
        this.judge = judge;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getHregion() {
        return hregion;
    }

    public void setHregion(String hregion) {
        this.hregion = hregion;
    }

    public String getSpeople() {
        return speople;
    }

    public void setSpeople(String speople) {
        this.speople = speople;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getManagerid() {
        return managerid;
    }

    public void setManagerid(String managerid) {
        this.managerid = managerid;
    }



    @Override
    public String toString() {
        return "Xdxw0078DataReqDto{" +
                "housename='" + housename + '\'' +
                "hbuild='" + hbuild + '\'' +
                "hfloor='" + hfloor + '\'' +
                "roomid='" + roomid + '\'' +
                "judge='" + judge + '\'' +
                "total='" + total + '\'' +
                "area='" + area + '\'' +
                "hregion='" + hregion + '\'' +
                "speople='" + speople + '\'' +
                "sdate='" + sdate + '\'' +
                "managerid='" + managerid + '\'' +
                "tel='" + tel + '\'' +
                '}';
    }
}
