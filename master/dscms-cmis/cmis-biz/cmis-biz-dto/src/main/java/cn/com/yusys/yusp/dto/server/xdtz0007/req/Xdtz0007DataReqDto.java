package cn.com.yusys.yusp.dto.server.xdtz0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据客户号获取非信用方式发放贷款的最长到期日
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0007DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusNo")
    private String cusNo;//客户号

    public String getCusNo() {
        return cusNo;
    }

    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }


    @Override
    public String toString() {
        return "Xdtz0007DataReqDto{" +
                "cusNo='" + cusNo + '\'' +
                '}';
    }
}
