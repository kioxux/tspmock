package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class GuarClientRsDto<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    //返回码值
    private String rtnCode;
    //返回信息
    private String rtnMsg;
    private List<GuarBaseInfoClientDto> GuarBaseInfoList;//押品集合
    
    //请求/响应数据
    private T data;

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    
    public void setGuarBaseInfoList(List<GuarBaseInfoClientDto> guarBaseInfoList) {
        GuarBaseInfoList = guarBaseInfoList;
    }
}
