package cn.com.yusys.yusp.dto.server.xdsx0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 14:58:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 14:58
 * @since 2021/5/19 14:58
 */
@JsonPropertyOrder(alphabetic = true)
public class ListDy implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guaranty_id")
    private String guaranty_id;//抵押物编号
    @JsonProperty(value = "guaranty_type")
    private String guaranty_type;//抵押物类型
    @JsonProperty(value = "vin")
    private String vin;//车架号
    @JsonProperty(value = "cus_id")
    private String cus_id;//质押人ID
    @JsonProperty(value = "cus_name")
    private String cus_name;//质押人名称
    @JsonProperty(value = "gage_name")
    private String gage_name;//质押物名称
    @JsonProperty(value = "eval_date")
    private String eval_date;//评估日期
    @JsonProperty(value = "max_mortagage_amt")
    private String max_mortagage_amt;//最高可质押金额（元）
    @JsonProperty(value = "guide_type")
    private String guide_type;//担保品类型细分
    @JsonProperty(value = "newcode")
    private String newcode;//新押品编码
    @JsonProperty(value = "confirm_eval")
    private String confirm_eval;//我行认定价值
    @JsonProperty(value = "confirm_date")
    private String confirm_date;//认定日期
    @JsonProperty(value = "db_flag")
    private String db_flag;//担保合同与抵押物对应关系

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    public String getGuaranty_type() {
        return guaranty_type;
    }

    public void setGuaranty_type(String guaranty_type) {
        this.guaranty_type = guaranty_type;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getGage_name() {
        return gage_name;
    }

    public void setGage_name(String gage_name) {
        this.gage_name = gage_name;
    }

    public String getEval_date() {
        return eval_date;
    }

    public void setEval_date(String eval_date) {
        this.eval_date = eval_date;
    }

    public String getMax_mortagage_amt() {
        return max_mortagage_amt;
    }

    public void setMax_mortagage_amt(String max_mortagage_amt) {
        this.max_mortagage_amt = max_mortagage_amt;
    }

    public String getGuide_type() {
        return guide_type;
    }

    public void setGuide_type(String guide_type) {
        this.guide_type = guide_type;
    }

    public String getNewcode() {
        return newcode;
    }

    public void setNewcode(String newcode) {
        this.newcode = newcode;
    }

    public String getConfirm_eval() {
        return confirm_eval;
    }

    public void setConfirm_eval(String confirm_eval) {
        this.confirm_eval = confirm_eval;
    }

    public String getConfirm_date() {
        return confirm_date;
    }

    public void setConfirm_date(String confirm_date) {
        this.confirm_date = confirm_date;
    }

    public String getDb_flag() {
        return db_flag;
    }

    public void setDb_flag(String db_flag) {
        this.db_flag = db_flag;
    }

    @Override
    public String toString() {
        return "ListDy{" +
                "guaranty_id='" + guaranty_id + '\'' +
                ", guaranty_type='" + guaranty_type + '\'' +
                ", vin='" + vin + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", gage_name='" + gage_name + '\'' +
                ", eval_date='" + eval_date + '\'' +
                ", max_mortagage_amt='" + max_mortagage_amt + '\'' +
                ", guide_type='" + guide_type + '\'' +
                ", newcode='" + newcode + '\'' +
                ", confirm_eval='" + confirm_eval + '\'' +
                ", confirm_date='" + confirm_date + '\'' +
                ", db_flag='" + db_flag + '\'' +
                '}';
    }
}
