package cn.com.yusys.yusp.dto.server.xdxw0023.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：在小贷是否有调查申请记录
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0023DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "num")
    private Integer num;//总数

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Xdxw0023DataRespDto{" +
                "num=" + num +
                '}';
    }
}
