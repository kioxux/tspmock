package cn.com.yusys.yusp.dto.server.xdtz0027.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0027DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;
    @JsonProperty(value = "sftqzz")
    private String sftqzz;//是否提前周转

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getSftqzz() {
        return sftqzz;
    }

    public void setSftqzz(String sftqzz) {
        this.sftqzz = sftqzz;
    }

    @Override
    public String toString() {
        return "Xdtz0027DataReqDto{" +
                "cusId='" + cusId + '\'' +
                "sftqzz='" + sftqzz + '\'' +
                '}';
    }
}
