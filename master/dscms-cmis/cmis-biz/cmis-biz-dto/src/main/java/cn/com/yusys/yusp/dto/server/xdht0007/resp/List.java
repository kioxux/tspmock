package cn.com.yusys.yusp.dto.server.xdht0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：合同面签信息列表查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同号（担保合同编号）
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同号（担保合同中文编号）
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称（借款人名称）
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同开始日
    @JsonProperty(value = "signVideo")
    private String signVideo;//签约视频
    @JsonProperty(value = "guarContType")
    private String guarContType;//担保合同类型


    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getSignVideo() {
        return signVideo;
    }

    public void setSignVideo(String signVideo) {
        this.signVideo = signVideo;
    }

    public String getGuarContType() {
        return guarContType;
    }

    public void setGuarContType(String guarContType) {
        this.guarContType = guarContType;
    }

    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", cusName='" + cusName + '\'' +
                ", cusId='" + cusId + '\'' +
                ", contStatus='" + contStatus + '\'' +
                ", prdName='" + prdName + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", contAmt=" + contAmt +
                ", contStartDate='" + contStartDate + '\'' +
                ", signVideo='" + signVideo + '\'' +
                ", guarContType='" + guarContType + '\'' +
                '}';
    }
}
