package cn.com.yusys.yusp.dto.server.xdtz0038.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：台账信息通用列表查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0038DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "centNo")
    private String centNo;//证件号
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "prdNo")
    private String prdNo;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "chnl")
    private String chnl;//渠道来源
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "startPageNum")
    private String startPageNum;//开始页数
    @JsonProperty(value = "pageSize")
    private String pageSize;//条数


    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCentNo() {
        return centNo;
    }

    public void setCentNo(String centNo) {
        this.centNo = centNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getChnl() {
        return chnl;
    }

    public void setChnl(String chnl) {
        this.chnl = chnl;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Xdtz0038DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", centNo='" + centNo + '\'' +
                ", billNo='" + billNo + '\'' +
                ", cusName='" + cusName + '\'' +
                ", prdNo='" + prdNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", chnl='" + chnl + '\'' +
                ", contNo='" + contNo + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", startPageNum='" + startPageNum + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
