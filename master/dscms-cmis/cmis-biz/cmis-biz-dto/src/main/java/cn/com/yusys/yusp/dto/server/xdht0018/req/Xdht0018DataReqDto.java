package cn.com.yusys.yusp.dto.server.xdht0018.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：担保合同文本生成pdf
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0018DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "grtContNo")
    private String grtContNo;//担保合同号
    @JsonProperty(value = "applyDate")
    private String applyDate;//申请日期

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGrtContNo() {
        return grtContNo;
    }

    public void setGrtContNo(String grtContNo) {
        this.grtContNo = grtContNo;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    @Override
    public String toString() {
        return "Xdht0018DataReqDto{" +
                "cusId='" + cusId + '\'' +
                "contNo='" + contNo + '\'' +
                "grtContNo='" + grtContNo + '\'' +
                "applyDate='" + applyDate + '\'' +
                '}';
    }
}
