package cn.com.yusys.yusp.dto.server.xdht0019.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：合同生成
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0019DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "date")
    private String date;//日期

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

	@Override
	public String toString() {
		return "Xdht0019DataRespDto{" +
				"contNo='" + contNo + '\'' +
                ", cnContNo=" + cnContNo +
				", applyAmt=" + applyAmt +
				", date='" + date + '\'' +
				'}';
	}
}
