package cn.com.yusys.yusp.dto.server.xddb0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：抵押登记注销风控
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0014DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "retype")
    private String retype;//登记类型
    @JsonProperty(value = "guarid")
    private String guarid;//押品编号

    public String getRetype() {
        return retype;
    }

    public void setRetype(String retype) {
        this.retype = retype;
    }

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    @Override
    public String toString() {
        return "Xddb0014DataReqDto{" +
                "retype='" + retype + '\'' +
                ", guarid='" + guarid + '\'' +
                '}';
    }
}
