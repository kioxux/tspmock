package cn.com.yusys.yusp.dto.server.xdcz0018.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：家速贷、极速快贷贷款申请
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0018DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "cusMobile")
    private String cusMobile;//客户手机号码
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "applyTerm")
    private String applyTerm;//申请期限
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//借款用途
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "appChnlCode")
    private String appChnlCode;//申请渠道码
    @JsonProperty(value = "appChnlName")
    private String appChnlName;//申请渠道名
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号
    @JsonProperty(value = "prdCode")
    private String prdCode;//产品代码
    @JsonProperty(value = "indivRsdAddr")
    private String indivRsdAddr;//居住地址
    @JsonProperty(value = "detailAddr")
    private String detailAddr;//详细地址
    @JsonProperty(value = "isRepr")
    private String isRepr;//是否法人
    @JsonProperty(value = "businessRegiNo")
    private String businessRegiNo;//工商注册码
    @JsonProperty(value = "houseSqu")
    private String houseSqu;//房屋面积
    @JsonProperty(value = "house")
    private String house;//房屋预估价值
    @JsonProperty(value = "houseAddr")
    private String houseAddr;//房产地址
    @JsonProperty(value = "conOperAddr")
    private String conOperAddr;//企业经营地址
    @JsonProperty(value = "houseOwner")
    private String houseOwner;//房产所有人
    @JsonProperty(value = "conName")
    private String conName;//企业名称
    @JsonProperty(value = "conDetailAddr")
    private String conDetailAddr;//企业详细地址
    @JsonProperty(value = "estateName")
    private String estateName;//小区名称
    @JsonProperty(value = "buildingInfo")
    private String buildingInfo;//楼栋信息
    @JsonProperty(value = "authbookContent")
    private String authbookContent;//授权书内容
    @JsonProperty(value = "authSignDate")
    private String authSignDate;//授权签订日期
    @JsonProperty(value = "videoNo")
    private String videoNo;//影像编号

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCusMobile() {
        return cusMobile;
    }

    public void setCusMobile(String cusMobile) {
        this.cusMobile = cusMobile;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public String getApplyTerm() {
        return applyTerm;
    }

    public void setApplyTerm(String applyTerm) {
        this.applyTerm = applyTerm;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getAppChnlCode() {
        return appChnlCode;
    }

    public void setAppChnlCode(String appChnlCode) {
        this.appChnlCode = appChnlCode;
    }

    public String getAppChnlName() {
        return appChnlName;
    }

    public void setAppChnlName(String appChnlName) {
        this.appChnlName = appChnlName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getPrdCode() {
        return prdCode;
    }

    public void setPrdCode(String prdCode) {
        this.prdCode = prdCode;
    }

    public String getIndivRsdAddr() {
        return indivRsdAddr;
    }

    public void setIndivRsdAddr(String indivRsdAddr) {
        this.indivRsdAddr = indivRsdAddr;
    }

    public String getDetailAddr() {
        return detailAddr;
    }

    public void setDetailAddr(String detailAddr) {
        this.detailAddr = detailAddr;
    }

    public String getIsRepr() {
        return isRepr;
    }

    public void setIsRepr(String isRepr) {
        this.isRepr = isRepr;
    }

    public String getBusinessRegiNo() {
        return businessRegiNo;
    }

    public void setBusinessRegiNo(String businessRegiNo) {
        this.businessRegiNo = businessRegiNo;
    }

    public String getHouseSqu() {
        return houseSqu;
    }

    public void setHouseSqu(String houseSqu) {
        this.houseSqu = houseSqu;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getHouseAddr() {
        return houseAddr;
    }

    public void setHouseAddr(String houseAddr) {
        this.houseAddr = houseAddr;
    }

    public String getConOperAddr() {
        return conOperAddr;
    }

    public void setConOperAddr(String conOperAddr) {
        this.conOperAddr = conOperAddr;
    }

    public String getHouseOwner() {
        return houseOwner;
    }

    public void setHouseOwner(String houseOwner) {
        this.houseOwner = houseOwner;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getConDetailAddr() {
        return conDetailAddr;
    }

    public void setConDetailAddr(String conDetailAddr) {
        this.conDetailAddr = conDetailAddr;
    }

    public String getEstateName() {
        return estateName;
    }

    public void setEstateName(String estateName) {
        this.estateName = estateName;
    }

    public String getBuildingInfo() {
        return buildingInfo;
    }

    public void setBuildingInfo(String buildingInfo) {
        this.buildingInfo = buildingInfo;
    }

    public String getAuthbookContent() {
        return authbookContent;
    }

    public void setAuthbookContent(String authbookContent) {
        this.authbookContent = authbookContent;
    }

    public String getAuthSignDate() {
        return authSignDate;
    }

    public void setAuthSignDate(String authSignDate) {
        this.authSignDate = authSignDate;
    }

    public String getVideoNo() {
        return videoNo;
    }

    public void setVideoNo(String videoNo) {
        this.videoNo = videoNo;
    }

    @Override
    public String toString() {
        return "Xdcz0018DataReqDto{" +
                "cusName='" + cusName + '\'' +
                "certNo='" + certNo + '\'' +
                "cusMobile='" + cusMobile + '\'' +
                "applyAmt='" + applyAmt + '\'' +
                "applyTerm='" + applyTerm + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "assureMeans='" + assureMeans + '\'' +
                "appChnlCode='" + appChnlCode + '\'' +
                "appChnlName='" + appChnlName + '\'' +
                "managerId='" + managerId + '\'' +
                "prdCode='" + prdCode + '\'' +
                "indivRsdAddr='" + indivRsdAddr + '\'' +
                "detailAddr='" + detailAddr + '\'' +
                "isRepr='" + isRepr + '\'' +
                "businessRegiNo='" + businessRegiNo + '\'' +
                "houseSqu='" + houseSqu + '\'' +
                "house='" + house + '\'' +
                "houseAddr='" + houseAddr + '\'' +
                "conOperAddr='" + conOperAddr + '\'' +
                "houseOwner='" + houseOwner + '\'' +
                "conName='" + conName + '\'' +
                "conDetailAddr='" + conDetailAddr + '\'' +
                "estateName='" + estateName + '\'' +
                "buildingInfo='" + buildingInfo + '\'' +
                "authbookContent='" + authbookContent + '\'' +
                "authSignDate='" + authSignDate + '\'' +
                "videoNo='" + videoNo + '\'' +
                '}';
    }
}
