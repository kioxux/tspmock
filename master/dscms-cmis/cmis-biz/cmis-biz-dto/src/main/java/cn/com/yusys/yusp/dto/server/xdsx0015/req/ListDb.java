package cn.com.yusys.yusp.dto.server.xdsx0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 14:56:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 14:56
 * @since 2021/5/19 14:56
 */
@JsonPropertyOrder(alphabetic = true)
public class ListDb implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "db_cus_id")
    private String db_cus_id;//担保合同客户编号
    @JsonProperty(value = "db_cus_name")
    private String db_cus_name;//担保合同客户名
    @JsonProperty(value = "guar_way")
    private String guar_way;//担保方式
    @JsonProperty(value = "db_flag")
    private String db_flag;//担保合同与抵押物对应关系

    public String getDb_cus_id() {
        return db_cus_id;
    }

    public void setDb_cus_id(String db_cus_id) {
        this.db_cus_id = db_cus_id;
    }

    public String getDb_cus_name() {
        return db_cus_name;
    }

    public void setDb_cus_name(String db_cus_name) {
        this.db_cus_name = db_cus_name;
    }

    public String getGuar_way() {
        return guar_way;
    }

    public void setGuar_way(String guar_way) {
        this.guar_way = guar_way;
    }

    public String getDb_flag() {
        return db_flag;
    }

    public void setDb_flag(String db_flag) {
        this.db_flag = db_flag;
    }

    @Override
    public String toString() {
        return "ListDb{" +
                "db_cus_id='" + db_cus_id + '\'' +
                ", db_cus_name='" + db_cus_name + '\'' +
                ", guar_way='" + guar_way + '\'' +
                ", db_flag='" + db_flag + '\'' +
                '}';
    }
}
