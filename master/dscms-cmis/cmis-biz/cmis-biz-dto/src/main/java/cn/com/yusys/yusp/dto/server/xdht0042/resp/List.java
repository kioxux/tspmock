package cn.com.yusys.yusp.dto.server.xdht0042.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 接口处理类:优企贷、优农贷合同信息查询
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//授信调查流水号
    @JsonProperty(value = "prd_type")
    private String prd_type;//产品类别
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户编号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同编号
    @JsonProperty(value = "cont_type")
    private String cont_type;//合同类型
    @JsonProperty(value = "cont_state")
    private String cont_state;//合同状态
    @JsonProperty(value = "cyc_ind")
    private String cyc_ind;//循环合同标志
    @JsonProperty(value = "sign_date")
    private String sign_date;//合同签订日期
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//合同起始日期
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//申请金额
    @JsonProperty(value = "manager_br_id")
    private String manager_br_id;//管户机构
    @JsonProperty(value = "assure_means_main")
    private String assure_means_main;//担保方式

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    public String getCyc_ind() {
        return cyc_ind;
    }

    public void setCyc_ind(String cyc_ind) {
        this.cyc_ind = cyc_ind;
    }

    public String getSign_date() {
        return sign_date;
    }

    public void setSign_date(String sign_date) {
        this.sign_date = sign_date;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getAssure_means_main() {
        return assure_means_main;
    }

    public void setAssure_means_main(String assure_means_main) {
        this.assure_means_main = assure_means_main;
    }

    @Override
    public String toString() {
        return "List{" +
                "survey_serno='" + survey_serno + '\'' +
                "prd_type='" + prd_type + '\'' +
                "cus_id='" + cus_id + '\'' +
                "cont_no='" + cont_no + '\'' +
                "cont_type='" + cont_type + '\'' +
                "cont_state='" + cont_state + '\'' +
                "cyc_ind='" + cyc_ind + '\'' +
                "sign_date='" + sign_date + '\'' +
                "loan_start_date='" + loan_start_date + '\'' +
                "apply_amount='" + apply_amount + '\'' +
                "manager_br_id='" + manager_br_id + '\'' +
                "assure_means_main='" + assure_means_main + '\'' +
                '}';
    }
}
