package cn.com.yusys.yusp.dto.server.xdca0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：大额分期合同列表查询
 * @author xll
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdca0003DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "totalNum")
	private String totalNum;//总数
	@JsonProperty(value = "list")
	private java.util.List<List> list;//list


	public String getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(String totalNum) {
		this.totalNum = totalNum;
	}
	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Xdca0003DataRespDto{" +
				"totalNum='" + totalNum + '\'' +
				"list='" + list + '\'' +
				'}';
	}

}  
