package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

public class GuarBaseInfoClientDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String guarContNo;//担保合同编号
    private String guarNo;//押品编号
    private String guarCusName;  //押品所有人名称
    private String guarBusistate;//押品所在业务阶段
    private List<GuarBaseInfoClientDto> list;//多个押品编号
	private String opeType;//操作类型

    public String getGuarContNo() {
        return guarContNo;
    }

    public String getGuarCusName() {
        return guarCusName;
    }

    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName;
    }

    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }
    
    public String getGuarBusistate() {
        return guarBusistate;
    }
    
    public void setGuarBusistate(String guarBusistate) {
        this.guarBusistate = guarBusistate;
    }
    
    public List<GuarBaseInfoClientDto> getList() {
        return list;
    }
    
    public void setList(List<GuarBaseInfoClientDto> list) {
        this.list = list;
    }
	
	public String getOpeType() {
		return opeType;
	}
	
	public void setOpeType(String opeType) {
		this.opeType = opeType;
	}
    
}
