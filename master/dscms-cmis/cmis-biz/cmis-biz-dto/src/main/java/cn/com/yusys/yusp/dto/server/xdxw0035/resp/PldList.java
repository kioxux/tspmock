package cn.com.yusys.yusp.dto.server.xdxw0035.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据流水号查询无还本续贷抵押信息
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class PldList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pldRate")
    private BigDecimal pldRate;//抵押率
    @JsonProperty(value = "pawnType")
    private String pawnType;//抵押类型
    @JsonProperty(value = "address")
    private String address;//地址
    @JsonProperty(value = "ownershipId")
    private String ownershipId;//所有权人
    @JsonProperty(value = "pawnName")
    private String pawnName;//抵押物名称

    public BigDecimal getPldRate() {
        return pldRate;
    }

    public void setPldRate(BigDecimal pldRate) {
        this.pldRate = pldRate;
    }

    public String getPawnType() {
        return pawnType;
    }

    public void setPawnType(String pawnType) {
        this.pawnType = pawnType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOwnershipId() {
        return ownershipId;
    }

    public void setOwnershipId(String ownershipId) {
        this.ownershipId = ownershipId;
    }

    public String getPawnName() {
        return pawnName;
    }

    public void setPawnName(String pawnName) {
        this.pawnName = pawnName;
    }

    @Override
    public String toString() {
        return "PldList{" +
                "pldRate=" + pldRate +
                ", pawnType='" + pawnType + '\'' +
                ", address='" + address + '\'' +
                ", ownershipId='" + ownershipId + '\'' +
                ", pawnName='" + pawnName + '\'' +
                '}';
    }
}