package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportQryLst
 * @类描述: credit_report_qry_lst数据实体类
 * @功能描述: 
 * @创建人: xx
 * @创建时间: 2021-05-18 20:47:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class InItCreditReportQryLstDto{
	/** 业务流水号 **/
	private String serno;

	/** 征信查询场景大类 **/
	/**[{"key":"01","value":"授信管理"},{"key":"02","value":"合同签订"},{"key":"03","value":"出账放款"},{"key":"04","value":"贷后检查"}]**/
	private String  period;

	/** 征信查询场景细类
	单一法人授信申报CRE01 2.单一法人授信变更申报、复审、复议、再议CRE01 3.单一客户预授信、预授信细化	CRE01
	单一授信批复变更和额度冻结解冻及终止	CRE02
    集团授信申报及变更、复议、再议、复审 CRE03 集团授信预授信、预授信细化	CRE03
	合同申请	CRE04
	出账申请	CRE05
	展期申请	CRE06
	担保变更	CRE07
	还款账号变更	CRE08
	延期还款	CRE09
	投贷后管理	CRE10
	零售业务申请	CRE12
	零售合同申请	CRE13
	零售放款申请	CRE14
	信用卡申请	CRE15
	信用卡额度调整（待定）	CRE16
	信用卡大额分期	CRE17
	小微授信调查	CRE18
	 **/
	private String bizScene;

	public String getSerno() {
		return serno;
	}

	public void setSerno(String serno) {
		this.serno = serno;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getBizScene() {
		return bizScene;
	}

	public void setBizScene(String bizScene) {
		this.bizScene = bizScene;
	}
}