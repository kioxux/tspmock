package cn.com.yusys.yusp.dto.server.xdsx0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：风控发信贷进行授信作废接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0014DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zf_flag")
    private String zf_flag;//是否作废成功

    public String getZf_flag() {
        return zf_flag;
    }

    public void setZf_flag(String zf_flag) {
        this.zf_flag = zf_flag;
    }

    @Override
    public String toString() {
        return "Xdsx0014DataRespDto{" +
                "zf_flag='" + zf_flag + '\'' +
                '}';
    }
}
