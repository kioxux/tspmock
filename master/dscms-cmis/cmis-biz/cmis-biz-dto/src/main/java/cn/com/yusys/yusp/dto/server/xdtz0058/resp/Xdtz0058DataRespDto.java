package cn.com.yusys.yusp.dto.server.xdtz0058.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：通过借据号查询借据信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0058DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构编号
    @JsonProperty(value = "loanAmt")
    private String loanAmt;//借据金额
    @JsonProperty(value = "loanTerm")
    private String loanTerm;//贷款期限
    @JsonProperty(value = "overdueTimesTotal")
    private String overdueTimesTotal;//累计逾期期数

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(String loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getOverdueTimesTotal() {
        return overdueTimesTotal;
    }

    public void setOverdueTimesTotal(String overdueTimesTotal) {
        this.overdueTimesTotal = overdueTimesTotal;
    }

    @Override
    public String toString() {
        return "Xdtz0058DataRespDto{" +
                "billNo='" + billNo + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", loanAmt='" + loanAmt + '\'' +
                ", loanTerm='" + loanTerm + '\'' +
                ", overdueTimesTotal='" + overdueTimesTotal + '\'' +
                '}';
    }
}
