package cn.com.yusys.yusp.dto.server.xdxw0046.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：优享贷批复结果反馈
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0046DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//批复流水号
    @JsonProperty(value = "manager_id")
    private String manager_id;//客户经理工号
    @JsonProperty(value = "manager_name")
    private String manager_name;//客户经理名字
    @JsonProperty(value = "org_id")
    private String org_id;//片区id
    @JsonProperty(value = "org_name")
    private String org_name;//片区名称
    @JsonProperty(value = "manager_phone")
    private String manager_phone;//客户经理电话


    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getManager_phone() {
        return manager_phone;
    }

    public void setManager_phone(String manager_phone) {
        this.manager_phone = manager_phone;
    }

    @Override
    public String toString() {
        return "Xdxw0046RespDto{" +
                "serno='" + serno + '\'' +
                "manager_id='" + manager_id + '\'' +
                "manager_name='" + manager_name + '\'' +
                "org_id='" + org_id + '\'' +
                "org_name='" + org_name + '\'' +
                "manager_phone='" + manager_phone + '\'' +
                '}';
    }
}  
