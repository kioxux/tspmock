package cn.com.yusys.yusp.dto.server.xddh0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/25 16:47
 * @Version 1.0
 */
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "iqpSerno")
    private String iqpSerno;//交易流水号
    @JsonProperty(value = "billNo")
    private String billNo;//交易流水号
    @JsonProperty(value = "repayMode")
    private String repayMode;//交易流水号
    @JsonProperty(value = "repayAmt")
    private String repayAmt;//交易流水号
    @JsonProperty(value = "fxAmt")
    private String fxAmt;//交易流水号
    @JsonProperty(value = "approveStatus")
    private String approveStatus;//交易流水号
    @JsonProperty(value = "repayAcctNo")
    private String repayAcctNo;//交易流水号
    @JsonProperty(value = "repayAcctName")
    private String repayAcctName;//交易流水号
    @JsonProperty(value = "isPeriod")
    private String isPeriod;//交易流水号
    @JsonProperty(value = "loanBalance")
    private String loanBalance;//交易流水号
    @JsonProperty(value = "totalTqlxAmt")
    private String totalTqlxAmt;//交易流水号
    @JsonProperty(value = "totalLxAmt")
    private String totalLxAmt;//交易流水号
    @JsonProperty(value = "contNo")
    private String contNo;//交易流水号
    @JsonProperty(value = "cusId")
    private String cusId;//交易流水号
    @JsonProperty(value = "inputId")
    private String inputId;//交易流水号
    @JsonProperty(value = "managerId")
    private String managerId;//交易流水号
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//交易流水号
    @JsonProperty(value = "repayType")
    private String repayType;//交易流水号
    @JsonProperty(value = "accStatus")
    private String accStatus;//交易流水号
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;//交易流水号

    @JsonProperty(value = "ysfxAmt")
    private String ysfxAmt;//交易流水号
    @JsonProperty(value = "zcbjAmt")
    private String zcbjAmt;//交易流水号
    @JsonProperty(value = "ysyjlxXmt")
    private String ysyjlxXmt;//交易流水号
    @JsonProperty(value = "isAllowTakeover")
    private String isAllowTakeover;//交易流水号
    @JsonProperty(value = "periodTimes")
    private String periodTimes;//交易流水号
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//交易流水号
    @JsonProperty(value = "isOtherAcct")
    private String isOtherAcct;//交易流水号
    @JsonProperty(value = "totalBjAmt")
    private String totalBjAmt;//交易流水号
    @JsonProperty(value = "totalFxAmt")
    private String totalFxAmt;//交易流水号
    @JsonProperty(value = "status")
    private String status;//交易流水号
    @JsonProperty(value = "inputDate")
    private String inputDate;//交易流水号

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getRepayAmt() {
        return repayAmt;
    }

    public void setRepayAmt(String repayAmt) {
        this.repayAmt = repayAmt;
    }

    public String getFxAmt() {
        return fxAmt;
    }

    public void setFxAmt(String fxAmt) {
        this.fxAmt = fxAmt;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getRepayAcctNo() {
        return repayAcctNo;
    }

    public void setRepayAcctNo(String repayAcctNo) {
        this.repayAcctNo = repayAcctNo;
    }

    public String getRepayAcctName() {
        return repayAcctName;
    }

    public void setRepayAcctName(String repayAcctName) {
        this.repayAcctName = repayAcctName;
    }

    public String getIsPeriod() {
        return isPeriod;
    }

    public void setIsPeriod(String isPeriod) {
        this.isPeriod = isPeriod;
    }

    public String getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(String loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getTotalTqlxAmt() {
        return totalTqlxAmt;
    }

    public void setTotalTqlxAmt(String totalTqlxAmt) {
        this.totalTqlxAmt = totalTqlxAmt;
    }

    public String getTotalLxAmt() {
        return totalLxAmt;
    }

    public void setTotalLxAmt(String totalLxAmt) {
        this.totalLxAmt = totalLxAmt;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getYsfxAmt() {
        return ysfxAmt;
    }

    public void setYsfxAmt(String ysfxAmt) {
        this.ysfxAmt = ysfxAmt;
    }

    public String getZcbjAmt() {
        return zcbjAmt;
    }

    public void setZcbjAmt(String zcbjAmt) {
        this.zcbjAmt = zcbjAmt;
    }

    public String getYsyjlxXmt() {
        return ysyjlxXmt;
    }

    public void setYsyjlxXmt(String ysyjlxXmt) {
        this.ysyjlxXmt = ysyjlxXmt;
    }

    public String getIsAllowTakeover() {
        return isAllowTakeover;
    }

    public void setIsAllowTakeover(String isAllowTakeover) {
        this.isAllowTakeover = isAllowTakeover;
    }

    public String getPeriodTimes() {
        return periodTimes;
    }

    public void setPeriodTimes(String periodTimes) {
        this.periodTimes = periodTimes;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getIsOtherAcct() {
        return isOtherAcct;
    }

    public void setIsOtherAcct(String isOtherAcct) {
        this.isOtherAcct = isOtherAcct;
    }

    public String getTotalBjAmt() {
        return totalBjAmt;
    }

    public void setTotalBjAmt(String totalBjAmt) {
        this.totalBjAmt = totalBjAmt;
    }

    public String getTotalFxAmt() {
        return totalFxAmt;
    }

    public void setTotalFxAmt(String totalFxAmt) {
        this.totalFxAmt = totalFxAmt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }


    @Override
    public String toString() {
        return "List{" +
                "iqpSerno='" + iqpSerno + '\'' +
                ", billNo='" + billNo + '\'' +
                ", repayMode='" + repayMode + '\'' +
                ", repayAmt='" + repayAmt + '\'' +
                ", fxAmt='" + fxAmt + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", repayAcctNo='" + repayAcctNo + '\'' +
                ", repayAcctName='" + repayAcctName + '\'' +
                ", isPeriod='" + isPeriod + '\'' +
                ", loanBalance='" + loanBalance + '\'' +
                ", totalTqlxAmt='" + totalTqlxAmt + '\'' +
                ", totalLxAmt='" + totalLxAmt + '\'' +
                ", contNo='" + contNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", repayType='" + repayType + '\'' +
                ", accStatus='" + accStatus + '\'' +
                ", loanStartDate='" + loanStartDate + '\'' +
                ", ysfxAmt='" + ysfxAmt + '\'' +
                ", zcbjAmt='" + zcbjAmt + '\'' +
                ", ysyjlxXmt='" + ysyjlxXmt + '\'' +
                ", isAllowTakeover='" + isAllowTakeover + '\'' +
                ", periodTimes='" + periodTimes + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", isOtherAcct='" + isOtherAcct + '\'' +
                ", totalBjAmt='" + totalBjAmt + '\'' +
                ", totalFxAmt='" + totalFxAmt + '\'' +
                ", status='" + status + '\'' +
                ", inputDate='" + inputDate + '\'' +
                '}';
    }
}
