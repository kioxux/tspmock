package cn.com.yusys.yusp.dto.server.xdtz0029.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询指定票号在信贷台账中是否已贴现
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0029DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "porderNo")
    private String porderNo;//汇票号码
    @JsonProperty(value = "porderAmt")
    private BigDecimal porderAmt;//票面金额
    @JsonProperty(value = "accStatus")
    private String accStatus;//台账状态

    public Xdtz0029DataRespDto() {
    }

    public Xdtz0029DataRespDto(String porderNo, BigDecimal porderAmt, String accStatus) {
        this.porderNo = porderNo;
        this.porderAmt = porderAmt;
        this.accStatus = accStatus;
    }

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

    public BigDecimal getPorderAmt() {
        return porderAmt;
    }

    public void setPorderAmt(BigDecimal porderAmt) {
        this.porderAmt = porderAmt;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

	@Override
	public String toString() {
		return "Xdtz0029DataRespDto{" +
				"porderNo='" + porderNo + '\'' +
				", porderAmt=" + porderAmt +
				", accStatus='" + accStatus + '\'' +
				'}';
	}
}
