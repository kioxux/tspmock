package cn.com.yusys.yusp.dto.server.xdtz0036.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0036DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "maxOverdueDays")
    private String maxOverdueDays;//最大逾期天数

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getMaxOverdueDays() {
        return maxOverdueDays;
    }

    public void setMaxOverdueDays(String maxOverdueDays) {
        this.maxOverdueDays = maxOverdueDays;
    }

    @Override
    public String toString() {
        return "Xdtz0036DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", maxOverdueDays='" + maxOverdueDays + '\'' +
                '}';
    }
}
