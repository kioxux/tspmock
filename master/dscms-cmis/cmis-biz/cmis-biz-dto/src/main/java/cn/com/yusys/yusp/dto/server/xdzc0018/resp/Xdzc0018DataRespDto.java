package cn.com.yusys.yusp.dto.server.xdzc0018.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：我的资产统计查询接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0018DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "assetSumAmt")
    private BigDecimal assetSumAmt;//资产总额
    @JsonProperty(value = "orderSumAmt")
    private BigDecimal orderSumAmt;//存单资产总额
    @JsonProperty(value = "eBillSumAmt")
    private BigDecimal eBillSumAmt;//电票资产总额
    @JsonProperty(value = "creditAssetAmt")
    private BigDecimal creditAssetAmt;//信用证资产总额
    @JsonProperty(value = "orderPer")
    private BigDecimal orderPer;//存单资产占比
    @JsonProperty(value = "eBillPer")
    private BigDecimal eBillPer;//电票资产占比
    @JsonProperty(value = "creditPer")
    private BigDecimal creditPer;//信用证资产占比

    public BigDecimal getAssetSumAmt() {
        return assetSumAmt;
    }

    public void setAssetSumAmt(BigDecimal assetSumAmt) {
        this.assetSumAmt = assetSumAmt;
    }

    public BigDecimal getOrderSumAmt() {
        return orderSumAmt;
    }

    public void setOrderSumAmt(BigDecimal orderSumAmt) {
        this.orderSumAmt = orderSumAmt;
    }

    public BigDecimal getEBillSumAmt() {
        return eBillSumAmt;
    }

    public void setEBillSumAmt(BigDecimal eBillSumAmt) {
        this.eBillSumAmt = eBillSumAmt;
    }

    public BigDecimal getCreditAssetAmt() {
        return creditAssetAmt;
    }

    public void setCreditAssetAmt(BigDecimal creditAssetAmt) {
        this.creditAssetAmt = creditAssetAmt;
    }

    public BigDecimal getOrderPer() {
        return orderPer;
    }

    public void setOrderPer(BigDecimal orderPer) {
        this.orderPer = orderPer;
    }

    public BigDecimal getEBillPer() {
        return eBillPer;
    }

    public void setEBillPer(BigDecimal eBillPer) {
        this.eBillPer = eBillPer;
    }

    public BigDecimal getCreditPer() {
        return creditPer;
    }

    public void setCreditPer(BigDecimal creditPer) {
        this.creditPer = creditPer;
    }

    @Override
    public String toString() {
        return "Xdzc0018DataRespDto{" +
                "assetSumAmt=" + assetSumAmt +
                ", orderSumAmt=" + orderSumAmt +
                ", eBillSumAmt=" + eBillSumAmt +
                ", creditAssetAmt=" + creditAssetAmt +
                ", orderPer=" + orderPer +
                ", eBillPer=" + eBillPer +
                ", creditPer=" + creditPer +
                '}';
    }
}
