package cn.com.yusys.yusp.dto.server.xdzc0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：购销合同查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "tContCnName")
    private String tContCnName;//合同名称
    @JsonProperty(value = "consumeName")
    private String consumeName;//公司名称（消费方）
    @JsonProperty(value = "tContNo")
    private String tContNo;//合同编号
    @JsonProperty(value = "tContAmt")
    private BigDecimal tContAmt;//合同金额
//    @JsonProperty(value = "comtAvaAmt")
//    private BigDecimal comtAvaAmt;//剩余可挂靠金额
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同到期日
    @JsonProperty(value = "tContYXSerno")
    private String tContYXSerno;//贸易合同影像流水号
    @JsonProperty(value = "contNo")
    private String contNo;//资产池协议编号
    @JsonProperty(value = "isBankAcct")
    private String isBankAcct;//交易对手是否本行
    @JsonProperty(value = "toppName")
    private String toppName;//交易对手名称
    @JsonProperty(value = "toppAcctNo")
    private String toppAcctNo;//交易对手账号
    @JsonProperty(value = "opanOrgNo")
    private String opanOrgNo;//开户行行号
    @JsonProperty(value = "opanOrgName")
    private String opanOrgName;//开户行行名
    @JsonProperty(value = "contHighAvlAmt")
    private BigDecimal contHighAvlAmt;//可用信用总金额
    @JsonProperty(value = "settlementMethod")
    private String settlementMethod;//结算方式
    @JsonProperty(value = "status")
    private String status;//状态
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号

    public String gettContCnName() {
        return tContCnName;
    }

    public void settContCnName(String tContCnName) {
        this.tContCnName = tContCnName;
    }

    public String getConsumeName() {
        return consumeName;
    }

    public void setConsumeName(String consumeName) {
        this.consumeName = consumeName;
    }

    public String gettContNo() {
        return tContNo;
    }

    public void settContNo(String tContNo) {
        this.tContNo = tContNo;
    }

    public BigDecimal gettContAmt() {
        return tContAmt;
    }

    public void settContAmt(BigDecimal tContAmt) {
        this.tContAmt = tContAmt;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String gettContYXSerno() {
        return tContYXSerno;
    }

    public void settContYXSerno(String tContYXSerno) {
        this.tContYXSerno = tContYXSerno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getIsBankAcct() {
        return isBankAcct;
    }

    public void setIsBankAcct(String isBankAcct) {
        this.isBankAcct = isBankAcct;
    }

    public String getToppName() {
        return toppName;
    }

    public void setToppName(String toppName) {
        this.toppName = toppName;
    }

    public String getToppAcctNo() {
        return toppAcctNo;
    }

    public void setToppAcctNo(String toppAcctNo) {
        this.toppAcctNo = toppAcctNo;
    }

    public String getOpanOrgNo() {
        return opanOrgNo;
    }

    public void setOpanOrgNo(String opanOrgNo) {
        this.opanOrgNo = opanOrgNo;
    }

    public String getOpanOrgName() {
        return opanOrgName;
    }

    public void setOpanOrgName(String opanOrgName) {
        this.opanOrgName = opanOrgName;
    }

    public BigDecimal getContHighAvlAmt() {
        return contHighAvlAmt;
    }

    public void setContHighAvlAmt(BigDecimal contHighAvlAmt) {
        this.contHighAvlAmt = contHighAvlAmt;
    }

    public String getSettlementMethod() {
        return settlementMethod;
    }

    public void setSettlementMethod(String settlementMethod) {
        this.settlementMethod = settlementMethod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "List{" +
                "tContCnName='" + tContCnName + '\'' +
                ", consumeName='" + consumeName + '\'' +
                ", tContNo='" + tContNo + '\'' +
                ", tContAmt=" + tContAmt +
                ", contStartDate='" + contStartDate + '\'' +
                ", contEndDate='" + contEndDate + '\'' +
                ", tContYXSerno='" + tContYXSerno + '\'' +
                ", contNo='" + contNo + '\'' +
                ", isBankAcct='" + isBankAcct + '\'' +
                ", toppName='" + toppName + '\'' +
                ", toppAcctNo='" + toppAcctNo + '\'' +
                ", opanOrgNo='" + opanOrgNo + '\'' +
                ", opanOrgName='" + opanOrgName + '\'' +
                ", contHighAvlAmt=" + contHighAvlAmt +
                ", settlementMethod='" + settlementMethod + '\'' +
                ", status='" + status + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
