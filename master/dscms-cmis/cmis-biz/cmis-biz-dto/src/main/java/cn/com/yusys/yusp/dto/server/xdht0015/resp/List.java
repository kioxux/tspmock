package cn.com.yusys.yusp.dto.server.xdht0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：合同房产人员信息查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号
    @JsonProperty(value = "cnLoanContNo")
    private String cnLoanContNo;//中文借款合同号
    @JsonProperty(value = "cnGrtContNo")
    private String cnGrtContNo;//中文担保合同号
    @JsonProperty(value = "grtContNo")
    private String grtContNo;//担保合同号
    @JsonProperty(value = "mainClaimsPldContNo")
    private String mainClaimsPldContNo;//主债权合同及抵押合同表编号
    @JsonProperty(value = "realEstateRegiAppbookNo")
    private String realEstateRegiAppbookNo;//不动产登记申请书编号
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号码
    @JsonProperty(value = "debitName")
    private String debitName;//借款人姓名
    @JsonProperty(value = "isMborrow")
    private String isMborrow;//是否主借款人
    @JsonProperty(value = "loanContSignStatus")
    private String loanContSignStatus;//借款合同签约状态
    @JsonProperty(value = "grtContSignStatus")
    private String grtContSignStatus;//担保合同签约状态
    @JsonProperty(value = "mainClaimsPldContSignStatus")
    private String mainClaimsPldContSignStatus;//主债权合同及抵押合同表签约状态
    @JsonProperty(value = "realEstateRegiAppbookSignStatus")
    private String realEstateRegiAppbookSignStatus;//不动产登记申请书签约状态
    @JsonProperty(value = "totlSignStatus")
    private String totlSignStatus;//总签约状态

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getCnLoanContNo() {
        return cnLoanContNo;
    }

    public void setCnLoanContNo(String cnLoanContNo) {
        this.cnLoanContNo = cnLoanContNo;
    }

    public String getCnGrtContNo() {
        return cnGrtContNo;
    }

    public void setCnGrtContNo(String cnGrtContNo) {
        this.cnGrtContNo = cnGrtContNo;
    }

    public String getGrtContNo() {
        return grtContNo;
    }

    public void setGrtContNo(String grtContNo) {
        this.grtContNo = grtContNo;
    }

    public String getMainClaimsPldContNo() {
        return mainClaimsPldContNo;
    }

    public void setMainClaimsPldContNo(String mainClaimsPldContNo) {
        this.mainClaimsPldContNo = mainClaimsPldContNo;
    }

    public String getRealEstateRegiAppbookNo() {
        return realEstateRegiAppbookNo;
    }

    public void setRealEstateRegiAppbookNo(String realEstateRegiAppbookNo) {
        this.realEstateRegiAppbookNo = realEstateRegiAppbookNo;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getDebitName() {
        return debitName;
    }

    public void setDebitName(String debitName) {
        this.debitName = debitName;
    }

    public String getIsMborrow() {
        return isMborrow;
    }

    public void setIsMborrow(String isMborrow) {
        this.isMborrow = isMborrow;
    }

    public String getLoanContSignStatus() {
        return loanContSignStatus;
    }

    public void setLoanContSignStatus(String loanContSignStatus) {
        this.loanContSignStatus = loanContSignStatus;
    }

    public String getGrtContSignStatus() {
        return grtContSignStatus;
    }

    public void setGrtContSignStatus(String grtContSignStatus) {
        this.grtContSignStatus = grtContSignStatus;
    }

    public String getMainClaimsPldContSignStatus() {
        return mainClaimsPldContSignStatus;
    }

    public void setMainClaimsPldContSignStatus(String mainClaimsPldContSignStatus) {
        this.mainClaimsPldContSignStatus = mainClaimsPldContSignStatus;
    }

    public String getRealEstateRegiAppbookSignStatus() {
        return realEstateRegiAppbookSignStatus;
    }

    public void setRealEstateRegiAppbookSignStatus(String realEstateRegiAppbookSignStatus) {
        this.realEstateRegiAppbookSignStatus = realEstateRegiAppbookSignStatus;
    }

    public String getTotlSignStatus() {
        return totlSignStatus;
    }

    public void setTotlSignStatus(String totlSignStatus) {
        this.totlSignStatus = totlSignStatus;
    }

    @Override
    public String toString() {
        return "List{" +
                "loanContNo='" + loanContNo + '\'' +
                "cnLoanContNo='" + cnLoanContNo + '\'' +
                "cnGrtContNo='" + cnGrtContNo + '\'' +
                "grtContNo='" + grtContNo + '\'' +
                "mainClaimsPldContNo='" + mainClaimsPldContNo + '\'' +
                "realEstateRegiAppbookNo='" + realEstateRegiAppbookNo + '\'' +
                "certNo='" + certNo + '\'' +
                "debitName='" + debitName + '\'' +
                "isMborrow='" + isMborrow + '\'' +
                "loanContSignStatus='" + loanContSignStatus + '\'' +
                "grtContSignStatus='" + grtContSignStatus + '\'' +
                "mainClaimsPldContSignStatus='" + mainClaimsPldContSignStatus + '\'' +
                "realEstateRegiAppbookSignStatus='" + realEstateRegiAppbookSignStatus + '\'' +
                "totlSignStatus='" + totlSignStatus + '\'' +
                '}';
    }
}
