package cn.com.yusys.yusp.dto.server.xddh0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：定期检查借据任务
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class PspDebitInfoRep implements Serializable {
    private static final long serialVersionUID = 1L;
    /** 主键 **/
    @JsonProperty(value = "pkId")
    private String pkId;

    /** 任务编号 **/
    @JsonProperty(value = "taskNo")
    private String taskNo;

    /** 客户编号 **/
    @JsonProperty(value = "cusId")
    private String cusId;

    /** 客户名称 **/
    @JsonProperty(value = "cusName")
    private String cusName;

    /** 借据编号 **/
    @JsonProperty(value = "billNo")
    private String billNo;

    /** 合同编号 **/
    @JsonProperty(value = "contNo")
    private String contNo;

    /** 产品编号 **/
    @JsonProperty(value = "prdId")
    private String prdId;

    /** 产品名称 **/
    @JsonProperty(value = "prdName")
    private String prdName;

    /** 担保方式 **/
    @JsonProperty(value = "guarMode")
    private String guarMode;

    /** 借据起始日 **/
    @JsonProperty(value = "loanStartDate")
    private String loanStartDate;

    /** 借据到期日 **/
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;
    /** 贷款金额（元）  **/
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;

    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;

    @JsonProperty(value = "debitInt")
    private BigDecimal debitInt;

    @JsonProperty(value = "overdueDay")
    private Integer overdueDay;

    @JsonProperty(value = "execRateYear")
    private BigDecimal execRateYear;

    @JsonProperty(value = "fiveClass")
    private String fiveClass;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getTaskNo() {
        return taskNo;
    }

    public void setTaskNo(String taskNo) {
        this.taskNo = taskNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }


    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getDebitInt() {
        return debitInt;
    }

    public void setDebitInt(BigDecimal debitInt) {
        this.debitInt = debitInt;
    }

    public Integer getOverdueDay() {
        return overdueDay;
    }

    public void setOverdueDay(Integer overdueDay) {
        this.overdueDay = overdueDay;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    @Override
    public String toString() {
        return "PspDebitInfoRep{" +
                "pkId='" + pkId + '\'' +
                "taskNo='" + taskNo + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "contNo='" + contNo + '\'' +
                "billNo='" + billNo + '\'' +
                "prdId='" + prdId + '\'' +
                "prdName='" + prdName + '\'' +
                "guarMode='" + guarMode + '\'' +
                "loanStartDate='" + loanStartDate + '\'' +
                "loanEndDate='" + loanEndDate + '\'' +
                "loanAmt='" + loanAmt + '\'' +
                "loanBalance='" + loanBalance + '\'' +
                "debitInt='" + debitInt + '\'' +
                "overdueDay='" + overdueDay + '\'' +
                "execRateYear='" + execRateYear + '\'' +
                "fiveClass='" + fiveClass + '\'' +
                '}';
    }
}
