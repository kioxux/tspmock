package cn.com.yusys.yusp.dto.server.xdxw0078.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：根据客户身份证号查询线上产品申请记录
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "inspectSerno")
    private String inspectSerno;//勘验流水号
    @JsonProperty(value = "surveySerno")
    private String surveySerno;//调查流水号
    @JsonProperty(value = "cloudEvalNo")
    private String cloudEvalNo;//云评估编号
    @JsonProperty(value = "inspector")
    private String inspector;//勘验人
    @JsonProperty(value = "inspectorCertType")
    private String inspectorCertType;//勘验人证件类型
    @JsonProperty(value = "inspectorCertCode")
    private String inspectorCertCode;//勘验人证件号码
    @JsonProperty(value = "pawnOwner")
    private String pawnOwner;//抵押物所有人
    @JsonProperty(value = "buildingName")
    private String buildingName;//楼盘名称
    @JsonProperty(value = "building")
    private String building;//楼栋
    @JsonProperty(value = "squ")
    private BigDecimal squ;//面积
    @JsonProperty(value = "addr")
    private String addr;//地址
    @JsonProperty(value = "videoSerno")
    private String videoSerno;//视频流水号
    @JsonProperty(value = "inspectStatus")
    private String inspectStatus;//勘验状态

    public String getInspectSerno() {
        return inspectSerno;
    }

    public void setInspectSerno(String inspectSerno) {
        this.inspectSerno = inspectSerno;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public String getCloudEvalNo() {
        return cloudEvalNo;
    }

    public void setCloudEvalNo(String cloudEvalNo) {
        this.cloudEvalNo = cloudEvalNo;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public String getInspectorCertType() {
        return inspectorCertType;
    }

    public void setInspectorCertType(String inspectorCertType) {
        this.inspectorCertType = inspectorCertType;
    }

    public String getInspectorCertCode() {
        return inspectorCertCode;
    }

    public void setInspectorCertCode(String inspectorCertCode) {
        this.inspectorCertCode = inspectorCertCode;
    }

    public String getPawnOwner() {
        return pawnOwner;
    }

    public void setPawnOwner(String pawnOwner) {
        this.pawnOwner = pawnOwner;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public BigDecimal getSqu() {
        return squ;
    }

    public void setSqu(BigDecimal squ) {
        this.squ = squ;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getVideoSerno() {
        return videoSerno;
    }

    public void setVideoSerno(String videoSerno) {
        this.videoSerno = videoSerno;
    }

    public String getInspectStatus() {
        return inspectStatus;
    }

    public void setInspectStatus(String inspectStatus) {
        this.inspectStatus = inspectStatus;
    }

    @Override
    public String toString() {
        return "List{" +
                "inspectSerno='" + inspectSerno + '\'' +
                "surveySerno='" + surveySerno + '\'' +
                "cloudEvalNo='" + cloudEvalNo + '\'' +
                "inspector='" + inspector + '\'' +
                "inspectorCertType='" + inspectorCertType + '\'' +
                "inspectorCertCode='" + inspectorCertCode + '\'' +
                "pawnOwner='" + pawnOwner + '\'' +
                "buildingName='" + buildingName + '\'' +
                "building='" + building + '\'' +
                "squ='" + squ + '\'' +
                "addr='" + addr + '\'' +
                "videoSerno='" + videoSerno + '\'' +
                "inspectStatus='" + inspectStatus + '\'' +
                '}';
    }
}
