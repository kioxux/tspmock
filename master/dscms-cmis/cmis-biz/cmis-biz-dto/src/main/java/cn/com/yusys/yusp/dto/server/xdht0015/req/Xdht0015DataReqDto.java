package cn.com.yusys.yusp.dto.server.xdht0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：合同房产人员信息查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0015DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号码
    @JsonProperty(value = "prdCode")
    private String prdCode;//产品码

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getPrdCode() {
        return prdCode;
    }

    public void setPrdCode(String prdCode) {
        this.prdCode = prdCode;
    }

    @Override
    public String toString() {
        return "Xdht0015DataReqDto{" +
                "certNo='" + certNo + '\'' +
                "prdCode='" + prdCode + '\'' +
                '}';
    }
}
