package cn.com.yusys.yusp.dto.server.xdsx0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：专业贷款评级结果同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0006DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "evalAppSerno")
    private String evalAppSerno;//评级申请流水号
    @JsonProperty(value = "lmtAppSerno")
    private String lmtAppSerno;//授信申请流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "evalYear")
    private String evalYear;//评级年度
    @JsonProperty(value = "evalHppType")
    private String evalHppType;//评级发生类型
    @JsonProperty(value = "evalModel")
    private String evalModel;//评级模型
    @JsonProperty(value = "majorLoanStartLvl")
    private String majorLoanStartLvl;//专业贷款系统初始等级
    @JsonProperty(value = "majorLoanAdjdLvl")
    private String majorLoanAdjdLvl;//专业贷款调整后等级
    @JsonProperty(value = "majorLoanAdviceLvl")
    private String majorLoanAdviceLvl;//专业贷款建议等级
    @JsonProperty(value = "majorLoanFinalLvl")
    private String majorLoanFinalLvl;//专业贷款最终认定等级
    @JsonProperty(value = "majorLoanExpectLossRate")
    private BigDecimal majorLoanExpectLossRate;//专业贷款预期损失率
    @JsonProperty(value = "evalStartDate")
    private String evalStartDate;//评级生效日
    @JsonProperty(value = "evalEndDate")
    private String evalEndDate;//评级到期日
    @JsonProperty(value = "riskTypeMax")
    private String riskTypeMax;//风险大类
    @JsonProperty(value = "riskType")
    private String riskType;//风险种类
    @JsonProperty(value = "riskTypeMin")
    private String riskTypeMin;//风险小类
    @JsonProperty(value = "riskDtghDate")
    private String riskDtghDate;//风险划分日期

    public String getEvalAppSerno() {
        return evalAppSerno;
    }

    public void setEvalAppSerno(String evalAppSerno) {
        this.evalAppSerno = evalAppSerno;
    }

    public String getLmtAppSerno() {
        return lmtAppSerno;
    }

    public void setLmtAppSerno(String lmtAppSerno) {
        this.lmtAppSerno = lmtAppSerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getEvalYear() {
        return evalYear;
    }

    public void setEvalYear(String evalYear) {
        this.evalYear = evalYear;
    }

    public String getEvalHppType() {
        return evalHppType;
    }

    public void setEvalHppType(String evalHppType) {
        this.evalHppType = evalHppType;
    }

    public String getEvalModel() {
        return evalModel;
    }

    public void setEvalModel(String evalModel) {
        this.evalModel = evalModel;
    }

    public String getMajorLoanStartLvl() {
        return majorLoanStartLvl;
    }

    public void setMajorLoanStartLvl(String majorLoanStartLvl) {
        this.majorLoanStartLvl = majorLoanStartLvl;
    }

    public String getMajorLoanAdjdLvl() {
        return majorLoanAdjdLvl;
    }

    public void setMajorLoanAdjdLvl(String majorLoanAdjdLvl) {
        this.majorLoanAdjdLvl = majorLoanAdjdLvl;
    }

    public String getMajorLoanAdviceLvl() {
        return majorLoanAdviceLvl;
    }

    public void setMajorLoanAdviceLvl(String majorLoanAdviceLvl) {
        this.majorLoanAdviceLvl = majorLoanAdviceLvl;
    }

    public String getMajorLoanFinalLvl() {
        return majorLoanFinalLvl;
    }

    public void setMajorLoanFinalLvl(String majorLoanFinalLvl) {
        this.majorLoanFinalLvl = majorLoanFinalLvl;
    }

    public BigDecimal getMajorLoanExpectLossRate() {
        return majorLoanExpectLossRate;
    }

    public void setMajorLoanExpectLossRate(BigDecimal majorLoanExpectLossRate) {
        this.majorLoanExpectLossRate = majorLoanExpectLossRate;
    }

    public String getEvalStartDate() {
        return evalStartDate;
    }

    public void setEvalStartDate(String evalStartDate) {
        this.evalStartDate = evalStartDate;
    }

    public String getEvalEndDate() {
        return evalEndDate;
    }

    public void setEvalEndDate(String evalEndDate) {
        this.evalEndDate = evalEndDate;
    }

    public String getRiskTypeMax() {
        return riskTypeMax;
    }

    public void setRiskTypeMax(String riskTypeMax) {
        this.riskTypeMax = riskTypeMax;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public String getRiskTypeMin() {
        return riskTypeMin;
    }

    public void setRiskTypeMin(String riskTypeMin) {
        this.riskTypeMin = riskTypeMin;
    }

    public String getRiskDtghDate() {
        return riskDtghDate;
    }

    public void setRiskDtghDate(String riskDtghDate) {
        this.riskDtghDate = riskDtghDate;
    }

    @Override
    public String toString() {
        return "Xdsx0006DataReqDto { " +
                "evalAppSerno='" + evalAppSerno + '\'' +
                "lmtAppSerno='" + lmtAppSerno + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "certType='" + certType + '\'' +
                "certNo='" + certNo + '\'' +
                "evalYear='" + evalYear + '\'' +
                "evalHppType='" + evalHppType + '\'' +
                "evalModel='" + evalModel + '\'' +
                "majorLoanStartLvl='" + majorLoanStartLvl + '\'' +
                "majorLoanAdjdLvl='" + majorLoanAdjdLvl + '\'' +
                "majorLoanAdviceLvl='" + majorLoanAdviceLvl + '\'' +
                "majorLoanFinalLvl='" + majorLoanFinalLvl + '\'' +
                "majorLoanExpectLossRate='" + majorLoanExpectLossRate + '\'' +
                "evalStartDate='" + evalStartDate + '\'' +
                "evalEndDate='" + evalEndDate + '\'' +
                "riskTypeMax='" + riskTypeMax + '\'' +
                "riskType='" + riskType + '\'' +
                "riskTypeMin='" + riskTypeMin + '\'' +
                "riskDtghDate='" + riskDtghDate + '\'' +
                '}';
    }
}
