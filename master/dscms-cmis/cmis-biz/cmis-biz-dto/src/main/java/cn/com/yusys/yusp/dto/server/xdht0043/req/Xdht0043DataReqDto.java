package cn.com.yusys.yusp.dto.server.xdht0043.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：小贷借款合同文本生成pdf
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0043DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "certNo")
    private String certNo;//身份证件号码
    @JsonProperty(value = "certType")
    private String certType;//身份证件类型
    @JsonProperty(value = "applyDate")
    private String applyDate;//申请日期
    @JsonProperty(value = "isdzqy")
    private String isdzqy;//是否电子签约:1-是 0-否

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public String getIsdzqy() {
        return isdzqy;
    }

    public void setIsdzqy(String isdzqy) {
        this.isdzqy = isdzqy;
    }

    @Override
    public String toString() {
        return "Xdht0043DataReqDto{" +
                "cusName='" + cusName + '\'' +
                "contNo='" + contNo + '\'' +
                "certNo='" + certNo + '\'' +
                "certType='" + certType + '\'' +
                "applyDate='" + applyDate + '\'' +
                "isdzqy='" + isdzqy + '\'' +
                '}';
    }
}
