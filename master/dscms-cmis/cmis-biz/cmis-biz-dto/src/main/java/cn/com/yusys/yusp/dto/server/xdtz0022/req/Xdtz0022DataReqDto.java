package cn.com.yusys.yusp.dto.server.xdtz0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：保证金台账入账
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0022DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "busiCurType")
    private String busiCurType;//业务币种
    @JsonProperty(value = "busiBal")
    private BigDecimal busiBal;//业务余额
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "bailAmt")
    private BigDecimal bailAmt;//保证金金额
    @JsonProperty(value = "exchgRate")
    private BigDecimal exchgRate;//汇率
    @JsonProperty(value = "bizType")
    private String bizType;//业务品种

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBusiCurType() {
        return busiCurType;
    }

    public void setBusiCurType(String busiCurType) {
        this.busiCurType = busiCurType;
    }

    public BigDecimal getBusiBal() {
        return busiBal;
    }

    public void setBusiBal(BigDecimal busiBal) {
        this.busiBal = busiBal;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public BigDecimal getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(BigDecimal bailAmt) {
        this.bailAmt = bailAmt;
    }

    public BigDecimal getExchgRate() {
        return exchgRate;
    }

    public void setExchgRate(BigDecimal exchgRate) {
        this.exchgRate = exchgRate;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    @Override
    public String toString() {
        return "Xdtz0022DataReqDto{" +
                "billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", busiCurType='" + busiCurType + '\'' +
                ", busiBal=" + busiBal +
                ", applyAmt=" + applyAmt +
                ", bailAmt=" + bailAmt +
                ", exchgRate=" + exchgRate +
                ", bizType='" + bizType + '\'' +
                '}';
    }
}
