package cn.com.yusys.yusp.dto.server.cmisbiz0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 集团客户授信复审
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisBiz0002RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "result")
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CmisBiz0002RespDto{" +
                "result='" + result + '\'' +
                '}';
    }
}
