package cn.com.yusys.yusp.dto.server.xdtz0034.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据客户号查询申请人是否有行内信用记录
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0034DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHavingCdtRecord")
    private String isHavingCdtRecord;//是否有行内信用记录

    public String getIsHavingCdtRecord() {
        return isHavingCdtRecord;
    }

    public void setIsHavingCdtRecord(String isHavingCdtRecord) {
        this.isHavingCdtRecord = isHavingCdtRecord;
    }

    @Override
    public String toString() {
        return "Xdtz0034DataRespDto{" +
                "isHavingCdtRecord='" + isHavingCdtRecord + '\'' +
                '}';
    }
}
