package cn.com.yusys.yusp.dto.server.xdxw0079.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：小微续贷白名单查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0079DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "type")
    private String type;//接口类型（0:校验白名单查询；1：名单查询；2：名单更新）
    @JsonProperty(value = "certCode")
    private String certCode;//申请人证件号
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdxw0079.req.List> list;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public java.util.List<cn.com.yusys.yusp.dto.server.xdxw0079.req.List> getList() {
        return list;
    }

    public void setList(java.util.List<cn.com.yusys.yusp.dto.server.xdxw0079.req.List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdxw0079DataReqDto{" +
                "type='" + type + '\'' +
                "certCode='" + certCode + '\'' +
                "list='" + list + '\'' +
                '}';
    }
}
