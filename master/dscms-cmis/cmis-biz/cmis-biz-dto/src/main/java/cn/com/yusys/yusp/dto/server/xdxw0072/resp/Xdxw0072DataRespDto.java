package cn.com.yusys.yusp.dto.server.xdxw0072.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据调查表编号前往信贷查找客户经理信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0072DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "managerId")
	private String managerId;//客户经理ID
	@JsonProperty(value = "managerName")
	private String managerName;//客户经理姓名

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	@Override
	public String toString() {
		return "Xdxw0072DataRespDto{" +
				"managerId='" + managerId + '\'' +
				", managerName='" + managerName + '\'' +
				'}';
	}
}
