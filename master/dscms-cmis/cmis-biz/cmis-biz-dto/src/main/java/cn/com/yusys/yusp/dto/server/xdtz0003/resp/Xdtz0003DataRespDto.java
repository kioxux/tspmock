package cn.com.yusys.yusp.dto.server.xdtz0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询小微借据余额
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0003DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "loanBal")
    private BigDecimal loanBal;//贷款余额

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    @Override
    public String toString() {
        return "Xdtz0003DataRespDto{" +
                "loanBal=" + loanBal +
                '}';
    }
}
