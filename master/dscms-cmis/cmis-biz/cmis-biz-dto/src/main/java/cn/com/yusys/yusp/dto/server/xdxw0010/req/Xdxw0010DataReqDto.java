package cn.com.yusys.yusp.dto.server.xdxw0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：勘验列表信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0010DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//勘验人身份证号（客户）
    @NotNull(message = "页数【startPageNum】不能为空！")
    @JsonProperty(value = "startPageNum")
    private Integer startPageNum;//开始页数
    @NotNull(message = "每页数【pageSize】不能为空！")
    @JsonProperty(value = "pageSize")
    private Integer pageSize;//每页数
    @JsonProperty(value = "status")
    private String status;//状态
    @JsonProperty(value = "surveySerno")
    private String surveySerno;//调查流水号

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public Integer getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(Integer startPageNum) {
        this.startPageNum = startPageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    @Override
    public String toString() {
        return "Xdxw0010DataReqDto{" +
                "certNo='" + certNo + '\'' +
                ", startPageNum=" + startPageNum +
                ", pageSize=" + pageSize +
                ", status='" + status + '\'' +
                ", surveySerno='" + surveySerno + '\'' +
                '}';
    }
}
