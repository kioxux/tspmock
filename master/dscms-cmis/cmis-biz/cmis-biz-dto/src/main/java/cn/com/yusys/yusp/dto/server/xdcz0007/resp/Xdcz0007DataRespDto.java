package cn.com.yusys.yusp.dto.server.xdcz0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询敞口额度及保证金校验
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0007DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "orderBal")
    private String orderBal;//敞开余额

    public String getOrderBal() {
        return orderBal;
    }

    public void setOrderBal(String orderBal) {
        this.orderBal = orderBal;
    }

    @Override
    public String toString() {
        return "Xdcz0007DataRespDto{" +
                "orderBal='" + orderBal + '\'' +
                '}';
    }
}