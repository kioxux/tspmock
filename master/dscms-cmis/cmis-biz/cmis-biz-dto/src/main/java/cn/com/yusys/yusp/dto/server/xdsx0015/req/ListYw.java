package cn.com.yusys.yusp.dto.server.xdsx0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 14:52:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/19 14:52
 * @since 2021/5/19 14:52
 */
@JsonPropertyOrder(alphabetic = true)
public class ListYw implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "biz_type")
    private String biz_type;//业务品种
    @JsonProperty(value = "crd_lmt")
    private String crd_lmt;//额度
    @JsonProperty(value = "mini_rate")
    private String mini_rate;//利率最低上浮比例
    @JsonProperty(value = "apply_amount")
    private String apply_amount;//贷款申请金额(元)
    @JsonProperty(value = "loan_term")
    private String loan_term;//申请期限(月)
    @JsonProperty(value = "ruling_ir")
    private String ruling_ir;//基准利率(年)
    @JsonProperty(value = "reality_ir_y")
    private String reality_ir_y;//执行利率(年)
    @JsonProperty(value = "floating_rate")
    private String floating_rate;//浮动比例
    @JsonProperty(value = "apply_score")
    private String apply_score;//申请评分
    @JsonProperty(value = "apply_score_risk_lvl")
    private String apply_score_risk_lvl;//申请评分风险等级
    @JsonProperty(value = "rule_risk_lvl")
    private String rule_risk_lvl;//规则风险等级
    @JsonProperty(value = "complex_risk_lvl")
    private String complex_risk_lvl;//综合风险等级
    @JsonProperty(value = "lmt_advice")
    private String lmt_advice;//额度建议
    @JsonProperty(value = "price_advice")
    private String price_advice;//定价建议
    @JsonProperty(value = "flag_serno")
    private String flag_serno;//标志字段

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getCrd_lmt() {
        return crd_lmt;
    }

    public void setCrd_lmt(String crd_lmt) {
        this.crd_lmt = crd_lmt;
    }

    public String getMini_rate() {
        return mini_rate;
    }

    public void setMini_rate(String mini_rate) {
        this.mini_rate = mini_rate;
    }

    public String getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(String apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getLoan_term() {
        return loan_term;
    }

    public void setLoan_term(String loan_term) {
        this.loan_term = loan_term;
    }

    public String getRuling_ir() {
        return ruling_ir;
    }

    public void setRuling_ir(String ruling_ir) {
        this.ruling_ir = ruling_ir;
    }

    public String getReality_ir_y() {
        return reality_ir_y;
    }

    public void setReality_ir_y(String reality_ir_y) {
        this.reality_ir_y = reality_ir_y;
    }

    public String getFloating_rate() {
        return floating_rate;
    }

    public void setFloating_rate(String floating_rate) {
        this.floating_rate = floating_rate;
    }

    public String getApply_score() {
        return apply_score;
    }

    public void setApply_score(String apply_score) {
        this.apply_score = apply_score;
    }

    public String getApply_score_risk_lvl() {
        return apply_score_risk_lvl;
    }

    public void setApply_score_risk_lvl(String apply_score_risk_lvl) {
        this.apply_score_risk_lvl = apply_score_risk_lvl;
    }

    public String getRule_risk_lvl() {
        return rule_risk_lvl;
    }

    public void setRule_risk_lvl(String rule_risk_lvl) {
        this.rule_risk_lvl = rule_risk_lvl;
    }

    public String getComplex_risk_lvl() {
        return complex_risk_lvl;
    }

    public void setComplex_risk_lvl(String complex_risk_lvl) {
        this.complex_risk_lvl = complex_risk_lvl;
    }

    public String getLmt_advice() {
        return lmt_advice;
    }

    public void setLmt_advice(String lmt_advice) {
        this.lmt_advice = lmt_advice;
    }

    public String getPrice_advice() {
        return price_advice;
    }

    public void setPrice_advice(String price_advice) {
        this.price_advice = price_advice;
    }

    public String getFlag_serno() {
        return flag_serno;
    }

    public void setFlag_serno(String flag_serno) {
        this.flag_serno = flag_serno;
    }

    @Override
    public String toString() {
        return "ListYw{" +
                "biz_type='" + biz_type + '\'' +
                ", crd_lmt='" + crd_lmt + '\'' +
                ", mini_rate='" + mini_rate + '\'' +
                ", apply_amount='" + apply_amount + '\'' +
                ", loan_term='" + loan_term + '\'' +
                ", ruling_ir='" + ruling_ir + '\'' +
                ", reality_ir_y='" + reality_ir_y + '\'' +
                ", floating_rate='" + floating_rate + '\'' +
                ", apply_score='" + apply_score + '\'' +
                ", apply_score_risk_lvl='" + apply_score_risk_lvl + '\'' +
                ", rule_risk_lvl='" + rule_risk_lvl + '\'' +
                ", complex_risk_lvl='" + complex_risk_lvl + '\'' +
                ", lmt_advice='" + lmt_advice + '\'' +
                ", price_advice='" + price_advice + '\'' +
                ", flag_serno='" + flag_serno + '\'' +
                '}';
    }
}
