package cn.com.yusys.yusp.dto.server.xdht0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：信贷贴现合同号查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "discAgrNo")
    private String discAgrNo;//贴现协议编号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getDiscAgrNo() {
        return discAgrNo;
    }

    public void setDiscAgrNo(String discAgrNo) {
        this.discAgrNo = discAgrNo;
    }

    @Override
    public String toString() {
        return "Xdht0003DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", discAgrNo='" + discAgrNo + '\'' +
                '}';
    }
}
