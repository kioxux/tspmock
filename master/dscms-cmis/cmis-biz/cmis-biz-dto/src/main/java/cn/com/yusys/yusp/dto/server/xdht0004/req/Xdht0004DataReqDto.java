package cn.com.yusys.yusp.dto.server.xdht0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：合同信息列表查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0004DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "type")
    private String type;//类型
    @JsonProperty(value = "busiClass")
    private String busiClass;//业务细分
    @JsonProperty(value = "cusId")
    private String cusId;//客户号

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBusiClass() {
        return busiClass;
    }

    public void setBusiClass(String busiClass) {
        this.busiClass = busiClass;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdht0004DataReqDto{" +
                "type='" + type + '\'' +
                ", busiClass='" + busiClass + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
