package cn.com.yusys.yusp.dto.server.xddb0018.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：押品状态同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0018DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarNo")
    @NotNull(message = "押品编号guarNo不能为空")
    @NotEmpty(message = "押品编号guarNo不能为空")
    private String guarNo;//押品编号
    @JsonProperty(value = "guarStatus")
    @NotNull(message = "押品状态状态guarStatus不能为空")
    @NotEmpty(message = "押品状态状态guarStatus不能为空")
    private String guarStatus;//押品状态状态

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getGuarStatus() {
        return guarStatus;
    }

    public void setGuarStatus(String guarStatus) {
        this.guarStatus = guarStatus;
    }

    @Override
    public String toString() {
        return "Xddb0018DataReqDto{" +
                "guarNo='" + guarNo + '\'' +
                ", guarStatus='" + guarStatus + '\'' +
                '}';
    }
}