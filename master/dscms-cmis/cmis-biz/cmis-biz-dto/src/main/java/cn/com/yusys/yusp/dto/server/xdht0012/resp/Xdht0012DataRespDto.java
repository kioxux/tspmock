package cn.com.yusys.yusp.dto.server.xdht0012.resp;

import cn.com.yusys.yusp.dto.server.xdht0011.resp.AccountList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.GuarContList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.HxdLoanContList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.LoanContList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Data：借款担保合同签订/支用
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0012DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pdfurl")
    private String pdfurl;//合同pdf文件路径
    @JsonProperty(value = "pvpSerno")
    private String pvpSerno;//出账流水号
    @JsonProperty(value = "pvpList")
    private java.util.List<PvpList> pvpList;
    @JsonProperty(value = "billList")
    private java.util.List<BillList> billList;

    public String getPdfurl() {
        return pdfurl;
    }

    public void setPdfurl(String pdfurl) {
        this.pdfurl = pdfurl;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public List<PvpList> getPvpList() {
        return pvpList;
    }

    public void setPvpList(List<PvpList> pvpList) {
        this.pvpList = pvpList;
    }

    public List<BillList> getBillList() {
        return billList;
    }

    public void setBillList(List<BillList> billList) {
        this.billList = billList;
    }

    @Override
    public String toString() {
        return "Xdht0012DataRespDto{" +
                "pdfurl='" + pdfurl + '\'' +
                "pvpSerno='" + pvpSerno + '\'' +
                "pvpList=" + pvpList +
                "billList=" + billList +
                '}';
    }
}
