package cn.com.yusys.yusp.dto.server.xdtz0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：取得记账日期为当天的提前还款授权表信息一览
 *
 * @author zhaoyue
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0013DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "billList")
    private java.util.List<BillList> billList;

    public java.util.List<BillList> getBillList() {
        return billList;
    }

    public void setBillList(java.util.List<BillList> billList) {
        this.billList = billList;
    }

    @Override
    public String toString() {
        return "Xdtz0013DataRespDto{" +
                "billList=" + billList +
                '}';
    }
}
