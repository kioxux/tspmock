package cn.com.yusys.yusp.dto.server.xdcz0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：支用列表查询(微信小程序)
 *
 * @author xs
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//支用金额
    @JsonProperty(value = "bizType")
    private String bizType;//产品类型
    @JsonProperty(value = "billStartDate")
    private String billStartDate;//借据起始日
    @JsonProperty(value = "billEndDate")
    private String billEndDate;//借据到期日
    @JsonProperty(value = "loanAcctNo")
    private String loanAcctNo;//借款卡号
    @JsonProperty(value = "repayAcctNo")
    private String repayAcctNo;//还款卡号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "loanStatus")
    private String loanStatus;//支用状态
    @JsonProperty(value = "phone")
    private String phone;//联系电话
    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getBillStartDate() {
        return billStartDate;
    }

    public void setBillStartDate(String billStartDate) {
        this.billStartDate = billStartDate;
    }

    public String getBillEndDate() {
        return billEndDate;
    }

    public void setBillEndDate(String billEndDate) {
        this.billEndDate = billEndDate;
    }

    public String getLoanAcctNo() {
        return loanAcctNo;
    }

    public void setLoanAcctNo(String loanAcctNo) {
        this.loanAcctNo = loanAcctNo;
    }

    public String getRepayAcctNo() {
        return repayAcctNo;
    }

    public void setRepayAcctNo(String repayAcctNo) {
        this.repayAcctNo = repayAcctNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Override
    public String toString() {
        return "List{" +
                "loanAmt=" + loanAmt +
                ", bizType='" + bizType + '\'' +
                ", billStartDate='" + billStartDate + '\'' +
                ", billEndDate='" + billEndDate + '\'' +
                ", loanAcctNo='" + loanAcctNo + '\'' +
                ", repayAcctNo='" + repayAcctNo + '\'' +
                ", prdName='" + prdName + '\'' +
                ", cusName='" + cusName + '\'' +
                ", repayType='" + repayType + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", loanStatus='" + loanStatus + '\'' +
                ", phone='" + phone + '\'' +
                ", billNo='" + billNo + '\'' +
                '}';
    }
}
