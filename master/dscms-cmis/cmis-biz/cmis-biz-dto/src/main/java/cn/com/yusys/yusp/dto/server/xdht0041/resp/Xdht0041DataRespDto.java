package cn.com.yusys.yusp.dto.server.xdht0041.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询受托记录状态
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0041DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "statusCode")
    private String statusCode;//状态码

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "Xdht0041DataRespDto{" +
                "statusCode='" + statusCode + '\'' +
                '}';
    }
}