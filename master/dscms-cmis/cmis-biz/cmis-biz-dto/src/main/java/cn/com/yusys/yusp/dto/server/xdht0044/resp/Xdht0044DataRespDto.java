package cn.com.yusys.yusp.dto.server.xdht0044.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：房群客户查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0044DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHouseCus")
    private String isHouseCus;//是否是房群客户

    public String getIsHouseCus() {
        return isHouseCus;
    }

    public void setIsHouseCus(String isHouseCus) {
        this.isHouseCus = isHouseCus;
    }

    @Override
    public String toString() {
        return "Xdht0044DataRespDto{" +
                "isHouseCus='" + isHouseCus + '\'' +
                '}';
    }
}
