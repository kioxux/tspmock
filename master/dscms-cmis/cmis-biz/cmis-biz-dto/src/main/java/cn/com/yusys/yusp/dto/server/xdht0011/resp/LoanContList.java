package cn.com.yusys.yusp.dto.server.xdht0011.resp;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LoanContList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "outstndLmt")
    private BigDecimal outstndLmt;//已用额度
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同起始日
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同到期日
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "signType")
    private String signType;//签约方式
    @JsonProperty(value = "signFlag")
    private String signFlag;//是否签约
    @JsonProperty(value = "realityIrY")
    private String realityIrY;//执行利率年
    @JsonProperty(value = "entruPayAcctNo")
    private String entruPayAcctNo;//受托支付账号
    @JsonProperty(value = "entruPayAcctName")
    private String entruPayAcctName;//受托账号名
    @JsonProperty(value = "entruAcctb")
    private String entruAcctb;//受托开户行
    @JsonProperty(value = "signChnl")
    private String signChnl;//签约渠道
    @JsonProperty(value = "prdId")
    private String prdId;//产品号
    @JsonProperty(value = "iqpSerno")
    private String iqpSerno;//合同申请流水号

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getOutstndLmt() {
        return outstndLmt;
    }

    public void setOutstndLmt(BigDecimal outstndLmt) {
        this.outstndLmt = outstndLmt;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getSignType() {
        return signType;
    }

    public void setSignType(String signType) {
        this.signType = signType;
    }

    public String getSignFlag() {
        return signFlag;
    }

    public void setSignFlag(String signFlag) {
        this.signFlag = signFlag;
    }

    public String getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(String realityIrY) {
        this.realityIrY = realityIrY;
    }

    public String getEntruPayAcctNo() {
        return entruPayAcctNo;
    }

    public void setEntruPayAcctNo(String entruPayAcctNo) {
        this.entruPayAcctNo = entruPayAcctNo;
    }

    public String getEntruPayAcctName() {
        return entruPayAcctName;
    }

    public void setEntruPayAcctName(String entruPayAcctName) {
        this.entruPayAcctName = entruPayAcctName;
    }

    public String getEntruAcctb() {
        return entruAcctb;
    }

    public void setEntruAcctb(String entruAcctb) {
        this.entruAcctb = entruAcctb;
    }

    public String getSignChnl() {
        return signChnl;
    }

    public void setSignChnl(String signChnl) {
        this.signChnl = signChnl;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    @Override
    public String toString() {
        return "LoanContList{" +
                "contNo='" + contNo + '\'' +
                "cnContNo='" + cnContNo + '\'' +
                "contType='" + contType + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "certNo='" + certNo + '\'' +
                "prdName='" + prdName + '\'' +
                "assureMeans='" + assureMeans + '\'' +
                "repayType='" + repayType + '\'' +
                "contAmt='" + contAmt + '\'' +
                "outstndLmt='" + outstndLmt + '\'' +
                "contStartDate='" + contStartDate + '\'' +
                "contEndDate='" + contEndDate + '\'' +
                "contStatus='" + contStatus + '\'' +
                "signType='" + signType + '\'' +
                "signFlag='" + signFlag + '\'' +
                "realityIrY='" + realityIrY + '\'' +
                "entruPayAcctNo='" + entruPayAcctNo + '\'' +
                "entruPayAcctName='" + entruPayAcctName + '\'' +
                "entruAcctb='" + entruAcctb + '\'' +
                "signChnl='" + signChnl + '\'' +
                "prdId='" + prdId + '\'' +
                '}';
    }
}
