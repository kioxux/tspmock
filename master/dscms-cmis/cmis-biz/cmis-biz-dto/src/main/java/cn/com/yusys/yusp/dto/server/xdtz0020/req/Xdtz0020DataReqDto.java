package cn.com.yusys.yusp.dto.server.xdtz0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：台账入账（保函）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0020DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bizType")
    private String bizType;//业务类型
    @JsonProperty(value = "oprtype")
    private String oprtype;//操作类型
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "guarantType")
    private String guarantType;//保函种类
    @JsonProperty(value = "guarantTypeName")
    private String guarantTypeName;//保函种类名称
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "guarantAmt")
    private BigDecimal guarantAmt;//保函金额
    @JsonProperty(value = "guarantAmtCurType")
    private String guarantAmtCurType;//保函金额币种
    @JsonProperty(value = "guarantBal")
    private BigDecimal guarantBal;//保函余额
    @JsonProperty(value = "startDate")
    private String startDate;//生效日期
    @JsonProperty(value = "endDate")
    private String endDate;//失效日期
    @JsonProperty(value = "settlAcctNo")
    private String settlAcctNo;//结算账号
    @JsonProperty(value = "settlAcctNoName")
    private String settlAcctNoName;//结算账号名称
    @JsonProperty(value = "riskSpacPerc")
    private String riskSpacPerc;//风险敞口比例
    @JsonProperty(value = "riskSpacAmt")
    private BigDecimal riskSpacAmt;//风险敞口金额
    @JsonProperty(value = "chrgRate")
    private BigDecimal chrgRate;//手续费率
    @JsonProperty(value = "chrgAmt")
    private BigDecimal chrgAmt;//手续费金额
    @JsonProperty(value = "overduePadYearRate")
    private BigDecimal overduePadYearRate;//逾期垫款年利率
    @JsonProperty(value = "guarantPayType")
    private String guarantPayType;//保函付款方式
    @JsonProperty(value = "beneficiar")
    private String beneficiar;//受益人
    @JsonProperty(value = "beneficiarAcctb")
    private String beneficiarAcctb;//受益人开户行
    @JsonProperty(value = "beneficiarAcctNo")
    private String beneficiarAcctNo;//受益人账号
    @JsonProperty(value = "accStatus")
    private String accStatus;//台账状态
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "finaBrId")
    private String finaBrId;//账务机构
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//责任机构
    @JsonProperty(value = "cretQuotationExchgRate")
    private BigDecimal cretQuotationExchgRate;//信贷牌价汇率
    @JsonProperty(value = "pvpSerno")
    private String pvpSerno;//交易编号
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "bailAmt")
    private String bailAmt;//保证金金额（人民币）
    @JsonProperty(value = "exchgRate")
    private String exchgRate;//汇率

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getOprtype() {
        return oprtype;
    }

    public void setOprtype(String oprtype) {
        this.oprtype = oprtype;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getGuarantType() {
        return guarantType;
    }

    public void setGuarantType(String guarantType) {
        this.guarantType = guarantType;
    }

    public String getGuarantTypeName() {
        return guarantTypeName;
    }

    public void setGuarantTypeName(String guarantTypeName) {
        this.guarantTypeName = guarantTypeName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public BigDecimal getGuarantAmt() {
        return guarantAmt;
    }

    public void setGuarantAmt(BigDecimal guarantAmt) {
        this.guarantAmt = guarantAmt;
    }

    public String getGuarantAmtCurType() {
        return guarantAmtCurType;
    }

    public void setGuarantAmtCurType(String guarantAmtCurType) {
        this.guarantAmtCurType = guarantAmtCurType;
    }

    public BigDecimal getGuarantBal() {
        return guarantBal;
    }

    public void setGuarantBal(BigDecimal guarantBal) {
        this.guarantBal = guarantBal;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSettlAcctNo() {
        return settlAcctNo;
    }

    public void setSettlAcctNo(String settlAcctNo) {
        this.settlAcctNo = settlAcctNo;
    }

    public String getSettlAcctNoName() {
        return settlAcctNoName;
    }

    public void setSettlAcctNoName(String settlAcctNoName) {
        this.settlAcctNoName = settlAcctNoName;
    }

    public String getRiskSpacPerc() {
        return riskSpacPerc;
    }

    public void setRiskSpacPerc(String riskSpacPerc) {
        this.riskSpacPerc = riskSpacPerc;
    }

    public BigDecimal getRiskSpacAmt() {
        return riskSpacAmt;
    }

    public void setRiskSpacAmt(BigDecimal riskSpacAmt) {
        this.riskSpacAmt = riskSpacAmt;
    }

    public BigDecimal getChrgRate() {
        return chrgRate;
    }

    public void setChrgRate(BigDecimal chrgRate) {
        this.chrgRate = chrgRate;
    }

    public BigDecimal getChrgAmt() {
        return chrgAmt;
    }

    public void setChrgAmt(BigDecimal chrgAmt) {
        this.chrgAmt = chrgAmt;
    }

    public BigDecimal getOverduePadYearRate() {
        return overduePadYearRate;
    }

    public void setOverduePadYearRate(BigDecimal overduePadYearRate) {
        this.overduePadYearRate = overduePadYearRate;
    }

    public String getGuarantPayType() {
        return guarantPayType;
    }

    public void setGuarantPayType(String guarantPayType) {
        this.guarantPayType = guarantPayType;
    }

    public String getBeneficiar() {
        return beneficiar;
    }

    public void setBeneficiar(String beneficiar) {
        this.beneficiar = beneficiar;
    }

    public String getBeneficiarAcctb() {
        return beneficiarAcctb;
    }

    public void setBeneficiarAcctb(String beneficiarAcctb) {
        this.beneficiarAcctb = beneficiarAcctb;
    }

    public String getBeneficiarAcctNo() {
        return beneficiarAcctNo;
    }

    public void setBeneficiarAcctNo(String beneficiarAcctNo) {
        this.beneficiarAcctNo = beneficiarAcctNo;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public BigDecimal getCretQuotationExchgRate() {
        return cretQuotationExchgRate;
    }

    public void setCretQuotationExchgRate(BigDecimal cretQuotationExchgRate) {
        this.cretQuotationExchgRate = cretQuotationExchgRate;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(String bailAmt) {
        this.bailAmt = bailAmt;
    }

    public String getExchgRate() {
        return exchgRate;
    }

    public void setExchgRate(String exchgRate) {
        this.exchgRate = exchgRate;
    }

    @Override
    public String toString() {
        return "Xdtz0020DataReqDto{" +
                "bizType='" + bizType + '\'' +
                ", oprtype='" + oprtype + '\'' +
                ", billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", guarantType='" + guarantType + '\'' +
                ", guarantTypeName='" + guarantTypeName + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", guarantAmt=" + guarantAmt +
                ", guarantAmtCurType='" + guarantAmtCurType + '\'' +
                ", guarantBal=" + guarantBal +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", settlAcctNo='" + settlAcctNo + '\'' +
                ", settlAcctNoName='" + settlAcctNoName + '\'' +
                ", riskSpacPerc='" + riskSpacPerc + '\'' +
                ", riskSpacAmt=" + riskSpacAmt +
                ", chrgRate=" + chrgRate +
                ", chrgAmt=" + chrgAmt +
                ", overduePadYearRate=" + overduePadYearRate +
                ", guarantPayType='" + guarantPayType + '\'' +
                ", beneficiar='" + beneficiar + '\'' +
                ", beneficiarAcctb='" + beneficiarAcctb + '\'' +
                ", beneficiarAcctNo='" + beneficiarAcctNo + '\'' +
                ", accStatus='" + accStatus + '\'' +
                ", inputId='" + inputId + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", cretQuotationExchgRate=" + cretQuotationExchgRate +
                ", pvpSerno='" + pvpSerno + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", bailAmt='" + bailAmt + '\'' +
                ", exchgRate='" + exchgRate + '\'' +
                '}';
    }
}
