package cn.com.yusys.yusp.dto.server.xddb0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：信贷押品状态查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0006DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "reocnu")
    private String reocnu;//登记簿概要信息-不动产权证号
    @JsonProperty(value = "reoreu")
    private String reoreu;//登记簿概要信息-不动产单元号
    @JsonProperty(value = "reoona")
    private String reoona;//登记簿概要信息-权利人
    @JsonProperty(value = "reoodn")
    private String reoodn;//登记簿概要信息-证件号
    @JsonProperty(value = "reobda")
    private String reobda;//登记簿概要信息-登簿日期
    @JsonProperty(value = "reooss")
    private String reooss;//登记簿概要信息-权属状态
    @JsonProperty(value = "reorem")
    private String reorem;//登记簿概要信息-附记
	@JsonProperty(value = "registerFacilityLists")
	private java.util.List<RegisterFacilityList> registerFacilityLists;
	@JsonProperty(value = "registerHouseLists")
	private java.util.List<RegisterHouseList> registerHouseLists;
	@JsonProperty(value = "registerLandLists")
	private java.util.List<RegisterLandList> registerLandLists;
	@JsonProperty(value = "registerMortgageLists")
	private java.util.List<RegisterMortgageList> registerMortgageLists;
	@JsonProperty(value = "registerOwnerLists")
	private java.util.List<RegisterOwnerList> registerOwnerLists;
	@JsonProperty(value = "registerSeizureList")
	private java.util.List<RegisterSeizureList> registerSeizureList;
	@JsonProperty(value = "opFlag")
	private String opFlag;//操作成功标志位
	@JsonProperty(value = "opMsg")
	private String opMsg;//描述信息

	public String getReocnu() {
		return reocnu;
	}

	public void setReocnu(String reocnu) {
		this.reocnu = reocnu;
	}

	public String getReoreu() {
		return reoreu;
	}

	public void setReoreu(String reoreu) {
		this.reoreu = reoreu;
	}

	public String getReoona() {
		return reoona;
	}

	public void setReoona(String reoona) {
		this.reoona = reoona;
	}

	public String getReoodn() {
		return reoodn;
	}

	public void setReoodn(String reoodn) {
		this.reoodn = reoodn;
	}

	public String getReobda() {
		return reobda;
	}

	public void setReobda(String reobda) {
		this.reobda = reobda;
	}

	public String getReooss() {
		return reooss;
	}

	public void setReooss(String reooss) {
		this.reooss = reooss;
	}

	public String getReorem() {
		return reorem;
	}

	public void setReorem(String reorem) {
		this.reorem = reorem;
	}

	public List<RegisterFacilityList> getRegisterFacilityLists() {
		return registerFacilityLists;
	}

	public void setRegisterFacilityLists(List<RegisterFacilityList> registerFacilityLists) {
		this.registerFacilityLists = registerFacilityLists;
	}

	public List<RegisterHouseList> getRegisterHouseLists() {
		return registerHouseLists;
	}

	public void setRegisterHouseLists(List<RegisterHouseList> registerHouseLists) {
		this.registerHouseLists = registerHouseLists;
	}

	public List<RegisterLandList> getRegisterLandLists() {
		return registerLandLists;
	}

	public void setRegisterLandLists(List<RegisterLandList> registerLandLists) {
		this.registerLandLists = registerLandLists;
	}

	public List<RegisterMortgageList> getRegisterMortgageLists() {
		return registerMortgageLists;
	}

	public void setRegisterMortgageLists(List<RegisterMortgageList> registerMortgageLists) {
		this.registerMortgageLists = registerMortgageLists;
	}

	public List<RegisterOwnerList> getRegisterOwnerLists() {
		return registerOwnerLists;
	}

	public void setRegisterOwnerLists(List<RegisterOwnerList> registerOwnerLists) {
		this.registerOwnerLists = registerOwnerLists;
	}

	public List<RegisterSeizureList> getRegisterSeizureList() {
		return registerSeizureList;
	}

	public void setRegisterSeizureList(List<RegisterSeizureList> registerSeizureList) {
		this.registerSeizureList = registerSeizureList;
	}

	public String getOpFlag() {
		return opFlag;
	}

	public void setOpFlag(String opFlag) {
		this.opFlag = opFlag;
	}

	public String getOpMsg() {
		return opMsg;
	}

	public void setOpMsg(String opMsg) {
		this.opMsg = opMsg;
	}

	@Override
	public String toString() {
		return "Xddb0006DataRespDto{" +
				"reocnu='" + reocnu + '\'' +
				", reoreu='" + reoreu + '\'' +
				", reoona='" + reoona + '\'' +
				", reoodn='" + reoodn + '\'' +
				", reobda='" + reobda + '\'' +
				", reooss='" + reooss + '\'' +
				", reorem='" + reorem + '\'' +
				", registerFacilityLists=" + registerFacilityLists +
				", registerHouseLists=" + registerHouseLists +
				", registerLandLists=" + registerLandLists +
				", registerMortgageLists=" + registerMortgageLists +
				", registerOwnerLists=" + registerOwnerLists +
				", registerSeizureList=" + registerSeizureList +
				", opFlag='" + opFlag + '\'' +
				", opMsg='" + opMsg + '\'' +
				'}';
	}
}
