package cn.com.yusys.yusp.dto.server.xdtz0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：贴现记账结果通知
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0015DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "discAgrNo")
    private String discAgrNo;//贴现协议编号
    @JsonProperty(value = "drftHldSettlAcctNo")
    private String drftHldSettlAcctNo;//持票人结算账号
    @JsonProperty(value = "drftHldSettlAcctName")
    private String drftHldSettlAcctName;//持票人结算账户户名
    @JsonProperty(value = "discDate")
    private String discDate;//贴现日期
    @JsonProperty(value = "drftType")
    private String drftType;//票据类型
    @JsonProperty(value = "buyType")
    private String buyType;//买入方式
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "drftTotlAmt")
    private BigDecimal drftTotlAmt;//票面总金额
    @JsonProperty(value = "drftQnt")
    private String drftQnt;//票据数量
    @JsonProperty(value = "discTotlInt")
    private BigDecimal discTotlInt;//贴现总利息
    @JsonProperty(value = "pintMode")
    private String pintMode;//付息方式
    @JsonProperty(value = "orgNo")
    private String orgNo;//机构号
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "porderNo")
    private String porderNo;//汇票号码
    @JsonProperty(value = "drftSignDate")
    private String drftSignDate;//票据签发日
    @JsonProperty(value = "drftSignArea")
    private String drftSignArea;//票据签发地（本地、异地）
    @JsonProperty(value = "discDay")
    private String discDay;//贴现日
    @JsonProperty(value = "discEndDate")
    private String discEndDate;//票据到期日
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行年利率
    @JsonProperty(value = "rulingIr")
    private BigDecimal rulingIr;//基准年利率
    @JsonProperty(value = "drwrName")
    private String drwrName;//出票人名称
    @JsonProperty(value = "drwrAcctbAcctNo")
    private String drwrAcctbAcctNo;//出票人开户行账号
    @JsonProperty(value = "drwrAcctbName")
    private String drwrAcctbName;//出票人开户行行名
    @JsonProperty(value = "drwrAcctbNo")
    private String drwrAcctbNo;//出票人开户行行号
    @JsonProperty(value = "accptrName")
    private String accptrName;//承兑人名称
    @JsonProperty(value = "accptrAcctbAcctNo")
    private String accptrAcctbAcctNo;//承兑人开户行账号
    @JsonProperty(value = "pyeeName")
    private String pyeeName;//收款人名称
    @JsonProperty(value = "pyeeAcctbAcctNo")
    private String pyeeAcctbAcctNo;//收款人开户行账号
    @JsonProperty(value = "pyeeAcctbName")
    private String pyeeAcctbName;//收款人开户行行名
    @JsonProperty(value = "pyeeAcctbNo")
    private String pyeeAcctbNo;//收款人开户行行号
    @JsonProperty(value = "aorgType")
    private String aorgType;//承兑行类型
    @JsonProperty(value = "aorgTypeNo")
    private String aorgTypeNo;//承兑行行号
    @JsonProperty(value = "isSxet")
    private String isSxet;//是否省心E帖




    public String getDiscAgrNo() {
        return discAgrNo;
    }

    public void setDiscAgrNo(String discAgrNo) {
        this.discAgrNo = discAgrNo;
    }

    public String getDrftHldSettlAcctNo() {
        return drftHldSettlAcctNo;
    }

    public void setDrftHldSettlAcctNo(String drftHldSettlAcctNo) {
        this.drftHldSettlAcctNo = drftHldSettlAcctNo;
    }

    public String getDrftHldSettlAcctName() {
        return drftHldSettlAcctName;
    }

    public void setDrftHldSettlAcctName(String drftHldSettlAcctName) {
        this.drftHldSettlAcctName = drftHldSettlAcctName;
    }

    public String getDiscDate() {
        return discDate;
    }

    public void setDiscDate(String discDate) {
        this.discDate = discDate;
    }

    public String getDrftType() {
        return drftType;
    }

    public void setDrftType(String drftType) {
        this.drftType = drftType;
    }

    public String getBuyType() {
        return buyType;
    }

    public void setBuyType(String buyType) {
        this.buyType = buyType;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getDrftTotlAmt() {
        return drftTotlAmt;
    }

    public void setDrftTotlAmt(BigDecimal drftTotlAmt) {
        this.drftTotlAmt = drftTotlAmt;
    }

    public String getDrftQnt() {
        return drftQnt;
    }

    public void setDrftQnt(String drftQnt) {
        this.drftQnt = drftQnt;
    }

    public BigDecimal getDiscTotlInt() {
        return discTotlInt;
    }

    public void setDiscTotlInt(BigDecimal discTotlInt) {
        this.discTotlInt = discTotlInt;
    }

    public String getPintMode() {
        return pintMode;
    }

    public void setPintMode(String pintMode) {
        this.pintMode = pintMode;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

    public String getDrftSignDate() {
        return drftSignDate;
    }

    public void setDrftSignDate(String drftSignDate) {
        this.drftSignDate = drftSignDate;
    }

    public String getDrftSignArea() {
        return drftSignArea;
    }

    public void setDrftSignArea(String drftSignArea) {
        this.drftSignArea = drftSignArea;
    }

    public String getDiscDay() {
        return discDay;
    }

    public void setDiscDay(String discDay) {
        this.discDay = discDay;
    }

    public String getDiscEndDate() {
        return discEndDate;
    }

    public void setDiscEndDate(String discEndDate) {
        this.discEndDate = discEndDate;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public BigDecimal getRulingIr() {
        return rulingIr;
    }

    public void setRulingIr(BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    public String getDrwrName() {
        return drwrName;
    }

    public void setDrwrName(String drwrName) {
        this.drwrName = drwrName;
    }

    public String getDrwrAcctbAcctNo() {
        return drwrAcctbAcctNo;
    }

    public void setDrwrAcctbAcctNo(String drwrAcctbAcctNo) {
        this.drwrAcctbAcctNo = drwrAcctbAcctNo;
    }

    public String getDrwrAcctbName() {
        return drwrAcctbName;
    }

    public void setDrwrAcctbName(String drwrAcctbName) {
        this.drwrAcctbName = drwrAcctbName;
    }

    public String getDrwrAcctbNo() {
        return drwrAcctbNo;
    }

    public void setDrwrAcctbNo(String drwrAcctbNo) {
        this.drwrAcctbNo = drwrAcctbNo;
    }

    public String getAccptrName() {
        return accptrName;
    }

    public void setAccptrName(String accptrName) {
        this.accptrName = accptrName;
    }

    public String getAccptrAcctbAcctNo() {
        return accptrAcctbAcctNo;
    }

    public void setAccptrAcctbAcctNo(String accptrAcctbAcctNo) {
        this.accptrAcctbAcctNo = accptrAcctbAcctNo;
    }

    public String getPyeeName() {
        return pyeeName;
    }

    public void setPyeeName(String pyeeName) {
        this.pyeeName = pyeeName;
    }

    public String getPyeeAcctbAcctNo() {
        return pyeeAcctbAcctNo;
    }

    public void setPyeeAcctbAcctNo(String pyeeAcctbAcctNo) {
        this.pyeeAcctbAcctNo = pyeeAcctbAcctNo;
    }

    public String getPyeeAcctbName() {
        return pyeeAcctbName;
    }

    public void setPyeeAcctbName(String pyeeAcctbName) {
        this.pyeeAcctbName = pyeeAcctbName;
    }

    public String getPyeeAcctbNo() {
        return pyeeAcctbNo;
    }

    public void setPyeeAcctbNo(String pyeeAcctbNo) {
        this.pyeeAcctbNo = pyeeAcctbNo;
    }

    public String getAorgType() {
        return aorgType;
    }

    public void setAorgType(String aorgType) {
        this.aorgType = aorgType;
    }

    public String getAorgTypeNo() {
        return aorgTypeNo;
    }

    public void setAorgTypeNo(String aorgTypeNo) {
        this.aorgTypeNo = aorgTypeNo;
    }

    public String getIsSxet() {
        return isSxet;
    }

    public void setIsSxet(String isSxet) {
        this.isSxet = isSxet;
    }

    @Override
    public String toString() {
        return "Xdtz0015DataReqDto{" +
                "discAgrNo='" + discAgrNo + '\'' +
                ", drftHldSettlAcctNo='" + drftHldSettlAcctNo + '\'' +
                ", drftHldSettlAcctName='" + drftHldSettlAcctName + '\'' +
                ", discDate='" + discDate + '\'' +
                ", drftType='" + drftType + '\'' +
                ", buyType='" + buyType + '\'' +
                ", curType='" + curType + '\'' +
                ", drftTotlAmt=" + drftTotlAmt +
                ", drftQnt='" + drftQnt + '\'' +
                ", discTotlInt=" + discTotlInt +
                ", pintMode='" + pintMode + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", managerId='" + managerId + '\'' +
                ", porderNo='" + porderNo + '\'' +
                ", drftSignDate='" + drftSignDate + '\'' +
                ", drftSignArea='" + drftSignArea + '\'' +
                ", discDay='" + discDay + '\'' +
                ", discEndDate='" + discEndDate + '\'' +
                ", realityIrY=" + realityIrY +
                ", rulingIr=" + rulingIr +
                ", drwrName='" + drwrName + '\'' +
                ", drwrAcctbAcctNo='" + drwrAcctbAcctNo + '\'' +
                ", drwrAcctbName='" + drwrAcctbName + '\'' +
                ", drwrAcctbNo='" + drwrAcctbNo + '\'' +
                ", accptrName='" + accptrName + '\'' +
                ", accptrAcctbAcctNo='" + accptrAcctbAcctNo + '\'' +
                ", pyeeName='" + pyeeName + '\'' +
                ", pyeeAcctbAcctNo='" + pyeeAcctbAcctNo + '\'' +
                ", pyeeAcctbName='" + pyeeAcctbName + '\'' +
                ", pyeeAcctbNo='" + pyeeAcctbNo + '\'' +
                ", aorgType='" + aorgType + '\'' +
                ", aorgTypeNo='" + aorgTypeNo + '\'' +
                ", isSxet='" + isSxet + '\'' +
                '}';
    }
}
