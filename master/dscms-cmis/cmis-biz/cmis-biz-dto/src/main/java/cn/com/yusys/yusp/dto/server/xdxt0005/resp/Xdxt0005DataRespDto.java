package cn.com.yusys.yusp.dto.server.xdxt0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询客户经理所在分部编号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0005DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "branchNo")
    private String branchNo;//分部编号

    public String getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(String branchNo) {
        this.branchNo = branchNo;
    }

    @Override
    public String toString() {
        return "Xdxt0005DataRespDto{" +
                "branchNo='" + branchNo + '\'' +
                '}';
    }
}
