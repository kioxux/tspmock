package cn.com.yusys.yusp.dto.server.xdht0014.req;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
/**
 * 请求Data：合同签订查询VX
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0014DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//调查流水号

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    @Override
    public String toString() {
        return "Xdht0014DataReqDto{" +
            "indgtSerno='" + indgtSerno + '\'' +
                    '}';
        }
    }

