package cn.com.yusys.yusp.dto.server.xdcz0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：网银推送相关受托支付信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0012DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "flag")
    private String flag;//标志
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

}

