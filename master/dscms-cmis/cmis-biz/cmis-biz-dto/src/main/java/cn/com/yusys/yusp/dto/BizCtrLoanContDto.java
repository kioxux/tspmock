package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 对应cmis-biz-core中的CtrLoanContDto类
 */
public class BizCtrLoanContDto implements Serializable{
    private static final long serialVersionUID = 1L;

    /** 合同编号 **/
    private String contNo;

    /** 中文合同编号 **/
    private String cnContNo;

    /** 全局流水号 **/
    private String serno;

    /** 业务申请流水号 **/
    private String iqpSerno;

    /** 客户编号 **/
    private String cusId;

    /** 产品编号 **/
    private String prdId;

    /** 业务类型 STD_ZB_BIZ_TYP **/
    private String bizType;

    /** 特殊业务类型 STD_ZB_SPEBS_TYP **/
    private String especBizType;

    /** 贷款用途 STD_ZB_LOAN_USE **/
    private String loanUse;

    /** 贷款形式 STD_ZB_LOAN_MODAL **/
    private String loanModal;

    /** 贷款性质 STD_ZB_LOAN_CHA **/
    private String loanCha;

    /** 是否曾被拒绝 **/
    private String isHasRefused;

    /** 主担保方式 STD_ZB_GUAR_WAY **/
    private String guarWay;

    /** 是否共同申请人 STD_ZB_YES_NO **/
    private String isCommonRqstr;

    /** 是否确认支付方式 STD_ZB_YES_NO **/
    private String isCfirmPayWay;

    /** 支付方式 STD_ZB_RAY_MODE **/
    private String defrayMode;

    /** 币种 STD_ZB_CUR_TYP **/
    private String curType;

    /** 合同金额 **/
    private BigDecimal contAmt;

    /** 合同余额 **/
    private BigDecimal contBalance;

    /** 合同汇率 **/
    private BigDecimal contRate;

    /** 保证金来源 STD_ZB_BAIL_SOUR **/
    private String bailSour;

    /** 保证金汇率 **/
    private BigDecimal exchangeRate;

    /** 保证金比例 **/
    private BigDecimal bailRate;

    /** 保证金币种 **/
    private String bailCurType;

    /** 保证金金额 **/
    private BigDecimal securityAmt;

    /** 保证金折算人民币金额 **/
    private BigDecimal bailRmbAmt;

    /** 合同起始日期 **/
    private String contStartDate;

    /** 合同到期日期 **/
    private String contEndDate;

    /** 期限类型 STD_ZB_TERM_TYP **/
    private String termType;

    /** 申请期限 **/
    private BigDecimal appTerm;

    /** 利率依据方式 STD_ZB_IR_WAY **/
    private String irAccordType;

    /** 利率种类 STD_ZB_IR_TYP **/
    private String irType;

    /** 基准利率（年） **/
    private BigDecimal rulingIr;

    /** 对应基准利率(月) **/
    private BigDecimal rulingIrM;

    /** 计息方式 STD_ZB_LOAN_RAT_TYPE **/
    private String loanRatType;

    /** 利率调整方式 STD_ZB_RADJ_TYP **/
    private String irAdjustType;

    /** 利率调整周期(月) **/
    private BigDecimal irAdjustTerm;

    /** 调息方式 STD_ZB_PRA_MODE **/
    private String praType;

    /** 利率形式 STD_ZB_RATE_TYPE **/
    private String rateType;

    /** 正常利率浮动方式 STD_ZB_RFLOAT_TYP **/
    private String irFloatType;

    /** 利率浮动百分比 **/
    private BigDecimal irFloatRate;

    /** 执行利率（年） **/
    private BigDecimal realityIrY;

    /** 执行利率(月) **/
    private BigDecimal realityIrM;

    /** 逾期利率浮动方式 STD_ZB_RFLOAT_TYP **/
    private String overdueFloatType;

    /** 逾期利率浮动加点值 **/
    private BigDecimal overduePoint;

    /** 逾期利率浮动百分比 **/
    private BigDecimal overdueRate;

    /** 逾期利率（年） **/
    private BigDecimal overdueRateY;

    /** 违约利率浮动方式 STD_ZB_RFLOAT_TYP **/
    private String defaultFloatType;

    /** 违约利率浮动加点值 **/
    private BigDecimal defaultPoint;

    /** 违约利率浮动百分比 **/
    private BigDecimal defaultRate;

    /** 违约利率（年） **/
    private BigDecimal defaultRateY;

    /** 风险敞口金额 **/
    private BigDecimal riskOpenAmt;

    /** 还款方式 STD_ZB_REPAY_TYP **/
    private String repayType;

    /** 停本付息期间 STD_ZB_PINT_TERM **/
    private String stopPintTerm;

    /** 还款间隔周期 STD_ZB_REPAY_TERM **/
    private String repayTerm;

    /** 还款间隔 STD_ZB_REPAY_SPACE **/
    private String repaySpace;

    /** 还款日确定规则 STD_ZB_REPAY_RULE **/
    private String repayRule;

    /** 还款日类型 STD_ZB_REPAY_DT_TYPE **/
    private String repayDtType;

    /** 还款日 **/
    private BigDecimal repayDate;

    /** 本金宽限方式 STD_ZB_GRAPER_TYPE **/
    private String capGraperType;

    /** 本金宽限天数 **/
    private BigDecimal capGraperDay;

    /** 利息宽限方式 STD_ZB_GRAPER_TYPE **/
    private String intGraperType;

    /** 利息宽限天数 **/
    private BigDecimal intGraperDay;

    /** 扣款扣息方式 STD_ZB_DEDUCT_TYPE **/
    private String deductDeduType;

    /** 还款频率类型 STD_ZB_REPAY_FRE_TYPE **/
    private String repayFreType;

    /** 本息还款频率 **/
    private BigDecimal repayFre;

    /** 提前还款违约金免除时间(月) **/
    private Integer liquFreeTime;

    /** 分段方式 STD_ZB_SUB_TYPE **/
    private String subType;

    /** 保留期限 **/
    private Integer reserveTerm;

    /** 计算期限 **/
    private Integer calTerm;

    /** 保留金额 **/
    private BigDecimal reserveAmt;

    /** 第一阶段还款期数 **/
    private Integer repayTermOne;

    /** 第一阶段还款本金 **/
    private BigDecimal repayAmtOne;

    /** 第二阶段还款期数 **/
    private Integer repayTermTwo;

    /** 第二阶段还款本金 **/
    private BigDecimal repayAmtTwo;

    /** 利率选取日期种类 STD_ZB_RATE_SEL_TYPE **/
    private String rateSelType;

    /** 贴息方式 STD_ZB_SBSY_WAY **/
    private String sbsyMode;

    /** 贴息比例 **/
    private BigDecimal sbsyPerc;

    /** 贴息金额 **/
    private BigDecimal sbsyAmt;

    /** 贴息单位名称 **/
    private String sbsyUnitName;

    /** 贴息方账户 **/
    private String sbsyAcct;

    /** 贴息方账户户名 **/
    private String sbsyAcctName;

    /** 五级分类 STD_ZB_FIVE_SORT **/
    private String fiveClass;

    /** 贷款投向 STD_GB_4754-2011 **/
    private String loanDirection;

    /** 工业转型升级标识 STD_ZB_YES_NO **/
    private String comUpIndtify;

    /** 战略新兴产业类型 STD_ZB_SEIS_TYP **/
    private String strategyNewLoan;

    /** 是否文化产业 STD_ZB_YES_NO **/
    private String isCulEstate;

    /** 贷款种类 STD_PER_POSITIONTYPE **/
    private String loanType;

    /** 产业结构调整类型 STD_ZB_EST_ADJ_TYP **/
    private String estateAdjustType;

    /** 新兴产业贷款 STD_ZB_NEWPRD_LOAN **/
    private String newPrdLoan;

    /** 还款来源 **/
    private String repaySrcDes;

    /** 是否委托人办理 STD_ZB_YES_NO **/
    private String isAuthorize;

    /** 委托人姓名 **/
    private String authedName;

    /** 委托人证件类型 STD_ZB_CERT_TYP **/
    private String authedCertType;

    /** 委托人证件号 **/
    private String authedCertCode;

    /** 委托人联系方式 **/
    private String authedTelNo;

    /** 签订日期 **/
    private String signDate;

    /** 注销日期 **/
    private String logoutDate;

    /** 渠道来源 STD_ZB_CHNL_SOUR **/
    private String chnlSour;

    /** 争议借据方式选项 STD_ZB_DISPUPE_OPT **/
    private String billDispupeOpt;

    /** 仲裁机构 **/
    private String arbitrateBch;

    /** 合同份数 **/
    private BigDecimal contQnt;

    /** 主办机构 **/
    private String managerBrId;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 最后修改日期 **/
    private String inputDate;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 合同状态 STD_ZB_CONT_TYP **/
    private String contStatus;

    /** 法院所在地 **/
    private String courtAddr;

    /** 仲裁委员会地点 **/
    private String arbitrateAddr;

    /** 其他方式 **/
    private String otherOpt;

    /** 公积金贷款合同编号 **/
    private String pundContNo;

    /** 补充条款 **/
    private String spplClause;

    /** 签约地点 **/
    private String signAddr;

    /** 营业网点 **/
    private String busiNetwork;

    /** 主要营业场所地址 **/
    private String mainBusiPalce;

    /** 合同模板 **/
    private String contTemplate;

    /** 固定加点值 **/
    private BigDecimal irFloatPoint;

    /**合同打印份数**/
    private BigDecimal contPrintNum;


    /**
     * @param contNo
     */
    public void setContNo(String contNo) {
        this.contNo = contNo == null ? null : contNo.trim();
    }

    /**
     * @return ContNo
     */
    public String getContNo() {
        return this.contNo;
    }

    /**
     * @param cnContNo
     */
    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo == null ? null : cnContNo.trim();
    }

    /**
     * @return CnContNo
     */
    public String getCnContNo() {
        return this.cnContNo;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno == null ? null : serno.trim();
    }

    /**
     * @return Serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param iqpSerno
     */
    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
    }

    /**
     * @return IqpSerno
     */
    public String getIqpSerno() {
        return this.iqpSerno;
    }

    /**
     * @param cusId
     */
    public void setCusId(String cusId) {
        this.cusId = cusId == null ? null : cusId.trim();
    }

    /**
     * @return CusId
     */
    public String getCusId() {
        return this.cusId;
    }

    /**
     * @param prdId
     */
    public void setPrdId(String prdId) {
        this.prdId = prdId == null ? null : prdId.trim();
    }

    /**
     * @return PrdId
     */
    public String getPrdId() {
        return this.prdId;
    }

    /**
     * @param bizType
     */
    public void setBizType(String bizType) {
        this.bizType = bizType == null ? null : bizType.trim();
    }

    /**
     * @return BizType
     */
    public String getBizType() {
        return this.bizType;
    }

    /**
     * @param especBizType
     */
    public void setEspecBizType(String especBizType) {
        this.especBizType = especBizType == null ? null : especBizType.trim();
    }

    /**
     * @return EspecBizType
     */
    public String getEspecBizType() {
        return this.especBizType;
    }

    /**
     * @param loanUse
     */
    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse == null ? null : loanUse.trim();
    }

    /**
     * @return LoanUse
     */
    public String getLoanUse() {
        return this.loanUse;
    }

    /**
     * @param loanModal
     */
    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal == null ? null : loanModal.trim();
    }

    /**
     * @return LoanModal
     */
    public String getLoanModal() {
        return this.loanModal;
    }

    /**
     * @param loanCha
     */
    public void setLoanCha(String loanCha) {
        this.loanCha = loanCha == null ? null : loanCha.trim();
    }

    /**
     * @return LoanCha
     */
    public String getLoanCha() {
        return this.loanCha;
    }

    /**
     * @param isHasRefused
     */
    public void setIsHasRefused(String isHasRefused) {
        this.isHasRefused = isHasRefused == null ? null : isHasRefused.trim();
    }

    /**
     * @return IsHasRefused
     */
    public String getIsHasRefused() {
        return this.isHasRefused;
    }

    /**
     * @param guarWay
     */
    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay == null ? null : guarWay.trim();
    }

    /**
     * @return GuarWay
     */
    public String getGuarWay() {
        return this.guarWay;
    }

    /**
     * @param isCommonRqstr
     */
    public void setIsCommonRqstr(String isCommonRqstr) {
        this.isCommonRqstr = isCommonRqstr == null ? null : isCommonRqstr.trim();
    }

    /**
     * @return IsCommonRqstr
     */
    public String getIsCommonRqstr() {
        return this.isCommonRqstr;
    }

    /**
     * @param isCfirmPayWay
     */
    public void setIsCfirmPayWay(String isCfirmPayWay) {
        this.isCfirmPayWay = isCfirmPayWay == null ? null : isCfirmPayWay.trim();
    }

    /**
     * @return IsCfirmPayWay
     */
    public String getIsCfirmPayWay() {
        return this.isCfirmPayWay;
    }

    /**
     * @param defrayMode
     */
    public void setDefrayMode(String defrayMode) {
        this.defrayMode = defrayMode == null ? null : defrayMode.trim();
    }

    /**
     * @return DefrayMode
     */
    public String getDefrayMode() {
        return this.defrayMode;
    }

    /**
     * @param curType
     */
    public void setCurType(String curType) {
        this.curType = curType == null ? null : curType.trim();
    }

    /**
     * @return CurType
     */
    public String getCurType() {
        return this.curType;
    }

    /**
     * @param contAmt
     */
    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    /**
     * @return ContAmt
     */
    public BigDecimal getContAmt() {
        return this.contAmt;
    }

    /**
     * @param contBalance
     */
    public void setContBalance(BigDecimal contBalance) {
        this.contBalance = contBalance;
    }

    /**
     * @return ContBalance
     */
    public BigDecimal getContBalance() {
        return this.contBalance;
    }

    /**
     * @param contRate
     */
    public void setContRate(BigDecimal contRate) {
        this.contRate = contRate;
    }

    /**
     * @return ContRate
     */
    public BigDecimal getContRate() {
        return this.contRate;
    }

    /**
     * @param bailSour
     */
    public void setBailSour(String bailSour) {
        this.bailSour = bailSour == null ? null : bailSour.trim();
    }

    /**
     * @return BailSour
     */
    public String getBailSour() {
        return this.bailSour;
    }

    /**
     * @param exchangeRate
     */
    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    /**
     * @return ExchangeRate
     */
    public BigDecimal getExchangeRate() {
        return this.exchangeRate;
    }

    /**
     * @param bailRate
     */
    public void setBailRate(BigDecimal bailRate) {
        this.bailRate = bailRate;
    }

    /**
     * @return BailRate
     */
    public BigDecimal getBailRate() {
        return this.bailRate;
    }

    /**
     * @param bailCurType
     */
    public void setBailCurType(String bailCurType) {
        this.bailCurType = bailCurType == null ? null : bailCurType.trim();
    }

    /**
     * @return BailCurType
     */
    public String getBailCurType() {
        return this.bailCurType;
    }

    /**
     * @param securityAmt
     */
    public void setSecurityAmt(BigDecimal securityAmt) {
        this.securityAmt = securityAmt;
    }

    /**
     * @return SecurityAmt
     */
    public BigDecimal getSecurityAmt() {
        return this.securityAmt;
    }

    /**
     * @param bailRmbAmt
     */
    public void setBailRmbAmt(BigDecimal bailRmbAmt) {
        this.bailRmbAmt = bailRmbAmt;
    }

    /**
     * @return BailRmbAmt
     */
    public BigDecimal getBailRmbAmt() {
        return this.bailRmbAmt;
    }

    /**
     * @param contStartDate
     */
    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate == null ? null : contStartDate.trim();
    }

    /**
     * @return ContStartDate
     */
    public String getContStartDate() {
        return this.contStartDate;
    }

    /**
     * @param contEndDate
     */
    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate == null ? null : contEndDate.trim();
    }

    /**
     * @return ContEndDate
     */
    public String getContEndDate() {
        return this.contEndDate;
    }

    /**
     * @param termType
     */
    public void setTermType(String termType) {
        this.termType = termType == null ? null : termType.trim();
    }

    /**
     * @return TermType
     */
    public String getTermType() {
        return this.termType;
    }

    /**
     * @param appTerm
     */
    public void setAppTerm(BigDecimal appTerm) {
        this.appTerm = appTerm;
    }

    /**
     * @return AppTerm
     */
    public BigDecimal getAppTerm() {
        return this.appTerm;
    }

    /**
     * @param irAccordType
     */
    public void setIrAccordType(String irAccordType) {
        this.irAccordType = irAccordType == null ? null : irAccordType.trim();
    }

    /**
     * @return IrAccordType
     */
    public String getIrAccordType() {
        return this.irAccordType;
    }

    /**
     * @param irType
     */
    public void setIrType(String irType) {
        this.irType = irType == null ? null : irType.trim();
    }

    /**
     * @return IrType
     */
    public String getIrType() {
        return this.irType;
    }

    /**
     * @param rulingIr
     */
    public void setRulingIr(BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    /**
     * @return RulingIr
     */
    public BigDecimal getRulingIr() {
        return this.rulingIr;
    }

    /**
     * @param rulingIrM
     */
    public void setRulingIrM(BigDecimal rulingIrM) {
        this.rulingIrM = rulingIrM;
    }

    /**
     * @return RulingIrM
     */
    public BigDecimal getRulingIrM() {
        return this.rulingIrM;
    }

    /**
     * @param loanRatType
     */
    public void setLoanRatType(String loanRatType) {
        this.loanRatType = loanRatType == null ? null : loanRatType.trim();
    }

    /**
     * @return LoanRatType
     */
    public String getLoanRatType() {
        return this.loanRatType;
    }

    /**
     * @param irAdjustType
     */
    public void setIrAdjustType(String irAdjustType) {
        this.irAdjustType = irAdjustType == null ? null : irAdjustType.trim();
    }

    /**
     * @return IrAdjustType
     */
    public String getIrAdjustType() {
        return this.irAdjustType;
    }

    /**
     * @param irAdjustTerm
     */
    public void setIrAdjustTerm(BigDecimal irAdjustTerm) {
        this.irAdjustTerm = irAdjustTerm;
    }

    /**
     * @return IrAdjustTerm
     */
    public BigDecimal getIrAdjustTerm() {
        return this.irAdjustTerm;
    }

    /**
     * @param praType
     */
    public void setPraType(String praType) {
        this.praType = praType == null ? null : praType.trim();
    }

    /**
     * @return PraType
     */
    public String getPraType() {
        return this.praType;
    }

    /**
     * @param rateType
     */
    public void setRateType(String rateType) {
        this.rateType = rateType == null ? null : rateType.trim();
    }

    /**
     * @return RateType
     */
    public String getRateType() {
        return this.rateType;
    }

    /**
     * @param irFloatType
     */
    public void setIrFloatType(String irFloatType) {
        this.irFloatType = irFloatType == null ? null : irFloatType.trim();
    }

    /**
     * @return IrFloatType
     */
    public String getIrFloatType() {
        return this.irFloatType;
    }

    /**
     * @param irFloatRate
     */
    public void setIrFloatRate(BigDecimal irFloatRate) {
        this.irFloatRate = irFloatRate;
    }

    /**
     * @return IrFloatRate
     */
    public BigDecimal getIrFloatRate() {
        return this.irFloatRate;
    }

    /**
     * @param realityIrY
     */
    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    /**
     * @return RealityIrY
     */
    public BigDecimal getRealityIrY() {
        return this.realityIrY;
    }

    /**
     * @param realityIrM
     */
    public void setRealityIrM(BigDecimal realityIrM) {
        this.realityIrM = realityIrM;
    }

    /**
     * @return RealityIrM
     */
    public BigDecimal getRealityIrM() {
        return this.realityIrM;
    }

    /**
     * @param overdueFloatType
     */
    public void setOverdueFloatType(String overdueFloatType) {
        this.overdueFloatType = overdueFloatType == null ? null : overdueFloatType.trim();
    }

    /**
     * @return OverdueFloatType
     */
    public String getOverdueFloatType() {
        return this.overdueFloatType;
    }

    /**
     * @param overduePoint
     */
    public void setOverduePoint(BigDecimal overduePoint) {
        this.overduePoint = overduePoint;
    }

    /**
     * @return OverduePoint
     */
    public BigDecimal getOverduePoint() {
        return this.overduePoint;
    }

    /**
     * @param overdueRate
     */
    public void setOverdueRate(BigDecimal overdueRate) {
        this.overdueRate = overdueRate;
    }

    /**
     * @return OverdueRate
     */
    public BigDecimal getOverdueRate() {
        return this.overdueRate;
    }

    /**
     * @param overdueRateY
     */
    public void setOverdueRateY(BigDecimal overdueRateY) {
        this.overdueRateY = overdueRateY;
    }

    /**
     * @return OverdueRateY
     */
    public BigDecimal getOverdueRateY() {
        return this.overdueRateY;
    }

    /**
     * @param defaultFloatType
     */
    public void setDefaultFloatType(String defaultFloatType) {
        this.defaultFloatType = defaultFloatType == null ? null : defaultFloatType.trim();
    }

    /**
     * @return DefaultFloatType
     */
    public String getDefaultFloatType() {
        return this.defaultFloatType;
    }

    /**
     * @param defaultPoint
     */
    public void setDefaultPoint(BigDecimal defaultPoint) {
        this.defaultPoint = defaultPoint;
    }

    /**
     * @return DefaultPoint
     */
    public BigDecimal getDefaultPoint() {
        return this.defaultPoint;
    }

    /**
     * @param defaultRate
     */
    public void setDefaultRate(BigDecimal defaultRate) {
        this.defaultRate = defaultRate;
    }

    /**
     * @return DefaultRate
     */
    public BigDecimal getDefaultRate() {
        return this.defaultRate;
    }

    /**
     * @param defaultRateY
     */
    public void setDefaultRateY(BigDecimal defaultRateY) {
        this.defaultRateY = defaultRateY;
    }

    /**
     * @return DefaultRateY
     */
    public BigDecimal getDefaultRateY() {
        return this.defaultRateY;
    }

    /**
     * @param riskOpenAmt
     */
    public void setRiskOpenAmt(BigDecimal riskOpenAmt) {
        this.riskOpenAmt = riskOpenAmt;
    }

    /**
     * @return RiskOpenAmt
     */
    public BigDecimal getRiskOpenAmt() {
        return this.riskOpenAmt;
    }

    /**
     * @param repayType
     */
    public void setRepayType(String repayType) {
        this.repayType = repayType == null ? null : repayType.trim();
    }

    /**
     * @return RepayType
     */
    public String getRepayType() {
        return this.repayType;
    }

    /**
     * @param stopPintTerm
     */
    public void setStopPintTerm(String stopPintTerm) {
        this.stopPintTerm = stopPintTerm == null ? null : stopPintTerm.trim();
    }

    /**
     * @return StopPintTerm
     */
    public String getStopPintTerm() {
        return this.stopPintTerm;
    }

    /**
     * @param repayTerm
     */
    public void setRepayTerm(String repayTerm) {
        this.repayTerm = repayTerm == null ? null : repayTerm.trim();
    }

    /**
     * @return RepayTerm
     */
    public String getRepayTerm() {
        return this.repayTerm;
    }

    /**
     * @param repaySpace
     */
    public void setRepaySpace(String repaySpace) {
        this.repaySpace = repaySpace == null ? null : repaySpace.trim();
    }

    /**
     * @return RepaySpace
     */
    public String getRepaySpace() {
        return this.repaySpace;
    }

    /**
     * @param repayRule
     */
    public void setRepayRule(String repayRule) {
        this.repayRule = repayRule == null ? null : repayRule.trim();
    }

    /**
     * @return RepayRule
     */
    public String getRepayRule() {
        return this.repayRule;
    }

    /**
     * @param repayDtType
     */
    public void setRepayDtType(String repayDtType) {
        this.repayDtType = repayDtType == null ? null : repayDtType.trim();
    }

    /**
     * @return RepayDtType
     */
    public String getRepayDtType() {
        return this.repayDtType;
    }

    /**
     * @param repayDate
     */
    public void setRepayDate(BigDecimal repayDate) {
        this.repayDate = repayDate;
    }

    /**
     * @return RepayDate
     */
    public BigDecimal getRepayDate() {
        return this.repayDate;
    }

    /**
     * @param capGraperType
     */
    public void setCapGraperType(String capGraperType) {
        this.capGraperType = capGraperType == null ? null : capGraperType.trim();
    }

    /**
     * @return CapGraperType
     */
    public String getCapGraperType() {
        return this.capGraperType;
    }

    /**
     * @param capGraperDay
     */
    public void setCapGraperDay(BigDecimal capGraperDay) {
        this.capGraperDay = capGraperDay;
    }

    /**
     * @return CapGraperDay
     */
    public BigDecimal getCapGraperDay() {
        return this.capGraperDay;
    }

    /**
     * @param intGraperType
     */
    public void setIntGraperType(String intGraperType) {
        this.intGraperType = intGraperType == null ? null : intGraperType.trim();
    }

    /**
     * @return IntGraperType
     */
    public String getIntGraperType() {
        return this.intGraperType;
    }

    /**
     * @param intGraperDay
     */
    public void setIntGraperDay(BigDecimal intGraperDay) {
        this.intGraperDay = intGraperDay;
    }

    /**
     * @return IntGraperDay
     */
    public BigDecimal getIntGraperDay() {
        return this.intGraperDay;
    }

    /**
     * @param deductDeduType
     */
    public void setDeductDeduType(String deductDeduType) {
        this.deductDeduType = deductDeduType == null ? null : deductDeduType.trim();
    }

    /**
     * @return DeductDeduType
     */
    public String getDeductDeduType() {
        return this.deductDeduType;
    }

    /**
     * @param repayFreType
     */
    public void setRepayFreType(String repayFreType) {
        this.repayFreType = repayFreType == null ? null : repayFreType.trim();
    }

    /**
     * @return RepayFreType
     */
    public String getRepayFreType() {
        return this.repayFreType;
    }

    /**
     * @param repayFre
     */
    public void setRepayFre(BigDecimal repayFre) {
        this.repayFre = repayFre;
    }

    /**
     * @return RepayFre
     */
    public BigDecimal getRepayFre() {
        return this.repayFre;
    }

    /**
     * @param liquFreeTime
     */
    public void setLiquFreeTime(Integer liquFreeTime) {
        this.liquFreeTime = liquFreeTime;
    }

    /**
     * @return LiquFreeTime
     */
    public Integer getLiquFreeTime() {
        return this.liquFreeTime;
    }

    /**
     * @param subType
     */
    public void setSubType(String subType) {
        this.subType = subType == null ? null : subType.trim();
    }

    /**
     * @return SubType
     */
    public String getSubType() {
        return this.subType;
    }

    /**
     * @param reserveTerm
     */
    public void setReserveTerm(Integer reserveTerm) {
        this.reserveTerm = reserveTerm;
    }

    /**
     * @return ReserveTerm
     */
    public Integer getReserveTerm() {
        return this.reserveTerm;
    }

    /**
     * @param calTerm
     */
    public void setCalTerm(Integer calTerm) {
        this.calTerm = calTerm;
    }

    /**
     * @return CalTerm
     */
    public Integer getCalTerm() {
        return this.calTerm;
    }

    /**
     * @param reserveAmt
     */
    public void setReserveAmt(BigDecimal reserveAmt) {
        this.reserveAmt = reserveAmt;
    }

    /**
     * @return ReserveAmt
     */
    public BigDecimal getReserveAmt() {
        return this.reserveAmt;
    }

    /**
     * @param repayTermOne
     */
    public void setRepayTermOne(Integer repayTermOne) {
        this.repayTermOne = repayTermOne;
    }

    /**
     * @return RepayTermOne
     */
    public Integer getRepayTermOne() {
        return this.repayTermOne;
    }

    /**
     * @param repayAmtOne
     */
    public void setRepayAmtOne(BigDecimal repayAmtOne) {
        this.repayAmtOne = repayAmtOne;
    }

    /**
     * @return RepayAmtOne
     */
    public BigDecimal getRepayAmtOne() {
        return this.repayAmtOne;
    }

    /**
     * @param repayTermTwo
     */
    public void setRepayTermTwo(Integer repayTermTwo) {
        this.repayTermTwo = repayTermTwo;
    }

    /**
     * @return RepayTermTwo
     */
    public Integer getRepayTermTwo() {
        return this.repayTermTwo;
    }

    /**
     * @param repayAmtTwo
     */
    public void setRepayAmtTwo(BigDecimal repayAmtTwo) {
        this.repayAmtTwo = repayAmtTwo;
    }

    /**
     * @return RepayAmtTwo
     */
    public BigDecimal getRepayAmtTwo() {
        return this.repayAmtTwo;
    }

    /**
     * @param rateSelType
     */
    public void setRateSelType(String rateSelType) {
        this.rateSelType = rateSelType == null ? null : rateSelType.trim();
    }

    /**
     * @return RateSelType
     */
    public String getRateSelType() {
        return this.rateSelType;
    }

    /**
     * @param sbsyMode
     */
    public void setSbsyMode(String sbsyMode) {
        this.sbsyMode = sbsyMode == null ? null : sbsyMode.trim();
    }

    /**
     * @return SbsyMode
     */
    public String getSbsyMode() {
        return this.sbsyMode;
    }

    /**
     * @param sbsyPerc
     */
    public void setSbsyPerc(BigDecimal sbsyPerc) {
        this.sbsyPerc = sbsyPerc;
    }

    /**
     * @return SbsyPerc
     */
    public BigDecimal getSbsyPerc() {
        return this.sbsyPerc;
    }

    /**
     * @param sbsyAmt
     */
    public void setSbsyAmt(BigDecimal sbsyAmt) {
        this.sbsyAmt = sbsyAmt;
    }

    /**
     * @return SbsyAmt
     */
    public BigDecimal getSbsyAmt() {
        return this.sbsyAmt;
    }

    /**
     * @param sbsyUnitName
     */
    public void setSbsyUnitName(String sbsyUnitName) {
        this.sbsyUnitName = sbsyUnitName == null ? null : sbsyUnitName.trim();
    }

    /**
     * @return SbsyUnitName
     */
    public String getSbsyUnitName() {
        return this.sbsyUnitName;
    }

    /**
     * @param sbsyAcct
     */
    public void setSbsyAcct(String sbsyAcct) {
        this.sbsyAcct = sbsyAcct == null ? null : sbsyAcct.trim();
    }

    /**
     * @return SbsyAcct
     */
    public String getSbsyAcct() {
        return this.sbsyAcct;
    }

    /**
     * @param sbsyAcctName
     */
    public void setSbsyAcctName(String sbsyAcctName) {
        this.sbsyAcctName = sbsyAcctName == null ? null : sbsyAcctName.trim();
    }

    /**
     * @return SbsyAcctName
     */
    public String getSbsyAcctName() {
        return this.sbsyAcctName;
    }

    /**
     * @param fiveClass
     */
    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass == null ? null : fiveClass.trim();
    }

    /**
     * @return FiveClass
     */
    public String getFiveClass() {
        return this.fiveClass;
    }

    /**
     * @param loanDirection
     */
    public void setLoanDirection(String loanDirection) {
        this.loanDirection = loanDirection == null ? null : loanDirection.trim();
    }

    /**
     * @return LoanDirection
     */
    public String getLoanDirection() {
        return this.loanDirection;
    }

    /**
     * @param comUpIndtify
     */
    public void setComUpIndtify(String comUpIndtify) {
        this.comUpIndtify = comUpIndtify == null ? null : comUpIndtify.trim();
    }

    /**
     * @return ComUpIndtify
     */
    public String getComUpIndtify() {
        return this.comUpIndtify;
    }

    /**
     * @param strategyNewLoan
     */
    public void setStrategyNewLoan(String strategyNewLoan) {
        this.strategyNewLoan = strategyNewLoan == null ? null : strategyNewLoan.trim();
    }

    /**
     * @return StrategyNewLoan
     */
    public String getStrategyNewLoan() {
        return this.strategyNewLoan;
    }

    /**
     * @param isCulEstate
     */
    public void setIsCulEstate(String isCulEstate) {
        this.isCulEstate = isCulEstate == null ? null : isCulEstate.trim();
    }

    /**
     * @return IsCulEstate
     */
    public String getIsCulEstate() {
        return this.isCulEstate;
    }

    /**
     * @param loanType
     */
    public void setLoanType(String loanType) {
        this.loanType = loanType == null ? null : loanType.trim();
    }

    /**
     * @return LoanType
     */
    public String getLoanType() {
        return this.loanType;
    }

    /**
     * @param estateAdjustType
     */
    public void setEstateAdjustType(String estateAdjustType) {
        this.estateAdjustType = estateAdjustType == null ? null : estateAdjustType.trim();
    }

    /**
     * @return EstateAdjustType
     */
    public String getEstateAdjustType() {
        return this.estateAdjustType;
    }

    /**
     * @param newPrdLoan
     */
    public void setNewPrdLoan(String newPrdLoan) {
        this.newPrdLoan = newPrdLoan == null ? null : newPrdLoan.trim();
    }

    /**
     * @return NewPrdLoan
     */
    public String getNewPrdLoan() {
        return this.newPrdLoan;
    }

    /**
     * @param repaySrcDes
     */
    public void setRepaySrcDes(String repaySrcDes) {
        this.repaySrcDes = repaySrcDes == null ? null : repaySrcDes.trim();
    }

    /**
     * @return RepaySrcDes
     */
    public String getRepaySrcDes() {
        return this.repaySrcDes;
    }

    /**
     * @param isAuthorize
     */
    public void setIsAuthorize(String isAuthorize) {
        this.isAuthorize = isAuthorize == null ? null : isAuthorize.trim();
    }

    /**
     * @return IsAuthorize
     */
    public String getIsAuthorize() {
        return this.isAuthorize;
    }

    /**
     * @param authedName
     */
    public void setAuthedName(String authedName) {
        this.authedName = authedName == null ? null : authedName.trim();
    }

    /**
     * @return AuthedName
     */
    public String getAuthedName() {
        return this.authedName;
    }

    /**
     * @param authedCertType
     */
    public void setAuthedCertType(String authedCertType) {
        this.authedCertType = authedCertType == null ? null : authedCertType.trim();
    }

    /**
     * @return AuthedCertType
     */
    public String getAuthedCertType() {
        return this.authedCertType;
    }

    /**
     * @param authedCertCode
     */
    public void setAuthedCertCode(String authedCertCode) {
        this.authedCertCode = authedCertCode == null ? null : authedCertCode.trim();
    }

    /**
     * @return AuthedCertCode
     */
    public String getAuthedCertCode() {
        return this.authedCertCode;
    }

    /**
     * @param authedTelNo
     */
    public void setAuthedTelNo(String authedTelNo) {
        this.authedTelNo = authedTelNo == null ? null : authedTelNo.trim();
    }

    /**
     * @return AuthedTelNo
     */
    public String getAuthedTelNo() {
        return this.authedTelNo;
    }

    /**
     * @param signDate
     */
    public void setSignDate(String signDate) {
        this.signDate = signDate == null ? null : signDate.trim();
    }

    /**
     * @return SignDate
     */
    public String getSignDate() {
        return this.signDate;
    }

    /**
     * @param logoutDate
     */
    public void setLogoutDate(String logoutDate) {
        this.logoutDate = logoutDate == null ? null : logoutDate.trim();
    }

    /**
     * @return LogoutDate
     */
    public String getLogoutDate() {
        return this.logoutDate;
    }

    /**
     * @param chnlSour
     */
    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour == null ? null : chnlSour.trim();
    }

    /**
     * @return ChnlSour
     */
    public String getChnlSour() {
        return this.chnlSour;
    }

    /**
     * @param billDispupeOpt
     */
    public void setBillDispupeOpt(String billDispupeOpt) {
        this.billDispupeOpt = billDispupeOpt == null ? null : billDispupeOpt.trim();
    }

    /**
     * @return BillDispupeOpt
     */
    public String getBillDispupeOpt() {
        return this.billDispupeOpt;
    }

    /**
     * @param arbitrateBch
     */
    public void setArbitrateBch(String arbitrateBch) {
        this.arbitrateBch = arbitrateBch == null ? null : arbitrateBch.trim();
    }

    /**
     * @return ArbitrateBch
     */
    public String getArbitrateBch() {
        return this.arbitrateBch;
    }

    /**
     * @param contQnt
     */
    public void setContQnt(BigDecimal contQnt) {
        this.contQnt = contQnt;
    }

    /**
     * @return ContQnt
     */
    public BigDecimal getContQnt() {
        return this.contQnt;
    }

    /**
     * @param managerBrId
     */
    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId == null ? null : managerBrId.trim();
    }

    /**
     * @return ManagerBrId
     */
    public String getManagerBrId() {
        return this.managerBrId;
    }

    /**
     * @param inputId
     */
    public void setInputId(String inputId) {
        this.inputId = inputId == null ? null : inputId.trim();
    }

    /**
     * @return InputId
     */
    public String getInputId() {
        return this.inputId;
    }

    /**
     * @param inputBrId
     */
    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId == null ? null : inputBrId.trim();
    }

    /**
     * @return InputBrId
     */
    public String getInputBrId() {
        return this.inputBrId;
    }

    /**
     * @param inputDate
     */
    public void setInputDate(String inputDate) {
        this.inputDate = inputDate == null ? null : inputDate.trim();
    }

    /**
     * @return InputDate
     */
    public String getInputDate() {
        return this.inputDate;
    }

    /**
     * @param updId
     */
    public void setUpdId(String updId) {
        this.updId = updId == null ? null : updId.trim();
    }

    /**
     * @return UpdId
     */
    public String getUpdId() {
        return this.updId;
    }

    /**
     * @param updBrId
     */
    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId == null ? null : updBrId.trim();
    }

    /**
     * @return UpdBrId
     */
    public String getUpdBrId() {
        return this.updBrId;
    }

    /**
     * @param updDate
     */
    public void setUpdDate(String updDate) {
        this.updDate = updDate == null ? null : updDate.trim();
    }

    /**
     * @return UpdDate
     */
    public String getUpdDate() {
        return this.updDate;
    }

    /**
     * @param contStatus
     */
    public void setContStatus(String contStatus) {
        this.contStatus = contStatus == null ? null : contStatus.trim();
    }

    /**
     * @return ContStatus
     */
    public String getContStatus() {
        return this.contStatus;
    }

    /**
     * @param courtAddr
     */
    public void setCourtAddr(String courtAddr) {
        this.courtAddr = courtAddr == null ? null : courtAddr.trim();
    }

    /**
     * @return CourtAddr
     */
    public String getCourtAddr() {
        return this.courtAddr;
    }

    /**
     * @param arbitrateAddr
     */
    public void setArbitrateAddr(String arbitrateAddr) {
        this.arbitrateAddr = arbitrateAddr == null ? null : arbitrateAddr.trim();
    }

    /**
     * @return ArbitrateAddr
     */
    public String getArbitrateAddr() {
        return this.arbitrateAddr;
    }

    /**
     * @param otherOpt
     */
    public void setOtherOpt(String otherOpt) {
        this.otherOpt = otherOpt == null ? null : otherOpt.trim();
    }

    /**
     * @return OtherOpt
     */
    public String getOtherOpt() {
        return this.otherOpt;
    }

    /**
     * @param pundContNo
     */
    public void setPundContNo(String pundContNo) {
        this.pundContNo = pundContNo == null ? null : pundContNo.trim();
    }

    /**
     * @return PundContNo
     */
    public String getPundContNo() {
        return this.pundContNo;
    }

    /**
     * @param spplClause
     */
    public void setSpplClause(String spplClause) {
        this.spplClause = spplClause == null ? null : spplClause.trim();
    }

    /**
     * @return SpplClause
     */
    public String getSpplClause() {
        return this.spplClause;
    }

    /**
     * @param signAddr
     */
    public void setSignAddr(String signAddr) {
        this.signAddr = signAddr == null ? null : signAddr.trim();
    }

    /**
     * @return SignAddr
     */
    public String getSignAddr() {
        return this.signAddr;
    }

    /**
     * @param busiNetwork
     */
    public void setBusiNetwork(String busiNetwork) {
        this.busiNetwork = busiNetwork == null ? null : busiNetwork.trim();
    }

    /**
     * @return BusiNetwork
     */
    public String getBusiNetwork() {
        return this.busiNetwork;
    }

    /**
     * @param mainBusiPalce
     */
    public void setMainBusiPalce(String mainBusiPalce) {
        this.mainBusiPalce = mainBusiPalce == null ? null : mainBusiPalce.trim();
    }

    /**
     * @return MainBusiPalce
     */
    public String getMainBusiPalce() {
        return this.mainBusiPalce;
    }

    /**
     * @param contTemplate
     */
    public void setContTemplate(String contTemplate) {
        this.contTemplate = contTemplate == null ? null : contTemplate.trim();
    }

    /**
     * @return ContTemplate
     */
    public String getContTemplate() {
        return this.contTemplate;
    }

    /**
     * @param irFloatPoint
     */
    public void setIrFloatPoint(BigDecimal irFloatPoint) {
        this.irFloatPoint = irFloatPoint;
    }

    /**
     * @return IrFloatPoint
     */
    public BigDecimal getIrFloatPoint() {
        return this.irFloatPoint;
    }

    public BigDecimal getContPrintNum() {
        return contPrintNum;
    }

    public void setContPrintNum(BigDecimal contPrintNum) {
        this.contPrintNum = contPrintNum;
    }
}