package cn.com.yusys.yusp.dto.server.xdxw0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 请求Data：优企贷共借人、合同信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0013DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "commonCertNo不能为空")
    @JsonProperty(value = "commonCertNo")
    private String commonCertNo;//共借人证件号

    public String getCommonCertNo() {
        return commonCertNo;
    }

    public void setCommonCertNo(String commonCertNo) {
        this.commonCertNo = commonCertNo;
    }

    @Override
    public String toString() {
        return "Xdxw0013DataReqDto{" +
                "commonCertNo='" + commonCertNo + '\'' +
                '}';
    }
}
