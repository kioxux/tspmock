package cn.com.yusys.yusp.dto.server.xdht0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：银承协议查询接口
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0002DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//银承协议编号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同号
    @JsonProperty(value = "drwrCusId")
    private String drwrCusId;//出票人客户号
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "drwrName")
    private String drwrName;//出票人名称
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "agrTotlAmt")
    private String agrTotlAmt;//协议总金额
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同签订日期
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同到期日期
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "aorgAgrLowSecurityRate")
    private BigDecimal aorgAgrLowSecurityRate;//银承协议最低保证金比例

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getDrwrCusId() {
        return drwrCusId;
    }

    public void setDrwrCusId(String drwrCusId) {
        this.drwrCusId = drwrCusId;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getDrwrName() {
        return drwrName;
    }

    public void setDrwrName(String drwrName) {
        this.drwrName = drwrName;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public String getAgrTotlAmt() {
        return agrTotlAmt;
    }

    public void setAgrTotlAmt(String agrTotlAmt) {
        this.agrTotlAmt = agrTotlAmt;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public BigDecimal getAorgAgrLowSecurityRate() {
        return aorgAgrLowSecurityRate;
    }

    public void setAorgAgrLowSecurityRate(BigDecimal aorgAgrLowSecurityRate) {
        this.aorgAgrLowSecurityRate = aorgAgrLowSecurityRate;
    }

    @Override
    public String toString() {
        return "Xdht0002DataRespDto{" +
                "contNo='" + contNo + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", drwrCusId='" + drwrCusId + '\'' +
                ", billNo='" + billNo + '\'' +
                ", drwrName='" + drwrName + '\'' +
                ", contType='" + contType + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", agrTotlAmt='" + agrTotlAmt + '\'' +
                ", curType='" + curType + '\'' +
                ", contStartDate='" + contStartDate + '\'' +
                ", contEndDate='" + contEndDate + '\'' +
                ", contStatus='" + contStatus + '\'' +
                ", aorgAgrLowSecurityRate=" + aorgAgrLowSecurityRate +
                '}';
    }
}
