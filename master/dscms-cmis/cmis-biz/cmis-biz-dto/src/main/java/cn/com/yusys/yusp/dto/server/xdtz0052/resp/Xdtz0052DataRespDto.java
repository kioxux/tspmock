package cn.com.yusys.yusp.dto.server.xdtz0052.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据借据号查询申请人行内还款（利息、本金）该笔借据次数
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0052DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "repayThisBillTimes")
    private String repayThisBillTimes;//行内还款（利息、本金）该笔借据次数

    public String getRepayThisBillTimes() {
        return repayThisBillTimes;
    }

    public void setRepayThisBillTimes(String repayThisBillTimes) {
        this.repayThisBillTimes = repayThisBillTimes;
    }

    @Override
    public String toString() {
        return "Xdtz0052DataRespDto{" +
                "repayThisBillTimes='" + repayThisBillTimes + '\'' +
                '}';
    }
}
