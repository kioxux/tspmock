package cn.com.yusys.yusp.dto.server.xdzc0003.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：客户资产池下资产清单列表查询（出池时查询）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0003DataRespDto implements Serializable{
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//协议编号
    @JsonProperty(value = "contCNNO")
    private String contCNNO;//中文协议编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "grtMode")
    private String grtMode;//担保方式
    @JsonProperty(value = "conrCurType")
    private String conrCurType;//合同币种
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "contTerm")
    private Integer contTerm;//合同期限
    @JsonProperty(value = "startDate")
    private String startDate;//起始日期
    @JsonProperty(value = "endDate")
    private String endDate;//到期日期
    @JsonProperty(value = "contSignDate")
    private String contSignDate;//签订日期
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "lowRiskLmt")
    private BigDecimal lowRiskLmt;//低风险授信额度
    @JsonProperty(value = "commenRiskLmt")
    private BigDecimal commenRiskLmt;//一般风险授信额度
    @JsonProperty(value = "supshAgrAmt")
    private BigDecimal supshAgrAmt;//超短贷协议金额
    @JsonProperty(value = "assetPoolFinAmt")
    private BigDecimal assetPoolFinAmt;//资产池融资额度
    @JsonProperty(value = "assetPoolAvaAmt")
    private BigDecimal assetPoolAvaAmt;//可用资产池融资额度
    @JsonProperty(value = "rateAdjMode")
    private String rateAdjMode;//利率调整方式
    @JsonProperty(value = "prPriceInterval")
    private String prPriceInterval;//LPR定价区间
    @JsonProperty(value = "rateFloatPoint")
    private BigDecimal rateFloatPoint;//浮动点数
    @JsonProperty(value = "curtLprRate")
    private BigDecimal curtLprRate;//当前LPR利率
    @JsonProperty(value = "execRateYear")
    private BigDecimal execRateYear;//执行年利率
    @JsonProperty(value = "overdueExecRate")
    private BigDecimal overdueExecRate;//逾期执行年利率
    @JsonProperty(value = "ciExecRate")
    private BigDecimal ciExecRate;//复息执行年利率
    @JsonProperty(value = "chgrRate")
    private BigDecimal chgrRate;//手续费率
    @JsonProperty(value = "padRateYear")
    private BigDecimal padRateYear;//垫款利率
    @JsonProperty(value = "acctNo")
    private String acctNo;//账号
    @JsonProperty(value = "acctName")
    private String acctName;//账号名称
    @JsonProperty(value = "guarContNo")
    private String guarContNo;//担保合同编号
    @JsonProperty(value = "guarContName")
    private String guarContName;//担保合同名称
    @JsonProperty(value = "bailAccNo")
    private String bailAccNo;//保证金账号
    @JsonProperty(value = "acctsvcrNo")
    private String acctsvcrNo;//保证金账号开户行行号
    @JsonProperty(value = "bailAccNoSubSeq")
    private String bailAccNoSubSeq;//保证金账号子序号
    @JsonProperty(value = "bailCurType")
    private String bailCurType;//保证金币种
    @JsonProperty(value = "bailRate")
    private BigDecimal bailRate;//保证金比例
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//主管机构
    @JsonProperty(value = "lmtStartDate")
    private String lmtStartDate;//授信起始日
    @JsonProperty(value = "lmtEndDate")
    private String lmtEndDate;//授信到期日( 到期+宽限期)

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getContCNNO() {
        return contCNNO;
    }

    public void setContCNNO(String contCNNO) {
        this.contCNNO = contCNNO;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getGrtMode() {
        return grtMode;
    }

    public void setGrtMode(String grtMode) {
        this.grtMode = grtMode;
    }

    public String getConrCurType() {
        return conrCurType;
    }

    public void setConrCurType(String conrCurType) {
        this.conrCurType = conrCurType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public Integer getContTerm() {
        return contTerm;
    }

    public void setContTerm(Integer contTerm) {
        this.contTerm = contTerm;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getContSignDate() {
        return contSignDate;
    }

    public void setContSignDate(String contSignDate) {
        this.contSignDate = contSignDate;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public BigDecimal getLowRiskLmt() {
        return lowRiskLmt;
    }

    public void setLowRiskLmt(BigDecimal lowRiskLmt) {
        this.lowRiskLmt = lowRiskLmt;
    }

    public BigDecimal getCommenRiskLmt() {
        return commenRiskLmt;
    }

    public void setCommenRiskLmt(BigDecimal commenRiskLmt) {
        this.commenRiskLmt = commenRiskLmt;
    }

    public BigDecimal getSupshAgrAmt() {
        return supshAgrAmt;
    }

    public void setSupshAgrAmt(BigDecimal supshAgrAmt) {
        this.supshAgrAmt = supshAgrAmt;
    }

    public BigDecimal getAssetPoolFinAmt() {
        return assetPoolFinAmt;
    }

    public void setAssetPoolFinAmt(BigDecimal assetPoolFinAmt) {
        this.assetPoolFinAmt = assetPoolFinAmt;
    }

    public BigDecimal getAssetPoolAvaAmt() {
        return assetPoolAvaAmt;
    }

    public void setAssetPoolAvaAmt(BigDecimal assetPoolAvaAmt) {
        this.assetPoolAvaAmt = assetPoolAvaAmt;
    }

    public String getRateAdjMode() {
        return rateAdjMode;
    }

    public void setRateAdjMode(String rateAdjMode) {
        this.rateAdjMode = rateAdjMode;
    }

    public String getPrPriceInterval() {
        return prPriceInterval;
    }

    public void setPrPriceInterval(String prPriceInterval) {
        this.prPriceInterval = prPriceInterval;
    }

    public BigDecimal getRateFloatPoint() {
        return rateFloatPoint;
    }

    public void setRateFloatPoint(BigDecimal rateFloatPoint) {
        this.rateFloatPoint = rateFloatPoint;
    }

    public BigDecimal getCurtLprRate() {
        return curtLprRate;
    }

    public void setCurtLprRate(BigDecimal curtLprRate) {
        this.curtLprRate = curtLprRate;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public BigDecimal getOverdueExecRate() {
        return overdueExecRate;
    }

    public void setOverdueExecRate(BigDecimal overdueExecRate) {
        this.overdueExecRate = overdueExecRate;
    }

    public BigDecimal getCiExecRate() {
        return ciExecRate;
    }

    public void setCiExecRate(BigDecimal ciExecRate) {
        this.ciExecRate = ciExecRate;
    }

    public BigDecimal getChgrRate() {
        return chgrRate;
    }

    public void setChgrRate(BigDecimal chgrRate) {
        this.chgrRate = chgrRate;
    }

    public BigDecimal getPadRateYear() {
        return padRateYear;
    }

    public void setPadRateYear(BigDecimal padRateYear) {
        this.padRateYear = padRateYear;
    }

    public String getAcctNo() {
        return acctNo;
    }

    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getGuarContNo() {
        return guarContNo;
    }

    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    public String getGuarContName() {
        return guarContName;
    }

    public void setGuarContName(String guarContName) {
        this.guarContName = guarContName;
    }

    public String getBailAccNo() {
        return bailAccNo;
    }

    public void setBailAccNo(String bailAccNo) {
        this.bailAccNo = bailAccNo;
    }

    public String getBailAccNoSubSeq() {
        return bailAccNoSubSeq;
    }

    public void setBailAccNoSubSeq(String bailAccNoSubSeq) {
        this.bailAccNoSubSeq = bailAccNoSubSeq;
    }

    public String getBailCurType() {
        return bailCurType;
    }

    public void setBailCurType(String bailCurType) {
        this.bailCurType = bailCurType;
    }

    public BigDecimal getBailRate() {
        return bailRate;
    }

    public void setBailRate(BigDecimal bailRate) {
        this.bailRate = bailRate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getLmtStartDate() {
        return lmtStartDate;
    }

    public void setLmtStartDate(String lmtStartDate) {
        this.lmtStartDate = lmtStartDate;
    }

    public String getLmtEndDate() {
        return lmtEndDate;
    }

    public void setLmtEndDate(String lmtEndDate) {
        this.lmtEndDate = lmtEndDate;
    }

    public String getAcctsvcrNo() {
        return acctsvcrNo;
    }

    public void setAcctsvcrNo(String acctsvcrNo) {
        this.acctsvcrNo = acctsvcrNo;
    }

    @Override
    public String toString() {
        return "Xdzc0003DataRespDto{" +
                "contNo='" + contNo + '\'' +
                ", contCNNO='" + contCNNO + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", grtMode='" + grtMode + '\'' +
                ", conrCurType='" + conrCurType + '\'' +
                ", contAmt=" + contAmt +
                ", contTerm=" + contTerm +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", contSignDate='" + contSignDate + '\'' +
                ", contStatus='" + contStatus + '\'' +
                ", lowRiskLmt=" + lowRiskLmt +
                ", commenRiskLmt=" + commenRiskLmt +
                ", supshAgrAmt=" + supshAgrAmt +
                ", assetPoolFinAmt=" + assetPoolFinAmt +
                ", assetPoolAvaAmt=" + assetPoolAvaAmt +
                ", rateAdjMode='" + rateAdjMode + '\'' +
                ", prPriceInterval='" + prPriceInterval + '\'' +
                ", rateFloatPoint=" + rateFloatPoint +
                ", curtLprRate=" + curtLprRate +
                ", execRateYear=" + execRateYear +
                ", overdueExecRate=" + overdueExecRate +
                ", ciExecRate=" + ciExecRate +
                ", chgrRate=" + chgrRate +
                ", padRateYear=" + padRateYear +
                ", acctNo='" + acctNo + '\'' +
                ", acctName='" + acctName + '\'' +
                ", guarContNo='" + guarContNo + '\'' +
                ", guarContName='" + guarContName + '\'' +
                ", bailAccNo='" + bailAccNo + '\'' +
                ", acctsvcrNo='" + acctsvcrNo + '\'' +
                ", bailAccNoSubSeq='" + bailAccNoSubSeq + '\'' +
                ", bailCurType='" + bailCurType + '\'' +
                ", bailRate=" + bailRate +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", lmtStartDate='" + lmtStartDate + '\'' +
                ", lmtEndDate='" + lmtEndDate + '\'' +
                '}';
    }
}