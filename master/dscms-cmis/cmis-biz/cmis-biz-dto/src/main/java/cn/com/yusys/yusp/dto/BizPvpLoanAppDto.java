package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @className BizPvpLoanAppDto
 * 放款申请dto
 * @Date 2021/4/17 : 11:38
 */

public class BizPvpLoanAppDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 放款流水号 **/
    private String pvpSerno;

    /** 业务流水号 **/
    private String iqpSerno;

    /** 借据编号 **/
    private String billNo;

    /** 合同编号 **/
    private String contNo;

    /** 中文合同编号 **/
    private String cnContNo;

    /** 客户编号 **/
    private String cusId;

    /** 产品编号 **/
    private String prdId;

    /** 本金宽限方式 **/
    private String capGraperType;

    /** 本金宽限天数 **/
    private java.math.BigDecimal capGraperDay;

    /** 利息宽限方式 **/
    private String intGraperType;

    /** 本金宽限天数 **/
    private java.math.BigDecimal intGraperDay;

    /** 扣款扣息方式 **/
    private String deductDeduType;

    /** 特殊业务类型 STD_ZB_SPEBS_TYP **/
    private String especBizType;

    /** 币种 STD_ZB_CUR_TYP **/
    private String curType;

    /** 合同金额 **/
    private java.math.BigDecimal contAmt;

    /** 放款金额 **/
    private java.math.BigDecimal pvpAmt;

    /** 支付方式 STD_ZB_RAY_MODE **/
    private String payWay;

    /** 是否立即发起受托支付 STD_ZB_YES_NO **/
    private String isCfirmPay;

    /** 期限类型 STD_ZB_TERM_TYP **/
    private String termType;

    /** 申请期限 **/
    private String approveTerm;

    /** 授权状态 STD_ZB_AUTH_ST **/
    private String authStatus;

    /** 渠道来源 STD_ZB_CHNL_SOUR **/
    private String chnlSour;

    /** 主办机构 **/
    private String managerBrId;

    /** 放款机构 **/
    private String acctBrId;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 申请状态 STD_ZB_APP_ST **/
    private String approveStatus;

    /** 到期日期 **/
    private String endDate;

    /** 操作类型  STD_ZB_OPR_TYPE **/
    private String oprType;

    /**业务申请类型**/
    private String bizType;

    /** 客户评级系数 **/
    private java.math.BigDecimal customerRatingFactor;

    /** 利率 **/
    private java.math.BigDecimal realityIrY;

    @Override
    public String toString() {
        return "BizPvpLoanAppDto{" +
                "pvpSerno='" + pvpSerno + '\'' +
                ", iqpSerno='" + iqpSerno + '\'' +
                ", billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", prdId='" + prdId + '\'' +
                ", capGraperType='" + capGraperType + '\'' +
                ", capGraperDay=" + capGraperDay +
                ", intGraperType='" + intGraperType + '\'' +
                ", intGraperDay=" + intGraperDay +
                ", deductDeduType='" + deductDeduType + '\'' +
                ", especBizType='" + especBizType + '\'' +
                ", curType='" + curType + '\'' +
                ", contAmt=" + contAmt +
                ", pvpAmt=" + pvpAmt +
                ", payWay='" + payWay + '\'' +
                ", isCfirmPay='" + isCfirmPay + '\'' +
                ", termType='" + termType + '\'' +
                ", approveTerm='" + approveTerm + '\'' +
                ", authStatus='" + authStatus + '\'' +
                ", chnlSour='" + chnlSour + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", acctBrId='" + acctBrId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", endDate='" + endDate + '\'' +
                ", oprType='" + oprType + '\'' +
                ", bizType='" + bizType + '\'' +
                ", customerRatingFactor=" + customerRatingFactor +
                ", realityIrY=" + realityIrY +
                '}';
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getCapGraperType() {
        return capGraperType;
    }

    public void setCapGraperType(String capGraperType) {
        this.capGraperType = capGraperType;
    }

    public BigDecimal getCapGraperDay() {
        return capGraperDay;
    }

    public void setCapGraperDay(BigDecimal capGraperDay) {
        this.capGraperDay = capGraperDay;
    }

    public String getIntGraperType() {
        return intGraperType;
    }

    public void setIntGraperType(String intGraperType) {
        this.intGraperType = intGraperType;
    }

    public BigDecimal getIntGraperDay() {
        return intGraperDay;
    }

    public void setIntGraperDay(BigDecimal intGraperDay) {
        this.intGraperDay = intGraperDay;
    }

    public String getDeductDeduType() {
        return deductDeduType;
    }

    public void setDeductDeduType(String deductDeduType) {
        this.deductDeduType = deductDeduType;
    }

    public String getEspecBizType() {
        return especBizType;
    }

    public void setEspecBizType(String especBizType) {
        this.especBizType = especBizType;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getPvpAmt() {
        return pvpAmt;
    }

    public void setPvpAmt(BigDecimal pvpAmt) {
        this.pvpAmt = pvpAmt;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public String getIsCfirmPay() {
        return isCfirmPay;
    }

    public void setIsCfirmPay(String isCfirmPay) {
        this.isCfirmPay = isCfirmPay;
    }

    public String getTermType() {
        return termType;
    }

    public void setTermType(String termType) {
        this.termType = termType;
    }

    public String getApproveTerm() {
        return approveTerm;
    }

    public void setApproveTerm(String approveTerm) {
        this.approveTerm = approveTerm;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getChnlSour() {
        return chnlSour;
    }

    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getAcctBrId() {
        return acctBrId;
    }

    public void setAcctBrId(String acctBrId) {
        this.acctBrId = acctBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public BigDecimal getCustomerRatingFactor() {
        return customerRatingFactor;
    }

    public void setCustomerRatingFactor(BigDecimal customerRatingFactor) {
        this.customerRatingFactor = customerRatingFactor;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }
}
