package cn.com.yusys.yusp.dto.server.xdtz0036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0036DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "overdueTimes")
    private String overdueTimes;//逾期次数

    public String getOverdueTimes() {
        return overdueTimes;
    }

    public void setOverdueTimes(String overdueTimes) {
        this.overdueTimes = overdueTimes;
    }

    @Override
    public String toString() {
        return "Xdtz0036DataRespDto{" +
                "overdueTimes='" + overdueTimes + '\'' +
                '}';
    }
}
