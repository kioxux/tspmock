package cn.com.yusys.yusp.dto.server.xdxw0058.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据流水号查询借据号
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0058DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applySerno")
    private String applySerno;//申请流水号


    public String getApplySerno() {
        return applySerno;
    }

    public void setApplySerno(String applySerno) {
        this.applySerno = applySerno;
    }

    @Override
    public String toString() {
        return "Xdxw0058DataReqDto{" +
                "applySerno='" + applySerno + '\'' +
                '}';
    }
}
