package cn.com.yusys.yusp.dto.server.xdtz0030.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查看信贷贴现台账中票据是否已经存在
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0030DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "qnt")
    private Integer qnt;//数量

    public Integer getQnt() {
        return qnt;
    }

    public void setQnt(Integer qnt) {
        this.qnt = qnt;
    }

    public Xdtz0030DataRespDto() {
    }

    public Xdtz0030DataRespDto(Integer qnt) {
        this.qnt = qnt;
    }

    @Override
    public String toString() {
        return "Xdtz0030DataRespDto{" +
                "qnt='" + qnt + '\'' +
                '}';
    }
}