package cn.com.yusys.yusp.dto.server.xdls0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/18 11:02:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/18 11:02
 * @since 2021/5/18 11:02
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款终止日期
    @JsonProperty(value = "loanTerm")
    private String loanTerm;//贷款期限
    @JsonProperty(value = "isEbankPay")
    private String isEbankPay;//是否网银支付
    @JsonProperty(value = "rate")
    private BigDecimal rate;//执行利率
    @JsonProperty(value = "lmt")
    private BigDecimal lmt;//可用余额
    @JsonProperty(value = "loanBal")
    private BigDecimal loanBal;//贷款余额
    @JsonProperty(value = "guarContNo")
    private String guarContNo;//担保合同号
    @JsonProperty(value = "orgNo")
    private String orgNo;//机构号
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态
    @JsonProperty(value = "guarWay")
    private String guarWay;//担保方式
    @JsonProperty(value = "lmtAccNo")
    private String lmtAccNo;//授信额度编号

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getIsEbankPay() {
        return isEbankPay;
    }

    public void setIsEbankPay(String isEbankPay) {
        this.isEbankPay = isEbankPay;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getLmt() {
        return lmt;
    }

    public void setLmt(BigDecimal lmt) {
        this.lmt = lmt;
    }

    public BigDecimal getLoanBal() {
        return loanBal;
    }

    public void setLoanBal(BigDecimal loanBal) {
        this.loanBal = loanBal;
    }

    public String getGuarContNo() {
        return guarContNo;
    }

    public void setGuarContNo(String guarContNo) {
        this.guarContNo = guarContNo;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getGuarWay() {
        return guarWay;
    }

    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }


    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", loanTerm='" + loanTerm + '\'' +
                ", isEbankPay='" + isEbankPay + '\'' +
                ", rate=" + rate +
                ", lmt=" + lmt +
                ", loanBal=" + loanBal +
                ", guarContNo='" + guarContNo + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", contStatus='" + contStatus + '\'' +
                ", contStatus='" + guarWay + '\'' +
                ", contStatus='" + lmtAccNo + '\'' +
                '}';
    }
}
