package cn.com.yusys.yusp.dto.server.xddh0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：推送优享贷预警信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0010DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    @NotBlank(message = "billNo借据号不得为空")
    private String billNo;//借据号
    @JsonProperty(value = "modelResult")
    @NotBlank(message = "modelResult模型结果")
    private String modelResult;//模型结果
    @JsonProperty(value = "warnDate")
    @NotBlank(message = "warnDate预警日期")
    private String warnDate;//预警日期
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//逾期本金
    @JsonProperty(value = "loanReality")
    private BigDecimal loanReality;//逾期利息

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getModelResult() {
        return modelResult;
    }

    public void setModelResult(String modelResult) {
        this.modelResult = modelResult;
    }

    public String getWarnDate() {
        return warnDate;
    }

    public void setWarnDate(String warnDate) {
        this.warnDate = warnDate;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getLoanReality() {
        return loanReality;
    }

    public void setLoanReality(BigDecimal loanReality) {
        this.loanReality = loanReality;
    }

    @Override
    public String toString() {
        return "Xddh0010DataReqDto{" +
                "billNo='" + billNo + '\'' +
                ", modelResult='" + modelResult + '\'' +
                ", warnDate='" + warnDate + '\'' +
                ", loanBalance=" + loanBalance +
                ", loanReality=" + loanReality +
                '}';
    }
}
