package cn.com.yusys.yusp.dto.server.xdxw0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：学区信息列表查询（分页）
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0026DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "admitArea")
    private String admitArea;//准入区域（省市区中文）
    @JsonProperty(value = "schoolName")
    private String schoolName;//学校名称
    @JsonProperty(value = "admitName")
    private String admitName;//准入名称
    @JsonProperty(value = "pageNum")
    private Integer pageNum;//页码
    @JsonProperty(value = "pageSize")
    private Integer pageSize;//一页查询数量

    public String getAdmitArea() {
        return admitArea;
    }

    public void setAdmitArea(String admitArea) {
        this.admitArea = admitArea;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getAdmitName() {
        return admitName;
    }

    public void setAdmitName(String admitName) {
        this.admitName = admitName;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Xdxw0026DataReqDto{" +
                "admitArea='" + admitArea + '\'' +
                "schoolName='" + schoolName + '\'' +
                "admitName='" + admitName + '\'' +
                "pageNum='" + pageNum + '\'' +
                "pageSize='" + pageSize + '\'' +
                '}';
    }
}
