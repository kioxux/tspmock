package cn.com.yusys.yusp.dto.server.xdxw0026.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：学区信息列表查询（分页）
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SchoolAreaList implements Serializable {
    @JsonProperty(value = "schoolAreaLvl")
    private String schoolAreaLvl;//学区级别
    @JsonProperty(value = "belgSchoolArea")
    private String belgSchoolArea;//所属学区
    @JsonProperty(value = "schoolName")
    private String schoolName;//学校名称

    public String getSchoolAreaLvl() {
        return schoolAreaLvl;
    }

    public void setSchoolAreaLvl(String schoolAreaLvl) {
        this.schoolAreaLvl = schoolAreaLvl;
    }

    public String getBelgSchoolArea() {
        return belgSchoolArea;
    }

    public void setBelgSchoolArea(String belgSchoolArea) {
        this.belgSchoolArea = belgSchoolArea;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public String toString() {
        return "SchoolAreaList{" +
                "schoolAreaLvl='" + schoolAreaLvl + '\'' +
                ", belgSchoolArea='" + belgSchoolArea + '\'' +
                ", schoolName='" + schoolName + '\'' +
                '}';
    }
}
