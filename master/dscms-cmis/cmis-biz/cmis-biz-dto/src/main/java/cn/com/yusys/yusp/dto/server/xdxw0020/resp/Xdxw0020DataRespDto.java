package cn.com.yusys.yusp.dto.server.xdxw0020.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：客户调查撤销
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0020DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cancelStatus")
    private String cancelStatus;//撤销状态

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    @Override
    public String toString() {
        return "Xdxw0020DataRespDto{" +
                "cancelStatus='" + cancelStatus + '\'' +
                '}';
    }
}  
