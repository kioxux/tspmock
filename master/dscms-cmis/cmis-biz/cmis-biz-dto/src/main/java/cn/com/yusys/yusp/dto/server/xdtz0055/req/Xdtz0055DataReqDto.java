package cn.com.yusys.yusp.dto.server.xdtz0055.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询退回受托信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0055DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusAcctNo")
    private String cusAcctNo;//客户账号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusAcctNo() {
        return cusAcctNo;
    }

    public void setCusAcctNo(String cusAcctNo) {
        this.cusAcctNo = cusAcctNo;
    }

    @Override
    public String toString() {
        return "Xdtz0055DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", cusAcctNo='" + cusAcctNo + '\'' +
                '}';
    }
}
