package cn.com.yusys.yusp.dto.server.xdht0015.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：合同房产人员信息查询
 * @author xll
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdht0015DataRespDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private java.util.List<List> list;

	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Xdht0015DataRespDto{" +
				"list=" + list +
				'}';
	}
}  
