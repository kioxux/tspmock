package cn.com.yusys.yusp.dto.server.xdxw0068.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：授信调查结论信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//主键
    @JsonProperty(value = "is_farm")
    private String is_farm;//是否农户
    @JsonProperty(value = "loan_amt")
    private BigDecimal loan_amt;//贷款金额
    @JsonProperty(value = "loan_term")
    private String loan_term;//贷款期限
    @JsonProperty(value = "loan_rate")
    private BigDecimal loan_rate;//贷款利率
    @JsonProperty(value = "assure_means")
    private String assure_means;//担保方式
    @JsonProperty(value = "assure_means_name")
    private String assure_means_name;//担保方式名称
    @JsonProperty(value = "is_access")
    private String is_access;//是否准入
    @JsonProperty(value = "replay_ways")
    private String replay_ways;//还款方式（调查结论）XD_HK_WAYS
    @JsonProperty(value = "is_good_business")
    private String is_good_business;//确定客户经营是否正常
    @JsonProperty(value = "cus_list_serno")
    private String cus_list_serno;//名单表流水号

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getIs_farm() {
        return is_farm;
    }

    public void setIs_farm(String is_farm) {
        this.is_farm = is_farm;
    }

    public BigDecimal getLoan_amt() {
        return loan_amt;
    }

    public void setLoan_amt(BigDecimal loan_amt) {
        this.loan_amt = loan_amt;
    }

    public String getLoan_term() {
        return loan_term;
    }

    public void setLoan_term(String loan_term) {
        this.loan_term = loan_term;
    }

    public BigDecimal getLoan_rate() {
        return loan_rate;
    }

    public void setLoan_rate(BigDecimal loan_rate) {
        this.loan_rate = loan_rate;
    }

    public String getAssure_means() {
        return assure_means;
    }

    public void setAssure_means(String assure_means) {
        this.assure_means = assure_means;
    }

    public String getAssure_means_name() {
        return assure_means_name;
    }

    public void setAssure_means_name(String assure_means_name) {
        this.assure_means_name = assure_means_name;
    }

    public String getIs_access() {
        return is_access;
    }

    public void setIs_access(String is_access) {
        this.is_access = is_access;
    }

    public String getReplay_ways() {
        return replay_ways;
    }

    public void setReplay_ways(String replay_ways) {
        this.replay_ways = replay_ways;
    }

    public String getIs_good_business() {
        return is_good_business;
    }

    public void setIs_good_business(String is_good_business) {
        this.is_good_business = is_good_business;
    }

    public String getCus_list_serno() {
        return cus_list_serno;
    }

    public void setCus_list_serno(String cus_list_serno) {
        this.cus_list_serno = cus_list_serno;
    }

    @Override
    public String toString() {
        return "List{" +
                "survey_serno='" + survey_serno + '\'' +
                ", is_farm='" + is_farm + '\'' +
                ", loan_amt=" + loan_amt +
                ", loan_term='" + loan_term + '\'' +
                ", loan_rate=" + loan_rate +
                ", assure_means='" + assure_means + '\'' +
                ", assure_means_name='" + assure_means_name + '\'' +
                ", is_access='" + is_access + '\'' +
                ", replay_ways='" + replay_ways + '\'' +
                ", is_good_business='" + is_good_business + '\'' +
                ", cus_list_serno='" + cus_list_serno + '\'' +
                '}';
    }
}
