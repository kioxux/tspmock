package cn.com.yusys.yusp.dto.server.xdtz0037.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：无还本续贷额度状态更新
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0037DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "zxSerno")
    private String zxSerno;//放款笔数


    public String getZxSerno() {
        return zxSerno;
    }

    public void setZxSerno(String zxSerno) {
        this.zxSerno = zxSerno;
    }

    @Override
    public String toString() {
        return "Xdtz0037DataRespDto{" +
                "zxSerno='" + zxSerno + '\'' +
                '}';
    }
}
