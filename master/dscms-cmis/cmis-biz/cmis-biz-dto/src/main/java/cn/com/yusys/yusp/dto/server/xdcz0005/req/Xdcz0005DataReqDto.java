package cn.com.yusys.yusp.dto.server.xdcz0005.req;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：资产池入池接口
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0005DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "chnl")
    private String chnl;//渠道
    @JsonProperty(value = "chnlSerNo")
    private String chnlSerNo;//渠道流水
    @JsonProperty(value = "clkId")
    private String clkId;//柜员号
    @JsonProperty(value = "deptNo")
    private String deptNo;//部门号
    @JsonProperty(value = "orgNo")
    private String orgNo;//机构号
    @JsonProperty(value = "singleOrgDiscAvlBal")
    private BigDecimal singleOrgDiscAvlBal;//单一机构当月贴现可用的发生额

    public String getChnl() {
        return chnl;
    }

    public void setChnl(String chnl) {
        this.chnl = chnl;
    }

    public String getChnlSerNo() {
        return chnlSerNo;
    }

    public void setChnlSerNo(String chnlSerNo) {
        this.chnlSerNo = chnlSerNo;
    }

    public String getClkId() {
        return clkId;
    }

    public void setClkId(String clkId) {
        this.clkId = clkId;
    }

    public String getDeptNo() {
        return deptNo;
    }

    public void setDeptNo(String deptNo) {
        this.deptNo = deptNo;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public BigDecimal getSingleOrgDiscAvlBal() {
        return singleOrgDiscAvlBal;
    }

    public void setSingleOrgDiscAvlBal(BigDecimal singleOrgDiscAvlBal) {
        this.singleOrgDiscAvlBal = singleOrgDiscAvlBal;
    }

    @Override
    public String toString() {
        return "Xdcz0005DataReqDto{" +
                "chnl='" + chnl + '\'' +
                "chnlSerNo='" + chnlSerNo + '\'' +
                "clkId='" + clkId + '\'' +
                "deptNo='" + deptNo + '\'' +
                "orgNo='" + orgNo + '\'' +
                "singleOrgDiscAvlBal='" + singleOrgDiscAvlBal + '\'' +
                '}';
    }
}


