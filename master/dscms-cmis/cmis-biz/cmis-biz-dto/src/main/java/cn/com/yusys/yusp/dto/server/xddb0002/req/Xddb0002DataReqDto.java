package cn.com.yusys.yusp.dto.server.xddb0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：押品状态查询
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0002DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarNo")
    @NotNull(message = "押品编号guarNo不能为空")
    @NotEmpty(message = "押品编号guarNo不能为空")
    private String guarNo;//押品编号

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    @Override
    public String toString() {
        return "Xddb0002DataReqDto{" +
                ", guarNo='" + guarNo + '\'' +
                '}';
    }
}
