package cn.com.yusys.yusp.dto.server.xddb0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：押品状态查询
 * @author xull
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xddb0002DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "status")
	private String status;//押品状态

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Xddb0002DataRespDto{" +
				"status=" + status +
				'}';
	}
}