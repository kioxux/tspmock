package cn.com.yusys.yusp.dto.server.xdkh0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：还款试算计划查询日期1
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdkh0023DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "contNo")
    private String contNo;// 合同号

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    @Override
    public String toString() {
        return "Xdkh0023DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certNo='" + certNo + '\'' +
                ", contNo='" + contNo + '\'' +
                '}';
    }
}
