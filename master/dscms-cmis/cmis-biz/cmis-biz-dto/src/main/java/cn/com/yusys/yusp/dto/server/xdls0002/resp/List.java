package cn.com.yusys.yusp.dto.server.xdls0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：市民贷联系人信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "linkmanName")
    private String linkmanName;//联系人姓名
    @JsonProperty(value = "sex")
    private String sex;//性别
    @JsonProperty(value = "nati")
    private String nati;//国籍
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号
    @JsonProperty(value = "certExpDt")
    private String certExpDt;//证件到期日
    @JsonProperty(value = "phone")
    private String phone;//联系电话
    @JsonProperty(value = "linkmanAddr")
    private String linkmanAddr;//联系人地址
    @JsonProperty(value = "rela")
    private String rela;//关系
    @JsonProperty(value = "memJob")
    private String memJob;//职业

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getLinkmanName() {
        return linkmanName;
    }

    public void setLinkmanName(String linkmanName) {
        this.linkmanName = linkmanName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNati() {
        return nati;
    }

    public void setNati(String nati) {
        this.nati = nati;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCertExpDt() {
        return certExpDt;
    }

    public void setCertExpDt(String certExpDt) {
        this.certExpDt = certExpDt;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLinkmanAddr() {
        return linkmanAddr;
    }

    public void setLinkmanAddr(String linkmanAddr) {
        this.linkmanAddr = linkmanAddr;
    }

    public String getRela() {
        return rela;
    }

    public void setRela(String rela) {
        this.rela = rela;
    }

    public String getMemJob() {
        return memJob;
    }

    public void setMemJob(String memJob) {
        this.memJob = memJob;
    }

    @Override
    public String toString() {
        return "List{" +
                "cusId='" + cusId + '\'' +
                "linkmanName='" + linkmanName + '\'' +
                ", sex='" + sex + '\'' +
                ", nati='" + nati + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", certExpDt='" + certExpDt + '\'' +
                ", phone='" + phone + '\'' +
                ", linkmanAddr='" + linkmanAddr + '\'' +
                ", rela='" + rela + '\'' +
                ", memJob='" + memJob + '\'' +
                '}';
    }
}
