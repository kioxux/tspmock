package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;

public class CreditReportAndRelDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 关联业务编号 **/
    private String cqbrSerno;

    /** 业务流水号 **/
    private String bizSerno;

    /** 征信查询场景 **/
    private String scene;

    /** 征信查询流水号 **/
    private String crqlSerno;

    /** 证件类型 **/
    private String certType;

    /** 证件号码 **/
    private String certCode;

    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 主借款人客户号 **/
    private String borrowerCusId;

    /** 主借款人证件号码 **/
    private String borrowerCertCode;

    /** 主借款人名称 **/
    private String borrowerCusName;

    /** 与主借款人关系 **/
    private String borrowRel;

    /** 征信查询类别 **/
    private String qryCls;

    /** 征信查询原因 **/
    private String qryResn;

    /** 查询原因描述 **/
    private String qryResnDec;

    /** 授权书编号 **/
    private String authbookNo;

    /** 授权书内容 **/
    private String authbookContent;

    /** 其他授权书内容 **/
    private String otherAuthbookExt;

    /** 授权书日期 **/
    private String authbookDate;

    /** 主管客户经理 **/
    private String managerId;

    /** 主管机构 **/
    private String managerBrId;

    /** 成功发起时间 **/
    private String sendTime;

    /** 审批状态 **/
    private String approveStatus;

    /** 是否成功发起 **/
    private String isSuccssInit;

    /** 征信报告编号 **/
    private String reportNo;

    /** 征信查询状态 **/
    private String qryStatus;

    /** 征信返回地址 **/
    private String creditUrl;

    /** 报告生成时间 **/
    private String reportCreateTime;

    /** 征信查询标识 **/
    private String qryFlag;

    /** 影像编号 **/
    private String imageNo;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    public String getCqbrSerno() {
        return cqbrSerno;
    }

    public void setCqbrSerno(String cqbrSerno) {
        this.cqbrSerno = cqbrSerno;
    }

    public String getBizSerno() {
        return bizSerno;
    }

    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getCrqlSerno() {
        return crqlSerno;
    }

    public void setCrqlSerno(String crqlSerno) {
        this.crqlSerno = crqlSerno;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBorrowerCusId() {
        return borrowerCusId;
    }

    public void setBorrowerCusId(String borrowerCusId) {
        this.borrowerCusId = borrowerCusId;
    }

    public String getBorrowerCertCode() {
        return borrowerCertCode;
    }

    public void setBorrowerCertCode(String borrowerCertCode) {
        this.borrowerCertCode = borrowerCertCode;
    }

    public String getBorrowerCusName() {
        return borrowerCusName;
    }

    public void setBorrowerCusName(String borrowerCusName) {
        this.borrowerCusName = borrowerCusName;
    }

    public String getBorrowRel() {
        return borrowRel;
    }

    public void setBorrowRel(String borrowRel) {
        this.borrowRel = borrowRel;
    }

    public String getQryCls() {
        return qryCls;
    }

    public void setQryCls(String qryCls) {
        this.qryCls = qryCls;
    }

    public String getQryResn() {
        return qryResn;
    }

    public void setQryResn(String qryResn) {
        this.qryResn = qryResn;
    }

    public String getQryResnDec() {
        return qryResnDec;
    }

    public void setQryResnDec(String qryResnDec) {
        this.qryResnDec = qryResnDec;
    }

    public String getAuthbookNo() {
        return authbookNo;
    }

    public void setAuthbookNo(String authbookNo) {
        this.authbookNo = authbookNo;
    }

    public String getAuthbookContent() {
        return authbookContent;
    }

    public void setAuthbookContent(String authbookContent) {
        this.authbookContent = authbookContent;
    }

    public String getOtherAuthbookExt() {
        return otherAuthbookExt;
    }

    public void setOtherAuthbookExt(String otherAuthbookExt) {
        this.otherAuthbookExt = otherAuthbookExt;
    }

    public String getAuthbookDate() {
        return authbookDate;
    }

    public void setAuthbookDate(String authbookDate) {
        this.authbookDate = authbookDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getIsSuccssInit() {
        return isSuccssInit;
    }

    public void setIsSuccssInit(String isSuccssInit) {
        this.isSuccssInit = isSuccssInit;
    }

    public String getReportNo() {
        return reportNo;
    }

    public void setReportNo(String reportNo) {
        this.reportNo = reportNo;
    }

    public String getQryStatus() {
        return qryStatus;
    }

    public void setQryStatus(String qryStatus) {
        this.qryStatus = qryStatus;
    }

    public String getCreditUrl() {
        return creditUrl;
    }

    public void setCreditUrl(String creditUrl) {
        this.creditUrl = creditUrl;
    }

    public String getReportCreateTime() {
        return reportCreateTime;
    }

    public void setReportCreateTime(String reportCreateTime) {
        this.reportCreateTime = reportCreateTime;
    }

    public String getQryFlag() {
        return qryFlag;
    }

    public void setQryFlag(String qryFlag) {
        this.qryFlag = qryFlag;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
