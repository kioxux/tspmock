package cn.com.yusys.yusp.dto.server.xddb0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：据押品编号查询核心及信贷系统有无押品数据
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0022DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "book_serno")
    @NotNull(message = "核心担保品编号book_serno不能为空")
    @NotEmpty(message = "核心担保品编号book_serno不能为空")
    private String book_serno;//押品编号

    public String getBook_serno() {
        return book_serno;
    }

    public void setBook_serno(String book_serno) {
        this.book_serno = book_serno;
    }

    @Override
    public String toString() {
        return "Xddb0022DataReqDto{" +
                "book_serno='" + book_serno + '\'' +
                '}';
    }
}
