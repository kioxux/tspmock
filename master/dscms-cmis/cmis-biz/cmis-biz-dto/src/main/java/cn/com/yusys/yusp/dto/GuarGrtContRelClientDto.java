package cn.com.yusys.yusp.dto;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 押品与担保合同交互dto
 */
public class GuarGrtContRelClientDto implements Serializable {
    private static final long serialVersionUID = 1L;
    //入参信息
    private String guarNos;//押品编号，可多个押品编号，以【,】进行分割
    private List<Map> list;

    //出参信息
    private String rtnCode;//返回码值，成功为"000000"
    private String rtnMsg;//返回信息
    private List<GuarEvalInfoClientDto> guarEvalInfoClientDtos;//押品信息列表,包含押品编号，币种，押品评估金额

    public String getGuarNos() {
        return guarNos;
    }

    public void setGuarNos(String guarNos) {
        this.guarNos = guarNos;
    }

    public String getRtnCode() {
        return rtnCode;
    }

    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }

    public List<GuarEvalInfoClientDto> getGuarEvalInfoClientDtos() {
        return guarEvalInfoClientDtos;
    }
    
    public List<Map> getList() {
        return list;
    }
    
    public void setList(List<Map> list) {
        this.list = list;
    }
    
    public void setGuarEvalInfoClientDtos(List<GuarEvalInfoClientDto> guarEvalInfoClientDtos) {
        this.guarEvalInfoClientDtos = guarEvalInfoClientDtos;
    }
}
