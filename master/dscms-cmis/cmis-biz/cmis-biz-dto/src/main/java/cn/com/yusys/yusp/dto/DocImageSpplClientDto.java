package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class DocImageSpplClientDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 业务流水号 **/
    private String bizSerno;

    /**
     * 影像补扫类型 STD_SPPL_TYPE
     * 01:放款影像补扫
     * 02:合同审核影像补扫
     * 03:授信类型资料补录
     * 04:合作方准入影像补扫
     * 05:合作方协议签订影像补扫
     * 06:征信影像补扫
     * 07:展期影像补扫
     * 08:担保变更影像补扫
     **/
    private String spplType;

    /**
     * 补扫业务品种 STD_SPPL_BIZ_TYPE
     * 01：一般贷款出账
     * 02：银承出账
     * 03：零售放款
     * 04：小微放款
     * 05：单户授信
     * 06：集团授信
     * 07：零售业务授信
     **/
    private String spplBizType;

    /** 原业务流程实例 **/
    private String bizInstanceId;
    /** 客户编号 **/
    private String cusId;
    /** 客户名称 **/
    private String cusName;
    /** 合同编号 **/
    private String contNo;
    /** 借据编号 **/
    private String billNo;
    /** 登记人 **/
    private String inputId;
    /** 登记机构 **/
    private String inputBrId;
    /** 产品编号 **/
    private String prdId;
    /** 产品名称 **/
    private String prdName;


    public String getBizSerno() {
        return bizSerno;
    }

    public void setBizSerno(String bizSerno) {
        this.bizSerno = bizSerno;
    }

    public String getSpplType() {
        return spplType;
    }

    public void setSpplType(String spplType) {
        this.spplType = spplType;
    }

    public String getSpplBizType() {
        return spplBizType;
    }

    public void setSpplBizType(String spplBizType) {
        this.spplBizType = spplBizType;
    }

    public String getBizInstanceId() {
        return bizInstanceId;
    }

    public void setBizInstanceId(String bizInstanceId) {
        this.bizInstanceId = bizInstanceId;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }
}
