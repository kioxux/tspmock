package cn.com.yusys.yusp.dto.server.xdcz0005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：资产池入池接口
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0005DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFlag")
    private String opFlag;//成功失败标志
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息
    public String  getOpFlag() { return opFlag; }
    public void setOpFlag(String opFlag ) { this.opFlag = opFlag;}
    public String  getOpMsg() { return opMsg; }
    public void setOpMsg(String opMsg ) { this.opMsg = opMsg;}
    @Override
    public String toString() {
        return "Xdcz0005DataRespDto{" +
            "opFlag='" + opFlag+ '\'' +
                    "opMsg='" + opMsg+ '\'' +
                    '}';
        }
    }
