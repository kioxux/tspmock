package cn.com.yusys.yusp.dto.server.xdzc0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author xs
 * @version 0.1
 * @date 2021/9/17 15:12
 * @since 2021/9/17 15:12
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty("eBillNo")
    private String eBillNo;//电票号码
    @JsonProperty("drftAmt")
    private BigDecimal drftAmt;//票面金额
    @JsonProperty("eBillCurType")
    private String eBillCurType;//电票币种
    @JsonProperty("isseDate")
    private String isseDate;//出票日
    @JsonProperty("isseEndDate")
    private String isseEndDate;//到期日
    @JsonProperty("imnManCusId")
    private String imnManCusId;//质押人客户号
    @JsonProperty("imnCusName")
    private String imnCusName;//质押客户名称
    @JsonProperty("collName")
    private String collName;//质押品名称
    @JsonProperty("aorgNo")
    private String aorgNo;//承兑行行号
    @JsonProperty("accptr")
    private String accptr;//承兑人

    public String geteBillNo() {
        return eBillNo;
    }

    public void seteBillNo(String eBillNo) {
        this.eBillNo = eBillNo;
    }

    public BigDecimal getDrftAmt() {
        return drftAmt;
    }

    public void setDrftAmt(BigDecimal drftAmt) {
        this.drftAmt = drftAmt;
    }

    public String geteBillCurType() {
        return eBillCurType;
    }

    public void seteBillCurType(String eBillCurType) {
        this.eBillCurType = eBillCurType;
    }

    public String getIsseDate() {
        return isseDate;
    }

    public void setIsseDate(String isseDate) {
        this.isseDate = isseDate;
    }

    public String getIsseEndDate() {
        return isseEndDate;
    }

    public void setIsseEndDate(String isseEndDate) {
        this.isseEndDate = isseEndDate;
    }

    public String getImnManCusId() {
        return imnManCusId;
    }

    public void setImnManCusId(String imnManCusId) {
        this.imnManCusId = imnManCusId;
    }

    public String getImnCusName() {
        return imnCusName;
    }

    public void setImnCusName(String imnCusName) {
        this.imnCusName = imnCusName;
    }

    public String getCollName() {
        return collName;
    }

    public void setCollName(String collName) {
        this.collName = collName;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getAccptr() {
        return accptr;
    }

    public void setAccptr(String accptr) {
        this.accptr = accptr;
    }

    @Override
    public String toString() {
        return "List{" +
                "eBillNo='" + eBillNo + '\'' +
                ", drftAmt=" + drftAmt +
                ", eBillCurType='" + eBillCurType + '\'' +
                ", isseDate='" + isseDate + '\'' +
                ", isseEndDate='" + isseEndDate + '\'' +
                ", imnManCusId='" + imnManCusId + '\'' +
                ", imnCusName='" + imnCusName + '\'' +
                ", collName='" + collName + '\'' +
                ", aorgNo='" + aorgNo + '\'' +
                ", accptr='" + accptr + '\'' +
                '}';
    }
}
