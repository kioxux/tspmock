package cn.com.yusys.yusp.dto.server.xddb0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：信贷押品状态查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0006DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "distco")
    private String distco;//区县代码
    @JsonProperty(value = "certnu")
    private String certnu;//不动产权证书号
    @JsonProperty(value = "mocenu")
    private String mocenu;//不动产登记证明号



    public String getDistco() {
        return distco;
    }

    public void setDistco(String distco) {
        this.distco = distco;
    }

    public String getCertnu() {
        return certnu;
    }

    public void setCertnu(String certnu) {
        this.certnu = certnu;
    }

    public String getMocenu() {
        return mocenu;
    }

    public void setMocenu(String mocenu) {
        this.mocenu = mocenu;
    }

	@Override
	public String toString() {
		return "Xddb0006DataReqDto{" +
				"distco='" + distco + '\'' +
				", certnu='" + certnu + '\'' +
				", mocenu='" + mocenu + '\'' +
				'}';
	}
}
