package cn.com.yusys.yusp.dto.server.xdsx0015.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：已批复的授信申请信息、押品信息以及合同信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0015DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "db_cus_name")
    private String db_cus_name;//担保合同客户名

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getDb_cus_name() {
        return db_cus_name;
    }

    public void setDb_cus_name(String db_cus_name) {
        this.db_cus_name = db_cus_name;
    }

    @Override
    public String toString() {
        return "Xdsx0015DataRespDto{" +
                "cus_id='" + cus_id + '\'' +
                ", db_cus_name='" + db_cus_name + '\'' +
                '}';
    }
}
