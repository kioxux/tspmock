package cn.com.yusys.yusp.dto.server.xdht0037.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：大家e贷、乐悠金根据客户号查询房贷首付款比例
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0037DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "firstpayPerc")
	private BigDecimal firstpayPerc;//首付款比例

	public BigDecimal getFirstpayPerc() {
		return firstpayPerc;
	}

	public void setFirstpayPerc(BigDecimal firstpayPerc) {
		this.firstpayPerc = firstpayPerc;
	}

	@Override
	public String toString() {
		return "Xdht0037DataRespDto{" +
				"firstpayPerc=" + firstpayPerc +
				'}';
	}
}
