package cn.com.yusys.yusp.dto.server.xddb0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：双录流水号与押品编号关系维护
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0009DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarNo")
    private String guarNo;//抵押物编号
    @JsonProperty(value = "imagePk1SerNo")
    private String imagePk1SerNo;//影像主键流水号
    @JsonProperty(value = "tranType")
    private String tranType;//交易类型

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getImagePk1SerNo() {
        return imagePk1SerNo;
    }

    public void setImagePk1SerNo(String imagePk1SerNo) {
        this.imagePk1SerNo = imagePk1SerNo;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    @Override
    public String toString() {
        return "Xddb0009DataReqDto{" +
                "guarNo='" + guarNo + '\'' +
                ", imagePk1SerNo='" + imagePk1SerNo + '\'' +
                ", tranType='" + tranType + '\'' +
                '}';
    }
}
