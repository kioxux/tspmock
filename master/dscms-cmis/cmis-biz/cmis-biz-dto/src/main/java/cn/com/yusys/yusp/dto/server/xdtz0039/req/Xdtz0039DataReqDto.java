package cn.com.yusys.yusp.dto.server.xdtz0039.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据企业名称查询申请企业在本行是否存在当前逾期贷款
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0039DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "certCodes")
    private String certCodes;

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCodes() {
        return certCodes;
    }

    public void setCertCodes(String certCodes) {
        this.certCodes = certCodes;
    }

    @Override
    public String toString() {
        return "Xdtz0039DataReqDto{" +
                "cusName='" + cusName + '\'' +
                ", certCodes='" + certCodes + '\'' +
                '}';
    }
}