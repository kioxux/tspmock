package cn.com.yusys.yusp.dto.server.xdxw0082.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：查询上一笔抵押贷款的余额接口（增享贷）
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0082DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cus_id")
    @NotNull(message = "核心客户号cus_id不能为空")
    @NotEmpty(message = "核心客户号cus_id不能为空")
    private String cus_id;//客户号

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    @Override
    public String toString() {
        return "Xdxw0080DataReqDto{" +
                "cus_id='" + cus_id + '\'' +
                '}';
    }
}
