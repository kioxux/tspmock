package cn.com.yusys.yusp.dto.server.xdtz0017.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：票据更换（通知信贷更改票据暂用额度台账）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0017DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "oldDrftNo")
    @NotNull(message = "旧票号oldDrftNo不能为空")
    @NotEmpty(message = "旧票号oldDrftNo不能为空")
    private String oldDrftNo;//旧票号
    @JsonProperty(value = "newDrftNo")
    @NotNull(message = "新票号newDrftNo不能为空")
    @NotEmpty(message = "新票号newDrftNo不能为空")
    private String newDrftNo;//新票号
    @JsonProperty(value = "pyeeName")
    @NotNull(message = "收款人名称pyeeName不能为空")
    @NotEmpty(message = "收款人名称pyeeName不能为空")
    private String pyeeName;//收款人名称
    @JsonProperty(value = "pyeeAcctbNo")
    @NotNull(message = "收款人开户行行号pyeeAcctbNo不能为空")
    @NotEmpty(message = "收款人开户行行号pyeeAcctbNo不能为空")
    private String pyeeAcctbNo;//收款人开户行行号
    @JsonProperty(value = "pyeeAcctNo")
    @NotNull(message = "收款人账号pyeeAcctNo不能为空")
    @NotEmpty(message = "收款人账号pyeeAcctNo不能为空")
    private String pyeeAcctNo;//收款人账号

    public String getOldDrftNo() {
        return oldDrftNo;
    }

    public void setOldDrftNo(String oldDrftNo) {
        this.oldDrftNo = oldDrftNo;
    }

    public String getNewDrftNo() {
        return newDrftNo;
    }

    public void setNewDrftNo(String newDrftNo) {
        this.newDrftNo = newDrftNo;
    }

    public String getPyeeName() {
        return pyeeName;
    }

    public void setPyeeName(String pyeeName) {
        this.pyeeName = pyeeName;
    }

    public String getPyeeAcctbNo() {
        return pyeeAcctbNo;
    }

    public void setPyeeAcctbNo(String pyeeAcctbNo) {
        this.pyeeAcctbNo = pyeeAcctbNo;
    }

    public String getPyeeAcctNo() {
        return pyeeAcctNo;
    }

    public void setPyeeAcctNo(String pyeeAcctNo) {
        this.pyeeAcctNo = pyeeAcctNo;
    }

    @Override
    public String toString() {
        return "Xdtz0017DataReqDto{" +
                "oldDrftNo='" + oldDrftNo + '\'' +
                ", newDrftNo='" + newDrftNo + '\'' +
                ", pyeeName='" + pyeeName + '\'' +
                ", pyeeAcctbNo='" + pyeeAcctbNo + '\'' +
                ", pyeeAcctNo='" + pyeeAcctNo + '\'' +
                '}';
    }
}
