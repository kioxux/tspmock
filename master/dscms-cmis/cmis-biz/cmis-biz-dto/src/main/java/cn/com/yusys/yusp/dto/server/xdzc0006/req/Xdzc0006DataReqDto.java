package cn.com.yusys.yusp.dto.server.xdzc0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：资产池出池校验接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0006DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//协议编号
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdzc0006.req.List> list;

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdzc0006DataReqDto{" +
                "contNo='" + contNo + '\'' +
                ", list=" + list +
                '}';
    }
}
