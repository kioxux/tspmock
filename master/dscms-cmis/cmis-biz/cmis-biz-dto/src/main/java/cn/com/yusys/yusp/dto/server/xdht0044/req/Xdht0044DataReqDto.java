package cn.com.yusys.yusp.dto.server.xdht0044.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：房群客户查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0044DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "isExcludeSettl")
    private String isExcludeSettl;//是否排除已结清
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getIsExcludeSettl() {
        return isExcludeSettl;
    }

    public void setIsExcludeSettl(String isExcludeSettl) {
        this.isExcludeSettl = isExcludeSettl;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    @Override
    public String toString() {
        return "Xdht0044DataReqDto{" +
                "certNo='" + certNo + '\'' +
                ", isExcludeSettl='" + isExcludeSettl + '\'' +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}
