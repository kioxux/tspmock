package cn.com.yusys.yusp.dto.server.xdtz0054.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/3 21:24
 * @since 2021/6/3 21:24
 */
@JsonPropertyOrder(alphabetic = true)
public class PvpInfoList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "pvpSerno")
    private String pvpSerno;//出账流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "managerId")
    private String managerId;//管理客户经理工号
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//管理机构号
    @JsonProperty(value = "isOnline")
    private String isOnline;//是否线上
    @JsonProperty(value = "acctsvcrNo")
    private String acctsvcrNo;
    @JsonProperty(value = "acctsvcrName")
    private String acctsvcrName;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(String isOnline) {
        this.isOnline = isOnline;
    }

    public String getAcctsvcrNo() {
        return acctsvcrNo;
    }

    public void setAcctsvcrNo(String acctsvcrNo) {
        this.acctsvcrNo = acctsvcrNo;
    }

    public String getAcctsvcrName() {
        return acctsvcrName;
    }

    public void setAcctsvcrName(String acctsvcrName) {
        this.acctsvcrName = acctsvcrName;
    }

    @Override
    public String toString() {
        return "ListLoan{" +
                "billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", pvpSerno='" + pvpSerno + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName=" + cusName + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", isOnline='" + isOnline + '\'' +
                ", acctsvcrNo='" + acctsvcrNo + '\'' +
                ", acctsvcrName='" + acctsvcrName + '\'' +
                '}';
    }
}
