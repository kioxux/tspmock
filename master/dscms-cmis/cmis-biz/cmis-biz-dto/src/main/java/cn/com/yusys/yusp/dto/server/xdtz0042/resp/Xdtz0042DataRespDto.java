package cn.com.yusys.yusp.dto.server.xdtz0042.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：申请人在本行当前逾期贷款数量
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0042DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalNum")
    private Integer totalNum;//数量

    public Integer getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(Integer totalNum) {
        this.totalNum = totalNum;
    }

    public Xdtz0042DataRespDto() {
    }

    public Xdtz0042DataRespDto(Integer totalNum) {
        this.totalNum = totalNum;
    }

    @Override
    public String toString() {
        return "Xdtz0042DataRespDto{" +
                "totalNum=" + totalNum +
                '}';
    }
}
