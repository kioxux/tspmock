package cn.com.yusys.yusp.dto.server.xdxw0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：批复信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0007DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//调查流水号
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "assureMeans")
    private String assureMeans;//担保方式
    @JsonProperty(value = "apprAmt")
    private BigDecimal apprAmt;//批复金额
    @JsonProperty(value = "apprStartDate")
    private String apprStartDate;//批复起始日
    @JsonProperty(value = "apprEndDate")
    private String apprEndDate;//批复到期日
    @JsonProperty(value = "rate")
    private BigDecimal rate;//利率
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式

    public Xdxw0007DataRespDto(String indgtSerno, String contType, String assureMeans, BigDecimal apprAmt, String apprStartDate, String apprEndDate, BigDecimal rate, String repayType) {
        this.indgtSerno = indgtSerno;
        this.contType = contType;
        this.assureMeans = assureMeans;
        this.apprAmt = apprAmt;
        this.apprStartDate = apprStartDate;
        this.apprEndDate = apprEndDate;
        this.rate = rate;
        this.repayType = repayType;
    }

    public Xdxw0007DataRespDto() {
    }

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public BigDecimal getApprAmt() {
        return apprAmt;
    }

    public void setApprAmt(BigDecimal apprAmt) {
        this.apprAmt = apprAmt;
    }

    public String getApprStartDate() {
        return apprStartDate;
    }

    public void setApprStartDate(String apprStartDate) {
        this.apprStartDate = apprStartDate;
    }

    public String getApprEndDate() {
        return apprEndDate;
    }

    public void setApprEndDate(String apprEndDate) {
        this.apprEndDate = apprEndDate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    @Override
    public String toString() {
        return "Xdxw0007DataRespDto{" +
                "indgtSerno='" + indgtSerno + '\'' +
                ", contType='" + contType + '\'' +
                ", assureMeans='" + assureMeans + '\'' +
                ", apprAmt='" + apprAmt + '\'' +
                ", apprStartDate='" + apprStartDate + '\'' +
                ", apprEndDate='" + apprEndDate + '\'' +
                ", rate='" + rate + '\'' +
                ", repayType='" + repayType + '\'' +
                '}';
    }
}
