package cn.com.yusys.yusp.dto.server.xddh0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：主动还款申请记录列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0003DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "managerId")
    private String managerId;//主管客户经理

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Xddh0003DataReqDto{" +
                "billNo='" + billNo + '\'' +
                ", managerId='" + managerId + '\'' +
                '}';
    }
}
