package cn.com.yusys.yusp.dto.server.xdzx0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询征信业务流水号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzx0001DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "applySerno")
    private String applySerno;//业务唯一编号

    public String getApplySerno() {
        return applySerno;
    }

    public void setApplySerno(String applySerno) {
        this.applySerno = applySerno;
    }

    @Override
    public String toString() {
        return "Xdzx0001DataReqDto{" +
                "applySerno='" + applySerno + '\'' +
                '}';
    }
}
