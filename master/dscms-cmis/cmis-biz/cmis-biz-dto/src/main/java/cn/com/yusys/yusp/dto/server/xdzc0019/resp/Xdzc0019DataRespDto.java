package cn.com.yusys.yusp.dto.server.xdzc0019.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Data：历史出入池记录查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0019DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalSize")
    private String totalSize;//总记录数
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.server.xdzc0019.resp.List> list;

    public String getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(String totalSize) {
        this.totalSize = totalSize;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdzc0019DataRespDto{" +
                "totalSize='" + totalSize + '\'' +
                ", list=" + list +
                '}';
    }
}
