package cn.com.yusys.yusp.dto.server.xdxw0061.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：通过无还本续贷调查表编号查询配偶核心客户号
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0061DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cusNo")
    private String cusNo;//客户代码



    public String getCusNo() {
        return cusNo;
    }

    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }

	@Override
	public String toString() {
		return "Xdxw0061DataRespDto{" +
				"cusNo='" + cusNo + '\'' +
				'}';
	}
}
