package cn.com.yusys.yusp.dto.server.xdcz0028.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author YD
 * @version 0.1
 * @date 2021/6/15 21:24
 * @since 2021/6/15 21:24
 */
@JsonPropertyOrder(alphabetic = true)
public class PvpCheckList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "enterAccount")
    private String enterAccount;
    @JsonProperty(value = "parceName")
    private String parceName;
    @JsonProperty(value = "bookingId")
    private String bookingId;
    @JsonProperty(value = "cifArea")
    private String cifArea;
    @JsonProperty(value = "housingTotal")
    private String housingTotal;
    @JsonProperty(value = "housingZone")
    private String housingZone;
    @JsonProperty(value = "gethomeNum1")
    private String gethomeNum1;
    @JsonProperty(value = "housingContId")
    private String housingContId;

    public String getEnterAccount() {
        return enterAccount;
    }

    public void setEnterAccount(String enterAccount) {
        this.enterAccount = enterAccount;
    }

    public String getParceName() {
        return parceName;
    }

    public void setParceName(String parceName) {
        this.parceName = parceName;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getCifArea() {
        return cifArea;
    }

    public void setCifArea(String cifArea) {
        this.cifArea = cifArea;
    }

    public String getHousingTotal() {
        return housingTotal;
    }

    public void setHousingTotal(String housingTotal) {
        this.housingTotal = housingTotal;
    }

    public String getHousingZone() {
        return housingZone;
    }

    public void setHousingZone(String housingZone) {
        this.housingZone = housingZone;
    }

    public String getGethomeNum1() {
        return gethomeNum1;
    }

    public void setGethomeNum1(String gethomeNum1) {
        this.gethomeNum1 = gethomeNum1;
    }

    public String getHousingContId() {
        return housingContId;
    }

    public void setHousingContId(String housingContId) {
        this.housingContId = housingContId;
    }

    @Override
    public String toString() {
        return "PvpCheckList{" +
                ", enterAccount='" + enterAccount + '\'' +
                ", parceName='" + parceName + '\'' +
                ", bookingId='" + bookingId + '\'' +
                ", cifArea='" + cifArea + '\'' +
                ", housingTotal='" + housingTotal + '\'' +
                ", housingZone='" + housingZone + '\'' +
                ", gethomeNum1='" + gethomeNum1 + '\'' +
                ", housingContId='" + housingContId + '\'' +
                '}';
    }
}
