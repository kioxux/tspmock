package cn.com.yusys.yusp.dto.server.xdxw0038.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "apply_type")
    private String apply_type;//申请类型
    @JsonProperty(value = "cus_relay_people")
    private String cus_relay_people;//评审人员
    @JsonProperty(value = "appr_status")
    private String appr_status;//状态
    @JsonProperty(value = "manager_name")
    private String manager_name;//管户经理名称
    @JsonProperty(value = "org_name")
    private String org_name;//机构名称
    @JsonProperty(value = "cus_fy_type")
    private String cus_fy_type;//复议类型
    @JsonProperty(value = "cus_cre_type")
    private String cus_cre_type;//业务类型
    @JsonProperty(value = "prd_name")
    private String prd_name;//产品名称
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cert_type")
    private String cert_type;//证件类型
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "now_dq_amount")
    private BigDecimal now_dq_amount;//申请金额
    @JsonProperty(value = "now_dq_bit")
    private BigDecimal now_dq_bit;//申请利率
    @JsonProperty(value = "now_dq_term")
    private BigDecimal now_dq_term;//申请期限
    @JsonProperty(value = "now_dq_ass")
    private String now_dq_ass;//申请担保方式
    @JsonProperty(value = "input_date")
    private String input_date;//申请日期

    public String getApply_type() {
        return apply_type;
    }

    public void setApply_type(String apply_type) {
        this.apply_type = apply_type;
    }

    public String getCus_relay_people() {
        return cus_relay_people;
    }

    public void setCus_relay_people(String cus_relay_people) {
        this.cus_relay_people = cus_relay_people;
    }

    public String getAppr_status() {
        return appr_status;
    }

    public void setAppr_status(String appr_status) {
        this.appr_status = appr_status;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getCus_fy_type() {
        return cus_fy_type;
    }

    public void setCus_fy_type(String cus_fy_type) {
        this.cus_fy_type = cus_fy_type;
    }

    public String getCus_cre_type() {
        return cus_cre_type;
    }

    public void setCus_cre_type(String cus_cre_type) {
        this.cus_cre_type = cus_cre_type;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public BigDecimal getNow_dq_amount() {
        return now_dq_amount;
    }

    public void setNow_dq_amount(BigDecimal now_dq_amount) {
        this.now_dq_amount = now_dq_amount;
    }

    public BigDecimal getNow_dq_bit() {
        return now_dq_bit;
    }

    public void setNow_dq_bit(BigDecimal now_dq_bit) {
        this.now_dq_bit = now_dq_bit;
    }

    public BigDecimal getNow_dq_term() {
        return now_dq_term;
    }

    public void setNow_dq_term(BigDecimal now_dq_term) {
        this.now_dq_term = now_dq_term;
    }

    public String getNow_dq_ass() {
        return now_dq_ass;
    }

    public void setNow_dq_ass(String now_dq_ass) {
        this.now_dq_ass = now_dq_ass;
    }

    public String getInput_date() {
        return input_date;
    }

    public void setInput_date(String input_date) {
        this.input_date = input_date;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                ", apply_type='" + apply_type + '\'' +
                ", cus_relay_people='" + cus_relay_people + '\'' +
                ", appr_status='" + appr_status + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", org_name='" + org_name + '\'' +
                ", cus_fy_type='" + cus_fy_type + '\'' +
                ", cus_cre_type='" + cus_cre_type + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", now_dq_amount=" + now_dq_amount +
                ", now_dq_bit=" + now_dq_bit +
                ", now_dq_term=" + now_dq_term +
                ", now_dq_ass='" + now_dq_ass + '\'' +
                ", input_date='" + input_date + '\'' +
                '}';
    }
}
