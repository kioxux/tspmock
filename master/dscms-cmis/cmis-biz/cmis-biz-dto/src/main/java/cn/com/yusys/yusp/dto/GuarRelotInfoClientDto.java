package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.List;

public class GuarRelotInfoClientDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String certiRecordId;//权证编号
    private String guarCusName;//权属人
    private String certiCatalog; //权证类型
    private List<String> guarNoList;//押品编号集合

    public String getCertiRecordId() {
        return certiRecordId;
    }

    public void setCertiRecordId(String certiRecordId) {
        this.certiRecordId = certiRecordId;
    }

    public String getGuarCusName() {
        return guarCusName;
    }

    public void setGuarCusName(String guarCusName) {
        this.guarCusName = guarCusName;
    }

    public List<String> getGuarNoList() {
        return guarNoList;
    }

    public void setGuarNoList(List<String> guarNoList) {
        this.guarNoList = guarNoList;
    }

    public String getCertiCatalog() {
        return certiCatalog;
    }

    public void setCertiCatalog(String certiCatalog) {
        this.certiCatalog = certiCatalog;
    }
}
