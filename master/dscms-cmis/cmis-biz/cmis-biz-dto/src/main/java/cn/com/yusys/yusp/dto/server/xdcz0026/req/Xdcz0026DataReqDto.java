package cn.com.yusys.yusp.dto.server.xdcz0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：风控调用信贷放款
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0026DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "loan_cont_no")
    private String loan_cont_no;//借款合同号
    @JsonProperty(value = "cn_cont_no")
    private String cn_cont_no;//中文合同号
    @JsonProperty(value = "certid")
    private String certid;//客户身份证号
    @JsonProperty(value = "biz_type")
    private String biz_type;//业务类型
    @JsonProperty(value = "apply_amount")
    private String apply_amount;//合同金额
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//合同开始日
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//合同到期日
    @JsonProperty(value = "rate")
    private String rate;//年利率
    @JsonProperty(value = "jzrate")
    private String jzrate;//基准利率
    @JsonProperty(value = "yzrate")
    private String yzrate;//逾期利率
    @JsonProperty(value = "manager_br_id")
    private String manager_br_id;//管户机构
    @JsonProperty(value = "bill_no")
    private String bill_no;//借据号
    @JsonProperty(value = "loan_amount")
    private String loan_amount;//出账金额
    @JsonProperty(value = "loan_start_date1")
    private String loan_start_date1;//借据起始日期
    @JsonProperty(value = "loan_term")
    private String loan_term;//借据期限
    @JsonProperty(value = "loan_end_date1")
    private String loan_end_date1;//借据到期日期
    @JsonProperty(value = "loan_paym_mtd")
    private String loan_paym_mtd;//还款方式
    @JsonProperty(value = "repayment_account")
    private String repayment_account;//借款人账号
    @JsonProperty(value = "repayment_account_tb")
    private String repayment_account_tb;//替补借款人账号
    @JsonProperty(value = "recommend_id")
    private String recommend_id;//推荐客户经理号
    @JsonProperty(value = "fina_br_id")
    private String fina_br_id;//账务机构
    @JsonProperty(value = "loan_direction")
    private String loan_direction;//贷款投向
    @JsonProperty(value = "account_class")
    private String account_class;//科目
    @JsonProperty(value = "zhifu_type")
    private String zhifu_type;//支付方式
    @JsonProperty(value = "shoutuo_account")
    private String shoutuo_account;//受托支付账号
    @JsonProperty(value = "shoutuo_name")
    private String shoutuo_name;//交易对手名称
    @JsonProperty(value = "managerId")
    private String managerId;//责任人
    @JsonProperty(value = "cust_manager_id")
    private String cust_manager_id;//客户经理号
    @JsonProperty(value = "cust_manager_name")
    private String cust_manager_name;//客户经理名称
    @JsonProperty(value = "is_self_bank_acc")
    private String is_self_bank_acc;//对方账号种类
    @JsonProperty(value = "khh_no")
    private String khh_no;//对方账号开户行号
    @JsonProperty(value = "khh_name")
    private String khh_name;//对方账号开户行名
    @JsonProperty(value = "trade_partner_acc")
    private String trade_partner_acc;//对方账号
    @JsonProperty(value = "trade_partner_nam")
    private String trade_partner_nam;//对方账户名
    @JsonProperty(value = "is_free_intere")
    private String is_free_intere;//是否免息
    @JsonProperty(value = "free_intere_days")
    private Integer free_intere_days;//免息天数
    @JsonProperty(value = "discount_code")
    private String discount_code;//优惠券编号
    @JsonProperty(value = "accImageNo")
    private String accImageNo;//借据影像编号
    @JsonProperty(value = "accAuthNo")
    private String accAuthNo;//额度支用授权书影像编号
    @JsonProperty(value = "accLoanDirect")
    private String accLoanDirect;//消费贷款用途承诺书影像编号
    @JsonProperty(value = "agriFlg")
    private String agriFlg;//是否农户
    @JsonProperty(value = "manager_id")
    private String manager_id;//责任人

    public String getLoan_cont_no() {
        return loan_cont_no;
    }

    public void setLoan_cont_no(String loan_cont_no) {
        this.loan_cont_no = loan_cont_no;
    }

    public String getCn_cont_no() {
        return cn_cont_no;
    }

    public void setCn_cont_no(String cn_cont_no) {
        this.cn_cont_no = cn_cont_no;
    }

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(String apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getJzrate() {
        return jzrate;
    }

    public void setJzrate(String jzrate) {
        this.jzrate = jzrate;
    }

    public String getYzrate() {
        return yzrate;
    }

    public void setYzrate(String yzrate) {
        this.yzrate = yzrate;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(String loan_amount) {
        this.loan_amount = loan_amount;
    }

    public String getLoan_start_date1() {
        return loan_start_date1;
    }

    public void setLoan_start_date1(String loan_start_date1) {
        this.loan_start_date1 = loan_start_date1;
    }

    public String getLoan_term() {
        return loan_term;
    }

    public void setLoan_term(String loan_term) {
        this.loan_term = loan_term;
    }

    public String getLoan_end_date1() {
        return loan_end_date1;
    }

    public void setLoan_end_date1(String loan_end_date1) {
        this.loan_end_date1 = loan_end_date1;
    }

    public String getLoan_paym_mtd() {
        return loan_paym_mtd;
    }

    public void setLoan_paym_mtd(String loan_paym_mtd) {
        this.loan_paym_mtd = loan_paym_mtd;
    }

    public String getRepayment_account() {
        return repayment_account;
    }

    public void setRepayment_account(String repayment_account) {
        this.repayment_account = repayment_account;
    }

    public String getRepayment_account_tb() {
        return repayment_account_tb;
    }

    public void setRepayment_account_tb(String repayment_account_tb) {
        this.repayment_account_tb = repayment_account_tb;
    }

    public String getRecommend_id() {
        return recommend_id;
    }

    public void setRecommend_id(String recommend_id) {
        this.recommend_id = recommend_id;
    }

    public String getFina_br_id() {
        return fina_br_id;
    }

    public void setFina_br_id(String fina_br_id) {
        this.fina_br_id = fina_br_id;
    }

    public String getLoan_direction() {
        return loan_direction;
    }

    public void setLoan_direction(String loan_direction) {
        this.loan_direction = loan_direction;
    }

    public String getAccount_class() {
        return account_class;
    }

    public void setAccount_class(String account_class) {
        this.account_class = account_class;
    }

    public String getZhifu_type() {
        return zhifu_type;
    }

    public void setZhifu_type(String zhifu_type) {
        this.zhifu_type = zhifu_type;
    }

    public String getShoutuo_account() {
        return shoutuo_account;
    }

    public void setShoutuo_account(String shoutuo_account) {
        this.shoutuo_account = shoutuo_account;
    }

    public String getShoutuo_name() {
        return shoutuo_name;
    }

    public void setShoutuo_name(String shoutuo_name) {
        this.shoutuo_name = shoutuo_name;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getCust_manager_id() {
        return cust_manager_id;
    }

    public void setCust_manager_id(String cust_manager_id) {
        this.cust_manager_id = cust_manager_id;
    }

    public String getCust_manager_name() {
        return cust_manager_name;
    }

    public void setCust_manager_name(String cust_manager_name) {
        this.cust_manager_name = cust_manager_name;
    }

    public String getIs_self_bank_acc() {
        return is_self_bank_acc;
    }

    public void setIs_self_bank_acc(String is_self_bank_acc) {
        this.is_self_bank_acc = is_self_bank_acc;
    }

    public String getKhh_no() {
        return khh_no;
    }

    public void setKhh_no(String khh_no) {
        this.khh_no = khh_no;
    }

    public String getKhh_name() {
        return khh_name;
    }

    public void setKhh_name(String khh_name) {
        this.khh_name = khh_name;
    }

    public String getTrade_partner_acc() {
        return trade_partner_acc;
    }

    public void setTrade_partner_acc(String trade_partner_acc) {
        this.trade_partner_acc = trade_partner_acc;
    }

    public String getTrade_partner_nam() {
        return trade_partner_nam;
    }

    public void setTrade_partner_nam(String trade_partner_nam) {
        this.trade_partner_nam = trade_partner_nam;
    }

    public String getIs_free_intere() {
        return is_free_intere;
    }

    public void setIs_free_intere(String is_free_intere) {
        this.is_free_intere = is_free_intere;
    }

    public Integer getFree_intere_days() {
        return free_intere_days;
    }

    public void setFree_intere_days(Integer free_intere_days) {
        this.free_intere_days = free_intere_days;
    }

    public String getDiscount_code() {
        return discount_code;
    }

    public void setDiscount_code(String discount_code) {
        this.discount_code = discount_code;
    }

    public String getAccImageNo() {
        return accImageNo;
    }

    public void setAccImageNo(String accImageNo) {
        this.accImageNo = accImageNo;
    }

    public String getAccAuthNo() {
        return accAuthNo;
    }

    public void setAccAuthNo(String accAuthNo) {
        this.accAuthNo = accAuthNo;
    }

    public String getAccLoanDirect() {
        return accLoanDirect;
    }

    public void setAccLoanDirect(String accLoanDirect) {
        this.accLoanDirect = accLoanDirect;
    }

    public String getAgriFlg() {
        return agriFlg;
    }

    public void setAgriFlg(String agriFlg) {
        this.agriFlg = agriFlg;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    @Override
    public String toString() {
        return "Xdcz0026DataReqDto{" +
                "loan_cont_no='" + loan_cont_no + '\'' +
                ", cn_cont_no='" + cn_cont_no + '\'' +
                ", certid='" + certid + '\'' +
                ", biz_type='" + biz_type + '\'' +
                ", apply_amount='" + apply_amount + '\'' +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", rate='" + rate + '\'' +
                ", jzrate='" + jzrate + '\'' +
                ", yzrate='" + yzrate + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", bill_no='" + bill_no + '\'' +
                ", loan_amount='" + loan_amount + '\'' +
                ", loan_start_date1='" + loan_start_date1 + '\'' +
                ", loan_term='" + loan_term + '\'' +
                ", loan_end_date1='" + loan_end_date1 + '\'' +
                ", loan_paym_mtd='" + loan_paym_mtd + '\'' +
                ", repayment_account='" + repayment_account + '\'' +
                ", repayment_account_tb='" + repayment_account_tb + '\'' +
                ", recommend_id='" + recommend_id + '\'' +
                ", fina_br_id='" + fina_br_id + '\'' +
                ", loan_direction='" + loan_direction + '\'' +
                ", account_class='" + account_class + '\'' +
                ", zhifu_type='" + zhifu_type + '\'' +
                ", shoutuo_account='" + shoutuo_account + '\'' +
                ", shoutuo_name='" + shoutuo_name + '\'' +
                ", cust_manager_id='" + cust_manager_id + '\'' +
                ", cust_manager_name='" + cust_manager_name + '\'' +
                ", is_self_bank_acc='" + is_self_bank_acc + '\'' +
                ", khh_no='" + khh_no + '\'' +
                ", khh_name='" + khh_name + '\'' +
                ", trade_partner_acc='" + trade_partner_acc + '\'' +
                ", trade_partner_nam='" + trade_partner_nam + '\'' +
                ", is_free_intere='" + is_free_intere + '\'' +
                ", free_intere_days='" + free_intere_days + '\'' +
                ", discount_code='" + discount_code + '\'' +
                ", accImageNo='" + accImageNo + '\'' +
                ", accAuthNo='" + accAuthNo + '\'' +
                ", accLoanDirect='" + accLoanDirect + '\'' +
                ", agriFlg='" + agriFlg + '\'' +
                ", manager_id='" + manager_id + '\'' +
                '}';
    }
}
