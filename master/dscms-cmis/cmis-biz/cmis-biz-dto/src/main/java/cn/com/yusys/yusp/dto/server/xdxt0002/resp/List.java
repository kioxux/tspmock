package cn.com.yusys.yusp.dto.server.xdxt0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 21:31:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 21:31
 * @since 2021/5/24 21:31
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理工号

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "List{" +
                "managerId='" + managerId + '\'' +
                '}';
    }
}
