package cn.com.yusys.yusp.dto.server.xdcz0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：企业网银查询影像补录批次
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "batchNo")
    private String batchNo;//批次号
    @JsonProperty(value = "videoNo")
    private String videoNo;//影像编号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getVideoNo() {
        return videoNo;
    }

    public void setVideoNo(String videoNo) {
        this.videoNo = videoNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    @Override
    public String toString() {
        return "List{" +
                "batchNo='" + batchNo + '\'' +
                ", videoNo='" + videoNo + '\'' +
                ", contNo='" + contNo + '\'' +
                '}';
    }
}