package cn.com.yusys.yusp.dto.server.xddb0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Data：提醒任务处理结果实时同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0004DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "reqList")
    private java.util.List<ReqList> reqList;

    public List<ReqList> getReqList() {
        return reqList;
    }

    public void setReqList(List<ReqList> reqList) {
        this.reqList = reqList;
    }

    @Override
    public String toString() {
        return "Xddb0004DataReqDto{" +
                "reqList=" + reqList +
                '}';
    }
}
