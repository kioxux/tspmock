package cn.com.yusys.yusp.dto.server.xddb0012.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Data：抵押登记获取押品信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0012DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "collid")
    private String collid;//押品编号
    @JsonProperty(value = "moname")
    private String moname;//抵押权人-姓名/名称
    @JsonProperty(value = "dotyco")
    private String dotyco;//抵押权人-证件类型-代码
    @JsonProperty(value = "dotyna")
    private String dotyna;//抵押权人-证件类型-名称
    @JsonProperty(value = "modomu")
    private String modomu;//抵押权人-证件号
    @JsonProperty(value = "monalr")
    private String monalr;//抵押权人-法人/负责人
    @JsonProperty(value = "mophon")
    private String mophon;//抵押权人-电话
    @JsonProperty(value = "moagna")
    private String moagna;//抵押权人代理人-姓名/名称
    @JsonProperty(value = "moagtc")
    private String moagtc;//抵押权人代理人-证件类型-代码
    @JsonProperty(value = "moagtn")
    private String moagtn;//抵押权人代理人-证件类型-名称
    @JsonProperty(value = "moagdn")
    private String moagdn;//抵押权人代理人-证件号
    @JsonProperty(value = "moagph")
    private String moagph;//抵押权人代理人-电话
	@JsonProperty(value = "mortgagorsList")
	private java.util.List<MortgagorsList> mortgagorsList;
    @JsonProperty(value = "moorna")
    private String moorna;//抵押人代理人-姓名/名称
    @JsonProperty(value = "moordc")
    private String moordc;//抵押人代理人-证件类型-代码
    @JsonProperty(value = "moordn")
    private String moordn;//抵押人代理人-证件类型-名称
    @JsonProperty(value = "moordu")
    private String moordu;//抵押人代理人-证件号
    @JsonProperty(value = "moorph")
    private String moorph;//抵押人代理人-电话
    @JsonProperty(value = "reeidc")
    private String reeidc;//不动产信息-所属区县-代码
    @JsonProperty(value = "reeidn")
    private String reeidn;//不动产信息-所属区县-名称
    @JsonProperty(value = "reeilo")
    private String reeilo;//不动产信息-坐落
    @JsonProperty(value = "reiheu")
    private String reiheu;//不动产信息-房屋-不动产单元号
    @JsonProperty(value = "reihcn")
    private String reihcn;//不动产信息-房屋-产权证书号
    @JsonProperty(value = "reihcs")
    private String reihcs;//不动产信息-房屋-产权证书号简称
    @JsonProperty(value = "reeiha")
    private BigDecimal reeiha;//不动产信息-房屋-产权面积
    @JsonProperty(value = "reihuc")
    private String reihuc;//不动产信息-房屋-用途-代码
    @JsonProperty(value = "reihun")
    private String reihun;//不动产信息-房屋-用途-名称
    @JsonProperty(value = "reilcn")
    private String reilcn;//不动产信息-土地-证号
    @JsonProperty(value = "reeila")
    private BigDecimal reeila;//不动产信息-土地-面积
    @JsonProperty(value = "reiluc")
    private String reiluc;//不动产信息-土地-用途-代码
    @JsonProperty(value = "reilun")
    private String reilun;//不动产信息-土地-用途-名称
    @JsonProperty(value = "micenu")
    private String micenu;//抵押信息-不动产登记证明号
    @JsonProperty(value = "micono")
    private String micono;//抵押信息-抵押合同编号
    @JsonProperty(value = "micova")
    private BigDecimal micova;//抵押信息-抵押物价值(万元)
    @JsonProperty(value = "mideam")
    private BigDecimal mideam;//抵押信息-被担保债权数额(万元)
    @JsonProperty(value = "mimmco")
    private String mimmco;//抵押信息-抵押方式-代码
    @JsonProperty(value = "mimmna")
    private String mimmna;//抵押信息-抵押方式-名称
    @JsonProperty(value = "migusc")
    private String migusc;//抵押信息-担保范围
    @JsonProperty(value = "midsti")
    private String midsti;//抵押信息-债务开始时间
    @JsonProperty(value = "mideti")
    private String mideti;//抵押信息-债务结束时间
    @JsonProperty(value = "midena")
    private String midena;//抵押信息-债务人-姓名/名称
    @JsonProperty(value = "middtc")
    private String middtc;//抵押信息-债务人-证件类型-代码
    @JsonProperty(value = "middtn")
    private String middtn;//抵押信息-债务人-证件类型-名称
    @JsonProperty(value = "middnu")
    private String middnu;//抵押信息-债务人-证件号
    @JsonProperty(value = "miorde")
    private String miorde;//抵押信息-抵押顺位
    @JsonProperty(value = "michco")
    private String michco;//抵押信息-抵押变更内容
    @JsonProperty(value = "mihaga")
    private String mihaga;//抵押信息-是否包含车库
    @JsonProperty(value = "mihalo")
    private String mihalo;//抵押信息-是否包含阁楼
    @JsonProperty(value = "distbu")
    private String distbu;//交易信息-存量业务
    @JsonProperty(value = "dinumb")
    private String dinumb;//交易信息-交易编号
    @JsonProperty(value = "dicono")
    private String dicono;//交易信息-买卖合同编号
    @JsonProperty(value = "dicova")
    private BigDecimal dicova;//交易信息-合同债权金额

    public String getCollid() {
        return collid;
    }

    public void setCollid(String collid) {
        this.collid = collid;
    }

    public String getMoname() {
        return moname;
    }

    public void setMoname(String moname) {
        this.moname = moname;
    }

    public String getDotyco() {
        return dotyco;
    }

    public void setDotyco(String dotyco) {
        this.dotyco = dotyco;
    }

    public String getDotyna() {
        return dotyna;
    }

    public void setDotyna(String dotyna) {
        this.dotyna = dotyna;
    }

    public String getModomu() {
        return modomu;
    }

    public void setModomu(String modomu) {
        this.modomu = modomu;
    }

    public String getMonalr() {
        return monalr;
    }

    public void setMonalr(String monalr) {
        this.monalr = monalr;
    }

    public String getMophon() {
        return mophon;
    }

    public void setMophon(String mophon) {
        this.mophon = mophon;
    }

    public String getMoagna() {
        return moagna;
    }

    public void setMoagna(String moagna) {
        this.moagna = moagna;
    }

    public String getMoagtc() {
        return moagtc;
    }

    public void setMoagtc(String moagtc) {
        this.moagtc = moagtc;
    }

    public String getMoagtn() {
        return moagtn;
    }

    public void setMoagtn(String moagtn) {
        this.moagtn = moagtn;
    }

    public String getMoagdn() {
        return moagdn;
    }

    public void setMoagdn(String moagdn) {
        this.moagdn = moagdn;
    }

    public String getMoagph() {
        return moagph;
    }

    public void setMoagph(String moagph) {
        this.moagph = moagph;
    }

	public List<MortgagorsList> getMortgagorsList() {
		return mortgagorsList;
	}

	public void setMortgagorsList(List<MortgagorsList> mortgagorsList) {
		this.mortgagorsList = mortgagorsList;
	}

	public String getMoorna() {
        return moorna;
    }

    public void setMoorna(String moorna) {
        this.moorna = moorna;
    }

    public String getMoordc() {
        return moordc;
    }

    public void setMoordc(String moordc) {
        this.moordc = moordc;
    }

    public String getMoordn() {
        return moordn;
    }

    public void setMoordn(String moordn) {
        this.moordn = moordn;
    }

    public String getMoordu() {
        return moordu;
    }

    public void setMoordu(String moordu) {
        this.moordu = moordu;
    }

    public String getMoorph() {
        return moorph;
    }

    public void setMoorph(String moorph) {
        this.moorph = moorph;
    }

    public String getReeidc() {
        return reeidc;
    }

    public void setReeidc(String reeidc) {
        this.reeidc = reeidc;
    }

    public String getReeidn() {
        return reeidn;
    }

    public void setReeidn(String reeidn) {
        this.reeidn = reeidn;
    }

    public String getReeilo() {
        return reeilo;
    }

    public void setReeilo(String reeilo) {
        this.reeilo = reeilo;
    }

    public String getReiheu() {
        return reiheu;
    }

    public void setReiheu(String reiheu) {
        this.reiheu = reiheu;
    }

    public String getReihcn() {
        return reihcn;
    }

    public void setReihcn(String reihcn) {
        this.reihcn = reihcn;
    }

    public String getReihcs() {
        return reihcs;
    }

    public void setReihcs(String reihcs) {
        this.reihcs = reihcs;
    }

    public BigDecimal getReeiha() {
        return reeiha;
    }

    public void setReeiha(BigDecimal reeiha) {
        this.reeiha = reeiha;
    }

    public String getReihuc() {
        return reihuc;
    }

    public void setReihuc(String reihuc) {
        this.reihuc = reihuc;
    }

    public String getReihun() {
        return reihun;
    }

    public void setReihun(String reihun) {
        this.reihun = reihun;
    }

    public String getReilcn() {
        return reilcn;
    }

    public void setReilcn(String reilcn) {
        this.reilcn = reilcn;
    }

    public BigDecimal  getReeila()

    {
        return reeila;
    }

    public void setReeila(BigDecimal reeila )

    {
        this.reeila = reeila;
    }

    public String getReiluc() {
        return reiluc;
    }

    public void setReiluc(String reiluc) {
        this.reiluc = reiluc;
    }

    public String getReilun() {
        return reilun;
    }

    public void setReilun(String reilun) {
        this.reilun = reilun;
    }

    public String getMicenu() {
        return micenu;
    }

    public void setMicenu(String micenu) {
        this.micenu = micenu;
    }

    public String getMicono() {
        return micono;
    }

    public void setMicono(String micono) {
        this.micono = micono;
    }

    public BigDecimal getMicova() {
        return micova;
    }

    public void setMicova(BigDecimal micova) {
        this.micova = micova;
    }

    public BigDecimal getMideam() {
        return mideam;
    }

    public void setMideam(BigDecimal mideam) {
        this.mideam = mideam;
    }

    public String getMimmco() {
        return mimmco;
    }

    public void setMimmco(String mimmco) {
        this.mimmco = mimmco;
    }

    public String getMimmna() {
        return mimmna;
    }

    public void setMimmna(String mimmna) {
        this.mimmna = mimmna;
    }

    public String getMigusc() {
        return migusc;
    }

    public void setMigusc(String migusc) {
        this.migusc = migusc;
    }

    public String getMidsti() {
        return midsti;
    }

    public void setMidsti(String midsti) {
        this.midsti = midsti;
    }

    public String getMideti() {
        return mideti;
    }

    public void setMideti(String mideti) {
        this.mideti = mideti;
    }

    public String getMidena() {
        return midena;
    }

    public void setMidena(String midena) {
        this.midena = midena;
    }

    public String getMiddtc() {
        return middtc;
    }

    public void setMiddtc(String middtc) {
        this.middtc = middtc;
    }

    public String getMiddtn() {
        return middtn;
    }

    public void setMiddtn(String middtn) {
        this.middtn = middtn;
    }

    public String getMiddnu() {
        return middnu;
    }

    public void setMiddnu(String middnu) {
        this.middnu = middnu;
    }

    public String getMiorde() {
        return miorde;
    }

    public void setMiorde(String miorde) {
        this.miorde = miorde;
    }

    public String getMichco() {
        return michco;
    }

    public void setMichco(String michco) {
        this.michco = michco;
    }

    public String getMihaga() {
        return mihaga;
    }

    public void setMihaga(String mihaga) {
        this.mihaga = mihaga;
    }

    public String getMihalo() {
        return mihalo;
    }

    public void setMihalo(String mihalo) {
        this.mihalo = mihalo;
    }

    public String getDistbu() {
        return distbu;
    }

    public void setDistbu(String distbu) {
        this.distbu = distbu;
    }

    public String getDinumb() {
        return dinumb;
    }

    public void setDinumb(String dinumb) {
        this.dinumb = dinumb;
    }

    public String getDicono() {
        return dicono;
    }

    public void setDicono(String dicono) {
        this.dicono = dicono;
    }

    public BigDecimal getDicova() {
        return dicova;
    }

    public void setDicova(BigDecimal dicova) {
        this.dicova = dicova;
    }

	@Override
	public String toString() {
		return "Xddb0012DataRespDto{" +
				"collid='" + collid + '\'' +
				", moname='" + moname + '\'' +
				", dotyco='" + dotyco + '\'' +
				", dotyna='" + dotyna + '\'' +
				", modomu='" + modomu + '\'' +
				", monalr='" + monalr + '\'' +
				", mophon='" + mophon + '\'' +
				", moagna='" + moagna + '\'' +
				", moagtc='" + moagtc + '\'' +
				", moagtn='" + moagtn + '\'' +
				", moagdn='" + moagdn + '\'' +
				", moagph='" + moagph + '\'' +
				", mortgagorsList=" + mortgagorsList +
				", moorna='" + moorna + '\'' +
				", moordc='" + moordc + '\'' +
				", moordn='" + moordn + '\'' +
				", moordu='" + moordu + '\'' +
				", moorph='" + moorph + '\'' +
				", reeidc='" + reeidc + '\'' +
				", reeidn='" + reeidn + '\'' +
				", reeilo='" + reeilo + '\'' +
				", reiheu='" + reiheu + '\'' +
				", reihcn='" + reihcn + '\'' +
				", reihcs='" + reihcs + '\'' +
				", reeiha=" + reeiha +
				", reihuc='" + reihuc + '\'' +
				", reihun='" + reihun + '\'' +
				", reilcn='" + reilcn + '\'' +
				", reeila=" + reeila +
				", reiluc='" + reiluc + '\'' +
				", reilun='" + reilun + '\'' +
				", micenu='" + micenu + '\'' +
				", micono='" + micono + '\'' +
				", micova=" + micova +
				", mideam=" + mideam +
				", mimmco='" + mimmco + '\'' +
				", mimmna='" + mimmna + '\'' +
				", migusc='" + migusc + '\'' +
				", midsti='" + midsti + '\'' +
				", mideti='" + mideti + '\'' +
				", midena='" + midena + '\'' +
				", middtc='" + middtc + '\'' +
				", middtn='" + middtn + '\'' +
				", middnu='" + middnu + '\'' +
				", miorde='" + miorde + '\'' +
				", michco='" + michco + '\'' +
				", mihaga='" + mihaga + '\'' +
				", mihalo='" + mihalo + '\'' +
				", distbu='" + distbu + '\'' +
				", dinumb='" + dinumb + '\'' +
				", dicono='" + dicono + '\'' +
				", dicova=" + dicova +
				'}';
	}
}
