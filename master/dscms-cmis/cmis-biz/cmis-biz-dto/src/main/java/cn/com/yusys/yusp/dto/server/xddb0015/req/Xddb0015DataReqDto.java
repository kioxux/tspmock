package cn.com.yusys.yusp.dto.server.xddb0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：押品状态变更推送
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0015DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "distco")
    private String distco;//区县代码
    @JsonProperty(value = "certnu")
    private String certnu;//不动产权证书号
    @JsonProperty(value = "status")
    private String status;//押品状态



    public String getDistco() {
        return distco;
    }

    public void setDistco(String distco) {
        this.distco = distco;
    }

    public String getCertnu() {
        return certnu;
    }

    public void setCertnu(String certnu) {
        this.certnu = certnu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

	@Override
	public String toString() {
		return "Xddb0015DataReqDto{" +
				"distco='" + distco + '\'' +
				", certnu='" + certnu + '\'' +
				", status='" + status + '\'' +
				'}';
	}
}
