package cn.com.yusys.yusp.dto.server.xdcz0027.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：审批失败支用申请( 登记支用失败)
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0027DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cert_code")
    private String cert_code;//证件号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "fail_reason")
    private String fail_reason;//失败原因
    @JsonProperty(value = "Bak")
    private String Bak;//备注

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getFail_reason() {
        return fail_reason;
    }

    public void setFail_reason(String fail_reason) {
        this.fail_reason = fail_reason;
    }

    public String getBak() {
        return Bak;
    }

    public void setBak(String Bak) {
        this.Bak = Bak;
    }

    @Override
    public String toString() {
        return "Xdcz0027ReqDto{" +
                "cert_code='" + cert_code + '\'' +
                "cus_id='" + cus_id + '\'' +
                "cont_no='" + cont_no + '\'' +
                "fail_reason='" + fail_reason + '\'' +
                "Bak='" + Bak + '\'' +
                '}';
    }
}
