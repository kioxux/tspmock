package cn.com.yusys.yusp.dto.server.xdtz0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：取得记账日期为当天的提前还款授权表信息一览
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0013DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xddh0013DataReqDto{" +
                "certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                '}';
    }
}
