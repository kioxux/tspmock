package cn.com.yusys.yusp.dto.server.xdht0028.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据合同号取得客户经理电话号码
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0028DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同号

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

	@Override
	public String toString() {
		return "Xdht0028DataReqDto{" +
				"contNo='" + contNo + '\'' +
				'}';
	}
}
