package cn.com.yusys.yusp.dto.server.xdtz0054.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：将需要修改的受托支付账号生成修改记录
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0054DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "toppAcctNo")
    private String toppAcctNo;//交易对手账号
    @JsonProperty(value = "toppName")
    private String toppName;//交易对手名称
    @JsonProperty(value = "toppAmt")
    private BigDecimal toppAmt;//交易对手金额

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getToppAcctNo() {
        return toppAcctNo;
    }

    public void setToppAcctNo(String toppAcctNo) {
        this.toppAcctNo = toppAcctNo;
    }

    public String getToppName() {
        return toppName;
    }

    public void setToppName(String toppName) {
        this.toppName = toppName;
    }

    public BigDecimal getToppAmt() {
        return toppAmt;
    }

    public void setToppAmt(BigDecimal toppAmt) {
        this.toppAmt = toppAmt;
    }

    @Override
    public String toString() {
        return "Xdtz0054DataReqDto{" +
                "serno='" + serno + '\'' +
                ", billNo='" + billNo + '\'' +
                ", toppAcctNo='" + toppAcctNo + '\'' +
                ", toppName='" + toppName + '\'' +
                ", toppAmt=" + toppAmt +
                '}';
    }
}
