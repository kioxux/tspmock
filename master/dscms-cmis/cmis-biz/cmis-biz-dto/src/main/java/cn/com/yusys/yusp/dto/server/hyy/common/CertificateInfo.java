package cn.com.yusys.yusp.dto.server.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 证件类型
 */
@JsonPropertyOrder(alphabetic = true)
public class CertificateInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "version")
    private String version;//押品登记证明-版本
    @JsonProperty(value = "location")
    private String location;//押品登记证明-坐落
    @JsonProperty(value = "real_estate_unit_number")
    private String real_estate_unit_number;//押品登记证明-不动产单元号
    @JsonProperty(value = "certificate_number")
    private String certificate_number;//押品登记证明-不动产权证书号
    @JsonProperty(value = "house_certificate_number")
    private String house_certificate_number;//押品登记证明-房产证号
    @JsonProperty(value = "land_certificate_number")
    private String land_certificate_number;//押品登记证明-土地证号
    @JsonProperty(value = "mortgage_certificate_number")
    private String mortgage_certificate_number;//押品登记证明-不动产登记证明号
    @JsonProperty(value = "business_number")
    private String business_number;//押品登记证明-报件编号
    @JsonProperty(value = "print_number")
    private String print_number;//押品登记证明-证书印刷编号
    @JsonProperty(value = "booking_date")
    private String booking_date;//押品登记证明-登簿日期
    @JsonProperty(value = "obligee")
    private String obligee;//押品登记证明-权利人
    @JsonProperty(value = "obligor")
    private String obligor;//押品登记证明-义务人
    @JsonProperty(value = "right_type")
    private String right_type;//押品登记证明-证明权利类型
    @JsonProperty(value = "debt_start_time")
    private String debt_start_time;//押品登记证明-债务履行期限起
    @JsonProperty(value = "debt_end_time")
    private String debt_end_time;//押品登记证明-债务履行期限止
    @JsonProperty(value = "mortgage_method")
    private String mortgage_method;//押品登记证明-抵押方式
    @JsonProperty(value = "guarantee_scope")
    private String guarantee_scope;//押品登记证明-担保范围
    @JsonProperty(value = "debt_amount")
    private String debt_amount;//押品登记证明-被担保债权数额
    @JsonProperty(value = "remark")
    private String remark;//押品登记证明-附记
    @JsonProperty(value = "mortgage_contract_no")
    private String mortgage_contract_no;//押品登记证明-抵押合同编号
    @JsonProperty(value = "djjg")
    private String djjg;//押品登记证明-登记机构
    @JsonProperty(value = "district_code")
    private String district_code;//押品登记证明-所属区县代码
    @JsonProperty(value = "district_name")
    private String district_name;//押品登记证明-所属区县名称
    @JsonProperty(value = "djzlx")
    private String djzlx;//押品登记证明-登记子类型
    @JsonProperty(value = "djzlxmc")
    private String djzlxmc;//押品登记证明-登记子类型名称


    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getReal_estate_unit_number() {
        return real_estate_unit_number;
    }

    public void setReal_estate_unit_number(String real_estate_unit_number) {
        this.real_estate_unit_number = real_estate_unit_number;
    }

    public String getCertificate_number() {
        return certificate_number;
    }

    public void setCertificate_number(String certificate_number) {
        this.certificate_number = certificate_number;
    }

    public String getHouse_certificate_number() {
        return house_certificate_number;
    }

    public void setHouse_certificate_number(String house_certificate_number) {
        this.house_certificate_number = house_certificate_number;
    }

    public String getLand_certificate_number() {
        return land_certificate_number;
    }

    public void setLand_certificate_number(String land_certificate_number) {
        this.land_certificate_number = land_certificate_number;
    }

    public String getMortgage_certificate_number() {
        return mortgage_certificate_number;
    }

    public void setMortgage_certificate_number(String mortgage_certificate_number) {
        this.mortgage_certificate_number = mortgage_certificate_number;
    }

    public String getBusiness_number() {
        return business_number;
    }

    public void setBusiness_number(String business_number) {
        this.business_number = business_number;
    }

    public String getPrint_number() {
        return print_number;
    }

    public void setPrint_number(String print_number) {
        this.print_number = print_number;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getObligee() {
        return obligee;
    }

    public void setObligee(String obligee) {
        this.obligee = obligee;
    }

    public String getObligor() {
        return obligor;
    }

    public void setObligor(String obligor) {
        this.obligor = obligor;
    }

    public String getRight_type() {
        return right_type;
    }

    public void setRight_type(String right_type) {
        this.right_type = right_type;
    }

    public String getDebt_start_time() {
        return debt_start_time;
    }

    public void setDebt_start_time(String debt_start_time) {
        this.debt_start_time = debt_start_time;
    }

    public String getDebt_end_time() {
        return debt_end_time;
    }

    public void setDebt_end_time(String debt_end_time) {
        this.debt_end_time = debt_end_time;
    }

    public String getMortgage_method() {
        return mortgage_method;
    }

    public void setMortgage_method(String mortgage_method) {
        this.mortgage_method = mortgage_method;
    }

    public String getGuarantee_scope() {
        return guarantee_scope;
    }

    public void setGuarantee_scope(String guarantee_scope) {
        this.guarantee_scope = guarantee_scope;
    }

    public String getDebt_amount() {
        return debt_amount;
    }

    public void setDebt_amount(String debt_amount) {
        this.debt_amount = debt_amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMortgage_contract_no() {
        return mortgage_contract_no;
    }

    public void setMortgage_contract_no(String mortgage_contract_no) {
        this.mortgage_contract_no = mortgage_contract_no;
    }

    public String getDjjg() { return djjg; }

    public void setDjjg(String djjg) {
        this.djjg = djjg;
    }

    public String getDistrict_code() { return district_code; }

    public void setDistrict_code(String district_code) {
        this.district_code = district_code;
    }

    public String getDistrict_name() { return district_name; }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getDjzlx() { return djzlx; }

    public void setDjzlx(String djzlx) {
        this.djzlx = djzlx;
    }

    public String getDjzlxmc() { return djzlxmc; }

    public void setDjzlxmc(String djzlxmc) {
        this.djzlxmc = djzlxmc;
    }

    @Override
    public String toString() {
        return "CertificateInfo{" +
                "version='" + version + '\'' +
                ", location='" + location + '\'' +
                ", real_estate_unit_number='" + real_estate_unit_number + '\'' +
                ", certificate_number='" + certificate_number + '\'' +
                ", house_certificate_number='" + house_certificate_number + '\'' +
                ", land_certificate_number='" + land_certificate_number + '\'' +
                ", mortgage_certificate_number='" + mortgage_certificate_number + '\'' +
                ", business_number='" + business_number + '\'' +
                ", print_number='" + print_number + '\'' +
                ", booking_date='" + booking_date + '\'' +
                ", obligee='" + obligee + '\'' +
                ", obligor='" + obligor + '\'' +
                ", right_type='" + right_type + '\'' +
                ", debt_start_time='" + debt_start_time + '\'' +
                ", debt_end_time='" + debt_end_time + '\'' +
                ", mortgage_method='" + mortgage_method + '\'' +
                ", guarantee_scope='" + guarantee_scope + '\'' +
                ", debt_amount='" + debt_amount + '\'' +
                ", remark='" + remark + '\'' +
                ", mortgage_contract_no='" + mortgage_contract_no + '\'' +
                ", djjg='" + djjg + '\'' +
                ", district_code='" + district_code + '\'' +
                ", district_name='" + district_name + '\'' +
                ", djzlx='" + djzlx + '\'' +
                ", djzlxmc='" + djzlxmc + '\'' +
                '}';
    }
}

