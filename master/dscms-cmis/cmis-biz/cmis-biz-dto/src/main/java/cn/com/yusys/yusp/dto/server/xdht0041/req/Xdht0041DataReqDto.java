package cn.com.yusys.yusp.dto.server.xdht0041.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询受托记录状态
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0041DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "accotPk")
    private String accotPk;//核算主键
    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public String getAccotPk() {
        return accotPk;
    }

    public void setAccotPk(String accotPk) {
        this.accotPk = accotPk;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Override
    public String toString() {
        return "Xdht0041DataReqDto{" +
                "accotPk='" + accotPk + '\'' +
                ", billNo='" + billNo + '\'' +
                '}';
    }
}