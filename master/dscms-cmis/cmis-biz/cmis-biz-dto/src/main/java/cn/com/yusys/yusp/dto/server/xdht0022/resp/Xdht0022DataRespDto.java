package cn.com.yusys.yusp.dto.server.xdht0022.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：合同信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0022DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "totalQnt")
    private Integer totalQnt;
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public Integer getTotalQnt() {
        return totalQnt;
    }

    public void setTotalQnt(Integer totalQnt) {
        this.totalQnt = totalQnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdht0022DataRespDto{" +
                "totalQnt=" + totalQnt +
                "list=" + list +
                '}';
    }
}
