package cn.com.yusys.yusp.dto;

/**
 * @author xll
 * @version 1.0.0
 * @date 2021/8/30 19:27
 * @desc 换醒贷分配列表查询dto
 * @修改历史: 修改时间    修改人员    修改原因
 */
public class HxdxdInfoDto {
    // 业务流水
    private String serno;

    //客户编号
    private String cusId;

    //客户名称
    private String cusName;

    //申请人证件号
    private String certCode;

    //申请人手机号
    private String telPhone;

    //经营地址
    private String businessAddress;

    //责任人
    private String managerId;

    //责任人
    private String managerName;

    //登记日期
    private String inputDate;

    //分配时间
    private String distributionDate;

    //分配人id
    private String distributionId;

    //分配人
    private String distributionName;

    //状态
    private String state;

    //开始页码
    private String startPageNum;

    //每页记录数
    private String pageSize;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(String distributionDate) {
        this.distributionDate = distributionDate;
    }

    public String getDistributionId() {
        return distributionId;
    }

    public void setDistributionId(String distributionId) {
        this.distributionId = distributionId;
    }

    public String getDistributionName() {
        return distributionName;
    }

    public void setDistributionName(String distributionName) {
        this.distributionName = distributionName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
