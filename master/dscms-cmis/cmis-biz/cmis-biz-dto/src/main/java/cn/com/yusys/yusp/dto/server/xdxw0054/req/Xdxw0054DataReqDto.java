package cn.com.yusys.yusp.dto.server.xdxw0054.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询优抵贷损益表明细
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0054DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "indgtSerno")
    private String indgtSerno;//客户调查表编号
    @JsonProperty(value = "itemValue")
    private String itemValue;//项目值

    public String getIndgtSerno() {
        return indgtSerno;
    }

    public void setIndgtSerno(String indgtSerno) {
        this.indgtSerno = indgtSerno;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    @Override
    public String toString() {
        return "Xdxw0054DataReqDto{" +
                "indgtSerno='" + indgtSerno + '\'' +
                ", itemValue='" + itemValue + '\'' +
                '}';
    }
}

