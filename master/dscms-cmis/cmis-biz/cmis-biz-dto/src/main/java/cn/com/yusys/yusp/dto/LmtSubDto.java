package cn.com.yusys.yusp.dto;

import java.io.Serializable;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSub
 * @类描述: lmt_sub数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2021-01-23 10:19:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSubDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 授信额度编号 **/
	private String lmtLimitNo;
	
	/** 全局流水号 **/
	private String serno;
	
	/** 业务申请编号 **/
	private String lmtSerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 分项类别 STD_ZB_SUB_TYP **/
	private String subType;
	
	/** 额度类型 STD_ZB_LMT_TYPE **/
	private String limitType;
	
	/** 额度品种编号 **/
	private String lmtVarietNo;
	
	/** 适用产品编号 **/
	private String suitPrdNo;
	
	/** 授信额度 **/
	private java.math.BigDecimal lmtAmt;
	
	/** 担保方式 STD_ZB_GUAR_WAY **/
	private String guarWay;
	
	/** 币种 STD_ZB_CUR_TYP **/
	private String curType;
	
	/** 是否预授信 STD_ZX_YES_NO **/
	private String isPreLmt;
	
	/** 原授信起始日 **/
	private String lmtStartDate;
	
	/** 原授信到期日 **/
	private String lmtEndDate;
	
	/** 是否调整期限 **/
	private String isAdjTerm;
	
	/** 授信期限类型 **/
	private String lmtTermType;
	
	/** 授信期限 **/
	private String lmtTerm;
	
	/** 绿色产业类型 **/
	private String greenIndusType;
	
	/** 绿色授信项目类型 STD_ZB_GREEN_PRO_TYP **/
	private String greenProType;
	
	/** 放款方式 STD_ZB_PUTOUT_TYP **/
	private String pvpWay;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;

	/** 渠道来源 **/
	private String chnlSour;
	
	/** 更新人 **/
	private String updId;
	
	/** 更新机构 **/
	private String updBrId;
	
	/** 更新日期 **/
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 原授信额度编号 **/
	private String oldLmtLimitNo;

	/**授信类型**/
	private String lmtType;

	/**
	 * @return lmtType
	 */
	public String getLmtType() {
		return lmtType;
	}

	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}

	/**
	 * @param lmtLimitNo
	 */
	public void setLmtLimitNo(String lmtLimitNo) {
		this.lmtLimitNo = lmtLimitNo == null ? null : lmtLimitNo.trim();
	}
	
    /**
     * @return LmtLimitNo
     */	
	public String getLmtLimitNo() {
		return this.lmtLimitNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lmtSerno
	 */
	public void setLmtSerno(String lmtSerno) {
		this.lmtSerno = lmtSerno == null ? null : lmtSerno.trim();
	}
	
    /**
     * @return LmtSerno
     */	
	public String getLmtSerno() {
		return this.lmtSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param subType
	 */
	public void setSubType(String subType) {
		this.subType = subType == null ? null : subType.trim();
	}
	
    /**
     * @return SubType
     */	
	public String getSubType() {
		return this.subType;
	}
	
	/**
	 * @param limitType
	 */
	public void setLimitType(String limitType) {
		this.limitType = limitType == null ? null : limitType.trim();
	}
	
    /**
     * @return limitType
     */	
	public String getLimitType() {
		return this.limitType;
	}
	
	/**
	 * @param lmtVarietNo
	 */
	public void setLmtVarietNo(String lmtVarietNo) {
		this.lmtVarietNo = lmtVarietNo == null ? null : lmtVarietNo.trim();
	}
	
    /**
     * @return LmtVarietNo
     */	
	public String getLmtVarietNo() {
		return this.lmtVarietNo;
	}
	
	/**
	 * @param suitPrdNo
	 */
	public void setSuitPrdNo(String suitPrdNo) {
		this.suitPrdNo = suitPrdNo == null ? null : suitPrdNo.trim();
	}
	
    /**
     * @return SuitPrdNo
     */	
	public String getSuitPrdNo() {
		return this.suitPrdNo;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return LmtAmt
     */	
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay == null ? null : guarWay.trim();
	}
	
    /**
     * @return GuarWay
     */	
	public String getGuarWay() {
		return this.guarWay;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType == null ? null : curType.trim();
	}
	
    /**
     * @return CurType
     */	
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param isPreLmt
	 */
	public void setIsPreLmt(String isPreLmt) {
		this.isPreLmt = isPreLmt == null ? null : isPreLmt.trim();
	}
	
    /**
     * @return IsPreLmt
     */	
	public String getIsPreLmt() {
		return this.isPreLmt;
	}
	
	/**
	 * @param lmtStartDate
	 */
	public void setLmtStartDate(String lmtStartDate) {
		this.lmtStartDate = lmtStartDate == null ? null : lmtStartDate.trim();
	}
	
    /**
     * @return LmtStartDate
     */	
	public String getLmtStartDate() {
		return this.lmtStartDate;
	}
	
	/**
	 * @param lmtEndDate
	 */
	public void setLmtEndDate(String lmtEndDate) {
		this.lmtEndDate = lmtEndDate == null ? null : lmtEndDate.trim();
	}
	
    /**
     * @return LmtEndDate
     */	
	public String getLmtEndDate() {
		return this.lmtEndDate;
	}
	
	/**
	 * @param isAdjTerm
	 */
	public void setIsAdjTerm(String isAdjTerm) {
		this.isAdjTerm = isAdjTerm == null ? null : isAdjTerm.trim();
	}
	
    /**
     * @return IsAdjTerm
     */	
	public String getIsAdjTerm() {
		return this.isAdjTerm;
	}
	
	/**
	 * @param lmtTermType
	 */
	public void setLmtTermType(String lmtTermType) {
		this.lmtTermType = lmtTermType;
	}

	/**
     * @return lmtTermType
     */
	public String getLmtTermType() {
		return lmtTermType;
	}

	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(String lmtTerm) {
		this.lmtTerm = lmtTerm == null ? null : lmtTerm.trim();
	}
	
    /**
     * @return LmtTerm
     */	
	public String getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param greenIndusType
	 */
	public void setGreenIndusType(String greenIndusType) {
		this.greenIndusType = greenIndusType == null ? null : greenIndusType.trim();
	}
	
    /**
     * @return GreenIndusType
     */	
	public String getGreenIndusType() {
		return this.greenIndusType;
	}
	
	/**
	 * @param greenProType
	 */
	public void setGreenProType(String greenProType) {
		this.greenProType = greenProType == null ? null : greenProType.trim();
	}
	
    /**
     * @return GreenProType
     */	
	public String getGreenProType() {
		return this.greenProType;
	}
	
	/**
	 * @param pvpWay
	 */
	public void setPvpWay(String pvpWay) {
		this.pvpWay = pvpWay == null ? null : pvpWay.trim();
	}
	
    /**
     * @return PvpWay
     */	
	public String getPvpWay() {
		return this.pvpWay;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour == null ? null : chnlSour.trim();
	}
	
    /**
     * @return ChnlSour
     */	
	public String getChnlSour() {
		return this.chnlSour;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param oldLmtLimitNo
	 */
	public void setOldLmtLimitNo(String oldLmtLimitNo) {
		this.oldLmtLimitNo = oldLmtLimitNo == null ? null : oldLmtLimitNo.trim();
	}
	
    /**
     * @return OldLmtLimitNo
     */	
	public String getOldLmtLimitNo() {
		return this.oldLmtLimitNo;
	}


}