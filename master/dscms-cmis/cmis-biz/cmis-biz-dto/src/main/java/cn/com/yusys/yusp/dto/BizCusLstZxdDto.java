package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * cus与biz增享贷交互dto
 */
public class BizCusLstZxdDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 流水号 **/
    private String serno;
    /** 客户编号 **/
    private String cusId;

    /** 客户名称 **/
    private String cusName;

    /** 证件号码 **/
    private String certCode;

    /** 借据号 **/
    private String billNo;

    /** 合同号 **/
    private String contNo;

    /** 借据金额 **/
    private java.math.BigDecimal billAmt;

    /** 借据余额 **/
    private java.math.BigDecimal billBal;

    /** 贷款起始日 **/
    private String loanStartDate;

    /** 贷款截止日 **/
    private String loanEndDate;

    /** 申请日期 **/
    private String appDate;

    /** 联系电话 **/
    private String phone;

    /** 配偶名称 **/
    private String spouseCusName;

    /** 配偶证件号码 **/
    private String spouseCertCode;

    /** 办理状态 **/
    private String applyStatus;

    /** 转换原因 **/
    private String changeRs;

    /** 婚姻状态 **/
    private String spouseMarryVal;

    /** 配偶电话 **/
    private String spousePhone;

    /** 押品他项权证金额 **/
    private java.math.BigDecimal evalAmt;

    /** 批复余额 **/
    private java.math.BigDecimal approvalBal;

    /** 配偶客户编号 **/
    private String spouseCusId;

    /** 配偶借据号 **/
    private String spouseBillNo;

    /** 账务机构 **/
    private String finaBrId;

    /** 配偶的客户经理 **/
    private String spouseManagerId;

    /** 客户经理 **/
    private String managerName;

    /** 配偶客户经理 **/
    private String spouseManagerName;

    /** 执行利率(年) **/
    private java.math.BigDecimal execRateYear;

    /** 房产类型编码 **/
    private String landUseWay;

    /** 还款期数 **/
    private Integer repayTerm;

    /** 本金逾期记录 **/
    private Integer overdueCount;

    /** 借据号汇总 **/
    private String billNoAll;

    /** 配偶本金逾期记录 **/
    private Integer memberOverdueCount;

    /** 配偶借据号汇总 **/
    private String memberBillNoAll;

    /** 调查流水号 **/
    private String surveySerno;

    /** 主管客户经理 **/
    private String managerId;

    /** 主管机构 **/
    private String managerBrId;

    /** 操作类型 **/
    private String oprType;

    /** 登记人 **/
    private String inputId;

    /** 登记机构 **/
    private String inputBrId;

    /** 登记日期 **/
    private String inputDate;

    /** 最后修改人 **/
    private String updId;

    /** 最后修改机构 **/
    private String updBrId;

    /** 最后修改日期 **/
    private String updDate;

    /** 创建时间 **/
    private java.util.Date createTime;

    /** 修改时间 **/
    private java.util.Date updateTime;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public BigDecimal getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(BigDecimal billAmt) {
        this.billAmt = billAmt;
    }

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpouseCusName() {
        return spouseCusName;
    }

    public void setSpouseCusName(String spouseCusName) {
        this.spouseCusName = spouseCusName;
    }

    public String getSpouseCertCode() {
        return spouseCertCode;
    }

    public void setSpouseCertCode(String spouseCertCode) {
        this.spouseCertCode = spouseCertCode;
    }

    public String getApplyStatus() {
        return applyStatus;
    }

    public void setApplyStatus(String applyStatus) {
        this.applyStatus = applyStatus;
    }

    public String getChangeRs() {
        return changeRs;
    }

    public void setChangeRs(String changeRs) {
        this.changeRs = changeRs;
    }

    public String getSpouseMarryVal() {
        return spouseMarryVal;
    }

    public void setSpouseMarryVal(String spouseMarryVal) {
        this.spouseMarryVal = spouseMarryVal;
    }

    public String getSpousePhone() {
        return spousePhone;
    }

    public void setSpousePhone(String spousePhone) {
        this.spousePhone = spousePhone;
    }

    public BigDecimal getEvalAmt() {
        return evalAmt;
    }

    public void setEvalAmt(BigDecimal evalAmt) {
        this.evalAmt = evalAmt;
    }

    public BigDecimal getApprovalBal() {
        return approvalBal;
    }

    public void setApprovalBal(BigDecimal approvalBal) {
        this.approvalBal = approvalBal;
    }

    public String getSpouseCusId() {
        return spouseCusId;
    }

    public void setSpouseCusId(String spouseCusId) {
        this.spouseCusId = spouseCusId;
    }

    public String getSpouseBillNo() {
        return spouseBillNo;
    }

    public void setSpouseBillNo(String spouseBillNo) {
        this.spouseBillNo = spouseBillNo;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getSpouseManagerId() {
        return spouseManagerId;
    }

    public void setSpouseManagerId(String spouseManagerId) {
        this.spouseManagerId = spouseManagerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getSpouseManagerName() {
        return spouseManagerName;
    }

    public void setSpouseManagerName(String spouseManagerName) {
        this.spouseManagerName = spouseManagerName;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public String getLandUseWay() {
        return landUseWay;
    }

    public void setLandUseWay(String landUseWay) {
        this.landUseWay = landUseWay;
    }

    public Integer getRepayTerm() {
        return repayTerm;
    }

    public void setRepayTerm(Integer repayTerm) {
        this.repayTerm = repayTerm;
    }

    public Integer getOverdueCount() {
        return overdueCount;
    }

    public void setOverdueCount(Integer overdueCount) {
        this.overdueCount = overdueCount;
    }

    public String getBillNoAll() {
        return billNoAll;
    }

    public void setBillNoAll(String billNoAll) {
        this.billNoAll = billNoAll;
    }

    public Integer getMemberOverdueCount() {
        return memberOverdueCount;
    }

    public void setMemberOverdueCount(Integer memberOverdueCount) {
        this.memberOverdueCount = memberOverdueCount;
    }

    public String getMemberBillNoAll() {
        return memberBillNoAll;
    }

    public void setMemberBillNoAll(String memberBillNoAll) {
        this.memberBillNoAll = memberBillNoAll;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "BizCusLstZxdDto{" +
                "serno='" + serno + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certCode='" + certCode + '\'' +
                ", billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", billAmt=" + billAmt +
                ", billBal=" + billBal +
                ", loanStartDate='" + loanStartDate + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", appDate='" + appDate + '\'' +
                ", phone='" + phone + '\'' +
                ", spouseCusName='" + spouseCusName + '\'' +
                ", spouseCertCode='" + spouseCertCode + '\'' +
                ", applyStatus='" + applyStatus + '\'' +
                ", changeRs='" + changeRs + '\'' +
                ", spouseMarryVal='" + spouseMarryVal + '\'' +
                ", spousePhone='" + spousePhone + '\'' +
                ", evalAmt=" + evalAmt +
                ", approvalBal=" + approvalBal +
                ", spouseCusId='" + spouseCusId + '\'' +
                ", spouseBillNo='" + spouseBillNo + '\'' +
                ", finaBrId='" + finaBrId + '\'' +
                ", spouseManagerId='" + spouseManagerId + '\'' +
                ", managerName='" + managerName + '\'' +
                ", spouseManagerName='" + spouseManagerName + '\'' +
                ", execRateYear=" + execRateYear +
                ", landUseWay='" + landUseWay + '\'' +
                ", repayTerm=" + repayTerm +
                ", overdueCount=" + overdueCount +
                ", billNoAll='" + billNoAll + '\'' +
                ", memberOverdueCount=" + memberOverdueCount +
                ", memberBillNoAll='" + memberBillNoAll + '\'' +
                ", surveySerno='" + surveySerno + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", oprType='" + oprType + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
