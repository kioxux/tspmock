package cn.com.yusys.yusp.dto.server.xdsx0011.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：查询优惠（省心E付）
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0011DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "preferPoints")
    private String preferPoints;//优惠点数
    @JsonProperty(value = "preferTimes")
    private String preferTimes;//优惠次数
    @JsonProperty(value = "erorcd")
    private String erorcd;//结果代码
    @JsonProperty(value = "erortx")
    private String erortx;//结果信息
    public String getPreferPoints() {
        return preferPoints;
    }

    public void setPreferPoints(String preferPoints) {
        this.preferPoints = preferPoints;
    }

    public String getPreferTimes() {
        return preferTimes;
    }

    public void setPreferTimes(String preferTimes) {
        this.preferTimes = preferTimes;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Xdsx0011RespDto{" +
                "preferPoints='" + preferPoints + '\'' +
                "preferTimes='" + preferTimes + '\'' +
                '}';
    }
}  
