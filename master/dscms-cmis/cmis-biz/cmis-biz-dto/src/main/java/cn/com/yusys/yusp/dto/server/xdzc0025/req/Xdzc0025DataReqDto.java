package cn.com.yusys.yusp.dto.server.xdzc0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：票据池托收回款通知信贷
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0025DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "openType")
    private String openType;// 开关类型
    @JsonProperty(value = "cusId")
    private String cusId;// 客户编号

    public String getOpenType() {
        return openType;
    }

    public void setOpenType(String openType) {
        this.openType = openType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdzc0025DataReqDto{" +
                "openType='" + openType + '\'' +
                ", cusId='" + cusId + '\'' +
                '}';
    }
}
