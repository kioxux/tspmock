package cn.com.yusys.yusp.dto.server.xddb0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：抵押登记获取押品信息
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0012DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarid")
    private String guarid;//押品编号



    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

	@Override
	public String toString() {
		return "Xddb0012DataReqDto{" +
				"guarid='" + guarid + '\'' +
				'}';
	}
}
