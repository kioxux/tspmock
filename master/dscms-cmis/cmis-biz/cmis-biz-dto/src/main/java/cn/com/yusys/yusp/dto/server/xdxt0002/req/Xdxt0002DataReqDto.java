package cn.com.yusys.yusp.dto.server.xdxt0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：根据直营团队类型查询客户经理工号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxt0002DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "teamType")
    private String teamType;//

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    @Override
    public String toString() {
        return "Xdxt0002DataReqDto{" +
                "teamType='" + teamType + '\'' +
                '}';
    }
}
