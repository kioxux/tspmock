package cn.com.yusys.yusp.dto.server.xdsx0011.req;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：查询优惠（省心E付）
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0011DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    @NotNull(message = "合同号contNo不能为空")
    @NotEmpty(message = "合同号contNo不能为空")
    private String contNo;//合同号
    @JsonProperty(value = "loanUseType")
    @NotNull(message = "贷款用途loanUseType不能为空")
    @NotEmpty(message = "贷款用途loanUseType不能为空")
    private String loanUseType;//贷款用途
    @JsonProperty(value = "cusId")
    @NotNull(message = "客户号cusId不能为空")
    @NotEmpty(message = "客户号cusId不能为空")
    private String cusId;//客户号

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Xdsx0011DataReqDto{" +
                "contNo='" + contNo + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
