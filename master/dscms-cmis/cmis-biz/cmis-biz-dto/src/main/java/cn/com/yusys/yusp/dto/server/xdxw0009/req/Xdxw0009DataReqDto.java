package cn.com.yusys.yusp.dto.server.xdxw0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：查询合同、抵押合同用章编码
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0009DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    @Override
    public String toString() {
        return "Xdxw0009DataReqDto{" +
                "loanContNo='" + loanContNo + '\'' +
                '}';
    }
}
