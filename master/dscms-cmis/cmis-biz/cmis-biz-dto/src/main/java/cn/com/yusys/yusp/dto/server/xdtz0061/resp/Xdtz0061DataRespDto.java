package cn.com.yusys.yusp.dto.server.xdtz0061.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：小微电子签约借据号生成
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0061DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFlag")
    private String opFlag;//操作成功标志位
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息
    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Override
    public String toString() {
        return "Xdtz0061DataRespDto{" +
                "opFlag='" + opFlag + '\'' +
                ", opMsg='" + opMsg + '\'' +
                ", billNo='" + billNo + '\'' +
                '}';
    }
}
