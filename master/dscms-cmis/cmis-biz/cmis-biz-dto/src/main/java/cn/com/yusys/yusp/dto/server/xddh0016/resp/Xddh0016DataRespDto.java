package cn.com.yusys.yusp.dto.server.xddh0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：提前还款申请
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0016DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFLag")
    private String opFLag;//登记标志
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getOpFLag() {
        return opFLag;
    }

    public void setOpFLag(String opFLag) {
        this.opFLag = opFLag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xddh0016DataRespDto{" +
                "opFLag='" + opFLag + '\'' +
                ", opMsg='" + opMsg + '\'' +
                '}';
    }
}
