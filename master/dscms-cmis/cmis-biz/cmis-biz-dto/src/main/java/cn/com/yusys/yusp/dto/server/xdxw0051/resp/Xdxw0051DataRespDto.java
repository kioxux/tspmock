package cn.com.yusys.yusp.dto.server.xdxw0051.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据业务唯一编号查询无还本续贷贷销售收入
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0051DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "saleAmt")
	private String saleAmt;//销售收入

	public String getSaleAmt() {
		return saleAmt;
	}

	public void setSaleAmt(String saleAmt) {
		this.saleAmt = saleAmt;
	}

	@Override
	public String toString() {
		return "Xdxw0051DataRespDto{" +
				"saleAmt='" + saleAmt + '\'' +
				'}';
	}
}
