package cn.com.yusys.yusp.dto.server.xdtz0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 请求Data：在查询经营性贷款借据信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0004DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "certNo")
    @NotNull(message = "证件号certNo不能为空")
    @NotEmpty(message = "证件号certNo不能为空")
    private String certNo;//证件号

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Xdtz0004DataReqDto{" +
                "certNo=" + certNo +
                '}';
    }
}
