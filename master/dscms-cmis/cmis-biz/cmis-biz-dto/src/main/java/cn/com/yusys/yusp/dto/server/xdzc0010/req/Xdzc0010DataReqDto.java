package cn.com.yusys.yusp.dto.server.xdzc0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：资产池超短贷校验接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0010DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contNo")
    private String contNo;//协议编号
    @JsonProperty(value = "appAmt")
    private BigDecimal appAmt;//申请金额
    @JsonProperty(value = "startDate")
    private String startDate;//贷款起始日
    @JsonProperty(value = "endDate")
    private String endDate;//贷款到期日
    @JsonProperty(value = "loanTerm")
    private int loanTerm;//出账期限

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public BigDecimal getAppAmt() {
        return appAmt;
    }

    public void setAppAmt(BigDecimal appAmt) {
        this.appAmt = appAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(int loanTerm) {
        this.loanTerm = loanTerm;
    }

    @Override
    public String toString() {
        return "Xdzc0010DataReqDto{" +
                "contNo='" + contNo + '\'' +
                ", appAmt=" + appAmt +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", loanTerm=" + loanTerm +
                '}';
    }
}
