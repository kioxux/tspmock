package cn.com.yusys.yusp.dto.server.xdsx0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：担保公司合作协议查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0020DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "contno")
    private String contno;//组织机构代码

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    @Override
    public String toString() {
        return "Xdsx0020DataReqDto{" +
                "contno='" + contno + '\'' +
                '}';
    }
}
