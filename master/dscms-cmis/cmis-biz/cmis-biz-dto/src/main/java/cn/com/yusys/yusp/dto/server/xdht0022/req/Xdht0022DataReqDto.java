package cn.com.yusys.yusp.dto.server.xdht0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：合同信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0022DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "servdt")
    private String servdt;//请求方日期
    @JsonProperty(value = "servti")
    private String servti;//请求方时间
    @JsonProperty(value = "ipaddr")
    private String ipaddr;//请求方IP
    @JsonProperty(value = "mac")
    private String mac;//请求方MAC
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码
    @JsonProperty(value = "startPageNum")
    private Integer startPageNum;//开始页数
    @JsonProperty(value = "pageSize")
    private Integer pageSize;//条数

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public Integer getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(Integer startPageNum) {
        this.startPageNum = startPageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }


}
