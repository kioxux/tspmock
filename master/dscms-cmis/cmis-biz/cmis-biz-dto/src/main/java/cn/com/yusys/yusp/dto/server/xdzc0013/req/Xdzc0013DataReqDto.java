package cn.com.yusys.yusp.dto.server.xdzc0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：贸易合同资料上传接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdzc0013DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "tContYXSerno")
    private String tContYXSerno;//贸易合同影像流水号
    @JsonProperty(value = "contNo")
    private String contNo;//资产池协议编号
    @JsonProperty(value = "tContNo")
    private String tContNo;//贸易合同编号
    @JsonProperty(value = "tContCnName")
    private String tContCnName;//贸易合同名称
    @JsonProperty(value = "tContAmt")
    private BigDecimal tContAmt;//贸易合同金额
    @JsonProperty(value = "tStartDate")
    private String tStartDate;//贸易合同起始日
    @JsonProperty(value = "tEndDate")
    private String tEndDate;//贸易合同到期日
    @JsonProperty(value = "isBankAcct")
    private String isBankAcct;//交易对手是否本行账户STD_ZB_YES_NO
    @JsonProperty(value = "toppName")
    private String toppName;//交易对手名称
    @JsonProperty(value = "toppAcctNo")
    private String toppAcctNo;//交易对手账号
    @JsonProperty(value = "opanOrgNo")
    private String opanOrgNo;//开户行行号
    @JsonProperty(value = "opanOrgName")
    private String opanOrgName;//开户行行名
    @JsonProperty(value = "contHighAvlAmt")
    private BigDecimal contHighAvlAmt;//可用信总金额
    @JsonProperty(value = "settlementMethod")
    private String settlementMethod;//结算方式STD_ZB_SETTLE_METH
    @JsonProperty(value = "cusId")
    private String cusId;// 客户编号
    @JsonProperty(value = "cusName")
    private String cusName;// 客户名称
    @JsonProperty(value = "supplierName")
    private String supplierName;//供货方
    @JsonProperty(value = "consumeName")
    private String consumeName;//公司名称(消费方)
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "prdQnt")
    private String prdQnt;//产品数量
    @JsonProperty(value = "prdPrice")
    private BigDecimal prdPrice;//产品单价
    @JsonProperty(value = "signDate")
    private String signDate;//签约日期
    @JsonProperty(value = "deliverDate")
    private String deliverDate;//交付日期
    @JsonProperty(value = "deliverType")
    private String deliverType;//交付方式

    public String gettContYXSerno() {
        return tContYXSerno;
    }

    public void settContYXSerno(String tContYXSerno) {
        this.tContYXSerno = tContYXSerno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String gettContNo() {
        return tContNo;
    }

    public void settContNo(String tContNo) {
        this.tContNo = tContNo;
    }

    public String gettContCnName() {
        return tContCnName;
    }

    public void settContCnName(String tContCnName) {
        this.tContCnName = tContCnName;
    }

    public BigDecimal gettContAmt() {
        return tContAmt;
    }

    public void settContAmt(BigDecimal tContAmt) {
        this.tContAmt = tContAmt;
    }

    public String gettStartDate() {
        return tStartDate;
    }

    public void settStartDate(String tStartDate) {
        this.tStartDate = tStartDate;
    }

    public String gettEndDate() {
        return tEndDate;
    }

    public void settEndDate(String tEndDate) {
        this.tEndDate = tEndDate;
    }

    public String getIsBankAcct() {
        return isBankAcct;
    }

    public void setIsBankAcct(String isBankAcct) {
        this.isBankAcct = isBankAcct;
    }

    public String getToppName() {
        return toppName;
    }

    public void setToppName(String toppName) {
        this.toppName = toppName;
    }

    public String getToppAcctNo() {
        return toppAcctNo;
    }

    public void setToppAcctNo(String toppAcctNo) {
        this.toppAcctNo = toppAcctNo;
    }

    public String getOpanOrgNo() {
        return opanOrgNo;
    }

    public void setOpanOrgNo(String opanOrgNo) {
        this.opanOrgNo = opanOrgNo;
    }

    public String getOpanOrgName() {
        return opanOrgName;
    }

    public void setOpanOrgName(String opanOrgName) {
        this.opanOrgName = opanOrgName;
    }

    public BigDecimal getContHighAvlAmt() {
        return contHighAvlAmt;
    }

    public void setContHighAvlAmt(BigDecimal contHighAvlAmt) {
        this.contHighAvlAmt = contHighAvlAmt;
    }

    public String getSettlementMethod() {
        return settlementMethod;
    }

    public void setSettlementMethod(String settlementMethod) {
        this.settlementMethod = settlementMethod;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getConsumeName() {
        return consumeName;
    }

    public void setConsumeName(String consumeName) {
        this.consumeName = consumeName;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getPrdQnt() {
        return prdQnt;
    }

    public void setPrdQnt(String prdQnt) {
        this.prdQnt = prdQnt;
    }

    public BigDecimal getPrdPrice() {
        return prdPrice;
    }

    public void setPrdPrice(BigDecimal prdPrice) {
        this.prdPrice = prdPrice;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getDeliverDate() {
        return deliverDate;
    }

    public void setDeliverDate(String deliverDate) {
        this.deliverDate = deliverDate;
    }

    public String getDeliverType() {
        return deliverType;
    }

    public void setDeliverType(String deliverType) {
        this.deliverType = deliverType;
    }

    @Override
    public String toString() {
        return "Xdzc0013DataReqDto{" +
                "tContYXSerno='" + tContYXSerno + '\'' +
                ", contNo='" + contNo + '\'' +
                ", tContNo='" + tContNo + '\'' +
                ", tContCnName='" + tContCnName + '\'' +
                ", tContAmt=" + tContAmt +
                ", tStartDate='" + tStartDate + '\'' +
                ", tEndDate='" + tEndDate + '\'' +
                ", isBankAcct='" + isBankAcct + '\'' +
                ", toppName='" + toppName + '\'' +
                ", toppAcctNo='" + toppAcctNo + '\'' +
                ", opanOrgNo='" + opanOrgNo + '\'' +
                ", opanOrgName='" + opanOrgName + '\'' +
                ", contHighAvlAmt=" + contHighAvlAmt +
                ", settlementMethod='" + settlementMethod + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", supplierName='" + supplierName + '\'' +
                ", consumeName='" + consumeName + '\'' +
                ", prdName='" + prdName + '\'' +
                ", prdQnt='" + prdQnt + '\'' +
                ", prdPrice=" + prdPrice +
                ", signDate='" + signDate + '\'' +
                ", deliverDate='" + deliverDate + '\'' +
                ", deliverType='" + deliverType + '\'' +
                '}';
    }
}
