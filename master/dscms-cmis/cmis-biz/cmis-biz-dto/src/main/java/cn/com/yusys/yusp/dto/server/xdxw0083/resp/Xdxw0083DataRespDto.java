package cn.com.yusys.yusp.dto.server.xdxw0083.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.io.Serializable;

/**
 * 请求Data：小微续贷白名单查询
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0083DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @JsonProperty(value = "opFlag")
    private String opFlag;//操作标志

    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息
    @Override
    public String toString() {
        return "Xdxw0083DataReqDto{" +
                "opFlag=" + opFlag +
                "opMsg=" + opMsg +
                '}';
    }
}
