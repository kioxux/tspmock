package cn.com.yusys.yusp.dto.server.xdtz0017.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：票据更换（通知信贷更改票据暂用额度台账）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0017DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "flag")
	private String flag;//成功失败标志


	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "Xdtz0017DataRespDto{" +
				"flag='" + flag + '\'' +
				'}';
	}
}
