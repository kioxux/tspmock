package cn.com.yusys.yusp.dto.server.xdtz0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：根据证件号查询借据信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0006DataRespDto implements Serializable {
	@JsonProperty(value = "list")
	private java.util.List<List> list;

	public java.util.List<List> getList() {
		return list;
	}

	public void setList(java.util.List<List> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Xdtz0006DataRespDto{" +
				"list=" + list +
				'}';
	}
}
