package cn.com.yusys.yusp.dto.server.xddh0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：推送优享贷预警信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0012DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yw_date")
    private String yw_date;//任务日期
    @JsonProperty(value = "dqrw_num")
    private BigDecimal dqrw_num;//任务数量
    @JsonProperty(value = "doc_name")
    private String doc_name;//文件名称

    public String getYw_date() {
        return yw_date;
    }

    public void setYw_date(String yw_date) {
        this.yw_date = yw_date;
    }

    public BigDecimal getDqrw_num() {
        return dqrw_num;
    }

    public void setDqrw_num(BigDecimal dqrw_num) {
        this.dqrw_num = dqrw_num;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    @Override
    public String toString() {
        return "Xddh0011DataReqDto{" +
                "yw_date='" + yw_date + '\'' +
                ", dqrw_num=" + dqrw_num +
                ", doc_name='" + doc_name + '\'' +
                '}';
    }
}
