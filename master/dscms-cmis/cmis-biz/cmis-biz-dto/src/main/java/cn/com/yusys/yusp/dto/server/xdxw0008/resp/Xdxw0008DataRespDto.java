package cn.com.yusys.yusp.dto.server.xdxw0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：优农贷黑白名单校验
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0008DataRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "checkResult")
	private String checkResult;//检查结果

	public String getCheckResult() {
		return checkResult;
	}

	public void setCheckResult(String checkResult) {
		this.checkResult = checkResult;
	}

	@Override
	public String toString() {
		return "Xdxw0008DataRespDto{" +
				"checkResult='" + checkResult + '\'' +
				'}';
	}
}
