package cn.com.yusys.yusp.dto.server.xdxw0002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：小微贷前调查信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0002DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalNum")
    private String totalNum;//总数
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(String totalNum) {
        this.totalNum = totalNum;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdxw0002DataRespDto{" +
                "totalNum='" + totalNum + '\'' +
                ", list=" + list +
                '}';
    }
}
