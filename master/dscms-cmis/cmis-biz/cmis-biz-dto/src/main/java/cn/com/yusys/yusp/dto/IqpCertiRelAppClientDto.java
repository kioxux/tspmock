package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class IqpCertiRelAppClientDto implements Serializable {
	private static final long serialVersionUID = 1L;
	/** 押品统一编号 **/
	private String guarNos;//入参多个押品编号
	public String getGuarNos() {
		return guarNos;
	}
	private IqpCertiRelAppClientDto iqpCertiRelAppClientDto;

	private String rtnCde;//返回码值，成功为"000000"
	private String rtnMsg;

	public void setGuarNos(String guarNos) {
		this.guarNos = guarNos;
	}

	public IqpCertiRelAppClientDto getIqpCertiRelAppClientDto() {
		return this.iqpCertiRelAppClientDto;
	}

	public void setIqpCertiRelAppClientDto(IqpCertiRelAppClientDto bizIqpLoanAppDto) {
		this.iqpCertiRelAppClientDto = iqpCertiRelAppClientDto;
	}
	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}

	public String getRtnMsg() {
		return this.rtnMsg;
	}

	public void setRtnCde(String rtnCde) {
		this.rtnCde = rtnCde;
	}

	public String getRtnCde(String guarPctTool02) {
		return this.rtnCde;
	}
}
