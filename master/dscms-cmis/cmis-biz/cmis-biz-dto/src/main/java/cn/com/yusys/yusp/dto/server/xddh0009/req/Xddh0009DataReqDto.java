package cn.com.yusys.yusp.dto.server.xddh0009.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：还款日期卡控
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddh0009DataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bill_no")
    private String bill_no;//借据号

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    @Override
    public String toString() {
        return "Xddh0009DataReqDto{" +
                "bill_no='" + bill_no + '\'' +
                '}';
    }
}
