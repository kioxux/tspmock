package cn.com.yusys.yusp.dto.server.cmisbiz0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 集团客户授信复审
 */
@JsonPropertyOrder(alphabetic = true)
public class CmisBiz0002ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    @JsonProperty(value = "pkId")
    private String pkId;

    /**
     * 集团申请流水号
     **/
    @JsonProperty(value = "grpSerno")
    private String grpSerno;

    /**
     * 授信类型
     **/
    @JsonProperty(value = "lmtType")
    private String lmtType;

    /**
     * 集团客户编号
     **/
    @JsonProperty(value = "grpCusId")
    private String grpCusId;

    /**
     * 集团客户名称
     **/
    @JsonProperty(value = "grpCusName")
    private String grpCusName;

    /**
     * 币种
     **/
    @JsonProperty(value = "curType")
    private String curType;

    /**
     * 原授信流水号
     **/
    @JsonProperty(value = "origiLmtSerno")
    private String origiLmtSerno;

    /**
     * 原授信批复流水号
     **/
    @JsonProperty(value = "origiLmtReplySerno")
    private String origiLmtReplySerno;

    /**
     * 敞口额度合计
     **/
    @JsonProperty(value = "openTotalLmtAmt")
    private java.math.BigDecimal openTotalLmtAmt;

    /**
     * 低风险额度合计
     **/
    @JsonProperty(value = "lowRiskTotalLmtAmt")
    private java.math.BigDecimal lowRiskTotalLmtAmt;

    /**
     * 授信期限
     **/
    @JsonProperty(value = "lmtTerm")
    private Integer lmtTerm;

    /**
     * 原授信期限
     **/
    @JsonProperty(value = "origiLmtTerm")
    private Integer origiLmtTerm;

    /**
     * MEM_LMT_ADJUST_DESC
     **/
    @JsonProperty(value = "memLmtAdjustDesc")
    private String memLmtAdjustDesc;

    /**
     * 操作类型
     **/
    @JsonProperty(value = "oprType")
    private String oprType;

    /**
     * 审批状态
     **/
    @JsonProperty(value = "approveStatus")
    private String approveStatus;

    /**
     * 登记人
     **/
    @JsonProperty(value = "inputId")
    private String inputId;

    /**
     * 登记机构
     **/
    @JsonProperty(value = "inputBrId")
    private String inputBrId;

    /**
     * 登记日期
     **/
    @JsonProperty(value = "inputDate")
    private String inputDate;

    /**
     * 最近修改人
     **/
    @JsonProperty(value = "updId")
    private String updId;

    /**
     * 最近修改机构
     **/
    @JsonProperty(value = "updBrId")
    private String updBrId;

    /**
     * 最近修改日期
     **/
    @JsonProperty(value = "updDate")
    private String updDate;

    /**
     * 主管客户经理
     **/
    @JsonProperty(value = "managerId")
    private String managerId;

    /**
     * 主管机构
     **/
    @JsonProperty(value = "managerBrId")
    private String managerBrId;

    /**
     * 创建时间
     **/
    @JsonProperty(value = "createTime")
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    @JsonProperty(value = "updateTime")
    private java.util.Date updateTime;

    /**
     * 是否重组贷款
     **/
    @JsonProperty(value = "isRestruLoan")
    private String isRestruLoan;

    /**
     * 是否允许额度在成员间调剂
     **/
    @JsonProperty(value = "isAllowAllocatMem")
    private String isAllowAllocatMem;

    /**
     * 综合评价
     **/
    @JsonProperty(value = "inteEvlu")
    private String inteEvlu;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getGrpSerno() {
        return grpSerno;
    }

    public void setGrpSerno(String grpSerno) {
        this.grpSerno = grpSerno;
    }

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getGrpCusId() {
        return grpCusId;
    }

    public void setGrpCusId(String grpCusId) {
        this.grpCusId = grpCusId;
    }

    public String getGrpCusName() {
        return grpCusName;
    }

    public void setGrpCusName(String grpCusName) {
        this.grpCusName = grpCusName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getOrigiLmtSerno() {
        return origiLmtSerno;
    }

    public void setOrigiLmtSerno(String origiLmtSerno) {
        this.origiLmtSerno = origiLmtSerno;
    }

    public String getOrigiLmtReplySerno() {
        return origiLmtReplySerno;
    }

    public void setOrigiLmtReplySerno(String origiLmtReplySerno) {
        this.origiLmtReplySerno = origiLmtReplySerno;
    }

    public BigDecimal getOpenTotalLmtAmt() {
        return openTotalLmtAmt;
    }

    public void setOpenTotalLmtAmt(BigDecimal openTotalLmtAmt) {
        this.openTotalLmtAmt = openTotalLmtAmt;
    }

    public BigDecimal getLowRiskTotalLmtAmt() {
        return lowRiskTotalLmtAmt;
    }

    public void setLowRiskTotalLmtAmt(BigDecimal lowRiskTotalLmtAmt) {
        this.lowRiskTotalLmtAmt = lowRiskTotalLmtAmt;
    }

    public Integer getLmtTerm() {
        return lmtTerm;
    }

    public void setLmtTerm(Integer lmtTerm) {
        this.lmtTerm = lmtTerm;
    }

    public Integer getOrigiLmtTerm() {
        return origiLmtTerm;
    }

    public void setOrigiLmtTerm(Integer origiLmtTerm) {
        this.origiLmtTerm = origiLmtTerm;
    }

    public String getMemLmtAdjustDesc() {
        return memLmtAdjustDesc;
    }

    public void setMemLmtAdjustDesc(String memLmtAdjustDesc) {
        this.memLmtAdjustDesc = memLmtAdjustDesc;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsRestruLoan() {
        return isRestruLoan;
    }

    public void setIsRestruLoan(String isRestruLoan) {
        this.isRestruLoan = isRestruLoan;
    }

    public String getIsAllowAllocatMem() {
        return isAllowAllocatMem;
    }

    public void setIsAllowAllocatMem(String isAllowAllocatMem) {
        this.isAllowAllocatMem = isAllowAllocatMem;
    }

    public String getInteEvlu() {
        return inteEvlu;
    }

    public void setInteEvlu(String inteEvlu) {
        this.inteEvlu = inteEvlu;
    }

    @Override
    public String toString() {
        return "CmisBiz0002ReqDto{" +
                "pkId='" + pkId + '\'' +
                ", grpSerno='" + grpSerno + '\'' +
                ", lmtType='" + lmtType + '\'' +
                ", grpCusId='" + grpCusId + '\'' +
                ", grpCusName='" + grpCusName + '\'' +
                ", curType='" + curType + '\'' +
                ", origiLmtSerno='" + origiLmtSerno + '\'' +
                ", origiLmtReplySerno='" + origiLmtReplySerno + '\'' +
                ", openTotalLmtAmt=" + openTotalLmtAmt +
                ", lowRiskTotalLmtAmt=" + lowRiskTotalLmtAmt +
                ", lmtTerm=" + lmtTerm +
                ", origiLmtTerm=" + origiLmtTerm +
                ", memLmtAdjustDesc='" + memLmtAdjustDesc + '\'' +
                ", oprType='" + oprType + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", isRestruLoan='" + isRestruLoan + '\'' +
                ", isAllowAllocatMem='" + isAllowAllocatMem + '\'' +
                ", inteEvlu='" + inteEvlu + '\'' +
                '}';
    }
}
