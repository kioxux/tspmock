package cn.com.yusys.yusp.dto.server.xddb0010.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Data：信贷押品列表查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0010DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

	@JsonProperty(value = "ypList")
	private java.util.List<YpList> ypList;

	public List<YpList> getYpList() {
		return ypList;
	}

	public void setYpList(List<YpList> ypList) {
		this.ypList = ypList;
	}

	@Override
	public String toString() {
		return "Xddb0010DataRespDto{" +
				"ypList=" + ypList +
				'}';
	}
}
