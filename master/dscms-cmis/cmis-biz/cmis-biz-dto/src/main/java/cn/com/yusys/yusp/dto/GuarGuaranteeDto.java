package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class GuarGuaranteeDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 客户编号 **/
    private String cusId;

    /** 担保人名称 **/
    private String assureName;

    /** 担保人证件类型 **/
    private String cerType;

    /** 担保人证件号码 **/
    private String assureCertCode;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getAssureName() {
        return assureName;
    }

    public void setAssureName(String assureName) {
        this.assureName = assureName;
    }

    public String getCerType() {
        return cerType;
    }

    public void setCerType(String cerType) {
        this.cerType = cerType;
    }

    public String getAssureCertCode() {
        return assureCertCode;
    }

    public void setAssureCertCode(String assureCertCode) {
        this.assureCertCode = assureCertCode;
    }
}
