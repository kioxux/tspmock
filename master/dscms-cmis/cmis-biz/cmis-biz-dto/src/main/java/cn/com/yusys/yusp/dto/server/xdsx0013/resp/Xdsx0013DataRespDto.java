package cn.com.yusys.yusp.dto.server.xdsx0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Data：风控发信贷根据客户号查询授信信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdsx0013DataRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "crd_status")
    private String crd_status;//客户授信状态
    @JsonProperty(value = "serno")
    private String serno;//批复的授信协议编号
    @JsonProperty(value = "lmt_total_amt")
    private String lmt_total_amt;//授信总金额
    @JsonProperty(value = "sx_start_date")
    private String sx_start_date;//授信起始日
    @JsonProperty(value = "sx_end_date")
    private String sx_end_date;//授信到期日
    @JsonProperty(value = "manager_id")
    private String manager_id;//管户客户经理编号
    @JsonProperty(value = "manager_name")
    private String manager_name;//管户客户经理名称
    @JsonProperty(value = "manager_org_id")
    private String manager_org_id;//管户客户经理所在机构ID
    @JsonProperty(value = "manager_org_name")
    private String manager_org_name;//管户客户经理所在机构名称
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getCrd_status() {
        return crd_status;
    }

    public void setCrd_status(String crd_status) {
        this.crd_status = crd_status;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getLmt_total_amt() {
        return lmt_total_amt;
    }

    public void setLmt_total_amt(String lmt_total_amt) {
        this.lmt_total_amt = lmt_total_amt;
    }

    public String getSx_start_date() {
        return sx_start_date;
    }

    public void setSx_start_date(String sx_start_date) {
        this.sx_start_date = sx_start_date;
    }

    public String getSx_end_date() {
        return sx_end_date;
    }

    public void setSx_end_date(String sx_end_date) {
        this.sx_end_date = sx_end_date;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_org_id() {
        return manager_org_id;
    }

    public void setManager_org_id(String manager_org_id) {
        this.manager_org_id = manager_org_id;
    }

    public String getManager_org_name() {
        return manager_org_name;
    }

    public void setManager_org_name(String manager_org_name) {
        this.manager_org_name = manager_org_name;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdsx0013DataRespDto{" +
                "crd_status='" + crd_status + '\'' +
                ", serno='" + serno + '\'' +
                ", lmt_total_amt='" + lmt_total_amt + '\'' +
                ", sx_start_date='" + sx_start_date + '\'' +
                ", sx_end_date='" + sx_end_date + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", manager_org_id='" + manager_org_id + '\'' +
                ", manager_org_name='" + manager_org_name + '\'' +
                ", list=" + list +
                '}';
    }
}
