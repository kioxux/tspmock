package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdls0001.req.Xdls0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0001.resp.Xdls0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdls0002.req.Xdls0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0002.resp.Xdls0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdls0003.req.Xdls0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0003.resp.Xdls0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdls0004.req.Xdls0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0004.resp.Xdls0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdls0005.req.Xdls0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0005.resp.Xdls0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdls0006.req.Xdls0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0006.resp.Xdls0006DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizLsClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:零售业务服务接口
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizLsClientServiceImpl.class)
public interface DscmsBizLsClientService {
    /**
     * 交易码：xdls0001
     * 交易描述：房贷要素查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizls4bsp/xdls0001")
    public ResultDto<Xdls0001DataRespDto> xdls0001(Xdls0001DataReqDto reqDto);

    /**
     * 交易码：xdls0002
     * 交易描述：市民贷联系人信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizls4bsp/xdls0002")
    public ResultDto<Xdls0002DataRespDto> xdls0002(Xdls0002DataReqDto reqDto);

    /**
     * 交易码：xdls0003
     * 交易描述：信贷授信协议合同金额查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizls4bsp/xdls0003")
    public ResultDto<Xdls0003DataRespDto> xdls0003(Xdls0003DataReqDto reqDto);

    /**
     * 交易码：xdls0004
     * 交易描述：查询信贷有无授信历史
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/bizls4bsp/xdls0004")
    public ResultDto<Xdls0004DataRespDto> xdls0004(Xdls0004DataReqDto reqDto);

    /**
     * 交易码：xdls0005
     * 交易描述：智能风控调用信贷通知
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizls4bsp/xdls0005")
    public ResultDto<Xdls0005DataRespDto> xdls0005(Xdls0005DataReqDto reqDto);

    /**
     * 交易码：xdls0006
     * 交易描述：白领贷额度查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizls4bsp/xdls0006")
    public ResultDto<Xdls0006DataRespDto> xdls0006(Xdls0006DataReqDto reqDto);
}
