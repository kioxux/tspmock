package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xddb0001.req.Xddb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0001.resp.Xddb0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0002.req.Xddb0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0002.resp.Xddb0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0003.req.Xddb0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0003.resp.Xddb0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0004.req.Xddb0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0004.resp.Xddb0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0005.req.Xddb0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0005.resp.Xddb0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0006.req.Xddb0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0006.resp.Xddb0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0007.req.Xddb0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0007.resp.Xddb0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0008.req.Xddb0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0008.resp.Xddb0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0009.req.Xddb0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0009.resp.Xddb0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0010.req.Xddb0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0010.resp.Xddb0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0011.req.Xddb0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0011.resp.Xddb0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0012.req.Xddb0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0012.resp.Xddb0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0013.req.Xddb0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0013.resp.Xddb0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0014.req.Xddb0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0014.resp.Xddb0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0015.req.Xddb0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0015.resp.Xddb0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0016.req.Xddb0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0016.resp.Xddb0016DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0017.req.Xddb0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0017.resp.Xddb0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0018.req.Xddb0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0018.resp.Xddb0018DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0019.req.Xddb0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0019.resp.Xddb0019DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0020.req.Xddb0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0020.resp.Xddb0020DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0021.req.Xddb0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0021.resp.Xddb0021DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0022.req.Xddb0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0022.resp.Xddb0022DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0023.req.Xddb0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0023.resp.Xddb0023DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0001.req.Xddh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0001.resp.Xddh0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0002.req.Xddh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0002.resp.Xddh0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsBizDbClientServiceImpl implements DscmsBizDbClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsBizDbClientServiceImpl.class);


    /**
     * 交易码：xddb0002
     * 交易描述：押品状态查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0002DataRespDto> xddb0002(Xddb0002DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0002.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0002.value));
        return null;
    }

    /**
     * 交易码：xddb0018
     * 交易描述：押品状态同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0018DataRespDto> xddb0018(Xddb0018DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0018.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0018.value));
        return null;
    }

    /**
     * 交易码：xddb0001
     * 交易描述：查询押品是否按揭
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0001DataRespDto> xddb0001(Xddb0001DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0001.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0001.value));
        return null;
    }


    /**
     * 交易码：xddb0006
     * 交易描述：信贷押品状态查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0006DataRespDto> xddb0006(Xddb0006DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0006.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0006.value));
        return null;
    }

    /**
     * 交易码：xddb0003
     * 交易描述：押品信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0003DataRespDto> xddb0003(Xddb0003DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0003.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0003.value));
        return null;
    }

    /**
     * 交易码：xddb0007
     * 交易描述：电票质押请求
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0007DataRespDto> xddb0007(Xddb0007DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0007.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0007.value));
        return null;
    }

    /**
     * 交易码：xddb0008
     * 交易描述：查询在线抵押信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0008DataRespDto> xddb0008(Xddb0008DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0008.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0008.value));
        return null;
    }

    /**
     * 交易码：xddb0016
     * 交易描述：押品信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0016DataRespDto> xddb0016(Xddb0016DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0016.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0016.value));
        return null;
    }

    /**
     * 交易码：xddb0010
     * 交易描述：信贷押品列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0010DataRespDto> xddb0010(Xddb0010DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0010.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0010.value));
        return null;
    }

    /**
     * 交易码：xddb0012
     * 交易描述：抵押登记获取押品信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0012DataRespDto> xddb0012(Xddb0012DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0012.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0012.value));
        return null;
    }

    /**
     * 交易码：xddb0013
     * 交易描述：抵押登记不动产登记证明入库
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0013DataRespDto> xddb0013(Xddb0013DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0013.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0013.value));
        return null;
    }

    /**
     * 交易码：xddb0014
     * 交易描述：抵押登记注销风控
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0014DataRespDto> xddb0014(Xddb0014DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0014.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0014.value));
        return null;
    }

    /**
     * 交易码：xddb0015
     * 交易描述：押品状态变更推送
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0015DataRespDto> xddb0015(Xddb0015DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0015.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0015.value));
        return null;
    }

    /**
     * 交易码：xddb0011
     * 交易描述：提前出库押品信息维护
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0011DataRespDto> xddb0011(Xddb0011DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0011.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0011.value));
        return null;
    }

    /**
     * 交易码：xddb0009
     * 交易描述：双录流水号与押品编号关系维护
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0009DataRespDto> xddb0009(Xddb0009DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0009.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0009.value));
        return null;
    }

    /**
     * 交易码：xddb0004
     * 交易描述：提醒任务处理结果实时同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0004DataRespDto> xddb0004(Xddb0004DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0004.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0004.value));
        return null;
    }

    /**
     * 交易码：xddb0005
     * 交易描述：重估任务分配同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0005DataRespDto> xddb0005(Xddb0005DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0005.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0005.value));
        return null;
    }

    /**
     * 交易码：xddb0020
     * 交易描述：根据客户名查询抵押物类型
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0020DataRespDto> xddb0020(Xddb0020DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0020.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0020.value));
        return null;
    }

    /**
     * 交易码：xddb0021
     * 交易描述：根据客户名查询抵押物类型
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0021DataRespDto> xddb0021(Xddb0021DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0021.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0021.value));
        return null;
    }

    /**
     * 交易码：xddb0022
     * 交易描述：根据押品编号查询核心及信贷系统有无押品数据
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0022DataRespDto> xddb0022(Xddb0022DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0022.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0022.value));
        return null;
    }

    /**
     * 交易码：xddb0017
     * 交易描述：获取抵押登记双录音视频信息列表
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0017DataRespDto> xddb0017(Xddb0017DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0017.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0017.value));
        return null;
    }

    /**
     * 交易码：xddb0019
     * 交易描述：解质押押品校验
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0019DataRespDto> xddb0019(Xddb0019DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0019.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0019.value));
        return null;
    }

    /**
     * 交易码：xddb0023
     * 交易描述：更新抵质押品状态
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb0023DataRespDto> xddb0023(Xddb0023DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDB0023.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDB0023.value));
        return null;
    }

}
