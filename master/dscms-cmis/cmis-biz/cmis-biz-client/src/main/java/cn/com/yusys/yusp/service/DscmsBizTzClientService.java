package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdtz0001.req.Xdtz0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0001.resp.Xdtz0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0003.req.Xdtz0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0003.resp.Xdtz0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.req.Xdtz0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.Xdtz0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0005.req.Xdtz0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0005.resp.Xdtz0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0006.req.Xdtz0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0006.resp.Xdtz0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0007.req.Xdtz0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0007.resp.Xdtz0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0008.req.Xdtz0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0008.resp.Xdtz0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0009.req.Xdtz0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0009.resp.Xdtz0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0010.req.Xdtz0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0010.resp.Xdtz0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0011.req.Xdtz0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0011.resp.Xdtz0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0012.req.Xdtz0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0012.resp.Xdtz0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0013.req.Xdtz0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0013.resp.Xdtz0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0014.req.Xdtz0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0014.resp.Xdtz0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0015.req.Xdtz0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0015.resp.Xdtz0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0016.req.Xdtz0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0016.resp.Xdtz0016DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0017.req.Xdtz0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0017.resp.Xdtz0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0018.req.Xdtz0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0018.resp.Xdtz0018DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0019.req.Xdtz0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0019.resp.Xdtz0019DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0020.req.Xdtz0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0020.resp.Xdtz0020DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0021.req.Xdtz0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0021.resp.Xdtz0021DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0022.req.Xdtz0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0022.resp.Xdtz0022DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0023.req.Xdtz0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0023.resp.Xdtz0023DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0025.req.Xdtz0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0025.resp.Xdtz0025DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0027.req.Xdtz0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0027.resp.Xdtz0027DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0029.req.Xdtz0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0029.resp.Xdtz0029DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0030.req.Xdtz0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0030.resp.Xdtz0030DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.req.Xdtz0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.resp.Xdtz0031DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0032.req.Xdtz0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0032.resp.Xdtz0032DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0033.req.Xdtz0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0033.resp.Xdtz0033DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0034.req.Xdtz0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0034.resp.Xdtz0034DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0035.req.Xdtz0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0035.resp.Xdtz0035DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0036.req.Xdtz0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0036.resp.Xdtz0036DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0037.req.Xdtz0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0037.resp.Xdtz0037DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0038.req.Xdtz0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0038.resp.Xdtz0038DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0039.req.Xdtz0039DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0039.resp.Xdtz0039DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0040.req.Xdtz0040DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0040.resp.Xdtz0040DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0041.req.Xdtz0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0041.resp.Xdtz0041DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0042.req.Xdtz0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0042.resp.Xdtz0042DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0043.req.Xdtz0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0043.resp.Xdtz0043DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0044.req.Xdtz0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0044.resp.Xdtz0044DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0045.req.Xdtz0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0045.resp.Xdtz0045DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0046.req.Xdtz0046DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0046.resp.Xdtz0046DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0047.req.Xdtz0047DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0047.resp.Xdtz0047DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0048.req.Xdtz0048DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0048.resp.Xdtz0048DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0049.req.Xdtz0049DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0049.resp.Xdtz0049DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0050.req.Xdtz0050DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0050.resp.Xdtz0050DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0051.req.Xdtz0051DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0051.resp.Xdtz0051DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0052.req.Xdtz0052DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0052.resp.Xdtz0052DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.req.Xdtz0053DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.resp.Xdtz0053DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0054.req.Xdtz0054DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0054.resp.Xdtz0054DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0055.req.Xdtz0055DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0055.resp.Xdtz0055DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0056.req.Xdtz0056DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0056.resp.Xdtz0056DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0057.req.Xdtz0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0057.resp.Xdtz0057DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0058.req.Xdtz0058DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0058.resp.Xdtz0058DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0059.req.Xdtz0059DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0059.resp.Xdtz0059DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.req.Xdtz0053DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.resp.Xdtz0053DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0060.req.Xdtz0060DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0060.resp.Xdtz0060DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0061.req.Xdtz0061DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0061.resp.Xdtz0061DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0062.req.Xdtz0062DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0062.resp.Xdtz0062DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0063.req.Xdtz0063DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0063.resp.Xdtz0063DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizTzClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 信贷台账服务接口
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizTzClientServiceImpl.class)
public interface DscmsBizTzClientService {

    /**
     * 交易码：xdtz0030
     * 交易描述：查看信贷贴现台账中票据是否已经存在
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0030")
    public ResultDto<Xdtz0030DataRespDto> xdtz0030(Xdtz0030DataReqDto reqDto);

    /**
     * 交易码：xdtz0037
     * 交易描述：无还本续贷额度状态更新
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0037")
    public ResultDto<Xdtz0037DataRespDto> xdtz0037(Xdtz0037DataReqDto reqDto);

    /**
     * 交易码：xdtz0032
     * 交易描述：询客户所担保的行内当前贷款逾期件数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0032")
    public ResultDto<Xdtz0032DataRespDto> xdtz0032(Xdtz0032DataReqDto reqDto);

    /**
     * 交易码：xdtz0033
     * 交易描述：查询客户所担保的行内贷款五级分类非正常状态件数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0033")
    public ResultDto<Xdtz0033DataRespDto> xdtz0033(Xdtz0033DataReqDto reqDto);

    /**
     * 交易码：xdtz0041
     * 交易描述：根据客户号前往信贷查找房贷借据信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0041")
    public ResultDto<Xdtz0041DataRespDto> xdtz0041(Xdtz0041DataReqDto reqDto);

    /**
     * 交易码：xdtz0042
     * 交易描述：申请人在本行当前逾期贷款数量
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0042")
    public ResultDto<Xdtz0042DataRespDto> xdtz0042(Xdtz0042DataReqDto reqDto);

    /**
     * 交易码：xdtz0001
     * 交易描述：客户信息查询(贷款信息)
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0001")
    public ResultDto<Xdtz0001DataRespDto> xdtz0001(Xdtz0001DataReqDto reqDto);

    /**
     * 交易码：xdtz0031
     * 交易描述：根据合同号获取借据信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0031")
    public ResultDto<Xdtz0031DataRespDto> xdtz0031(Xdtz0031DataReqDto reqDto);

    /**
     * 交易码：xdtz0043
     * 交易描述：统计客户行内信用类贷款余额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0043")
    public ResultDto<Xdtz0043DataRespDto> xdtz0043(Xdtz0043DataReqDto reqDto);

    /**
     * 交易码：xdtz0044
     * 交易描述：根据客户号前往信贷查找房贷借据信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0044")
    public ResultDto<Xdtz0044DataRespDto> xdtz0044(Xdtz0044DataReqDto reqDto);

    /**
     * 交易码：xdtz0039
     * 交易描述：根据企业名称查询申请企业在本行是否存在当前逾期贷款
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0039")
    public ResultDto<Xdtz0039DataRespDto> xdtz0039(Xdtz0039DataReqDto reqDto);

    /**
     * 交易码：xdtz0040
     * 交易描述：申请人在本行当前逾期贷款数量
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0040")
    public ResultDto<Xdtz0040DataRespDto> xdtz0040(Xdtz0040DataReqDto reqDto);

    /**
     * 交易码：xdtz0029
     * 交易描述：查询指定票号在信贷台账中是否已贴现
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0029")
    public ResultDto<Xdtz0029DataRespDto> xdtz0029(Xdtz0029DataReqDto reqDto);

    /**
     * 交易码：xdtz0004
     * 交易描述：在查询经营性贷款借据信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0004")
    public ResultDto<Xdtz0004DataRespDto> xdtz0004(Xdtz0004DataReqDto reqDto);

    /**
     * 交易码：xdtz0005
     * 交易描述：根据流水号查询是否放款标记
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0005")
    public ResultDto<Xdtz0005DataRespDto> xdtz0005(Xdtz0005DataReqDto reqDto);

    /**
     * 交易码：xdtz0003
     * 交易描述：查询小微借据余额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0003")
    public ResultDto<Xdtz0003DataRespDto> xdtz0003(Xdtz0003DataReqDto reqDto);

    /**
     * 交易码：xdtz0016
     * 交易描述：通知信贷系统更新贴现台账状态
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0016")
    public ResultDto<Xdtz0016DataRespDto> xdtz0016(Xdtz0016DataReqDto reqDto);

    /**
     * 交易码：xdtz0012
     * 交易描述：根据借款人证件号，判断配偶经营性贷款是否存在余额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0012")
    public ResultDto<Xdtz0012DataRespDto> xdtz0012(Xdtz0012DataReqDto reqDto);

    /**
     * 交易码：xdtz0015
     * 交易描述：贴现记账结果通知
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/biztz4bsp/xdtz0015")
    public ResultDto<Xdtz0015DataRespDto> xdtz0015(Xdtz0015DataReqDto reqDto);

    /**
     * 交易码：xdtz0038
     * 交易描述：台账信息通用列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0038")
    public ResultDto<Xdtz0038DataRespDto> xdtz0038(Xdtz0038DataReqDto reqDto);

    /**
     * 交易码：xdtz0017
     * 交易描述：票据更换（通知信贷更改票据暂用额度台账）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0017")
    public ResultDto<Xdtz0017DataRespDto> xdtz0017(Xdtz0017DataReqDto reqDto);

    /**
     * 交易码：xdtz0010
     * 交易描述：根据身份证号获取借据信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0010")
    public ResultDto<Xdtz0010DataRespDto> xdtz0010(Xdtz0010DataReqDto reqDto);

    /**
     * 交易码：xdtz0008
     * 交易描述：根据客户号获取正常周转次数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0008")
    public ResultDto<Xdtz0008DataRespDto> xdtz0008(Xdtz0008DataReqDto reqDto);


    /**
     * 交易码：xdtz0009
     * 交易描述：查询客户经理不良率
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0009")
    public ResultDto<Xdtz0009DataRespDto> xdtz0009(Xdtz0009DataReqDto reqDto);

    /**
     * 交易码：xdtz0045
     * 交易描述：商贷分户实时查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0045")
    public ResultDto<Xdtz0045DataRespDto> xdtz0045(Xdtz0045DataReqDto reqDto);

    /**
     * 交易码：xdtz0006
     * 交易描述：根据证件号查询借据信息
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/biztz4bsp/xdtz0006")
    public ResultDto<Xdtz0006DataRespDto> xdtz0006(Xdtz0006DataReqDto reqDto);

    /**
     * 交易码：xdtz0007
     * 交易描述：根据客户号获取非信用方式发放贷款的最长到期日
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0007")
    public ResultDto<Xdtz0007DataRespDto> xdtz0007(Xdtz0007DataReqDto reqDto);

    /**
     * 交易码：xdtz0011
     * 交易描述：借据明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0011")
    public ResultDto<Xdtz0011DataRespDto> xdtz0011(Xdtz0011DataReqDto reqDto);

    /**
     * 交易码：xdtz0049
     * 交易描述：客户贷款信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0049")
    public ResultDto<Xdtz0049DataRespDto> xdtz0049(Xdtz0049DataReqDto reqDto);


    /**
     * 交易码：xdtz0027
     * 交易描述：根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0027")
    public ResultDto<Xdtz0027DataRespDto> xdtz0027(Xdtz0027DataReqDto reqDto);

    /**
     * 交易码：xdtz0053
     * 交易描述：个人社会关系查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0053")
    public ResultDto<Xdtz0053DataRespDto> xdtz0053(Xdtz0053DataReqDto reqDto);

    /**
     * 交易码：xdtz0058
     * 交易描述：台账信息通用列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0058")
    public ResultDto<Xdtz0058DataRespDto> xdtz0058(Xdtz0058DataReqDto reqDto);

    /**
     * 交易码：xdtz0047
     * 交易描述：借据信息查询（按证件号）
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/biztz4bsp/xdtz0047")
    public ResultDto<Xdtz0047DataRespDto> xdtz0047(Xdtz0047DataReqDto reqDto);

    /**
     * 交易码：xdtz0057
     * 交易描述：根据流水号查询客户调查的放款信息（在途需求）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0057")
    public ResultDto<Xdtz0057DataRespDto> xdtz0057(Xdtz0057DataReqDto reqDto);

    /**
     * 交易码：xdtz0014
     * 交易描述：保证金补交/冲补交结果通知服务
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0014")
    public ResultDto<Xdtz0014DataRespDto> xdtz0014(Xdtz0014DataReqDto reqDto);

    /**
     * 交易码：xdtz0018
     * 交易描述：额度占用释放接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0018")
    public ResultDto<Xdtz0018DataRespDto> xdtz0018(Xdtz0018DataReqDto reqDto);

    /**
     * 交易码：xdtz0019
     * 交易描述：台账入账
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0019")
    public ResultDto<Xdtz0019DataRespDto> xdtz0019(Xdtz0019DataReqDto reqDto);

    /**
     * 交易码：xdtz0020
     * 交易描述：台账入账（保函）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0020")
    public ResultDto<Xdtz0020DataRespDto> xdtz0020(Xdtz0020DataReqDto reqDto);

    /**
     * 交易码：xdtz0021
     * 交易描述：台账入账（贸易融资\福费廷）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0021")
    public ResultDto<Xdtz0021DataRespDto> xdtz0021(Xdtz0021DataReqDto reqDto);

    /**
     * 交易码：xdtz0022
     * 交易描述：保证金台账入账
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0022")
    public ResultDto<Xdtz0022DataRespDto> xdtz0022(Xdtz0022DataReqDto reqDto);

    /**
     * 交易码：xdtz0023
     * 交易描述：保证金等级入账
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0023")
    public ResultDto<Xdtz0023DataRespDto> xdtz0023(Xdtz0023DataReqDto reqDto);

    /**
     * 交易码：xdtz0025
     * 交易描述：查询指定贷款开始日的优企贷客户贷款余额合计
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0025")
    public ResultDto<Xdtz0025DataRespDto> xdtz0025(Xdtz0025DataReqDto reqDto);

    /**
     * 交易码：xdtz0034
     * 交易描述：根据客户号查询申请人是否有行内信用记录
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0034")
    public ResultDto<Xdtz0034DataRespDto> xdtz0034(Xdtz0034DataReqDto reqDto);

    /**
     * 交易码：xdtz0035
     * 交易描述：根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0035")
    public ResultDto<Xdtz0035DataRespDto> xdtz0035(Xdtz0035DataReqDto reqDto);

    /**
     * 交易码：xdtz0036
     * 交易描述：根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0036")
    public ResultDto<Xdtz0036DataRespDto> xdtz0036(Xdtz0036DataReqDto reqDto);

    /**
     * 交易码：xdtz0046
     * 交易描述：根据借据号获取共同借款人信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0046")
    public ResultDto<Xdtz0046DataRespDto> xdtz0046(Xdtz0046DataReqDto reqDto);

    /**
     * 交易码：xdtz0050
     * 交易描述：对私客户关联业务检查
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0050")
    public ResultDto<Xdtz0050DataRespDto> xdtz0050(Xdtz0050DataReqDto reqDto);

    /**
     * 交易码：xdtz0051
     * 交易描述：对公客户关联业务检查
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0051")
    public ResultDto<Xdtz0051DataRespDto> xdtz0051(Xdtz0051DataReqDto reqDto);

    /**
     * 交易码：xdtz0052
     * 交易描述：根据借据号查询申请人行内还款（利息、本金）该笔借据次数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0052")
    public ResultDto<Xdtz0052DataRespDto> xdtz0052(Xdtz0052DataReqDto reqDto);


    /**
     * 交易码：xdtz0054
     * 交易描述：将需要修改的受托支付账号生成修改记录
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0054")
    public ResultDto<Xdtz0054DataRespDto> xdtz0054(Xdtz0054DataReqDto reqDto);

    /**
     * 交易码：xdtz0055
     * 交易描述：查询退回受托信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0055")
    public ResultDto<Xdtz0055DataRespDto> xdtz0055(Xdtz0055DataReqDto reqDto);

    /**
     * 交易码：xdtz0056
     * 交易描述：查询还款业务类型
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0056")
    public ResultDto<Xdtz0056DataRespDto> xdtz0056(Xdtz0056DataReqDto reqDto);

    /**
     * 交易码：xdtz0013
     * 交易描述：查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0013")
    public ResultDto<Xdtz0013DataRespDto> xdtz0013(Xdtz0013DataReqDto reqDto);

    /**
     * 交易码：xdtz0048
     * 交易描述：小贷借款借据文本生成pdf
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/biztz4bsp/xdtz0048")
    public ResultDto<Xdtz0048DataRespDto> xdtz0048(Xdtz0048DataReqDto reqDto);

    /**
     * 交易码：xdtz0059
     * 交易描述：更新信贷台账信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0059")
    public ResultDto<Xdtz0059DataRespDto> xdtz0059(Xdtz0059DataReqDto reqDto);

    /**
     * 交易码：xdtz0060
     * 交易描述：查询我行有未结清贷款
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0060")
    public ResultDto<Xdtz0060DataRespDto> xdtz0060(Xdtz0060DataReqDto reqDto);

    /**
     * 交易码：xdtz0061
     * 交易描述：小微电子签约借据号生成
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0061")
    public ResultDto<Xdtz0061DataRespDto> xdtz0061(Xdtz0061DataReqDto reqDto);

    /**
     * 交易码：xdtz0062
     * 交易描述：小微电子签约借据号生成
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0062")
    public ResultDto<Xdtz0062DataRespDto> xdtz0062(Xdtz0062DataReqDto reqDto);

    /**
     * 交易码：xdtz0063
     * 交易描述：他行退汇冻结编号更新
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/biztz4bsp/xdtz0063")
    public ResultDto<Xdtz0063DataRespDto> xdtz0063(Xdtz0063DataReqDto reqDto);

}
