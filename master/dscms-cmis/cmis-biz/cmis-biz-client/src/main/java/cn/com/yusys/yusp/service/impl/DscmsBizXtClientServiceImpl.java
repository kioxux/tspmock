package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.req.Xdxt0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.resp.Xdxt0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.req.Xdxt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.resp.Xdxt0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.req.Xdxt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.resp.Xdxt0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.req.Xdxt0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.resp.Xdxt0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.req.Xdxt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.resp.Xdxt0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.req.Xdxt0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.resp.Xdxt0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.req.Xdxt0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.resp.Xdxt0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import cn.com.yusys.yusp.service.DscmsBizXtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsBizXtClientServiceImpl implements DscmsBizXtClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsBizXtClientServiceImpl.class);

    @Override
    public ResultDto<Xdxt0004DataRespDto> xdxt0004(Xdxt0004DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0004.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0004.value));
        return null;
    }

    /**
     * 交易码：xdxt0009
     * 交易描述：客户经理是否为小微客户经理
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0009DataRespDto> xdxt0009(Xdxt0009DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0009.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0009.value));
        return null;
    }

    /**
     * 交易码：xdxt0002
     * 交易描述：根据直营团队类型查询客户经理工号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0002DataRespDto> xdxt0002(Xdxt0002DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0002.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0002.value));
        return null;
    }

    /**
     * 交易码：xdxt0010
     * 交易描述：查询客户经理名单，包括工号、姓名
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0010DataRespDto> xdxt0010(Xdxt0010DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0010.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0010.value));
        return null;
    }
    /**
     * 交易码：xdxt0005
     * 交易描述：查询客户经理所在分部编号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0005DataRespDto> xdxt0005(Xdxt0005DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0005.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0005.value));
        return null;
    }

    /**
     * 交易码：xdxt0006
     * 交易描述：查询信贷用户的特定岗位信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0006DataRespDto> xdxt0006(Xdxt0006DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0006.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0006.value));
        return null;
    }

    /**
     * 客户经理信息通用查询
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdxt0011DataRespDto> xdxt0011(Xdxt0011DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDXT0011.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDXT0011.value));
        return null;
    }

}
