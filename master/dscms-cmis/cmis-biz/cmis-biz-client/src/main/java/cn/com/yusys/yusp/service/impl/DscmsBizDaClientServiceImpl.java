package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdda0001.req.Xdda0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdda0001.resp.Xdda0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsBizDaClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsBizDaClientServiceImpl implements DscmsBizDaClientService {

    private static final Logger logger = LoggerFactory.getLogger(DscmsBizDaClientServiceImpl.class);

//    @Override
//    public ResultDto<Integer> createCentralFileTask(CentralFileTaskDto centralFileTaskdto) {
//        logger.error("档案任务新建失败，触发熔断。");
//        return null;
//    }

    /**
     * 交易码：xdda0001
     * 交易描述：扫描人信息登记
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdda0001DataRespDto> xdda0001(Xdda0001DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDA0001.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDA0001.value));
        return null;
    }
}
