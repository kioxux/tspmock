package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 对外提供服务的实现类
 */
@Component
public class GuarClientServiceImpl implements IGuarClientService {
    private static final Logger logger = LoggerFactory.getLogger(GuarClientServiceImpl.class);

    @Override
    public List<GuarInfGuarantorClientDto> getGuarInfGuarantor(GuarInfGuarantorClientDto guarInfGuarantorDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public GuarClientRsDto updateGuarBusistate(GuarBaseInfoClientDto guarBaseInfoClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }
    
    @Override
    public GuarGrtContRelClientDto getGuarAmtInfoByGuarNos(GuarGrtContRelClientDto guarGrtContRelClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public GuarClientRsDto udpateGuarCertiRela(GuarCertiRelaClientDto guarCertiRelaClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public GuarClientRsDto getGuarCertiRelaByCertiState(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }
    
    @Override
    public GuarClientRsDto getGuarCertiRelaByGuarNo(GuarCertiRelaClientDto guarCertiRelaClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }
    
    @Override
    public GuarClientRsDto getGuarBaseInfoByParams(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

/*    @Override
    public GuarEvalInfoClientDto saveGuarEval(GuarEvalInfoClientDto guarEvalInfo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }*/

/*    @Override
    public List<GuarEvalRelotResultDto> getGuarRelotInfo(GuarRelotInfoClientDto guarRelotInfoClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }*/
}
