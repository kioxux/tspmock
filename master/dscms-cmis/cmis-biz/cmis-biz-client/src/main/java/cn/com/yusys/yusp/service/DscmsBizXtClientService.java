package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.req.Xdxt0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.resp.Xdxt0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.req.Xdxt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.resp.Xdxt0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.req.Xdxt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.resp.Xdxt0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.req.Xdxt0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.resp.Xdxt0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.req.Xdxt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.resp.Xdxt0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.req.Xdxt0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.resp.Xdxt0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.req.Xdxt0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.resp.Xdxt0011DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizXtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizXtClientServiceImpl.class)
public interface DscmsBizXtClientService {

    /**
     * 交易码：xdxt0004
     * 交易描述：根据工号获取所辖区域
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizxt4bsp/xdxt0004")
    public ResultDto<Xdxt0004DataRespDto> xdxt0004(Xdxt0004DataReqDto reqDto);

    /**
     * 交易码：xdxt0009
     * 交易描述：客户经理是否为小微客户经理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizxt4bsp/xdxt0009")
    public ResultDto<Xdxt0009DataRespDto> xdxt0009(Xdxt0009DataReqDto reqDto);

    /* 交易码：xdxt0002
     * 交易描述：根据直营团队类型查询客户经理工号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizxt4bsp/xdxt0002")
    public ResultDto<Xdxt0002DataRespDto> xdxt0002(Xdxt0002DataReqDto reqDto);


    /**
     * 交易码：xdxt0010
     * 交易描述：查询客户经理名单，包括工号、姓名
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizxt4bsp/xdxt0010")
    public ResultDto<Xdxt0010DataRespDto> xdxt0010(Xdxt0010DataReqDto reqDto);


    /**
     * 交易码：xdxt0005
     * 交易描述：查询客户经理所在分部编号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizxt4bsp/xdxt0005")
    public ResultDto<Xdxt0005DataRespDto> xdxt0005(Xdxt0005DataReqDto reqDto);

    /**
     * 交易码：xdxt0006
     * 交易描述：查询信贷用户的特定岗位信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizxt4bsp/xdxt0006")
    public ResultDto<Xdxt0006DataRespDto> xdxt0006(Xdxt0006DataReqDto reqDto);

    /**
     * 交易码：xdxt0011
     * 交易描述：客户经理信息通用查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizxt4bsp/xdxt0011")
    public ResultDto<Xdxt0011DataRespDto> xdxt0011(Xdxt0011DataReqDto reqDto);
}
