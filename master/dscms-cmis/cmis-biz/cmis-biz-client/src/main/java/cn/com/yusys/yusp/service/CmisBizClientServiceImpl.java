package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.req.CmisBiz0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.resp.CmisBiz0001RespDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.req.CmisBiz0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.resp.CmisBiz0002RespDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.req.CmisBiz0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.resp.CmisBiz0003RespDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * cmis-biz服务对外提供服务接口实现类
 *
 * @author monchi
 * @date 2020-12-19 11:50:00
 */
@Component
public class CmisBizClientServiceImpl implements CmisBizClientService {

    private static final Logger logger = LoggerFactory.getLogger(CmisBizClientService.class);

    /**
     * 单一客户授信复审
     *
     * @param cmisBiz0001ReqDto
     * @return
     */
    @Override
    public ResultDto<CmisBiz0001RespDto> cmisBiz0001(CmisBiz0001ReqDto cmisBiz0001ReqDto) {
        logger.error("访问接口失败！");
        return null;
    }

    /**
     * 集团客户授信复审
     *
     * @param cmisBiz0002ReqDto
     * @return
     */
    @Override
    public ResultDto<CmisBiz0002RespDto> cmisBiz0002(CmisBiz0002ReqDto cmisBiz0002ReqDto) {
        logger.error("访问接口失败！");
        return null;
    }

    @Override
    public ResultDto<CmisBiz0003RespDto> cmisBiz0003(CmisBiz0003ReqDto cmisBiz0002ReqDto) {
        logger.error("访问接口失败！");
        return null;
    }

    @Override
    public BizGuarExchangeDto saveGrtGuarContRelInter(BizGuarExchangeDto bizGuarExchangeDto) {
        logger.error("访问接口失败！");
        return null;
    }

    @Override
    public BizGuarExchangeDto queryGrtGuarContRelInter(BizGuarExchangeDto bizGuarExchangeDto) {
        logger.error("访问接口失败！");
        return null;
    }

    @Override
    public List<GrtGuarContClientDto> getGrtGuarCont(GrtGuarContClientDto grtGuarContClientDto) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public List<BizCtrLoanContDto> getCtrLoanCont(Map map) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public ResultDto<List<BizCtrLoanContDto>> queryAllBizCtrLoanContDto(@RequestBody QueryModel queryModel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    /**
     * 全表查询 放款申请dto.
     *
     * @return
     */
    @Override
    public List<BizPvpLoanAppDto> queryPvpLoanAppDto(@RequestBody QueryModel queryModel) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public List<BizAccLoanDto> getAccLoan(BizAccLoanDto bizAccLoanDto) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public List<String> getIqpCertiInoutRelInGuarNo(Map map) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public List<Map> getBusRows(Map map) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public List<Map> getBusAmt(Map map) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public List<Map> getBusBalance(Map map) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }


    @Override
    public List<String> getContNumbers(String lmtctrno) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public List<String> getIqpSernoNumbers(String lmtCtrNo) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }


    /**
     * 注销合作方授信协议  校验逻辑  返回生效状态的合同数量
     *
     * @param lmtCtrNo
     * @return
     */
    @Override
    public Integer checkRoot(String lmtCtrNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public Integer getZaiTuAppCount(String lmtCtrNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<AccLoanDto>> queryAccLoanByContNoForNpam(@RequestBody List<String> contNoList) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<GrtGuarContClientDto>> queryGrtGuarContByContNo(@RequestBody List<String> contNoList) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> createCentralFileTask(CentralFileTaskDto centralFileTaskdto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> createDocArchiveBySys(DocArchiveClientDto archiveClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> createDocImageSpplBySys(DocImageSpplClientDto docImageSpplClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> createDocCreateArchiveBySys(DocCreditArchiveClientDto docCreditArchiveClientDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public List<String> getIqpsernoByLmtCtrNo(Map map) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public List<String> getContnoByLmtCtrNo(Map map) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }

    @Override
    public List<BizCtrLoanContDto> getContDataByLmtLimitNo(Map queryMap) {
        logger.error("访问失败，触发熔断。");
        return Collections.emptyList();
    }


    @Override
    public BusRemindDto getCallbackInfoByInstanceId(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public BusRemindDto getRefuseInfoByInstanceId(Map map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public BizGuarExchangeDto queryBizServiceStates(BizGuarExchangeDto bizGuarExchangeDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public Integer queryLmtSurveyReportByCusNo(String certCode) {
        logger.error("访问失败，触发熔断。");
        return 0;
    }

    @Override
    public ResultDto<List<AccLoanDto>> selectByCusIdOnAssetType(String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<AccLoanDto>> selectByCusId(AccLoanDto accloan) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<AccLoanDto>> selectByCusIdNotType(@RequestBody String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> selectXwztCount(@RequestBody String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> invalidByBizSerno(String serno) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> createCreditAuto(CreditReportQryLstAndRealDto creditReportQryLstAndRealDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> updateAccLoanByBillNo(Map<String, String> map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<GuarBaseInfoClientDto>> selectByGuarBaseInfo(@RequestBody String guarContNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<List<GuarGuaranteeDto>> queryGuarGuaranteeByGuarContNo(@RequestBody String guarContNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<BigDecimal> selectOpenTotalLmtAmtByCusId(@RequestBody String cusId) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> updateByPrimaryKeySelective(BizCusLstZxdDto bizCusLstZxdDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> updateAccLoan(List<AccLoanDto> accLoanDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> getMaxOverdueDay(List<String> billNos) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<BizCusLstZxdDto> selectByPrimaryKey(String serno) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> updatestatusbyprono(@RequestBody CoopProAccInfoDto coopProAccInfoDto) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public Map<String, List<String>> queryManagerByXwRole(String userNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public Map<String, List<String>> loadXwAreaManagerDataAuthWithUser() {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public String selectAccCvrsBillNo(String pvpSerno) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<BizCtrLoanContDto> selectCtrLoanContByContNo(String contNo) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public String selectAccTfLocBillNo(String pvpSerno) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<AccLoanDto> selectClassByBillNo(AccLoanDto accloan) {
        logger.error("访问失败，触发熔断。");
        return null;
    }

    @Override
    public ResultDto<Integer> updateOtherLoanByCusId(Map<String, String> map) {
        logger.error("访问失败，触发熔断。");
        return null;
    }
}
