package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.req.CmisBiz0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0001.resp.CmisBiz0001RespDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.req.CmisBiz0002ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0002.resp.CmisBiz0002RespDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.req.CmisBiz0003ReqDto;
import cn.com.yusys.yusp.dto.server.cmisbiz0003.resp.CmisBiz0003RespDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * cmis-biz服务对外提供服务接口
 *
 * @author monchi
 * @date 2020-12-19 11:50:00
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = CmisBizClientServiceImpl.class)
public interface CmisBizClientService {
    /**
     * 单一客户授信复审
     *
     * @param cmisBiz0001ReqDto
     * @return
     */
    @PostMapping("/biz4inner/cmisbiz0001")
    public ResultDto<CmisBiz0001RespDto> cmisBiz0001(@RequestBody CmisBiz0001ReqDto cmisBiz0001ReqDto);
    /**
     * 集团客户授信复审
     *
     * @param cmisBiz0002ReqDto
     * @return
     */
    @PostMapping("/biz4inner/cmisbiz0002")
    public ResultDto<CmisBiz0002RespDto> cmisBiz0002(@RequestBody CmisBiz0002ReqDto cmisBiz0002ReqDto);

    /**
     * 获取机构余额
     * @param cmisBiz0002ReqDto
     * @return
     */
    @PostMapping("/biz4inner/cmisbiz0003")
    public ResultDto<CmisBiz0003RespDto> cmisBiz0003(@RequestBody CmisBiz0003ReqDto cmisBiz0002ReqDto);

    /**
     * 保存押品与担保合同关系接口，调用担保合同服务中的添加关系方法
     *
     * @param bizGuarExchangeDto
     * @return
     */
    @PostMapping("/grtguarcontrel/saveGrtGuarContRelInter")
    public BizGuarExchangeDto saveGrtGuarContRelInter(@RequestBody BizGuarExchangeDto bizGuarExchangeDto);

    /**
     * 查询担保合同与押品关系数据接口，调用担保合同服务中的查询方法
     */
    @PostMapping("/grtguarcontrel/queryGrtGuarContRelInter")
    public BizGuarExchangeDto queryGrtGuarContRelInter(@RequestBody BizGuarExchangeDto bizGuarExchangeDto);

    /**
     * 获取担保合同信息
     *
     * @param grtGuarContClientDto
     * @return
     */
    @PostMapping(value = "/grtguarcont/getGrtGuarCont")
    public List<GrtGuarContClientDto> getGrtGuarCont(@RequestBody GrtGuarContClientDto grtGuarContClientDto);

    /**
     * 获取合同待签数据
     *
     * @param map
     * @return
     */
    @PostMapping(value = "/ctrloancont/getCtrLoanCont")
    public List<BizCtrLoanContDto> getCtrLoanCont(@RequestBody Map map);

    /**
     * 获取贷款台账信息
     *
     * @param bizAccLoanDto
     * @return
     */
    @PostMapping(value = "/accloan/getAccLoan")
    public List<BizAccLoanDto> getAccLoan(@RequestBody BizAccLoanDto bizAccLoanDto);

    /**
     * 获取权证与权证出入库申请的关系表中的押品统一编号
     *
     * @param map
     * @return
     */
    @PostMapping("/iqpcertiinoutrel/getIqpCertiInoutRelInGuarNo")
    public List<String> getIqpCertiInoutRelInGuarNo(@RequestBody Map map);

    /**
     * 业务统计-笔数
     *
     * @param map
     * @return
     */
    @PostMapping("/ctrloancont/getbusrows")
    public List<Map> getBusRows(@RequestBody Map map);

    /**
     * 业务统计-金额（万元）
     *
     * @param map
     * @return
     */
    @PostMapping("/ctrloancont/getbusamt")
    public List<Map> getBusAmt(@RequestBody Map map);

    /**
     * 业务统计-余额（万元）
     *
     * @param map
     * @return
     */
    @PostMapping("/accloan/getbusbalance")
    public List<Map> getBusBalance(@RequestBody Map map);


    /**
     * 根据合作方协议编号  查询下属合同编号  返回值为<String>集合
     *
     * @param lmtctrno
     * @return
     */
    @PostMapping("/ctrloancont/getContNumbers")
    public List<String> getContNumbers(@RequestBody String lmtctrno);

    /**
     * 根据合作方协议编号  查询下属在途合同申请的流水号编号  返回值为<String>集合
     *
     * @param lmtCtrNo
     * @return
     */
    @PostMapping("/iqploanapp/getIqpSernoNumbers")
    List<String> getIqpSernoNumbers(@RequestBody String lmtCtrNo);

    /**
     * 注销合作方授信协议  校验逻辑  返回生效状态的合同数量
     *
     * @param lmtCtrNo
     * @return
     */
    @PostMapping("/ctrloancont/getCount")
    Integer checkRoot(@RequestBody String lmtCtrNo);

    /**
     * 注销合作方授信协议  校验逻辑  返回在途状态的合同申请数量
     *
     * @param lmtCtrNo
     * @return
     */
    @PostMapping("/iqploanapp/getZaiTuAppCount")
    Integer getZaiTuAppCount(@RequestBody String lmtCtrNo);

    /**
     * 根据个人授信额度分项编号获取 在途业务申请流水号（特殊业务、额度项下业务）
     *
     * @param map
     * @return
     */
    @PostMapping("/iqploanapp/getIqpsernoByLmtCtrNo")
    public List<String> getIqpsernoByLmtCtrNo(@RequestBody Map map);

    /**
     * 根据个人授信协议编号获取合同信息：500中止600注销700撤回800作废
     *
     * @param queryMap
     * @return
     */
    @PostMapping("/ctrlmtrel/getContnoByLmtCtrNo")
    public List<String> getContnoByLmtCtrNo(Map queryMap);

    /**
     * 根据授信台账编号获取合同信息
     *
     * @param queryMap
     * @return
     */
    @PostMapping("/ctrloancont/getContDataByLmtLimitNo")
    public List<BizCtrLoanContDto> getContDataByLmtLimitNo(Map queryMap);

    /**
     * 全表查询 合同签订dto.
     *
     * @return
     */
    @PostMapping("/ctrloancont/query/all/queryAllBizCtrLoanContDto")
    public ResultDto<List<BizCtrLoanContDto>> queryAllBizCtrLoanContDto(@RequestBody QueryModel queryModel);

    /**
     * 全表查询 放款申请dto.
     *
     * @return
     */
    @PostMapping("/pvploanapp/query/all/queryPvpLoanAppDto")
    public List<BizPvpLoanAppDto> queryPvpLoanAppDto(@RequestBody QueryModel queryModel);

    /**
     * 根据流程实例id查询关联的当前登陆账号的所有被打回节点信息
     *
     * @param map
     * @return
     */
    @PostMapping("/flowdata/callbackInfoByInstanceId")
    public BusRemindDto getCallbackInfoByInstanceId(@RequestBody Map map);

    /**
     * 根据流程实例id查询关联的当前登陆账号的所有被否决节点信息
     *
     * @param map
     * @return
     */
    @PostMapping("/flowdata/refuseInfoByInstanceId")
    public BusRemindDto getRefuseInfoByInstanceId(@RequestBody Map map);

    /**
     * 获取押品所在业务阶段
     *
     * @param bizGuarExchangeDto
     * @return
     */
    @PostMapping("/client/queryBizServiceStates")
    public BizGuarExchangeDto queryBizServiceStates(@RequestBody BizGuarExchangeDto bizGuarExchangeDto);

    /**
     * 根据客户证件号查新信贷调查主表
     *
     * @param certCode
     * @return
     */
    @PostMapping("/lmtsurveyreportbasicinfo/selecylmtsurveyreportbasicinfo")
    public Integer queryLmtSurveyReportByCusNo(@RequestBody String certCode);

    /**
     * 查看非不良贷款条件查询
     *
     * @param cusId
     * @return
     */
    @PostMapping("/accloan/selectByCusIdOnAssetType")
    public ResultDto<List<AccLoanDto>> selectByCusIdOnAssetType(@RequestBody String cusId);

    /**
     * 根据客户编号查询借据编号，客户名称是否存在
     *
     * @param
     * @return
     */
    @PostMapping("/accloan/selectByCusId")
    public ResultDto<List<AccLoanDto>> selectByCusId(@RequestBody AccLoanDto accloan);

    /**
     * 查看不良贷款条件查询
     *
     * @param cusId
     * @return
     */
    @PostMapping("/accloan/selectByCusIdNotType")
    public ResultDto<List<AccLoanDto>> selectByCusIdNotType(@RequestBody String cusId);

    /**
     * 通过合同编号查询合同下未结清借据
     *
     * @param
     * @return
     * @创建人：周茂伟
     */
    @PostMapping("/accloan/queryAccLoanByContNoForNpam")
    public ResultDto<List<AccLoanDto>> queryAccLoanByContNoForNpam(@RequestBody List<String> contNoList);

    /**
     * 通过合同编号查询担保合同
     *
     * @param
     * @return
     * @创建人：周茂伟
     */
    @PostMapping("/grtguarcont/queryGrtGuarContByContNo")
    public ResultDto<List<GrtGuarContClientDto>> queryGrtGuarContByContNo(@RequestBody List<String> contNoList);

    /**
     * 创建档案任务
     *
     * @param centralFileTaskdto
     * @return
     */
    @PostMapping("/centralfiletask/create")
    public ResultDto<Integer> createCentralFileTask(@RequestBody CentralFileTaskDto centralFileTaskdto);

    /**
     * 系统生成档案归档信息
     *
     * @author jijian_yx
     * @date 2021/6/29 20:10
     **/
    @PostMapping("/docarchiveinfo/createDocArchiveBySys")
    public ResultDto<Integer> createDocArchiveBySys(@RequestBody DocArchiveClientDto archiveClientDto);

    /**
     * 系统生成影像补扫
     *
     * @author jijian_yx
     * @date 2021/6/30 10:45
     **/
    @PostMapping("/docimagespplinfo/createDocImageSpplBySys")
    public ResultDto<Integer> createDocImageSpplBySys(@RequestBody DocImageSpplClientDto docImageSpplClientDto);

    /**
     * 系统生成人行征信档案归档任务
     *
     * @author jijian_yx
     * @date 2021/7/1 10:21
     **/
    @PostMapping("/doccreditarchiveinfo/createDocCreateArchiveBySys")
    public ResultDto<Integer> createDocCreateArchiveBySys(@RequestBody DocCreditArchiveClientDto docCreditArchiveClientDto);

    /**
     * 根据客户号查询是否有在途业务-小微客户批量移交
     *
     * @param cusId
     * @return
     */
    @PostMapping("/iqploanapp/selectXwztCount")
    public ResultDto<Integer> selectXwztCount(@RequestBody String cusId);

    /**
     * 根据业务流水号作废"等待入库"状态归档任务和台账
     *
     * @author jijian_yx
     * @date 2021/7/22 16:31
     **/
    @PostMapping("/docarchiveinfo/invalidByBizSerno")
    public ResultDto<Integer> invalidByBizSerno(@RequestBody String serno);

    /**
     * 创建业务征信信息
     *
     * @param creditReportQryLstAndRealDto
     * @return
     */
    @PostMapping("/creditreportqrylst/createcreditauto")
    public ResultDto<Integer> createCreditAuto(@RequestBody CreditReportQryLstAndRealDto creditReportQryLstAndRealDto);

    /**
     * 更新台账五级/十级分类
     *
     * @author jijian_yx
     * @date 2021/8/10 23:23
     **/
    @PostMapping("/accloan/updateAccLoanByBillNo")
    public ResultDto<Integer> updateAccLoanByBillNo(@RequestBody Map<String, String> map);


    /**
     * 通过担保合同编号查询抵质押基本信息
     *
     * @param
     * @return
     * @创建人：liuquan
     */
    @PostMapping("/guarbaseinfo/selectByGuarBaseInfo")
    public ResultDto<List<GuarBaseInfoClientDto>> selectByGuarBaseInfo(@RequestBody String guarContNo);

    /**
     * 根据担保合同编号关联查询保证人信息
     *
     * @param guarContNo
     * @return
     * liuquan
     */
    @PostMapping("/guarguarantee/queryGuarGuaranteeByGuarContNo")
    public ResultDto<List<GuarGuaranteeDto>> queryGuarGuaranteeByGuarContNo(@RequestBody String guarContNo);

    /**
     * 查询客户的敞口额度合计
     *
     * @param cusId
     * @return
     * liuquan
     */
    @PostMapping("/lmtreply/selectopentotallmtamtbycusid")
    public ResultDto<BigDecimal> selectOpenTotalLmtAmtByCusId(@RequestBody String cusId);

    /**
     * 查询客户的敞口额度合计
     *
     * @param coopProAccInfoDto
     * @return
     * xc
     */
    @PostMapping("/coopproaccinfo/updatestatusbyprono")
    public ResultDto<Integer> updatestatusbyprono(@RequestBody CoopProAccInfoDto coopProAccInfoDto);

    /**
     * 根据逐渐查询增享贷信息
     *
     * @param
     * @return
     * shangzy
     */
    @PostMapping("/cuslstzxd/selectbyprimarykey")
    public ResultDto<BizCusLstZxdDto> selectByPrimaryKey(String serno);

    /**
     * 更新增享贷信息
     * @param
     * @return
     * shangzy
     */
    @PostMapping("/cuslstzxd/updatebyprimarykeyselective")
    public ResultDto<Integer> updateByPrimaryKeySelective(@RequestBody BizCusLstZxdDto bizCusLstZxdDto);

    /**
     * 修改借据信息
     * @param accLoanDto
     * @return
     * @throws URISyntaxException
     */
    @PostMapping("/accloan/updateAccLoan")
    public ResultDto<Integer> updateAccLoan(@RequestBody List<AccLoanDto> accLoanDto);

    /**
     * 获取借据下最大逾期天数
     * @author jijian_yx
     * @date 2021/10/13 22:48
     **/
    @PostMapping("/accloan/getmaxoverdueday")
    public ResultDto<Integer> getMaxOverdueDay(@RequestBody List<String> billNos);

    /**
     * 获取 小微分中心/小微分部部长下的客户经理
     * @创建人：周茂伟
     * @return
     */
    @PostMapping(value = "/xwcommonservice/queryManagerByXwRole")
    public Map<String, List<String>> queryManagerByXwRole(String userNo);

    /**
     * 获取  获取所有小微区域管理人员（分中心负责人、分部部长）下的小微客户经理
     * @创建人：周茂伟
     * @return
     */
    @PostMapping(value = "/xwcommonservice/loadXwAreaManagerDataAuthWithUser")
    public Map<String, List<String>> loadXwAreaManagerDataAuthWithUser();

    /**
     * 查询国界存量数据借据号
     * @创建人：李召星
     * @return
     */
    @PostMapping(value = "/acccvrs/selectacccvrsbillno")
    public String selectAccCvrsBillNo(String pvpSerno);

    /**
     * 根据合同号查询合同信息（XDKH0023获取还款日合同到期日）
     * @param
     * @return
     */
    @PostMapping("/ctrloancont/selectCtrLoanContByContNo")
    public ResultDto<BizCtrLoanContDto> selectCtrLoanContByContNo(@RequestBody String contNo);

    /**
     * 查询国界存量数据借据号
     * @创建人：李召星
     * @return
     */
    @PostMapping(value = "/acctfloc/selectacctflocbillno")
    public String selectAccTfLocBillNo(String pvpSerno);



    /**
     * 根据借据号查询借据信息（获取五级十级分类）
     *
     * @param
     * @return
     */
    @PostMapping("/accloan/selectclassbybillno")
    public ResultDto<AccLoanDto> selectClassByBillNo(@RequestBody AccLoanDto accloan);

    /**
     * 更新客户名下未结清的银承、保函、开证、委托贷款台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 22:49
     **/
    @PostMapping("/docAccList/updateOtherLoanByCusId")
    public ResultDto<Integer> updateOtherLoanByCusId(@RequestBody Map<String, String> map);
}
