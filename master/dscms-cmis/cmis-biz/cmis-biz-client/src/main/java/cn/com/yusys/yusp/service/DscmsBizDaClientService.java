package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdda0001.req.Xdda0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdda0001.resp.Xdda0001DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizDaClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 档案管理服务接口
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizDaClientServiceImpl.class)
public interface DscmsBizDaClientService {
//    public ResultDto<Integer> createCentralFileTask(CentralFileTaskDto centralFileTaskdto);

    /**
     * 交易码：xdda0001
     * 交易描述：扫描人信息登记
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizda4bsp/xdda0001")
    public ResultDto<Xdda0001DataRespDto>  xdda0001(Xdda0001DataReqDto reqDto);
}
