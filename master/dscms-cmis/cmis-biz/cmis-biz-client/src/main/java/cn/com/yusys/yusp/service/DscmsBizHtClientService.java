package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdht0001.req.Xdht0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0001.resp.Xdht0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0002.req.Xdht0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0002.resp.Xdht0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0003.req.Xdht0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0003.resp.Xdht0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0004.req.Xdht0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0004.resp.Xdht0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0005.req.Xdht0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0005.resp.Xdht0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0006.req.Xdht0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0006.resp.Xdht0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0007.req.Xdht0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0007.resp.Xdht0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0008.req.Xdht0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0008.resp.Xdht0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0009.req.Xdht0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0009.resp.Xdht0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0010.req.Xdht0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0010.resp.Xdht0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0011.req.Xdht0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.Xdht0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0012.req.Xdht0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0012.resp.Xdht0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0013.req.Xdht0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0013.resp.Xdht0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0014.req.Xdht0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0014.resp.Xdht0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0015.req.Xdht0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0015.resp.Xdht0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0016.req.Xdht0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0016.resp.Xdht0016DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0017.req.Xdht0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0017.resp.Xdht0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0018.req.Xdht0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0018.resp.Xdht0018DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0019.req.Xdht0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0019.resp.Xdht0019DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0020.req.Xdht0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0021.req.Xdht0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0021.resp.Xdht0021DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0022.req.Xdht0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0022.resp.Xdht0022DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0023.req.Xdht0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0023.resp.Xdht0023DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0024.req.Xdht0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0024.resp.Xdht0024DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0025.req.Xdht0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0025.resp.Xdht0025DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0026.req.Xdht0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0026.resp.Xdht0026DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0027.req.Xdht0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0027.resp.Xdht0027DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0028.req.Xdht0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0028.resp.Xdht0028DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0029.req.Xdht0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0029.resp.Xdht0029DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0030.req.Xdht0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0030.resp.Xdht0030DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0031.req.Xdht0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0031.resp.Xdht0031DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0032.req.Xdht0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0032.resp.Xdht0032DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0033.req.Xdht0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0033.resp.Xdht0033DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0034.req.Xdht0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0034.resp.Xdht0034DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0036.req.Xdht0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0036.resp.Xdht0036DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0037.req.Xdht0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0037.resp.Xdht0037DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0038.req.Xdht0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0038.resp.Xdht0038DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0040.req.Xdht0040DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0040.resp.Xdht0040DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0041.req.Xdht0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0041.resp.Xdht0041DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0042.req.Xdht0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0042.resp.Xdht0042DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0043.req.Xdht0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0043.resp.Xdht0043DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0044.req.Xdht0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0044.resp.Xdht0044DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0045.req.Xdht0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0045.resp.Xdht0045DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizHtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 合同管理服务接口
 *
 * @author zhugenrong
 * @author leehuang
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizHtClientServiceImpl.class)
public interface DscmsBizHtClientService {

    /**
     * 交易码：xdht0001
     * 交易描述：查询省心E付合同信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0001")
    public ResultDto<Xdht0001DataRespDto>  xdht0001(Xdht0001DataReqDto reqDto);
    /**
     * 交易码：xdht0002
     * 交易描述：银承协议查询接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0002")
    public ResultDto<Xdht0002DataRespDto> xdht0002(Xdht0002DataReqDto reqDto);

    /**
     * 交易码：xdht0003
     * 交易描述：信贷贴现合同号查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0003")
    public ResultDto<Xdht0003DataRespDto> xdht0003(Xdht0003DataReqDto reqDto);

    /**
     * 交易码：xdht0004
     * 交易描述：合同信息列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0004")
    public ResultDto<Xdht0004DataRespDto> xdht0004(Xdht0004DataReqDto reqDto);

    /**
     * 交易码：xdht0005
     * 交易描述：合同面签信息列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0005")
    public ResultDto<Xdht0005DataRespDto> xdht0005(Xdht0005DataReqDto reqDto);

    /**
     * 交易码：xdht0006
     * 交易描述：双录流水与合同号同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0006")
    public ResultDto<Xdht0006DataRespDto>  xdht0006(Xdht0006DataReqDto reqDto);

    /**
     * 交易码：xdht0008
     * 交易描述：借款合同双录流水同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0008")
    public ResultDto<Xdht0008DataRespDto>  xdht0008(Xdht0008DataReqDto reqDto);

    /**
     * 交易码：xdht0010
     * 交易描述：查询符合条件的省心快贷合同
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0010")
    public ResultDto<Xdht0010DataRespDto>  xdht0010(Xdht0010DataReqDto reqDto);

    /**
     * 交易码：xdht0011
     * 交易描述：查询符合条件的省心快贷合同
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0011")
    public ResultDto<Xdht0011DataRespDto>  xdht0011(Xdht0011DataReqDto reqDto);

    /**
     * 交易码：xdht0012
     * 交易描述：借款担保合同签订/支用
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0012")
    public ResultDto<Xdht0012DataRespDto>  xdht0012(Xdht0012DataReqDto reqDto);

    /**
     * 交易码：xdht0015
     * 交易描述：合同房产人员信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0015")
    public ResultDto<Xdht0015DataRespDto>  xdht0015(Xdht0015DataReqDto reqDto);

    /**
     * 交易码：xdht0018
     * 交易描述：担保合同文本生成pdf
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0018")
    public ResultDto<Xdht0018DataRespDto>  xdht0018(Xdht0018DataReqDto reqDto);

    /**
     * 交易码：xdht0021
     * 交易描述：合同签订
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0021")
    public ResultDto<Xdht0021DataRespDto>  xdht0021(Xdht0021DataReqDto reqDto);

    /**
     * 交易码：xdht0023
     * 交易描述：信贷系统信贷合同生成接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0023")
    public ResultDto<Xdht0023DataRespDto>  xdht0023(Xdht0023DataReqDto reqDto);

    /**
     * 交易码：xdht0044
     * 交易描述：房群客户查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0044")
    public ResultDto<Xdht0044DataRespDto> xdht0044(Xdht0044DataReqDto reqDto);

    /**
     * 交易码：xdht0041
     * 交易描述：查询受托记录状态
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0041")
    public ResultDto<Xdht0041DataRespDto> xdht0041(Xdht0041DataReqDto reqDto);

    /**
     * 交易码：xdht0014
     * 交易描述：合同签订查询VX
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0014")
    public ResultDto<Xdht0014DataRespDto> xdht0014(Xdht0014DataReqDto reqDto);

    /**
     * 交易码：xdht0027
     * 交易描述：根据客户调查表编号取得贷款合同主表的合同状态
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0027")
    public ResultDto<Xdht0027DataRespDto> xdht0027(Xdht0027DataReqDto reqDto);

    /**
     * 交易码：xdht0007
     * 交易描述：借款合同/担保合同列表信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0007")
    public ResultDto<Xdht0007DataRespDto> xdht0007(Xdht0007DataReqDto reqDto);


    /**
     * 交易码：xdht0024
     * 交易描述：合同详情查看
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0024")
    public ResultDto<Xdht0024DataRespDto> xdht0024(Xdht0024DataReqDto reqDto);


    /**
     * 交易码：xdht0028
     * 交易描述：根据合同号取得客户经理电话号码
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/bizht4bsp/xdht0028")
    public ResultDto<Xdht0028DataRespDto> xdht0028(Xdht0028DataReqDto reqDto);

    /**
     * 交易码：xdht0040
     * 交易描述：修改受托信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0040")
    public ResultDto<Xdht0040DataRespDto> xdht0040(Xdht0040DataReqDto reqDto);

    /**
     * 交易码：xdht0019
     * 交易描述：合同生成
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0019")
    public ResultDto<Xdht0019DataRespDto> xdht0019(Xdht0019DataReqDto reqDto);

    /**
     * 交易码：xdht0013
     * 交易描述：合同信息查询(微信小程序)
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0013")
    public ResultDto<Xdht0013DataRespDto> xdht0013(Xdht0013DataReqDto reqDto);

    /**
     * 交易码：xdht0017
     * 交易描述：借款、担保合同PDF生成
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0017")
    public ResultDto<Xdht0017DataRespDto> xdht0017(Xdht0017DataReqDto reqDto);

    /**
     * 交易码：xdht0030
     * 交易描述：查询客户我行合作次数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0030")
    public ResultDto<Xdht0030DataRespDto> xdht0030(Xdht0030DataReqDto reqDto);

    /**
     * 交易码：xdht0020
     * 交易描述：合同信息查看
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0020")
    public ResultDto<Xdht0020DataRespDto> xdht0020(Xdht0020DataReqDto reqDto);

    /**
     * 交易码：xdht0032
     * 交易描述：根据核心客户号查询我行信用类合同金额汇总
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0032")
    public ResultDto<Xdht0032DataRespDto> xdht0032(Xdht0032DataReqDto reqDto);

    /**
     * 交易码：xdht0033
     * 交易描述：根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0033")
    public ResultDto<Xdht0033DataRespDto> xdht0033(Xdht0033DataReqDto reqDto);

    /**
     * 交易码：xdht0034
     * 交易描述：根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0034")
    public ResultDto<Xdht0034DataRespDto> xdht0034(Xdht0034DataReqDto reqDto);

    /**
     * 交易码：xdht0026
     * 交易描述：根据zheng获取信贷合同信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0026")
    public ResultDto<Xdht0026DataRespDto> xdht0026(Xdht0026DataReqDto reqDto);

    /**
     * 交易码：xdht0016
     * 交易描述：合同签订维护（优抵贷不见面抵押签约）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0016")
    public ResultDto<Xdht0016DataRespDto> xdht0016(Xdht0016DataReqDto reqDto);

    /**
     * 交易码：xdht0022
     * 交易描述：合同信息列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0022")
    public ResultDto<Xdht0022DataRespDto> xdht0022(Xdht0022DataReqDto reqDto);

    /**
     * 交易码：xdht0036
     * 交易描述：根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0036")
    public ResultDto<Xdht0036DataRespDto> xdht0036(Xdht0036DataReqDto reqDto);

    /**
     * 交易码：xdht0042
     * 交易描述：优企贷、优农贷合同信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0042")
    public ResultDto<Xdht0042DataRespDto> xdht0042(Xdht0042DataReqDto reqDto);

    /**
     * 交易码：xdht0009
     * 交易描述：银承合同信息列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0009")
    public ResultDto<Xdht0009DataRespDto> xdht0009(Xdht0009DataReqDto reqDto);

    /**
     * 交易码：xdht0037
     * 交易描述：大家e贷、乐悠金根据客户号查询房贷首付款比例
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0037")
    public ResultDto<Xdht0037DataRespDto> xdht0037(Xdht0037DataReqDto reqDto);

    /**
     * 交易码：xdht0038
     * 交易描述：乐悠金根据核心客户号查询房贷首付款比例进行额度计算
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0038")
    public ResultDto<Xdht0038DataRespDto> xdht0038(Xdht0038DataReqDto reqDto);


    /**
     * 交易码：xdht0025
     * 交易描述： *接口处理类:合同签订/撤销（优享贷和增享贷）
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0025")
    public ResultDto<Xdht0025DataRespDto> xdht0025(Xdht0025DataReqDto reqDto);

    /**
     * 交易码：xdht0029
     * 交易描述： 接口处理类:客户非小微业务经营性贷款总金额查询
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0029")
    public ResultDto<Xdht0029DataRespDto> xdht0029(Xdht0029DataReqDto reqDto);

    /**
     * 交易码：xdht0029
     * 交易描述： 接口处理类:客户非小微业务经营性贷款总金额查询
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0031")
    public ResultDto<Xdht0031DataRespDto> xdht0031(Xdht0031DataReqDto reqDto);

    /**
     * 交易码：xdht0043
     * 交易描述： 小贷借款合同文本生成pdf
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0043")
    public ResultDto<Xdht0043DataRespDto> xdht0043(Xdht0043DataReqDto reqDto);

    /**
     * 交易码：xdht0045
     * 交易描述： 查询信贷客户名称合同状态
     * @param reqDto
     * @return
     */
    @PostMapping("/bizht4bsp/xdht0045")
    public ResultDto<Xdht0045DataRespDto> xdht0045(Xdht0045DataReqDto reqDto);
}
