package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xddh0001.req.Xddh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0001.resp.Xddh0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0002.req.Xddh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0002.resp.Xddh0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0003.req.Xddh0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0003.resp.Xddh0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0004.req.Xddh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0004.resp.Xddh0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0005.resp.Xddh0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0006.req.Xddh0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0006.resp.Xddh0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0007.req.Xddh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0007.resp.Xddh0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0008.req.Xddh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0008.resp.Xddh0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0009.req.Xddh0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0009.resp.Xddh0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0010.req.Xddh0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0010.resp.Xddh0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0011.req.Xddh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0011.resp.Xddh0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0012.req.Xddh0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0012.resp.Xddh0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0013.req.Xddh0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0013.resp.Xddh0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0015.req.Xddh0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0015.resp.Xddh0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0016.req.Xddh0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0016.resp.Xddh0016DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizDhClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 贷后管理服务接口
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizDhClientServiceImpl.class)
public interface DscmsBizDhClientService {
    /**
     * 交易码：xddh0013
     * 交易描述：查询优抵贷抵质押品信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0013")
    public ResultDto<Xddh0013DataRespDto> xddh0013(Xddh0013DataReqDto reqDto);

    /**
     * 交易码：xddh0001
     * 交易描述：查询贷后任务是否完成现场检查
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0001")
    public ResultDto<Xddh0001DataRespDto> xddh0001(Xddh0001DataReqDto reqDto);

    /**
     * 交易码：xddh0002
     * 交易描述：新增主动还款申请记录
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0002")
    public ResultDto<Xddh0002DataRespDto> xddh0002(Xddh0002DataReqDto reqDto);

    /**
     * 交易码：xddh0003
     * 交易描述：主动还款申请记录列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0003")
    public ResultDto<Xddh0003DataRespDto> xddh0003(Xddh0003DataReqDto reqDto);

    /**
     * 交易码：xddh0004
     * 交易描述：查询还款是否在途
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0004")
    public ResultDto<Xddh0004DataRespDto> xddh0004(Xddh0004DataReqDto reqDto);

    /**
     * 交易码：xddh0005
     * 交易描述：提前还款
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0005")
    public ResultDto<Xddh0005DataRespDto> xddh0005(Xddh0005DataReqDto reqDto);

    /**
     * 交易码：xddh0006
     * 交易描述：提前还款试算查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0006")
    public ResultDto<Xddh0006DataRespDto> xddh0006(Xddh0006DataReqDto reqDto);

    /**
     * 交易码：xddh0009
     * 交易描述：还款日期卡控
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0009")
    public ResultDto<Xddh0009DataRespDto> xddh0009(Xddh0009DataReqDto reqDto);

    /**
     * 交易码：xddh0010
     * 交易描述：推送优享贷预警信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0010")
    public ResultDto<Xddh0010DataRespDto> xddh0010(Xddh0010DataReqDto reqDto);

    /**
     * 交易码：xddh0011
     * 交易描述：推送优享贷预警信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0011")
    public ResultDto<Xddh0011DataRespDto> xddh0011(Xddh0011DataReqDto reqDto);

    /**
     * 交易码：xddh0011
     * 交易描述：推送优享贷预警信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0012")
    public ResultDto<Xddh0012DataRespDto> xddh0012(Xddh0012DataReqDto reqDto);

    /**
     * 交易码：xddh0015
     * 交易描述：优农贷贷后预警通知
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0015")
    public ResultDto<Xddh0015DataRespDto> xddh0015(Xddh0015DataReqDto reqDto);

    /**
     * 交易码：xddh0016
     * 交易描述：提前还款申请
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0016")
    public ResultDto<Xddh0016DataRespDto> xddh0016(Xddh0016DataReqDto reqDto);

    /**
     * 交易码：xddh0007
     * 交易描述：还款记录列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0007")
    public ResultDto<Xddh0007DataRespDto> xddh0007(Xddh0007DataReqDto reqDto);

    /**
     * 交易码：xddh0008
     * 交易描述：还款计划列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizdh4bsp/xddh0008")
    public ResultDto<Xddh0008DataRespDto> xddh0008(Xddh0008DataReqDto reqDto);
}
