package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdsx0006.req.Xdsx0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0006.resp.Xdsx0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0008.req.Xdsx0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0008.resp.Xdsx0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.req.Xdsx0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.resp.Xdsx0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0013.req.Xdsx0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0013.resp.Xdsx0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0014.req.Xdsx0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0014.resp.Xdsx0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0015.req.Xdsx0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0015.resp.Xdsx0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0016.req.Xdsx0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0016.resp.Xdsx0016DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0017.req.Xdsx0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0017.resp.Xdsx0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.req.Xdsx0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.resp.Xdsx0018DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0019.req.Xdsx0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0019.resp.Xdsx0019DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0020.req.Xdsx0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0020.resp.Xdsx0020DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0022.req.Xdsx0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0022.resp.Xdsx0022DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0023.req.Xdsx0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0023.resp.Xdsx0023DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0024.req.Xdsx0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0024.resp.Xdsx0024DataRespDto;
import cn.com.yusys.yusp.dto.server.xdsx0026.req.Xdsx0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0026.resp.Xdsx0026DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizSxClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:对公授信服务接口
 *
 * @author zhugenrong
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizSxClientServiceImpl.class)
public interface DscmsBizSxClientService {
    /**
     * 交易码：xdxw0006
     * 交易描述：专业贷款评级结果同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0006")
    public ResultDto<Xdsx0006DataRespDto> xdsx0006(Xdsx0006DataReqDto reqDto);

    /**
     * 交易码：xdsx0001
     * 交易描述：公司客户评级相关信息同步
     *
     * @param reqDto
     * @return
     */


    /**
     * 交易码：xdsx0008
     * 交易描述：单一客户人工限额同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0008")
    public ResultDto<Xdsx0008DataRespDto> xdsx0008(Xdsx0008DataReqDto reqDto);


    /**
     * 交易码：xdsx0011
     * 交易描述：查询优惠（省心E付）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0011")
    public ResultDto<Xdsx0011DataRespDto>  xdsx0011(Xdsx0011DataReqDto reqDto);

    /**
     * 交易码：xdsx0022
     * 交易描述：推送苏州地方征信给信贷
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0022")
    public ResultDto<Xdsx0022DataRespDto>  xdsx0022(Xdsx0022DataReqDto reqDto);

    /**
     * 交易码：xdsx0024
     * 交易描述：省心快贷plus授信，风控自动审批结果推送信贷
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0024")
    public ResultDto<Xdsx0024DataRespDto>  xdsx0024(Xdsx0024DataReqDto reqDto);

    /**
     * 交易码：xdsx0016
     * 交易描述：抵押查封信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0016")
    public ResultDto<Xdsx0016DataRespDto> xdsx0016(Xdsx0016DataReqDto reqDto);

    /**
     * 交易码：xdsx0018
     * 交易描述：房抵e点贷无还本续贷审核
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0018")
    public ResultDto<Xdsx0018DataRespDto> xdsx0018(Xdsx0018DataReqDto reqDto);


    /**
     * 交易码：xdsx0020
     * 交易描述：担保公司合作协议查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0020")
    public ResultDto<Xdsx0020DataRespDto> xdsx0020(Xdsx0020DataReqDto reqDto);

    /**
     * 交易码：xdsx0017
     * 交易描述：信贷提供风控查询授信信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0017")
    public ResultDto<Xdsx0017DataRespDto> xdsx0017(Xdsx0017DataReqDto reqDto);

    /**
     * 交易码：xdsx0023
     * 交易描述：保函协议查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0023")
    public ResultDto<Xdsx0023DataRespDto> xdsx0023(Xdsx0023DataReqDto reqDto);

    /**
     * 交易码：xdsx0013
     * 交易描述：风控发信贷根据客户号查询授信信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0013")
    public ResultDto<Xdsx0013DataRespDto> xdsx0013(Xdsx0013DataReqDto reqDto);

    /**
     * 交易码：xdsx0026
     * 交易描述：风控推送面签信息至信贷系统
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0026")
    public ResultDto<Xdsx0026DataRespDto> xdsx0026(Xdsx0026DataReqDto reqDto);

    /**
     * 交易码：xdsx0015
     * 交易描述：已批复的授信申请信息、押品信息以及合同信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0015")
    public ResultDto<Xdsx0015DataRespDto> xdsx0015(Xdsx0015DataReqDto reqDto);

    /**
     * 交易码：xdsx0019
     * 交易描述：风控发送信贷审核受托信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0019")
    public ResultDto<Xdsx0019DataRespDto> xdsx0019(Xdsx0019DataReqDto reqDto);

    /**
     * 交易码：xdsx0014
     * 交易描述：风控发信贷进行授信作废接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizsx4bsp/xdsx0014")
    public ResultDto<Xdsx0014DataRespDto> xdsx0014(Xdsx0014DataReqDto reqDto);

}


