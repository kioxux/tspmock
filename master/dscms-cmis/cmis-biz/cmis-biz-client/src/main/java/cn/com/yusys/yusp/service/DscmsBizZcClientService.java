package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdzc0001.req.Xdzc0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0001.resp.Xdzc0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0002.req.Xdzc0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0002.resp.Xdzc0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0003.req.Xdzc0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0003.resp.Xdzc0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0004.req.Xdzc0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0004.resp.Xdzc0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0005.req.Xdzc0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0005.resp.Xdzc0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0006.req.Xdzc0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0006.resp.Xdzc0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.req.Xdzc0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.resp.Xdzc0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0008.req.Xdzc0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0008.resp.Xdzc0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0009.req.Xdzc0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0009.resp.Xdzc0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0010.req.Xdzc0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0010.resp.Xdzc0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0011.req.Xdzc0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0011.resp.Xdzc0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0012.req.Xdzc0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0012.resp.Xdzc0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0013.req.Xdzc0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0013.resp.Xdzc0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0014.req.Xdzc0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0014.resp.Xdzc0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0015.req.Xdzc0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0015.resp.Xdzc0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0016.req.Xdzc0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0016.resp.Xdzc0016DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0017.req.Xdzc0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0017.resp.Xdzc0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0018.req.Xdzc0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0018.resp.Xdzc0018DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0019.req.Xdzc0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0019.resp.Xdzc0019DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0020.req.Xdzc0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0020.resp.Xdzc0020DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0021.req.Xdzc0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0021.resp.Xdzc0021DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0022.req.Xdzc0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0022.resp.Xdzc0022DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0023.req.Xdzc0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0023.resp.Xdzc0023DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0024.req.Xdzc0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0024.resp.Xdzc0024DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0025.req.Xdzc0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0025.resp.Xdzc0025DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzc0026.req.Xdzc0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0026.resp.Xdzc0026DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizZcClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 资产池管理服务接口
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizZcClientServiceImpl.class)
public interface DscmsBizZcClientService {

    /**
     * 交易码：xdzc0001
     * 交易描述：客户资产池协议列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0001")
    public ResultDto<Xdzc0001DataRespDto> xdzc0001(Xdzc0001DataReqDto reqDto);

    /**
     * 交易码：xdzc0002
     * 交易描述：客户资产池协议维护（协议激活）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0002")
    public ResultDto<Xdzc0002DataRespDto> xdzc0002(Xdzc0002DataReqDto reqDto);

    /**
     * 交易码：xdzc0003
     * 交易描述：客户资产池下资产清单列表查询（出池时查询）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0003")
    public ResultDto<Xdzc0003DataRespDto> xdzc0003(Xdzc0003DataReqDto reqDto);

    /**
     * 交易码：xdzc0005
     * 交易描述：资产池入池接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0005")
    public ResultDto<Xdzc0005DataRespDto> xdzc0005(Xdzc0005DataReqDto reqDto);

    /**
     * 交易码：xdzc0005
     * 交易描述：资产池出池校验
     *
     * @param xdzc0004DataReqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0004")
    ResultDto<Xdzc0004DataRespDto> xdzc0004(Xdzc0004DataReqDto xdzc0004DataReqDto);


    /**
     * 交易码：xdzc0006
     * 交易描述：资产池出池校验接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0006")
    public ResultDto<Xdzc0006DataRespDto> xdzc0006(Xdzc0006DataReqDto reqDto);

    /**
     * 交易码：xdzc0007
     * 交易描述：资产池出池接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0007")
    public ResultDto<Xdzc0007DataRespDto> xdzc0007(Xdzc0007DataReqDto reqDto);

    /**
     * 交易码：xdzc0008
     * 交易描述：资产池出票校验接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0008")
    public ResultDto<Xdzc0008DataRespDto> xdzc0008(Xdzc0008DataReqDto reqDto);

    /**
     * 交易码：xdzc0009
     * 交易描述：资产池出票交易接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0009")
    public ResultDto<Xdzc0009DataRespDto> xdzc0009(Xdzc0009DataReqDto reqDto);

    /**
     * 交易码：xdzc0010
     * 交易描述：资产池超短贷校验接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0010")
    public ResultDto<Xdzc0010DataRespDto> xdzc0010(Xdzc0010DataReqDto reqDto);

    /**
     * 交易码：xdzc0011
     * 交易描述：资产池超短贷放款接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0011")
    public ResultDto<Xdzc0011DataRespDto> xdzc0011(Xdzc0011DataReqDto reqDto);

    /**
     * 交易码：xdzc0012
     * 交易描述：贸易背景资料收集通知接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0012")
    public ResultDto<Xdzc0012DataRespDto> xdzc0012(Xdzc0012DataReqDto reqDto);

    /**
     * 交易码：xdzc0013
     * 交易描述：贸易合同资料上传接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0013")
    public ResultDto<Xdzc0013DataRespDto> xdzc0013(Xdzc0013DataReqDto reqDto);

    /**
     * 交易码：xdzc0014
     * 交易描述：资产池主动还款接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0014")
    public ResultDto<Xdzc0014DataRespDto> xdzc0014(Xdzc0014DataReqDto reqDto);

    /**
     * 交易码：xdzc0015
     * 交易描述：保证金查询接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0015")
    public ResultDto<Xdzc0015DataRespDto> xdzc0015(Xdzc0015DataReqDto reqDto);

    /**
     * 交易码：xdzc0016
     * 交易描述：购销合同查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0016")
    public ResultDto<Xdzc0016DataRespDto> xdzc0016(Xdzc0016DataReqDto reqDto);

    /**
     * 交易码：xdzc0017
     * 交易描述：新增购销合同申请撤回
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0017")
    public ResultDto<Xdzc0017DataRespDto> xdzc0017(Xdzc0017DataReqDto reqDto);

    /**
     * 交易码：xdzc0018
     * 交易描述：我的资产统计查询接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0018")
    public ResultDto<Xdzc0018DataRespDto> xdzc0018(Xdzc0018DataReqDto reqDto);

    /**
     * 交易码：xdzc0019
     * 交易描述：历史出入池记录查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0019")
    public ResultDto<Xdzc0019DataRespDto> xdzc0019(Xdzc0019DataReqDto reqDto);

    /**
     * 交易码：xdzc0020
     * 交易描述：出入池详情查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0020")
    public ResultDto<Xdzc0020DataRespDto> xdzc0020(Xdzc0020DataReqDto reqDto);

    /**
     * 交易码：xdzc0021
     * 交易描述：融资汇总查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0021")
    public ResultDto<Xdzc0021DataRespDto> xdzc0021(Xdzc0021DataReqDto reqDto);

    /**
     * 交易码：xdzc0022
     * 交易描述：发票补录
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0022")
    public ResultDto<Xdzc0022DataRespDto> xdzc0022(Xdzc0022DataReqDto reqDto);

    /**
     * 交易码：xdzc0023
     * 交易描述：资产池白名单
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0023")
    public ResultDto<Xdzc0023DataRespDto> xdzc0023(Xdzc0023DataReqDto reqDto);

    /**
     * 交易码：xdzc0024
     * 交易描述：票据池资料补全查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0024")
    ResultDto<Xdzc0024DataRespDto> xdzc0024(Xdzc0024DataReqDto reqDto);


    /**
     * 交易码：xdzc0025
     * 交易描述：票据池托收回款通知信贷
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0025")
    public ResultDto<Xdzc0025DataRespDto> xdzc0025(Xdzc0025DataReqDto reqDto);

    /**
     * 交易码：xdzc0026
     * 交易描述：资产池主动还款接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzc4bsp/xdzc0026")
    public ResultDto<Xdzc0026DataRespDto> xdzc0026(Xdzc0026DataReqDto reqDto);
}
