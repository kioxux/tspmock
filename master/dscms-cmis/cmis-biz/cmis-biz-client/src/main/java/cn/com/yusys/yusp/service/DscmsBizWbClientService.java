package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdwb0001.req.Xdwb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdwb0001.resp.Xdwb0001DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizWbClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 外部服务接口
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizWbClientServiceImpl.class)
public interface DscmsBizWbClientService {
    /**
     * 交易码：xdwb0001
     * 交易描述：省联社金融服务平台借据信息查询接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizwb4bsp/xdwb0001")
    ResultDto<Xdwb0001DataRespDto> xdwb0001(Xdwb0001DataReqDto reqDto);


}
