package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdcz0001.req.Xdcz0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0001.resp.Xdcz0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0002.req.Xdcz0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0002.resp.Xdcz0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0003.req.Xdcz0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0003.resp.Xdcz0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0004.req.Xdcz0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0004.resp.Xdcz0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0005.req.Xdcz0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0005.resp.Xdcz0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0006.req.Xdcz0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0006.resp.Xdcz0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0007.req.Xdcz0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0007.resp.Xdcz0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0008.req.Xdcz0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0008.resp.Xdcz0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0009.req.Xdcz0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0009.resp.Xdcz0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0010.req.Xdcz0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0010.resp.Xdcz0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0011.req.Xdcz0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0011.resp.Xdcz0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0012.req.Xdcz0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0012.resp.Xdcz0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0013.req.Xdcz0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0013.resp.Xdcz0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0014.req.Xdcz0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0014.resp.Xdcz0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0015.req.Xdcz0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0015.resp.Xdcz0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0016.req.Xdcz0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0016.resp.Xdcz0016DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0017.req.Xdcz0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0017.resp.Xdcz0017DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0018.req.Xdcz0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0018.resp.Xdcz0018DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0019.req.Xdcz0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0019.resp.Xdcz0019DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0020.req.Xdcz0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0020.resp.Xdcz0020DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0021.req.Xdcz0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0021.resp.Xdcz0021DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0022.req.Xdcz0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0022.resp.Xdcz0022DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0023.req.Xdcz0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0023.resp.Xdcz0023DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0024.req.Xdcz0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0024.resp.Xdcz0024DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0025.req.Xdcz0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0025.resp.Xdcz0025DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0026.req.Xdcz0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0026.resp.Xdcz0026DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0027.req.Xdcz0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0027.resp.Xdcz0027DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0028.req.Xdcz0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0028.resp.Xdcz0028DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0029.req.Xdcz0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0029.resp.Xdcz0029DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0030.req.Xdcz0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0030.resp.Xdcz0030DataRespDto;
import cn.com.yusys.yusp.dto.server.xdcz0031.req.Xdcz0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0031.resp.Xdcz0031DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizCzClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 出账管理服务接口
 *
 * @author chenyong
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizCzClientServiceImpl.class)
public interface DscmsBizCzClientService {
    /**
     * 交易码：xdcz0026
     * 交易描述：风控调用信贷放款
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0026")
    public ResultDto<Xdcz0026DataRespDto> xdcz0026(Xdcz0026DataReqDto reqDto);

    /**
     * 交易码：xdcz0001
     * 交易描述：电子保函开立
     *
     * @param xdcz0001DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0001")
    ResultDto<Xdcz0001DataRespDto> xdcz0001(Xdcz0001DataReqDto xdcz0001DataReqDto);

    /**
     * 交易码：xdcz0002
     * 交易描述：电子保函注销
     *
     * @param xdcz0002DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0002")
    ResultDto<Xdcz0002DataRespDto> xdcz0002(Xdcz0002DataReqDto xdcz0002DataReqDto);

    /**
     * 交易码：xdcz0003
     * 交易描述：电子银行承兑汇票出账申请
     *
     * @param xdcz0003DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0003")
    ResultDto<Xdcz0003DataRespDto> xdcz0003(Xdcz0003DataReqDto xdcz0003DataReqDto);

    /**
     * 交易码：xdcz0004
     * 交易描述：承兑签发审批结果综合服务
     *
     * @param xdcz0004DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0004")
    ResultDto<Xdcz0004DataRespDto> xdcz0004(Xdcz0004DataReqDto xdcz0004DataReqDto);

    /**
     * 交易码：xdcz0005
     * 交易描述：资产池入池接口
     *
     * @param xdcz0005DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0005")
    ResultDto<Xdcz0005DataRespDto> xdcz0005(Xdcz0005DataReqDto xdcz0005DataReqDto);

    /**
     * 交易码：xdcz0006
     * 交易描述：资产池入池接口
     *
     * @param xdcz0006DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0006")
    ResultDto<Xdcz0006DataRespDto> xdcz0006(Xdcz0006DataReqDto xdcz0006DataReqDto);


    /**
     * 交易码：xdcz0007
     * 交易描述：查询敞口额度及保证金校验
     *
     * @param xdcz0007DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0007")
    ResultDto<Xdcz0007DataRespDto> xdcz0007(Xdcz0007DataReqDto xdcz0007DataReqDto);


    /**
     * 交易码：xdcz0008
     * 交易描述：代开保函
     *
     * @param xdcz0008DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0008")
    ResultDto<Xdcz0008DataRespDto> xdcz0008(Xdcz0008DataReqDto xdcz0008DataReqDto);

    /**
     * 交易码：xdcz0009
     * 交易描述：代开信用证
     *
     * @param xdcz0009DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0009")
    ResultDto<Xdcz0009DataRespDto> xdcz0009(Xdcz0009DataReqDto xdcz0009DataReqDto);

    /**
     * 交易码：xdcz0010
     * 交易描述：企业网银省心E付放款
     *
     * @param xdcz0010DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0010")
    ResultDto<Xdcz0010DataRespDto> xdcz0010(Xdcz0010DataReqDto xdcz0010DataReqDto);

    /**
     * 交易码：xdcz0011
     * 交易描述：省心E付放款记录列表查询
     *
     * @param xdcz0011DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0011")
    ResultDto<Xdcz0011DataRespDto> xdcz0011(Xdcz0011DataReqDto xdcz0011DataReqDto);

    /**
     * 交易码：xdcz0012
     * 交易描述：网银推送相关受托支付信息
     *
     * @param xdcz0012DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0012")
    ResultDto<Xdcz0012DataRespDto> xdcz0012(Xdcz0012DataReqDto xdcz0012DataReqDto);

    /**
     * 交易码：xdcz0013
     * 交易描述：企业网银出票申请额度校验
     *
     * @param xdcz0013DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0013")
    ResultDto<Xdcz0013DataRespDto> xdcz0013(Xdcz0013DataReqDto xdcz0013DataReqDto);

    /**
     * 交易码：xdcz0014
     * 交易描述：企业网银查询影像补录批次
     *
     * @param xdcz0014DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0014")
    ResultDto<Xdcz0014DataRespDto> xdcz0014(Xdcz0014DataReqDto xdcz0014DataReqDto);

    /**
     * 交易码：xdcz0015
     * 交易描述：银票影像补录同步
     *
     * @param xdcz0015DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0015")
    ResultDto<Xdcz0015DataRespDto> xdcz0015(Xdcz0015DataReqDto xdcz0015DataReqDto);

    /**
     * 交易码：xdcz0016
     * 交易描述：支用列表查询(微信小程序)
     *
     * @param xdcz0016DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0016")
    ResultDto<Xdcz0016DataRespDto> xdcz0016(Xdcz0016DataReqDto xdcz0016DataReqDto);

    /**
     * 交易码：xdcz0017
     * 交易描述：支用(微信小程序)
     *
     * @param xdcz0017DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0017")
    ResultDto<Xdcz0017DataRespDto> xdcz0017(Xdcz0017DataReqDto xdcz0017DataReqDto);

    /**
     * 交易码：xdcz0018
     * 交易描述：家速贷、极速快贷贷款申请
     *
     * @param xdcz0018DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0018")
    ResultDto<Xdcz0018DataRespDto> xdcz0018(Xdcz0018DataReqDto xdcz0018DataReqDto);

    /**
     * 交易码：xdcz0019
     * 交易描述：小贷额度支用申请书生成pdf
     *
     * @param xdcz0019DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0019")
    ResultDto<Xdcz0019DataRespDto> xdcz0019(Xdcz0019DataReqDto xdcz0019DataReqDto);


    /**
     * 交易码：xdcz0020
     * 交易描述：小贷用途承诺书文本生成pdf
     *
     * @param xdcz0020DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0020")
    ResultDto<Xdcz0020DataRespDto> xdcz0020(Xdcz0020DataReqDto xdcz0020DataReqDto);

    /**
     * 交易码：xdcz0021
     * 交易描述：支用申请
     *
     * @param xdcz0021DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0021")
    ResultDto<Xdcz0021DataRespDto> xdcz0021(Xdcz0021DataReqDto xdcz0021DataReqDto);

    /**
     * 交易码：xdcz0022
     * 交易描述：风控发送相关信息至信贷进行支用校验
     *
     * @param xdcz0022DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0022")
    ResultDto<Xdcz0022DataRespDto> xdcz0022(Xdcz0022DataReqDto xdcz0022DataReqDto);

    /**
     * 交易码：xdcz0023
     * 交易描述：小企业无还本续贷放款
     *
     * @param xdcz0023DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0023")
    ResultDto<Xdcz0023DataRespDto> xdcz0023(Xdcz0023DataReqDto xdcz0023DataReqDto);

    /**
     * 交易码：xdcz0024
     * 交易描述：推送在线保函预约信息
     *
     * @param xdcz0024DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0024")
    ResultDto<Xdcz0024DataRespDto> xdcz0024(Xdcz0024DataReqDto xdcz0024DataReqDto);

    /**
     * 交易码：xdcz0025
     * 交易描述：未中标业务注销
     *
     * @param xdcz0025DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0025")
    ResultDto<Xdcz0025DataRespDto> xdcz0025(Xdcz0025DataReqDto xdcz0025DataReqDto);


    /**
     * 交易码：xdcz0027
     * 交易描述：审批失败支用申请( 登记支用失败)
     *
     * @param xdcz0027DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0027")
    ResultDto<Xdcz0027DataRespDto> xdcz0027(Xdcz0027DataReqDto xdcz0027DataReqDto);

    /**
     * 交易码：xdcz0028
     * 交易描述：生成商贷账号数据文件
     *
     * @param xdcz0028DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0028")
    ResultDto<Xdcz0028DataRespDto> xdcz0028(Xdcz0028DataReqDto xdcz0028DataReqDto);

    /**
     * 交易码：xdcz0029
     * 交易描述：网银推送省心快贷审核任务至信贷
     *
     * @param xdcz0029DataReqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0029")
    ResultDto<Xdcz0029DataRespDto> xdcz0029(Xdcz0029DataReqDto xdcz0029DataReqDto);

    /**
     * 交易码：xdcz0030
     * 交易描述：校验额度是否足额，合同是否足额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0030")
    public ResultDto<Xdcz0030DataRespDto> xdcz0030(Xdcz0030DataReqDto reqDto);

    /**
     * 交易码：xdcz0031
     * 交易描述：
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizcz4bsp/xdcz0031")
    public ResultDto<Xdcz0031DataRespDto> xdcz0031(Xdcz0031DataReqDto reqDto);
}
