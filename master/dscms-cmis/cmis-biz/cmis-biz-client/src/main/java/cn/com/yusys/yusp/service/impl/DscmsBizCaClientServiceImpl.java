package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdca0001.req.Xdca0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0001.resp.Xdca0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0002.req.Xdca0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0002.resp.Xdca0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0003.req.Xdca0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0003.resp.Xdca0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0004.req.Xdca0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0004.resp.Xdca0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0005.req.Xdca0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0005.resp.Xdca0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0006.req.Xdca0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0006.resp.Xdca0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsBizCaClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsBizCaClientServiceImpl implements DscmsBizCaClientService {

    private static final Logger logger = LoggerFactory.getLogger(DscmsBizCaClientServiceImpl.class);

    /**
     * 交易码：xdca0001
     * 交易描述：信用卡调额申请
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdca0001DataRespDto> xdca0001(Xdca0001DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDCA0001.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDCA0001.value));
        return null;
    }


    /**
     * 交易码：xdca0002
     * 交易描述：大额分期申请（试算）接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdca0002DataRespDto>  xdca0002(Xdca0002DataReqDto reqDto){
        logger.error("访问{}失败，触发熔断。",DscmsEnum.TRADE_CODE_XDCA0002.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDCA0002.value));
        return null;
    }

    /**
     * 交易码：xdca0003
     * 交易描述：大额分期合同列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdca0003DataRespDto>  xdca0003(Xdca0003DataReqDto reqDto){
        logger.error("访问{}失败，触发熔断。",DscmsEnum.TRADE_CODE_XDCA0003.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDCA0003.value));
        return null;
    }

    /**
     * 交易码：xdca0004
     * 交易描述：大额分期合同详情查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdca0004DataRespDto>  xdca0004(Xdca0004DataReqDto reqDto){
        logger.error("访问{}失败，触发熔断。",DscmsEnum.TRADE_CODE_XDCA0004.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDCA0004.value));
        return null;
    }

    /**
     * 交易码：xdca0005
     * 交易描述：大额分期合同签订接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdca0005DataRespDto>  xdca0005(Xdca0005DataReqDto reqDto){
        logger.error("访问{}失败，触发熔断。",DscmsEnum.TRADE_CODE_XDCA0005.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDCA0005.value));
        return null;
    }

    @Override
    public ResultDto<Xdca0006DataRespDto> xdca0006(Xdca0006DataReqDto reqDto) {
        return null;
    }

}
