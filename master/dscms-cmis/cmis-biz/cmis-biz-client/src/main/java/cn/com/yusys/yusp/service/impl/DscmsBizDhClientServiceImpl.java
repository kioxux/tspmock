package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xddh0001.req.Xddh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0001.resp.Xddh0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0002.req.Xddh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0002.resp.Xddh0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0003.req.Xddh0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0003.resp.Xddh0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0004.req.Xddh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0004.resp.Xddh0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0005.resp.Xddh0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0006.req.Xddh0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0006.resp.Xddh0006DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0007.req.Xddh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0007.resp.Xddh0007DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0008.req.Xddh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0008.resp.Xddh0008DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0009.req.Xddh0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0009.resp.Xddh0009DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0010.req.Xddh0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0010.resp.Xddh0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0011.req.Xddh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0011.resp.Xddh0011DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0012.req.Xddh0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0012.resp.Xddh0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0013.req.Xddh0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0013.resp.Xddh0013DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0015.req.Xddh0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0015.resp.Xddh0015DataRespDto;
import cn.com.yusys.yusp.dto.server.xddh0016.req.Xddh0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0016.resp.Xddh0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsBizDhClientServiceImpl implements DscmsBizDhClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsBizDhClientService.class);

    /**
     * 交易码：xddh0013
     * 交易描述：查询优抵贷抵质押品信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0013DataRespDto> xddh0013(Xddh0013DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0013.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0013.value));
        return null;
    }

    /**
     * 交易码：xddh0001
     * 交易描述：查询贷后任务是否完成现场检查
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0001DataRespDto> xddh0001(Xddh0001DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0001.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0001.value));
        return null;
    }

    /**
     * 交易码：xddh0002
     * 交易描述：新增主动还款申请记录s
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0002DataRespDto> xddh0002(Xddh0002DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0002.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0002.value));
        return null;
    }

    /**
     * 交易码：xddh0003
     * 交易描述：主动还款申请记录列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0003DataRespDto> xddh0003(Xddh0003DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0003.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0003.value));
        return null;
    }

    /**
     * 交易码：xddh0004
     * 交易描述：查询还款是否在途
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0004DataRespDto> xddh0004(Xddh0004DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0004.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0004.value));
        return null;
    }

    /**
     * 交易码：xddh0005
     * 交易描述：提前还款
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0005DataRespDto> xddh0005(Xddh0005DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0005.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0005.value));
        return null;
    }

    /**
     * 交易码：xddh0006
     * 交易描述：提前还款试算查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0006DataRespDto> xddh0006(Xddh0006DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0006.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0006.value));
        return null;
    }

    /**
     * 交易码：xddh0009
     * 交易描述：还款日期卡控
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0009DataRespDto> xddh0009(Xddh0009DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0009.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0009.value));
        return null;
    }

    /**
     * 交易码：xddh0010
     * 交易描述：推送优享贷预警信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0010DataRespDto> xddh0010(Xddh0010DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0010.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0010.value));
        return null;
    }

    /**
     * 交易码：xddh0011
     * 交易描述：推送优享贷预警信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0011DataRespDto> xddh0011(Xddh0011DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0011.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0011.value));
        return null;
    }

    /**
     * 交易码：xddh0011
     * 交易描述：推送优享贷预警信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0012DataRespDto> xddh0012(Xddh0012DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0012.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0012.value));
        return null;
    }

    /**
     * 交易码：xddh0015
     * 交易描述：优农贷贷后预警通知
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0015DataRespDto> xddh0015(Xddh0015DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0015.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0015.value));
        return null;
    }

    /**
     * 交易码：xddh0016
     * 交易描述：提前还款申请
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0016DataRespDto> xddh0016(Xddh0016DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0016.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0016.value));
        return null;
    }

    /**
     * 交易码：xddh0007
     * 交易描述：还款记录列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0007DataRespDto> xddh0007(Xddh0007DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0007.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0007.value));
        return null;
    }

    /**
     * 交易码：xddh0008
     * 交易描述：还款计划列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddh0008DataRespDto> xddh0008(Xddh0008DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDDH0008.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDDH0008.value));
        return null;
    }
}
