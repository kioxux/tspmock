package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.req.Xdqt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.resp.Xdqt0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0004.req.Xdqt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0004.resp.Xdqt0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0005.req.Xdqt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0005.resp.Xdqt0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0009.req.Xdqt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0009.resp.Xdqt0009DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizQtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 出账管理服务接口
 *
 * @author chenyong
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizQtClientServiceImpl.class)
public interface DscmsBizQtClientService {

    /**
     * 交易码：xdqt0003
     * 交易描述：企业网银推送预约信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizqt4bsp/xdqt0003")
    public ResultDto<Xdqt0003DataRespDto>  xdqt0003(Xdqt0003DataReqDto reqDto);

    /**
     * 交易码：xdqt0004
     * 交易描述：贷款申请预约（个人客户）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizqt4bsp/xdqt0004")
    public ResultDto<Xdqt0004DataRespDto>  xdqt0004(Xdqt0004DataReqDto reqDto);

    /**
     * 交易码：xdqt0005
     * 交易描述：贷款申请预约（企业客户）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizqt4bsp/xdqt0005")
    public ResultDto<Xdqt0005DataRespDto>  xdqt0005(Xdqt0005DataReqDto reqDto);

    /**
     * 交易码：xdqt0009
     * 交易描述：微业贷信贷文件处理通知
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizqt4bsp/xdqt0009")
    ResultDto<Xdqt0009DataRespDto>  xdqt0009(Xdqt0009DataReqDto reqDto);
}
