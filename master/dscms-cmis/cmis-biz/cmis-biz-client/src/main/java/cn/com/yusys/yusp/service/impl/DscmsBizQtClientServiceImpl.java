package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.req.Xdqt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.resp.Xdqt0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0004.req.Xdqt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0004.resp.Xdqt0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0005.req.Xdqt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0005.resp.Xdqt0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdqt0009.req.Xdqt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0009.resp.Xdqt0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsBizQtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsBizQtClientServiceImpl implements DscmsBizQtClientService {

    private static final Logger logger = LoggerFactory.getLogger(DscmsBizQtClientServiceImpl.class);

    /**
     * 交易码：xdqt0003
     * 交易描述：企业网银推送预约信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdqt0003DataRespDto> xdqt0003(Xdqt0003DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDQT0003.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDQT0003.value));
        return null;
    }

    /**
     * 交易码：xdqt0004
     * 交易描述：贷款申请预约（个人客户）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdqt0004DataRespDto> xdqt0004(Xdqt0004DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDQT0004.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDQT0004.value));
        return null;
    }

    /**
     * 交易码：xdqt0005
     * 交易描述：贷款申请预约（企业客户）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdqt0005DataRespDto> xdqt0005(Xdqt0005DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDQT0005.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDQT0005.value));
        return null;
    }

    /**
     * 交易码：xdqt0009
     * 交易描述：微业贷信贷文件处理通知
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdqt0009DataRespDto> xdqt0009(Xdqt0009DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDQT0009.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDQT0009.value));
        return null;
    }
}
