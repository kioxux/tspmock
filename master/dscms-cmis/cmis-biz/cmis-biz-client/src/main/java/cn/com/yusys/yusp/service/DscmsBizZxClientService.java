package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.req.Xdzx0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.resp.Xdzx0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.req.Xdzx0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.resp.Xdzx0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzx0003.req.Xdzx0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0003.resp.Xdzx0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.req.Xdzx0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.resp.Xdzx0004DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizZxClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 征信服务接口
 *
 * @author chenyong
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizZxClientServiceImpl.class)
public interface DscmsBizZxClientService {
    /**
     * 交易码：xdzx0001
     * 交易描述：查询征信业务流水号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzx4bsp/xdzx0001")
    public ResultDto<Xdzx0001DataRespDto> xdzx0001(Xdzx0001DataReqDto reqDto);


    /**
     * 交易码：xdzx0002
     * 交易描述：征信授权查看
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzx4bsp/xdzx0002")
    public ResultDto<Xdzx0002DataRespDto> xdzx0002(Xdzx0002DataReqDto reqDto);

    /**
     * 交易码：xdzx0003
     * 交易描述：征信查询授权状态同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzx4bsp/xdzx0003")
    public ResultDto<Xdzx0003DataRespDto> xdzx0003(Xdzx0003DataReqDto reqDto);

    /**
     * 交易码：xdzx0004
     * 交易描述：授权结果反馈接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizzx4bsp/xdzx0004")
    public ResultDto<Xdzx0004DataRespDto> xdzx0004(Xdzx0004DataReqDto reqDto);

}
