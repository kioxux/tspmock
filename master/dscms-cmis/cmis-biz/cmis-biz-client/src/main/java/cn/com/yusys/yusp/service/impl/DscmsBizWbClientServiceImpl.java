package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdwb0001.req.Xdwb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdwb0001.resp.Xdwb0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsBizWbClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 描述：外部服务接口实现类
 *
 * @author zcrbank-fengjj
 * @description 外部服务接口实现类
 * @date 2021/8/13
 */
@Component
public class DscmsBizWbClientServiceImpl implements DscmsBizWbClientService {

    private static final Logger logger = LoggerFactory.getLogger(DscmsBizXtClientServiceImpl.class);

    /**
     * 省联社金融服务平台借据信息查询接口
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdwb0001DataRespDto> xdwb0001(Xdwb0001DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDWB0001.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDWB0001.value));
        return null;
    }
}
