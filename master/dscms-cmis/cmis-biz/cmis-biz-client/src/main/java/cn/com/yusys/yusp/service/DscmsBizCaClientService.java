package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdca0001.req.Xdca0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0001.resp.Xdca0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0002.req.Xdca0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0002.resp.Xdca0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0003.req.Xdca0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0003.resp.Xdca0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0004.req.Xdca0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0004.resp.Xdca0004DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0005.req.Xdca0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0005.resp.Xdca0005DataRespDto;
import cn.com.yusys.yusp.dto.server.xdca0006.req.Xdca0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0006.resp.Xdca0006DataRespDto;
import cn.com.yusys.yusp.service.impl.DscmsBizCaClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类: 出账管理服务接口
 *
 * @author chenyong
 * @version 1.0
 */
@FeignClient(name = "cmis-biz", path = "/api", fallback = DscmsBizCaClientServiceImpl.class)
public interface DscmsBizCaClientService {
    /**
     * 交易码：xdca0001
     * 交易描述：信用卡调额申请
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizca4bsp/xdca0001")
    public ResultDto<Xdca0001DataRespDto>  xdca0001(Xdca0001DataReqDto reqDto);

    /**
     * 交易码：xdca0002
     * 交易描述：大额分期申请（试算）接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizca4bsp/xdca0002")
    public ResultDto<Xdca0002DataRespDto>  xdca0002(Xdca0002DataReqDto reqDto);

    /**
     * 交易码：xdca0003
     * 交易描述：大额分期合同列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizca4bsp/xdca0003")
    public ResultDto<Xdca0003DataRespDto>  xdca0003(Xdca0003DataReqDto reqDto);

    /**
     * 交易码：xdca0004
     * 交易描述：大额分期合同详情查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizca4bsp/xdca0004")
    public ResultDto<Xdca0004DataRespDto>  xdca0004(Xdca0004DataReqDto reqDto);

    /**
     * 交易码：xdca0005
     * 交易描述：大额分期合同签订接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizca4bsp/xdca0005")
    public ResultDto<Xdca0005DataRespDto>  xdca0005(Xdca0005DataReqDto reqDto);

    /**
     * 交易码：xdca0005
     * 交易描述：大额分期合同签订接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/bizca4bsp/xdca0006")
    public ResultDto<Xdca0006DataRespDto>  xdca0006(Xdca0006DataReqDto reqDto);


}
