package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.req.Xdzx0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.resp.Xdzx0001DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.req.Xdzx0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.resp.Xdzx0002DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzx0003.req.Xdzx0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0003.resp.Xdzx0003DataRespDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.req.Xdzx0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.resp.Xdzx0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsBizZxClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DscmsBizZxClientServiceImpl implements DscmsBizZxClientService {
    private static final Logger logger = LoggerFactory.getLogger(DscmsBizZxClientServiceImpl.class);

    /**
     * 交易码：xdzx0001
     * 交易描述：查询征信业务流水号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdzx0001DataRespDto> xdzx0001(Xdzx0001DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDZX0001.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDZX0001.value));
        return null;
    }


    /**
     * 交易码：xdzx0002
     * 交易描述：征信授权查看
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdzx0002DataRespDto> xdzx0002(Xdzx0002DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDZX0002.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDZX0002.value));
        return null;
    }

    /**
     * 交易码：xdzx0003
     * 交易描述：征信查询授权状态同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdzx0003DataRespDto> xdzx0003(Xdzx0003DataReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", DscmsEnum.TRADE_CODE_XDZX0003.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDZX0003.value));
        return null;
    }

    /**
     * 交易码：xdzx0004
     * 交易描述：授权结果反馈接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdzx0004DataRespDto>  xdzx0004(Xdzx0004DataReqDto reqDto){
        logger.error("访问{}失败，触发熔断。",DscmsEnum.TRADE_CODE_XDZX0004.key.concat("|").concat(DscmsEnum.TRADE_CODE_XDZX0004.value));
        return null;
    }
}
