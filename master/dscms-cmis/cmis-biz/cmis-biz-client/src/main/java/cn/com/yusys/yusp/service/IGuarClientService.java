package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.dto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

@FeignClient(name = "cmis-biz",path = "/api",fallback = GuarClientServiceImpl.class )
public interface IGuarClientService {
    @PostMapping(value = "/guarinfguarantor/getGuarInfGuarantor")
    public List<GuarInfGuarantorClientDto> getGuarInfGuarantor(@RequestBody GuarInfGuarantorClientDto guarInfGuarantorDto);

    /**
     * 更新押品所在业务阶段
     *
     * @param guarBaseInfoClientDto
     * @return
     */
    @PostMapping(value = "/guarinfguarantor/updateGuarBusistate")
    public GuarClientRsDto updateGuarBusistate(@RequestBody GuarBaseInfoClientDto guarBaseInfoClientDto);
    
    /**
     * 通过押品编号信息获取押品金额信息
     *
     * @param guarGrtContRelClientDto
     * @return
     */
    @PostMapping(value = "/guarevalinfo/getGuarAmtInfoByGuarNos")
    public GuarGrtContRelClientDto getGuarAmtInfoByGuarNos(@RequestBody GuarGrtContRelClientDto guarGrtContRelClientDto);

    /**
     * 更新担保权证信息
     * @param guarCertiRelaClientDto
     * @return
     */
    @PostMapping(value = "/guarcertirela/udpateguarcertirela")
    public GuarClientRsDto udpateGuarCertiRela(@RequestBody GuarCertiRelaClientDto guarCertiRelaClientDto);

    /**
     * 获取当前用户权证借出的数据（权证状态：07-出借已记账）
     * @param map
     * @return
     */
    @PostMapping(value = "/guarcertirela/getGuarCertiRelaByCertiState")
    public GuarClientRsDto getGuarCertiRelaByCertiState(@RequestBody Map  map);

    /**
     * 获取担保权证信息
     * @param guarCertiRelaClientDto
     * @return
     */
    @PostMapping(value = "/guarcertirela/getGuarCertiRelaByGuarNo")
    public GuarClientRsDto getGuarCertiRelaByGuarNo(@RequestBody GuarCertiRelaClientDto guarCertiRelaClientDto);
    
    /**
     * 获取满足当前查询条件的押品信息(权证出入库申请)
     * @param map
     * @return
     */
    @PostMapping(value = "/guarbaseinfo/getGuarBaseInfoByParams")
    public GuarClientRsDto getGuarBaseInfoByParams(@RequestBody Map map);

    /**
     * 估值流程结束生成一条押品估值记录
     * @param guarEvalInfo
     * @return
     */
/*    @PostMapping(value = "/guarevalinfo/saveGuarEval")
    public GuarEvalInfoClientDto saveGuarEval(@RequestBody GuarEvalInfoClientDto guarEvalInfo);*/

    /**
     * 估值流程结束生成一条押品估值记录
     * @param guarRelotInfoClientDto
     * @return
     */
/*    @PostMapping(value = "/guarbaseinfo/getGuarRelotInfo")
    public List<GuarEvalRelotResultDto> getGuarRelotInfo(@RequestBody GuarRelotInfoClientDto guarRelotInfoClientDto);*/
}
