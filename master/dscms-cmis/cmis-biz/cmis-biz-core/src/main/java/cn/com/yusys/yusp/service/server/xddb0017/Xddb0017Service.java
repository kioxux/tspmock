package cn.com.yusys.yusp.service.server.xddb0017;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.slno01.Slno01ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.slno01.Slno01RespDto;
import cn.com.yusys.yusp.dto.server.xddb0017.req.Xddb0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0017.resp.SlSernoQueryDto;
import cn.com.yusys.yusp.dto.server.xddb0017.resp.Xddb0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.SlSernoQueryMapper;
import cn.com.yusys.yusp.service.Dscms2OuterdataClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接口处理类:押品信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Service
public class Xddb0017Service {

    private static final Logger logger = LoggerFactory.getLogger(Xddb0017Service.class);
    @Autowired
    private Dscms2OuterdataClientService dscms2OuterdataClientService;

    @Resource
    private SlSernoQueryMapper slSernoQueryMapper;

    /**
     * 获取抵押登记双录音视频信息列表
     *
     * @param xdkh0017DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0017DataRespDto xddb0017(Xddb0017DataReqDto xdkh0017DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value);
        Xddb0017DataRespDto xddb0017DataRespDto = new Xddb0017DataRespDto();
        try {
            List<cn.com.yusys.yusp.dto.server.xddb0017.resp.List> yxlist = new ArrayList();
            cn.com.yusys.yusp.dto.server.xddb0017.resp.List list = null;
            String coreGrtNo = xdkh0017DataReqDto.getGuarno();//抵押物编号
            String guid = "";
            String[] str = coreGrtNo.split("#");
            coreGrtNo = str[0];
            guid = coreGrtNo.substring(0, 2);
            if ("YP".equals(guid)) {
                coreGrtNo = slSernoQueryMapper.getCoreGrtNo(coreGrtNo);
            } else if ("DY".equals(guid) || "DW".equals(guid)) {
                coreGrtNo = coreGrtNo;
            }
       /* if(str.length>=2){
            guar_cont_no=str[1];
        }*/
            Map QueryMap = new HashMap();
            if (StringUtil.isNotEmpty(coreGrtNo)) {//押品编号不能为空！
                logger.info("**************************根据押品编号查询调用参数**************************");
                QueryMap.put("coreGrtNo", coreGrtNo);
                List<SlSernoQueryDto> slSernoQueryDtos = slSernoQueryMapper.selectSlSernoQueryByGuarNo(QueryMap);
                Map<String, String> resMap = new HashMap<>();
                if (slSernoQueryDtos.size() == 0) {//查询记录为空
                    //throw BizException.error(null,EcbEnum.EVAL_CLIENT_CUSNULL_EXCEPTION.key, EcbEnum.EVAL_CLIENT_CUSNULL_EXCEPTION.value);
                } else {
                    for (SlSernoQueryDto s : slSernoQueryDtos) {
                        String yxSerno = s.getYxSerno();
                        String cusName = s.getCusName();//抵押人名称
                        String areaLocation = s.getAreaLocation();//抵押物坐落
                        String guarContCnNo = s.getGuarCoutNo();//抵押合同编号
                        String guarantyId = s.getGuarantyId();//抵押物编号
                        String guarContState = s.getGuarContState();

                        if("101".equals(guarContState)){//已签约的担保合同状态
                            String result = "";
                            //获取双录流水号
                            Slno01ReqDto reqDto = new Slno01ReqDto();
                            reqDto.setGuarid(guarantyId);//抵押物编号
                            reqDto.setYxlsno(yxSerno);//影像主键流水号
                            reqDto.setJytype("01");//交易类型 01-取双录流水  02-同步流水
                            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, JSON.toJSONString(reqDto));
                            ResultDto<Slno01RespDto> slno01RespDtoResultDto = dscms2OuterdataClientService.slno01(reqDto);
                            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, JSON.toJSONString(slno01RespDtoResultDto));
                            Slno01RespDto sl = slno01RespDtoResultDto.getData();
                            String yxzjls = sl.getYxzjls();
                            String insert_time = sl.getInsert_time();

                            list = new cn.com.yusys.yusp.dto.server.xddb0017.resp.List();
                            list.setFile_id(yxzjls);//文件ID
                            list.setMortgagor(cusName);//抵押人名称
                            list.setMortgage_no(guarantyId);//抵押物编号
                            list.setContract_no(guarContCnNo);//抵押合同编号
                            list.setCreate_time(insert_time);//录制时间
                            list.setMortgage_location(areaLocation);//抵押物坐落
                            yxlist.add(list);
                        }
                    }
                }
                xddb0017DataRespDto.setList(yxlist);
            } else {
                //请求参数不存在,返回空值
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value);
                throw BizException.error(null, EcbEnum.ECBHYY001.key, EcbEnum.ECBHYY001.value);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }

        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value);
        return xddb0017DataRespDto;
    }
}
