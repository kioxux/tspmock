package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpJxhjRepayLoan
 * @类描述: pvp_jxhj_repay_loan数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-23 13:44:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class PvpJxhjRepayLoanDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务申请流水号 **/
	private String serno;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 拖欠本金 **/
	private String prcp;
	
	/** 拖欠利息 **/
	private String normInt;
	
	/** 拖欠罚息 **/
	private String odInt;
	
	/** 拖欠复利 **/
	private String commOdInt;
	
	/** 未到期本金 **/
	private String nextPrcp;
	
	/** 应计利息 **/
	private String nextInt;
	
	/** 自还本金 **/
	private String selfPrcp;
	
	/** 自还利息 **/
	private String selfInt;
	
	/** 试算检查状态000未验证,111已经验证 **/
	private String checkStatus;
	
	/** 贷款起始日期 **/
	private String loanStartDate;
	
	/** 贷款到期日期 **/
	private String loanEndDate;
	
	/** 贷款台帐状态 **/
	private String accStatus;
	
	/** 贷款余额 **/
	private String loanBalance;
	
	/** 贷款金额 **/
	private String loanAmt;
	
	/** 借新本金 **/
	private String newLoanAmt;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param prcp
	 */
	public void setPrcp(String prcp) {
		this.prcp = prcp == null ? null : prcp.trim();
	}
	
    /**
     * @return Prcp
     */	
	public String getPrcp() {
		return this.prcp;
	}
	
	/**
	 * @param normInt
	 */
	public void setNormInt(String normInt) {
		this.normInt = normInt == null ? null : normInt.trim();
	}
	
    /**
     * @return NormInt
     */	
	public String getNormInt() {
		return this.normInt;
	}
	
	/**
	 * @param odInt
	 */
	public void setOdInt(String odInt) {
		this.odInt = odInt == null ? null : odInt.trim();
	}
	
    /**
     * @return OdInt
     */	
	public String getOdInt() {
		return this.odInt;
	}
	
	/**
	 * @param commOdInt
	 */
	public void setCommOdInt(String commOdInt) {
		this.commOdInt = commOdInt == null ? null : commOdInt.trim();
	}
	
    /**
     * @return CommOdInt
     */	
	public String getCommOdInt() {
		return this.commOdInt;
	}
	
	/**
	 * @param nextPrcp
	 */
	public void setNextPrcp(String nextPrcp) {
		this.nextPrcp = nextPrcp == null ? null : nextPrcp.trim();
	}
	
    /**
     * @return NextPrcp
     */	
	public String getNextPrcp() {
		return this.nextPrcp;
	}
	
	/**
	 * @param nextInt
	 */
	public void setNextInt(String nextInt) {
		this.nextInt = nextInt == null ? null : nextInt.trim();
	}
	
    /**
     * @return NextInt
     */	
	public String getNextInt() {
		return this.nextInt;
	}
	
	/**
	 * @param selfPrcp
	 */
	public void setSelfPrcp(String selfPrcp) {
		this.selfPrcp = selfPrcp == null ? null : selfPrcp.trim();
	}
	
    /**
     * @return SelfPrcp
     */	
	public String getSelfPrcp() {
		return this.selfPrcp;
	}
	
	/**
	 * @param selfInt
	 */
	public void setSelfInt(String selfInt) {
		this.selfInt = selfInt == null ? null : selfInt.trim();
	}
	
    /**
     * @return SelfInt
     */	
	public String getSelfInt() {
		return this.selfInt;
	}
	
	/**
	 * @param checkStatus
	 */
	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus == null ? null : checkStatus.trim();
	}
	
    /**
     * @return CheckStatus
     */	
	public String getCheckStatus() {
		return this.checkStatus;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate == null ? null : loanEndDate.trim();
	}
	
    /**
     * @return LoanEndDate
     */	
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus == null ? null : accStatus.trim();
	}
	
    /**
     * @return AccStatus
     */	
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(String loanBalance) {
		this.loanBalance = loanBalance == null ? null : loanBalance.trim();
	}
	
    /**
     * @return LoanBalance
     */	
	public String getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(String loanAmt) {
		this.loanAmt = loanAmt == null ? null : loanAmt.trim();
	}
	
    /**
     * @return LoanAmt
     */	
	public String getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param newLoanAmt
	 */
	public void setNewLoanAmt(String newLoanAmt) {
		this.newLoanAmt = newLoanAmt == null ? null : newLoanAmt.trim();
	}
	
    /**
     * @return NewLoanAmt
     */	
	public String getNewLoanAmt() {
		return this.newLoanAmt;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}