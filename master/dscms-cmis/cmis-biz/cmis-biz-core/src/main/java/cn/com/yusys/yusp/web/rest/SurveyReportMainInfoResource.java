///*
// * 代码生成器自动生成的
// * Since 2008 - 2021
// *
// */
//package cn.com.yusys.yusp.web.rest;
//
//import java.net.URISyntaxException;
//import java.util.List;
//import java.util.Map;
//import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
//
//import cn.com.yusys.yusp.service.SurveyConInfoService;
//import cn.com.yusys.yusp.service.SurveyReportBasicInfoService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
//import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
//import cn.com.yusys.yusp.domain.SurveyReportMainInfo;
//import cn.com.yusys.yusp.service.SurveyReportMainInfoService;
//
///**
// * @项目名称: cmis-biz-core模块
// * @类名称: SurveyReportMainInfoResource
// * @类描述: #资源类
// * @功能描述:
// * @创建人: Administrator
// * @创建时间: 2021-04-12 10:02:12
// * @修改备注:
// * @修改记录: 修改时间    修改人员    修改原因
// * -------------------------------------------------------------
// * @version 1.0.0
// * @Copyright (c) 宇信科技-版权所有
// */
//@RestController
//@RequestMapping("/api/surveyreportmaininfo")
//public class SurveyReportMainInfoResource {
//    @Autowired
//    private SurveyReportMainInfoService surveyReportMainInfoService;
//
//	/**
//     * 全表查询.
//     *
//     * @return
//     */
//    @GetMapping("/query/all")
//    protected ResultDto<List<SurveyReportMainInfo>> query() {
//        QueryModel queryModel = new QueryModel();
//        List<SurveyReportMainInfo> list = surveyReportMainInfoService.selectAll(queryModel);
//        return new ResultDto<List<SurveyReportMainInfo>>(list);
//    }
//
//    /**
//     * @函数名称:index
//     * @函数描述:查询对象列表，公共API接口
//     * @参数与返回说明:
//     * @param queryModel
//     *            分页查询类
//     * @算法描述:
//     */
//    @GetMapping("/")
//    protected ResultDto<List<SurveyReportMainInfo>> index(QueryModel queryModel) {
//        List<SurveyReportMainInfo> list = surveyReportMainInfoService.selectByModel(queryModel);
//        return new ResultDto<List<SurveyReportMainInfo>>(list);
//    }
//
//    /**
//     * @函数名称:show
//     * @函数描述:查询单个对象，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @GetMapping("/{surveyNo}")
//    protected ResultDto<SurveyReportMainInfo> show(@PathVariable("surveyNo") String surveyNo) {
//        SurveyReportMainInfo surveyReportMainInfo = surveyReportMainInfoService.selectByPrimaryKey(surveyNo);
//        return new ResultDto<SurveyReportMainInfo>(surveyReportMainInfo);
//    }
//
//    /**
//     * @函数名称:create
//     * @函数描述:实体类创建，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/")
//    protected ResultDto<SurveyReportMainInfo> create(@RequestBody SurveyReportMainInfo surveyReportMainInfo) throws URISyntaxException {
//        surveyReportMainInfoService.insert(surveyReportMainInfo);
//        return new ResultDto<SurveyReportMainInfo>(surveyReportMainInfo);
//    }
//
//    /**
//     * @函数名称:update
//     * @函数描述:对象修改，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/update")
//    protected ResultDto<Integer> update(@RequestBody SurveyReportMainInfo surveyReportMainInfo) throws URISyntaxException {
//        int result = surveyReportMainInfoService.update(surveyReportMainInfo);
//        return new ResultDto<Integer>(result);
//    }
//
//
//    /**
//     * @函数名称:delete
//     * @函数描述:单个对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/delete/{surveyNo}")
//    protected ResultDto<Integer> delete(@PathVariable("surveyNo") String surveyNo) {
//        int result = surveyReportMainInfoService.deleteByPrimaryKey(surveyNo);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @函数名称:batchdelete
//     * @函数描述:批量对象删除，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/batchdelete/{ids}")
//    protected ResultDto<Integer> deletes(@PathVariable String ids) {
//        int result = surveyReportMainInfoService.deleteByIds(ids);
//        return new ResultDto<Integer>(result);
//    }
//
//    /**
//     * @函数名称:HandleAgain
//     * @函数描述:根据调查主表主键，自动生成一个新的流水号，赋值原来的信息，更改审批状态，公共API接口
//     * @参数与返回说明:
//     * @算法描述:
//     */
//    @PostMapping("/handle/{serno}")
//    protected Map  HandleAgain(@PathVariable String serno) {
//        Map   rtnData = surveyReportMainInfoService.handleAgain(serno);
//        return rtnData;
//    }
//    /**
//     * @创建人 WH
//     * @创建时间 16:44 2021-04-14
//     * @return 修改状态为 审批中
//     **/
//    @PostMapping("/updatastatus/{id}")
//    protected ResultDto<Integer> updatastatus(@PathVariable String id) {
//        ResultDto<Integer> resultDto= surveyReportMainInfoService.updatastatus(id);
//        return resultDto;
//    }
//    /**
//     * @创建人 WH
//     * @创建时间 2021-04-16 16:46
//     * @注释 根据插入字段新增一条数据
//     */
//    @PostMapping("/installonedata")
//    protected ResultDto<SurveyReportMainInfo> installonedata(@RequestBody SurveyReportMainInfo surveyReportMainInfo) {
//        return surveyReportMainInfoService.installonedata(surveyReportMainInfo);
//    }
//
//    /***
//     * @param map
//     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
//     * @author 王玉坤
//     * @date 2021/4/17 13:51
//     * @version 1.0.0
//     * @desc 调查报告提交审批校验
//     * @修改历史:
//     */
//    @PostMapping(value = "/submit")
//    public ResultDto<Boolean> submit(@RequestBody Map<String,Object> map) {
//          return surveyReportMainInfoService.submit((String)map.get("surveyNo"));
//    }
//}
//
