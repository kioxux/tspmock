package cn.com.yusys.yusp.service.server.xdht0041;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0041.req.Xdht0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0041.resp.Xdht0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpChgTrupayAcctAppMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0041Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-04-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0041Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0041Service.class);

    @Resource
    private IqpChgTrupayAcctAppMapper iqpChgTrupayAcctAppMapper;

    /**
     * XDHT0041 查询受托记录状态
     *
     * @param xdht0041DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0041DataRespDto getXdht0041(Xdht0041DataReqDto xdht0041DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041DataReqDto));
        Xdht0041DataRespDto xdht0041DataRespDto = new Xdht0041DataRespDto();
        try {
            xdht0041DataRespDto = new Xdht0041DataRespDto();
            if (StringUtils.isEmpty(xdht0041DataReqDto.getAccotPk())) {
                throw BizException.error(null, EcbEnum.ECB010011.key, EcbEnum.ECB010011.value);
            }
            if (StringUtils.isEmpty(xdht0041DataReqDto.getBillNo())) {
                throw BizException.error(null, EcbEnum.ECB010012.key, EcbEnum.ECB010012.value);
            }
            String flag = "0";
            //非审批通过非否决
            int num = iqpChgTrupayAcctAppMapper.getXdht0041(xdht0041DataReqDto);
            if (num > 0) {
                flag = CmisBizConstants.STATUS_CODE_1;
            } else {
                //审批通过
                int num1 = iqpChgTrupayAcctAppMapper.getApprPassNum(xdht0041DataReqDto);
                if (num1 > 0) {
                    flag = CmisBizConstants.STATUS_CODE_2;
                }
            }
            xdht0041DataRespDto.setStatusCode(flag);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041DataRespDto));
        return xdht0041DataRespDto;
    }
}
