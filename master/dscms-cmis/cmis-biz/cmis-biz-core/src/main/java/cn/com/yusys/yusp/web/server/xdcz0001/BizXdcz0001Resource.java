package cn.com.yusys.yusp.web.server.xdcz0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0001.req.Xdcz0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0001.resp.Xdcz0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0001.Xdcz0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:电子保函开立
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0001:电子保函开立")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0001Resource.class);
    @Autowired
    private Xdcz0001Service xdcz0001Service;

    /**
     * 交易码：xdcz0001
     * 交易描述：电子保函开立
     *
     * @param xdcz0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("电子保函开立")
    @PostMapping("/xdcz0001")
    protected @ResponseBody
    ResultDto<Xdcz0001DataRespDto> xdcz0001(@Validated @RequestBody Xdcz0001DataReqDto xdcz0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, JSON.toJSONString(xdcz0001DataReqDto));
        Xdcz0001DataRespDto xdcz0001DataRespDto = new Xdcz0001DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0001DataRespDto> xdcz0001DataResultDto = new ResultDto<>();


        try {
            // 从xdcz0001DataResultDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, JSON.toJSONString(xdcz0001DataReqDto));
            xdcz0001DataRespDto = xdcz0001Service.xdcz0001(xdcz0001DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, JSON.toJSONString(xdcz0001DataReqDto));
            // 封装xdcz0001DataResultDto中正确的返回码和返回信息
            xdcz0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            xdcz0001DataResultDto.setCode(e.getErrorCode());
            xdcz0001DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            // 封装xdcz0001DataResultDto中异常返回码和返回信息
            xdcz0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0001DataRespDto到xdcz0001DataResultDto中
        xdcz0001DataResultDto.setData(xdcz0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, JSON.toJSONString(xdcz0001DataRespDto));
        return xdcz0001DataResultDto;
    }
}
