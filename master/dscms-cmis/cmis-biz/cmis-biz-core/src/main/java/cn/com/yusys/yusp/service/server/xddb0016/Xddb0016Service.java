package cn.com.yusys.yusp.service.server.xddb0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0016.req.Xddb0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0016.resp.List;
import cn.com.yusys.yusp.dto.server.xddb0016.resp.Xddb0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:押品信息查询
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xddb0016Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0016Service.class);

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    /**
     * 押品信息查询
     *
     * @param xdkh0016DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0016DataRespDto selcetGuarBaseInfoByCusName(Xddb0016DataReqDto xdkh0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value);
        Xddb0016DataRespDto xddb0016DataRespDto = new Xddb0016DataRespDto();
        try {
            String guarCusName = xdkh0016DataReqDto.getGuarna();//抵押人名称
            String ypPage = xdkh0016DataReqDto.getYppage();//页码
            String ypSize = xdkh0016DataReqDto.getYpsize();//每页条数
            //分页查询
            int startPageNum = 1;//页码
            int pageSize = 10;//每页条数
            if (StringUtil.isNotEmpty(ypPage) && StringUtil.isNotEmpty(ypSize)) {
                startPageNum = Integer.parseInt(ypPage);
                pageSize = Integer.parseInt(ypSize);
            }

            if (StringUtil.isNotEmpty(guarCusName)) {//押品编号非空校验
                logger.info("**************************根据抵押人名称查询所属押品信息**************************");
                // 调用xdxw0013Service层开始
                Map QueryMap = new HashMap();
                QueryMap.put("guarCusName", guarCusName);//抵押人名称
                QueryMap.put("ypPage", ypPage);//页码
                QueryMap.put("ypSize", ypSize);//每页条数
                PageHelper.startPage(startPageNum, pageSize);
                java.util.List<List> lists = guarBaseInfoMapper.selectGuarBaseInfoByGuarName(QueryMap);
                PageHelper.clearPage();
                xddb0016DataRespDto.setList(lists);
            } else {
                //请求参数不存在,返回空值
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value);
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value);
        return xddb0016DataRespDto;
    }
}
