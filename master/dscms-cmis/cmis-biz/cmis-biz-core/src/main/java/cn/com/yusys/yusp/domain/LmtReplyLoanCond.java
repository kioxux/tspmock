/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyLoanCond
 * @类描述: lmt_reply_loan_cond数据实体类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:33:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_reply_loan_cond")
public class LmtReplyLoanCond extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 审批主键 **/
	@Column(name = "REPLY_SERNO", unique = false, nullable = false, length = 40)
	private String replySerno;

	/** 条件类型 **/
	@Column(name = "COND_TYPE", unique = false, nullable = true, length = 2000)
	private String condType;

	/** 条件说明 **/
	@Column(name = "COND_DESC", unique = false, nullable = true, length = 2000)
	private String condDesc;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 10)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 10)
	private java.util.Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}

	/**
	 * @return replySerno
	 */
	public String getReplySerno() {
		return this.replySerno;
	}

	/**
	 * @param condType
	 */
	public void setCondType(String condType) {
		this.condType = condType;
	}

	/**
	 * @return condType
	 */
	public String getCondType() {
		return this.condType;
	}

	/**
	 * @param condDesc
	 */
	public void setCondDesc(String condDesc) {
		this.condDesc = condDesc;
	}

	/**
	 * @return condDesc
	 */
	public String getCondDesc() {
		return this.condDesc;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}