package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.CommonUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusForZxdDto;
import cn.com.yusys.yusp.dto.CusUpdateInitLoanDateDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd02.Fbyd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd02.Fbyd02RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003.Wxd003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003.Wxd003RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.req.Znsp08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.resp.Znsp08RespDto;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001LmtSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.LoanContList;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizXwEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw01.Fbxw01Service;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw03.Fbxw03Service;
import cn.com.yusys.yusp.service.client.bsp.rircp.fbxw05.Fbxw05Service;
import cn.com.yusys.yusp.service.client.bsp.rircp.znsp04.Znsp04Service;
import cn.com.yusys.yusp.service.client.bsp.znwdspxt.znsp08.Znsp08Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportMainInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-20 14:19:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class LmtSurveyReportMainInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportMainInfoService.class);

    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoService;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private LmtModelApprResultInfoMapper lmtModelApprResultInfoMapper;
    @Autowired
    private LmtSurveyReportComInfoService lmtSurveyReportComInfoService;
    @Autowired
    private LmtSurveyConInfoService lmtSurveyConInfoService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private LmtModelApprResultInfoService lmtModelApprResultInfoService;
    @Autowired
    private LmtPerferRateApplyInfoService lmtPerferRateApplyInfoService;
    @Autowired
    private Fbxw03Service fbxw03Service;
    @Autowired
    private Fbxw01Service fbxw01Service;
    @Autowired
    private LmtCrdReplyInfoService lmtCrdReplyInfoService;
    @Autowired
    private Fbxw05Service fbxw05Service;
    @Autowired
    private Znsp04Service znsp04Service;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private DscmsCusClientService dscmsCusClientService;
    @Autowired
    private CreditReportQryLstService creditReportQryLstService;
    @Autowired
    private LmtCobInfoService lmtCobInfoService;
    @Autowired
    private Dscms2ImageClientService dscms2ImageClientService;
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;
    @Autowired
    private Dscms2XwywglptClientService dscms2XwywglptClientService;
    @Autowired
    private LmtEgcInfoService lmtEgcInfoService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private Znsp08Service znsp08Service;
    @Autowired
    private Dscms2Ciis2ndClientService dscms2Ciis2ndClientService;
    @Autowired
    private CusLstZxdService cusLstZxdService;
    @Autowired
    private CusLstMclWhbxdService cusLstMclWhbxdService;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSurveyReportMainInfo selectByPrimaryKey(String surveySerno) {
        return lmtSurveyReportMainInfoMapper.selectByPrimaryKey(surveySerno);
    }

    /**
     * @param listSerno
     * @return cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo
     * @author 王玉坤
     * @date 2021/10/7 20:58
     * @version 1.0.0
     * @desc 根据名单流水号信息查询调查表主表信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public LmtSurveyReportMainInfo selectByListSerno(String listSerno) {
        return lmtSurveyReportMainInfoMapper.selectByListSerno(listSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtSurveyReportMainInfo> selectAll(QueryModel model) {
        List<LmtSurveyReportMainInfo> records = (List<LmtSurveyReportMainInfo>) lmtSurveyReportMainInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSurveyReportMainInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSurveyReportMainInfo> list = lmtSurveyReportMainInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtSurveyReportMainInfo record) {
        return lmtSurveyReportMainInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtSurveyReportMainInfo record) {
        return lmtSurveyReportMainInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtSurveyReportMainInfo record) {
        return lmtSurveyReportMainInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtSurveyReportMainInfo record) {
        return lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String surveySerno) {
        return lmtSurveyReportMainInfoMapper.deleteByPrimaryKey(surveySerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtSurveyReportMainInfoMapper.deleteByIds(ids);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/31 19:50
     * @注释 重新办理授信表
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto handleAgain(LmtSurveyReportMainInfo mainInfo) {
        String serno = mainInfo.getSurveySerno();
        /*“是否需要线下调查=否”不允许点击【重新办理】。
        （3）风险拦截校验。
        （4）拦截校验通过，则弹出重新办理成功提示。重新办理成功，授信调查待办表生成一条“审批状态=待发起”的待办任务，流水号重新生成，
        信息完全复制；当前选择的授信批复“批复状态”更新为“重新办理”。
        影像信息需要重新上传。
        3、调查流水号：链接的形式，点击链接，可以查看授信调查详情信息。*/

        //查询客户授信调查主表数据
        // “是否需要线下调查=否”不允许点击【重新办理】。
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(serno);
        if (lmtSurveyReportMainInfo.getIsStopOffline() == null) {
            return new ResultDto(false).message("该调查单 是否需要线下调查 字段为空 禁止重新办理!");
        }
        if (lmtSurveyReportMainInfo.getIsStopOffline().equals("0")) {
            return new ResultDto(false).message("该调查单不需要线下调查 不允许重新办理!");
        }
        //系统校验该授信批复项下是否已存在在途的合同和放款，若存在，则拦截并提示；（需提示到具体的数据流水号）。
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("surveySerno", serno);

        List<CtrLoanContAndPvp> list = ctrLoanContMapper.selectContAndPvp(serno);
        if (list != null && list.size() > 0) {
            //存在生效的合同 返回合同信息

            StringBuffer message = new StringBuffer("禁止重新办理!该授信单下存在生效的合同:");
            StringBuffer message2 = new StringBuffer("禁止重新办理!该授信单下存在流程中的放款单:");
            AtomicInteger integer = new AtomicInteger();
            AtomicInteger integer2 = new AtomicInteger();
            list.stream().forEach(
                    // TODO  收集合同状态=200和放款状态=111的数据
                    s -> {
                        if (s.getContStatus().equals("200")) {
                            message.append(s.getContNo() + ',');
                            integer.getAndIncrement();
                        }
                        if (s.getApproveStatus() != null && s.getApproveStatus().equals("111")) {
                            message2.append(s.getPvpSerno() + ",");
                            integer2.getAndIncrement();
                        }
                    }
            );

            if (integer.get() > 0) {
                return new ResultDto(false).message(message.toString());
            }
            if (integer2.get() > 0) {
                return new ResultDto(false).message(message2.toString());
            }

        }

        //查询调查报告基本信息
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(serno);
        LmtSurveyReportComInfo lmtSurveyReportComInfo = lmtSurveyReportComInfoService.selectByPrimaryKey(serno);
        //查询调查结论信息
        LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(serno);

        int result = 0;
        int result2 = 0;
        int result3 = 0;
        int result4 = 0;

        try {
            // 修改批复信息
            int updataCrd = lmtCrdReplyInfoService.updateSurveySerno(serno);
            if (updataCrd <= 0) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",修改批复信息失败！");
            }
            //生成新的流水号
            serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
            //复制客户授信调查主表数据
            lmtSurveyReportMainInfo.setSurveySerno(serno);
            lmtSurveyReportMainInfo.setApproveStatus("000");
            Date date = new Date();
            lmtSurveyReportMainInfo.setCreateTime(date);
            lmtSurveyReportMainInfo.setUpdateTime(date);
            result = lmtSurveyReportMainInfoMapper.insertSelective(lmtSurveyReportMainInfo);
            if (result != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成授信调查信息失败！");
            }
            //复制调查报告基本信息
            if (lmtSurveyReportBasicInfo != null) {
                lmtSurveyReportBasicInfo.setSurveySerno(serno);
                result2 = lmtSurveyReportBasicInfoService.insertSelective(lmtSurveyReportBasicInfo);
                if (result2 != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成授信调查信息失败！");
                }
            }
            //复制调查结论信息
            if (lmtSurveyConInfo != null) {
                lmtSurveyConInfo.setSurveySerno(serno);
                result3 = lmtSurveyConInfoService.insertSelective(lmtSurveyConInfo);
                if (result3 != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成授信调查信息失败！");
                }
            }
            //复制企业信息

            if (lmtSurveyReportComInfo != null) {
                lmtSurveyReportComInfo.setSurveySerno(serno);
                result4 = lmtSurveyReportComInfoService.insertSelective(lmtSurveyReportComInfo);
                if (result4 != 1) {
                    //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                    throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",生成授信调查信息失败！");
                }
            }
            //TODO 都成功之后 要去作废原有批复信息 废除原有额度 2021年7月20日09:58:23  wh
            CmisLmt0008ReqDto cmisLmt0008ReqDto = new CmisLmt0008ReqDto();
            cmisLmt0008ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);// 系统编号
            cmisLmt0008ReqDto.setInputBrId(lmtSurveyReportMainInfo.getManagerId());
            cmisLmt0008ReqDto.setInputBrId(lmtSurveyReportMainInfo.getManagerBrId());
            cmisLmt0008ReqDto.setInputDate(LocalDate.now().toString());
            cmisLmt0008ReqDto.setLmtType("01");
            //查询批复编号
            LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectBySurveySerno(mainInfo.getSurveySerno());
            if (lmtCrdReplyInfo == null) {
                throw new YuspException("未查询到批复数据", "未查询到批复数据");
            }
            List ApprList = new ArrayList();
            CmisLmt0008ApprListReqDto cmisLmt0008ApprListReqDto = new CmisLmt0008ApprListReqDto();
            cmisLmt0008ApprListReqDto.setCusId(lmtSurveyReportMainInfo.getCusId());
            cmisLmt0008ApprListReqDto.setApprSerno(lmtCrdReplyInfo.getReplySerno());//批复编号
            cmisLmt0008ApprListReqDto.setOptType("02");
            ApprList.add(cmisLmt0008ApprListReqDto);
            cmisLmt0008ReqDto.setApprList(ApprList);

            List ApprSubList = new ArrayList();
            CmisLmt0008ApprSubListReqDto cmisLmt0008ApprSubListReqDto = new CmisLmt0008ApprSubListReqDto();
            cmisLmt0008ApprSubListReqDto.setCusId(lmtSurveyReportMainInfo.getCusId());
            cmisLmt0008ApprSubListReqDto.setApprSubSerno(lmtCrdReplyInfo.getReplySerno());//批复编号
            ApprSubList.add(cmisLmt0008ApprSubListReqDto);
            cmisLmt0008ReqDto.setApprSubList(ApprSubList);
            logger.info("根据调查流水号{}调用cmisLmt0008RespDtoResultDto，开始！", mainInfo.getSurveySerno());
            ResultDto<CmisLmt0008RespDto> cmisLmt0008RespDtoResultDto = cmisLmtClientService.cmisLmt0008(cmisLmt0008ReqDto);
            logger.info("根据调查流水号{}调用cmisLmt0008RespDtoResultDto，成功！", cmisLmt0008RespDtoResultDto.toString());
            // 2021年9月9日14:18:40 hubp 如果为惠享贷，前往风控调用fbxw05交易，接着调用智能审贷系统znsp08交易，最后作废批复信息
            if (!StringUtils.isBlank(lmtSurveyReportMainInfo.getPrdId()) && "SC060001".equals(lmtSurveyReportMainInfo.getPrdId())) {
                // 调用内部方法
                this.hxdZf(lmtSurveyReportMainInfo);
            }
        } catch (Exception e) {
            logger.error("根据调查流水号{}调用cmisLmt0008RespDtoResultDto，异常！", mainInfo.getSurveySerno(), e.getMessage(), e);
            throw e;
        }
        return new ResultDto(result);
    }

    /**
     * @param mainInfo
     * @return void
     * @author hubp
     * @date 2021/9/9 14:40
     * @version 1.0.0
     * @desc 重新办理  如果为惠享贷--- 前往风控调用fbxw05交易，接着调用智能审贷系统znsp08交易
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void hxdZf(LmtSurveyReportMainInfo mainInfo) {
        try {
            Fbxw05ReqDto fbxw05ReqDto = new Fbxw05ReqDto();
            LmtCrdReplyInfo crdReplyInfo = lmtCrdReplyInfoService.selectBySurveySerno(mainInfo.getSurveySerno());
            //拼装报文
            // channel_type渠道来源
            fbxw05ReqDto.setChannel_type(CmisBizConstants.XW_CHANNEL_TYPE_11);
            // co_platform合作平台1002
            fbxw05ReqDto.setCo_platform(CmisBizConstants.XW_PLATFORM_1002);
            // app_no申请流水号 非必传
            fbxw05ReqDto.setApp_no(mainInfo.getBizSerno());
            // prd_type产品类别000020
            fbxw05ReqDto.setPrd_type(CmisBizConstants.CODE_HXD);
            // prd_code产品代码（零售智能风控内部代码）1002000020
            fbxw05ReqDto.setPrd_code(CmisBizConstants.LSFK_PRD_CODE_HXD);
            // survey_serno调查表编号
            fbxw05ReqDto.setSurvey_serno(mainInfo.getSurveySerno());
            //fbxw05ReqDto.setSurvey_serno(crdReplyInfo.getSurveySerno());
            // app_status批复状态00 批复通过  01 批复拒绝   03：合同已签订
            fbxw05ReqDto.setApp_status(CmisBizConstants.XW_APP_STATUS_01);
            // appr_amt终审额度批复通过时必输
            fbxw05ReqDto.setAppr_amt(crdReplyInfo.getReplyAmt().toString());
            // crd_start_date授信起始日期yyyy-MM-dd
            fbxw05ReqDto.setCrd_start_date(crdReplyInfo.getReplyStartDate());
            // crd_end_date授信结束日期yyyy-MM-dd
            fbxw05ReqDto.setCrd_end_date(crdReplyInfo.getReplyEndDate());
            // crd_term授信期限(月)
            fbxw05ReqDto.setCrd_term(crdReplyInfo.getAppTerm().toString());
            // appr_rate审批利率
            fbxw05ReqDto.setAppr_rate(crdReplyInfo.getExecRateYear().toString());
            // refusal_reason拒绝原因批复拒接时必输
            fbxw05ReqDto.setRefusal_reason("作废");
            logger.error("根据调查流水号{}作废惠享贷，调用fbxw05，传入数据：【{}】", JSON.toJSONString(fbxw05ReqDto));
            Fbxw05RespDto fbxw05RespDto = fbxw05Service.fbxw05(fbxw05ReqDto);
            logger.error("根据调查流水号{}作废惠享贷，调用fbxw05，返回数据：【{}】", JSON.toJSONString(fbxw05RespDto));
            // 开始调用 znsp08
            Znsp08ReqDto znsp08ReqDto = new Znsp08ReqDto();
            znsp08ReqDto.setSuvery_serno(mainInfo.getSurveySerno());
            logger.error("根据调查流水号{}作废惠享贷，调用znsp08，传入数据：【{}】", JSON.toJSONString(znsp08ReqDto));
            Znsp08RespDto znsp08RespDto = znsp08Service.znsp08(znsp08ReqDto);
            logger.error("根据调查流水号{}作废惠享贷，调用znsp08，返回数据：【{}】", JSON.toJSONString(znsp08RespDto));

        } catch (Exception e) {
            logger.error("根据调查流水号{}重新办理惠享贷失败，异常信息：【{}】！", JSON.toJSONString(e));
        }
    }

    /**
     * @创建人 WH
     * @创建时间 2021-04-16 16:48
     * @注释 生成流水号 根据字段插入 记得修改 此时没有默认产品类型 无默认调查报告类型 均用假数据 因为无法根据产品信息带出相应数据
     */
    public ResultDto<LmtSurveyReportMainInfo> installonedata(LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        //生成新的流水号
        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
        lmtSurveyReportMainInfo.setSurveySerno(serno);
        //审批状态为待发起
        lmtSurveyReportMainInfo.setApproveStatus(CmisBizConstants.STD_ZB_APP_ST_000);
        //数据来源为人工新增
        lmtSurveyReportMainInfo.setDataSource(CmisBizConstants.STD_DATA_SOUR_00);
        //需要线下调查  是
        lmtSurveyReportMainInfo.setIsStopOffline("1");
        //是否自动分配 否
        lmtSurveyReportMainInfo.setIsAutodivis("0");
        //操作类型 新增
        lmtSurveyReportMainInfo.setOprType("01");

        //产品类型
//        lmtSurveyReportMainInfo.setPrdType();
        //此时是假数据
//        if (lmtSurveyReportMainInfo.getSurveyType().equals("")) {
//            lmtSurveyReportMainInfo.setSurveyType("03");
//        }
        //获取调查表类型  TODO 理论上来说是根据产品类型去想办法取到调查表类型 莫名其妙变成这样子了
        this.setData(lmtSurveyReportMainInfo);
        lmtSurveyReportMainInfoMapper.insertSelective(lmtSurveyReportMainInfo);
        //这时候去新增主借款人的征信信息
        int i = lmtCobInfoService.addHXD(lmtSurveyReportMainInfo);
        return new ResultDto<>(lmtSurveyReportMainInfo).message("新增成功");
    }

    /**
     * @创建人 WH
     * @创建时间 2021/6/1 10:55
     * @注释 检测流程能不能提交审批 风控校验数据
     */

    public ResultDto<Boolean> submit(String surveySerno) {
        // 调查报告主表信息
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = null;
        // 调查报告基本信息
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = null;
        //调查报告企业信息
        LmtSurveyReportComInfo lmtSurveyReportComInfo = null;
        // 模型审批结果
        LmtModelApprResultInfo lmtModelApprResultInfo = null;
        // 查询优惠利率审批
        LmtPerferRateApplyInfo lmtPerferRateApplyInfo = null;

        // 1.根据流水号查询调查报告基本信息，若无，则提示客户未完善信息
        lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(surveySerno);
        if (lmtSurveyReportMainInfo == null) {
            logger.info("根据调查流水号{}未获取到调查报告基本信息，不允许提交！", surveySerno);
            return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("未查询到授信单信息");
        }

        String managerId = lmtSurveyReportMainInfo.getManagerId();
        if (!managerId.equals(SessionUtils.getUserInformation().getLoginCode())) {
            return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("只能提交责任人为自己的授信单");
        }
        lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(surveySerno);
        if (lmtSurveyReportBasicInfo == null) {
            logger.info("根据调查流水号{}未获取到调查报告基本信息，不允许提交！", surveySerno);
            return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("请完善调查报告基本信息信息");
        }
        lmtSurveyReportComInfo = lmtSurveyReportComInfoService.selectByPrimaryKey(surveySerno);
        if (lmtSurveyReportComInfo == null) {
            logger.info("根据调查流水号{}未获取到调查报告基本信息，不允许提交！", surveySerno);
            return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("请完善调查报告企业信息");
        }
        LmtSurveyConInfo conInfo = lmtSurveyConInfoService.selectByPrimaryKey(surveySerno);
        if (conInfo == null) {
            logger.info("根据调查流水号{}未获取到调查结论信息，不允许提交！", surveySerno);
            return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("请完善调查结论");
        }
        // 2021年9月23日20:33:51 hubp 新增如果为惠享贷，判断有无共借人
        if ("11".equals(lmtSurveyReportMainInfo.getSurveyType())) {
            List<LmtCobInfo> lmtCobInfoList = lmtCobInfoService.selectByIqpSerno(lmtSurveyReportMainInfo.getSurveySerno());
            if (lmtCobInfoList.size() == 0) {
                logger.info("根据调查流水号{}未获取到惠享贷共同借款人信息，不允许提交！", surveySerno);
                return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("请添加共同借款人");
            }
        }
        //校验是否完成影像
        ImageDataReqDto imageDataReqDto = new ImageDataReqDto();
        imageDataReqDto.setDocId(surveySerno);
        imageDataReqDto.setOutSystemCode("ZX_HXD");
        logger.info("影像信息发送报文【{}】", JSON.toJSON(imageDataReqDto));
        ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
        logger.info("影像信息返回报文【{}】", JSON.toJSON(map));
        if (map.getData() == null || map.getData().size() == 0) {
            logger.info("根据调查流水号{}未获取到影像信息，不允许提交！", surveySerno);
            return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("请上传影像资料");
        }

        // 2.根据不同调查报告类型，做不同逻辑判断
        // 2.1 惠享贷
        if ("11".equals(lmtSurveyReportMainInfo.getSurveyType())) {
            // 2.1.1先查询是否完成模型审批
            lmtModelApprResultInfo = lmtModelApprResultInfoService.selectByPrimaryKey(surveySerno);
            if (lmtModelApprResultInfo == null) {

                return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("请先进行模型审批");
            } else {
                // 校验模型审批是否通过
                if (!"003".equals(lmtModelApprResultInfo.getModelRstStatus())) {

                    return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("模型审批未通过");
                }
            }
            // 2.1.2查询优惠利率申请是否在审批中
            lmtPerferRateApplyInfo = lmtPerferRateApplyInfoService.selectByPrimaryKey(surveySerno);
            if (lmtPerferRateApplyInfo != null && !CmisCommonConstants.WF_STATUS_997.equals(lmtPerferRateApplyInfo.getApproveStatus())) {

                return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("存在审批中的优惠利率申请");
            }
        }
        // 返回结果
        return new ResultDto<Boolean>(Boolean.TRUE);
    }

    /**
     * @param lmtCrdReplyInfo
     * @return ResultDto
     * @author hubp
     * @date 2021/6/4 15:12
     * @version 1.0.0
     * @desc 小微授信批复 - 前往额度系统占用额度
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto handleBizEnd(LmtCrdReplyInfo lmtCrdReplyInfo) {
        //LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoService.selectByPrimaryKey(replySerno);
        logger.info("授信批复 - 前往额度系统占用额度开始【{}】", lmtCrdReplyInfo.getSurveySerno());
        ResultDto<CmisLmt0001RespDto> cmisLmt0001RespDto = null;
        try {
            // 2、前往额度系统建立额度
            // 组装额度系统报文
            CmisLmt0001ReqDto cmisLmt0001ReqDto = new CmisLmt0001ReqDto();
            cmisLmt0001ReqDto.setSysId(CmisCommonConstants.SYS_SHORT_NAME);
            cmisLmt0001ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
            cmisLmt0001ReqDto.setAccNo(lmtCrdReplyInfo.getSurveySerno());//批复台账编号
            cmisLmt0001ReqDto.setCurType(lmtCrdReplyInfo.getCurType());//币种
            cmisLmt0001ReqDto.setCusId(lmtCrdReplyInfo.getCusId());
            cmisLmt0001ReqDto.setCusName(lmtCrdReplyInfo.getCusName());
            cmisLmt0001ReqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG1); // 个人客户
            cmisLmt0001ReqDto.setIsCreateAcc("0");//是否生成新批复台账
            cmisLmt0001ReqDto.setLmtAmt(lmtCrdReplyInfo.getReplyAmt());//授信金额
            cmisLmt0001ReqDto.setLmtMode(CmisLmtConstants.STD_ZB_LMT_MODE_02);//批复台账状态
            cmisLmt0001ReqDto.setTerm(lmtCrdReplyInfo.getAppTerm());//授信期限
            cmisLmt0001ReqDto.setStartDate(lmtCrdReplyInfo.getReplyStartDate());//起始日
            cmisLmt0001ReqDto.setEndDate(lmtCrdReplyInfo.getReplyEndDate());//到期日
            cmisLmt0001ReqDto.setAccStatus(CmisLmtConstants.STD_ZB_APPR_ST_01);//批复台账状态
            cmisLmt0001ReqDto.setManagerId(lmtCrdReplyInfo.getManagerId());//责任人
            cmisLmt0001ReqDto.setManagerBrId(lmtCrdReplyInfo.getManagerBrId());//责任机构
            cmisLmt0001ReqDto.setInputId(lmtCrdReplyInfo.getInputId());//登记人
            cmisLmt0001ReqDto.setInputBrId(lmtCrdReplyInfo.getInputBrId());//登记机构
            cmisLmt0001ReqDto.setInputDate(lmtCrdReplyInfo.getInputDate());//登记日期
            /* 额度分项*/
            List<CmisLmt0001LmtSubListReqDto> CmisLmt0001LmtSubListReqDtoList = new ArrayList<>();
            CmisLmt0001LmtSubListReqDto cmisLmt0001LmtSubListReqDto = new CmisLmt0001LmtSubListReqDto();
            cmisLmt0001LmtSubListReqDto.setAccSubNo(lmtCrdReplyInfo.getReplySerno());//授信分项编号
            cmisLmt0001LmtSubListReqDto.setOrigiAccSubNo("");//原授信分项编号
            cmisLmt0001LmtSubListReqDto.setParentId("");//父节点
            cmisLmt0001LmtSubListReqDto.setLimitSubNo(lmtCrdReplyInfo.getPrdId());// 授信品种编号
            cmisLmt0001LmtSubListReqDto.setLimitSubName(lmtCrdReplyInfo.getPrdName()); //授信品种名称
            cmisLmt0001LmtSubListReqDto.setLmtSubType(CmisLmtConstants.STD_ZB_LMT_SUB_TYPE_02); //操作类型
            cmisLmt0001LmtSubListReqDto.setAvlamt(cmisLmt0001ReqDto.getLmtAmt()); // 授信金额
            cmisLmt0001LmtSubListReqDto.setCurType(cmisLmt0001ReqDto.getCurType()); // 币种
            cmisLmt0001LmtSubListReqDto.setTerm(cmisLmt0001ReqDto.getTerm());// 授信期限
            cmisLmt0001LmtSubListReqDto.setStartDate(cmisLmt0001ReqDto.getStartDate()); // 额度起始日
            cmisLmt0001LmtSubListReqDto.setEndDate(cmisLmt0001ReqDto.getEndDate()); // 额度到期日
            //cmisLmt0001LmtSubListReqDto.setBailPreRate();//批复保证金比例
            cmisLmt0001LmtSubListReqDto.setRateYear(lmtCrdReplyInfo.getExecRateYear()); //年利率
            cmisLmt0001LmtSubListReqDto.setSuitGuarWay(lmtCrdReplyInfo.getGuarMode()); // 担保方式
            cmisLmt0001LmtSubListReqDto.setLmtGraper(lmtCrdReplyInfo.getLmtGraper()); //宽限期
            cmisLmt0001LmtSubListReqDto.setIsRevolv("1"); // 是否可循环
            cmisLmt0001LmtSubListReqDto.setIsPreCrd("0"); // 是否预授信
            cmisLmt0001LmtSubListReqDto.setIsLriskLmt("0"); // 是否低风险
            cmisLmt0001LmtSubListReqDto.setAccSubStatus(CmisLmtConstants.STD_ZB_APPR_ST_01); // 批复分项状态
            cmisLmt0001LmtSubListReqDto.setOprType(CmisCommonConstants.OPR_TYPE_ADD); // 操作类型
            CmisLmt0001LmtSubListReqDtoList.add(cmisLmt0001LmtSubListReqDto);
            cmisLmt0001ReqDto.setLmtSubList(CmisLmt0001LmtSubListReqDtoList);

            // 调用额度接口
            logger.info("根据批复信息编号【" + lmtCrdReplyInfo.getSurveySerno() + "】前往额度系统-建立额度请求报文：" + cmisLmt0001ReqDto.toString());
            cmisLmt0001RespDto = cmisLmtClientService.cmisLmt0001(cmisLmt0001ReqDto);
            logger.info("根据批复信息编号【" + lmtCrdReplyInfo.getSurveySerno() + "】前往额度系统-建立额度返回报文：" + cmisLmt0001RespDto.toString());
        } catch (Exception e) {
            logger.error("小微授信批复额度同步发生异常【{}】", e.getMessage());
            return new ResultDto(null).code(9999).message("数据异常，同步失败");
        } finally {
            logger.info("小微授信批复额度同步结束【{}】", lmtCrdReplyInfo.getSurveySerno());
        }
        return cmisLmt0001RespDto;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/19 20:10
     * @注释 条件列表查询
     */
    public List<LmtSurveyReportMainInfoAndCrd> findlistbymodel(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LmtSurveyReportMainInfoAndCrd> list = lmtSurveyReportMainInfoMapper.findlistbymodel(queryModel);
        //(lmt_survey_report_main_info A left JOIN lmt_crd_reply_info b on a.SURVEY_SERNO =b.SURVEY_SERNO)  left join lmt_model_appr_result_info d on a.SURVEY_SERNO=d.SURVEY_SERNO
        if(list!=null && list.size()>0){
            List<String> surveySernos = new ArrayList<String>();
            for(int i=0 ;i<list.size();i++){
                LmtSurveyReportMainInfoAndCrd domain = list.get(i);
                if(!StringUtils.isBlank(domain.getSurveySerno())){
                    surveySernos.add(domain.getSurveySerno());
                }
            }
            if(surveySernos.size()>0){//拼接
                List<LmtSurveyReportMainInfoAndCrd> subListOne =  lmtCrdReplyInfoMapper.findBySurveySernos(surveySernos);
                List<LmtSurveyReportMainInfoAndCrd> subListTwo =  lmtModelApprResultInfoMapper.findBySurveySernos(surveySernos);

                for (int i = 0; i < list.size(); i++) {
                    LmtSurveyReportMainInfoAndCrd domain = list.get(i);
                    String surveySerno = domain.getSurveySerno();
                    if (!StringUtils.isBlank(surveySerno)) {
                        for (int j = 0; j < subListOne.size(); j++) {
                            if (subListOne.get(j)!=null && surveySerno.equals(subListOne.get(j).getSurveySerno())) {
                                domain.setReplyStatus(subListOne.get(j).getReplyStatus());
                                list.set(i, domain);
                                break;
                            }
                        }
                        for (int j = 0; j < subListTwo.size(); j++) {
                            if (subListTwo.get(j)!=null && surveySerno.equals(subListTwo.get(j).getSurveySerno())) {
                                domain.setModelRstStatus(subListTwo.get(j).getModelRstStatus());
                                list.set(i, domain);
                                break;
                            }
                        }
                    }
                }
            }
        }
        PageHelper.clearPage();
        return list;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/19 20:09
     * @注释 惠享待审批通过的操作 更新状态 增加批复信息 调用外部接口
     */
    public void endPass(String serno) {
        logger.info("惠享贷审批通过后续处理开始-流水号：【{}】", serno);
        String openDay = stringRedisTemplate.opsForValue().get("openDay");

        LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(serno);
        LmtCrdReplyInfo crdReplyInfo = new LmtCrdReplyInfo();
        BeanUtils.copyProperties(mainInfo, crdReplyInfo);
        String surveySerno = mainInfo.getSurveySerno();
        LmtSurveyConInfo conInfo = lmtSurveyConInfoService.selectByPrimaryKey(surveySerno);
        //查询模型结果
        LmtModelApprResultInfo lmtModelApprResultInfo = lmtModelApprResultInfoService.selectByPrimaryKey(surveySerno);

        //新的流水号
        serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
        crdReplyInfo.setReplySerno(serno);
        //补充其余条件
        //宽限期 额度接口要用 默认赛格0
        crdReplyInfo.setLmtGraper(0);
        // GUAR_MODE担保方式
        crdReplyInfo.setGuarMode(conInfo.getGuarMode());

        // 2021年9月8日11:21:04 hubp 根据BUG10424去除判断
        crdReplyInfo.setReplyAmt(conInfo.getAdviceAmt());
        crdReplyInfo.setCurtLoanAmt(conInfo.getAdviceAmt());//本次用信金额 TODO  增加了该字段 2021年7月21日16:45:08
        // TERM_TYPE期限类型
        crdReplyInfo.setTermType("M");
        // APP_TERM申请期限  adviceTerm  用的建议期限
        if (StringUtils.isBlank(conInfo.getAdviceTerm())) {
            crdReplyInfo.setAppTerm(0);
        } else {
            crdReplyInfo.setAppTerm(Integer.valueOf(conInfo.getAdviceTerm()));
        }
        // 批复日期先赋假值
        crdReplyInfo.setReplyStartDate(openDay);
        // ReplyEndDate批复到期日
        crdReplyInfo.setReplyEndDate(LocalDate.parse(openDay, DateTimeFormatter.ofPattern("yyyy-MM-dd")).plusMonths(crdReplyInfo.getAppTerm()).toString());

        // EXEC_RATE_YEAR执行年利率
        crdReplyInfo.setExecRateYear(conInfo.getAdviceRate());
        // REPAY_MODE还款方式
        crdReplyInfo.setRepayMode(conInfo.getRepayMode());
//         LIMIT_TYPE额度类型
        crdReplyInfo.setLimitType("02");
        // CURT_LOAN_AMT本次用信金额
        // IS_BE_ENTRUSTED_PAY是否受托支付
        // LOAN_COND_FLG是否有用信条件
        // TRU_PAY_TYPE受托类型
        // APPR_TYPE审批类型
        crdReplyInfo.setApprType(mainInfo.getApproveStatus());
        //审批模式
        crdReplyInfo.setApprMode("01");
        //本次用信金额

        // OPR_TYPE操作类型
        crdReplyInfo.setOprType("01");
        //批复状态
        crdReplyInfo.setReplyStatus("01");
        //批复条线
        crdReplyInfo.setBelgLine("01");
        // 币种 默认人民币
        crdReplyInfo.setCurType("CNY");
        //调外部接口同步第一次授信时间
        CusUpdateInitLoanDateDto cusUpdateInitLoanDateDto = new CusUpdateInitLoanDateDto();
        cusUpdateInitLoanDateDto.setCusId(mainInfo.getCusId());
        cusUpdateInitLoanDateDto.setInitLoanDate(LocalDate.now().toString());
        //同步额度
        try {
            ResultDto<Integer> integerResultDto = iCusClientService.updateInitLoanDate(cusUpdateInitLoanDateDto);
            logger.info("调用客户同步首次建立信贷关系时间结束，响应信息" + integerResultDto);
        } catch (Exception e) {
            logger.info("调用客户同步首次建立信贷关系时间失败", e.getMessage(), e);
        }
        //插入批复信息
        logger.info("惠享贷审批通过后续处理开始-流水号：【{}】，插入批复信息：【{}】", serno, JSON.toJSONString(crdReplyInfo));
        int insert = lmtCrdReplyInfoService.insert(crdReplyInfo);
        //TODO 俩接口都未调通
        if (insert == 1) {
            try {
                //占用额度
                this.handleBizEnd(crdReplyInfo);
            } catch (YuspException e) {
                logger.error(e.getMessage(), e);
            }

            //调用外部接口发送批复
            try {
                this.fbxw05(crdReplyInfo);
            } catch (YuspException e) {
                logger.error(e.getMessage(), e);
            }

            try {
                this.znsp04(crdReplyInfo);
            } catch (YuspException e) {
                logger.error(e.getMessage(), e);
            }

        }
        //修改为通过
        mainInfo.setApproveStatus(CmisBizConstants.APPLY_STATE_PASS);
        logger.info("惠享贷审批通过后续处理开始-流水号：【{}】，修改为通过：【{}】", serno, JSON.toJSONString(mainInfo));
        lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(mainInfo);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/15 10:47
     * @注释 优抵贷审批通过后的操作
     */
    public void yddEndPass(LmtSurveyReportMainInfo mainInfo) {

        //优抵贷通过后 先跑 fbyd02
        Fbyd02ReqDto fbyd02ReqDto = new Fbyd02ReqDto();
//        channel_type渠道来源否varchar2(2)y默认 11
        fbyd02ReqDto.setChannel_type("11");
//        co_platform合作平台否varchar2(4)y默认 1002
        fbyd02ReqDto.setCo_platform("1002");
//        prd_type产品类别否varchar2(6)y默认 000017
        fbyd02ReqDto.setPrd_type("000017");
//        prd_code产品代码（零售智能风控内部代码）否varchar2(12)y默认 1002000017
        fbyd02ReqDto.setPrd_code("1002000017");
//        cust_name客户姓名否varchar2(100)y
        fbyd02ReqDto.setCust_name(mainInfo.getCusName());
//        cert_code客户证件号码否varchar2(20)y申请人身份证号码
        fbyd02ReqDto.setCert_code(mainInfo.getCertCode());
//        spouse_name配偶姓名否varchar2(100)n
//        spouse_cert_code配偶证件号码否varchar2(20)n
//        qy_grant_no企业征信查询授权书编号否varchar2(100)n如果需要查询企业征信
//        report_type调查报告类型否varchar2(2)y"01：税务调查报告  02：尽职调查报告"
        fbyd02ReqDto.setReport_type("9".equals(mainInfo.getSurveyType()) ? "02" : "01");
//        survey_serno信贷调查表编号否varchar2(50)y信贷调查表编号
        fbyd02ReqDto.setSurvey_serno(mainInfo.getSurveySerno());
//        app_no业务流水号否varchar2(30)y分配客户经理时，风控送给信贷的授信申请流水号要回传回来，进行业务关联
        fbyd02ReqDto.setApp_no(mainInfo.getSurveySerno());
        try {
            logger.info("***********调用fbyd02开始,接口传入参数【{}】**********" , JSON.toJSONString(fbyd02ReqDto));
            ResultDto<Fbyd02RespDto> fbyd02RespDtoResultDto = dscms2RircpClientService.fbyd02(fbyd02ReqDto);
            logger.info("***********调用fbyd02开始,接口响应参数【{}】**********" , JSON.toJSONString(fbyd02RespDtoResultDto));
            //TODO 这里不改状态 由风控回推
//            mainInfo.setApproveStatus("997");
//            lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(mainInfo);
        } catch (YuspException e) {
            logger.info("调用fbyd02异常结束，流水号{}", mainInfo.getSurveySerno());
        }
    }

    /*
     * @param [mainInfo]
     * @return void
     * @author 王玉坤
     * @date 2021/8/25 0:15
     * @version 1.0.0
     * @desc 无还本续贷审批通过后处理逻辑
     * 普转
     * 1、前往风控进行二级准入fbxw01
     * 2、风控将结果推送至智能审贷系统
     * 3、若是成功则智能审批系统推送批复给信贷
     * 优转
     * 1、提交后直接将批复信息推送至智能审批系统znsp04
     * 2、生成批复信息
     * 3、同步额度系统
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public void whbEndPass(LmtSurveyReportMainInfo mainInfo) {
        // 智能风控二级准入请求对象
        Fbxw01ReqDto fbxw01ReqDto = null;
        // 智能风控二级准入返回对象
        Fbxw01RespDto fbxw01RespDto = null;
        // 智能审批请求对象
        Znsp04ReqDto znsp04ReqDto = null;
        // 智能审批返回对象
        Znsp04RespDto znsp04RespDto = null;
        // 调查报告基本信息
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = null;
        // 调查结论信息
        LmtSurveyConInfo lmtSurveyConInfo = null;
        // 批复信息
        LmtCrdReplyInfo lmtCrdReplyInfo = null;
        try {
            logger.info("调查流水号【{}】,查询调查基本信息开始！", mainInfo.getSurveySerno());
            lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(mainInfo.getSurveySerno());
            logger.info("调查流水号【{}】,查询调查基本信息结束！", mainInfo.getSurveySerno());

            logger.info("调查流水号【{}】,查询调查结论信息开始！", mainInfo.getSurveySerno());
            lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(mainInfo.getSurveySerno());
            logger.info("调查流水号【{}】,查询调查结论信息结束！", mainInfo.getSurveySerno());
            // 1、普转处理逻辑
            if (DscmsBizXwEnum.SURVEY_TYPE_7.key.equals(mainInfo.getSurveyType())) {
                logger.info("调查流水号【{}】,审批通过后，普转处理逻辑开始！", mainInfo.getSurveySerno());
                // 1.1 发送二级准入信息至智能风控
                fbxw01ReqDto = new Fbxw01ReqDto();
                // 渠道来源  11
                fbxw01ReqDto.setChannel_type("11");
                // 合作平台  1002
                fbxw01ReqDto.setCo_platform("1002");
                // 产品类别  000015
                fbxw01ReqDto.setPrd_type("000015");
                // 产品代码（零售智能风控内部代码）   1002000015
                fbxw01ReqDto.setPrd_code("1002000015");
                // 操作类型  01 一级准入  02 二级准入
                fbxw01ReqDto.setOp_flag("02");
                // 业务唯一编号
                fbxw01ReqDto.setApply_no(mainInfo.getListSerno());
                // 证件类型  10 身份证
                fbxw01ReqDto.setCert_type("10");
                // 证件号码
                fbxw01ReqDto.setCert_code(mainInfo.getCertCode());
                // 客户姓名
                fbxw01ReqDto.setCust_name(mainInfo.getCusName());
                // 移动电话
                fbxw01ReqDto.setPhone(lmtSurveyReportBasicInfo.getPhone());
                // 核心客户号
                fbxw01ReqDto.setCust_id_core(mainInfo.getCusId());
                // 原借据编号 一级准入必输
                fbxw01ReqDto.setOld_bill_no(lmtSurveyReportBasicInfo.getOldBillNo());
                //spouse_name配偶姓名
                fbxw01ReqDto.setSpouse_name(lmtSurveyReportBasicInfo.getSpouseName());
                //spouse_cert_type配偶证件类型
                fbxw01ReqDto.setSpouse_cert_type("10");
                //spouse_cert_code配偶证件号码
                fbxw01ReqDto.setSpouse_cert_code(lmtSurveyReportBasicInfo.getCertCode());
                // 客户经理号
                fbxw01ReqDto.setBiz_manager_id(mainInfo.getManagerId());
                // 归属机构
                fbxw01ReqDto.setBiz_org_id(mainInfo.getManagerBrId());

                // 调用接口
                fbxw01RespDto = fbxw01Service.fbxw01(fbxw01ReqDto);
                logger.info("调查流水号【{}】,审批通过后，普转处理逻辑结束！", mainInfo.getSurveySerno());
            } else { // 2.优转处理逻辑
                logger.info("调查流水号【{}】,审批通过后，优转处理逻辑开始！", mainInfo.getSurveySerno());
                logger.info("调查流水号【{}】,审批通过后，调用智能审批系统znsp04开始！", mainInfo.getSurveySerno());
                // 2.1 调用智能审批
                znsp04ReqDto = new Znsp04ReqDto();
                // 调查流水号
                znsp04ReqDto.setSurvey_serno(mainInfo.getSurveySerno());
                // 申请类型 默认01--存量周转
                znsp04ReqDto.setApply_type("01");
                // 产品代码
                znsp04ReqDto.setPrd_id("SC010010");
                // 产品名称
                znsp04ReqDto.setPrd_name("优转续贷");
                // 客户编号
                znsp04ReqDto.setCus_id(mainInfo.getCusId());
                // 客户名称
                znsp04ReqDto.setCus_name(mainInfo.getCusName());
                // 调查类型
                znsp04ReqDto.setLoan_type("7");
                // 是否随借随还
                znsp04ReqDto.setIs_sjsh("");
                // 证件号码
                znsp04ReqDto.setCert_no(mainInfo.getCertCode());
                // 客户经理
                znsp04ReqDto.setZb_manager_id(mainInfo.getManagerId());
                // 协办客户经理
                znsp04ReqDto.setXb_manager_id("");
                // 贷款金额
                znsp04ReqDto.setLoan_amt(lmtSurveyConInfo.getAdviceAmt());
                // 利率
                znsp04ReqDto.setReality_ir_y(lmtSurveyConInfo.getAdviceRate());
                // 期限
                znsp04ReqDto.setLoan_term(new BigDecimal(lmtSurveyConInfo.getAdviceTerm()));
                // 担保方式
                znsp04ReqDto.setGuara_way(lmtSurveyReportBasicInfo.getGuarMode());
                // 还款方式
                znsp04ReqDto.setRepay_way(lmtSurveyReportBasicInfo.getRepayMode());
                logger.info("调查流水号【{}】,审批通过后，调用智能审批系统znsp04开始！,送出参数：【{}】", mainInfo.getSurveySerno(),JSON.toJSONString(znsp04ReqDto));
                znsp04RespDto = znsp04Service.znsp04(znsp04ReqDto);
                logger.info("调查流水号【{}】,审批通过后，调用智能审批系统znsp04结束！,返回参数：【{}】", mainInfo.getSurveySerno(),JSON.toJSONString(znsp04RespDto));

                // 2.2 生成批复信息
                // 更新审批状态为通过
                mainInfo.setApproveStatus("997");
                this.update(mainInfo);

                logger.info("调查流水号【{}】,审批通过后，生成批复开始！", mainInfo.getSurveySerno());
                // 生成批复信息
                lmtCrdReplyInfo = new LmtCrdReplyInfo();
                String crdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
                // 批复流水号
                lmtCrdReplyInfo.setReplySerno(crdSerno);
                // 调查流水号
                lmtCrdReplyInfo.setSurveySerno(mainInfo.getSurveySerno());
                // 产品名称
                lmtCrdReplyInfo.setPrdName(mainInfo.getPrdName());
                // 产品编号
                lmtCrdReplyInfo.setPrdId(mainInfo.getPrdId());
                // 客户编号
                lmtCrdReplyInfo.setCusId(mainInfo.getCusId());
                // 客户名称
                lmtCrdReplyInfo.setCusName(mainInfo.getCusName());
                // 证件类型
                lmtCrdReplyInfo.setCertType("A");
                // 证件号码
                lmtCrdReplyInfo.setCertCode(mainInfo.getCertCode());
                // 担保方式
                lmtCrdReplyInfo.setGuarMode(lmtSurveyReportBasicInfo.getGuarMode());
                // 批复金额
                lmtCrdReplyInfo.setReplyAmt(lmtSurveyConInfo.getAdviceAmt());
                // 期限类型
                lmtCrdReplyInfo.setTermType("M");
                // 期限
                lmtCrdReplyInfo.setAppTerm(Integer.valueOf(lmtSurveyConInfo.getLoanTerm()));
                // 还款方式
                lmtCrdReplyInfo.setRepayMode(lmtSurveyReportBasicInfo.getRepayMode());
                // 币种
                lmtCrdReplyInfo.setCurType("CNY");
                // 批复利率
                lmtCrdReplyInfo.setExecRateYear(lmtSurveyConInfo.getAdviceRate());
                //2021年10月19日16:57:58 hubp
                lmtCrdReplyInfo.setLmtGraper(0);
                // 批复起始日
                String openDay = stringRedisTemplate.opsForValue().get("openDay");
                lmtCrdReplyInfo.setReplyStartDate(openDay);
                // 批复到期日
                lmtCrdReplyInfo.setReplyEndDate(DateUtils.addMonth(lmtCrdReplyInfo.getReplyStartDate(),
                        "yyyy-MM-dd", lmtCrdReplyInfo.getAppTerm()));
                // 批复状态 01--生效 02--冻结
                lmtCrdReplyInfo.setReplyStatus("01");
                // 额度类型 01--一次性额度 02--循环额度
                lmtCrdReplyInfo.setLimitType("01");
                // 是否无还本续贷 1 -- 是
                lmtCrdReplyInfo.setIsWxbxd("1");
                // 所属条线 01--小微
                lmtCrdReplyInfo.setBelgLine("01");
                // 操作类型
                lmtCrdReplyInfo.setOprType("01");
                // 客户经理
                lmtCrdReplyInfo.setManagerId(mainInfo.getManagerId());
                // 所属机构
                lmtCrdReplyInfo.setManagerBrId(mainInfo.getManagerBrId());
                // 录入人
                lmtCrdReplyInfo.setInputId(mainInfo.getInputId());
                // 录入机构
                lmtCrdReplyInfo.setInputBrId(mainInfo.getInputBrId());
                // 录入日期
                lmtCrdReplyInfo.setInputDate(openDay);
                // 最后修改人
                lmtCrdReplyInfo.setUpdId(mainInfo.getInputId());
                // 最后修改人机构
                lmtCrdReplyInfo.setUpdBrId(mainInfo.getInputBrId());
                // 最后修改日期
                lmtCrdReplyInfo.setUpdDate(openDay);

                lmtCrdReplyInfoService.insert(lmtCrdReplyInfo);
                logger.info("调查流水号【{}】,审批通过后，生成批复结束！", mainInfo.getSurveySerno());
                // 2021年10月15日13:56:45 hubp
                logger.info("调查流水号【{}】,审批通过后，开始更新无还本续贷优转名单【{}】状态！", mainInfo.getSurveySerno(),mainInfo.getListSerno());
                QueryModel model = new QueryModel();
                model.addCondition("serno",mainInfo.getListSerno());
                List<CusLstMclWhbxd> cusLstMclWhbxdList = cusLstMclWhbxdService.selectByModel(model);
                if(cusLstMclWhbxdList.size() > 0){
                    CusLstMclWhbxd cusLstMclWhbxd = cusLstMclWhbxdList.get(0);
                    cusLstMclWhbxd.setApplyStatus("3");
                    cusLstMclWhbxdService.updateSelective(cusLstMclWhbxd);
                    logger.info("调查流水号【{}】,审批通过后，更新无还本续贷优转名单【{}】状态结束！", mainInfo.getSurveySerno(),JSON.toJSONString(cusLstMclWhbxd));
                }
                // 2.3 同步额度系统开始 无还本续贷暂不同步额度系统
                cmisBizXwCommonService.sendCmisLmt0001(lmtCrdReplyInfo);
                logger.info("调查流水号【{}】,审批通过后，优转处理逻辑结束！", mainInfo.getSurveySerno());
            }
        } catch (Exception e) {
            logger.error("调查流水号【{}】,审批通过后，优转处理逻辑异常！:【{}】", JSON.toJSONString(e));
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/15 10:47
     * @注释 优企贷审批通过后的操作
     */
    public ResultDto yqdEndPass(LmtSurveyReportMainInfo mainInfo, String yOrN) {
        LmtSurveyConInfo conInfo = lmtSurveyConInfoService.selectByPrimaryKey(mainInfo.getSurveySerno());
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(mainInfo.getSurveySerno());
        Wxd003ReqDto wxd003ReqDto = new Wxd003ReqDto();
        wxd003ReqDto.setBusinessid(mainInfo.getBizSerno());// 业务申请编号 (银行业务单号)
        wxd003ReqDto.setGrantdate(mainInfo.getInputDate());// 授权日期
        wxd003ReqDto.setScenecheckresult(yOrN);// 现场核验结果是否通过
        String prcAdvice = conInfo.getPrcAdvice();
        if ("01".equals(prcAdvice)) {
            wxd003ReqDto.setScenechecksuggest("Y");// 客户经理勘验意见
        } else {
            wxd003ReqDto.setScenechecksuggest("N");// 客户经理勘验意见
        }
        wxd003ReqDto.setLatestamount(conInfo.getAdviceAmt().toString());// 调整后最新额度
        wxd003ReqDto.setLatestinterest(conInfo.getAdviceRate().toString());// 调整后最新利率
        wxd003ReqDto.setRepaytype(conInfo.getRepayMode());// 还款方式
        wxd003ReqDto.setApplyterm(conInfo.getAdviceTerm());// 申请期限
        wxd003ReqDto.setTurnover(lmtSurveyReportBasicInfo.getAppLoanWay());// 申贷类型
        logger.info("优企贷提交申请开始.. 调用wxd003开始..............." + wxd003ReqDto);
        ResultDto<Wxd003RespDto> wxd003RespDtoResultDto = dscms2XwywglptClientService.wxd003(wxd003ReqDto);
        logger.info("优企贷提交申请结束.................." + wxd003RespDtoResultDto);
        return new ResultDto<>(wxd003RespDtoResultDto);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/15 10:47
     * @注释 增享待审批通过后的操作
     */
    public void zxdEndPass(LmtSurveyReportMainInfo mainInfo) {
        logger.info("增享待提交申请开始...调用fbxw03开始..............." + mainInfo.getSurveySerno());
        this.zxdSubmit(mainInfo.getSurveySerno());
        logger.info("增享待提交申请开始...调用fbxw03结束......." + mainInfo.getSurveySerno());
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/15 10:47
     * @注释 优享贷审批通过后的操作
     */
    public void yxdEndPass(LmtSurveyReportMainInfo mainInfo) {

    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/20 11:24
     * @注释 授信批复同步
     */
    public Fbxw05RespDto fbxw05(LmtCrdReplyInfo crdReplyInfo) {
        Fbxw05ReqDto fbxw05ReqDto = new Fbxw05ReqDto();
        LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(crdReplyInfo.getSurveySerno());

        //拼装报文
        // channel_type渠道来源
        fbxw05ReqDto.setChannel_type(CmisBizConstants.XW_CHANNEL_TYPE_11);
        // co_platform合作平台1002
        fbxw05ReqDto.setCo_platform(CmisBizConstants.XW_PLATFORM_1002);
        // app_no申请流水号 非必传
        fbxw05ReqDto.setApp_no(mainInfo.getBizSerno());
        // prd_type产品类别000020
        fbxw05ReqDto.setPrd_type(CmisBizConstants.CODE_HXD);
        // prd_code产品代码（零售智能风控内部代码）1002000020
        fbxw05ReqDto.setPrd_code(CmisBizConstants.LSFK_PRD_CODE_HXD);
        // survey_serno调查表编号 TODO目前挡板 只能放这个流水号
//        fbxw05ReqDto.setSurvey_serno("XD00235801");
        fbxw05ReqDto.setSurvey_serno(mainInfo.getSurveySerno());
        //fbxw05ReqDto.setSurvey_serno(crdReplyInfo.getSurveySerno());
        // app_status批复状态00 批复通过  01 批复拒绝   03：合同已签订
        fbxw05ReqDto.setApp_status(CmisBizConstants.XW_APP_STATUS_00);
        // appr_amt终审额度批复通过时必输
        fbxw05ReqDto.setAppr_amt(crdReplyInfo.getReplyAmt().toString());
        // crd_start_date授信起始日期yyyy-MM-dd
        fbxw05ReqDto.setCrd_start_date(crdReplyInfo.getReplyStartDate());
        // crd_end_date授信结束日期yyyy-MM-dd
        fbxw05ReqDto.setCrd_end_date(crdReplyInfo.getReplyEndDate());
        // crd_term授信期限(月)
        fbxw05ReqDto.setCrd_term(crdReplyInfo.getAppTerm().toString());
        // appr_rate审批利率
        fbxw05ReqDto.setAppr_rate(crdReplyInfo.getExecRateYear().toString());
        // refusal_reason拒绝原因批复拒接时必输
        Fbxw05RespDto fbxw05RespDto = fbxw05Service.fbxw05(fbxw05ReqDto);
        return fbxw05RespDto;
    }

    public Fbxw05RespDto fbxw05test(Fbxw05ReqDto fbxw05ReqDto) {
        Fbxw05RespDto fbxw05RespDto = fbxw05Service.fbxw05(fbxw05ReqDto);
        return fbxw05RespDto;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/20 11:24
     * @注释 自动审批调查报告  惠享贷
     */
    public Znsp04RespDto znsp04(LmtCrdReplyInfo crdReplyInfo) {
        Znsp04ReqDto znsp04ReqDto = new Znsp04ReqDto();
        LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(crdReplyInfo.getSurveySerno());
        LmtSurveyReportBasicInfo basicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(crdReplyInfo.getSurveySerno());
        //拼装报文

        // survey_serno业务流水号否varchar(40)是调查报告流水号
        znsp04ReqDto.setSurvey_serno(crdReplyInfo.getSurveySerno());
        // apply_type申请类型否varchar(5)是"01 存量周转 02 新增 03 存量周转加新增   04 存量不周转新增 05：不良处置落实 06：不良处置周转 07：不良处置借新还旧"
        znsp04ReqDto.setApply_type(CmisBizConstants.XW_APPLY_TYPE_02);
        // prd_id产品编号否varchar(10)是  TODO 先默认惠享贷
        znsp04ReqDto.setPrd_id("SC060001");
        // prd_name产品名称否varchar(50)是
        znsp04ReqDto.setPrd_name(mainInfo.getPrdName());
        // cus_name客户名称否varchar(50)是
        znsp04ReqDto.setCus_name(mainInfo.getCusName());
        // loan_type贷款类型否varchar(5)是"SC 经营贷  PW 消费贷 CM 车易贷" TODO 先默认 SC经营贷
        znsp04ReqDto.setLoan_type("SC");
        // cus_id客户号否varchar(20)是
        znsp04ReqDto.setCus_id(mainInfo.getCusId());
        // cert_no身份证号码否varchar(20)是
        znsp04ReqDto.setCert_no(mainInfo.getCertCode());
        // zb_manager_id主办客户经理工号否varchar(20)否
        znsp04ReqDto.setZb_manager_id(mainInfo.getManagerId());
        // xb_manager_id协办客户经理否varchar(50)是
        znsp04ReqDto.setXb_manager_id(mainInfo.getManagerId());
        // loan_amt贷款金额否NUMBER(16,6)是
        znsp04ReqDto.setLoan_amt(crdReplyInfo.getReplyAmt());
        // reality_ir_y执行利率否NUMBER(16,6)是
        znsp04ReqDto.setReality_ir_y(crdReplyInfo.getExecRateYear());
        // loan_term贷款期限否NUMBER(16)是
        znsp04ReqDto.setLoan_term(new BigDecimal(12L));
        // guara_way担保方式否varchar(8)是"30 保证 10 抵押 20 质押 00 信用 21 低风险质押 40 全额保证金"
        znsp04ReqDto.setGuara_way(crdReplyInfo.getGuarMode());
        // is_sjsh是否随机随还否varchar(8)是"01 否 02 是"
        znsp04ReqDto.setIs_sjsh(CmisBizConstants.XW_IS_SJSH_01);
        // repay_way还款方式否varchar(8)是
        znsp04ReqDto.setRepay_way(crdReplyInfo.getRepayMode());
        // cont_type合同类型否varchar(2)否1：一般额合同，2：最高额合同 TODO:非必填
        //guara_way 担保方式
        znsp04ReqDto.setGuara_way(crdReplyInfo.getGuarMode());
        //还款方式
        znsp04ReqDto.setRepay_way(crdReplyInfo.getRepayMode());
        // term_loan_type期限类型否varchar(2)否"002 年 TODO:非必填
        znsp04ReqDto.setTerm_loan_type("003");
        // 001 月
        // 003 日"
        // is_trustee_pay是否受托支付否varchar(2)否"01是 TODO:非必填
        // 02否"
        // entrust_type受托类型否varchar(2)否"1 公司法人、股东 TODO:非必填
        // 2 个体工商户、合作社
        // 3 无营业执照客户
        // 4 消费性业务"
        // is_credit_condition是否有放款条件否varchar(2)否"01是 TODO:非必填
        // 02否"
        // credit_condition放款条件否varchar(256)否 TODO:非必填
        // is_whbxd是否无还本续贷否varchar(2)否"1 是 TODO:非必填

        // 2 否"
        // turnover_amt周转金额否NUMBER(16,6)否 TODO:非必填
        // add_amt新增金额否NUMBER(16,6)否 TODO:非必填
        // cus_phone客户手机号否varchar(11)是
        // znsp04ReqDto.setCus_phone(basicInfo.getPhone());
        if (null != basicInfo && !StringUtils.isBlank(basicInfo.getPhone())) {
            znsp04ReqDto.setCus_phone(basicInfo.getPhone());
        }
        // loan_use贷款用途否varchar(256)否 TODO:非必填

        Znsp04RespDto znsp04RespDto = znsp04Service.znsp04(znsp04ReqDto);
        return znsp04RespDto;
    }

    /**
     * @param crdReplyInfo
     * @return cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto
     * @author hubp
     * @date 2021/11/21 15:12
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Znsp04RespDto znsp04Yxd(LmtCrdReplyInfo crdReplyInfo) {
        Znsp04ReqDto znsp04ReqDto = new Znsp04ReqDto();
        LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(crdReplyInfo.getSurveySerno());
        LmtSurveyReportBasicInfo basicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(crdReplyInfo.getSurveySerno());
        //拼装报文

        // survey_serno业务流水号否varchar(40)是调查报告流水号
        znsp04ReqDto.setSurvey_serno(crdReplyInfo.getSurveySerno());
        // apply_type申请类型否varchar(5)是"01 存量周转 02 新增 03 存量周转加新增   04 存量不周转新增 05：不良处置落实 06：不良处置周转 07：不良处置借新还旧"
        znsp04ReqDto.setApply_type(CmisBizConstants.XW_APPLY_TYPE_01);
        // prd_id产品编号否varchar(10)是
        znsp04ReqDto.setPrd_id("PW010004");
        // prd_name产品名称否varchar(50)是
        znsp04ReqDto.setPrd_name(crdReplyInfo.getPrdName());
        // cus_name客户名称否varchar(50)是
        znsp04ReqDto.setCus_name(crdReplyInfo.getCusName());
        // loan_type贷款类型否varchar(5)是"SC 经营贷  PW 消费贷 CM 车易贷"
        znsp04ReqDto.setLoan_type("PW");
        // cus_id客户号否varchar(20)是
        znsp04ReqDto.setCus_id(mainInfo.getCusId());
        // cert_no身份证号码否varchar(20)是
        znsp04ReqDto.setCert_no(mainInfo.getCertCode());
        // zb_manager_id主办客户经理工号否varchar(20)否
        znsp04ReqDto.setZb_manager_id(mainInfo.getManagerId());
        // xb_manager_id协办客户经理否varchar(50)是
        znsp04ReqDto.setXb_manager_id(mainInfo.getManagerId());
        // loan_amt贷款金额否NUMBER(16,6)是
        znsp04ReqDto.setLoan_amt(crdReplyInfo.getReplyAmt());
        // reality_ir_y执行利率否NUMBER(16,6)是
        znsp04ReqDto.setReality_ir_y(crdReplyInfo.getExecRateYear());
        // loan_term贷款期限否NUMBER(16)是
        znsp04ReqDto.setLoan_term(BigDecimal.valueOf(crdReplyInfo.getAppTerm()));
        // guara_way担保方式否varchar(8)是"30 保证 10 抵押 20 质押 00 信用 21 低风险质押 40 全额保证金"
        znsp04ReqDto.setGuara_way(crdReplyInfo.getGuarMode());
        // is_sjsh是否随机随还否varchar(8)是"01 否 02 是"
        znsp04ReqDto.setIs_sjsh(CmisBizConstants.XW_IS_SJSH_01);
        // repay_way还款方式否varchar(8)是
        znsp04ReqDto.setRepay_way(crdReplyInfo.getRepayMode());
        // cont_type合同类型否varchar(2)否1：一般额合同，2：最高额合同
        znsp04ReqDto.setTerm_loan_type("001");
        if (null != basicInfo && !StringUtils.isBlank(basicInfo.getPhone())) {
            znsp04ReqDto.setCus_phone(basicInfo.getPhone());
        }
        Znsp04RespDto znsp04RespDto = znsp04Service.znsp04(znsp04ReqDto);
        return znsp04RespDto;
    }

    public Znsp04RespDto znsp04test(Znsp04ReqDto znsp04ReqDto) {
        Znsp04RespDto znsp04RespDto = znsp04Service.znsp04(znsp04ReqDto);
        return znsp04RespDto;
    }

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/5/20 9:58
     * @version 1.0.0
     * @desc 增享贷提交申请至风控
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto zxdSubmit(String  surveySerno) {
        logger.info("fbxw03增享贷推送风控开始..................");
        ResultDto result = new ResultDto();
        // 风控返回对象
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = null;
        Fbxw03RespDto fbxw03RespDto = null;
        // 调查表基本信息
        LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = null;
        // 调查模型审批信息
        LmtModelApprResultInfo lmtModelApprResultInfo = null;
        try {
            Fbxw03ReqDto fbxw03ReqDto = new Fbxw03ReqDto();
            lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(surveySerno);
            if (Objects.isNull(lmtSurveyReportMainInfo)) {
                throw BizException.error(null, "9999", "未查询到调查主表信息！");
            }
            logger.info("根据调查流水号【{}】查询调查表基本信息开始！", lmtSurveyReportMainInfo.getSurveySerno());
            lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
            if (Objects.isNull(lmtSurveyReportBasicInfo)) {
                throw BizException.error(null, "9999", "未查询到调查基本信息！");
            }
            logger.info("根据调查流水号【{}】查询调查表基本信息结束！", lmtSurveyReportMainInfo.getSurveySerno());

            logger.info("根据调查流水号【{}】查询调查表模型信息开始！", lmtSurveyReportMainInfo.getSurveySerno());
            lmtModelApprResultInfo = lmtModelApprResultInfoService.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
            if (Objects.isNull(lmtModelApprResultInfo)) {
                throw BizException.error(null, "9999", "未查询到调查模型信息！");
            }
            logger.info("根据调查流水号【{}】查询调查表模型信息结束！", lmtSurveyReportMainInfo.getSurveySerno());

            //channel_type渠道来源
            fbxw03ReqDto.setChannel_type("11");
            //co_platform合作平台
            fbxw03ReqDto.setCo_platform("1002");
            //prd_type产品类别
            fbxw03ReqDto.setPrd_type("000016");
            //prd_code产品代码（零售智能风控内部代码）
            fbxw03ReqDto.setPrd_code("1002000016");
            // apply_no业务唯一编号  白名单bizNo
            fbxw03ReqDto.setApply_no(lmtSurveyReportMainInfo.getBizSerno());
            //cust_id客户号
            fbxw03ReqDto.setCust_id(lmtSurveyReportBasicInfo.getCusId());
            //telphone手机号码
            fbxw03ReqDto.setTelphone(lmtSurveyReportBasicInfo.getPhone());
            //manager_id客户经理号
            fbxw03ReqDto.setManager_id(lmtSurveyReportBasicInfo.getManagerId());
            //manager_br_id管户机构
            fbxw03ReqDto.setManager_br_id(lmtSurveyReportBasicInfo.getManagerBrId());
            //cust_name借款人姓名
            fbxw03ReqDto.setCust_name(lmtSurveyReportBasicInfo.getCusName());
            //cert_code借款人身份证号码
            fbxw03ReqDto.setCert_code(lmtSurveyReportBasicInfo.getCertCode());
            //根据编号去查询客户经理信息
            String userName = OcaTranslatorUtils.getUserName(lmtSurveyReportBasicInfo.getManagerId());
            // manager_name客户经理姓名
            fbxw03ReqDto.setManager_name(userName);
            //amt客户经理建议金额  引用页面 建议金额
            fbxw03ReqDto.setAmt(lmtSurveyReportBasicInfo.getAdviceAmt().toString());
            //rate客户经理建议利率 引用页面 建议利率
            fbxw03ReqDto.setRate(lmtSurveyReportBasicInfo.getAdviceRate().toString());
            // repay_type还款方式  引用页面 还款方式  lmtSurveyReportBasicInfo.getRepayMode()
//            fbxw03ReqDto.setRepay_type(DicTranEnum.lookup("REPAY_MODE_XDTOHX_"+lmtSurveyReportBasicInfo.getRepayMode()));
            fbxw03ReqDto.setRepay_type("A001".equals(lmtSurveyReportBasicInfo.getRepayMode()) ? "05" : "01");
            //crd_term 贷款期限
            fbxw03ReqDto.setCrd_term(lmtSurveyReportBasicInfo.getAdviceTerm());
            //spouse_name配偶姓名  送地址信息 老信贷替代配偶姓名  basic取地址信息  LIVING_ADDR
            fbxw03ReqDto.setSpouse_name(lmtSurveyReportBasicInfo.getLivingAddr());

            //查询客户信息
            ResultDto<CusForZxdDto> cusForZxdDto = iCusClientService.selectbycusidforzxd(lmtSurveyReportBasicInfo.getCusId());
            CusForZxdDto cusData = cusForZxdDto.getData();
            if (cusData == null) {
                throw BizException.error(null, "9999", "根据客户编号未查询到可用的客户白名单基本信息");
            }

            //agri_flg 是否农户
            fbxw03ReqDto.setAgri_flg("1".equals(cusData.getIsAgri()) ? "Y" : "N");

            //loan_direction贷款投向  取客户行业信息的前三位 1
            String indivComTrade = cusData.getIndivComTrade();
            if (StringUtils.isEmpty(indivComTrade)) {
                throw BizException.error(null, "9999", "客户基本信息中行业信息为空");
            }
            if (indivComTrade.length() > 4) {//123
                //切割 获取前三位
                fbxw03ReqDto.setLoan_direction(indivComTrade.substring(1, 3));
            } else {//0123
                fbxw03ReqDto.setLoan_direction(indivComTrade);

            }
            if (StringUtils.isBlank(cusData.getQq()) && StringUtils.isBlank(cusData.getEmail()) && StringUtils.isBlank(cusData.getWechatNo())) {
                throw BizException.error(null, "9999", "根据客户编号未查询到客户基本信息中QQ、电子邮箱和微信其中之一");
            }

            // qq
            fbxw03ReqDto.setQq(cusData.getQq());
            // email电子邮箱
            fbxw03ReqDto.setEmail(cusData.getEmail());
            // wechat微信
            fbxw03ReqDto.setWechat(cusData.getWechatNo());
            //cert_type 证件类型  可不传
            fbxw03ReqDto.setCert_type("10");
            //loan_purpose贷款用途
            fbxw03ReqDto.setLoan_purpose("10");
            //spouse_cert_code配偶身份证号码
            fbxw03ReqDto.setSpouse_cert_code("");
            //op_flag操作类型
            fbxw03ReqDto.setOp_flag("");
            // bill_no借据号
            fbxw03ReqDto.setBill_no("");
            //amount申请金额
            fbxw03ReqDto.setAmount("");
            fbxw03RespDto = fbxw03Service.fbxw03(fbxw03ReqDto);
            result.setData(fbxw03RespDto);
            // 更新模型状态信息为 20 -- 征信规则审批中
            lmtModelApprResultInfo.setModelRstStatus(DscmsBizXwEnum.XW_MODEL_RST_STATUS_20.key);
            lmtModelApprResultInfoService.update(lmtModelApprResultInfo);
            logger.info("fbxw03增享贷推送风控结束..................");
        } catch (BizException e) {
            logger.info("fbxw03增享贷推送风控失败..................");
            logger.error(e.getMessage(), e);
            throw e;
        } catch (YuspException e) {
            logger.info("fbxw03增享贷推送风控失败..................");
            logger.error(e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            logger.info("fbxw03增享贷推送风控失败..................");
            throw e;
        }
        return result;
    }

    /**
     * @param surveySerno
     * @return cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto
     * @author hubp
     * @date 2021/5/20 14:46
     * @version 1.0.0
     * @desc 无还本续贷普转，审批通过后，提交至风控二级准入
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public Fbxw01RespDto noRepayInfoSubmit(String surveySerno) {
        logger.info("无还本续贷普转提交至风控二级准入开始..................");
        Fbxw01RespDto fbxw01RespDto = null;
        try {
            Fbxw01ReqDto fbxw01ReqDto = new Fbxw01ReqDto();
            LmtSurveyReportBasicInfo lmtSurveyReportBasicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(surveySerno);
            //channel_type渠道来源
            fbxw01ReqDto.setChannel_type("11");
            //co_platform合作平台
            fbxw01ReqDto.setCo_platform("1002");
            //prd_type产品类别
            fbxw01ReqDto.setPrd_type("'000015");
            //prd_code产品代码（零售智能风控内部代码）
            fbxw01ReqDto.setPrd_code("1002000015");
            //op_flag操作类型 TODO 不清楚填02是否正确
            fbxw01ReqDto.setOp_flag("02");
            //apply_no业务唯一编号
            fbxw01ReqDto.setApply_no(surveySerno);
            //cert_type证件类型
            fbxw01ReqDto.setCert_type("10");
            //cert_code证件号码
            fbxw01ReqDto.setCert_code(lmtSurveyReportBasicInfo.getCertCode());
            //cust_name客户姓名
            fbxw01ReqDto.setCust_name(lmtSurveyReportBasicInfo.getCusName());
            //phone移动电话
            fbxw01ReqDto.setPhone(lmtSurveyReportBasicInfo.getPhone());
            //cust_id_core核心客户号
            fbxw01ReqDto.setCust_id_core(lmtSurveyReportBasicInfo.getCusId());
            //spouse_name配偶姓名
            fbxw01ReqDto.setSpouse_name(lmtSurveyReportBasicInfo.getSpouseName());
            //spouse_cert_type配偶证件类型
            fbxw01ReqDto.setSpouse_cert_type("10");
            //spouse_cert_code配偶证件号码
            fbxw01ReqDto.setSpouse_cert_code(lmtSurveyReportBasicInfo.getCertCode());
            //old_bill_no原借据编号
            fbxw01ReqDto.setOld_bill_no(lmtSurveyReportBasicInfo.getOldBillNo());
            //biz_manager_id客户经理号
            fbxw01ReqDto.setBiz_manager_id(lmtSurveyReportBasicInfo.getManagerId());
            //biz_org_id归属机构
            fbxw01ReqDto.setBiz_org_id(lmtSurveyReportBasicInfo.getManagerBrId());
            fbxw01RespDto = fbxw01Service.fbxw01(fbxw01ReqDto);
        } catch (YuspException e) {
            logger.info("无还本续贷普转提交至风控二级准入失败..................");
            throw BizException.error(null, e.getCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("无还本续贷普转提交至风控二级准入结束..................");
        }
        return fbxw01RespDto;
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/25 11:16
     * @注释 删除单条数据
     */
    public ResultDto delectdata(LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        /*选择一条满足条件的数据，点击按钮，系统弹出删除成功提示框。
            删除“审批状态=待发起且数据来源=人工新增”的调查任务：物理删除，删除成功，数据消失；
            删除“审批状态=退回”或“数据来源不等于人工新增”的调查任务：逻辑删除，删除成功，
            更新“审批状态=自行退出”，数据进入1.2.4授信调查历史菜单表。*/
        int i = 0;
        LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
        String loginCode = SessionUtils.getLoginCode();
        if (!mainInfo.getManagerId().equals(loginCode)) {
            return new ResultDto(i).message("只能删除自己的调查任务");
        }

        if (mainInfo.getApproveStatus().equals(CmisBizConstants.STD_ZB_APP_ST_000) || mainInfo.getApproveStatus().equals(CmisBizConstants.STD_ZB_APP_ST_992)) {
            //可以删除 现在根据逻辑是进行物理删除还是逻辑删除 TODO 数据来源为人工 物理删除  否则逻辑删除  只有 待发起+人工新增可以无理删除
            if (mainInfo.getApproveStatus().equals(CmisBizConstants.STD_ZB_APP_ST_000) && mainInfo.getDataSource().equals(CmisBizConstants.STD_DATA_SOUR_00)) {
                i = lmtSurveyReportMainInfoMapper.deleteByPrimaryKey(mainInfo.getSurveySerno());
            } else {
                logger.info("删除业务" + lmtSurveyReportMainInfo.getSurveySerno() + "信息-业务主表");
                if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(lmtSurveyReportMainInfo.getApproveStatus())) {
                    //流程删除 修改为自行退出
                    logger.info("流程删除==》bizId：", lmtSurveyReportMainInfo.getSurveySerno());
                    // 删除流程实例
                    workflowCoreClient.deleteByBizId(lmtSurveyReportMainInfo.getSurveySerno());
                }
                //设置为自行退出  TODO 自行退出 996
                mainInfo.setApproveStatus(CmisBizConstants.STD_ZB_APP_ST_996);
                mainInfo.setOprType("02");
                i = lmtSurveyReportMainInfoMapper.updateByPrimaryKeySelective(mainInfo);
            }
        } else {
            return new ResultDto(i).message("只能删除状态为待发起或打回/退回的调查任务");
        }
        return new ResultDto(i).message("删除成功");
    }


    /**
     * @创建人 WH
     * @创建时间 2021/5/21 14:30
     * @注释 公共的方法 补全固定参数
     */
    public LmtSurveyReportMainInfo setData(LmtSurveyReportMainInfo mainInfo) {
        User user = SessionUtils.getUserInformation();
        if (user != null) {
            //登记人
            mainInfo.setUpdId(user.getLoginCode());
            //主管客户经理
            mainInfo.setManagerId(user.getLoginCode());
            //最后修改人
            mainInfo.setInputId(user.getLoginCode());
            //获取登记机构
            UserIdentity org = user.getOrg();
            //登记机构
            if (null != org) {
                mainInfo.setInputBrId(org.getCode());
                //主管机构
                mainInfo.setManagerBrId(org.getCode());
                //最后修改机构
                mainInfo.setUpdBrId(org.getCode());
            }
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String s = LocalDate.now().toString();
            //进件时间 字段长为10
            mainInfo.setIntoTime(s);
            //登记日期
//            mainInfo.setInputDate(simpleDateFormat.format(date));
            mainInfo.setInputDate(s);
            //最后修改日期
//            mainInfo.setUpdDate(simpleDateFormat.format(date));
            mainInfo.setUpdDate(s);
            //创建时间
            mainInfo.setCreateTime(date);
            //修改时间
            mainInfo.setUpdateTime(date);
        }
        return mainInfo;

    }

    /**
     * @方法名称: selectByCertCode
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSurveyReportMainInfo> selectByCertCode(QueryModel model) {
        List<LmtSurveyReportMainInfo> list = lmtSurveyReportMainInfoMapper.selectByModel(model);
        return list;
    }

    /**
     * @方法名称: riskItem0019
     * @方法描述: 小微贷款产品互斥校验
     * @参数与返回说明:
     * @算法描述: 1）若当前授信产品为惠享贷，且该客户下存在在途或者生效的优企贷、优农贷授信，则拦截
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0019(String serNo) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (com.mysql.cj.util.StringUtils.isNullOrEmpty(serNo)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 查询客户授信调查表
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(serNo);
        if (lmtSurveyReportMainInfo == null) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0103);
            return riskResultDto;
        }
        // 判断当前授信产品是否为惠享贷
        if (Objects.nonNull(lmtSurveyReportMainInfo) && "SC060001".equals(lmtSurveyReportMainInfo.getPrdId())) {
            // 根据证件号去查询
            QueryModel model = new QueryModel();
            model.addCondition("certCode", lmtSurveyReportMainInfo.getCertCode());
            List<LmtSurveyReportMainInfo> list = lmtSurveyReportMainInfoMapper.selectByModel(model);
            if (list != null && list.size() > 0) {
                for (LmtSurveyReportMainInfo mainInfo : list) {
                    // SC010008：即是优企贷产品，SC020010：即是优农贷产品
                    if ("SC010008".equals(mainInfo.getPrdId()) || "SC020010".equals(mainInfo.getPrdId())) {
                        // 审批通过，即是：生效
                        if ("997".equals(mainInfo.getApproveStatus())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1901);
                            return riskResultDto;
                        }
                        // 在途，即是：审批中
                        if ("111".equals(mainInfo.getApproveStatus())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_1902);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }


    /**
     * @方法名称: riskItem0020
     * @方法描述: 小微企业主关联征信校验
     * @参数与返回说明:
     * @算法描述: 1）若当前授信为房抵循环贷(无还本续贷优转)，且客户类型为小微企业主，则征信查询列表必须存在借款人经营企业的征信数据
     * @创建人: mashun
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0020(String serNo) {
        RiskResultDto riskResultDto = new RiskResultDto();
        if (com.mysql.cj.util.StringUtils.isNullOrEmpty(serNo)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        // 查询调查主表信息
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(serNo);
        if (lmtSurveyReportMainInfo == null) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0103);
            return riskResultDto;
        }

        // 判断当前授信产品是否为房抵循环贷(无还本续贷优转)
        if (Objects.nonNull(lmtSurveyReportMainInfo) && "SC010010".equals(lmtSurveyReportMainInfo.getPrdId())) {
            // 根据客户id获取客户基本信息
            Xdkh0001DataRespDto xdkh0001DataRespDto = queryXdkh0001DataRespDto(lmtSurveyReportMainInfo.getCusId());
            if (xdkh0001DataRespDto == null) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
                return riskResultDto;
            }

            // 查询企业信息
            LmtSurveyReportComInfo lmtSurveyReportComInfo = lmtSurveyReportComInfoService.selectByPrimaryKey(serNo);
            if (lmtSurveyReportComInfo == null) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_010301);
                return riskResultDto;
            }

            // 判定该客户是否是小微企业主，150：小微企业主
            if (Objects.nonNull(xdkh0001DataRespDto) && Objects.equals("150", xdkh0001DataRespDto.getCusType())) {
                // 根据证件号去查询
                QueryModel model = new QueryModel();
                model.addCondition("certCode", lmtSurveyReportComInfo.getCorpCertCode());
                // 1，代表企业查询
                model.addCondition("qryCls", "1");
                // 取得所有的企业征信查询
                List<CreditReportQryLst> list = creditReportQryLstService.selectCreditByCertCode(model);
                if (list != null && list.size() > 0) {
                    for (CreditReportQryLst creditReportQryLst : list) {
                        if (!com.mysql.cj.util.StringUtils.isNullOrEmpty(creditReportQryLst.getReportNo())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                            return riskResultDto;
                        }
                    }
                }
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0005);
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 客户基本信息
     *
     * @param cusId
     * @return
     */
    public Xdkh0001DataRespDto queryXdkh0001DataRespDto(String cusId) {
        Xdkh0001DataReqDto xdkh0001DataReqDto = new Xdkh0001DataReqDto();//请求Dto：查询个人客户基本信息
        Xdkh0001DataRespDto xdkh0001DataRespDto = null;//响应Dto：查询个人客户基本信息
        xdkh0001DataReqDto.setCusId(cusId);
        xdkh0001DataReqDto.setQueryType("01");

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
        ResultDto<Xdkh0001DataRespDto> xdkh0001DataRespDtoResultDto = dscmsCusClientService.xdkh0001(xdkh0001DataReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataRespDtoResultDto));

        String xdkh0001Code = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String xdkh0001Meesage = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0001DataRespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            xdkh0001DataRespDto = xdkh0001DataRespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, xdkh0001Code, xdkh0001Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value);
        return xdkh0001DataRespDto;
    }

    public ResultDto<Map<String, Integer>> imageSum(ImageDataReqDto imageDataReqDto) {
        return dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
    }

    /**
     * @param lmtSurveyReportMainInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Boolean>
     * @author hubp
     * @date 2021/7/15 10:15
     * @version 1.0.0
     * @desc 优抵贷调查报告提交审批校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto submitYdd(LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        logger.info("****************优抵贷调查报告提交审批校验开始*************");
        try {
            if (StringUtils.isBlank(lmtSurveyReportMainInfo.getSurveySerno())) {
                throw BizException.error("9999", "9999", "流水号获取异常！");
            }
            LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
            if (Objects.isNull(lmtSurveyConInfo)) {
                throw BizException.error("9999", "9999", "调查结论未完善！");
            }
            LmtSurveyReportBasicInfo basicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
            if (Objects.isNull(basicInfo)) {
                throw BizException.error("9999", "9999", "未查询到基本信息！");
            }
            LmtPerferRateApplyInfo rateApplyInfo = lmtPerferRateApplyInfoService.selectBySurveySerno(lmtSurveyReportMainInfo.getSurveySerno());
            if (Objects.nonNull(rateApplyInfo) && StringUtils.nonBlank(rateApplyInfo.getApproveStatus()) && "111".equals(rateApplyInfo.getApproveStatus())) {
                throw BizException.error("9999", "9999", "优惠利率审批尚在审批中！");
            }
            String isOnlinePld = basicInfo.getIsOnlinePld();
            if (StringUtils.isEmpty(isOnlinePld)) {
                logger.error("根据调查流水号{}基本信息未补全！", lmtSurveyReportMainInfo.getSurveySerno());
                return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("基本信息'是否线上抵押'为空");
            }
            if (basicInfo.getIsOnlinePld().equals("1")) {
                //是否线上抵押  选择为 是
                //查询影像信息
                //校验是否完成影像
                ImageDataReqDto imageDataReqDto = new ImageDataReqDto();
                imageDataReqDto.setDocId(lmtSurveyReportMainInfo.getSurveySerno());
                imageDataReqDto.setOutSystemCode("JYD1");
                ResultDto<Map<String, Integer>> map = dscms2ImageClientService.imageImageDataSize(imageDataReqDto);
                if (map.getData() == null || map.getData().size() == 0 || map.getData().get("JYD1XSBY01") == null || map.getData().get("JYD1XSBY03") == null) {
                    if (map.getData() == null || map.getData().size() == 0) {
                        logger.error("根据调查流水号{}未获取到影像信息，不允许提交！", lmtSurveyReportMainInfo.getSurveySerno());
                        return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("请上传影像资料");
                    }
                    if (map.getData().get("JYD1XSBY01") == null) {
                        logger.error("根据调查流水号{}未获取到影像信息借款人及共有人证件，不允许提交！", lmtSurveyReportMainInfo.getSurveySerno());
                        return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message("请上传影像资料中借款人及共有人证件");
                    }
                    if (map.getData().get("JYD1XSBY03") == null) {
                        logger.error("根据调查流水号{}未获取到影像信息房产证明，不允许提交！", lmtSurveyReportMainInfo.getSurveySerno());
                        return new ResultDto<Boolean>(Boolean.FALSE).code(-1).message(",请上传影像资料中房产证明");
                    }
                }
            } else {
                //是否线上抵押 为空 或否
            }
        } catch (Exception e) {
            logger.error("****************优抵贷调查报告提交审批校验异常*************", e);
            return new ResultDto(null).code("9999").message(e.getMessage());
        } finally {
            logger.error("****************优抵贷调查报告提交审批校验结束*************");
        }
        return new ResultDto(0);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/23 10:12
     * @注释
     */
    public ResultDto submitYqd(LmtSurveyReportMainInfo mainInfo) {
        logger.error("****************优企贷调查报告提交审批校验开始  *************");
        // 查询紧急联系人
        List<LmtEgcInfo> lmtEgcInfoList = lmtEgcInfoService.selectBySerno(mainInfo.getSurveySerno());
        if (lmtEgcInfoList.size() == 0) {
            return new ResultDto(false).message("请添加紧急联系人！");
        }
        //如果调查结论的意见是否的话 直接调用接口 不走审批流
        LmtSurveyConInfo conInfo = lmtSurveyConInfoService.selectByPrimaryKey(mainInfo.getSurveySerno());

        LmtSurveyReportBasicInfo basicInfo = lmtSurveyReportBasicInfoService.selectByPrimaryKey(mainInfo.getSurveySerno());

        if (Objects.isNull(basicInfo)) {
            return new ResultDto(false).message("调查基本信息不能为空！");
        }
        // 2021年9月8日16:49:43 hubp 加个判断，
        if (Objects.isNull(conInfo) || StringUtils.isBlank(conInfo.getPrcAdvice())) {
            return new ResultDto(false).message("请完善调查结论");
        }
        // 2021年9月9日21:31 尚智勇 调查结论内建议授信金额＞模型审批，提交审批失败
        // 2021年11月18日18:06:08 hubp 修改判断
        if (Objects.nonNull(conInfo.getModelAmt())) {
            // 如果为无还本续贷，则建议金额不能大于无还本模型金额，如果不是，则建议金额不能大于模型金额
            if (CmisCommonConstants.YES_NO_1.equals(basicInfo.getIsTqsd())) {
                logger.info("*****优企贷为提前续贷 , IsTqsd:【{}】*************" ,basicInfo.getIsTqsd());
                if ("W".equals(basicInfo.getAppLoanWay())) {
                    logger.error("*****优企贷为无还本续贷 , AppLoanWay:【{}】*************" ,basicInfo.getAppLoanWay());
                    if (Objects.isNull(conInfo.getAdviceAmt()) || Objects.isNull(basicInfo.getWhbModelAmt())) {
                        return new ResultDto(false).message("无还本续贷建议金额或无还本模型金额不能为空!");
                    }
                    if (conInfo.getAdviceAmt().compareTo(basicInfo.getWhbModelAmt()) > 0) {
                        return new ResultDto(false).message("建议金额不能超过无还本模型金额!");
                    }
                } else {
                    logger.info("*****优企贷为还本续贷 , AppLoanWay:【{}】*************" ,basicInfo.getAppLoanWay());
                    if (Objects.isNull(conInfo.getAdviceAmt()) || Objects.isNull(conInfo.getModelAmt())) {
                        return new ResultDto(false).message("建议金额或模型金额不能为空!");
                    }
                    if (conInfo.getAdviceAmt().compareTo(conInfo.getModelAmt()) > 0) {
                        return new ResultDto(false).message("建议金额不能超过模型金额!");
                    }
                }
            } else {
                logger.info("*****优企贷不为提前续贷 , IsTqsd:【{}】*************" ,basicInfo.getIsTqsd());
                if (Objects.isNull(conInfo.getAdviceAmt()) || Objects.isNull(conInfo.getModelAmt())) {
                    return new ResultDto(false).message("建议金额或模型金额不能为空!");
                }
                if (conInfo.getAdviceAmt().compareTo(conInfo.getModelAmt()) > 0) {
                    return new ResultDto(false).message("建议金额不能超过模型金额!");
                }
            }
        } else {
            return new ResultDto(false).message("请完善调查结论！建议金额或建议周转方式不能为空！");
        }
        String prcAdvice = conInfo.getPrcAdvice();
        if ("01".equals(prcAdvice)) {
            //调查结论为 通过  直接过
        } else {
            //客户经理调查结论为不通过  否决然后
            Wxd003ReqDto wxd003ReqDto = new Wxd003ReqDto();
            wxd003ReqDto.setBusinessid(mainInfo.getSurveySerno());// 业务申请编号 (银行业务单号)
            wxd003ReqDto.setGrantdate(LocalDate.now().toString());// 授权日期
            wxd003ReqDto.setScenecheckresult("N");// 现场核验结果是否通过  填 审批意见
            wxd003ReqDto.setScenechecksuggest("N");// 客户经理勘验意见
            wxd003ReqDto.setLatestamount(conInfo.getAdviceAmt().toString());// 调整后最新额度
            wxd003ReqDto.setLatestinterest(conInfo.getAdviceRate().toString());// 调整后最新利率
            wxd003ReqDto.setRepaytype(conInfo.getRepayMode());// 还款方式
            wxd003ReqDto.setApplyterm(conInfo.getAdviceTerm());// 申请期限
            logger.error("优企贷提交申请开始.. 调用wxd003开始..............." + wxd003ReqDto);
            ResultDto<Wxd003RespDto> wxd003RespDtoResultDto = dscms2XwywglptClientService.wxd003(wxd003ReqDto);
            logger.error("优企贷提交申请结束.................." + wxd003RespDtoResultDto);
            logger.error("*****优企贷调查报告提交审批校验结束 开始修改主表状态为否决  998*************");
            //
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(mainInfo.getSurveySerno());
            lmtSurveyReportMainInfo.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
            int i = lmtSurveyReportMainInfoMapper.updateByPrimaryKey(lmtSurveyReportMainInfo);
            logger.error("*****优企贷调查报告修改主表状态为否决_998 修改条数*************" + i);
            return new ResultDto(false).message("调查结论办理建议为拒绝 无需审批流程 提交成功");
        }

        return new ResultDto(true);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/7/23 10:12
     * @注释
     */
    public ResultDto submitwhb(LmtSurveyReportMainInfo lmtSurveyReportMainInfo) {
        logger.info("****************无还本调查报告提交审批校验开始*************");
        LmtSurveyConInfo lmtSurveyConInfo = lmtSurveyConInfoService.selectByPrimaryKey(lmtSurveyReportMainInfo.getSurveySerno());
        if (null == lmtSurveyConInfo) {
            throw BizException.error("9999", "9999", "调查结论未完善！");
        }
        LmtPerferRateApplyInfo rateApplyInfo = lmtPerferRateApplyInfoService.selectBySurveySerno(lmtSurveyReportMainInfo.getSurveySerno());
        if (Objects.nonNull(rateApplyInfo) && StringUtils.nonBlank(rateApplyInfo.getApproveStatus()) && "111".equals(rateApplyInfo.getApproveStatus())) {
            throw BizException.error("9999", "9999", "优惠利率审批尚在审批中！");
        }
        logger.info("****************无还本调查报告提交审批校验结束*************");
        return new ResultDto(true);
    }

    /**
     * @param lmtSurveyReportMainInfo
     * @return ResultDto
     * @author 尚智勇
     * @date 2021/9/08 16:49
     * @version 1.0.0
     * @desc 增享贷提交审批之前查询是否有三十天征信报告
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto submitzxd(LmtSurveyReportMainInfo lmtSurveyReportMainInfoTemp) {

        // 获取系统时间
        logger.info("调用征信查询接口开始，业务流水号【{}】", lmtSurveyReportMainInfoTemp.getSurveySerno());
        logger.info("根据调查流水号【】查询调查信息开始！", lmtSurveyReportMainInfoTemp.getSurveySerno());
        LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(lmtSurveyReportMainInfoTemp.getSurveySerno());
        if (Objects.isNull(lmtSurveyReportMainInfo)) {
            throw BizException.error("","9999","请完善调查信息！");
        }
        logger.info("根据调查流水号【】查询调查信息结束！", lmtSurveyReportMainInfoTemp.getSurveySerno());
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        Date nowDay = DateUtils.parseDate(openDay, "yyyy-MM-dd");
        // 30天前
        Date oldDay = DateUtils.addDay(nowDay, -30);
        // 调用通用接口
        CredzbReqDto credzbReqDto = new CredzbReqDto();
        credzbReqDto.setRuleCode("R003");
        credzbReqDto.setReqId(lmtSurveyReportMainInfo.getSurveySerno());
        // 2021年11月17日15:35:20 hubp 问题编号：202111117-00082 修复取值或为null的情况
        if(StringUtils.nonBlank(lmtSurveyReportMainInfo.getManagerBrId())){
            credzbReqDto.setBrchno(lmtSurveyReportMainInfo.getManagerBrId());
        } else {
            credzbReqDto.setBrchno(lmtSurveyReportMainInfo.getInputBrId());
        }
        credzbReqDto.setCertificateNum(lmtSurveyReportMainInfo.getCertCode());
        credzbReqDto.setCustomName(lmtSurveyReportMainInfo.getCusName());
        credzbReqDto.setStartDate(DateUtils.formatDate(oldDay, "yyyy-MM-dd"));
        credzbReqDto.setEndDate(openDay);
        // 查询征信的报文
        logger.info("调查征信接口报文【{}】", credzbReqDto);
        // 调查征信报告
        ResultDto<CredzbRespDto> credzbRespDto = dscms2Ciis2ndClientService.credzb(credzbReqDto);
        if (credzbRespDto != null && SuccessEnum.CMIS_SUCCSESS.key.equals(credzbRespDto.getCode())) {
            logger.info("调用征信查询接口成功，业务流水号【{}】", lmtSurveyReportMainInfo.getSurveySerno());
            if (CommonUtils.nonNull(credzbRespDto.getData()) && CommonUtils.nonNull(credzbRespDto.getData().getR003()) && StringUtils.nonBlank(credzbRespDto.getData().getR003().getREPORTID())) {
                logger.info("30天内征信报告编号【{}】", credzbRespDto.getData().getR003().getREPORTID());
            } else {
                logger.info("不存在30天内有效的征信报告");
                return new ResultDto(-1).code(-1).message("未查询到用户三十天内征信报告");
            }
        } else {
            String errorMsg = "";
            if (credzbRespDto != null) {
                errorMsg = credzbRespDto.getMessage();
            }
            logger.info("征信查询接口调用失败，失败原因【{}】", errorMsg);
            return new ResultDto(-1).code(-1).message("未查询到用户三十天内征信报告");
        }
        logger.info("调用征信查询接口结束，业务流水号【{}】", lmtSurveyReportMainInfo.getSurveySerno());

//        logger.info("调查流水号{}，前往征信系统查询客户近30天征信信息开始..............", lmtSurveyReportMainInfo.getSurveySerno());
//        Boolean b = creditReportQryLstService.selectbyxw(lmtSurveyReportMainInfo.getCertCode());
//        if (!b) {
//            // 未查询到近30天征信报告
//            logger.info("调查流水号{}，未查询到客户近30天征信报告..............", lmtSurveyReportMainInfo.getSurveySerno());
//            return new ResultDto(-1).code(-1).message("未查询到用户三十天内征信报告");
//        }
//        logger.info("调查流水号{}，前往征信系统查询客户近30天征信信息结束..............", lmtSurveyReportMainInfo.getSurveySerno());
        return new ResultDto(0);
    }

    /**
     * @param serno
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/10/8 11:18
     * @version 1.0.0
     * @desc  根据调查流水号返回借据流水号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto getBillNo(String serno) {
        logger.info("根据调查流水号返回借据流水号开始，调查流水号：【{}】", serno);
        LmtSurveyReportMainInfo mainInfo = lmtSurveyReportMainInfoMapper.selectByPrimaryKey(serno);
        if (Objects.isNull(mainInfo)) {
            return new ResultDto().code(9999).message("未获取到调查主表信息！");
        }
        CusLstZxd cusLstZxd = cusLstZxdService.selectByPrimaryKey(mainInfo.getListSerno());
        if (Objects.isNull(cusLstZxd)) {
            return new ResultDto().code(9999).message("未获取到增享贷白名单信息！");
        }
        logger.info("根据调查流水号返回借据流水号结束，借据流水号：【{}】", cusLstZxd.getBillNo());
        return new ResultDto(cusLstZxd.getBillNo());
    }
}
