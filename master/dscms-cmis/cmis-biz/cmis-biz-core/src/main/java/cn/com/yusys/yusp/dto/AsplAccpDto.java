package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplAccp
 * @类描述: aspl_accp数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-06-04 14:09:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class AsplAccpDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 出账流水号 **/
	private String pvpSerno;

	/** 主管客户经理 **/
	private String managerId;

	/** 主管机构 **/
	private String managerBrId;

	/** 资产池协议编号 **/
	private String contNo;

	/** 中文合同编号 **/
	private String contCnNo;

	/** 任务编号 **/
	private String taskId;

	/** 核心合同号 **/
	private String coreContNo;

	/** 生成日期 **/
	private String taskCreateDate;

	/** 要求完成日期 **/
	private String needFinishDate;

	/** 批次票面金额 **/
	private BigDecimal batchDrftAmt;

	/** 批次条数 **/
	private Integer batchQnt;

	/** 收集日期 **/
	private String collectDate;

	/** 审批状态 **/
	private String approveStatus;

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getContNo() {
		return contNo;
	}

	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	public String getContCnNo() {
		return contCnNo;
	}

	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getCoreContNo() {
		return coreContNo;
	}

	public void setCoreContNo(String coreContNo) {
		this.coreContNo = coreContNo;
	}

	public String getTaskCreateDate() {
		return taskCreateDate;
	}

	public void setTaskCreateDate(String taskCreateDate) {
		this.taskCreateDate = taskCreateDate;
	}

	public String getNeedFinishDate() {
		return needFinishDate;
	}

	public void setNeedFinishDate(String needFinishDate) {
		this.needFinishDate = needFinishDate;
	}

	public BigDecimal getBatchDrftAmt() {
		return batchDrftAmt;
	}

	public void setBatchDrftAmt(BigDecimal batchDrftAmt) {
		this.batchDrftAmt = batchDrftAmt;
	}

	public Integer getBatchQnt() {
		return batchQnt;
	}

	public void setBatchQnt(Integer batchQnt) {
		this.batchQnt = batchQnt;
	}

	public String getCollectDate() {
		return collectDate;
	}

	public void setCollectDate(String collectDate) {
		this.collectDate = collectDate;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getPvpSerno() {
		return pvpSerno;
	}

	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

}