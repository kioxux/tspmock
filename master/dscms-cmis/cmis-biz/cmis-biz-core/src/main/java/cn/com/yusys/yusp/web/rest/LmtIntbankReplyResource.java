/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.domain.LmtReply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtIntbankReply;
import cn.com.yusys.yusp.service.LmtIntbankReplyService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankReplyResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-25 20:55:02
 * @创建时间: 2021-05-21 20:32:04
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtintbankreply")
public class LmtIntbankReplyResource {
    @Autowired
    private LmtIntbankReplyService lmtIntbankReplyService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtIntbankReply>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtIntbankReply> list = lmtIntbankReplyService.selectAll(queryModel);
        return new ResultDto<List<LmtIntbankReply>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtIntbankReply>> index(QueryModel queryModel) {
        List<LmtIntbankReply> list = lmtIntbankReplyService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankReply>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtIntbankReply> show(@PathVariable("pkId") String pkId) {
        LmtIntbankReply lmtIntbankReply = lmtIntbankReplyService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtIntbankReply>(lmtIntbankReply);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtIntbankReply> create(@RequestBody LmtIntbankReply lmtIntbankReply) throws URISyntaxException {
        lmtIntbankReplyService.insert(lmtIntbankReply);
        return new ResultDto<LmtIntbankReply>(lmtIntbankReply);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtIntbankReply lmtIntbankReply) throws URISyntaxException {
        int result = lmtIntbankReplyService.update(lmtIntbankReply);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtIntbankReplyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtIntbankReplyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    @PostMapping("/selectByModel")
    protected  ResultDto<List<LmtIntbankReply>> selectByModel(@RequestBody QueryModel queryModel){
        List<LmtIntbankReply> list = lmtIntbankReplyService.selectByModel(queryModel);
        return new ResultDto<List<LmtIntbankReply>>(list);
    }

    /**
     * @函数名称:updateRestByPkId
     * @函数描述:根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateRestByPkId")
    protected int updateRestByPkId(@RequestBody QueryModel model){
        return lmtIntbankReplyService.updateRestByPkId(model);
    }

}
