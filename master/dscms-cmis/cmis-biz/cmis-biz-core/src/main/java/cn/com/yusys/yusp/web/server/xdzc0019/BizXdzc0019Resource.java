package cn.com.yusys.yusp.web.server.xdzc0019;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0019.req.Xdzc0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0019.resp.Xdzc0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0019.Xdzc0019Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:历史出入池记录查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0019:历史出入池记录查询")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0019Resource.class);

    @Autowired
    private Xdzc0019Service xdzc0019Service;
    /**
     * 交易码：xdzc0019
     * 交易描述：历史出入池记录查询
     *
     * @param xdzc0019DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("历史出入池记录查询")
    @PostMapping("/xdzc0019")
    protected @ResponseBody
    ResultDto<Xdzc0019DataRespDto> xdzc0019(@Validated @RequestBody Xdzc0019DataReqDto xdzc0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value, JSON.toJSONString(xdzc0019DataReqDto));
        Xdzc0019DataRespDto xdzc0019DataRespDto = new Xdzc0019DataRespDto();// 响应Dto:历史出入池记录查询
        ResultDto<Xdzc0019DataRespDto> xdzc0019DataResultDto = new ResultDto<>();

        try {
            xdzc0019DataRespDto = xdzc0019Service.xdzc0019Service(xdzc0019DataReqDto);
            xdzc0019DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0019DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value, e.getMessage());
            // 封装xdzc0019DataResultDto中异常返回码和返回信息
            xdzc0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0019DataRespDto到xdzc0019DataResultDto中
        xdzc0019DataResultDto.setData(xdzc0019DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value, JSON.toJSONString(xdzc0019DataResultDto));
        return xdzc0019DataResultDto;
    }
}
