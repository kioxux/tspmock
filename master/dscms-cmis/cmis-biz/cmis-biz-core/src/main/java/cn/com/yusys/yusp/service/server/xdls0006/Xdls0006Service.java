package cn.com.yusys.yusp.service.server.xdls0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.req.CmisCus0006ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CmisCus0006RespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdls0006.req.Xdls0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0006.resp.GuarList;
import cn.com.yusys.yusp.dto.server.xdls0006.resp.Xdls0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CoopPartnerAgrAccInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CoopPlanAppMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.client.cus.cmiscus0006.CmisCus0006Service;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class Xdls0006Service {
    @Autowired
    private CoopPlanAppMapper coopPlanAppMapper;

    @Autowired
    private CoopPartnerAgrAccInfoMapper coopPartnerAgrAccInfoMapper;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private AdminSmOrgService adminSmOrgService;

    @Autowired
    private CmisCus0006Service cmisCus0006Service;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdls0006.Xdls0006Service.class);

    /**
     * 白领贷额度查询
     *
     * @param xdls0006DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdls0006DataRespDto xdls0006(Xdls0006DataReqDto xdls0006DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0006.key, DscmsEnum.TRADE_CODE_XDLS0006.value);
        //返回对象
        Xdls0006DataRespDto xdls0006DataRespDto = new Xdls0006DataRespDto();
        CmisCus0006ReqDto cmisCus0006ReqDto = new CmisCus0006ReqDto();//请求Dto：查询客户基本信息
        CmisCus0006RespDto cmisCus0006RespDto = new CmisCus0006RespDto();//响应Dto：查询客户基本信息
        try {
            String certNo = xdls0006DataReqDto.getCertNo();//证件号
            String biz_type = xdls0006DataReqDto.getPrdType();//产品类型
            if ("2".equals(biz_type)) {
                biz_type = "022032";//工资贷
            } else if ("3".equals(biz_type)) {
                biz_type = "022033";//公积金贷
            } else if ("5".equals(biz_type)) {
                biz_type = "022034";//工薪贷
            } else if ("6".equals(biz_type)) {
                biz_type = "6";//房易贷
            } else if ("7".equals(biz_type)) {
                biz_type = "7";//一商场
            } else {
                biz_type = "022028";//默认白领易贷通
            }
            //取系统日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");//当前日期
            //通过客户证件号查询客户信息
            cmisCus0006ReqDto.setCertCode(certNo);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
            cmisCus0006RespDto = cmisCus0006Service.cmisCus0006(cmisCus0006ReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0006.key, DscmsEnum.TRADE_CODE_CMISCUS0006.value, JSON.toJSONString(cmisCus0006ReqDto));
            List<CusBaseDto> cusBaseList = cmisCus0006RespDto.getCusBaseList();
            // 客户ids
            String cusId = cusBaseList.parallelStream()
                    .filter(cusBaseDto -> Objects.equals(certNo, cusBaseDto.getCertCode()))//判断 请求参数中客户证件号和cusBaseDto中证件号是否相等
                    .map(cusBaseDto -> cusBaseDto.getCusId()).limit(1).collect(Collectors.joining(StringUtils.EMPTY));
            ; // 获取cusBaseDto中客户号
            java.util.List<cn.com.yusys.yusp.dto.server.xdls0006.resp.List> result = new java.util.ArrayList<>();
            Map queryMap = new HashMap();
            //得到合同额度基本信息
            cn.com.yusys.yusp.dto.server.xdls0006.resp.List xdls0006List = null;
            cn.com.yusys.yusp.dto.server.xdls0006.resp.List list = new cn.com.yusys.yusp.dto.server.xdls0006.resp.List();
            if ("022032".equals(biz_type) || "022033".equals(biz_type) || "022034".equals(biz_type)) {//工资贷等信用类看这里
                queryMap.put("prd_id", biz_type);
                queryMap.put("cert_code", certNo);
                queryMap.put("openDay", openDay);
                queryMap.put("cusId", cusId);
                if ("022032".equals(biz_type) || "022033".equals(biz_type)) {//工资贷和公积金贷为网银签订
                    logger.info("*********XDLS0006*工资贷和公积金贷为网银签订记录查询开始,查询参数为:{}", JSON.toJSONString(queryMap));
                    result = ctrLoanContMapper.selectAmountByCertNo((HashMap<String, String>) queryMap);
                    logger.info("*********XDLS0006*工资贷和公积金贷为网银签订记录查询结束,返回参数为:{}", JSON.toJSONString(result));
                } else if ("022034".equals(biz_type)) {//工薪贷为信贷系统签订
                    logger.info("*********XDLS0006*工薪贷为信贷系统签订记录查询开始,查询参数为:{}", JSON.toJSONString(queryMap));
                    result = ctrLoanContMapper.selectWagesAmountByCertNo((HashMap<String, String>) queryMap);
                    logger.info("*********XDLS0006*工薪贷为信贷系统签订记录查询结束,返回参数为:{}", JSON.toJSONString(result));
                }
                if (result.size() > 0) {//存在合同额度记录
                    for (int i = 0; i < result.size(); i++) {
                        xdls0006List = result.get(i);
                        String contNo = xdls0006List.getContNo();//获取合同编号
                        String loanEndDate = xdls0006List.getLoanEndDate();// 贷款终止日期
                        String loanTerm = xdls0006List.getLoanTerm();// 贷款期限
                        BigDecimal rate = xdls0006List.getRate();// 执行利率
                        BigDecimal loanBal = xdls0006List.getLoanBal();//贷款余额
                        BigDecimal lmt = xdls0006List.getLmt();//可用余额
                        String guarContNo = xdls0006List.getGuarContNo();// 担保合同号
                        String contStatus = xdls0006List.getContStatus();// 合同状态
                        String guarWay1 = xdls0006List.getGuarWay();//担保方式
                        //根据合同号查询责任机构
                        //设置查询参数
                        Map map = new HashMap();
                        map.put("contNo", contNo);//合同号
                        //查询机构号码
                        String managerBrId = ctrLoanContMapper.queryCtrLoanContManagerBrIdByContNo(map);
                        //根据机构号查询机构名称
                        ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                        String orgName = "";//机构名称
                        if (ResultDto.success().getCode().equals(adminSmOrgDtoResultDto.getCode())) {
                            AdminSmOrgDto adminSmOrgDto = adminSmOrgDtoResultDto.getData();
                            if (!Objects.isNull(adminSmOrgDto)) {
                                orgName = adminSmOrgDto.getOrgName();
                            }
                        }
                        //  封装xdls0006DataRespDto对象开始
                        list.setContNo(contNo);// 合同号
                        list.setLoanEndDate(loanEndDate);// 贷款终止日期
                        list.setLoanTerm(loanTerm);// 贷款期限
                        list.setIsEbankPay(StringUtils.EMPTY);// 是否网银支付，新信贷无此字段，需王玉坤确认
                        list.setRate(rate.multiply(BigDecimal.valueOf(100)));// 执行利率
                        list.setLoanBal(loanBal);// 贷款余额
                        list.setLmt(lmt);
                        list.setGuarContNo(guarContNo);// 担保合同号
                        list.setOrgNo(orgName);// 机构名称
                        list.setContStatus(contStatus);// 合同状态
                        list.setGuarWay(guarWay1);
                        //  封装xdls0006DataRespDto对象结束
                    }
                }
            } else if ("022028".equals(biz_type)) {
                queryMap.put("cert_code", certNo);
                queryMap.put("openDay", openDay);
                queryMap.put("cusId", cusId);
                logger.info("*********XDLS0006*biz_type=022028*ctrLoanContMapper查询开始,查询参数为:{}", JSON.toJSONString(queryMap));
                result = ctrLoanContMapper.selectWhiteCollarAmountByCertNo((HashMap<String, String>) queryMap);
                logger.info("*********XDLS0006*biz_type=022028*ctrLoanContMapper查询结束,返回参数为:{}", JSON.toJSONString(result));
                int j = 0;
                int p = 0;
                int g = 0;
                String Gage_status_code = null;
                if (result.size() > 0) {//存在合同额度记录
                    xdls0006List = result.get(0);//这里只取最新的一条
                    String guarWay = xdls0006List.getGuarWay();//担保方式
                    String lmtAccNo = xdls0006List.getLmtAccNo();//授信额度编号
                    String contNo = xdls0006List.getContNo();//获取合同编号
                    Map mortMap = new HashMap();
                    mortMap.put("lmtAccNo", lmtAccNo);
                    mortMap.put("contNo", contNo);
                    if ("10".equals(guarWay) || "20".equals(guarWay)) {//担保方式为抵押
                        logger.info("*********XDLS0006*担保方式为抵押的押品信息查询开始,查询参数为:{}", JSON.toJSONString(mortMap));
                        java.util.List<GuarList> guarBaseInfo = guarBaseInfoMapper.selectMortInfoBySerno((HashMap<String, String>) mortMap);
                        logger.info("*********XDLS0006*担保方式为抵押的押品信息查询结束,返回参数为:{}", JSON.toJSONString(guarBaseInfo));
                        if (guarBaseInfo.size() > 0) {
                            cn.com.yusys.yusp.dto.server.xdls0006.resp.GuarList xdls0006GuarList = guarBaseInfo.get(0);
                            Gage_status_code = xdls0006GuarList.getGuarNo();
                        }
                        if (Gage_status_code == null) {
                            String loanEndDate = xdls0006List.getLoanEndDate();// 贷款终止日期
                            String loanTerm = xdls0006List.getLoanTerm();// 贷款期限
                            BigDecimal rate = xdls0006List.getRate();// 执行利率
                            BigDecimal loanBal = xdls0006List.getLoanBal();//贷款余额
                            BigDecimal lmt = xdls0006List.getLmt();
                            String guarContNo = xdls0006List.getGuarContNo();// 担保合同号
                            String contStatus = xdls0006List.getContStatus();// 合同状态
                            String guarWay2 = xdls0006List.getGuarWay();//担保方式
                            //根据合同号查询责任机构
                            //设置查询参数
                            Map map = new HashMap();
                            map.put("contNo", contNo);//合同号
                            //查询客户经理工号
                            String managerBrId = ctrLoanContMapper.queryCtrLoanContManagerBrIdByContNo(map);
                            //根据机构号查询机构名称
                            ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                            String orgName = "";//机构名称
                            if (ResultDto.success().getCode().equals(adminSmOrgDtoResultDto.getCode())) {
                                AdminSmOrgDto adminSmOrgDto = adminSmOrgDtoResultDto.getData();
                                if (!Objects.isNull(adminSmOrgDto)) {
                                    orgName = adminSmOrgDto.getOrgName();
                                }
                            }
                            //  封装xdls0006DataRespDto对象开始
                            list.setContNo(contNo);// 合同号
                            list.setLoanEndDate(loanEndDate);// 贷款终止日期
                            list.setLoanTerm(loanTerm);// 贷款期限
                            list.setIsEbankPay(StringUtils.EMPTY);// 是否网银支付，新信贷无此字段，需王玉坤确认
                            list.setRate(rate.multiply(BigDecimal.valueOf(100)));// 执行利率
                            list.setLoanBal(loanBal);// 贷款余额
                            list.setLmt(lmt);
                            list.setGuarContNo(guarContNo);// 担保合同号
                            list.setOrgNo(orgName);// 机构名称
                            list.setContStatus(contStatus);// 合同状态
                            list.setGuarWay(guarWay2);
                            //  封装xdls0006DataRespDto对象结束
                        }
                    } else if ("00".equals(guarWay) || "30".equals(guarWay)) {
                        String loanEndDate = xdls0006List.getLoanEndDate();// 贷款终止日期
                        String loanTerm = xdls0006List.getLoanTerm();// 贷款期限
                        BigDecimal rate = xdls0006List.getRate();// 执行利率
                        BigDecimal loanBal = xdls0006List.getLoanBal();//贷款余额
                        BigDecimal lmt = xdls0006List.getLmt();//可用余额
                        String guarContNo = xdls0006List.getGuarContNo();// 担保合同号
                        String contStatus = xdls0006List.getContStatus();// 合同状态
                        String guarWay3 = xdls0006List.getGuarWay();//担保方式
                        //根据合同号查询责任机构
                        //设置查询参数
                        Map map = new HashMap();
                        map.put("contNo", contNo);//合同号
                        //查询客户经理工号
                        String managerBrId = ctrLoanContMapper.queryCtrLoanContManagerBrIdByContNo(map);
                        //根据机构号查询机构名称
                        ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                        String orgName = "";//机构名称
                        if (ResultDto.success().getCode().equals(adminSmOrgDtoResultDto.getCode())) {
                            AdminSmOrgDto adminSmOrgDto = adminSmOrgDtoResultDto.getData();
                            if (!Objects.isNull(adminSmOrgDto)) {
                                orgName = adminSmOrgDto.getOrgName();
                            }
                        }
                        //  封装xdls0006DataRespDto对象开始
                        list.setContNo(contNo);// 合同号
                        list.setLoanEndDate(loanEndDate);// 贷款终止日期
                        list.setLoanTerm(loanTerm);// 贷款期限
                        list.setIsEbankPay(StringUtils.EMPTY);// 是否网银支付，新信贷无此字段，需王玉坤确认
                        list.setRate(rate.multiply(BigDecimal.valueOf(100)));// 执行利率
                        list.setLoanBal(loanBal);// 贷款余额
                        list.setLmt(lmt);// 可用余额
                        list.setGuarContNo(guarContNo);// 担保合同号
                        list.setOrgNo(orgName);// 机构名称
                        list.setContStatus(contStatus);// 合同状态
                        list.setGuarWay(guarWay3);
                        //  封装xdls0006DataRespDto对象结束
                    }
                }
            } else {////业务类型不定，根据调查决议中的biz_type来确定是房易贷或者一商场的最高额合同
                String biz_type_ = "";
                if ("6".equals(biz_type)) {//房易贷最高额合同
                    biz_type = "PW030004";
                    biz_type_ = "PW030004','SC030023";
                } else if ("7".equals(biz_type)) {//商场供应商贷
                    biz_type = "SC030014";
                    biz_type_ = "SC030014";
                }
                //这里比较难统计考虑先获取到最高额的合同号
                queryMap.put("cert_code", certNo);
                queryMap.put("prd_id", biz_type);
                queryMap.put("cusId", cusId);
                logger.info("*********XDLS0006*业务类型不定，根据调查决议中的biz_type来确定是房易贷或者一商场的最高额合同查询开始,查询参数为:{}", JSON.toJSONString(queryMap));
                String cont_no = ctrLoanContMapper.queryContNoByCertNo(queryMap);
                logger.info("*********XDLS0006业务类型不定，根据调查决议中的biz_type来确定是房易贷或者一商场的最高额合同查询结束,返回参数为:{}", JSON.toJSONString(cont_no));

                if (!"".equals(cont_no)) {
                    Map map = new HashMap();
                    map.put("openDay", openDay);
                    map.put("cont_no", cont_no);
                    result = ctrLoanContMapper.selectAmountInfoByContNo((HashMap<String, String>) map);
                    if (result.size() > 0) {
                        for (int i = 0; i < result.size(); i++) {
                            xdls0006List = result.get(i);
                            String contNo = xdls0006List.getContNo();//获取合同编号
                            String loanEndDate = xdls0006List.getLoanEndDate();// 贷款终止日期
                            String loanTerm = xdls0006List.getLoanTerm();// 贷款期限
                            BigDecimal rate = xdls0006List.getRate();// 执行利率
                            BigDecimal loanBal = xdls0006List.getLoanBal();//贷款余额
                            BigDecimal lmt = xdls0006List.getLmt();//可用余额
                            String guarContNo = xdls0006List.getGuarContNo();// 担保合同号
                            String contStatus = xdls0006List.getContStatus();// 合同状态
                            String guarWay4 = xdls0006List.getGuarWay();//担保方式
                            //根据合同号查询责任机构
                            //设置查询参数
                            Map contMap = new HashMap();
                            contMap.put("contNo", contNo);//合同号
                            //查询客户经理工号
                            String managerBrId = ctrLoanContMapper.queryCtrLoanContManagerBrIdByContNo(contMap);
                            //根据机构号查询机构名称
                            ResultDto<AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(managerBrId);
                            String orgName = "";//机构名称
                            if (ResultDto.success().getCode().equals(adminSmOrgDtoResultDto.getCode())) {
                                AdminSmOrgDto adminSmOrgDto = adminSmOrgDtoResultDto.getData();
                                if (!Objects.isNull(adminSmOrgDto)) {
                                    orgName = adminSmOrgDto.getOrgName();
                                }
                            }
                            //  封装xdls0006DataRespDto对象开始
                            list.setContNo(contNo);// 合同号
                            list.setLoanEndDate(loanEndDate);// 贷款终止日期
                            list.setLoanTerm(loanTerm);// 贷款期限
                            list.setIsEbankPay(StringUtils.EMPTY);// 是否网银支付，新信贷无此字段，需王玉坤确认
                            list.setRate(rate.multiply(BigDecimal.valueOf(100)));// 执行利率
                            list.setLoanBal(loanBal);// 贷款余额
                            list.setLmt(lmt);
                            list.setGuarContNo(guarContNo);// 担保合同号
                            list.setOrgNo(orgName);// 机构名称
                            list.setContStatus(contStatus);// 合同状态
                            list.setGuarWay(guarWay4);
                            //  封装xdls0006DataRespDto对象结束
                        }
                    }
                }
            }
            xdls0006DataRespDto.setList(Arrays.asList(list));
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0006.key, DscmsEnum.TRADE_CODE_XDLS0006.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0006.key, DscmsEnum.TRADE_CODE_XDLS0006.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0006.key, DscmsEnum.TRADE_CODE_XDLS0006.value);
        return xdls0006DataRespDto;
    }
}
