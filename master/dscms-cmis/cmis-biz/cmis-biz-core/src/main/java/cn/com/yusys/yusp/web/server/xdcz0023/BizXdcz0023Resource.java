package cn.com.yusys.yusp.web.server.xdcz0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0023.req.Xdcz0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0023.resp.Xdcz0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0023.Xdcz0023Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:小企业无还本续贷放款
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0023:小企业无还本续贷放款")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0023Resource.class);

    @Autowired
    private Xdcz0023Service xdcz0023Service;

    /**
     * 交易码：xdcz0023
     * 交易描述：小企业无还本续贷放款
     *
     * @param xdcz0023DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小企业无还本续贷放款")
    @PostMapping("/xdcz0023")
    protected @ResponseBody
    ResultDto<Xdcz0023DataRespDto> xdcz0023(@Validated @RequestBody Xdcz0023DataReqDto xdcz0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023DataReqDto));
        Xdcz0023DataRespDto xdcz0023DataRespDto = new Xdcz0023DataRespDto();// 响应Dto:小企业无还本续贷放款
        ResultDto<Xdcz0023DataRespDto> xdcz0023DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0023DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023DataReqDto));
            xdcz0023DataRespDto = xdcz0023Service.loanWithoutRepayForSmall(xdcz0023DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023DataRespDto));
            // 封装xdcz0023DataResultDto中正确的返回码和返回信息
            xdcz0023DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0023DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, e.getMessage());
            // 封装xdcz0023DataResultDto中异常返回码和返回信息
            xdcz0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0023DataRespDto到xdcz0023DataResultDto中
        xdcz0023DataResultDto.setData(xdcz0023DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023DataRespDto));
        return xdcz0023DataResultDto;
    }
}
