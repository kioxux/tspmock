package cn.com.yusys.yusp.service.server.xdzc0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.DocAsplTcont;
import cn.com.yusys.yusp.dto.server.xdzc0016.req.Xdzc0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0016.resp.List;
import cn.com.yusys.yusp.dto.server.xdzc0016.resp.Xdzc0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:购销合同查询
 *
 * @Author xs
 * @Date 2021/6/11 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0016Service {

    @Autowired
    private DocAsplTcontService docAsplTcontService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private AccTContRelService accTContRelService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0016.Xdzc0016Service.class);

    /**
     * 交易码：xdzc0016
     * 交易描述：
     * 购销合同查询
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdzc0016DataRespDto xdzc0016Service(Xdzc0016DataReqDto xdzc0016DataReqDto) throws BizException, Exception{
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value);
        Xdzc0016DataRespDto xdzc0016DataRespDto = new Xdzc0016DataRespDto();

        String cusId = xdzc0016DataReqDto.getCusId();//客户号
        String queryType = xdzc0016DataReqDto.getQueryType();//查询类型
        String contStartDateAfter = xdzc0016DataReqDto.getContStartDateAfter();//合同起始日开始
        String contStartDateBerfore = xdzc0016DataReqDto.getContStartDateBerfore();//合同起始日截止
        String contEndDateAfter = xdzc0016DataReqDto.getContEndDateAfter();//合同到期日开始
        String contEndDateBefore = xdzc0016DataReqDto.getContEndDateBefore();//合同到期日截止
        String tContCnName = xdzc0016DataReqDto.gettContCnName();//合同名称
        String tContNo = xdzc0016DataReqDto.gettContNo();//合同编号
//        xdzc0016DataRespDto.setTContCnName(StringUtils.EMPTY);// 合同名称
//        xdzc0016DataRespDto.setCusName(StringUtils.EMPTY);// 公司名称
//        xdzc0016DataRespDto.setTContNo(StringUtils.EMPTY);// 合同编号
//        xdzc0016DataRespDto.setTContAmt(new BigDecimal(0L));// 合同金额
//        xdzc0016DataRespDto.setComtAvaAmt(new BigDecimal(0L));// 剩余可挂靠金额
//        xdzc0016DataRespDto.setContStartDate(StringUtils.EMPTY);// 合同起始日
//        xdzc0016DataRespDto.setContEndDate(StringUtils.EMPTY);// 合同到期日
//        xdzc0016DataRespDto.setStatus(StringUtils.EMPTY);// 状态
        int page = xdzc0016DataReqDto.getPage();// 页数
        int size = xdzc0016DataReqDto.getSize();// 条数
        String tContYXSerno = xdzc0016DataReqDto.gettContYXSerno();//购销合同影像流水号
        try {
            // 根据客户号获取资产池下的银票贸易背景收集任务表的 贸易合同编号
            QueryModel model = new QueryModel();
            model.addCondition("cusId",cusId);//客户号
            if(StringUtils.nonEmpty(tContNo)){
                model.addCondition("tContNo",tContNo+"%");//购销合同
            }
            if(StringUtils.nonEmpty(tContCnName)){
                model.addCondition("tContCnName",tContCnName+"%");//客户号
            }
            if(StringUtils.nonEmpty(tContYXSerno)){
                model.addCondition("tcontImgId",tContYXSerno+"%");//购销合同影像流水号
            }

            if("0".equals(queryType)){// 当前
                model.addCondition("approveStatus", CmisCommonConstants.WF_STATUS_000992111 );
            }else if("1".equals(queryType)){// 历史
                model.addCondition("approveStatus",CmisCommonConstants.WF_STATUS_996997998);
            }else if("3".equals(queryType)){// 审批
                model.addCondition("approveStatus",CmisCommonConstants.WF_STATUS_997);
            }
            model.addCondition("startDateAfter",contStartDateAfter);//合同起始日开始
            model.addCondition("startDateBerfore",contStartDateBerfore);//合同起始日截止
            model.addCondition("endDateAfter",contEndDateAfter);//合同到期日开始
            model.addCondition("endDateBefore",contEndDateBefore);//合同到期日截止
            PageHelper.startPage(page, size);
            java.util.List<List> resultList = docAsplTcontService.selectByAsplAccpTask(model);
            PageInfo<List> pageinfo = new PageInfo<>(resultList);
            int total = (int)pageinfo.getTotal();
            PageHelper.clearPage();
            // 是否购销合同实时计算(支持详情查询)
            if ("4".equals(queryType)){
                if(total == 1){
                    // 开始计算  查台账（ 银承、贷款）
                    List list = resultList.get(0);
                    BigDecimal loanSum = Optional.ofNullable(accTContRelService.selectSumLoan(list.getCusId(),tContYXSerno)).orElse(BigDecimal.ZERO);
                    BigDecimal accpSum = Optional.ofNullable(accTContRelService.selectSumAccp(list.getCusId(),tContYXSerno)).orElse(BigDecimal.ZERO);
                    BigDecimal contAmt = list.gettContAmt();;
                    BigDecimal contHighAvlAmt = contAmt.subtract(loanSum).subtract(accpSum);
                    list.setContHighAvlAmt(contHighAvlAmt);
                    resultList.set(0,list);
                }else if(total == 0){
                    throw BizException.error(null, "9999", "查询到多条数据，仅支持单条详情数据查询");
                }else{
                    throw BizException.error(null, "9999", "未查询到客户号："+cusId+",购销影像流水为："+tContYXSerno);
                }
            }
            xdzc0016DataRespDto.setList(resultList);
            xdzc0016DataRespDto.setTotal(total);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value);
        return xdzc0016DataRespDto;
    }

}
