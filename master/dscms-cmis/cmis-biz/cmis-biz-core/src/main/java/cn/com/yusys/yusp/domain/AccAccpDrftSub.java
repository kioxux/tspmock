/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccAccpDrftSub
 * @类描述: acc_accp_drft_sub数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-09 10:19:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_accp_drft_sub")
public class AccAccpDrftSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 核心银承编号 **/
	@Column(name = "CORE_BILL_NO", unique = false, nullable = true, length = 40)
	private String coreBillNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 20)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 是否电子票据 **/
	@Column(name = "IS_E_DRFT", unique = false, nullable = true, length = 5)
	private String isEDrft;
	
	/** 汇票号码 **/
	@Column(name = "PORDER_NO", unique = false, nullable = true, length = 40)
	private String porderNo;
	
	/** 是否他行代签 **/
	@Column(name = "IS_OTHER_BANK_SIGN", unique = false, nullable = true, length = 5)
	private String isOtherBankSign;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 票据金额 **/
	@Column(name = "DRAFT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal draftAmt;
	
	/** 出票日期 **/
	@Column(name = "ISSE_DATE", unique = false, nullable = true, length = 20)
	private String isseDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;
	
	/** 收款人名称 **/
	@Column(name = "PYEE_NAME", unique = false, nullable = true, length = 80)
	private String pyeeName;
	
	/** 收款人账号 **/
	@Column(name = "PYEE_ACCNO", unique = false, nullable = true, length = 40)
	private String pyeeAccno;
	
	/** 收款人开户行行号 **/
	@Column(name = "PYEE_ACCTSVCR_NO", unique = false, nullable = true, length = 40)
	private String pyeeAcctsvcrNo;
	
	/** 收款人开户行名称 **/
	@Column(name = "PYEE_ACCTSVCR_NAME", unique = false, nullable = true, length = 80)
	private String pyeeAcctsvcrName;
	
	/** 承兑行类型 **/
	@Column(name = "AORG_TYPE", unique = false, nullable = true, length = 5)
	private String aorgType;
	
	/** 承兑行行号 **/
	@Column(name = "AORG_NO", unique = false, nullable = true, length = 40)
	private String aorgNo;
	
	/** 承兑行名称 **/
	@Column(name = "AORG_NAME", unique = false, nullable = true, length = 80)
	private String aorgName;
	
	/** 出票人开户行账号 **/
	@Column(name = "DAORG_NO", unique = false, nullable = true, length = 40)
	private String daorgNo;
	
	/** 出票人开户户名 **/
	@Column(name = "DAORG_NAME", unique = false, nullable = true, length = 80)
	private String daorgName;
	
	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;
	
	/** 授信台账编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 20)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 账务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
	private String finaBrId;
	
	/** 账务机构名称 **/
	@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 80)
	private String finaBrIdName;
	
	/** 签发机构编号 **/
	@Column(name = "ISSUED_ORG_NO", unique = false, nullable = true, length = 40)
	private String issuedOrgNo;
	
	/** 签发机构名称 **/
	@Column(name = "ISSUED_ORG_NAME", unique = false, nullable = true, length = 80)
	private String issuedOrgName;
	
	/** 兑付机构编号 **/
	@Column(name = "PAY_ORG_NO", unique = false, nullable = true, length = 40)
	private String payOrgNo;
	
	/** 兑付机构名称 **/
	@Column(name = "PAY_ORG_NAME", unique = false, nullable = true, length = 80)
	private String payOrgName;
	
	/** 五级分类 **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;
	
	/** 十级分类 **/
	@Column(name = "TEN_CLASS", unique = false, nullable = true, length = 5)
	private String tenClass;
	
	/** 分类日期 **/
	@Column(name = "CLASS_DATE", unique = false, nullable = true, length = 20)
	private String classDate;
	
	/** 台账状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param coreBillNo
	 */
	public void setCoreBillNo(String coreBillNo) {
		this.coreBillNo = coreBillNo;
	}
	
    /**
     * @return coreBillNo
     */
	public String getCoreBillNo() {
		return this.coreBillNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param isEDrft
	 */
	public void setIsEDrft(String isEDrft) {
		this.isEDrft = isEDrft;
	}
	
    /**
     * @return isEDrft
     */
	public String getIsEDrft() {
		return this.isEDrft;
	}
	
	/**
	 * @param porderNo
	 */
	public void setPorderNo(String porderNo) {
		this.porderNo = porderNo;
	}
	
    /**
     * @return porderNo
     */
	public String getPorderNo() {
		return this.porderNo;
	}
	
	/**
	 * @param isOtherBankSign
	 */
	public void setIsOtherBankSign(String isOtherBankSign) {
		this.isOtherBankSign = isOtherBankSign;
	}
	
    /**
     * @return isOtherBankSign
     */
	public String getIsOtherBankSign() {
		return this.isOtherBankSign;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param draftAmt
	 */
	public void setDraftAmt(java.math.BigDecimal draftAmt) {
		this.draftAmt = draftAmt;
	}
	
    /**
     * @return draftAmt
     */
	public java.math.BigDecimal getDraftAmt() {
		return this.draftAmt;
	}
	
	/**
	 * @param isseDate
	 */
	public void setIsseDate(String isseDate) {
		this.isseDate = isseDate;
	}
	
    /**
     * @return isseDate
     */
	public String getIsseDate() {
		return this.isseDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}
	
    /**
     * @return bailAmt
     */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}
	
	/**
	 * @param pyeeName
	 */
	public void setPyeeName(String pyeeName) {
		this.pyeeName = pyeeName;
	}
	
    /**
     * @return pyeeName
     */
	public String getPyeeName() {
		return this.pyeeName;
	}
	
	/**
	 * @param pyeeAccno
	 */
	public void setPyeeAccno(String pyeeAccno) {
		this.pyeeAccno = pyeeAccno;
	}
	
    /**
     * @return pyeeAccno
     */
	public String getPyeeAccno() {
		return this.pyeeAccno;
	}
	
	/**
	 * @param pyeeAcctsvcrNo
	 */
	public void setPyeeAcctsvcrNo(String pyeeAcctsvcrNo) {
		this.pyeeAcctsvcrNo = pyeeAcctsvcrNo;
	}
	
    /**
     * @return pyeeAcctsvcrNo
     */
	public String getPyeeAcctsvcrNo() {
		return this.pyeeAcctsvcrNo;
	}
	
	/**
	 * @param pyeeAcctsvcrName
	 */
	public void setPyeeAcctsvcrName(String pyeeAcctsvcrName) {
		this.pyeeAcctsvcrName = pyeeAcctsvcrName;
	}
	
    /**
     * @return pyeeAcctsvcrName
     */
	public String getPyeeAcctsvcrName() {
		return this.pyeeAcctsvcrName;
	}
	
	/**
	 * @param aorgType
	 */
	public void setAorgType(String aorgType) {
		this.aorgType = aorgType;
	}
	
    /**
     * @return aorgType
     */
	public String getAorgType() {
		return this.aorgType;
	}
	
	/**
	 * @param aorgNo
	 */
	public void setAorgNo(String aorgNo) {
		this.aorgNo = aorgNo;
	}
	
    /**
     * @return aorgNo
     */
	public String getAorgNo() {
		return this.aorgNo;
	}
	
	/**
	 * @param aorgName
	 */
	public void setAorgName(String aorgName) {
		this.aorgName = aorgName;
	}
	
    /**
     * @return aorgName
     */
	public String getAorgName() {
		return this.aorgName;
	}
	
	/**
	 * @param daorgNo
	 */
	public void setDaorgNo(String daorgNo) {
		this.daorgNo = daorgNo;
	}
	
    /**
     * @return daorgNo
     */
	public String getDaorgNo() {
		return this.daorgNo;
	}
	
	/**
	 * @param daorgName
	 */
	public void setDaorgName(String daorgName) {
		this.daorgName = daorgName;
	}
	
    /**
     * @return daorgName
     */
	public String getDaorgName() {
		return this.daorgName;
	}
	
	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}
	
    /**
     * @return isUtilLmt
     */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}
	
    /**
     * @return finaBrIdName
     */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}
	
	/**
	 * @param issuedOrgNo
	 */
	public void setIssuedOrgNo(String issuedOrgNo) {
		this.issuedOrgNo = issuedOrgNo;
	}
	
    /**
     * @return issuedOrgNo
     */
	public String getIssuedOrgNo() {
		return this.issuedOrgNo;
	}
	
	/**
	 * @param issuedOrgName
	 */
	public void setIssuedOrgName(String issuedOrgName) {
		this.issuedOrgName = issuedOrgName;
	}
	
    /**
     * @return issuedOrgName
     */
	public String getIssuedOrgName() {
		return this.issuedOrgName;
	}
	
	/**
	 * @param payOrgNo
	 */
	public void setPayOrgNo(String payOrgNo) {
		this.payOrgNo = payOrgNo;
	}
	
    /**
     * @return payOrgNo
     */
	public String getPayOrgNo() {
		return this.payOrgNo;
	}
	
	/**
	 * @param payOrgName
	 */
	public void setPayOrgName(String payOrgName) {
		this.payOrgName = payOrgName;
	}
	
    /**
     * @return payOrgName
     */
	public String getPayOrgName() {
		return this.payOrgName;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	
    /**
     * @return fiveClass
     */
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param tenClass
	 */
	public void setTenClass(String tenClass) {
		this.tenClass = tenClass;
	}
	
    /**
     * @return tenClass
     */
	public String getTenClass() {
		return this.tenClass;
	}
	
	/**
	 * @param classDate
	 */
	public void setClassDate(String classDate) {
		this.classDate = classDate;
	}
	
    /**
     * @return classDate
     */
	public String getClassDate() {
		return this.classDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}