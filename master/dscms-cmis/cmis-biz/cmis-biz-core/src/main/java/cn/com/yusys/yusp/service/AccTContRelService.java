/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccTContRel;
import cn.com.yusys.yusp.repository.mapper.AccTContRelMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccTContRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 16:43:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AccTContRelService {

    @Autowired
    private AccTContRelMapper accTContRelMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AccTContRel selectByPrimaryKey(String pkId, String pvpSerno) {
        return accTContRelMapper.selectByPrimaryKey(pkId, pvpSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AccTContRel> selectAll(QueryModel model) {
        List<AccTContRel> records = (List<AccTContRel>) accTContRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AccTContRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccTContRel> list = accTContRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AccTContRel record) {
        return accTContRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AccTContRel record) {
        return accTContRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AccTContRel record) {
        return accTContRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AccTContRel record) {
        return accTContRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId, String pvpSerno) {
        return accTContRelMapper.deleteByPrimaryKey(pkId, pvpSerno);
    }

    /**
     * @方法名称: selectbyaccp
     * @方法描述: 根据银承核心编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AccTContRel> selectbyaccp(String  coreBillNo) {
        return accTContRelMapper.selectbyaccp(coreBillNo);
    }

    public BigDecimal selectSumLoan(String cusId,String tcontImgId) {
        return accTContRelMapper.selectSumLoan(cusId,tcontImgId);
    }

    public BigDecimal selectSumAccp(String cusId,String tcontImgId) {
        return accTContRelMapper.selectSumAccp(cusId,tcontImgId);
    }
}
