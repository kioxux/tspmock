package cn.com.yusys.yusp.service.server.xdcz0026;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0013.resp.CmisLmt0013RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0057.resp.CmisLmt0057RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0026.req.Xdcz0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0026.resp.Xdcz0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.RepayDayRecordInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * 接口处理类:风控调用信贷放款
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdcz0026Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0026Service.class);

    @Autowired
    private CtrLoanContService ctrLoanContService;//合同主表
    @Autowired
    private IqpLoanAppService iqpLoanAppService;//合同申请信息表
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;//授权信息表
    @Autowired
    private AccLoanService accLoanService;//贷款台账
    @Autowired
    private CmisCusClientService cmisCusClientService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private AccLoanMapper accLoanMapper;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private ToppAcctSubService toppAcctSubService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Resource
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;//授信调查批复表
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private RepayDayRecordInfoMapper repayDayRecordInfoMapper;

    /**
     * 风控调用信贷放款</br>
     * 业务场景：渠道端发起贷款申请，经风控模型审批通过后通知信贷系统放款
     * 接口说明：信贷接收贷款申请通知后，自动生成从贷款申请iqp_loan_app-->贷款合同ctr_loan_cont-->出账申请pvp_loan_app-->出账授权pvp_authorize-->贷款台账acc_cvrg一整套全流程表
     * 业务逻辑：
     * 1.请求报文校验
     * 1).字段普通空值校验
     * 2).部分字段特殊校验说明：贷款产品为022095-阳光速贷/022096-教育分期/022097-市民贷/022099-惠民贷四种时，其【替补借款人账号repayment_account_tb】须必填
     * 2.请求报文校验通过，执行主逻辑处理：
     * 1).根据请求报文的【合同编号loan_cont_no】、【身份证号certid】查询贷款合同CTR_LOAN_CONT验证该合同是否被其他借款人占用
     * 即验证"select count(1) from ctr_loan_cont where cont_no='+"loan_cont_no"+'
     * and cus_id in (select cus_id from cus_indiv where cert_code<>'+"certid"+')"，存在则拦截并提示“合同号已被占用!”
     * 2).上述1验证通过，插入贷款申请表iqp_loan_app、贷款合同表ctr_loan_cont
     * A).iqp_loan_app重要字段赋值说明（以下仅列出部分重要字段，开发时重点关注）：
     * biz_type映射转换
     * 贷款形式loan_form='1'
     * 程序BUG纠正:人民币汇率应赋值1.00,即KeyedCollectionUtil.setValue(iqpLoanAppKColl, "exchange_rate", "0.00")-->KeyedCollectionUtil.setValue(iqpLoanAppKColl, "exchange_rate", "1.00")
     * 申请金额apply_amount、申请起始日期loan_start_date、申请到日期loan_end_date均使用接口参数值apply_amount、loan_start_date、loan_end_date
     * 基准利率从基准利率表PRD_RATE_STD获取  新信贷对应的表是 cfg_rate
     * 相关利率字段须除以100转为为小数、注意小数点保留位数
     * 担保方式加工assure_means_main：022011产品担保方式默认"抵押-10"，其他产品默认"00-信用"
     * 授信标识limit_ind='1'，即默认不使用授信
     * iqp_loan_app.approve_status='997'(默认值)、合同类型cont_type='2'（默认最高额合同）、剩余金额avail_amount=0（默认值）、repay_flag='1'(默认值)、
     * B).ctr_loan_cont重要字段赋值说明：
     * 【合同编号cont_no】【中文合同编号cn_cont_no】应使用接口参数值loan_cont_no、cn_cont_no
     * 【流水号serno】使用与iqp_loan_app相同的serno
     * 【合同状态cont_state】='200'（默认值）、【五级分类cla】='10'
     * 3).插入出账申请表PVP_LOAN_APP、出账授权表pvp_authorize、贷款台账表acc_loan
     * A).PVP_LOAN_APP重要字段赋值说明（以下仅列出部分重要字段，开发时重点关注）：
     * SERNO重新生成
     * cont_no、bill_no使用接口参数值loan_cont_no、bill_no
     * loan_start_date='当前时间'、loan_end_date使用接口参数值loan_end_date1
     * con_start_date使用接口参数值loan_start_date、con_end_date使用接口参数值loan_end_date
     * interest_acc_mode='IP001'(默认值)、ir_adjust_mode='G'(默认值)、approve_status='997'(默认值)
     * 注意【扣款日due_day】的加工、【逾期利率浮动比loan_od_rate_adj_pct1】=0.50（默认值）
     * B).pvp_authorize重要字段赋值说明（以下仅列出部分重要字段，开发时重点关注）：
     * 生成【出账号LOAN_NO】、【授权状态trade_status】='0'（默认值）
     * C).acc_loan重要字段赋值说明（以下仅列出部分重要字段，开发时重点关注）：
     * 接口参数【是否免息is_free_intere】有值时，需同时记录免息信息至【台账免息信息表 acc_loan_free_inter TODO 新信贷对应的表是啥？ 无该需求,宇信与小微和网金确认需求,含小微借据和大家E贷 】
     * 接口参数【优惠券discount_code】有值时，需同时记录信息至【优惠券业务关联表表 disc_coupon_busin_rel TODO 新信贷对应的表是啥？ 新信贷未涵盖需求，需重新讨论】
     * 接口discount_code有值，且biz_type='022100'时，发送yx0003接口通知市民贷优惠券核销锁定;biz_type为其他产品时，发送 xwh003接口通知优惠券核销锁定 TODO yx0003接口  xwh003接口
     * 4).从出账授权表获取加工后的业务数据，调用通知核心出账接口，成功后更新acc_loan.account_status='1'、pvp_authorize.trade_status='2'
     * 受托支付，记录TRUSTEE_PAY表  对应新信贷  IQP_CHG_TRUPAY_ACCT_APP 受托账号修改申请表
     * 房抵e点贷放款成功后 生成购销合同补扫ADD_IMAGEINFO表  对应新信贷 doc_image_sppl_info 档案影像补扫表    TODO 新信贷对应的表 还没有建立
     * 如果责任机构是016000，出账成功发送短信通知客户经理、发送小微公众号 TODO  确定接口文档
     * 出账成功后，如果是优享贷保存影像信息(LINE172) TODO 确定表名
     * 建议：老系统代码FkAction.java重复度高，字段重复获取。只需iqp_loan_app从请求参数获取，后续其他表的相同字段可直接从加工后的iqp_loan_app中copy获取即可
     *
     * @param xdcz0026DataReqDto
     * @return
     */
    public Xdcz0026DataRespDto xdcz0026(Xdcz0026DataReqDto xdcz0026DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026DataReqDto));
        Xdcz0026DataRespDto xdcz0026DataRespDto = new Xdcz0026DataRespDto();
        // 响应Data：风控调用信贷放款
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        String opFlag = StringUtils.EMPTY;// 操作成功标志位
        String opMsg = StringUtils.EMPTY;// 描述信息
        Boolean exists = false;//合同是否已存在
        try {
            // 1). 校验风险送过来的数据
            Map validateMap = validate(xdcz0026DataReqDto);
            if (EcbEnum.COMMON_SUCCESS_DEF.value.equals(validateMap.get("code"))) {
                xdcz0026DataReqDto = (Xdcz0026DataReqDto) validateMap.get("message");
            } else {
                opFlag = DscmsBizCzEnum.OP_FLAG_F.key;
                opMsg = validateMap.get("message").toString();
                logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, opMsg);
                xdcz0026DataRespDto.setOpFlag(opFlag);// 操作标志
                xdcz0026DataRespDto.setOpMsg(opMsg);// 描述信息
                throw BizException.error(null, EcbEnum.COMMON_EXCEPTION_DEF.value, opMsg);
            }


            // 2). 根据请求报文的【合同编号loan_cont_no】、【身份证号certid】查询贷款合同CTR_LOAN_CONT验证该合同是否被其他借款人占用
            // 根据证件号码查询客户信息
            //通过客户证件号查询客户信息
            logger.info("***************XDHT0020*根据输入参数【{}】查询客户信息开始", JSON.toJSONString(xdcz0026DataReqDto.getCertid()));
            CusBaseClientDto cusBaseClientDto = Optional.ofNullable(iCusClientService.queryCusByCertCode(xdcz0026DataReqDto.getCertid())).orElse(new CusBaseClientDto());
            logger.info("***************XDHT0020*根据输入参数查询客户信息结果为【{}】", JSON.toJSONString(cusBaseClientDto));
            /********************************查询客户是否已存在合同信息,如果存在查询出合同信息*********************************************/
            CtrLoanCont ctrLoanCont = null;
            IqpLoanApp iqpLoanApp = null;
            LmtCrdReplyInfo lmtCrdReplyInfo = null;
            Map ctrLoanContMap = new HashMap<>();
            ctrLoanContMap.put("contNo", xdcz0026DataReqDto.getLoan_cont_no());//借款合同号
            ctrLoanContMap.put("cusId", cusBaseClientDto.getCusId());//客户号
            int ctrLoanContNum = ctrLoanContService.countCtrLoanContByMap(ctrLoanContMap);
            if (ctrLoanContNum == 0) {//该客户没有此合同信息
                Map ctrLoanMap = new HashMap<>();
                ctrLoanMap.put("contNo", xdcz0026DataReqDto.getLoan_cont_no());//借款合同号
                int ctrLoanContCont = ctrLoanContService.countCtrLoanContByMap(ctrLoanMap);
                if (ctrLoanContCont > 0) {//改合同号已被其他客户占用
                    opFlag = DscmsBizCzEnum.OP_FLAG_F.key;
                    opMsg = EcbEnum.ECB010045.value + xdcz0026DataReqDto.getLoan_cont_no();// 合同号[{}]已被占用
                    logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, opMsg);
                    xdcz0026DataRespDto.setOpFlag(opFlag);// 操作标志
                    xdcz0026DataRespDto.setOpMsg(opMsg);// 描述信息
                    throw BizException.error(null, EcbEnum.ECB010045.key, opMsg);
                }
            } else {
                ctrLoanCont = ctrLoanContService.selectContByContno(xdcz0026DataReqDto.getLoan_cont_no());
                lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectBySurveySerno(ctrLoanCont.getSurveySerno());
                iqpLoanApp = new IqpLoanApp();
                cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(ctrLoanCont, iqpLoanApp);
            }

            //3).插入批复表
            // 查询产品信息
            String prdId = translateFengKongBizType(xdcz0026DataReqDto.getBiz_type());//根据biz_type转prd_id
            CfgPrdBasicinfoDto cfgPrdBasicinfoDto = null;
            ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
            if (Objects.isNull(cfgPrdBasicinfoDtoResultDto) || Objects.isNull(cfgPrdBasicinfoDtoResultDto.getData())) {
                throw BizException.error(null, "9999", "未查询到产品信息！");
            } else {
                cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoResultDto.getData();
            }
            //获取产品条线 1-小贷
            String prdBelgLine = cfgPrdBasicinfoDto.getPrdBelgLine();
            // 生成批复信息
            if (lmtCrdReplyInfo == null && ("01".equals(prdBelgLine) || "02".equals(prdBelgLine) || "05".equals(prdBelgLine))) {
                lmtCrdReplyInfo = buildLmtCrdReply(xdcz0026DataReqDto, cfgPrdBasicinfoDto, cusBaseClientDto);//批复表
                lmtCrdReplyInfoMapper.insertSelective(lmtCrdReplyInfo);
            }

            // 4).插入贷款申请表iqp_loan_app
            if (iqpLoanApp == null) {
                iqpLoanApp = buildIqpLoanApp(xdcz0026DataReqDto, cfgPrdBasicinfoDto, cusBaseClientDto);//合同申请信息表
                if (lmtCrdReplyInfo != null) {
                    iqpLoanApp.setReplyNo(lmtCrdReplyInfo.getReplySerno());
                    iqpLoanApp.setSurveySerno(lmtCrdReplyInfo.getSurveySerno());
                    iqpLoanApp.setLmtAccNo(lmtCrdReplyInfo.getReplySerno());
                }
                iqpLoanAppService.insertSelective(iqpLoanApp);
            }


            // 5).插入贷款合同表ctr_loan_cont
            if (ctrLoanCont == null) {
                ctrLoanCont = buildCtrLoanCont(xdcz0026DataReqDto, iqpLoanApp, cusBaseClientDto);//合同主表
                ctrLoanContService.insertSelective(ctrLoanCont);
            } else {//合同已经存在
                exists = true;
            }

            // 6).插入出账申请表PVP_LOAN_APP
            PvpLoanApp pvpLoanApp = buildPvpLoanApp(xdcz0026DataReqDto, ctrLoanCont, cusBaseClientDto, prdBelgLine);// 插入出账申请表
            pvpLoanAppService.insertSelective(pvpLoanApp);


            // 7).出账授权表pvp_authorize
            PvpAuthorize pvpAuthorize = buildPvpAuthorize(xdcz0026DataReqDto, pvpLoanApp, cusBaseClientDto);//出账授权表
            pvpAuthorizeService.insertSelective(pvpAuthorize);


            // 8).贷款台账表acc_loan
            AccLoan accLoan = buildAccLoan(xdcz0026DataReqDto, pvpLoanApp, cusBaseClientDto);//贷款台账表
            accLoanService.insertSelective(accLoan);

            // 9).交易对手账户明细 topp_acct_sub
            if ("1".equals(xdcz0026DataReqDto.getZhifu_type())) {//2	自主支付 1	受托支付
                ToppAcctSub toppAcctSub = buildToppAcctSub(xdcz0026DataReqDto, pvpLoanApp);
                toppAcctSubService.insertSelective(toppAcctSub);
            }

            // 10).优惠券处理信息
            this.saveDiscountCoupon(xdcz0026DataReqDto, accLoan, ctrLoanCont);

            /***************************************放款前进行发台账校验*********************************************/
            if ("01".equals(ctrLoanCont.getBelgLine()) || "02".equals(ctrLoanCont.getBelgLine()) || "03".equals(ctrLoanCont.getBelgLine()) || "05".equals(ctrLoanCont.getBelgLine())) {//网金产品
                if (exists) {//已存在存量合同信息
                    logger.info("************XDCZ0021*放款成功后进行台账校验开始,校验信息【{}】", JSON.toJSONString(pvpLoanApp));
                    CmisLmt0010RespDto cmisLmt0010RespDto = cmisBizXwCommonService.sendCmisLmt0010(pvpLoanApp);
                    //CmisLmt0010RespDto cmisLmt0010RespDto = resultLmt0010Dto.getData();
                    logger.info("************XDCZ0021*放款成功后进行台账校验结束,返回信息【{}】", JSON.toJSONString(cmisLmt0010RespDto));
                    //台账占用接口
                    logger.info("************XDCZ0021*放款成功后进行台账占用开始,校验信息【{}】", JSON.toJSONString(pvpLoanApp));
                    CmisLmt0013RespDto cmisLmt0013RespDto = cmisBizXwCommonService.sendCmisLmt0013(pvpLoanApp);
                    // CmisLmt0013RespDto cmisLmt0013RespDto = resultLmt0130Dto.getData();
                    logger.info("************XDCZ0021*放款成功后进行台账占用结束,返回信息【{}】", JSON.toJSONString(cmisLmt0013RespDto));
                }
            }

            // 调用核心ln3020接口
            ResultDto resultDto = pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);
            if ("9999".equals(resultDto.getCode())) {
                xdcz0026DataRespDto.setOpFlag(DscmsBizCzEnum.OP_FLAG_F.key);// 操作标志
                xdcz0026DataRespDto.setOpMsg(resultDto.getMessage());// 描述信息
            } else {
                // 返回信息
                xdcz0026DataRespDto.setOpFlag(DscmsBizCzEnum.OP_FLAG_S.key);// 操作标志
                xdcz0026DataRespDto.setOpMsg(DscmsBizCzEnum.OP_FLAG_S.value);// 描述信息

                /***************************************额度占用*********************************************/
                if ("01".equals(ctrLoanCont.getBelgLine()) || "02".equals(ctrLoanCont.getBelgLine()) || "03".equals(ctrLoanCont.getBelgLine()) || "05".equals(ctrLoanCont.getBelgLine())) {//网金产品
                    if (exists) {//已存在存量合同信息
                    } else {//首次申请提款
                        logger.info("************XDCZ0021*放款成功后线上产品自动生成合同台账开始,校验合同信息【{}】出账信息【{}】", JSON.toJSONString(ctrLoanCont), JSON.toJSONString(pvpLoanApp));
                        CmisLmt0057RespDto cmisLmt0057RespDto = cmisBizXwCommonService.sendCmisLmt0057(ctrLoanCont, pvpLoanApp, lmtCrdReplyInfo);
                        // CmisLmt0057RespDto cmisLmt0057RespDto = resultLmt0570Dto.getData();
                        logger.info("************XDCZ0021*放款成功后线上产品自动生成合同台账结束,返回信息【{}】", JSON.toJSONString(cmisLmt0057RespDto));
                    }
                }
            }

            // 仅对于房抵e点贷，房抵e点贷产品线上提款信贷系统自动出账后，影像补扫中生成一条待发起的影像补扫记录； P034 房抵e点贷
            if (Objects.equals("P034", ctrLoanCont.getPrdTypeProp())) {
                logger.info("房抵e点贷线上提款生成一条待发起的影像补扫记录开始,业务流水号【{}】", pvpLoanApp.getPvpSerno());
                pvpLoanAppService.sxkdOnlinecreateImageSpplInfo(pvpLoanApp.getPvpSerno());
                logger.info("房抵e点贷线上提款生成一条待发起的影像补扫记录结束,业务流水号【{}】", pvpLoanApp.getPvpSerno());
            }

        } catch (BizException e) {
            xdcz0026DataRespDto.setOpFlag(opFlag);// 操作标志
            xdcz0026DataRespDto.setOpMsg(opMsg);// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            xdcz0026DataRespDto.setOpFlag(opFlag);// 操作标志
            xdcz0026DataRespDto.setOpMsg(opMsg);// 描述信息
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026DataRespDto));
        return xdcz0026DataRespDto;
    }

    /**
     * @param xdcz0026DataReqDto,accLoan
     * @return void
     * @author 王玉坤
     * @date 2021/10/8 22:23
     * @version 1.0.0
     * @desc 保存优惠券信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void saveDiscountCoupon(Xdcz0026DataReqDto xdcz0026DataReqDto, AccLoan accLoan, CtrLoanCont ctrLoanCont) {

        // 是否免息 1:是、2:否
        String is_free_intere = xdcz0026DataReqDto.getIs_free_intere();
        // 优惠券编号
        String discount_code = xdcz0026DataReqDto.getDiscount_code();
        // 客户证件号
        String certCode = xdcz0026DataReqDto.getCertid();
        // 保存免息信息
        if (Objects.equals("1", is_free_intere)) {
            // 免息天数
            int free_intere_days = xdcz0026DataReqDto.getFree_intere_days();
            cmisBizXwCommonService.saveAccLoanFreeInter(accLoan.getBillNo(), is_free_intere, free_intere_days);
        }
        // 保存优惠券信息
        if (!StringUtils.isEmpty(discount_code)) {
            cmisBizXwCommonService.saveDiscountCoupon(discount_code, accLoan.getBillNo(), "02");
            // 核销优惠券信息
            if ("022100".equals(accLoan.getPrdId())) {
                // 大家e贷核销优惠券信息
                cmisBizXwCommonService.cancelAfterVerificaDiscount(discount_code, accLoan.getBillNo(), "2",
                        accLoan.getCusId(), ctrLoanCont.getPhone());
            } else {
                cmisBizXwCommonService.cancelAfterVerificaDiscountCoupon(discount_code, accLoan.getBillNo(), "2",
                        accLoan.getCusId());
            }
        }
    }

    /**
     * 将[]到 [受托账号修改申请表]
     * TODO 待坤经理确认  StringUtils.EMPTY 和 null 值逻辑
     *
     * @return
     */
    private IqpChgTrupayAcctApp buildIqpChgTrupayAcctApp(Xdcz0026DataReqDto xdcz0026DataReqDto, CusBaseClientDto cusBaseClientDto) {
        IqpChgTrupayAcctApp iqpChgTrupayAcctApp = new IqpChgTrupayAcctApp();
        iqpChgTrupayAcctApp.setPkId(StringUtils.EMPTY);//主键
        iqpChgTrupayAcctApp.setSerno(StringUtils.EMPTY);//业务流水号
        iqpChgTrupayAcctApp.setContNo(xdcz0026DataReqDto.getLoan_cont_no());//合同编号
        iqpChgTrupayAcctApp.setBillNo(xdcz0026DataReqDto.getBill_no());//借据编号
        iqpChgTrupayAcctApp.setCusId(cusBaseClientDto.getCusId());//客户编号
        iqpChgTrupayAcctApp.setCusName(cusBaseClientDto.getCusName());//客户名称
        iqpChgTrupayAcctApp.setToppAccno(xdcz0026DataReqDto.getShoutuo_account());//交易对手账户
        iqpChgTrupayAcctApp.setToppName(xdcz0026DataReqDto.getShoutuo_name());//交易对手名称
        iqpChgTrupayAcctApp.setToppAmt(NumberUtils.toBigDecimal(xdcz0026DataReqDto.getApply_amount()));//交易对手金额
        iqpChgTrupayAcctApp.setToppBankNo(xdcz0026DataReqDto.getKhh_no());//交易对手开户行号
        iqpChgTrupayAcctApp.setToppBankName(xdcz0026DataReqDto.getKhh_name());//交易对手开户行名称
        iqpChgTrupayAcctApp.setOrigiToppAccno(StringUtils.EMPTY);//原交易对手账户
        iqpChgTrupayAcctApp.setOrigiToppName(StringUtils.EMPTY);//原交易对手名称
        iqpChgTrupayAcctApp.setOrigiToppAmt(null);//原交易对手金额
        iqpChgTrupayAcctApp.setOrigiToppBankno(StringUtils.EMPTY);//原交易对手开户行号
        iqpChgTrupayAcctApp.setOrigiToppBankname(StringUtils.EMPTY);//原交易对手开户行名称
        iqpChgTrupayAcctApp.setIsOwner(StringUtils.EMPTY);//是否客户修改
        iqpChgTrupayAcctApp.setHxSerno(StringUtils.EMPTY);//核心流水号
        iqpChgTrupayAcctApp.setApproveStatus(StringUtils.EMPTY);//审批状态
        iqpChgTrupayAcctApp.setAuthStatus(StringUtils.EMPTY);//授权状态
        iqpChgTrupayAcctApp.setInputId(xdcz0026DataReqDto.getManagerId());//登记人
        iqpChgTrupayAcctApp.setInputBrId(xdcz0026DataReqDto.getManager_br_id());//登记机构
        iqpChgTrupayAcctApp.setInputDate(DateUtils.getCurrDateStr());//登记日期
        iqpChgTrupayAcctApp.setUpdId(xdcz0026DataReqDto.getManagerId());//最近修改人
        iqpChgTrupayAcctApp.setUpdBrId(xdcz0026DataReqDto.getManager_br_id());//最近修改机构
        iqpChgTrupayAcctApp.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
        iqpChgTrupayAcctApp.setManagerId(xdcz0026DataReqDto.getManagerId());//主管客户经理
        iqpChgTrupayAcctApp.setManagerBrId(xdcz0026DataReqDto.getManager_br_id());//主管机构
        iqpChgTrupayAcctApp.setCreateTime(DateUtils.getCurrDate());//创建时间
        iqpChgTrupayAcctApp.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间
        return iqpChgTrupayAcctApp;
    }

    /**
     * @param xdcz0026DataReqDto, pvpLoanApp, cusBaseClientDto
     * @return cn.com.yusys.yusp.domain.AccLoan
     * @author 王玉坤
     * @date 2021/10/12 15:05
     * @version 1.0.0
     * @desc 生成台账信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private AccLoan buildAccLoan(Xdcz0026DataReqDto xdcz0026DataReqDto, PvpLoanApp pvpLoanApp, CusBaseClientDto cusBaseClientDto) {
        AccLoan accLoan = new AccLoan();
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(pvpLoanApp, accLoan);
        //未出帐6
        accLoan.setAccStatus("6");
        Map map = pvpLoanAppService.getFiveAndTenClass(pvpLoanApp.getCusId());
        //五级分类默认正常
        accLoan.setFiveClass((String) map.get("fiveClass"));
        //出账金额
        accLoan.setLoanAmt(pvpLoanApp.getPvpAmt());
        //币种
        accLoan.setContCurType(pvpLoanApp.getCurType());
        //基准利率  rulingIr   取 LPR利率
        accLoan.setRulingIr(pvpLoanApp.getCurtLprRate());
        // 折合人民币金额
        accLoan.setExchangeRmbAmt(pvpLoanApp.getCvtCnyAmt());
        // 折合人民币余额
        accLoan.setExchangeRmbBal(pvpLoanApp.getCvtCnyAmt());
        //分类日期
        accLoan.setClassDate(pvpLoanApp.getLoanStartDate());
        //原到期日期
        accLoan.setOrigExpiDate(pvpLoanApp.getLoanEndDate());
        // 科目
        accLoan.setSubjectNo(pvpLoanApp.getLoanSubjectNo());
        // 科目名称
        accLoan.setSubjectName(pvpAuthorizeService.calHxkuaijilb(pvpLoanApp.getLoanSubjectNo()).getAccountClassName());

        return accLoan;
    }


    /**
     * 将 [请求Data：风控调用信贷放款]到 [出账授权表]
     * TODO 待坤经理确认  StringUtils.EMPTY 和 null 值逻辑
     *
     * @param xdcz0026DataReqDto
     * @return
     */
    private PvpAuthorize buildPvpAuthorize(Xdcz0026DataReqDto xdcz0026DataReqDto, PvpLoanApp pvpLoanApp, CusBaseClientDto cusBaseClientDto) {
        PvpAuthorize pvpAuthorize = new PvpAuthorize();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        //授权申请交易流水号
        //String tranSerNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_NO, new HashMap<>());
        Map seqMap = new HashMap();
        String tranSerNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PA_TRAN_SERNO, seqMap);
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(pvpLoanApp, pvpAuthorize);
        pvpAuthorize.setAuthStatus("0");
        String bizType = xdcz0026DataReqDto.getBiz_type();//获取biz_type
        String prdId = translateFengKongBizType(bizType);//根据biz_type转prd_id
        String repayment_account_tb = xdcz0026DataReqDto.getRepayment_account_tb();//替补还款账号
        // 如果满足1、022097--市民贷 022099--惠民贷 022100--大家e贷 2、替补还款账号不为空
        if (("022097".equals(prdId) || "022099".equals(prdId) || "022100".equals(prdId)) && StringUtil.isNotEmpty(repayment_account_tb)) {
            pvpAuthorize.setFldvalue04(repayment_account_tb);//替补还款账号
        }
        pvpAuthorize.setTranId("CRE400");//交易编号
        pvpAuthorize.setTranSerno(tranSerNo);//交易流水号
        pvpAuthorize.setContNo(xdcz0026DataReqDto.getLoan_cont_no());//合同号
        pvpAuthorize.setTranAmt(NumberUtils.toBigDecimal(xdcz0026DataReqDto.getLoan_amount()));//出账金额
        pvpAuthorize.setTranDate(openDay);
        return pvpAuthorize;
    }

    /**
     * 将 [请求Data：风控调用信贷放款]到 [出账申请表]
     * TODO 待坤经理确认  StringUtils.EMPTY 和 null 值逻辑
     *
     * @param xdcz0026DataReqDto
     * @return
     */
    private PvpLoanApp buildPvpLoanApp(Xdcz0026DataReqDto xdcz0026DataReqDto, CtrLoanCont ctrLoanCont, CusBaseClientDto cusBaseClientDto, String prdBelgLine) {
        PvpLoanApp pvpLoanApp = new PvpLoanApp();
        /**********涉农贷款投向、贷款类别细分映射********/
        Map<String, String> map = this.getLoanDetail(ctrLoanCont.getCusId(), xdcz0026DataReqDto);
        //生成流水号
        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(ctrLoanCont, pvpLoanApp);
        pvpLoanApp.setApproveStatus("997");//审批状态
        pvpLoanApp.setBillNo(xdcz0026DataReqDto.getBill_no());//借据编号
        pvpLoanApp.setPvpSerno(serno);//放款流水号
        pvpLoanApp.setPrdTypeProp(ctrLoanCont.getPrdTypeProp());// 产品类型属性
        pvpLoanApp.setContNo(xdcz0026DataReqDto.getLoan_cont_no());//合同编号
        pvpLoanApp.setCusId(cusBaseClientDto.getCusId());//客户编号
        pvpLoanApp.setCusName(cusBaseClientDto.getCusName());//客户名称
        pvpLoanApp.setPrdId(ctrLoanCont.getPrdId());//产品编号
        pvpLoanApp.setPrdName(ctrLoanCont.getPrdName());//产品名称
        pvpLoanApp.setGuarMode(ctrLoanCont.getGuarWay());//担保方式 主担保方式 零售产品及小微优享贷为信用类贷款 00--信用
        pvpLoanApp.setLoanModal("1");//贷款形式 1--新增
        pvpLoanApp.setContCurType("CNY");//贷款发放币种
        pvpLoanApp.setExchangeRate(new BigDecimal("1"));//汇率
        /******************************贷款出账金额***************************************/
        BigDecimal pvpAmt = Optional.ofNullable(new BigDecimal(xdcz0026DataReqDto.getLoan_amount())).orElse(new BigDecimal("0"));
        pvpLoanApp.setPvpAmt(pvpAmt);// 出账金额不能去合同金额，取本次的支用金额
        pvpLoanApp.setCvtCnyAmt(pvpAmt);//折合人民币金额
        pvpLoanApp.setLoanStartDate(xdcz0026DataReqDto.getLoan_start_date1());//贷款起始日期
        pvpLoanApp.setLoanEndDate(xdcz0026DataReqDto.getLoan_end_date1());//贷款到期日期
        /********************************根据起始日、到期日计算贷款期数开始********************************************/
        String loanStartDate = xdcz0026DataReqDto.getLoan_start_date1();
        String loanEndDate = xdcz0026DataReqDto.getLoan_end_date1();
        if (StringUtil.isNotEmpty(loanStartDate) && StringUtil.isNotEmpty(loanEndDate) && loanStartDate.contains("-")) {
            loanStartDate = loanStartDate.replaceAll("-", "");
            loanEndDate = loanEndDate.replaceAll("-", "");
        }
        int term = DateUtils.getMonthsByTwoDates(loanStartDate, "yyyyMMdd", loanEndDate, "yyyyMMdd");
        String targetDate = DateUtils.addMonth(loanStartDate, "yyyyMMdd", term);
        if (!loanEndDate.equals(targetDate)) {//不满一个月按一个月计算
            term++;
        }
        /********************************根据起始日、到期日计算贷款期数结束********************************************/
        pvpLoanApp.setLoanTerm(term + "");//贷款期限
        pvpLoanApp.setLoanTermUnit("M");//贷款期限单位
        pvpLoanApp.setRateAdjMode("01");//利率调整方式 信贷字典项 01--固定利率 02--浮动利率
        pvpLoanApp.setIsSegInterest("0");//是否分段计息
        /*****************************************执行利率(传递过来的是100%的形式)**************************************/
        String rate = xdcz0026DataReqDto.getRate();
        if (rate.contains("%")) {
            rate = rate.replace("%", "");
        } else if (StringUtil.isEmpty(rate)) {
            rate = "0";
        }
        BigDecimal rateBD = new BigDecimal(rate).divide(new BigDecimal("100"), 8, BigDecimal.ROUND_HALF_UP);
        String curLprRate = (String) iqpLoanAppService.getLprRate(new BigDecimal(term).compareTo(new BigDecimal("60")) > 0 ? "A2" : "A1").get("rate");
        pvpLoanApp.setCurtLprRate(new BigDecimal(curLprRate));//当前LPR利率
        pvpLoanApp.setIrFloatType("");//正常利率浮动方式
        //pvpLoanApp.setIrFloatRate(ctrLoanCont.getIrFloatRate());//利率浮动百分比 固定利率不需要
        pvpLoanApp.setRateFloatPoint(rateBD.subtract(new BigDecimal(curLprRate)).multiply(new BigDecimal("10000")));//浮动点数
        pvpLoanApp.setExecRateYear(rateBD);//执行年利率 风控利率形式 x%
        pvpLoanApp.setOverdueRatePefloat(new BigDecimal("0.5"));//逾期利率浮动比
        pvpLoanApp.setOverdueExecRate(rateBD.multiply(new BigDecimal("1.5")));//逾期年利率 风控利率形式 x%
        pvpLoanApp.setCiRatePefloat(new BigDecimal("0.5"));//复息利率浮动比
        pvpLoanApp.setCiExecRate(rateBD.multiply(new BigDecimal("1.5")));//复息执行利率(年利率)
        pvpLoanApp.setRateAdjType(StringUtils.EMPTY);//利率调整选项
        pvpLoanApp.setNextRateAdjInterval(StringUtils.EMPTY);//下一次利率调整间隔
        pvpLoanApp.setNextRateAdjUnit(StringUtils.EMPTY);//下一次利率调整间隔单位
        pvpLoanApp.setFirstAdjDate(StringUtils.EMPTY);//第一次调整日
        pvpLoanApp.setRepayMode(xdcz0026DataReqDto.getLoan_paym_mtd());//还款方式
        pvpLoanApp.setEiIntervalCycle("1");//结息间隔周期
        pvpLoanApp.setEiIntervalUnit("M");//结息间隔周期单位
        pvpLoanApp.setDeductType("AUTO");//扣款方式
        String repayDate = ctrLoanCont.getRepayDate();
        if (StringUtil.isNotEmpty(repayDate)) {//合同扣款日不为空
            pvpLoanApp.setDeductDay(repayDate);//扣款日
        } else {
            pvpLoanApp.setDeductDay(queryRepayDate(prdBelgLine));//扣款日
        }
        String biz_type = xdcz0026DataReqDto.getBiz_type();
        String accno = xdcz0026DataReqDto.getRepayment_account();
        pvpLoanApp.setLoanPayoutAccno(accno);//贷款发放账号
        pvpLoanApp.setLoanPayoutSubNo("00001");//贷款发放账号子序号
        pvpLoanApp.setPayoutAcctName(ctrLoanCont.getCusName());//发放账号名称
        pvpLoanApp.setRepayAccno(accno);//贷款还款账号
        pvpLoanApp.setRepaySubAccno("00001");//贷款还款账户子序号
        pvpLoanApp.setRepayAcctName(ctrLoanCont.getCusName());//还款账户名称
        pvpLoanApp.setLoanTer(map.get("loanDirection"));//贷款投向
        pvpLoanApp.setLoanUseType(StringUtils.EMPTY);//借款用途类型
        pvpLoanApp.setLoanSubjectNo(xdcz0026DataReqDto.getAccount_class());
        pvpLoanApp.setAgriType(map.get("ariFlag"));// 农户类型
        pvpLoanApp.setAgriLoanTer(map.get("farmDirection"));// 涉农贷款投向
        pvpLoanApp.setLoanTypeDetail(map.get("loanTypeDetail"));// 贷款类别细分

        pvpLoanApp.setLoanPromiseFlag(StringUtils.EMPTY);//贷款承诺标志
        pvpLoanApp.setLoanPromiseType(StringUtils.EMPTY);//贷款承诺类型
        pvpLoanApp.setIsSbsy(StringUtils.EMPTY);//是否贴息
        pvpLoanApp.setSbsyDepAccno(StringUtils.EMPTY);//贴息人存款账号
        pvpLoanApp.setSbsyPerc(null);//贴息比例
        pvpLoanApp.setSbysEnddate(StringUtils.EMPTY);//贴息到期日
        pvpLoanApp.setIsUtilLmt("1");//是否使用授信额度
        pvpLoanApp.setLmtAccNo(ctrLoanCont.getLmtAccNo());//授信额度编号
        pvpLoanApp.setReplyNo(ctrLoanCont.getReplyNo());//批复编号
        pvpLoanApp.setSurveySerno(ctrLoanCont.getSurveySerno());
        pvpLoanApp.setIsPactLoan(StringUtils.EMPTY);//是否落实贷款
        pvpLoanApp.setIsGreenIndustry(StringUtils.EMPTY);//是否绿色产业
        pvpLoanApp.setIsOperPropertyLoan(StringUtils.EMPTY);//是否经营性物业贷款
        pvpLoanApp.setIsSteelLoan(StringUtils.EMPTY);//是否钢贸行业贷款
        pvpLoanApp.setIsStainlessLoan(StringUtils.EMPTY);//是否不锈钢行业贷款
        pvpLoanApp.setIsPovertyReliefLoan(StringUtils.EMPTY);//是否扶贫贴息贷款
        pvpLoanApp.setIsLaborIntenSbsyLoan(StringUtils.EMPTY);//是否劳动密集型小企业贴息贷款
        pvpLoanApp.setGoverSubszHouseLoan(StringUtils.EMPTY);//保障性安居工程贷款
        pvpLoanApp.setEngyEnviProteLoan(StringUtils.EMPTY);//项目贷款节能环保
        pvpLoanApp.setIsCphsRurDelpLoan(StringUtils.EMPTY);//是否农村综合开发贷款标志
        pvpLoanApp.setRealproLoan(StringUtils.EMPTY);//房地产贷款
        pvpLoanApp.setRealproLoanRate(null);//房产开发贷款资本金比例
        pvpLoanApp.setGuarDetailMode("12".equals(biz_type) ? "03" : "");//担保方式细分 03--抵押住房  20--房抵e点贷

        // 小贷机构获取
        String finaBrIdName = StringUtils.EMPTY;
        String disbOrgName = StringUtils.EMPTY;
        if ("01".equals(ctrLoanCont.getBelgLine())) {
            logger.info("处理小微放款机构信息开始！");
            CfgXdLoanOrgDto cfgXdLoanOrgDto = new CfgXdLoanOrgDto();
            cfgXdLoanOrgDto.setXdOrg(cusBaseClientDto.getManagerBrId());
            if (!Objects.isNull(cusBaseClientDto.getManagerBrId())) {
                logger.info("根据管护机构【{}】，查询到出账机构！", cusBaseClientDto.getManagerBrId());
                ResultDto<CfgXdLoanOrgDto> xdLoanOrgDtoResultDto = iCmisCfgClientService.queryCfgXdLoanOrg(cfgXdLoanOrgDto);
                if (!Objects.isNull(xdLoanOrgDtoResultDto) && !Objects.isNull(xdLoanOrgDtoResultDto.getData())) {
                    finaBrIdName = xdLoanOrgDtoResultDto.getData().getXdFinaOrgName();
                    disbOrgName = xdLoanOrgDtoResultDto.getData().getXdPvpOrgName();
                }
            }
        }

        pvpLoanApp.setFinaBrId(xdcz0026DataReqDto.getFina_br_id());//账务机构编号
        pvpLoanApp.setFinaBrIdName(finaBrIdName);// 账务机构名称
        pvpLoanApp.setDisbOrgNo(xdcz0026DataReqDto.getManager_br_id());//放款机构编号
        pvpLoanApp.setDisbOrgName(disbOrgName);// 放款机构名称
        pvpLoanApp.setLoanSubjectNo(xdcz0026DataReqDto.getAccount_class());
        pvpLoanApp.setBelgLine(ctrLoanCont.getBelgLine());//所属条线
        pvpLoanApp.setInputId(xdcz0026DataReqDto.getManagerId());//登记人
        pvpLoanApp.setInputBrId(xdcz0026DataReqDto.getManager_br_id());//登记机构
        pvpLoanApp.setInputDate(DateUtils.getCurrDateStr());//登记日期
        pvpLoanApp.setUpdId(xdcz0026DataReqDto.getManagerId());//最近修改人
        pvpLoanApp.setUpdBrId(xdcz0026DataReqDto.getManager_br_id());//最近修改机构
        pvpLoanApp.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
        pvpLoanApp.setManagerId(xdcz0026DataReqDto.getManagerId());//主管客户经理
        pvpLoanApp.setManagerBrId(xdcz0026DataReqDto.getManager_br_id());//主管机构
        pvpLoanApp.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
        pvpLoanApp.setCreateTime(DateUtils.getCurrDate());//创建时间
        pvpLoanApp.setUpdateTime(DateUtils.getCurrDate());//修改时间
        pvpLoanApp.setChnlSour("01");// 默认手机银行
        pvpLoanApp.setStartDate(xdcz0026DataReqDto.getLoan_start_date());//合同开始日期
        pvpLoanApp.setEndDate(xdcz0026DataReqDto.getLoan_end_date());//合同结束日期
        pvpLoanApp.setPvpMode("1");//自动出账
        pvpLoanApp.setPayMode("2".equals(xdcz0026DataReqDto.getZhifu_type()) ? "0" : "1");//支付方式 风控字典项 1--受托支付 2--自主支付  信贷字典项 0--自主支付 1--受托支付
        // 支付类型 2--自主支付 1--受托支付
        if ("1".equals(xdcz0026DataReqDto.getZhifu_type())) {
            pvpLoanApp.setIsBeEntrustedPay("1");
        } else {
            pvpLoanApp.setIsBeEntrustedPay("0");
        }
        // 房抵e点贷科目单独计算
        if ("12".equals(biz_type)) {
            CfgAccountClassChooseDto cfgAccountClassChooseDto = pvpAuthorizeService.getClassSubNoForOnline(pvpLoanApp, "022011");
            pvpLoanApp.setLoanSubjectNo(cfgAccountClassChooseDto.getAccountClass());
        } else{
            pvpLoanApp.setLoanSubjectNo(xdcz0026DataReqDto.getAccount_class());
        }
        return pvpLoanApp;
    }

    /**
     * @param xdcz0026DataReqDto, cfgPrdBasicinfoDto, cusBaseClientDto
     * @return cn.com.yusys.yusp.domain.IqpLoanApp
     * @author 王玉坤
     * @date 2021/10/12 14:45
     * @version 1.0.0
     * @desc 生成合同申请信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private IqpLoanApp buildIqpLoanApp(Xdcz0026DataReqDto xdcz0026DataReqDto, CfgPrdBasicinfoDto cfgPrdBasicinfoDto, CusBaseClientDto cusBaseClientDto) {
        logger.info("【{}】业务申请表IqpLoanApp赋值开始", xdcz0026DataReqDto.getLoan_cont_no());
        String prdBelgLine = cfgPrdBasicinfoDto.getPrdBelgLine();//产品条线
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, new HashMap<>());
        IqpLoanApp iqpLoanApp = new IqpLoanApp();
        iqpLoanApp.setIqpSerno(serno);//申请流水号
        iqpLoanApp.setContNo(xdcz0026DataReqDto.getLoan_cont_no());
        iqpLoanApp.setReplyNo(StringUtils.EMPTY);//批复编号
        iqpLoanApp.setSurveySerno(StringUtils.EMPTY);//调查编号
        iqpLoanApp.setLmtAccNo(StringUtils.EMPTY);// 额度分项编号
        iqpLoanApp.setOldIqpSerno(StringUtils.EMPTY);//原申请流水号
        iqpLoanApp.setIsReconsid(StringUtils.EMPTY);//是否复议
        iqpLoanApp.setCusId(cusBaseClientDto.getCusId());//客户编号
        iqpLoanApp.setCusName(cusBaseClientDto.getCusName());//客户名称
        iqpLoanApp.setCertCode(cusBaseClientDto.getCertCode());//证件号码
        iqpLoanApp.setPhone(StringUtils.EMPTY);//手机号码
        iqpLoanApp.setBizType(StringUtils.EMPTY);//业务类型
        iqpLoanApp.setEspecBizType(StringUtils.EMPTY);//特殊业务类型
        iqpLoanApp.setAppDate(openDay);//申请日期
        iqpLoanApp.setPrdId(cfgPrdBasicinfoDto.getPrdId());//产品编号
        iqpLoanApp.setPrdName(cfgPrdBasicinfoDto.getPrdName());//产品名称
        iqpLoanApp.setLoanPurp(StringUtils.EMPTY);//贷款用途
        iqpLoanApp.setLoanModal("1");//贷款形式 1--新增
        iqpLoanApp.setLoanCha("01");//贷款性质 01--自营贷款
        iqpLoanApp.setPreferRateFlag("0");//是否申请优惠利率 0--否
        iqpLoanApp.setLoanRateAdjDay(StringUtils.EMPTY);//借款利率调整日
        iqpLoanApp.setOtherComsumeRepayAmt(null);//其他消费贷款月还款额
        iqpLoanApp.setPundLoanMonRepayAmt(null);//本笔公积金贷款月还款额
        iqpLoanApp.setMonthRepayAmt(null);//本笔月还款额
        iqpLoanApp.setBeforehandInd("0");//是否先放款后抵押
        iqpLoanApp.setIsOlPld("0");//是否在线抵押 0--否
        iqpLoanApp.setContMode(StringUtils.EMPTY);//合同模式
        iqpLoanApp.setIsOnlineDraw(StringUtils.EMPTY);//是否线上提款
        iqpLoanApp.setIsOutstndTrdLmtAmt("0");//第三方标识
        iqpLoanApp.setProNo(StringUtils.EMPTY);//项目编号
        iqpLoanApp.setTdpAgrNo(StringUtils.EMPTY);//项目流水号
        iqpLoanApp.setInveAdvice(StringUtils.EMPTY);//调查人意见
        iqpLoanApp.setIndivCdtExpl(StringUtils.EMPTY);//个人信用情况其他说明
        iqpLoanApp.setContType("2");//合同类型 与老信贷保持一致
        iqpLoanApp.setCurType("CNY");//合同币种
        iqpLoanApp.setContAmt(NumberUtils.toBigDecimal(xdcz0026DataReqDto.getApply_amount()));//合同金额
        try {
            iqpLoanApp.setContTerm(cmisBizXwCommonService.getBetweenMonth(xdcz0026DataReqDto.getLoan_start_date(), xdcz0026DataReqDto.getLoan_end_date()));
        } catch (Exception e) {
        }
        iqpLoanApp.setStartDate(xdcz0026DataReqDto.getLoan_start_date());//合同起始日
        iqpLoanApp.setEndDate(xdcz0026DataReqDto.getLoan_end_date());//合同到期日
        iqpLoanApp.setTermType("M");//期限类型 M-- 月
        try {
            iqpLoanApp.setAppTerm(new BigDecimal(cmisBizXwCommonService.getBetweenMonth(xdcz0026DataReqDto.getLoan_start_date(), xdcz0026DataReqDto.getLoan_end_date())));
        } catch (Exception e) {
        }
        iqpLoanApp.setRulingIr(null);//基准利率（年）
        iqpLoanApp.setRulingIrM(null);//对应基准利率(月)
        iqpLoanApp.setSignMode(StringUtils.EMPTY);//签约方式
        iqpLoanApp.setLoanTer(xdcz0026DataReqDto.getLoan_direction());//贷款投向
        iqpLoanApp.setOtherAgreed(StringUtils.EMPTY);//其他约定
        iqpLoanApp.setRepayMode(xdcz0026DataReqDto.getLoan_paym_mtd());//还款方式
        iqpLoanApp.setRepayDate(queryRepayDate(prdBelgLine));//还款日
        iqpLoanApp.setLoanType(StringUtils.EMPTY);//借款种类
        iqpLoanApp.setEiMode(StringUtils.EMPTY);//结息方式
        iqpLoanApp.setLoanPayoutAccno(xdcz0026DataReqDto.getRepayment_account());//贷款发放账号
        iqpLoanApp.setLoanPayoutAccName(StringUtils.EMPTY);//贷款发放账号名称
        iqpLoanApp.setGraper(StringUtils.EMPTY);//宽限期
        iqpLoanApp.setContTotalAmt(null);//合同总价
        iqpLoanApp.setFirstpayAmt(null);//首付/定金
        iqpLoanApp.setIsHasRefused(StringUtils.EMPTY);//是否曾被拒绝
        iqpLoanApp.setGuarWay("022011".equals(cfgPrdBasicinfoDto.getPrdId()) ? "10" : "00");//主担保方式 零售产品及小微优享贷为信用类贷款 00--信用 022011(其他个人经营性贷款)为抵押，其余均为信用
        iqpLoanApp.setIsCommonRqstr(StringUtils.EMPTY);//是否共同申请人
        iqpLoanApp.setIsCfirmPayWay(StringUtils.EMPTY);//是否确认支付方式
        iqpLoanApp.setPayMode("2".equals(xdcz0026DataReqDto.getZhifu_type()) ? "0" : "1");//支付方式 风控字典项 1--受托支付 2--自主支付  信贷字典项 0--自主支付 1--受托支付
        iqpLoanApp.setAppCurType("CNY");//申请币种
        iqpLoanApp.setAppAmt(new BigDecimal(xdcz0026DataReqDto.getApply_amount()));//申请金额
        iqpLoanApp.setAppRate(BigDecimal.ONE);//申请汇率
        iqpLoanApp.setBailSour(StringUtils.EMPTY);//保证金来源
        iqpLoanApp.setBailPerc(null);//保证金比例
        iqpLoanApp.setBailCurType(StringUtils.EMPTY);//保证金币种
        iqpLoanApp.setBailAmt(null);//保证金金额
        iqpLoanApp.setBailExchangeRate(null);//保证金汇率
        iqpLoanApp.setRiskOpenAmt(null);//风险敞口金额
        iqpLoanApp.setIrAccordType(StringUtils.EMPTY);//利率依据方式
        iqpLoanApp.setIrType(StringUtils.EMPTY);//利率种类
        iqpLoanApp.setLoanRatType(StringUtils.EMPTY);//计息方式
        iqpLoanApp.setIrAdjustType(StringUtils.EMPTY);//利率调整方式
        iqpLoanApp.setIrAdjustTerm(null);//利率调整周期(月)
        iqpLoanApp.setPraType(StringUtils.EMPTY);//调息方式
        iqpLoanApp.setRateType(StringUtils.EMPTY);//利率形式
        iqpLoanApp.setIrFloatType(StringUtils.EMPTY);//利率浮动方式
        iqpLoanApp.setIrFloatRate(null);//利率浮动百分比
        /*********执行利率(传递过来的是10%的形式)**********/
        String rate = xdcz0026DataReqDto.getRate();
        if (rate.contains("%")) {
            rate = rate.replace("%", "");
        } else if (StringUtil.isEmpty(rate)) {
            rate = "0";
        }
        BigDecimal rateBD = new BigDecimal(rate).divide(new BigDecimal("100"), 8, BigDecimal.ROUND_HALF_UP);
        iqpLoanApp.setLprRateIntval(iqpLoanApp.getAppTerm().compareTo(new BigDecimal("60"))
                >= 0 ? "A2" : "A1");// 利率区间
        String curLprRate = (String) iqpLoanAppService.getLprRate(iqpLoanApp.getAppTerm().compareTo(new BigDecimal("60")) >= 0 ? "A2" : "A1").get("rate");
        iqpLoanApp.setCurtLprRate(new BigDecimal(curLprRate));// 当前LPR利率
        iqpLoanApp.setIrFloatType("00");// LPR加点
        iqpLoanApp.setIrAdjustType("01");// 01--固定利率
        iqpLoanApp.setExecRateYear(rateBD);// 执行年利率
        iqpLoanApp.setRateFloatPoint(new BigDecimal(rateBD.subtract(iqpLoanApp.getCurtLprRate()).multiply(new BigDecimal("10000")).intValue()));//浮动点数
        iqpLoanApp.setRealityIrM(null);//执行利率(月)
        iqpLoanApp.setOverdueRatePefloat(new BigDecimal("0.5"));//逾期利率浮动百分比
        iqpLoanApp.setOverdueExecRate(rateBD.multiply(new BigDecimal("1.5")));//逾期利率（年）
        iqpLoanApp.setDefaultRate(new BigDecimal("0.5"));//违约利率浮动百分比
        iqpLoanApp.setDefaultRateY(rateBD.multiply(new BigDecimal("1.5")));//违约利率（年）
        iqpLoanApp.setCiRatePefloat(new BigDecimal("0.5"));//复息利率浮动百分比
        iqpLoanApp.setCiExecRate(rateBD.multiply(new BigDecimal("1.5")));//复息利率（年）
        iqpLoanApp.setApproveStatus("997");//申请状态
        iqpLoanApp.setApprovePassDate(StringUtils.EMPTY);//审批通过日期（精确到秒）
        iqpLoanApp.setContHighAvlAmt(new BigDecimal(xdcz0026DataReqDto.getApply_amount()));//本合同项下最高可用信金额
        iqpLoanApp.setIsRenew(StringUtils.EMPTY);//是否续签
        iqpLoanApp.setOrigiContNo(StringUtils.EMPTY);//原合同编号
//            iqpLoanApp.setIsUseLmtAmt(StringUtils.EMPTY);//是否使用授信额度
        iqpLoanApp.setLmtAccNo(StringUtils.EMPTY);//授信额度编号
        iqpLoanApp.setIsESeal(StringUtils.EMPTY);//是否电子用印
        iqpLoanApp.setDebtLevel(StringUtils.EMPTY);//债项等级DEBT_GRADE
        iqpLoanApp.setEad(new BigDecimal("0"));//违约风险暴露EAD
        iqpLoanApp.setTargetCorp(StringUtils.EMPTY);//目标企业
        iqpLoanApp.setMergerAgr(StringUtils.EMPTY);//并购协议
        iqpLoanApp.setMergerTranAmt(null);//并购交易价款
        iqpLoanApp.setIsOutstndTrdLmtAmt("0");//是否占用第三方额度
        iqpLoanApp.setTdpAgrNo(StringUtils.EMPTY);//第三方合同协议编号
        iqpLoanApp.setCoopCusId(StringUtils.EMPTY);//合作方客户编号
        iqpLoanApp.setCoopCusName(StringUtils.EMPTY);//合作方客户名称
        iqpLoanApp.setIsSeajnt(StringUtils.EMPTY);//是否无缝对接
        iqpLoanApp.setCvtCnyAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));//折算人民币金额
        iqpLoanApp.setPurpAnaly(StringUtils.EMPTY);//用途分析
        iqpLoanApp.setCrossChkDetailAnaly(StringUtils.EMPTY);//交叉核验详细分析
        iqpLoanApp.setRepaySour(StringUtils.EMPTY);//还款来源
        iqpLoanApp.setInveConclu(StringUtils.EMPTY);//调查人结论
        iqpLoanApp.setChnlSour("01");// 默认手机银行
        iqpLoanApp.setOprType("01");//操作类型
        iqpLoanApp.setIsUtilLmt("1");
        iqpLoanApp.setBelgLine(cfgPrdBasicinfoDto.getPrdBelgLine());// 所属条线
        iqpLoanApp.setInputId(xdcz0026DataReqDto.getManagerId());//登记人
        iqpLoanApp.setInputBrId(xdcz0026DataReqDto.getManager_br_id());//登记机构
        iqpLoanApp.setInputDate(openDay);//登记日期
        iqpLoanApp.setUpdId(xdcz0026DataReqDto.getManagerId());//最近修改人
        iqpLoanApp.setUpdBrId(xdcz0026DataReqDto.getManager_br_id());//最近修改机构
        iqpLoanApp.setUpdDate(openDay);//最近修改日期
        iqpLoanApp.setManagerId(xdcz0026DataReqDto.getManagerId());//主管客户经理
        iqpLoanApp.setManagerBrId(xdcz0026DataReqDto.getManager_br_id());//主管机构
        iqpLoanApp.setUpdDate(openDay);//最后修改日期
        iqpLoanApp.setCreateTime(DateUtils.getCurrDate());//创建时间
        iqpLoanApp.setUpdateTime(DateUtils.getCurrTimestamp());//修改时间

        // 大家e贷、惠民贷特殊处理 022099--惠民贷  022100--大家e贷 修订为一般合同
        if (Objects.equals("022099", cfgPrdBasicinfoDto.getPrdId()) || Objects.equals("022100", cfgPrdBasicinfoDto.getPrdId())) {
            // 合同为一般合同 1--一般合同 2--最高额合同
            iqpLoanApp.setContType("1");
            // 合同金额 取贷款金额
            iqpLoanApp.setContAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));
            // 合同最高可用金额
            iqpLoanApp.setContHighAvlAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));
            // 折算人民币金额
            iqpLoanApp.setCvtCnyAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));
            // 合同期限
            try {
                iqpLoanApp.setContTerm(cmisBizXwCommonService.getBetweenMonth(xdcz0026DataReqDto.getLoan_start_date1(), xdcz0026DataReqDto.getLoan_end_date1()));
            } catch (Exception e) {
            }
            // 申请金额 取贷款金额
            iqpLoanApp.setAppAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));
            // 申请期限
            try {
                iqpLoanApp.setAppTerm(new BigDecimal(cmisBizXwCommonService.getBetweenMonth(xdcz0026DataReqDto.getLoan_start_date1(), xdcz0026DataReqDto.getLoan_end_date1())));
            } catch (Exception e) {
            }
            // 起始日期
            iqpLoanApp.setStartDate(xdcz0026DataReqDto.getLoan_start_date1());
            // 到期日期
            iqpLoanApp.setEndDate(xdcz0026DataReqDto.getLoan_end_date1());
        }
        logger.info("【{}】业务申请表IqpLoanApp赋值结束", xdcz0026DataReqDto.getLoan_cont_no());
        return iqpLoanApp;
    }

    /**
     * 将 [请求Data：风控调用信贷放款]到 [贷款合同表]
     * TODO 待坤经理确认  StringUtils.EMPTY 和 null 值逻辑
     *
     * @param xdcz0026DataReqDto
     * @return
     */
    private CtrLoanCont buildCtrLoanCont(Xdcz0026DataReqDto xdcz0026DataReqDto, IqpLoanApp iqpLoanApp, CusBaseClientDto cusBaseClientDto) {

        /*****查询客户的联系信息****/
        logger.info("****************根据输入参数【{}】查询客户的联系信息", JSON.toJSONString(cusBaseClientDto.getCusId()));
        CusIndivContactDto cusIndivContactDto = Optional.ofNullable(iCusClientService.queryCusIndivByCusId(cusBaseClientDto.getCusId())).orElse(new CusIndivContactDto());
        logger.info("****************根据输入参数【{}】查询客户的联系信息返回值", JSON.toJSONString(cusIndivContactDto));
        CtrLoanCont ctrLoanCont = new CtrLoanCont();
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(iqpLoanApp, ctrLoanCont);
        ctrLoanCont.setContNo(xdcz0026DataReqDto.getLoan_cont_no());//合同编号
        ctrLoanCont.setContCnNo(xdcz0026DataReqDto.getCn_cont_no());//中文合同编号
        ctrLoanCont.setApproveStatus("997");
        ctrLoanCont.setContAmt(iqpLoanApp.getAppAmt());
        ctrLoanCont.setAddr(iqpLoanApp.getDeliveryAddr());
        ctrLoanCont.setUpdateTime(DateUtils.getCurrDate());
        ctrLoanCont.setCreateTime(DateUtils.getCurrDate());
        ctrLoanCont.setCusId(cusBaseClientDto.getCusId());//客户编号
        ctrLoanCont.setCusName(cusBaseClientDto.getCusName());//客户名称
        ctrLoanCont.setCertType(cusBaseClientDto.getCertType());//证件类型
        ctrLoanCont.setCertCode(cusBaseClientDto.getCertCode());//证件号码
        //ctrLoanCont.setAddr(cusIndivContactDto.getSendAddr());//地址(区域码)
        ctrLoanCont.setAddr(cusIndivContactDto.getDeliveryStreet());//送达地址街道（中文地址）
        ctrLoanCont.setContStartDate(xdcz0026DataReqDto.getLoan_start_date());//合同起始日期
        ctrLoanCont.setContEndDate(xdcz0026DataReqDto.getLoan_end_date());//合同到期日期
        ctrLoanCont.setContAmt(NumberUtils.toBigDecimal(xdcz0026DataReqDto.getApply_amount()));//合同金额
        ctrLoanCont.setCvtCnyAmt(iqpLoanApp.getCvtCnyAmt());// 折算人民币金额
        ctrLoanCont.setTermType("M"); // 期限类型
        ctrLoanCont.setContTerm(iqpLoanApp.getAppTerm().intValue());//合同期限
        ctrLoanCont.setPaperContSignDate(xdcz0026DataReqDto.getLoan_start_date());//纸质合同签订日期

        ctrLoanCont.setBizType("02");//普通贷款合同
        ctrLoanCont.setPhone(cusIndivContactDto.getMobileNo());//手机号码
        ctrLoanCont.setLmtAccNo(iqpLoanApp.getLmtAccNo());// 授信额度编号
        ctrLoanCont.setLoanPurp(StringUtils.EMPTY);// 贷款用途
        ctrLoanCont.setLoanModal("1");// 贷款形式 1--新增
        ctrLoanCont.setLoanCha("01");// 贷款性质 01--自营贷款
        ctrLoanCont.setIsHasRefused(StringUtils.EMPTY);//  是否曾被拒绝
        ctrLoanCont.setGuarWay("022011".equals(iqpLoanApp.getPrdId()) ? "10" : "00");// 主担保方式
        ctrLoanCont.setIsCommonRqstr(StringUtils.EMPTY);// 是否共同申请人
        ctrLoanCont.setIsCfirmPayWay(StringUtils.EMPTY);// 是否确认支付方式
        ctrLoanCont.setCurType("CNY");// 币种

        ctrLoanCont.setContType("2");//  合同类型
        ctrLoanCont.setContStatus("200");// 合同状态 200-生效
        ctrLoanCont.setContBalance(new BigDecimal(xdcz0026DataReqDto.getApply_amount()));// 合同余额
        ctrLoanCont.setContRate(BigDecimal.ONE);// 合同汇率
        ctrLoanCont.setCurtLprRate(iqpLoanApp.getCurtLprRate());// 当前LPR利率
        ctrLoanCont.setIrFloatType(iqpLoanApp.getIrFloatType());// LPR加点
        ctrLoanCont.setIrAdjustType("01");// 01--固定利率
        ctrLoanCont.setExecRateYear(iqpLoanApp.getExecRateYear());// 执行年利率
        ctrLoanCont.setRateFloatPoint(iqpLoanApp.getRateFloatPoint());//浮动点数
        ctrLoanCont.setOverdueRatePefloat(iqpLoanApp.getOverdueRatePefloat());//逾期利率浮动百分比
        ctrLoanCont.setOverdueExecRate(iqpLoanApp.getOverdueExecRate());//逾期利率（年）
        ctrLoanCont.setDefaultRate(iqpLoanApp.getDefaultRate());//违约利率浮动百分比
        ctrLoanCont.setDefaultRateY(iqpLoanApp.getDefaultRateY());//违约利率（年）
        ctrLoanCont.setCiRatePefloat(iqpLoanApp.getCiRatePefloat());//复息利率浮动百分比
        ctrLoanCont.setCiExecRate(iqpLoanApp.getCiExecRate());//复息利率（年）
        ctrLoanCont.setRealityIrM(ctrLoanCont.getExecRateYear().divide(new BigDecimal("12"), RoundingMode.HALF_EVEN).setScale(9));//执行利率(月)
        ctrLoanCont.setEiMode(StringUtils.EMPTY);// 结息方式
        ctrLoanCont.setEiModeExpl(StringUtils.EMPTY);// 结息具体说明
        ctrLoanCont.setFax(cusIndivContactDto.getFaxCode());// 传真
        ctrLoanCont.setLinkman(cusBaseClientDto.getCusName());//联系人
        ctrLoanCont.setEmail(cusIndivContactDto.getEmail());// 邮箱
        ctrLoanCont.setQq(cusIndivContactDto.getQq());//  QQ
        ctrLoanCont.setWechat(cusIndivContactDto.getMobileNo());//  微信
        ctrLoanCont.setOtherPhone(StringUtils.EMPTY);//  其他通讯方式及帐号
        ctrLoanCont.setSignMode(StringUtils.EMPTY);//签订方式
        ctrLoanCont.setSignChannel(StringUtils.EMPTY);//签订渠道
        ctrLoanCont.setBailExchangeRate(null);//保证金汇率
        ctrLoanCont.setBailCurType(StringUtils.EMPTY);//保证金币种
        ctrLoanCont.setBailAmt(null);//保证金金额
        ctrLoanCont.setCvtCnyAmt(iqpLoanApp.getAppAmt());//折算人民币金额
        ctrLoanCont.setRulingIr(null);//基准利率（年）
        ctrLoanCont.setRulingIrM(null);//对应基准利率(月)
        ctrLoanCont.setIrAdjustTerm(null);//利率调整周期(月)
        ctrLoanCont.setIrFloatType(StringUtils.EMPTY);//正常利率浮动方式
        ctrLoanCont.setIrFloatRate(null);//利率浮动百分比
        ctrLoanCont.setRiskOpenAmt(null);//风险敞口金额
        ctrLoanCont.setRepayMode(StringUtils.EMPTY);//还款方式
        ctrLoanCont.setRepayDate(iqpLoanApp.getRepayDate());//还款日
        ctrLoanCont.setRepayFreType(StringUtils.EMPTY);//还款频率类型
        ctrLoanCont.setRepayFre(StringUtils.EMPTY);//本息还款频率
        ctrLoanCont.setLiquFreeTime(null);//提前还款违约金免除时间(月)
        ctrLoanCont.setReserveTerm(null);//保留期限
        ctrLoanCont.setCalTerm(null);//计算期限
        ctrLoanCont.setReserveAmt(null);//保留金额
        ctrLoanCont.setRepayTermOne(null);//第一阶段还款期数
        ctrLoanCont.setRepayAmtOne(null);//第一阶段还款本金
        ctrLoanCont.setRepayTermTwo(null);//第二阶段还款期数
        ctrLoanCont.setRepayAmtTwo(null);//第二阶段还款本金
        ctrLoanCont.setSbsyPerc(null);//贴息比例
        ctrLoanCont.setSbsyAmt(null);//贴息金额
        ctrLoanCont.setSbsyUnitName(StringUtils.EMPTY);//贴息单位名称
        ctrLoanCont.setSbsyAcct(StringUtils.EMPTY);//贴息方账户
        ctrLoanCont.setSbsyAcctName(StringUtils.EMPTY);//贴息方账户户名
        ctrLoanCont.setFiveClass(StringUtils.EMPTY);//五级分类
        ctrLoanCont.setLoanTer(xdcz0026DataReqDto.getLoan_direction());//贷款投向
        ctrLoanCont.setComUpIndtify(StringUtils.EMPTY);//工业转型升级标识
        ctrLoanCont.setStrategyNewLoan(StringUtils.EMPTY);//战略新兴产业类型
        ctrLoanCont.setIsCulEstate(StringUtils.EMPTY);//是否文化产业
        ctrLoanCont.setLoanType(StringUtils.EMPTY);//借款种类
        ctrLoanCont.setEstateAdjustType(StringUtils.EMPTY);//产业结构调整类型
        ctrLoanCont.setIsAuthorize(StringUtils.EMPTY);//是否委托人办理
        ctrLoanCont.setAuthedName(StringUtils.EMPTY);//委托人姓名
        ctrLoanCont.setConsignorCertType(StringUtils.EMPTY);//委托人证件类型
        ctrLoanCont.setConsignorCertCode(StringUtils.EMPTY);//委托人证件号
        ctrLoanCont.setAuthedTelNo(StringUtils.EMPTY);//委托人联系方式
        ctrLoanCont.setSignDate(StringUtils.EMPTY);//签订日期
        ctrLoanCont.setLogoutDate(StringUtils.EMPTY);//注销日期
        ctrLoanCont.setCourtAddr(StringUtils.EMPTY);//法院所在地
        ctrLoanCont.setArbitrateBch(StringUtils.EMPTY);//仲裁委员会
        ctrLoanCont.setArbitrateAddr(StringUtils.EMPTY);//仲裁委员会地点
        ctrLoanCont.setOtherOpt(StringUtils.EMPTY);//其他方式
        ctrLoanCont.setContQnt(null);//合同份数
        ctrLoanCont.setLgd(BigDecimal.ZERO);
        ctrLoanCont.setPundContNo(StringUtils.EMPTY);//公积金贷款合同编号
        ctrLoanCont.setSpplClause(StringUtils.EMPTY);//补充条款
        ctrLoanCont.setSignAddr(StringUtils.EMPTY);//签约地点
        ctrLoanCont.setBusiNetwork(StringUtils.EMPTY);//营业网点
        ctrLoanCont.setMainBusiPalce(StringUtils.EMPTY);//主要营业场所地址
        ctrLoanCont.setContTemplate(StringUtils.EMPTY);//合同模板
        ctrLoanCont.setContPrintNum(null);//合同打印次数
        ctrLoanCont.setSignApproveStatus(StringUtils.EMPTY);//签章审批状态
        ctrLoanCont.setTeam(StringUtils.EMPTY);//所属团队
        ctrLoanCont.setOtherAgreed(StringUtils.EMPTY);//其他约定
        ctrLoanCont.setAcctsvcrName(StringUtils.EMPTY);//开户行名称
        ctrLoanCont.setLoanPayoutAccno(StringUtils.EMPTY);//贷款发放账号
        ctrLoanCont.setLoanPayoutAccName(StringUtils.EMPTY);//贷款发放账号名称
        ctrLoanCont.setContMode(StringUtils.EMPTY);//合同模式
        ctrLoanCont.setIsOnlineDraw(StringUtils.EMPTY);//是否线上提款
        ctrLoanCont.setCtrBeginFlag(StringUtils.EMPTY);//线上合同启用标识
        ctrLoanCont.setApproveStatus("997");//申请状态
        ctrLoanCont.setHighAvlAmt(new BigDecimal(xdcz0026DataReqDto.getApply_amount()));//本合同项下最高可用信金额
        ctrLoanCont.setIsOlPld("");// 是否在线抵押
        ctrLoanCont.setBeforehandInd("0");//是否先放款后抵押
        ctrLoanCont.setIsESeal("0");//是否电子用印
        ctrLoanCont.setDoubleRecordNo(StringUtils.EMPTY);//双录编号
        ctrLoanCont.setDrawMode(StringUtils.EMPTY);//提款方式
        ctrLoanCont.setBelgLine(iqpLoanApp.getBelgLine());//所属条线
        ctrLoanCont.setOprType("01");//操作类型
        ctrLoanCont.setChnlSour("01");// 默认手机银行
        ctrLoanCont.setIsUtilLmt("1");
        ctrLoanCont.setPayMode("2".equals(xdcz0026DataReqDto.getZhifu_type()) ? "0" : "1");//支付方式 风控字典项 1--受托支付 2--自主支付  信贷字典项 0--自主支付 1--受托支付

        // 大家e贷、惠民贷特殊处理 022099--惠民贷  022100--大家e贷 修订为一般合同
        if (Objects.equals("022099", iqpLoanApp.getPrdId()) || Objects.equals("022100", iqpLoanApp.getPrdId())) {
            // 合同为一般合同 1--一般合同 2--最高额合同
            ctrLoanCont.setContType("1");
            // 合同金额 取贷款金额
            ctrLoanCont.setContAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));
            // 合同最高可用金额
            ctrLoanCont.setHighAvlAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));
            // 合同余额
            ctrLoanCont.setContBalance(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));
            try {
                logger.info("**********计算日期相差月份=合同期限");
                // 合同期限
                ctrLoanCont.setContTerm(cmisBizXwCommonService.getBetweenMonth(xdcz0026DataReqDto.getLoan_start_date1(), xdcz0026DataReqDto.getLoan_end_date1()));
            } catch (Exception e) {
            }
            // 起始日期
            ctrLoanCont.setContStartDate(xdcz0026DataReqDto.getLoan_start_date1());
            // 到期日期
            ctrLoanCont.setContEndDate(xdcz0026DataReqDto.getLoan_end_date1());
        }
        return ctrLoanCont;
    }

    /**
     * @param xdcz0026DataReqDto, cfgPrdBasicinfoDto, cusBaseClientDto
     * @return cn.com.yusys.yusp.domain.LmtCrdReplyInfo
     * @author 王玉坤
     * @date 2021/10/12 14:40
     * @version 1.0.0
     * @desc 生成批复信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private LmtCrdReplyInfo buildLmtCrdReply(Xdcz0026DataReqDto xdcz0026DataReqDto, CfgPrdBasicinfoDto cfgPrdBasicinfoDto, CusBaseClientDto cusBaseClientDto) {
        LmtCrdReplyInfo lmtCrdReplyInfo = new LmtCrdReplyInfo();
        String crdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_XD_SERNO, new HashMap<>());
        //插入授信调查批复表
        lmtCrdReplyInfo.setReplySerno(crdSerno);//主键
        lmtCrdReplyInfo.setSurveySerno(xdcz0026DataReqDto.getLoan_cont_no());
        lmtCrdReplyInfo.setCusId(cusBaseClientDto.getCusId());
        lmtCrdReplyInfo.setCusName(cusBaseClientDto.getCusName());//客户姓名
        lmtCrdReplyInfo.setCertCode(cusBaseClientDto.getCertCode());//证件号
        lmtCrdReplyInfo.setCusLvl("");//客户等级
        lmtCrdReplyInfo.setGuarMode("022011".equals(cfgPrdBasicinfoDto.getPrdId()) ? "10" : "00");//担保方式
        lmtCrdReplyInfo.setReplyAmt(new BigDecimal(xdcz0026DataReqDto.getApply_amount()));//批复金额
        try {
            logger.info("**********计算日期相差月份=批复表期限");
            // 批复表期限
            lmtCrdReplyInfo.setAppTerm(cmisBizXwCommonService.getBetweenMonth(xdcz0026DataReqDto.getLoan_start_date(), xdcz0026DataReqDto.getLoan_end_date()));
        } catch (Exception e) {
        }
        lmtCrdReplyInfo.setReplyStartDate(xdcz0026DataReqDto.getLoan_start_date());
        lmtCrdReplyInfo.setReplyEndDate(xdcz0026DataReqDto.getLoan_end_date());
        /************************利率处理********************************/
        String rate = xdcz0026DataReqDto.getRate();
        if (org.apache.commons.lang.StringUtils.isNotBlank(rate) && rate.contains("%")) {
            rate = rate.replaceAll("%", org.apache.commons.lang.StringUtils.EMPTY);
            lmtCrdReplyInfo.setExecRateYear(new BigDecimal(rate).divide(new BigDecimal("1"), 9, BigDecimal.ROUND_HALF_UP));
        }
        /************************利率处理结束********************************/
        String certType = cusBaseClientDto.getCertType();
        lmtCrdReplyInfo.setCertType(certType);
        lmtCrdReplyInfo.setRepayMode(xdcz0026DataReqDto.getLoan_paym_mtd());
        // 额度类型 01--临时额度 02--循环额度
        lmtCrdReplyInfo.setLimitType("02");
        lmtCrdReplyInfo.setIsBeEntrustedPay("0");// 是否受托支付
        lmtCrdReplyInfo.setLoanCondFlg("0");//是否有用信条件
        lmtCrdReplyInfo.setTruPayType("");//受托类型
        lmtCrdReplyInfo.setApprType("");
        lmtCrdReplyInfo.setPrdId(cfgPrdBasicinfoDto.getPrdId());
        lmtCrdReplyInfo.setPrdName(cfgPrdBasicinfoDto.getPrdName());
        lmtCrdReplyInfo.setBelgLine(cfgPrdBasicinfoDto.getPrdBelgLine());//小微条线
        lmtCrdReplyInfo.setOprType("01");//新增
        lmtCrdReplyInfo.setCurType("CNY");//币种
        lmtCrdReplyInfo.setReplyStatus("01");//批复状态 01--生效
        lmtCrdReplyInfo.setLmtGraper(0);//宽限期
        lmtCrdReplyInfo.setTermType("M");//期限类型 月
        lmtCrdReplyInfo.setManagerId(xdcz0026DataReqDto.getManagerId());
        lmtCrdReplyInfo.setManagerBrId(xdcz0026DataReqDto.getManager_br_id());
        lmtCrdReplyInfo.setInputId(xdcz0026DataReqDto.getManagerId());
        lmtCrdReplyInfo.setInputBrId(xdcz0026DataReqDto.getManager_br_id());
        lmtCrdReplyInfo.setUpdId(xdcz0026DataReqDto.getManagerId());
        lmtCrdReplyInfo.setUpdBrId(xdcz0026DataReqDto.getManager_br_id());

        // 大家e贷、惠民贷特殊处理 022099--惠民贷  022100--大家e贷 修订为一般合同
        if (Objects.equals("022099", cfgPrdBasicinfoDto.getPrdId()) || Objects.equals("022100", cfgPrdBasicinfoDto.getPrdId())) {
            // 01 --一次性额度
            lmtCrdReplyInfo.setReplyAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));
            lmtCrdReplyInfo.setLimitType("01");
            lmtCrdReplyInfo.setReplyStartDate(xdcz0026DataReqDto.getLoan_start_date1());
            lmtCrdReplyInfo.setReplyEndDate(xdcz0026DataReqDto.getLoan_end_date1());
            try {
                logger.info("**********计算日期相差月份=批复表期限");
                // 批复表期限
                lmtCrdReplyInfo.setAppTerm(cmisBizXwCommonService.getBetweenMonth(xdcz0026DataReqDto.getLoan_start_date1(), xdcz0026DataReqDto.getLoan_end_date1()));
            } catch (Exception e) {
            }
        }
        // 返回
        return lmtCrdReplyInfo;
    }


    /**
     * @param feng_kong_biz_type
     * @return String
     * @author shenli
     * @date 2021-7-14 20:53:17
     * @version 1.0.0
     * @desc 将风控传过来的业务类型，翻译成信贷系统的业务品种类型
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private String translateFengKongBizType(String feng_kong_biz_type) {
        String biz_type = "";
        //优享贷:022050(小贷)
        if ("7".equals(feng_kong_biz_type)) {
            biz_type = "PW010004";
        }
        //惠民贷:022099(网金部)
        else if ("8".equals(feng_kong_biz_type)) {
            biz_type = "022099";
        }
        //光伏贷:022098(零售部)
        else if ("9".equals(feng_kong_biz_type)) {
            biz_type = "022098";
        }
        //市民贷:022097(网金部)
        else if ("10".equals(feng_kong_biz_type)) {
            biz_type = "022097";
        }
        //教育分期:022096(零售部)
        else if ("11".equals(feng_kong_biz_type)) {
            biz_type = "022096";
        }
        //个人经营性贷款(如风控的房抵e点贷)
        else if ("12".equals(feng_kong_biz_type)) {
            biz_type = "022011";
        }
        //阳光速贷:022095(零售部)
        else if ("13".equals(feng_kong_biz_type)) {
            biz_type = "022095";
        }
        //小贷的"木材贷"、"服装贷"，风控送过来的产品号即为信贷的品种号
        else if ("SC010001".equals(feng_kong_biz_type)) {
            biz_type = "SC010001";
        }
        //大家e贷:022100(网金部)
        else if ("18".equals(feng_kong_biz_type)) {
            biz_type = "022100";
        }
        //增享贷(小贷)
        else if ("20".equals(feng_kong_biz_type)) {
            biz_type = "SC010014";
        }
        // 借e点
        else if ("25".equals(feng_kong_biz_type)) {
            biz_type = "022101";
        } else {
            throw BizException.error(null, null, "产品升级中，请稍后重试！");
        }

        return biz_type;
    }

    /**
     * @param xdcz0026DataReqDto
     * @return Map
     * @author shenli
     * @date 2021-7-14 20:53:17
     * @version 1.0.0
     * @desc 校验风险送过来的数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private Map<String, Object> validate(Xdcz0026DataReqDto xdcz0026DataReqDto) {

        logger.info("校验风险送过来的数据开始");
        Map validateMap = new HashMap();
        boolean pass = true;
        String validateResult = "系统检测到";

        try {
            //将风控传过来的业务类型，翻译成信贷系统的业务品种类型
            String prd_id = translateFengKongBizType(xdcz0026DataReqDto.getBiz_type());
            if (StringUtils.isBlank(prd_id)) {
                pass = false;
                validateResult += "品种类型为空或者无法识别!";
            }
            //xdcz0026DataReqDto.setBiz_type(biz_type);

            if (StringUtils.isBlank(xdcz0026DataReqDto.getManagerId())) {
                pass = false;
                validateResult += "责任人为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getManager_br_id())) {

                pass = false;
                validateResult += "责任机构为空!";

            }

            //  1).部分字段特殊校验说明：贷款产品为022095-阳光速贷/022096-教育分期/022097-市民贷/022099-惠民贷四种时，其【替补借款人账号repayment_account_tb】须必填
            if (Objects.equals(prd_id, "022095") || Objects.equals(prd_id, "022096") || Objects.equals(prd_id, "022097") || Objects.equals(prd_id, "022099")) {
                if (Objects.isNull(xdcz0026DataReqDto.getRepayment_account_tb())) {
                    pass = false;
                    validateResult += EcbEnum.ECB010044.value;
                }
            }


            if (StringUtils.isBlank(xdcz0026DataReqDto.getFina_br_id())) {
                //优享贷的帐务机构由信贷自行判断
                if ("PW010004".equals(prd_id) || "20".equals(xdcz0026DataReqDto.getBiz_type())) {

                    ResultDto<AdminSmUserDto> adminSmUserDto = adminSmUserService.getByLoginCode(xdcz0026DataReqDto.getManagerId());
                    String orgid = adminSmUserDto.getData().getOrgId();

                    //本地小贷都记在016001, 异地的为归属地最后一位改成6
                    if (StringUtils.nonBlank(orgid)) {
                        xdcz0026DataReqDto.setFina_br_id(orgid.startsWith("01") ? "016001" : orgid.substring(0, 5) + "6");
                    } else {
                        xdcz0026DataReqDto.setFina_br_id(xdcz0026DataReqDto.getManager_br_id().substring(0, 5) + "1");
                    }
                } else {
                    pass = false;
                    validateResult += "账务机构为空";
                }
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getCertid())) {

                pass = false;
                validateResult += "用户身份证号码为空!";

            } else {
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("certCode", xdcz0026DataReqDto.getCertid());
                ResultDto<List<CusIndivDto>> CusIndivDtoList = cmisCusClientService.selectDtoByCondition(queryModel);
                if (CusIndivDtoList.getData() == null || CusIndivDtoList.getData().size() == 0) {
                    pass = false;
                    validateResult += "该身份证号码未开户，请先开户!";
                }
            }


            if (StringUtils.isBlank(xdcz0026DataReqDto.getApply_amount())) {
                pass = false;
                validateResult += "合同金额为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getLoan_start_date())) {
                pass = false;
                validateResult += "合同开始日期为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getLoan_end_date())) {
                pass = false;
                validateResult += "合同结束日期为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getRate())) {
                pass = false;
                validateResult += "利率为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getBill_no())) {
                pass = false;
                validateResult += "借据号为空!";
            } else {

                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("billNo", xdcz0026DataReqDto.getBill_no());
                List<AccLoan> accLoanList = accLoanMapper.selectByModel(queryModel);
                if (accLoanList != null && accLoanList.size() > 0) {
                    pass = false;
                    validateResult += "借据号已被占用!";
                }
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getLoan_cont_no())) {
                pass = false;
                validateResult += "合同号为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getCn_cont_no())) {
                pass = false;
                validateResult += "中文合同号为空!";
            }


            //科目号
            if (StringUtils.isBlank(xdcz0026DataReqDto.getAccount_class())) {
                //022011为个人经营性贷款，科目号可以不传
                if (!"022011".equals(prd_id)) {
                    pass = false;
                    validateResult += "科目号为空!";
                }
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getZhifu_type())) {
                pass = false;
                validateResult += "支付方式为空!";
            } else if ("1".equals(xdcz0026DataReqDto.getZhifu_type()) && (StringUtils.isBlank(xdcz0026DataReqDto.getShoutuo_account()) || StringUtils.isBlank(xdcz0026DataReqDto.getShoutuo_name()))) {
                pass = false;
                validateResult += "光伏贷受托账号或者受托账号名为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getLoan_amount())) {
                pass = false;
                validateResult += "出账金额为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getRepayment_account())) {
                pass = false;
                validateResult += "借款人账号为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getLoan_end_date1())) {
                pass = false;
                validateResult += "借据开始日期(风控系统当前日期)为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getLoan_start_date1())) {
                pass = false;
                validateResult += "借据到期日期为空!";
            }

            if (StringUtils.isBlank(xdcz0026DataReqDto.getLoan_paym_mtd())) {
                pass = false;
                validateResult += "还款方式为空!";
            }

            //逾期利率
            if (StringUtils.isBlank(xdcz0026DataReqDto.getYzrate())) {
                pass = false;
                validateResult += "逾期利率为空!";
            }

            //是否免息 1:是、2:否  Todo 无参
            if ("1".equals(xdcz0026DataReqDto.getIs_free_intere())) {
                //免息天数
                if (Objects.isNull(xdcz0026DataReqDto.getFree_intere_days())) {
                    pass = false;
                    validateResult += "免息天数为空!";
                }

                //优惠券编号
                if (StringUtils.isBlank(xdcz0026DataReqDto.getDiscount_code())) {
                    pass = false;
                    validateResult += "优惠券编号为空!";
                }
            }

            if (pass) {
                validateMap.put("code", EcbEnum.COMMON_SUCCESS_DEF.value);
                validateMap.put("message", xdcz0026DataReqDto);
            } else {
                validateMap.put("code", EcbEnum.COMMON_EXCEPTION_DEF.value);
                validateMap.put("message", validateResult);
            }

            return validateMap;
        } catch (Exception e) {
            validateMap.put("code", EcbEnum.COMMON_EXCEPTION_DEF.value);
            validateMap.put("message", "校验风险送过来的数据异常");
            logger.info("校验风险送过来的数据异常：" + e.getMessage());
        }
        return validateMap;
    }


    /**
     * @param xdcz0026DataReqDto
     * @return Map
     * @author shenli
     * @date 2021-7-14 22:13:03
     * @version 1.0.0
     * @desc 交易对手账号信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private ToppAcctSub buildToppAcctSub(Xdcz0026DataReqDto xdcz0026DataReqDto, PvpLoanApp pvpLoanApp) {
        ToppAcctSub toppAcctSub = new ToppAcctSub();
        toppAcctSub.setBizSerno(pvpLoanApp.getPvpSerno());
        toppAcctSub.setBizSence("3");//业务场景
        toppAcctSub.setIsOnline("1");//是否线上
        if (Objects.equals(xdcz0026DataReqDto.getIs_self_bank_acc(), "1")) {
            toppAcctSub.setIsBankAcct("1");//是否本行账户
        } else {
            toppAcctSub.setIsBankAcct("0");//是否本行账户
        }
        toppAcctSub.setToppAcctNo(xdcz0026DataReqDto.getShoutuo_account());//交易对手账号
        toppAcctSub.setToppName(xdcz0026DataReqDto.getShoutuo_name());//交易对手名称
        toppAcctSub.setToppAmt(new BigDecimal(xdcz0026DataReqDto.getLoan_amount()));//交易对手金额
        toppAcctSub.setAcctsvcrNo(xdcz0026DataReqDto.getKhh_no());//开户行行号
        toppAcctSub.setAcctsvcrName(xdcz0026DataReqDto.getKhh_name());//开户行名称
        toppAcctSub.setOprType("01");
        return toppAcctSub;
    }

    /**
     * @param prdBelgLine
     * @return Map
     * @author shenli
     * @date 2021-7-14 22:13:03
     * @version 1.0.0
     * @desc 还款日期判断
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String queryRepayDate(String prdBelgLine) {
        String repayDate = "21";
        if ("01".equals(prdBelgLine)) {//如果是小贷产品取配置表，其他产品默认21
            logger.info("****************扣款日期获取开始*START**************");
            RepayDayRecordInfo repayDayRecordInfo = repayDayRecordInfoMapper.selectNewData();
            if (repayDayRecordInfo != null) {
                repayDate = repayDayRecordInfo.getRepayDate().toString();
            }
            logger.info("****************扣款日期获取结束*END**************还款日" + repayDate);
        }
        return repayDate;
    }

    /**
     * @param cusId, xdcz0026DataReqDto, pvpLoanApp
     * @return java.util.Map
     * @author 王玉坤
     * @date 2021/11/18 13:38
     * @version 1.0.0
     * @desc 获取贷款类别细分
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private Map<String, String> getLoanDetail(String cusId, Xdcz0026DataReqDto xdcz0026DataReqDto) {

        // 获取客户是否农户标识 信贷字典项 1--是，0--否 风控字典项 Y--是 N--否
        String ariFlag = "0";
        // 涉农贷款投向
        String farmDirection = "";
        // 贷款类别细分
        String loanTypeDetail = "";
        // 贷款投向 消费为空、经营必输
        String loanDirection = "";

        logger.info("根据客户号【{}】,查询客户信息开始", cusId);
        ResultDto<CusIndivDto> cusIndivDto = cmisCusClientService.queryCusindivByCusid(cusId);
        logger.info("根据客户号【{}】,查询客户信息结束", cusId);

        logger.info("根据客户号【{}】,查询客户单位信息开始", cusId);
        ResultDto<CusIndivUnitDto> cusIndivUnitDtoResultDto = cmisCusClientService.queryCusindivUnitByCusid(cusId);
        CusIndivUnitDto cusIndivUnitDto = null;
        if (!Objects.isNull(cusIndivUnitDtoResultDto) && !Objects.isNull(cusIndivUnitDtoResultDto.getData())) {
            logger.info("根据客户号【{}】,获取到单位信息", cusId);
            cusIndivUnitDto = cusIndivUnitDtoResultDto.getData();
        } else {
            logger.info("根据客户号【{}】,未获取到单位信息", cusId);
            cusIndivUnitDto = new CusIndivUnitDto();
        }
        logger.info("根据客户号【{}】,查询客户单位信息结束", cusId);
        // 若风控推送为空则取客户信息中的
        if (StringUtils.isEmpty(xdcz0026DataReqDto.getAgriFlg())) {
            logger.info("风控推送农户标识为空，取客户表中的");
            if (!StringUtils.isEmpty(cusIndivDto.getData().getAgriFlg())) {
                ariFlag = cusIndivDto.getData().getAgriFlg();
            }
        } else {// 取风控推送结果
            ariFlag = "Y".equals(xdcz0026DataReqDto.getAgriFlg()) ? "1" : "0";
        }

        // 根据产品判断台账涉农投向、贷款类型细分
        if ("7".equals(xdcz0026DataReqDto.getBiz_type())) {// 优享贷
            farmDirection = "1".equals(ariFlag) ? "F16" : "F14";// F16--其它消费贷款 F14--非涉农贷款
            loanTypeDetail = "05";// 消费-其他
        } else if ("11".equals(xdcz0026DataReqDto.getBiz_type())) {// 教育分期:022096(零售部)
            farmDirection = "1".equals(ariFlag) ? "F16" : "F14";// F16--其它消费贷款 F14--非涉农贷款
            loanTypeDetail = "05";// 消费-其他
        } else if ("12".equals(xdcz0026DataReqDto.getBiz_type()) || "20".equals(xdcz0026DataReqDto.getBiz_type())
                || "023001".equals(xdcz0026DataReqDto.getBiz_type())) {// 增享贷(小贷) 个人经营性贷款(如风控的房抵e点贷) 小贷的"木材贷"、"服装贷"，风控送过来的产品号即为信贷的品种号
            loanDirection = cusIndivUnitDto.getIndivComTrade();

            // 根据贷款投向映射涉农投向
            if (!StringUtils.isEmpty(loanDirection)) {
                if (loanDirection.startsWith("A01")) {// A01--农业
                    farmDirection = "F01";// F01--农业贷款
                } else if (loanDirection.startsWith("A02")) {// A02--林业
                    farmDirection = "F02";// F02--林业贷款
                } else if (loanDirection.startsWith("A03")) {// A03-畜牧业
                    farmDirection = "F03";// F03--畜牧业贷款
                } else if (loanDirection.startsWith("A04")) {// A04--渔业
                    farmDirection = "F04";// F04--渔业贷款
                } else if (loanDirection.startsWith("A05")) {// A05--农、林、牧、渔专业及辅助性活动
                    farmDirection = "F05";// F05--农林牧渔服务业贷款
                } else {
                    farmDirection = "1".equals(ariFlag) ? "F13" : "F14";// F13--其它涉农贷款 F14--非涉农贷款
                }
            } else {
                logger.info("未获取到客户行业分类！");
                farmDirection = "1".equals(ariFlag) ? "F13" : "F14";// F13--其它涉农贷款 F14--非涉农贷款
            }
            loanTypeDetail = "07";// 经营-其它
        } else if ("9".equals(xdcz0026DataReqDto.getBiz_type())) {// 光伏贷:022098(零售部)
            farmDirection = "1".equals(ariFlag) ? "F16" : "F14";// F16--其它消费贷款 F14--非涉农贷款
            loanTypeDetail = "05";// 消费-其他
        } else if ("8".equals(xdcz0026DataReqDto.getBiz_type())) {// 惠民贷:022099(网金部)
            farmDirection = "1".equals(ariFlag) ? "F16" : "F14";// F16--其它消费贷款 F14--非涉农贷款
            loanTypeDetail = "05";// 消费-其他
        } else if ("10".equals(xdcz0026DataReqDto.getBiz_type())) {// 市民贷:022097(网金部)
            farmDirection = "1".equals(ariFlag) ? "F16" : "F14";// F16--其它消费贷款 F14--非涉农贷款
            loanTypeDetail = "05";// 消费-其他
        } else if ("13".equals(xdcz0026DataReqDto.getBiz_type())) {// 阳光速贷:022095(零售部)
            farmDirection = "1".equals(ariFlag) ? "F16" : "F14";// F16--其它消费贷款 F14--非涉农贷款
            loanTypeDetail = "05";// 消费-其他
        } else if ("18".equals(xdcz0026DataReqDto.getBiz_type())) {// 大家e贷:022100(网金部)
            farmDirection = "1".equals(ariFlag) ? "F16" : "F14";// F16--其它消费贷款 F14--非涉农贷款
            loanTypeDetail = "05";// 消费-其他
        } else if ("25".equals(xdcz0026DataReqDto.getBiz_type())) {// 借e点
            farmDirection = "1".equals(ariFlag) ? "F16" : "F14";// F16--其它消费贷款 F14--非涉农贷款
            loanTypeDetail = "05";// 消费-其他
        } else {
            throw BizException.error("", "", "暂不支持该产品线上申贷！");
        }

        // 返回参数
        Map<String, String> map = new HashMap<>();
        map.put("farmDirection", farmDirection);
        map.put("loanTypeDetail", loanTypeDetail);
        map.put("loanDirection", loanDirection);
        map.put("ariFlag", ariFlag);
        return map;
    }

}
