/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.LmtFinSpApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtFinSpAppMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinSpAppService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-02-03 09:44:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtFinSpAppService {

    @Autowired
    private LmtFinSpAppMapper lmtFinSpAppMapper;
    @Autowired
    private LmtSpListService lmtSpListService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtFinSpApp selectByPrimaryKey(String serno) {
        return lmtFinSpAppMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtFinSpApp> selectAll(QueryModel model) {
        List<LmtFinSpApp> records = (List<LmtFinSpApp>) lmtFinSpAppMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtFinSpApp> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtFinSpApp> list = lmtFinSpAppMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtFinSpApp record) {
        return lmtFinSpAppMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtFinSpApp record) {
        return lmtFinSpAppMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtFinSpApp record) {
        return lmtFinSpAppMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtFinSpApp record) {
        return lmtFinSpAppMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return lmtFinSpAppMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtFinSpAppMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: checkIsExistLmtFinSpAppBySerno
     * @方法描述: 提交-校验
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int checkIsExistLmtFinSpAppBySerno(LmtFinSpApp lmtFinSpApp) {
        HashMap map = new HashMap();
        map.put("serno", lmtFinSpApp.getSerno());
        map.put("finCtrNo", lmtFinSpApp.getFinCtrNo());
        map.put("approveStatus", CmisFlowConstants.REPAY_WAY_CHG_WF_STATUS_CANNOT_COMMIT_SAME);
        if (lmtFinSpAppMapper.checkIsExistLmtFinSpAppBySerno(map) > 0) {
            throw new YuspException(EcbEnum.IS_EXIST_LMT_FIN_SP_APP_RECORD.key, EcbEnum.IS_EXIST_LMT_FIN_SP_APP_RECORD.value + "融资协议编号：" + lmtFinSpApp.getFinCtrNo());
        }
        return lmtFinSpAppMapper.checkIsExistLmtFinSpAppBySerno(map);
    }

    /**
     * @方法名称: deleteLmtFinSpAppBySerno
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean deleteLmtFinSpAppBySerno(LmtFinSpApp lmtFinSpApp) {
        lmtFinSpApp.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        Boolean result1 = updateSelective(lmtFinSpApp) == 1?true:false;
        Boolean result2 = lmtSpListService.updateOprTypeBySerno(lmtFinSpApp) >= 0?true:false;
        return result1&&result2?true:false;
    }

    /**
     * @函数名称:reCalCurtCurSubpayQntBySerno
     * @函数描述:重新计算代偿笔数
     * @参数与返回说明:
     * @算法描述:
     */

    public Boolean reCalCurtCurSubpayQntBySerno(String serno) {
        // 计算代偿笔数
        int num = lmtSpListService.calCurtCurSubpayQntBySerno(serno);
        // 更新代偿笔数
        LmtFinSpApp lmtFinSpApp = new LmtFinSpApp();
        lmtFinSpApp.setSerno(serno);
        lmtFinSpApp.setCurtCurSubpayQnt(String.valueOf(num));
        updateSelective(lmtFinSpApp);
        return true;
    }
}
