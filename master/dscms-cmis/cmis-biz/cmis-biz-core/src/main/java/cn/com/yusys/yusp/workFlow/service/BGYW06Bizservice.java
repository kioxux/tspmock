package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpBillAcctChgApp;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.Dscms2CoreLnClientService;
import cn.com.yusys.yusp.service.IqpBillAcctChgAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author tangxun
 * @version 1.0.0
 * @date 2021/6/2120:06
 * @desc 业务变更-还款帐号变更申请
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BGYW06Bizservice implements ClientBizInterface {
    //定义log
    private final Logger log = LoggerFactory.getLogger(BGYW03Bizservice.class);


    @Autowired
    private AmqpTemplate amqpTemplate;

    @Autowired
    private IqpBillAcctChgAppService iqpBillAcctChgAppService;
    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private BGYW04Bizservice BGYW04Bizservice;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        //利率变更（小微）
        if (CmisFlowConstants.FLOW_TYPE_TYPE_BG014.equals(bizType)) {
            iqpBillAcctChgBiz(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 还款账号变更
    private void iqpBillAcctChgBiz(ResultInstanceDto instanceInfo, String currentOpType, String iqpSerno, String currentUserId, String currentOrgId) {
        Map paramMap = instanceInfo.getParam();
        //  具体流程待定  流程节点控制判断暂时忽略，后期根据节点去控制
        /***当前节点 ***/
        log.info("后业务处理类型:" + currentOpType);
        // 判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        IqpBillAcctChgApp iqpBillAcctChgApp = iqpBillAcctChgAppService.selectByPrimaryKey(iqpSerno);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起流程操作:" + instanceInfo);

            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,操作如下：-- ----" + instanceInfo);
                // 改变标志 待发起 -> 审批中
                iqpBillAcctChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_111);
                iqpBillAcctChgAppService.updateSelective(iqpBillAcctChgApp);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
                //异步执行项目池自动分配操作，防止消息阻塞
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                //1.复制业务数据插入业务合同表
                //2.更新业务申请状态 由审批中111 -> 审批通过 997
                IqpBillAcctChgApp domain = iqpBillAcctChgAppService.selectByPrimaryKey(iqpSerno);
                String billNo = domain.getBillNo();
                AccLoan accLoan = accLoanService.selectByBillNo(billNo);
                /*
                 *************************************************************************
                 * 审批通过调用 贷款信息维护    ln3030
                 * *************************************************************************/
                Ln3030ReqDto reqDto = new Ln3030ReqDto();
                reqDto.setDkkhczbz("4");//操作标志 4 直通
                reqDto.setDkjiejuh(domain.getBillNo());
                reqDto.setKehuhaoo(domain.getCusId());
                reqDto.setKehmingc(domain.getCusName());
                reqDto.setHuankzhh(domain.getRepayAccno());
                reqDto.setHkzhhzxh(domain.getRepaySubAccno());
                reqDto.setHuobdhao("01");
                ResultDto<Ln3030RespDto> ln3030ResultDto = dscms2CoreLnClientService.ln3030(reqDto);
                String ln3030Code = Optional.ofNullable(ln3030ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
                String ln3030Meesage = Optional.ofNullable(ln3030ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
                Ln3030RespDto l = null;
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, ln3030ResultDto.getCode())) {
                    //  获取相关的值并解析
                    l = ln3030ResultDto.getData();
                    BGYW04Bizservice.changeAccLoan(iqpSerno);
                } else {
                    //  抛出错误异常
                    throw BizException.error(null, ln3030Code, ln3030Meesage);
                }
                /*
                 *************************************************************************
                 * 审批通过调用 贷款信息维护    ln3030
                 * *************************************************************************/

                iqpBillAcctChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
                iqpBillAcctChgAppService.updateSelective(iqpBillAcctChgApp);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                iqpBillAcctChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                iqpBillAcctChgAppService.updateSelective(iqpBillAcctChgApp);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                if (BizCommonUtils.isFirstNodeCheck(instanceInfo)) {
                    log.info("打回至发起人员处理操作，修改申请状态为：" + CmisCommonConstants.WF_STATUS_992);
                    iqpBillAcctChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_992);
                    iqpBillAcctChgAppService.updateSelective(iqpBillAcctChgApp);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
                iqpBillAcctChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_991);
                iqpBillAcctChgAppService.updateSelective(iqpBillAcctChgApp);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + instanceInfo);
                iqpBillAcctChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                iqpBillAcctChgAppService.updateSelective(iqpBillAcctChgApp);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                iqpBillAcctChgApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                iqpBillAcctChgAppService.updateSelective(iqpBillAcctChgApp);
                log.info("否决操作结束:" + instanceInfo);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return CmisFlowConstants.FLOW_ID_BGYW06.equals(flowCode);
    }
}
