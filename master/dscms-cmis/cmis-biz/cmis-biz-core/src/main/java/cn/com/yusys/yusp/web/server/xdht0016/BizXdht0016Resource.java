package cn.com.yusys.yusp.web.server.xdht0016;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0016.req.Xdht0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0016.resp.Xdht0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0016.Xdht0016Service;
import cn.com.yusys.yusp.service.server.xdxw0041.Xdxw0041Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:合同签订维护（优抵贷不见面抵押签约）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0016:合同签订维护（优抵贷不见面抵押签约）")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0016Resource.class);

    @Resource
    private Xdht0016Service xdht0016Service;

    /**
     * 交易码：xdht0016
     * 交易描述：合同签订维护（优抵贷不见面抵押签约）
     *
     * @param xdht0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同签订维护（优抵贷不见面抵押签约）")
    @PostMapping("/xdht0016")
    protected @ResponseBody
    ResultDto<Xdht0016DataRespDto> xdht0016(@Validated @RequestBody Xdht0016DataReqDto xdht0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016DataReqDto));
        Xdht0016DataRespDto xdht0016DataRespDto = new Xdht0016DataRespDto();// 响应Dto:合同签订维护（优抵贷不见面抵押签约）
        ResultDto<Xdht0016DataRespDto> xdht0016DataResultDto = new ResultDto<>();
        try {
            // 从xdht0016DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016DataReqDto));
            xdht0016DataRespDto = xdht0016Service.xdht0016(xdht0016DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016DataRespDto));
            // 封装xdht0016DataResultDto中正确的返回码和返回信息
            String opFlag = xdht0016DataRespDto.getOpFlag();
            String opMessage = xdht0016DataRespDto.getOpMsg();
            //如果失败，返回9999
            if("F".equals(opFlag)){//出账失败
                xdht0016DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0016DataResultDto.setMessage(opMessage);
            }else{
                xdht0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdht0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, e.getMessage());
            // 封装xdht0016DataResultDto中异常返回码和返回信息
            xdht0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0016DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0016DataRespDto到xdht0016DataResultDto中
        xdht0016DataResultDto.setData(xdht0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016DataResultDto));
        return xdht0016DataResultDto;
    }
}
