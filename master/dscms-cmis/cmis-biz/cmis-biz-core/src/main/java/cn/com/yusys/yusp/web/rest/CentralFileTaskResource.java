/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.CentralFileTask;
import cn.com.yusys.yusp.dto.CentralFileTaskDto;
import cn.com.yusys.yusp.service.CentralFileTaskService;
import cn.com.yusys.yusp.vo.CentralFileVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralFileTaskResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: tangxun
 * @创建时间: 2021-04-22 15:19:01
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/centralfiletask")
public class CentralFileTaskResource {
    @Autowired
    private CentralFileTaskService centralFileTaskService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CentralFileTask>> query() {
        QueryModel queryModel = new QueryModel();
        List<CentralFileTask> list = centralFileTaskService.selectAll(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CentralFileTask>> index(QueryModel queryModel) {
        List<CentralFileTask> list = centralFileTaskService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CentralFileTask>> query(@RequestBody QueryModel queryModel) {
        List<CentralFileTask> list = centralFileTaskService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/{taskNo}")
    protected ResultDto<CentralFileTask> show(@PathVariable("taskNo") String taskNo) {
        CentralFileTask centralFileTask = centralFileTaskService.selectByPrimaryKey(taskNo);
        return new ResultDto<>(centralFileTask);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<Integer> create(@RequestBody CentralFileTaskDto centralFileTaskdto) {
        int count = centralFileTaskService.insertSelective(centralFileTaskdto);
        return new ResultDto<>(count);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CentralFileTask centralFileTask) throws URISyntaxException {
        int result = centralFileTaskService.updateSelective(centralFileTask);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{taskNo}")
    protected ResultDto<Integer> delete(@PathVariable("taskNo") String taskNo) {
        int result = centralFileTaskService.deleteByPrimaryKey(taskNo);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = centralFileTaskService.deleteByIds(ids);
        return new ResultDto<>(result);
    }

    /***
     * 批量领取与分配
     * @param lists
     * @return
     * @author tangxun
     * @date 2021/5/7 4:59 下午
     * @version 1.0.0
     * @desc 循环修改对象
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/batchreceive")
    protected ResultDto<Integer> batchreceive(@RequestBody List<CentralFileTask> lists) {
        int count = centralFileTaskService.batchreceive(lists);
        return new ResultDto<>(count);
    }

    /***
     * 批量废止
     * @param lists
     * @return
     * @author tangxun
     * @date 2021/5/7 4:59 下午
     * @version 1.0.0
     * @desc 循环修改对象
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/batchabolish")
    protected ResultDto<Integer> batchabolish(@RequestBody List<CentralFileTask> lists) {
        int count = centralFileTaskService.batchabolish(lists);
        return new ResultDto<>(count);
    }

    /**
     * 暂存、派发、暂存及派发  提交处理
     * @param centralFileVo
     * @return
     */
    @PostMapping("/savecommit")
    protected ResultDto<String> savecommit(@RequestBody CentralFileVo centralFileVo){
        String result = centralFileTaskService.savecommit(centralFileVo);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatebycondition")
    protected ResultDto<Integer> updateByCondition(@RequestBody CentralFileTask centralFileTask) throws URISyntaxException {
        int result = 0;
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("instanceId", centralFileTask.getInstanceId());
        queryModel.addCondition("serno", centralFileTask.getSerno());
        queryModel.addCondition("bizType", centralFileTask.getBizType());
        List<CentralFileTask> list = centralFileTaskService.selectAll(queryModel);
        if(CollectionUtils.nonEmpty(list)){
            for (int i = 0; i < list.size(); i++) {
                centralFileTask.setTaskNo(list.get(i).getTaskNo());
                result = centralFileTaskService.updateSelective(centralFileTask);
            }
        }
        return new ResultDto<>(result);
    }


    /* @param  lists
     * @return
     * @author wzy
     * @date 2021/8/13 16:01
     * @version 1.0.0
     * @desc  审批池分配方法
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/commitnextnode")
    protected ResultDto<Integer> commitNextNode(@RequestBody List<CentralFileTask> lists) {
        int count = centralFileTaskService.commitNextNode(lists);
        return new ResultDto<>(count);
    }

}
