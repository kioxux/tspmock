package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmPropDto;
import cn.com.yusys.yusp.dto.AdminSmPropQueryDto;
import cn.com.yusys.yusp.dto.GuarBizRelGuarBaseDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 银票重复使用拦截校验
 */
@Service
public class RiskItem0012Service {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0012Service.class);

    @Autowired
    private LmtReplySubService lmtReplySubService;

    // 系统参数配置服务
    @Autowired
    private AdminSmPropService adminSmPropService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private LmtAppService lmtAppService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private AccDiscService accDiscService;
    /**
     * @方法名称: riskItem0012
     * @方法描述: 银票重复使用拦截校验（单一授信和集团客户授信）
     * @参数与返回说明:
     * @算法描述:
     * 授信申请中存在银票质押物时触发拦截规则：
     * （1）若银票质押物存在贴现台账列表，则拦截。
     * @创建人: lixy
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0012(String serno) {
        log.info("银票重复使用拦截校验开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
        }
        // 获取申请信息
        Map<String,Object> map = cusCheck(serno);
        // 单一客户授信
        LmtApp lmtApp = (LmtApp) map.get("lmtApp");
        // 集团客户授信
        LmtGrpApp lmtGrpApp = (LmtGrpApp) map.get("lmtGrpApp");
        if (Objects.isNull(lmtApp) && Objects.isNull(lmtGrpApp)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        }
        //获取系统参数获取
        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
        adminSmPropQueryDto.setPropName(CmisCommonConstants.BANK_VOUCHER);
        AdminSmPropDto adminSmPropDto= adminSmPropService.getPropValue(adminSmPropQueryDto).getData();
        if(Objects.isNull(adminSmPropDto) || StringUtils.isEmpty(adminSmPropDto.getPropValue())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_014);
            return riskResultDto;
        }
        String bizTypeCond = adminSmPropDto.getPropValue();
        // 如果是集团客户，则去获取对应的集团成员客户信息
        if(Objects.nonNull(lmtGrpApp)) {
            List<LmtGrpMemRel> list = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(serno);
            if(CollectionUtils.nonEmpty(list)) {
                for(LmtGrpMemRel lmtGrpMemRel : list) {
                    riskResultDto = riskItem0012Check(lmtGrpMemRel.getSingleSerno(),bizTypeCond);
                    if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0006);
                return riskResultDto;
            }
        }
        // 如果是个人客户
        if(Objects.nonNull(lmtApp)) {
            riskResultDto = riskItem0012Check(lmtApp.getSerno(),bizTypeCond);
            if(StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                return riskResultDto;
            }
        }
        log.info("银票重复使用拦截校验结束*******************业务流水号：【{}】",serno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     *
     * @param serno
     * @return
     */
    public RiskResultDto riskItem0012Check(String serno,String bizTypeCond) {
        Map<String,Object> subMap  = lmtAppSubService.riskItemCommonAppSubCheck(serno);
        RiskResultDto riskResultDto = (RiskResultDto) subMap.get("riskResultDto");
        if(Objects.nonNull(riskResultDto.getRiskResultType())) {
            return riskResultDto;
        }
        List<LmtAppSubPrd> subPrdList = (List<LmtAppSubPrd>) subMap.get("subPrdList");
        if(CollectionUtils.nonEmpty(subPrdList)) {
            for (LmtAppSubPrd LmtAppSubPrd : subPrdList) {
                //判断授信分项抵押类型是否为 质押 类型
                if (CmisBizConstants.STD_ZB_GUAR_WAY_20.equals(LmtAppSubPrd.getGuarMode())) {
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("serno", LmtAppSubPrd.getSubSerno());
                    //获取 抵押物品列表
                    List<GuarBizRelGuarBaseDto> guarBizRelGuarBaseDtos = guarBaseInfoService.queryGuarInfoSell(queryModel);
                    if (CollectionUtils.nonEmpty(guarBizRelGuarBaseDtos)) {
                        for (GuarBizRelGuarBaseDto guarBizRelGuarBaseDto : guarBizRelGuarBaseDtos) {
                            //获取担保分类名称
                            GuarBaseInfo guarBaseInfo = guarBaseInfoService.getGuarBaseInfoByGuarNo(guarBizRelGuarBaseDto.getGuarNo());
                            String guarTypeCd = guarBizRelGuarBaseDto.getGuarTypeCd();
                            //判断该分类是否为票据
//                            if (!StringUtils.isBlank(guarTypeCd) && !StringUtils.isBlank(bizTypeCond) && bizTypeCond.indexOf(guarTypeCd+",") != -1) {
//                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
//                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0012);
//                                return riskResultDto;
//                            }
                            if (guarTypeCd.startsWith("ZY03")){
                                String drftNo = guarBaseInfo.getDrftNo();
                                QueryModel model = new QueryModel();
                                model.getCondition().put("porderNo",drftNo);
                                List<AccDisc> accDiscList = accDiscService.querymodelByCondition(model);
                                if(accDiscList != null && accDiscList.size() >0){
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00012);
                                    return riskResultDto;
                                }
                            }
                        }
                    }
                }
            }
        }
        return riskResultDto;
    }

    /**
     * 根据申请流水号去判定是单一客户授信或者是集团客户授信
     * @param serno
     * @return
     */
    public Map<String,Object> cusCheck(String serno) {
        Map<String,Object> map = new HashMap<>();
        // 查询是否是单一客户授信
        LmtApp lmtApp = lmtAppService.selectBySerno(serno);
        if (Objects.isNull(lmtApp)) {
            // 查询是否是集团客户授信
            LmtGrpApp lmtGrpApp = lmtGrpAppService.queryLmtGrpAppByGrpSerno(serno);
            if(Objects.nonNull(lmtGrpApp)) {
                map.put("lmtGrpApp",lmtGrpApp);
            }
        } else {
            map.put("lmtApp",lmtApp);
        }
        return map;
    }

}
