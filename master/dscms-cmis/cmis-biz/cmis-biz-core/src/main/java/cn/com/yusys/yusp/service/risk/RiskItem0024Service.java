package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0056.req.CmisLmt0056ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.resp.CmisLmt0060RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 合同可用金额校验
 */
@Service
public class RiskItem0024Service {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0024Service.class);

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    PvpEntrustLoanAppService pvpEntrustLoanAppService;

    @Autowired
    private CtrEntrustLoanContService ctrEntrustLoanContService;

    @Autowired
    private CtrAccpContService ctrAccpContService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    /**
     * @方法名称: riskItem0024
     * @方法描述: 合同可用金额校验
     * @参数与返回说明:
     * @算法描述:
     * （1）最高额合同：
     * 合同项下的借据余额+合同项下在途放款申请金额+当前放款申请金额，必须小于等于合同最高可用金额
     * （2）一般合同
     * 合同项下的借据金额+合同项下在途放款申请金额+当前放款申请金额，必须小于等于合同最高可用金额
     * @创建人: lixy
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0024(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("合同可用金额校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        BigDecimal otherPvpAmt = BigDecimal.ZERO;
        String contType = "";
        // 对公贷款出账申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType) || CmisFlowConstants.FLOW_TYPE_SGD02.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_XW016.equals(bizType) || CmisFlowConstants.FLOW_TYPE_DHD02.equals(bizType)) {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            String isInTable = CmisCommonConstants.STD_ZB_YES_NO_1;
            // 贷款形式	3	借新还旧
            // 贷款形式	6	无还本续贷
            // 贷款形式	8	小企业无还本续贷
            if(Objects.equals("6",pvpLoanApp.getLoanModal()) || Objects.equals("8",pvpLoanApp.getLoanModal())
                    || Objects.equals("3",pvpLoanApp.getLoanModal())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }
            //判断是否为最高额合同和一般合同
            if (Objects.nonNull(pvpLoanApp)) {
                //根据合同编号获取合同详情
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(pvpLoanApp.getContNo());
                if(Objects.isNull(ctrLoanCont)) {
                    CtrHighAmtAgrCont ctrhighamtagrcont = ctrHighAmtAgrContService.selectDataByContNo(pvpLoanApp.getContNo());
                    if(Objects.isNull(ctrhighamtagrcont)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                        return riskResultDto;
                    } else {
                        contType = CmisBizConstants.STD_CONT_TYPE_2;
                    }
                } else {
                    contType = ctrLoanCont.getContType();
                }
                riskResultDto = checkIsOverHighAmt(pvpLoanApp.getContNo(),contType,pvpLoanApp.getContHighDisb(),pvpLoanApp.getCvtCnyAmt(),pvpLoanApp.getGuarMode(),isInTable,pvpLoanApp.getBillNo());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
        }
        // 委托贷款出账申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX013.equals(bizType)) {
            PvpEntrustLoanApp pvpEntrustLoanApp = pvpEntrustLoanAppService.selectByEntrustSernoKey(serno);
            String isInTable = CmisCommonConstants.STD_ZB_YES_NO_1;
            //判断是否为最高额合同和一般合同
            if (Objects.nonNull(pvpEntrustLoanApp)) {
                CtrEntrustLoanCont ctrEntrustLoanCont = ctrEntrustLoanContService.selectByContNo(pvpEntrustLoanApp.getContNo());
                if(Objects.isNull(ctrEntrustLoanCont)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                    return riskResultDto;
                }
                riskResultDto = checkIsOverHighAmt(pvpEntrustLoanApp.getContNo(),ctrEntrustLoanCont.getContType(),pvpEntrustLoanApp.getContHighDisb(),pvpEntrustLoanApp.getPvpAmt(),pvpEntrustLoanApp.getGuarMode(),isInTable,pvpEntrustLoanApp.getBillNo());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
        }

        // 银承出账申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_YX012.equals(bizType)) {
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
            String isInTable = CmisCommonConstants.STD_ZB_YES_NO_0;
            String guarMode = "";
            BigDecimal appAmt = BigDecimal.ZERO;
            if (pvpAccpApp != null) {
                CtrAccpCont ctrAccpCont = ctrAccpContService.selectByContNo(pvpAccpApp.getContNo());
                if(Objects.isNull(ctrAccpCont)) {
                    CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(pvpAccpApp.getContNo());
                    if(Objects.isNull(ctrHighAmtAgrCont)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                        return riskResultDto;
                    }
                    guarMode = ctrHighAmtAgrCont.getGuarMode();
                }else {
                    guarMode = ctrAccpCont.getGuarMode();
                }
                if(CmisCommonConstants.GUAR_MODE_40.equals(guarMode)||CmisCommonConstants.GUAR_MODE_21.equals(guarMode)){
                    appAmt = pvpAccpApp.getAppAmt();
                }else {
                    appAmt = pvpAccpApp.getAppAmt().subtract(pvpAccpApp.getAppAmt().multiply(pvpAccpApp.getBailPerc()));
                }
                riskResultDto = checkIsOverHighAmt(pvpAccpApp.getContNo(),pvpAccpApp.getContType(),pvpAccpApp.getContHighDisb(),appAmt,pvpAccpApp.getGuarMode(),isInTable,pvpAccpApp.getPvpSerno());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
        }
        // 小微放款申请
        if (CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType)) {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            String isInTable = CmisCommonConstants.STD_ZB_YES_NO_1;
            // 贷款形式	3	借新还旧
            // 贷款形式	6	无还本续贷
            // 贷款形式	8	小企业无还本续贷
            if(Objects.equals("6",pvpLoanApp.getLoanModal()) || Objects.equals("8",pvpLoanApp.getLoanModal())
                    || Objects.equals("3",pvpLoanApp.getLoanModal())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }
            if (pvpLoanApp != null) {
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(pvpLoanApp.getContNo());
                if(Objects.isNull(ctrLoanCont)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                    return riskResultDto;
                }
                riskResultDto = checkIsOverHighAmt(pvpLoanApp.getContNo(),ctrLoanCont.getContType(),pvpLoanApp.getContHighDisb(),pvpLoanApp.getCvtCnyAmt(),pvpLoanApp.getGuarMode(),isInTable,pvpLoanApp.getBillNo());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
        }
        //零售放款申请-----------------------------------------------------------------------
        //零售放款申请（空白合同模式）
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_LS005.equals(bizType)
                || "SGE04".equals(bizType) || "DHE04".equals(bizType)) {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            String isInTable = CmisCommonConstants.STD_ZB_YES_NO_1;
            // 贷款形式	3	借新还旧
            // 贷款形式	6	无还本续贷
            // 贷款形式	8	小企业无还本续贷
            if(Objects.equals("6",pvpLoanApp.getLoanModal()) || Objects.equals("8",pvpLoanApp.getLoanModal())
                    || Objects.equals("3",pvpLoanApp.getLoanModal())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }
            if (pvpLoanApp != null && !StringUtils.isBlank(pvpLoanApp.getContNo())) {
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(pvpLoanApp.getContNo());
                if(Objects.isNull(ctrLoanCont)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                    return riskResultDto;
                }
                riskResultDto = checkIsOverHighAmt(pvpLoanApp.getContNo(),ctrLoanCont.getContType(),ctrLoanCont.getHighAvlAmt(),pvpLoanApp.getCvtCnyAmt(),pvpLoanApp.getGuarMode(),isInTable,pvpLoanApp.getBillNo());
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
        }
        //零售放款申请（生成打印模式）
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_LS006.equals(bizType)) {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            // 贷款形式	3	借新还旧
            // 贷款形式	6	无还本续贷
            // 贷款形式	8	小企业无还本续贷
            if(Objects.equals("6",pvpLoanApp.getLoanModal()) || Objects.equals("8",pvpLoanApp.getLoanModal())
                    || Objects.equals("3",pvpLoanApp.getLoanModal())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                return riskResultDto;
            }
            if (pvpLoanApp != null && !StringUtils.isBlank(pvpLoanApp.getContNo())) {
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(pvpLoanApp.getContNo());
                if(Objects.isNull(ctrLoanCont)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                    return riskResultDto;
                }
                //合同项下在途放款申请金额
                List<PvpLoanApp> otherPvpLoanAppList = pvpLoanAppService.selectByContNo(pvpLoanApp.getContNo());
                if (CollectionUtils.nonEmpty(otherPvpLoanAppList)) {
                    for (PvpLoanApp loanApp : otherPvpLoanAppList) {
                        if(!Objects.equals(pvpLoanApp.getPvpSerno(),loanApp.getPvpSerno()) && (Objects.equals(CmisFlowConstants.WF_STATUS_111,loanApp.getApproveStatus()) || Objects.equals(CmisFlowConstants.WF_STATUS_992,loanApp.getApproveStatus()))) {
                            otherPvpAmt = otherPvpAmt.add(Objects.nonNull(loanApp.getPvpAmt()) ? loanApp.getPvpAmt() : BigDecimal.ZERO);
                        }
                    }
                }
                riskResultDto = checkIsOverHighAmtLS(pvpLoanApp.getContNo(),ctrLoanCont.getHighAvlAmt(),pvpLoanApp.getPvpAmt(),otherPvpAmt);
                return riskResultDto;
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
        }
        log.info("合同可用金额校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        return riskResultDto;
    }

    /**
     * 判断是否超过最高授信金额
     * @param contNo 合同编号
     * @param contType 合同类型
     * @param contHighDisb 本合同项下最高可用金额
     * @param amt 金额
     * @return
     */
    private RiskResultDto checkIsOverHighAmt(String contNo,String contType,BigDecimal contHighDisb,BigDecimal amt,String guarMode,String isIntable,String billNo) {
        log.info("合同可用金额校验开始（判断是否超过最高授信金额）*******************业务流水号：【{}】",contNo);
        RiskResultDto riskResultDto = new RiskResultDto();
        //合同最高可放金额为空
        if (Objects.isNull(contHighDisb)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02404);
            return riskResultDto;
        }
        // 根据合同号调额度接口查询合同项下余额
        CmisLmt0060ReqDto cmisLmt0060ReqDto = new CmisLmt0060ReqDto();
        cmisLmt0060ReqDto.setDealBizNo(contNo);
        cmisLmt0060ReqDto.setTranAccNo(billNo);
        log.info("根据合同编号【{}】，前往额度系统查询合同余额开始,请求报文为:【{}】", contNo, cmisLmt0060ReqDto.toString());
        ResultDto<CmisLmt0060RespDto> resultDtoDto = cmisLmtClientService.cmislmt0060(cmisLmt0060ReqDto);
        log.info("根据合同编号【{}】，前往额度系统查询合同余额结束,响应报文为:【{}】", contNo, resultDtoDto.toString());
        if(!"0".equals(resultDtoDto.getCode())){
            log.error("cmisLmt60接口调用失败！");
            throw BizException.error(null, EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        if(!"0000".equals(resultDtoDto.getData().getErrorCode())){
            log.error(resultDtoDto.getData().getErrorMsg());
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(resultDtoDto.getData().getErrorMsg());
            return riskResultDto;
        }
        BigDecimal totalCny = resultDtoDto.getData().getTotalCny();//初始占用总额
        BigDecimal spacCny = resultDtoDto.getData().getSpacCny();//初始占用敞口金额
        BigDecimal totalBalanceCny = resultDtoDto.getData().getTotalBalanceCny();//初始占用总余额
        BigDecimal spacBalanceCny = resultDtoDto.getData().getSpacBalanceCny();//初始占用敞口余额
        //总金额
        BigDecimal add = BigDecimal.ZERO;
        // 非循环额度
        if (CmisBizConstants.STD_CONT_TYPE_1.equals(contType)){
            // 低风险业务
            if(CmisCommonConstants.GUAR_MODE_21.equals(guarMode) || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_60.equals(guarMode)){
                // 表内业务
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isIntable)){
                    //总金额
                    add = totalCny.add(amt);
                }else{
                    add = totalCny.add(amt);
                }
            }else {
                // 表内业务
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isIntable)){
                    add = spacCny.add(amt);
                }else {
                    add = spacCny.add(amt);
                }
            }
        }else{
        // 循环额度
            // 低风险业务
            if(CmisCommonConstants.GUAR_MODE_21.equals(guarMode) || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)
                    || CmisCommonConstants.GUAR_MODE_60.equals(guarMode)){
                // 表内业务
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isIntable)){
                    add = totalBalanceCny.add(amt);
                }else {
                    add = totalBalanceCny.add(amt);
                }
            }else {
                // 表内业务
                if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isIntable)){
                    add = spacBalanceCny.add(amt);
                }else {
                    add = spacBalanceCny.add(amt);
                }
            }
        }
        log.info("合同已用金额+本次出账金额为【{}】",add);
        if (contHighDisb.compareTo(add) < 0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc("合同已用金额+当前放款申请金额"+add+"，必须小于等于合同最高可用金额"+contHighDisb+"！");
            return riskResultDto;
        }
        log.info("合同可用金额校验结束（判断是否超过最高授信金额）*******************业务流水号：【{}】",contNo);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param contNo, contHighDisb, amt
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/7/29 15:41
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto checkIsOverHighAmtLS(String contNo,BigDecimal contHighDisb,BigDecimal amt,BigDecimal otherPvpAmt) {
        log.info("合同可用金额校验开始（判断是否超过最高授信金额）*******************业务流水号：【{}】",contNo);
        RiskResultDto riskResultDto = new RiskResultDto();
        //合同最高可放金额为空
        if (Objects.isNull(contHighDisb)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02404);
            return riskResultDto;
        }
        //根据合同编号获取合同详情
        CtrLoanCont ctrLoanCont = ctrLoanContService.selectContByContno(contNo);
        //判断当前合同是否为最高额合同或一般合同
        if (Objects.nonNull(ctrLoanCont) && CmisBizConstants.STD_CONT_TYPE_1.equals(ctrLoanCont.getContType())){
            try {
                //借据余额
                BigDecimal loanBalance = BigDecimal.ZERO;
                //获取台账表中的放款金额
                List<AccLoan> accLoans = accLoanService.selectByContNo(contNo);
                if (CollectionUtils.nonEmpty(accLoans)) {
                    for (AccLoan accLoan : accLoans) {
                        loanBalance = loanBalance.add(Objects.nonNull(accLoan.getLoanAmt()) ? accLoan.getLoanAmt() : BigDecimal.ZERO);
                    }
                }
                //合同项下的借据余额+合同项下在途放款申请金额+当前放款申请金额，必须小于等于合同最高可用金额
                BigDecimal add = amt.add(otherPvpAmt).add(loanBalance);
                if (contHighDisb.compareTo(add) < 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02401);
                    return riskResultDto;
                }
            } catch (Exception e) {
                log.error("金额计算错误！",e);
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02403);
                return riskResultDto;
            }
        }else if (CmisBizConstants.STD_CONT_TYPE_2.equals(ctrLoanCont.getContType())) {
            try {
                //借据余额
                BigDecimal loanBalance = BigDecimal.ZERO;
                //获取台账表中的放款金额
                List<AccLoan> accLoans = accLoanService.selectByContNo(contNo);
                if (CollectionUtils.nonEmpty(accLoans)) {
                    for (AccLoan accLoan : accLoans) {
                        loanBalance = loanBalance.add(Objects.nonNull(accLoan.getLoanBalance()) ? accLoan.getLoanBalance() : BigDecimal.ZERO);
                    }
                }
                //合同项下的借据余额+合同项下在途放款申请金额+当前放款申请金额，必须小于等于合同最高可用金额
                BigDecimal add = amt.add(otherPvpAmt).add(loanBalance);
                if (contHighDisb.compareTo(add) < 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02401);
                    return riskResultDto;
                }
            } catch (Exception e) {
                log.error("金额计算错误！",e);
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02403);
                return riskResultDto;
            }
        }else{
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02402);
            return riskResultDto;
        }
        log.info("合同可用金额校验结束（判断是否超过最高授信金额）*******************业务流水号：【{}】",contNo);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

}
