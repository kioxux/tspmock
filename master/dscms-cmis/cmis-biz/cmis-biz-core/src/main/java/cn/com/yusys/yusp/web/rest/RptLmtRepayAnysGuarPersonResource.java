/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptLmtRepayAnysGuarPerson;
import cn.com.yusys.yusp.service.RptLmtRepayAnysGuarPersonService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptLmtRepayAnysGuarPersonResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-05 23:35:10
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptlmtrepayanysguarperson")
public class RptLmtRepayAnysGuarPersonResource {
    @Autowired
    private RptLmtRepayAnysGuarPersonService rptLmtRepayAnysGuarPersonService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptLmtRepayAnysGuarPerson>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptLmtRepayAnysGuarPerson> list = rptLmtRepayAnysGuarPersonService.selectAll(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarPerson>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptLmtRepayAnysGuarPerson>> index(QueryModel queryModel) {
        List<RptLmtRepayAnysGuarPerson> list = rptLmtRepayAnysGuarPersonService.selectByModel(queryModel);
        return new ResultDto<List<RptLmtRepayAnysGuarPerson>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptLmtRepayAnysGuarPerson> show(@PathVariable("pkId") String pkId) {
        RptLmtRepayAnysGuarPerson rptLmtRepayAnysGuarPerson = rptLmtRepayAnysGuarPersonService.selectByPrimaryKey(pkId);
        return new ResultDto<RptLmtRepayAnysGuarPerson>(rptLmtRepayAnysGuarPerson);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptLmtRepayAnysGuarPerson> create(@RequestBody RptLmtRepayAnysGuarPerson rptLmtRepayAnysGuarPerson) throws URISyntaxException {
        rptLmtRepayAnysGuarPersonService.insert(rptLmtRepayAnysGuarPerson);
        return new ResultDto<RptLmtRepayAnysGuarPerson>(rptLmtRepayAnysGuarPerson);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptLmtRepayAnysGuarPerson rptLmtRepayAnysGuarPerson) throws URISyntaxException {
        int result = rptLmtRepayAnysGuarPersonService.update(rptLmtRepayAnysGuarPerson);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptLmtRepayAnysGuarPersonService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptLmtRepayAnysGuarPersonService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号查询
     * @param map RptLmtRepayAnysGuarPerson rptLmtRepayAnysGuarPerson
     * @return
     */
    @ApiOperation("根据流水号查看担保人为自然人信息")
    @PostMapping("/selectBySerno")
    protected ResultDto<List<RptLmtRepayAnysGuarPerson>> selectBySerno(@RequestBody Map<String, Object> map){
        String serno = map.get("serno").toString();
        QueryModel model  = new QueryModel();
        model.addCondition("serno",serno);
        return new ResultDto<List<RptLmtRepayAnysGuarPerson>>(rptLmtRepayAnysGuarPersonService.selectByModel(model));
    }

    /**
     * 保存信息
     * @param rptLmtRepayAnysGuarPerson
     * @return
     */
    @PostMapping("/saveGuarPerson")
    protected ResultDto<Integer> saveGuarPerson(@RequestBody RptLmtRepayAnysGuarPerson rptLmtRepayAnysGuarPerson){
        return new ResultDto<Integer>(rptLmtRepayAnysGuarPersonService.saveGuarPerson(rptLmtRepayAnysGuarPerson));
    }
}
