/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.flow.dto.WFBizParamDto;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestApprMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoApprMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicInfoSubApprMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSigInvestBasicLmtApprMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestApprService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-17 15:28:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtSigInvestApprService extends BizInvestCommonService{

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtSigInvestApprService.class);

    @Autowired
    private LmtSigInvestApprMapper lmtSigInvestApprMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;

    @Autowired
    private LmtAppRelGuarService lmtAppRelGuarService;

    @Autowired
    private LmtSigInvestBasicInfoApprService lmtSigInvestBasicInfoApprService ;

    @Autowired
    private LmtSigInvestBasicInfoApprMapper lmtSigInvestBasicInfoApprMapper ;

    @Autowired
    private LmtSigInvestBasicInfoSubApprService lmtSigInvestBasicInfoSubApprService ;

    @Autowired
    private LmtSigInvestBasicInfoSubApprMapper lmtSigInvestBasicInfoSubApprMapper ;

    @Autowired
    private LmtSigInvestBasicLmtApprService lmtSigInvestBasicLmtApprService ;

    @Autowired
    private LmtSigInvestBasicLmtApprMapper lmtSigInvestBasicLmtApprMapper ;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private LmtAppRelCusInfoService lmtAppRelCusInfoService;
    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private LmtApprLoanCondService lmtApprLoanCondService;



    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtSigInvestAppr selectByPrimaryKey(String pkId) {
        return lmtSigInvestApprMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtSigInvestAppr> selectAll(QueryModel model) {
        List<LmtSigInvestAppr> records = (List<LmtSigInvestAppr>) lmtSigInvestApprMapper.selectByModel(model);
        return records;
    }


    /**
     * 根据申请流水号获取最新的审批表数据信息
     * @param serno
     * @return
     */
    public LmtSigInvestAppr selectBySerno(String serno) {
        return lmtSigInvestApprMapper.selectNewApprInfoBySerno(serno);
    }


    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtSigInvestAppr> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSigInvestAppr> list = lmtSigInvestApprMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtSigInvestAppr record) {
        return lmtSigInvestApprMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtSigInvestAppr record) {
        return lmtSigInvestApprMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtSigInvestAppr record) {
        return lmtSigInvestApprMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int updateSelective(LmtSigInvestAppr record) {
        //判断pkid是否存在
        if (StringUtils.isBlank(record.getPkId())) {
            LmtSigInvestAppr lmtSigInvestAppr = selectBySerno(record.getSerno());
            if (lmtSigInvestAppr != null) {
                record.setPkId(lmtSigInvestAppr.getPkId());
            } else {
                return 0;
            }
        }
        return lmtSigInvestApprMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtSigInvestApprMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtSigInvestApprMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: initLmtSigInvestApprInfo
     * @方法描述: 根据申请信息，组装审批表数据
     * @参数与返回说明: 返回审批数据对象信息
     * @算法描述: 无
     */

    public LmtSigInvestAppr initLmtSigInvestApprInfo(LmtSigInvestApp lmtSigInvestApp) {
        //初始化对象
        LmtSigInvestAppr lmtSigInvestAppr = new LmtSigInvestAppr() ;
        //拷贝对象
        BeanUtils.copyProperties(lmtSigInvestApp, lmtSigInvestAppr);
        //生成主键
        lmtSigInvestAppr.setPkId(generatePkId());
        //审批流水号设置  TODO:该流水号设置，是否根据流程审批流水号走？
        lmtSigInvestAppr.setApproveSerno(generateSerno(SeqConstant.INVEST_LMT_SEQ));

        /**
         * modify by zhangjw 20210721
         * 综合分析、授信限额情况、评审结论、结论性描述  为审批中有权限审批人员录入要素，发起人初始化审批要素时，无数据
         * 出具报告类型、核查报告模式、核查报告路径   由审批节点确认，发起人初始化时无数据
         * 审批模式 可以由路由条件判断得到结果
         */
        //综合分析
        lmtSigInvestAppr.setInteAnaly("");
        //授信限额情况
        lmtSigInvestAppr.setLmtQuotaSitu("");
        //评审结论
        lmtSigInvestAppr.setReviewResult("");
        //结论性描述
        lmtSigInvestAppr.setRestDesc("");
        //出具报告类型
        lmtSigInvestAppr.setIssueReportType("");
        //核查报告模式
        lmtSigInvestAppr.setIndgtReportMode("");
        //核查报告路径
        lmtSigInvestAppr.setIndgtReportPath("");
        //终审机构
        lmtSigInvestAppr.setFinalApprBrType(CmisCommonConstants.STD_FINAL_APPR_BR_TYPE_03);
        //审批模式
        lmtSigInvestAppr.setApprMode("");
        lmtSigInvestAppr.setUpdDate(DateUtils.getCurrDateStr());
        lmtSigInvestAppr.setCreateTime(DateUtils.getCurrTimestamp());
        lmtSigInvestAppr.setUpdateTime(DateUtils.getCurrTimestamp());
        lmtSigInvestAppr.setOprType(CmisBizConstants.OPR_TYPE_01);
        //判断是否存在币种
        if (StringUtils.isBlank(lmtSigInvestApp.getCurType())){
            //默认为人民币
            lmtSigInvestAppr.setCurType(CmisBizConstants.STD_ZB_CUR_TYP_CNY);
        }
        //判断是否循环 默认为是
        if (StringUtils.isBlank(lmtSigInvestApp.getIsRevolv())){
            //默认为是
            lmtSigInvestAppr.setIsRevolv(CmisBizConstants.STD_ZB_YES_NO_Y);
        }
        //是否上调权限 默认为否
        lmtSigInvestAppr.setIsUpperApprAuth(CmisBizConstants.STD_ZB_YES_NO_N);
        //end
        //返回对象信息
        return lmtSigInvestAppr ;
    }

    public Map<String,Object> selectBySerno(QueryModel model){
        Map<String,Object> resultMap = new HashMap<>();
        String serno = model.getCondition().get("serno").toString();
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        List<LmtSigInvestAppr> lmtSigInvestApprs = lmtSigInvestApprMapper.selectByModel(model);

        if (lmtSigInvestApprs!=null && lmtSigInvestApprs.size()>0){
            LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprMapper.selectByModel(model).get(0);
            Map<String,Object> lmtSigInvestApprMap = new HashMap<>();
            Map<String, Object> newMap = (Map<String, Object>) cn.com.yusys.yusp.commons.util.BeanUtils.beanToMap(lmtSigInvestAppr);
            lmtSigInvestApprMap.putAll(newMap);
            AdminSmUserDto adminSmUserDto = commonService.getByLoginCode(lmtSigInvestAppr.getInputId());
            AdminSmOrgDto adminSmOrgDto = commonService.getByOrgCode(lmtSigInvestAppr.getInputBrId());
            lmtSigInvestApprMap.put("inputIdName",adminSmUserDto.getUserName());
            lmtSigInvestApprMap.put("inputBrIdName",adminSmOrgDto.getOrgName());
            resultMap.put("lmtSigInvestAppr",lmtSigInvestApprMap);
        }else{
            resultMap.put("lmtSigInvestAppr",new LmtSigInvestAppr());
        }

        //获取担保方式
        Map<String,Object> guarMap = lmtAppRelGuarService.selectGuarTypeName(serno);
        if(guarMap!=null) {
            String guarTypeName = guarMap.get("guarTypeName").toString();
            resultMap.put("guarTypeName",guarTypeName);
        }

        return resultMap;
    }

    /**
     * @方法名称: updateRestByPkId
     * @方法描述: 根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestByPkId(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<LmtSigInvestAppr> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            LmtSigInvestAppr lmtSigInvestAppr = list.get(0);
            //获取前端传入参数 reviewResult-审批结论
            String reviewResult = (String)model.getCondition().get("reviewResult");
            //码值转换
            reviewResult = CmisCommonConstants.commonSignMap.get(reviewResult);
            lmtSigInvestAppr.setReviewResult(reviewResult);
            //获取前端传入参数restDesc-审批意见
            String restDesc = (String)model.getCondition().get("restDesc");
            lmtSigInvestAppr.setRestDesc(restDesc);
            //更新 更新数据时间
            lmtSigInvestAppr.setUpdateTime(getCurrrentDate());
            return lmtSigInvestApprMapper.updateByPrimaryKey(lmtSigInvestAppr);
        }
        return 0;
    }

    /**
     * @方法名称: updateRestByPkId
     * @方法描述: 根据Serno更新审批结论、审批意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestByPkIdZH(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<LmtSigInvestAppr> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            LmtSigInvestAppr lmtSigInvestAppr = list.get(0);
            //获取前端传入参数 reviewResult-审批结论
            String reviewResult = (String)model.getCondition().get("reviewResult");
            //码值转换
            reviewResult = CmisCommonConstants.commonSignMap.get(reviewResult);
            lmtSigInvestAppr.setReviewResultZh(reviewResult);
            //获取前端传入参数restDesc-审批意见
            String restDesc = (String)model.getCondition().get("restDesc");
            lmtSigInvestAppr.setRestDescZh(restDesc);
            //更新 更新数据时间
            lmtSigInvestAppr.setUpdateTime(getCurrrentDate());
            return lmtSigInvestApprMapper.updateByPrimaryKey(lmtSigInvestAppr);
        }
        return 0;
    }

    /**
     * @方法名称: updateRestBySerno
     * @方法描述: 根据Serno更新核查结论、核查意见
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateRestBySerno(QueryModel model) {
        String serno = (String) model.getCondition().get("serno");
        //根据流水号获取申请流水号对应审批表最新的数据
        QueryModel selectModel = new QueryModel();
        selectModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        selectModel.addCondition("oprType", CmisLmtConstants.OPR_TYPE_ADD);
        //按照创建时间倒序排序
        selectModel.setSort(" createTime desc ");
        List<LmtSigInvestAppr> list = selectByModel(selectModel);
        //如果流水号对应审批表最新的数据有值，则更新审批表对应的审批结论和审批意见
        if(list!=null && list.size()>0){
            LmtSigInvestAppr lmtSigInvestAppr = list.get(0);
            //获取前端传入参数 reviewResult-审批结论
            lmtSigInvestAppr.setReviewResult((String)model.getCondition().get("reviewResult"));
            //获取前端传入参数restDesc-审批意见
            lmtSigInvestAppr.setRestDesc((String)model.getCondition().get("restDesc"));
            //核查报告
            lmtSigInvestAppr.setIssueReportType(CmisBizConstants.STD_ISSUE_REPORT_TYPE_02);
            //更新 更新数据时间
            lmtSigInvestAppr.setUpdateTime(getCurrrentDate());
            return lmtSigInvestApprMapper.updateByPrimaryKey(lmtSigInvestAppr);
        }
        return 0;
    }

    /**
     * @方法名称: generateSigInvestAppr
     * @方法描述: 根据流程审批表数据，获取最近的(协办客户经理编号字段）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public void generateSigInvestAppr(LmtSigInvestApp lmtSigInvestApp, String issueReportType, String currentUserId, String cur_next_id){
        log.info("用信条件拷贝判断 开始==》cur_next_id【{}】",cur_next_id);
        String[] ingoreProperties = getApprIngoreProperties(cur_next_id);
        log.info("用信条件拷贝判断 结束==》ingoreProperties【{}】", Arrays.toString(ingoreProperties));

        //根据审批表，获取最新审批记录
        String serno = lmtSigInvestApp.getSerno() ;
        //根据流水号，查询最新的审批信息
        LmtSigInvestAppr lmtSigInvestApprOld = lmtSigInvestApprMapper.selectLmtSigInvestApprByApproveSerno(serno) ;
        //审批流水号
        String approveSerno = lmtSigInvestApprOld.getApproveSerno() ;
        //初始化审批主表对象
        LmtSigInvestAppr lmtSigInvestAppr = new LmtSigInvestAppr() ;
        //数据拷贝
        BeanUtils.copyProperties(lmtSigInvestApprOld, lmtSigInvestAppr);

        //判断是否拷贝 评审结论和结论性描述
        //1.通用规则：评审结论和结论性描述，都不copy至下一节点；
        //2.例外规则：
        //      流程审批的下一节点如果是信贷管理部风险派驻岗，
        //      则获取上一条审批记录中出具报告类型为审查报告的且ZH审批结论为空的数据，
        //      copy评审结论和结论性描述至生成的最新审批记录数据中
        String nextNodeId = cur_next_id.split(";")[1];
        if (CmisBizConstants.Xdglbfxpzg_NodeIds.contains(nextNodeId+",")){
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno",serno);
            BizInvestCommonService.checkParamsIsNull("serno",serno);
            queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
            queryModel.addCondition("issueReportType",CmisBizConstants.STD_ISSUE_REPORT_TYPE_01);
            queryModel.setSort(" createTime desc");
            List<LmtSigInvestAppr> lmtSigInvestApprs = selectAll(queryModel);
            if (CollectionUtils.isNotEmpty(lmtSigInvestApprs)){
                for (LmtSigInvestAppr a : lmtSigInvestApprs) {
                    if (StringUtils.isBlank(a.getReviewResultZh())){
                        lmtSigInvestAppr.setRestDesc(a.getRestDesc());
                        lmtSigInvestAppr.setRestDescZh(a.getRestDescZh());
                        lmtSigInvestAppr.setReviewResultZh(a.getReviewResultZh());
                        lmtSigInvestAppr.setReviewResult(a.getReviewResult());
                        break;
                    }
                }
                //拷贝上个审查报告相关信息
                LmtSigInvestAppr lmtSigInvestAppr1 = lmtSigInvestApprs.get(0);
                lmtSigInvestAppr.setInteAnaly(lmtSigInvestAppr1.getInteAnaly());
                lmtSigInvestAppr.setInteAnalyZh(lmtSigInvestAppr1.getInteAnalyZh());
            }
        }

        //初始化主键信息
        lmtSigInvestAppr.setPkId(generatePkId());
        //初始化审批流水号
        lmtSigInvestAppr.setApproveSerno(generateSerno(CmisLmtConstants.LMT_SERNO));
        //初始化协办客户经理编号
        lmtSigInvestAppr.setAssistManagerId(currentUserId);
        //报告类型
        lmtSigInvestAppr.setIssueReportType(issueReportType);
        //初始化更新人跟新机构，跟新日期等
        initInsertDomainPropertiesForWF(lmtSigInvestAppr) ;
        //数据导入
        lmtSigInvestApprMapper.insert(lmtSigInvestAppr) ;

        //审批流水号
        String approveSernoNew = lmtSigInvestAppr.getApproveSerno() ;
        if (ingoreProperties.length == 4){
            log.info("{}==={} 【{}】其他要求拷贝====》开始",approveSernoNew,approveSerno,serno);
            // 通过授信审批用信条件生成新的授信审批用信条件  add by zhangjw 20210827
            lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(approveSernoNew, approveSerno);
            log.info("{}==={} 【{}】其他要求拷贝====》结束",approveSernoNew,approveSerno,serno);
        }

        /****** 根据审批流水号，获取对应单笔投资对应底层资产基本情况审批表 ********/
        LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoApprOld = lmtSigInvestBasicInfoApprService.selectByApproveSerno(approveSerno) ;
        if (lmtSigInvestBasicInfoApprOld!=null){
            LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = new LmtSigInvestBasicInfoAppr() ;
            //数据拷贝
            BeanUtils.copyProperties(lmtSigInvestBasicInfoApprOld, lmtSigInvestBasicInfoAppr);
            lmtSigInvestBasicInfoAppr.setPkId(generatePkId());
            //初始化审批流水号
            lmtSigInvestBasicInfoAppr.setApproveSerno(approveSernoNew);
            //初始化更新人跟新机构，跟新日期等
            initInsertDomainProperties(lmtSigInvestAppr,lmtSigInvestBasicInfoApprOld.getInputId(),lmtSigInvestBasicInfoApprOld.getInputBrId()) ;
            //数据插入
            lmtSigInvestBasicInfoApprMapper.insert(lmtSigInvestBasicInfoAppr) ;

//            /**
//             * 拷贝用信条件
//             */
//            // 通过授信审批用信条件生成新的授信审批用信条件  add by zhangjw 20210827
//            lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(approveSernoNew, lmtSigInvestBasicInfoApprOld.getApproveSerno());
        }


        /****** 根据审批流水号，获取对应单笔投资关联底层信息明细审批表 ********/
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprService.selectByApproveSerno(approveSerno) ;
        if (lmtSigInvestBasicInfoSubApprList!=null && lmtSigInvestBasicInfoSubApprList.size()>0){
            //遍历地缝明细审批，重新生成
            Supplier<LmtSigInvestBasicInfoSubAppr> supplierSubAppr = LmtSigInvestBasicInfoSubAppr :: new ;
            for (LmtSigInvestBasicInfoSubAppr sigInvestBasicInfoSubApprOld : lmtSigInvestBasicInfoSubApprList) {
                LmtSigInvestBasicInfoSubAppr lmtSigInvestBasicInfoSubAppr = supplierSubAppr.get() ;
                //数据拷贝
                BeanUtils.copyProperties(sigInvestBasicInfoSubApprOld, lmtSigInvestBasicInfoSubAppr);
                lmtSigInvestBasicInfoSubAppr.setPkId(generatePkId());
                //初始化批复流水号
                lmtSigInvestBasicInfoSubAppr.setApproveSerno(approveSernoNew);
                //初始化更新人跟新机构，跟新日期等
                initInsertDomainProperties(lmtSigInvestAppr,sigInvestBasicInfoSubApprOld.getInputId(),sigInvestBasicInfoSubApprOld.getInputBrId()) ;
                lmtSigInvestBasicInfoSubApprMapper.insert(lmtSigInvestBasicInfoSubAppr) ;
            }
        }


        /****** 根据审批流水号，获取对应底层资产额度审批表 ********/
        List<LmtSigInvestBasicLmtAppr> lmtSigInvestBasicLmtApprs = lmtSigInvestBasicLmtApprService.selectByApproveSerno(approveSerno);
        if (lmtSigInvestBasicLmtApprs!=null && lmtSigInvestBasicLmtApprs.size()>0){
            Supplier<LmtSigInvestBasicLmtAppr> supplierBasicLmtAppr = LmtSigInvestBasicLmtAppr :: new ;
            for (LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtApprOld : lmtSigInvestBasicLmtApprs) {
                LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr = supplierBasicLmtAppr.get() ;
                BeanUtils.copyProperties(lmtSigInvestBasicLmtApprOld, lmtSigInvestBasicLmtAppr);
                lmtSigInvestBasicLmtAppr.setPkId(generatePkId());
                //初始化批复流水号
                lmtSigInvestBasicLmtAppr.setApproveSerno(approveSernoNew);
                //初始化更新人跟新机构，跟新日期等
                initInsertDomainProperties(lmtSigInvestAppr,lmtSigInvestBasicLmtApprOld.getInputId(),lmtSigInvestBasicLmtApprOld.getInputBrId()) ;
                lmtSigInvestBasicLmtApprMapper.insert(lmtSigInvestBasicLmtAppr) ;
            }
        }

        lmtSigInvestApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
        lmtSigInvestAppService.update(lmtSigInvestApp);
    }

    /**
     * @方法名称: generateSigInvestApprService
     * @方法描述: 根据流程审批表数据，获取最近的
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(rollbackFor=Exception.class)
    public void generateSigInvestApprService(LmtSigInvestApp lmtSigInvestApp, String issueReportType, String cur_next_id, String currentUserId, String currentOrgId) throws Exception{
        log.info("用信条件拷贝判断 开始==》cur_next_id【{}】",cur_next_id);
        String[] ingoreProperties = getApprIngoreProperties(cur_next_id);
        log.info("用信条件拷贝判断 结束==》ingoreProperties【{}】",Arrays.toString(ingoreProperties));
        //根据审批表，获取最新审批记录
        String serno = lmtSigInvestApp.getSerno() ;
        //根据流水号，查询最新的审批信息
        LmtSigInvestAppr lmtSigInvestApprOld = lmtSigInvestApprMapper.selectLmtSigInvestApprByApproveSerno(serno) ;
        //审批流水号
        String approveSerno = lmtSigInvestApprOld.getApproveSerno() ;
        //初始化审批主表对象
        LmtSigInvestAppr newLmtSigInvestAppr = new LmtSigInvestAppr() ;
        //数据拷贝
        BeanUtils.copyProperties(lmtSigInvestApprOld, newLmtSigInvestAppr,ingoreProperties);

        //判断是否拷贝 评审结论和结论性描述
        //1.通用规则：评审结论和结论性描述，都不copy至下一节点；
        //2.例外规则：
        //      流程审批的下一节点如果是信贷管理部风险派驻岗，
        //      则获取上一条审批记录中出具报告类型为审查报告的且ZH审批结论为空的数据，
        //      copy评审结论和结论性描述至生成的最新审批记录数据中
        String nextNodeId = cur_next_id.split(";")[1];
        if (CmisBizConstants.Xdglbfxpzg_NodeIds.contains(nextNodeId+",")){
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("serno",serno);
            BizInvestCommonService.checkParamsIsNull("serno",serno);
            queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
            queryModel.addCondition("issueReportType",CmisBizConstants.STD_ISSUE_REPORT_TYPE_01);
            queryModel.setSort(" createTime desc");
            List<LmtSigInvestAppr> lmtSigInvestApprs = selectAll(queryModel);
            if (CollectionUtils.isNotEmpty(lmtSigInvestApprs)){
                for (LmtSigInvestAppr a : lmtSigInvestApprs) {
                    if (StringUtils.isBlank(a.getReviewResultZh())){
                        newLmtSigInvestAppr.setRestDesc(a.getRestDesc());
                        newLmtSigInvestAppr.setRestDescZh(a.getRestDescZh());
                        newLmtSigInvestAppr.setReviewResultZh(a.getReviewResultZh());
                        newLmtSigInvestAppr.setReviewResult(a.getReviewResult());
                        break;
                    }
                }
                //拷贝上个审查报告相关信息
                LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprs.get(0);
                newLmtSigInvestAppr.setInteAnaly(lmtSigInvestAppr.getInteAnaly());
                newLmtSigInvestAppr.setInteAnalyZh(lmtSigInvestAppr.getInteAnalyZh());
            }
        }

        //初始化主键信息
        newLmtSigInvestAppr.setPkId(generatePkId());
        //初始化审批流水号
        newLmtSigInvestAppr.setApproveSerno(generateSerno(SeqConstant.INVEST_LMT_SEQ));
        //报告类型
        newLmtSigInvestAppr.setIssueReportType(issueReportType);
        //初始化更新人跟新机构，跟新日期等
        initInsertDomainPropertiesForWF(newLmtSigInvestAppr,currentUserId,currentOrgId) ;
        //数据导入
        lmtSigInvestApprMapper.insert(newLmtSigInvestAppr);
        if (ingoreProperties.length == 4){
            log.info("{}==={} 【{}】其他要求拷贝====》开始",newLmtSigInvestAppr.getApproveSerno(),lmtSigInvestApprOld.getApproveSerno(),serno);
            // 通过授信审批用信条件生成新的授信审批用信条件  add by zhangjw 20210827
            lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(newLmtSigInvestAppr.getApproveSerno(), lmtSigInvestApprOld.getApproveSerno());
            log.info("{}==={} 【{}】其他要求拷贝====》结束",newLmtSigInvestAppr.getApproveSerno(),lmtSigInvestApprOld.getApproveSerno(),serno);
        }
        //审批流水号
        String approveSernoNew = newLmtSigInvestAppr.getApproveSerno() ;
        /****** 根据审批流水号，获取对应单笔投资对应底层资产基本情况审批表 ********/
        LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoApprOld = lmtSigInvestBasicInfoApprService.selectByApproveSerno(approveSerno) ;
        if (lmtSigInvestBasicInfoApprOld!=null){
            LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = new LmtSigInvestBasicInfoAppr() ;
            //数据拷贝
            BeanUtils.copyProperties(lmtSigInvestBasicInfoApprOld, lmtSigInvestBasicInfoAppr);
            lmtSigInvestBasicInfoAppr.setPkId(generatePkId());
            //初始化审批流水号
            lmtSigInvestBasicInfoAppr.setApproveSerno(approveSernoNew);
            //初始化更新人跟新机构，跟新日期等
            initInsertDomainProperties(lmtSigInvestBasicInfoAppr,lmtSigInvestBasicInfoApprOld.getInputId(),lmtSigInvestBasicInfoApprOld.getInputBrId()) ;
            //数据插入
            lmtSigInvestBasicInfoApprMapper.insert(lmtSigInvestBasicInfoAppr) ;

//            /**
//             * 拷贝用信条件
//             */
//            // 通过授信审批用信条件生成新的授信审批用信条件  add by zhangjw 20210827
//            lmtApprLoanCondService.generateLmtApprLoanCondByCopyLast(approveSernoNew, lmtSigInvestBasicInfoApprOld.getApproveSerno());
        }

        /****** 根据审批流水号，获取对应单笔投资关联底层信息明细审批表 ********/
        List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprService.selectByApproveSerno(approveSerno) ;
        if (lmtSigInvestBasicInfoSubApprList!=null && lmtSigInvestBasicInfoSubApprList.size()>0){
            //遍历地缝明细审批，重新生成
            Supplier<LmtSigInvestBasicInfoSubAppr> supplierSubAppr = LmtSigInvestBasicInfoSubAppr :: new ;
            for (LmtSigInvestBasicInfoSubAppr sigInvestBasicInfoSubApprOld : lmtSigInvestBasicInfoSubApprList) {
                LmtSigInvestBasicInfoSubAppr lmtSigInvestBasicInfoSubAppr = supplierSubAppr.get() ;
                //数据拷贝
                BeanUtils.copyProperties(sigInvestBasicInfoSubApprOld, lmtSigInvestBasicInfoSubAppr);
                lmtSigInvestBasicInfoSubAppr.setPkId(generatePkId());
                //初始化批复流水号
                lmtSigInvestBasicInfoSubAppr.setApproveSerno(approveSernoNew);
                //初始化更新人跟新机构，跟新日期等
                initInsertDomainProperties(lmtSigInvestBasicInfoSubAppr,sigInvestBasicInfoSubApprOld.getInputId(),sigInvestBasicInfoSubApprOld.getInputBrId()) ;
                lmtSigInvestBasicInfoSubApprMapper.insert(lmtSigInvestBasicInfoSubAppr) ;
            }
        }

        /****** 根据审批流水号，获取对应底层资产额度审批表 ********/
        List<LmtSigInvestBasicLmtAppr> lmtSigInvestBasicLmtApprs = lmtSigInvestBasicLmtApprService.selectByApproveSerno(approveSerno);
        if (lmtSigInvestBasicLmtApprs!=null && lmtSigInvestBasicLmtApprs.size()>0){
            Supplier<LmtSigInvestBasicLmtAppr> supplierBasicLmtAppr = LmtSigInvestBasicLmtAppr :: new ;
            for (LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtApprOld : lmtSigInvestBasicLmtApprs) {
                LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr = supplierBasicLmtAppr.get() ;
                BeanUtils.copyProperties(lmtSigInvestBasicLmtApprOld, lmtSigInvestBasicLmtAppr);
                lmtSigInvestBasicLmtAppr.setPkId(generatePkId());
                //初始化批复流水号
                lmtSigInvestBasicLmtAppr.setApproveSerno(approveSernoNew);
                //初始化更新人跟新机构，跟新日期等
                initInsertDomainProperties(lmtSigInvestBasicLmtAppr,lmtSigInvestBasicLmtApprOld.getInputId(),lmtSigInvestBasicLmtApprOld.getInputBrId()) ;
                lmtSigInvestBasicLmtApprMapper.insert(lmtSigInvestBasicLmtAppr) ;
            }
        }


        lmtSigInvestApp.setApproveStatus(CmisBizConstants.APPLY_STATE_APP);
        lmtSigInvestAppService.update(lmtSigInvestApp);
    }

    /**
     * 更新核查报告文件路径
     * @param condition
     * @return
     */
    public String updateFilePath(Map condition) {
        String fileId = (String) condition.get("fileId");
        String pkId = (String) condition.get("pkId");
        String indgtReportMode = (String) condition.get("indgtReportMode");
        String serverPath = (String) condition.get("serverPath");

        //获取文件绝对路径
        String fileAbsolutePath = " ";
        String relativePath = "/LmtSigInvest/"+getCurrrentDateStr();
        if (!StringUtils.isBlank(fileId)){
            fileAbsolutePath = getFileAbsolutePath(fileId,serverPath,relativePath,sftpFileSystemTemplate,false);
        }

        LmtSigInvestAppr lmtSigInvestAppr = new LmtSigInvestAppr();
        lmtSigInvestAppr.setPkId(pkId);
        lmtSigInvestAppr.setIndgtReportMode(indgtReportMode);
        lmtSigInvestAppr.setIndgtReportPath(fileAbsolutePath);
        updateSelective(lmtSigInvestAppr);
        return fileAbsolutePath;
    }

    /**
     * 获取最新审批表数据
     * @param serno
     * @return
     */
    public LmtSigInvestAppr selectLastBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setSort(" createTime desc");
        queryModel.setPage(1);
        queryModel.setSize(1);

        List<LmtSigInvestAppr> lmtSigInvestApprs = selectByModel(queryModel);
        if (lmtSigInvestApprs!=null && lmtSigInvestApprs.size()>0){
            return lmtSigInvestApprs.get(0);
        }
        return null;
    }

    /**
     * 上調權限修改
     * @param lmtSigInvestApprMap
     * @return
     */
    public Integer updateUpApprAuth(Map lmtSigInvestApprMap) {
        LmtSigInvestAppr lmtSigInvestApprL = new LmtSigInvestAppr();
        String instanceId = (String) lmtSigInvestApprMap.get("instanceId");
        mapToBean(lmtSigInvestApprMap,lmtSigInvestApprL);

        WFBizParamDto param = new WFBizParamDto();
        param.setBizId(lmtSigInvestApprL.getSerno());
        param.setInstanceId(instanceId);//
        Map<String, Object> params = new HashMap<>();
        params.put("isUpAppr",lmtSigInvestApprL.getIsUpperApprAuth());
        param.setParam(params);
        workflowCoreClient.updateFlowParam(param);

        return updateSelective(lmtSigInvestApprL);
    }
}
