/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarWarrantRenewApp
 * @类描述: guar_warrant_renew_app数据实体类
 * @功能描述: 
 * @创建人: lizhaoxing
 * @创建时间: 2021-04-14 16:47:49
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_warrant_renew_app")
public class GuarWarrantRenewApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 核心担保编号 **/
	@Column(name = "CORE_GRT_NO", unique = false, nullable = true, length = 40)
	private String coreGrtNo;
	
	/** 押品编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 押品名称 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 100)
	private String pldimnMemo;
	
	/** 押品类型 **/
	@Column(name = "GUAR_TYPE", unique = false, nullable = true, length = 20)
	private String guarType;
	
	/** 权利价值 **/
	@Column(name = "CERTI_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal certiAmt;
	
	/** 抵押人编号 **/
	@Column(name = "GUAR_CUS_ID", unique = false, nullable = true, length = 40)
	private String guarCusId;
	
	/** 抵押人 **/
	@Column(name = "GUAR_CUS_NAME", unique = false, nullable = true, length = 100)
	private String guarCusName;
	
	/** 账务机构 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 20)
	private String finaBrId;
	
	/** 权证状态 **/
	@Column(name = "WARRANT_STATE", unique = false, nullable = true, length = 10)
	private String warrantState;
	
	/** 权证出库日期 **/
	@Column(name = "WARRANT_OUT_DATE", unique = false, nullable = true, length = 10)
	private String warrantOutDate;
	
	/** 经办人 **/
	@Column(name = "HUSER", unique = false, nullable = true, length = 20)
	private String huser;
	
	/** 经办机构 **/
	@Column(name = "HORG", unique = false, nullable = true, length = 20)
	private String horg;
	
	/** 是否发送集中作业中心 **/
	@Column(name = "IS_SEND", unique = false, nullable = true, length = 10)
	private String isSend;
	
	/** 出库类型 **/
	@Column(name = "OUT_TYPE", unique = false, nullable = true, length = 20)
	private String outType;
	
	/** 权证出库类型 **/
	@Column(name = "WARRANT_OUT_TYPE", unique = false, nullable = true, length = 20)
	private String warrantOutType;
	
	/** 归还时间 **/
	@Column(name = "BACK_DATE", unique = false, nullable = true, length = 20)
	private String backDate;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 40)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coreGrtNo
	 */
	public void setCoreGrtNo(String coreGrtNo) {
		this.coreGrtNo = coreGrtNo;
	}
	
    /**
     * @return coreGrtNo
     */
	public String getCoreGrtNo() {
		return this.coreGrtNo;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	
    /**
     * @return guarType
     */
	public String getGuarType() {
		return this.guarType;
	}
	
	/**
	 * @param certiAmt
	 */
	public void setCertiAmt(java.math.BigDecimal certiAmt) {
		this.certiAmt = certiAmt;
	}
	
    /**
     * @return certiAmt
     */
	public java.math.BigDecimal getCertiAmt() {
		return this.certiAmt;
	}
	
	/**
	 * @param guarCusId
	 */
	public void setGuarCusId(String guarCusId) {
		this.guarCusId = guarCusId;
	}
	
    /**
     * @return guarCusId
     */
	public String getGuarCusId() {
		return this.guarCusId;
	}
	
	/**
	 * @param guarCusName
	 */
	public void setGuarCusName(String guarCusName) {
		this.guarCusName = guarCusName;
	}
	
    /**
     * @return guarCusName
     */
	public String getGuarCusName() {
		return this.guarCusName;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param warrantState
	 */
	public void setWarrantState(String warrantState) {
		this.warrantState = warrantState;
	}
	
    /**
     * @return warrantState
     */
	public String getWarrantState() {
		return this.warrantState;
	}
	
	/**
	 * @param warrantOutDate
	 */
	public void setWarrantOutDate(String warrantOutDate) {
		this.warrantOutDate = warrantOutDate;
	}
	
    /**
     * @return warrantOutDate
     */
	public String getWarrantOutDate() {
		return this.warrantOutDate;
	}
	
	/**
	 * @param huser
	 */
	public void setHuser(String huser) {
		this.huser = huser;
	}
	
    /**
     * @return huser
     */
	public String getHuser() {
		return this.huser;
	}
	
	/**
	 * @param horg
	 */
	public void setHorg(String horg) {
		this.horg = horg;
	}
	
    /**
     * @return horg
     */
	public String getHorg() {
		return this.horg;
	}
	
	/**
	 * @param isSend
	 */
	public void setIsSend(String isSend) {
		this.isSend = isSend;
	}
	
    /**
     * @return isSend
     */
	public String getIsSend() {
		return this.isSend;
	}
	
	/**
	 * @param outType
	 */
	public void setOutType(String outType) {
		this.outType = outType;
	}
	
    /**
     * @return outType
     */
	public String getOutType() {
		return this.outType;
	}
	
	/**
	 * @param warrantOutType
	 */
	public void setWarrantOutType(String warrantOutType) {
		this.warrantOutType = warrantOutType;
	}
	
    /**
     * @return warrantOutType
     */
	public String getWarrantOutType() {
		return this.warrantOutType;
	}
	
	/**
	 * @param backDate
	 */
	public void setBackDate(String backDate) {
		this.backDate = backDate;
	}
	
    /**
     * @return backDate
     */
	public String getBackDate() {
		return this.backDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}