/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.RptCptlSituLegalRepreLoan;
import cn.com.yusys.yusp.repository.mapper.RptCptlSituLegalRepreLoanMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituLegalRepreLoanService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-18 09:49:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RptCptlSituLegalRepreLoanService {

    @Autowired
    private RptCptlSituLegalRepreLoanMapper rptCptlSituLegalRepreLoanMapper;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public RptCptlSituLegalRepreLoan selectByPrimaryKey(String pkId) {
        return rptCptlSituLegalRepreLoanMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RptCptlSituLegalRepreLoan> selectAll(QueryModel model) {
        List<RptCptlSituLegalRepreLoan> records = (List<RptCptlSituLegalRepreLoan>) rptCptlSituLegalRepreLoanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<RptCptlSituLegalRepreLoan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RptCptlSituLegalRepreLoan> list = rptCptlSituLegalRepreLoanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(RptCptlSituLegalRepreLoan record) {
        return rptCptlSituLegalRepreLoanMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(RptCptlSituLegalRepreLoan record) {
        return rptCptlSituLegalRepreLoanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(RptCptlSituLegalRepreLoan record) {
        return rptCptlSituLegalRepreLoanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(RptCptlSituLegalRepreLoan record) {
        return rptCptlSituLegalRepreLoanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return rptCptlSituLegalRepreLoanMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return rptCptlSituLegalRepreLoanMapper.deleteByIds(ids);
    }

    public List<Map<String,Object>> selectGrpRepreLoan(QueryModel model){
        String grpSerno = model.getCondition().get("serno").toString();
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        List<LmtGrpMemRel> lmtGrpMemRelList = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSerno(grpSerno);
        for (LmtGrpMemRel lmtGrpMemRel : lmtGrpMemRelList) {
            String serno = lmtGrpMemRel.getSingleSerno();
            String cusName = lmtGrpMemRel.getCusName();
            if(StringUtils.isBlank(serno)){
                return new ArrayList<>();
            }
            List<RptCptlSituLegalRepreLoan> rptCptlSituLegalRepreLoanList = rptCptlSituLegalRepreLoanMapper.selectBySerno(serno);
            for (RptCptlSituLegalRepreLoan rptCptlSituLegalRepreLoan : rptCptlSituLegalRepreLoanList) {
                Map<String, Object> map = (Map<String, Object>) BeanUtils.beanToMap(rptCptlSituLegalRepreLoan);
                map.put("cusName", cusName);
                result.add(map);
            }
        }
        return result;
    }
    public List<RptCptlSituLegalRepreLoan> selectBySerno(String serno){
        return rptCptlSituLegalRepreLoanMapper.selectBySerno(serno);
    }
}
