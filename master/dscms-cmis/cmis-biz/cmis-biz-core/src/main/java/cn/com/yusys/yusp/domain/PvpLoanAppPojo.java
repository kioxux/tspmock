package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @创建人 WH
 * @创建时间 2021/5/17 13:48
 * @注释 因为零售和小微基本信息需要额外的参数值 固在此处扩展返回对象
 */
public class PvpLoanAppPojo extends BaseDomain {

    //合同类型
    private String contType;
    //纸质合同签订日期
    private String paperContSignDate;

    //还款日
    private Integer repayDay;



    /**
     * 放款流水号
     **/

    //@Column(name = "PVP_SERNO")
    private String pvpSerno;

    /**
     * 业务申请流水号
     **/
    //@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
    private String iqpSerno;

    /**
     * 借据编号
     **/
    //@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
    private String billNo;

    /**
     * 合同编号
     **/
    //@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
    private String contNo;

    /**
     * 调查编号
     **/
    //@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
    private String surveySerno;

    /**
     * 中文合同编号
     **/
    //@Column(name = "CONT_CN_NO", unique = false, nullable = true, length = 80)
    private String contCnNo;

    /**
     * 客户编号
     **/
    //@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
    private String cusId;

    /**
     * 客户名称
     **/
    //@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
    private String cusName;

    /**
     * 产品编号
     **/
    //@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
    private String prdId;

    /**
     * 产品名称
     **/
    //@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
    private String prdName;

    /**
     * 贷款形式
     **/
    //@Column(name = "LOAN_MODAL", unique = false, nullable = true, length = 5)
    private String loanModal;

    /**
     * 借新还旧类型
     **/
    //@Column(name = "REFINANCING_TYPE", unique = false, nullable = true, length = 5)
    private String refinancingType;

    /**
     * FTP剔除考核
     **/
    //@Column(name = "FTP", unique = false, nullable = true, length = 5)
    private String ftp;

    /**
     * 是否为疫情相关企业
     **/
    //@Column(name = "IS_EPIDEMIC_CORRE_CON", unique = false, nullable = true, length = 5)
    private String isEpidemicCorreCon;

    /**
     * 出账模式
     **/
    //@Column(name = "PVP_MODE", unique = false, nullable = true, length = 5)
    private String pvpMode;

    /**
     * 合同影像是否审核
     **/
    //@Column(name = "IS_CONT_IMAGE_AUDIT", unique = false, nullable = true, length = 5)
    private String isContImageAudit;

    /**
     * 合同币种
     **/
    //@Column(name = "CONT_CUR_TYPE", unique = false, nullable = true, length = 5)
    private String contCurType;

    /**
     * 合同最高可放金额
     **/
    //@Column(name = "CONT_HIGH_DISB", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal contHighDisb;

    /**
     * 起始日
     **/
    //@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
    private String startDate;

    /**
     * 到期日
     **/
    //@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
    private String endDate;

    /**
     * 是否用印
     **/
    //@Column(name = "IS_SEAL", unique = false, nullable = true, length = 5)
    private String isSeal;

    /**
     * 贷款起始日
     **/
    //@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
    private String loanStartDate;

    /**
     * 贷款到期日
     **/
    //@Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
    private String loanEndDate;

    /**
     * 贷款期限
     **/
    //@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 20)
    private String loanTerm;

    /**
     * 贷款期限单位
     **/
    //@Column(name = "LOAN_TERM_UNIT", unique = false, nullable = true, length = 20)
    private String loanTermUnit;

    /**
     * 利率调整方式
     **/
    //@Column(name = "RATE_ADJ_MODE", unique = false, nullable = true, length = 5)
    private String rateAdjMode;

    /**
     * 是否分段计息
     **/
    //@Column(name = "IS_SEG_INTEREST", unique = false, nullable = true, length = 5)
    private String isSegInterest;

    /**
     * LPR授信利率区间
     **/
    //@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
    private String lprRateIntval;

    /**
     * 当前LPR利率
     **/
    //@Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal curtLprRate;

    /**
     * 浮动点数
     **/
    //@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal rateFloatPoint;

    /**
     * 逾期利率浮动比
     **/
    //@Column(name = "OVERDUE_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal overdueRatePefloat;

    /**
     * 逾期执行利率(年利率)
     **/
    //@Column(name = "OVERDUE_EXEC_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal overdueExecRate;

    /**
     * 复息利率浮动比
     **/
    //@Column(name = "CI_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal ciRatePefloat;

    /**
     * 复息执行利率(年利率)
     **/
    //@Column(name = "CI_EXEC_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal ciExecRate;

    /**
     * 利率调整选项
     **/
    //@Column(name = "RATE_ADJ_TYPE", unique = false, nullable = true, length = 5)
    private String rateAdjType;

    /**
     * 下一次利率调整间隔
     **/
    //@Column(name = "NEXT_RATE_ADJ_INTERVAL", unique = false, nullable = true, length = 10)
    private String nextRateAdjInterval;

    /**
     * 下一次利率调整间隔单位
     **/
    //@Column(name = "NEXT_RATE_ADJ_UNIT", unique = false, nullable = true, length = 5)
    private String nextRateAdjUnit;

    /**
     * 第一次调整日
     **/
    //@Column(name = "FIRST_ADJ_DATE", unique = false, nullable = true, length = 20)
    private String firstAdjDate;

    /**
     * 还款方式
     **/
    //@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
    private String repayMode;

    /**
     * 结息间隔周期
     **/
    //@Column(name = "EI_INTERVAL_CYCLE", unique = false, nullable = true, length = 10)
    private String eiIntervalCycle;

    /**
     * 结息间隔周期单位
     **/
    //@Column(name = "EI_INTERVAL_UNIT", unique = false, nullable = true, length = 5)
    private String eiIntervalUnit;

    /**
     * 扣款方式
     **/
    //@Column(name = "DEDUCT_TYPE", unique = false, nullable = true, length = 5)
    private String deductType;

    /**
     * 扣款日
     **/
    //@Column(name = "DEDUCT_DAY", unique = false, nullable = true, length = 20)
    private String deductDay;

    /**
     * 贷款发放账号
     **/
    //@Column(name = "LOAN_PAYOUT_ACCNO", unique = false, nullable = true, length = 20)
    private String loanPayoutAccno;

    /**
     * 贷款发放账号子序号
     **/
    //@Column(name = "LOAN_PAYOUT_SUB_NO", unique = false, nullable = true, length = 80)
    private String loanPayoutSubNo;

    /**
     * 发放账号名称
     **/
    //@Column(name = "PAYOUT_ACCT_NAME", unique = false, nullable = true, length = 80)
    private String payoutAcctName;

    /**
     * 是否受托支付
     **/
    //@Column(name = "IS_BE_ENTRUSTED_PAY", unique = false, nullable = true, length = 5)
    private String isBeEntrustedPay;

    /**
     * 贷款还款账号
     **/
    //@Column(name = "REPAY_ACCNO", unique = false, nullable = true, length = 80)
    private String repayAccno;

    /**
     * 贷款还款账户子序号
     **/
    //@Column(name = "REPAY_SUB_ACCNO", unique = false, nullable = true, length = 80)
    private String repaySubAccno;

    /**
     * 还款账户名称
     **/
    //@Column(name = "REPAY_ACCT_NAME", unique = false, nullable = true, length = 80)
    private String repayAcctName;

    /**
     * 贷款承诺标志
     **/
    //@Column(name = "LOAN_PROMISE_FLAG", unique = false, nullable = true, length = 5)
    private String loanPromiseFlag;

    /**
     * 贷款承诺类型
     **/
    //@Column(name = "LOAN_PROMISE_TYPE", unique = false, nullable = true, length = 5)
    private String loanPromiseType;

    /**
     * 贴息人存款账号
     **/
    //@Column(name = "SBSY_DEP_ACCNO", unique = false, nullable = true, length = 40)
    private String sbsyDepAccno;

    /**
     * 贴息比例
     **/
    //@Column(name = "SBSY_PERC", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal sbsyPerc;

    /**
     * 贴息到期日
     **/
    //@Column(name = "SBYS_ENDDATE", unique = false, nullable = true, length = 20)
    private String sbysEnddate;

    /**
     * 是否使用授信额度
     **/
    //@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
    private String isUtilLmt;

    /**
     * 授信额度编号
     **/
    //@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
    private String lmtAccNo;

    /**
     * 批复编号
     **/
    //@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
    private String replyNo;

    /**
     * 贷款类别
     **/
    //@Column(name = "LOAN_TYPE", unique = false, nullable = true, length = 5)
    private String loanTypeDetail;

    /**
     * 是否落实贷款
     **/
    //@Column(name = "IS_PACT_LOAN", unique = false, nullable = true, length = 5)
    private String isPactLoan;

    /**
     * 是否绿色产业
     **/
    //@Column(name = "IS_GREEN_INDUSTRY", unique = false, nullable = true, length = 5)
    private String isGreenIndustry;

    /**
     * 是否经营性物业贷款
     **/
    //@Column(name = "IS_OPER_PROPERTY_LOAN", unique = false, nullable = true, length = 5)
    private String isOperPropertyLoan;

    /**
     * 是否钢贸行业贷款
     **/
    //@Column(name = "IS_STEEL_LOAN", unique = false, nullable = true, length = 5)
    private String isSteelLoan;

    /**
     * 是否不锈钢行业贷款
     **/
    //@Column(name = "IS_STAINLESS_LOAN", unique = false, nullable = true, length = 5)
    private String isStainlessLoan;

    /**
     * 是否扶贫贴息贷款
     **/
    //@Column(name = "IS_POVERTY_RELIEF_LOAN", unique = false, nullable = true, length = 5)
    private String isPovertyReliefLoan;

    /**
     * 是否劳动密集型小企业贴息贷款
     **/
    //@Column(name = "IS_LABOR_INTEN_SBSY_LOAN", unique = false, nullable = true, length = 5)
    private String isLaborIntenSbsyLoan;

    /**
     * 保障性安居工程贷款
     **/
    //@Column(name = "GOVER_SUBSZ_HOUSE_LOAN", unique = false, nullable = true, length = 10)
    private String goverSubszHouseLoan;

    /**
     * 项目贷款节能环保
     **/
    //@Column(name = "ENGY_ENVI_PROTE_LOAN", unique = false, nullable = true, length = 10)
    private String engyEnviProteLoan;

    /**
     * 是否农村综合开发贷款标志
     **/
    //@Column(name = "IS_CPHS_RUR_DELP_LOAN", unique = false, nullable = true, length = 5)
    private String isCphsRurDelpLoan;

    /**
     * 房地产贷款
     **/
    //@Column(name = "REALPRO_LOAN", unique = false, nullable = true, length = 5)
    private String realproLoan;

    /**
     * 房产开发贷款资本金比例
     **/
    //@Column(name = "REALPRO_LOAN_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal realproLoanRate;

    /**
     * 账务机构编号
     **/
    //@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 20)
    private String finaBrId;

    /**
     * 账务机构名称
     **/
    //@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 40)
    private String finaBrIdName;

    /**
     * 放款机构编号
     **/
    //@Column(name = "DISB_ORG_NO", unique = false, nullable = true, length = 20)
    private String disbOrgNo;

    /**
     * 放款机构名称
     **/
    //@Column(name = "DISB_ORG_NAME", unique = false, nullable = true, length = 40)
    private String disbOrgName;

    /**
     * 贷款担保方式
     **/
    //@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
    private String guarMode;

    /**
     * 农户类型
     **/
    //@Column(name = "AGRI_TYPE", unique = false, nullable = true, length = 10)
    private String agriType;

    /**
     * 涉农贷款投向
     **/
    //@Column(name = "AGRI_LOAN_TER", unique = false, nullable = true, length = 10)
    private String agriLoanTer;

    /**
     * 是否资料补充
     **/
    //@Column(name = "IS_MATER_COMP", unique = false, nullable = true, length = 1)
    private String isMaterComp;

    /**
     * 担保方式明细
     **/
    //@Column(name = "GUAR_DETAIL_MODE", unique = false, nullable = true, length = 500)
    private String guarDetailMode;

    /**
     * 贷款投向
     **/
    //@Column(name = "LOAN_TER", unique = false, nullable = true, length = 20)
    private String loanTer;

    /**
     * 贷款科目号
     **/
    //@Column(name = "LOAN_SUBJECT_NO", unique = false, nullable = true, length = 20)
    private String loanSubjectNo;

    /**
     * 借款用途类型
     **/
    //@Column(name = "LOAN_USE_TYPE", unique = false, nullable = true, length = 10)
    private String loanUseType;

    /**
     * 产业结构类型
     **/
    //@Column(name = "ESTATE_TYPE", unique = false, nullable = true, length = 10)
    private String estateType;

    /**
     * 工业结构转型升级标识
     **/
    //@Column(name = "INDT_UP_FLAG", unique = false, nullable = true, length = 10)
    private String indtUpFlag;

    /**
     * 战略新兴产业类型
     **/
    //@Column(name = "STRATEGY_NEW_LOAN", unique = false, nullable = true, length = 10)
    private String strategyNewLoan;

    /**
     * 文化产业标识
     **/
    //@Column(name = "CUL_INDUSTRY_FLAG", unique = false, nullable = true, length = 10)
    private String culIndustryFlag;

    /**
     * 本金自动归还标志
     **/
    //@Column(name = "CAP_AUTOBACK_FLAG", unique = false, nullable = true, length = 10)
    private String capAutobackFlag;

    /**
     * 是否贴息
     **/
    //@Column(name = "IS_SBSY", unique = false, nullable = true, length = 1)
    private String isSbsy;

    /**
     * 是否计复息
     **/
    //@Column(name = "IS_METER_CI", unique = false, nullable = true, length = 1)
    private String isMeterCi;

    /**
     * 利息自动归还标志
     **/
    //@Column(name = "INT_AUTOBACK_FLAG", unique = false, nullable = true, length = 10)
    private String intAutobackFlag;

    /**
     * 币种
     **/
    //@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
    private String curType;

    /**
     * 合同金额
     **/
    //@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal contAmt;

    /**
     * 放款金额
     **/
    //@Column(name = "PVP_AMT", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal pvpAmt;

    /**
     * 支付方式
     **/
    //@Column(name = "PAY_MODE", unique = false, nullable = true, length = 5)
    private String payMode;

    /**
     * 是否立即发起受托支付
     **/
    //@Column(name = "IS_CFIRM_PAY", unique = false, nullable = true, length = 2)
    private String isCfirmPay;

    /**
     * 期限类型
     **/
    //@Column(name = "TERM_TYPE", unique = false, nullable = true, length = 5)
    private String termType;

    /**
     * 申请期限
     **/
    //@Column(name = "APP_TERM", unique = false, nullable = true, length = 10)
    private String appTerm;

    /**
     * 客户评级系数
     **/
    //@Column(name = "CUSTOMER_RATING_FACTOR", unique = false, nullable = true, length = 5)
    private java.math.BigDecimal customerRatingFactor;

    /**
     * 执行年利率
     **/
    //@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal execRateYear;

    /**
     * 授权状态
     **/
    //@Column(name = "AUTH_STATUS", unique = false, nullable = true, length = 5)
    private String authStatus;

    /**
     * 渠道来源
     **/
    //@Column(name = "CHNL_SOUR", unique = false, nullable = true, length = 5)
    private String chnlSour;

    /**
     * 放款机构
     **/
    //@Column(name = "ACCT_BR_ID", unique = false, nullable = true, length = 20)
    private String acctBrId;

    /**
     * 申请状态
     **/
    //@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 操作类型
     **/
    //@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;

    /**
     * 所属条线
     **/
    //@Column(name = "BELG_LINE", unique = false, nullable = true, length = 40)
    private String belgLine;

    /**
     * 主管机构
     **/
    //@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 主管客户经理
     **/
    //@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 登记人
     **/
    //@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    //@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    //@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 最后修改人
     **/
    //@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    //@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    //@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 创建时间
     **/
    //@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date createTime;

    /**
     * 修改时间
     **/
    //@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
    private java.util.Date updateTime;

    /**
     * 是否先放款后抵押
     **/
    //@Column(name = "BEFOREHAND_IND", unique = false, nullable = true, length = 5)
    private String beforehandInd;

    /**
     * 借款利率调整日
     **/
    //@Column(name = "LOAN_RATE_ADJ_DAY", unique = false, nullable = true, length = 5)
    private String loanRateAdjDay;

    /**
     * 正常利率浮动方式
     **/
    //@Column(name = "IR_FLOAT_TYPE", unique = false, nullable = true, length = 5)
    private String irFloatType;

    /**
     * 利率浮动百分比
     **/
    //@Column(name = "IR_FLOAT_RATE", unique = false, nullable = true, length = 16)
    private java.math.BigDecimal irFloatRate;




    @Override
    public String toString() {
        return "PvpLoanAppPojo{" +
                "pvpSerno='" + pvpSerno + '\'' +
                ", iqpSerno='" + iqpSerno + '\'' +
                ", billNo='" + billNo + '\'' +
                ", contNo='" + contNo + '\'' +
                ", surveySerno='" + surveySerno + '\'' +
                ", contCnNo='" + contCnNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", loanModal='" + loanModal + '\'' +
                ", refinancingType='" + refinancingType + '\'' +
                ", ftp='" + ftp + '\'' +
                ", isEpidemicCorreCon='" + isEpidemicCorreCon + '\'' +
                ", pvpMode='" + pvpMode + '\'' +
                ", isContImageAudit='" + isContImageAudit + '\'' +
                ", contCurType='" + contCurType + '\'' +
                ", contHighDisb=" + contHighDisb +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", isSeal='" + isSeal + '\'' +
                ", loanStartDate='" + loanStartDate + '\'' +
                ", loanEndDate='" + loanEndDate + '\'' +
                ", loanTerm='" + loanTerm + '\'' +
                ", loanTermUnit='" + loanTermUnit + '\'' +
                ", rateAdjMode='" + rateAdjMode + '\'' +
                ", isSegInterest='" + isSegInterest + '\'' +
                ", lprRateIntval='" + lprRateIntval + '\'' +
                ", curtLprRate=" + curtLprRate +
                ", rateFloatPoint=" + rateFloatPoint +
                ", overdueRatePefloat=" + overdueRatePefloat +
                ", overdueExecRate=" + overdueExecRate +
                ", ciRatePefloat=" + ciRatePefloat +
                ", ciExecRate=" + ciExecRate +
                ", rateAdjType='" + rateAdjType + '\'' +
                ", nextRateAdjInterval='" + nextRateAdjInterval + '\'' +
                ", nextRateAdjUnit='" + nextRateAdjUnit + '\'' +
                ", firstAdjDate='" + firstAdjDate + '\'' +
                ", repayMode='" + repayMode + '\'' +
                ", eiIntervalCycle='" + eiIntervalCycle + '\'' +
                ", eiIntervalUnit='" + eiIntervalUnit + '\'' +
                ", deductType='" + deductType + '\'' +
                ", deductDay='" + deductDay + '\'' +
                ", loanPayoutAccno='" + loanPayoutAccno + '\'' +
                ", loanPayoutSubNo='" + loanPayoutSubNo + '\'' +
                ", payoutAcctName='" + payoutAcctName + '\'' +
                ", isBeEntrustedPay='" + isBeEntrustedPay + '\'' +
                ", repayAccno='" + repayAccno + '\'' +
                ", repaySubAccno='" + repaySubAccno + '\'' +
                ", repayAcctName='" + repayAcctName + '\'' +
                ", loanPromiseFlag='" + loanPromiseFlag + '\'' +
                ", loanPromiseType='" + loanPromiseType + '\'' +
                ", sbsyDepAccno='" + sbsyDepAccno + '\'' +
                ", sbsyPerc=" + sbsyPerc +
                ", sbysEnddate='" + sbysEnddate + '\'' +
                ", isUtilLmt='" + isUtilLmt + '\'' +
                ", lmtAccNo='" + lmtAccNo + '\'' +
                ", replyNo='" + replyNo + '\'' +
                ", loanTypeDetail='" + loanTypeDetail + '\'' +
                ", isPactLoan='" + isPactLoan + '\'' +
                ", isGreenIndustry='" + isGreenIndustry + '\'' +
                ", isOperPropertyLoan='" + isOperPropertyLoan + '\'' +
                ", isSteelLoan='" + isSteelLoan + '\'' +
                ", isStainlessLoan='" + isStainlessLoan + '\'' +
                ", isPovertyReliefLoan='" + isPovertyReliefLoan + '\'' +
                ", isLaborIntenSbsyLoan='" + isLaborIntenSbsyLoan + '\'' +
                ", goverSubszHouseLoan='" + goverSubszHouseLoan + '\'' +
                ", engyEnviProteLoan='" + engyEnviProteLoan + '\'' +
                ", isCphsRurDelpLoan='" + isCphsRurDelpLoan + '\'' +
                ", realproLoan='" + realproLoan + '\'' +
                ", realproLoanRate=" + realproLoanRate +
                ", finaBrId='" + finaBrId + '\'' +
                ", finaBrIdName='" + finaBrIdName + '\'' +
                ", disbOrgNo='" + disbOrgNo + '\'' +
                ", disbOrgName='" + disbOrgName + '\'' +
                ", guarMode='" + guarMode + '\'' +
                ", agriType='" + agriType + '\'' +
                ", agriLoanTer='" + agriLoanTer + '\'' +
                ", isMaterComp='" + isMaterComp + '\'' +
                ", guarDetailMode='" + guarDetailMode + '\'' +
                ", loanTer='" + loanTer + '\'' +
                ", loanSubjectNo='" + loanSubjectNo + '\'' +
                ", loanUseType='" + loanUseType + '\'' +
                ", estateType='" + estateType + '\'' +
                ", indtUpFlag='" + indtUpFlag + '\'' +
                ", strategyNewLoan='" + strategyNewLoan + '\'' +
                ", culIndustryFlag='" + culIndustryFlag + '\'' +
                ", capAutobackFlag='" + capAutobackFlag + '\'' +
                ", isSbsy='" + isSbsy + '\'' +
                ", isMeterCi='" + isMeterCi + '\'' +
                ", intAutobackFlag='" + intAutobackFlag + '\'' +
                ", curType='" + curType + '\'' +
                ", contAmt=" + contAmt +
                ", pvpAmt=" + pvpAmt +
                ", payMode='" + payMode + '\'' +
                ", isCfirmPay='" + isCfirmPay + '\'' +
                ", termType='" + termType + '\'' +
                ", appTerm='" + appTerm + '\'' +
                ", customerRatingFactor=" + customerRatingFactor +
                ", execRateYear=" + execRateYear +
                ", authStatus='" + authStatus + '\'' +
                ", chnlSour='" + chnlSour + '\'' +
                ", acctBrId='" + acctBrId + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", oprType='" + oprType + '\'' +
                ", belgLine='" + belgLine + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", managerId='" + managerId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", beforehandInd='" + beforehandInd + '\'' +
                ", loanRateAdjDay='" + loanRateAdjDay + '\'' +
                ", irFloatType='" + irFloatType + '\'' +
                ", irFloatRate=" + irFloatRate +
                ", contType='" + contType + '\'' +
                ", paperContSignDate='" + paperContSignDate + '\'' +
                ", repayDay=" + repayDay +
                '}';
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public String getContCnNo() {
        return contCnNo;
    }

    public void setContCnNo(String contCnNo) {
        this.contCnNo = contCnNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getRefinancingType() {
        return refinancingType;
    }

    public void setRefinancingType(String refinancingType) {
        this.refinancingType = refinancingType;
    }

    public String getFtp() {
        return ftp;
    }

    public void setFtp(String ftp) {
        this.ftp = ftp;
    }

    public String getIsEpidemicCorreCon() {
        return isEpidemicCorreCon;
    }

    public void setIsEpidemicCorreCon(String isEpidemicCorreCon) {
        this.isEpidemicCorreCon = isEpidemicCorreCon;
    }

    public String getPvpMode() {
        return pvpMode;
    }

    public void setPvpMode(String pvpMode) {
        this.pvpMode = pvpMode;
    }

    public String getIsContImageAudit() {
        return isContImageAudit;
    }

    public void setIsContImageAudit(String isContImageAudit) {
        this.isContImageAudit = isContImageAudit;
    }

    public String getContCurType() {
        return contCurType;
    }

    public void setContCurType(String contCurType) {
        this.contCurType = contCurType;
    }

    public BigDecimal getContHighDisb() {
        return contHighDisb;
    }

    public void setContHighDisb(BigDecimal contHighDisb) {
        this.contHighDisb = contHighDisb;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getIsSeal() {
        return isSeal;
    }

    public void setIsSeal(String isSeal) {
        this.isSeal = isSeal;
    }

    public String getLoanStartDate() {
        return loanStartDate;
    }

    public void setLoanStartDate(String loanStartDate) {
        this.loanStartDate = loanStartDate;
    }

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    public String getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(String loanTerm) {
        this.loanTerm = loanTerm;
    }

    public String getLoanTermUnit() {
        return loanTermUnit;
    }

    public void setLoanTermUnit(String loanTermUnit) {
        this.loanTermUnit = loanTermUnit;
    }

    public String getRateAdjMode() {
        return rateAdjMode;
    }

    public void setRateAdjMode(String rateAdjMode) {
        this.rateAdjMode = rateAdjMode;
    }

    public String getIsSegInterest() {
        return isSegInterest;
    }

    public void setIsSegInterest(String isSegInterest) {
        this.isSegInterest = isSegInterest;
    }

    public String getLprRateIntval() {
        return lprRateIntval;
    }

    public void setLprRateIntval(String lprRateIntval) {
        this.lprRateIntval = lprRateIntval;
    }

    public BigDecimal getCurtLprRate() {
        return curtLprRate;
    }

    public void setCurtLprRate(BigDecimal curtLprRate) {
        this.curtLprRate = curtLprRate;
    }

    public BigDecimal getRateFloatPoint() {
        return rateFloatPoint;
    }

    public void setRateFloatPoint(BigDecimal rateFloatPoint) {
        this.rateFloatPoint = rateFloatPoint;
    }

    public BigDecimal getOverdueRatePefloat() {
        return overdueRatePefloat;
    }

    public void setOverdueRatePefloat(BigDecimal overdueRatePefloat) {
        this.overdueRatePefloat = overdueRatePefloat;
    }

    public BigDecimal getOverdueExecRate() {
        return overdueExecRate;
    }

    public void setOverdueExecRate(BigDecimal overdueExecRate) {
        this.overdueExecRate = overdueExecRate;
    }

    public BigDecimal getCiRatePefloat() {
        return ciRatePefloat;
    }

    public void setCiRatePefloat(BigDecimal ciRatePefloat) {
        this.ciRatePefloat = ciRatePefloat;
    }

    public BigDecimal getCiExecRate() {
        return ciExecRate;
    }

    public void setCiExecRate(BigDecimal ciExecRate) {
        this.ciExecRate = ciExecRate;
    }

    public String getRateAdjType() {
        return rateAdjType;
    }

    public void setRateAdjType(String rateAdjType) {
        this.rateAdjType = rateAdjType;
    }

    public String getNextRateAdjInterval() {
        return nextRateAdjInterval;
    }

    public void setNextRateAdjInterval(String nextRateAdjInterval) {
        this.nextRateAdjInterval = nextRateAdjInterval;
    }

    public String getNextRateAdjUnit() {
        return nextRateAdjUnit;
    }

    public void setNextRateAdjUnit(String nextRateAdjUnit) {
        this.nextRateAdjUnit = nextRateAdjUnit;
    }

    public String getFirstAdjDate() {
        return firstAdjDate;
    }

    public void setFirstAdjDate(String firstAdjDate) {
        this.firstAdjDate = firstAdjDate;
    }

    public String getRepayMode() {
        return repayMode;
    }

    public void setRepayMode(String repayMode) {
        this.repayMode = repayMode;
    }

    public String getEiIntervalCycle() {
        return eiIntervalCycle;
    }

    public void setEiIntervalCycle(String eiIntervalCycle) {
        this.eiIntervalCycle = eiIntervalCycle;
    }

    public String getEiIntervalUnit() {
        return eiIntervalUnit;
    }

    public void setEiIntervalUnit(String eiIntervalUnit) {
        this.eiIntervalUnit = eiIntervalUnit;
    }

    public String getDeductType() {
        return deductType;
    }

    public void setDeductType(String deductType) {
        this.deductType = deductType;
    }

    public String getDeductDay() {
        return deductDay;
    }

    public void setDeductDay(String deductDay) {
        this.deductDay = deductDay;
    }

    public String getLoanPayoutAccno() {
        return loanPayoutAccno;
    }

    public void setLoanPayoutAccno(String loanPayoutAccno) {
        this.loanPayoutAccno = loanPayoutAccno;
    }

    public String getLoanPayoutSubNo() {
        return loanPayoutSubNo;
    }

    public void setLoanPayoutSubNo(String loanPayoutSubNo) {
        this.loanPayoutSubNo = loanPayoutSubNo;
    }

    public String getPayoutAcctName() {
        return payoutAcctName;
    }

    public void setPayoutAcctName(String payoutAcctName) {
        this.payoutAcctName = payoutAcctName;
    }

    public String getIsBeEntrustedPay() {
        return isBeEntrustedPay;
    }

    public void setIsBeEntrustedPay(String isBeEntrustedPay) {
        this.isBeEntrustedPay = isBeEntrustedPay;
    }

    public String getRepayAccno() {
        return repayAccno;
    }

    public void setRepayAccno(String repayAccno) {
        this.repayAccno = repayAccno;
    }

    public String getRepaySubAccno() {
        return repaySubAccno;
    }

    public void setRepaySubAccno(String repaySubAccno) {
        this.repaySubAccno = repaySubAccno;
    }

    public String getRepayAcctName() {
        return repayAcctName;
    }

    public void setRepayAcctName(String repayAcctName) {
        this.repayAcctName = repayAcctName;
    }

    public String getLoanPromiseFlag() {
        return loanPromiseFlag;
    }

    public void setLoanPromiseFlag(String loanPromiseFlag) {
        this.loanPromiseFlag = loanPromiseFlag;
    }

    public String getLoanPromiseType() {
        return loanPromiseType;
    }

    public void setLoanPromiseType(String loanPromiseType) {
        this.loanPromiseType = loanPromiseType;
    }

    public String getSbsyDepAccno() {
        return sbsyDepAccno;
    }

    public void setSbsyDepAccno(String sbsyDepAccno) {
        this.sbsyDepAccno = sbsyDepAccno;
    }

    public BigDecimal getSbsyPerc() {
        return sbsyPerc;
    }

    public void setSbsyPerc(BigDecimal sbsyPerc) {
        this.sbsyPerc = sbsyPerc;
    }

    public String getSbysEnddate() {
        return sbysEnddate;
    }

    public void setSbysEnddate(String sbysEnddate) {
        this.sbysEnddate = sbysEnddate;
    }

    public String getIsUtilLmt() {
        return isUtilLmt;
    }

    public void setIsUtilLmt(String isUtilLmt) {
        this.isUtilLmt = isUtilLmt;
    }

    public String getLmtAccNo() {
        return lmtAccNo;
    }

    public void setLmtAccNo(String lmtAccNo) {
        this.lmtAccNo = lmtAccNo;
    }

    public String getReplyNo() {
        return replyNo;
    }

    public void setReplyNo(String replyNo) {
        this.replyNo = replyNo;
    }

    public String getLoanTypeDetail() {
        return loanTypeDetail;
    }

    public void setLoanTypeDetail(String loanTypeDetail) {
        this.loanTypeDetail = loanTypeDetail;
    }

    public String getIsPactLoan() {
        return isPactLoan;
    }

    public void setIsPactLoan(String isPactLoan) {
        this.isPactLoan = isPactLoan;
    }

    public String getIsGreenIndustry() {
        return isGreenIndustry;
    }

    public void setIsGreenIndustry(String isGreenIndustry) {
        this.isGreenIndustry = isGreenIndustry;
    }

    public String getIsOperPropertyLoan() {
        return isOperPropertyLoan;
    }

    public void setIsOperPropertyLoan(String isOperPropertyLoan) {
        this.isOperPropertyLoan = isOperPropertyLoan;
    }

    public String getIsSteelLoan() {
        return isSteelLoan;
    }

    public void setIsSteelLoan(String isSteelLoan) {
        this.isSteelLoan = isSteelLoan;
    }

    public String getIsStainlessLoan() {
        return isStainlessLoan;
    }

    public void setIsStainlessLoan(String isStainlessLoan) {
        this.isStainlessLoan = isStainlessLoan;
    }

    public String getIsPovertyReliefLoan() {
        return isPovertyReliefLoan;
    }

    public void setIsPovertyReliefLoan(String isPovertyReliefLoan) {
        this.isPovertyReliefLoan = isPovertyReliefLoan;
    }

    public String getIsLaborIntenSbsyLoan() {
        return isLaborIntenSbsyLoan;
    }

    public void setIsLaborIntenSbsyLoan(String isLaborIntenSbsyLoan) {
        this.isLaborIntenSbsyLoan = isLaborIntenSbsyLoan;
    }

    public String getGoverSubszHouseLoan() {
        return goverSubszHouseLoan;
    }

    public void setGoverSubszHouseLoan(String goverSubszHouseLoan) {
        this.goverSubszHouseLoan = goverSubszHouseLoan;
    }

    public String getEngyEnviProteLoan() {
        return engyEnviProteLoan;
    }

    public void setEngyEnviProteLoan(String engyEnviProteLoan) {
        this.engyEnviProteLoan = engyEnviProteLoan;
    }

    public String getIsCphsRurDelpLoan() {
        return isCphsRurDelpLoan;
    }

    public void setIsCphsRurDelpLoan(String isCphsRurDelpLoan) {
        this.isCphsRurDelpLoan = isCphsRurDelpLoan;
    }

    public String getRealproLoan() {
        return realproLoan;
    }

    public void setRealproLoan(String realproLoan) {
        this.realproLoan = realproLoan;
    }

    public BigDecimal getRealproLoanRate() {
        return realproLoanRate;
    }

    public void setRealproLoanRate(BigDecimal realproLoanRate) {
        this.realproLoanRate = realproLoanRate;
    }

    public String getFinaBrId() {
        return finaBrId;
    }

    public void setFinaBrId(String finaBrId) {
        this.finaBrId = finaBrId;
    }

    public String getFinaBrIdName() {
        return finaBrIdName;
    }

    public void setFinaBrIdName(String finaBrIdName) {
        this.finaBrIdName = finaBrIdName;
    }

    public String getDisbOrgNo() {
        return disbOrgNo;
    }

    public void setDisbOrgNo(String disbOrgNo) {
        this.disbOrgNo = disbOrgNo;
    }

    public String getDisbOrgName() {
        return disbOrgName;
    }

    public void setDisbOrgName(String disbOrgName) {
        this.disbOrgName = disbOrgName;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getAgriType() {
        return agriType;
    }

    public void setAgriType(String agriType) {
        this.agriType = agriType;
    }

    public String getAgriLoanTer() {
        return agriLoanTer;
    }

    public void setAgriLoanTer(String agriLoanTer) {
        this.agriLoanTer = agriLoanTer;
    }

    public String getIsMaterComp() {
        return isMaterComp;
    }

    public void setIsMaterComp(String isMaterComp) {
        this.isMaterComp = isMaterComp;
    }

    public String getGuarDetailMode() {
        return guarDetailMode;
    }

    public void setGuarDetailMode(String guarDetailMode) {
        this.guarDetailMode = guarDetailMode;
    }

    public String getLoanTer() {
        return loanTer;
    }

    public void setLoanTer(String loanTer) {
        this.loanTer = loanTer;
    }

    public String getLoanSubjectNo() {
        return loanSubjectNo;
    }

    public void setLoanSubjectNo(String loanSubjectNo) {
        this.loanSubjectNo = loanSubjectNo;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getEstateType() {
        return estateType;
    }

    public void setEstateType(String estateType) {
        this.estateType = estateType;
    }

    public String getIndtUpFlag() {
        return indtUpFlag;
    }

    public void setIndtUpFlag(String indtUpFlag) {
        this.indtUpFlag = indtUpFlag;
    }

    public String getStrategyNewLoan() {
        return strategyNewLoan;
    }

    public void setStrategyNewLoan(String strategyNewLoan) {
        this.strategyNewLoan = strategyNewLoan;
    }

    public String getCulIndustryFlag() {
        return culIndustryFlag;
    }

    public void setCulIndustryFlag(String culIndustryFlag) {
        this.culIndustryFlag = culIndustryFlag;
    }

    public String getCapAutobackFlag() {
        return capAutobackFlag;
    }

    public void setCapAutobackFlag(String capAutobackFlag) {
        this.capAutobackFlag = capAutobackFlag;
    }

    public String getIsSbsy() {
        return isSbsy;
    }

    public void setIsSbsy(String isSbsy) {
        this.isSbsy = isSbsy;
    }

    public String getIsMeterCi() {
        return isMeterCi;
    }

    public void setIsMeterCi(String isMeterCi) {
        this.isMeterCi = isMeterCi;
    }

    public String getIntAutobackFlag() {
        return intAutobackFlag;
    }

    public void setIntAutobackFlag(String intAutobackFlag) {
        this.intAutobackFlag = intAutobackFlag;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getPvpAmt() {
        return pvpAmt;
    }

    public void setPvpAmt(BigDecimal pvpAmt) {
        this.pvpAmt = pvpAmt;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getIsCfirmPay() {
        return isCfirmPay;
    }

    public void setIsCfirmPay(String isCfirmPay) {
        this.isCfirmPay = isCfirmPay;
    }

    public String getTermType() {
        return termType;
    }

    public void setTermType(String termType) {
        this.termType = termType;
    }

    public String getAppTerm() {
        return appTerm;
    }

    public void setAppTerm(String appTerm) {
        this.appTerm = appTerm;
    }

    public BigDecimal getCustomerRatingFactor() {
        return customerRatingFactor;
    }

    public void setCustomerRatingFactor(BigDecimal customerRatingFactor) {
        this.customerRatingFactor = customerRatingFactor;
    }

    public BigDecimal getExecRateYear() {
        return execRateYear;
    }

    public void setExecRateYear(BigDecimal execRateYear) {
        this.execRateYear = execRateYear;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getChnlSour() {
        return chnlSour;
    }

    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour;
    }

    public String getAcctBrId() {
        return acctBrId;
    }

    public void setAcctBrId(String acctBrId) {
        this.acctBrId = acctBrId;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBeforehandInd() {
        return beforehandInd;
    }

    public void setBeforehandInd(String beforehandInd) {
        this.beforehandInd = beforehandInd;
    }

    public String getLoanRateAdjDay() {
        return loanRateAdjDay;
    }

    public void setLoanRateAdjDay(String loanRateAdjDay) {
        this.loanRateAdjDay = loanRateAdjDay;
    }

    public String getIrFloatType() {
        return irFloatType;
    }

    public void setIrFloatType(String irFloatType) {
        this.irFloatType = irFloatType;
    }

    public BigDecimal getIrFloatRate() {
        return irFloatRate;
    }

    public void setIrFloatRate(BigDecimal irFloatRate) {
        this.irFloatRate = irFloatRate;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getPaperContSignDate() {
        return paperContSignDate;
    }

    public void setPaperContSignDate(String paperContSignDate) {
        this.paperContSignDate = paperContSignDate;
    }

    public Integer getRepayDay() {
        return repayDay;
    }

    public void setRepayDay(Integer repayDay) {
        this.repayDay = repayDay;
    }
}
