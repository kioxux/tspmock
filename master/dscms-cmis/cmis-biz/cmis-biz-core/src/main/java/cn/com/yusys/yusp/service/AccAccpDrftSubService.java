/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccAccpDrftSub;
import cn.com.yusys.yusp.domain.AsplAssetsList;
import cn.com.yusys.yusp.domain.InPoolAssetListExport;
import cn.com.yusys.yusp.dto.AccAccpDrftSubDto;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.repository.mapper.AccAccpDrftSubMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.AccAccpDrftSubVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static cn.com.yusys.yusp.constants.CmisBizConstants.IQP_ACC_STATUS_9;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccAccpDrftSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zxz
 * @创建时间: 2021-05-05 11:24:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AccAccpDrftSubService {

    @Autowired
    private AccAccpDrftSubMapper accAccpDrftSubMapper;
    @Autowired
    private CommonService commonService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AccAccpDrftSub selectByPrimaryKey(String pkId) {
        return accAccpDrftSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AccAccpDrftSub> selectAll(QueryModel model) {
        List<AccAccpDrftSub> records = (List<AccAccpDrftSub>) accAccpDrftSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AccAccpDrftSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccpDrftSub> list = accAccpDrftSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AccAccpDrftSub record) {
        return accAccpDrftSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AccAccpDrftSub record) {
        return accAccpDrftSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AccAccpDrftSub record) {
        return accAccpDrftSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AccAccpDrftSub record) {
        return accAccpDrftSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return accAccpDrftSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return accAccpDrftSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称：selecByBillContNo
     * @方法描述：  根据银承台账的合同编号查询
     * @创建人：xs
     * @创建时间：2021/5/25 16:49
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccAccpDrftSub> selectByBillContNo(String billContNo){
        return accAccpDrftSubMapper.selectByBillContNo(billContNo);
    }
    /**
     * @方法名称：selecByBillContNo
     * @方法描述：  根据客户号灵活查询
     * @创建人：xs
     * @创建时间：2021/5/25 16:49
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<cn.com.yusys.yusp.dto.server.xdzc0014.resp.List> selectInfoByCusIdDate(QueryModel model) {
        return accAccpDrftSubMapper.selectInfoByCusIdDate(model);
    }

    /**
     * @方法名称: selectByCoreBillNo
     * @方法描述: 根据核心银承编号回显银承台账票据明细
     * @参数与返回说明:
     * @算法描述: 无
     */

    public AccAccpDrftSubDto selectByCoreBillNo(AccAccpDrftSub accAccpDrftSub) {
        AccAccpDrftSubDto accAccpDrftSubDto = accAccpDrftSubMapper.selectByCoreBillNo(accAccpDrftSub);
        AdminSmUserDto inputIdDto = commonService.getByLoginCode(accAccpDrftSubDto.getInputId());
        AdminSmOrgDto inputBrIdDto = commonService.getByOrgCode(accAccpDrftSubDto.getInputBrId());
        AdminSmUserDto managerIdDto = commonService.getByLoginCode(accAccpDrftSubDto.getManagerId());
        AdminSmOrgDto managerBrIdDto = commonService.getByOrgCode(accAccpDrftSubDto.getManagerBrId());
        if (null != inputBrIdDto) {
            accAccpDrftSubDto.setInputIdName(inputIdDto.getUserName());
        }
        if (null != inputBrIdDto) {
            accAccpDrftSubDto.setInputBrIdName(inputBrIdDto.getOrgName());
        }
        if (null != managerIdDto) {
            accAccpDrftSubDto.setManagerIdName(managerIdDto.getUserName());
        }
        if (null != managerBrIdDto) {
            accAccpDrftSubDto.setManagerBrIdName(managerBrIdDto.getOrgName());
        }
        return accAccpDrftSubDto;
    }

    /**
     * 异步导出诉讼申请列表数据
     * @return 导出进度信息
     */
    public ProgressDto asyncExportAccAccpDrftSub(QueryModel model) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel)object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            return exportAccAccpDrftSub(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(AccAccpDrftSubVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, model);
        return ExcelUtils.asyncExport(exportContext);
    }
    public List<AccAccpDrftSubVo> exportAccAccpDrftSub(QueryModel model) {
        List<AccAccpDrftSubVo> accAccpDrftSubVos = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccpDrftSub> list = selectByContNo(model);
        PageHelper.clearPage();
        for (AccAccpDrftSub accAccpDrftSub : list) {
            AccAccpDrftSubVo accAccpDrftSubVo = new AccAccpDrftSubVo();
            org.springframework.beans.BeanUtils.copyProperties(accAccpDrftSub, accAccpDrftSubVo);

            accAccpDrftSubVos.add(accAccpDrftSubVo);
        }
        return accAccpDrftSubVos;
    }
    /**
     * @方法名称: querymodelByCondition
     * @方法描述: 根据查询条件查询台账信息并返回
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccAccpDrftSub> querymodelByCondition(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccpDrftSub> list = accAccpDrftSubMapper.querymodelByCondition(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称：selectByBillNo
     * @方法描述： 根据银承台账的借据编号查询(未用退回、作废状态的除外)
     * @创建人：qw
     * @创建时间：2021/8/20 16:49
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<AccAccpDrftSub> selectByBillNo(String billNo){
        return accAccpDrftSubMapper.selectByBillNo(billNo);
    }

    /**
     * @方法名称: selectByContNo
     * @方法描述: 根据合同编号查询票据明细台账信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AccAccpDrftSub> selectByContNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AccAccpDrftSub> list = accAccpDrftSubMapper.selectByContNo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectUnClearByBillContNo
     * @方法描述: 根据合同编号查询票据明细台账总金额(未结清)
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BigDecimal selectUnClearByBillContNo(String contNo) {
        return accAccpDrftSubMapper.selectUnClearByBillContNo(contNo);
    }

    /**
     * @方法名称: update
     * @方法描述: 更新台账状态
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByAccStatus(AccAccpDrftSub record) {
        record.setAccStatus("3");
        return accAccpDrftSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: updateByPorderNo
     * @方法描述: 更改票据号码
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateByPorderNo(AccAccpDrftSub record) {
        return accAccpDrftSubMapper.updateByPorderNo(record);
    }

    /**
     * @方法名称: selectEndaDateByCoreBillNo
     * @方法描述: 根据银承核心编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<AccAccpDrftSub> selectDetailsByCoreBillNo(String coreBillNo) {
        return accAccpDrftSubMapper.selectDetailsByCoreBillNo(coreBillNo);
    }
}
