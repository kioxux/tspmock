package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpRepayPlan;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.repository.mapper.IqpRepayPlanMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRepayPlanService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 12393
 * @创建时间: 2021-04-23 14:36:55
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpRepayPlanService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportBasicInfo.class);

    @Autowired
    private IqpRepayPlanMapper iqpRepayPlanMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public IqpRepayPlan selectByPrimaryKey(String pkId) {
        return iqpRepayPlanMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<IqpRepayPlan> selectAll(QueryModel model) {
        List<IqpRepayPlan> records = (List<IqpRepayPlan>) iqpRepayPlanMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<IqpRepayPlan> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpRepayPlan> list = iqpRepayPlanMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /***
     * @param contNo
     * @return java.util.List<cn.com.yusys.yusp.domain.IqpLoanAppDramPlanSub>
     * @author hubp
     * @date 2021/4/23 16:27
     * @version 1.0.0
     * @desc 根据合同号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public List<IqpRepayPlan> selectByContNo(String contNo) {
        List<IqpRepayPlan> list = iqpRepayPlanMapper.selectByContNo(contNo);
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(IqpRepayPlan record) {
        return iqpRepayPlanMapper.insert(record);
    }

    /***
     * @param record
     * @return int
     * @author hubp
     * @date 2021/4/24 9:37
     * @version 1.0.0
     * @desc 从主键是否有值来判断是插入还是更新
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional(rollbackFor = Exception.class)
    public int queryIqpRepayPlan(IqpRepayPlan record) {
        logger.info("开始保存还款信息----合同编号：" + record.getContNo());
        String pkId = record.getPkId();
        int row = 0;
        try {
            if (pkId == null || "".equals(pkId)) {
                record.setPkId(UUID.randomUUID().toString());
                row = this.insert(record);
            } else {
                row = this.updateSelective(record);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw e;
        } finally {
            logger.info("还款信息保存成功----合同编号：" + record.getContNo());
        }
        return row;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpRepayPlan record) {
        return iqpRepayPlanMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpRepayPlan record) {
        return iqpRepayPlanMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpRepayPlan record) {
        return iqpRepayPlanMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpRepayPlanMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpRepayPlanMapper.deleteByIds(ids);
    }
}
