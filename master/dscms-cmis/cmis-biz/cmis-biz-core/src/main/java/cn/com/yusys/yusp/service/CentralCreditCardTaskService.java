/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.TaskPoolOptRecord;
import cn.com.yusys.yusp.repository.mapper.TaskPoolOptRecordMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CentralCreditCardTask;
import cn.com.yusys.yusp.repository.mapper.CentralCreditCardTaskMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralCreditCardTaskService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 09:44:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CentralCreditCardTaskService {

    @Autowired
    private CentralCreditCardTaskMapper centralCreditCardTaskMapper;

    @Autowired
    private TaskPoolOptRecordMapper taskPoolOptRecordMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CentralCreditCardTask selectByPrimaryKey(String taskNo) {
        return centralCreditCardTaskMapper.selectByPrimaryKey(taskNo);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CentralCreditCardTask> selectAll(QueryModel model) {
        List<CentralCreditCardTask> records = (List<CentralCreditCardTask>) centralCreditCardTaskMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CentralCreditCardTask> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CentralCreditCardTask> list = centralCreditCardTaskMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CentralCreditCardTask record) {
        return centralCreditCardTaskMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CentralCreditCardTask record) {
        return centralCreditCardTaskMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CentralCreditCardTask record) {
        return centralCreditCardTaskMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CentralCreditCardTask record) {
        return centralCreditCardTaskMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String taskNo) {
        return centralCreditCardTaskMapper.deleteByPrimaryKey(taskNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return centralCreditCardTaskMapper.deleteByIds(ids);
    }

    //批量更新
    public long dealList(List<CentralCreditCardTask> lists){
        return lists.parallelStream().map(item -> {
            //分配，领取，作废
            String status = item.getTaskStatus();
            String updId = item.getUpdId();
            String receiverId = item.getReceiverId();
            if ("02".equals(status) || "03".equals(status)) {
                //TaskPoolOptRecord 增加一条数据
                TaskPoolOptRecord taskPoolOptRecord = new TaskPoolOptRecord();
                BeanUtils.copyProperties(item, taskPoolOptRecord);
                //判断操作类型
                if("03".equals(status)){
                    taskPoolOptRecord.setOptType("03");
                }else{
                    if(updId != null && !"".equals(updId) && receiverId != null && !"".equals(receiverId)){
                        if(updId.equals(receiverId)){
                            taskPoolOptRecord.setOptType("02");
                        }else{
                            taskPoolOptRecord.setOptType("01");
                        }
                    }
                }
                taskPoolOptRecord.setOptUsr(item.getUpdId());
                taskPoolOptRecord.setOptOrg(item.getUpdBrId());
                taskPoolOptRecord.setOptTime(item.getUpdDate());
                taskPoolOptRecord.setOptReason(item.getCancelResn());
                taskPoolOptRecordMapper.insert(taskPoolOptRecord);
            }
            return item;
        }).map(centralCreditCardTaskMapper::updateByPrimaryKeySelective).count();
    }
}
