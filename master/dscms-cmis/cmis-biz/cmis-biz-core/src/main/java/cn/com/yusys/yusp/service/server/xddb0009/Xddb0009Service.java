package cn.com.yusys.yusp.service.server.xddb0009;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0009.req.Xddb0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0009.resp.Xddb0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.SlSernoQueryMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类:查询在线抵押信息
 *
 * @author chenyong
 * @version 1.0
 */
@Service
public class Xddb0009Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0009Service.class);

    @Autowired
    private SlSernoQueryMapper slSernoQueryMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    /**
     * 查询在线抵押信息
     *
     * @param xddb0009DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0009DataRespDto xddb0009(Xddb0009DataReqDto xddb0009DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value);
        Xddb0009DataRespDto xddb0009DataRespDto = new Xddb0009DataRespDto();
        String coreGrtNo = xddb0009DataReqDto.getGuarNo();//抵押物编号
        String imagePk1SerNo = xddb0009DataReqDto.getImagePk1SerNo();//影像主键流水号
        String tranType = xddb0009DataReqDto.getTranType();//交易类型
        String pkId = "";
        if ("02".equals(tranType) && StringUtils.isEmpty(imagePk1SerNo)) {
            //影像主键流水号
            throw new YuspException(EcbEnum.ECB010009.key, EcbEnum.ECB010009.value);
        }
        if (StringUtils.isEmpty(tranType)) {
            //交易类型
            throw new YuspException(EcbEnum.ECB010009.key, EcbEnum.ECB010009.value);
        }

        int result = 0;
        if (StringUtil.isNotEmpty(coreGrtNo)) {
            Map QueryMap = new HashMap();
            QueryMap.put("coreGrtNo", coreGrtNo);
            QueryMap.put("imagePk1SerNo", imagePk1SerNo);
            if (tranType.equals("01")) {// 01-保存
                if (StringUtil.isEmpty(imagePk1SerNo)) {
                    Map seqMap = new HashMap();
                    seqMap.put("contNo", coreGrtNo);
                    imagePk1SerNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YX_SERNO_SEQ, new HashMap<>());
                    pkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, new HashMap<>());
                }
                QueryMap.put("yxlsno", imagePk1SerNo);
                QueryMap.put("pkId", pkId);
                result = slSernoQueryMapper.insertSlSernoQuery(QueryMap);
            } else if (tranType.equals("02")) {//02-更新
                String create_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                QueryMap.put("createTime", create_time);
                result = slSernoQueryMapper.updateSlSernoQuery(QueryMap);
            }
            if (result > 0) {//更新保存成功
                xddb0009DataRespDto.setImagePk1SerNo(imagePk1SerNo);
                xddb0009DataRespDto.setGuarNo(coreGrtNo);
            } else {//失败
                logger.info("**************************双录流水号与押品编号关系维护失败**************************");
                xddb0009DataRespDto.setGuarNo(StringUtils.EMPTY);
                xddb0009DataRespDto.setImagePk1SerNo(StringUtils.EMPTY);
                throw new YuspException(EcbEnum.EDS990001.key, EcbEnum.EDS990001.value());
            }

        } else {
            //请求参数不存在,返回空值
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value);
            xddb0009DataRespDto.setImagePk1SerNo(StringUtils.EMPTY);
            xddb0009DataRespDto.setGuarNo(StringUtils.EMPTY);
            throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value);
        return xddb0009DataRespDto;
    }
}
