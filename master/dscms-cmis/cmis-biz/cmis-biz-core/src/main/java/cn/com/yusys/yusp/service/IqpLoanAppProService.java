/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpLoanAppPro;
import cn.com.yusys.yusp.repository.mapper.IqpLoanAppProMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppProService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-20 21:02:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class IqpLoanAppProService {

    @Autowired
    private IqpLoanAppProMapper iqpLoanAppProMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public IqpLoanAppPro selectByPrimaryKey(String pkId) {
        return iqpLoanAppProMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpLoanAppPro> selectAll(QueryModel model) {
        List<IqpLoanAppPro> records = (List<IqpLoanAppPro>) iqpLoanAppProMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<IqpLoanAppPro> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanAppPro> list = iqpLoanAppProMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(IqpLoanAppPro record) {
        return iqpLoanAppProMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanAppPro record) {
        return iqpLoanAppProMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(IqpLoanAppPro record) {
        return iqpLoanAppProMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(IqpLoanAppPro record) {
        return iqpLoanAppProMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return iqpLoanAppProMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpLoanAppProMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:addOrUpdateIqpLoanAppProDataBySerno
     * @函数描述:有则更新，无则插入
     * @参数与返回说明:
     * @算法描述:
     */

    public int addOrUpdateIqpLoanAppProDataBySerno(IqpLoanAppPro iqpLoanAppPro) {
        String pkId = iqpLoanAppPro.getPkId();
        int count = 0;
        //获取当前登录信息
        User userInfo = SessionUtils.getUserInformation();
        String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
        if("".equals(pkId) || pkId == null ){
            // 执行新增操作
            iqpLoanAppPro.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            iqpLoanAppPro.setManagerId(userInfo.getLoginCode());
            iqpLoanAppPro.setManagerBrId(userInfo.getOrg().getCode());
            iqpLoanAppPro.setInputId(userInfo.getLoginCode());
            iqpLoanAppPro.setInputBrId(userInfo.getOrg().getCode());
            iqpLoanAppPro.setInputDate(nowDate);
            iqpLoanAppPro.setUpdId(userInfo.getLoginCode());
            iqpLoanAppPro.setUpdBrId(userInfo.getOrg().getCode());
            iqpLoanAppPro.setUpdDate(nowDate);
            iqpLoanAppPro.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            // 主键  UUID
            iqpLoanAppPro.setPkId(UUID.randomUUID().toString());
            int insertCount = insertSelective(iqpLoanAppPro);
            if(insertCount<=0){
                throw BizException.error(null, EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            count = insertCount;
        }else{
            iqpLoanAppPro.setUpdId(userInfo.getLoginCode());
            iqpLoanAppPro.setUpdBrId(userInfo.getOrg().getCode());
            iqpLoanAppPro.setUpdDate(nowDate);
            iqpLoanAppPro.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            // 执行更新操作
            int updateCount = updateSelective(iqpLoanAppPro);
            if(updateCount<=0){
                throw BizException.error(null, EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
            }
            count = updateCount;
        }
        return count;
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:通过流水号查询
     * @参数与返回说明:
     * @算法描述:
     */

    public IqpLoanAppPro selectBySerno(String serno) {
        return iqpLoanAppProMapper.selectBySerno(serno);
    }
}
