package cn.com.yusys.yusp.domain;

import javax.persistence.Column;

/**
 * @创建人 WH
 * @创建时间 2021-05-07
 * @return CtrContImageAuditAppDto
 **/
public class CtrContImageAuditAppDomain extends CtrContImageAuditApp{
    @Column(name = "CTR_BEGIN_FLAG", unique = false, nullable = true, length = 40)
    private String ctrBeginFlag;

    public String getCtrBeginFlag() {
        return ctrBeginFlag;
    }

    public void setCtrBeginFlag(String ctrBeginFlag) {
        this.ctrBeginFlag = ctrBeginFlag;
    }
}
