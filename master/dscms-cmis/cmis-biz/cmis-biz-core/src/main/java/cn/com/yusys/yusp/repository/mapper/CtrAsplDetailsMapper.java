/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.domain.IqpAppAspl;
import cn.com.yusys.yusp.dto.AsplAccpDto;
import cn.com.yusys.yusp.dto.AsplAccpEntrustAccDto;
import cn.com.yusys.yusp.dto.AsplBailAcctDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrAsplDetailsMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:55:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CtrAsplDetailsMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CtrAsplDetails selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CtrAsplDetails> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CtrAsplDetails record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CtrAsplDetails record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CtrAsplDetails record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CtrAsplDetails record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: queryCtrAsplDetailsDataByParams
     * @方法描述: 根据入参查询资产池协议数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    CtrAsplDetails queryCtrAsplDetailsDataByParams(Map queryMap);
   
    /**
     * @方法名称: updateActiveByContNO
     * @方法描述:  根据资产池协议编号更新合同状态
     * @参数与返回说明: map
     * @算法描述: 无
     */
    int updateActiveByContNo(Map<String,String> map);

    /**
     * @方法名称: selectCtrAsplDetailsInfoByContNo
     * @方法描述:  根据合同编号查询资产详情
     * @参数与返回说明: contNo
     * @算法描述: 无
     */
    CtrAsplDetails selectCtrAsplDetailsInfoByContNo(@Param("contNo") String contNo);

    /**
     * 保证金信息列表
     *
     * @param model
     * @return
     */
    List<AsplBailAcctDto> ctrAspiDetailsAndBailAccInfoList(QueryModel model);

    /**
     * 根据流水号得到资产池协议关联的保证金信息
     *
     * @param map
     * @return
     */
    AsplBailAcctDto queryCtrAspiDetailsAndBailAccInfoByParams(Map map);

    /**
     * @方法名称: inPoolAccList
     * @方法描述: 池内业务台账列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<AsplAccpEntrustAccDto> inPoolAccList(QueryModel model);

    /**
     * 根据流水号得到贸易背景资料审核信息
     *
     * @param map
     * @return
     */
    AsplAccpDto queryAsplAccpDtoInfoByParams(Map map);

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据授信额度编号查询资产池协议列表
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<CtrAsplDetails> selectByLmtAccNo(String lmtAccNo);

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据授信额度编号查询资产池协议列表
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    CtrAsplDetails selectBySerno(@Param("serno")String serno);


    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据合同编号查询资产池协议
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    CtrAsplDetails selectInfoByContNo(@Param("contNo")String contNo);

    /**
     * @方法名称: deleteByContNo
     * @方法描述: 根据协议编号删除
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    int deleteByContNo(@Param("contNo")String contNo);

    /**
     * @方法名称: getDealBizList
     * @方法描述: 初始化出账校验参数
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    CmisLmt0010ReqDealBizListDto getDealBizList(@Param("contNo")String contNo);

    /**
     * 查询客户下所有的资产池协议编号
     * @param cusId
     * @return
     */
    List<String> selectContNoListByCusId(@Param("cusId")String cusId);
}