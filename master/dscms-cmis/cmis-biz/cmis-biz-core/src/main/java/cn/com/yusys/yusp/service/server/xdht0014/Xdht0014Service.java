package cn.com.yusys.yusp.service.server.xdht0014;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0014.resp.Xdht0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0014Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-04-27 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0014Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0014Service.class);

    @Resource
    private CtrLoanContMapper ctrLoanContMapper;

    /**
     * 合同签订查询VX
     *
     * @param indgtSerno
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0014DataRespDto getContSignInfo(String indgtSerno) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, "请求参数：" + indgtSerno);
        Xdht0014DataRespDto xdht0014DataRespDto = new Xdht0014DataRespDto();
        try {
            //根据调查编号查询合同信息
            if (StringUtils.isEmpty(indgtSerno)) {
                //调查编号为空
                throw BizException.error(null, EcbEnum.ECB010009.key, EcbEnum.ECB010009.value);
            } else {
                xdht0014DataRespDto = ctrLoanContMapper.getContSignInfo(indgtSerno);
            }
            //根据合同编号计算可用
            Optional.ofNullable(xdht0014DataRespDto).orElseThrow(() -> BizException.error(null, EcbEnum.ECB010010.key, EcbEnum.ECB010010.value));
            Optional.ofNullable(xdht0014DataRespDto.getContNo()).orElseThrow(() -> BizException.error(null, EcbEnum.ECB010010.key, EcbEnum.ECB010010.value));
            //计算利息
            BigDecimal contAmt = xdht0014DataRespDto.getContAmt();
            BigDecimal exeRate = xdht0014DataRespDto.getExeRate();
            //日利息
            BigDecimal dailyInt = contAmt.multiply(exeRate).divide(new BigDecimal(360), 2, BigDecimal.ROUND_HALF_UP);
            //日息精确到角，四舍五入
            dailyInt = dailyInt.multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal(100));
            xdht0014DataRespDto.setDailyInt(dailyInt);
            //可用额度
            BigDecimal avlAmt = ctrLoanContMapper.calAvlLmt(xdht0014DataRespDto.getContNo());
            xdht0014DataRespDto.setAvlLmt(avlAmt);

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014DataRespDto));
        return xdht0014DataRespDto;
    }

}
