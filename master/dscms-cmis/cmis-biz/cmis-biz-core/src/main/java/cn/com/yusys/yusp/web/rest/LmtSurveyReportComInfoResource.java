/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.dto.req.QueryEnterpriseReqDto;
import cn.com.yusys.yusp.dto.resp.QueryEnterpriseRespDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSurveyReportComInfo;
import cn.com.yusys.yusp.service.LmtSurveyReportComInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportComInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-20 14:19:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "调查报告企业信息")
@RequestMapping("/api/lmtsurveyreportcominfo")
public class LmtSurveyReportComInfoResource {
    @Autowired
    private LmtSurveyReportComInfoService lmtSurveyReportComInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @ApiOperation("全表查询，公共API接口")
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSurveyReportComInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSurveyReportComInfo> list = lmtSurveyReportComInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtSurveyReportComInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询对象列表，公共API接口")
    @GetMapping("/")
    protected ResultDto<List<LmtSurveyReportComInfo>> index(QueryModel queryModel) {
        List<LmtSurveyReportComInfo> list = lmtSurveyReportComInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSurveyReportComInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("查询单个对象，公共API接口")
    @GetMapping("/{surveySerno}")
    protected ResultDto<LmtSurveyReportComInfo> show(@PathVariable("surveySerno") String surveySerno) {
        LmtSurveyReportComInfo lmtSurveyReportComInfo = lmtSurveyReportComInfoService.selectByPrimaryKey(surveySerno);
        return new ResultDto<LmtSurveyReportComInfo>(lmtSurveyReportComInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("实体类创建，公共API接口")
    @PostMapping("/")
    protected ResultDto<LmtSurveyReportComInfo> create(@RequestBody LmtSurveyReportComInfo lmtSurveyReportComInfo) throws URISyntaxException {
        lmtSurveyReportComInfoService.insert(lmtSurveyReportComInfo);
        return new ResultDto<LmtSurveyReportComInfo>(lmtSurveyReportComInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("对象修改，公共API接口")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSurveyReportComInfo lmtSurveyReportComInfo) throws URISyntaxException {
        int result = lmtSurveyReportComInfoService.update(lmtSurveyReportComInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("单个对象删除，公共API接口")
    @PostMapping("/delete/{surveySerno}")
    protected ResultDto<Integer> delete(@PathVariable("surveySerno") String surveySerno) {
        int result = lmtSurveyReportComInfoService.deleteByPrimaryKey(surveySerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("批量对象删除，公共API接口")
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSurveyReportComInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param lmtSurveyReportComInfo
     * @return ResultDto<QueryEnterpriseRespDto
     * @author 王玉坤
     * @date 2021/4/24 15:23
     * @version 1.0.0
     * @desc    根据企业名称查找企业工商信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据企业名称查找企业工商信息")
    @PostMapping("/queryenterprise")
    protected ResultDto<LmtSurveyReportComInfo> queryEnterprise(@RequestBody LmtSurveyReportComInfo lmtSurveyReportComInfo) {
        return lmtSurveyReportComInfoService.queryEnterprise(lmtSurveyReportComInfo);
    }

    /**
    * @author zlf
    * @date 2021/6/8 10:55
    * @version 1.0.0
    * @desc   查单条
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/selectone")
    protected ResultDto<LmtSurveyReportComInfo> selectone(@RequestBody LmtSurveyReportComInfo lmtSurveyReportComInfo) {
        LmtSurveyReportComInfo data = lmtSurveyReportComInfoService.selectByPrimaryKey(lmtSurveyReportComInfo.getSurveySerno());
        return new ResultDto<LmtSurveyReportComInfo>(data);
    }

}
