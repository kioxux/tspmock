/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.RetailPrimeRateAppr;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RetailPrimeRateApprMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-24 09:20:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface RetailPrimeRateApprMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    RetailPrimeRateAppr selectByPrimaryKey(@Param("pkId") String pkId, @Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<RetailPrimeRateAppr> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(RetailPrimeRateAppr record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(RetailPrimeRateAppr record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(RetailPrimeRateAppr record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(RetailPrimeRateAppr record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId, @Param("serno") String serno);


    /**
     * @param serno
     * @return cn.com.yusys.yusp.domain.RetailPrimeRateAppr
     * @author 王玉坤
     * @date 2021/9/10 14:24
     * @version 1.0.0
     * @desc 根据流水号查询最新一笔审批信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    RetailPrimeRateAppr selectBySernoOrderByDate(@Param("serno") String serno);

    RetailPrimeRateAppr  selectBySerno(String serno);

    RetailPrimeRateAppr selectBySernoNew(RetailPrimeRateAppr retailPrimeRateAppr);

    RetailPrimeRateAppr selectBySernoAndNode(Map param);
}