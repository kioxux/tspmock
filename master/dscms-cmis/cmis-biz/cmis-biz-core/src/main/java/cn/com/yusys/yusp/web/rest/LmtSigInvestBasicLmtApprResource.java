/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicLmtAppr;
import cn.com.yusys.yusp.service.LmtSigInvestBasicLmtApprService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLmtApprResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 11:22:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestbasiclmtappr")
public class LmtSigInvestBasicLmtApprResource {
    @Autowired
    private LmtSigInvestBasicLmtApprService lmtSigInvestBasicLmtApprService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestBasicLmtAppr>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestBasicLmtAppr> list = lmtSigInvestBasicLmtApprService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestBasicLmtAppr>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestBasicLmtAppr>> index(QueryModel queryModel) {
        List<LmtSigInvestBasicLmtAppr> list = lmtSigInvestBasicLmtApprService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestBasicLmtAppr>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestBasicLmtAppr> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr = lmtSigInvestBasicLmtApprService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestBasicLmtAppr>(lmtSigInvestBasicLmtAppr);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestBasicLmtAppr> create(@RequestBody LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr) throws URISyntaxException {
        lmtSigInvestBasicLmtApprService.insert(lmtSigInvestBasicLmtAppr);
        return new ResultDto<LmtSigInvestBasicLmtAppr>(lmtSigInvestBasicLmtAppr);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestBasicLmtAppr lmtSigInvestBasicLmtAppr) throws URISyntaxException {
        int result = lmtSigInvestBasicLmtApprService.update(lmtSigInvestBasicLmtAppr);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestBasicLmtApprService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestBasicLmtApprService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectbasic")
    protected ResultDto<List<Map<String,Object>>> selectbasic(@RequestBody QueryModel model){
        return new ResultDto<List<Map<String,Object>>>(lmtSigInvestBasicLmtApprService.selectbasic(model));
    }
}
