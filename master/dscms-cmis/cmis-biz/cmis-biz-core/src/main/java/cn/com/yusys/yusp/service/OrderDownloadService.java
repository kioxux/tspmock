/*
package cn.com.yusys.yusp.service;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import cn.com.yusys.yusp.dto.CfgOrderDownloadDto;
import cn.com.yusys.yusp.dto.CfgOrderDownloadRecordDto;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.config.TriggerTask;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.dto.OrderDownloadDto;
import cn.com.yusys.yusp.repository.mapper.OrderDownloadMapper;
import cn.com.yusys.yusp.util.BizUtils;
import cn.com.yusys.yusp.util.ExcelWaterMarkUtils;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;

*/
/**
 * 预约下载逻辑
 * @author Administrator
 *
 *//*

@Service
public class OrderDownloadService {

	@Autowired
	private OrderDownloadTaskConfigurer orderDownloadTaskConfigurer;
	
	@Autowired
	private OrderDownloadMapper orderDownloadMapper;
	
	@Autowired
	private ICmisCfgClientService iCmisCfgClientService;
	


	
//	@Autowired
//	private OrderDownloadToFileService orderDownloadToFileService;
	
    @Value("${application.order.temppath:/home/cmis/temp}")
    private String tempPath;
	
    @Value("${application.order.excelpassword:test123}")
    private String excelPassWord;
    
	private static final Logger log = LoggerFactory.getLogger(OrderDownloadService.class);
	
	public String start(OrderDownloadDto orderDownload) {
		String time = orderDownload.getCronTime();
		String info = orderDownload.getDownloadAction();
		String taskId = orderDownload.getPkId();
		String fileName = orderDownload.getFileName();
		String rowNeedLock = orderDownload.getRowCfgId();
		if (!CronExpression.isValidExpression(time)) {
			return "1";
		}
//		String waterMarkId = orderDownload.getWatermarkId();
//		CfgExcelWaterMarkDto waterCfg = new CfgExcelWaterMarkDto();
//		if(StringUtils.isNotBlank(waterMarkId)) {
//			waterCfg = iCmisCfgClientService.getCfgExcelWaterMarkById(waterMarkId);
//		}
		int orderNum = orderDownload.getOrderNum()!=null?orderDownload.getOrderNum():0;
		String queryCons = StringUtils.isNotBlank(orderDownload.getQueryCons())?orderDownload.getQueryCons():" 1=1 "; 	
		ResultDto<Map> result = iCmisCfgClientService.generateQueryListSQLByQryCode(info);
		Map sqlMap = result.getData();
		String queryConsStr = "${QueryCons}";
		
		
		List<Map<String,String>> cfgFlexQryParamIndexDtoList = (List<Map<String,String>>) sqlMap.get("listTemp");
		
		new Thread() {
			public void run() {
				try {
					// 等待任务调度初始化完成
					while (!orderDownloadTaskConfigurer.inited()) {
						Thread.sleep(100);
					}
				} catch (Exception e) {
					log.error("任务调度初始化失败", e);
				}
				log.info("任务调度初始化完成，添加任务: " + taskId);
				orderDownloadTaskConfigurer.addTriggerTask(taskId, new TriggerTask(new Runnable() {

					@Override
					public void run() {
						CfgOrderDownloadDto cfgDto = iCmisCfgClientService.getOrderCfg(taskId);
						if(cfgDto==null) {
							throw new YuspException("500","预约配置"+taskId+"异常");
						}
						//每次运行前去查询一下
						int downCount = cfgDto.getDownloadCount() + 1;
						
						log.info(
								"开始执行预约下载...预约配置编号:" + taskId + "---------第" + downCount +"次下载");
					
						String querySql = (String) sqlMap.get("querySql");
						querySql = querySql.replace(queryConsStr, queryCons);
						List<Map<String, Object>> reusltlist = orderDownloadMapper.queryResultBySql(querySql); //查询出来的结果 
						String dateTime = DateUtils.getCurrentDate(DateFormatEnum.DATETIME);
						String path = createExcel(reusltlist,cfgFlexQryParamIndexDtoList,fileName,orderDownload);
						log.info("预约下载服务器路径保存在："+path + "记录发送至cmis-cfg服务");
						CfgOrderDownloadRecordDto cfgOrderDownloadRecordDto = new CfgOrderDownloadRecordDto();
						cfgOrderDownloadRecordDto.setRelPkId(taskId);
						cfgOrderDownloadRecordDto.setFilePath(path);
						cfgOrderDownloadRecordDto.setDataDate(dateTime);
						iCmisCfgClientService.insertDownloadRecord(cfgOrderDownloadRecordDto);
						if(downCount>=orderNum&&orderNum>0) {
							log.info("预约次数已到,终止任务");
							orderDownloadTaskConfigurer.cancelTriggerTask(taskId);
						}
					}
				}, new CronTrigger(time)));
			};
		}.start();
		return "0";
	}
	
	public String end(OrderDownloadDto cfgOrderDownload) {
		String time = cfgOrderDownload.getCronTime();
		String info = cfgOrderDownload.getDownloadAction();
		String taskId = cfgOrderDownload.getPkId();
		new Thread() {
			public void run() {
				try {
					// 等待任务调度初始化完成
					while (!orderDownloadTaskConfigurer.inited()) {
						Thread.sleep(100);
					}
				} catch (Exception e) {
					log.error("任务调度初始化失败", e);
				}
				log.info("手工终止任务" + taskId);
				orderDownloadTaskConfigurer.cancelTriggerTask(taskId);
			};
		}.start();
		return "0";
	}
	
	
    */
/**
     * 将 List<Map<String,Object>> 类型的数据导出为 Excel
     * 默认 Excel 文件的输出路径为 项目根目录下
     * 文件名为 filename + 时间戳 + .xlsx
     *
     * @param mapList 数据源(通常为数据库查询数据)
     * @param filename   文件名前缀, 实际文件名后会加上日期
     * @param title   表格首行标题
     * @return  文件输出路径
     *//*

    public String  createExcel(List<Map<String, Object>> mapList,List<Map<String,String>> cfgFlexQryParamIndexDtoList ,String filename,OrderDownloadDto orderDownload ) {
        //获取数据源的 key, 用于获取列数及设置标题
//        Map<String, Object> map = mapList.get(0);
//        Set<String> stringSet = map.keySet();
//        ArrayList<String> headList = new ArrayList<>();
    	List<String> needLockList = new ArrayList<String>();
    	String needLock = orderDownload.getRowCfgId(); // 获取要限制修改的字段
    	if(StringUtils.isNotBlank(needLock)) {
    		needLockList= Arrays.asList(needLock.split(","));
    	}
        //定义一个新的工作簿
        XSSFWorkbook wb = new XSSFWorkbook();
        //创建一个Sheet页
        XSSFSheet sheet = wb.createSheet(filename);
        //设置行高
        sheet.setDefaultRowHeight((short) (2 * 256));
        //为有数据的每列设置列宽
        for (int i = 0; i < cfgFlexQryParamIndexDtoList.size(); i++) {
            sheet.setColumnWidth(i, 8000);
        }
        //设置单元格字体样式
        XSSFFont font = wb.createFont();
        font.setFontName("等线");
        font.setFontHeightInPoints((short) 16);

        //在sheet里创建第一行，并设置单元格内容为 title (标题)
        XSSFRow titleRow = sheet.createRow(0);
        XSSFCell titleCell = titleRow.createCell(0);
        titleCell.setCellValue(filename);
        //合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, cfgFlexQryParamIndexDtoList.size() - 1));
        // 创建单元格文字居中样式并设置标题单元格居中
        XSSFCellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        titleCell.setCellStyle(cellStyle);

        
        //单元格锁定的样式
        XSSFCellStyle lockstyle = wb.createCellStyle();
        lockstyle.setLocked(true);
//        lockstyle.setFillForegroundColor(IndexedColors.RED.getIndex());
        //单元格不锁定的样式
        XSSFCellStyle unlockstyle = wb.createCellStyle();
        unlockstyle.setLocked(false);
	
        
        //获得表格第二行
        XSSFRow row = sheet.createRow(1);
        //根据数据源信息给第二行每一列设置标题
        for (int i = 0; i < cfgFlexQryParamIndexDtoList.size(); i++) {
            XSSFCell cell = row.createCell(i);
            cell.setCellValue(cfgFlexQryParamIndexDtoList.get(i).get("paramValue"));
            if (needLockList.contains(cfgFlexQryParamIndexDtoList.get(i).get("colNameEn"))){
            	cell.setCellStyle(lockstyle);
            }else{
            	cell.setCellStyle(unlockstyle);
            }
        }

        XSSFRow rows;
        XSSFCell cells;
        //循环拿到的数据给所有行每一列设置对应的值
        if(mapList!=null&&mapList.size()>0) {
        	for (int i = 0; i < mapList.size(); i++) {
                //在这个sheet页里创建一行
                rows = sheet.createRow(i + 2);
                //给该行数据赋值
                for (int j = 0; j < cfgFlexQryParamIndexDtoList.size(); j++) {
                	String colKey = cfgFlexQryParamIndexDtoList.get(j).get("colNameEn");
                	colKey = BizUtils.underlineToHump(col.key;
                	String value = "";
                	if( mapList.get(i).containsKey(colKey)&& mapList.get(i).get(colKey)!=null) {
                		value = mapList.get(i).get(colKey).toString();
                	}
                    cells = rows.createCell(j);
                    cells.setCellValue(value);
                    if (needLockList.contains(cfgFlexQryParamIndexDtoList.get(j).get("colNameEn"))){
                    	cells.setCellStyle(lockstyle);
                    }else{
                    	cells.setCellStyle(unlockstyle);
                    }
                }
            }
        }
        
        sheet.protectSheet(excelPassWord);
        sheet.enableLocking(); 
        
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        // 使用项目根目录, 文件名加上时间戳
        String path = tempPath + filename + dateFormat.format(date) + ".xlsx";
       	log.info("Excel文件输出路径: "+path);
        try {
        	if(StringUtils.isNotBlank(orderDownload.getWaterContent())) {
        		BufferedImage water = ExcelWaterMarkUtils.createWatermarkImage(orderDownload);
            	ExcelWaterMarkUtils.setWaterMarkToExcel2(wb, water);
        	}
        	//初始化连接
            File file = new File(path);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            wb.write(fileOutputStream);
            wb.close();
            fileOutputStream.close();
            
//           Map map = orderDownloadToFileService.uploadfile(file);   文件服务似乎有点问题，本地凑合用吧。。
//           log.info(map.toString());
           
        } catch (Exception e) {
          	log.error("预约下载生成文件异常",e);
        }
        return path;
    }

	public void download(String pkId,HttpServletResponse response) {
		String filePath = iCmisCfgClientService.getOrderFilePath(pkId);
	 	log.info("Excel文件下载路径: "+filePath);
	 	OutputStream toClient = null;
	 	InputStream fis = null;
	 	 try {
	            // path是指欲下载的文件的路径。
	            File file = new File(filePath);
	            // 取得文件名。
	            String filename = file.getName();
	            // 取得文件的后缀名。
	            String ext = filename.substring(filename.lastIndexOf(".") + 1).toUpperCase();
	 
	            // 以流的形式下载文件。
	            fis = new BufferedInputStream(new FileInputStream(filePath));
	            byte[] buffer = new byte[fis.available()];
	            if (fis.read(buffer) > 0) {
	            	fis.close();
	 	            response.reset();
	 	            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename,"UTF-8"));
	 	            toClient = new BufferedOutputStream(response.getOutputStream());
	 	            response.setContentType("application/vnd.ms-excel;charset=utf-8");
	 	            toClient.write(buffer);
	 	            toClient.flush();
	 	            toClient.close();
	            }
	        } catch (IOException e) {
	            log.error("文件下载异常",e);
	        } finally {
	        	 if (toClient != null) {
	        		 try {
						toClient.close();
					} catch (IOException e) {
						log.error("关闭OutputStream异常",e);
					}
                 }
	        	 
	        	 if (fis != null) {
	        		 try {
	        			 fis.close();
					} catch (IOException e) {
						log.error("关闭InputStream异常",e);
					}
                 }
	        }

	}

}
*/
