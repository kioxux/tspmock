/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccSubMapper;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccSubPrdMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccSubPrdService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-09 16:14:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtReplyAccSubPrdService {
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtAppService.class);

    @Resource
    private LmtReplyAccSubPrdMapper lmtReplyAccSubPrdMapper;

    @Resource
    private LmtReplyAccSubMapper lmtReplyAccSubMapper;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtReplySubPrdService lmtReplySubPrdService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private LmtReplyAccOperAppSubPrdService lmtReplyAccOperAppSubPrdService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private CtrLoanContService ctrLoanContService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtReplyAccSubPrd selectByPrimaryKey(String pkId) {
        return lmtReplyAccSubPrdMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtReplyAccSubPrd> selectAll(QueryModel model) {
        List<LmtReplyAccSubPrd> records = (List<LmtReplyAccSubPrd>) lmtReplyAccSubPrdMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtReplyAccSubPrd> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtReplyAccSubPrd> list = lmtReplyAccSubPrdMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtReplyAccSubPrd record) {
        return lmtReplyAccSubPrdMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtReplyAccSubPrd record) {
        return lmtReplyAccSubPrdMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public int update(LmtReplyAccSubPrd record) {
        return lmtReplyAccSubPrdMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtReplyAccSubPrd record) {
        return lmtReplyAccSubPrdMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtReplyAccSubPrdMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtReplyAccSubPrdMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryLmtReplyAccSubPrdByParams
     * @方法描述: 通过查询条件查询授批复台账分项适用品种信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtReplyAccSubPrd> queryLmtReplyAccSubPrdByParams(HashMap<String, String> queryMap) {
        return lmtReplyAccSubPrdMapper.queryLmtReplyAccSubPrdByParams(queryMap);
    }

    /**
     * @方法名称: queryLmtReplyAccSubPrdByAccSubPrdNo
     * @方法描述: 通过授信台账分项适用品种编号查询授批复台账分项适用品种信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtReplyAccSubPrd queryLmtReplyAccSubPrdByAccSubPrdNo(String accSubPrdNo) throws Exception {
        Map map = new HashMap();
        map.put("accSubPrdNo",accSubPrdNo);
        map.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdData(map);
        return lmtReplyAccSubPrd;
    }

    /**
     * @方法名称: queryLmtReplyAccSubPrdByAccSubPrdNo
     * @方法描述: 通过授信台账分项适用品种编号查询授批复台账分项适用品种信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtReplyAccSubPrd selectLmtReplyAccSubPrdByAccSubPrdNo(String accSubPrdNo){
        Map map = new HashMap();
        map.put("accSubPrdNo",accSubPrdNo);
        map.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdData(map);
        return lmtReplyAccSubPrd;
    }


    /**
     * @方法名称: selectByAccSubNo
     * @方法描述: 根据授信分项台帐号获取对应授信分项台账适用品种明细
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<LmtReplyAccSubPrd> selectByAccSubNo(String accSubNo) {
        Map map = new HashMap();
        map.put("accSubNo",accSubNo);
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return lmtReplyAccSubPrdMapper.queryLmtReplyAccSubPrdByParams(map);
    }

    /**
     * @方法名称: copyLmtAppSub
     * @方法描述: 发起授信申请变更时，将台账对应的授信分项挂载到新的授信变更申请下
     * @参数与返回说明:
     * @算法描述: 无
     */
    public boolean copyToLmtAppSubPrd(String originAccSubNo, String currentSubNo) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        String subPrdSerno = "";
        LmtAppSubPrd lmtAppSubPrd = new LmtAppSubPrd();
        LmtReplyAccSubPrd lmtReplyAccSubPrd = new LmtReplyAccSubPrd();
        Map map = new HashMap();
            map.put("accSubNo",originAccSubNo);
            List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdMapper.queryLmtReplyAccSubPrdByParams(map);
            if(lmtReplyAccSubPrdList != null && lmtReplyAccSubPrdList.size() > 0){
                for (int i = 0; i < lmtReplyAccSubPrdList.size(); i++) {
                    lmtReplyAccSubPrd = lmtReplyAccSubPrdList.get(i);
                    subPrdSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_SERNO, new HashMap<>());
                    if (StringUtils.isEmpty(subPrdSerno)) {
                        return false;
                    }
                    BeanUtils.copyProperties(lmtReplyAccSubPrd, lmtAppSubPrd);
                    lmtAppSubPrd.setPkId(UUID.randomUUID().toString());
                    lmtAppSubPrd.setSubSerno(currentSubNo);
                    lmtAppSubPrd.setSubPrdSerno(subPrdSerno);
                    User userInfo = SessionUtils.getUserInformation();
                    if (userInfo == null) {
                        return false;
                    } else {
                        lmtAppSubPrd.setInputId(userInfo.getLoginCode());
                        lmtAppSubPrd.setInputBrId(userInfo.getOrg().getCode());
                        lmtAppSubPrd.setInputDate(openday);
                        lmtAppSubPrd.setUpdId(userInfo.getLoginCode());
                        lmtAppSubPrd.setUpdBrId(userInfo.getOrg().getCode());
                        lmtAppSubPrd.setUpdDate(openday);
                        lmtAppSubPrd.setCreateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                        lmtAppSubPrd.setUpdateTime(DateUtils.parseDate(openday, DateFormatEnum.DEFAULT.getValue()));
                    }
                    int count = lmtAppSubPrdService.insert(lmtAppSubPrd);
                    if (count != 1) {
                        //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                        throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",分项挂载失败！");
                    }
                }
            }
        return true;
    }


    /**
     * @方法名称: generateNewLmtReplyAccSubPrd
     * @方法描述: 生成全新的授信批复分项适用品种台账
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplyAccSubPrdHandle(String replySubSerno, String accSubNo) throws Exception {
        // 通过查询授信批复分项适用品种，将查出的数据插入至批复分项台账适用品种中
        HashMap<String, String> queryReplyAccSubPrdMap = new HashMap<String, String>();
        queryReplyAccSubPrdMap.put("replySubSerno", replySubSerno);
        queryReplyAccSubPrdMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtReplySubPrd> lmtReplySubPrdList = lmtReplySubPrdService.queryLmtReplySubPrdByParams(queryReplyAccSubPrdMap);
        if(lmtReplySubPrdList != null && lmtReplySubPrdList.size() > 0){
            for (LmtReplySubPrd lmtReplySubPrd : lmtReplySubPrdList) {
                // 通过查询授信批复分项适用品种，将查出的数据插入至批复分项台账适用品种中
                this.generateNewLmtReplyAccSubPrdByLmtReplySubPrd(lmtReplySubPrd, accSubNo);
            }
        }else{
            throw new Exception("授信批复分项适用品种查询异常!");
        }
    }

    /**
     * @方法名称: generateNewLmtReplyAccSubPrdByLmtReplySubPrd
     * @方法描述: 通过批复分项适用品种生成新的授信台账分项适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void generateNewLmtReplyAccSubPrdByLmtReplySubPrd(LmtReplySubPrd lmtReplySubPrd, String accSubNo) {
        // 获取营业日期
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        LmtReplyAccSubPrd lmtReplyAccSubPrd = new LmtReplyAccSubPrd();
        BeanUtils.copyProperties(lmtReplySubPrd, lmtReplyAccSubPrd);
        lmtReplyAccSubPrd.setPkId(UUID.randomUUID().toString());
        String accSubPrdNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_DETAILS, new HashMap<>());
        log.info("生成新的授信台账分项适用品种号:" + accSubPrdNo);
        lmtReplyAccSubPrd.setAccSubNo(accSubNo);
        lmtReplyAccSubPrd.setAccSubPrdNo(accSubPrdNo);
        lmtReplyAccSubPrd.setReplySubSerno(lmtReplySubPrd.getReplySubSerno());
        lmtReplyAccSubPrd.setSubSerno(lmtReplySubPrd.getSubSerno());
        lmtReplyAccSubPrd.setAccStatus(CmisLmtConstants.STD_ZB_LMT_STATE_01);
        this.insert(lmtReplyAccSubPrd);
        Map<String,String> map = new HashMap<>();
        map.put("accSubNo",accSubNo);
        LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubService.getLmtReplyAccSubByAccSubNo(map);
        if(lmtReplyAccSub != null && StringUtils.nonEmpty(lmtReplyAccSub.getAccNo())){
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(lmtReplyAccSub.getAccNo());
            if(CmisCommonConstants.LMT_TYPE_03.equals(lmtReplyAcc.getLmtType())){
                if(StringUtils.nonEmpty(lmtReplySubPrd.getOrigiLmtAccSubPrdNo())){
                    ctrLoanContService.updateLmtAccNoByOldLmtAccNo(lmtReplySubPrd.getOrigiLmtAccSubPrdNo(),lmtReplyAccSubPrd.getAccSubPrdNo(),lmtReplyAcc.getReplySerno());
                }
            }
        }
        log.info("生成新的授信台账分项适用品种:" + accSubPrdNo);

        // 复制品种编号下的还款计划
        List<RepayCapPlan> list = repayCapPlanService.selectBySerno(lmtReplySubPrd.getReplySubPrdSerno());
        for(RepayCapPlan repayCapPlan : list){
            repayCapPlan.setPkId(UUID.randomUUID().toString());
            repayCapPlan.setSerno(accSubPrdNo);
            repayCapPlan.setUpdId(lmtReplyAccSubPrd.getUpdId());
            repayCapPlan.setUpdBrId(lmtReplyAccSubPrd.getUpdBrId());
            repayCapPlan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            repayCapPlan.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
            repayCapPlanService.insert(repayCapPlan);
        }
    }

    /**
     * @方法名称: updateLmtReplyAccSubPrdByLmtReplySubPrd
     * @方法描述: 通过
     * @参数与返回说明:
     * @算法描述:
     *  1.授信批复分项适用品种数据存在，对应分项台账适用品种数据也存在，则直接一一对应更新分项台账适用品种数据
     *  2.授信批复分项适用品种数据存在，对应分项台账适用品种数据不存在，则新增授信分项台账适用品种数据
     *  3.授信批复分项适用品种数据不存在，对应分项台账适用品种数据存在，经过与李成金讨论，申请时不存在此类数据，将分项金额改为0即可，不做处理。
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyAccSubPrdByLmtReplySubPrd(LmtReplySub lmtReplySub, String accSubNo) throws Exception {
        // 通过查询授信批复分项适用品种，将查出的数据插入至批复分项台账适用品种中
        HashMap<String, String> queryReplyAccSubPrdMap = new HashMap<String, String>();
        queryReplyAccSubPrdMap.put("replySubSerno", lmtReplySub.getReplySubSerno());
        queryReplyAccSubPrdMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        // 新生成的批复分项品种数据
        List<LmtReplySubPrd> lmtReplySubPrdList = lmtReplySubPrdService.queryLmtReplySubPrdByParams(queryReplyAccSubPrdMap);

        queryReplyAccSubPrdMap.put("accSubNo", lmtReplySub.getOrigiLmtAccSubNo());
        List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = this.queryLmtReplyAccSubPrdByParams(queryReplyAccSubPrdMap);

        if(lmtReplySubPrdList != null && lmtReplySubPrdList.size() > 0 && lmtReplyAccSubPrdList != null && lmtReplyAccSubPrdList.size() > 0){
            log.info("循环最新的批复分项品种信息,批复编号:"+lmtReplySubPrdList.get(0).getReplySubSerno());
            for (LmtReplySubPrd lmtReplySubPrd : lmtReplySubPrdList) {
                LmtReplyAccSubPrd lmtReplyAccSubPrd = new LmtReplyAccSubPrd();
                boolean isNew = true;
                for(LmtReplyAccSubPrd item : lmtReplyAccSubPrdList){
                    // 如果授信批复台账分项适用品种号与批复中的原批复台账分项适用品种一致，则将批复分项适用品种内容复制至批复台账分项适用品种中
                    log.info("如果授信批复台账分项品种号与批复中的原批复台账分项品种号一致，则将批复分项品种内容复制至批复台账分项品种中."+lmtReplySub.getOrigiLmtAccSubNo());
                    if(item.getAccSubPrdNo().equals(lmtReplySubPrd.getOrigiLmtAccSubPrdNo())){
                        BeanUtils.copyProperties(lmtReplySubPrd, lmtReplyAccSubPrd);
                        lmtReplyAccSubPrd.setPkId(item.getPkId());
                        lmtReplyAccSubPrd.setAccSubPrdNo(item.getAccSubPrdNo());
                        lmtReplyAccSubPrd.setAccSubNo(item.getAccSubNo());
                        lmtReplyAccSubPrd.setReplySubSerno(lmtReplySubPrd.getReplySubSerno());
                        lmtReplyAccSubPrd.setSubSerno(lmtReplySubPrd.getSubSerno());
                        // 如果是为授信分项品种的期限调整 并且为循环额度  ，则重新计算到期日
//                        if(!item.getLmtTerm().equals(lmtReplySubPrd.getLmtTerm()) && CmisCommonConstants.STD_ZB_YES_NO_1.equals(item.getIsRevolvLimit())){
//                            log.info("更新授信结束日，起始日为:"+item.getStartDate()+"授信期限为:"+lmtReplySubPrd.getLmtTerm()+","+item.getAccSubPrdNo()+","+lmtReplySubPrd.getReplySubPrdSerno());
//                            if(StringUtils.nonBlank(item.getStartDate())){
//                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//                                Date startDate = simpleDateFormat.parse(item.getStartDate());
//                                Date endDate = DateUtils.addMonth(startDate,lmtReplySubPrd.getLmtTerm());
//                                lmtReplyAccSubPrd.setEndDate(simpleDateFormat.format(endDate));
//                                // lmtReplyAccSubPrd.setEndDate(DateUtils.formatDate(DateUtils.addMonth(DateUtils.parseDate(item.getStartDate(), String.valueOf(DateFormatEnum.DEFAULT)), lmtReplySubPrd.getLmtTerm()), String.valueOf(DateFormatEnum.DEFAULT)));
//                            }
//                        }else{
//                            lmtReplyAccSubPrd.setEndDate(item.getEndDate());
//                        }
                        lmtReplyAccSubPrd.setAccStatus(CmisCommonConstants.ACC_STATUS_1);
                        this.update(lmtReplyAccSubPrd);
                        log.info("更新授信台账分项适用品种:" + lmtReplyAccSubPrd.getAccSubPrdNo());
                        isNew = false;
                        break;
                    }
                }
                if(isNew){
                    this.generateNewLmtReplyAccSubPrdByLmtReplySubPrd(lmtReplySubPrd, accSubNo);
                }
            }
            log.info("循环原批复台账分项品种信息,批复台账编号:"+lmtReplyAccSubPrdList.get(0).getReplySubSerno());
            for(LmtReplyAccSubPrd lmtReplyAccSubPrd : lmtReplyAccSubPrdList){
                boolean isExist = false;
                for(LmtReplySubPrd lmtReplySubPrd : lmtReplySubPrdList){
                    if(lmtReplySubPrd.getOrigiLmtAccSubPrdNo().equals(lmtReplyAccSubPrd.getAccSubPrdNo())){
                        isExist = true;
                        break;
                    }
                }
                if(isExist){
                    continue;
                }else{
                    log.info("当前批复台账分项品种信息已不存在,对当前分项品种信息执行逻辑删除操作:"+lmtReplyAccSubPrd.getAccSubPrdNo());
                    lmtReplyAccSubPrd.setOprType(CmisCommonConstants.OP_TYPE_02);
                    lmtReplyAccSubPrd.setLmtAmt(new BigDecimal(0));
                    this.updateSelective(lmtReplyAccSubPrd);
                }
            }
        }else{
            throw new Exception("查询批复分适用品种项或批复台账分项适用品种异常");
        }
    }


    /**
     * @方法名称: updateLmtReplyAccSubPrdBySerno
     * @方法描述: 通过授信批复台账冻结解冻终止编号更新授信批复分项台账适用品种
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-03 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public void updateLmtReplyAccSubPrdBySerno(String serno) throws Exception {
        List<LmtReplyAccOperAppSubPrd> lmtReplyAccOperAppSubPrdList = lmtReplyAccOperAppSubPrdService.queryLmtReplyAccOperAppSubPrdBySerno(serno);
        if(lmtReplyAccOperAppSubPrdList != null & lmtReplyAccOperAppSubPrdList.size() > 0){
            for (LmtReplyAccOperAppSubPrd lmtReplyAccOperAppSubPrd : lmtReplyAccOperAppSubPrdList) {
                log.info("更新授信台账分项适用品种状态:" + lmtReplyAccOperAppSubPrd.getAccSubPrdNo());
                LmtReplyAccSubPrd lmtReplyAccSubPrd = queryLmtReplyAccSubPrdByAccSubPrdNo(lmtReplyAccOperAppSubPrd.getAccSubPrdNo());
                lmtReplyAccSubPrd.setAccStatus(lmtReplyAccOperAppSubPrd.getAfterLmtStatus());
                update(lmtReplyAccSubPrd);
            }
        }else{
            throw new Exception("查询授信冻结解冻终止分项适用品种数据异常！");
        }
    }

    /**
     * @方法名称: queryLmtReplyAccSubPrdDataByParams
     * @方法描述: 查询客户名下生效的资产池额度批复
     * @参数与返回说明:
     * @创建人: qw
     * @创建时间: 2021-05-05 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public LmtReplyAccSubPrd queryLmtReplyAccSubPrdDataByParams(String cusId) {
        LmtReplyAccSubPrd lmtReplyAccSubPrd = null;
        HashMap<String, String> queryMap = new HashMap<>();
        queryMap.put("cusId",cusId);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        queryMap.put("lmtBizType", "10030101");
        List<LmtReplyAccSubPrd> LmtReplyAccSubPrdList = lmtReplyAccSubPrdMapper.queryLmtReplyAccSubPrdByParams(queryMap);
        if(CollectionUtils.nonEmpty(LmtReplyAccSubPrdList)){
            for (LmtReplyAccSubPrd replyAccSubPrdData : LmtReplyAccSubPrdList) {
                CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
                cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(replyAccSubPrdData.getInputId()));//金融机构代码
                cmisLmt0026ReqDto.setSubSerno(replyAccSubPrdData.getAccSubPrdNo());//分项编号
                cmisLmt0026ReqDto.setQueryType(CmisLmtConstants.STD_ZB_LMT_TYPE_01);//分项类型
                log.info("额度状态校验【{}】，前往额度系统进行额度状态校验开始,请求报文为:【{}】", replyAccSubPrdData.getAccSubPrdNo(), cmisLmt0026ReqDto.toString());
                ResultDto<CmisLmt0026RespDto> cmisLmt0026RespDtoResultDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto);
                log.info("额度状态校验【{}】，前往额度系统进行额度状态校验结束,响应报文为:【{}】", replyAccSubPrdData.getAccSubPrdNo(), cmisLmt0026RespDtoResultDto.toString());
                if(!"0".equals(cmisLmt0026RespDtoResultDto.getCode())){
                    throw BizException.error(null, EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
                }
                if(!"0000".equals(cmisLmt0026RespDtoResultDto.getData().getErrorCode())){
                    throw BizException.error(null,cmisLmt0026RespDtoResultDto.getData().getErrorCode(),cmisLmt0026RespDtoResultDto.getData().getErrorMsg());
                }
                String status = cmisLmt0026RespDtoResultDto.getData().getStatus();
                if(CmisLmtConstants.STD_ZB_LMT_STATE_01.equals(status)){
                    lmtReplyAccSubPrd =  replyAccSubPrdData;
                    break;
                }
            }
        }
        return lmtReplyAccSubPrd;
    }


    /**
     * @函数名称:getLmtReplyAccSubPrdByAccSubNo
     * @函数描述:根据分项额度编号查询
     * @参数与返回说明:
     * @算法描述:
     */
    public LmtReplyAccSubPrd getLmtReplyAccSubPrdByAccSubNo(String accSubNo) {
        if("".equals(accSubNo)||accSubNo == null){
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"参数为空!\"" + EcsEnum.E_SAVE_FAIL.value);
        }
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdByAccSubNo(accSubNo);
        return  lmtReplyAccSubPrd;
    }

    /**
     * @方法名称: queryLmtReplyAccSubPrdByAccSubPrdNo
     * @方法描述: 通过授信台账分项适用品种编号查询授批复台账分项适用品种信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtReplyAccSubPrd queryByParams(Map map) {
        map.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        LmtReplyAccSubPrd lmtReplyAccSubPrd = lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdData(map);
        return lmtReplyAccSubPrd;
    }

    /**
     * @函数名称:getLmtReplyAccSubDataByAccSubPrdNo
     * @函数描述:通过分项品种编号查询分项流水号
     * @参数与返回说明:
     * @算法描述:
     */
    public String getLmtReplyAccSubDataByAccSubPrdNo(String accSubPrdNo) {
        if("".equals(accSubPrdNo) || accSubPrdNo == null){
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"参数为空!\"" + EcsEnum.E_SAVE_FAIL.value);
        }
        String subSerno = lmtReplyAccSubPrdMapper.getLmtReplyAccSubDataByAccSubPrdNo(accSubPrdNo);
        return subSerno;
    }

    /**
     * @函数名称:getLmtReplyAccSubDataByAccSubPrdNo
     * @函数描述:通过分项额度号查询分项流水号
     * @参数与返回说明:
     * @算法描述:
     */
    public String getLmtReplyAccSubDataByAccSubNo(String accSubNo) {
        if("".equals(accSubNo) || accSubNo == null){
            throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"参数为空!\"" + EcsEnum.E_SAVE_FAIL.value);
        }
        String subSerno = lmtReplyAccSubPrdMapper.getLmtReplyAccSubDataByAccSubNo(accSubNo);
        return subSerno;
    }

    /**
     * @函数名称:getLmtReplyAccSubPrdByReplyNo
     * @函数描述:根据批复编号查询分项明细信息
     * @参数与返回说明:
     * @算法描述:
     */
    public List<LmtReplyAccSubPrd> getLmtReplyAccSubPrdByReplyNo(String replyNo){

        List<String> replySubNoList = lmtReplyAccSubMapper.getReplySubSernosByReplySerno(replyNo);
        List<LmtReplyAccSubPrd> result = new ArrayList<>();
        if(CollectionUtils.nonEmpty(replySubNoList)){
            result = lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdByReplySubNos(replySubNoList);
        }
        return result;
        //return lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdByReplyNo(replyNo);
    }

    /**
     * @函数名称:getLastEndDate
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    public String getLastEndDate(Map<String,String> map){
        return lmtReplyAccSubPrdMapper.getLastEndDate(map);
    }
    /**
     * @函数名称:getLmtReplyAccSubPrdByCusId
     * @函数描述:根据查到的集团成员客户号 查询授信批复台账
     * @参数与返回说明:
     * @算法描述:
     */
    public List<LmtReplyAccSubPrd> getLmtReplyAccSubPrdByCusId(String cusId){
        return lmtReplyAccSubPrdMapper.getLmtReplyAccSubPrdByCusId(cusId);
    }

    /**
     * @函数名称:getEarlyStartDate
     * @函数描述:
     * @参数与返回说明:
     * @算法描述:
     */
    public String getEarlyStartDate(Map<String,String> map){
        return lmtReplyAccSubPrdMapper.getEarlyStartDate(map);
    }

    public String getLastDateByAccNo(String accNo){
        return lmtReplyAccSubPrdMapper.getLastDateByAccNo(accNo);
    }

    public String getEarlyDateByAccNo(String accNo){
        return lmtReplyAccSubPrdMapper.getEarlyDateByAccNo(accNo);
    }

    public String getRevolimitLastDateByAccNo(String accNo){
        return lmtReplyAccSubPrdMapper.getRevolimitLastDateByAccNo(accNo);
    }

    public String getEarlyDateByGrpAccNo(String grpAccNo){
        boolean first = true;
        QueryModel model = new QueryModel();
        model.getCondition().put("grpSerno",grpAccNo);
        model.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
        model.getCondition().put("isPrtcptCurtDeclare",CmisCommonConstants.OP_TYPE_01);
        List<LmtGrpMemRel> accList = lmtGrpMemRelService.selectByModel(model);
        Date earlyDate = new Date();
        for(LmtGrpMemRel lmtGrpMemRel:accList){
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(lmtGrpMemRel.getSingleSerno());
            Date early = DateUtils.parseDate(getEarlyDateByAccNo(lmtReplyAcc.getAccNo()), DateFormatEnum.DEFAULT.getValue());
            if(first){
                earlyDate = early;
                first = false;
            }else{
                if(early.compareTo(earlyDate) < 0){
                    earlyDate = early;
                }
            }
        }
        return DateUtils.formatDate(earlyDate,DateFormatEnum.DEFAULT.getValue());
    }

    public String getLastDateByGrpAccNo(String grpAccNo){
        boolean first = true;
        QueryModel model = new QueryModel();
        model.getCondition().put("grpSerno",grpAccNo);
        model.getCondition().put("oprType",CmisCommonConstants.OP_TYPE_01);
        model.getCondition().put("isPrtcptCurtDeclare",CmisCommonConstants.OP_TYPE_01);
        List<LmtGrpMemRel> accList = lmtGrpMemRelService.selectByModel(model);
        Date lastDate = new Date();
        for(LmtGrpMemRel lmtGrpMemRel:accList){
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLmtReplyAccByAccNo(lmtGrpMemRel.getSingleSerno());
            Date last = DateUtils.parseDate(getLastDateByGrpAccNo(lmtReplyAcc.getAccNo()), DateFormatEnum.DEFAULT.getValue());
            if(first){
                lastDate = last;
                first = false;
            }else{
                if(last.compareTo(lastDate) > 0){
                    lastDate = last;
                }
            }
        }
        return DateUtils.formatDate(lastDate,DateFormatEnum.DEFAULT.getValue());
    }
}
