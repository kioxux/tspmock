/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.annotation.SeqId;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpPrePayment
 * @类描述: iqp_pre_payment数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-24 10:34:52
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_pre_payment")
public class IqpPrePayment extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@SeqId("GRT_SERNO")
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 20)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 贷款金额 **/
	@Column(name = "LOAN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanBalance;
	
	/** 贷款起始日 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
	private String loanStartDate;
	
	/** 贷款到期日 **/
	@Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
	private String loanEndDate;
	
	/** 贷款申请渠道 **/
	@Column(name = "LOAN_APP_CHNL", unique = false, nullable = true, length = 80)
	private String loanAppChnl;
	
	/** 台账状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;
	
	/** 是否可转让 **/
	@Column(name = "IS_ALLOW_TAKEOVER", unique = false, nullable = true, length = 5)
	private String isAllowTakeover;
	
	/** 还款模式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 还款模式明细 **/
	@Column(name = "REPAY_MODE_DESC", unique = false, nullable = true, length = 5)
	private String repayModeDesc;
	
	/** 贷款收回方式 **/
	@Column(name = "REPAY_TYPE", unique = false, nullable = true, length = 5)
	private String repayType;
	
	/** 资金来源 **/
	@Column(name = "AMT_SOURCE", unique = false, nullable = true, length = 5)
	private String amtSource;
	
	/** 还本金额 **/
	@Column(name = "REPAY_PRI_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayPriAmt;
	
	/** 还款金额 **/
	@Column(name = "REPAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal repayAmt;
	
	/** 是否缩期 **/
	@Column(name = "IS_PERIOD", unique = false, nullable = true, length = 5)
	private String isPeriod;
	
	/** 缩期期数 **/
	@Column(name = "PERIOD_TIMES", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal periodTimes;
	
	/** 是否第三方还款 **/
	@Column(name = "IS_OTHER_ACCT", unique = false, nullable = true, length = 5)
	private String isOtherAcct;
	
	/** 第三方还款账号 **/
	@Column(name = "REPAY_ACCT_NO", unique = false, nullable = true, length = 40)
	private String repayAcctNo;
	
	/** 还款账户名称 **/
	@Column(name = "REPAY_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String repayAcctName;
	
	/** 还款账户子序号 **/
	@Column(name = "REPAY_SUB_ACCNO", unique = false, nullable = true, length = 40)
	private String repaySubAccno;
	
	/** 1-归还正常本金 **/
	@Column(name = "ZCBJ_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal zcbjAmt;
	
	/** 2-归还呆滞本金 **/
	@Column(name = "DZBJ_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dzbjAmt;
	
	/** 3-归还应收应计利息 **/
	@Column(name = "YSYJLX_XMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ysyjlxXmt;
	
	/** 4-归还应收欠息 **/
	@Column(name = "YSQX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ysqxAmt;
	
	/** 5-归还应收应计罚息 **/
	@Column(name = "YSYJFX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ysyjfxAmt;
	
	/** 6-归还应收罚息 **/
	@Column(name = "YSFX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ysfxAmt;
	
	/** 7-归还应计复息 **/
	@Column(name = "YJFX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yjfxAmt;
	
	/** 8-归还应收罚金 **/
	@Column(name = "YSFJ_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ysfjAmt;
	
	/** 9-归还逾期本金 **/
	@Column(name = "YQBJ_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal yqbjAmt;
	
	/** 10-归还呆账本金 **/
	@Column(name = "DAIZBJ_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal daizbjAmt;
	
	/** 11-归还催收应计立项 **/
	@Column(name = "CSYJLX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal csyjlxAmt;
	
	/** 12-归还催收欠息 **/
	@Column(name = "CSQX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal csqxAmt;
	
	/** 13-归还催收应计罚息 **/
	@Column(name = "CSYJFX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal csyjfxAmt;
	
	/** 14-归还催收罚息 **/
	@Column(name = "CSFX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal csfxAmt;
	
	/** 15-归还复息 **/
	@Column(name = "FX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal fxAmt;
	
	/** 本金 **/
	@Column(name = "TOTAL_BJ_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalBjAmt;
	
	/** 利息 **/
	@Column(name = "TOTAL_LX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalLxAmt;
	
	/** 罚息 **/
	@Column(name = "TOTAL_FX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalFxAmt;
	
	/** 复息 **/
	@Column(name = "TOTAL_FUX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalFuxAmt;
	
	/** 核销本金 **/
	@Column(name = "TOTAL_HXBJ_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalHxbjAmt;
	
	/** 核销利息 **/
	@Column(name = "TOTAL_HXLX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalHxlxAmt;
	
	/** 拖欠利息总额 **/
	@Column(name = "TOTAL_TQLX_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalTqlxAmt;
	
	/** 申请渠道 **/
	@Column(name = "APP_CHNL", unique = false, nullable = true, length = 80)
	private String appChnl;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 5)
	private String status;
	
	/** 原因 **/
	@Column(name = "RESN", unique = false, nullable = true, length = 4000)
	private String resn;

	/** 核心交易流水号 **/
	@Column(name = "HXSERNO", unique = false, nullable = true, length = 40)
	private String hxSerno;

	/** 核心交易日期 **/
	@Column(name = "HXDATE", unique = false, nullable = true, length = 40)
	private String hxDate;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return loanAmt
     */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}
	
    /**
     * @return loanStartDate
     */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}
	
    /**
     * @return loanEndDate
     */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param loanAppChnl
	 */
	public void setLoanAppChnl(String loanAppChnl) {
		this.loanAppChnl = loanAppChnl;
	}
	
    /**
     * @return loanAppChnl
     */
	public String getLoanAppChnl() {
		return this.loanAppChnl;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param isAllowTakeover
	 */
	public void setIsAllowTakeover(String isAllowTakeover) {
		this.isAllowTakeover = isAllowTakeover;
	}
	
    /**
     * @return isAllowTakeover
     */
	public String getIsAllowTakeover() {
		return this.isAllowTakeover;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param repayModeDesc
	 */
	public void setRepayModeDesc(String repayModeDesc) {
		this.repayModeDesc = repayModeDesc;
	}
	
    /**
     * @return repayModeDesc
     */
	public String getRepayModeDesc() {
		return this.repayModeDesc;
	}
	
	/**
	 * @param repayType
	 */
	public void setRepayType(String repayType) {
		this.repayType = repayType;
	}
	
    /**
     * @return repayType
     */
	public String getRepayType() {
		return this.repayType;
	}
	
	/**
	 * @param amtSource
	 */
	public void setAmtSource(String amtSource) {
		this.amtSource = amtSource;
	}
	
    /**
     * @return amtSource
     */
	public String getAmtSource() {
		return this.amtSource;
	}
	
	/**
	 * @param repayPriAmt
	 */
	public void setRepayPriAmt(java.math.BigDecimal repayPriAmt) {
		this.repayPriAmt = repayPriAmt;
	}
	
    /**
     * @return repayPriAmt
     */
	public java.math.BigDecimal getRepayPriAmt() {
		return this.repayPriAmt;
	}
	
	/**
	 * @param repayAmt
	 */
	public void setRepayAmt(java.math.BigDecimal repayAmt) {
		this.repayAmt = repayAmt;
	}
	
    /**
     * @return repayAmt
     */
	public java.math.BigDecimal getRepayAmt() {
		return this.repayAmt;
	}
	
	/**
	 * @param isPeriod
	 */
	public void setIsPeriod(String isPeriod) {
		this.isPeriod = isPeriod;
	}
	
    /**
     * @return isPeriod
     */
	public String getIsPeriod() {
		return this.isPeriod;
	}
	
	/**
	 * @param periodTimes
	 */
	public void setPeriodTimes(java.math.BigDecimal periodTimes) {
		this.periodTimes = periodTimes;
	}
	
    /**
     * @return periodTimes
     */
	public java.math.BigDecimal getPeriodTimes() {
		return this.periodTimes;
	}
	
	/**
	 * @param isOtherAcct
	 */
	public void setIsOtherAcct(String isOtherAcct) {
		this.isOtherAcct = isOtherAcct;
	}
	
    /**
     * @return isOtherAcct
     */
	public String getIsOtherAcct() {
		return this.isOtherAcct;
	}
	
	/**
	 * @param repayAcctNo
	 */
	public void setRepayAcctNo(String repayAcctNo) {
		this.repayAcctNo = repayAcctNo;
	}
	
    /**
     * @return repayAcctNo
     */
	public String getRepayAcctNo() {
		return this.repayAcctNo;
	}
	
	/**
	 * @param repayAcctName
	 */
	public void setRepayAcctName(String repayAcctName) {
		this.repayAcctName = repayAcctName;
	}
	
    /**
     * @return repayAcctName
     */
	public String getRepayAcctName() {
		return this.repayAcctName;
	}
	
	/**
	 * @param repaySubAccno
	 */
	public void setRepaySubAccno(String repaySubAccno) {
		this.repaySubAccno = repaySubAccno;
	}
	
    /**
     * @return repaySubAccno
     */
	public String getRepaySubAccno() {
		return this.repaySubAccno;
	}
	
	/**
	 * @param zcbjAmt
	 */
	public void setZcbjAmt(java.math.BigDecimal zcbjAmt) {
		this.zcbjAmt = zcbjAmt;
	}
	
    /**
     * @return zcbjAmt
     */
	public java.math.BigDecimal getZcbjAmt() {
		return this.zcbjAmt;
	}
	
	/**
	 * @param dzbjAmt
	 */
	public void setDzbjAmt(java.math.BigDecimal dzbjAmt) {
		this.dzbjAmt = dzbjAmt;
	}
	
    /**
     * @return dzbjAmt
     */
	public java.math.BigDecimal getDzbjAmt() {
		return this.dzbjAmt;
	}
	
	/**
	 * @param ysyjlxXmt
	 */
	public void setYsyjlxXmt(java.math.BigDecimal ysyjlxXmt) {
		this.ysyjlxXmt = ysyjlxXmt;
	}
	
    /**
     * @return ysyjlxXmt
     */
	public java.math.BigDecimal getYsyjlxXmt() {
		return this.ysyjlxXmt;
	}
	
	/**
	 * @param ysqxAmt
	 */
	public void setYsqxAmt(java.math.BigDecimal ysqxAmt) {
		this.ysqxAmt = ysqxAmt;
	}
	
    /**
     * @return ysqxAmt
     */
	public java.math.BigDecimal getYsqxAmt() {
		return this.ysqxAmt;
	}
	
	/**
	 * @param ysyjfxAmt
	 */
	public void setYsyjfxAmt(java.math.BigDecimal ysyjfxAmt) {
		this.ysyjfxAmt = ysyjfxAmt;
	}
	
    /**
     * @return ysyjfxAmt
     */
	public java.math.BigDecimal getYsyjfxAmt() {
		return this.ysyjfxAmt;
	}
	
	/**
	 * @param ysfxAmt
	 */
	public void setYsfxAmt(java.math.BigDecimal ysfxAmt) {
		this.ysfxAmt = ysfxAmt;
	}
	
    /**
     * @return ysfxAmt
     */
	public java.math.BigDecimal getYsfxAmt() {
		return this.ysfxAmt;
	}
	
	/**
	 * @param yjfxAmt
	 */
	public void setYjfxAmt(java.math.BigDecimal yjfxAmt) {
		this.yjfxAmt = yjfxAmt;
	}
	
    /**
     * @return yjfxAmt
     */
	public java.math.BigDecimal getYjfxAmt() {
		return this.yjfxAmt;
	}
	
	/**
	 * @param ysfjAmt
	 */
	public void setYsfjAmt(java.math.BigDecimal ysfjAmt) {
		this.ysfjAmt = ysfjAmt;
	}
	
    /**
     * @return ysfjAmt
     */
	public java.math.BigDecimal getYsfjAmt() {
		return this.ysfjAmt;
	}
	
	/**
	 * @param yqbjAmt
	 */
	public void setYqbjAmt(java.math.BigDecimal yqbjAmt) {
		this.yqbjAmt = yqbjAmt;
	}
	
    /**
     * @return yqbjAmt
     */
	public java.math.BigDecimal getYqbjAmt() {
		return this.yqbjAmt;
	}
	
	/**
	 * @param daizbjAmt
	 */
	public void setDaizbjAmt(java.math.BigDecimal daizbjAmt) {
		this.daizbjAmt = daizbjAmt;
	}
	
    /**
     * @return daizbjAmt
     */
	public java.math.BigDecimal getDaizbjAmt() {
		return this.daizbjAmt;
	}
	
	/**
	 * @param csyjlxAmt
	 */
	public void setCsyjlxAmt(java.math.BigDecimal csyjlxAmt) {
		this.csyjlxAmt = csyjlxAmt;
	}
	
    /**
     * @return csyjlxAmt
     */
	public java.math.BigDecimal getCsyjlxAmt() {
		return this.csyjlxAmt;
	}
	
	/**
	 * @param csqxAmt
	 */
	public void setCsqxAmt(java.math.BigDecimal csqxAmt) {
		this.csqxAmt = csqxAmt;
	}
	
    /**
     * @return csqxAmt
     */
	public java.math.BigDecimal getCsqxAmt() {
		return this.csqxAmt;
	}
	
	/**
	 * @param csyjfxAmt
	 */
	public void setCsyjfxAmt(java.math.BigDecimal csyjfxAmt) {
		this.csyjfxAmt = csyjfxAmt;
	}
	
    /**
     * @return csyjfxAmt
     */
	public java.math.BigDecimal getCsyjfxAmt() {
		return this.csyjfxAmt;
	}
	
	/**
	 * @param csfxAmt
	 */
	public void setCsfxAmt(java.math.BigDecimal csfxAmt) {
		this.csfxAmt = csfxAmt;
	}
	
    /**
     * @return csfxAmt
     */
	public java.math.BigDecimal getCsfxAmt() {
		return this.csfxAmt;
	}
	
	/**
	 * @param fxAmt
	 */
	public void setFxAmt(java.math.BigDecimal fxAmt) {
		this.fxAmt = fxAmt;
	}
	
    /**
     * @return fxAmt
     */
	public java.math.BigDecimal getFxAmt() {
		return this.fxAmt;
	}
	
	/**
	 * @param totalBjAmt
	 */
	public void setTotalBjAmt(java.math.BigDecimal totalBjAmt) {
		this.totalBjAmt = totalBjAmt;
	}
	
    /**
     * @return totalBjAmt
     */
	public java.math.BigDecimal getTotalBjAmt() {
		return this.totalBjAmt;
	}
	
	/**
	 * @param totalLxAmt
	 */
	public void setTotalLxAmt(java.math.BigDecimal totalLxAmt) {
		this.totalLxAmt = totalLxAmt;
	}
	
    /**
     * @return totalLxAmt
     */
	public java.math.BigDecimal getTotalLxAmt() {
		return this.totalLxAmt;
	}
	
	/**
	 * @param totalFxAmt
	 */
	public void setTotalFxAmt(java.math.BigDecimal totalFxAmt) {
		this.totalFxAmt = totalFxAmt;
	}
	
    /**
     * @return totalFxAmt
     */
	public java.math.BigDecimal getTotalFxAmt() {
		return this.totalFxAmt;
	}
	
	/**
	 * @param totalFuxAmt
	 */
	public void setTotalFuxAmt(java.math.BigDecimal totalFuxAmt) {
		this.totalFuxAmt = totalFuxAmt;
	}
	
    /**
     * @return totalFuxAmt
     */
	public java.math.BigDecimal getTotalFuxAmt() {
		return this.totalFuxAmt;
	}
	
	/**
	 * @param totalHxbjAmt
	 */
	public void setTotalHxbjAmt(java.math.BigDecimal totalHxbjAmt) {
		this.totalHxbjAmt = totalHxbjAmt;
	}
	
    /**
     * @return totalHxbjAmt
     */
	public java.math.BigDecimal getTotalHxbjAmt() {
		return this.totalHxbjAmt;
	}
	
	/**
	 * @param totalHxlxAmt
	 */
	public void setTotalHxlxAmt(java.math.BigDecimal totalHxlxAmt) {
		this.totalHxlxAmt = totalHxlxAmt;
	}
	
    /**
     * @return totalHxlxAmt
     */
	public java.math.BigDecimal getTotalHxlxAmt() {
		return this.totalHxlxAmt;
	}
	
	/**
	 * @param totalTqlxAmt
	 */
	public void setTotalTqlxAmt(java.math.BigDecimal totalTqlxAmt) {
		this.totalTqlxAmt = totalTqlxAmt;
	}
	
    /**
     * @return totalTqlxAmt
     */
	public java.math.BigDecimal getTotalTqlxAmt() {
		return this.totalTqlxAmt;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl;
	}
	
    /**
     * @return appChnl
     */
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param resn
	 */
	public void setResn(String resn) {
		this.resn = resn;
	}

    /**
     * @return resn
     */
	public String getResn() {
		return this.resn;
	}

	/**
	 * @param hxSerno
	 */
	public void setHxSerno(String hxSerno) {
		this.hxSerno = hxSerno;
	}

	/**
     * @return hxSerno
     */
	public String getHxSerno() {
		return this.hxSerno;
	}

	/**
	 * @param hxDate
	 */
	public void setHxDate(String hxDate) {
		this.hxDate = hxDate;
	}

	/**
     * @return hxDate
     */
	public String getHxDate() {
		return this.hxDate;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}