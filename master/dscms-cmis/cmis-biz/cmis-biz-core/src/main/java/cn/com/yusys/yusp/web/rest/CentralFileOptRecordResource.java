/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CentralFileOptRecord;
import cn.com.yusys.yusp.service.CentralFileOptRecordService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralFileOptRecordResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-08 23:24:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/centralfileoptrecord")
public class CentralFileOptRecordResource {
    @Autowired
    private CentralFileOptRecordService centralFileOptRecordService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CentralFileOptRecord>> query() {
        QueryModel queryModel = new QueryModel();
        List<CentralFileOptRecord> list = centralFileOptRecordService.selectAll(queryModel);
        return new ResultDto<List<CentralFileOptRecord>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CentralFileOptRecord>> index(QueryModel queryModel) {
        List<CentralFileOptRecord> list = centralFileOptRecordService.selectByModel(queryModel);
        return new ResultDto<List<CentralFileOptRecord>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CentralFileOptRecord>> query(@RequestBody QueryModel queryModel) {
        List<CentralFileOptRecord> list = centralFileOptRecordService.selectByModel(queryModel);
        return new ResultDto<List<CentralFileOptRecord>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CentralFileOptRecord> show(@PathVariable("pkId") String pkId) {
        CentralFileOptRecord centralFileOptRecord = centralFileOptRecordService.selectByPrimaryKey(pkId);
        return new ResultDto<CentralFileOptRecord>(centralFileOptRecord);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<Integer> create(@RequestBody CentralFileOptRecord centralFileOptRecord) throws URISyntaxException {
        int result = centralFileOptRecordService.insert(centralFileOptRecord);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CentralFileOptRecord centralFileOptRecord) throws URISyntaxException {
        int result = centralFileOptRecordService.update(centralFileOptRecord);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = centralFileOptRecordService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = centralFileOptRecordService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
