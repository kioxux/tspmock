package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplAppt
 * @类描述: iqp_appl_appt数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-07 10:27:27
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpApplApptDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 贷款申请人主键 **/
	private String apptCode;

	/** 申请流水号 **/
	private String iqpSerno;

	/** 预受理申请编号 **/
	private String preIqpSerno;

	/** 客户编号 **/
	private String cusId;

	/** 申请人姓名 **/
	private String cusName;

	/** 证件类型 STD_ZB_CERT_TYPE **/
	private String certType;

	/** 证件号码 **/
	private String certCode;

	/** 发证机关 **/
	private String issuDep;

	/** 有效期开始日期 **/
	private String issuStartDt;

	/** 有效期截止日期 **/
	private String issuEndDt;

	/** 客户曾用名 **/
	private String perCustName;

	/** 英文名称 **/
	private String enName;

	/** 性别 **/
	private String sex;

	/** 国籍 **/
	private String cnty;

	/** 国籍补充信息 **/
	private String cntyExt;

	/** 地区 **/
	private String area;

	/** 户籍地址（省/直辖市 ） **/
	private String permanentPrivice;

	/** 地级市/区 **/
	private String permanentCity;

	/** 市级行政区/县/县级市 **/
	private String permanentArea;

	/** 户籍详细地址 **/
	private String permanentAddr;

	/** 婚姻状况 **/
	private String marrSts;

	/** 学历 **/
	private String edu;

	/** 学位 **/
	private String degree;

	/** 是否为农户 **/
	private String isHousehold;

	/** 是否自雇人士 **/
	private String isSelfEmploy;

	/** 居住信息-省/直辖市 **/
	private String livingPrivice;

	/** 居住信息-地级市/区 **/
	private String livingCity;

	/** 居住信息-市级行政区/县/县级市 **/
	private String livingArea;

	/** 居住地址 **/
	private String livingAddr;

	/** 居住状况 **/
	private String livingSts;

	/** 通讯地址类型
 **/
	private String postSendAddrKind;

	/** 通讯信息-省/直辖市 **/
	private String postSendProvience;

	/** 通讯信息-地级市/区 **/
	private String postSendCity;

	/** 通讯信息-市级行政区/县/县级市 **/
	private String postSendArea;

	/** 通讯地址补充 **/
	private String postSendAddr;

	/** 邮编 **/
	private String postCode;

	/** 电话号码/手机号 **/
	private String tel;

	/** 备用电话 **/
	private String backTel;

	/** 电子邮件 **/
	private String mail;

	/** 年龄 **/
	private Integer age;

	/** 出生日期 **/
	private String birthday;

	/** 配偶是否共借人 **/
	private String isCommBorr;

	/** 工作单位 **/
	private String companyName;

	/** 月收入（元） **/
	private java.math.BigDecimal monIncome;

	/** 居住信息-邮编 **/
	private String livingZip;

	/** 与主借人关系 STD_ZB_REL_WITH_APP **/
	private String relWithApplyer;

	/** 登记时间 **/
	private String createTime;

	/** 登记人 **/
	private String createUsr;

	/** 最后修改时间 **/
	private String uPDDate;

	/** 最后修改人 **/
	private String updId;

	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;

	/** 关系类型 **/
	private String relType;

	public String getRelType() {
		return relType;
	}

	public void setRelType(String relType) {
		this.relType = relType;
	}

	/**
	 * @param apptCode
	 */
	public void setApptCode(String apptCode) {
		this.apptCode = apptCode == null ? null : apptCode.trim();
	}

    /**
     * @return ApptCode
     */
	public String getApptCode() {
		return this.apptCode;
	}

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}

    /**
     * @return IqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param preIqpSerno
	 */
	public void setPreIqpSerno(String preIqpSerno) {
		this.preIqpSerno = preIqpSerno == null ? null : preIqpSerno.trim();
	}

    /**
     * @return PreIqpSerno
     */
	public String getPreIqpSerno() {
		return this.preIqpSerno;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}

    /**
     * @return CusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}

    /**
     * @return CusName
     */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}

    /**
     * @return CertType
     */
	public String getCertType() {
		return this.certType;
	}

	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}

    /**
     * @return CertCode
     */
	public String getCertCode() {
		return this.certCode;
	}

	/**
	 * @param issuDep
	 */
	public void setIssuDep(String issuDep) {
		this.issuDep = issuDep == null ? null : issuDep.trim();
	}

    /**
     * @return IssuDep
     */
	public String getIssuDep() {
		return this.issuDep;
	}

	/**
	 * @param issuStartDt
	 */
	public void setIssuStartDt(String issuStartDt) {
		this.issuStartDt = issuStartDt == null ? null : issuStartDt.trim();
	}

    /**
     * @return IssuStartDt
     */
	public String getIssuStartDt() {
		return this.issuStartDt;
	}

	/**
	 * @param issuEndDt
	 */
	public void setIssuEndDt(String issuEndDt) {
		this.issuEndDt = issuEndDt == null ? null : issuEndDt.trim();
	}

    /**
     * @return IssuEndDt
     */
	public String getIssuEndDt() {
		return this.issuEndDt;
	}

	/**
	 * @param perCustName
	 */
	public void setPerCustName(String perCustName) {
		this.perCustName = perCustName == null ? null : perCustName.trim();
	}

    /**
     * @return PerCustName
     */
	public String getPerCustName() {
		return this.perCustName;
	}

	/**
	 * @param enName
	 */
	public void setEnName(String enName) {
		this.enName = enName == null ? null : enName.trim();
	}

    /**
     * @return EnName
     */
	public String getEnName() {
		return this.enName;
	}

	/**
	 * @param sex
	 */
	public void setSex(String sex) {
		this.sex = sex == null ? null : sex.trim();
	}

    /**
     * @return Sex
     */
	public String getSex() {
		return this.sex;
	}

	/**
	 * @param cnty
	 */
	public void setCnty(String cnty) {
		this.cnty = cnty == null ? null : cnty.trim();
	}

    /**
     * @return Cnty
     */
	public String getCnty() {
		return this.cnty;
	}

	/**
	 * @param cntyExt
	 */
	public void setCntyExt(String cntyExt) {
		this.cntyExt = cntyExt == null ? null : cntyExt.trim();
	}

    /**
     * @return CntyExt
     */
	public String getCntyExt() {
		return this.cntyExt;
	}

	/**
	 * @param area
	 */
	public void setArea(String area) {
		this.area = area == null ? null : area.trim();
	}

    /**
     * @return Area
     */
	public String getArea() {
		return this.area;
	}

	/**
	 * @param permanentPrivice
	 */
	public void setPermanentPrivice(String permanentPrivice) {
		this.permanentPrivice = permanentPrivice == null ? null : permanentPrivice.trim();
	}

    /**
     * @return PermanentPrivice
     */
	public String getPermanentPrivice() {
		return this.permanentPrivice;
	}

	/**
	 * @param permanentCity
	 */
	public void setPermanentCity(String permanentCity) {
		this.permanentCity = permanentCity == null ? null : permanentCity.trim();
	}

    /**
     * @return PermanentCity
     */
	public String getPermanentCity() {
		return this.permanentCity;
	}

	/**
	 * @param permanentArea
	 */
	public void setPermanentArea(String permanentArea) {
		this.permanentArea = permanentArea == null ? null : permanentArea.trim();
	}

    /**
     * @return PermanentArea
     */
	public String getPermanentArea() {
		return this.permanentArea;
	}

	/**
	 * @param permanentAddr
	 */
	public void setPermanentAddr(String permanentAddr) {
		this.permanentAddr = permanentAddr == null ? null : permanentAddr.trim();
	}

    /**
     * @return PermanentAddr
     */
	public String getPermanentAddr() {
		return this.permanentAddr;
	}

	/**
	 * @param marrSts
	 */
	public void setMarrSts(String marrSts) {
		this.marrSts = marrSts == null ? null : marrSts.trim();
	}

    /**
     * @return MarrSts
     */
	public String getMarrSts() {
		return this.marrSts;
	}

	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu == null ? null : edu.trim();
	}

    /**
     * @return Edu
     */
	public String getEdu() {
		return this.edu;
	}

	/**
	 * @param degree
	 */
	public void setDegree(String degree) {
		this.degree = degree == null ? null : degree.trim();
	}

    /**
     * @return Degree
     */
	public String getDegree() {
		return this.degree;
	}

	/**
	 * @param isHousehold
	 */
	public void setIsHousehold(String isHousehold) {
		this.isHousehold = isHousehold == null ? null : isHousehold.trim();
	}

    /**
     * @return IsHousehold
     */
	public String getIsHousehold() {
		return this.isHousehold;
	}

	/**
	 * @param isSelfEmploy
	 */
	public void setIsSelfEmploy(String isSelfEmploy) {
		this.isSelfEmploy = isSelfEmploy == null ? null : isSelfEmploy.trim();
	}

    /**
     * @return IsSelfEmploy
     */
	public String getIsSelfEmploy() {
		return this.isSelfEmploy;
	}

	/**
	 * @param livingPrivice
	 */
	public void setLivingPrivice(String livingPrivice) {
		this.livingPrivice = livingPrivice == null ? null : livingPrivice.trim();
	}

    /**
     * @return LivingPrivice
     */
	public String getLivingPrivice() {
		return this.livingPrivice;
	}

	/**
	 * @param livingCity
	 */
	public void setLivingCity(String livingCity) {
		this.livingCity = livingCity == null ? null : livingCity.trim();
	}

    /**
     * @return LivingCity
     */
	public String getLivingCity() {
		return this.livingCity;
	}

	/**
	 * @param livingArea
	 */
	public void setLivingArea(String livingArea) {
		this.livingArea = livingArea == null ? null : livingArea.trim();
	}

    /**
     * @return LivingArea
     */
	public String getLivingArea() {
		return this.livingArea;
	}

	/**
	 * @param livingAddr
	 */
	public void setLivingAddr(String livingAddr) {
		this.livingAddr = livingAddr == null ? null : livingAddr.trim();
	}

    /**
     * @return LivingAddr
     */
	public String getLivingAddr() {
		return this.livingAddr;
	}

	/**
	 * @param livingSts
	 */
	public void setLivingSts(String livingSts) {
		this.livingSts = livingSts == null ? null : livingSts.trim();
	}

    /**
     * @return LivingSts
     */
	public String getLivingSts() {
		return this.livingSts;
	}

	/**
	 * @param postSendAddrKind
	 */
	public void setPostSendAddrKind(String postSendAddrKind) {
		this.postSendAddrKind = postSendAddrKind == null ? null : postSendAddrKind.trim();
	}

    /**
     * @return PostSendAddrKind
     */
	public String getPostSendAddrKind() {
		return this.postSendAddrKind;
	}

	/**
	 * @param postSendProvience
	 */
	public void setPostSendProvience(String postSendProvience) {
		this.postSendProvience = postSendProvience == null ? null : postSendProvience.trim();
	}

    /**
     * @return PostSendProvience
     */
	public String getPostSendProvience() {
		return this.postSendProvience;
	}

	/**
	 * @param postSendCity
	 */
	public void setPostSendCity(String postSendCity) {
		this.postSendCity = postSendCity == null ? null : postSendCity.trim();
	}

    /**
     * @return PostSendCity
     */
	public String getPostSendCity() {
		return this.postSendCity;
	}

	/**
	 * @param postSendArea
	 */
	public void setPostSendArea(String postSendArea) {
		this.postSendArea = postSendArea == null ? null : postSendArea.trim();
	}

    /**
     * @return PostSendArea
     */
	public String getPostSendArea() {
		return this.postSendArea;
	}

	/**
	 * @param postSendAddr
	 */
	public void setPostSendAddr(String postSendAddr) {
		this.postSendAddr = postSendAddr == null ? null : postSendAddr.trim();
	}

    /**
     * @return PostSendAddr
     */
	public String getPostSendAddr() {
		return this.postSendAddr;
	}

	/**
	 * @param postCode
	 */
	public void setPostCode(String postCode) {
		this.postCode = postCode == null ? null : postCode.trim();
	}

    /**
     * @return PostCode
     */
	public String getPostCode() {
		return this.postCode;
	}

	/**
	 * @param tel
	 */
	public void setTel(String tel) {
		this.tel = tel == null ? null : tel.trim();
	}

    /**
     * @return Tel
     */
	public String getTel() {
		return this.tel;
	}

	/**
	 * @param backTel
	 */
	public void setBackTel(String backTel) {
		this.backTel = backTel == null ? null : backTel.trim();
	}

    /**
     * @return BackTel
     */
	public String getBackTel() {
		return this.backTel;
	}

	/**
	 * @param mail
	 */
	public void setMail(String mail) {
		this.mail = mail == null ? null : mail.trim();
	}

    /**
     * @return Mail
     */
	public String getMail() {
		return this.mail;
	}

	/**
	 * @param age
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

    /**
     * @return Age
     */
	public Integer getAge() {
		return this.age;
	}

	/**
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		this.birthday = birthday == null ? null : birthday.trim();
	}

    /**
     * @return Birthday
     */
	public String getBirthday() {
		return this.birthday;
	}

	/**
	 * @param isCommBorr
	 */
	public void setIsCommBorr(String isCommBorr) {
		this.isCommBorr = isCommBorr == null ? null : isCommBorr.trim();
	}

    /**
     * @return IsCommBorr
     */
	public String getIsCommBorr() {
		return this.isCommBorr;
	}

	/**
	 * @param companyName
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName == null ? null : companyName.trim();
	}

    /**
     * @return CompanyName
     */
	public String getCompanyName() {
		return this.companyName;
	}

	/**
	 * @param monIncome
	 */
	public void setMonIncome(java.math.BigDecimal monIncome) {
		this.monIncome = monIncome;
	}

    /**
     * @return MonIncome
     */
	public java.math.BigDecimal getMonIncome() {
		return this.monIncome;
	}

	/**
	 * @param livingZip
	 */
	public void setLivingZip(String livingZip) {
		this.livingZip = livingZip == null ? null : livingZip.trim();
	}

    /**
     * @return LivingZip
     */
	public String getLivingZip() {
		return this.livingZip;
	}

	/**
	 * @param relWithApplyer
	 */
	public void setRelWithApplyer(String relWithApplyer) {
		this.relWithApplyer = relWithApplyer == null ? null : relWithApplyer.trim();
	}

    /**
     * @return RelWithApplyer
     */
	public String getRelWithApplyer() {
		return this.relWithApplyer;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(String createTime) {
		this.createTime = createTime == null ? null : createTime.trim();
	}

    /**
     * @return CreateTime
     */
	public String getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param createUsr
	 */
	public void setCreateUsr(String createUsr) {
		this.createUsr = createUsr == null ? null : createUsr.trim();
	}

    /**
     * @return CreateUsr
     */
	public String getCreateUsr() {
		return this.createUsr;
	}

	/**
	 * @param uPDDate
	 */
	public void setUPDDate(String uPDDate) {
		this.uPDDate = uPDDate == null ? null : uPDDate.trim();
	}

    /**
     * @return UPDDate
     */
	public String getUPDDate() {
		return this.uPDDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

    /**
     * @return UpdId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

    /**
     * @return OprType
     */
	public String getOprType() {
		return this.oprType;
	}


}
