package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditReportLoanFina
 * @类描述: credit_report_loan_fina数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-06 17:03:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CreditReportLoanFinaDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String crlfSerno;

	/** 关联业务流水号 **/
	private String bizSerno;

	/** 征信查询场景 **/
	private String scene;

	/** 是否自动审批通过 0手动审批 1自动审批 **/
	private String isAutoCheckPass;

	/** 征信查询流水号 **/
	private String crqlSerno;

	/** 证件类型 **/
	private String certType;

	/** 证件号码 **/
	private String certCode;

	/** 客户编号 **/
	private String cusId;

	/** 客户名称 **/
	private String cusName;

	/** 主借款人客户号 **/
	private String borrowerCusId;

	/** 主借款人证件号码 **/
	private String borrowerCertCode;

	/** 主借款人名称 **/
	private String borrowerCusName;

	/** 与主借款人关系 **/
	private String borrowRel;

	/** 征信查询类别 **/
	private String qryCls;

	/** 征信查询原因 **/
	private String qryResn;

	/** 查询原因描述 **/
	private String qryResnDec;

	/** 授权书编号 **/
	private String authbookNo;

	/** 授权书内容 **/
	private String authbookContent;

	/** 其他授权书内容 **/
	private String otherAuthbookExt;

	/** 授权书日期 **/
	private String authbookDate;

	/** 主管客户经理 **/
	private String managerId;

	/** 主管机构 **/
	private String managerBrId;

	/** 成功发起时间 **/
	private String sendTime;

	/** 审批状态 **/
	private String approveStatus;

	/** 是否成功发起 **/
	private String isSuccssInit;

	/** 征信报告编号 **/
	private String reportNo;

	/** 征信查询状态 **/
	private String qryStatus;

	/** 征信返回地址 **/
	private String creditUrl;

	/** 报告生成时间 **/
	private String reportCreateTime;

	/** 征信查询标识 **/
	private String qryFlag;

	/** 影像编号 **/
	private String imageNo;

	/** 授权模式 **/
	private String authWay;
	
	/** 法定代表人姓名 **/
	private String legalName;
	
	/** 法定代表人联系电话 **/
	private String legalPhone;
	
	/** 他行是否有融资 **/
	private String otherBankIsFin;
	
	/** 本行是否已有融资 **/
	private String ourBankIsFin;
	
	/** 本次融资申请金额 **/
	private java.math.BigDecimal applyFinAmt;
	
	/** 本次融资期限（月） **/
	private String applyFinTerm;
	
	/** 本次融资担保方式 **/
	private String applyFinGuarType;
	
	/** 本次融资原因 **/
	private String applyFinRemark;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;

	public String getBizSerno() {
		return bizSerno;
	}

	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getIsAutoCheckPass() {
		return isAutoCheckPass;
	}

	public void setIsAutoCheckPass(String isAutoCheckPass) {
		this.isAutoCheckPass = isAutoCheckPass;
	}

	/**
	 * @param crlfSerno
	 */
	public void setCrlfSerno(String crlfSerno) {
		this.crlfSerno = crlfSerno == null ? null : crlfSerno.trim();
	}
	
    /**
     * @return CrlfSerno
     */	
	public String getCrlfSerno() {
		return this.crlfSerno;
	}
	
	/**
	 * @param crqlSerno
	 */
	public void setCrqlSerno(String crqlSerno) {
		this.crqlSerno = crqlSerno == null ? null : crqlSerno.trim();
	}
	
    /**
     * @return CrqlSerno
     */	
	public String getCrqlSerno() {
		return this.crqlSerno;
	}
	
	/**
	 * @param legalName
	 */
	public void setLegalName(String legalName) {
		this.legalName = legalName == null ? null : legalName.trim();
	}
	
    /**
     * @return LegalName
     */	
	public String getLegalName() {
		return this.legalName;
	}
	
	/**
	 * @param legalPhone
	 */
	public void setLegalPhone(String legalPhone) {
		this.legalPhone = legalPhone == null ? null : legalPhone.trim();
	}
	
    /**
     * @return LegalPhone
     */	
	public String getLegalPhone() {
		return this.legalPhone;
	}
	
	/**
	 * @param otherBankIsFin
	 */
	public void setOtherBankIsFin(String otherBankIsFin) {
		this.otherBankIsFin = otherBankIsFin == null ? null : otherBankIsFin.trim();
	}
	
    /**
     * @return OtherBankIsFin
     */	
	public String getOtherBankIsFin() {
		return this.otherBankIsFin;
	}
	
	/**
	 * @param ourBankIsFin
	 */
	public void setOurBankIsFin(String ourBankIsFin) {
		this.ourBankIsFin = ourBankIsFin == null ? null : ourBankIsFin.trim();
	}
	
    /**
     * @return OurBankIsFin
     */	
	public String getOurBankIsFin() {
		return this.ourBankIsFin;
	}
	
	/**
	 * @param applyFinAmt
	 */
	public void setApplyFinAmt(java.math.BigDecimal applyFinAmt) {
		this.applyFinAmt = applyFinAmt;
	}
	
    /**
     * @return ApplyFinAmt
     */	
	public java.math.BigDecimal getApplyFinAmt() {
		return this.applyFinAmt;
	}
	
	/**
	 * @param applyFinTerm
	 */
	public void setApplyFinTerm(String applyFinTerm) {
		this.applyFinTerm = applyFinTerm == null ? null : applyFinTerm.trim();
	}
	
    /**
     * @return ApplyFinTerm
     */	
	public String getApplyFinTerm() {
		return this.applyFinTerm;
	}
	
	/**
	 * @param applyFinGuarType
	 */
	public void setApplyFinGuarType(String applyFinGuarType) {
		this.applyFinGuarType = applyFinGuarType == null ? null : applyFinGuarType.trim();
	}
	
    /**
     * @return ApplyFinGuarType
     */	
	public String getApplyFinGuarType() {
		return this.applyFinGuarType;
	}
	
	/**
	 * @param applyFinRemark
	 */
	public void setApplyFinRemark(String applyFinRemark) {
		this.applyFinRemark = applyFinRemark == null ? null : applyFinRemark.trim();
	}
	
    /**
     * @return ApplyFinRemark
     */	
	public String getApplyFinRemark() {
		return this.applyFinRemark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getBorrowerCusId() {
		return borrowerCusId;
	}

	public void setBorrowerCusId(String borrowerCusId) {
		this.borrowerCusId = borrowerCusId;
	}

	public String getBorrowerCertCode() {
		return borrowerCertCode;
	}

	public void setBorrowerCertCode(String borrowerCertCode) {
		this.borrowerCertCode = borrowerCertCode;
	}

	public String getBorrowerCusName() {
		return borrowerCusName;
	}

	public void setBorrowerCusName(String borrowerCusName) {
		this.borrowerCusName = borrowerCusName;
	}

	public String getBorrowRel() {
		return borrowRel;
	}

	public void setBorrowRel(String borrowRel) {
		this.borrowRel = borrowRel;
	}

	public String getQryCls() {
		return qryCls;
	}

	public void setQryCls(String qryCls) {
		this.qryCls = qryCls;
	}

	public String getQryResn() {
		return qryResn;
	}

	public void setQryResn(String qryResn) {
		this.qryResn = qryResn;
	}

	public String getQryResnDec() {
		return qryResnDec;
	}

	public void setQryResnDec(String qryResnDec) {
		this.qryResnDec = qryResnDec;
	}

	public String getAuthbookNo() {
		return authbookNo;
	}

	public void setAuthbookNo(String authbookNo) {
		this.authbookNo = authbookNo;
	}

	public String getAuthbookContent() {
		return authbookContent;
	}

	public void setAuthbookContent(String authbookContent) {
		this.authbookContent = authbookContent;
	}

	public String getOtherAuthbookExt() {
		return otherAuthbookExt;
	}

	public void setOtherAuthbookExt(String otherAuthbookExt) {
		this.otherAuthbookExt = otherAuthbookExt;
	}

	public String getAuthbookDate() {
		return authbookDate;
	}

	public void setAuthbookDate(String authbookDate) {
		this.authbookDate = authbookDate;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManagerBrId() {
		return managerBrId;
	}

	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	public String getSendTime() {
		return sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getIsSuccssInit() {
		return isSuccssInit;
	}

	public void setIsSuccssInit(String isSuccssInit) {
		this.isSuccssInit = isSuccssInit;
	}

	public String getReportNo() {
		return reportNo;
	}

	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}

	public String getQryStatus() {
		return qryStatus;
	}

	public void setQryStatus(String qryStatus) {
		this.qryStatus = qryStatus;
	}

	public String getCreditUrl() {
		return creditUrl;
	}

	public void setCreditUrl(String creditUrl) {
		this.creditUrl = creditUrl;
	}

	public String getReportCreateTime() {
		return reportCreateTime;
	}

	public void setReportCreateTime(String reportCreateTime) {
		this.reportCreateTime = reportCreateTime;
	}

	public String getQryFlag() {
		return qryFlag;
	}

	public void setQryFlag(String qryFlag) {
		this.qryFlag = qryFlag;
	}

	public String getImageNo() {
		return imageNo;
	}

	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}

	public String getAuthWay() {
		return authWay;
	}

	public void setAuthWay(String authWay) {
		this.authWay = authWay;
	}

	@Override
	public String toString() {
		return "CreditReportLoanFinaDto{" +
				"crlfSerno='" + crlfSerno + '\'' +
				", bizSerno='" + bizSerno + '\'' +
				", scene='" + scene + '\'' +
				", isAutoCheckPass='" + isAutoCheckPass + '\'' +
				", crqlSerno='" + crqlSerno + '\'' +
				", certType='" + certType + '\'' +
				", certCode='" + certCode + '\'' +
				", cusId='" + cusId + '\'' +
				", cusName='" + cusName + '\'' +
				", borrowerCusId='" + borrowerCusId + '\'' +
				", borrowerCertCode='" + borrowerCertCode + '\'' +
				", borrowerCusName='" + borrowerCusName + '\'' +
				", borrowRel='" + borrowRel + '\'' +
				", qryCls='" + qryCls + '\'' +
				", qryResn='" + qryResn + '\'' +
				", qryResnDec='" + qryResnDec + '\'' +
				", authbookNo='" + authbookNo + '\'' +
				", authbookContent='" + authbookContent + '\'' +
				", otherAuthbookExt='" + otherAuthbookExt + '\'' +
				", authbookDate='" + authbookDate + '\'' +
				", managerId='" + managerId + '\'' +
				", managerBrId='" + managerBrId + '\'' +
				", sendTime='" + sendTime + '\'' +
				", approveStatus='" + approveStatus + '\'' +
				", isSuccssInit='" + isSuccssInit + '\'' +
				", reportNo='" + reportNo + '\'' +
				", qryStatus='" + qryStatus + '\'' +
				", creditUrl='" + creditUrl + '\'' +
				", reportCreateTime='" + reportCreateTime + '\'' +
				", qryFlag='" + qryFlag + '\'' +
				", imageNo='" + imageNo + '\'' +
				", authWay='" + authWay + '\'' +
				", legalName='" + legalName + '\'' +
				", legalPhone='" + legalPhone + '\'' +
				", otherBankIsFin='" + otherBankIsFin + '\'' +
				", ourBankIsFin='" + ourBankIsFin + '\'' +
				", applyFinAmt=" + applyFinAmt +
				", applyFinTerm='" + applyFinTerm + '\'' +
				", applyFinGuarType='" + applyFinGuarType + '\'' +
				", applyFinRemark='" + applyFinRemark + '\'' +
				", inputId='" + inputId + '\'' +
				", inputBrId='" + inputBrId + '\'' +
				", inputDate='" + inputDate + '\'' +
				", updId='" + updId + '\'' +
				", updBrId='" + updBrId + '\'' +
				", updDate='" + updDate + '\'' +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				'}';
	}
}