package cn.com.yusys.yusp.web.server.xdxw0072;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxw0072.req.Xdxw0072DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0072.resp.Xdxw0072DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.server.xdxw0072.Xdxw0072Service;
import org.apache.commons.lang.StringUtils;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:根据调查表编号前往信贷查找客户经理信息
 *
 * @author xs
 * @version 1.0
 */
@Api(tags = "XDXW0072:根据调查表编号前往信贷查找客户经理信息")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0072Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0072Resource.class);

    @Autowired
    private Xdxw0072Service xdxw0072Service;
    @Autowired
    private AdminSmUserService adminSmUserService;
    /**
     * 交易码：xdxw0072
     * 交易描述：根据调查表编号前往信贷查找客户经理信息
     *
     * @param xdxw0072DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据调查表编号前往信贷查找客户经理信息")
    @PostMapping("/xdxw0072")
    protected @ResponseBody
    ResultDto<Xdxw0072DataRespDto> xdxw0072(@Validated @RequestBody Xdxw0072DataReqDto xdxw0072DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0072.key, DscmsEnum.TRADE_CODE_XDXW0072.value, JSON.toJSONString(xdxw0072DataReqDto));
        Xdxw0072DataRespDto xdxw0072DataRespDto = new Xdxw0072DataRespDto();// 响应Dto:根据调查表编号前往信贷查找客户经理信息
        ResultDto<Xdxw0072DataRespDto> xdxw0072DataResultDto = new ResultDto<>();
        String indgtSerno = xdxw0072DataReqDto.getIndgtSerno();//客户调查表编号
        try {
            // 从xdxw0072DataReqDto获取业务值进行业务逻辑处理
            String managerId = xdxw0072Service.getManagerIdBySurveyNo(indgtSerno);
            String managerName = StringUtils.EMPTY;// 客户经理姓名
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                managerName = adminSmUserDto.getUserName();
            }
            xdxw0072DataRespDto.setManagerId(managerId);// 客户经理ID
            xdxw0072DataRespDto.setManagerName(managerName);// 客户经理姓名
            // 封装xdxw0072DataResultDto中正确的返回码和返回信息
            xdxw0072DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0072DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0072.key, DscmsEnum.TRADE_CODE_XDXW0072.value, e.getMessage());
            // 封装xdxw0072DataResultDto中异常返回码和返回信息
            xdxw0072DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0072DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0072DataRespDto到xdxw0072DataResultDto中
        xdxw0072DataResultDto.setData(xdxw0072DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0072.key, DscmsEnum.TRADE_CODE_XDXW0072.value, JSON.toJSONString(xdxw0072DataResultDto));
        return xdxw0072DataResultDto;
    }
}
