package cn.com.yusys.yusp.service.server.xdxw0076;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0076.req.Xdxw0076DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0076.resp.Xdxw0076DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0076Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xuchao
 * @创建时间: 2021-06-12 14:03:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0076Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0076Service.class);

    @Resource
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;
    /**
     *  渠道端查询个人客户我的授信（授信申请流程监控）
     * @param xdxw0076DataReqDto
     * @return
     */
    public Xdxw0076DataRespDto getXdxw0076(Xdxw0076DataReqDto xdxw0076DataReqDto){
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076DataReqDto));
        Xdxw0076DataRespDto xdxw0076DataRespDto = new Xdxw0076DataRespDto();
        try {
            java.util.List<cn.com.yusys.yusp.dto.server.xdxw0076.resp.List> lists = new ArrayList<>();
            lists = lmtSurveyReportMainInfoMapper.getXdxw0076(xdxw0076DataReqDto);
            xdxw0076DataRespDto.setList(lists);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076DataRespDto));
        return xdxw0076DataRespDto;
    }

}
