/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRepayWayChg;
import cn.com.yusys.yusp.domain.ReyPlan;
import cn.com.yusys.yusp.service.ReyPlanService;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * 还款计划变更resource
 */
@RestController
@RequestMapping("/api/reyPlan")
public class ReyPlanResource {
    @Autowired
    private ReyPlanService reyPlanService;

    private static final Logger log = LoggerFactory.getLogger(ReyPlanService.class);

    /**
     * 新增页面保存还款计划变更申请数据
     * @param params
     * @return
     */

    @PostMapping("/saveReyPlanInfo")
    protected ResultDto<Integer> insertIqpRepayWayChgByBillNo(@RequestBody ReyPlan reyPlan) throws  URISyntaxException{
        log.info("新增还款方式申请表数据【{}】", JSONObject.toJSON(reyPlan));
        int result = reyPlanService.saveReyPlan(reyPlan);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: checkIsExistWFByBillNo
     * @函数描述: 根据借据编号查询是否存在在途的变更业务
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkIsExistWFBizByBillNo")
    protected ResultDto<Integer> checkIsExistWFByBillNo(@RequestBody ReyPlan reyPlan) throws  URISyntaxException{
        log.info("新增还款方式申请表数据【{}】", JSONObject.toJSON(reyPlan));
        int result = reyPlanService.checkIsExistWFByBillNo(reyPlan);
        return new ResultDto<Integer>(result);
    }


}
