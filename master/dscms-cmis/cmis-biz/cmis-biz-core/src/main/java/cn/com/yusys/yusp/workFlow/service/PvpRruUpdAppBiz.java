package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.PvpRruUpdAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 退汇申请流程审批业务处理
 */
public class PvpRruUpdAppBiz implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(PvpRruUpdAppBiz.class);

    @Autowired
    private AmqpTemplate amqpTemplate;



    @Autowired
    private PvpRruUpdAppService pvpRruUpdAppService;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String rruSerno = instanceInfo.getBizId();
        log.info("后业务处理类型:" + currentOpType);
        //todo  判定下一处理节点  是否为第一个节点（发起节点）,Y-> 打回,退回,拿回,追回,才会更改对应的状态（992,991）
        // N-> 不做任何处理,一直为审批中的状态
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
                pvpRruUpdAppService.handleBusinessDataAfterStart(rruSerno);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----"+instanceInfo);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                pvpRruUpdAppService.handleBusinessDataAfterEnd(rruSerno);
                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                pvpRruUpdAppService.handleBusinessDataAfteCallBack(rruSerno);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                //项目全部使用拿回,状态改为追回 991
                log.info("拿回操作:" + instanceInfo);
                pvpRruUpdAppService.handleBusinessDataAfteTackBack(rruSerno);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                pvpRruUpdAppService.handleBusinessDataAfterRefuse(rruSerno);
                log.info("否决操作结束:" + instanceInfo);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                WFException exception = new WFException();
                exception.setBizId(instanceInfo.getBizId());
                exception.setBizType(instanceInfo.getBizType());
                exception.setFlowName(instanceInfo.getFlowName());
                exception.setInstanceId(instanceInfo.getInstanceId());
                exception.setNodeId(instanceInfo.getNodeId());
                exception.setNodeName(instanceInfo.getNodeName());
                exception.setUserId(instanceInfo.getCurrentUserId());
                exception.setOpType(currentOpType);
                // 后业务处理失败时，将异常信息保存到异常表中
                amqpTemplate.convertAndSend(ClientCons.queue_exception, exception);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }
    // 判定流程能否进行业务处理
    @Override
    public boolean should(ResultInstanceDto instanceInfo) {
        String flowCode = instanceInfo.getFlowCode();
        return CmisFlowConstants.PVP_RRU_UPD_APP.equals(flowCode);
    }
}
