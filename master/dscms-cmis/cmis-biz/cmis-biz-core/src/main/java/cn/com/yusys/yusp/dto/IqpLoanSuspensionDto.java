package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanSuspension
 * @类描述: iqp_loan_suspension数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-31 14:19:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpLoanSuspensionDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 停息流水号 **/
	private String ilsSerno;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;
	
	/** 贷款余额 **/
	private java.math.BigDecimal loanBalance;
	
	/** 贷款起始日 **/
	private String loanStartDate;
	
	/** 贷款到期日 **/
	private String loanEndDate;
	
	/** 申请渠道 **/
	private String appChnl;
	
	/** 台账状态 **/
	private String accStatus;
	
	/** 停止操作码 **/
	private String susOpt;
	
	/** 是否补计提停息阶段利息 **/
	private String isCalculateInt;
	
	/** 停息生效日期 **/
	private String susInputDate;
	
	/** 停息原因 **/
	private String susReason;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 记账状态 **/
	private String recordStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param ilsSerno
	 */
	public void setIlsSerno(String ilsSerno) {
		this.ilsSerno = ilsSerno == null ? null : ilsSerno.trim();
	}
	
    /**
     * @return IlsSerno
     */	
	public String getIlsSerno() {
		return this.ilsSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}
	
    /**
     * @return LoanAmt
     */	
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(java.math.BigDecimal loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return LoanBalance
     */	
	public java.math.BigDecimal getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate == null ? null : loanEndDate.trim();
	}
	
    /**
     * @return LoanEndDate
     */	
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param appChnl
	 */
	public void setAppChnl(String appChnl) {
		this.appChnl = appChnl == null ? null : appChnl.trim();
	}
	
    /**
     * @return AppChnl
     */	
	public String getAppChnl() {
		return this.appChnl;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus == null ? null : accStatus.trim();
	}
	
    /**
     * @return AccStatus
     */	
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param susOpt
	 */
	public void setSusOpt(String susOpt) {
		this.susOpt = susOpt == null ? null : susOpt.trim();
	}
	
    /**
     * @return SusOpt
     */	
	public String getSusOpt() {
		return this.susOpt;
	}
	
	/**
	 * @param isCalculateInt
	 */
	public void setIsCalculateInt(String isCalculateInt) {
		this.isCalculateInt = isCalculateInt == null ? null : isCalculateInt.trim();
	}
	
    /**
     * @return IsCalculateInt
     */	
	public String getIsCalculateInt() {
		return this.isCalculateInt;
	}
	
	/**
	 * @param susInputDate
	 */
	public void setSusInputDate(String susInputDate) {
		this.susInputDate = susInputDate == null ? null : susInputDate.trim();
	}
	
    /**
     * @return SusInputDate
     */	
	public String getSusInputDate() {
		return this.susInputDate;
	}
	
	/**
	 * @param susReason
	 */
	public void setSusReason(String susReason) {
		this.susReason = susReason == null ? null : susReason.trim();
	}
	
    /**
     * @return SusReason
     */	
	public String getSusReason() {
		return this.susReason;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param recordStatus
	 */
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus == null ? null : recordStatus.trim();
	}
	
    /**
     * @return RecordStatus
     */	
	public String getRecordStatus() {
		return this.recordStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}