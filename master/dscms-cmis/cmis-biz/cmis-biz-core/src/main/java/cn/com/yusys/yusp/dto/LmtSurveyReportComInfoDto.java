package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportComInfo
 * @类描述: lmt_survey_report_com_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-05-10 22:39:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSurveyReportComInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 企业名称 **/
	private String conName;
	
	/** 法人代表 **/
	private String legal;
	
	/** 经营地址 **/
	private String operAddr;
	
	/** 经营期限 **/
	private String operTerm;
	
	/** 主营业务 **/
	private String mainBusi;
	
	/** 企业类型 **/
	private String corpType;
	
	/** 营业执照年限 **/
	private String blicYears;
	
	/** 行业 **/
	private String trade;
	
	/** 企业证件类型 **/
	private String corpCertType;
	
	/** 企业证件号码 **/
	private String corpCertCode;
	
	/** 实际经营地址 **/
	private String actMngAddr;
	
	/** 实际行业 **/
	private String actOperTrade;
	
	/** 是否实际经营人 **/
	private String isActOperPerson;
	
	/** 经营是否正常 **/
	private String isOperNormal;
	
	/** 司法审核结果 **/
	private String judicialAuditResult;
	
	/** 企业完整纳税月份数 **/
	private String corpFullTaxMonths;
	
	/** 企业当前税务信用评级 **/
	private String corpCurtTxtCdtEval;
	
	/** 企业近1年综合应纳税额 **/
	private java.math.BigDecimal corpLt1yearInteTax;
	
	/** 企业近1年税前利润率 **/
	private java.math.BigDecimal corpLt1yearPretaxProfit;
	
	/** 企业近1年销售收入 **/
	private java.math.BigDecimal corpLt1yearSaleIncome;
	
	/** 税务模型评级结果 **/
	private String taxModelEvalResult;
	
	/** 税务模型评分 **/
	private String taxModelGrade;
	
	/** 负债收入比 **/
	private java.math.BigDecimal debtEarningPerc;
	
	/** 企业描述 **/
	private String corpDesc;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param conName
	 */
	public void setConName(String conName) {
		this.conName = conName == null ? null : conName.trim();
	}
	
    /**
     * @return ConName
     */	
	public String getConName() {
		return this.conName;
	}
	
	/**
	 * @param legal
	 */
	public void setLegal(String legal) {
		this.legal = legal == null ? null : legal.trim();
	}
	
    /**
     * @return Legal
     */	
	public String getLegal() {
		return this.legal;
	}
	
	/**
	 * @param operAddr
	 */
	public void setOperAddr(String operAddr) {
		this.operAddr = operAddr == null ? null : operAddr.trim();
	}
	
    /**
     * @return OperAddr
     */	
	public String getOperAddr() {
		return this.operAddr;
	}
	
	/**
	 * @param operTerm
	 */
	public void setOperTerm(String operTerm) {
		this.operTerm = operTerm == null ? null : operTerm.trim();
	}
	
    /**
     * @return OperTerm
     */	
	public String getOperTerm() {
		return this.operTerm;
	}
	
	/**
	 * @param mainBusi
	 */
	public void setMainBusi(String mainBusi) {
		this.mainBusi = mainBusi == null ? null : mainBusi.trim();
	}
	
    /**
     * @return MainBusi
     */	
	public String getMainBusi() {
		return this.mainBusi;
	}
	
	/**
	 * @param corpType
	 */
	public void setCorpType(String corpType) {
		this.corpType = corpType == null ? null : corpType.trim();
	}
	
    /**
     * @return CorpType
     */	
	public String getCorpType() {
		return this.corpType;
	}
	
	/**
	 * @param blicYears
	 */
	public void setBlicYears(String blicYears) {
		this.blicYears = blicYears == null ? null : blicYears.trim();
	}
	
    /**
     * @return BlicYears
     */	
	public String getBlicYears() {
		return this.blicYears;
	}
	
	/**
	 * @param trade
	 */
	public void setTrade(String trade) {
		this.trade = trade == null ? null : trade.trim();
	}
	
    /**
     * @return Trade
     */	
	public String getTrade() {
		return this.trade;
	}
	
	/**
	 * @param corpCertType
	 */
	public void setCorpCertType(String corpCertType) {
		this.corpCertType = corpCertType == null ? null : corpCertType.trim();
	}
	
    /**
     * @return CorpCertType
     */	
	public String getCorpCertType() {
		return this.corpCertType;
	}
	
	/**
	 * @param corpCertCode
	 */
	public void setCorpCertCode(String corpCertCode) {
		this.corpCertCode = corpCertCode == null ? null : corpCertCode.trim();
	}
	
    /**
     * @return CorpCertCode
     */	
	public String getCorpCertCode() {
		return this.corpCertCode;
	}
	
	/**
	 * @param actMngAddr
	 */
	public void setActMngAddr(String actMngAddr) {
		this.actMngAddr = actMngAddr == null ? null : actMngAddr.trim();
	}
	
    /**
     * @return ActMngAddr
     */	
	public String getActMngAddr() {
		return this.actMngAddr;
	}
	
	/**
	 * @param actOperTrade
	 */
	public void setActOperTrade(String actOperTrade) {
		this.actOperTrade = actOperTrade == null ? null : actOperTrade.trim();
	}
	
    /**
     * @return ActOperTrade
     */	
	public String getActOperTrade() {
		return this.actOperTrade;
	}
	
	/**
	 * @param isActOperPerson
	 */
	public void setIsActOperPerson(String isActOperPerson) {
		this.isActOperPerson = isActOperPerson == null ? null : isActOperPerson.trim();
	}
	
    /**
     * @return IsActOperPerson
     */	
	public String getIsActOperPerson() {
		return this.isActOperPerson;
	}
	
	/**
	 * @param isOperNormal
	 */
	public void setIsOperNormal(String isOperNormal) {
		this.isOperNormal = isOperNormal == null ? null : isOperNormal.trim();
	}
	
    /**
     * @return IsOperNormal
     */	
	public String getIsOperNormal() {
		return this.isOperNormal;
	}
	
	/**
	 * @param judicialAuditResult
	 */
	public void setJudicialAuditResult(String judicialAuditResult) {
		this.judicialAuditResult = judicialAuditResult == null ? null : judicialAuditResult.trim();
	}
	
    /**
     * @return JudicialAuditResult
     */	
	public String getJudicialAuditResult() {
		return this.judicialAuditResult;
	}
	
	/**
	 * @param corpFullTaxMonths
	 */
	public void setCorpFullTaxMonths(String corpFullTaxMonths) {
		this.corpFullTaxMonths = corpFullTaxMonths == null ? null : corpFullTaxMonths.trim();
	}
	
    /**
     * @return CorpFullTaxMonths
     */	
	public String getCorpFullTaxMonths() {
		return this.corpFullTaxMonths;
	}
	
	/**
	 * @param corpCurtTxtCdtEval
	 */
	public void setCorpCurtTxtCdtEval(String corpCurtTxtCdtEval) {
		this.corpCurtTxtCdtEval = corpCurtTxtCdtEval == null ? null : corpCurtTxtCdtEval.trim();
	}
	
    /**
     * @return CorpCurtTxtCdtEval
     */	
	public String getCorpCurtTxtCdtEval() {
		return this.corpCurtTxtCdtEval;
	}
	
	/**
	 * @param corpLt1yearInteTax
	 */
	public void setCorpLt1yearInteTax(java.math.BigDecimal corpLt1yearInteTax) {
		this.corpLt1yearInteTax = corpLt1yearInteTax;
	}
	
    /**
     * @return CorpLt1yearInteTax
     */	
	public java.math.BigDecimal getCorpLt1yearInteTax() {
		return this.corpLt1yearInteTax;
	}
	
	/**
	 * @param corpLt1yearPretaxProfit
	 */
	public void setCorpLt1yearPretaxProfit(java.math.BigDecimal corpLt1yearPretaxProfit) {
		this.corpLt1yearPretaxProfit = corpLt1yearPretaxProfit;
	}
	
    /**
     * @return CorpLt1yearPretaxProfit
     */	
	public java.math.BigDecimal getCorpLt1yearPretaxProfit() {
		return this.corpLt1yearPretaxProfit;
	}
	
	/**
	 * @param corpLt1yearSaleIncome
	 */
	public void setCorpLt1yearSaleIncome(java.math.BigDecimal corpLt1yearSaleIncome) {
		this.corpLt1yearSaleIncome = corpLt1yearSaleIncome;
	}
	
    /**
     * @return CorpLt1yearSaleIncome
     */	
	public java.math.BigDecimal getCorpLt1yearSaleIncome() {
		return this.corpLt1yearSaleIncome;
	}
	
	/**
	 * @param taxModelEvalResult
	 */
	public void setTaxModelEvalResult(String taxModelEvalResult) {
		this.taxModelEvalResult = taxModelEvalResult == null ? null : taxModelEvalResult.trim();
	}
	
    /**
     * @return TaxModelEvalResult
     */	
	public String getTaxModelEvalResult() {
		return this.taxModelEvalResult;
	}
	
	/**
	 * @param taxModelGrade
	 */
	public void setTaxModelGrade(String taxModelGrade) {
		this.taxModelGrade = taxModelGrade == null ? null : taxModelGrade.trim();
	}
	
    /**
     * @return TaxModelGrade
     */	
	public String getTaxModelGrade() {
		return this.taxModelGrade;
	}
	
	/**
	 * @param debtEarningPerc
	 */
	public void setDebtEarningPerc(java.math.BigDecimal debtEarningPerc) {
		this.debtEarningPerc = debtEarningPerc;
	}
	
    /**
     * @return DebtEarningPerc
     */	
	public java.math.BigDecimal getDebtEarningPerc() {
		return this.debtEarningPerc;
	}
	
	/**
	 * @param corpDesc
	 */
	public void setCorpDesc(String corpDesc) {
		this.corpDesc = corpDesc == null ? null : corpDesc.trim();
	}
	
    /**
     * @return CorpDesc
     */	
	public String getCorpDesc() {
		return this.corpDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}