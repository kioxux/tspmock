/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.domain.LmtGrpMemRel;
import cn.com.yusys.yusp.domain.LmtGrpSubPrdAdjustRel;
import cn.com.yusys.yusp.dto.LmtGrpSubPrdAdjustCreateDto;
import cn.com.yusys.yusp.dto.LmtGrpSubPrdAdjustRelAllListDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtGrpSubPrdAdjustRelMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpSubPrdAdjustRelService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:25:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtGrpSubPrdAdjustRelService {

    @Autowired
    private LmtGrpSubPrdAdjustRelMapper lmtGrpSubPrdAdjustRelMapper;

    @Autowired
    private LmtGrpMemRelService lmtGrpMemRelService;

    private static final Logger log = LoggerFactory.getLogger(LmtAppService.class);

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtGrpSubPrdAdjustRel selectByPrimaryKey(String pkId) {
        return lmtGrpSubPrdAdjustRelMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtGrpSubPrdAdjustRel> selectAll(QueryModel model) {
        List<LmtGrpSubPrdAdjustRel> records = (List<LmtGrpSubPrdAdjustRel>) lmtGrpSubPrdAdjustRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtGrpSubPrdAdjustRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtGrpSubPrdAdjustRel> list = lmtGrpSubPrdAdjustRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional
    public int insert(LmtGrpSubPrdAdjustRel record) {
        return lmtGrpSubPrdAdjustRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtGrpSubPrdAdjustRel record) {
        return lmtGrpSubPrdAdjustRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtGrpSubPrdAdjustRel record) {
        return lmtGrpSubPrdAdjustRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtGrpSubPrdAdjustRel record) {
        return lmtGrpSubPrdAdjustRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtGrpSubPrdAdjustRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtGrpSubPrdAdjustRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryLmtGrpSubPrdAdjustRelByParams
     * @方法描述: 根据查询参数查询集团成员调剂关系表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpSubPrdAdjustRel> queryLmtGrpSubPrdAdjustRelByParams(HashMap<String, String> params) {
        return lmtGrpSubPrdAdjustRelMapper.queryLmtGrpSubPrdAdjustRelByParams(params);
    }

    /**
     * @方法名称: createLmtGrpSubPrdAdjustRelListInfo
     * @方法描述: 根据传入的cusIds的客户号调用创建额度调剂数据, cusIds为cusId用逗号拼接字符串
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Integer createLmtGrpSubPrdAdjustRelListInfo(LmtGrpSubPrdAdjustCreateDto lmtGrpSubPrdAdjustCreateDto) {
        Integer count = 0;
        try {
            String cusIds = lmtGrpSubPrdAdjustCreateDto.getCusIds();
            String grpSerno = lmtGrpSubPrdAdjustCreateDto.getGrpSerno();
            if (StringUtils.isBlank(cusIds)) {
                BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            log.info(String.format("根据集团客户号%s,查询客户已有的调剂关系", cusIds));
            HashMap paramMap = new HashMap<String, Object>();
            paramMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            paramMap.put("cusIds", cusIds);
            List<LmtGrpSubPrdAdjustRel> lmtGrpSubPrdAdjustRelList = this.queryLmtGrpSubPrdAdjustRelByParams(paramMap);
            String[] cusList = cusIds.split(CmisCommonConstants.COMMON_SPLIT_COMMA);
            for (String cusId: cusList) {
                Boolean isFind = false;
                for (LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel : lmtGrpSubPrdAdjustRelList) {
                    if(cusId.equals(lmtGrpSubPrdAdjustRel.getCusId())){
                        isFind = true;
                        break;
                    }
                }
                // 新增前先判断当前客户是否已经存在 额度调剂 存在 测跳出当前客户的新增调剂关系
                HashMap map = new HashMap();
                map.put("cusId",cusId);
                map.put("grpSerno",grpSerno);
                map.put("oprType",CmisCommonConstants.OPR_TYPE_ADD);
                if(queryLmtGrpSubPrdAdjustRelByParams(map).size() > 0){
                    continue;
                }
                if(!isFind){
                    LmtGrpMemRel lmtGrpMemRel = lmtGrpMemRelService.queryLmtGrpMemRelByGrpSernoAndCusId(grpSerno, cusId);
                    LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel = new LmtGrpSubPrdAdjustRel();
                    lmtGrpSubPrdAdjustRel.setPkId(UUID.randomUUID().toString());
                    lmtGrpSubPrdAdjustRel.setGrpSerno(grpSerno);
                    lmtGrpSubPrdAdjustRel.setCusId(cusId);
                    lmtGrpSubPrdAdjustRel.setCusName(lmtGrpMemRel.getCusName());
                    lmtGrpSubPrdAdjustRel.setOpenLmtAmt(lmtGrpMemRel.getOpenLmtAmt());
                    lmtGrpSubPrdAdjustRel.setLowRiskLmtAmt(lmtGrpMemRel.getLowRiskLmtAmt());
                    lmtGrpSubPrdAdjustRel.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    lmtGrpSubPrdAdjustRel.setInputId(SessionUtils.getLoginCode());
                    lmtGrpSubPrdAdjustRel.setInputId(SessionUtils.getUserOrganizationId());
                    lmtGrpSubPrdAdjustRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtGrpSubPrdAdjustRel.setUpdId(SessionUtils.getLoginCode());
                    lmtGrpSubPrdAdjustRel.setUpdBrId(SessionUtils.getUserOrganizationId());
                    lmtGrpSubPrdAdjustRel.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
                    lmtGrpSubPrdAdjustRel.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    lmtGrpSubPrdAdjustRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
                    count = this.insert(lmtGrpSubPrdAdjustRel);
                }
            }

        } catch (YuspException e) {
            BizException.error(null, e.getCode(), e.getMsg());
        } catch (Exception e) {
            log.error("根据集团客户号查询客户是否存在在途的授信申请！", e);
            BizException.error(null, EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + "," + e.getMessage());
        }
        return count;
    }

    /**
     * @方法名称: queryLmtGrpSubPrdAdjustRelByGrpSerno
     * @方法描述: 根据集团授信申请流水号查询调剂数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<LmtGrpSubPrdAdjustRel> queryLmtGrpSubPrdAdjustRelByGrpSerno(String grpSerno) {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("grpSerno", grpSerno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        return this.queryLmtGrpSubPrdAdjustRelByParams(queryMap);
    }

    /**
     * @方法名称: updateLmtGrpSubPrdAdjustRelData
     * @方法描述: 更新单条数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Integer updateLmtGrpSubPrdAdjustRelData(LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel) {
        LmtGrpSubPrdAdjustRel updateGrpSubPrdAdjustRel = this.selectByPrimaryKey(lmtGrpSubPrdAdjustRel.getPkId());
        updateGrpSubPrdAdjustRel.setUpdId(SessionUtils.getLoginCode());
        updateGrpSubPrdAdjustRel.setUpdBrId(SessionUtils.getUserOrganizationId());
        updateGrpSubPrdAdjustRel.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        updateGrpSubPrdAdjustRel.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        updateGrpSubPrdAdjustRel.setAdjustLowRiskLmtAmt(lmtGrpSubPrdAdjustRel.getAdjustLowRiskLmtAmt());
        updateGrpSubPrdAdjustRel.setAdjustOpenLmtAmt(lmtGrpSubPrdAdjustRel.getAdjustOpenLmtAmt());
        return this.update(lmtGrpSubPrdAdjustRel);
    }

    /**
     * @方法名称: updateLmtGrpSubPrdAdjustRelData
     * @方法描述: 校验调整前后的金额是否一致
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Boolean verifyAmtSumEqualBefore(String grpSerno) {
        HashMap<String, String> queryMap = new HashMap<String, String>();
        queryMap.put("grpSerno",grpSerno);
        queryMap.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<LmtGrpSubPrdAdjustRel> lmtGrpSubPrdAdjustRelList = lmtGrpSubPrdAdjustRelMapper.queryLmtGrpSubPrdAdjustRelByParams(queryMap);
        BigDecimal openLmtAmt = new BigDecimal("0.0");
        BigDecimal adjustOpenLmtAmt = new BigDecimal("0.0");
        BigDecimal lowRiskLmtAmt = new BigDecimal("0.0");
        BigDecimal adjustLowRiskLmtAmt = new BigDecimal("0.0");
        for (LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel:lmtGrpSubPrdAdjustRelList) {
            openLmtAmt = openLmtAmt.add(lmtGrpSubPrdAdjustRel.getOpenLmtAmt() == null ? BigDecimal.ZERO : lmtGrpSubPrdAdjustRel.getOpenLmtAmt());
            adjustOpenLmtAmt = adjustOpenLmtAmt.add(lmtGrpSubPrdAdjustRel.getAdjustOpenLmtAmt() == null ? BigDecimal.ZERO : lmtGrpSubPrdAdjustRel.getAdjustOpenLmtAmt());
            lowRiskLmtAmt = lowRiskLmtAmt.add(lmtGrpSubPrdAdjustRel.getLowRiskLmtAmt() == null ? BigDecimal.ZERO : lmtGrpSubPrdAdjustRel.getLowRiskLmtAmt());
            adjustLowRiskLmtAmt = adjustLowRiskLmtAmt.add(lmtGrpSubPrdAdjustRel.getAdjustLowRiskLmtAmt() == null ? BigDecimal.ZERO : lmtGrpSubPrdAdjustRel.getAdjustLowRiskLmtAmt());
        }
        return (openLmtAmt.compareTo(adjustOpenLmtAmt) == 0 && lowRiskLmtAmt.compareTo(adjustLowRiskLmtAmt) == 0);
    }



    /**
     * @方法名称: updateAllListData
     * @方法描述: 更新列表数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-06-08 13:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public Integer updateAllListData(List<LmtGrpSubPrdAdjustRel> lmtGrpSubPrdAdjustRelList) {
        int num = 0;
        try {
            // 循环保存数据
            for(LmtGrpSubPrdAdjustRel lmtGrpSubPrdAdjustRel : lmtGrpSubPrdAdjustRelList){
                updateSelective(lmtGrpSubPrdAdjustRel);
                num++;
            }
        }catch(Exception e){
            log.error("批量更新额度调剂数据失败！", e);
            throw BizException.error(null, EpbEnum.EPB090002.key, EpbEnum.EPB090002.value + "," + e.getMessage());
        }

        return num;
    }
    
}
