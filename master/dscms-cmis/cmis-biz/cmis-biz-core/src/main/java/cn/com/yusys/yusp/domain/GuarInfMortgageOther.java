/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfMortgageOther
 * @类描述: guar_inf_mortgage_other数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-17 15:17:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_inf_mortgage_other")
public class GuarInfMortgageOther extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 押品统一编号 **/
	@Column(name = "GUAR_NO", unique = false, nullable = true, length = 40)
	private String guarNo;
	
	/** 用途 **/
	@Column(name = "USR", unique = false, nullable = true, length = 750)
	private String usr;
	
	/** 数量 **/
	@Column(name = "QNT", unique = false, nullable = true, length = 10)
	private String qnt;
	
	/** 购入日期 **/
	@Column(name = "PUR_DATE", unique = false, nullable = true, length = 10)
	private String purDate;
	
	/** 持有到期日 **/
	@Column(name = "HOLD_END_DATE", unique = false, nullable = true, length = 10)
	private String holdEndDate;
	
	/** 抵押质物说明 **/
	@Column(name = "PLDIMN_MEMO", unique = false, nullable = true, length = 750)
	private String pldimnMemo;
	
	/** 备注 **/
	@Column(name = "MEMO", unique = false, nullable = true, length = 100)
	private String memo;
	
	/** 押品取得方式  STD_ZB_GUAR_GET_TYPE **/
	@Column(name = "GUAR_GET_TYPE", unique = false, nullable = true, length = 100)
	private String guarGetType;
	
	/** 计量单位 STD_GB_GUAR_UNIT **/
	@Column(name = "GUAR_UNIT", unique = false, nullable = true, length = 4)
	private String guarUnit;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 19)
	private java.util.Date updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo;
	}
	
    /**
     * @return guarNo
     */
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param usr
	 */
	public void setUsr(String usr) {
		this.usr = usr;
	}
	
    /**
     * @return usr
     */
	public String getUsr() {
		return this.usr;
	}
	
	/**
	 * @param qnt
	 */
	public void setQnt(String qnt) {
		this.qnt = qnt;
	}
	
    /**
     * @return qnt
     */
	public String getQnt() {
		return this.qnt;
	}
	
	/**
	 * @param purDate
	 */
	public void setPurDate(String purDate) {
		this.purDate = purDate;
	}
	
    /**
     * @return purDate
     */
	public String getPurDate() {
		return this.purDate;
	}
	
	/**
	 * @param holdEndDate
	 */
	public void setHoldEndDate(String holdEndDate) {
		this.holdEndDate = holdEndDate;
	}
	
    /**
     * @return holdEndDate
     */
	public String getHoldEndDate() {
		return this.holdEndDate;
	}
	
	/**
	 * @param pldimnMemo
	 */
	public void setPldimnMemo(String pldimnMemo) {
		this.pldimnMemo = pldimnMemo;
	}
	
    /**
     * @return pldimnMemo
     */
	public String getPldimnMemo() {
		return this.pldimnMemo;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
    /**
     * @return memo
     */
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param guarGetType
	 */
	public void setGuarGetType(String guarGetType) {
		this.guarGetType = guarGetType;
	}
	
    /**
     * @return guarGetType
     */
	public String getGuarGetType() {
		return this.guarGetType;
	}
	
	/**
	 * @param guarUnit
	 */
	public void setGuarUnit(String guarUnit) {
		this.guarUnit = guarUnit;
	}
	
    /**
     * @return guarUnit
     */
	public String getGuarUnit() {
		return this.guarUnit;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}


}