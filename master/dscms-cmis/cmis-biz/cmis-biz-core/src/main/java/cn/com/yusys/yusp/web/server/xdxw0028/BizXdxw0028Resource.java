package cn.com.yusys.yusp.web.server.xdxw0028;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0028.req.Xdxw0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0028.resp.Xdxw0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0028.Xdxw0028Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:根据客户号查询我行现有融资情况
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0028:根据客户号查询我行现有融资情况")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0028Resource.class);

    @Autowired
    private Xdxw0028Service xdxw0028Service;

    /**
     * 交易码：xdxw0028
     * 交易描述：根据客户号查询我行现有融资情况
     *
     * @param xdxw0028DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询我行现有融资情况")
    @PostMapping("/xdxw0028")
    protected @ResponseBody
    ResultDto<Xdxw0028DataRespDto> xdxw0028(@Validated @RequestBody Xdxw0028DataReqDto xdxw0028DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, JSON.toJSONString(xdxw0028DataReqDto));
        Xdxw0028DataRespDto xdxw0028DataRespDto = new Xdxw0028DataRespDto();// 响应Dto:根据客户号查询我行现有融资情况
        ResultDto<Xdxw0028DataRespDto> xdxw0028DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0028DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, JSON.toJSONString(xdxw0028DataReqDto));
            xdxw0028DataRespDto = xdxw0028Service.xdxw0028(xdxw0028DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, JSON.toJSONString(xdxw0028DataRespDto));
            // 封装xdxw0028DataResultDto中正确的返回码和返回信息
            xdxw0028DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0028DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, e.getMessage());
            // 封装xdxw0028DataResultDto中异常返回码和返回信息
            xdxw0028DataResultDto.setCode(e.getErrorCode());
            xdxw0028DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, e.getMessage());
            // 封装xdxw0028DataResultDto中异常返回码和返回信息
            xdxw0028DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0028DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0028DataRespDto到xdxw0028DataResultDto中
        xdxw0028DataResultDto.setData(xdxw0028DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, JSON.toJSONString(xdxw0028DataResultDto));
        return xdxw0028DataResultDto;
    }
}
