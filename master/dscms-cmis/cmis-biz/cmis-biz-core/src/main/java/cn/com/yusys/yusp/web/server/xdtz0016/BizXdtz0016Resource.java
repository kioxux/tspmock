package cn.com.yusys.yusp.web.server.xdtz0016;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0016.req.Xdtz0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0016.resp.Xdtz0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0004.Xdtz0004Service;
import cn.com.yusys.yusp.service.server.xdtz0016.Xdtz0016Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:通知信贷系统更新贴现台账状态
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0016:通知信贷系统更新贴现台账状态")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0016Resource.class);

    @Autowired
    private Xdtz0016Service xdtz0016Service;

    /**
     * 交易码：xdtz0016
     * 交易描述：通知信贷系统更新贴现台账状态
     *
     * @param xdtz0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("通知信贷系统更新贴现台账状态")
    @PostMapping("/xdtz0016")
    protected @ResponseBody
    ResultDto<Xdtz0016DataRespDto> xdtz0016(@Validated @RequestBody Xdtz0016DataReqDto xdtz0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016DataReqDto));
        Xdtz0016DataRespDto xdtz0016DataRespDto = new Xdtz0016DataRespDto();// 响应Dto:通知信贷系统更新贴现台账状态
        ResultDto<Xdtz0016DataRespDto> xdtz0016DataResultDto = new ResultDto<>();
        String drftNo = xdtz0016DataReqDto.getDrftNo();//票号
        String drftStatus = xdtz0016DataReqDto.getDrftStatus();//票据状态
        try {
            // 从xdtz0016DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016DataReqDto));
            xdtz0016DataRespDto = xdtz0016Service.xdtz0016(xdtz0016DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016DataRespDto));
            // 封装xdtz0016DataResultDto中正确的返回码和返回信息
            xdtz0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, e.getMessage());
            // 封装xdtz0016DataResultDto中异常返回码和返回信息
            xdtz0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0016DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0016DataRespDto到xdtz0016DataResultDto中
        xdtz0016DataResultDto.setData(xdtz0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016DataResultDto));
        return xdtz0016DataResultDto;
    }
}
