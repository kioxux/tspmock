/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpSehandEsInfo
 * @类描述: pvp_sehand_es_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-09-01 22:15:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_sehand_es_info")
public class PvpSehandEsInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 放款流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PVP_SERNO")
	private String pvpSerno;
	
	/** 托管协议编号 **/
	@Column(name = "TGXY_NO", unique = false, nullable = true, length = 100)
	private String tgxyNo;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 买方身份证号码 **/
	@Column(name = "BUYER_CERT_CODE", unique = false, nullable = true, length = 20)
	private String buyerCertCode;
	
	/** 买方姓名 **/
	@Column(name = "BUYER_NAME", unique = false, nullable = true, length = 80)
	private String buyerName;
	
	/** 贷款金额 **/
	@Column(name = "APPLY_AMOUNT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal applyAmount;
	
	/** 资金性质 **/
	@Column(name = "CAPITAL_TYPE", unique = false, nullable = true, length = 5)
	private String capitalType;
	
	/** 核心交易流水 **/
	@Column(name = "CORE_TRAN_SERNO", unique = false, nullable = true, length = 40)
	private String coreTranSerno;
	
	/** 发送状态 **/
	@Column(name = "SEND_STATUS", unique = false, nullable = true, length = 5)
	private String sendStatus;
	
	/** 监管账号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 100)
	private String acctNo;

	/** 监管账号名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 255)
	private String acctName;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 100)
	private String contNo;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 100)
	private String remark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param tgxyNo
	 */
	public void setTgxyNo(String tgxyNo) {
		this.tgxyNo = tgxyNo;
	}
	
    /**
     * @return tgxyNo
     */
	public String getTgxyNo() {
		return this.tgxyNo;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param buyerCertCode
	 */
	public void setBuyerCertCode(String buyerCertCode) {
		this.buyerCertCode = buyerCertCode;
	}
	
    /**
     * @return buyerCertCode
     */
	public String getBuyerCertCode() {
		return this.buyerCertCode;
	}
	
	/**
	 * @param buyerName
	 */
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	
    /**
     * @return buyerName
     */
	public String getBuyerName() {
		return this.buyerName;
	}
	
	/**
	 * @param applyAmount
	 */
	public void setApplyAmount(java.math.BigDecimal applyAmount) {
		this.applyAmount = applyAmount;
	}
	
    /**
     * @return applyAmount
     */
	public java.math.BigDecimal getApplyAmount() {
		return this.applyAmount;
	}
	
	/**
	 * @param capitalType
	 */
	public void setCapitalType(String capitalType) {
		this.capitalType = capitalType;
	}
	
    /**
     * @return capitalType
     */
	public String getCapitalType() {
		return this.capitalType;
	}
	
	/**
	 * @param coreTranSerno
	 */
	public void setCoreTranSerno(String coreTranSerno) {
		this.coreTranSerno = coreTranSerno;
	}
	
    /**
     * @return coreTranSerno
     */
	public String getCoreTranSerno() {
		return this.coreTranSerno;
	}
	
	/**
	 * @param sendStatus
	 */
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}
	
    /**
     * @return sendStatus
     */
	public String getSendStatus() {
		return this.sendStatus;
	}
	
	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	
    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}


}