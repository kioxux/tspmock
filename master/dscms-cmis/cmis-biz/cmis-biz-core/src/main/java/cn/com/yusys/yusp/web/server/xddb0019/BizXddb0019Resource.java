package cn.com.yusys.yusp.web.server.xddb0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xddb0019.Xddb0019Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xddb0019.req.Xddb0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0019.resp.Xddb0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:解质押押品校验
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDB0019:解质押押品校验")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0019Resource.class);

    @Autowired
    private Xddb0019Service xddb0019Service;

    /**
     * 交易码：xddb0019
     * 交易描述：解质押押品校验
     *
     * @param xddb0019DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("解质押押品校验")
    @PostMapping("/xddb0019")
    protected @ResponseBody
    ResultDto<Xddb0019DataRespDto> xddb0019(@Validated @RequestBody Xddb0019DataReqDto xddb0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, JSON.toJSONString(xddb0019DataReqDto));
        Xddb0019DataRespDto xddb0019DataRespDto = new Xddb0019DataRespDto();// 响应Dto:解质押押品校验
        ResultDto<Xddb0019DataRespDto> xddb0019DataResultDto = new ResultDto<>();
        String drftNo = xddb0019DataReqDto.getDrftNo();//票号
        try {
            // 从xddb0019DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xddb0019DataRespDto = xddb0019Service.xddb0019(xddb0019DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xddb0019DataRespDto对象开始
            //xddb0019DataRespDto.setGuarStatus(StringUtils.EMPTY);// 押品状态
            // TODO 封装xddb0019DataRespDto对象结束
            // 封装xddb0019DataResultDto中正确的返回码和返回信息
            xddb0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);// 操作成功标志位
            xddb0019DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);// 描述信息
            xddb0019DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0019DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e){
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, e.getMessage());
            // 封装xddb0019DataResultDto中异常返回码和返回信息
            xddb0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);// 操作成功标志位
            xddb0019DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);// 描述信息
            xddb0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, e.getMessage());
            // 封装xddb0019DataResultDto中异常返回码和返回信息
            xddb0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);// 操作成功标志位
            xddb0019DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);// 描述信息
            xddb0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xddb0019DataRespDto到xddb0019DataResultDto中
        xddb0019DataResultDto.setData(xddb0019DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, JSON.toJSONString(xddb0019DataResultDto));
        return xddb0019DataResultDto;
    }
}
