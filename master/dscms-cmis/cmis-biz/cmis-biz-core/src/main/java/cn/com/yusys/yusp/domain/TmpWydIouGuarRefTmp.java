/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydIouGuarRef
 * @类描述: tmp_wyd_iou_guar_ref数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_iou_guar_ref_tmp")
public class TmpWydIouGuarRefTmp {
	
	/** 贷款借据编号 **/
	@Column(name = "LENDING_REF")
	private String lendingRef;
	
	/** 担保合同号 **/
	@Column(name = "GUAR_CONTRACT_NO")
	private String guarContractNo;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 合同号 **/
	@Column(name = "CONTRACT_NO", unique = false, nullable = true, length = 64)
	private String contractNo;
	
	/** 机构号 **/
	@Column(name = "ORG_ID", unique = false, nullable = true, length = 20)
	private String orgId;
	
	/** 贷款余额 **/
	@Column(name = "LOAN_BALANCE", unique = false, nullable = true, length = 20)
	private String loanBalance;
	
	/** 此笔贷款担保金额（保证人担保金额） **/
	@Column(name = "GUARANTY_AMT", unique = false, nullable = true, length = 20)
	private String guarantyAmt;
	
	/** 担保关系状态(1-正常 0-解除) **/
	@Column(name = "GUARANTY_STAT", unique = false, nullable = true, length = 1)
	private String guarantyStat;
	
	/** 担保关系建立时间 **/
	@Column(name = "INPUT_TIME", unique = false, nullable = true, length = 10)
	private String inputTime;
	
	/** 担保关系解除日期 **/
	@Column(name = "MATURITY_DATE", unique = false, nullable = true, length = 10)
	private String maturityDate;
	
	/** 平台id **/
	@Column(name = "MERCHANT_ID", unique = false, nullable = true, length = 32)
	private String merchantId;
	
	/** 借款额度 **/
	@Column(name = "LOAN_QUOTA", unique = false, nullable = true, length = 20)
	private String loanQuota;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param contractNo
	 */
	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}
	
    /**
     * @return contractNo
     */
	public String getContractNo() {
		return this.contractNo;
	}
	
	/**
	 * @param lendingRef
	 */
	public void setLendingRef(String lendingRef) {
		this.lendingRef = lendingRef;
	}
	
    /**
     * @return lendingRef
     */
	public String getLendingRef() {
		return this.lendingRef;
	}
	
	/**
	 * @param guarContractNo
	 */
	public void setGuarContractNo(String guarContractNo) {
		this.guarContractNo = guarContractNo;
	}
	
    /**
     * @return guarContractNo
     */
	public String getGuarContractNo() {
		return this.guarContractNo;
	}
	
	/**
	 * @param orgId
	 */
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
    /**
     * @return orgId
     */
	public String getOrgId() {
		return this.orgId;
	}
	
	/**
	 * @param loanBalance
	 */
	public void setLoanBalance(String loanBalance) {
		this.loanBalance = loanBalance;
	}
	
    /**
     * @return loanBalance
     */
	public String getLoanBalance() {
		return this.loanBalance;
	}
	
	/**
	 * @param guarantyAmt
	 */
	public void setGuarantyAmt(String guarantyAmt) {
		this.guarantyAmt = guarantyAmt;
	}
	
    /**
     * @return guarantyAmt
     */
	public String getGuarantyAmt() {
		return this.guarantyAmt;
	}
	
	/**
	 * @param guarantyStat
	 */
	public void setGuarantyStat(String guarantyStat) {
		this.guarantyStat = guarantyStat;
	}
	
    /**
     * @return guarantyStat
     */
	public String getGuarantyStat() {
		return this.guarantyStat;
	}
	
	/**
	 * @param inputTime
	 */
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	
    /**
     * @return inputTime
     */
	public String getInputTime() {
		return this.inputTime;
	}
	
	/**
	 * @param maturityDate
	 */
	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}
	
    /**
     * @return maturityDate
     */
	public String getMaturityDate() {
		return this.maturityDate;
	}
	
	/**
	 * @param merchantId
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	
    /**
     * @return merchantId
     */
	public String getMerchantId() {
		return this.merchantId;
	}
	
	/**
	 * @param loanQuota
	 */
	public void setLoanQuota(String loanQuota) {
		this.loanQuota = loanQuota;
	}
	
    /**
     * @return loanQuota
     */
	public String getLoanQuota() {
		return this.loanQuota;
	}


}