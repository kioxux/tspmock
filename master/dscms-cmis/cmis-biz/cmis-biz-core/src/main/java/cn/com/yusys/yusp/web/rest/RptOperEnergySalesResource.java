/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperEnergySales;
import cn.com.yusys.yusp.service.RptOperEnergySalesService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergySalesResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 09:44:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperenergysales")
public class RptOperEnergySalesResource {
    @Autowired
    private RptOperEnergySalesService rptOperEnergySalesService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperEnergySales>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperEnergySales> list = rptOperEnergySalesService.selectAll(queryModel);
        return new ResultDto<List<RptOperEnergySales>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperEnergySales>> index(QueryModel queryModel) {
        List<RptOperEnergySales> list = rptOperEnergySalesService.selectByModel(queryModel);
        return new ResultDto<List<RptOperEnergySales>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptOperEnergySales> show(@PathVariable("serno") String serno) {
        RptOperEnergySales rptOperEnergySales = rptOperEnergySalesService.selectByPrimaryKey(serno);
        return new ResultDto<RptOperEnergySales>(rptOperEnergySales);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperEnergySales> create(@RequestBody RptOperEnergySales rptOperEnergySales) throws URISyntaxException {
        rptOperEnergySalesService.insert(rptOperEnergySales);
        return new ResultDto<RptOperEnergySales>(rptOperEnergySales);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperEnergySales rptOperEnergySales) throws URISyntaxException {
        int result = rptOperEnergySalesService.update(rptOperEnergySales);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptOperEnergySalesService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperEnergySalesService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectBySerno")
    protected ResultDto<RptOperEnergySales> selectBySerno(@RequestBody Map<String,Object> map){
        String serno = map.get("serno").toString();
        return new ResultDto<RptOperEnergySales>(rptOperEnergySalesService.selectByPrimaryKey(serno));
    }
    @PostMapping("/updateSales")
    protected ResultDto<Integer> updateSales(@RequestBody RptOperEnergySales rptOperEnergySales){
        return new ResultDto<Integer>(rptOperEnergySalesService.updateSelective(rptOperEnergySales));
    }
    @PostMapping("/insertSales")
    protected ResultDto<Integer> insertSales(@RequestBody RptOperEnergySales rptOperEnergySales){
        return new ResultDto<Integer>(rptOperEnergySalesService.insertSelective(rptOperEnergySales));
    }

    /**
     * 初始化信息
     * @param map
     * @return
     */
    @PostMapping("/initSales")
    protected ResultDto<Integer> initSales(@RequestBody Map map){
        return  new ResultDto<Integer>(rptOperEnergySalesService.initSales(map));
    }

    @PostMapping("/deleteSales")
    protected ResultDto<Integer> deleteSales(@RequestBody String serno){
        return new ResultDto<Integer>(rptOperEnergySalesService.deleteByPrimaryKey(serno));
    }
}
