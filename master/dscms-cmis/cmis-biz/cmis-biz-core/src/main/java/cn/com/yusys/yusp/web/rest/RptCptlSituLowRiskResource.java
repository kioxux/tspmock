/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptCptlSituLowRisk;
import cn.com.yusys.yusp.service.RptCptlSituLowRiskService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptCptlSituLowRiskResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-09-26 15:24:15
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptcptlsitulowrisk")
public class RptCptlSituLowRiskResource {
    @Autowired
    private RptCptlSituLowRiskService rptCptlSituLowRiskService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptCptlSituLowRisk>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptCptlSituLowRisk> list = rptCptlSituLowRiskService.selectAll(queryModel);
        return new ResultDto<List<RptCptlSituLowRisk>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptCptlSituLowRisk>> index(QueryModel queryModel) {
        List<RptCptlSituLowRisk> list = rptCptlSituLowRiskService.selectByModel(queryModel);
        return new ResultDto<List<RptCptlSituLowRisk>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptCptlSituLowRisk> create(@RequestBody RptCptlSituLowRisk rptCptlSituLowRisk) throws URISyntaxException {
        rptCptlSituLowRiskService.insert(rptCptlSituLowRisk);
        return new ResultDto<RptCptlSituLowRisk>(rptCptlSituLowRisk);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptCptlSituLowRisk rptCptlSituLowRisk) throws URISyntaxException {
        int result = rptCptlSituLowRiskService.update(rptCptlSituLowRisk);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String serno) {
        int result = rptCptlSituLowRiskService.deleteByPrimaryKey(pkId, serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * 条件查询
     * @param model
     * @return
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<RptCptlSituLowRisk>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<RptCptlSituLowRisk>>(rptCptlSituLowRiskService.selectByModel(model));
    }

    /**
     * 删除
     * @param rptCptlSituLowRisk
     * @return
     */
    @PostMapping("/deleteLowRisk")
    protected ResultDto<Integer> deleteLowRisk(@RequestBody RptCptlSituLowRisk rptCptlSituLowRisk) {
        return new ResultDto<Integer>(rptCptlSituLowRiskService.deleteByPrimaryKey(rptCptlSituLowRisk.getPkId(), rptCptlSituLowRisk.getSerno()));
    }

    /**
     * 保存信息
     * @param rptCptlSituLowRisk
     * @return
     */
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptCptlSituLowRisk rptCptlSituLowRisk) {
        return new ResultDto<Integer>(rptCptlSituLowRiskService.save(rptCptlSituLowRisk));
    }
}
