/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.HashMap;
import java.util.List;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditCardTelvQuestionDetail;
import cn.com.yusys.yusp.repository.mapper.CreditCardTelvQuestionDetailMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardTelvQuestionDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:44:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCardTelvQuestionDetailService {
    private static final Logger log = LoggerFactory.getLogger(CreditCardTelvQuestionDetailService.class);
    @Autowired
    private CreditCardTelvQuestionDetailMapper creditCardTelvQuestionDetailMapper;
    @Autowired
    private  CreditCardTelvOtherInfoService creditCardTelvOtherInfoService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCardTelvQuestionDetail selectByPrimaryKey(String pkId) {
        return creditCardTelvQuestionDetailMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCardTelvQuestionDetail> selectAll(QueryModel model) {
        List<CreditCardTelvQuestionDetail> records = (List<CreditCardTelvQuestionDetail>) creditCardTelvQuestionDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCardTelvQuestionDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCardTelvQuestionDetail> list = creditCardTelvQuestionDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCardTelvQuestionDetail record) {
        return creditCardTelvQuestionDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCardTelvQuestionDetail record) {
        return creditCardTelvQuestionDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCardTelvQuestionDetail record) {
        return creditCardTelvQuestionDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCardTelvQuestionDetail record) {
        return creditCardTelvQuestionDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return creditCardTelvQuestionDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCardTelvQuestionDetailMapper.deleteByIds(ids);
    }

    /**
     * @param list
     * @return int
     * @author wzy
     * @date 2021/5/26 10:41
     * @version 1.0.0
     * @desc 新增电话调查问题数据
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int saveQuestionAndOther(List<CreditCardTelvQuestionDetail> list) {
        int result = 0;
        try {
            for (int i = 0;i<list.size();i++){
                CreditCardTelvQuestionDetail creditCardTelvQuestionDetail = list.get(i);
                String pkId  = creditCardTelvQuestionDetail.getPkId();
                if(pkId ==null || "".equals(pkId)){
                    pkId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, new HashMap<>());
                    creditCardTelvQuestionDetail.setPkId(pkId);
                    result = this.insertSelective(creditCardTelvQuestionDetail);
                }else{
                    result = this.updateSelective(creditCardTelvQuestionDetail);
                }
                if(result !=1){
                    throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, "修改电话调查问题数据异常！");
                }
            }
        }catch (Exception e){
            log.error("新增电话调查数据异常！",e);
            throw new YuspException(EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        return result;
    }

    public List<CreditCardTelvQuestionDetail> selectBySerno(String serno) {
        return  creditCardTelvQuestionDetailMapper.selectBySerno(serno);
    }
}
