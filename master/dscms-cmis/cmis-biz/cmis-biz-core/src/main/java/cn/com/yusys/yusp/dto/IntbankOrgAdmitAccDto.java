package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IntbankOrgAdmitAcc
 * @类描述: intbank_org_admit_acc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-13 09:04:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IntbankOrgAdmitAccDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 授信台帐号 **/
	private String accNo;
	
	/** 批复流水号 **/
	private String replySerno;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 同业机构准入 **/
	private String intbankOrgAdmit;
	
	/** 调查结论 **/
	private String indgtResult;
	
	/** 期限 **/
	private String term;
	
	/** 起始日期 **/
	private String startDate;
	
	/** 到期日期 **/
	private String endDate;
	
	/** 台账状态 **/
	private String accStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param accNo
	 */
	public void setAccNo(String accNo) {
		this.accNo = accNo == null ? null : accNo.trim();
	}
	
    /**
     * @return AccNo
     */	
	public String getAccNo() {
		return this.accNo;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno == null ? null : replySerno.trim();
	}
	
    /**
     * @return ReplySerno
     */	
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param intbankOrgAdmit
	 */
	public void setIntbankOrgAdmit(String intbankOrgAdmit) {
		this.intbankOrgAdmit = intbankOrgAdmit == null ? null : intbankOrgAdmit.trim();
	}
	
    /**
     * @return IntbankOrgAdmit
     */	
	public String getIntbankOrgAdmit() {
		return this.intbankOrgAdmit;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult == null ? null : indgtResult.trim();
	}
	
    /**
     * @return IndgtResult
     */	
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param term
	 */
	public void setTerm(String term) {
		this.term = term == null ? null : term.trim();
	}
	
    /**
     * @return Term
     */	
	public String getTerm() {
		return this.term;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate == null ? null : startDate.trim();
	}
	
    /**
     * @return StartDate
     */	
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate == null ? null : endDate.trim();
	}
	
    /**
     * @return EndDate
     */	
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus == null ? null : accStatus.trim();
	}
	
    /**
     * @return AccStatus
     */	
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}