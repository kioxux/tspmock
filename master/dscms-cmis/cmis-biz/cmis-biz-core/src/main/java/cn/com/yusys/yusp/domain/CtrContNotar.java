/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContNotar
 * @类描述: ctr_cont_notar数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-30 19:17:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "ctr_cont_notar")
public class CtrContNotar extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = false, length = 40)
	private String contNo;
	
	/** 是否公证 STD_ZB_IS_NOTAR **/
	@Column(name = "IS_NOTAR", unique = false, nullable = true, length = 5)
	private String isNotar;
	
	/** 公证员 **/
	@Column(name = "GREFFIER_NAME", unique = false, nullable = true, length = 50)
	private String greffierName;
	
	/** 公证合同编号 **/
	@Column(name = "NOTARY_CONT_CODE", unique = false, nullable = true, length = 32)
	private String notaryContCode;
	
	/** 公证单位注册号 **/
	@Column(name = "NOTARY_UNIT_NUM", unique = false, nullable = true, length = 32)
	private String notaryUnitNum;
	
	/** 公证单位名称 **/
	@Column(name = "NOTARY_UNIT_NAME", unique = false, nullable = true, length = 100)
	private String notaryUnitName;
	
	/** 公证书编号 **/
	@Column(name = "NOTARIZATION_CODE", unique = false, nullable = true, length = 32)
	private String notarizationCode;
	
	/** 公证日期 **/
	@Column(name = "NOTARY_DATE", unique = false, nullable = true, length = 10)
	private String notaryDate;
	
	/** 公证合同类型 **/
	@Column(name = "NOTARY_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String notaryContType;
	
	/** 备注 **/
	@Column(name = "REMARK", unique = false, nullable = true, length = 200)
	private String remark;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param isNotar
	 */
	public void setIsNotar(String isNotar) {
		this.isNotar = isNotar;
	}
	
    /**
     * @return isNotar
     */
	public String getIsNotar() {
		return this.isNotar;
	}
	
	/**
	 * @param greffierName
	 */
	public void setGreffierName(String greffierName) {
		this.greffierName = greffierName;
	}
	
    /**
     * @return greffierName
     */
	public String getGreffierName() {
		return this.greffierName;
	}
	
	/**
	 * @param notaryContCode
	 */
	public void setNotaryContCode(String notaryContCode) {
		this.notaryContCode = notaryContCode;
	}
	
    /**
     * @return notaryContCode
     */
	public String getNotaryContCode() {
		return this.notaryContCode;
	}
	
	/**
	 * @param notaryUnitNum
	 */
	public void setNotaryUnitNum(String notaryUnitNum) {
		this.notaryUnitNum = notaryUnitNum;
	}
	
    /**
     * @return notaryUnitNum
     */
	public String getNotaryUnitNum() {
		return this.notaryUnitNum;
	}
	
	/**
	 * @param notaryUnitName
	 */
	public void setNotaryUnitName(String notaryUnitName) {
		this.notaryUnitName = notaryUnitName;
	}
	
    /**
     * @return notaryUnitName
     */
	public String getNotaryUnitName() {
		return this.notaryUnitName;
	}
	
	/**
	 * @param notarizationCode
	 */
	public void setNotarizationCode(String notarizationCode) {
		this.notarizationCode = notarizationCode;
	}
	
    /**
     * @return notarizationCode
     */
	public String getNotarizationCode() {
		return this.notarizationCode;
	}
	
	/**
	 * @param notaryDate
	 */
	public void setNotaryDate(String notaryDate) {
		this.notaryDate = notaryDate;
	}
	
    /**
     * @return notaryDate
     */
	public String getNotaryDate() {
		return this.notaryDate;
	}
	
	/**
	 * @param notaryContType
	 */
	public void setNotaryContType(String notaryContType) {
		this.notaryContType = notaryContType;
	}
	
    /**
     * @return notaryContType
     */
	public String getNotaryContType() {
		return this.notaryContType;
	}
	
	/**
	 * @param remark
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
    /**
     * @return remark
     */
	public String getRemark() {
		return this.remark;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}