package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarCertiRela
 * @类描述: guar_certi_rela数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-01-18 11:38:32
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarCertiRelaDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 权证类型 STD_ZB_CERTI_TYPE_CD **/
	private String certiTypeCd;
	
	/** 权利凭证号 **/
	private String certiRecordId;
	
	/** 权证类别 STD_ZB_CERTI_CATALOG  **/
	private String certiCatalog;
	
	/** 权证发证机关名称 STD_ZB_ **/
	private String certiOrgName;
	
	/** 权证发证日期 **/
	private String certiStartDate;
	
	/** 权证到期日期 **/
	private String certiEndDate;
	
	/** 权证入库日期 **/
	private String inDate;
	
	/** 权证正常出库日期 **/
	private String outDate;
	
	/** 权证出库原因 STD_ZB_OUT_REASON **/
	private String outReason;
	
	/** 权证临时借用人名称 **/
	private String tempBorrowerName;
	
	/** 权证临时出库日期 **/
	private String tempOutDate;
	
	/** 权证预计归还时间 **/
	private String preBackDate;
	
	/** 权证实际归还日期 **/
	private String realBackDate;
	
	/** 权证临时出库原因 **/
	private String tempOutReason;
	
	/** 权证状态 STD_ZB_CERTI_STATE **/
	private String certiState;
	
	/** 是否主权证 **/
	private String isMainCerti;
	
	/** 保管机构 **/
	private String depositOrgNo;
	
	/** 权证备注信息 **/
	private String certiComment;
	
	/** 权证续期原因 **/
	private String extReason;
	
	/** 权利金额 **/
	private java.math.BigDecimal certiAmt;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param certiTypeCd
	 */
	public void setCertiTypeCd(String certiTypeCd) {
		this.certiTypeCd = certiTypeCd == null ? null : certiTypeCd.trim();
	}
	
    /**
     * @return CertiTypeCd
     */	
	public String getCertiTypeCd() {
		return this.certiTypeCd;
	}
	
	/**
	 * @param certiRecordId
	 */
	public void setCertiRecordId(String certiRecordId) {
		this.certiRecordId = certiRecordId == null ? null : certiRecordId.trim();
	}
	
    /**
     * @return CertiRecordId
     */	
	public String getCertiRecordId() {
		return this.certiRecordId;
	}
	
	/**
	 * @param certiCatalog
	 */
	public void setCertiCatalog(String certiCatalog) {
		this.certiCatalog = certiCatalog == null ? null : certiCatalog.trim();
	}
	
    /**
     * @return CertiCatalog
     */	
	public String getCertiCatalog() {
		return this.certiCatalog;
	}
	
	/**
	 * @param certiOrgName
	 */
	public void setCertiOrgName(String certiOrgName) {
		this.certiOrgName = certiOrgName == null ? null : certiOrgName.trim();
	}
	
    /**
     * @return CertiOrgName
     */	
	public String getCertiOrgName() {
		return this.certiOrgName;
	}
	
	/**
	 * @param certiStartDate
	 */
	public void setCertiStartDate(String certiStartDate) {
		this.certiStartDate = certiStartDate == null ? null : certiStartDate.trim();
	}
	
    /**
     * @return CertiStartDate
     */	
	public String getCertiStartDate() {
		return this.certiStartDate;
	}
	
	/**
	 * @param certiEndDate
	 */
	public void setCertiEndDate(String certiEndDate) {
		this.certiEndDate = certiEndDate == null ? null : certiEndDate.trim();
	}
	
    /**
     * @return CertiEndDate
     */	
	public String getCertiEndDate() {
		return this.certiEndDate;
	}
	
	/**
	 * @param inDate
	 */
	public void setInDate(String inDate) {
		this.inDate = inDate == null ? null : inDate.trim();
	}
	
    /**
     * @return InDate
     */	
	public String getInDate() {
		return this.inDate;
	}
	
	/**
	 * @param outDate
	 */
	public void setOutDate(String outDate) {
		this.outDate = outDate == null ? null : outDate.trim();
	}
	
    /**
     * @return OutDate
     */	
	public String getOutDate() {
		return this.outDate;
	}
	
	/**
	 * @param outReason
	 */
	public void setOutReason(String outReason) {
		this.outReason = outReason == null ? null : outReason.trim();
	}
	
    /**
     * @return OutReason
     */	
	public String getOutReason() {
		return this.outReason;
	}
	
	/**
	 * @param tempBorrowerName
	 */
	public void setTempBorrowerName(String tempBorrowerName) {
		this.tempBorrowerName = tempBorrowerName == null ? null : tempBorrowerName.trim();
	}
	
    /**
     * @return TempBorrowerName
     */	
	public String getTempBorrowerName() {
		return this.tempBorrowerName;
	}
	
	/**
	 * @param tempOutDate
	 */
	public void setTempOutDate(String tempOutDate) {
		this.tempOutDate = tempOutDate == null ? null : tempOutDate.trim();
	}
	
    /**
     * @return TempOutDate
     */	
	public String getTempOutDate() {
		return this.tempOutDate;
	}
	
	/**
	 * @param preBackDate
	 */
	public void setPreBackDate(String preBackDate) {
		this.preBackDate = preBackDate == null ? null : preBackDate.trim();
	}
	
    /**
     * @return PreBackDate
     */	
	public String getPreBackDate() {
		return this.preBackDate;
	}
	
	/**
	 * @param realBackDate
	 */
	public void setRealBackDate(String realBackDate) {
		this.realBackDate = realBackDate == null ? null : realBackDate.trim();
	}
	
    /**
     * @return RealBackDate
     */	
	public String getRealBackDate() {
		return this.realBackDate;
	}
	
	/**
	 * @param tempOutReason
	 */
	public void setTempOutReason(String tempOutReason) {
		this.tempOutReason = tempOutReason == null ? null : tempOutReason.trim();
	}
	
    /**
     * @return TempOutReason
     */	
	public String getTempOutReason() {
		return this.tempOutReason;
	}
	
	/**
	 * @param certiState
	 */
	public void setCertiState(String certiState) {
		this.certiState = certiState == null ? null : certiState.trim();
	}
	
    /**
     * @return CertiState
     */	
	public String getCertiState() {
		return this.certiState;
	}
	
	/**
	 * @param isMainCerti
	 */
	public void setIsMainCerti(String isMainCerti) {
		this.isMainCerti = isMainCerti == null ? null : isMainCerti.trim();
	}
	
    /**
     * @return IsMainCerti
     */	
	public String getIsMainCerti() {
		return this.isMainCerti;
	}
	
	/**
	 * @param depositOrgNo
	 */
	public void setDepositOrgNo(String depositOrgNo) {
		this.depositOrgNo = depositOrgNo == null ? null : depositOrgNo.trim();
	}
	
    /**
     * @return DepositOrgNo
     */	
	public String getDepositOrgNo() {
		return this.depositOrgNo;
	}
	
	/**
	 * @param certiComment
	 */
	public void setCertiComment(String certiComment) {
		this.certiComment = certiComment == null ? null : certiComment.trim();
	}
	
    /**
     * @return CertiComment
     */	
	public String getCertiComment() {
		return this.certiComment;
	}
	
	/**
	 * @param extReason
	 */
	public void setExtReason(String extReason) {
		this.extReason = extReason == null ? null : extReason.trim();
	}
	
    /**
     * @return ExtReason
     */	
	public String getExtReason() {
		return this.extReason;
	}
	
	/**
	 * @param certiAmt
	 */
	public void setCertiAmt(java.math.BigDecimal certiAmt) {
		this.certiAmt = certiAmt;
	}
	
    /**
     * @return CertiAmt
     */	
	public java.math.BigDecimal getCertiAmt() {
		return this.certiAmt;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}


}