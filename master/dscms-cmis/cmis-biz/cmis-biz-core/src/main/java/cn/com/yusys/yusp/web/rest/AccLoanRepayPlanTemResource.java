/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccLoanRepayPlanTem;
import cn.com.yusys.yusp.service.AccLoanRepayPlanTemService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoanRepayPlanTemResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-06 23:07:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accloanrepayplantem")
public class AccLoanRepayPlanTemResource {
    @Autowired
    private AccLoanRepayPlanTemService accLoanRepayPlanTemService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccLoanRepayPlanTem>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccLoanRepayPlanTem> list = accLoanRepayPlanTemService.selectAll(queryModel);
        return new ResultDto<List<AccLoanRepayPlanTem>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccLoanRepayPlanTem>> index(QueryModel queryModel) {
        List<AccLoanRepayPlanTem> list = accLoanRepayPlanTemService.selectByModel(queryModel);
        return new ResultDto<List<AccLoanRepayPlanTem>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AccLoanRepayPlanTem> show(@PathVariable("pkId") String pkId) {
        AccLoanRepayPlanTem accLoanRepayPlanTem = accLoanRepayPlanTemService.selectByPrimaryKey(pkId);
        return new ResultDto<AccLoanRepayPlanTem>(accLoanRepayPlanTem);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccLoanRepayPlanTem> create(@RequestBody AccLoanRepayPlanTem accLoanRepayPlanTem) throws URISyntaxException {
        accLoanRepayPlanTemService.insert(accLoanRepayPlanTem);
        return new ResultDto<AccLoanRepayPlanTem>(accLoanRepayPlanTem);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccLoanRepayPlanTem accLoanRepayPlanTem) throws URISyntaxException {
        int result = accLoanRepayPlanTemService.update(accLoanRepayPlanTem);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = accLoanRepayPlanTemService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accLoanRepayPlanTemService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: deleteByBillNo
     * @函数描述: 根据贷款台账传的借据编号删除
     * @参数与返回说明:
     * @算法描述:
     * liuquan
     */
    @PostMapping("/deleteByBillNo")
    protected ResultDto<Integer> deleteByBillNo(String billNo) {
        int result = accLoanRepayPlanTemService.deleteByBillNo(billNo);
        return new ResultDto<Integer>(result);
    }
}
