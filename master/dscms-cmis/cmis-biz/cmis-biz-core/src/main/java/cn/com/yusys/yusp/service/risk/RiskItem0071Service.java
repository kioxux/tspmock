package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.req.CmisLmt0015ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015LmtSubAccListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0015.resp.CmisLmt0015RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.Comm4CalFormulaUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0071Service
 * @类描述: 还款能力分析校验
 * @功能描述: 还款能力分析校验
 * @创建人: dumd
 * @创建时间: 2021年8月2日15:23:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0071Service {

    private static final Logger logger = LoggerFactory.getLogger(RiskItem0071Service.class);

    @Autowired
    private LmtSigInvestAppService lmtSigInvestAppService ;

    @Autowired
    private LmtSigInvestBasicInfoService lmtSigInvestBasicInfoService ;

    @Autowired
    private LmtSigInvestBasicLimitAppService lmtSigInvestBasicLimitAppService ;

    @Autowired
    private LmtSigInvestBasicInfoSubService lmtSigInvestBasicInfoSubService ;

    @Autowired
    private Comm4CalFormulaUtils comm4CalFormulaUtils ;

    @Autowired
    private  CmisLmtClientService cmisLmtClientService ;

    @Autowired
    private LmtSigInvestApprService lmtSigInvestApprService ;

    @Autowired
    private LmtSigInvestBasicInfoApprService lmtSigInvestBasicInfoApprService ;

    @Autowired
    private LmtSigInvestBasicInfoSubApprService lmtSigInvestBasicInfoSubApprService ;

    @Autowired
    private LmtSigInvestChgReplyService lmtSigInvestChgReplyService ;

    @Autowired
    private LmtSigInvestRstService lmtSigInvestRstService ;

    @Autowired
    private LmtSigInvestSubAppService lmtSigInvestSubAppService;


    /**
     * @方法名称: riskItem0071
     * @方法描述: 还款能力分析校验
     * @参数与返回说明:
     * @算法描述: 第二还款来源分析信息补全校验
     * @创建人: dumd
     * @创建时间: 2021年8月2日15:23:06
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0071(QueryModel queryModel) {
        logger.info(this.getClass().getName()+"校验额度风险拦截开始--------------->start");
        RiskResultDto riskResultDto = new RiskResultDto();
        Map<String, String> paramMap = (Map<String, String>) queryModel.getCondition().get("param");
        String tableName = paramMap.get("tableName") ;
        //必须是资金同业授信，才进行风险拦截校验
        if(!("lmtsiginvestapp".equals(tableName) || "lmtSigInvestChgReply".equals(tableName))) {
            logger.info(this.getClass().getName()+"非资金主体产品授信，风险拦截默认通过---------------");
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
            return riskResultDto;
        }
        String serno = queryModel.getCondition().get("bizId").toString();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }

        if("lmtsiginvestapp".equals(tableName)){
            //更具申请流水号获取产品授信申请信息
            LmtSigInvestApp lmtSigInvestApp = lmtSigInvestAppService.selectBySerno(serno);
            if (lmtSigInvestApp == null) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07101); //授信申请信息查询失败
                return riskResultDto;
            }
            String approveStatus = lmtSigInvestApp.getApproveStatus();
            //流程审批中调用审批表数据进行校验处理
            if (CmisLmtConstants.APP_STATUS_111.equals(approveStatus)) {
                logger.info("单笔透支授信，流程审批中校验--------start");
                RiskResultDto riskResultDtoAppr =  getRiskByApprResultDto(riskResultDto, serno, lmtSigInvestApp);
                logger.info("单笔透支授信，流程审批中校验--------end，校验结果{}", JSON.toJSONString(riskResultDtoAppr));
                if (riskResultDtoAppr != null) return riskResultDtoAppr;
            } else {
                //待发起或者打回从申请表中获取数据进行判断
                logger.info("单笔透支授信，流程审批开始校验--------start");
                RiskResultDto riskResultDtoApp = getRiskByAppResultDto(lmtSigInvestApp, serno);
                logger.info("单笔透支授信，流程审批开始校验--------end，校验结果{}", JSON.toJSONString(riskResultDtoApp));
                if (riskResultDtoApp != null) return riskResultDtoApp;
            }
        }else{
            logger.info("批复变更校验--------start");
            //tableName="lmtSigInvestChgReply" 批复变更处理
            LmtSigInvestChgReply lmtSigInvestChgReply = lmtSigInvestChgReplyService.selectBySerno(serno) ;
            BigDecimal lmtAmt = NumberUtils.nullDefaultZero(lmtSigInvestChgReply.getLmtAmt());
            String replySerno = lmtSigInvestChgReply.getReplySerno();
            //更新批复结果表
            LmtSigInvestRst lmtSigInvestRst = lmtSigInvestRstService.selectByReplySerno(replySerno);
            String cusCatalog = lmtSigInvestRst.getCusCatalog() ;
            BigDecimal origiLmtAmt = lmtSigInvestRst.getLmtAmt() ;
            if (CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(cusCatalog)) {
                String limitSubNo = lmtSigInvestRst.getLmtBizType() ;
                //同业客户
                String lmtCusId = lmtSigInvestRst.getCusId() ;
                //获取占用总额
                BigDecimal useAmt = NumberUtils.sub(lmtAmt, origiLmtAmt);
                if(useAmt.compareTo(BigDecimal.ZERO)>0){
                    RiskResultDto riskResultDto1 = generateLmtContRel4Check(serno, limitSubNo, lmtCusId, useAmt);
                    logger.info("批复变更校验--------结果{}", JSON.toJSONString(riskResultDto1));
                    if (riskResultDto1 != null) return riskResultDto1;
                }
            }
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        logger.info(this.getClass().getName()+"校验额度风险拦截--------------->end");
        return riskResultDto;
    }

    /**
     *
     * @param riskResultDto
     * @param serno
     * @param lmtSigInvestApp
     * @return
     */
    private RiskResultDto getRiskByApprResultDto(RiskResultDto riskResultDto, String serno, LmtSigInvestApp lmtSigInvestApp) {
        LmtSigInvestAppr lmtSigInvestAppr = lmtSigInvestApprService.selectBySerno(serno);

        if (lmtSigInvestAppr == null) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07101); //授信申请信息查询失败
            return riskResultDto;
        }

        BigDecimal oriUserAmt = BigDecimal.ZERO ;
        String appType = lmtSigInvestApp.getAppType() ;
        // 变更
        if(CmisBizConstants.STD_SX_LMT_TYPE_02.equals(appType) || CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(appType)
                || CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(appType)){
            oriUserAmt = NumberUtils.nullDefaultZero(lmtSigInvestAppr.getOrigiLmtAmt()) ;
        }

        String cusCatalog = lmtSigInvestAppr.getCusCatalog() ;
        if (CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(cusCatalog)) {
            logger.info("单笔透支授信，同业客户校验是否有同业投资类等额度--------start");
            String limitSubNo = lmtSigInvestAppr.getLmtBizType() ;
            //同业客户
            String lmtCusId = lmtSigInvestAppr.getCusId() ;
            //获取占用总额
            BigDecimal useAmt = NumberUtils.nullDefaultZero(lmtSigInvestAppr.getLmtAmt());
            useAmt = useAmt.subtract(oriUserAmt) ;
            RiskResultDto riskResultDto1 = generateLmtContRel4Check(serno, limitSubNo, lmtCusId, useAmt);
            logger.info("单笔透支授信，同业客户校验是否有同业投资类额度--------end,校验结果{}", JSON.toJSONString(riskResultDto1));
            if (riskResultDto1 != null) return riskResultDto1;
        }

        //获取审批申请流水号
        String approveSerno = lmtSigInvestAppr.getApproveSerno();
        //根据审批流水号，获取底层
        //根据授信申请信息查询是否生成
        LmtSigInvestBasicInfoAppr lmtSigInvestBasicInfoAppr = lmtSigInvestBasicInfoApprService.selectByApproveSerno(approveSerno);
        String isPassBasicAsset = "" ;
        if (lmtSigInvestBasicInfoAppr != null) {
            isPassBasicAsset = lmtSigInvestBasicInfoAppr.getIsPassBasicAsset();
        }

        //穿透到底层额度
        logger.info("单笔透支授信，是否穿透到底层额度【{}】(1-是 2-否)", isPassBasicAsset);
        if (CommonConstance.STD_ZB_YES_NO_1.equals(isPassBasicAsset)) {
            //获取单笔投资关联底层信息明细申请信息，查看是否生成穿透话额度
            List<LmtSigInvestBasicInfoSubAppr> lmtSigInvestBasicInfoSubApprList = lmtSigInvestBasicInfoSubApprService.selectByApproveSerno(approveSerno)  ;
            if (CollectionUtils.isEmpty(lmtSigInvestBasicInfoSubApprList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07102); //穿透到底层查询底层额度信息失败
                return riskResultDto;
            }

            LmtSigInvestSubApp lmtSigInvestSubApp = lmtSigInvestSubAppService.selectBySerno(lmtSigInvestAppr.getSerno());
            if (lmtSigInvestSubApp != null &&
                    (lmtSigInvestSubApp.getPrdTotalAmt() == null || BigDecimal.ZERO.compareTo(lmtSigInvestSubApp.getPrdTotalAmt()) == 0 )){
                throw BizException.error(null,"9999","项目总金额获取失败");
            }

            for (LmtSigInvestBasicInfoSubAppr lmtSigInvestBasicInfoSubAppr : lmtSigInvestBasicInfoSubApprList) {
                String isAppBasicLmt = lmtSigInvestBasicInfoSubAppr.getIsAppBasicLmt();
                if (StringUtils.isBlank(isAppBasicLmt)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07103); //穿透到底层查询底层额度信息失败
                    return riskResultDto;
                }
                //不申报额度，则需要占用底层额度，校验额度是否足额
                logger.info("单笔透支授信，不申报额度，则需要占用底层额度，是否申报额度【{}】(1-是 2-否)", isAppBasicLmt);
                if (CommonConstance.STD_ZB_YES_NO_0.equals(isAppBasicLmt)) {
                    //获取占用总额
                    BigDecimal prdLmtAmt = lmtSigInvestApp.getLmtAmt();
                    BigDecimal basicAssetBal = lmtSigInvestBasicInfoSubAppr.getBasicAssetBalanceAmt();
                    BigDecimal prdTotalAmt = lmtSigInvestSubApp.getPrdTotalAmt();
                    BigDecimal useAmt = comm4CalFormulaUtils.getPrdLmtUseAmt(prdLmtAmt, basicAssetBal, prdTotalAmt);
                    useAmt = useAmt.subtract(oriUserAmt) ;
                    //穿透底层额度分项编号
                    String subSerno = lmtSigInvestBasicInfoSubAppr.getUseBasicLmtSubSerno();
                    String basicCusId = lmtSigInvestBasicInfoSubAppr.getBasicCusId();
                    String basicCusName = lmtSigInvestBasicInfoSubAppr.getBasicCusName();
                    //底层客户大类
                    String basicCusCatalog = lmtSigInvestBasicInfoSubAppr.getBasicCusCatalog() ;
                    String lmtType = CmisLmtConstants.STD_ZB_LMT_TYPE_01 ;
                    if (CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(basicCusCatalog)) {
                        lmtType = CmisLmtConstants.STD_ZB_LMT_TYPE_07 ;
                    }
                    //组装请求报文
                    CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
                    cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0009ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);//金融机构代码
                    cmisLmt0009ReqDto.setDealBizNo(subSerno);//合同编号
                    cmisLmt0009ReqDto.setCusId(basicCusId);//客户编号
                    cmisLmt0009ReqDto.setCusName(basicCusName);//客户名称
                    cmisLmt0009ReqDto.setDealBizType(CmisCommonConstants.STD_CONT_TYPE_1);//交易业务类型
                    //cmisLmt0009ReqDto.setPrdId(useBasicLmtItemNo);//产品编号
                    //cmisLmt0009ReqDto.setPrdName(useBasicLmtItemName);//产品名称
                    cmisLmt0009ReqDto.setIsLriskBiz(CommonConstance.STD_ZB_YES_NO_0);//是否低风险
                    cmisLmt0009ReqDto.setIsFollowBiz(CommonConstance.STD_ZB_YES_NO_0);//是否无缝衔接
                    cmisLmt0009ReqDto.setIsBizRev(CommonConstance.STD_ZB_YES_NO_0);//是否合同重签
                    cmisLmt0009ReqDto.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                    cmisLmt0009ReqDto.setDealBizAmt(useAmt);//交易业务金额
                    cmisLmt0009ReqDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
                    cmisLmt0009ReqDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金
                    List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
                    CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
                    cmisLmt0009OccRelListReqDto.setLmtType(lmtType);//额度类型
                    cmisLmt0009OccRelListReqDto.setLmtSubNo(subSerno);//额度分项编号
                    cmisLmt0009OccRelListReqDto.setBizTotalAmt(useAmt);//占用总额(折人民币)
                    cmisLmt0009OccRelListReqDto.setBizSpacAmt(useAmt);//占用敞口(折人民币)
                    cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
                    cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);

                    logger.info("根据业务申请编号【" + subSerno + "】前往额度系统-额度校验【cmislmt0009】请求报文：" + JSON.toJSONString(cmisLmt0009ReqDto));
                    ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
                    logger.info("根据业务申请编号【" + subSerno + "】前往额度系统-额度校验【cmislmt0009】返回报文：" + JSON.toJSONString(cmisLmt0009RespDto));

                    String code = cmisLmt0009RespDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(cmisLmt0009RespDto.getData().getErrorMsg()); //接口返回不通过信息
                        return riskResultDto;
                    }
                }
            }
        }
        return null;
    }

    /**
     * @作者:lizx
     * @方法名称: generateLmtContRel4Check
     * @方法描述:  占用同业管理类额度、占用同业投资类额度 判断额度是否足额
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/8/10 9:31
     * @param serno:
     * @param limitSubNo:
     * @param lmtCusId:
     * @param useAmt:
     * @return: cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @算法描述: 无
    */
    private RiskResultDto generateLmtContRel4Check(String serno, String limitSubNo ,String lmtCusId, BigDecimal useAmt ) {
        logger.info("占用同业管理类额度、占用同业投资类额度 判断额度是否足额---start");
        RiskResultDto riskResultDto = new RiskResultDto();
        String lmtBizType = "" ;
        if (CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4003.equals(limitSubNo) || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4004.equals(limitSubNo)
                || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4005.equals(limitSubNo) || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4006.equals(limitSubNo)
                || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4007.equals(limitSubNo)) {
            //资产-其他、资产-债权融资计划、理财直融工具、净值型产品、资产证券化产品（标准）、资产证券化产品（非标）需要占用同业管理类额度
            lmtBizType = CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3006;
        }else if (CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4001.equals(limitSubNo) || CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4002.equals(limitSubNo)
                ||CmisLmtConstants.STD_ZB_PRD_BIZ_TYPE_4008.equals(limitSubNo)) {
            //债券池、其他标准化债权投资类需要占用同业投资类额度
            lmtBizType = CmisLmtConstants.STD_ZB_LMT_BIZ_TYPE_3002;
        }

        // lmtBizType 不为空，则需要占用同业管理类额度或同业投资类额度
        if(StringUtils.nonBlank(lmtBizType)){
            CmisLmt0015ReqDto reqDto = new CmisLmt0015ReqDto() ;
            reqDto.setCusId(lmtCusId);
            reqDto.setSerno(serno);
            reqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);
            reqDto.setQueryType(CmisLmtConstants.STD_ZB_LMT_TYPE_07);
            reqDto.setCusType(CmisLmtConstants.STD_ZB_CUS_CATALOG3);
            reqDto.setStartNum(0);
            reqDto.setPageCount(10000);
            logger.info(this.getClass().getName()+"产品授信调用cmislmt0015查询客户【{}】,额度信息-----------start 请求报文【{}】", lmtCusId, reqDto);
            ResultDto<CmisLmt0015RespDto> resultDtoCmisLmt0015Dto = cmisLmtClientService.cmisLmt0015(reqDto) ;
            logger.info(this.getClass().getName()+"产品授信调用cmislmt0015查询客户【{}】,额度信息-----------end 相应报文【{}】", lmtCusId, resultDtoCmisLmt0015Dto);
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, resultDtoCmisLmt0015Dto.getCode())) {
                CmisLmt0015RespDto cmisLmt0015RespDto = resultDtoCmisLmt0015Dto.getData();
                List<CmisLmt0015LmtSubAccListRespDto> cmisLmt0015LmtSubAccListRespDtoList = cmisLmt0015RespDto.getLmtSubAccList();
                if (CollectionUtils.nonEmpty(cmisLmt0015LmtSubAccListRespDtoList)) {
                    String finalLmtBizType = lmtBizType;
                    //定义过滤条件
                    Predicate<CmisLmt0015LmtSubAccListRespDto> resFilter = (resIns) -> (finalLmtBizType.equals(resIns.getLimitSubNo())) ;
                    //定义过滤后的list
                    List<CmisLmt0015LmtSubAccListRespDto> lmtBizTypeList = new ArrayList<>() ;
                    //对数据据进行过滤
                    cmisLmt0015LmtSubAccListRespDtoList.stream().filter(resFilter).forEach((reqResut)-> lmtBizTypeList.add(reqResut));

                    logger.info("占用同业管理类额度、占用同业投资类额度 判断额度是否足额，额度类型{}---start", finalLmtBizType);
                    if(CollectionUtils.nonEmpty(lmtBizTypeList)){
                        if(lmtBizTypeList.size()>1){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07105); //产品授信，查到多条同业投资类额度或同业管理类额度！
                            return riskResultDto;
                        }

                        //获取分项信息
                        CmisLmt0015LmtSubAccListRespDto lmtSubAccListRespDto = lmtBizTypeList.get(0) ;
                        logger.info("占用同业管理类额度、占用同业投资类额度,额度信息{}", JSON.toJSONString(lmtSubAccListRespDto));
                        String status = lmtSubAccListRespDto.getStatus() ;
                        if(!CmisLmtConstants.STD_ZB_APPR_ST_01.equals(status)){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07106); //产品授信，占用同业投资类额度或同业管理类额度状态不可用!
                            return riskResultDto;
                        }

                        //判断是否足额
                        if(useAmt.compareTo(NumberUtils.nullDefaultZero(lmtSubAccListRespDto.getAvlAvailAmt()))>0){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07107); //产品授信，占用同业投资类额度或同业管理类额度状态不可用!
                            return riskResultDto;
                        }
                    }else{
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07104); //产品授信，未查到需占用的同业投资类额度和同业管理类额度
                        return riskResultDto;
                    }
                }else{
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07104); //产品授信，未查到需占用的同业投资类额度和同业管理类额度
                    return riskResultDto;
                }
            }else{
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07108); //产品授信，调用CmisLmt0015接口查询同业投资类额度或同业管理类额度失败！
                return riskResultDto;
            }
        }
        logger.info("占用同业管理类额度、占用同业投资类额度 判断额度是否足额--end");
        return null;
    }

    /**
     * @作者:lizx
     * @方法名称: getRiskByAppResultDto
     * @方法描述: 流程第一个节点提交，从申请表中去相关数据信息进行风险拦截校验
     * @参数与返回说明: 
     * @算法描述: 无
     * @日期：2021/8/4 16:31
     * @param serno:
     * @return: cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @算法描述: 无
    */
    private RiskResultDto getRiskByAppResultDto(LmtSigInvestApp lmtSigInvestApp, String serno) {
        RiskResultDto riskResultDto = new RiskResultDto();
        //根据授信申请信息查询是否生成
        LmtSigInvestBasicInfo lmtSigInvestBasicInfo = lmtSigInvestBasicInfoService.selectBySerno(serno);

        String cusCatalog = lmtSigInvestApp.getCusCatalog() ;
        BigDecimal oriUserAmt = BigDecimal.ZERO ;
        String appType = lmtSigInvestApp.getAppType() ;
        // 变更  复议  续作
        if(CmisBizConstants.STD_SX_LMT_TYPE_02.equals(appType) || CmisLmtConstants.STD_SX_LMT_TYPE_03.equals(appType)
                || CmisLmtConstants.STD_SX_LMT_TYPE_05.equals(appType)){
            oriUserAmt = NumberUtils.nullDefaultZero(lmtSigInvestApp.getOrigiLmtAmt()) ;
        }

        if (CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(cusCatalog)) {
            String limitSubNo = lmtSigInvestApp.getLmtBizType() ;
            //同业客户
            String lmtCusId = lmtSigInvestApp.getCusId() ;
            //获取占用总额
            BigDecimal useAmt = lmtSigInvestApp.getLmtAmt();
            useAmt = useAmt.subtract(oriUserAmt) ;
            RiskResultDto riskResultDto1 = generateLmtContRel4Check(serno, limitSubNo, lmtCusId, useAmt);
            if (riskResultDto1 != null) return riskResultDto1;
        }

        String isPassBasicAsset = "" ;
        if(lmtSigInvestBasicInfo != null){
            isPassBasicAsset = lmtSigInvestBasicInfo.getIsPassBasicAsset() ;
        }
        //穿透到底层额度
        if(CommonConstance.STD_ZB_YES_NO_1.equals(isPassBasicAsset)){
            logger.info("穿透到底层校验处理开始---------------start");
            //获取单笔投资关联底层信息明细申请信息，查看是否生成穿透话额度
            List<LmtSigInvestBasicInfoSub> lmtSigInvestBasicInfoSubList = lmtSigInvestBasicInfoSubService.selectBySerno(serno) ;
            if(CollectionUtils.isEmpty(lmtSigInvestBasicInfoSubList)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07102); //穿透到底层查询底层额度信息失败
                return riskResultDto;
            }

            for (LmtSigInvestBasicInfoSub lmtSigInvestBasicInfoSub : lmtSigInvestBasicInfoSubList) {
                String isAppBasicLmt = lmtSigInvestBasicInfoSub.getIsAppBasicLmt() ;
                if(StringUtils.isBlank(isAppBasicLmt)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_07103); //穿透到底层查询底层额度信息失败
                    return riskResultDto;
                }
                //不申报额度，则需要占用底层额度，校验额度是否足额
                if(CommonConstance.STD_ZB_YES_NO_0.equals(isAppBasicLmt)){
                    //获取占用总额
                    BigDecimal prdLmtAmt = lmtSigInvestApp.getLmtAmt() ;
                    BigDecimal basicAssetBal = lmtSigInvestBasicInfoSub.getBasicAssetBalanceAmt() ;
                    BigDecimal actualIssuAmt = lmtSigInvestApp.getIntendActualIssuedScale() ;
                    BigDecimal useAmt = comm4CalFormulaUtils.getPrdLmtUseAmt(prdLmtAmt, basicAssetBal, actualIssuAmt) ;
                    useAmt = useAmt.subtract(oriUserAmt) ;
                    //穿透底层额度分项编号
                    String subSerno = lmtSigInvestBasicInfoSub.getUseBasicLmtSubSerno() ;
                    String basicCusId = lmtSigInvestBasicInfoSub.getBasicCusId() ;
                    String basicCusName = lmtSigInvestBasicInfoSub.getBasicCusName() ;
                    String useBasicLmtItemNo = lmtSigInvestBasicInfoSub.getUseBasicLmtItemNo() ;
                    String useBasicLmtItemName = lmtSigInvestBasicInfoSub.getUseBasicLmtItemName() ;
                    //底层客户大类
                    String basicCusCatalog = lmtSigInvestBasicInfoSub.getBasicCusCatalog() ;
                    String lmtType = CmisLmtConstants.STD_ZB_LMT_TYPE_01 ;
                    if (CmisLmtConstants.STD_ZB_CUS_CATALOG3.equals(basicCusCatalog)) {
                        lmtType = CmisLmtConstants.STD_ZB_LMT_TYPE_07 ;
                    }
                    //组装请求报文
                    CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
                    cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0009ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);//金融机构代码
                    cmisLmt0009ReqDto.setDealBizNo(subSerno);//合同编号
                    cmisLmt0009ReqDto.setCusId(basicCusId);//客户编号
                    cmisLmt0009ReqDto.setCusName(basicCusName);//客户名称
                    cmisLmt0009ReqDto.setDealBizType(CmisCommonConstants.STD_CONT_TYPE_1);//交易业务类型
                    cmisLmt0009ReqDto.setPrdId(useBasicLmtItemNo);//产品编号
                    cmisLmt0009ReqDto.setPrdName(useBasicLmtItemName);//产品名称
                    cmisLmt0009ReqDto.setIsLriskBiz(CommonConstance.STD_ZB_YES_NO_0);//是否低风险
                    cmisLmt0009ReqDto.setIsFollowBiz(CommonConstance.STD_ZB_YES_NO_0);//是否无缝衔接
                    cmisLmt0009ReqDto.setIsBizRev(CommonConstance.STD_ZB_YES_NO_0);//是否合同重签
                    cmisLmt0009ReqDto.setBizAttr(CmisLmtConstants.STD_ZB_BIZ_ATTR_1);//交易属性
                    cmisLmt0009ReqDto.setDealBizAmt(useAmt);//交易业务金额
                    cmisLmt0009ReqDto.setDealBizBailPreRate(BigDecimal.ZERO);//保证金比例
                    cmisLmt0009ReqDto.setDealBizBailPreAmt(BigDecimal.ZERO);//保证金
                    List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
                    CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
                    cmisLmt0009OccRelListReqDto.setLmtType(lmtType);//额度类型
                    cmisLmt0009OccRelListReqDto.setLmtSubNo(subSerno);//额度分项编号
                    cmisLmt0009OccRelListReqDto.setBizTotalAmt(useAmt);//占用总额(折人民币)
                    cmisLmt0009OccRelListReqDto.setBizSpacAmt(useAmt);//占用敞口(折人民币)
                    cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
                    cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);

                    logger.info("根据业务申请编号【" + subSerno + "】前往额度系统-额度校验请求报文：" + JSON.toJSONString(cmisLmt0009ReqDto));
                    ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
                    logger.info("根据业务申请编号【" + subSerno + "】前往额度系统-额度校验返回报文：" + JSON.toJSONString(cmisLmt0009RespDto));

                    String code = cmisLmt0009RespDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(cmisLmt0009RespDto.getData().getErrorMsg()); //接口返回不通过信息
                        return riskResultDto;
                    }
                }
            }
            logger.info("穿透到底层校验处理开始---------------end");
        }
        return null ;
    }
}
