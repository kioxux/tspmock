package cn.com.yusys.yusp.web.server.xdzc0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0013.req.Xdzc0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0013.resp.Xdzc0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0013.Xdzc0013Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:贸易新增接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0013:贸易新增接口")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0013Resource.class);

    @Autowired
    private Xdzc0013Service xdzc0013Service;
    /**
     * 交易码：xdzc0013
     * 交易描述：贸易合同资料上传接口
     *
     * @param xdzc0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贸易新增接口")
    @PostMapping("/xdzc0013")
    protected @ResponseBody
    ResultDto<Xdzc0013DataRespDto> xdzc0013(@Validated @RequestBody Xdzc0013DataReqDto xdzc0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, JSON.toJSONString(xdzc0013DataReqDto));
        Xdzc0013DataRespDto xdzc0013DataRespDto = new Xdzc0013DataRespDto();// 响应Dto:贸易合同资料上传接口
        ResultDto<Xdzc0013DataRespDto> xdzc0013DataResultDto = new ResultDto<>();

        try {
            xdzc0013DataRespDto = xdzc0013Service.xdzc0013Service(xdzc0013DataReqDto);
            xdzc0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0013DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, e.getMessage());
            // 封装xdzc0013DataResultDto中异常返回码和返回信息
            xdzc0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdzc0013DataRespDto到xdzc0013DataResultDto中
        xdzc0013DataResultDto.setData(xdzc0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, JSON.toJSONString(xdzc0013DataResultDto));
        return xdzc0013DataResultDto;
    }
}
