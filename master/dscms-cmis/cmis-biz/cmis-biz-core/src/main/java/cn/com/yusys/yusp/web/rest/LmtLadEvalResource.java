/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtReplyChg;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98RespDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtLadEval;
import cn.com.yusys.yusp.service.LmtLadEvalService;

/**
 * @项目名称: cmis-biz模块
 * @类名称: LmtLadEvalResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-02 08:37:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtladeval")
public class LmtLadEvalResource {
    @Autowired
    private LmtLadEvalService lmtLadEvalService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtLadEval>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtLadEval> list = lmtLadEvalService.selectAll(queryModel);
        return new ResultDto<List<LmtLadEval>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtLadEval>> index(QueryModel queryModel) {
        List<LmtLadEval> list = lmtLadEvalService.selectByModel(queryModel);
        return new ResultDto<List<LmtLadEval>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtLadEval> show(@PathVariable("pkId") String pkId) {
        LmtLadEval lmtLadEval = lmtLadEvalService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtLadEval>(lmtLadEval);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtLadEval> create(@RequestBody LmtLadEval lmtLadEval) throws URISyntaxException {
        lmtLadEvalService.insert(lmtLadEval);
        return new ResultDto<LmtLadEval>(lmtLadEval);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtLadEval lmtLadEval) throws URISyntaxException {
        int result = lmtLadEvalService.update(lmtLadEval);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtLadEvalService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtLadEvalService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：calSingleLmt
     * @方法描述：单一客户限额测算
     * @参数与返回说明：
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-13 下午 4:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/calSingleLmt")
    protected ResultDto<LmtLadEval> calSingleLmt(@RequestBody LmtLadEval lmtLadEval) {
        return ResultDto.success(lmtLadEvalService.calSingleLmt(lmtLadEval));
    }

    /**
     * @方法名称：loanRating
     * @方法描述：债项评级测算
     * @参数与返回说明：新增时债项测算
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-13 下午 4:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/loanRating")
    protected ResultDto<LmtLadEval> loanRating97(@RequestBody LmtLadEval lmtLadEval) throws Exception{
        return ResultDto.success(lmtLadEvalService.loanRating(lmtLadEval));
    }

    /**
     * @方法名称：loanRating98
     * @方法描述：债项评级测算
     * @参数与返回说明：授信变更时债项测算
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-13 下午 4:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/loanRating98")
    protected ResultDto<LmtLadEval> loanRating98(@RequestBody LmtLadEval lmtLadEval) throws Exception {
        return ResultDto.success(lmtLadEvalService.loanRating98(lmtLadEval));
    }

    /**
     * @方法名称：loanRating99
     * @方法描述：债项评级测算
     * @参数与返回说明：合同申请时债项测算
     * @算法描述：
     * @创建人：
     * @创建时间：2021-04-13 下午 4:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/loanRating99")
    protected ResultDto<LmtLadEval> loanRating99(@RequestBody LmtLadEval lmtLadEval) throws Exception {
        return ResultDto.success(lmtLadEvalService.loanRating99(lmtLadEval));
    }

    /**
     * @方法名称：insertSelective
     * @方法描述：新增保存
     * @创建人：zhangming12
     * @创建时间：2021/5/25 9:57
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/insertselective")
    protected ResultDto<Integer> insertSelective(@RequestBody LmtLadEval lmtLadEval){
        int i = lmtLadEvalService.insertSelective(lmtLadEval);
        return new ResultDto<>(i);
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据多授信申请流水号查询客户评级和债项评级
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<List<LmtLadEval>> selectBySerno(@RequestBody QueryModel queryModel){
        String serno = (String)queryModel.getCondition().get("serno");
        List<LmtLadEval> lmtLadEvalList = lmtLadEvalService.selectBySerno(serno);
        return new ResultDto<>(lmtLadEvalList);
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据多授信申请流水号查询客户评级和债项评级
     * @参数与返回说明:
     * @算法描述: 无
     */
    @PostMapping("/selectsinglebyserno")
    protected ResultDto<LmtLadEval> selectSingleBySerno(@RequestBody String serno){
        LmtLadEval lmtLadEval = lmtLadEvalService.selectSingleBySerno(serno);
        return ResultDto.success(lmtLadEval);
    }

    /**
     * 校验是否可以仅增内评低准入数据
     * @param serno
     * @return
     */
    @PostMapping("/checkNpGreenApp")
    protected ResultDto<Map> checkNpGreenApp(@RequestBody String serno){
        return new ResultDto<Map>(lmtLadEvalService.checkNpGreenApp(serno));
    }

    /**
     * 获取当前客户对应的在我行的敞口余额
     * @param map
     * @return
     */
    @PostMapping("/calcurotherbankdebt")
    protected ResultDto<BigDecimal> calCurOtherBankDebt(@RequestBody Map map){
        return new ResultDto<BigDecimal>(lmtLadEvalService.calCurOtherBankDebt(map));
    }

}
