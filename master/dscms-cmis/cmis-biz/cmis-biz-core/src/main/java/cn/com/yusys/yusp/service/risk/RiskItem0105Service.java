package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusCorpDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author lyj
 * @version 1.0.0
 * @date 2021/7/13 15:51
 * @desc 对公客户授信有债项评级测试
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0105Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0105Service.class);

    @Autowired
    private LmtNpGreenAppService lmtNpGreenAppService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private LmtLadEvalService lmtLadEvalService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtAppSubService lmtAppSubService;
    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author lyj
     * @date 2021/8/31 21:20
     * @version 1.0.0
     * @desc 授信申请时,PD*LGD>5.5% 且 授信申请数据不在内评低准入例外审批表中为审批通过状态时进行拦截
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0105(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = "";
        String cusId = "";
        Boolean isAllLowRisk = true;
        Boolean isOnlyWtdk = true;
        log.info("*************对公客户授信有债项评级测试校验开始***********【{}】",serno);
        if (StringUtils.isBlank(queryModel.getCondition().get("bizId").toString())) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        cusId=queryModel.getCondition().get("bizUserId").toString();
        // 如果对公客户的行业分类为 J开头（金融业) 则客户评级直接通过
        CusCorpDto cusCorpDto = iCusClientService.queryCusCropDtoByCusId(cusId).getData();
        if (cusCorpDto != null && !StringUtils.isEmpty(cusCorpDto.getTradeClass()) && cusCorpDto.getTradeClass().startsWith("J")) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); // 通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); // 校验通过
            return riskResultDto;
        }

        serno = queryModel.getCondition().get("bizId").toString();
        if (StringUtils.isBlank(queryModel.getCondition().get("bizUserId").toString())) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0099);
            return riskResultDto;
        }
        List<LmtAppSub> subList = lmtAppSubService.queryLmtAppSubBySerno(serno);
        for(LmtAppSub lmtAppSub : subList){
              if(!CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())){
                  if(!isOnlyWtdk){
                      break;
                  }
                  isAllLowRisk = false;
                  List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                  for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                      if(!"14020301".equals(lmtAppSubPrd.getLmtBizType()) && !"20040101".equals(lmtAppSubPrd.getLmtBizType())){
                          isOnlyWtdk = false;
                          break;
                      }
                  }
              }
        }
        if(isAllLowRisk || isOnlyWtdk){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
            log.info("*************申请下分项全部为低风险/申请分项产品只包含委托贷款***********【{}】",serno);
            return riskResultDto;
        }
        CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
        if(Objects.isNull(cusBaseClientDto)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0108); //获取客户信息失败
            return riskResultDto;
        }

        //获取客户大类(对公客户则进行校验)
        String cusCatalog = cusBaseClientDto.getCusCatalog();
        if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusCatalog)){
            // 查询法人客户的信用评级信息
            ResultDto<Map<String, String>> result = iCusClientService.selectGradeInfoByCusId(cusId);
            if(Objects.isNull(result) || Objects.isNull(result.getData())) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00802); //业务流水号为空
                return riskResultDto;
            }
            Map<String, String> map = result.getData();
            String pd = map.get("pd");
            if(StringUtils.isBlank(pd)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10503); //违约概率PD为空！
                return riskResultDto;
            }
            //获取非零内评违约概率
            BigDecimal PD = new BigDecimal(pd);
            LmtLadEval lmtLadEval = lmtLadEvalService.selectSingleBySerno(serno);
            if(Objects.isNull(lmtLadEval)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10501); //获取债项评级信息失败
                return riskResultDto;
            }
            //获取债项评级LGD违约损失率
            BigDecimal lgd = lmtLadEval.getLgd();
            if(Objects.isNull(lgd)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10504); //违约损失率LGD为空！
                return riskResultDto;
            }
            if((PD.multiply(lgd)).compareTo(new BigDecimal(0.055))<= 0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
                log.info("*************对公客户授信有债项评级测试校验结束,PD*LGD<=5.5%***********【{}】",serno);
                return riskResultDto;
            }
            //若PD * LGD>5.5% 则校验内评低准入审批是否通过
            LmtNpGreenApp lmtNpGreenApp = lmtNpGreenAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(lmtNpGreenApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10502);
                return riskResultDto;
            }
            //获取审批状态
            String approveStatus = lmtNpGreenApp.getApproveStatus();
            if(!CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_10502);
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        log.info("*************对公客户授信有债项评级测试校验结束***********【{}】",serno);
        return riskResultDto;
    }
}
