/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtReconsideDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtRedicussDetail;
import cn.com.yusys.yusp.service.LmtRedicussDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRedicussDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-06-10 00:27:28
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtredicussdetail")
public class LmtRedicussDetailResource {
    @Autowired
    private LmtRedicussDetailService lmtRedicussDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtRedicussDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtRedicussDetail> list = lmtRedicussDetailService.selectAll(queryModel);
        return new ResultDto<List<LmtRedicussDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtRedicussDetail>> index(QueryModel queryModel) {
        List<LmtRedicussDetail> list = lmtRedicussDetailService.selectByModel(queryModel);
        return new ResultDto<List<LmtRedicussDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtRedicussDetail> show(@PathVariable("pkId") String pkId) {
        LmtRedicussDetail lmtRedicussDetail = lmtRedicussDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtRedicussDetail>(lmtRedicussDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtRedicussDetail> create(@RequestBody LmtRedicussDetail lmtRedicussDetail) throws URISyntaxException {
        lmtRedicussDetailService.insert(lmtRedicussDetail);
        return new ResultDto<LmtRedicussDetail>(lmtRedicussDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtRedicussDetail lmtRedicussDetail) throws URISyntaxException {
        int result = lmtRedicussDetailService.update(lmtRedicussDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtRedicussDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtRedicussDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:save
     * @函数描述:保存
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<LmtRedicussDetail> save(@RequestBody LmtRedicussDetail lmtRedicussDetail) throws URISyntaxException {
        LmtRedicussDetail lmtRedicussDetail1 = lmtRedicussDetailService.selectByLmtSerno(lmtRedicussDetail.getLmtSerno());
        lmtRedicussDetail.setOprType("01");
        if(lmtRedicussDetail1!=null){
            lmtRedicussDetailService.update(lmtRedicussDetail);
        }else{
            lmtRedicussDetailService.insert(lmtRedicussDetail);
        }
        return ResultDto.success(lmtRedicussDetail);
    }


    /**
     * @函数名称:show
     * @函数描述:进入页面查询数据返回前台
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtRedicussDetail> selectBySerno(@RequestBody String lmtSerno) {
        LmtRedicussDetail lmtReconsideDetail = lmtRedicussDetailService.selectByLmtSerno(lmtSerno);
        return  ResultDto.success(lmtReconsideDetail);
    }

    /**
     * @函数名称:queryDetailByLmtSerno
     * @函数描述:根据lmtSerno查询分项数据
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryDetailByLmtSerno")
    protected ResultDto<List<Map<String,String>>> queryDetailByLmtSerno(@RequestBody Map<String,String> map) {
        List<Map<String,String>> lmtRedicussDetails = lmtRedicussDetailService.queryDetailByLmtSerno(map.get("lmtSerno"));
        return  ResultDto.success(lmtRedicussDetails);
    }
}
