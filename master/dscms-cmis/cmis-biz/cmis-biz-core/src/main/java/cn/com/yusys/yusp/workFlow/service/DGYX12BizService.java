package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.SxkdLoanRateChange;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author zhangliang15
 * @version 1.0.0
 * @date
 * @desc 省心快贷提款利率修改流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DGYX12BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX12BizService.class);//定义log

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private SxkdLoanRateChangeService sxkdLoanRateChangeService;
    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 省心快贷利率修改
        if ("YX020".equals(bizType)){
            sxkdLoanRateChangeBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }
        else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param instanceInfo, currentOpType, iqpSerno, currentUserId, currentOrgId
     * @return void
     * @author zhangliang15
     * @date 2021/8/26 21:38
     * @version 1.0.0
     * @desc 省心快贷利率修改
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void sxkdLoanRateChangeBizApp(ResultInstanceDto instanceInfo, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        try {
            SxkdLoanRateChange sxkdLoanRateChange = sxkdLoanRateChangeService.selectByPrimaryKey(serno);
            log.info("省心快贷利率修改:"+serno+"流程操作:"+currentOpType+"业务处理");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("省心快贷利率修改:"+serno+"流程流转,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 111--审批中
                updateApproveStatus(sxkdLoanRateChange, CmisCommonConstants.WF_STATUS_111);
            }else if(OpType.RETURN_BACK.equals(currentOpType)){//流程退回
                log.info("省心快贷利率修改:"+serno+"流程退回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(sxkdLoanRateChange, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.CALL_BACK.equals(currentOpType)){//流程打回
                log.info("省心快贷利率修改:"+serno+"流程打回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(sxkdLoanRateChange, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.TACK_BACK.equals(currentOpType)){//流程拿回
                log.info("省心快贷利率修改:"+serno+"流程拿回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(sxkdLoanRateChange, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.TACK_BACK_FIRST.equals(currentOpType)){//流程拿回到初始节点
                log.info("省心快贷利率修改:"+serno+"流程拿回到初始节点,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(sxkdLoanRateChange, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.END.equals(currentOpType)){//流程审批通过
                log.info("省心快贷利率修改:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 997--通过
                updateApproveStatus(sxkdLoanRateChange, CmisCommonConstants.WF_STATUS_997);
                //审批通过业务处理
                String contNo = "";
                contNo = sxkdLoanRateChange.getContNo();
                CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
                // 获取营业日期
                String openday = stringRedisTemplate.opsForValue().get("openDay");
                ctrLoanCont.setIrAdjustType(sxkdLoanRateChange.getIrAdjustType()); //利率调整方式
                ctrLoanCont.setLprRateIntval(sxkdLoanRateChange.getLprPriceInterval());//LPR定价区间
                ctrLoanCont.setCurtLprRate(sxkdLoanRateChange.getCurtLprRate());//当前lpr利率
                ctrLoanCont.setRateFloatPoint(sxkdLoanRateChange.getLprBp());//浮动点数(BP)
                // 利率调整方式01固定利率 02浮动利率 固定利率显示执行年利率,浮动利率不展示
                if(sxkdLoanRateChange.getIrAdjustType().equals("01")){
                    ctrLoanCont.setExecRateYear(sxkdLoanRateChange.getExecRateYear());//执行年利率
                }
                ctrLoanCont.setUpdDate(openday);//修改时间
                ctrLoanContService.update(ctrLoanCont);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("省心快贷利率修改:"+serno+"流程否决，参数："+ instanceInfo.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                updateApproveStatus(sxkdLoanRateChange, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("省心快贷利率修改:"+serno+"未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }

    }

    /***
     * 流程审批状态更新
     * 申请信息
     * approveStatus 审批状态
     * */
    public void updateApproveStatus (SxkdLoanRateChange sxkdLoanRateChange, String approveStatus){
        sxkdLoanRateChange.setApproveStatus(approveStatus);
        sxkdLoanRateChangeService.updateSelective(sxkdLoanRateChange);
    }


    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.DGYX12).equals(flowCode);
    }
}
