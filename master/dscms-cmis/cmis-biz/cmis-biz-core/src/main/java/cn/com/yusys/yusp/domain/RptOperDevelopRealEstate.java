/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperDevelopRealEstate
 * @类描述: rpt_oper_develop_real_estate数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-17 14:16:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_oper_develop_real_estate")
public class RptOperDevelopRealEstate extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 项目性质 **/
	@Column(name = "PRO_CHA", unique = false, nullable = true, length = 40)
	private String proCha;
	
	/** 项目名称 **/
	@Column(name = "PRO_NAME", unique = false, nullable = true, length = 200)
	private String proName;
	
	/** 投资比例 **/
	@Column(name = "INVEST_SCALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal investScale;
	
	/** 开工日期 **/
	@Column(name = "START_WORK_DATE", unique = false, nullable = true, length = 10)
	private String startWorkDate;
	
	/** 竣工日期 **/
	@Column(name = "END_WORK_DATE", unique = false, nullable = true, length = 10)
	private String endWorkDate;
	
	/** 占地面积 **/
	@Column(name = "COVER_AREA", unique = false, nullable = true, length = 20)
	private String coverArea;
	
	/** 建筑面积 **/
	@Column(name = "BUILD_AREA", unique = false, nullable = true, length = 20)
	private String buildArea;
	
	/** 已售面积 **/
	@Column(name = "SOLD_AREA", unique = false, nullable = true, length = 20)
	private String soldArea;
	
	/** 未售面积 **/
	@Column(name = "UNSOLD_AREA", unique = false, nullable = true, length = 20)
	private String unsoldArea;
	
	/** 开发总成本 **/
	@Column(name = "TOTAL_COST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalCost;
	
	/** 已实现销售 **/
	@Column(name = "REAL_SALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realSale;
	
	/** 未实现销售 **/
	@Column(name = "UNREAL_SALE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal unrealSale;
	
	/** 面积 **/
	@Column(name = "AREA", unique = false, nullable = true, length = 20)
	private String area;
	
	/** 性质 **/
	@Column(name = "CHA", unique = false, nullable = true, length = 20)
	private String cha;
	
	/** 预计开发时间 **/
	@Column(name = "PLAN_STAR_DATE", unique = false, nullable = true, length = 10)
	private String planStarDate;
	
	/** 是否质押 **/
	@Column(name = "PLEDGE_IND", unique = false, nullable = true, length = 50)
	private String pledgeInd;
	
	/** 抵押行 **/
	@Column(name = "PLEDGE_BANK", unique = false, nullable = true, length = 40)
	private String pledgeBank;
	
	/** 土地费交纳应付 **/
	@Column(name = "LAND_FEE_PAYABLE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal landFeePayable;
	
	/** 土地费交纳未付 **/
	@Column(name = "LAND_FEE_NO_PAY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal landFeeNoPay;
	
	/** 预计总投资 **/
	@Column(name = "ESTIMATE_INVEST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal estimateInvest;
	
	/** 自有资金 **/
	@Column(name = "SELF_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal selfAmt;
	
	/** 银行融资 **/
	@Column(name = "BANK_FINANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bankFinance;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param proCha
	 */
	public void setProCha(String proCha) {
		this.proCha = proCha;
	}
	
    /**
     * @return proCha
     */
	public String getProCha() {
		return this.proCha;
	}
	
	/**
	 * @param proName
	 */
	public void setProName(String proName) {
		this.proName = proName;
	}
	
    /**
     * @return proName
     */
	public String getProName() {
		return this.proName;
	}
	
	/**
	 * @param investScale
	 */
	public void setInvestScale(java.math.BigDecimal investScale) {
		this.investScale = investScale;
	}
	
    /**
     * @return investScale
     */
	public java.math.BigDecimal getInvestScale() {
		return this.investScale;
	}
	
	/**
	 * @param startWorkDate
	 */
	public void setStartWorkDate(String startWorkDate) {
		this.startWorkDate = startWorkDate;
	}
	
    /**
     * @return startWorkDate
     */
	public String getStartWorkDate() {
		return this.startWorkDate;
	}
	
	/**
	 * @param endWorkDate
	 */
	public void setEndWorkDate(String endWorkDate) {
		this.endWorkDate = endWorkDate;
	}
	
    /**
     * @return endWorkDate
     */
	public String getEndWorkDate() {
		return this.endWorkDate;
	}
	
	/**
	 * @param coverArea
	 */
	public void setCoverArea(String coverArea) {
		this.coverArea = coverArea;
	}
	
    /**
     * @return coverArea
     */
	public String getCoverArea() {
		return this.coverArea;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(String buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return buildArea
     */
	public String getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param soldArea
	 */
	public void setSoldArea(String soldArea) {
		this.soldArea = soldArea;
	}
	
    /**
     * @return soldArea
     */
	public String getSoldArea() {
		return this.soldArea;
	}
	
	/**
	 * @param unsoldArea
	 */
	public void setUnsoldArea(String unsoldArea) {
		this.unsoldArea = unsoldArea;
	}
	
    /**
     * @return unsoldArea
     */
	public String getUnsoldArea() {
		return this.unsoldArea;
	}
	
	/**
	 * @param totalCost
	 */
	public void setTotalCost(java.math.BigDecimal totalCost) {
		this.totalCost = totalCost;
	}
	
    /**
     * @return totalCost
     */
	public java.math.BigDecimal getTotalCost() {
		return this.totalCost;
	}
	
	/**
	 * @param realSale
	 */
	public void setRealSale(java.math.BigDecimal realSale) {
		this.realSale = realSale;
	}
	
    /**
     * @return realSale
     */
	public java.math.BigDecimal getRealSale() {
		return this.realSale;
	}
	
	/**
	 * @param unrealSale
	 */
	public void setUnrealSale(java.math.BigDecimal unrealSale) {
		this.unrealSale = unrealSale;
	}
	
    /**
     * @return unrealSale
     */
	public java.math.BigDecimal getUnrealSale() {
		return this.unrealSale;
	}
	
	/**
	 * @param area
	 */
	public void setArea(String area) {
		this.area = area;
	}
	
    /**
     * @return area
     */
	public String getArea() {
		return this.area;
	}
	
	/**
	 * @param cha
	 */
	public void setCha(String cha) {
		this.cha = cha;
	}
	
    /**
     * @return cha
     */
	public String getCha() {
		return this.cha;
	}
	
	/**
	 * @param planStarDate
	 */
	public void setPlanStarDate(String planStarDate) {
		this.planStarDate = planStarDate;
	}
	
    /**
     * @return planStarDate
     */
	public String getPlanStarDate() {
		return this.planStarDate;
	}
	
	/**
	 * @param pledgeInd
	 */
	public void setPledgeInd(String pledgeInd) {
		this.pledgeInd = pledgeInd;
	}
	
    /**
     * @return pledgeInd
     */
	public String getPledgeInd() {
		return this.pledgeInd;
	}
	
	/**
	 * @param pledgeBank
	 */
	public void setPledgeBank(String pledgeBank) {
		this.pledgeBank = pledgeBank;
	}
	
    /**
     * @return pledgeBank
     */
	public String getPledgeBank() {
		return this.pledgeBank;
	}
	
	/**
	 * @param landFeePayable
	 */
	public void setLandFeePayable(java.math.BigDecimal landFeePayable) {
		this.landFeePayable = landFeePayable;
	}
	
    /**
     * @return landFeePayable
     */
	public java.math.BigDecimal getLandFeePayable() {
		return this.landFeePayable;
	}
	
	/**
	 * @param landFeeNoPay
	 */
	public void setLandFeeNoPay(java.math.BigDecimal landFeeNoPay) {
		this.landFeeNoPay = landFeeNoPay;
	}
	
    /**
     * @return landFeeNoPay
     */
	public java.math.BigDecimal getLandFeeNoPay() {
		return this.landFeeNoPay;
	}
	
	/**
	 * @param estimateInvest
	 */
	public void setEstimateInvest(java.math.BigDecimal estimateInvest) {
		this.estimateInvest = estimateInvest;
	}
	
    /**
     * @return estimateInvest
     */
	public java.math.BigDecimal getEstimateInvest() {
		return this.estimateInvest;
	}
	
	/**
	 * @param selfAmt
	 */
	public void setSelfAmt(java.math.BigDecimal selfAmt) {
		this.selfAmt = selfAmt;
	}
	
    /**
     * @return selfAmt
     */
	public java.math.BigDecimal getSelfAmt() {
		return this.selfAmt;
	}
	
	/**
	 * @param bankFinance
	 */
	public void setBankFinance(java.math.BigDecimal bankFinance) {
		this.bankFinance = bankFinance;
	}
	
    /**
     * @return bankFinance
     */
	public java.math.BigDecimal getBankFinance() {
		return this.bankFinance;
	}


}