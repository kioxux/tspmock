/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.GuarContRelWarrant;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.GuarWarrantInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarWarrantInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 17:13:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GuarWarrantInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    GuarWarrantInfo selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<GuarWarrantInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: selectWarrantInInfoByModel
     * @方法描述: 条件列表查询(适用于权证出库选择权证)
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<GuarWarrantInfo> selectWarrantInInfoByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(GuarWarrantInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(GuarWarrantInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(GuarWarrantInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(GuarWarrantInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据权证编号查询权证信息
     * @param warrantNo
     * @return
     */
    GuarWarrantInfo queryByWarrantNo(@Param("warrantNo") String warrantNo);

    /**
     * 根据权证编号查询权证信息
     * @param warrantNo
     * @return
     */
    GuarWarrantInfo selectByWarrantNo(@Param("warrantNo") String warrantNo);

    /**
     * 根据核心担保编号查询
     * @param coreGuarantyNo
     * @return
     */
    GuarWarrantInfo selectByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);


    /**
     * @方法名称: updateManagerIdByCuarNo
     * @方法描述: 根据押品编号修改管户人管户机构
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateToManagerIdByCuarNo(Map QueryMap);

    /**
     * @方法名称: queryCountFromGuarWarrntInfoByGuarrNo
     * @方法描述: 根据押品编号查询是否存在满足条件的权证记录
     * @参数与返回说明:
     * @算法描述: 无
     */
    int queryCountFromGuarWarrntInfoByGuarrNo(Map QueryMap);

    /**
     * 根据核心担保编号查询押品编号
     * @param collid
     * @return
     */
    String getGuarNoByCoreGrtNo(@Param("collid")String collid);

    /**
     * 根据核心担保编号查询押品权证号
     * @param collid
     * @return
     */
    String getCreditRecordIdByCoreGuarNo(@Param("collid")String collid);

    /**
     * @param contNo
     * @return int
     * @author hubp
     * @date 2021/8/14 17:22
     * @version 1.0.0
     * @desc    根据合同编号查询合同项下押品是否入库
     * @修改历史: 修改时间    修改人员    修改原因
     */
    int getInfoByContNo(@Param("contNo")String contNo);

    /**
     * 根据合同编号查询合同项下入库已记账的权证数
     * @param contNo
     * @return
     */
    int countWarrantByContNo(@Param("contNo")String contNo);

    /**
     *
     * @return
     */
    int countWarrantInRecordsByGuarNoAndGuarContNo(QueryModel queryModel);

    /**
     * @param coreGrtNo
     * @return
     */
    String xddb0019(@Param("coreGrtNo")String coreGrtNo);

    /**
     * 根据核心担保编号物理删除记录
     * @param coreGuarantyNo
     * @return
     */
    int deleteByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据核心担保编号逻辑删除记录
     * @param coreGuarantyNo
     * @return
     */
    int deleteOnLogicByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据核心担保编号查询权证是否电子权证
     * @param coreGuarantyNo
     * @return
     */
    String selectIsEWarrantByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据担保合同编号和押品编号查询最新的权证信息
     * @param model
     * @return
     */
    GuarWarrantInfo selectByGuarContNoAndGuarNo(QueryModel model);

    /**
     * @方法名称: selectAccommoDation
     * @方法描述: 查询是否存在住房按揭抵押
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectAccommoDation(@Param("coreGrtNo") String coreGrtNo);

    /**
     * @方法名称: updateWarrantStateByCoreGrtNo
     * @方法描述: 根据押品编号修改权证出入库状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateWarrantStateByCoreGrtNo(Map QueryMap);

    /**
     * @方法名称: selectIsExistByCoreGuarantyNo
     * @方法描述: 查询是否存在指定的核心担保编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectIsExistByCoreGuarantyNo(@Param("coreGrtNo") String coreGrtNo);

    /**
     * 查询入库在途、入库未记账、入库已记账的权证记录数
     * @param coreGrtNo
     * @return
     */
    int countWarrantInStateByCoreGuarantyNo(@Param("coreGrtNo") String coreGrtNo);

    /**
     * 根据核心担保编号列表查询权证信息
     * @param ypbhs
     * @return
     */
    List<GuarWarrantInfo> selectByCoreGuarantyNos(@Param("ypbhs") List<String> ypbhs);

    /**
     * 根据核心押品编号查询最新的权证信息
     * @param coreGuarNoList
     * @return
     */
    GuarWarrantInfo selectByCoreGuarNos(@Param("list") List<String> coreGuarNoList);

}