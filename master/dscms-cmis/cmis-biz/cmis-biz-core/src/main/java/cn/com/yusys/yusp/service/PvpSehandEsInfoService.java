/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.ToppAcctSub;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.req.ClzjcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.resp.ClzjcxRespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.ToppAcctSubMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpSehandEsInfo;
import cn.com.yusys.yusp.repository.mapper.PvpSehandEsInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpSehandEsInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-09-01 22:15:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class PvpSehandEsInfoService {
    private static final Logger log = LoggerFactory.getLogger(PvpLoanAppService.class);

    @Autowired
    private PvpSehandEsInfoMapper pvpSehandEsInfoMapper;


    @Autowired
    private Dscms2ZjywxtClientService dscms2ZjywxtClientService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;


    @Autowired
    private ToppAcctSubMapper toppAcctSubMapper;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public PvpSehandEsInfo selectByPrimaryKey(String pvpSerno) {
        return pvpSehandEsInfoMapper.selectByPrimaryKey(pvpSerno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<PvpSehandEsInfo> selectAll(QueryModel model) {
        List<PvpSehandEsInfo> records = (List<PvpSehandEsInfo>) pvpSehandEsInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<PvpSehandEsInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<PvpSehandEsInfo> list = pvpSehandEsInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(PvpSehandEsInfo record) {
        return pvpSehandEsInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(PvpSehandEsInfo record) {
        return pvpSehandEsInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(PvpSehandEsInfo record) {
        return pvpSehandEsInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(PvpSehandEsInfo record) {
        return pvpSehandEsInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pvpSerno) {
        return pvpSehandEsInfoMapper.deleteByPrimaryKey(pvpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return pvpSehandEsInfoMapper.deleteByIds(ids);
    }



    /**
     * @方法名称: updatePvpSehandEsInfo
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updatePvpSehandEsInfo(PvpSehandEsInfo pvpSehandEsInfo) {
        PvpSehandEsInfo pvpSehandEsInfoTemp =  pvpSehandEsInfoMapper.selectByPrimaryKey(pvpSehandEsInfo.getPvpSerno());
        pvpSehandEsInfoTemp.setTgxyNo(pvpSehandEsInfo.getTgxyNo());
        return pvpSehandEsInfoMapper.updateByPrimaryKeySelective(pvpSehandEsInfoTemp);
    }


    /**
     * @方法名称: queryClzjcx
     * @方法描述: 查询连云港存量房资金托管监管协议信息
     * @参数与返回说明:
     * @创建人 shenli
     * @算法描述: 无
     */
    public PvpSehandEsInfo queryClzjcx(PvpSehandEsInfo pvpSehandEsInfo) {
        log.error("查询连云港存量房资金托管监管协议开始 "+pvpSehandEsInfo.getPvpSerno());
        PvpSehandEsInfo pvpSehandEsInfoTemp = null;

        try{
            ClzjcxReqDto clzjcxReqDto = new ClzjcxReqDto();
            clzjcxReqDto.setSavvou(pvpSehandEsInfo.getTgxyNo());

            log.error("查询连云港存量房资金托管监管协议请求报文 "+clzjcxReqDto.toString());
            ResultDto<ClzjcxRespDto> clzjcxRespDto =  dscms2ZjywxtClientService.clzjcx(clzjcxReqDto);
            log.error("查询连云港存量房资金托管监管协议返回报文 "+clzjcxRespDto.getData().toString());

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, clzjcxRespDto.getCode())) {

                //监管账号
                String acctno = clzjcxRespDto.getData().getAcctno();
                //账号名称
                String acctna = clzjcxRespDto.getData().getAcctna();
                //买方姓名
                String byidna = clzjcxRespDto.getData().getByidna();
                //买方证件号码
                String byidno = clzjcxRespDto.getData().getByidno();
                //合同编号
                String contno = clzjcxRespDto.getData().getContno();

                PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSehandEsInfo.getPvpSerno());

                //判断买方姓名是否与申请客户姓名一致
                if(!byidna.equals(pvpLoanApp.getCusName())){
                    log.info("买方姓名 "+byidna+" 与出账申请客户姓名 "+pvpLoanApp.getCusName() +" 不一致！");
                    throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "买方姓名 "+byidna+" 与出账申请客户姓名 "+pvpLoanApp.getCusName() +" 不一致！");
                }else{
                    pvpSehandEsInfoTemp = this.selectByPrimaryKey(pvpSehandEsInfo.getPvpSerno());
                    pvpSehandEsInfoTemp.setTgxyNo(pvpSehandEsInfo.getTgxyNo());
                    pvpSehandEsInfoTemp.setBuyerName(byidna);
                    pvpSehandEsInfoTemp.setAcctNo(acctno);
                    pvpSehandEsInfoTemp.setAcctName(acctna);
                    pvpSehandEsInfoTemp.setBuyerCertCode(byidno);
                    pvpSehandEsInfoTemp.setContNo(contno);

                    if(this.updatePvpSehandEsInfo(pvpSehandEsInfoTemp) == 0){
                        throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, "个人二手房资金托管信息表更新失败！");
                    }

                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("bizSerno", pvpSehandEsInfo.getPvpSerno());
                    List<ToppAcctSub> toppAcctSubList = toppAcctSubMapper.selectByModel(queryModel);
                    //先删除受托支付信息
                    if (toppAcctSubList != null && toppAcctSubList.size() > 0) {
                        for (int j = 0; j < toppAcctSubList.size(); j++) {
                            ToppAcctSub toppAcctSub = toppAcctSubList.get(j);
                            toppAcctSubMapper.deleteByPrimaryKey(toppAcctSub.getPkId());
                        }
                    }

                    //插入一个固定的托管账号
                    ToppAcctSub toppAcctSub = new ToppAcctSub();
                    toppAcctSub.setBizSerno(pvpLoanApp.getPvpSerno());
                    toppAcctSub.setToppAcctNo(acctno);
                    toppAcctSub.setToppName(acctna);
                    toppAcctSub.setToppAmt(pvpLoanApp.getPvpAmt());
                    toppAcctSub.setIsBankAcct("1");
                    toppAcctSub.setBizSence("3");
                    toppAcctSub.setIsOnline("1");
                    toppAcctSub.setOprType("01");
                    toppAcctSub.setPkId(StringUtils.uuid(true));
                    toppAcctSubMapper.insert(toppAcctSub);
                }
            }
        }catch (Exception e) {
            log.error("查询连云港存量房资金托管监管协议信息失败：", e);
            throw BizException.error(null, EcbEnum.IQP_EXCEPTION_DEF.key, e.getMessage());
        }
        log.error("查询连云港存量房资金托管监管协议结束 "+pvpSehandEsInfo.getPvpSerno());
        return pvpSehandEsInfoTemp;
    }


}
