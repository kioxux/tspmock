package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.domain.CentralFileInfo;
import cn.com.yusys.yusp.domain.CentralFileTask;

/**
 * 档案任务池，前后端交互
 */
public class CentralFileVo {
    private CentralFileInfo centralFileInfo;

    private CentralFileTask centralFileTask;

    public CentralFileInfo getCentralFileInfo() {
        return centralFileInfo;
    }

    public void setCentralFileInfo(CentralFileInfo centralFileInfo) {
        this.centralFileInfo = centralFileInfo;
    }

    public CentralFileTask getCentralFileTask() {
        return centralFileTask;
    }

    public void setCentralFileTask(CentralFileTask centralFileTask) {
        this.centralFileTask = centralFileTask;
    }
}
