package cn.com.yusys.yusp.web.server.xdcz0013;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0013.req.Xdcz0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0013.resp.Xdcz0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0013.Xdcz0013Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:出账记录详情查看
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0013:出账记录详情查看")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0013.BizXdcz0013Resource.class);

    @Autowired
    private Xdcz0013Service xdcz0013Service;

    /**
     * 交易码：xdcz0013
     * 交易描述：出账记录详情查看
     *
     * @param xdcz0013DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("出账记录详情查看")
    @PostMapping("/xdcz0013")
    protected @ResponseBody
    ResultDto<Xdcz0013DataRespDto> xdcz0013(@Validated @RequestBody Xdcz0013DataReqDto xdcz0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013DataReqDto));
        Xdcz0013DataRespDto xdcz0013DataRespDto = new Xdcz0013DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0013DataRespDto> xdcz0013DataResultDto = new ResultDto<>();
        try {
            // 从xdcz0013DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013DataReqDto));
            xdcz0013DataRespDto = xdcz0013Service.checkLmt(xdcz0013DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013DataRespDto));
            // 封装xdcz0013DataResultDto中正确的返回码和返回信息
            xdcz0013DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0013DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, e.getMessage());
            // 封装xdcz0013DataResultDto中异常返回码和返回信息
            xdcz0013DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0013DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0013DataRespDto到xdcz0013DataResultDto中
        xdcz0013DataResultDto.setData(xdcz0013DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013DataRespDto));
        return xdcz0013DataResultDto;
    }
}
