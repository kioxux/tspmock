package cn.com.yusys.yusp.constant;

/**
 * @author 王玉坤
 * @date 2021/4/17 15:48
 * @version 1.0.0
 * @desc 小微授信调查枚举类
 * @修改历史:
 */
public enum LmtSurveyEnums {
    /** 调查报告类型 **/
    SURVEY_TYPE_01("01", "优企贷调查表"),
    SURVEY_TYPE_02("02", "优农贷调查表"),
    SURVEY_TYPE_03("03", "增享贷调查表"),
    SURVEY_TYPE_04("04", "惠享贷调查表"),
    SURVEY_TYPE_05("05", "无还本续贷调查表（普转）"),
    SURVEY_TYPE_06("06", "无还本续贷调查表（优转）"),
    SURVEY_TYPE_07("07", "优抵贷-税务勘验表"),
    SURVEY_TYPE_08("08", "优享贷调查表"),
    SURVEY_TYPE_09("09", "优抵贷-尽职调查表"),

    /** 分配状态 **/
    DIVIS_STATUS_100("100", "已分配"),
    DIVIS_STATUS_101("101", "未分配"),
    DIVIS_STATUS_110("110", "重新分配"),

    /** 模型审批状态 **/
    STD_XD_JD_MODEL_STATUS_000("000","待发起"),
    STD_XD_JD_MODEL_STATUS_001("001","失效"),
    STD_XD_JD_MODEL_STATUS_002("002","拒绝"),
    STD_XD_JD_MODEL_STATUS_003("003","通过"),
    STD_XD_JD_MODEL_STATUS_004("004","审批中"),
    STD_XD_JD_MODEL_STATUS_007("007","异常"),

    /** 续贷名单状态 **/
    STD_XD_LIST_STATUS_000("000", "待发起"),
    STD_XD_LIST_STATUS_001("001", "模型通过"),
    STD_XD_LIST_STATUS_002("002", "模型拒绝"),
    STD_XD_LIST_STATUS_997("997", "审批通过"),
    STD_XD_LIST_STATUS_140("140", "审批拒绝"),
    STD_XD_LIST_STATUS_200("200", "失效"),

    /** 产品枚举 **/
    PRD_ID_SC010008("SC010008", "优企贷"),
    PRD_ID_SC020010("SC020010", "优农贷"),
    PRD_ID_SC020009("SC020009", "优抵贷"),
    PRD_ID_SC010014("SC010014", "增享贷"),
    PRD_ID_SC060001("SC060001", "惠享贷"),
    PRD_ID_SC010010("SC010010", "房抵循环贷"),
    PRD_ID_PW010004("PW010004", "优享贷");



    private String value;
    private String desc;

    LmtSurveyEnums(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public String getDesc() {
        return desc;
    }
}
