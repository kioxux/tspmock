package cn.com.yusys.yusp.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: OtherCnyRateApprSubRequestDto
 * @类描述: OtherCnyRateApprSubRequestDto数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-10-17 14:27:17
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class OtherCnyRateApprSubRequestDto implements Serializable{
	private static final long serialVersionUID = 1L;

	/** 分项流水号 **/
	private String subSerno;
	/** 申请流水号 **/
	private String serno;

	/** 所属年份 **/
	private String belgYear;

	/** 是否我行 **/
	private String isSelfBank;

	/** 他行名称 **/
	private String otherBankName;

	/** 是否关联客户 **/
	private String isRelCus;

	/** 关联客户类型 **/
	private String relCusType;

	/** 关联客户编号 **/
	private String relCusId;

	/** 关联客户名称 **/
	private String relCusName;

	/** 上期客户评级 **/
	private String lastCusCrdGrade;

	/** 本期客户评级 **/
	private String curCusCrdGrade;

	/** 分项品种流水号 **/
	private String subPrdSerno;

	/** 额度分项编号 **/
	private String accSubPrdNo;

	/** 授信品种编号 **/
	private String lmtBizType;

	/** 授信品种名称 **/
	private String lmtBizTypeName;

	/** 贷款期限 **/
	private Integer loanTerm;

	/** 担保方式 **/
	private String guarType;

	/** 贷款金额 **/
	private java.math.BigDecimal loanAmt;

	/** 执行利率(年) **/
	private java.math.BigDecimal execRateYear;

	/** 定价方式 **/
	private String priceMode;

	/** LPR利率区间 **/
	private String lprRateIntval;

	/** LPR基准日期 **/
	private String rateIntvalDate;

	/** 浮动点数 **/
	private java.math.BigDecimal rateFloatPoint;

	/** 当前LPR值 **/
	private java.math.BigDecimal curLpr;

	/** 利率调整选项 **/
	private String rateAdjustCycle;

	/** 下一次利率调整间隔 **/
	private Integer nextRateAdjInterval;

	/** 第一次调整日 **/
	private String firstAdjDate;

	/** 保证人编号 **/
	private String guarntrNo;

	/** 对外担保总额 **/
	private java.math.BigDecimal outguarTotalAmt;

	/** 其中为本行担保额 **/
	private java.math.BigDecimal orgGuarAmt;

	/** 状态 **/
	private String status;

	/** 操作类型 **/
	private String oprType;

	/** 登记人 **/
	private String inputId;

	/** 登记机构 **/
	private String inputBrId;

	/** 登记日期 **/
	private String inputDate;

	/** 最近修改人 **/
	private String updId;

	/** 最近修改机构 **/
	private String updBrId;

	/** 最近修改日期 **/
	private String updDate;

	/** 创建时间 **/
	private java.util.Date createTime;

	/** 修改时间 **/
	private java.util.Date updateTime;

	/** 审批流水号 **/
	private String approveSerno;

	/** 批准贷款利率 **/
	private java.math.BigDecimal apprLoanRate;

	/** 批准定价方式 **/
	private String apprRateType;

	/** LPR利率 **/
	private java.math.BigDecimal lprRate;

	/** 利率调整周期 **/
	private String rateAdjustCycleApprSub;

	/** 固定日期 **/
	private String fixedDate;

	public String getApproveSerno() {
		return approveSerno;
	}

	public void setApproveSerno(String approveSerno) {
		this.approveSerno = approveSerno;
	}

	public BigDecimal getApprLoanRate() {
		return apprLoanRate;
	}

	public void setApprLoanRate(BigDecimal apprLoanRate) {
		this.apprLoanRate = apprLoanRate;
	}

	public String getApprRateType() {
		return apprRateType;
	}

	public void setApprRateType(String apprRateType) {
		this.apprRateType = apprRateType;
	}

	public BigDecimal getLprRate() {
		return lprRate;
	}

	public void setLprRate(BigDecimal lprRate) {
		this.lprRate = lprRate;
	}

	public String getRateAdjustCycleApprSub() {
		return rateAdjustCycleApprSub;
	}

	public void setRateAdjustCycleApprSub(String rateAdjustCycleApprSub) {
		this.rateAdjustCycleApprSub = rateAdjustCycleApprSub;
	}

	public String getFixedDate() {
		return fixedDate;
	}

	public void setFixedDate(String fixedDate) {
		this.fixedDate = fixedDate;
	}

	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno == null ? null : subSerno.trim();
	}

	/**
	 * @return SubSerno
	 */
	public String getSubSerno() {
		return this.subSerno;
	}

	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}

	/**
	 * @return Serno
	 */
	public String getSerno() {
		return this.serno;
	}

	/**
	 * @param belgYear
	 */
	public void setBelgYear(String belgYear) {
		this.belgYear = belgYear == null ? null : belgYear.trim();
	}

	/**
	 * @return BelgYear
	 */
	public String getBelgYear() {
		return this.belgYear;
	}

	/**
	 * @param isSelfBank
	 */
	public void setIsSelfBank(String isSelfBank) {
		this.isSelfBank = isSelfBank == null ? null : isSelfBank.trim();
	}

	/**
	 * @return IsSelfBank
	 */
	public String getIsSelfBank() {
		return this.isSelfBank;
	}

	/**
	 * @param otherBankName
	 */
	public void setOtherBankName(String otherBankName) {
		this.otherBankName = otherBankName == null ? null : otherBankName.trim();
	}

	/**
	 * @return OtherBankName
	 */
	public String getOtherBankName() {
		return this.otherBankName;
	}

	/**
	 * @param isRelCus
	 */
	public void setIsRelCus(String isRelCus) {
		this.isRelCus = isRelCus == null ? null : isRelCus.trim();
	}

	/**
	 * @return IsRelCus
	 */
	public String getIsRelCus() {
		return this.isRelCus;
	}

	/**
	 * @param relCusType
	 */
	public void setRelCusType(String relCusType) {
		this.relCusType = relCusType == null ? null : relCusType.trim();
	}

	/**
	 * @return RelCusType
	 */
	public String getRelCusType() {
		return this.relCusType;
	}

	/**
	 * @param relCusId
	 */
	public void setRelCusId(String relCusId) {
		this.relCusId = relCusId == null ? null : relCusId.trim();
	}

	/**
	 * @return RelCusId
	 */
	public String getRelCusId() {
		return this.relCusId;
	}

	/**
	 * @param relCusName
	 */
	public void setRelCusName(String relCusName) {
		this.relCusName = relCusName == null ? null : relCusName.trim();
	}

	/**
	 * @return RelCusName
	 */
	public String getRelCusName() {
		return this.relCusName;
	}

	/**
	 * @param lastCusCrdGrade
	 */
	public void setLastCusCrdGrade(String lastCusCrdGrade) {
		this.lastCusCrdGrade = lastCusCrdGrade == null ? null : lastCusCrdGrade.trim();
	}

	/**
	 * @return LastCusCrdGrade
	 */
	public String getLastCusCrdGrade() {
		return this.lastCusCrdGrade;
	}

	/**
	 * @param curCusCrdGrade
	 */
	public void setCurCusCrdGrade(String curCusCrdGrade) {
		this.curCusCrdGrade = curCusCrdGrade == null ? null : curCusCrdGrade.trim();
	}

	/**
	 * @return CurCusCrdGrade
	 */
	public String getCurCusCrdGrade() {
		return this.curCusCrdGrade;
	}

	/**
	 * @param subPrdSerno
	 */
	public void setSubPrdSerno(String subPrdSerno) {
		this.subPrdSerno = subPrdSerno == null ? null : subPrdSerno.trim();
	}

	/**
	 * @return SubPrdSerno
	 */
	public String getSubPrdSerno() {
		return this.subPrdSerno;
	}

	/**
	 * @param accSubPrdNo
	 */
	public void setAccSubPrdNo(String accSubPrdNo) {
		this.accSubPrdNo = accSubPrdNo == null ? null : accSubPrdNo.trim();
	}

	/**
	 * @return AccSubPrdNo
	 */
	public String getAccSubPrdNo() {
		return this.accSubPrdNo;
	}

	/**
	 * @param lmtBizType
	 */
	public void setLmtBizType(String lmtBizType) {
		this.lmtBizType = lmtBizType == null ? null : lmtBizType.trim();
	}

	/**
	 * @return LmtBizType
	 */
	public String getLmtBizType() {
		return this.lmtBizType;
	}

	/**
	 * @param lmtBizTypeName
	 */
	public void setLmtBizTypeName(String lmtBizTypeName) {
		this.lmtBizTypeName = lmtBizTypeName == null ? null : lmtBizTypeName.trim();
	}

	/**
	 * @return LmtBizTypeName
	 */
	public String getLmtBizTypeName() {
		return this.lmtBizTypeName;
	}

	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}

	/**
	 * @return LoanTerm
	 */
	public Integer getLoanTerm() {
		return this.loanTerm;
	}

	/**
	 * @param guarType
	 */
	public void setGuarType(String guarType) {
		this.guarType = guarType == null ? null : guarType.trim();
	}

	/**
	 * @return GuarType
	 */
	public String getGuarType() {
		return this.guarType;
	}

	/**
	 * @param loanAmt
	 */
	public void setLoanAmt(java.math.BigDecimal loanAmt) {
		this.loanAmt = loanAmt;
	}

	/**
	 * @return LoanAmt
	 */
	public java.math.BigDecimal getLoanAmt() {
		return this.loanAmt;
	}

	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	/**
	 * @return ExecRateYear
	 */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}

	/**
	 * @param priceMode
	 */
	public void setPriceMode(String priceMode) {
		this.priceMode = priceMode == null ? null : priceMode.trim();
	}

	/**
	 * @return PriceMode
	 */
	public String getPriceMode() {
		return this.priceMode;
	}

	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval == null ? null : lprRateIntval.trim();
	}

	/**
	 * @return LprRateIntval
	 */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}

	/**
	 * @param rateIntvalDate
	 */
	public void setRateIntvalDate(String rateIntvalDate) {
		this.rateIntvalDate = rateIntvalDate == null ? null : rateIntvalDate.trim();
	}

	/**
	 * @return RateIntvalDate
	 */
	public String getRateIntvalDate() {
		return this.rateIntvalDate;
	}

	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}

	/**
	 * @return RateFloatPoint
	 */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}

	/**
	 * @param curLpr
	 */
	public void setCurLpr(java.math.BigDecimal curLpr) {
		this.curLpr = curLpr;
	}

	/**
	 * @return CurLpr
	 */
	public java.math.BigDecimal getCurLpr() {
		return this.curLpr;
	}

	/**
	 * @param rateAdjustCycle
	 */
	public void setRateAdjustCycle(String rateAdjustCycle) {
		this.rateAdjustCycle = rateAdjustCycle == null ? null : rateAdjustCycle.trim();
	}

	/**
	 * @return RateAdjustCycle
	 */
	public String getRateAdjustCycle() {
		return this.rateAdjustCycle;
	}

	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(Integer nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval;
	}

	/**
	 * @return NextRateAdjInterval
	 */
	public Integer getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}

	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate == null ? null : firstAdjDate.trim();
	}

	/**
	 * @return FirstAdjDate
	 */
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}

	/**
	 * @param guarntrNo
	 */
	public void setGuarntrNo(String guarntrNo) {
		this.guarntrNo = guarntrNo == null ? null : guarntrNo.trim();
	}

	/**
	 * @return GuarntrNo
	 */
	public String getGuarntrNo() {
		return this.guarntrNo;
	}

	/**
	 * @param outguarTotalAmt
	 */
	public void setOutguarTotalAmt(java.math.BigDecimal outguarTotalAmt) {
		this.outguarTotalAmt = outguarTotalAmt;
	}

	/**
	 * @return OutguarTotalAmt
	 */
	public java.math.BigDecimal getOutguarTotalAmt() {
		return this.outguarTotalAmt;
	}

	/**
	 * @param orgGuarAmt
	 */
	public void setOrgGuarAmt(java.math.BigDecimal orgGuarAmt) {
		this.orgGuarAmt = orgGuarAmt;
	}

	/**
	 * @return OrgGuarAmt
	 */
	public java.math.BigDecimal getOrgGuarAmt() {
		return this.orgGuarAmt;
	}

	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	/**
	 * @return Status
	 */
	public String getStatus() {
		return this.status;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}

	/**
	 * @return OprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}

	/**
	 * @return InputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}

	/**
	 * @return InputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}

	/**
	 * @return InputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}

	/**
	 * @return UpdId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}

	/**
	 * @return UpdBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}

	/**
	 * @return UpdDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return CreateTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return UpdateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}