/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CtrContImageAuditAppDomain;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrContImageAuditApp;
import cn.com.yusys.yusp.service.CtrContImageAuditAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContImageAuditAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-24 10:06:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/ctrcontimageauditapp")
public class CtrContImageAuditAppResource {
    @Autowired
    private CtrContImageAuditAppService ctrContImageAuditAppService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrContImageAuditApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrContImageAuditApp> list = ctrContImageAuditAppService.selectAll(queryModel);
        return new ResultDto<List<CtrContImageAuditApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrContImageAuditApp>> index(QueryModel queryModel) {
        List<CtrContImageAuditApp> list = ctrContImageAuditAppService.selectByModel(queryModel);
        return new ResultDto<List<CtrContImageAuditApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CtrContImageAuditApp> show(@PathVariable("serno") String serno) {
        CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByPrimaryKey(serno);
        return new ResultDto<CtrContImageAuditApp>(ctrContImageAuditApp);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping ("/selectbyprimarykey")
    protected ResultDto<CtrContImageAuditApp> selectByPrimaryKey(@RequestBody CtrContImageAuditApp ctrContImageAuditApp) {
        return new ResultDto<CtrContImageAuditApp>(ctrContImageAuditAppService.selectByPrimaryKey(ctrContImageAuditApp.getSerno()));
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrContImageAuditApp> create(@RequestBody CtrContImageAuditApp ctrContImageAuditApp) throws URISyntaxException {
        ctrContImageAuditAppService.insert(ctrContImageAuditApp);
        return new ResultDto<CtrContImageAuditApp>(ctrContImageAuditApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrContImageAuditApp ctrContImageAuditApp) throws URISyntaxException {
        int result = ctrContImageAuditAppService.update(ctrContImageAuditApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = ctrContImageAuditAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrContImageAuditAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

   /**
    * @创建人 WH
    * @创建时间 2021/6/24 10:35
    * @注释 分页条件查询
    */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<CtrContImageAuditApp>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<CtrContImageAuditApp> list = ctrContImageAuditAppService.selectByModel(queryModel);
        return new ResultDto<List<CtrContImageAuditApp>>(list);
    }

    /**
     * @param ctrContImageAuditApp
     * @return ResultDto<java.lang.Integer>
     * @author 王玉坤
     * @date 2021/5/4 10:45
     * @version 1.0.0
     * @desc 根据主键删除信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/deletebypkid")
    protected ResultDto<Integer> deleteByPk(@RequestBody CtrContImageAuditApp ctrContImageAuditApp) {
        int result = ctrContImageAuditAppService.deleteByPrimaryKey(ctrContImageAuditApp.getSerno());
        return new ResultDto<Integer>(result);
    }



    /**
     * @return ResultDto<java.lang.Boolean>
     * @author 王玉坤
     * @date 2021/5/3 18:05
     * @version 1.0.0
     * @desc 点击下一步插入线上合同提款申请处理逻辑
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/donextstep")
    protected ResultDto<CtrContImageAuditApp> doNextStep(@RequestBody CtrLoanCont ctrLoanCont) {
        return ctrContImageAuditAppService.doNextStep(ctrLoanCont);
    }

    /**
     * @return ResultDto<java.lang.Boolean>
     * @author zxz
     * @date 2021/5/3 18:05
     * @version 1.0.0
     * @desc 点击下一步插入线上合同提款申请处理逻辑
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/doneimagestep")
    protected ResultDto<Boolean> doSavleImageStep(@RequestBody CtrLoanCont ctrLoanCont) {
        return ResultDto.success(ctrContImageAuditAppService.doSavleImageStep(ctrLoanCont));
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-07 19:43
     * @注释 条件历史查询 selectByModel2
     */
    @PostMapping("/selectByModel2")
    protected ResultDto<List<CtrContImageAuditAppDomain>> selectByModel2(@RequestBody QueryModel queryModel) {
        List<CtrContImageAuditAppDomain> list = ctrContImageAuditAppService.selectByModel2(queryModel);
        return new ResultDto<List<CtrContImageAuditAppDomain>>(list);
    }
    /**
     * @创建人 WH
     * @创建时间 2021-05-07 20:35
     * @注释 修改合同启用标识
     */
    @PostMapping("/updatactrbeginflag")
    protected ResultDto updatactrbeginflag(@RequestBody CtrContImageAuditApp ctrContImageAuditApp){
        return ctrContImageAuditAppService.updatactrbeginflag(ctrContImageAuditApp);
    }

    /**
     * @创建人 zxz
     * @创建时间 2021-05-11 19:43
     * @注释 条件历史查询 selectctrimagebymode
     */
    @PostMapping("/selectctrimagebymode")
    protected ResultDto<List<CtrContImageAuditAppDomain>> selectCtrImageByMode(@RequestBody QueryModel queryModel) {
        List<CtrContImageAuditAppDomain> list = ctrContImageAuditAppService.selectCtrImageByMode(queryModel);
        return new ResultDto<List<CtrContImageAuditAppDomain>>(list);
    }

    /**
     * @创建人 zxz
     * @创建时间 2021-05-11 19:43
     * @注释 条件历史查询 selectctrimagehisbymode
     */
    @PostMapping("/selectctrimagehisbymode")
    protected ResultDto<List<CtrContImageAuditAppDomain>> selectCtrImageHisByMode(@RequestBody QueryModel queryModel) {
        List<CtrContImageAuditAppDomain> list = ctrContImageAuditAppService.selectCtrImageHisByMode(queryModel);
        return new ResultDto<List<CtrContImageAuditAppDomain>>(list);
    }

    /**
     * @创建人 qw
     * @方法名 queryCtrContImageAuditAppBySerno
     * @创建时间 2021-08-02 17:01
     * @描述 通过流水号查询数据
     */
    @PostMapping("/queryctrcontimageauditappbyserno")
    protected ResultDto<CtrContImageAuditApp> queryCtrContImageAuditAppBySerno(@RequestBody String serno) {
        CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.queryCtrContImageAuditAppBySerno(serno);
        return new ResultDto<CtrContImageAuditApp>(ctrContImageAuditApp);
    }

    /**
     * 根据合同号查询影像审核申请信息
     * @param contNo
     * @return
     */
    @PostMapping("/selectByContNo")
    protected ResultDto<CtrContImageAuditApp> selectByContNo(@RequestBody String contNo){
        CtrContImageAuditApp ctrContImageAuditApp = ctrContImageAuditAppService.selectByContNo(contNo);
        return new ResultDto<CtrContImageAuditApp>(ctrContImageAuditApp);
    }

    /**
     * @创建人 qw
     * @方法名 saveCtrLoanContDtoData
     * @创建时间 2021-08-02 17:01
     * @描述 合同影像审核下一步新增
     */
    @PostMapping("/savectrLoancontdtodata")
    protected ResultDto<Map> saveCtrLoanContDtoData(@RequestBody CtrLoanContDto ctrLoanContDto) {
        return ResultDto.success(ctrContImageAuditAppService.saveCtrLoanContDtoData(ctrLoanContDto));
    }

    /**
     * @创建人 qw
     * @方法名 pendingList
     * @创建时间 2021-08-25 17:01
     * @描述 合同影像审核待处理列表
     */
    @PostMapping("/pendinglist")
    protected ResultDto<List<CtrContImageAuditApp>> pendingList(@RequestBody QueryModel queryModel) {
        List<CtrContImageAuditApp> contImageAuditAppDomainList = ctrContImageAuditAppService.pendingList(queryModel);
        return new ResultDto<List<CtrContImageAuditApp>>(contImageAuditAppDomainList);
    }

    /**
     * @创建人 qw
     * @方法名 processedList
     * @创建时间 2021-08-25 17:01
     * @描述 合同影像审核已处理列表
     */
    @PostMapping("/processedlist")
    protected ResultDto<List<CtrContImageAuditApp>> processedList(@RequestBody QueryModel queryModel) {
        List<CtrContImageAuditApp> contImageAuditAppDomainList = ctrContImageAuditAppService.processedList(queryModel);
        return new ResultDto<List<CtrContImageAuditApp>>(contImageAuditAppDomainList);
    }

    /**
     * @创建人 lihh
     * @创建时间 2021-05-07 20:35
     * @注释 房抵e点贷发送风控
     */
    @PostMapping("/sendfk")
    protected ResultDto sendfk(@RequestBody CtrContImageAuditApp ctrContImageAuditApp){
        return ctrContImageAuditAppService.sendfk(ctrContImageAuditApp);
    }
}
