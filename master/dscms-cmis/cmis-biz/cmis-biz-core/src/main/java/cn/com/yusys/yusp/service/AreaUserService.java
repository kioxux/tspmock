/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AreaUser;
import cn.com.yusys.yusp.repository.mapper.AreaUserMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaUserService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-13 20:05:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AreaUserService {

    @Autowired
    private AreaUserMapper areaUserMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AreaUser selectByPrimaryKey(String pkId) {
        return areaUserMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AreaUser> selectAll(QueryModel model) {
        List<AreaUser> records = (List<AreaUser>) areaUserMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AreaUser> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AreaUser> list = areaUserMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AreaUser record) {
        User userInfo = SessionUtils.getUserInformation();
        record.setAreaNo(record.getAreaNo());
        record.setInputId(userInfo.getLoginCode());
        record.setInputBrId(userInfo.getOrg().getCode());
        record.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        record.setUpdId(userInfo.getLoginCode());
        record.setUpdBrId(userInfo.getOrg().getCode());
        record.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        record.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        record.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return areaUserMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AreaUser record) {
        return areaUserMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AreaUser record) {
        return areaUserMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AreaUser record) {
        return areaUserMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return areaUserMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return areaUserMapper.deleteByIds(ids);
    }

    /** 
     * @author zlf
     * @date 2021/4/26 15:28
     * @version 1.0.0
     * @desc    根据区域编号查询表信息
     * @修改历史: 修改时间    修改人员    修改原因
    */
    public List<AreaUser> selectByAreaNo(String areaNo) {
        return areaUserMapper.selectByAreaNo(areaNo);
    }

    public int deleteByPrimaryAreaNo(String areaNo) {
        return areaUserMapper.deleteByPrimaryAreaNo(areaNo);
    }
}
