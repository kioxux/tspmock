/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtSurveyReportBasicInfo;
import cn.com.yusys.yusp.dto.server.xdxw0053.resp.Xdxw0053DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0062.req.Xdxw0062DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0063.req.Xdxw0063DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0063.resp.Xdxw0063DataRespDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportBasicInfoMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: sl
 * @创建时间: 2021-04-26 14:23:36
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtSurveyReportBasicInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtSurveyReportBasicInfo selectByPrimaryKey(@Param("surveySerno") String surveySerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtSurveyReportBasicInfo> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(LmtSurveyReportBasicInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(LmtSurveyReportBasicInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(LmtSurveyReportBasicInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(LmtSurveyReportBasicInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("surveySerno") String surveySerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selcetYndRecordBycoerCode
     * @方法描述: 根据配偶证件号查询配偶是否存在优农贷申请
     * @参数与返回说明:
     * @算法描述: 无
     */

    int selcetYndRecordBycoerCode(String spouseCertCode);

    /**
     * @方法名称: selcetYndApplySernoBycoerCode
     * @方法描述: 根据证件号查询授信调查流水
     * @参数与返回说明:
     * @算法描述: 无
     */

    String selcetYndApplySernoBycoerCode(String certCode);

    /**
     * @方法名称: selcetYndMAxApplySernoBycoerCode
     * @方法描述: 根据证件号查询授信调查流水
     * @参数与返回说明:
     * @算法描述: 无
     */

    String selcetYndMAxApplySernoBycoerCode(String certCode);


    /**
     * 查询经营性贷款客户基本信息
     * @param indgtSerno
     * @return
     */
    Xdxw0053DataRespDto getSurveyReportByIndgtSerno(String indgtSerno);

    /**
     * 根据调差流水号查询是否有重复
     * @param xdxw0062DataReqDto
     * @return
     */
    int getExist(Xdxw0062DataReqDto xdxw0062DataReqDto);

    /**
     * @方法名称: selcetLastReportBycoerCode
     * @方法描述: 根据证件号查询最新的授信调查
     * @参数与返回说明:
     * @算法描述: 无
     */

    LmtSurveyReportBasicInfo selcetLastReportBycoerCode(String certCode);

    /**
     * 根据风控流水号查询借据号
     *
     * @param queryMap
     * @return
     */
    String queryXDBillNo(Map queryMap);

    /**
     * 调查基本信息查询
     * @param xdxw0063DataReqDto
     * @return
     */
    Xdxw0063DataRespDto getReportBasicInfo(Xdxw0063DataReqDto xdxw0063DataReqDto);
}