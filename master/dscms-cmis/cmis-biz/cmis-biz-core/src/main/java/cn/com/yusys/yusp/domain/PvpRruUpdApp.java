/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpRruUpdApp
 * @类描述: pvp_rru_upd_app数据实体类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-01-28 14:37:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_rru_upd_app")
public class PvpRruUpdApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 退汇修改申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "RRU_SERNO")
	private String rruSerno;

	/** 业务流水号 **/
	@Column(name = "OLD_SERNO", unique = false, nullable = false, length = 40)
	private String oldSerno;

	/** 受托支付编号 **/
	@Column(name = "ACCPT_BOOK_NO", unique = false, nullable = false, length = 40)
	private String accptBookNo;

	/** 新支付交易流水 **/
	@Column(name = "NEW_AUTH_NO", unique = false, nullable = false, length = 40)
	private String newAuthNo;

	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = false, length = 40)
	private String billNo;

	/** 授权编号 **/
	@Column(name = "AUTH_NO", unique = false, nullable = true, length = 40)
	private String authNo;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 放款时间 **/
	@Column(name = "PVP_DATE", unique = false, nullable = true, length = 10)
	private String pvpDate;

	/** 账户属性 **/
	@Column(name = "ACCT_ATTR", unique = false, nullable = true, length = 5)
	private String acctAttr;

	/** 原账号归属 **/
	@Column(name = "OLD_ACCT_BELONG", unique = false, nullable = true, length = 5)
	private String oldAcctBelong;

	/** 原账号分类 **/
	@Column(name = "OLD_ACCT_CLASS", unique = false, nullable = true, length = 5)
	private String oldAcctClass;

	/** 原账号 **/
	@Column(name = "OLD_ACCT_NO", unique = false, nullable = true, length = 40)
	private String oldAcctNo;

	/** 原账户名称 **/
	@Column(name = "OLD_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String oldAcctName;

	/** 原开户行行号/机构编号 **/
	@Column(name = "OLD_OPAN_ORG_NO", unique = false, nullable = true, length = 40)
	private String oldOpanOrgNo;

	/** 原开户行行名/机构名称 **/
	@Column(name = "OLD_OPAN_ORG_NAME", unique = false, nullable = true, length = 80)
	private String oldOpanOrgName;

	/** 原币种 **/
	@Column(name = "OLD_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String oldCurType;

	/** 原支付金额 **/
	@Column(name = "OLD_DEFRAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldDefrayAmt;

	/** 账号归属 **/
	@Column(name = "ACCT_BELONG", unique = false, nullable = true, length = 5)
	private String acctBelong;

	/** 账号分类 **/
	@Column(name = "ACCT_CLASS", unique = false, nullable = true, length = 5)
	private String acctClass;

	/** 账号 **/
	@Column(name = "ACCT_NO", unique = false, nullable = true, length = 40)
	private String acctNo;

	/** 账号名称 **/
	@Column(name = "ACCT_NAME", unique = false, nullable = true, length = 80)
	private String acctName;

	/** 开户行行号 **/
	@Column(name = "OPAN_ORG_NO", unique = false, nullable = true, length = 20)
	private String opanOrgNo;

	/** 开户行行名 **/
	@Column(name = "OPAN_ORG_NAME", unique = false, nullable = true, length = 100)
	private String opanOrgName;

	/** 机构编号 **/
	@Column(name = "ORG_NO", unique = false, nullable = true, length = 40)
	private String orgNo;

	/** 机构名称 **/
	@Column(name = "ORG_NAME", unique = false, nullable = true, length = 80)
	private String orgName;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 支付金额 **/
	@Column(name = "DEFRAY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal defrayAmt;

	/** 退汇原因 **/
	@Column(name = "RRU_RESN", unique = false, nullable = true, length = 200)
	private String rruResn;

	/** 授权状态 **/
	@Column(name = "AUTH_STATUS", unique = false, nullable = true, length = 5)
	private String authStatus;

	/** 退汇修改说明 **/
	@Column(name = "RRU_EXPL", unique = false, nullable = true, length = 200)
	private String rruExpl;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 申请状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 产品编号	 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getPrdId() {
		return prdId;
	}

	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	/**
	 * @param rruSerno
	 */
	public void setRruSerno(String rruSerno) {
		this.rruSerno = rruSerno;
	}

    /**
     * @return rruSerno
     */
	public String getRruSerno() {
		return this.rruSerno;
	}

	/**
	 * @param oldSerno
	 */
	public void setOldSerno(String oldSerno) {
		this.oldSerno = oldSerno;
	}

    /**
     * @return oldSerno
     */
	public String getOldSerno() {
		return this.oldSerno;
	}

	/**
	 * @param accptBookNo
	 */
	public void setAccptBookNo(String accptBookNo) {
		this.accptBookNo = accptBookNo;
	}

    /**
     * @return accptBookNo
     */
	public String getAccptBookNo() {
		return this.accptBookNo;
	}

	/**
	 * @param newAuthNo
	 */
	public void setNewAuthNo(String newAuthNo) {
		this.newAuthNo = newAuthNo;
	}

    /**
     * @return newAuthNo
     */
	public String getNewAuthNo() {
		return this.newAuthNo;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param authNo
	 */
	public void setAuthNo(String authNo) {
		this.authNo = authNo;
	}

    /**
     * @return authNo
     */
	public String getAuthNo() {
		return this.authNo;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param pvpDate
	 */
	public void setPvpDate(String pvpDate) {
		this.pvpDate = pvpDate;
	}

    /**
     * @return pvpDate
     */
	public String getPvpDate() {
		return this.pvpDate;
	}

	/**
	 * @param acctAttr
	 */
	public void setAcctAttr(String acctAttr) {
		this.acctAttr = acctAttr;
	}

    /**
     * @return acctAttr
     */
	public String getAcctAttr() {
		return this.acctAttr;
	}

	/**
	 * @param oldAcctBelong
	 */
	public void setOldAcctBelong(String oldAcctBelong) {
		this.oldAcctBelong = oldAcctBelong;
	}

    /**
     * @return oldAcctBelong
     */
	public String getOldAcctBelong() {
		return this.oldAcctBelong;
	}

	/**
	 * @param oldAcctClass
	 */
	public void setOldAcctClass(String oldAcctClass) {
		this.oldAcctClass = oldAcctClass;
	}

    /**
     * @return oldAcctClass
     */
	public String getOldAcctClass() {
		return this.oldAcctClass;
	}

	/**
	 * @param oldAcctNo
	 */
	public void setOldAcctNo(String oldAcctNo) {
		this.oldAcctNo = oldAcctNo;
	}

    /**
     * @return oldAcctNo
     */
	public String getOldAcctNo() {
		return this.oldAcctNo;
	}

	/**
	 * @param oldAcctName
	 */
	public void setOldAcctName(String oldAcctName) {
		this.oldAcctName = oldAcctName;
	}

    /**
     * @return oldAcctName
     */
	public String getOldAcctName() {
		return this.oldAcctName;
	}

	/**
	 * @param oldOpanOrgNo
	 */
	public void setOldOpanOrgNo(String oldOpanOrgNo) {
		this.oldOpanOrgNo = oldOpanOrgNo;
	}

    /**
     * @return oldOpanOrgNo
     */
	public String getOldOpanOrgNo() {
		return this.oldOpanOrgNo;
	}

	/**
	 * @param oldOpanOrgName
	 */
	public void setOldOpanOrgName(String oldOpanOrgName) {
		this.oldOpanOrgName = oldOpanOrgName;
	}

    /**
     * @return oldOpanOrgName
     */
	public String getOldOpanOrgName() {
		return this.oldOpanOrgName;
	}

	/**
	 * @param oldCurType
	 */
	public void setOldCurType(String oldCurType) {
		this.oldCurType = oldCurType;
	}

    /**
     * @return oldCurType
     */
	public String getOldCurType() {
		return this.oldCurType;
	}

	/**
	 * @param oldDefrayAmt
	 */
	public void setOldDefrayAmt(java.math.BigDecimal oldDefrayAmt) {
		this.oldDefrayAmt = oldDefrayAmt;
	}

    /**
     * @return oldDefrayAmt
     */
	public java.math.BigDecimal getOldDefrayAmt() {
		return this.oldDefrayAmt;
	}

	/**
	 * @param acctBelong
	 */
	public void setAcctBelong(String acctBelong) {
		this.acctBelong = acctBelong;
	}

    /**
     * @return acctBelong
     */
	public String getAcctBelong() {
		return this.acctBelong;
	}

	/**
	 * @param acctClass
	 */
	public void setAcctClass(String acctClass) {
		this.acctClass = acctClass;
	}

    /**
     * @return acctClass
     */
	public String getAcctClass() {
		return this.acctClass;
	}

	/**
	 * @param acctNo
	 */
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

    /**
     * @return acctNo
     */
	public String getAcctNo() {
		return this.acctNo;
	}

	/**
	 * @param acctName
	 */
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

    /**
     * @return acctName
     */
	public String getAcctName() {
		return this.acctName;
	}

	/**
	 * @param opanOrgNo
	 */
	public void setOpanOrgNo(String opanOrgNo) {
		this.opanOrgNo = opanOrgNo;
	}

    /**
     * @return opanOrgNo
     */
	public String getOpanOrgNo() {
		return this.opanOrgNo;
	}

	/**
	 * @param opanOrgName
	 */
	public void setOpanOrgName(String opanOrgName) {
		this.opanOrgName = opanOrgName;
	}

    /**
     * @return opanOrgName
     */
	public String getOpanOrgName() {
		return this.opanOrgName;
	}

	/**
	 * @param orgNo
	 */
	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}

    /**
     * @return orgNo
     */
	public String getOrgNo() {
		return this.orgNo;
	}

	/**
	 * @param orgName
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

    /**
     * @return orgName
     */
	public String getOrgName() {
		return this.orgName;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param defrayAmt
	 */
	public void setDefrayAmt(java.math.BigDecimal defrayAmt) {
		this.defrayAmt = defrayAmt;
	}

    /**
     * @return defrayAmt
     */
	public java.math.BigDecimal getDefrayAmt() {
		return this.defrayAmt;
	}

	/**
	 * @param rruResn
	 */
	public void setRruResn(String rruResn) {
		this.rruResn = rruResn;
	}

    /**
     * @return rruResn
     */
	public String getRruResn() {
		return this.rruResn;
	}

	/**
	 * @param authStatus
	 */
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

    /**
     * @return authStatus
     */
	public String getAuthStatus() {
		return this.authStatus;
	}

	/**
	 * @param rruExpl
	 */
	public void setRruExpl(String rruExpl) {
		this.rruExpl = rruExpl;
	}

    /**
     * @return rruExpl
     */
	public String getRruExpl() {
		return this.rruExpl;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}
