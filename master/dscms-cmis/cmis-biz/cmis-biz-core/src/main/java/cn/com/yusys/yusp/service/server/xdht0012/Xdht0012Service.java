package cn.com.yusys.yusp.service.server.xdht0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.req.Xwh001ReqDto;
import cn.com.yusys.yusp.dto.server.xdht0012.req.Xdht0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0012.resp.Xdht0012DataRespDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcfEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.xwh.xwh001.Xwh001Service;
import com.alibaba.fastjson.JSON;
import com.mysql.cj.xdevapi.JsonString;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class Xdht0012Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0012Service.class);

    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Resource
    private GuarGuaranteeMapper guarGuaranteeMapper;
    @Resource
    private GrtGuarContMapper grtGuarContMapper;
    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;
    @Resource
    private PvpLoanAppService pvpLoanAppService;
    @Resource
    private PvpAuthorizeMapper pvpAuthorizeMapper;
    @Resource
    private PvpAuthorizeService pvpAuthorizeService;
    @Resource
    private LmtCobInfoMapper lmtCobInfoMapper;
    @Resource
    private ICusClientService icusClientService;
    @Resource
    private AdminSmUserService adminSmUserService;
    @Resource
    private AccLoanMapper accLoanMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private AccLoanService accLoanService;
    @Resource
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Resource
    private DscmsCfgClientService dscmsCfgClientService;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口
    @Autowired
    private AdminSmPropService adminSmPropService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private RepayDayRecordInfoMapper repayDayRecordInfoMapper;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private Xwh001Service xwh001Service;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易描述：借款担保合同签订/支用
     *
     * @param xdht0012DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdht0012DataRespDto getXdht0012(Xdht0012DataReqDto xdht0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012DataReqDto));
        //返回对象
        Xdht0012DataRespDto xdht0012DataRespDto = new Xdht0012DataRespDto();
        try {
            //获取营业日期
            String openday = stringRedisTemplate.opsForValue().get("openDay");
            //获取请求参数
            String oprType = xdht0012DataReqDto.getOprType();//操作类型
            String managerId = xdht0012DataReqDto.getManagerId();//客户经理号
            String grtContNo = xdht0012DataReqDto.getGrtContNo();//担保合同号
            String loanContNo = xdht0012DataReqDto.getLoanContNo();//借款合同号
            String pvpSerno = xdht0012DataReqDto.getPvpSerno();//出账流水号
            String cusId = xdht0012DataReqDto.getCusId();//客户号
            String cusName = xdht0012DataReqDto.getCusName();//客户名称
            String certNo = xdht0012DataReqDto.getCertNo();//证件号
            String curtPvpAmt = xdht0012DataReqDto.getCurtPvpAmt();//本次出账金额
            String billStartDate = xdht0012DataReqDto.getBillStartDate();//借据起始日
            String billEndDate = xdht0012DataReqDto.getBillEndDate();//借据到期日
            String term = xdht0012DataReqDto.getTerm();//期限
            String loanAcctNo = xdht0012DataReqDto.getLoanAcctNo();//贷款发放账户
            String loanAcctName = xdht0012DataReqDto.getLoanAcctName();//贷款账户名称
            String loanSubAcctNo = xdht0012DataReqDto.getLoanSubAcctNo();//贷款发放账户子账户序号
            String pvpStatus = xdht0012DataReqDto.getPvpStatus();//出账状态
            String chnlSour = xdht0012DataReqDto.getChnlSour();//来源渠道
            String imageSerno1 = xdht0012DataReqDto.getImageSerno1();//影像流水号1
            String imageSerno2 = xdht0012DataReqDto.getImageSerno2();//影像流水号2

            //请求参数必输校验
            if (StringUtils.isBlank(oprType)) {
                throw new Exception("操作类型为空！");
            } else if (StringUtils.isBlank(chnlSour)) {
                throw new Exception("来源渠道为空！");
            } else if (!Objects.equals("01", chnlSour) && !Objects.equals("02", chnlSour)) {
                throw new Exception("来源渠道【01手机/02pad】未知！");
            }

            int result = 0;//执行结果
            //根据操作类型分别进行业务处理
            if (oprType.equals(CmisBizConstants.XW_OP_TYPE_01)) {
                //01：签订前（生成pdf）：借款合同和担保合同先生成word后转化pdf
                if (StringUtil.isNotEmpty(loanContNo) && StringUtil.isNotEmpty(grtContNo)) {
                    throw new Exception("借款合同/担保合同不可全填，同时生成pdf！");
                } else {
                    /**
                     * 生成借款合同
                     */
                    if (StringUtil.isNotEmpty(loanContNo)) {
                        CtrLoanCont ctrLoanCont1 = ctrLoanContMapper.selectContByContno(loanContNo);
                        if (ctrLoanCont1 != null) {
                            String cont_cn_no = ctrLoanCont1.getContCnNo();//中文合同编号
                            String cont_type = ctrLoanCont1.getContType();//合同类型
                            String serno = ctrLoanCont1.getIqpSerno();//全局流水号
                            String manager_br_id = ctrLoanCont1.getManagerBrId();//主管机构
                            String sign_channel = ctrLoanCont1.getSignChannel();//签约渠道
                            //校验签约渠道与传入的来源渠道是否一致（20210714 此会校验新信贷不要）
//                            if(sign_channel.equals(chnlSour)){
                            String pdfurl = genContPdf(ctrLoanCont1);
                            xdht0012DataRespDto.setPdfurl(pdfurl);
//                            }else{
//                                throw new Exception("本次签约渠道与借款合同设定的签约渠道不符，请重新选择渠道进行签约！");
//                            }
                        } else {
                            throw new Exception("该借款合同号【" + loanContNo + "】查询不到对应的合同信息！");
                        }
                    }
                    /**
                     * 生成担保合同
                     */
                    if (StringUtil.isNotEmpty(grtContNo)) {
                        String str[] = grtContNo.split("#");
                        if (str.length < 2) {
                            throw new Exception("担保合同号格式应为：【担保号#借款号】");
                        } else {
                            if (StringUtils.isBlank(cusId)) {
                                throw new Exception("担保合同签约客户号必传！");
                            } else {
                                if ("null".equals(str[0]) || "null".equals(str[1])) {
                                    throw new Exception("担保合同号或借款合同为空！");
                                } else {
                                    //设置查询参数
                                    Map queryMap = new HashMap();
                                    queryMap.put("cusId", cusId);//客户号
                                    queryMap.put("dbContNo", str[0]);//担保合同号
                                    //查询签订日期
                                    String signDate = guarGuaranteeMapper.queryGuarGuaranteeSignDateByMap(queryMap);
                                    if (StringUtil.isNotEmpty(signDate)) {
                                        throw new Exception("您已在【" + signDate + "】签约过本笔担保合同！");
                                    } else {
                                        HashMap<String, Object> list = guarGuaranteeMapper.selectlListByDbContNo(str[0]);
                                        String cusid_list = list.get("cusidList").toString();
                                        String cusname_list = list.get("cusnameList").toString();
                                        if (!cusid_list.startsWith(cusId)) {
                                            throw new Exception("按照担保编号顺序签订规则【" + cusname_list + "】，当前客户签约序号未到！");
                                        } else {
                                            String guarContNo = str[0];//担保合同
                                            String loan_Cont_No = str[1];//担保合同
                                            GrtGuarCont grtGuarCont = grtGuarContMapper.selectContNo(guarContNo);
                                            CtrLoanCont ctrLoanCont2 = ctrLoanContMapper.selectContByContno(loan_Cont_No);
                                            String pdfurl = genGuarContPdf(grtGuarCont, ctrLoanCont2);
                                            xdht0012DataRespDto.setPdfurl(pdfurl);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } else if (oprType.equals(CmisBizConstants.XW_OP_TYPE_02)) {
                //02：签订后（合同签约）：更新借款合同与担保合同签约状态，担保合同下所有保证人都已签约才更新状态。
                if (StringUtil.isNotEmpty(loanContNo) && StringUtil.isNotEmpty(grtContNo)) {
                    throw BizException.error(null, EpbEnum.EPB099999.key, "借款合同/担保合同不可全填！");
                } else {
                    if (StringUtils.isBlank(loanContNo) && StringUtils.isBlank(grtContNo)) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "借款合同/担保合同号全为空，无法签订完成！");
                    } else {
                        if (StringUtil.isNotEmpty(loanContNo)) {
                            //设置查询参数
                            Map queryMap = new HashMap();
                            queryMap.put("openDay", openday);//当前日期
                            queryMap.put("ContNo", loanContNo);//借款合同号
                            result = ctrLoanContMapper.updateContStatusByContNo(queryMap);
                            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, result);
                            if (result < 1) {//更新失败
                                throw BizException.error(null, EpbEnum.EPB099999.key, "更新借款合同签约状态失败！");
                            } else {//借款合同签约成功,推送额度系统
                                // 合同校验接口开设
                                //1、获取合同信息
                                CtrLoanCont ctrLoanCont = ctrLoanContMapper.queryCtrLoanContInfoByContNo(loanContNo);
                                logger.info("************XDHT0012*发送额度系统cmislmt0009合同校验接口开始,校验信息【{}】", JSON.toJSONString(ctrLoanCont));
                                ResultDto cmisLmt0009RespDto = cmisBizXwCommonService.contLmtForXw(ctrLoanCont);
                                logger.info("************XDHT0012*发送额度系统cmislmt0009合同校验接口结束,返回信息【{}】", JSON.toJSONString(cmisLmt0009RespDto));
                            }
                        }
                        if (StringUtil.isNotEmpty(grtContNo)) {
                            String str[] = grtContNo.split("#");
                            if (str.length < 2) {
                                throw new Exception("担保合同号格式应为：【担保号#借款号】");
                            } else {
                                if (StringUtils.isBlank(cusId)) {
                                    throw BizException.error(null, EpbEnum.EPB099999.key, "担保合同签约客户号必传！");
                                } else {
                                    if ("null".equals(str[0]) || "null".equals(str[1])) {
                                        throw BizException.error(null, EpbEnum.EPB099999.key, "担保合同号或借款合同为空！");
                                    } else {
                                        // 借款合同是否已签约
                                        String contStatus = ctrLoanContMapper.queryContStatusByContNo(str[1]);
                                        if (!"200".equals(contStatus)) {//借款合同未签约
                                            throw BizException.error(null, EpbEnum.EPB099999.key, "借款合同未签约，请先签约借款合同！");
                                        } else {
                                            //根据担保合同号查询合同状态
                                            HashMap<String, Object> map = grtGuarContMapper.selectContStatuByContNo(str[0]);
                                            String guarContState = map.get("guarcontstate").toString();//担保合同号
                                            String signDate = map.get("signdate").toString();// 签订日期
                                            if ("101".equals(guarContState)) {
                                                throw BizException.error(null, EpbEnum.EPB099999.key, "您已签约过本笔担保合同");
                                            } else {
                                                //设置查询参数
                                                Map queryMap = new HashMap();
                                                queryMap.put("openDay", openday);//当前日期
                                                queryMap.put("ContNo", str[0]);//担保合同号
                                                queryMap.put("cusId", cusId);//客户号
                                                result = guarGuaranteeMapper.updateContStatusByContNo(queryMap);
                                                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, result);
                                                if (result != 1) {//更新失败
                                                    throw BizException.error(null, EpbEnum.EPB099999.key, "更新担保合同签约状态失败！");
                                                }
                                            }
                                            int count = guarGuaranteeMapper.selectCountByContNo(str[0]);
                                            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, count);
                                            if (count == 0) {
                                                //设置查询参数
                                                Map queryMap = new HashMap();
                                                queryMap.put("openDay", openday);//当前日期
                                                queryMap.put("ContNo", str[0]);//担保合同号
                                                result = grtGuarContMapper.updateContStatusByContNo(queryMap);
                                                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, result);
                                                if (result != 1) {//更新失败
                                                    throw BizException.error(null, EpbEnum.EPB099999.key, "更新担保合同签约状态失败！");
                                                }
                                            } else {
                                                logger.info("担保合同未签约的保证人数量：" + count);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (oprType.equals(CmisBizConstants.XW_OP_TYPE_03)) {
                //03：先检测借款合同和担保合同是否签约，若签约，则生成出账信息
                if (StringUtils.isBlank(loanContNo)) {
                    throw new Exception("借款合同号必填！");
                } else if (StringUtils.isBlank(billStartDate) || StringUtils.isBlank(billEndDate) || StringUtils.isBlank(term)) {
                    throw new Exception("借据起始到期日及期限必填！");
                } else if (StringUtils.isBlank(cusId) || StringUtils.isBlank(cusName)) {
                    throw new Exception("客户信息必填！");
                } else if (StringUtils.isBlank(loanAcctNo) || StringUtils.isBlank(loanAcctName) || StringUtils.isBlank(loanSubAcctNo)) {
                    throw new Exception("贷款发放账户相关信息必填！");
                } else if (StringUtils.isBlank(curtPvpAmt)) {
                    throw new Exception("出账金额必填！");
                } else {
                    CtrLoanCont ctrLoanCont1 = ctrLoanContMapper.selectContByContno(loanContNo);
                    if (StringUtils.isBlank(ctrLoanCont1.getContNo())) {
                        throw BizException.error(null, EpbEnum.EPB099999.key, "无此借款合同！");
                    } else {
                        //判断本月线上额度使用上限开始
                        BigDecimal thisPvpAmt = new BigDecimal(curtPvpAmt);
                        //获取配置全行线上贷款放款限额
                        AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
                        adminSmPropQueryDto.setPropName("TOTAL_LIMIT");
                        // 获小贷电子签约限额
                        logger.info("****************获取小贷电子签约限额开始*******************");
                        String grpLmtMax = adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
                        logger.info("****************获取小贷电子签约限额结束，小贷电子签约限额为【{}】", grpLmtMax);
                        BigDecimal totalLimit = new BigDecimal(grpLmtMax);
                        //获取全行线上签约的合同的总贷款余额
                        logger.info("****************获取全行线上签约的合同的总贷款余额开始*******************");
                        BigDecimal totalUsed = accLoanService.getTotalOnlineLoanBalance();
                        logger.info("****************获取全行线上签约的合同的总贷款余额结束，标准化产品授信分项总额【{}】", totalUsed);
                        if (totalLimit.compareTo(totalUsed.add(thisPvpAmt)) < 0) {
                            throw BizException.error(null, EpbEnum.EPB099999.key, "我行本月线上额度使用已达上限。如需支用，请联系专属客户经理。");
                        }
                        //判断本月线上额度使用上限结束

                        //判断借款合同状态-开始
                        String guarWay = ctrLoanCont1.getGuarWay();
                        String cont_status = ctrLoanCont1.getContStatus();
                        String cont_end_date = ctrLoanCont1.getContEndDate();
                        BigDecimal cont_amt = Optional.ofNullable(ctrLoanCont1.getHighAvlAmt()).orElse(BigDecimal.ZERO);

                        if (cont_status.equals("200")) {
                            Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(billEndDate);
                            Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(cont_end_date);
                            if (d1.compareTo(d2) > 0) {
                                throw BizException.error(null, EpbEnum.EPB099999.key, "本笔出账申请借据到期日大于合同到期日，请重新选择！");
                            }
                        } else {
                            throw BizException.error(null, EpbEnum.EPB099999.key, "借款合同未签约，请先签约借款合同！");
                        }
                        //判断借款合同状态-结束

                        /*****************************判断担保合同是否签约开始*********************************/
                        if ("10".equals(guarWay) || "20".equals(guarWay) || "21".equals(guarWay) || "30".equals(guarWay)) {//非信用类担保合同
                            Map queryMap = new HashMap();
                            queryMap.put("contNo", loanContNo);//合同号
                            queryMap.put("Flag", "1");//查询是否存在未签约的担保合同
                            int count = ctrLoanContMapper.selectSignStateGuarLoanContByCertNo(queryMap);

                            Map queryContMap = new HashMap();
                            queryContMap.put("contNo", loanContNo);//合同号
                            queryContMap.put("Flag", "2");//查询是否存在已签约的担保合同
                            int countNum = ctrLoanContMapper.selectSignStateGuarLoanContByCertNo2(queryContMap);

                            if (count > 0 || countNum == 0) {
                                throw BizException.error(null, EpbEnum.EPB099999.key, "担保合同未签约，请先签约担保合同！");
                            }
                        }
                        /*****************************判断担保合同是否签约结束**********************************/

                        //校验金额开始
                        //校验合同可用金额是否超出，如果超出返回并提示："本次出账金额【a】元，已超合同可用额度(包括在途出账记录)【b】元！"
                        //获取合同已使用金额
                        logger.info("****************根据贷款合同号【{}】获取合同已使用金额开始", loanContNo);
                        BigDecimal usedAmt = accLoanService.selectSumLoanBalAmtByParams(loanContNo, "'1', '2', '3','4','5'".split(","));
                        logger.info("****************根据贷款合同号【{}】获取合同已使用金额结束，已使用金额为【{}】", loanContNo, usedAmt);

                        BigDecimal lmt = cont_amt.subtract(usedAmt).subtract(thisPvpAmt);
                        if (lmt.compareTo(BigDecimal.ZERO) < 0) {
                            throw new Exception("本次出账金额【" + thisPvpAmt + "】元，已超合同可用额度(包括在途出账记录)【" + lmt + "】元！");
                        }
                        //校验金额结束
                        //获取是否小微客户经理
                        String isXWUser = Strings.EMPTY;
                        managerId = ctrLoanCont1.getManagerId();
                        if (StringUtil.isNotEmpty(managerId)) {//客户经理不能为空
                            logger.info("************XDHT0012*第一步根据客户经理编号【{}】查询客户经理信息开始", managerId);
                            ResultDto<GetIsXwUserDto> getIsXwUserDtoResultDto = adminSmUserService.getIsXWUserByLoginCode(managerId);
                            logger.info("************XDHT0012*第一步根据客户经理编号【{}】查询客户经理信息结束,客户信息【{}】", managerId, JSON.toJSONString(getIsXwUserDtoResultDto));
                            if (ResultDto.success().getCode().equals(getIsXwUserDtoResultDto.getCode())) {
                                GetIsXwUserDto getIsXwUserDto = getIsXwUserDtoResultDto.getData();
                                if (Objects.nonNull(getIsXwUserDto)) {
                                    isXWUser = getIsXwUserDto.getIsXWUser();
                                }
                            }
                        }
                        // 账务机构FinaBrId
                        String FinaBrId = "";
                        String loan_br_id = ctrLoanCont1.getManagerBrId();
                        if (StringUtil.isNotEmpty(isXWUser) && "Y".equals(isXWUser)) {//是小微客户经理
                            if (loan_br_id.startsWith("01")) {
                                FinaBrId = "016001";
                            } else {
                                FinaBrId = loan_br_id.substring(0, 5) + "6";
                            }
                        } else {
                            FinaBrId = loan_br_id.substring(0, 5) + "1";
                        }
                        logger.info("************XDHT0012*获取账务机构【{}】结束", FinaBrId);

                        //查询授信批复信息
                        //授信调查编号
                        String survey_serno = ctrLoanCont1.getSurveySerno();
                        logger.info("**************XDHT0012**根据合同号【{}】查询授信批复信息开始", survey_serno);
                        LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectBySurveySerno(survey_serno);
                        String repayMode = lmtCrdReplyInfo.getRepayMode();
                        logger.info("**************XDHT0012**根据合同号【{}】查询授信批复信息结束,查询结果信息【{}】", survey_serno, JSON.toJSONString(lmtCrdReplyInfo));
                        if (StringUtil.isEmpty(repayMode)) {
                            throw BizException.error(null, EpbEnum.EPB099999.key, "该笔业务的授信批复还款方式为空,请检查！");
                        }
                        //PvpLoanApp表插入数据
                        /****************生成出账流水号********************/
                        pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
                        /****************生成借据号********************/
                        String bill_no = cmisBizXwCommonService.getBillNo(ctrLoanCont1.getContNo());
                        PvpLoanApp pvpLoanApp = new PvpLoanApp();
                        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(ctrLoanCont1, pvpLoanApp);
                        pvpLoanApp.setPvpSerno(pvpSerno);//放款流水号
                        pvpLoanApp.setIqpSerno(ctrLoanCont1.getIqpSerno());//业务申请流水号
                        pvpLoanApp.setBillNo(bill_no);//借据编号
                        pvpLoanApp.setContNo(ctrLoanCont1.getContNo());//合同编号
                        pvpLoanApp.setSurveySerno(ctrLoanCont1.getSurveySerno());//调查编号
                        pvpLoanApp.setContCnNo(ctrLoanCont1.getContCnNo());//中文合同编号
                        pvpLoanApp.setCusId(cusId);//客户编号
                        pvpLoanApp.setCusName(cusName);//客户名称
                        pvpLoanApp.setPrdId(ctrLoanCont1.getPrdId());//产品编号
                        pvpLoanApp.setPrdName(ctrLoanCont1.getPrdName());//产品名称
                        pvpLoanApp.setLoanModal(ctrLoanCont1.getLoanModal());//贷款形式
                        pvpLoanApp.setGuarMode(ctrLoanCont1.getGuarWay());//担保方式 主担保方式 零售产品及小微优享贷为信用类贷款 00--信用
                        pvpLoanApp.setRepayMode(repayMode);
                        //pvpLoanApp.setRefinancingType();//借新还旧类型
                        //pvpLoanApp.setFtp();//FTP剔除考核
                        //pvpLoanApp.setIsEpidemicCorreCon();//是否为疫情相关企业
                        //pvpLoanApp.getPvpMode();//出账模式
                        //pvpLoanApp.setIsContImageAudit();//合同影像是否审核
                        pvpLoanApp.setContCurType(ctrLoanCont1.getCurType());//合同币种
                        /******************************贷款出账金额***************************************/
                        pvpLoanApp.setPvpAmt(new BigDecimal(curtPvpAmt));// 出账金额
                        //pvpLoanApp.setContHighDisb(ctrLoanCont1.getContBalance());//合同最高可放金额
                        pvpLoanApp.setStartDate(ctrLoanCont1.getContStartDate());//起始日
                        pvpLoanApp.setEndDate(ctrLoanCont1.getContEndDate());//到期日
                        //pvpLoanApp.setIsSeal();//是否用印
                        pvpLoanApp.setLoanStartDate(billStartDate);//贷款起始日
                        pvpLoanApp.setLoanEndDate(billEndDate);//贷款到期日
                        pvpLoanApp.setLoanTerm(term);//贷款期限
                        pvpLoanApp.setLoanTermUnit("M");//贷款期限单位
                        pvpLoanApp.setRateAdjMode("01");//利率调整方式 信贷字典项 01--固定利率 02--浮动利率
                        pvpLoanApp.setIsSegInterest("0");//是否分段计息
                        BigDecimal curtLprRate = queryLprRate(new BigDecimal(term));
                        pvpLoanApp.setCurtLprRate(curtLprRate);//当前LPR利率
                        pvpLoanApp.setExecRateYear(ctrLoanCont1.getExecRateYear());
                        pvpLoanApp.setCiExecRate(ctrLoanCont1.getExecRateYear().multiply(new BigDecimal(1.5)));
                        pvpLoanApp.setCiRatePefloat(BigDecimal.valueOf(0.5));
                        pvpLoanApp.setOverdueExecRate(ctrLoanCont1.getExecRateYear().multiply(new BigDecimal(1.5)));
                        pvpLoanApp.setOverdueRatePefloat(BigDecimal.valueOf(0.5));
                        pvpLoanApp.setEiIntervalCycle("1");//结息间隔周期
                        pvpLoanApp.setEiIntervalUnit("M");//结息间隔周期单位
                        pvpLoanApp.setDeductType("AUTO");//扣款方式
                        String repayDate = ctrLoanCont1.getRepayDate();//还款日
                        /******************扣款日期获取开始******************/
                        RepayDayRecordInfo repayDayRecordInfo = repayDayRecordInfoMapper.selectNewData();
                        /******************扣款日期获取结束******************/
                        if (StringUtil.isNotEmpty(repayDate)) {//合同扣款日不为空,取合同还款日
                            pvpLoanApp.setDeductDay(repayDate);
                        } else {
                            pvpLoanApp.setDeductDay(repayDayRecordInfo.getRepayDate().toString());//扣款日
                        }
                        pvpLoanApp.setBelgLine("01");//业务条线
                        pvpLoanApp.setLoanPayoutAccno(loanAcctNo);//贷款发放账号
                        pvpLoanApp.setLoanPayoutSubNo(loanSubAcctNo);//贷款发放账号子序号
                        pvpLoanApp.setPayoutAcctName(loanAcctName);//发放账号名称
                        pvpLoanApp.setRepayAccno(loanAcctNo);//贷款还款账号
                        pvpLoanApp.setRepaySubAccno(loanSubAcctNo);//贷款还款账户子序号
                        pvpLoanApp.setRepayAcctName(loanAcctName);//还款账户名称
                        pvpLoanApp.setLoanTer(ctrLoanCont1.getLoanTer());//贷款投向
                        pvpLoanApp.setLoanSubjectNo(StringUtils.EMPTY);//科目
                        pvpLoanApp.setFinaBrId(FinaBrId);//账务机构编号
                        pvpLoanApp.setDisbOrgNo(FinaBrId);//放款机构编号
                        pvpLoanApp.setInputId(ctrLoanCont1.getInputId());//登记人
                        pvpLoanApp.setInputBrId(ctrLoanCont1.getManagerBrId());//登记机构
                        pvpLoanApp.setInputDate(DateUtils.getCurrDateStr());//登记日期
                        pvpLoanApp.setUpdId(ctrLoanCont1.getUpdId());//最近修改人
                        pvpLoanApp.setUpdBrId(ctrLoanCont1.getUpdBrId());//最近修改机构
                        pvpLoanApp.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                        pvpLoanApp.setManagerId(ctrLoanCont1.getManagerId());//主管客户经理
                        pvpLoanApp.setManagerBrId(ctrLoanCont1.getManagerBrId());//主管机构
                        pvpLoanApp.setDisbOrgNo(ctrLoanCont1.getManagerBrId());//放款机构
                        pvpLoanApp.setUpdDate(DateUtils.getCurrDateStr());//最近修改日期
                        pvpLoanApp.setCreateTime(DateUtils.getCurrDate());//创建时间
                        pvpLoanApp.setUpdateTime(DateUtils.getCurrDate());//修改时间
                        pvpLoanApp.setApproveStatus("000");
                        pvpLoanApp.setChnlSour("01");//渠道
                        pvpLoanApp.setExchangeRate(BigDecimal.ONE);//汇率
                        pvpLoanApp.setLprRateIntval(new BigDecimal(term).compareTo(new BigDecimal("60")) > 0 ? "A2" : "A1");//LPR授信利率区间
                        pvpLoanApp.setCvtCnyAmt(new BigDecimal(curtPvpAmt));//折算人民币金额
                        pvpLoanApp.setRateFloatPoint(new BigDecimal(ctrLoanCont1.getExecRateYear().subtract(curtLprRate).multiply(new BigDecimal("10000")).intValue()));//浮动点数
                        //初始化统计分类信息
                        pvpLoanApp = initTjflInfo(pvpLoanApp, ctrLoanCont1);
                        //执行插入操作
                        result = pvpLoanAppMapper.insert(pvpLoanApp);
                        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, result);
                        if (result != 1) {//插入数据失败
                            throw BizException.error(null, EpbEnum.EPB099999.key, "贷款出账申请表插入数据失败！");
                        }
                        xdht0012DataRespDto.setPvpSerno(pvpSerno);//返回出账流水
                    }
                }
            } else if (oprType.equals(CmisBizConstants.XW_OP_TYPE_04)) {
                //04：查询出帐信息
                //设置查询参数
                Map queryMap = new HashMap();
                if (StringUtils.isBlank(certNo) && "01".equals(chnlSour)) {
                    throw new Exception("身份证号码手机端必填！");
                } else {
                    if (StringUtil.isNotEmpty(grtContNo)) {
                        CusBaseClientDto cusBaseClientDto = icusClientService.queryCusByCertCode(certNo);
                        String cus_id = cusBaseClientDto.getCusId();
                        queryMap.put("cusId", cus_id);//客户号
                    }
                }
                if (StringUtils.isBlank(managerId) && "02".equals(chnlSour)) {
                    throw new Exception("客户经理号pad端必填！");
                } else {
                    if (StringUtil.isNotEmpty(managerId)) {
                        queryMap.put("managerId", managerId);//客户经理号
                    }
                }
                if (StringUtil.isNotEmpty(loanContNo)) {
                    queryMap.put("contNo", loanContNo);//合同编号
                }
                if (StringUtil.isNotEmpty(cusName)) {
                    queryMap.put("cusName", cusName);//客户名称
                }
                if (StringUtil.isNotEmpty(billStartDate)) {
                    queryMap.put("loanStartDate", billStartDate);//贷款起始日
                }
                if (StringUtil.isNotEmpty(billEndDate)) {
                    queryMap.put("loanEndDate", billEndDate);//贷款到期日
                }
                if (StringUtil.isNotEmpty(pvpStatus)) {
                    queryMap.put("approveStatus", pvpStatus);//出账状态
                }
                logger.info("查询开始,查询参数为:{}", JSON.toJSONString(queryMap));
                List<cn.com.yusys.yusp.dto.server.xdht0012.resp.PvpList> lists = pvpLoanAppMapper.getPvpLoanAppList(queryMap);
                logger.info("查询结束,返回结果为:{}", JSON.toJSONString(lists));
                xdht0012DataRespDto.setPvpList(lists);
            } else if (oprType.equals(CmisBizConstants.XW_OP_TYPE_05)) {
                //05：调用核心放款
                if (StringUtils.isBlank(pvpSerno)) {
                    throw BizException.error(null, null, "出账流水号必填！");
                }
                //本次出帐金额
                BigDecimal pvp_amt = new BigDecimal(curtPvpAmt);
                //推荐人工号
                String manager_id = xdht0012DataReqDto.getManagerId();

                //查询pvploanapp出帐申请表
                logger.info("****************根据出账流水号【{}】查询pvploanapp出帐申请表信息开始", pvpSerno);
                PvpLoanApp pvpLoanApp = pvpLoanAppMapper.selectByPvpLoanSernoKey(pvpSerno);
                if (Objects.isNull(pvpLoanApp)) {
                    logger.info("出账流水号【{}】,未查询到出账申请信息！", pvpLoanApp.getPvpSerno());
                    throw BizException.error(null, null, "未查询到出账申请信息！");
                }
                logger.info("****************根据出账流水号【{}】查询pvploanapp出帐申请表信息结束,查询结果信息【{}】", pvpSerno, JSON.toJSONString(pvpLoanApp));

                // 幂等性校验 若已审批通过，直接返回
                if ("997".equals(pvpLoanApp.getApproveStatus())) {
                    logger.info("该笔出账信息已出账，请勿重新发起，出账流水号【{}】", pvpLoanApp.getPvpSerno());
                    throw BizException.error(null, null, "本笔出账已成功，请勿重复出账！");
                }

                // 起始日期校验
                if (!Objects.equals(pvpLoanApp.getLoanStartDate(), openday)) {
                    logger.info("信贷系统当前日期为【{}】,与借据起始日【{}】不一致，请重新出账！", openday, pvpLoanApp.getLoanStartDate());
                    throw BizException.error(null, null, "信贷系统当前日期与借据起始日不一致，请重新出账！");
                }

                //获取借款合同号
                String cont_no = pvpLoanApp.getContNo();
                //查询ctrloancont合同表
                logger.info("****************根据合同号【{}】查询ctrloancont合同表信息开始", cont_no);
                CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectContByContno(cont_no);
                logger.info("****************根据合同号【{}】查询ctrloancont合同表信息结束,查询结果信息【{}】", cont_no, JSON.toJSONString(ctrLoanCont));
                //授信调查编号
                String survey_serno = ctrLoanCont.getSurveySerno();
                //查询授信批复信息
                logger.info("****************根据合同号【{}】查询授信批复信息开始", survey_serno);
                LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectBySurveySerno(survey_serno);
                logger.info("****************根据合同号【{}】查询授信批复信息结束,查询结果信息【{}】", survey_serno, JSON.toJSONString(lmtCrdReplyInfo));
                //受托类型 1公司法人股东 2个体工商户、合作社 3、无营业执照客户 4、消费性业务
                String tru_pay_type = lmtCrdReplyInfo.getTruPayType();

                //最高支用金额校验开始:本业务最高支用金额校验，如果金额已经超出，将pvp表改为作废状态
                //1、获取合同下7天内的放款金额
                Map paramMap = new HashMap();
                paramMap.put("openday", openday);
                paramMap.put("cont_no", cont_no);
                BigDecimal usedAmt7Days = accLoanService.get7DayTotalLoanAmtByContNo(paramMap);
                //2、获取业务最高支用金额
                CfgPrdBasicinfoDto cfgPrdBasicinfoDto = null;
                ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDtoResultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(ctrLoanCont.getPrdId());
                if (Objects.isNull(cfgPrdBasicinfoDtoResultDto) || Objects.isNull(cfgPrdBasicinfoDtoResultDto.getData())) {
                    throw BizException.error(null, "9999", "未查询到产品信息！");
                } else {
                    cfgPrdBasicinfoDto = cfgPrdBasicinfoDtoResultDto.getData();
                }
                //获取产品属性 08--小微经营性 09--小微消费性
                String bizType = cfgPrdBasicinfoDto.getPrdType();
                BigDecimal maxAmt = BigDecimal.ZERO;
                if ("09".equals(bizType)) {
                    maxAmt = new BigDecimal(300000);
                } else {
                    if ("1".equals(tru_pay_type)) {
                        maxAmt = new BigDecimal(3000000);
                    } else if ("2".equals(tru_pay_type)) {
                        maxAmt = new BigDecimal(500000);
                    } else if ("3".equals(tru_pay_type)) {
                        maxAmt = new BigDecimal(300000);
                    } else {
                        maxAmt = new BigDecimal(300000);
                    }
                }
                if (usedAmt7Days.add(pvp_amt).compareTo(maxAmt) > 0) {
                    //本业务最高支用金额校验，如果金额已经超出，将pvp表改为作废状态 996--自行退出
                    if ("01".equals(chnlSour)) {
                        logger.info("手机端需作废放款申请信息！");
                        //手机端失败作废相关记录
                        pvpLoanAppMapper.updateApproveStatus(pvpSerno, "996");
                    }
                    throw BizException.error(null, EpbEnum.EPB099999.key, "您7天内累计已支用" + usedAmt7Days + "元，加上本次支用金额" + pvp_amt + "元，已超过本业务最高支用额度" + maxAmt + "元！请联系我行客户经理。");
                }
                //最高支用金额校验结束

                //调用公共出帐方法开始
                //1、生成pvpAuthorize和acc_loan表
                logger.info("****************根据出账流水号【{}】生成出账授权信息开始", pvpSerno);
                PvpAuthorize pvpAuthorize = this.buildPvpAuthorize(pvpLoanApp);
                pvpAuthorizeService.insert(pvpAuthorize);
                logger.info("****************根据出账流水号【{}】生成出账授权信息结束", pvpSerno);
                //2、查询pvpAuthorize
                logger.info("****************根据出账流水号【{}】生成台账信息开始", pvpSerno);
                AccLoan accLoan = this.buildAccLoan(pvpLoanApp);
                accLoanService.insert(accLoan);
                logger.info("****************根据出账流水号【{}】生成台账信息结束", pvpSerno);
                //3、调用公共放款交易
                logger.info("****************根据出账信息【{}】调用公共放款交易开始", JSON.toJSONString(pvpAuthorize));
                ResultDto resultDto = pvpAuthorizeService.sendAuthToCoreForXd(pvpAuthorize);
                logger.info("****************根据出账信息【{}】调用公共出帐方法结束,调用公共出帐方法结束信息【{}】", JSON.toJSONString(pvpAuthorize), JSON.toJSONString(resultDto));
                //调用公共出帐方法结束
                String coreRepCode = resultDto.getCode();
                String errorMsg = resultDto.getMessage();
                if ("9999".equals(resultDto.getCode())) {//调用出账失败
                    throw BizException.error(null, EpbEnum.EPB099999.key, errorMsg);
                }
                if (SuccessEnum.CMIS_SUCCSESS.key.equals(coreRepCode)) {
                    logger.info("****************根据出账流水号【{}】更新出账申请信息审批通过开始", pvpSerno);
                    pvpLoanApp.setApproveStatus("997");
                    pvpLoanAppService.update(pvpLoanApp);
                    logger.info("****************根据出账流水号【{}】更新出账申请信息审批通过结束", pvpSerno);

                    /***************************************放款成功后进行台账校验*********************************************/
                    logger.info("************XDCZ0021*放款成功后进行台账校验开始,校验信息【{}】", JSON.toJSONString(pvpLoanApp));
                    cmisBizXwCommonService.loanLmtForXw(pvpLoanApp);
                    logger.info("************XDCZ0021*放款成功后进行台账校验结束");

                    /***************************************通知小微公众号*********************************************/
                    this.sendXwh001(accLoan);
                }
            } else if (oprType.equals(CmisBizConstants.XW_OP_TYPE_06)) {
                //06：作废出帐信息
                if (StringUtils.isBlank(pvpSerno)) {
                    throw new Exception("出账流水号必填！");
                }
                result = pvpLoanAppMapper.updateApproveStatusByPvpSerno(pvpSerno);
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, result);
                if (result != 1) {//更新失败
                    throw new Exception("作废出帐信息失败！");
                }
                result = pvpAuthorizeMapper.updateAuthStatusByPvpSerno(pvpSerno);
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, result);
                if (result < 1) {//更新失败
                    throw new Exception("作废出帐信息失败！");
                }
            } else if (oprType.equals(CmisBizConstants.XW_OP_TYPE_07)) {
                //07：查询借据信息
                if (StringUtils.isBlank(loanContNo)) {
                    throw new Exception("借款合同号必填！");
                }
                List<cn.com.yusys.yusp.dto.server.xdht0012.resp.BillList> lists = accLoanMapper.getAccLoanList(loanContNo);
                for (int i = 0; i < lists.size(); i++) {
                    String manager_id = lists.get(i).getManagerId();
                    ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(manager_id);
                    String managerName = adminSmUserDtoResultDto.getData().getUserName();
                    lists.get(i).setManagerName(managerName);
                }
                xdht0012DataRespDto.setBillList(lists);
            } else if (oprType.equals(CmisBizConstants.XW_OP_TYPE_08)) {
                //08：惠享贷共借人签约
                if (StringUtils.isBlank(certNo)) {
                    throw new Exception("共借人身份证号必填！");
                } else if (StringUtils.isBlank(imageSerno1) || StringUtils.isBlank(imageSerno2)) {
                    throw new Exception("影像流水号必填！");
                } else if (StringUtils.isBlank(loanContNo)) {
                    throw new Exception("借款合同号必填！");
                }
                //设置查询参数
                Map queryMap = new HashMap();
                queryMap.put("certNo", certNo);//共借人身份证号
                queryMap.put("ContNo", loanContNo);//借款合同号
                queryMap.put("imageSerno1", imageSerno1);//影像流水号
                queryMap.put("imageSerno2", imageSerno2);//影像流水号
                result = lmtCobInfoMapper.updateSignStateByContNo(queryMap);
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, result);
                if (result < 1) {//更新失败
                    throw new Exception("更新担保合同签约状态失败！");
                }
            }

        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key,
                    DscmsEnum.TRADE_CODE_XDHT0012.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012DataRespDto));
        return xdht0012DataRespDto;
    }

    /**
     * 生成担保合同的方法
     */
    private String genGuarContPdf(GrtGuarCont grtGuarCont, CtrLoanCont ctrLoanCont2) {
        String pdfurl = "";//返回的格式为pdfurl=PdfFilename#ip#port#username#password#path
        String guar_cont_no = grtGuarCont.getGuarContNo();
        String GuarContType = grtGuarCont.getGuarContType();// 担保合同类型
        String guarWay = grtGuarCont.getGuarWay();//担保方式
        String pldContType = grtGuarCont.getPldContType();//质押合同类型
        String cusId = ctrLoanCont2.getCusId();
        String contNo = ctrLoanCont2.getContNo();

        String TempleteName = StringUtils.EMPTY;// 合同模板名称

        if ("A".equals(GuarContType)) {//A-一般担保合同
            if ("20".equals(guarWay)) {//质押
                if ("01".equals(pldContType)) {//动产质押
                    TempleteName = "ybdczydbht.cpt";// 担保类2021年版本
                } else if ("02".equals(pldContType)) {//权利质押
                    TempleteName = "ybdczydbht.cpt";// 担保类2021年版本
                } else {//股权质押
                    TempleteName = "ybdczydbht.cpt";// 担保类2021年版本
                }
            } else if ("30".equals(guarWay)) {
                TempleteName = "line_ybbzdbht.cpt";// 合同模板名字 一般保证担保合同
            } else {
                TempleteName = "ybdczydbht.cpt";// 担保类2021年版本
            }
        } else {//B-最高额担保
            if ("20".equals(guarWay)) {//质押
                TempleteName = "zgeqlzydbht.cpt";// B 最高额担保合同
            } else if ("30".equals(guarWay)) {
                TempleteName = "line_zgebzdbht.cpt";// 合同模板名字 最高额保证担保合同
            } else {
                TempleteName = "zgedydbht.cpt";// B 最高额担保合同
            }
        }

        String saveFileName = "dbht_" + guar_cont_no + "";//担保合同另保存名字 （模板名字+担保合同号）
        //查询服务器配置
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("pkId", "00001");
        ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
        List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
        CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);

        //查询存储地址信息
        String ip = cfgGenerateTempFileDto.getLoginIp();
        String port = cfgGenerateTempFileDto.getLoginPort();
        String username = cfgGenerateTempFileDto.getLoginUsername();
        String password = cfgGenerateTempFileDto.getLoginPwd();
        String path = cfgGenerateTempFileDto.getFilePath();
        String url = cfgGenerateTempFileDto.getMemo();

        //调用帆软的生成pdf的方法
        //1、传入帆软报表生成需要的参数
        HashMap<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put("guarContNo", guar_cont_no);
        parameterMap.put("contNo", contNo);
        parameterMap.put("cusId", cusId);
        //传入公共参数
        parameterMap.put("TempleteName", TempleteName);//模板名称（附带路径）
        parameterMap.put("saveFileName", saveFileName);//待生成的PDF文件名称
        parameterMap.put("path", path);
        //2、调用公共方法生成pdf
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if (StringUtils.isEmpty(url)) {
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            // 生成PDF文件
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                frptPdfArgsDto.setNewFileName(saveFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(guar_cont_no);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                //HttpEntity<HashMap> entity = new HttpEntity<HashMap>(parameterMap,headers);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        pdfurl = saveFileName + ".pdf#" + ip + "#" + port + "#" + username + "#" + password + "#" + path;
        return pdfurl;
    }

    /**
     * 生成借款合同的方法
     */
    private String genContPdf(CtrLoanCont ctrLoanCont) {
        String pdfurl = "";//返回的格式为pdfurl=PdfFilename#ip#port#username#password#path
        String loanContNo = ctrLoanCont.getContNo();// 借款合同号
        String contType = ctrLoanCont.getContType();// 合同类型
        String TempleteName = StringUtils.EMPTY;// 合同模板名称

        if ("1".equals(contType)) {//一般借款合同
            TempleteName = "line_xwybjkht.cpt";// 借款合同（2021年版，小微金融）
        } else {//最高额借款合同
            TempleteName = "line_xwzgejkht.cpt";// 最高额借款合同（2021年版，小微金融）
        }
        String saveFileName = "jkht_" + loanContNo + "";//合同另保存名字 （模板名字+担保合同号）
        //查询服务器配置
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("pkId", "00001");
        ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
        List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
        CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);

        //查询存储地址信息
        String ip = cfgGenerateTempFileDto.getLoginIp();
        String port = cfgGenerateTempFileDto.getLoginPort();
        String username = cfgGenerateTempFileDto.getLoginUsername();
        String password = cfgGenerateTempFileDto.getLoginPwd();
        String path = cfgGenerateTempFileDto.getFilePath();
        String url = cfgGenerateTempFileDto.getMemo();

        //调用帆软的生成pdf的方法
        //1、传入帆软报表生成需要的参数
        HashMap<String, Object> parameterMap = new HashMap<String, Object>();
        parameterMap.put("contNo", loanContNo);
        //传入公共参数
        parameterMap.put("TempleteName", TempleteName);//模板名称（附带路径）
        parameterMap.put("saveFileName", saveFileName);//待生成的PDF文件名称
        parameterMap.put("path", path);
        //2、调用公共方法生成pdf
        try {
            //2、调用公共方法生成pdf
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if (StringUtils.isEmpty(url)) {
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            // 生成PDF文件
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                frptPdfArgsDto.setNewFileName(saveFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(loanContNo);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                //HttpEntity<HashMap> entity = new HttpEntity<HashMap>(parameterMap,headers);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        pdfurl = saveFileName + ".pdf#" + ip + "#" + port + "#" + username + "#" + password + "#" + path;
        return pdfurl;
    }

    /********
     *根据贷款期限查询lpr利率
     * *********/
    public BigDecimal queryLprRate(BigDecimal term) {
        //查询下lpr利率
        BigDecimal curtLprRate = new BigDecimal("0");
        try {
            String newVal = "A1";//
            if (term.compareTo(new BigDecimal(60)) > 0) {
                newVal = "A2";
            }
            Map rtnData = iqpLoanAppService.getLprRate(newVal);
            if (rtnData != null && rtnData.containsKey("rate")) {
                curtLprRate = new BigDecimal(rtnData.get("rate").toString());
            } else {
                throw BizException.error("", "", "未查询到LPR利率,请检查!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return curtLprRate;
    }

    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.domain.PvpAuthorize
     * @author 王玉坤
     * @date 2021/9/29 21:34
     * @version 1.0.0
     * @desc 构建出账授权信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private PvpAuthorize buildPvpAuthorize(PvpLoanApp pvpLoanApp) {
        PvpAuthorize pvpAuthorize = new PvpAuthorize();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        //授权申请交易流水号
        String tranSerNo = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_ACC_NO, new HashMap<>());
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(pvpLoanApp, pvpAuthorize);
        pvpAuthorize.setAuthStatus("0");
        pvpAuthorize.setTranId("CRE400");//交易编号
        pvpAuthorize.setTranSerno(tranSerNo);//交易流水号
        pvpAuthorize.setContNo(pvpLoanApp.getContNo());//合同号
        pvpAuthorize.setTranAmt(pvpLoanApp.getPvpAmt());//出账金额
        pvpAuthorize.setTranDate(openDay);
        return pvpAuthorize;
    }


    /**
     * @param pvpLoanApp
     * @return cn.com.yusys.yusp.domain.AccLoan
     * @author 王玉坤
     * @date 2021/9/29 21:46
     * @version 1.0.0
     * @desc
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private AccLoan buildAccLoan(PvpLoanApp pvpLoanApp) {
        AccLoan accLoan = new AccLoan();
        cn.com.yusys.yusp.commons.util.BeanUtils.beanCopy(pvpLoanApp, accLoan);
        //未出帐6
        accLoan.setAccStatus("6");
        //五级分类默认正常
        Map map = pvpLoanAppService.getFiveAndTenClass(pvpLoanApp.getCusId());
        accLoan.setFiveClass((String) map.get("fiveClass"));
        //出账金额
        accLoan.setLoanAmt(pvpLoanApp.getPvpAmt());
        //币种
        accLoan.setContCurType(pvpLoanApp.getCurType());
        // 人民币金额
        accLoan.setExchangeRmbBal(pvpLoanApp.getPvpAmt());
        //折合人民币金额
        accLoan.setExchangeRmbAmt(pvpLoanApp.getPvpAmt());
        // 汇率
        accLoan.setExchangeRate(BigDecimal.ONE);
        // 币种
        accLoan.setContCurType("CNY");
        //科目号
        String SubjectNo = pvpLoanApp.getLoanSubjectNo();
        accLoan.setSubjectNo(SubjectNo);
        if (StringUtil.isNotEmpty(SubjectNo)) {//科目号不为空
            //科目名称
            accLoan.setSubjectName(calHxkuaijilb(SubjectNo));
        }
        //基准利率  rulingIr   取 LPR利率
        accLoan.setRulingIr(pvpLoanApp.getCurtLprRate());
        return accLoan;
    }

    /**
     * @param accLoan
     * @return void
     * @author 王玉坤
     * @date 2021/9/29 22:36
     * @version 1.0.0
     * @desc 通知小微公众号信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void sendXwh001(AccLoan accLoan) {
        logger.info("推送小微公众号信息开始，借据号【{}】", accLoan.getBillNo());
        // 小微公众号信息
        Xwh001ReqDto xwh001ReqDto = new Xwh001ReqDto();
        // 客户基本对象信息
        ResultDto<CusBaseDto> cusBaseDto = null;
        // 客户基本对象信息体
        CusBaseDto baseDto = null;
        // 客户联系对象信息
        ResultDto<CusIndivContactDto> cusIndivContactDtoResultDto = null;
        // 客户联系对象信息体
        CusIndivContactDto cusIndivContactDto = null;
        try {
            logger.info("根据客户号【{}】查询客户信息开始！", accLoan.getCusId());
            cusBaseDto = icusClientService.queryCusBaseByCusId(accLoan.getCusId());
            logger.info("根据客户号【{}】查询客户信息结束！", accLoan.getCusId());
            if (Objects.isNull(cusBaseDto) || Objects.isNull(cusBaseDto.getData())) {
                throw BizException.error(null, null, "未查询到客户基本信息！");
            } else {
                baseDto = cusBaseDto.getData();
            }
            // 借据号
            xwh001ReqDto.setBillNo(accLoan.getBillNo());
            // 客户号
            xwh001ReqDto.setCusId(accLoan.getCusId());
            // 客户名称
            xwh001ReqDto.setCusName(accLoan.getCusName());
            // 客户证件号
            xwh001ReqDto.setIdCard(baseDto.getCertCode());
            // 放款金额
            xwh001ReqDto.setLoanAmount(accLoan.getLoanAmt());
            // 期限
            xwh001ReqDto.setMonth(Integer.valueOf(accLoan.getLoanTerm()));
            // 借据开始日
            xwh001ReqDto.setStartDate(accLoan.getLoanStartDate());
            // 借据到期日
            xwh001ReqDto.setEndDate(accLoan.getLoanEndDate());
            // 推荐客户经理号
            xwh001ReqDto.setRecommendId(accLoan.getManagerId());
            // 产品名称
            xwh001ReqDto.setProductName(accLoan.getPrdName());
            // 客户手机号
            xwh001ReqDto.setPhone(cusIndivContactDto.getMobile());

            // 发送小微公众号信息
            xwh001Service.xwh001(xwh001ReqDto);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("推送小微公众号信息异常，借据号【{}】", accLoan.getBillNo());
        }
    }

    /******
     *初始化统计分类信息
     * ******/
    public PvpLoanApp initTjflInfo(PvpLoanApp pvpLoanApp, CtrLoanCont ctrLoanCont) {
        logger.info("************************初始化统计分类信息开始***************************");
        try {
            //1、获取产品编号
            String prdId = ctrLoanCont.getPrdId();
            int term = ctrLoanCont.getContTerm();
            //2、查看产品是不是为消费性的贷款  01--消费贷  02-经营贷
            String prdType = checkPrdType(prdId);
            /************************查询单位所属行业开始***************************/
            logger.info("************************查询单位所属行业开始***************************");
            String cusId = ctrLoanCont.getCusId();
            // 查询客户基本信息
            Xdkh0001DataRespDto xdkh0001DataRespDto = queryXdkh0001DataRespDto(cusId);
            String indivComFld = xdkh0001DataRespDto.getIndivComTrade();//单位所属行业
            String agriFlg = xdkh0001DataRespDto.getIsAgri();//是否为农户 1-是 0-否
            String cusType = xdkh0001DataRespDto.getCusType();//客户类别
            /************************查询单位所属行业结束**********************************/

            /***********贷款投向*************/
            logger.info("************************贷款投向***************************");
            if (StringUtil.isNotEmpty(indivComFld) && indivComFld.startsWith("A")) {
                if (indivComFld.startsWith("A01")) {
                    pvpLoanApp.setAgriLoanTer("F01");// 农业贷款
                } else if (indivComFld.startsWith("A02")) {
                    pvpLoanApp.setAgriLoanTer("F02");// 林业贷款
                } else if (indivComFld.startsWith("A03")) {
                    pvpLoanApp.setAgriLoanTer("F03");// 畜牧业贷款
                } else if (indivComFld.startsWith("A04")) {
                    pvpLoanApp.setAgriLoanTer("F04");// 渔业贷款
                } else if (indivComFld.startsWith("A05")) {
                    pvpLoanApp.setAgriLoanTer("F05");// 农林牧渔服务业贷款
                }
            } else {
                if ("1".equals(agriFlg)) {//农户
                    if ("02".equals(prdType)) {// 个人经营性贷款\惠享贷
                        pvpLoanApp.setAgriLoanTer("F13");// 它涉农贷款
                    } else if ("01".equals(prdType)) {// 个人消费贷款
                        pvpLoanApp.setAgriLoanTer("F16");// 其它消费贷款
                    } else if ("022019".equals(prdId)) {// 个人汽车按揭贷款
                        pvpLoanApp.setAgriLoanTer("F16");// 其它消费贷款
                    }
                } else {
                    pvpLoanApp.setAgriLoanTer("F14");// 非涉农贷款
                }
            }

            /*****************贷款类别、借款用途类型*******************/
            logger.info("************************贷款类别、借款用途类型***************************");
            if ("02".equals(prdType)) {// 个人经营性贷款\惠享贷
                pvpLoanApp.setLoanTypeDetail("07");//贷款类别细分  经营-其它
                pvpLoanApp.setLoanUseType("A08");//借款用途类型
            } else if ("01".equals(prdType)) {// 个人消费贷款
                pvpLoanApp.setLoanTypeDetail("05");//贷款类别细分 消费-其它
                pvpLoanApp.setLoanUseType("C04");//借款用途类型
            } else if ("022019".equals(prdId)) {// 个人汽车按揭贷款
                pvpLoanApp.setLoanTypeDetail("02");//贷款类别细分 消费-汽车贷款
                pvpLoanApp.setLoanUseType("C04");//借款用途类型
            }

            /*****************贷款科目、贷款科目名称*******************/
            logger.info("****贷款科目贷款科目名称*****产品类型:" + prdType + "是否农户:" + agriFlg + "客户类型:" + cusType);
            if ("02".equals(prdType)) {// 个人经营性贷款\惠享贷
                if ("1".equals(agriFlg)) {//农户
                    if (term <= 12) {// 短期
                        if ("120".equals(cusType)) {// 个体工商户
                            pvpLoanApp.setLoanSubjectNo("13011305");//短期农村个体工商户贷款本金
                        } else {
                            pvpLoanApp.setLoanSubjectNo("13011405");//短期农户其他生产经营贷款本金
                        }
                    } else {// 中长期
                        if ("120".equals(cusType)) {// 个体工商户
                            pvpLoanApp.setLoanSubjectNo("13013305");//中长期农村个体工商户贷款本金
                        } else {
                            pvpLoanApp.setLoanSubjectNo("13013405");//中长期农户其他生产经营贷款本金
                        }
                    }
                } else if ("0".equals(agriFlg)) {//非农户
                    if (term <= 12) {// 短期
                        if (StringUtil.isNotEmpty(indivComFld) && indivComFld.startsWith("A")) {//农、林、牧、渔业
                            pvpLoanApp.setLoanSubjectNo("13040705");//短期非农个人农林牧渔贷款本金
                        } else {
                            if ("120".equals(cusType)) {// 个体工商户
                                pvpLoanApp.setLoanSubjectNo("13040805");//短期非农个体工商户贷款本金
                            } else {
                                pvpLoanApp.setLoanSubjectNo("13041005");//短期非农个人生产经营性贷款本金
                            }
                        }
                    } else {// 中长期
                        if (StringUtil.isNotEmpty(indivComFld) && indivComFld.startsWith("A")) {//农、林、牧、渔业
                            pvpLoanApp.setLoanSubjectNo("13042705");//中长期非农个人农林牧渔贷款本金
                        } else {
                            if ("120".equals(cusType)) {// 个体工商户
                                pvpLoanApp.setLoanSubjectNo("13042805");//中长期非农个体工商户贷款本金
                            } else {
                                pvpLoanApp.setLoanSubjectNo("13043005");//中长期非农个人生产经营性贷款本金
                            }
                        }
                    }
                }
            } else if ("01".equals(prdType) || "022019".equals(prdId)) {// 个人消费贷款+个人汽车按揭贷款
                if (term <= 12) {// 短期
                    if ("1".equals(agriFlg)) {//农户
                        if ("022019".equals(prdId)) {
                            pvpLoanApp.setLoanSubjectNo("13011705");//短期农户汽车消费贷款本金
                        } else {
                            pvpLoanApp.setLoanSubjectNo("13011805");//短期农户其他消费贷款本金
                        }
                    } else {//非农户
                        if ("022019".equals(prdId)) {
                            pvpLoanApp.setLoanSubjectNo("13040305");//短期非农个人汽车消费贷款本金
                        } else {
                            pvpLoanApp.setLoanSubjectNo("13041105");//短期非农个人其他消费贷款本金
                        }
                    }
                } else {// 中长期
                    if ("1".equals(agriFlg)) {//农户
                        if ("022019".equals(prdId)) {
                            pvpLoanApp.setLoanSubjectNo("13013705");//中长期农户汽车消费贷款本金
                        } else {
                            pvpLoanApp.setLoanSubjectNo("13013805");//中长期农户其他消费贷款本金
                        }
                    } else {//非农户
                        if ("022019".equals(prdId)) {
                            pvpLoanApp.setLoanSubjectNo("13042305");//中长期非农个人汽车消费贷款本金
                        } else {
                            pvpLoanApp.setLoanSubjectNo("13043105");//中长期非农个人其他消费贷款本金
                        }
                    }
                }
            }
            pvpLoanApp.setIsSteelLoan("0");//是否钢贸
            pvpLoanApp.setIsStainlessLoan("0");//是否不锈钢行业贷款
            pvpLoanApp.setIsPovertyReliefLoan("0");//是否扶贫贴息贷款
            pvpLoanApp.setIsLaborIntenSbsyLoan("0");//是否劳动密集型小企业贴息贷款
            pvpLoanApp.setGoverSubszHouseLoan("00");//保障性安居工程贷款
            pvpLoanApp.setIsCphsRurDelpLoan("0");//是否农村综合开发贷款标志
        } catch (Exception e) {
            logger.error("初始化统计分类信息失败");
            e.printStackTrace();
        }
        logger.info("************初始化统计分类信息返回信息【{}】", JSON.toJSONString(pvpLoanApp));
        return pvpLoanApp;
    }

    /******
     *查看产品是不是为消费性的贷款
     * 1-消费贷款
     * 2--经营性
     * ******/
    public String checkPrdType(String prdId) {
        String prdType = "02";//贷款品种类型  01-消费 02-经营
        try {
            logger.info("***********调用iCmisCfgClientService查询产品类别*START**************产品编号" + prdId);
            ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
            String prdCode = prdresultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                if (CfgPrdBasicinfoDto != null) {
                    prdType = CfgPrdBasicinfoDto.getPrdType();
                }
            }

            /****
             * 08小微经营性
             * 09小微消费性
             * 10零售按揭类
             * 11零售非按揭类
             * 12互联网贷款
             * ****/
            if ("09".equals(prdType) || "10".equals(prdType) || "11".equals(prdType) || "12".equals(prdType)) {//消费贷款
                prdType = "01";//消费贷款
            } else {//否则为 08小微经营性 或 对公贷款
                prdType = "02";//经营性贷款
            }
        } catch (Exception e) {
            logger.error("查看产品是不是为消费性的贷款失败");
            e.printStackTrace();
        }
        return prdType;
    }

    /**
     * 客户基本信息
     *
     * @param cusId
     * @return
     */
    private Xdkh0001DataRespDto queryXdkh0001DataRespDto(String cusId) {
        Xdkh0001DataReqDto xdkh0001DataReqDto = new Xdkh0001DataReqDto();//请求Dto：查询个人客户基本信息
        Xdkh0001DataRespDto xdkh0001DataRespDto = null;//响应Dto：查询个人客户基本信息
        try {
            xdkh0001DataReqDto.setCusId(cusId);
            xdkh0001DataReqDto.setQueryType("01");

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
            ResultDto<Xdkh0001DataRespDto> xdkh0001DataRespDtoResultDto = dscmsCusClientService.xdkh0001(xdkh0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataRespDtoResultDto));

            String xdkh0001Code = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String xdkh0001Meesage = Optional.ofNullable(xdkh0001DataRespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0001DataRespDtoResultDto.getCode())) {
                //  获取相关的值并解析
                xdkh0001DataRespDto = xdkh0001DataRespDtoResultDto.getData();
            } else {
                //  抛出错误异常
                throw BizException.error(null, xdkh0001Code, xdkh0001Meesage);
            }
        } catch (Exception e) {
            logger.error("查看产品是不是为消费性的贷款失败");
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value);
        return xdkh0001DataRespDto;
    }

    /**
     * @方法名称: calHxkuaijilb
     * @方法描述: 通过科目号查询对应的科目名称核心会计类别
     * @参数与返回说明:
     * @算法描述:
     * @创建人: 王玉坤
     * @创建时间: 2021-06-22 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public String calHxkuaijilb(String subJectNo) {
        String subjectName = "";
        try {
            logger.info("***********通过科目号查询对应的科目名称核心会计类别*START**************");
            CfgAccountClassChooseDto cfgAccountClassChooseDto = iCmisCfgClientService.queryHxAccountClassByAcccountClass(subJectNo).getData();
            logger.info("***********通过科目号查询对应的科目名称核心会计类别*END**************");
            subjectName = cfgAccountClassChooseDto.getAccountClassName();//获取科目号
        } catch (Exception e) {
            logger.error("查看产品是不是为消费性的贷款失败");
        }
        return subjectName;
    }

}
