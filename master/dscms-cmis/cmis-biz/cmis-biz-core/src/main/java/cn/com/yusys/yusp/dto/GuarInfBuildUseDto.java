package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBuildUse
 * @类描述: guar_inf_build_use数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:43:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfBuildUseDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 土地证（不动产权证号） **/
	private String landNo;
	
	/** 土地使用权性质 STD_ZB_LAND_TYPE **/
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_GAIN **/
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	private String landUseBeginDate;
	
	/** 土地使用权使用年限到期日期 **/
	private String landUseEndDate;
	
	/** 土地用途 STD_ZB_LANDYT **/
	private String landPurp;
	
	/** 土地使用权面积 **/
	private String landUseArea;
	
	/** 闲置土地类型STD_ZB_XZTDLX **/
	private String landNotinuseType;
	
	/** 土地说明 **/
	private String landExplain;
	
	/** 所在/注册省份 **/
	private String provinceCd;
	
	/** 所在/注册市 **/
	private String cityCd;
	
	/** 所在县（区） **/
	private String countyCd;
	
	/** 土地详细地址 **/
	private String landDetailadd;
	
	/** 宗地号 **/
	private String parcelNo;
	
	/** 购买时间 **/
	private String purchaseDate;
	
	/** 购买价格（元） **/
	private java.math.BigDecimal purchaseAccnt;
	
	/** 是否有地上定着物 **/
	private String landUp;
	
	/** 定着物种类 STD_ZB_DZWZL **/
	private String landUpType;
	
	/** 地上建筑物项数 **/
	private Integer landBuildAmount;
	
	/** 定着物所有权人名称 **/
	private String landUpOwnershipName;
	
	/** 定着物所有权人范围 STD_ZB_LAND_UP_SCOPE **/
	private String landUpOwnershipScope;
	
	/** 地上定着物说明 **/
	private String landUpExplain;
	
	/** 地上定着物总面积 **/
	private String landUpAllArea;
	
	/** 使用权抵押登记证号 **/
	private String useCertNo;
	
	/** 使用权登记机关 **/
	private String useCertDep;
	
	/** 土地所在地段情况 STD_ZB_FCDDQK **/
	private String landPInfo;
	
	/** 房产类别 STD_ZB_HOUSE_TYPE **/
	private String houseType;
	
	/** 产权年限 **/
	private String housePr;
	
	/** 建筑年份 **/
	private String archYear;
	
	/** 土地使用类型 STD_ZB_PROJECT_TYPE **/
	private String landUsedType;
	
	/** 转让金拖欠金额（元） **/
	private java.math.BigDecimal transfUnpaidAmt;
	
	/** 所属地段STD_ZB_BELG_PLACE **/
	private String belongArea;
	
	/** 街道/村镇/路名 **/
	private String street;
	
	/** 押品使用情况STD_ZB_GUAR_UTIL_CASE **/
	private String guarUtilCase;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo == null ? null : landNo.trim();
	}
	
    /**
     * @return LandNo
     */	
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual == null ? null : landUseQual.trim();
	}
	
    /**
     * @return LandUseQual
     */	
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay == null ? null : landUseWay.trim();
	}
	
    /**
     * @return LandUseWay
     */	
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate == null ? null : landUseBeginDate.trim();
	}
	
    /**
     * @return LandUseBeginDate
     */	
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseEndDate
	 */
	public void setLandUseEndDate(String landUseEndDate) {
		this.landUseEndDate = landUseEndDate == null ? null : landUseEndDate.trim();
	}
	
    /**
     * @return LandUseEndDate
     */	
	public String getLandUseEndDate() {
		return this.landUseEndDate;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp == null ? null : landPurp.trim();
	}
	
    /**
     * @return LandPurp
     */	
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landUseArea
	 */
	public void setLandUseArea(String landUseArea) {
		this.landUseArea = landUseArea == null ? null : landUseArea.trim();
	}
	
    /**
     * @return LandUseArea
     */	
	public String getLandUseArea() {
		return this.landUseArea;
	}
	
	/**
	 * @param landNotinuseType
	 */
	public void setLandNotinuseType(String landNotinuseType) {
		this.landNotinuseType = landNotinuseType == null ? null : landNotinuseType.trim();
	}
	
    /**
     * @return LandNotinuseType
     */	
	public String getLandNotinuseType() {
		return this.landNotinuseType;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain == null ? null : landExplain.trim();
	}
	
    /**
     * @return LandExplain
     */	
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd == null ? null : provinceCd.trim();
	}
	
    /**
     * @return ProvinceCd
     */	
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd == null ? null : cityCd.trim();
	}
	
    /**
     * @return CityCd
     */	
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd == null ? null : countyCd.trim();
	}
	
    /**
     * @return CountyCd
     */	
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param landDetailadd
	 */
	public void setLandDetailadd(String landDetailadd) {
		this.landDetailadd = landDetailadd == null ? null : landDetailadd.trim();
	}
	
    /**
     * @return LandDetailadd
     */	
	public String getLandDetailadd() {
		return this.landDetailadd;
	}
	
	/**
	 * @param parcelNo
	 */
	public void setParcelNo(String parcelNo) {
		this.parcelNo = parcelNo == null ? null : parcelNo.trim();
	}
	
    /**
     * @return ParcelNo
     */	
	public String getParcelNo() {
		return this.parcelNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate == null ? null : purchaseDate.trim();
	}
	
    /**
     * @return PurchaseDate
     */	
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return PurchaseAccnt
     */	
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param landUp
	 */
	public void setLandUp(String landUp) {
		this.landUp = landUp == null ? null : landUp.trim();
	}
	
    /**
     * @return LandUp
     */	
	public String getLandUp() {
		return this.landUp;
	}
	
	/**
	 * @param landUpType
	 */
	public void setLandUpType(String landUpType) {
		this.landUpType = landUpType == null ? null : landUpType.trim();
	}
	
    /**
     * @return LandUpType
     */	
	public String getLandUpType() {
		return this.landUpType;
	}
	
	/**
	 * @param landBuildAmount
	 */
	public void setLandBuildAmount(Integer landBuildAmount) {
		this.landBuildAmount = landBuildAmount;
	}
	
    /**
     * @return LandBuildAmount
     */	
	public Integer getLandBuildAmount() {
		return this.landBuildAmount;
	}
	
	/**
	 * @param landUpOwnershipName
	 */
	public void setLandUpOwnershipName(String landUpOwnershipName) {
		this.landUpOwnershipName = landUpOwnershipName == null ? null : landUpOwnershipName.trim();
	}
	
    /**
     * @return LandUpOwnershipName
     */	
	public String getLandUpOwnershipName() {
		return this.landUpOwnershipName;
	}
	
	/**
	 * @param landUpOwnershipScope
	 */
	public void setLandUpOwnershipScope(String landUpOwnershipScope) {
		this.landUpOwnershipScope = landUpOwnershipScope == null ? null : landUpOwnershipScope.trim();
	}
	
    /**
     * @return LandUpOwnershipScope
     */	
	public String getLandUpOwnershipScope() {
		return this.landUpOwnershipScope;
	}
	
	/**
	 * @param landUpExplain
	 */
	public void setLandUpExplain(String landUpExplain) {
		this.landUpExplain = landUpExplain == null ? null : landUpExplain.trim();
	}
	
    /**
     * @return LandUpExplain
     */	
	public String getLandUpExplain() {
		return this.landUpExplain;
	}
	
	/**
	 * @param landUpAllArea
	 */
	public void setLandUpAllArea(String landUpAllArea) {
		this.landUpAllArea = landUpAllArea == null ? null : landUpAllArea.trim();
	}
	
    /**
     * @return LandUpAllArea
     */	
	public String getLandUpAllArea() {
		return this.landUpAllArea;
	}
	
	/**
	 * @param useCertNo
	 */
	public void setUseCertNo(String useCertNo) {
		this.useCertNo = useCertNo == null ? null : useCertNo.trim();
	}
	
    /**
     * @return UseCertNo
     */	
	public String getUseCertNo() {
		return this.useCertNo;
	}
	
	/**
	 * @param useCertDep
	 */
	public void setUseCertDep(String useCertDep) {
		this.useCertDep = useCertDep == null ? null : useCertDep.trim();
	}
	
    /**
     * @return UseCertDep
     */	
	public String getUseCertDep() {
		return this.useCertDep;
	}
	
	/**
	 * @param landPInfo
	 */
	public void setLandPInfo(String landPInfo) {
		this.landPInfo = landPInfo == null ? null : landPInfo.trim();
	}
	
    /**
     * @return LandPInfo
     */	
	public String getLandPInfo() {
		return this.landPInfo;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType == null ? null : houseType.trim();
	}
	
    /**
     * @return HouseType
     */	
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param housePr
	 */
	public void setHousePr(String housePr) {
		this.housePr = housePr == null ? null : housePr.trim();
	}
	
    /**
     * @return HousePr
     */	
	public String getHousePr() {
		return this.housePr;
	}
	
	/**
	 * @param archYear
	 */
	public void setArchYear(String archYear) {
		this.archYear = archYear == null ? null : archYear.trim();
	}
	
    /**
     * @return ArchYear
     */	
	public String getArchYear() {
		return this.archYear;
	}
	
	/**
	 * @param landUsedType
	 */
	public void setLandUsedType(String landUsedType) {
		this.landUsedType = landUsedType == null ? null : landUsedType.trim();
	}
	
    /**
     * @return LandUsedType
     */	
	public String getLandUsedType() {
		return this.landUsedType;
	}
	
	/**
	 * @param transfUnpaidAmt
	 */
	public void setTransfUnpaidAmt(java.math.BigDecimal transfUnpaidAmt) {
		this.transfUnpaidAmt = transfUnpaidAmt;
	}
	
    /**
     * @return TransfUnpaidAmt
     */	
	public java.math.BigDecimal getTransfUnpaidAmt() {
		return this.transfUnpaidAmt;
	}
	
	/**
	 * @param belongArea
	 */
	public void setBelongArea(String belongArea) {
		this.belongArea = belongArea == null ? null : belongArea.trim();
	}
	
    /**
     * @return BelongArea
     */	
	public String getBelongArea() {
		return this.belongArea;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}
	
    /**
     * @return Street
     */	
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param guarUtilCase
	 */
	public void setGuarUtilCase(String guarUtilCase) {
		this.guarUtilCase = guarUtilCase == null ? null : guarUtilCase.trim();
	}
	
    /**
     * @return GuarUtilCase
     */	
	public String getGuarUtilCase() {
		return this.guarUtilCase;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}