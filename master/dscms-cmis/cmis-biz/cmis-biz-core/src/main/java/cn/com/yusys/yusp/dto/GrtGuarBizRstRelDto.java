package cn.com.yusys.yusp.dto;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GrtGuarBizRstRel
 * @类描述: grt_guar_biz_rst_rel数据实体类
 * @功能描述: 
 * @创建人: 刘权
 * @创建时间: 2021-04-20 17:02:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GrtGuarBizRstRelDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 担保合同流水号  **/
	private String guarPkId;
	
	/** 担保合同编号 **/
	private String guarContNo;
	
	/** 本次担保金额 **/
	private java.math.BigDecimal guarAmt;
	
	/** 是否追加担保 STD_ZB_YES_NO **/
	private String isAddGuar;
	
	/** 是否阶段性担保 STD_ZB_YES_NO **/
	private String isPerGur;
	
	/** 关联关系 STD_ZB_CORRE_REL **/
	private String correRel;
	
	/** 关联类型 **/
	private String rType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;

	/** 担保合同中文合同编号 **/
	private String guarContCnNo;

	/** 创建时间 **/
	private Date createTime;

	/** 修改时间 **/
	private Date updateTime;

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/** 客户编号 **/
	private String cusId;

	/** 是否追加担保  STD_ZB_YES_NO **/
	private String isSuperaddGuar;

	/** 担保合同类型 STD_ZB_GRT_TYP **/
	private String guarContType;

	/** 担保合同担保方式 STD_ZB_GUAR_WAY **/
	private String guarWay;

	/** 保证形式 STD_ZB_ASSURE_MODAL **/
	private String assureModal;

	/** 保证方式 **/
	private String assureWay;

	/** 纸质合同编号 **/
	private String paperContNo;

	/** 是否在线抵押 **/
	private String isOnlinePld;

	/** 抵质押合同类型 **/
	private String pldContType;

	/** 是否授信项下 STD_ZB_YES_NO **/
	private String isUnderLmt;

	/** 抵押顺位 **/
	private String pldOrder;

	/** 是否浮动抵押 **/
	private String isFloatPld;

	/** 借款人编号 **/
	private String borrowerId;

	/** 贷款卡号 **/
	private String lnCardNo;

	/** 币种 **/
	private String curType;

	/** 是否合格担保 STD_ZB_YES_NO **/
	private String isQualifiedGuar;

	/** 期限类型 STD_ZB_TERM_TYP **/
	private String termType;

	/** 担保期限 **/
	private Integer guarTerm;

	/** 担保起始日 **/
	private String guarStartDate;

	/** 担保终止日 **/
	private String guarEndDate;

	/** 签订日期 **/
	private String signDate;

	/** 质押类型 STD_ZB_IMN_TYPE **/
	private String imnType;

	/** 担保合同状态 STD_ZB_GRT_ST **/
	private String guarContState;

	/** 担保双录编号 **/
	private String guarIserchNo;

	/** 备注 **/
	private String remark;

	/** 争议借据方式选项 **/
	private String billDispupeOpt;

	/** 法院所在地 **/
	private String courtAddr;

	/** 仲裁委员会地点 **/
	private String arbitrateAddr;

	/** 其他方式 **/
	private String otherOpt;

	/** 仲裁机构 **/
	private String arbitrateBch;

	/** 合同份数 **/
	private Integer contQnt;

	/** 甲方执合同份数 **/
	private Integer contQntOwner;

	/** 乙方执合同份数 **/
	private Integer contQntPartyB;

	/** 丙方执合同份数 **/
	private Integer contQntPartyC;

	/** 丁方执合同份数 **/
	private Integer contQntPartyD;

	/** 戊方执合同份数 **/
	private Integer contQntPartyE;

	/** 其他主合同 **/
	private String otherMainCont;

	/** 确认最高债权额方式 STD_ZB_MAX_CLAIM_TP **/
	private String maxClaimTp;

	/** 最高债权额 **/
	private java.math.BigDecimal maxClaimAmt;

	/** 其他约定事项 **/
	private String otherAgreedEvent;

	/** 合同打印次数 **/
	private Integer contPrintNum;

	/** 申请类型 STD_ZB_GRT_APP_TYP **/
	private String approveType;

	/** 注销日期 **/
	private String logoutDate;

	/** 担保人编号 **/
	private String guarantorId;

	/** 担保人名称 **/
	private String assureName;

	/** 担保人证件类型 **/
	private String assureCertType;

	/** 业务条线 **/
	private String bizLine;

	/** 担保人证件号码 **/
	private String assureCertCode;

	/** 客户名称 **/
	private String cusName;

	/** 是否将存量债务纳入担保范围 **/
	private String isDebtGuar;

	/** 授信额度编号 **/
	private String lmtAccNo;

	/** 批复编号 **/
	private String replyNo;

	/** 合同金额 **/
	private BigDecimal contAmt;

	/** 合同起始日 **/
	private String contStartDate;

	/** 合同结束日 **/
	private String contEndDate;

	/** 合同状态 **/
	private String contStatus;

	public String getrType() {
		return rType;
	}

	public void setrType(String rType) {
		this.rType = rType;
	}

	public String getGuarContCnNo() {
		return guarContCnNo;
	}

	public void setGuarContCnNo(String guarContCnNo) {
		this.guarContCnNo = guarContCnNo;
	}

	public String getCusId() {
		return cusId;
	}

	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	public String getIsSuperaddGuar() {
		return isSuperaddGuar;
	}

	public void setIsSuperaddGuar(String isSuperaddGuar) {
		this.isSuperaddGuar = isSuperaddGuar;
	}

	public String getGuarContType() {
		return guarContType;
	}

	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType;
	}

	public String getGuarWay() {
		return guarWay;
	}

	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay;
	}

	public String getAssureModal() {
		return assureModal;
	}

	public void setAssureModal(String assureModal) {
		this.assureModal = assureModal;
	}

	public String getAssureWay() {
		return assureWay;
	}

	public void setAssureWay(String assureWay) {
		this.assureWay = assureWay;
	}

	public String getPaperContNo() {
		return paperContNo;
	}

	public void setPaperContNo(String paperContNo) {
		this.paperContNo = paperContNo;
	}

	public String getIsOnlinePld() {
		return isOnlinePld;
	}

	public void setIsOnlinePld(String isOnlinePld) {
		this.isOnlinePld = isOnlinePld;
	}

	public String getPldContType() {
		return pldContType;
	}

	public void setPldContType(String pldContType) {
		this.pldContType = pldContType;
	}

	public String getIsUnderLmt() {
		return isUnderLmt;
	}

	public void setIsUnderLmt(String isUnderLmt) {
		this.isUnderLmt = isUnderLmt;
	}

	public String getPldOrder() {
		return pldOrder;
	}

	public void setPldOrder(String pldOrder) {
		this.pldOrder = pldOrder;
	}

	public String getIsFloatPld() {
		return isFloatPld;
	}

	public void setIsFloatPld(String isFloatPld) {
		this.isFloatPld = isFloatPld;
	}

	public String getBorrowerId() {
		return borrowerId;
	}

	public void setBorrowerId(String borrowerId) {
		this.borrowerId = borrowerId;
	}

	public String getLnCardNo() {
		return lnCardNo;
	}

	public void setLnCardNo(String lnCardNo) {
		this.lnCardNo = lnCardNo;
	}

	public String getCurType() {
		return curType;
	}

	public void setCurType(String curType) {
		this.curType = curType;
	}

	public String getIsQualifiedGuar() {
		return isQualifiedGuar;
	}

	public void setIsQualifiedGuar(String isQualifiedGuar) {
		this.isQualifiedGuar = isQualifiedGuar;
	}

	public String getTermType() {
		return termType;
	}

	public void setTermType(String termType) {
		this.termType = termType;
	}

	public Integer getGuarTerm() {
		return guarTerm;
	}

	public void setGuarTerm(Integer guarTerm) {
		this.guarTerm = guarTerm;
	}

	public String getGuarStartDate() {
		return guarStartDate;
	}

	public void setGuarStartDate(String guarStartDate) {
		this.guarStartDate = guarStartDate;
	}

	public String getGuarEndDate() {
		return guarEndDate;
	}

	public void setGuarEndDate(String guarEndDate) {
		this.guarEndDate = guarEndDate;
	}

	public String getSignDate() {
		return signDate;
	}

	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

	public String getImnType() {
		return imnType;
	}

	public void setImnType(String imnType) {
		this.imnType = imnType;
	}

	public String getGuarContState() {
		return guarContState;
	}

	public void setGuarContState(String guarContState) {
		this.guarContState = guarContState;
	}

	public String getGuarIserchNo() {
		return guarIserchNo;
	}

	public void setGuarIserchNo(String guarIserchNo) {
		this.guarIserchNo = guarIserchNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBillDispupeOpt() {
		return billDispupeOpt;
	}

	public void setBillDispupeOpt(String billDispupeOpt) {
		this.billDispupeOpt = billDispupeOpt;
	}

	public String getCourtAddr() {
		return courtAddr;
	}

	public void setCourtAddr(String courtAddr) {
		this.courtAddr = courtAddr;
	}

	public String getArbitrateAddr() {
		return arbitrateAddr;
	}

	public void setArbitrateAddr(String arbitrateAddr) {
		this.arbitrateAddr = arbitrateAddr;
	}

	public String getOtherOpt() {
		return otherOpt;
	}

	public void setOtherOpt(String otherOpt) {
		this.otherOpt = otherOpt;
	}

	public String getArbitrateBch() {
		return arbitrateBch;
	}

	public void setArbitrateBch(String arbitrateBch) {
		this.arbitrateBch = arbitrateBch;
	}

	public Integer getContQnt() {
		return contQnt;
	}

	public void setContQnt(Integer contQnt) {
		this.contQnt = contQnt;
	}

	public Integer getContQntOwner() {
		return contQntOwner;
	}

	public void setContQntOwner(Integer contQntOwner) {
		this.contQntOwner = contQntOwner;
	}

	public Integer getContQntPartyB() {
		return contQntPartyB;
	}

	public void setContQntPartyB(Integer contQntPartyB) {
		this.contQntPartyB = contQntPartyB;
	}

	public Integer getContQntPartyC() {
		return contQntPartyC;
	}

	public void setContQntPartyC(Integer contQntPartyC) {
		this.contQntPartyC = contQntPartyC;
	}

	public Integer getContQntPartyD() {
		return contQntPartyD;
	}

	public void setContQntPartyD(Integer contQntPartyD) {
		this.contQntPartyD = contQntPartyD;
	}

	public Integer getContQntPartyE() {
		return contQntPartyE;
	}

	public void setContQntPartyE(Integer contQntPartyE) {
		this.contQntPartyE = contQntPartyE;
	}

	public String getOtherMainCont() {
		return otherMainCont;
	}

	public void setOtherMainCont(String otherMainCont) {
		this.otherMainCont = otherMainCont;
	}

	public String getMaxClaimTp() {
		return maxClaimTp;
	}

	public void setMaxClaimTp(String maxClaimTp) {
		this.maxClaimTp = maxClaimTp;
	}

	public BigDecimal getMaxClaimAmt() {
		return maxClaimAmt;
	}

	public void setMaxClaimAmt(BigDecimal maxClaimAmt) {
		this.maxClaimAmt = maxClaimAmt;
	}

	public String getOtherAgreedEvent() {
		return otherAgreedEvent;
	}

	public void setOtherAgreedEvent(String otherAgreedEvent) {
		this.otherAgreedEvent = otherAgreedEvent;
	}

	public Integer getContPrintNum() {
		return contPrintNum;
	}

	public void setContPrintNum(Integer contPrintNum) {
		this.contPrintNum = contPrintNum;
	}

	public String getApproveType() {
		return approveType;
	}

	public void setApproveType(String approveType) {
		this.approveType = approveType;
	}

	public String getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(String logoutDate) {
		this.logoutDate = logoutDate;
	}

	public String getGuarantorId() {
		return guarantorId;
	}

	public void setGuarantorId(String guarantorId) {
		this.guarantorId = guarantorId;
	}

	public String getAssureName() {
		return assureName;
	}

	public void setAssureName(String assureName) {
		this.assureName = assureName;
	}

	public String getAssureCertType() {
		return assureCertType;
	}

	public void setAssureCertType(String assureCertType) {
		this.assureCertType = assureCertType;
	}

	public String getBizLine() {
		return bizLine;
	}

	public void setBizLine(String bizLine) {
		this.bizLine = bizLine;
	}

	public String getAssureCertCode() {
		return assureCertCode;
	}

	public void setAssureCertCode(String assureCertCode) {
		this.assureCertCode = assureCertCode;
	}

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getIsDebtGuar() {
		return isDebtGuar;
	}

	public void setIsDebtGuar(String isDebtGuar) {
		this.isDebtGuar = isDebtGuar;
	}

	public String getLmtAccNo() {
		return lmtAccNo;
	}

	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	public String getReplyNo() {
		return replyNo;
	}

	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param guarPkId
	 */
	public void setGuarPkId(String guarPkId) {
		this.guarPkId = guarPkId == null ? null : guarPkId.trim();
	}
	
    /**
     * @return GuarPkId
     */	
	public String getGuarPkId() {
		return this.guarPkId;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo == null ? null : guarContNo.trim();
	}
	
    /**
     * @return GuarContNo
     */	
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return GuarAmt
     */	
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param isAddGuar
	 */
	public void setIsAddGuar(String isAddGuar) {
		this.isAddGuar = isAddGuar == null ? null : isAddGuar.trim();
	}
	
    /**
     * @return IsAddGuar
     */	
	public String getIsAddGuar() {
		return this.isAddGuar;
	}
	
	/**
	 * @param isPerGur
	 */
	public void setIsPerGur(String isPerGur) {
		this.isPerGur = isPerGur == null ? null : isPerGur.trim();
	}
	
    /**
     * @return IsPerGur
     */	
	public String getIsPerGur() {
		return this.isPerGur;
	}
	
	/**
	 * @param correRel
	 */
	public void setCorreRel(String correRel) {
		this.correRel = correRel == null ? null : correRel.trim();
	}
	
    /**
     * @return CorreRel
     */	
	public String getCorreRel() {
		return this.correRel;
	}
	
	/**
	 * @param rType
	 */
	public void setRType(String rType) {
		this.rType = rType == null ? null : rType.trim();
	}
	
    /**
     * @return RType
     */	
	public String getRType() {
		return this.rType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}

	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}

	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}

	public BigDecimal getContAmt() {
		return contAmt;
	}

	public void setContAmt(BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	public String getContStartDate() {
		return contStartDate;
	}

	public void setContStartDate(String contStartDate) {
		this.contStartDate = contStartDate;
	}

	public String getContEndDate() {
		return contEndDate;
	}

	public void setContEndDate(String contEndDate) {
		this.contEndDate = contEndDate;
	}

	public String getContStatus() {
		return contStatus;
	}

	public void setContStatus(String contStatus) {
		this.contStatus = contStatus;
	}

	public String getInputDate() {
		return inputDate;
	}

	public String getUpdDate() {
		return updDate;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}