package cn.com.yusys.yusp.service.server.xdcz0027;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.dto.server.xdcz0027.req.Xdcz0027DataReqDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 接口处理类: 审批失败支用申请( 登记支用失败)
 *
 * @author chenpeng
 * @version 1.0
 */
@Service
public class Xdcz0027Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdcz0027Service.class);

//    @Autowired
//    private RegZhiyongFailRecordService regZhiyongFailRecordService;

    /**
     * 审批失败支用申请( 登记支用失败)
     *
     * @param xdcz0027DataReqDto
     * @return
     */
    public Map<String, String> execute(Xdcz0027DataReqDto xdcz0027DataReqDto) {
        Map<String, String> resultMap = new HashMap<>();
        // 解析请求参数
        // 证件号
        String certCode = xdcz0027DataReqDto.getCert_code();
        // 客户号
        String cusId = xdcz0027DataReqDto.getCus_id();
        // 合同号
        String contNo = xdcz0027DataReqDto.getCont_no();
        // 失败原因
        // 000：通过, AR01：特殊交易展期 , BR01：百融规则验证失败, ER01：已婚状态下配偶信息缺失
        String failReason = xdcz0027DataReqDto.getFail_reason();
        // 备注
        String bak = xdcz0027DataReqDto.getBak();
        String returnMessage = "";
        try {
            boolean pass = true;
            if (StringUtils.isBlank(certCode)) {
                pass = false;
                returnMessage += "系统检测到证件号为空!";
            }
            if (StringUtils.isBlank(cusId)) {
                pass = false;
                returnMessage += "系统检测到客户号为空!";
            }
            if (StringUtils.isBlank(contNo)) {
                pass = false;
                returnMessage += "系统检测到合同号!";
            }
            if (StringUtils.isBlank(failReason)) {
                pass = false;
                returnMessage += "系统检测到失败原因为空!";
            }
            if (pass) {
//                RegZhiyongFailRecord record = new RegZhiyongFailRecord();
//                //主键暂时不确定
//                String serno = "111114";
//                record.setSerno(serno);
//                record.setCertCode(certCode);
//                record.setContNo(contNo);
//                record.setCusId(cusId);
//                record.setFailReason(failReason);
//                record.setBak(bak);
                // 数据入库
                int result = 0;//regZhiyongFailRecordService.insert(record);
                if (result > 0) {
                    resultMap.put("code", "S");
                    resultMap.put("message", "操作成功");
                } else {
                    resultMap.put("code", "F");
                    resultMap.put("message", "操作失败");
                    logger.error("审批失败支用申请( 登记支用失败),合同号：{}，证件号：{}", contNo, certCode);
                }
            } else {
                resultMap.put("code", "F");
                resultMap.put("message", returnMessage);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return resultMap;
    }
}
