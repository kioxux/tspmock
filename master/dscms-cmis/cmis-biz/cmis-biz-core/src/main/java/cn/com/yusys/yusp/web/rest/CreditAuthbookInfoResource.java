/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditAuthbookInfo;
import cn.com.yusys.yusp.service.CreditAuthbookInfoService;

import javax.validation.constraints.NotBlank;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditAuthbookInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: tangxun
 * @创建时间: 2021-04-26 15:05:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditauthbookinfo")
public class CreditAuthbookInfoResource {
    @Autowired
    private CreditAuthbookInfoService creditAuthbookInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditAuthbookInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditAuthbookInfo> list = creditAuthbookInfoService.selectAll(queryModel);
        return new ResultDto<List<CreditAuthbookInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<List<CreditAuthbookInfo>> index(@RequestBody QueryModel queryModel) {
        List<CreditAuthbookInfo> list = creditAuthbookInfoService.selectByModel(queryModel);
        return new ResultDto<List<CreditAuthbookInfo>>(list);
    }


    @PostMapping("/query")
    protected ResultDto<List<CreditAuthbookInfo>> querybymodel(@RequestBody QueryModel queryModel) {
        List<CreditAuthbookInfo> list = creditAuthbookInfoService.selectallByModel(queryModel);
        return new ResultDto<List<CreditAuthbookInfo>>(list);
    }
    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{caiSerno}")
    protected ResultDto<CreditAuthbookInfo> show(@PathVariable("caiSerno") String caiSerno) {
        CreditAuthbookInfo creditAuthbookInfo = creditAuthbookInfoService.selectByPrimaryKey(caiSerno);
        return new ResultDto<CreditAuthbookInfo>(creditAuthbookInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<CreditAuthbookInfo> create(@RequestBody CreditAuthbookInfo creditAuthbookInfo) throws URISyntaxException {
        creditAuthbookInfoService.insert(creditAuthbookInfo);
        return new ResultDto<CreditAuthbookInfo>(creditAuthbookInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditAuthbookInfo creditAuthbookInfo) throws URISyntaxException {
        int result = creditAuthbookInfoService.update(creditAuthbookInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{caiSerno}")
    protected ResultDto<Integer> delete(@PathVariable("caiSerno") String caiSerno) {
        int result = creditAuthbookInfoService.deleteByPrimaryKey(caiSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditAuthbookInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:asyncexportcreditauthbook
     * @函数描述:异步导出征信授权书台账明细
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/asyncexportcreditauthbook")
    public ResultDto<ProgressDto> asyncExportCreditAuthbook(@RequestBody QueryModel queryModel) {
        ProgressDto progressDto = creditAuthbookInfoService.asyncExportCreditAuthbook(queryModel);
        return ResultDto.success(progressDto);
    }

    /**
     * @函数名称:queryAuthbook
     * @函数描述:授权书列表查询接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation(value = "授权书列表查询")
    @PostMapping("/queryauthbook")
    protected ResultDto<List<CreditAuthbookInfo>> queryAuthbook(@Validated @RequestBody CreditAuthbookInfoContionDto creditAuthbookInfoContionDto) {
        List<CreditAuthbookInfo> list = creditAuthbookInfoService.queryAuthbook(creditAuthbookInfoContionDto.getCertCode(), creditAuthbookInfoContionDto.getManagerId());
        return new ResultDto<List<CreditAuthbookInfo>>(list);
    }

    static class CreditAuthbookInfoContionDto {
        @NotBlank(message = "客户证件号不能为空")
        private String certCode;

        @NotBlank(message = "客户经理编号不能为空")
        private String managerId;

        public String getCertCode() {
            return certCode;
        }

        public void setCertCode(String certCode) {
            this.certCode = certCode;
        }

        public String getManagerId() {
            return managerId;
        }

        public void setManagerId(String managerId) {
            this.managerId = managerId;
        }
    }

    /**
     * @函数名称:selectAuthbook
     * @函数描述:贷后授权书列表查询接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectauthbook")
    protected ResultDto<List<CreditAuthbookInfo>> selectAuthbook(@RequestBody QueryModel queryModel) {
        List<CreditAuthbookInfo> list = creditAuthbookInfoService.selectAuthbook(queryModel);
        return new ResultDto<List<CreditAuthbookInfo>>(list);
    }

    /**
     * @函数名称:queryAuthBookList
     * @函数描述:查询授权书列表
     * @参数与返回说明:
     * @param queryModel
     */
    @PostMapping("/queryauthbooklist")
    protected ResultDto<List<CreditAuthbookInfo>> queryAuthBookList(@RequestBody QueryModel queryModel) {
        List<CreditAuthbookInfo> list = creditAuthbookInfoService.queryAuthBookList(queryModel);
        return new ResultDto<List<CreditAuthbookInfo>>(list);
    }

    /**
     * @函数名称:queryAuthBookListByModel
     * @函数描述:查询授权书列表 规则带入
     * @参数与返回说明:
     * @param queryModel
     */
    @PostMapping("/queryauthbooklistbymodel")
    protected ResultDto<List<CreditAuthbookInfo>> queryAuthBookListByModel(@RequestBody QueryModel queryModel) {
        List<CreditAuthbookInfo> list = creditAuthbookInfoService.queryAuthBookListByModel(queryModel);
        return new ResultDto<List<CreditAuthbookInfo>>(list);
    }

    /**
     * @函数名称:queryAuthBookinfo
     * @函数描述:该客户经理发起查询过的，且与主借款人关系一致的最近一份授权书
     * @参数与返回说明:
     * @param map
     */
    @PostMapping("/queryauthbookinfo")
    protected ResultDto<CreditAuthbookInfo> queryAuthBookinfo(@RequestBody Map map) {
        CreditAuthbookInfo creditAuthbookInfo = creditAuthbookInfoService.queryAuthBookinfoByMap(map);
        return new ResultDto<CreditAuthbookInfo>(creditAuthbookInfo);
    }
}
