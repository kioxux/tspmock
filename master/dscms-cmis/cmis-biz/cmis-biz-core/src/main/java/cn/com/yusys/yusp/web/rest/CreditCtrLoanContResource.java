/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCtrLoanCont;
import cn.com.yusys.yusp.service.CreditCtrLoanContService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCtrLoanContResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 21:18:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/creditctrloancont")
public class CreditCtrLoanContResource {
    @Autowired
    private CreditCtrLoanContService creditCtrLoanContService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCtrLoanCont>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCtrLoanCont> list = creditCtrLoanContService.selectAll(queryModel);
        return new ResultDto<List<CreditCtrLoanCont>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("//")
    protected ResultDto<List<CreditCtrLoanCont>> index(QueryModel queryModel) {
        List<CreditCtrLoanCont> list = creditCtrLoanContService.selectByModel(queryModel);
        return new ResultDto<List<CreditCtrLoanCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{contNo}")
    protected ResultDto<CreditCtrLoanCont> show(@PathVariable("contNo") String contNo) {
        CreditCtrLoanCont creditCtrLoanCont = creditCtrLoanContService.selectByPrimaryKey(contNo);
        return new ResultDto<CreditCtrLoanCont>(creditCtrLoanCont);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CreditCtrLoanCont> create(@RequestBody CreditCtrLoanCont creditCtrLoanCont) throws URISyntaxException {
        creditCtrLoanContService.insert(creditCtrLoanCont);
        return new ResultDto<CreditCtrLoanCont>(creditCtrLoanCont);
    }

    /**
     * @函数名称:save
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/save")
    protected ResultDto<CreditCtrLoanCont> save(@RequestBody CreditCtrLoanCont creditCtrLoanCont) throws URISyntaxException {
        creditCtrLoanContService.save(creditCtrLoanCont);
        return new ResultDto<CreditCtrLoanCont>(creditCtrLoanCont);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCtrLoanCont creditCtrLoanCont) throws URISyntaxException {
        int result = creditCtrLoanContService.update(creditCtrLoanCont);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{contNo}")
    protected ResultDto<Integer> delete(@PathVariable("contNo") String contNo) {
        int result = creditCtrLoanContService.deleteByPrimaryKey(contNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:cut
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/cut")
    protected ResultDto<Integer> cut(@RequestBody CreditCtrLoanCont creditCtrLoanCont) {
        String contNo = creditCtrLoanCont.getContNo();
        int result = creditCtrLoanContService.deleteByPrimaryKey(contNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCtrLoanContService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询大额分期合同为待发起")
    @PostMapping("/querymodel")
    protected ResultDto<List<CreditCtrLoanCont>> indexPost(@RequestBody QueryModel queryModel) {
        List<CreditCtrLoanCont> list = creditCtrLoanContService.selectByModelStatus(queryModel);
        return new ResultDto<List<CreditCtrLoanCont>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询大额分期合同")
    @PostMapping("/query")
    protected ResultDto<List<CreditCtrLoanCont>> queryPost(@RequestBody QueryModel queryModel) {
        List<CreditCtrLoanCont> list = creditCtrLoanContService.selectByModel(queryModel);
        return new ResultDto<List<CreditCtrLoanCont>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询大额分期合同")
    @PostMapping("/querybystatus")
    protected ResultDto<List<CreditCtrLoanCont>> queryByStatus(@RequestBody QueryModel queryModel) {
        List<CreditCtrLoanCont> list = creditCtrLoanContService.selectByNotStatus(queryModel);
        return new ResultDto<List<CreditCtrLoanCont>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("根据主键查询大额分期合同")
    @PostMapping("/selectbyserno")
    protected ResultDto<CreditCtrLoanCont> showPost(@RequestBody CreditCtrLoanCont creditCtrLoanConts) {
        CreditCtrLoanCont creditCtrLoanCont = creditCtrLoanContService.selectBySerno(creditCtrLoanConts.getSerno());
        return new ResultDto<CreditCtrLoanCont>(creditCtrLoanCont);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("流程结束处理类")
    @PostMapping("/handleBusinessDataAfterEnd")
    protected ResultDto<CreditCtrLoanCont> handleBusinessDataAfterEnd(@RequestBody CreditCtrLoanCont creditCtrLoanConts) {
        creditCtrLoanContService.handleBusinessDataAfterEnd(creditCtrLoanConts.getSerno(),"1");
        return new ResultDto<CreditCtrLoanCont>(creditCtrLoanConts);
    }
}
