/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtThrShrsAppRel
 * @类描述: lmt_thr_shrs_app_rel数据实体类
 * @功能描述: 
 * @创建人: 99479
 * @创建时间: 2021-01-23 16:40:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_thr_shrs_app_rel")
public class LmtThrShrsAppRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "serno", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 类型 STD_ZB_CORRE_TYP **/
	@Column(name = "type", unique = false, nullable = true, length = 5)
	private String type;
	
	/** 所属机构编号 **/
	@Column(name = "belg_org_no", unique = false, nullable = true, length = 40)
	private String belgOrgNo;
	
	/** 所属机构名称 **/
	@Column(name = "belg_org_name", unique = false, nullable = true, length = 80)
	private String belgOrgName;
	
	/** 操作类型 STD_ZB_OPR_TYP **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
    /**
     * @return type
     */
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param belgOrgNo
	 */
	public void setBelgOrgNo(String belgOrgNo) {
		this.belgOrgNo = belgOrgNo;
	}
	
    /**
     * @return belgOrgNo
     */
	public String getBelgOrgNo() {
		return this.belgOrgNo;
	}
	
	/**
	 * @param belgOrgName
	 */
	public void setBelgOrgName(String belgOrgName) {
		this.belgOrgName = belgOrgName;
	}
	
    /**
     * @return belgOrgName
     */
	public String getBelgOrgName() {
		return this.belgOrgName;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}