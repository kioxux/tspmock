package cn.com.yusys.yusp.web.server.xddh0011;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.service.server.xddh0011.Xddh0011Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xddh0011.req.Xddh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0011.resp.Xddh0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:推送优享贷预警信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0011:推送优享贷预警信息")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0011Resource.class);

    @Autowired
    private Xddh0011Service xddh0011Service;

    /**
     * 交易码：xddh0011
     * 交易描述：推送优享贷预警信息
     *
     * @param xddh0011DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送优享贷预警信息")
    @PostMapping("/xddh0011")
    protected @ResponseBody
    ResultDto<Xddh0011DataRespDto> xddh0011(@Validated @RequestBody Xddh0011DataReqDto xddh0011DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, JSON.toJSONString(xddh0011DataReqDto));
        Xddh0011DataRespDto xddh0011DataRespDto = new Xddh0011DataRespDto();// 响应Dto:推送优享贷预警信息
        ResultDto<Xddh0011DataRespDto> xddh0011DataResultDto = new ResultDto<>();
        String yw_date = xddh0011DataReqDto.getYw_date();//任务日期
        BigDecimal dqrw_num = xddh0011DataReqDto.getDqrw_num();//任务数量
        String doc_name = xddh0011DataReqDto.getDoc_name();//文件名称
        try {
            // 从xddh0011DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xddh0011DataRespDto = xddh0011Service.xddh0011(xddh0011DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xddh0011DataRespDto对象开始
            xddh0011DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xddh0011DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xddh0011DataRespDto对象结束
            // 封装xddh0011DataResultDto中正确的返回码和返回信息
            xddh0011DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0011DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, e.getMessage());
            // 封装xddh0011DataResultDto中异常返回码和返回信息
            xddh0011DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0011DataResultDto.setMessage(EpbEnum.EPB099999.value);

        }
        // 封装xddh0011DataRespDto到xddh0011DataResultDto中
        xddh0011DataResultDto.setData(xddh0011DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, JSON.toJSONString(xddh0011DataResultDto));
        return xddh0011DataResultDto;
    }
}
