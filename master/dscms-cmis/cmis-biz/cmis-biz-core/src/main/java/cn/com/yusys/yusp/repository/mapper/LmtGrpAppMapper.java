/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.dto.LmtGrpSubDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtGrpApp;
//import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz模块
 * @类名称: LmtGrpAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-07 10:06:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtGrpAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtGrpApp selectByPrimaryKey(@Param("pkId") String pkId);


    /**
     * 根据Map 删除
     * @param delMap
     * @return
     */
    int updateByParams(Map delMap);

    /**
     * 根据组织流水号进行子项的查询
     * @param pkId
     * @return
     */
    List<LmtGrpSubDto> selectBySubInfo(@Param("grpserno") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtGrpApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtGrpApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtGrpApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtGrpApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtGrpApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectLmtGrpAppByParams
     * @方法描述: 通过入参查询授信申请数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<LmtGrpApp> selectLmtGrpAppByParams(Map queryMap);

    /**
     * @方法名称: queryRemindCountByManagerId
     * @方法描述: 查询待处理的集团客户授信申报填报的数量
     * @参数与返回说明:
     * @算法描述: 不查询具体多少客户,只查询申请数量多少笔
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    int queryRemindCountByManagerId(@Param("managerId") String managerId);

    /**
     * @方法名称: queryAllGrpAppListForPartMgr
     * @方法描述: 集团授信分开填报列表
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-04-08 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    List<LmtGrpApp> queryAllGrpAppListForPartMgr(@Param("managerId") String managerId);

    /**
     * 查询所有的申请对象实体
     * @return
     */
    List<LmtGrpApp>  selectAll();

    /**
     * @方法名称：queryByGrpCusId
     * @方法描述：根据集团客户号查询申请信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-05-24 上午 8:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtGrpApp> queryByGrpCusId(QueryModel queryModel);

    /**
     * @方法名称: deleteDataByGrpSerno
     * @方法描述: 新增事务回滚操作
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteDataByGrpSerno(@Param("grpSerno") String grpSerno);

    /**
     * 根据集团申请流水号查询数据返回
     * @param grpSerno
     * @return
     */
    LmtGrpApp queryInfoByGrpSerno(String grpSerno);

    /**
     * 根据流水号获取原申请信息
     */
    LmtGrpApp getLastGrpAppBySerno(String grpSerno);
}