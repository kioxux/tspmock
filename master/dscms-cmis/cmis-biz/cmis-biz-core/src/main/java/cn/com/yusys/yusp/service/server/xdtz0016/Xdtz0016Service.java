package cn.com.yusys.yusp.service.server.xdtz0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccDisc;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0016.req.Xdtz0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0016.resp.Xdtz0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.AccDiscMapper;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0040Service
 * @类描述: #服务类
 * @功能描述:票据系统对贴现台账结清或撤销结清时调用此接口 1结清：信贷根据票号判断贴现台账状态，正常状态下允许结清，修改台账状态为结清；
 * 2撤销结清：信贷根据票号判断贴现台账状态和结清日期字段，当日发生结清操作的台账，允许撤销结清，修改台账状态为正常。
 * @创建人: xll
 * @创建时间: 2021-05-03 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdtz0016Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0016Service.class);

    @Autowired
    private AccDiscMapper accDiscMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：Xdtz0016
     * 交易描述：通知信贷系统更新贴现台账状态
     *
     * @param xdtz0016DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0016DataRespDto xdtz0016(Xdtz0016DataReqDto xdtz0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016DataReqDto));
        //定义返回信息
        Xdtz0016DataRespDto xdtz0016DataRespDto = new Xdtz0016DataRespDto();
        //得到请求字段
        String drftNos = xdtz0016DataReqDto.getDrftNo();//票号（可以传多个票号 以&分开）
        String accStatus = xdtz0016DataReqDto.getDrftStatus();//票据台账状态
        //业务处理开始
        try {
            List<String> drftNoList = new ArrayList<>();
            String[] drftNoAry = drftNos.split("&");
            for (int j = 0; j < drftNoAry.length; j++) {
                String drftNo = drftNoAry[j];

                //查询出票号下对应的票据状态
                Map queryMap = new HashMap();
                queryMap.put("drftNo", drftNo);//票号
                queryMap.put("accStatus", accStatus);//状态
                logger.info("根据票号查询该笔票据台账状态开始,查询参数为:{}", JSON.toJSONString(queryMap));
                String resultStatus = accDiscMapper.selectAccStatusByDrftNo(queryMap);//返回状态
                logger.info("根据票号查询该笔票据台账状态结束,返回参数为:{}", JSON.toJSONString(resultStatus));
                if (StringUtil.isNotEmpty(resultStatus)) {
                    if (CmisBizConstants.IQP_ACC_STATUS_9.equals(accStatus)) { //结清状态
                        if (CmisBizConstants.IQP_ACC_STATUS_1.equals(resultStatus)) {//正常状态变成结清状态
                            logger.info("更新台账状态开始,查询参数为:{}", JSON.toJSONString(queryMap));
                            int result = accDiscMapper.updateCountNumByDrftNo(queryMap);
                            logger.info("更新台账状态结束,返回参数为:{}", JSON.toJSONString(result));
                            if (result > 0) {//更新成功
                                CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();
                                cmisLmt0014ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                                cmisLmt0014ReqDto.setInstuCde(CmisCommonConstants.INSTUCDE_001);//金融机构代码
                                CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
                                cmisLmt0014ReqdealBizListDto.setDealBizNo(drftNo);
                                cmisLmt0014ReqdealBizListDto.setRecoverType("01");
                                cmisLmt0014ReqDto.setDealBizList(Arrays.asList(cmisLmt0014ReqdealBizListDto));
                                ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
                                if (Objects.nonNull(cmisLmt0014RespDtoResultDto) && Objects.nonNull(cmisLmt0014RespDtoResultDto.getData()) && Objects.equals(cmisLmt0014RespDtoResultDto.getData().getErrorCode(), SuccessEnum.SUCCESS.key)) {
                                    xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
                                    xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.SUCCSEE.value);
                                } else {
                                    xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                                    xdtz0016DataRespDto.setOpMsg(cmisLmt0014RespDtoResultDto.getData().getErrorMsg());
                                }

                            } else {//更新失败
                                xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                                xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.FAIL.value);
                            }
                        } else {
                            xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                            xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.FAIL_MESSAGE_1.value);
                        }
                    } else if (CmisBizConstants.IQP_ACC_STATUS_1.equals(accStatus)) {////撤销结清
                        //取出结清日期
                        String discDate = accDiscMapper.selectDiscDateByDrftNo(queryMap);//返回状态
                        //取系统日期
                        String openDay = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
                        if (CmisBizConstants.IQP_ACC_STATUS_0.equals(resultStatus) && discDate.equals(openDay)) {//当日发生结清操作,撤销结清
                            logger.info("更新台账状态开始,查询参数为:{}", JSON.toJSONString(queryMap));
                            int result = accDiscMapper.updateCountNumByDrftNo(queryMap);
                            logger.info("更新台账状态结束,返回参数为:{}", JSON.toJSONString(result));
                            if (result > 0) {//更新成功
                                xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
                                xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.SUCCSEE.value);
                            } else {//更新失败
                                xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                                xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.FAIL.value);
                            }
                        } else {
                            xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                            xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.FAIL_MESSAGE_2.value);
                        }
                    } else if (CmisBizConstants.IQP_ACC_STATUS_0.equals(accStatus)) {//撤销出账
                        if (CmisBizConstants.IQP_ACC_STATUS_0.equals(resultStatus)) {//台账状态为结清,不能出账撤销
                            xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                            xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.FAIL_MESSAGE_3.value);
                        } else {//撤销出账
                            //查询出记录数
                            int num = accDiscMapper.selectCountFromAccDiscBybillNo(queryMap);
                            String billNo = drftNo + "_" + num;//更新
                            queryMap.put("recordNum", billNo);//票号
                            logger.info("更新台账状态开始,查询参数为:{}", JSON.toJSONString(queryMap));
                            AccDisc accDisc = accDiscMapper.selectByBillNo(drftNo);
                            //再删除
                            accDiscMapper.deleteByIds(accDisc.getPkId());
                            //再新增
                            accDisc.setBillNo(billNo);
                            accDisc.setAccStatus("20");
                            int result =accDiscMapper.insert(accDisc);
                            //int result = accDiscMapper.updateCountNumByBillNo(queryMap);
                            logger.info("更新台账状态结束,返回参数为:{}", JSON.toJSONString(result));
                            if (result > 0) {//更新成功
                                xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
                                xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.SUCCSEE.value);
                            } else {//更新失败
                                xdtz0016DataRespDto.setOpFlag(DscmsBizTzEnum.FAIL.key);
                                xdtz0016DataRespDto.setOpMsg(DscmsBizTzEnum.FAIL.value);
                            }
                        }
                    }
                }
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016DataRespDto));
        return xdtz0016DataRespDto;
    }

}
