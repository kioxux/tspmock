package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DataModify
 * @类描述: data_modify数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-24 17:25:07
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class DataModifyDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	
	/** 修改类型 **/
	private String modifyType;
	
	/** 借据编号 **/
	private String billNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 贷款类别 **/
	private String loanTypeDetail;
	
	/** 是否落实贷款 **/
	private String isPactLoan;
	
	/** 是否绿色产业 **/
	private String isGreenIndustry;
	
	/** 是否经营性物业贷款 **/
	private String isOperPropertyLoan;
	
	/** 是否钢贸行业贷款 **/
	private String isSteelLoan;
	
	/** 是否不锈钢行业贷款 **/
	private String isStainlessLoan;
	
	/** 是否扶贫贴息贷款 **/
	private String isPovertyReliefLoan;
	
	/** 是否劳动密集型小企业贴息贷款 **/
	private String isLaborIntenSbsyLoan;
	
	/** 保障性安居工程贷款 **/
	private String goverSubszHouseLoan;
	
	/** 项目贷款节能环保 **/
	private String engyEnviProteLoan;
	
	/** 是否农村综合开发贷款标志 **/
	private String isCphsRurDelpLoan;
	
	/** 房地产贷款 **/
	private String realproLoan;
	
	/** 房产开发贷款资本金比例 **/
	private String realproLoanRate;
	
	/** 担保方式细分 **/
	private String guarDetailMode;
	
	/** 贷款类别 **/
	private String oldLoanTypeDetail;
	
	/** 是否落实贷款 **/
	private String oldIsPactLoan;
	
	/** 是否绿色产业 **/
	private String oldIsGreenIndustry;
	
	/** 是否经营性物业贷款 **/
	private String oldIsOperPropertyLoan;
	
	/** 是否钢贸行业贷款 **/
	private String oldIsSteelLoan;
	
	/** 是否不锈钢行业贷款 **/
	private String oldIsStainlessLoan;
	
	/** 是否扶贫贴息贷款 **/
	private String oldIsPovertyReliefLoan;
	
	/** 是否劳动密集型小企业贴息贷款 **/
	private String oldIsLaborIntenSbsyLoan;
	
	/** 保障性安居工程贷款 **/
	private String oldGoverSubszHouseLoan;
	
	/** 项目贷款节能环保 **/
	private String oldEngyEnviProteLoan;
	
	/** 是否农村综合开发贷款标志 **/
	private String oldIsCphsRurDelpLoan;
	
	/** 房地产贷款 **/
	private String oldRealproLoan;
	
	/** 房产开发贷款资本金比例 **/
	private String oldRealproLoanRate;
	
	/** 担保方式细分 **/
	private String oldGuarDetailMode;

	/** 农户类型 **/
	private String agriType;

	/** 涉农贷款投向 **/
	private String agriLoanTer;

	/** 农户类型 **/
	private String oldAgriType;

	/** 涉农贷款投向 **/
	private String oldAgriLoanTer;

	/** 贷款投向 **/
	private String loanTer;
	
	/** 贷款投向 **/
	private String oldLoanTer;
	
	/** 科目号 **/
	private String subjectNo;
	
	/** 科目名称 **/
	private String subjectName;
	
	/** 科目号 **/
	private String oldSubjectNo;
	
	/** 科目名称 **/
	private String oldSubjectName;
	
	/** 修改内容 **/
	private String modificationContent;
	
	/** 修改原因 **/
	private String modificationReason;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param modifyType
	 */
	public void setModifyType(String modifyType) {
		this.modifyType = modifyType == null ? null : modifyType.trim();
	}
	
    /**
     * @return ModifyType
     */	
	public String getModifyType() {
		return this.modifyType;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param loanTypeDetail
	 */
	public void setLoanTypeDetail(String loanTypeDetail) {
		this.loanTypeDetail = loanTypeDetail == null ? null : loanTypeDetail.trim();
	}
	
    /**
     * @return LoanTypeDetail
     */	
	public String getLoanTypeDetail() {
		return this.loanTypeDetail;
	}
	
	/**
	 * @param isPactLoan
	 */
	public void setIsPactLoan(String isPactLoan) {
		this.isPactLoan = isPactLoan == null ? null : isPactLoan.trim();
	}
	
    /**
     * @return IsPactLoan
     */	
	public String getIsPactLoan() {
		return this.isPactLoan;
	}
	
	/**
	 * @param isGreenIndustry
	 */
	public void setIsGreenIndustry(String isGreenIndustry) {
		this.isGreenIndustry = isGreenIndustry == null ? null : isGreenIndustry.trim();
	}
	
    /**
     * @return IsGreenIndustry
     */	
	public String getIsGreenIndustry() {
		return this.isGreenIndustry;
	}
	
	/**
	 * @param isOperPropertyLoan
	 */
	public void setIsOperPropertyLoan(String isOperPropertyLoan) {
		this.isOperPropertyLoan = isOperPropertyLoan == null ? null : isOperPropertyLoan.trim();
	}
	
    /**
     * @return IsOperPropertyLoan
     */	
	public String getIsOperPropertyLoan() {
		return this.isOperPropertyLoan;
	}
	
	/**
	 * @param isSteelLoan
	 */
	public void setIsSteelLoan(String isSteelLoan) {
		this.isSteelLoan = isSteelLoan == null ? null : isSteelLoan.trim();
	}
	
    /**
     * @return IsSteelLoan
     */	
	public String getIsSteelLoan() {
		return this.isSteelLoan;
	}
	
	/**
	 * @param isStainlessLoan
	 */
	public void setIsStainlessLoan(String isStainlessLoan) {
		this.isStainlessLoan = isStainlessLoan == null ? null : isStainlessLoan.trim();
	}
	
    /**
     * @return IsStainlessLoan
     */	
	public String getIsStainlessLoan() {
		return this.isStainlessLoan;
	}
	
	/**
	 * @param isPovertyReliefLoan
	 */
	public void setIsPovertyReliefLoan(String isPovertyReliefLoan) {
		this.isPovertyReliefLoan = isPovertyReliefLoan == null ? null : isPovertyReliefLoan.trim();
	}
	
    /**
     * @return IsPovertyReliefLoan
     */	
	public String getIsPovertyReliefLoan() {
		return this.isPovertyReliefLoan;
	}
	
	/**
	 * @param isLaborIntenSbsyLoan
	 */
	public void setIsLaborIntenSbsyLoan(String isLaborIntenSbsyLoan) {
		this.isLaborIntenSbsyLoan = isLaborIntenSbsyLoan == null ? null : isLaborIntenSbsyLoan.trim();
	}
	
    /**
     * @return IsLaborIntenSbsyLoan
     */	
	public String getIsLaborIntenSbsyLoan() {
		return this.isLaborIntenSbsyLoan;
	}
	
	/**
	 * @param goverSubszHouseLoan
	 */
	public void setGoverSubszHouseLoan(String goverSubszHouseLoan) {
		this.goverSubszHouseLoan = goverSubszHouseLoan == null ? null : goverSubszHouseLoan.trim();
	}
	
    /**
     * @return GoverSubszHouseLoan
     */	
	public String getGoverSubszHouseLoan() {
		return this.goverSubszHouseLoan;
	}
	
	/**
	 * @param engyEnviProteLoan
	 */
	public void setEngyEnviProteLoan(String engyEnviProteLoan) {
		this.engyEnviProteLoan = engyEnviProteLoan == null ? null : engyEnviProteLoan.trim();
	}
	
    /**
     * @return EngyEnviProteLoan
     */	
	public String getEngyEnviProteLoan() {
		return this.engyEnviProteLoan;
	}
	
	/**
	 * @param isCphsRurDelpLoan
	 */
	public void setIsCphsRurDelpLoan(String isCphsRurDelpLoan) {
		this.isCphsRurDelpLoan = isCphsRurDelpLoan == null ? null : isCphsRurDelpLoan.trim();
	}
	
    /**
     * @return IsCphsRurDelpLoan
     */	
	public String getIsCphsRurDelpLoan() {
		return this.isCphsRurDelpLoan;
	}
	
	/**
	 * @param realproLoan
	 */
	public void setRealproLoan(String realproLoan) {
		this.realproLoan = realproLoan == null ? null : realproLoan.trim();
	}
	
    /**
     * @return RealproLoan
     */	
	public String getRealproLoan() {
		return this.realproLoan;
	}
	
	/**
	 * @param realproLoanRate
	 */
	public void setRealproLoanRate(String realproLoanRate) {
		this.realproLoanRate = realproLoanRate == null ? null : realproLoanRate.trim();
	}
	
    /**
     * @return RealproLoanRate
     */	
	public String getRealproLoanRate() {
		return this.realproLoanRate;
	}
	
	/**
	 * @param guarDetailMode
	 */
	public void setGuarDetailMode(String guarDetailMode) {
		this.guarDetailMode = guarDetailMode == null ? null : guarDetailMode.trim();
	}
	
    /**
     * @return GuarDetailMode
     */	
	public String getGuarDetailMode() {
		return this.guarDetailMode;
	}
	
	/**
	 * @param oldLoanTypeDetail
	 */
	public void setOldLoanTypeDetail(String oldLoanTypeDetail) {
		this.oldLoanTypeDetail = oldLoanTypeDetail == null ? null : oldLoanTypeDetail.trim();
	}
	
    /**
     * @return OldLoanTypeDetail
     */	
	public String getOldLoanTypeDetail() {
		return this.oldLoanTypeDetail;
	}
	
	/**
	 * @param oldIsPactLoan
	 */
	public void setOldIsPactLoan(String oldIsPactLoan) {
		this.oldIsPactLoan = oldIsPactLoan == null ? null : oldIsPactLoan.trim();
	}
	
    /**
     * @return OldIsPactLoan
     */	
	public String getOldIsPactLoan() {
		return this.oldIsPactLoan;
	}
	
	/**
	 * @param oldIsGreenIndustry
	 */
	public void setOldIsGreenIndustry(String oldIsGreenIndustry) {
		this.oldIsGreenIndustry = oldIsGreenIndustry == null ? null : oldIsGreenIndustry.trim();
	}
	
    /**
     * @return OldIsGreenIndustry
     */	
	public String getOldIsGreenIndustry() {
		return this.oldIsGreenIndustry;
	}
	
	/**
	 * @param oldIsOperPropertyLoan
	 */
	public void setOldIsOperPropertyLoan(String oldIsOperPropertyLoan) {
		this.oldIsOperPropertyLoan = oldIsOperPropertyLoan == null ? null : oldIsOperPropertyLoan.trim();
	}
	
    /**
     * @return OldIsOperPropertyLoan
     */	
	public String getOldIsOperPropertyLoan() {
		return this.oldIsOperPropertyLoan;
	}
	
	/**
	 * @param oldIsSteelLoan
	 */
	public void setOldIsSteelLoan(String oldIsSteelLoan) {
		this.oldIsSteelLoan = oldIsSteelLoan == null ? null : oldIsSteelLoan.trim();
	}
	
    /**
     * @return OldIsSteelLoan
     */	
	public String getOldIsSteelLoan() {
		return this.oldIsSteelLoan;
	}
	
	/**
	 * @param oldIsStainlessLoan
	 */
	public void setOldIsStainlessLoan(String oldIsStainlessLoan) {
		this.oldIsStainlessLoan = oldIsStainlessLoan == null ? null : oldIsStainlessLoan.trim();
	}
	
    /**
     * @return OldIsStainlessLoan
     */	
	public String getOldIsStainlessLoan() {
		return this.oldIsStainlessLoan;
	}
	
	/**
	 * @param oldIsPovertyReliefLoan
	 */
	public void setOldIsPovertyReliefLoan(String oldIsPovertyReliefLoan) {
		this.oldIsPovertyReliefLoan = oldIsPovertyReliefLoan == null ? null : oldIsPovertyReliefLoan.trim();
	}
	
    /**
     * @return OldIsPovertyReliefLoan
     */	
	public String getOldIsPovertyReliefLoan() {
		return this.oldIsPovertyReliefLoan;
	}
	
	/**
	 * @param oldIsLaborIntenSbsyLoan
	 */
	public void setOldIsLaborIntenSbsyLoan(String oldIsLaborIntenSbsyLoan) {
		this.oldIsLaborIntenSbsyLoan = oldIsLaborIntenSbsyLoan == null ? null : oldIsLaborIntenSbsyLoan.trim();
	}
	
    /**
     * @return OldIsLaborIntenSbsyLoan
     */	
	public String getOldIsLaborIntenSbsyLoan() {
		return this.oldIsLaborIntenSbsyLoan;
	}
	
	/**
	 * @param oldGoverSubszHouseLoan
	 */
	public void setOldGoverSubszHouseLoan(String oldGoverSubszHouseLoan) {
		this.oldGoverSubszHouseLoan = oldGoverSubszHouseLoan == null ? null : oldGoverSubszHouseLoan.trim();
	}
	
    /**
     * @return OldGoverSubszHouseLoan
     */	
	public String getOldGoverSubszHouseLoan() {
		return this.oldGoverSubszHouseLoan;
	}
	
	/**
	 * @param oldEngyEnviProteLoan
	 */
	public void setOldEngyEnviProteLoan(String oldEngyEnviProteLoan) {
		this.oldEngyEnviProteLoan = oldEngyEnviProteLoan == null ? null : oldEngyEnviProteLoan.trim();
	}
	
    /**
     * @return OldEngyEnviProteLoan
     */	
	public String getOldEngyEnviProteLoan() {
		return this.oldEngyEnviProteLoan;
	}
	
	/**
	 * @param oldIsCphsRurDelpLoan
	 */
	public void setOldIsCphsRurDelpLoan(String oldIsCphsRurDelpLoan) {
		this.oldIsCphsRurDelpLoan = oldIsCphsRurDelpLoan == null ? null : oldIsCphsRurDelpLoan.trim();
	}
	
    /**
     * @return OldIsCphsRurDelpLoan
     */	
	public String getOldIsCphsRurDelpLoan() {
		return this.oldIsCphsRurDelpLoan;
	}
	
	/**
	 * @param oldRealproLoan
	 */
	public void setOldRealproLoan(String oldRealproLoan) {
		this.oldRealproLoan = oldRealproLoan == null ? null : oldRealproLoan.trim();
	}
	
    /**
     * @return OldRealproLoan
     */	
	public String getOldRealproLoan() {
		return this.oldRealproLoan;
	}
	
	/**
	 * @param oldRealproLoanRate
	 */
	public void setOldRealproLoanRate(String oldRealproLoanRate) {
		this.oldRealproLoanRate = oldRealproLoanRate == null ? null : oldRealproLoanRate.trim();
	}
	
    /**
     * @return OldRealproLoanRate
     */	
	public String getOldRealproLoanRate() {
		return this.oldRealproLoanRate;
	}
	
	/**
	 * @param oldGuarDetailMode
	 */
	public void setOldGuarDetailMode(String oldGuarDetailMode) {
		this.oldGuarDetailMode = oldGuarDetailMode == null ? null : oldGuarDetailMode.trim();
	}

	public String getAgriType() {
		return agriType;
	}

	public void setAgriType(String agriType) {
		this.agriType = agriType;
	}

	public String getAgriLoanTer() {
		return agriLoanTer;
	}

	public void setAgriLoanTer(String agriLoanTer) {
		this.agriLoanTer = agriLoanTer;
	}

	public String getOldAgriType() {
		return oldAgriType;
	}

	public void setOldAgriType(String oldAgriType) {
		this.oldAgriType = oldAgriType;
	}

	public String getOldAgriLoanTer() {
		return oldAgriLoanTer;
	}

	public void setOldAgriLoanTer(String oldAgriLoanTer) {
		this.oldAgriLoanTer = oldAgriLoanTer;
	}


	/**
     * @return OldGuarDetailMode
     */	
	public String getOldGuarDetailMode() {
		return this.oldGuarDetailMode;
	}
	
	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer == null ? null : loanTer.trim();
	}
	
    /**
     * @return LoanTer
     */	
	public String getLoanTer() {
		return this.loanTer;
	}
	
	/**
	 * @param oldLoanTer
	 */
	public void setOldLoanTer(String oldLoanTer) {
		this.oldLoanTer = oldLoanTer == null ? null : oldLoanTer.trim();
	}
	
    /**
     * @return OldLoanTer
     */	
	public String getOldLoanTer() {
		return this.oldLoanTer;
	}
	
	/**
	 * @param subjectNo
	 */
	public void setSubjectNo(String subjectNo) {
		this.subjectNo = subjectNo == null ? null : subjectNo.trim();
	}
	
    /**
     * @return SubjectNo
     */	
	public String getSubjectNo() {
		return this.subjectNo;
	}
	
	/**
	 * @param subjectName
	 */
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName == null ? null : subjectName.trim();
	}
	
    /**
     * @return SubjectName
     */	
	public String getSubjectName() {
		return this.subjectName;
	}
	
	/**
	 * @param oldSubjectNo
	 */
	public void setOldSubjectNo(String oldSubjectNo) {
		this.oldSubjectNo = oldSubjectNo == null ? null : oldSubjectNo.trim();
	}
	
    /**
     * @return OldSubjectNo
     */	
	public String getOldSubjectNo() {
		return this.oldSubjectNo;
	}
	
	/**
	 * @param oldSubjectName
	 */
	public void setOldSubjectName(String oldSubjectName) {
		this.oldSubjectName = oldSubjectName == null ? null : oldSubjectName.trim();
	}
	
    /**
     * @return OldSubjectName
     */	
	public String getOldSubjectName() {
		return this.oldSubjectName;
	}
	
	/**
	 * @param modificationContent
	 */
	public void setModificationContent(String modificationContent) {
		this.modificationContent = modificationContent == null ? null : modificationContent.trim();
	}
	
    /**
     * @return ModificationContent
     */	
	public String getModificationContent() {
		return this.modificationContent;
	}
	
	/**
	 * @param modificationReason
	 */
	public void setModificationReason(String modificationReason) {
		this.modificationReason = modificationReason == null ? null : modificationReason.trim();
	}
	
    /**
     * @return ModificationReason
     */	
	public String getModificationReason() {
		return this.modificationReason;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}