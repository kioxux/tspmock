package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: iqpRepayInterestChg
 * @类描述: iqp_repay_date_chg数据实体类
 * @功能描述:
 * @创建人: 方圳
 * @创建时间: 2020-1-22 16:49:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_int_reduc_app")
public class IqpRepayInterestChg extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 业务流水号
     **/
    @Id
    @Generated(KeyConstants.UUID)
    @Column(name = "IQP_SERNO")
    private String iqpSerno;

    /**
     * 借据编号
     **/
    @Column(name = "BILL_NO", unique = false, nullable = false, length = 40)
    private String billNo;

    /**
     * 客户编号
     **/
    @Column(name = "CUS_ID", unique = false, nullable = false, length = 40)
    private String cusId;
    /**
     * 期号
     **/
    @Column(name = "DATE_NO", unique = false, nullable = true, length = 10)
    private Integer dateNo;

    /**
     * 还款日期
     **/
    @Column(name = "REPAY_DATE", unique = false, nullable = true, length = 10)
    private Integer repayDate;


    /**
     * 贷款执行利率
     **/
    @Column(name = "LOAN_RATE", unique = false, nullable = true, length = 10)
    private BigDecimal loanRate;

    /**
     * 分期金额
     **/
    @Column(name = "AMOR_AMT", unique = false, nullable = true, length = 10)
    private BigDecimal amorAmt;

    /**
     * 剩余金额
     **/
    @Column(name = "SURPLUS_AMT", unique = false, nullable = true, length = 10)
    private BigDecimal surplusAmt;
    /**
     * 本期应还本金
     **/
    @Column(name = "REC_CAP", unique = false, nullable = true, length = 10)
    private BigDecimal recCap;

    /**
     * 本期已还本金
     **/
    @Column(name = "HASBC_CAP", unique = false, nullable = true, length = 10)
    private BigDecimal hasbcCap;


    /**
     * 本期应还正常利息
     **/
    @Column(name = "REC_NORMAL_INT", unique = false, nullable = true, length = 10)
    private BigDecimal recNormalInt;

    /**
     * 本期已还正常利息
     **/
    @Column(name = "HASBC_NORMAL_INT", unique = false, nullable = true, length = 10)
    private BigDecimal hasbcNormalInt;

    /**
     * 本期应还逾期利息
     **/
    @Column(name = "REC_OVERDUE_INT", unique = false, nullable = true, length = 10)
    private BigDecimal recOverdueInt;

    /**
     * 本期已还逾期利息
     **/
    @Column(name = "HASBC_OVERDUE_INT", unique = false, nullable = true, length = 10)
    private BigDecimal hasbcOverdueInt;
    /**
     * 本期应还复利利息
     **/
    @Column(name = "REC_CI_INT", unique = false, nullable = true, length = 10)
    private BigDecimal recCiInt;
    /**
     * 本期已还复利利息
     **/
    @Column(name = "HASBC_CI_INT", unique = false, nullable = true, length = 10)
    private BigDecimal hasbcCiInt;
    /**
     * 当前试算未结正常利息
     **/
    @Column(name = "NK_NORMAL_INT", unique = false, nullable = true, length = 10)
    private BigDecimal nkNormalInt;
    /**
     * 当前试算未结正常复利
     **/
    @Column(name = "NK_NORMAL_CI", unique = false, nullable = true, length = 10)
    private BigDecimal nkNormalCi;
    /**
     * 当前试算未结正常罚息
     **/
    @Column(name = "NK_NORMAL_ODINT", unique = false, nullable = true, length = 10)
    private BigDecimal nkNormalOdint;
    /**
     * 减免正常利息
     **/
    @Column(name = "REDUC_NORMAL_INT", unique = false, nullable = true, length = 10)
    private BigDecimal reducNormalInt;
    /**
     * 减免逾期利息
     **/
    @Column(name = "REDUC_OVERDUE_INT", unique = false, nullable = true, length = 10)
    private BigDecimal reducOverdueInt;
    /**
     * 减免复利
     **/
    @Column(name = "REDUC_CI", unique = false, nullable = true, length = 10)
    private BigDecimal reducCi;
    /**
     * 减免总额
     **/
    @Column(name = "REDUC_TOTAL_AMT", unique = false, nullable = true, length = 10)
    private BigDecimal reducTotalAmt;
    /**
     * 减免原因
     **/
    @Column(name = "REDUC_RESN", unique = false, nullable = true, length = 400)
    private String reducResn;

    /**
     * 登记人
     **/
    @Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
    private String inputId;

    /**
     * 登记机构
     **/
    @Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
    private String inputBrId;

    /**
     * 登记日期
     **/
    @Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
    private String inputDate;

    /**
     * 主办人
     **/
    @Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
    private String managerId;

    /**
     * 主办机构
     **/
    @Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
    private String managerBrId;

    /**
     * 最后修改人
     **/
    @Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
    private String updId;

    /**
     * 最后修改机构
     **/
    @Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
    private String updBrId;

    /**
     * 最后修改日期
     **/
    @Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
    private String updDate;

    /**
     * 申请状态
     **/
    @Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
    private String approveStatus;

    /**
     * 操作类型
     **/
    @Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
    private String oprType;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public Integer getDateNo() {
        return dateNo;
    }

    public void setDateNo(Integer dateNo) {
        this.dateNo = dateNo;
    }

    public Integer getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(Integer repayDate) {
        this.repayDate = repayDate;
    }

    public BigDecimal getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(BigDecimal loanRate) {
        this.loanRate = loanRate;
    }

    public BigDecimal getAmorAmt() {
        return amorAmt;
    }

    public void setAmorAmt(BigDecimal amorAmt) {
        this.amorAmt = amorAmt;
    }

    public BigDecimal getSurplusAmt() {
        return surplusAmt;
    }

    public void setSurplusAmt(BigDecimal surplusAmt) {
        this.surplusAmt = surplusAmt;
    }

    public BigDecimal getRecCap() {
        return recCap;
    }

    public void setRecCap(BigDecimal recCap) {
        this.recCap = recCap;
    }

    public BigDecimal getHasbcCap() {
        return hasbcCap;
    }

    public void setHasbcCap(BigDecimal hasbcCap) {
        this.hasbcCap = hasbcCap;
    }

    public BigDecimal getRecNormalInt() {
        return recNormalInt;
    }

    public void setRecNormalInt(BigDecimal recNormalInt) {
        this.recNormalInt = recNormalInt;
    }

    public BigDecimal getHasbcNormalInt() {
        return hasbcNormalInt;
    }

    public void setHasbcNormalInt(BigDecimal hasbcNormalInt) {
        this.hasbcNormalInt = hasbcNormalInt;
    }

    public BigDecimal getrecOverdueInt() {
        return recOverdueInt;
    }

    public void setrecOverdueInt(BigDecimal recOverdueInt) {
        this.recOverdueInt = recOverdueInt;
    }

    public BigDecimal gethasbcOverdueInt() {
        return hasbcOverdueInt;
    }

    public void sethasbcOverdueInt(BigDecimal hasbcOverdueInt) {
        this.hasbcOverdueInt = hasbcOverdueInt;
    }

    public BigDecimal getRecCiInt() {
        return recCiInt;
    }

    public void setRecCiInt(BigDecimal recCiInt) {
        this.recCiInt = recCiInt;
    }

    public BigDecimal getHasbcCiInt() {
        return hasbcCiInt;
    }

    public void setHasbcCiInt(BigDecimal hasbcCiInt) {
        this.hasbcCiInt = hasbcCiInt;
    }

    public BigDecimal getNkNormalInt() {
        return nkNormalInt;
    }

    public void setNkNormalInt(BigDecimal nkNormalInt) {
        this.nkNormalInt = nkNormalInt;
    }

    public BigDecimal getNkNormalCi() {
        return nkNormalCi;
    }

    public void setNkNormalCi(BigDecimal nkNormalCi) {
        this.nkNormalCi = nkNormalCi;
    }

    public BigDecimal getNkNormalOdint() {
        return nkNormalOdint;
    }

    public void setNkNormalOdint(BigDecimal nkNormalOdint) {
        this.nkNormalOdint = nkNormalOdint;
    }

    public BigDecimal getReducNormalInt() {
        return reducNormalInt;
    }

    public void setReducNormalInt(BigDecimal reducNormalInt) {
        this.reducNormalInt = reducNormalInt;
    }

    public BigDecimal getreducOverdueInt() {
        return reducOverdueInt;
    }

    public void setreducOverdueInt(BigDecimal reducOverdueInt) {
        this.reducOverdueInt = reducOverdueInt;
    }

    public BigDecimal getReducCi() {
        return reducCi;
    }

    public void setReducCi(BigDecimal reducCi) {
        this.reducCi = reducCi;
    }

    public BigDecimal getReducTotalAmt() {
        return reducTotalAmt;
    }

    public void setReducTotalAmt(BigDecimal reducTotalAmt) {
        this.reducTotalAmt = reducTotalAmt;
    }

    public String getReducResn() {
        return reducResn;
    }

    public void setReducResn(String reducResn) {
        this.reducResn = reducResn;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    @Override
    public String toString() {
        return "iqpRepayInterestChg{" +
                "iqpSerno='" + iqpSerno + '\'' +
                ", billNo='" + billNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", dateNo=" + dateNo +
                ", repayDate=" + repayDate +
                ", loanRate=" + loanRate +
                ", amorAmt=" + amorAmt +
                ", surplusAmt=" + surplusAmt +
                ", recCap=" + recCap +
                ", hasbcCap=" + hasbcCap +
                ", recNormalInt=" + recNormalInt +
                ", hasbcNormalInt=" + hasbcNormalInt +
                ", recOverdueInt=" + recOverdueInt +
                ", hasbcOverdueInt=" + hasbcOverdueInt +
                ", recCiInt=" + recCiInt +
                ", hasbcCiInt=" + hasbcCiInt +
                ", nkNormalInt=" + nkNormalInt +
                ", nkNormalCi=" + nkNormalCi +
                ", nkNormalOdint=" + nkNormalOdint +
                ", reducNormalInt=" + reducNormalInt +
                ", reducOverdueInt=" + reducOverdueInt +
                ", reducCi=" + reducCi +
                ", reducTotalAmt=" + reducTotalAmt +
                ", reducResn='" + reducResn + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", oprType='" + oprType + '\'' +
                '}';
    }
}
