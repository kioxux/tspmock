package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ComBail
 * @类描述: com_bail数据实体类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 11:01:48
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class ComBailDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 业务流水号 **/
	private String iqpSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 保证金序号 **/
	private java.math.BigDecimal serialNumber;
	
	/** 保证金账户编号 **/
	private String bailAcctNo;
	
	/** 保证金账户名称 **/
	private String bailAcctName;
	
	/** 保证金币种 STD_ZB_CUR_TYP **/
	private String bailCurType;
	
	/** 账户余额 **/
	private java.math.BigDecimal acctAmtBalance;
	
	/** 保证金来源 STD_ZB_BAIL_SOUR **/
	private String bailSour;
	
	/** 保证金类型 **/
	private String bailType;
	
	/** 存期 **/
	private java.math.BigDecimal depTerm;
	
	/** 开户机构编号 **/
	private String openOrg;
	
	/** 保证金汇率 **/
	private java.math.BigDecimal exchangeRate;
	
	/** 类型 STD_ZB_COM_BAIL_TP **/
	private String type;
	
	/** 开户起始日期 **/
	private String depStarDate;
	
	/** 开户到期日期 **/
	private String depEndDate;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param serialNumber
	 */
	public void setSerialNumber(java.math.BigDecimal serialNumber) {
		this.serialNumber = serialNumber;
	}
	
    /**
     * @return SerialNumber
     */	
	public java.math.BigDecimal getSerialNumber() {
		return this.serialNumber;
	}
	
	/**
	 * @param bailAcctNo
	 */
	public void setBailAcctNo(String bailAcctNo) {
		this.bailAcctNo = bailAcctNo == null ? null : bailAcctNo.trim();
	}
	
    /**
     * @return BailAcctNo
     */	
	public String getBailAcctNo() {
		return this.bailAcctNo;
	}
	
	/**
	 * @param bailAcctName
	 */
	public void setBailAcctName(String bailAcctName) {
		this.bailAcctName = bailAcctName == null ? null : bailAcctName.trim();
	}
	
    /**
     * @return BailAcctName
     */	
	public String getBailAcctName() {
		return this.bailAcctName;
	}
	
	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType == null ? null : bailCurType.trim();
	}
	
    /**
     * @return BailCurType
     */	
	public String getBailCurType() {
		return this.bailCurType;
	}
	
	/**
	 * @param acctAmtBalance
	 */
	public void setAcctAmtBalance(java.math.BigDecimal acctAmtBalance) {
		this.acctAmtBalance = acctAmtBalance;
	}
	
    /**
     * @return AcctAmtBalance
     */	
	public java.math.BigDecimal getAcctAmtBalance() {
		return this.acctAmtBalance;
	}
	
	/**
	 * @param bailSour
	 */
	public void setBailSour(String bailSour) {
		this.bailSour = bailSour == null ? null : bailSour.trim();
	}
	
    /**
     * @return BailSour
     */	
	public String getBailSour() {
		return this.bailSour;
	}
	
	/**
	 * @param bailType
	 */
	public void setBailType(String bailType) {
		this.bailType = bailType == null ? null : bailType.trim();
	}
	
    /**
     * @return BailType
     */	
	public String getBailType() {
		return this.bailType;
	}
	
	/**
	 * @param depTerm
	 */
	public void setDepTerm(java.math.BigDecimal depTerm) {
		this.depTerm = depTerm;
	}
	
    /**
     * @return DepTerm
     */	
	public java.math.BigDecimal getDepTerm() {
		return this.depTerm;
	}
	
	/**
	 * @param openOrg
	 */
	public void setOpenOrg(String openOrg) {
		this.openOrg = openOrg == null ? null : openOrg.trim();
	}
	
    /**
     * @return OpenOrg
     */	
	public String getOpenOrg() {
		return this.openOrg;
	}
	
	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(java.math.BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
	
    /**
     * @return ExchangeRate
     */	
	public java.math.BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type == null ? null : type.trim();
	}
	
    /**
     * @return Type
     */	
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param depStarDate
	 */
	public void setDepStarDate(String depStarDate) {
		this.depStarDate = depStarDate == null ? null : depStarDate.trim();
	}
	
    /**
     * @return DepStarDate
     */	
	public String getDepStarDate() {
		return this.depStarDate;
	}
	
	/**
	 * @param depEndDate
	 */
	public void setDepEndDate(String depEndDate) {
		this.depEndDate = depEndDate == null ? null : depEndDate.trim();
	}
	
    /**
     * @return DepEndDate
     */	
	public String getDepEndDate() {
		return this.depEndDate;
	}


}