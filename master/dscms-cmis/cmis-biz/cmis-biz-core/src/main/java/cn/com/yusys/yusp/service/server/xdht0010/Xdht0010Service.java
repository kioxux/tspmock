package cn.com.yusys.yusp.service.server.xdht0010;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.GrtGuarBizRstRel;
import cn.com.yusys.yusp.domain.OtherCnyRateAppFinDetails;
import cn.com.yusys.yusp.dto.server.xdht0010.req.Xdht0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0010.resp.ContList;
import cn.com.yusys.yusp.dto.server.xdht0010.resp.Xdht0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author qinyadong
 * @create 2021-06-12 14:05
 */
@Service
public class Xdht0010Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdht0010Service.class);

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private AccLoanMapper accLoanMapper;

    @Resource
    private CtrLoanContMapper ctrLoanContMapper;

    @Resource
    private CtrContImageAuditAppMapper ctrContImageAuditAppMapper;

    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Resource
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;

    @Resource
    private OtherCnyRateAppFinDetailsMapper otherCnyRateAppFinDetailsMapper;

    @Resource
    private LmtReplyAccSubMapper lmtReplyAccSubMapper;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    public Xdht0010DataRespDto getIndivCusBaseInfo(Xdht0010DataReqDto xdht0010DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010DataReqDto));
        Map map = new HashMap<>();
        Xdht0010DataRespDto xdht0010DataRespDto = new Xdht0010DataRespDto(); // 查询查询符合条件的省心快贷合同
        try {
//          获取系统时间
            String openDay = DateUtils.getCurrDateStr();

//          获取请求参数
            String flag = xdht0010DataReqDto.getFlag(); //标志信息
            String cusId = xdht0010DataReqDto.getCusId();// 客户号

            if ("1".equals(flag)){
                //            查找出物联产贷的已签订的合同
                List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.getselectByCusIdOne(cusId);
                if (ctrLoanContList != null && ctrLoanContList.size() > 0) {
                    for (int i = 0; i < ctrLoanContList.size(); i++) {
                        CtrLoanCont ctrLoanCont = ctrLoanContList.get(i);
//                      根据合同号查询出关联的担保合同号
//                      合同号
                        String contNo = ctrLoanCont.getContNo();
                        List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByContNo(contNo);
                        StringBuffer ss = new StringBuffer();
                        for (int j = 0; j < grtGuarBizRstRelList.size(); j++) {
                            GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelList.get(j);
//                            担保合同编号
                            String guarContNo = grtGuarBizRstRel.getGuarContNo();
                            if (j < 1) {
                                ss.append(guarContNo);
                            } else {
                                ss.append(guarContNo + ",");
                            }

                        }
//                        转化为 字符类型
                        String guar_cont_no = ss.toString();
//                        保证金折算人民币金额
                            BigDecimal bailCvtCnyAmt = ctrLoanCont.getBailCvtCnyAmt();
//                      中文合同编号
                        String contCnNo = ctrLoanCont.getContCnNo();
//                      合同起始日期
                        String contStartDate = ctrLoanCont.getContStartDate();
//                        合同到期日期
                        String contEndDate = ctrLoanCont.getContEndDate();
//                        执行利率
                        BigDecimal realityIrM = ctrLoanCont.getExecRateYear();
                        if (Objects.isNull(realityIrM)) {
                            realityIrM = new BigDecimal(0);
                        }
                        //根据贷款期限计算出利率
                        String rate = "";
                        DecimalFormat dec = new DecimalFormat("0.####");
                        rate = "0.0435";
                        //计算出利率浮动比例
//                        根据合同号查询货款余额
                        BigDecimal loan_balance = accLoanMapper.selectAccloanByContnoOne(contNo);
                        //动产质押贷额度不循环
                        BigDecimal loan_amt = accLoanMapper.selectAccLoangetByContnogetAmt(contNo);
                        double cont_balance = 0;
//                      是否在途  Y-是   N-否
                        String isOnTheWay = "";
//                        在途金额
                        String onTheWayAmt = "";
                        if ("1".equals(flag)) {
                            cont_balance = bailCvtCnyAmt.subtract(loan_amt).doubleValue();
                        } else {
                            cont_balance = bailCvtCnyAmt.subtract(loan_balance).doubleValue();
                        }
//                        新增在途金额 是否在途字段（物联动产质押贷)  CHNL_SOUR 渠道来源 0是 物联 1是省心快贷
                        if ("1".equals(flag)) {
                            i = pvpLoanAppMapper.countSelectPvpLoanAPPByContno(contNo);
                            if (i > 0) {
//                                属于在途
                                isOnTheWay = "Y";
//                                根据合同号查询放款金额
                                onTheWayAmt = pvpLoanAppMapper.selectPvpLoanAppSumByContno(contNo);
                            } else {
                                isOnTheWay = "N";
                                onTheWayAmt = "0";
                            }

                        }
                        //当前lpr
                        BigDecimal curtLPR = new BigDecimal(0);
                        //利率调整方式
                        String irAdjustType = "";
                        ContList contList = new ContList();
                        contList.setContNo(contNo);
                        contList.setCnContNo(contCnNo);
                        contList.setContAmt(bailCvtCnyAmt);
                        contList.setAvlBal(new BigDecimal(dec.format(cont_balance)));
                        contList.setContStartDate(contStartDate);
                        contList.setContEndDate(contEndDate);
                        contList.setCurtLPR(curtLPR);
                        contList.setIrAdjustType(irAdjustType);
                        contList.setRealityIrY(realityIrM.toString());
                        contList.setOnTheWayAmt(new BigDecimal(onTheWayAmt));
                        contList.setIsOnTheWay(isOnTheWay);
                        List<ContList> contLists = new ArrayList<>();
                        contLists.add(contList);
                        xdht0010DataRespDto.setContList(contLists);
                    }

                }
            }else {

                //根据客户号查找生效的合同到期日超过当天的存在当天的通过的查封记录的省心快贷合同
                Map auditAppParamMap = new HashMap();
                auditAppParamMap.put("openDay",openDay);
                auditAppParamMap.put("cusId",cusId);
                logger.info("根据客户号查找生效的合同到期日超过当天的存在当天的通过的查封记录的省心快贷合同开始:{}", cusId);
                List<CtrLoanCont> ctrLoanContList = ctrLoanContMapper.getsxkdCont(auditAppParamMap);
                logger.info("根据客户号查找生效的合同到期日超过当天的存在当天的通过的查封记录的省心快贷合同结束:{}");
                if (ctrLoanContList != null && ctrLoanContList.size() > 0) {
                    for (int i = 0; i < ctrLoanContList.size(); i++) {
                        CtrLoanCont ctrLoanCont = ctrLoanContList.get(i);
                        // 合同编号
                        String cont_no = (String) ctrLoanCont.getContNo();
                        //获取省心快贷批复额度
                        logger.info("根据合同号获取担保合同和业务的关系信息开始", cont_no);
                        List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByContNo(cont_no);
                        logger.info("根据合同号获取担保合同和业务的关系信息结束");
                        StringBuffer ss = new StringBuffer();
                        for (int j = 0; j < grtGuarBizRstRelList.size(); j++) {
                            GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelList.get(j);
//                            担保合同编号
                            String guarContNo = grtGuarBizRstRel.getGuarContNo();
                            if (j < 1) {
                                ss.append(guarContNo);
                            } else {
                                ss.append(guarContNo + ",");
                            }

                        }
//                     关联的担保合同号   转化为 字符类型
                        String guar_cont_no = ss.toString();
//                      中文合同编号
                        String cont_cn_no = ctrLoanCont.getContCnNo();
//                      申请期限
                        BigDecimal app_term = ctrLoanCont.getAppTerm();
//                       合同起始日期
                        String cont_start_date = ctrLoanCont.getContStartDate();
//                      合同到期日期
                        String cont_end_date = ctrLoanCont.getContEndDate();
//                      合同金额
                        BigDecimal cont_amt = Optional.ofNullable(ctrLoanCont.getHighAvlAmt()).orElse(Optional.ofNullable(ctrLoanCont.getContAmt()).orElse(BigDecimal.ZERO));
//                      折算人民币金额
                        BigDecimal bail_cvt_cny_amt = Optional.ofNullable(ctrLoanCont.getCvtCnyAmt()).orElse(BigDecimal.ZERO);
//                      基准利率
                        BigDecimal ruling_ir = Optional.ofNullable(ctrLoanCont.getRulingIr()).orElse(BigDecimal.ZERO);
//                      利率浮动百分比
                        BigDecimal ir_float_rate = Optional.ofNullable(ctrLoanCont.getIrFloatRate()).orElse(BigDecimal.ZERO);
//                      利率调整周期(月)
                        BigDecimal ir_adjust_term =  ctrLoanCont.getIrAdjustTerm();
//                      授信额度编号
                        String lmt_acc_no =  ctrLoanCont.getLmtAccNo();

                        // 根据合同表授信额度编号查询人民币定价利率申请表信息
                        logger.info("根据合同号获取人民币定价利率信息开始",cont_no);
                        OtherCnyRateAppFinDetails otherCnyRateAppFinDetails =  otherCnyRateAppFinDetailsMapper.selectByLmtAccNo(cont_no);
                        logger.info("根据合同号获取人民币定价利率信息结束",otherCnyRateAppFinDetails);
                        if (Objects.isNull(otherCnyRateAppFinDetails) || StringUtils.isBlank(otherCnyRateAppFinDetails.getExecRateYear().toString()) ||
                                StringUtils.isBlank(otherCnyRateAppFinDetails.getPriceMode())){
                            throw new BizException(null, "", null, "信贷系统人民币利率定价申请未维护！请维护");
                        }

                        // 利率调整类型 01:固定 02：浮动  STD_PRICE_MODE
                        String ir_adjust_type = otherCnyRateAppFinDetails.getPriceMode();
                        // 定价方式
                        String price_mode = "";
                        // 利率调整类型映射 G-固定 F-浮动
                        if("01".equals(ir_adjust_type)){
                            price_mode = "G";
                        } else if("02".equals(ir_adjust_type)){
                            price_mode = "F";
                        } else {
                            logger.info("信贷系统人民币利率获取利率调整方式异常！");
                            throw new BizException(null, "", null, "信贷系统人民币利率获取定价方式异常！");
                        }

                        // 执行利率
                        BigDecimal reality_ir_y = Optional.ofNullable(otherCnyRateAppFinDetails.getExecRateYear()).orElse(BigDecimal.ZERO);

                        // 获取LPR利率 省心快贷默认取一年期Lpr利率
                        BigDecimal db_ir_reality_ir_y = queryLprRate("A1");

                        //  根据授权额度编号获取省心快贷批复额度
                        logger.info("根据授信额度编号获取授信批复台账分项明细表开始",lmt_acc_no);
                        Map  imtReplyAccSubMap = lmtReplyAccSubMapper.selectLmtReplyAccBylmtaccNo(lmt_acc_no);
                        logger.info("根据授信额度编号获取授信批复台账分项明细表结束");
                        double bj_apply_amount = Double.valueOf(cont_amt.toString());
                        String return_date = openDay;
                        if (Objects.nonNull(imtReplyAccSubMap)) {
                            //授信额度
                            double lmt_amt = ((BigDecimal) imtReplyAccSubMap.get("lmtAmt")).doubleValue();
                            // 宽限期（月）
                            String lmt_graper_term = String.valueOf((long) imtReplyAccSubMap.get("lmtGraperTerm"));
                            // 生效日期
                            String upd_date = (String) imtReplyAccSubMap.get("inureDate");
                            SimpleDateFormat sf  = new SimpleDateFormat(DateFormatEnum.DEFAULT.getValue());
                            // 授信期限
                            int lmt_term =  (int)imtReplyAccSubMap.get("lmtTerm");
                            // 最后到期日 =   生效日期 + 授信期限
                            Date expi_date = DateUtils.addMonth( sf.parse(upd_date),lmt_term);
                            // 授信到期日+宽限期
                            //String toString = expi_date.toString();
                            Date jy_expi_date = DateUtils.addMonth(expi_date,Integer.valueOf(lmt_graper_term));
                            String jy_expi_date1 = jy_expi_date.toString();
                            // 取最小日期
                            String end = "";
                            Date endDate = DateUtils.parseDate(cont_end_date, end);
                            int return_expi_date = DateUtils.compare(jy_expi_date, endDate);
                            return_date=(return_expi_date>0)?cont_end_date:jy_expi_date1;


                            double db_apply_amount=Double.parseDouble(bail_cvt_cny_amt.toString());
                            double db_ctr_apply_amount=Double.parseDouble(cont_amt.toString());
                            double db_lmt_apply_amount=lmt_amt;
                            //比较3个申请金额取小
                            bj_apply_amount=Math.min(db_apply_amount,Math.min(db_ctr_apply_amount,db_lmt_apply_amount));

                        }
                        //double bj_apply_amount=(db_apply_amount>db_ctr_apply_amount)?db_ctr_apply_amount:db_apply_amount;
                        DecimalFormat dec = new DecimalFormat("0.####");
                        String jj_balance = accLoanMapper.selectAccLoanBycontno(cont_no);
                        double cont_balance =Double.valueOf(bj_apply_amount)-Double.valueOf(jj_balance);
                        BigDecimal  onTheWayAmt = BigDecimal.ZERO;
                        String  isOnTheWay = "";
                        ContList contList = new ContList();
                        contList.setContNo(cont_no); // 合同号
                        contList.setCnContNo(cont_cn_no); //中文合同编号
                        contList.setContAmt(BigDecimal.valueOf(bj_apply_amount)); //合同金额
                        contList.setAvlBal(new BigDecimal(dec.format(cont_balance))); //可用余额
                        contList.setContStartDate(cont_start_date); //合同起始日
                        contList.setContEndDate(cont_end_date); //合同到期日
                        contList.setCurtLPR(db_ir_reality_ir_y); //当前lpr
                        contList.setIrAdjustType(price_mode); //利率调整方式
                        contList.setRealityIrY(String.valueOf(reality_ir_y)); //执行利率
                        contList.setOnTheWayAmt(onTheWayAmt); //在途金额
                        contList.setIsOnTheWay(isOnTheWay); //是否在途
                        List<ContList> contLists = new ArrayList<>();
                        contLists.add(contList);
                        xdht0010DataRespDto.setContList(contLists);
                    }
                }

            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010DataReqDto));
        return xdht0010DataRespDto;
    }
    /********
     *根据贷款期限查询lpr利率
     * *********/
    public BigDecimal queryLprRate(String newVal) {
        //查询下lpr利率
        BigDecimal curtLprRate = new BigDecimal("0.0485");
        try {
            logger.info("根据贷款期限查询lpr利率开始{}", newVal);
            Map rtnData = iqpLoanAppService.getLprRate(newVal);
            if (rtnData != null && rtnData.containsKey("rate")) {
                curtLprRate = new BigDecimal(rtnData.get("rate").toString());
                logger.info("根据贷款期限查询lpr利率开始{}", curtLprRate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return curtLprRate;
    }

}