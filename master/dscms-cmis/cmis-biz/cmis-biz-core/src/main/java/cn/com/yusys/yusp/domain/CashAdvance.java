/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CashAdvance
 * @类描述: cash_advance垫款处理申请数据实体类
 * @功能描述: 
 * @创建人: zrcbank-fengjj
 * @创建时间: 2021-11-15 15:43:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cash_advance")
public class CashAdvance {

	private static final long serialVersionUID = 1L;

	/** 放款流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PVP_SERNO")
	private String pvpSerno;

	/** 业务申请流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;

	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;

	/** 垫款借据编号 **/
	@Column(name = "CASH_BILL_NO", unique = false, nullable = true, length = 40)
	private String cashBillNo;

	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;

	/** 调查编号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;

	/** 中文合同编号 **/
	@Column(name = "CONT_CN_NO", unique = false, nullable = true, length = 80)
	private String contCnNo;

	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;

	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
	private String cusName;

	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;

	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;

	/** 贷款形式 **/
	@Column(name = "LOAN_MODAL", unique = false, nullable = true, length = 5)
	private String loanModal;

	/** 借新还旧类型 **/
	@Column(name = "REFINANCING_TYPE", unique = false, nullable = true, length = 5)
	private String refinancingType;

	/** 借新还旧标识 **/
	@Column(name = "REFINANCING_FLAG", unique = false, nullable = true, length = 5)
	private String refinancingFlag;

	/** FTP剔除考核 **/
	@Column(name = "FTP", unique = false, nullable = true, length = 5)
	private String ftp;

	/** 是否为疫情相关企业 **/
	@Column(name = "IS_EPIDEMIC_CORRE_CON", unique = false, nullable = true, length = 5)
	private String isEpidemicCorreCon;

	/** 出账模式 **/
	@Column(name = "PVP_MODE", unique = false, nullable = true, length = 5)
	private String pvpMode;

	/** 合同影像是否审核 **/
	@Column(name = "IS_CONT_IMAGE_AUDIT", unique = false, nullable = true, length = 5)
	private String isContImageAudit;

	/** 合同币种 **/
	@Column(name = "CONT_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String contCurType;

	/** 汇率 **/
	@Column(name = "EXCHANGE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exchangeRate;

	/** 折算人民币金额 **/
	@Column(name = "CVT_CNY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cvtCnyAmt;

	/** 合同最高可放金额 **/
	@Column(name = "CONT_HIGH_DISB", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contHighDisb;

	/** 起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 20)
	private String startDate;

	/** 到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 20)
	private String endDate;

	/** 是否用印 **/
	@Column(name = "IS_SEAL", unique = false, nullable = true, length = 5)
	private String isSeal;

	/** 贷款起始日 **/
	@Column(name = "LOAN_START_DATE", unique = false, nullable = true, length = 20)
	private String loanStartDate;

	/** 贷款到期日 **/
	@Column(name = "LOAN_END_DATE", unique = false, nullable = true, length = 20)
	private String loanEndDate;

	/** 贷款期限 **/
	@Column(name = "LOAN_TERM", unique = false, nullable = true, length = 20)
	private String loanTerm;

	/** 贷款期限单位 **/
	@Column(name = "LOAN_TERM_UNIT", unique = false, nullable = true, length = 20)
	private String loanTermUnit;

	/** 利率调整方式 **/
	@Column(name = "RATE_ADJ_MODE", unique = false, nullable = true, length = 5)
	private String rateAdjMode;

	/** 是否分段计息 **/
	@Column(name = "IS_SEG_INTEREST", unique = false, nullable = true, length = 5)
	private String isSegInterest;

	/** 是否自动审批中 **/
	@Column(name = "IS_AUTO_APPR", unique = false, nullable = true, length = 5)
	private String isAutoAppr;

	/** LPR授信利率区间 **/
	@Column(name = "LPR_RATE_INTVAL", unique = false, nullable = true, length = 5)
	private String lprRateIntval;

	/** 当前LPR利率 **/
	@Column(name = "CURT_LPR_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curtLprRate;

	/** 浮动点数 **/
	@Column(name = "RATE_FLOAT_POINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal rateFloatPoint;

	/** 逾期利率浮动比 **/
	@Column(name = "OVERDUE_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueRatePefloat;

	/** 逾期执行利率(年利率) **/
	@Column(name = "OVERDUE_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal overdueExecRate;

	/** 复息利率浮动比 **/
	@Column(name = "CI_RATE_PEFLOAT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciRatePefloat;

	/** 复息执行利率(年利率) **/
	@Column(name = "CI_EXEC_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ciExecRate;

	/** 利率调整选项 **/
	@Column(name = "RATE_ADJ_TYPE", unique = false, nullable = true, length = 5)
	private String rateAdjType;

	/** 下一次利率调整间隔 **/
	@Column(name = "NEXT_RATE_ADJ_INTERVAL", unique = false, nullable = true, length = 10)
	private String nextRateAdjInterval;

	/** 下一次利率调整间隔单位 **/
	@Column(name = "NEXT_RATE_ADJ_UNIT", unique = false, nullable = true, length = 5)
	private String nextRateAdjUnit;

	/** 第一次调整日 **/
	@Column(name = "FIRST_ADJ_DATE", unique = false, nullable = true, length = 20)
	private String firstAdjDate;

	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;

	/** 贷款类别细分 **/
	@Column(name = "LOAN_TYPE_DETAIL", unique = false, nullable = true, length = 5)
	private String loanTypeDetail;

	/** 结息间隔周期 **/
	@Column(name = "EI_INTERVAL_CYCLE", unique = false, nullable = true, length = 10)
	private String eiIntervalCycle;

	/** 结息间隔周期单位 **/
	@Column(name = "EI_INTERVAL_UNIT", unique = false, nullable = true, length = 5)
	private String eiIntervalUnit;

	/** 扣款方式 **/
	@Column(name = "DEDUCT_TYPE", unique = false, nullable = true, length = 5)
	private String deductType;

	/** 扣款日 **/
	@Column(name = "DEDUCT_DAY", unique = false, nullable = true, length = 20)
	private String deductDay;

	/** 贷款发放账号 **/
	@Column(name = "LOAN_PAYOUT_ACCNO", unique = false, nullable = true, length = 20)
	private String loanPayoutAccno;

	/** 贷款发放账号子序号 **/
	@Column(name = "LOAN_PAYOUT_SUB_NO", unique = false, nullable = true, length = 80)
	private String loanPayoutSubNo;

	/** 发放账号名称 **/
	@Column(name = "PAYOUT_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String payoutAcctName;

	/** 是否受托支付 **/
	@Column(name = "IS_BE_ENTRUSTED_PAY", unique = false, nullable = true, length = 5)
	private String isBeEntrustedPay;

	/** 贷款还款账号 **/
	@Column(name = "REPAY_ACCNO", unique = false, nullable = true, length = 80)
	private String repayAccno;

	/** 贷款还款账户子序号 **/
	@Column(name = "REPAY_SUB_ACCNO", unique = false, nullable = true, length = 80)
	private String repaySubAccno;

	/** 还款账户名称 **/
	@Column(name = "REPAY_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String repayAcctName;

	/** 贷款承诺标志 **/
	@Column(name = "LOAN_PROMISE_FLAG", unique = false, nullable = true, length = 5)
	private String loanPromiseFlag;

	/** 贷款承诺类型 **/
	@Column(name = "LOAN_PROMISE_TYPE", unique = false, nullable = true, length = 5)
	private String loanPromiseType;

	/** 贴息人存款账号 **/
	@Column(name = "SBSY_DEP_ACCNO", unique = false, nullable = true, length = 40)
	private String sbsyDepAccno;

	/** 贴息比例 **/
	@Column(name = "SBSY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sbsyPerc;

	/** 贴息到期日 **/
	@Column(name = "SBYS_ENDDATE", unique = false, nullable = true, length = 20)
	private String sbysEnddate;

	/** 是否使用授信额度 **/
	@Column(name = "IS_UTIL_LMT", unique = false, nullable = true, length = 5)
	private String isUtilLmt;

	/** 授信额度编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;

	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;

	/** 是否落实贷款 **/
	@Column(name = "IS_PACT_LOAN", unique = false, nullable = true, length = 5)
	private String isPactLoan;

	/** 是否绿色产业 **/
	@Column(name = "IS_GREEN_INDUSTRY", unique = false, nullable = true, length = 5)
	private String isGreenIndustry;

	/** 是否经营性物业贷款 **/
	@Column(name = "IS_OPER_PROPERTY_LOAN", unique = false, nullable = true, length = 5)
	private String isOperPropertyLoan;

	/** 是否钢贸行业贷款 **/
	@Column(name = "IS_STEEL_LOAN", unique = false, nullable = true, length = 5)
	private String isSteelLoan;

	/** 是否不锈钢行业贷款 **/
	@Column(name = "IS_STAINLESS_LOAN", unique = false, nullable = true, length = 5)
	private String isStainlessLoan;

	/** 是否扶贫贴息贷款 **/
	@Column(name = "IS_POVERTY_RELIEF_LOAN", unique = false, nullable = true, length = 5)
	private String isPovertyReliefLoan;

	/** 是否劳动密集型小企业贴息贷款 **/
	@Column(name = "IS_LABOR_INTEN_SBSY_LOAN", unique = false, nullable = true, length = 5)
	private String isLaborIntenSbsyLoan;

	/** 保障性安居工程贷款 **/
	@Column(name = "GOVER_SUBSZ_HOUSE_LOAN", unique = false, nullable = true, length = 10)
	private String goverSubszHouseLoan;

	/** 项目贷款节能环保 **/
	@Column(name = "ENGY_ENVI_PROTE_LOAN", unique = false, nullable = true, length = 10)
	private String engyEnviProteLoan;

	/** 是否农村综合开发贷款标志 **/
	@Column(name = "IS_CPHS_RUR_DELP_LOAN", unique = false, nullable = true, length = 5)
	private String isCphsRurDelpLoan;

	/** 房地产贷款 **/
	@Column(name = "REALPRO_LOAN", unique = false, nullable = true, length = 5)
	private String realproLoan;

	/** 房产开发贷款资本金比例 **/
	@Column(name = "REALPRO_LOAN_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal realproLoanRate;

	/** 账务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 20)
	private String finaBrId;

	/** 账务机构名称 **/
	@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 40)
	private String finaBrIdName;

	/** 放款机构编号 **/
	@Column(name = "DISB_ORG_NO", unique = false, nullable = true, length = 20)
	private String disbOrgNo;

	/** 放款机构名称 **/
	@Column(name = "DISB_ORG_NAME", unique = false, nullable = true, length = 40)
	private String disbOrgName;

	/** 贷款担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;

	/** 农户类型 **/
	@Column(name = "AGRI_TYPE", unique = false, nullable = true, length = 10)
	private String agriType;

	/** 涉农贷款投向 **/
	@Column(name = "AGRI_LOAN_TER", unique = false, nullable = true, length = 10)
	private String agriLoanTer;

	/** 是否资料补充 **/
	@Column(name = "IS_MATER_COMP", unique = false, nullable = true, length = 1)
	private String isMaterComp;

	/** 担保方式明细 **/
	@Column(name = "GUAR_DETAIL_MODE", unique = false, nullable = true, length = 500)
	private String guarDetailMode;

	/** 贷款投向 **/
	@Column(name = "LOAN_TER", unique = false, nullable = true, length = 20)
	private String loanTer;

	/** 贷款科目号 **/
	@Column(name = "LOAN_SUBJECT_NO", unique = false, nullable = true, length = 20)
	private String loanSubjectNo;

	/** 借款用途类型 **/
	@Column(name = "LOAN_USE_TYPE", unique = false, nullable = true, length = 10)
	private String loanUseType;

	/** 产业结构类型 **/
	@Column(name = "ESTATE_TYPE", unique = false, nullable = true, length = 10)
	private String estateType;

	/** 工业结构转型升级标识 **/
	@Column(name = "INDT_UP_FLAG", unique = false, nullable = true, length = 10)
	private String indtUpFlag;

	/** 战略新兴产业类型 **/
	@Column(name = "STRATEGY_NEW_LOAN", unique = false, nullable = true, length = 10)
	private String strategyNewLoan;

	/** 文化产业标识 **/
	@Column(name = "CUL_INDUSTRY_FLAG", unique = false, nullable = true, length = 10)
	private String culIndustryFlag;

	/** 本金自动归还标志 **/
	@Column(name = "CAP_AUTOBACK_FLAG", unique = false, nullable = true, length = 10)
	private String capAutobackFlag;

	/** 是否贴息 **/
	@Column(name = "IS_SBSY", unique = false, nullable = true, length = 1)
	private String isSbsy;

	/** 节假日是否顺延 **/
	@Column(name = "IS_HOLIDAY_DELAY", unique = false, nullable = true, length = 5)
	private String isHolidayDelay;

	/** 是否计复息 **/
	@Column(name = "IS_METER_CI", unique = false, nullable = true, length = 1)
	private String isMeterCi;

	/** 利息自动归还标志 **/
	@Column(name = "INT_AUTOBACK_FLAG", unique = false, nullable = true, length = 10)
	private String intAutobackFlag;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;

	/** 放款金额 **/
	@Column(name = "CASH_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cashAmt;

	/** 支付方式 **/
	@Column(name = "PAY_MODE", unique = false, nullable = true, length = 5)
	private String payMode;

	/** 是否立即发起受托支付 **/
	@Column(name = "IS_CFIRM_PAY", unique = false, nullable = true, length = 2)
	private String isCfirmPay;

	/** 期限类型 **/
	@Column(name = "TERM_TYPE", unique = false, nullable = true, length = 5)
	private String termType;

	/** 申请期限 **/
	@Column(name = "APP_TERM", unique = false, nullable = true, length = 10)
	private String appTerm;

	/** 客户评级系数 **/
	@Column(name = "CUSTOMER_RATING_FACTOR", unique = false, nullable = true, length = 5)
	private java.math.BigDecimal customerRatingFactor;

	/** 执行年利率 **/
	@Column(name = "EXEC_RATE_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal execRateYear;

	/** 授权状态 **/
	@Column(name = "AUTH_STATUS", unique = false, nullable = true, length = 5)
	private String authStatus;

	/** 渠道来源 **/
	@Column(name = "CHNL_SOUR", unique = false, nullable = true, length = 5)
	private String chnlSour;

	/** 申请状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 40)
	private String belgLine;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 是否先放款后抵押 **/
	@Column(name = "BEFOREHAND_IND", unique = false, nullable = true, length = 5)
	private String beforehandInd;

	/** 借款利率调整日 **/
	@Column(name = "LOAN_RATE_ADJ_DAY", unique = false, nullable = true, length = 5)
	private String loanRateAdjDay;

	/** 正常利率浮动方式 **/
	@Column(name = "IR_FLOAT_TYPE", unique = false, nullable = true, length = 5)
	private String irFloatType;

	/** 利率浮动百分比 **/
	@Column(name = "IR_FLOAT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal irFloatRate;

	/** 是否省心E付(STD_ZB_YES_NO) **/
	@Column(name = "IS_SXEF", unique = false, nullable = true, length = 5)
	private String isSxef;

	/** 其他借款用途 **/
	@Column(name = "OTHER_LOAN_PURP", unique = false, nullable = true, length = 200)
	private String otherLoanPurp;

	/** 是否我行监管账户 **/
	@Column(name = "IS_LOCAL_MANAG", unique = false, nullable = true, length = 5)
	private String isLocalManag;

	/** 是否资产池(STD_ZB_YES_NO) **/
	@Column(name = "IS_POOL", unique = false, nullable = true, length = 5)
	private String isPool;

	/** 返回编码 **/
	@Column(name = "RETURN_CODE", unique = false, nullable = true, length = 5)
	private String returnCode;

	/** 返回说明 **/
	@Column(name = "RETURN_DESC", unique = false, nullable = true, length = 5)
	private String returnDesc;

	/** 核心交易日期**/
	@Column(name = "CORE_TRAN_DATE", unique = false, nullable = true, length = 5)
	private String coreTranDate;

	/** 核心交易流水 **/
	@Column(name = "CORE_TRAN_SERNO", unique = false, nullable = true, length = 5)
	private String coreTranSerno;

	/** 出账状态 **/
	@Column(name = "TRADE_STATUS", unique = false, nullable = true, length = 5)
	private String tradeStatus;

	/** 业务品种 **/
	@Column(name = "TRADE_CODE", unique = false, nullable = true, length = 10)
	private String tradeCode;

	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}

	/**
	 * @return pvpSerno
	 */
	public String getPvpSerno() {
		return this.pvpSerno;
	}

	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

	/**
	 * @return iqpSerno
	 */
	public String getIqpSerno() {
		return this.iqpSerno;
	}

	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	/**
	 * @return billNo
	 */
	public String getBillNo() {
		return this.billNo;
	}

	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}

	/**
	 * @return contNo
	 */
	public String getContNo() {
		return this.contNo;
	}

	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}

	/**
	 * @return surveySerno
	 */
	public String getSurveySerno() {
		return this.surveySerno;
	}

	/**
	 * @param contCnNo
	 */
	public void setContCnNo(String contCnNo) {
		this.contCnNo = contCnNo;
	}

	/**
	 * @return contCnNo
	 */
	public String getContCnNo() {
		return this.contCnNo;
	}

	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}

	/**
	 * @return cusId
	 */
	public String getCusId() {
		return this.cusId;
	}

	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	/**
	 * @return cusName
	 */
	public String getCusName() {
		return this.cusName;
	}

	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}

	/**
	 * @return prdId
	 */
	public String getPrdId() {
		return this.prdId;
	}

	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}

	/**
	 * @return prdName
	 */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}

	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}

	/**
	 * @param loanModal
	 */
	public void setLoanModal(String loanModal) {
		this.loanModal = loanModal;
	}

	/**
	 * @return loanModal
	 */
	public String getLoanModal() {
		return this.loanModal;
	}

	/**
	 * @param refinancingType
	 */
	public void setRefinancingType(String refinancingType) {
		this.refinancingType = refinancingType;
	}

	/**
	 * @return refinancingType
	 */
	public String getRefinancingType() {
		return this.refinancingType;
	}


	/**
	 * @param refinancingFlag
	 */
	public void setRefinancingFlag(String refinancingFlag) {
		this.refinancingFlag = refinancingFlag;
	}

	/**
	 * @return refinancingFlag
	 */
	public String getRefinancingFlag() {
		return this.refinancingFlag;
	}

	/**
	 * @param ftp
	 */
	public void setFtp(String ftp) {
		this.ftp = ftp;
	}

	/**
	 * @return ftp
	 */
	public String getFtp() {
		return this.ftp;
	}

	/**
	 * @param isEpidemicCorreCon
	 */
	public void setIsEpidemicCorreCon(String isEpidemicCorreCon) {
		this.isEpidemicCorreCon = isEpidemicCorreCon;
	}

	/**
	 * @return isEpidemicCorreCon
	 */
	public String getIsEpidemicCorreCon() {
		return this.isEpidemicCorreCon;
	}

	/**
	 * @param pvpMode
	 */
	public void setPvpMode(String pvpMode) {
		this.pvpMode = pvpMode;
	}

	/**
	 * @return pvpMode
	 */
	public String getPvpMode() {
		return this.pvpMode;
	}

	/**
	 * @param isContImageAudit
	 */
	public void setIsContImageAudit(String isContImageAudit) {
		this.isContImageAudit = isContImageAudit;
	}

	/**
	 * @return isContImageAudit
	 */
	public String getIsContImageAudit() {
		return this.isContImageAudit;
	}

	/**
	 * @param contCurType
	 */
	public void setContCurType(String contCurType) {
		this.contCurType = contCurType;
	}

	/**
	 * @return contCurType
	 */
	public String getContCurType() {
		return this.contCurType;
	}


	/**
	 * @return exchangeRate
	 */
	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}

	/**
	 * @param exchangeRate
	 */
	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	/**
	 * @return cvtCnyAmt
	 */
	public BigDecimal getCvtCnyAmt() {
		return this.cvtCnyAmt;
	}

	/**
	 * @param cvtCnyAmt
	 */
	public void setCvtCnyAmt(BigDecimal cvtCnyAmt) {
		this.cvtCnyAmt = cvtCnyAmt;
	}

	/**
	 * @param contHighDisb
	 */
	public void setContHighDisb(java.math.BigDecimal contHighDisb) {
		this.contHighDisb = contHighDisb;
	}

	/**
	 * @return contHighDisb
	 */
	public java.math.BigDecimal getContHighDisb() {
		return this.contHighDisb;
	}

	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return startDate
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return endDate
	 */
	public String getEndDate() {
		return this.endDate;
	}

	/**
	 * @param isSeal
	 */
	public void setIsSeal(String isSeal) {
		this.isSeal = isSeal;
	}

	/**
	 * @return isSeal
	 */
	public String getIsSeal() {
		return this.isSeal;
	}

	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate;
	}

	/**
	 * @return loanStartDate
	 */
	public String getLoanStartDate() {
		return this.loanStartDate;
	}

	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}

	/**
	 * @return loanEndDate
	 */
	public String getLoanEndDate() {
		return this.loanEndDate;
	}

	/**
	 * @param loanTerm
	 */
	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}

	/**
	 * @return loanTerm
	 */
	public String getLoanTerm() {
		return this.loanTerm;
	}

	/**
	 * @param loanTermUnit
	 */
	public void setLoanTermUnit(String loanTermUnit) {
		this.loanTermUnit = loanTermUnit;
	}

	/**
	 * @return loanTermUnit
	 */
	public String getLoanTermUnit() {
		return this.loanTermUnit;
	}

	/**
	 * @param rateAdjMode
	 */
	public void setRateAdjMode(String rateAdjMode) {
		this.rateAdjMode = rateAdjMode;
	}

	/**
	 * @return rateAdjMode
	 */
	public String getRateAdjMode() {
		return this.rateAdjMode;
	}

	/**
	 * @param isSegInterest
	 */
	public void setIsSegInterest(String isSegInterest) {
		this.isSegInterest = isSegInterest;
	}

	/**
	 * @return isSegInterest
	 */
	public String getIsSegInterest() {
		return this.isSegInterest;
	}

	/**
	 * @return isAutoAppr
	 */
	public String getIsAutoAppr() {
		return this.isAutoAppr;
	}

	/**
	 * @param isAutoAppr
	 */
	public void setIsAutoAppr(String isAutoAppr) {
		this.isAutoAppr = isAutoAppr;
	}

	/**
	 * @param lprRateIntval
	 */
	public void setLprRateIntval(String lprRateIntval) {
		this.lprRateIntval = lprRateIntval;
	}

	/**
	 * @return lprRateIntval
	 */
	public String getLprRateIntval() {
		return this.lprRateIntval;
	}

	/**
	 * @param curtLprRate
	 */
	public void setCurtLprRate(java.math.BigDecimal curtLprRate) {
		this.curtLprRate = curtLprRate;
	}

	/**
	 * @return curtLprRate
	 */
	public java.math.BigDecimal getCurtLprRate() {
		return this.curtLprRate;
	}

	/**
	 * @param rateFloatPoint
	 */
	public void setRateFloatPoint(java.math.BigDecimal rateFloatPoint) {
		this.rateFloatPoint = rateFloatPoint;
	}

	/**
	 * @return rateFloatPoint
	 */
	public java.math.BigDecimal getRateFloatPoint() {
		return this.rateFloatPoint;
	}

	/**
	 * @param overdueRatePefloat
	 */
	public void setOverdueRatePefloat(java.math.BigDecimal overdueRatePefloat) {
		this.overdueRatePefloat = overdueRatePefloat;
	}

	/**
	 * @return overdueRatePefloat
	 */
	public java.math.BigDecimal getOverdueRatePefloat() {
		return this.overdueRatePefloat;
	}

	/**
	 * @param overdueExecRate
	 */
	public void setOverdueExecRate(java.math.BigDecimal overdueExecRate) {
		this.overdueExecRate = overdueExecRate;
	}

	/**
	 * @return overdueExecRate
	 */
	public java.math.BigDecimal getOverdueExecRate() {
		return this.overdueExecRate;
	}

	/**
	 * @param ciRatePefloat
	 */
	public void setCiRatePefloat(java.math.BigDecimal ciRatePefloat) {
		this.ciRatePefloat = ciRatePefloat;
	}

	/**
	 * @return ciRatePefloat
	 */
	public java.math.BigDecimal getCiRatePefloat() {
		return this.ciRatePefloat;
	}

	/**
	 * @param ciExecRate
	 */
	public void setCiExecRate(java.math.BigDecimal ciExecRate) {
		this.ciExecRate = ciExecRate;
	}

	/**
	 * @return ciExecRate
	 */
	public java.math.BigDecimal getCiExecRate() {
		return this.ciExecRate;
	}

	/**
	 * @param rateAdjType
	 */
	public void setRateAdjType(String rateAdjType) {
		this.rateAdjType = rateAdjType;
	}

	/**
	 * @return rateAdjType
	 */
	public String getRateAdjType() {
		return this.rateAdjType;
	}

	/**
	 * @param nextRateAdjInterval
	 */
	public void setNextRateAdjInterval(String nextRateAdjInterval) {
		this.nextRateAdjInterval = nextRateAdjInterval;
	}

	/**
	 * @return nextRateAdjInterval
	 */
	public String getNextRateAdjInterval() {
		return this.nextRateAdjInterval;
	}

	/**
	 * @param nextRateAdjUnit
	 */
	public void setNextRateAdjUnit(String nextRateAdjUnit) {
		this.nextRateAdjUnit = nextRateAdjUnit;
	}

	/**
	 * @return nextRateAdjUnit
	 */
	public String getNextRateAdjUnit() {
		return this.nextRateAdjUnit;
	}

	/**
	 * @param firstAdjDate
	 */
	public void setFirstAdjDate(String firstAdjDate) {
		this.firstAdjDate = firstAdjDate;
	}

	/**
	 * @return firstAdjDate
	 */
	public String getFirstAdjDate() {
		return this.firstAdjDate;
	}

	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}

	/**
	 * @return repayMode
	 */
	public String getRepayMode() {
		return this.repayMode;
	}

	/**
	 * @param loanTypeDetail
	 */
	public void setLoanTypeDetail(String loanTypeDetail) {
		this.loanTypeDetail = loanTypeDetail;
	}

	/**
	 * @return loanTypeDetail
	 */
	public String getLoanTypeDetail() {
		return this.loanTypeDetail;
	}

	/**
	 * @param eiIntervalCycle
	 */
	public void setEiIntervalCycle(String eiIntervalCycle) {
		this.eiIntervalCycle = eiIntervalCycle;
	}

	/**
	 * @return eiIntervalCycle
	 */
	public String getEiIntervalCycle() {
		return this.eiIntervalCycle;
	}

	/**
	 * @param eiIntervalUnit
	 */
	public void setEiIntervalUnit(String eiIntervalUnit) {
		this.eiIntervalUnit = eiIntervalUnit;
	}

	/**
	 * @return eiIntervalUnit
	 */
	public String getEiIntervalUnit() {
		return this.eiIntervalUnit;
	}

	/**
	 * @param deductType
	 */
	public void setDeductType(String deductType) {
		this.deductType = deductType;
	}

	/**
	 * @return deductType
	 */
	public String getDeductType() {
		return this.deductType;
	}

	/**
	 * @param deductDay
	 */
	public void setDeductDay(String deductDay) {
		this.deductDay = deductDay;
	}

	/**
	 * @return deductDay
	 */
	public String getDeductDay() {
		return this.deductDay;
	}

	/**
	 * @param loanPayoutAccno
	 */
	public void setLoanPayoutAccno(String loanPayoutAccno) {
		this.loanPayoutAccno = loanPayoutAccno;
	}

	/**
	 * @return loanPayoutAccno
	 */
	public String getLoanPayoutAccno() {
		return this.loanPayoutAccno;
	}

	/**
	 * @param loanPayoutSubNo
	 */
	public void setLoanPayoutSubNo(String loanPayoutSubNo) {
		this.loanPayoutSubNo = loanPayoutSubNo;
	}

	/**
	 * @return loanPayoutSubNo
	 */
	public String getLoanPayoutSubNo() {
		return this.loanPayoutSubNo;
	}

	/**
	 * @param payoutAcctName
	 */
	public void setPayoutAcctName(String payoutAcctName) {
		this.payoutAcctName = payoutAcctName;
	}

	/**
	 * @return payoutAcctName
	 */
	public String getPayoutAcctName() {
		return this.payoutAcctName;
	}

	/**
	 * @param isBeEntrustedPay
	 */
	public void setIsBeEntrustedPay(String isBeEntrustedPay) {
		this.isBeEntrustedPay = isBeEntrustedPay;
	}

	/**
	 * @return isBeEntrustedPay
	 */
	public String getIsBeEntrustedPay() {
		return this.isBeEntrustedPay;
	}

	/**
	 * @param repayAccno
	 */
	public void setRepayAccno(String repayAccno) {
		this.repayAccno = repayAccno;
	}

	/**
	 * @return repayAccno
	 */
	public String getRepayAccno() {
		return this.repayAccno;
	}

	/**
	 * @param repaySubAccno
	 */
	public void setRepaySubAccno(String repaySubAccno) {
		this.repaySubAccno = repaySubAccno;
	}

	/**
	 * @return repaySubAccno
	 */
	public String getRepaySubAccno() {
		return this.repaySubAccno;
	}

	/**
	 * @param repayAcctName
	 */
	public void setRepayAcctName(String repayAcctName) {
		this.repayAcctName = repayAcctName;
	}

	/**
	 * @return repayAcctName
	 */
	public String getRepayAcctName() {
		return this.repayAcctName;
	}

	/**
	 * @param loanPromiseFlag
	 */
	public void setLoanPromiseFlag(String loanPromiseFlag) {
		this.loanPromiseFlag = loanPromiseFlag;
	}

	/**
	 * @return loanPromiseFlag
	 */
	public String getLoanPromiseFlag() {
		return this.loanPromiseFlag;
	}

	/**
	 * @param loanPromiseType
	 */
	public void setLoanPromiseType(String loanPromiseType) {
		this.loanPromiseType = loanPromiseType;
	}

	/**
	 * @return loanPromiseType
	 */
	public String getLoanPromiseType() {
		return this.loanPromiseType;
	}

	/**
	 * @param sbsyDepAccno
	 */
	public void setSbsyDepAccno(String sbsyDepAccno) {
		this.sbsyDepAccno = sbsyDepAccno;
	}

	/**
	 * @return sbsyDepAccno
	 */
	public String getSbsyDepAccno() {
		return this.sbsyDepAccno;
	}

	/**
	 * @param sbsyPerc
	 */
	public void setSbsyPerc(java.math.BigDecimal sbsyPerc) {
		this.sbsyPerc = sbsyPerc;
	}

	/**
	 * @return sbsyPerc
	 */
	public java.math.BigDecimal getSbsyPerc() {
		return this.sbsyPerc;
	}

	/**
	 * @param sbysEnddate
	 */
	public void setSbysEnddate(String sbysEnddate) {
		this.sbysEnddate = sbysEnddate;
	}

	/**
	 * @return sbysEnddate
	 */
	public String getSbysEnddate() {
		return this.sbysEnddate;
	}

	/**
	 * @param isUtilLmt
	 */
	public void setIsUtilLmt(String isUtilLmt) {
		this.isUtilLmt = isUtilLmt;
	}

	/**
	 * @return isUtilLmt
	 */
	public String getIsUtilLmt() {
		return this.isUtilLmt;
	}

	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}

	/**
	 * @return lmtAccNo
	 */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}

	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}

	/**
	 * @return replyNo
	 */
	public String getReplyNo() {
		return this.replyNo;
	}

	/**
	 * @param isPactLoan
	 */
	public void setIsPactLoan(String isPactLoan) {
		this.isPactLoan = isPactLoan;
	}

	/**
	 * @return isPactLoan
	 */
	public String getIsPactLoan() {
		return this.isPactLoan;
	}

	/**
	 * @param isGreenIndustry
	 */
	public void setIsGreenIndustry(String isGreenIndustry) {
		this.isGreenIndustry = isGreenIndustry;
	}

	/**
	 * @return isGreenIndustry
	 */
	public String getIsGreenIndustry() {
		return this.isGreenIndustry;
	}

	/**
	 * @param isOperPropertyLoan
	 */
	public void setIsOperPropertyLoan(String isOperPropertyLoan) {
		this.isOperPropertyLoan = isOperPropertyLoan;
	}

	/**
	 * @return isOperPropertyLoan
	 */
	public String getIsOperPropertyLoan() {
		return this.isOperPropertyLoan;
	}

	/**
	 * @param isSteelLoan
	 */
	public void setIsSteelLoan(String isSteelLoan) {
		this.isSteelLoan = isSteelLoan;
	}

	/**
	 * @return isSteelLoan
	 */
	public String getIsSteelLoan() {
		return this.isSteelLoan;
	}

	/**
	 * @param isStainlessLoan
	 */
	public void setIsStainlessLoan(String isStainlessLoan) {
		this.isStainlessLoan = isStainlessLoan;
	}

	/**
	 * @return isStainlessLoan
	 */
	public String getIsStainlessLoan() {
		return this.isStainlessLoan;
	}

	/**
	 * @param isPovertyReliefLoan
	 */
	public void setIsPovertyReliefLoan(String isPovertyReliefLoan) {
		this.isPovertyReliefLoan = isPovertyReliefLoan;
	}

	/**
	 * @return isPovertyReliefLoan
	 */
	public String getIsPovertyReliefLoan() {
		return this.isPovertyReliefLoan;
	}

	/**
	 * @param isLaborIntenSbsyLoan
	 */
	public void setIsLaborIntenSbsyLoan(String isLaborIntenSbsyLoan) {
		this.isLaborIntenSbsyLoan = isLaborIntenSbsyLoan;
	}

	/**
	 * @return isLaborIntenSbsyLoan
	 */
	public String getIsLaborIntenSbsyLoan() {
		return this.isLaborIntenSbsyLoan;
	}

	/**
	 * @param goverSubszHouseLoan
	 */
	public void setGoverSubszHouseLoan(String goverSubszHouseLoan) {
		this.goverSubszHouseLoan = goverSubszHouseLoan;
	}

	/**
	 * @return goverSubszHouseLoan
	 */
	public String getGoverSubszHouseLoan() {
		return this.goverSubszHouseLoan;
	}

	/**
	 * @param engyEnviProteLoan
	 */
	public void setEngyEnviProteLoan(String engyEnviProteLoan) {
		this.engyEnviProteLoan = engyEnviProteLoan;
	}

	/**
	 * @return engyEnviProteLoan
	 */
	public String getEngyEnviProteLoan() {
		return this.engyEnviProteLoan;
	}

	/**
	 * @param isCphsRurDelpLoan
	 */
	public void setIsCphsRurDelpLoan(String isCphsRurDelpLoan) {
		this.isCphsRurDelpLoan = isCphsRurDelpLoan;
	}

	/**
	 * @return isCphsRurDelpLoan
	 */
	public String getIsCphsRurDelpLoan() {
		return this.isCphsRurDelpLoan;
	}

	/**
	 * @param realproLoan
	 */
	public void setRealproLoan(String realproLoan) {
		this.realproLoan = realproLoan;
	}

	/**
	 * @return realproLoan
	 */
	public String getRealproLoan() {
		return this.realproLoan;
	}

	/**
	 * @param realproLoanRate
	 */
	public void setRealproLoanRate(java.math.BigDecimal realproLoanRate) {
		this.realproLoanRate = realproLoanRate;
	}

	/**
	 * @return realproLoanRate
	 */
	public java.math.BigDecimal getRealproLoanRate() {
		return this.realproLoanRate;
	}

	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}

	/**
	 * @return finaBrId
	 */
	public String getFinaBrId() {
		return this.finaBrId;
	}

	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}

	/**
	 * @return finaBrIdName
	 */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}

	/**
	 * @param disbOrgNo
	 */
	public void setDisbOrgNo(String disbOrgNo) {
		this.disbOrgNo = disbOrgNo;
	}

	/**
	 * @return disbOrgNo
	 */
	public String getDisbOrgNo() {
		return this.disbOrgNo;
	}

	/**
	 * @param disbOrgName
	 */
	public void setDisbOrgName(String disbOrgName) {
		this.disbOrgName = disbOrgName;
	}

	/**
	 * @return disbOrgName
	 */
	public String getDisbOrgName() {
		return this.disbOrgName;
	}

	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}

	/**
	 * @return guarMode
	 */
	public String getGuarMode() {
		return this.guarMode;
	}

	/**
	 * @param agriType
	 */
	public void setAgriType(String agriType) {
		this.agriType = agriType;
	}

	/**
	 * @return agriType
	 */
	public String getAgriType() {
		return this.agriType;
	}

	/**
	 * @param agriLoanTer
	 */
	public void setAgriLoanTer(String agriLoanTer) {
		this.agriLoanTer = agriLoanTer;
	}

	/**
	 * @return agriLoanTer
	 */
	public String getAgriLoanTer() {
		return this.agriLoanTer;
	}

	/**
	 * @param isMaterComp
	 */
	public void setIsMaterComp(String isMaterComp) {
		this.isMaterComp = isMaterComp;
	}

	/**
	 * @return isMaterComp
	 */
	public String getIsMaterComp() {
		return this.isMaterComp;
	}

	/**
	 * @param guarDetailMode
	 */
	public void setGuarDetailMode(String guarDetailMode) {
		this.guarDetailMode = guarDetailMode;
	}

	/**
	 * @return guarDetailMode
	 */
	public String getGuarDetailMode() {
		return this.guarDetailMode;
	}

	/**
	 * @param loanTer
	 */
	public void setLoanTer(String loanTer) {
		this.loanTer = loanTer;
	}

	/**
	 * @return loanTer
	 */
	public String getLoanTer() {
		return this.loanTer;
	}

	/**
	 * @param loanSubjectNo
	 */
	public void setLoanSubjectNo(String loanSubjectNo) {
		this.loanSubjectNo = loanSubjectNo;
	}

	/**
	 * @return loanSubjectNo
	 */
	public String getLoanSubjectNo() {
		return this.loanSubjectNo;
	}

	/**
	 * @param loanUseType
	 */
	public void setLoanUseType(String loanUseType) {
		this.loanUseType = loanUseType;
	}

	/**
	 * @return loanUseType
	 */
	public String getLoanUseType() {
		return this.loanUseType;
	}

	/**
	 * @param estateType
	 */
	public void setEstateType(String estateType) {
		this.estateType = estateType;
	}

	/**
	 * @return estateType
	 */
	public String getEstateType() {
		return this.estateType;
	}

	/**
	 * @param indtUpFlag
	 */
	public void setIndtUpFlag(String indtUpFlag) {
		this.indtUpFlag = indtUpFlag;
	}

	/**
	 * @return indtUpFlag
	 */
	public String getIndtUpFlag() {
		return this.indtUpFlag;
	}

	/**
	 * @param strategyNewLoan
	 */
	public void setStrategyNewLoan(String strategyNewLoan) {
		this.strategyNewLoan = strategyNewLoan;
	}

	/**
	 * @return strategyNewLoan
	 */
	public String getStrategyNewLoan() {
		return this.strategyNewLoan;
	}

	/**
	 * @param culIndustryFlag
	 */
	public void setCulIndustryFlag(String culIndustryFlag) {
		this.culIndustryFlag = culIndustryFlag;
	}

	/**
	 * @return culIndustryFlag
	 */
	public String getCulIndustryFlag() {
		return this.culIndustryFlag;
	}

	/**
	 * @param capAutobackFlag
	 */
	public void setCapAutobackFlag(String capAutobackFlag) {
		this.capAutobackFlag = capAutobackFlag;
	}

	/**
	 * @return capAutobackFlag
	 */
	public String getCapAutobackFlag() {
		return this.capAutobackFlag;
	}

	/**
	 * @param isSbsy
	 */
	public void setIsSbsy(String isSbsy) {
		this.isSbsy = isSbsy;
	}

	/**
	 * @return isSbsy
	 */
	public String getIsSbsy() {
		return this.isSbsy;
	}

	/**
	 * @param isHolidayDelay
	 */
	public void setIsHolidayDelay(String isHolidayDelay) {
		this.isHolidayDelay = isHolidayDelay;
	}

	/**
	 * @return isHolidayDelay
	 */
	public String getIsHolidayDelay() {
		return this.isHolidayDelay;
	}

	/**
	 * @param isMeterCi
	 */
	public void setIsMeterCi(String isMeterCi) {
		this.isMeterCi = isMeterCi;
	}

	/**
	 * @return isMeterCi
	 */
	public String getIsMeterCi() {
		return this.isMeterCi;
	}

	/**
	 * @param intAutobackFlag
	 */
	public void setIntAutobackFlag(String intAutobackFlag) {
		this.intAutobackFlag = intAutobackFlag;
	}

	/**
	 * @return intAutobackFlag
	 */
	public String getIntAutobackFlag() {
		return this.intAutobackFlag;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}

	/**
	 * @return contAmt
	 */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}

	/**
	 * @param cashAmt
	 */
	public void setCashAmt(java.math.BigDecimal cashAmt) {
		this.cashAmt = cashAmt;
	}

	/**
	 * @return cashAmt
	 */
	public java.math.BigDecimal getCashAmt() {
		return this.cashAmt;
	}

	/**
	 * @param payMode
	 */
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	/**
	 * @return payMode
	 */
	public String getPayMode() {
		return this.payMode;
	}

	/**
	 * @param isCfirmPay
	 */
	public void setIsCfirmPay(String isCfirmPay) {
		this.isCfirmPay = isCfirmPay;
	}

	/**
	 * @return isCfirmPay
	 */
	public String getIsCfirmPay() {
		return this.isCfirmPay;
	}

	/**
	 * @param termType
	 */
	public void setTermType(String termType) {
		this.termType = termType;
	}

	/**
	 * @return termType
	 */
	public String getTermType() {
		return this.termType;
	}

	/**
	 * @param appTerm
	 */
	public void setAppTerm(String appTerm) {
		this.appTerm = appTerm;
	}

	/**
	 * @return appTerm
	 */
	public String getAppTerm() {
		return this.appTerm;
	}

	/**
	 * @param customerRatingFactor
	 */
	public void setCustomerRatingFactor(java.math.BigDecimal customerRatingFactor) {
		this.customerRatingFactor = customerRatingFactor;
	}

	/**
	 * @return customerRatingFactor
	 */
	public java.math.BigDecimal getCustomerRatingFactor() {
		return this.customerRatingFactor;
	}

	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}

	/**
	 * @return execRateYear
	 */
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}

	/**
	 * @param authStatus
	 */
	public void setAuthStatus(String authStatus) {
		this.authStatus = authStatus;
	}

	/**
	 * @return authStatus
	 */
	public String getAuthStatus() {
		return this.authStatus;
	}

	/**
	 * @param chnlSour
	 */
	public void setChnlSour(String chnlSour) {
		this.chnlSour = chnlSour;
	}

	/**
	 * @return chnlSour
	 */
	public String getChnlSour() {
		return this.chnlSour;
	}

	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	/**
	 * @return approveStatus
	 */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}

	/**
	 * @return belgLine
	 */
	public String getBelgLine() {
		return this.belgLine;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd;
	}

	/**
	 * @return beforehandInd
	 */
	public String getBeforehandInd() {
		return this.beforehandInd;
	}

	/**
	 * @param loanRateAdjDay
	 */
	public void setLoanRateAdjDay(String loanRateAdjDay) {
		this.loanRateAdjDay = loanRateAdjDay;
	}

	/**
	 * @return loanRateAdjDay
	 */
	public String getLoanRateAdjDay() {
		return this.loanRateAdjDay;
	}

	/**
	 * @param irFloatType
	 */
	public void setIrFloatType(String irFloatType) {
		this.irFloatType = irFloatType;
	}

	/**
	 * @return irFloatType
	 */
	public String getIrFloatType() {
		return this.irFloatType;
	}

	/**
	 * @param irFloatRate
	 */
	public void setIrFloatRate(java.math.BigDecimal irFloatRate) {
		this.irFloatRate = irFloatRate;
	}

	/**
	 * @return irFloatRate
	 */
	public java.math.BigDecimal getIrFloatRate() {
		return this.irFloatRate;
	}

	/**
	 * @param isSxef
	 */
	public void setIsSxef(String isSxef) {
		this.isSxef = isSxef;
	}

	/**
	 * @return isSxef
	 */
	public String getIsSxef() {
		return this.isSxef;
	}

	/**
	 * @param otherLoanPurp
	 */
	public void setOtherLoanPurp(String otherLoanPurp) {
		this.otherLoanPurp = otherLoanPurp;
	}

	/**
	 * @return otherLoanPurp
	 */
	public String getOtherLoanPurp() {
		return this.otherLoanPurp;
	}

	public String getIsLocalManag() {
		return isLocalManag;
	}

	public void setIsLocalManag(String isLocalManag) {
		this.isLocalManag = isLocalManag;
	}

	public String getIsPool() {
		return isPool;
	}

	public void setIsPool(String isPool) {
		this.isPool = isPool;
	}

	public String getCashBillNo() {
		return cashBillNo;
	}

	public void setCashBillNo(String cashBillNo) {
		this.cashBillNo = cashBillNo;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getReturnDesc() {
		return returnDesc;
	}

	public void setReturnDesc(String returnDesc) {
		this.returnDesc = returnDesc;
	}

	public String getCoreTranDate() {
		return coreTranDate;
	}

	public void setCoreTranDate(String coreTranDate) {
		this.coreTranDate = coreTranDate;
	}

	public String getCoreTranSerno() {
		return coreTranSerno;
	}

	public void setCoreTranSerno(String coreTranSerno) {
		this.coreTranSerno = coreTranSerno;
	}

	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public String getTradeCode() {
		return tradeCode;
	}

	public void setTradeCode(String tradeCode) {
		this.tradeCode = tradeCode;
	}
}