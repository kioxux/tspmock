/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtInspectInfo;
import cn.com.yusys.yusp.dto.server.xdxw0010.req.Xdxw0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0011.req.Xdxw0011DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtInspectInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-13 17:13:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtInspectInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtInspectInfo selectByPrimaryKey(@Param("inspectSerno") String inspectSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtInspectInfo> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtInspectInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtInspectInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtInspectInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtInspectInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("inspectSerno") String inspectSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtInspectInfo> selectBySurveySerno(String surveySerno);

    /**
     * 勘验列表信息查询
     * @param xdxw0010DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0010.resp.List> getInspectInfoList(Xdxw0010DataReqDto xdxw0010DataReqDto);

    /**
     * 勘验任务查询
     * @param certCode
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdxw0078.resp.List> getxw0078List(@Param("certCode") String certCode);

    /**
     * 更新勘验信息表状态
     * @param map
     * @return
     */
    int updateXw0079InspectStatus(HashMap map);


    /**
     * 提交勘验信息
     * @param xdxw0011DataReqDto
     * @return
     */
    int updateLmtInspectInfo(Xdxw0011DataReqDto xdxw0011DataReqDto);
}