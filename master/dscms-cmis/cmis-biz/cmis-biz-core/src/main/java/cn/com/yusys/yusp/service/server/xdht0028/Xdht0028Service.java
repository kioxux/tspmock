package cn.com.yusys.yusp.service.server.xdht0028;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdht0028.req.Xdht0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0028.resp.Xdht0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0028Service
 * @类描述: #服务类
 * @功能描述:根据合同号取得客户经理电话号码
 * @创建人: xll
 * @创建时间: 2021-05-14 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0028Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0028Service.class);

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;

    /**
     * 交易码：Xdht0028
     * 交易描述：根据合同号取得客户经理电话号码
     *
     * @param xdht0028DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0028DataRespDto xdht0028(Xdht0028DataReqDto xdht0028DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028DataReqDto));
        //定义返回信息
        Xdht0028DataRespDto xdht0028DataRespDto = new Xdht0028DataRespDto();
        //得到请求字段
        String contNo = xdht0028DataReqDto.getContNo();//获取合同号
        //用户电话
        String userMobilephone = "";

        //业务处理开始
        try {
            //设置查询参数
            Map queryMap = new HashMap();
            queryMap.put("contNo", contNo);//合同号
            //查询客户经理工号
            String managerId = ctrLoanContMapper.queryCtrLoanContManagerIdByContNo(queryMap);
            if(StringUtil.isEmpty(managerId)){
                return xdht0028DataRespDto;
            }
            // 根据工号获取用户表中的联系电话
            logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
            ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
            logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
            String code = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                AdminSmUserDto adminSmUserDto = resultDto.getData();
                userMobilephone = adminSmUserDto.getUserMobilephone();
            }
            //返回信息
            xdht0028DataRespDto.setManagerId(managerId);
            if (StringUtil.isNotEmpty(userMobilephone)) {//查询结果不为空
                xdht0028DataRespDto.setManager_phone(userMobilephone);// 客户经理电话号码
            } else {
                xdht0028DataRespDto.setManager_phone(StringUtils.EMPTY);// 客户经理电话号码
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028DataRespDto));
        return xdht0028DataRespDto;
    }
}
