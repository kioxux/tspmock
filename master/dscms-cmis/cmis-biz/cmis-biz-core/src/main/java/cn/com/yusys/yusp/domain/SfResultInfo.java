/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SfResultInfo
 * @类描述: sf_result_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-04 21:30:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "sf_result_info")
public class SfResultInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 核心客户号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 40)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 10)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 手机银行录入信息是否准确 **/
	@Column(name = "IS_MOBILE_BANK_INFO", unique = false, nullable = true, length = 10)
	private String isMobileBankInfo;
	
	/** 是否收集现场照片 **/
	@Column(name = "IS_SCENE_PHOTO", unique = false, nullable = true, length = 10)
	private String isScenePhoto;
	
	/** 是否完成抵押物勘验表 **/
	@Column(name = "IS_PLDIMN_INSPECT", unique = false, nullable = true, length = 10)
	private String isPldimnInspect;
	
	/** 房产抵押物情况 **/
	@Column(name = "HOUSE_PLD_CASE", unique = false, nullable = true, length = 10)
	private String housePldCase;
	
	/** 是否完成经营场所勘验表 **/
	@Column(name = "IS_OPER_INSPECT", unique = false, nullable = true, length = 10)
	private String isOperInspect;
	
	/** 企业经营情况 **/
	@Column(name = "OPER_CASE", unique = false, nullable = true, length = 10)
	private String operCase;
	
	/** 经营企业近2年无逾期贷款 **/
	@Column(name = "IS_CORP_OVERDUE_LOAN", unique = false, nullable = true, length = 10)
	private String isCorpOverdueLoan;
	
	/** 经营企业无风险分类后三类贷款 **/
	@Column(name = "IS_CORP_THREE_LOAN", unique = false, nullable = true, length = 10)
	private String isCorpThreeLoan;
	
	/** 借款人是否有配偶 **/
	@Column(name = "IS_CUS_SPOUSE", unique = false, nullable = true, length = 10)
	private String isCusSpouse;
	
	/** 配偶近2年无逾期经营性贷款 **/
	@Column(name = "IS_SPOUSE_OPER_LOAN", unique = false, nullable = true, length = 10)
	private String isSpouseOperLoan;
	
	/** 配偶近2年房贷、消费贷逾期不超过5次 **/
	@Column(name = "IS_SPOUSE_OVERDUE_TIMES", unique = false, nullable = true, length = 10)
	private String isSpouseOverdueTimes;
	
	/** 配偶无风险分类后三类贷款 **/
	@Column(name = "IS_SPOUSE_THREE_LOAN", unique = false, nullable = true, length = 10)
	private String isSpouseThreeLoan;
	
	/** 是否予以业务准入 **/
	@Column(name = "IS_ADMIT", unique = false, nullable = true, length = 10)
	private String isAdmit;
	
	/** 提交时间 **/
	@Column(name = "SUBMIT_DATE", unique = false, nullable = true, length = 40)
	private String submitDate;
	
	/** 流程状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 10)
	private String approveStatus;

	/** 信息修改状态 **/
	@Column(name = "CRP_STATUS", unique = false, nullable = true, length = 10)
	private String crpStatus;

	/** 处理人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 10)
	private String managerId;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param isMobileBankInfo
	 */
	public void setIsMobileBankInfo(String isMobileBankInfo) {
		this.isMobileBankInfo = isMobileBankInfo;
	}
	
    /**
     * @return isMobileBankInfo
     */
	public String getIsMobileBankInfo() {
		return this.isMobileBankInfo;
	}
	
	/**
	 * @param isScenePhoto
	 */
	public void setIsScenePhoto(String isScenePhoto) {
		this.isScenePhoto = isScenePhoto;
	}
	
    /**
     * @return isScenePhoto
     */
	public String getIsScenePhoto() {
		return this.isScenePhoto;
	}
	
	/**
	 * @param isPldimnInspect
	 */
	public void setIsPldimnInspect(String isPldimnInspect) {
		this.isPldimnInspect = isPldimnInspect;
	}
	
    /**
     * @return isPldimnInspect
     */
	public String getIsPldimnInspect() {
		return this.isPldimnInspect;
	}
	
	/**
	 * @param housePldCase
	 */
	public void setHousePldCase(String housePldCase) {
		this.housePldCase = housePldCase;
	}
	
    /**
     * @return housePldCase
     */
	public String getHousePldCase() {
		return this.housePldCase;
	}
	
	/**
	 * @param isOperInspect
	 */
	public void setIsOperInspect(String isOperInspect) {
		this.isOperInspect = isOperInspect;
	}
	
    /**
     * @return isOperInspect
     */
	public String getIsOperInspect() {
		return this.isOperInspect;
	}
	
	/**
	 * @param operCase
	 */
	public void setOperCase(String operCase) {
		this.operCase = operCase;
	}
	
    /**
     * @return operCase
     */
	public String getOperCase() {
		return this.operCase;
	}
	
	/**
	 * @param isCorpOverdueLoan
	 */
	public void setIsCorpOverdueLoan(String isCorpOverdueLoan) {
		this.isCorpOverdueLoan = isCorpOverdueLoan;
	}
	
    /**
     * @return isCorpOverdueLoan
     */
	public String getIsCorpOverdueLoan() {
		return this.isCorpOverdueLoan;
	}
	
	/**
	 * @param isCorpThreeLoan
	 */
	public void setIsCorpThreeLoan(String isCorpThreeLoan) {
		this.isCorpThreeLoan = isCorpThreeLoan;
	}
	
    /**
     * @return isCorpThreeLoan
     */
	public String getIsCorpThreeLoan() {
		return this.isCorpThreeLoan;
	}
	
	/**
	 * @param isCusSpouse
	 */
	public void setIsCusSpouse(String isCusSpouse) {
		this.isCusSpouse = isCusSpouse;
	}
	
    /**
     * @return isCusSpouse
     */
	public String getIsCusSpouse() {
		return this.isCusSpouse;
	}
	
	/**
	 * @param isSpouseOperLoan
	 */
	public void setIsSpouseOperLoan(String isSpouseOperLoan) {
		this.isSpouseOperLoan = isSpouseOperLoan;
	}
	
    /**
     * @return isSpouseOperLoan
     */
	public String getIsSpouseOperLoan() {
		return this.isSpouseOperLoan;
	}
	
	/**
	 * @param isSpouseOverdueTimes
	 */
	public void setIsSpouseOverdueTimes(String isSpouseOverdueTimes) {
		this.isSpouseOverdueTimes = isSpouseOverdueTimes;
	}
	
    /**
     * @return isSpouseOverdueTimes
     */
	public String getIsSpouseOverdueTimes() {
		return this.isSpouseOverdueTimes;
	}
	
	/**
	 * @param isSpouseThreeLoan
	 */
	public void setIsSpouseThreeLoan(String isSpouseThreeLoan) {
		this.isSpouseThreeLoan = isSpouseThreeLoan;
	}
	
    /**
     * @return isSpouseThreeLoan
     */
	public String getIsSpouseThreeLoan() {
		return this.isSpouseThreeLoan;
	}
	
	/**
	 * @param isAdmit
	 */
	public void setIsAdmit(String isAdmit) {
		this.isAdmit = isAdmit;
	}
	
    /**
     * @return isAdmit
     */
	public String getIsAdmit() {
		return this.isAdmit;
	}
	
	/**
	 * @param submitDate
	 */
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}
	
    /**
     * @return submitDate
     */
	public String getSubmitDate() {
		return this.submitDate;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}

	/**
	 * @param crpStatus
	 */
	public void setCrpStatus(String crpStatus) {
		this.crpStatus = crpStatus;
	}

	/**
	 * @return crpStatus
	 */
	public String getCrpStatus() {
		return this.crpStatus;
	}


	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}