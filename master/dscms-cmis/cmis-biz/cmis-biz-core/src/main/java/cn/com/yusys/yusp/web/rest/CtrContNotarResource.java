/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CtrContNotarSaveDto;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CtrContNotar;
import cn.com.yusys.yusp.service.CtrContNotarService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContNotarResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-30 19:17:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/ctrcontnotar")
public class CtrContNotarResource {
    private static final Logger log = LoggerFactory.getLogger(CtrContNotarService.class);

    @Autowired
    private CtrContNotarService ctrContNotarService;

	/**
     * 全表查询.
     * 
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CtrContNotar>> query() {
        QueryModel queryModel = new QueryModel();
        List<CtrContNotar> list = ctrContNotarService.selectAll(queryModel);
        return new ResultDto<List<CtrContNotar>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CtrContNotar>> index(QueryModel queryModel) {
        List<CtrContNotar> list = ctrContNotarService.selectByModel(queryModel);
        return new ResultDto<List<CtrContNotar>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CtrContNotar> show(@PathVariable("pkId") String pkId) {
        CtrContNotar ctrContNotar = ctrContNotarService.selectByPrimaryKey(pkId);
        return new ResultDto<CtrContNotar>(ctrContNotar);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CtrContNotar> create(@RequestBody CtrContNotar ctrContNotar) throws URISyntaxException {
        ctrContNotarService.insert(ctrContNotar);
        return new ResultDto<CtrContNotar>(ctrContNotar);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CtrContNotar ctrContNotar) throws URISyntaxException {
        int result = ctrContNotarService.update(ctrContNotar);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = ctrContNotarService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = ctrContNotarService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称: savectrcontnotar
     * @方法描述: 合同公正信息保存
     * @参数与返回说明:
     * @算法描述: 无
     */

    @PostMapping("/savectrcontnotar")
    protected ResultDto<Integer> savectrcontnotar(@RequestBody CtrContNotarSaveDto ctrContNotarSaveDto) throws  URISyntaxException{
        log.info("查询合同编号请求信息【{}】", JSONObject.toJSON(ctrContNotarSaveDto));
        int rtnData = ctrContNotarService.savectrcontnotar(ctrContNotarSaveDto);
        return new ResultDto<Integer>(rtnData);
    }
}
