/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtReplyChg;
import cn.com.yusys.yusp.service.LmtReplyChgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyChgResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-22 10:22:18
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtreplychg")
public class LmtReplyChgResource {

    @Autowired
    private LmtReplyChgService lmtReplyChgService;

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtReplyChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtReplyChg> list = lmtReplyChgService.selectAll(queryModel);
        return new ResultDto<List<LmtReplyChg>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtReplyChg>> index(QueryModel queryModel) {
        List<LmtReplyChg> list = lmtReplyChgService.selectByModel(queryModel);
        return new ResultDto<List<LmtReplyChg>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtReplyChg> show(@PathVariable("pkId") String pkId) {
        LmtReplyChg lmtReplyChg = lmtReplyChgService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtReplyChg>(lmtReplyChg);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtReplyChg> create(@RequestBody LmtReplyChg lmtReplyChg) throws URISyntaxException {
        lmtReplyChgService.insert(lmtReplyChg);
        return new ResultDto<LmtReplyChg>(lmtReplyChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtReplyChg lmtReplyChg) throws URISyntaxException {
        int result = lmtReplyChgService.update(lmtReplyChg);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtReplyChgService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtReplyChgService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
/*
    @PostMapping("/queryByManagerId")
    protected ResultDto<List<LmtReplyChg>> queryByManagerId(@RequestBody QueryModel queryModel) {
        List<LmtReplyChg> list = lmtReplyChgService.queryByManagerId(queryModel);
        return new ResultDto<>(list);
    }*/

    /**
     * @方法名称：insertAllReplyChg
     * @方法描述：批复变更时，将批复表落到批复变更表里
     * @算法描述：
     * @创建人：zhangming12
     * @创建时间：2021-04-27 上午 9:53
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/insertAllReplyChg")
    protected ResultDto<Map<String, Object>> insertAllReplyChg(@RequestBody LmtReplyChg lmtReplyChg) {
        return ResultDto.success(lmtReplyChgService.insertAllReplyChg(lmtReplyChg));
    }

    /**
     * @方法名称：getAllReplyChgInfo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/14 8:58
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/getAllReplyChgInfo")
    protected ResultDto<Map<String, Object>> getAllReplyChgInfo(@RequestBody String serno) {
        HashMap<String, String> reqMap = new HashMap<>();
        reqMap.put("serno", serno);
        Map<String, Object> RstData = lmtReplyChgService.getAllReplyChgInfo(reqMap);
        return new ResultDto<>(RstData);
    }

    /**
     * @方法名称：modifyReplyChg
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/14 8:58
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/modifyReplyChg")
    protected ResultDto<Integer> modifyReplyChg(@RequestBody LmtReplyChg lmtReplyChg) {
        int result = lmtReplyChgService.updateSelective(lmtReplyChg);
        return new ResultDto<>(result);
    }

    /**
     * @方法名称：deleteReplyChg
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/14 9:01
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/deleteReplyChg")
    protected ResultDto<Integer> deleteReplyChg(@RequestBody LmtReplyChg lmtReplyChg){
        int rowCount = lmtReplyChgService.deleteReplyChg(lmtReplyChg);
        return new ResultDto<>(rowCount);
    }

    /**
     * @方法名称：replyChg
     * @方法描述：获取批复变更申请
     * @创建人：zhangming12
     * @创建时间：2021/5/14 14:49
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/replychg")
    protected ResultDto<List<LmtReplyChg>> replyChg(@RequestBody QueryModel queryModel){
        List<LmtReplyChg> list = lmtReplyChgService.queryByManagerId(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @方法名称：replyChgHis
     * @方法描述：获取批复变更历史
     * @创建人：zhangming12
     * @创建时间：2021/5/14 14:49
     * @修改记录：修改时间 修改人员 修改时间
    */
    @PostMapping("/replychghis")
    protected ResultDto<List<LmtReplyChg>> replyChgHis(@RequestBody QueryModel queryModel){
        List<LmtReplyChg> lmtReplyChgHisList = lmtReplyChgService.getLmtReplyChgHis(queryModel);
        return new ResultDto<>(lmtReplyChgHisList);
    }

    /**
     * @方法名称：selectBySerno
     * @方法描述：获取批复变更历史
     * @创建人：zhangming12
     * @创建时间：2021/5/14 14:49
     * @修改记录：修改时间 修改人员 修改时间
     */
    @PostMapping("/selectbyserno")
    protected ResultDto<LmtReplyChg> selectBySerno(@RequestBody String serno){
        LmtReplyChg lmtReplyChg = lmtReplyChgService.queryLmtReplyChgBySerno(serno);
        return new ResultDto<>(lmtReplyChg);
    }
}
