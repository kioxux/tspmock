package cn.com.yusys.yusp.service.risk;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.CusIndivAllDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.req.CmisLmt0038ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038PrdListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0038.resp.CmisLmt0038RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.common.CommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 个人经营性贷款时借款人年龄校验
 * TODO 最高额授信协议申请 待校验
 */
@Service
public class RiskItem0009Service {

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskItem0009Service.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtSurveyReportMainInfoService lmtSurveyReportMainInfoService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    //CTR_LOAN_CONT
    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private LmtReplyAccSubService lmtReplyAccSubService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Resource
    private ICusClientService icusClientService;

    /**
     * @方法名称: riskItem0009
     * @方法描述: 个人经营性贷款时借款人年龄校验
     * @参数与返回说明:
     * @算法描述: 1、授信申请提交时点触发：
     * （1）借款人以个人名义申请的经营性贷款，若当前时点年龄超过65周岁，则拦截
     * 2、合同/放款申请提交时点触发：
     * （1）借款人以个人名义申请的经营性贷款，若贷款到期日时点借款人年龄超过65周岁，则拦截
     * ---解释：个人经营性贷款目录下所有产品
     * @创建人: lixy
     * @创建时间: 2021-06-21 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0009(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String cusId = queryModel.getCondition().get("bizUserId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("个人经营性贷款时借款人年龄校验开始*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }

        // 根据客户编号查询其管护客户信息
        log.info("通过客户编号：" + cusId + " 调客户接口查询客户基本信息开始," + "请求报文：" + cusId);
        CusBaseClientDto cusBaseDtoResultDto = icusClientService.queryCus(cusId);
        log.info("通过客户编号：" + cusId + " 调客户接口查询客户基本信息开始," + "响应报文：" + cusBaseDtoResultDto.toString());
        if(CmisCusConstants.STD_ZB_CUS_CATALOG_2.equals(cusBaseDtoResultDto.getCusCatalog())){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015);
            return riskResultDto;
        }

        //授信分项-客户号-判断是否为个人（cusId） 产品名称（LMT_BIZ_TYPE）
        //判断单一客户授信、集团授信
        if (CmisFlowConstants.FLOW_TYPE_TYPE_SINGLE_LMT.contains(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_SX008_TO_SX014.indexOf(bizType + ",") != -1) {
            log.info("个人经营性贷款时借款人年龄校验执行中（单一客户授信、集团授信）*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
            Map<String, String> map = new HashMap<>();
            map.put("serno", serno);
            map.put("oprType", "01");
            List<LmtAppSub> lmtAppSubList = lmtAppSubService.selectByParams(map);
            if (Objects.isNull(lmtAppSubList) || lmtAppSubList.size() == 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0004);
                return riskResultDto;
            }
            for (LmtAppSub lmtAppSub : lmtAppSubList) {
                List<LmtAppSubPrd> lmtAppSubPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                if (CollectionUtils.nonEmpty(lmtAppSubPrdList)) {
                    for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrdList) {
                        //判断当前贷款产品是否为 个人经营贷
                        String lmtBizType = lmtAppSubPrd.getLmtBizType();
                        if (StringUtils.nonEmpty(lmtBizType) && CmisBizConstants.GE_REN_JING_YING_XING_DAI_KUAN_LMT_PREFIX.indexOf(lmtBizType + ",") != -1) {
                            //进行人员信息判断
                            //判断当前人员是否为个人 并且 授信品种为个人经营贷
                            riskResultDto = checkCusInfo(lmtAppSubPrd.getCusId(),"");
                            if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                                return riskResultDto;
                            }
                        }
                    }
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0013);
                    return riskResultDto;
                }
            }
        }
        //小微授信调查业务场景编号
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_XW001.equals(bizType)) {
            log.info("个人经营性贷款时借款人年龄校验执行中（小微授信调查业务场景编号）*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
//            LMT_SURVEY_REPORT_MAIN_INFO
            LmtSurveyReportMainInfo lmtSurveyReportMainInfo = lmtSurveyReportMainInfoService.selectByPrimaryKey(serno);
            if (lmtSurveyReportMainInfo != null) {
                //判断是否为个人经营贷
                String prdId = lmtSurveyReportMainInfo.getPrdId();
                if (StringUtils.nonEmpty(prdId) && CmisBizConstants.GE_REN_JING_YING_XING_DAI_KUAN_PRD.indexOf(prdId + ",") != -1) {
                    //进行人员信息判断
                    //判断当前人员是否为个人 并且 授信品种为个人经营贷
                    riskResultDto = checkCusInfo(lmtSurveyReportMainInfo.getCusId(),"");
                    if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        //最高额授信协议申请
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX001.equals(bizType)) {
            log.info("个人经营性贷款时借款人年龄校验执行中（最高额授信协议申请）*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByHighAmtAgrSernoKey(serno);
            if (iqpHighAmtAgrApp != null) {
                List<LmtReplyAccSubPrd> lmtReplyAccSubPrdList = lmtReplyAccSubPrdService.selectByAccSubNo(iqpHighAmtAgrApp.getLmtAccNo());
                if(CollectionUtils.isEmpty(lmtReplyAccSubPrdList)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00091);
                    return riskResultDto;
                }
                for (LmtReplyAccSubPrd lmtReplyAccSubPrd : lmtReplyAccSubPrdList) {
                    String lmtBizType = lmtReplyAccSubPrd.getLmtBizType();
                    CmisLmt0038ReqDto cmisLmt0038ReqDto = new CmisLmt0038ReqDto();
                    cmisLmt0038ReqDto.setLimitSubNo(lmtBizType);
                    log.info("通过授信品种编号【{}】，前往额度系统查询对应的用信产品开始,请求报文为:【{}】", lmtBizType, cmisLmt0038ReqDto.toString());
                    ResultDto<CmisLmt0038RespDto> resultDtoDto = cmisLmtClientService.cmislmt0038(cmisLmt0038ReqDto);
                    log.info("通过授信品种编号【{}】，前往额度系统查询对应的用信产品结束,响应报文为:【{}】", lmtBizType, resultDtoDto.toString());
                    if(!"0".equals(resultDtoDto.getCode())){
                        log.error("接口调用失败！");
                        throw BizException.error(null, EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
                    }
                    if(!"0000".equals(resultDtoDto.getData().getErrorCode())){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(resultDtoDto.getData().getErrorMsg());
                        return riskResultDto;
                    }
                    List<CmisLmt0038PrdListRespDto> respDtoList = resultDtoDto.getData().getCmisLmt0038PrdListRespDtoList();
                    if(CollectionUtils.isEmpty(respDtoList)){
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(resultDtoDto.getData().getErrorMsg());
                        return riskResultDto;
                    }
                    for (CmisLmt0038PrdListRespDto cmisLmt0038PrdListRespDto : respDtoList) {
                        String prdId = cmisLmt0038PrdListRespDto.getPrdId();
                        if (StringUtils.nonEmpty(prdId) && CmisBizConstants.GE_REN_JING_YING_XING_DAI_KUAN_PRD.contains(prdId)) {
                            riskResultDto = checkCusInfo(iqpHighAmtAgrApp.getCusId(),iqpHighAmtAgrApp.getEndDate());
                            if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                                return riskResultDto;
                            }
                        }
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        //IQP_LOAN_APP  普通贷款合同申请
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_YX002.equals(bizType)) {
            log.info("个人经营性贷款时借款人年龄校验执行中（普通贷款合同申请）*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectBySerno(serno);
            if (iqpLoanApp != null) {
                String prdId = iqpLoanApp.getPrdId();
                if (StringUtils.nonEmpty(prdId) && CmisBizConstants.GE_REN_JING_YING_XING_DAI_KUAN_PRD.contains(prdId)) {
                    //进行人员信息判断
                    //判断当前人员是否为个人 并且 授信品种为个人经营贷
                    riskResultDto = checkCusInfo(iqpLoanApp.getCusId(),iqpLoanApp.getEndDate());
                    if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }

        //FLOW_TYPE_TYPE_XW002 小微放款申请 （小微合同申请）、 对公贷款出账申请 （PvpLoanApp）
        else if (CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType)
                || CmisFlowConstants.FLOW_TYPE_TYPE_YX011.equals(bizType)) {
            log.info("个人经营性贷款时借款人年龄校验执行中（小微放款申请 （小微合同申请）、 对公贷款出账申请）*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if (pvpLoanApp != null) {
                String prdId = pvpLoanApp.getPrdId();
                if (StringUtils.nonEmpty(prdId) && prdId.indexOf(CmisBizConstants.GE_REN_JING_YING_XING_DAI_KUAN_PRD) == 0) {
                    //进行人员信息判断
                    //判断当前人员是否为个人 并且 授信品种为个人经营贷
                    riskResultDto = checkCusInfo(pvpLoanApp.getCusId(),"");
                    if (StringUtils.nonEmpty(riskResultDto.getRiskResultType())) {
                        return riskResultDto;
                    }
                }
            } else {
                //授信数据获取失败 RISK_ERROR_0005
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
                return riskResultDto;
            }
        }
        log.info("个人经营性贷款时借款人年龄校验结束*******************业务流水号：【{}】，流程类型：【{}】", serno, bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * 判断当前分项是否为个人，并且人员年龄超过65
     *
     * @param cusId
     * @return
     */
    private RiskResultDto checkCusInfo(String cusId,String endDateStr) {
        log.info("个人经营性贷款时借款人年龄校验开始（判定年龄是否超过65岁）*******************");
        RiskResultDto riskResultDto = new RiskResultDto();
        CusBaseDto cusBase = commonService.getCusBaseByCusId(cusId);
        if (CmisBizConstants.STD_ZB_CUS_CATALOG_1.equals(cusBase.getCusCatalog())) {
            //获取当前人员个人信息
            CusIndivAllDto cusIndiv = commonService.getCusIndivAllDtoByCusId(cusId);
            if (cusIndiv != null) {
                //获取出生日期
                String indivDtOfBirth = cusIndiv.getIndivDtOfBirth();
                Date birthDay = new Date();
                //计算年龄
                if (!StringUtils.isBlank(indivDtOfBirth)) {
                    try {
                        //获取出生年月日
                        if(indivDtOfBirth.contains("-")){
                            birthDay = DateUtils.parseDate(indivDtOfBirth, DateUtils.PATTERN_DEFAULT);
                        }else if(indivDtOfBirth.contains("/")){
                            birthDay = DateUtils.parseDate(indivDtOfBirth.replace("/","-"), DateUtils.PATTERN_DEFAULT);
                        }else {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                            Date date = sdf.parse(indivDtOfBirth);
                            String birthDayString = new SimpleDateFormat("yyyy-MM-dd").format(date);
                            birthDay = DateUtils.parseDate(birthDayString, DateUtils.PATTERN_DEFAULT);
                        }

                        //获取当前年份
                        Date currDay = DateUtils.getCurrDate();
                        if (DateUtils.addYear(birthDay, 65).before(currDay)) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00903);
                            return riskResultDto;
                        }
                        if(StringUtils.nonBlank(endDateStr)){
                            // 若合同到期时借款人年龄已超过65岁，则拦截
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            Date endDate = sdf.parse(endDateStr);
                            if(DateUtils.addYear(birthDay, 65).before(endDate)){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00092);
                                return riskResultDto;
                            }
                        }
                    } catch (NumberFormatException | ParseException e) {
                        log.error("年龄计算失败！", e);
                        //当前借款人年龄计算失败
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00904);
                        return riskResultDto;
                    }
                } else {
                    //当前授信人员出生年份获取失败
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00902);
                    return riskResultDto;
                }
            } else {
                //当前授信人员个人信息获取失败
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00901);
                return riskResultDto;
            }
        }
        log.info("个人经营性贷款时借款人年龄校验结束（判定年龄是否超过65岁）*******************");
        return riskResultDto;
    }

}
