package cn.com.yusys.yusp.web.server.xdcz0007;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0007.req.List;
import cn.com.yusys.yusp.dto.server.xdcz0007.req.Xdcz0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0007.resp.Xdcz0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdcz0007.Xdcz0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:出账记录详情查看
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0007:出账记录详情查看")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.xdcz0007.BizXdcz0007Resource.class);

    @Autowired
    private Xdcz0007Service xdcz0007Service;
    /**
     * 交易码：xdcz0007
     * 交易描述：出账记录详情查看
     *
     * @param xdcz0007DataReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("出账记录详情查看")
    @PostMapping("/xdcz0007")
    protected @ResponseBody
    ResultDto<Xdcz0007DataRespDto>  xdcz0007(@Validated @RequestBody Xdcz0007DataReqDto xdcz0007DataReqDto ) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, JSON.toJSONString(xdcz0007DataReqDto));
        Xdcz0007DataRespDto  xdcz0007DataRespDto  = new Xdcz0007DataRespDto();// 响应Dto:电子保函开立
        ResultDto<Xdcz0007DataRespDto>xdcz0007DataResultDto = new ResultDto<>();
        String oprtype = xdcz0007DataReqDto.getOprtype();//操作类型
        String contNo = xdcz0007DataReqDto.getContNo();//合同号
        BigDecimal loanAmt = xdcz0007DataReqDto.getLoanAmt();//放款金额
        BigDecimal loanAmtExchgRat = xdcz0007DataReqDto.getLoanAmtExchgRat();//放款金额汇率
        java.util.List<List> list = xdcz0007DataReqDto.getList();
        /*String curType = xdcz0007DataReqDto.getCurType();//币种
        BigDecimal bail = xdcz0007DataReqDto.getBail();//保证金
        BigDecimal bailExchgRate = xdcz0007DataReqDto.getBailExchgRate();//保证金汇率*/
        try {
            // 从xdcz0007DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xdcz0007DataRespDto = xdcz0007Service.xdcz0007(xdcz0007DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xdcz0007DataRespDto对象开始
            Xdcz0007DataRespDto xdcz0007RespDto = new Xdcz0007DataRespDto(); //响应Data:查询敞口额度及保证金校验
            // TODO 封装xdcz0007DataRespDto对象结束
            // 封装xdcz0007DataResultDto中正确的返回码和返回信息
            xdcz0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value,e.getMessage());
            // 封装xdcz0007DataResultDto中异常返回码和返回信息
            xdcz0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0007DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value,e.getMessage());
            // 封装xdcz0007DataResultDto中异常返回码和返回信息
            xdcz0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0007DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0007DataRespDto到xdcz0007DataResultDto中
        xdcz0007DataResultDto.setData(xdcz0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, JSON.toJSONString(xdcz0007DataRespDto));
        return xdcz0007DataResultDto;
    }
}
