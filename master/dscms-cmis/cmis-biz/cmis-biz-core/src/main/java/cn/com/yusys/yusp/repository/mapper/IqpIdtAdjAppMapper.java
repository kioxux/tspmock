/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.IqpIdtAdjApp;
import cn.com.yusys.yusp.domain.ReyPlan;
import org.apache.ibatis.annotations.Param;

/**
 * 授信审批通知书有效期mapper
 */
public interface IqpIdtAdjAppMapper {
    /**
     * 插入从借据表中插入的数据
     * @param record
     * @return
     */
    int insertSelective(IqpIdtAdjApp record);

    /**
     * 根据授信协议编号查询是否存在在途的变更业务
     * @param lmtCtrNo
     * @return
     */
    int checkIsExistWFByBillNo(@Param("lmtCtrNo") String lmtCtrNo);

    /**
     * 根据主键查询数据
     * @param iqpSerno
     * @return
     */
    IqpIdtAdjApp selectByPrimaryKey(String iqpSerno);

    /**
     * 根据传入的主键宇状态更改审批状态
     * @param iqpSerno
     * @param approveStatus
     * @return
     */
    int updateApproveStatusByIqpSerno(@Param("iqpSerno")String iqpSerno,@Param("approveStatus") String approveStatus);

    /**
     * @方法名称: updateAccLoanRepayWayByBillNo
     * @方法描述: 通过借据号更新贷款台账还款方式
     * @参数与返回说明:iqpRepayWayChg
     * @算法描述:
     */
    void updateAccLoanRepayWayByBillNo(IqpIdtAdjApp iqpIdtAdjApp);

    /**
     * 向iqpRepayPlan表中插入数据
     * @param iqpIdtAdjApp
     */
    void insertIntoIqpRepayPlan(IqpIdtAdjApp iqpIdtAdjApp);

    //根据借据号查询借据信息
    AccLoan selectPvpLoanAppByBillNo(String billNo);

    /**
     * 根据主键更改审批状态
     * @param iqpIdtAdjApp
     * @return
     */
    int updateByPrimaryKeySelective(IqpIdtAdjApp iqpIdtAdjApp);
}