/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportBasicInfo
 * @类描述: lmt_survey_report_basic_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-13 21:56:31
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_survey_report_basic_info")
public class LmtSurveyReportBasicInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SURVEY_SERNO")
	private String surveySerno;
	
	/** 客户姓名 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 40)
	private String certCode;
	
	/** 电话号码 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 11)
	private String phone;
	
	/** 工作单位 **/
	@Column(name = "WORK_UNIT", unique = false, nullable = true, length = 200)
	private String workUnit;
	
	/** 婚姻状况 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
	private String marStatus;
	
	/** 配偶姓名 **/
	@Column(name = "SPOUSE_NAME", unique = false, nullable = true, length = 35)
	private String spouseName;
	
	/** 配偶证件号码 **/
	@Column(name = "SPOUSE_CERT_CODE", unique = false, nullable = true, length = 40)
	private String spouseCertCode;
	
	/** 配偶电话 **/
	@Column(name = "SPOUSE_PHONE", unique = false, nullable = true, length = 30)
	private String spousePhone;
	
	/** 有无子女 **/
	@Column(name = "IS_HAVE_CHILDREN", unique = false, nullable = true, length = 2)
	private String isHaveChildren;
	
	/** 学历 **/
	@Column(name = "EDU", unique = false, nullable = true, length = 10)
	private String edu;
	
	/** 居住年限 **/
	@Column(name = "RESI_YEARS", unique = false, nullable = true, length = 20)
	private String resiYears;
	
	/** 居住地址 **/
	@Column(name = "LIVING_ADDR", unique = false, nullable = true, length = 500)
	private String livingAddr;
	
	/** 是否线上抵押 **/
	@Column(name = "IS_ONLINE_PLD", unique = false, nullable = true, length = 1)
	private String isOnlinePld;
	
	/** 申请金额 **/
	@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;

	/** 是否提前申贷 **/
	@Column(name = "IS_TQSD", unique = false, nullable = true, length = 5)
	private String isTqsd;

	/** 申贷类型 **/
	@Column(name = "APP_LOAN_WAY", unique = false, nullable = true, length = 5)
	private String appLoanWay;

	/** 申贷类型 **/
	@Column(name = "IS_CWHB", unique = false, nullable = true, length = 5)
	private String isCwhb;

	/** 无还本模型利率 **/
	@Column(name = "WHB_MODEL_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal whbModelRate;

	/** 无还本模型金额（元） **/
	@Column(name = "WHB_MODEL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal whbModelAmt;

	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;

	/** 模型建议金额 **/
	@Column(name = "MODEL_ADVICE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelAdviceAmt;
	
	/** 模型建议利率 **/
	@Column(name = "MODEL_ADVICE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal modelAdviceRate;
	
	/** 参考利率 **/
	@Column(name = "REF_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal refRate;
	
	/** 建议金额 **/
	@Column(name = "ADVICE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adviceAmt;
	
	/** 建议利率 **/
	@Column(name = "ADVICE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adviceRate;
	
	/** 配偶客户编号 **/
	@Column(name = "SPOUSE_CUS_ID", unique = false, nullable = true, length = 40)
	private String spouseCusId;
	
	/** 建议期限 **/
	@Column(name = "ADVICE_TERM", unique = false, nullable = true, length = 20)
	private String adviceTerm;
	
	/** 还款方式 **/
	@Column(name = "REPAY_MODE", unique = false, nullable = true, length = 5)
	private String repayMode;
	
	/** 是否续贷 **/
	@Column(name = "IS_RENEW_LOAN", unique = false, nullable = true, length = 1)
	private String isRenewLoan;
	
	/** 是否现场勘验 **/
	@Column(name = "IS_SCENE_INQUEST", unique = false, nullable = true, length = 1)
	private String isSceneInquest;
	
	/** 模型初步结果 **/
	@Column(name = "MODEL_FST_RESULT", unique = false, nullable = true, length = 5)
	private String modelFstResult;
	
	/** 贷款用途 **/
	@Column(name = "LOAN_PURP", unique = false, nullable = true, length = 5)
	private String loanPurp;
	
	/** 是否农户 **/
	@Column(name = "IS_AGRI", unique = false, nullable = true, length = 1)
	private String isAgri;
	
	/** 是否新员工 **/
	@Column(name = "IS_NEW_EMPLOYEE", unique = false, nullable = true, length = 1)
	private String isNewEmployee;
	
	/** 新员工名称 **/
	@Column(name = "NEW_EMPLOYEE_NAME", unique = false, nullable = true, length = 80)
	private String newEmployeeName;
	
	/** 新员工电话 **/
	@Column(name = "NEW_EMPLOYEE_PHONE", unique = false, nullable = true, length = 11)
	private String newEmployeePhone;
	
	/** 原借据号 **/
	@Column(name = "OLD_BILL_NO", unique = false, nullable = true, length = 40)
	private String oldBillNo;
	
	/** 原借据金额 **/
	@Column(name = "OLD_BILL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldBillAmt;
	
	/** 原借据余额 **/
	@Column(name = "OLD_BILL_BALANCE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldBillBalance;
	
	/** 原借据利率 **/
	@Column(name = "OLD_BILL_LOAN_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oldBillLoanRate;
	
	/** 借款人负债较上期增加是否超50% **/
	@Column(name = "DEBT_FLAG", unique = false, nullable = true, length = 1)
	private String debtFlag;
	
	/** 借款人对外是否提供过多担保或大量资产被抵押 **/
	@Column(name = "GUAR_FLAG", unique = false, nullable = true, length = 1)
	private String guarFlag;
	
	/** 借款人及其家庭是否发生意外 **/
	@Column(name = "ACCIDENT_FLAG", unique = false, nullable = true, length = 1)
	private String accidentFlag;
	
	/** 抵/质押物是否异常 **/
	@Column(name = "GUARANTEE_FLAG", unique = false, nullable = true, length = 1)
	private String guaranteeFlag;
	
	/** 借款人经营活动是否正常 **/
	@Column(name = "ACTIVITY_FLAG", unique = false, nullable = true, length = 1)
	private String activityFlag;
	
	/** 借款人经营所有权是否发生重大变化 **/
	@Column(name = "MANAGEMENT_BELONG_FLAG", unique = false, nullable = true, length = 1)
	private String managementBelongFlag;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 营销人工号 **/
	@Column(name = "MAR_ID", unique = false, nullable = true, length = 20)
	private String marId;
	
	/** 单人调查流水号 **/
	@Column(name = "SINGLE_SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String singleSurveySerno;
	
	/** 调查模式 **/
	@Column(name = "SURVEY_MODE", unique = false, nullable = true, length = 5)
	private String surveyMode;

	public String getIsTqsd() {
		return isTqsd;
	}

	public void setIsTqsd(String isTqsd) {
		this.isTqsd = isTqsd;
	}

	public String getAppLoanWay() {
		return appLoanWay;
	}

	public void setAppLoanWay(String appLoanWay) {
		this.appLoanWay = appLoanWay;
	}

	public String getIsCwhb() {
		return isCwhb;
	}

	public void setIsCwhb(String isCwhb) {
		this.isCwhb = isCwhb;
	}

	public BigDecimal getWhbModelRate() {
		return whbModelRate;
	}

	public void setWhbModelRate(BigDecimal whbModelRate) {
		this.whbModelRate = whbModelRate;
	}

	public BigDecimal getWhbModelAmt() {
		return whbModelAmt;
	}

	public void setWhbModelAmt(BigDecimal whbModelAmt) {
		this.whbModelAmt = whbModelAmt;
	}

	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param workUnit
	 */
	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}
	
    /**
     * @return workUnit
     */
	public String getWorkUnit() {
		return this.workUnit;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}
	
    /**
     * @return marStatus
     */
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param spouseName
	 */
	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}
	
    /**
     * @return spouseName
     */
	public String getSpouseName() {
		return this.spouseName;
	}
	
	/**
	 * @param spouseCertCode
	 */
	public void setSpouseCertCode(String spouseCertCode) {
		this.spouseCertCode = spouseCertCode;
	}
	
    /**
     * @return spouseCertCode
     */
	public String getSpouseCertCode() {
		return this.spouseCertCode;
	}
	
	/**
	 * @param spousePhone
	 */
	public void setSpousePhone(String spousePhone) {
		this.spousePhone = spousePhone;
	}
	
    /**
     * @return spousePhone
     */
	public String getSpousePhone() {
		return this.spousePhone;
	}
	
	/**
	 * @param isHaveChildren
	 */
	public void setIsHaveChildren(String isHaveChildren) {
		this.isHaveChildren = isHaveChildren;
	}
	
    /**
     * @return isHaveChildren
     */
	public String getIsHaveChildren() {
		return this.isHaveChildren;
	}
	
	/**
	 * @param edu
	 */
	public void setEdu(String edu) {
		this.edu = edu;
	}
	
    /**
     * @return edu
     */
	public String getEdu() {
		return this.edu;
	}
	
	/**
	 * @param resiYears
	 */
	public void setResiYears(String resiYears) {
		this.resiYears = resiYears;
	}
	
    /**
     * @return resiYears
     */
	public String getResiYears() {
		return this.resiYears;
	}
	
	/**
	 * @param livingAddr
	 */
	public void setLivingAddr(String livingAddr) {
		this.livingAddr = livingAddr;
	}
	
    /**
     * @return livingAddr
     */
	public String getLivingAddr() {
		return this.livingAddr;
	}
	
	/**
	 * @param isOnlinePld
	 */
	public void setIsOnlinePld(String isOnlinePld) {
		this.isOnlinePld = isOnlinePld;
	}
	
    /**
     * @return isOnlinePld
     */
	public String getIsOnlinePld() {
		return this.isOnlinePld;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return appAmt
     */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param modelAdviceAmt
	 */
	public void setModelAdviceAmt(java.math.BigDecimal modelAdviceAmt) {
		this.modelAdviceAmt = modelAdviceAmt;
	}
	
    /**
     * @return modelAdviceAmt
     */
	public java.math.BigDecimal getModelAdviceAmt() {
		return this.modelAdviceAmt;
	}
	
	/**
	 * @param modelAdviceRate
	 */
	public void setModelAdviceRate(java.math.BigDecimal modelAdviceRate) {
		this.modelAdviceRate = modelAdviceRate;
	}
	
    /**
     * @return modelAdviceRate
     */
	public java.math.BigDecimal getModelAdviceRate() {
		return this.modelAdviceRate;
	}
	
	/**
	 * @param refRate
	 */
	public void setRefRate(java.math.BigDecimal refRate) {
		this.refRate = refRate;
	}
	
    /**
     * @return refRate
     */
	public java.math.BigDecimal getRefRate() {
		return this.refRate;
	}
	
	/**
	 * @param adviceAmt
	 */
	public void setAdviceAmt(java.math.BigDecimal adviceAmt) {
		this.adviceAmt = adviceAmt;
	}
	
    /**
     * @return adviceAmt
     */
	public java.math.BigDecimal getAdviceAmt() {
		return this.adviceAmt;
	}
	
	/**
	 * @param adviceRate
	 */
	public void setAdviceRate(java.math.BigDecimal adviceRate) {
		this.adviceRate = adviceRate;
	}
	
    /**
     * @return adviceRate
     */
	public java.math.BigDecimal getAdviceRate() {
		return this.adviceRate;
	}
	
	/**
	 * @param spouseCusId
	 */
	public void setSpouseCusId(String spouseCusId) {
		this.spouseCusId = spouseCusId;
	}
	
    /**
     * @return spouseCusId
     */
	public String getSpouseCusId() {
		return this.spouseCusId;
	}
	
	/**
	 * @param adviceTerm
	 */
	public void setAdviceTerm(String adviceTerm) {
		this.adviceTerm = adviceTerm;
	}
	
    /**
     * @return adviceTerm
     */
	public String getAdviceTerm() {
		return this.adviceTerm;
	}
	
	/**
	 * @param repayMode
	 */
	public void setRepayMode(String repayMode) {
		this.repayMode = repayMode;
	}
	
    /**
     * @return repayMode
     */
	public String getRepayMode() {
		return this.repayMode;
	}
	
	/**
	 * @param isRenewLoan
	 */
	public void setIsRenewLoan(String isRenewLoan) {
		this.isRenewLoan = isRenewLoan;
	}
	
    /**
     * @return isRenewLoan
     */
	public String getIsRenewLoan() {
		return this.isRenewLoan;
	}
	
	/**
	 * @param isSceneInquest
	 */
	public void setIsSceneInquest(String isSceneInquest) {
		this.isSceneInquest = isSceneInquest;
	}
	
    /**
     * @return isSceneInquest
     */
	public String getIsSceneInquest() {
		return this.isSceneInquest;
	}
	
	/**
	 * @param modelFstResult
	 */
	public void setModelFstResult(String modelFstResult) {
		this.modelFstResult = modelFstResult;
	}
	
    /**
     * @return modelFstResult
     */
	public String getModelFstResult() {
		return this.modelFstResult;
	}
	
	/**
	 * @param loanPurp
	 */
	public void setLoanPurp(String loanPurp) {
		this.loanPurp = loanPurp;
	}
	
    /**
     * @return loanPurp
     */
	public String getLoanPurp() {
		return this.loanPurp;
	}
	
	/**
	 * @param isAgri
	 */
	public void setIsAgri(String isAgri) {
		this.isAgri = isAgri;
	}
	
    /**
     * @return isAgri
     */
	public String getIsAgri() {
		return this.isAgri;
	}
	
	/**
	 * @param isNewEmployee
	 */
	public void setIsNewEmployee(String isNewEmployee) {
		this.isNewEmployee = isNewEmployee;
	}
	
    /**
     * @return isNewEmployee
     */
	public String getIsNewEmployee() {
		return this.isNewEmployee;
	}
	
	/**
	 * @param newEmployeeName
	 */
	public void setNewEmployeeName(String newEmployeeName) {
		this.newEmployeeName = newEmployeeName;
	}
	
    /**
     * @return newEmployeeName
     */
	public String getNewEmployeeName() {
		return this.newEmployeeName;
	}
	
	/**
	 * @param newEmployeePhone
	 */
	public void setNewEmployeePhone(String newEmployeePhone) {
		this.newEmployeePhone = newEmployeePhone;
	}
	
    /**
     * @return newEmployeePhone
     */
	public String getNewEmployeePhone() {
		return this.newEmployeePhone;
	}
	
	/**
	 * @param oldBillNo
	 */
	public void setOldBillNo(String oldBillNo) {
		this.oldBillNo = oldBillNo;
	}
	
    /**
     * @return oldBillNo
     */
	public String getOldBillNo() {
		return this.oldBillNo;
	}
	
	/**
	 * @param oldBillAmt
	 */
	public void setOldBillAmt(java.math.BigDecimal oldBillAmt) {
		this.oldBillAmt = oldBillAmt;
	}
	
    /**
     * @return oldBillAmt
     */
	public java.math.BigDecimal getOldBillAmt() {
		return this.oldBillAmt;
	}
	
	/**
	 * @param oldBillBalance
	 */
	public void setOldBillBalance(java.math.BigDecimal oldBillBalance) {
		this.oldBillBalance = oldBillBalance;
	}
	
    /**
     * @return oldBillBalance
     */
	public java.math.BigDecimal getOldBillBalance() {
		return this.oldBillBalance;
	}
	
	/**
	 * @param oldBillLoanRate
	 */
	public void setOldBillLoanRate(java.math.BigDecimal oldBillLoanRate) {
		this.oldBillLoanRate = oldBillLoanRate;
	}
	
    /**
     * @return oldBillLoanRate
     */
	public java.math.BigDecimal getOldBillLoanRate() {
		return this.oldBillLoanRate;
	}
	
	/**
	 * @param debtFlag
	 */
	public void setDebtFlag(String debtFlag) {
		this.debtFlag = debtFlag;
	}
	
    /**
     * @return debtFlag
     */
	public String getDebtFlag() {
		return this.debtFlag;
	}
	
	/**
	 * @param guarFlag
	 */
	public void setGuarFlag(String guarFlag) {
		this.guarFlag = guarFlag;
	}
	
    /**
     * @return guarFlag
     */
	public String getGuarFlag() {
		return this.guarFlag;
	}
	
	/**
	 * @param accidentFlag
	 */
	public void setAccidentFlag(String accidentFlag) {
		this.accidentFlag = accidentFlag;
	}
	
    /**
     * @return accidentFlag
     */
	public String getAccidentFlag() {
		return this.accidentFlag;
	}
	
	/**
	 * @param guaranteeFlag
	 */
	public void setGuaranteeFlag(String guaranteeFlag) {
		this.guaranteeFlag = guaranteeFlag;
	}
	
    /**
     * @return guaranteeFlag
     */
	public String getGuaranteeFlag() {
		return this.guaranteeFlag;
	}
	
	/**
	 * @param activityFlag
	 */
	public void setActivityFlag(String activityFlag) {
		this.activityFlag = activityFlag;
	}
	
    /**
     * @return activityFlag
     */
	public String getActivityFlag() {
		return this.activityFlag;
	}
	
	/**
	 * @param managementBelongFlag
	 */
	public void setManagementBelongFlag(String managementBelongFlag) {
		this.managementBelongFlag = managementBelongFlag;
	}
	
    /**
     * @return managementBelongFlag
     */
	public String getManagementBelongFlag() {
		return this.managementBelongFlag;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param marId
	 */
	public void setMarId(String marId) {
		this.marId = marId;
	}
	
    /**
     * @return marId
     */
	public String getMarId() {
		return this.marId;
	}
	
	/**
	 * @param singleSurveySerno
	 */
	public void setSingleSurveySerno(String singleSurveySerno) {
		this.singleSurveySerno = singleSurveySerno;
	}
	
    /**
     * @return singleSurveySerno
     */
	public String getSingleSurveySerno() {
		return this.singleSurveySerno;
	}
	
	/**
	 * @param surveyMode
	 */
	public void setSurveyMode(String surveyMode) {
		this.surveyMode = surveyMode;
	}
	
    /**
     * @return surveyMode
     */
	public String getSurveyMode() {
		return this.surveyMode;
	}


}