package cn.com.yusys.yusp.web.server.xdtz0035;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0035.req.Xdtz0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0035.resp.Xdtz0035DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0035.Xdtz0035Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDTZ0035:根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0035Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0035Resource.class);

    @Autowired
    private Xdtz0035Service xdtz0035Service;

    /**
     * 交易码：xdtz0035
     * 交易描述：根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
     *
     * @param xdtz0035DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录")
    @PostMapping("/xdtz0035")
    protected @ResponseBody
    ResultDto<Xdtz0035DataRespDto> xdtz0035(@Validated @RequestBody Xdtz0035DataReqDto xdtz0035DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035DataReqDto));
        Xdtz0035DataRespDto xdtz0035DataRespDto = new Xdtz0035DataRespDto();// 响应Dto:根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
        ResultDto<Xdtz0035DataRespDto> xdtz0035DataResultDto = new ResultDto<>();
        try {
            // 借据编号
            String billNo = xdtz0035DataReqDto.getBillNo();
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035DataReqDto));
            xdtz0035DataRespDto = xdtz0035Service.xdtz0035(xdtz0035DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035DataRespDto));
            // 封装xdtz0035DataResultDto中正确的返回码和返回信息
            xdtz0035DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0035DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, e.getMessage());
            // 封装xdtz0035DataResultDto中异常返回码和返回信息
            xdtz0035DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0035DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0035DataRespDto到xdtz0035DataResultDto中
        xdtz0035DataResultDto.setData(xdtz0035DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035DataResultDto));
        return xdtz0035DataResultDto;
    }
}
