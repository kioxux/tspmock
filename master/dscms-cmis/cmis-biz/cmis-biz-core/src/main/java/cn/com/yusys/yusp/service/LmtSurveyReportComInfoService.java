package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtSurveyReportComInfo;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.BASIC;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.Data;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ENT_INFO;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.dto.req.QueryEnterpriseReqDto;
import cn.com.yusys.yusp.dto.resp.QueryEnterpriseRespDto;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportComInfoMapper;
import cn.com.yusys.yusp.service.client.bsp.outerdata.zsnew.ZsnewService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportComInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2021-04-20 14:19:38
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class LmtSurveyReportComInfoService {

    private static final Logger logger = LoggerFactory.getLogger(LmtSurveyReportComInfo.class);

    @Autowired
    private LmtSurveyReportComInfoMapper lmtSurveyReportComInfoMapper;

    @Autowired
    private ZsnewService zsnewService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public LmtSurveyReportComInfo selectByPrimaryKey(String surveySerno) {
        return lmtSurveyReportComInfoMapper.selectByPrimaryKey(surveySerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<LmtSurveyReportComInfo> selectAll(QueryModel model) {
        List<LmtSurveyReportComInfo> records = (List<LmtSurveyReportComInfo>) lmtSurveyReportComInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<LmtSurveyReportComInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtSurveyReportComInfo> list = lmtSurveyReportComInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(LmtSurveyReportComInfo record) {
        return lmtSurveyReportComInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(LmtSurveyReportComInfo record) {
        return lmtSurveyReportComInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(LmtSurveyReportComInfo record) {
        return lmtSurveyReportComInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(LmtSurveyReportComInfo record) {
        return lmtSurveyReportComInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String surveySerno) {
        return lmtSurveyReportComInfoMapper.deleteByPrimaryKey(surveySerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return lmtSurveyReportComInfoMapper.deleteByIds(ids);
    }

    /**
     * @param lmtSurveyReportComInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto>
     * @author hubp
     * @date 2021/5/24 10:10
     * @version 1.0.0
     * @desc 查询工商信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto<LmtSurveyReportComInfo> queryEnterprise(LmtSurveyReportComInfo lmtSurveyReportComInfo) {
        logger.info("查询工商信息开始..................");
        ZsnewRespDto zsnewRespDto = null;
        // 企业名称
        String entreName = "";
        // 企业证件类型
        String entreCertType = "";
        // 企业证件号码
        String entreCertNo = "";
        try {
            ZsnewReqDto zsnewReqDto = new ZsnewReqDto();
            zsnewReqDto.setName(lmtSurveyReportComInfo.getConName());

            // 根据不同证件类型填充不同字段
            if ("R".equals(lmtSurveyReportComInfo.getCorpCertType())) {
                // 统一社会信用代码
                zsnewReqDto.setCreditcode(lmtSurveyReportComInfo.getCorpCertCode());
            } else if ("Q".equals(lmtSurveyReportComInfo.getCorpCertType())){
                // 组织机构代码
                zsnewReqDto.setOrgcode(lmtSurveyReportComInfo.getCorpCertCode());
            } else if ("M".equals(lmtSurveyReportComInfo.getCorpCertType())) {
                // 营业执照
                zsnewReqDto.setRegno(lmtSurveyReportComInfo.getCorpCertCode());
            }
            zsnewRespDto = zsnewService.zsnew(zsnewReqDto);
            if (null != zsnewRespDto && null != zsnewRespDto.getData()) {
                Data data = zsnewRespDto.getData();
                //查询成功后返回
                if (null != data.getCODE() && "200".equals(data.getCODE())) {
                    ENT_INFO entInfo = (ENT_INFO) data.getENT_INFO();
                    BASIC basic = entInfo.getBASIC();
                    // 2021年11月20日15:58:36 hubp 问题编号：20211119-00025
                    lmtSurveyReportComInfo.setConName(basic.getENTNAME());
                    //法人代表
                    lmtSurveyReportComInfo.setLegal(basic.getFRNAME());
                    //企业类型
                    String enttype = basic.getENTTYPE();
                    enttype = enttype.contains("个体") ? "2" : "1";
                    lmtSurveyReportComInfo.setCorpType(enttype);

                    //统一信用代码
                    entreCertNo = basic.getCREDITCODE();
                    if (!StringUtils.isEmpty(entreCertNo)) {
                        // 统一社会信用代码
                        entreCertType = "R";
                    } else {
                        //工商注册号
                        entreCertNo = basic.getREGNO();
                        if (!StringUtils.isEmpty(entreCertNo)) {
                            // 营业执照
                            entreCertType = "M";
                        } else {
                            // 组织机构代码
                            entreCertNo = basic.getORGCODES();
                            if (!StringUtils.isEmpty(entreCertNo))  {
                                // 组织机构代码
                                entreCertType = "Q";
                            } else {
                                entreCertType = "";
                            }
                        }
                    }

                    // 证件类型
                    lmtSurveyReportComInfo.setCorpCertType(entreCertType);
                    // 证件号码
                    lmtSurveyReportComInfo.setCorpCertCode(entreCertNo);
                    //营业执照年限（计算规则：当前日期减去营业起始日期）
                    String opFrom = basic.getOPFROM();
                    // 营业日期
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    if(StringUtil.isNotEmpty(opFrom)){//营业起始日期不能为空
                        lmtSurveyReportComInfo.setBlicYears(String.valueOf(cmisBizXwCommonService.getBetweenMonth(opFrom, openDay)));
                    }
                    //行业
                    lmtSurveyReportComInfo.setTrade(basic.getINDUSTRYPHYNAME());
                    lmtSurveyReportComInfo.setOperAddr(basic.getDOM());
                }
            }
        } catch (YuspException e) {
            logger.info("查询工商信息失败..................");
            //throw BizException.error(null, e.getCode(), e.getMessage());
            return new ResultDto<>(null).message("未查询到工商信息").code(-1);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            logger.info("查询工商信息结束..................");
        }
        return new ResultDto<LmtSurveyReportComInfo>(lmtSurveyReportComInfo);
    }
}