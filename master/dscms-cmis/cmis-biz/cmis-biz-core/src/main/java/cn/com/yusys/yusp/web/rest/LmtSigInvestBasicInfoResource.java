/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtAppRelCusInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestBasicInfo;
import cn.com.yusys.yusp.service.LmtSigInvestBasicInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-29 11:00:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestbasicinfo")
public class LmtSigInvestBasicInfoResource {
    @Autowired
    private LmtSigInvestBasicInfoService lmtSigInvestBasicInfoService;

    @Value("${yusp.file-server.home-path}")
    private String serverPath;


    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestBasicInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestBasicInfo> list = lmtSigInvestBasicInfoService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestBasicInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtSigInvestBasicInfo>> index(QueryModel queryModel) {
        List<LmtSigInvestBasicInfo> list = lmtSigInvestBasicInfoService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestBasicInfo>>(list);
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:根据serno获取企业基本信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<LmtSigInvestBasicInfo> selectBySerno(@RequestBody Map map) {
        LmtSigInvestBasicInfo lmtSigInvestBasicInfo = lmtSigInvestBasicInfoService.selectBySernoAndCusId(map);
        return new ResultDto<LmtSigInvestBasicInfo>(lmtSigInvestBasicInfo);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestBasicInfo> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestBasicInfo lmtSigInvestBasicInfo = lmtSigInvestBasicInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestBasicInfo>(lmtSigInvestBasicInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestBasicInfo> create(@RequestBody LmtSigInvestBasicInfo lmtSigInvestBasicInfo) throws URISyntaxException {
        lmtSigInvestBasicInfoService.insert(lmtSigInvestBasicInfo);
        return new ResultDto<LmtSigInvestBasicInfo>(lmtSigInvestBasicInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestBasicInfo lmtSigInvestBasicInfo) throws URISyntaxException {
        int result = lmtSigInvestBasicInfoService.update(lmtSigInvestBasicInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestBasicInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestBasicInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * 图片
     * @param condition
     * @return
     */
    @PostMapping("/updatePicAbsoultPath")
    protected ResultDto<String> updateFilePath(@RequestBody Map condition){
        condition.put("serverPath",serverPath);
        String result = lmtSigInvestBasicInfoService.updatePicAbsoultPath(condition);
        return new ResultDto<>(result);
    }


    /**
     * @函数名称:新增底层资产分析
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insertDczcfx")
    protected ResultDto<LmtSigInvestBasicInfo> insertDczcfx(@RequestBody LmtSigInvestBasicInfo lmtSigInvestBasicInfo) throws URISyntaxException {
        lmtSigInvestBasicInfoService.insertDczcfx(lmtSigInvestBasicInfo);
        return new ResultDto<LmtSigInvestBasicInfo>(lmtSigInvestBasicInfo);
    }


    /**
     * @函数名称:保存底层资产分析
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateDczcfx")
    protected ResultDto<Integer> updateDczcfx(@RequestBody LmtSigInvestBasicInfo lmtSigInvestBasicInfo) throws URISyntaxException {
        int result = lmtSigInvestBasicInfoService.updateDczcfx(lmtSigInvestBasicInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:保存总体情况分析
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateZtqkfx")
    protected ResultDto<Integer> updateZtqkfx(@RequestBody LmtSigInvestBasicInfo lmtSigInvestBasicInfo) throws URISyntaxException {
        int result = lmtSigInvestBasicInfoService.updateZtqkfx(lmtSigInvestBasicInfo);
        return new ResultDto<Integer>(result);
    }

}
