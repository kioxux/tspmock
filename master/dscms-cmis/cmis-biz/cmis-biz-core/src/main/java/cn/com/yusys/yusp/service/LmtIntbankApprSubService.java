/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.LmtIntbankAppSub;
import cn.com.yusys.yusp.domain.LmtIntbankApprSub;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankApprSubMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankApprSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-26 08:56:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankApprSubService {

    @Autowired
    private LmtIntbankApprSubMapper lmtIntbankApprSubMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient ;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankApprSub selectByPrimaryKey(String pkId) {
        return lmtIntbankApprSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankApprSub> selectAll(QueryModel model) {
        List<LmtIntbankApprSub> records = (List<LmtIntbankApprSub>) lmtIntbankApprSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankApprSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankApprSub> list = lmtIntbankApprSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankApprSub record) {
        return lmtIntbankApprSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankApprSub record) {
        return lmtIntbankApprSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankApprSub record) {
        return lmtIntbankApprSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankApprSub record) {
        return lmtIntbankApprSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankApprSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankApprSubMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: initLmtIntbankApprSubInfo
     * @方法描述: 根据分项申请实例对象，生成同业授分项审批表信息
     * @参数与返回说明: 返回分项审批表对象
     * @算法描述: 无
     */

    public LmtIntbankApprSub initLmtIntbankApprSubInfo(LmtIntbankAppSub lmtIntbankAppSub){
        //主键流水号
        String pkValue = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PK_ID, new HashedMap());
        //审批流水号
        String approveSubSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.INTBANK_LMT_SUB_SEQ, new HashedMap());
        //初始化对象
        LmtIntbankApprSub lmtIntbankApprSub = new LmtIntbankApprSub() ;
        //拷贝数据
        BeanUtils.copyProperties(lmtIntbankAppSub, lmtIntbankApprSub);
        //主键
        lmtIntbankApprSub.setPkId(pkValue);
        //审批分项流水号
        lmtIntbankApprSub.setApproveSubSerno(approveSubSerno);
        //期限 申请表和审批表不一直 特殊处理
        lmtIntbankApprSub.setLmtTerm(lmtIntbankAppSub.getTerm());
        //最新更新日期
        lmtIntbankApprSub.setUpdDate(DateUtils.getCurrDateStr());
        //创建日期
        lmtIntbankApprSub.setCreateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        //更新日期
        lmtIntbankApprSub.setUpdateTime(DateUtils.parseDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME), DateFormatEnum.DATETIME.getValue()));
        return lmtIntbankApprSub ;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int logicDelete(String pkId) {
        //逻辑删除操作参数
        Map delMap = new HashMap();
        delMap.put("pkId",pkId);
        //获取业务标识位，通过标识位进行逻辑删除操作
        delMap.put("oprType",  CmisLmtConstants.OPR_TYPE_DELETE);
        return lmtIntbankApprSubMapper.updateByParams(delMap);
    }

    /**
     * 根据审批流水号，获取审批流水号下对应的分项信息
     * @param apprvoeSerno
     * @return
     */
    public List<LmtIntbankApprSub> selectSubListByApproveSerno(String apprvoeSerno){
        List<LmtIntbankApprSub> lmtIntbankApprSubList = lmtIntbankApprSubMapper.selectSubListByApproveSerno(apprvoeSerno) ;
        return lmtIntbankApprSubMapper.selectSubListByApproveSerno(apprvoeSerno) ;
    }
}
