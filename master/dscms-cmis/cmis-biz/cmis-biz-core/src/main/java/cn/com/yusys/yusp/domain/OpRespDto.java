package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;

public class OpRespDto extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    private String opFlag;
    private String opMsg;

    public String getOpFlag() {
        return opFlag;
    }

    public void setOpFlag(String opFlag) {
        this.opFlag = opFlag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "OpRespDto{" +
                "opFlag='" + opFlag + '\'' +
                ", opMsg='" + opMsg + '\'' +
                '}';
    }
}
