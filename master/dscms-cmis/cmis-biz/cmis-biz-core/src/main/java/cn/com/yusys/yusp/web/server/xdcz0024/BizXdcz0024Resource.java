package cn.com.yusys.yusp.web.server.xdcz0024;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdcz0024.req.Xdcz0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0024.resp.Xdcz0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:推送在线保函预约信息
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0024:推送在线保函预约信息")
@RestController
@RequestMapping("/api/bizcz4bsp")
public class BizXdcz0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdcz0024Resource.class);
    @Autowired
    private cn.com.yusys.yusp.service.server.xdcz0024.Xdcz0024Service xdcz0024Service;

    /**
     * 交易码：xdcz0024
     * 交易描述：推送在线保函预约信息
     *
     * @param xdcz0024DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送在线保函预约信息")
    @PostMapping("/xdcz0024")
    protected @ResponseBody
    ResultDto<Xdcz0024DataRespDto> xdcz0024(@Validated @RequestBody Xdcz0024DataReqDto xdcz0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, JSON.toJSONString(xdcz0024DataReqDto));
        Xdcz0024DataRespDto xdcz0024DataRespDto = new Xdcz0024DataRespDto();// 响应Dto:推送在线保函预约信息
        ResultDto<Xdcz0024DataRespDto> xdcz0024DataResultDto = new ResultDto<>();

        try {
            // 从xdcz0024DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, JSON.toJSONString(xdcz0024DataReqDto));
            xdcz0024DataRespDto = xdcz0024Service.xdcz0024(xdcz0024DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, JSON.toJSONString(xdcz0024DataRespDto));
            // 封装xdcz0024DataResultDto中正确的返回码和返回信息
            xdcz0024DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdcz0024DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, e.getMessage());
            // 封装xdcz0024DataResultDto中异常返回码和返回信息
            xdcz0024DataResultDto.setCode(e.getErrorCode());
            xdcz0024DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, e.getMessage());
            // 封装xdcz0024DataResultDto中异常返回码和返回信息
            xdcz0024DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdcz0024DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdcz0024DataRespDto到xdcz0024DataResultDto中
        xdcz0024DataResultDto.setData(xdcz0024DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, JSON.toJSONString(xdcz0024DataRespDto));
        return xdcz0024DataResultDto;
    }
}
