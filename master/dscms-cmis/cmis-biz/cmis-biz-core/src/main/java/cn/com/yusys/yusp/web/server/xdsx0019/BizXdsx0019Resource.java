package cn.com.yusys.yusp.web.server.xdsx0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0019.req.Xdsx0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0019.resp.Xdsx0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0019.Xdsx0019Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:风控发送信贷审核受托信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0019:风控发送信贷审核受托信息")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0019Resource.class);

    @Autowired
    private Xdsx0019Service xdsx0019Service;

    /**
     * 交易码：xdsx0019
     * 交易描述：风控发送信贷审核受托信息
     *
     * @param xdsx0019DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控发送信贷审核受托信息")
    @PostMapping("/xdsx0019")
    protected @ResponseBody
    ResultDto<Xdsx0019DataRespDto> xdsx0019(@Validated @RequestBody Xdsx0019DataReqDto xdsx0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019DataReqDto));
        Xdsx0019DataRespDto xdsx0019DataRespDto = new Xdsx0019DataRespDto();// 响应Dto:风控发送信贷审核受托信息
        ResultDto<Xdsx0019DataRespDto> xdsx0019DataResultDto = new ResultDto<>();

        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019DataReqDto));
            xdsx0019DataRespDto = xdsx0019Service.getXdsx0019(xdsx0019DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019DataRespDto));
            // 封装xdsx0019DataResultDto中正确的返回码和返回信息
            xdsx0019DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0019DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, e.getMessage());
            xdsx0019DataResultDto.setCode(e.getErrorCode());
            xdsx0019DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, e.getMessage());
            // 封装xdsx0019DataResultDto中异常返回码和返回信息
            xdsx0019DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0019DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0019DataRespDto到xdsx0019DataResultDto中
        xdsx0019DataResultDto.setData(xdsx0019DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019DataResultDto));
        return xdsx0019DataResultDto;
    }
}
