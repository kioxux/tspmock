/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpDemandRepayAppInfo
 * @类描述: pvp_demand_repay_app_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-27 20:43:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "pvp_demand_repay_app_info")
public class PvpDemandRepayAppInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 借据编号 **/
	@Id
	@Column(name = "BILL_NO")
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 归还正常本金 **/
	@Column(name = "GUIHZCBJ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guihzcbj;
	
	/** 归还逾期本金 **/
	@Column(name = "GUIHYQBJ", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guihyqbj;
	
	/** 归还应收应计利息 **/
	@Column(name = "GHYSYJLX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghysyjlx;
	
	/** 归还催收应计利息 **/
	@Column(name = "GHCSYJLX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghcsyjlx;
	
	/** 归还应收欠息 **/
	@Column(name = "GHYNSHQX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghynshqx;
	
	/** 归还催收欠息 **/
	@Column(name = "GHCUSHQX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghcushqx;
	
	/** 归还应收应计罚息 **/
	@Column(name = "GHYSYJFX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghysyjfx;
	
	/** 归还催收应计罚息 **/
	@Column(name = "GHCSYJFX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghcsyjfx;
	
	/** 归还应收罚息 **/
	@Column(name = "GHYNSHFX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghynshfx;
	
	/** 归还催收罚息 **/
	@Column(name = "GHCUSHFX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghcushfx;
	
	/** 归还应计复息 **/
	@Column(name = "GHYJFUXI", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghyjfuxi;
	
	/** 归还复息 **/
	@Column(name = "GHFXFUXI", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghfxfuxi;
	
	/** 归还罚金 **/
	@Column(name = "GHFAJINN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ghfajinn;
	
	/** 核心交易日期 **/
	@Column(name = "CORE_TRAN_DATE", unique = false, nullable = true, length = 10)
	private String coreTranDate;
	
	/** 核心交易流水 **/
	@Column(name = "CORE_TRAN_SERNO", unique = false, nullable = true, length = 40)
	private String coreTranSerno;
	
	/** 是否第三方还款 **/
	@Column(name = "ACCT_ID_TYPE", unique = false, nullable = true, length = 5)
	private String acctIdType;
	
	/** 还款账户名称 **/
	@Column(name = "REPAY_ACCT_NAME", unique = false, nullable = true, length = 80)
	private String repayAcctName;
	
	/** 还款账号 **/
	@Column(name = "REPAY_ACCT_NO", unique = false, nullable = true, length = 40)
	private String repayAcctNo;
	
	/** 第三方还款账号 **/
	@Column(name = "OTHER_REPAY_ACCT_NO", unique = false, nullable = true, length = 40)
	private String otherRepayAcctNo;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param guihzcbj
	 */
	public void setGuihzcbj(java.math.BigDecimal guihzcbj) {
		this.guihzcbj = guihzcbj;
	}
	
    /**
     * @return guihzcbj
     */
	public java.math.BigDecimal getGuihzcbj() {
		return this.guihzcbj;
	}
	
	/**
	 * @param guihyqbj
	 */
	public void setGuihyqbj(java.math.BigDecimal guihyqbj) {
		this.guihyqbj = guihyqbj;
	}
	
    /**
     * @return guihyqbj
     */
	public java.math.BigDecimal getGuihyqbj() {
		return this.guihyqbj;
	}
	
	/**
	 * @param ghysyjlx
	 */
	public void setGhysyjlx(java.math.BigDecimal ghysyjlx) {
		this.ghysyjlx = ghysyjlx;
	}
	
    /**
     * @return ghysyjlx
     */
	public java.math.BigDecimal getGhysyjlx() {
		return this.ghysyjlx;
	}
	
	/**
	 * @param ghcsyjlx
	 */
	public void setGhcsyjlx(java.math.BigDecimal ghcsyjlx) {
		this.ghcsyjlx = ghcsyjlx;
	}
	
    /**
     * @return ghcsyjlx
     */
	public java.math.BigDecimal getGhcsyjlx() {
		return this.ghcsyjlx;
	}
	
	/**
	 * @param ghynshqx
	 */
	public void setGhynshqx(java.math.BigDecimal ghynshqx) {
		this.ghynshqx = ghynshqx;
	}
	
    /**
     * @return ghynshqx
     */
	public java.math.BigDecimal getGhynshqx() {
		return this.ghynshqx;
	}
	
	/**
	 * @param ghcushqx
	 */
	public void setGhcushqx(java.math.BigDecimal ghcushqx) {
		this.ghcushqx = ghcushqx;
	}
	
    /**
     * @return ghcushqx
     */
	public java.math.BigDecimal getGhcushqx() {
		return this.ghcushqx;
	}
	
	/**
	 * @param ghysyjfx
	 */
	public void setGhysyjfx(java.math.BigDecimal ghysyjfx) {
		this.ghysyjfx = ghysyjfx;
	}
	
    /**
     * @return ghysyjfx
     */
	public java.math.BigDecimal getGhysyjfx() {
		return this.ghysyjfx;
	}
	
	/**
	 * @param ghcsyjfx
	 */
	public void setGhcsyjfx(java.math.BigDecimal ghcsyjfx) {
		this.ghcsyjfx = ghcsyjfx;
	}
	
    /**
     * @return ghcsyjfx
     */
	public java.math.BigDecimal getGhcsyjfx() {
		return this.ghcsyjfx;
	}
	
	/**
	 * @param ghynshfx
	 */
	public void setGhynshfx(java.math.BigDecimal ghynshfx) {
		this.ghynshfx = ghynshfx;
	}
	
    /**
     * @return ghynshfx
     */
	public java.math.BigDecimal getGhynshfx() {
		return this.ghynshfx;
	}
	
	/**
	 * @param ghcushfx
	 */
	public void setGhcushfx(java.math.BigDecimal ghcushfx) {
		this.ghcushfx = ghcushfx;
	}
	
    /**
     * @return ghcushfx
     */
	public java.math.BigDecimal getGhcushfx() {
		return this.ghcushfx;
	}
	
	/**
	 * @param ghyjfuxi
	 */
	public void setGhyjfuxi(java.math.BigDecimal ghyjfuxi) {
		this.ghyjfuxi = ghyjfuxi;
	}
	
    /**
     * @return ghyjfuxi
     */
	public java.math.BigDecimal getGhyjfuxi() {
		return this.ghyjfuxi;
	}
	
	/**
	 * @param ghfxfuxi
	 */
	public void setGhfxfuxi(java.math.BigDecimal ghfxfuxi) {
		this.ghfxfuxi = ghfxfuxi;
	}
	
    /**
     * @return ghfxfuxi
     */
	public java.math.BigDecimal getGhfxfuxi() {
		return this.ghfxfuxi;
	}
	
	/**
	 * @param ghfajinn
	 */
	public void setGhfajinn(java.math.BigDecimal ghfajinn) {
		this.ghfajinn = ghfajinn;
	}
	
    /**
     * @return ghfajinn
     */
	public java.math.BigDecimal getGhfajinn() {
		return this.ghfajinn;
	}
	
	/**
	 * @param coreTranDate
	 */
	public void setCoreTranDate(String coreTranDate) {
		this.coreTranDate = coreTranDate;
	}
	
    /**
     * @return coreTranDate
     */
	public String getCoreTranDate() {
		return this.coreTranDate;
	}
	
	/**
	 * @param coreTranSerno
	 */
	public void setCoreTranSerno(String coreTranSerno) {
		this.coreTranSerno = coreTranSerno;
	}
	
    /**
     * @return coreTranSerno
     */
	public String getCoreTranSerno() {
		return this.coreTranSerno;
	}
	
	/**
	 * @param acctIdType
	 */
	public void setAcctIdType(String acctIdType) {
		this.acctIdType = acctIdType;
	}
	
    /**
     * @return acctIdType
     */
	public String getAcctIdType() {
		return this.acctIdType;
	}
	
	/**
	 * @param repayAcctName
	 */
	public void setRepayAcctName(String repayAcctName) {
		this.repayAcctName = repayAcctName;
	}
	
    /**
     * @return repayAcctName
     */
	public String getRepayAcctName() {
		return this.repayAcctName;
	}
	
	/**
	 * @param repayAcctNo
	 */
	public void setRepayAcctNo(String repayAcctNo) {
		this.repayAcctNo = repayAcctNo;
	}
	
    /**
     * @return repayAcctNo
     */
	public String getRepayAcctNo() {
		return this.repayAcctNo;
	}
	
	/**
	 * @param otherRepayAcctNo
	 */
	public void setOtherRepayAcctNo(String otherRepayAcctNo) {
		this.otherRepayAcctNo = otherRepayAcctNo;
	}
	
    /**
     * @return otherRepayAcctNo
     */
	public String getOtherRepayAcctNo() {
		return this.otherRepayAcctNo;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}