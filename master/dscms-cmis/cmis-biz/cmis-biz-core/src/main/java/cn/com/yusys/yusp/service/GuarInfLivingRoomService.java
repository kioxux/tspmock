/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.domain.GuarInfBusinessIndustryHousr;
import cn.com.yusys.yusp.domain.GuarInfCargoPledge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarInfLivingRoom;
import cn.com.yusys.yusp.repository.mapper.GuarInfLivingRoomMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfLivingRoomService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:42:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class GuarInfLivingRoomService {

    @Autowired
    private GuarInfLivingRoomMapper guarInfLivingRoomMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public GuarInfLivingRoom selectByPrimaryKey(String serno) {
        return guarInfLivingRoomMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GuarInfLivingRoom> selectAll(QueryModel model) {
        List<GuarInfLivingRoom> records = (List<GuarInfLivingRoom>) guarInfLivingRoomMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<GuarInfLivingRoom> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarInfLivingRoom> list = guarInfLivingRoomMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(GuarInfLivingRoom record) {
        return guarInfLivingRoomMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(GuarInfLivingRoom record) {
        return guarInfLivingRoomMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(GuarInfLivingRoom record) {
        return guarInfLivingRoomMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(GuarInfLivingRoom record) {
        return guarInfLivingRoomMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return guarInfLivingRoomMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return guarInfLivingRoomMapper.deleteByIds(ids);
    }

    /**
     * 对guarInfLivingRoom单个对象保存(新增或修改),如果没有数据新增,有数据修改
     * @param guarInfLivingRoom
     * @return
     */
    public int preserveGguarInfLivingRoom(GuarInfLivingRoom guarInfLivingRoom) {
        String serno = guarInfLivingRoom.getSerno();
        //查询该对象是否存在
        GuarInfLivingRoom queryguarInfLivingRoom = guarInfLivingRoomMapper.selectByPrimaryKey(serno);
        if (null == queryguarInfLivingRoom){
            return guarInfLivingRoomMapper.insert(guarInfLivingRoom);
        }else{
            return guarInfLivingRoomMapper.updateByPrimaryKey(guarInfLivingRoom);
        }
    }

    /**
     * @函数名称:checkGuarInfoIsExist
     * @函数描述:根据流水号校验数据是否存在
     * @参数与返回说明: 存在 为 1 ,不存在为 0
     * @算法描述:
     */
    public int checkGuarInfoIsExist(String serno) {
        GuarInfLivingRoom queryguarInfLivingRoom = guarInfLivingRoomMapper.selectByPrimaryKey(serno);
        return (null==queryguarInfLivingRoom || null==queryguarInfLivingRoom.getSerno()||"".equals(queryguarInfLivingRoom.getSerno())) ? 0 : 1 ;
    }

    public GuarInfLivingRoom selectByGuarNo(String guarNo) {
        return guarInfLivingRoomMapper.selectByGuarNo(guarNo);
    }
}
