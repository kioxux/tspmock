/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CreditCardLargeAmtInfo;
import cn.com.yusys.yusp.domain.CreditCardLargeJudgInifo;
import cn.com.yusys.yusp.domain.CreditCardLargeLoanApp;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.req.D13087ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.resp.D13087RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.req.D13162ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.resp.D13162RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CreditCtrLoanCont;
import cn.com.yusys.yusp.repository.mapper.CreditCtrLoanContMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCtrLoanContService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 21:18:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CreditCtrLoanContService {
    private final Logger log = LoggerFactory.getLogger(CreditCtrLoanContService.class);

    @Autowired
    private CreditCtrLoanContMapper creditCtrLoanContMapper;
    @Autowired
    private CreditCardLargeLoanAppService CreditCardLargeLoanAppService;
    @Autowired
    private Dscms2TonglianClientService dscms2TonglianClientService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private  CmisLmtClientService  cmisLmtClientService;
    @Autowired
    private DocArchiveInfoService docArchiveInfoService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CreditCtrLoanCont selectByPrimaryKey(String contNo) {
        return creditCtrLoanContMapper.selectByPrimaryKey(contNo);
    }

    /**
     * @方法名称: selectBySerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CreditCtrLoanCont selectBySerno(String serno) {
        return creditCtrLoanContMapper.selectBySerno(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CreditCtrLoanCont> selectAll(QueryModel model) {
        List<CreditCtrLoanCont> records = (List<CreditCtrLoanCont>) creditCtrLoanContMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CreditCtrLoanCont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCtrLoanCont> list = creditCtrLoanContMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModelStatus
     * @方法描述: 条件查询 - 查询状态为未处理的
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditCtrLoanCont> selectByModelStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCtrLoanCont> list = creditCtrLoanContMapper.selectByModelStatus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByNotStatus
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CreditCtrLoanCont> selectByNotStatus(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CreditCtrLoanCont> list = creditCtrLoanContMapper.selectByNotStatus(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CreditCtrLoanCont record) {
        return creditCtrLoanContMapper.insert(record);
    }

    /**
     * @方法名称: save
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int save(CreditCtrLoanCont record) {
        return creditCtrLoanContMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CreditCtrLoanCont record) {
        return creditCtrLoanContMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CreditCtrLoanCont record) {
        return creditCtrLoanContMapper.updateBySerno(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CreditCtrLoanCont record) {
        return creditCtrLoanContMapper.updateBySernoSelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String contNo) {
        return creditCtrLoanContMapper.deleteByPrimaryKey(contNo);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return creditCtrLoanContMapper.deleteByIds(ids);
    }

    /**
     * @param serno
     * @return
     * @author zsm
     * @date 2021/5/26 9:57
     * @version 1.0.0
     * @desc 额度申请流程处理类
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void handleBusinessDataAfterEnd(String serno,String opt) {
        // TODO(“调用13162放款接口") ；
        try {
            if (StringUtils.isBlank(serno)) {
                throw new YuspException(EcbEnum.E_IQP_PARAMS_EXCEPTION.key, EcbEnum.E_IQP_PARAMS_EXCEPTION.value);
            }
            log.info("流程发起-获取大额分期合同" + serno + "大额分期合同信息");
            CreditCtrLoanCont creditCtrLoanCont = this.selectBySerno(serno);
            log.info("流程发起-更新大额分期合同" + serno + "流程审批状态为【997】-已通过");
            creditCtrLoanCont.setApproveStatus(CmisCommonConstants.WF_STATUS_997);
            creditCtrLoanCont.setContStatus("200");
            int result = this.updateSelective(creditCtrLoanCont);
            if (result < 0) {
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.value);
            }
            //调用13162放款接口
            CreditCardLargeLoanApp CreditCardLargeLoanApp = CreditCardLargeLoanAppService.selectByPrimaryKey(serno);
            D13162ReqDto d13162ReqDto = new D13162ReqDto();
            d13162ReqDto.setCardno(CreditCardLargeLoanApp.getCardNo());//卡号
            d13162ReqDto.setRgstid(CreditCardLargeLoanApp.getRegisterId());//分期申请号
            d13162ReqDto.setLoanit(creditCtrLoanCont.getLoanAmount());//分期总本金
            d13162ReqDto.setOpt(opt);//0-拒绝 1-通过
            d13162ReqDto.setSendnd("Y");//Y-代表实时放款模式审核通过后直接放款  N-代表实时放款模式审核通过后不直接放款：
            log.info("放款请求报文："+d13162ReqDto);
            ResultDto<D13162RespDto> d13162RespDtoResultDto = dscms2TonglianClientService.d13162(d13162ReqDto);
            log.info("放款返回报文："+d13162RespDtoResultDto);
            String  code1 = d13162RespDtoResultDto.getCode();
            if("0".equals(code1)){
                //分期注册状态
                // D-拒绝
                //A-通过
                //O-实时放款超时
                //S-实时放款成功
                //F-实时放款失败
                String lnrgst = d13162RespDtoResultDto.getData().getLnrgst();
                if(!"S".equals(lnrgst)){
                    throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key,d13162RespDtoResultDto.getMessage());
                }
            }else{
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key,d13162RespDtoResultDto.getMessage());
            }
            CusBaseClientDto cusBaseClientDto = iCusClientService.queryCusByCertCode(CreditCardLargeLoanApp.getCertCode());
            //2. 向额度系统发送接口：占用额度
            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(creditCtrLoanCont.getManagerBrId()));//金融机构代码
            cmisLmt0011ReqDto.setDealBizNo(creditCtrLoanCont.getContNo());//合同编号
            cmisLmt0011ReqDto.setCusId(cusBaseClientDto.getCusId());//客户编号
            cmisLmt0011ReqDto.setCusName(creditCtrLoanCont.getCusName());//客户名称
            cmisLmt0011ReqDto.setDealBizType("1");//交易业务类型
            cmisLmt0011ReqDto.setPrdId("22010201");//产品编号
            cmisLmt0011ReqDto.setBizAttr("1");//交易属性
            cmisLmt0011ReqDto.setPrdName("大额分期");//产品名称
            cmisLmt0011ReqDto.setIsLriskBiz("0");//是否低风险
            cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
            cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
            cmisLmt0011ReqDto.setDealBizAmt(creditCtrLoanCont.getLoanAmount()); //交易业务金额
            String nowDate = DateUtils.getCurrentDate(DateFormatEnum.DEFAULT);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date futureDay = DateUtils.addMonth(simpleDateFormat.parse(nowDate),Integer.parseInt(creditCtrLoanCont.getLoanTerm()));
            String  lastDay = DateUtils.formatDate(futureDay,"yyyy-MM-dd");
            cmisLmt0011ReqDto.setStartDate(nowDate);//合同起始日
            cmisLmt0011ReqDto.setEndDate(lastDay);//合同到期日
            cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态
            cmisLmt0011ReqDto.setInputId(creditCtrLoanCont.getInputId());//登记人
            cmisLmt0011ReqDto.setInputBrId(creditCtrLoanCont.getInputBrId());//登记机构
            cmisLmt0011ReqDto.setInputDate(creditCtrLoanCont.getInputDate());//登记日期
            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
            cmisLmt0011OccRelListDto.setLmtType("01");//额度类型
            cmisLmt0011OccRelListDto.setLmtSubNo(CreditCardLargeLoanApp.getSerno());//额度分项编号
            cmisLmt0011OccRelListDto.setBizTotalAmtCny(creditCtrLoanCont.getLoanAmount());//占用敞口(折人民币)
            cmisLmt0011OccRelListDto.setBizSpacAmtCny(creditCtrLoanCont.getLoanAmount());//占用敞口(折人民币)
            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);
            log.info("根据业务申请编号【{}】,前往额度系统-额度占用请求报文{}！", creditCtrLoanCont.getSerno(), JSON.toJSONString(cmisLmt0011ReqDto));
            ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
            log.info("根据业务申请编号【{}】,前往额度系统-额度占用响应报文{}！", creditCtrLoanCont.getSerno(), JSON.toJSONString(cmisLmt0011RespDto));
            String code = cmisLmt0011RespDto.getData().getErrorCode();
            if (!"0000".equals(code)) {
                log.info("根据业务申请编号【{}】,前往额度系统-额度占用失败！", creditCtrLoanCont.getSerno());
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, cmisLmt0011RespDto.getMessage());
            }
            // 生成归档任务
            log.info("开始系统生成档案归档信息");
            DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
            docArchiveClientDto.setArchiveMode("02");//01:异地分支机构归档,02:本地集中归档,03:本地分支机构归档
            docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
            docArchiveClientDto.setDocType("20");// 20信用卡
            docArchiveClientDto.setDocBizType("04");// 04大额分期
            docArchiveClientDto.setBizSerno(serno);
            docArchiveClientDto.setCusId(cusBaseClientDto.getCusId());
            docArchiveClientDto.setCusName(creditCtrLoanCont.getCusName());
            docArchiveClientDto.setCertType(creditCtrLoanCont.getCertType());
            docArchiveClientDto.setCertCode(creditCtrLoanCont.getCertCode());
            docArchiveClientDto.setManagerId(CreditCardLargeLoanApp.getInputId());
            docArchiveClientDto.setManagerBrId(CreditCardLargeLoanApp.getInputBrId());
            docArchiveClientDto.setInputId(CreditCardLargeLoanApp.getInputId());
            docArchiveClientDto.setInputBrId(CreditCardLargeLoanApp.getInputBrId());
            docArchiveClientDto.setContNo(creditCtrLoanCont.getContNo());
            docArchiveClientDto.setLoanAmt(creditCtrLoanCont.getLoanAmount());
            docArchiveClientDto.setPrdName("大额分期申请");
            int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
            if(num < 1){
                log.info("系统生成信用卡大额分期档案归档信息失败,业务流水号[{}]", serno);
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, "系统生成信用卡大额分期档案归档信息失败");
            }
            if("1".equals(opt)){
                //更新审批状态
                creditCtrLoanCont.setContStatus("200");
                creditCtrLoanCont.setApproveStatus("997");
            }else{
                //更新审批状态
                creditCtrLoanCont.setContStatus("800");
                creditCtrLoanCont.setApproveStatus("998");
            }
            result = updateSelective(creditCtrLoanCont);
            if(result != 1){
                log.info("合同号："+creditCtrLoanCont.getContNo()+",更新合同状态异常！");
                throw new YuspException(EcbEnum.E_IQP_UPDATEIQPSTATUS_EXCEPTION.key, "更新合同状态异常！");
            }
        } catch (YuspException e) {
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key,e.getMessage());
        } catch (Exception e) {
            log.error("业务申请流程发起业务处理发生异常！", e);
            throw new YuspException(EcbEnum.E_IQP_HANDLE_EXCEPTION.key,e.getMessage());
        }
    }
}
