/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AccAccp;
import cn.com.yusys.yusp.domain.AccCvrs;
import cn.com.yusys.yusp.dto.BailInfoDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccCvrsMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-27 21:54:21
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface AccCvrsMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    AccCvrs selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<AccCvrs> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(AccCvrs record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(AccCvrs record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(AccCvrs record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(AccCvrs record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);


    /**
     * @方法名称：selectForAccCvrsInfo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/17 15:11
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<AccCvrs> selectForAccCvrsInfo(@Param("cusId") String cusId);

    /**
     * @方法名称：selectByContNo
     * @方法描述：
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:56
     * @修改记录：修改时间 修改人员 修改时间
    */
    List<AccCvrs> selectByContNo(@Param("contNo") String contNo);
    /**
     * @方法名称：updateAccStatusByBillNo
     * @方法描述：根据借据号更新保函台账表中台账状态
     * @创建人：zhangming12
     * @创建时间：2021/5/21 15:59
     * @修改记录：修改时间 修改人员 修改时间
     */
    int updateAccStatusByBillNo(Map queryMap);

    /**
     * 根据业务编号查询保函台账
     * @param billno
     * @return
     */
    AccCvrs selectByBillno(String billno);

    int countAccCvrgLoanCountByContNo(String contNo);

    /**
     * @方法名称：updateAccbailAmtByBillNo
     * @方法描述：根据借据号更新保函台账表中保证金额度

     */
    int updateAccbailAmtByBillNo(AccCvrs record);


    /**
     * @方法名称：getAccCvrsInfoByContNo
     * @方法描述：根据合同编号查询借据信息

     */
    java.util.List<AccCvrs> getAccCvrsInfoByContNo(@Param("contNo") String contNo);



    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    void updateByBillNoForStatus(AccCvrs accCvrs);

    void deleteByBillNo(@Param("billNo") String billNo);

    void updateByBillNoForamt(AccCvrs accCvrs);


    /**
     * @Description: 根据客户号查询存量在线保函的签发额度之和
     * @param cusId
     * @return
     */
    BigDecimal selectSumAmtByCusId(@Param("cusId") String cusId);


    /**
     * @方法名称: selectByBillNo
     * @方法描述: 根据借据编号查询保函台账列表信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    AccCvrs selectByBillNo(@Param("billNo") String billNo);

    /**
     * 根据查询条件查询台账信息并返回
     * @param model
     * @return
     */
    List<AccCvrs> querymodelByCondition(QueryModel model);

    /**
     /**
     * @方法名称: selectAccCvrsBillNo
     * @方法描述: 根据申请流水号查询保函台账借据编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    AccCvrs selectAccCvrsBillNo(@Param("pvpSerno") String pvpSerno);

    /**
     * 根据合同编号查询用信敞口余额
     * @param contNos
     * @return
     */
    BigDecimal selectTotalSpacAmtByContNos(@Param("contNos") String contNos);

    /**
     * 风险分类审批结束更新客户未结清保函台账五十级分类结果
     * @author jijian_yx
     * @date 2021/10/25 23:36
     **/
    int updateLoanFiveAndTenClassByCusId(Map<String, String> map);

    List<BailInfoDto> getBailInfoDto(String cusId);
}