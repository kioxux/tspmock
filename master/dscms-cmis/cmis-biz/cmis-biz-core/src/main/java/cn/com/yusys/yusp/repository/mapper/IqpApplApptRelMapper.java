/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpApplApptRel;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpApplApptRelMapper
 * @类描述: #Dao类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-07 10:23:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface IqpApplApptRelMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    IqpApplApptRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<IqpApplApptRel> selectByModel(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insert(IqpApplApptRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int insertSelective(IqpApplApptRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKey(IqpApplApptRel record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateByPrimaryKeySelective(IqpApplApptRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */

    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    int deleteBySernoAndRelType(@Param("iqpSerno") String iqpSerno, @Param("relType") String s);

    IqpApplApptRel selectBySernoAndApptCode(@Param("iqpSerno") String iqpSerno,@Param("apptCode")  String apptCode);

    IqpApplApptRel selectBySernoAndRelType(@Param("iqpSerno")String iqpSerno,@Param("relType") String relType);

    void deleteByIqpSernoAndApptCode(@Param("iqpSerno") String iqpSerno,@Param("apptCode") String apptCode);
    /**
     * 通过业务申请主键进行逻辑删除，即修改opr_type
     * @param delMap
     * @return
     */
    int updateByParams(Map delMap);

    void logicDeleteBySernoAndApptCode(@Param("iqpSerno") String iqpSerno, @Param("oldWIfeApptCode") String oldWIfeApptCode);

    List<IqpApplApptRel> selectByIqpSerno(@Param("iqpSernoOld") String iqpSernoOld);

    IqpApplApptRel findWifeApptInfo(@Param("iqpSerno") String iqpSerno,@Param("apptCode") String apptCode);


    List<IqpApplApptRel> selectByParam(Map param);
}
