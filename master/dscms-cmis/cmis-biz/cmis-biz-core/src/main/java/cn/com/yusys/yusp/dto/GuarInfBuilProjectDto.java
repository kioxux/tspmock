package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarInfBuilProject
 * @类描述: guar_inf_buil_project数据实体类
 * @功能描述: 
 * @创建人: 18301
 * @创建时间: 2021-04-16 13:44:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class GuarInfBuilProjectDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 业务流水号 **/
	private String serno;
	
	/** 押品统一编号 **/
	private String guarNo;
	
	/** 在建工程关联的土地证 **/
	private String landNo;
	
	/** 工程项目名称 **/
	private String projectName;
	
	/** 建设用地规划许可证号 **/
	private String landLicence;
	
	/** 建设工程规划许可证号 **/
	private String layoutLicence;
	
	/** 施工许可证号 **/
	private String consLicence;
	
	/** 工程启动日期 **/
	private String projStartDate;
	
	/** 工程竣工预计交付日期 **/
	private String planFinishDate;
	
	/** 工程预计总造价（元） **/
	private java.math.BigDecimal projPlanAccnt;
	
	/** 工程契约合同编号 **/
	private String businessContractNo;
	
	/** 合同签订日期 **/
	private String purchaseDate;
	
	/** 合同签订金额（元） **/
	private java.math.BigDecimal purchaseAccnt;
	
	/** 建筑面积 **/
	private java.math.BigDecimal buildArea;
	
	/** 所在/注册省份 **/
	private String provinceCd;
	
	/** 所在/注册市 **/
	private String cityCd;
	
	/** 所在县（区） **/
	private String countyCd;
	
	/** 详细地址 **/
	private String detailsAddress;
	
	/** 所得税率 **/
	private java.math.BigDecimal getTax;
	
	/** 房产类别 STD_ZB_HOUSE_TYPE **/
	private String houseType;
	
	/** 建筑年份 **/
	private String archYear;
	
	/** 已缴纳土地出让金（元） **/
	private java.math.BigDecimal landSellAmt;
	
	/** 已投入工程款（元） **/
	private java.math.BigDecimal useProjectAmt;
	
	/** 已完成工程量 **/
	private java.math.BigDecimal finishProjectQnt;
	
	/** 在建工程类型 STD_ZB_PROJECT_TYPE **/
	private String builtProjectType;
	
	/** 土地使用权性质 STD_ZB_LAND_TYPE **/
	private String landUseQual;
	
	/** 土地使用权取得方式 STD_ZB_LAND_GAIN **/
	private String landUseWay;
	
	/** 土地使用权使用年限起始日期 **/
	private String landUseBeginDate;
	
	/** 土地使用年限 **/
	private java.math.BigDecimal landUseYears;
	
	/** 土地用途 STD_ZB_LANDYT **/
	private String landPurp;
	
	/** 土地说明 **/
	private String landExplain;
	
	/** 是否欠工程款 **/
	private String isDelayProjectAmt;
	
	/** 欠工程款金额 **/
	private java.math.BigDecimal delayProjectAmt;
	
	/** 街道/村镇/路名 **/
	private String street;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private java.util.Date inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private java.util.Date updDate;
	
	/** 操作类型   **/
	private String oprType;
	
	/** 责任人 **/
	private String managerId;
	
	/** 责任机构 **/
	private String managerBrId;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param guarNo
	 */
	public void setGuarNo(String guarNo) {
		this.guarNo = guarNo == null ? null : guarNo.trim();
	}
	
    /**
     * @return GuarNo
     */	
	public String getGuarNo() {
		return this.guarNo;
	}
	
	/**
	 * @param landNo
	 */
	public void setLandNo(String landNo) {
		this.landNo = landNo == null ? null : landNo.trim();
	}
	
    /**
     * @return LandNo
     */	
	public String getLandNo() {
		return this.landNo;
	}
	
	/**
	 * @param projectName
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName == null ? null : projectName.trim();
	}
	
    /**
     * @return ProjectName
     */	
	public String getProjectName() {
		return this.projectName;
	}
	
	/**
	 * @param landLicence
	 */
	public void setLandLicence(String landLicence) {
		this.landLicence = landLicence == null ? null : landLicence.trim();
	}
	
    /**
     * @return LandLicence
     */	
	public String getLandLicence() {
		return this.landLicence;
	}
	
	/**
	 * @param layoutLicence
	 */
	public void setLayoutLicence(String layoutLicence) {
		this.layoutLicence = layoutLicence == null ? null : layoutLicence.trim();
	}
	
    /**
     * @return LayoutLicence
     */	
	public String getLayoutLicence() {
		return this.layoutLicence;
	}
	
	/**
	 * @param consLicence
	 */
	public void setConsLicence(String consLicence) {
		this.consLicence = consLicence == null ? null : consLicence.trim();
	}
	
    /**
     * @return ConsLicence
     */	
	public String getConsLicence() {
		return this.consLicence;
	}
	
	/**
	 * @param projStartDate
	 */
	public void setProjStartDate(String projStartDate) {
		this.projStartDate = projStartDate == null ? null : projStartDate.trim();
	}
	
    /**
     * @return ProjStartDate
     */	
	public String getProjStartDate() {
		return this.projStartDate;
	}
	
	/**
	 * @param planFinishDate
	 */
	public void setPlanFinishDate(String planFinishDate) {
		this.planFinishDate = planFinishDate == null ? null : planFinishDate.trim();
	}
	
    /**
     * @return PlanFinishDate
     */	
	public String getPlanFinishDate() {
		return this.planFinishDate;
	}
	
	/**
	 * @param projPlanAccnt
	 */
	public void setProjPlanAccnt(java.math.BigDecimal projPlanAccnt) {
		this.projPlanAccnt = projPlanAccnt;
	}
	
    /**
     * @return ProjPlanAccnt
     */	
	public java.math.BigDecimal getProjPlanAccnt() {
		return this.projPlanAccnt;
	}
	
	/**
	 * @param businessContractNo
	 */
	public void setBusinessContractNo(String businessContractNo) {
		this.businessContractNo = businessContractNo == null ? null : businessContractNo.trim();
	}
	
    /**
     * @return BusinessContractNo
     */	
	public String getBusinessContractNo() {
		return this.businessContractNo;
	}
	
	/**
	 * @param purchaseDate
	 */
	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate == null ? null : purchaseDate.trim();
	}
	
    /**
     * @return PurchaseDate
     */	
	public String getPurchaseDate() {
		return this.purchaseDate;
	}
	
	/**
	 * @param purchaseAccnt
	 */
	public void setPurchaseAccnt(java.math.BigDecimal purchaseAccnt) {
		this.purchaseAccnt = purchaseAccnt;
	}
	
    /**
     * @return PurchaseAccnt
     */	
	public java.math.BigDecimal getPurchaseAccnt() {
		return this.purchaseAccnt;
	}
	
	/**
	 * @param buildArea
	 */
	public void setBuildArea(java.math.BigDecimal buildArea) {
		this.buildArea = buildArea;
	}
	
    /**
     * @return BuildArea
     */	
	public java.math.BigDecimal getBuildArea() {
		return this.buildArea;
	}
	
	/**
	 * @param provinceCd
	 */
	public void setProvinceCd(String provinceCd) {
		this.provinceCd = provinceCd == null ? null : provinceCd.trim();
	}
	
    /**
     * @return ProvinceCd
     */	
	public String getProvinceCd() {
		return this.provinceCd;
	}
	
	/**
	 * @param cityCd
	 */
	public void setCityCd(String cityCd) {
		this.cityCd = cityCd == null ? null : cityCd.trim();
	}
	
    /**
     * @return CityCd
     */	
	public String getCityCd() {
		return this.cityCd;
	}
	
	/**
	 * @param countyCd
	 */
	public void setCountyCd(String countyCd) {
		this.countyCd = countyCd == null ? null : countyCd.trim();
	}
	
    /**
     * @return CountyCd
     */	
	public String getCountyCd() {
		return this.countyCd;
	}
	
	/**
	 * @param detailsAddress
	 */
	public void setDetailsAddress(String detailsAddress) {
		this.detailsAddress = detailsAddress == null ? null : detailsAddress.trim();
	}
	
    /**
     * @return DetailsAddress
     */	
	public String getDetailsAddress() {
		return this.detailsAddress;
	}
	
	/**
	 * @param getTax
	 */
	public void setGetTax(java.math.BigDecimal getTax) {
		this.getTax = getTax;
	}
	
    /**
     * @return GetTax
     */	
	public java.math.BigDecimal getGetTax() {
		return this.getTax;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType == null ? null : houseType.trim();
	}
	
    /**
     * @return HouseType
     */	
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param archYear
	 */
	public void setArchYear(String archYear) {
		this.archYear = archYear == null ? null : archYear.trim();
	}
	
    /**
     * @return ArchYear
     */	
	public String getArchYear() {
		return this.archYear;
	}
	
	/**
	 * @param landSellAmt
	 */
	public void setLandSellAmt(java.math.BigDecimal landSellAmt) {
		this.landSellAmt = landSellAmt;
	}
	
    /**
     * @return LandSellAmt
     */	
	public java.math.BigDecimal getLandSellAmt() {
		return this.landSellAmt;
	}
	
	/**
	 * @param useProjectAmt
	 */
	public void setUseProjectAmt(java.math.BigDecimal useProjectAmt) {
		this.useProjectAmt = useProjectAmt;
	}
	
    /**
     * @return UseProjectAmt
     */	
	public java.math.BigDecimal getUseProjectAmt() {
		return this.useProjectAmt;
	}
	
	/**
	 * @param finishProjectQnt
	 */
	public void setFinishProjectQnt(java.math.BigDecimal finishProjectQnt) {
		this.finishProjectQnt = finishProjectQnt;
	}
	
    /**
     * @return FinishProjectQnt
     */	
	public java.math.BigDecimal getFinishProjectQnt() {
		return this.finishProjectQnt;
	}
	
	/**
	 * @param builtProjectType
	 */
	public void setBuiltProjectType(String builtProjectType) {
		this.builtProjectType = builtProjectType == null ? null : builtProjectType.trim();
	}
	
    /**
     * @return BuiltProjectType
     */	
	public String getBuiltProjectType() {
		return this.builtProjectType;
	}
	
	/**
	 * @param landUseQual
	 */
	public void setLandUseQual(String landUseQual) {
		this.landUseQual = landUseQual == null ? null : landUseQual.trim();
	}
	
    /**
     * @return LandUseQual
     */	
	public String getLandUseQual() {
		return this.landUseQual;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay == null ? null : landUseWay.trim();
	}
	
    /**
     * @return LandUseWay
     */	
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param landUseBeginDate
	 */
	public void setLandUseBeginDate(String landUseBeginDate) {
		this.landUseBeginDate = landUseBeginDate == null ? null : landUseBeginDate.trim();
	}
	
    /**
     * @return LandUseBeginDate
     */	
	public String getLandUseBeginDate() {
		return this.landUseBeginDate;
	}
	
	/**
	 * @param landUseYears
	 */
	public void setLandUseYears(java.math.BigDecimal landUseYears) {
		this.landUseYears = landUseYears;
	}
	
    /**
     * @return LandUseYears
     */	
	public java.math.BigDecimal getLandUseYears() {
		return this.landUseYears;
	}
	
	/**
	 * @param landPurp
	 */
	public void setLandPurp(String landPurp) {
		this.landPurp = landPurp == null ? null : landPurp.trim();
	}
	
    /**
     * @return LandPurp
     */	
	public String getLandPurp() {
		return this.landPurp;
	}
	
	/**
	 * @param landExplain
	 */
	public void setLandExplain(String landExplain) {
		this.landExplain = landExplain == null ? null : landExplain.trim();
	}
	
    /**
     * @return LandExplain
     */	
	public String getLandExplain() {
		return this.landExplain;
	}
	
	/**
	 * @param isDelayProjectAmt
	 */
	public void setIsDelayProjectAmt(String isDelayProjectAmt) {
		this.isDelayProjectAmt = isDelayProjectAmt == null ? null : isDelayProjectAmt.trim();
	}
	
    /**
     * @return IsDelayProjectAmt
     */	
	public String getIsDelayProjectAmt() {
		return this.isDelayProjectAmt;
	}
	
	/**
	 * @param delayProjectAmt
	 */
	public void setDelayProjectAmt(java.math.BigDecimal delayProjectAmt) {
		this.delayProjectAmt = delayProjectAmt;
	}
	
    /**
     * @return DelayProjectAmt
     */	
	public java.math.BigDecimal getDelayProjectAmt() {
		return this.delayProjectAmt;
	}
	
	/**
	 * @param street
	 */
	public void setStreet(String street) {
		this.street = street == null ? null : street.trim();
	}
	
    /**
     * @return Street
     */	
	public String getStreet() {
		return this.street;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(java.util.Date inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return InputDate
     */	
	public java.util.Date getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(java.util.Date updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return UpdDate
     */	
	public java.util.Date getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}


}