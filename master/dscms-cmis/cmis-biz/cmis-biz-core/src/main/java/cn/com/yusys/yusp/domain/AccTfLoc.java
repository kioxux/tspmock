/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccTfLoc
 * @类描述: acc_tf_loc数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 14:04:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_tf_loc")
public class AccTfLoc extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 出账流水号 **/
	@Column(name = "PVP_SERNO", unique = false, nullable = true, length = 40)
	private String pvpSerno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户外文名称 **/
	@Column(name = "CUS_NAME_EN", unique = false, nullable = true, length = 80)
	private String cusNameEn;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;

	/** 产品类型属性 **/
	@Column(name = "PRD_TYPE_PROP", unique = false, nullable = true, length = 5)
	private String prdTypeProp;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 信用证编号 **/
	@Column(name = "CREDIT_NO", unique = false, nullable = true, length = 5)
	private String creditNo;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 汇率 **/
	@Column(name = "EXCHG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal exchgRate;
	
	/** 开证金额 **/
	@Column(name = "OCER_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ocerAmt;
	
	/** 折算人民币金额 **/
	@Column(name = "CVT_CNY_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cvtCnyAmt;
	
	/** 敞口余额 **/
	@Column(name = "SPAC_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal spacBal;

	/** 原始敞口金额 **/
	@Column(name = "ORIGI_OPEN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiOpenAmt;

	/** 保证金币种 **/
	@Column(name = "BAIL_CUR_TYPE", unique = false, nullable = true, length = 5)
	private String bailCurType;

	/** 保证金金额 **/
	@Column(name = "BAIL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAmt;

	/** 信用证余额 **/
	@Column(name = "CREDIT_BAL", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal creditBal;

	/** 折算人民币敞口 **/
	@Column(name = "CVT_CNY_SPAC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cvtCnySpac;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 溢装比例 **/
	@Column(name = "FLOODACT_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal floodactPerc;
	
	/** 起始日期 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 到期日期 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 手续费率（‰） **/
	@Column(name = "CHRG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgRate;
	
	/** 手续费金额 **/
	@Column(name = "CHRG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal chrgAmt;
	
	/** 信用证期限类型 **/
	@Column(name = "CREDIT_TERM_TYPE", unique = false, nullable = true, length = 5)
	private String creditTermType;
	
	/** 远期天数 **/
	@Column(name = "FAST_DAY", unique = false, nullable = true, length = 20)
	private String fastDay;
	
	/** 信用证付款期限 **/
	@Column(name = "CREDIT_PAY_TERM", unique = false, nullable = true, length = 20)
	private String creditPayTerm;
	
	/** 贸易合同号 **/
	@Column(name = "TCONT_NO", unique = false, nullable = true, length = 20)
	private String tcontNo;
	
	/** 货物名称 **/
	@Column(name = "GOODS_NAME", unique = false, nullable = true, length = 20)
	private String goodsName;
	
	/** 受益人名称 **/
	@Column(name = "BENEFICIAR_NAME", unique = false, nullable = true, length = 20)
	private String beneficiarName;
	
	/** 是否使用授信额度 **/
	@Column(name = "IS_USE_LMT_AMT", unique = false, nullable = true, length = 5)
	private String isUseLmtAmt;
	
	/** 授信额度编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 财务机构编号 **/
	@Column(name = "FINA_BR_ID", unique = false, nullable = true, length = 40)
	private String finaBrId;
	
	/** 财务机构名称 **/
	@Column(name = "FINA_BR_ID_NAME", unique = false, nullable = true, length = 80)
	private String finaBrIdName;
	
	/** 十级分类 **/
	@Column(name = "TEN_CLASS", unique = false, nullable = true, length = 5)
	private String tenClass;
	
	/** 五级分类 **/
	@Column(name = "FIVE_CLASS", unique = false, nullable = true, length = 5)
	private String fiveClass;
	
	/** 分类日期 **/
	@Column(name = "CLASS_DATE", unique = false, nullable = true, length = 20)
	private String classDate;
	
	/** 台账状态 **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;
	
	/** 操作类型 STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusNameEn
	 */
	public void setCusNameEn(String cusNameEn) {
		this.cusNameEn = cusNameEn;
	}
	
    /**
     * @return cusNameEn
     */
	public String getCusNameEn() {
		return this.cusNameEn;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}

	/**
	 * @param prdTypeProp
	 */
	public void setPrdTypeProp(String prdTypeProp) {
		this.prdTypeProp = prdTypeProp;
	}
	/**
	 * @return prdTypeProp
	 */
	public String getPrdTypeProp() {
		return this.prdTypeProp;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param creditNo
	 */
	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}
	
    /**
     * @return creditNo
     */
	public String getCreditNo() {
		return this.creditNo;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param exchgRate
	 */
	public void setExchgRate(java.math.BigDecimal exchgRate) {
		this.exchgRate = exchgRate;
	}
	
    /**
     * @return exchgRate
     */
	public java.math.BigDecimal getExchgRate() {
		return this.exchgRate;
	}
	
	/**
	 * @param ocerAmt
	 */
	public void setOcerAmt(java.math.BigDecimal ocerAmt) {
		this.ocerAmt = ocerAmt;
	}
	
    /**
     * @return ocerAmt
     */
	public java.math.BigDecimal getOcerAmt() {
		return this.ocerAmt;
	}
	
	/**
	 * @param cvtCnyAmt
	 */
	public void setCvtCnyAmt(java.math.BigDecimal cvtCnyAmt) {
		this.cvtCnyAmt = cvtCnyAmt;
	}
	
    /**
     * @return cvtCnyAmt
     */
	public java.math.BigDecimal getCvtCnyAmt() {
		return this.cvtCnyAmt;
	}
	
	/**
	 * @param spacBal
	 */
	public void setSpacBal(java.math.BigDecimal spacBal) {
		this.spacBal = spacBal;
	}


	/**
	 * @param origiOpenAmt
	 */
	public void setOrigiOpenAmt(java.math.BigDecimal origiOpenAmt) {
		this.origiOpenAmt = origiOpenAmt;
	}

	/**
	 * @return origiOpenAmt
	 */
	public java.math.BigDecimal getOrigiOpenAmt() {
		return this.origiOpenAmt;
	}

	/**
	 * @param bailCurType
	 */
	public void setBailCurType(String bailCurType) {
		this.bailCurType = bailCurType;
	}

	/**
	 * @return bailCurType
	 */
	public String getBailCurType() {
		return this.bailCurType;
	}

	/**
	 * @param bailAmt
	 */
	public void setBailAmt(java.math.BigDecimal bailAmt) {
		this.bailAmt = bailAmt;
	}

	/**
	 * @return bailAmt
	 */
	public java.math.BigDecimal getBailAmt() {
		return this.bailAmt;
	}

	/**
	 * @param creditBal
	 */
	public void setCreditBal(java.math.BigDecimal creditBal) {
		this.creditBal = creditBal;
	}

	/**
	 * @return creditBal
	 */
	public java.math.BigDecimal getCreditBal() {
		return this.creditBal;
	}

    /**
     * @return spacBal
     */
	public java.math.BigDecimal getSpacBal() {
		return this.spacBal;
	}
	
	/**
	 * @param cvtCnySpac
	 */
	public void setCvtCnySpac(java.math.BigDecimal cvtCnySpac) {
		this.cvtCnySpac = cvtCnySpac;
	}
	
    /**
     * @return cvtCnySpac
     */
	public java.math.BigDecimal getCvtCnySpac() {
		return this.cvtCnySpac;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param floodactPerc
	 */
	public void setFloodactPerc(java.math.BigDecimal floodactPerc) {
		this.floodactPerc = floodactPerc;
	}
	
    /**
     * @return floodactPerc
     */
	public java.math.BigDecimal getFloodactPerc() {
		return this.floodactPerc;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param chrgRate
	 */
	public void setChrgRate(java.math.BigDecimal chrgRate) {
		this.chrgRate = chrgRate;
	}
	
    /**
     * @return chrgRate
     */
	public java.math.BigDecimal getChrgRate() {
		return this.chrgRate;
	}
	
	/**
	 * @param chrgAmt
	 */
	public void setChrgAmt(java.math.BigDecimal chrgAmt) {
		this.chrgAmt = chrgAmt;
	}
	
    /**
     * @return chrgAmt
     */
	public java.math.BigDecimal getChrgAmt() {
		return this.chrgAmt;
	}
	
	/**
	 * @param creditTermType
	 */
	public void setCreditTermType(String creditTermType) {
		this.creditTermType = creditTermType;
	}
	
    /**
     * @return creditTermType
     */
	public String getCreditTermType() {
		return this.creditTermType;
	}
	
	/**
	 * @param fastDay
	 */
	public void setFastDay(String fastDay) {
		this.fastDay = fastDay;
	}
	
    /**
     * @return fastDay
     */
	public String getFastDay() {
		return this.fastDay;
	}
	
	/**
	 * @param creditPayTerm
	 */
	public void setCreditPayTerm(String creditPayTerm) {
		this.creditPayTerm = creditPayTerm;
	}
	
    /**
     * @return creditPayTerm
     */
	public String getCreditPayTerm() {
		return this.creditPayTerm;
	}
	
	/**
	 * @param tcontNo
	 */
	public void setTcontNo(String tcontNo) {
		this.tcontNo = tcontNo;
	}
	
    /**
     * @return tcontNo
     */
	public String getTcontNo() {
		return this.tcontNo;
	}
	
	/**
	 * @param goodsName
	 */
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	
    /**
     * @return goodsName
     */
	public String getGoodsName() {
		return this.goodsName;
	}
	
	/**
	 * @param beneficiarName
	 */
	public void setBeneficiarName(String beneficiarName) {
		this.beneficiarName = beneficiarName;
	}
	
    /**
     * @return beneficiarName
     */
	public String getBeneficiarName() {
		return this.beneficiarName;
	}
	
	/**
	 * @param isUseLmtAmt
	 */
	public void setIsUseLmtAmt(String isUseLmtAmt) {
		this.isUseLmtAmt = isUseLmtAmt;
	}
	
    /**
     * @return isUseLmtAmt
     */
	public String getIsUseLmtAmt() {
		return this.isUseLmtAmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId;
	}
	
    /**
     * @return finaBrId
     */
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param finaBrIdName
	 */
	public void setFinaBrIdName(String finaBrIdName) {
		this.finaBrIdName = finaBrIdName;
	}
	
    /**
     * @return finaBrIdName
     */
	public String getFinaBrIdName() {
		return this.finaBrIdName;
	}
	
	/**
	 * @param tenClass
	 */
	public void setTenClass(String tenClass) {
		this.tenClass = tenClass;
	}
	
    /**
     * @return tenClass
     */
	public String getTenClass() {
		return this.tenClass;
	}
	
	/**
	 * @param fiveClass
	 */
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	
    /**
     * @return fiveClass
     */
	public String getFiveClass() {
		return this.fiveClass;
	}
	
	/**
	 * @param classDate
	 */
	public void setClassDate(String classDate) {
		this.classDate = classDate;
	}
	
    /**
     * @return classDate
     */
	public String getClassDate() {
		return this.classDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}



}