/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtIntbankReplyChg;
import org.apache.poi.hpsf.Decimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtIntbankReplyChgSub;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankReplyChgSubMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankReplyChgSubService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-20 21:18:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankReplyChgSubService {

    @Autowired
    private LmtIntbankReplyChgSubMapper lmtIntbankReplyChgSubMapper;
	@Autowired
    private LmtIntbankReplyChgService lmtIntbankReplyChgService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankReplyChgSub selectByPrimaryKey(String pkId) {
        return lmtIntbankReplyChgSubMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankReplyChgSub> selectAll(QueryModel model) {
        List<LmtIntbankReplyChgSub> records = (List<LmtIntbankReplyChgSub>) lmtIntbankReplyChgSubMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankReplyChgSub> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankReplyChgSub> list = lmtIntbankReplyChgSubMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankReplyChgSub record) {
        return lmtIntbankReplyChgSubMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankReplyChgSub record) {
        return lmtIntbankReplyChgSubMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankReplyChgSub record) {
        return lmtIntbankReplyChgSubMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankReplyChgSub record) {
        return lmtIntbankReplyChgSubMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankReplyChgSubMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankReplyChgSubMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号查询
     * @param serno
     * @return
     */
    public List<LmtIntbankReplyChgSub> selectBySerno(String serno){
        return lmtIntbankReplyChgSubMapper.selectBySerno(serno);
    }

    /**
     * 更改分项批复申请并动态更改授信总额度
     * @param lmtIntbankReplyChgSub
     * @return
     */
    public int updateReplyChgSub(LmtIntbankReplyChgSub lmtIntbankReplyChgSub){
        int count=0;
        lmtIntbankReplyChgSubMapper.updateByPrimaryKeySelective(lmtIntbankReplyChgSub);
        count++;
        Map<String,Object> map = lmtIntbankReplyChgSubMapper.selectLmtAmtBySerno(lmtIntbankReplyChgSub.getSerno());
        String lmtAmt = map.get("lmtamt").toString();
        LmtIntbankReplyChg lmtIntbankReplyChg = new LmtIntbankReplyChg();
        lmtIntbankReplyChg.setSerno(lmtIntbankReplyChgSub.getSerno());
        lmtIntbankReplyChg.setLmtAmt(new BigDecimal(lmtAmt));
        lmtIntbankReplyChgService.updateLmtAmt(lmtIntbankReplyChg);
        count++;
        return count;
    }
}
