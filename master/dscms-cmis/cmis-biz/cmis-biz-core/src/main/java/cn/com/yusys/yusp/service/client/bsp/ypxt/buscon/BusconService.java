package cn.com.yusys.yusp.service.client.bsp.ypxt.buscon;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.resp.BusconRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author 王玉坤
 * @version 1.0.0
 * @date 2021/6/28 14:21
 * @desc 信贷业务与押品关联关系信息同步
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class BusconService {
    private static final Logger logger = LoggerFactory.getLogger(BusconService.class);

    // 1）注入：BSP封装调用押品系统的接口
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    /**
     * @param busconReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.resp.busconRespDto
     * @author 王玉坤
     * @date 2021/6/28 23:39
     * @version 1.0.0
     * @desc 信贷业务与押品关联关系信息同步
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public BusconRespDto buscon(BusconReqDto busconReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value, JSON.toJSONString(busconReqDto));
        ResultDto<BusconRespDto> buscon2ResultDto = dscms2YpxtClientService.buscon(busconReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value, JSON.toJSONString(buscon2ResultDto));

        String busconCode = Optional.ofNullable(buscon2ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String busconMeesage = Optional.ofNullable(buscon2ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        BusconRespDto busconRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, buscon2ResultDto.getCode())) {
            //  获取相关的值并解析
            busconRespDto = buscon2ResultDto.getData();
        } else {//未查询到相关信息
            buscon2ResultDto.setCode(EpbEnum.EPB099999.key);
            //  抛出错误异常
            //throw new YuspException(busconCode, busconMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value);
        return busconRespDto;
    }
}
