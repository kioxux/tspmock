package cn.com.yusys.yusp.service.server.xdcz0024;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.OnlineCvrgCusInfo;
import cn.com.yusys.yusp.dto.server.xdcz0024.req.Xdcz0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0024.resp.Xdcz0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.OnlineCvrgCusInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:推送在线保函预约信息
 *
 * @author xull
 * @version 1.0
 */
@Service
public class Xdcz0024Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0024Service.class);
    @Autowired
    private OnlineCvrgCusInfoService onlineCvrgCusInfoService;

    /**
     * 推送在线保函预约信息
     *
     * @param xdcz0024DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdcz0024DataRespDto xdcz0024(Xdcz0024DataReqDto xdcz0024DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value);
        Xdcz0024DataRespDto xdcz0024DataRespDto = new Xdcz0024DataRespDto();
        String serno = xdcz0024DataReqDto.getSerno();//任务编号
        String creditcode = xdcz0024DataReqDto.getCreditcode();//统一社会信用代码
        String cusname = xdcz0024DataReqDto.getCusname();//客户名称
        String contacts = xdcz0024DataReqDto.getContacts();//联系人
        String phone = xdcz0024DataReqDto.getPhone();//联系电话
        String input_time = xdcz0024DataReqDto.getInput_time();//登记时间
        String main_br_id = xdcz0024DataReqDto.getMain_br_id();//管户机构
        // 响应Data：推送在线保函预约信息
        String opFlag = null;// 操作成功标志位
        String opMsg = null;// 描述信息
        try {
            OnlineCvrgCusInfo record = new OnlineCvrgCusInfo();
            record.setSerno(serno);//任务编号
            record.setCreditCode(creditcode);//统一社会信用代码
            record.setCusName(cusname);//客户名称
            record.setContacts(contacts);//联系人
            record.setPhone(phone);//联系电话
            record.setInputTime(input_time);//登记时间
            record.setManagerBrId(main_br_id);//管户机构
            onlineCvrgCusInfoService.insert(record);
            xdcz0024DataRespDto.setOpFlag(DscmsBizCzEnum.OP_FLAG_S.key);// 操作成功标志位
            xdcz0024DataRespDto.setOpMsg(DscmsBizCzEnum.OP_FLAG_S.value);// 描述信息
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value);
        return xdcz0024DataRespDto;
    }
}
