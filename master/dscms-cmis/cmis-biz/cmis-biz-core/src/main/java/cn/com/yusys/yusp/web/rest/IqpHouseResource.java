/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.dto.IqpHouseDto;
import cn.com.yusys.yusp.service.IqpHouseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpHouseResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: monchi
 * @创建时间: 2020-12-12 10:31:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "房屋信息")
@RequestMapping("/api/iqphouse")
public class IqpHouseResource {
    @Autowired
    private IqpHouseService iqpHouseService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @ApiOperation("全表查询")
    @GetMapping("/query/all")
    protected ResultDto<List<IqpHouse>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpHouse> list = iqpHouseService.selectAll(queryModel);
        return new ResultDto<List<IqpHouse>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpHouse>> index(QueryModel queryModel) {
        List<IqpHouse> list = iqpHouseService.selectByModel(queryModel);
        return new ResultDto<List<IqpHouse>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpHouse> show(@PathVariable("pkId") String pkId) {
        IqpHouse iqpHouse = iqpHouseService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpHouse>(iqpHouse);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpHouse> create(@RequestBody IqpHouse iqpHouse) throws URISyntaxException {
        iqpHouseService.insert(iqpHouse);
        return new ResultDto<IqpHouse>(iqpHouse);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpHouse iqpHouse) throws URISyntaxException {
        int result = iqpHouseService.updateSelective(iqpHouse);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpHouseService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpHouseService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * 业务申请获取商贷月还款额
     *
     * @param map
     * @return
     */
    @ApiOperation("业务申请获取商贷月还款额")
    @PostMapping("/getMonthRepayAmt")
    public ResultDto<IqpHouseDto> getMonthRepayAmt(@RequestBody Map map) {
        IqpHouseDto iqpHouseDto = iqpHouseService.getMonthRepayAmt(map);
        return new ResultDto<>(iqpHouseDto);
    }

    /**
     *功能:根据业务流水号查询房贷信息
     * @param iqpHouse
     * @return
     */
    @ApiOperation("根据业务流水号查询房贷信息")
    @PostMapping("/querybyiqpserno")
    public ResultDto<IqpHouse> querybyiqpserno(@RequestBody IqpHouse iqpHouse) {
        return new ResultDto<IqpHouse>(iqpHouseService.selectByIqpSernos(iqpHouse.getIqpSerno()));
    }
}
