/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.LmtApp;
import cn.com.yusys.yusp.domain.LmtRedicussDetail;
import cn.com.yusys.yusp.domain.LmtRefineDetail;
import cn.com.yusys.yusp.repository.mapper.LmtRefineDetailMapper;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtRefineDetailService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 11:23:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtRefineDetailService {

    @Resource
    private LmtRefineDetailMapper lmtRefineDetailMapper;

    @Autowired
    private LmtAppService lmtAppService;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtRefineDetail selectByPrimaryKey(String pkId) {
        return lmtRefineDetailMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtRefineDetail> selectAll(QueryModel model) {
        List<LmtRefineDetail> records = (List<LmtRefineDetail>) lmtRefineDetailMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtRefineDetail> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtRefineDetail> list = lmtRefineDetailMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtRefineDetail record) {
        return lmtRefineDetailMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtRefineDetail record) {
        return lmtRefineDetailMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtRefineDetail record) {
        return lmtRefineDetailMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtRefineDetail record) {
        return lmtRefineDetailMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtRefineDetailMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtRefineDetailMapper.deleteByIds(ids);
    }

    /**
     * 根据申请流水号查询数据
     * @param lmtSerno
     * @return
     */
    public LmtRefineDetail selectByLmtSerno(String lmtSerno) {
        return lmtRefineDetailMapper.selectByLmtSerno(lmtSerno);
    }

    /**
     * 根據申請流水號查詢分項
     * @param lmtSerno
     * @return
     */
    public List<Map<String,String>> queryDetailByLmtSerno(String lmtSerno) {
        List<Map<String,String>> list = new ArrayList<>();
        List<LmtRefineDetail> lmtRefineDetails = lmtRefineDetailMapper.queryDetailByLmtSerno(lmtSerno);
        if(!lmtRefineDetails.isEmpty() && lmtRefineDetails.size() != 0){
            for(LmtRefineDetail lmtRefineDetail : lmtRefineDetails){
                HashMap<String , String> map = new HashMap();
                // 处理成员客户
                // 获取成员客户号
                LmtApp lmtApp = lmtAppService.selectBySerno(lmtRefineDetail.getLmtSerno());
                if(lmtApp != null ){
                    map.put("cusId",lmtApp.getCusId());
                    map.put("cusName",lmtApp.getCusName());
                }
                map.put("pkId",lmtRefineDetail.getPkId());
                map.put("lmtSerno",lmtRefineDetail.getLmtSerno());
                map.put("origiLmtSurvey",lmtRefineDetail.getOrigiLmtSurvey());
                map.put("lmtRefineContent",lmtRefineDetail.getLmtRefineContent());
                map.put("lmtRefineResn",lmtRefineDetail.getLmtRefineResn());
                map.put("inputId",lmtRefineDetail.getInputId());
                map.put("inputBrId",lmtRefineDetail.getInputBrId());
                map.put("inputDate",lmtRefineDetail.getInputDate());
                list.add(map);
            }
        }
        return list;
    }
}
