package cn.com.yusys.yusp.service.server.xdxw0041;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.PvpLoanAppXqywhbxdResult;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList;
import cn.com.yusys.yusp.dto.server.xdxw0041.req.Xdxw0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0041.resp.Xdxw0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppXqywhbxdResultMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.ICusClientService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.UUID;

@Service
public class Xdxw0041Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0041Service.class);

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private SenddxService senddxService;

    @Resource
    private ICusClientService icusClientService;

    @Autowired
    private PvpLoanAppXqywhbxdResultMapper pvpLoanAppXqywhbxdResultMapper;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0041DataRespDto xdxw0041(Xdxw0041DataReqDto xdxw0041DataReqDto) throws Exception{
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041DataReqDto));
        Xdxw0041DataRespDto xdxw0041DataRespDto = new Xdxw0041DataRespDto();
        String cusMainId = "";
        String cusName = "";
        String msg = "";
        String actorname = "";
        String telnum = "";
        try {
            // 5指无还本续贷
            if ("5".equals(xdxw0041DataReqDto.getApp_type())) {
                // 区分个人还是企业，取对应客户号
                if ("1".equals(xdxw0041DataReqDto.getCrd_type())) {//个人
                    cusMainId = xdxw0041DataReqDto.getCust_id();
                } else if ("2".equals(xdxw0041DataReqDto.getCrd_type())) {//企业
                    cusMainId = xdxw0041DataReqDto.getEnt_cust_id();
                }
                //根据借款人证件号certNo 获取借款人客户号 调用cus服务
                CusBaseClientDto cusBaseClientDto = icusClientService.queryCus(cusMainId);
                cusName = cusBaseClientDto.getCusName();
                if (StringUtils.isEmpty(xdxw0041DataReqDto.getApproval_result())) {
                    msg = "审批结果不能为空";
                    xdxw0041DataRespDto.setOpFlag("F");
                    xdxw0041DataRespDto.setOpMsg(msg);
                } else if (StringUtils.isEmpty(cusMainId)) {
                    msg = "客户号不能为空";
                    xdxw0041DataRespDto.setOpFlag("F");
                    xdxw0041DataRespDto.setOpMsg(msg);
                } else if (StringUtils.isEmpty(xdxw0041DataReqDto.getBill_no())) {
                    msg = "借据号不能为空";
                    xdxw0041DataRespDto.setOpFlag("F");
                    xdxw0041DataRespDto.setOpMsg(msg);
                } else {
                    // 如果审批结果为0，则审批结果记录到“小企业无还本续贷申请”表
                    if ("0".equals(xdxw0041DataReqDto.getApproval_result())) {
                        // 记录到“小企业无还本续贷申请”表
                        // 审批通过，修改该笔出账的审批状态为待发起
                        PvpLoanApp pvpLoanApp = pvpLoanAppService.queryPvpLoanAppDataByBillNo(xdxw0041DataReqDto.getBill_no());
                        pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
                        pvpLoanAppMapper.updateApproveStatusByBillNo(pvpLoanApp);
                        PvpLoanAppXqywhbxdResult pvpLoanAppXqywhbxdResult = new PvpLoanAppXqywhbxdResult();
                        pvpLoanAppXqywhbxdResult.setPkId(UUID.randomUUID().toString());
                        pvpLoanAppXqywhbxdResult.setBillNo(xdxw0041DataReqDto.getBill_no());
                        pvpLoanAppXqywhbxdResult.setContNo(xdxw0041DataReqDto.getCrd_cont_no());
                        pvpLoanAppXqywhbxdResult.setCusId(xdxw0041DataReqDto.getCust_id());
                        pvpLoanAppXqywhbxdResult.setApproveStatus(xdxw0041DataReqDto.getApproval_result());
                        pvpLoanAppXqywhbxdResult.setRule01(xdxw0041DataReqDto.getRule01());
                        pvpLoanAppXqywhbxdResult.setRule02(xdxw0041DataReqDto.getRule02());
                        pvpLoanAppXqywhbxdResult.setRule03(xdxw0041DataReqDto.getRule03());
                        pvpLoanAppXqywhbxdResult.setRule04(xdxw0041DataReqDto.getRule04());
                        pvpLoanAppXqywhbxdResult.setRule05(xdxw0041DataReqDto.getRule05());
                        pvpLoanAppXqywhbxdResult.setRule06(xdxw0041DataReqDto.getRule06());
                        pvpLoanAppXqywhbxdResult.setRule07(xdxw0041DataReqDto.getRule07());

                        logger.info("**********XDXW0041**审批结果为0:插入pvpLoanAppXqywhbxdResult开始,插入参数为:{}", JSON.toJSONString(pvpLoanAppXqywhbxdResult));
                        pvpLoanAppXqywhbxdResultMapper.insert(pvpLoanAppXqywhbxdResult);
                        // 通知短信
                        // 获取客户经理号
//                        AccLoan accLoan = accLoanMapper.queryAccLoanDataByBillNo(xdxw0041DataReqDto.getBill_no());
//                        ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(accLoan.getManagerId());
//                        actorname = manager.getData().getUserName();
//                        telnum = manager.getData().getUserMobilephone();
//                        if (!StringUtils.isBlank(telnum)) {
//                            String sendMessage = "客户经理" + actorname + "，您发起的小企业无还本续贷申请[客户名为" + cusName + "],合同号为[" + xdxw0041DataReqDto.getCrd_cont_no() + "]已通过系统风控审批，请继续完成续贷流程!";
//                            logger.info("发送的短信内容：" + sendMessage);
//                            SenddxReqDto senddxReqDto = new SenddxReqDto();
//                            senddxReqDto.setInfopt("dx");
//                            SenddxReqList senddxReqList = new SenddxReqList();
//                            senddxReqList.setMobile(telnum);
//                            senddxReqList.setSmstxt(sendMessage);
//                            ArrayList<SenddxReqList> list = new ArrayList<>();
//                            list.add(senddxReqList);
//                            senddxReqDto.setSenddxReqList(list);
//                            try {
//                                senddxService.senddx(senddxReqDto);
//                            } catch (Exception e) {
//                                throw new Exception("发送短信失败！");
//                            }
//                        }
                    } else {
                        // 审批失败，修改该笔出账的审批状态为拒绝
                        PvpLoanApp pvpLoanApp = pvpLoanAppService.queryPvpLoanAppDataByBillNo(xdxw0041DataReqDto.getBill_no());
                        pvpLoanApp.setApproveStatus(CmisCommonConstants.WF_STATUS_998);
                        pvpLoanAppMapper.updateApproveStatusByBillNo(pvpLoanApp);
                        // 记录到“小企业无还本续贷申请”表
                        PvpLoanAppXqywhbxdResult pvpLoanAppXqywhbxdResult = new PvpLoanAppXqywhbxdResult();
                        pvpLoanAppXqywhbxdResult.setPkId(UUID.randomUUID().toString());
                        pvpLoanAppXqywhbxdResult.setBillNo(xdxw0041DataReqDto.getBill_no());
                        pvpLoanAppXqywhbxdResult.setContNo(xdxw0041DataReqDto.getCrd_cont_no());
                        pvpLoanAppXqywhbxdResult.setCusId(xdxw0041DataReqDto.getCust_id());
                        pvpLoanAppXqywhbxdResult.setApproveStatus(xdxw0041DataReqDto.getApproval_result());
                        pvpLoanAppXqywhbxdResult.setRule01(xdxw0041DataReqDto.getRule01());
                        pvpLoanAppXqywhbxdResult.setRule02(xdxw0041DataReqDto.getRule02());
                        pvpLoanAppXqywhbxdResult.setRule03(xdxw0041DataReqDto.getRule03());
                        pvpLoanAppXqywhbxdResult.setRule04(xdxw0041DataReqDto.getRule04());
                        pvpLoanAppXqywhbxdResult.setRule05(xdxw0041DataReqDto.getRule05());
                        pvpLoanAppXqywhbxdResult.setRule06(xdxw0041DataReqDto.getRule06());
                        pvpLoanAppXqywhbxdResult.setRule07(xdxw0041DataReqDto.getRule07());
                        logger.info("**********XDXW0041*审批失败:插入pvpLoanAppXqywhbxdResult开始,插入参数为:{}", JSON.toJSONString(pvpLoanAppXqywhbxdResult));
                        pvpLoanAppXqywhbxdResultMapper.insert(pvpLoanAppXqywhbxdResult);
                        // 通知短信
//                        AccLoan accLoan = accLoanMapper.queryAccLoanDataByBillNo(xdxw0041DataReqDto.getBill_no());
//                        ResultDto<AdminSmUserDto> manager = adminSmUserService.getByLoginCode(accLoan.getManagerId());
//                        actorname = manager.getData().getUserName();
//                        telnum = manager.getData().getUserMobilephone();
//                        if (!StringUtils.isBlank(telnum)) {
//                            String sendMessage = "客户经理" + actorname + "，您发起的小企业无还本续贷申请[客户名为" + cusName + "],合同号为[" + xdxw0041DataReqDto.getCrd_cont_no() + "]未通过系统风控审批，审批结果可见出账申请中的模型结果指标!";
//                            logger.info("发送的短信内容：" + sendMessage);
//                            SenddxReqDto senddxReqDto = new SenddxReqDto();
//                            senddxReqDto.setInfopt("dx");
//                            SenddxReqList senddxReqList = new SenddxReqList();
//                            senddxReqList.setMobile(telnum);
//                            senddxReqList.setSmstxt(sendMessage);
//                            ArrayList<SenddxReqList> list = new ArrayList<>();
//                            list.add(senddxReqList);
//                            senddxReqDto.setSenddxReqList(list);
//                            try {
//                                senddxService.senddx(senddxReqDto);
//                            } catch (Exception e) {
//                                throw new Exception("发送短信失败！");
//                            }
//                        }
                    }
                    xdxw0041DataRespDto.setOpFlag("S");
                    xdxw0041DataRespDto.setOpMsg("交易成功");
                }
            } else {
                xdxw0041DataRespDto.setOpFlag("F");
                xdxw0041DataRespDto.setOpMsg("续贷主体类型不正确");
            }
        }catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            xdxw0041DataRespDto.setOpFlag("F");
            xdxw0041DataRespDto.setOpMsg("交易失败");
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, e.getMessage());
            throw new Exception(e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041DataReqDto));
        return xdxw0041DataRespDto;
    }
}
