/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpRiskChkCfrm;
import cn.com.yusys.yusp.repository.mapper.IqpRiskChkCfrmMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpRiskChkCfrmService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-23 15:04:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpRiskChkCfrmService {

    @Autowired
    private IqpRiskChkCfrmMapper iqpRiskChkCfrmMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpRiskChkCfrm selectByPrimaryKey(String serno) {
        return iqpRiskChkCfrmMapper.selectByPrimaryKey(serno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpRiskChkCfrm> selectAll(QueryModel model) {
        List<IqpRiskChkCfrm> records = (List<IqpRiskChkCfrm>) iqpRiskChkCfrmMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpRiskChkCfrm> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpRiskChkCfrm> list = iqpRiskChkCfrmMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpRiskChkCfrm record) {
        return iqpRiskChkCfrmMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpRiskChkCfrm record) {
        return iqpRiskChkCfrmMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpRiskChkCfrm record) {
        return iqpRiskChkCfrmMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpRiskChkCfrm record) {
        return iqpRiskChkCfrmMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return iqpRiskChkCfrmMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpRiskChkCfrmMapper.deleteByIds(ids);
    }

    public List<IqpRiskChkCfrm> selectByIqpSerno(String iqpSernoOld) {
        return iqpRiskChkCfrmMapper.selectByIqpSerno(iqpSernoOld);
    }
}
