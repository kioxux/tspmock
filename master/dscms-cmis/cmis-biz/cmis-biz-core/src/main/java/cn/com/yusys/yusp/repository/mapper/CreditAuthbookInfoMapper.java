/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.CreditReportQryLst;
import cn.com.yusys.yusp.vo.CreditAuthbookInfoVo;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import cn.com.yusys.yusp.domain.CreditAuthbookInfo;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditAuthbookInfoMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: tangxun
 * @创建时间: 2021-04-26 15:05:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CreditAuthbookInfoMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CreditAuthbookInfo selectByPrimaryKey(@Param("caiSerno") String caiSerno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CreditAuthbookInfo> selectByModel(QueryModel model);

    List<CreditAuthbookInfoVo> selectByModelVo(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CreditAuthbookInfo record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CreditAuthbookInfo record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CreditAuthbookInfo record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CreditAuthbookInfo record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("caiSerno") String caiSerno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: selectByCertCode
     * @方法描述: 通过证件号查询授权书信息
     * @参数与返回说明:
     */
    List<CreditAuthbookInfo> selectByCertCode(@Param("certCodes") String certCodes);

    List<CreditAuthbookInfo> queryByCertCode(QueryModel model);

    CreditAuthbookInfo selectByAuthBookNo(CreditAuthbookInfo record);

    List<CreditAuthbookInfo> queryAuthBookList(QueryModel model);

    CreditAuthbookInfo queryAuthBookinfo(CreditReportQryLst record);

    List<CreditAuthbookInfo> queryAuthBookListByModel(QueryModel model);
}