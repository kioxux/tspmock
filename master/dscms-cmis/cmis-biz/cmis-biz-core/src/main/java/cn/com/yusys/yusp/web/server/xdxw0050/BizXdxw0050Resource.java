package cn.com.yusys.yusp.web.server.xdxw0050;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0050.req.Xdxw0050DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0050.resp.Xdxw0050DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0050.Xdxw0050Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:资产负债表信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0050:资产负债表信息查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0050Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0050Resource.class);
    @Resource
    private Xdxw0050Service xdxw0050Service;

    /**
     * 交易码：xdxw0050
     * 交易描述：资产负债表信息查询
     *
     * @param xdxw0050DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产负债表信息查询")
    @PostMapping("/xdxw0050")
    protected @ResponseBody
    ResultDto<Xdxw0050DataRespDto> xdxw0050(@Validated @RequestBody Xdxw0050DataReqDto xdxw0050DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0050DataReqDto));
        Xdxw0050DataRespDto xdxw0050DataRespDto = new Xdxw0050DataRespDto();// 响应Dto:资产负债表信息查询
        ResultDto<Xdxw0050DataRespDto> xdxw0050DataResultDto = new ResultDto<>();
        String queryType = xdxw0050DataReqDto.getQueryType();//查询类型
        String indgtSerno = xdxw0050DataReqDto.getIndgtSerno();//客户调查表编号
        String applySerno = xdxw0050DataReqDto.getApplySerno();//业务唯一编号
        String subject = xdxw0050DataReqDto.getSubject();//科目
        try {
            // 从xdxw0050DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0050DataReqDto));
            xdxw0050DataRespDto = xdxw0050Service.xdxw0050(xdxw0050DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0050DataRespDto));
            // 封装xdxw0050DataResultDto中正确的返回码和返回信息
            xdxw0050DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0050DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            // 封装xdxw0050DataResultDto中异常返回码和返回信息
            xdxw0050DataResultDto.setCode(e.getErrorCode());
            xdxw0050DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, e.getMessage());
            // 封装xdxw0050DataResultDto中异常返回码和返回信息
            xdxw0050DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0050DataResultDto.setCode(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0050DataRespDto到xdxw0050DataResultDto中
        xdxw0050DataResultDto.setData(xdxw0050DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0050DataResultDto));
        return xdxw0050DataResultDto;
    }
}
