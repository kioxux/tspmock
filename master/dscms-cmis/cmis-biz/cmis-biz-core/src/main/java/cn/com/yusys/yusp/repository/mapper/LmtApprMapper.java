/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import cn.com.yusys.yusp.domain.LmtAppr;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtApprMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-09 10:54:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtApprMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtAppr selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtAppr> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtAppr record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtAppr record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtAppr record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtAppr record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    List<LmtAppr> selectLmtApprByParams(HashMap<String, String> paramsMap);

    /**
     * @方法名称: queryGrpLmtApprByGrpSerno
     * @方法描述: 查询集团成员的审批数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<LmtAppr> queryGrpLmtApprByGrpSerno(String grpSerno);

    /**
     * @方法名称：queryLmtApproveDataByParams
     * @方法描述：根据条件查询授信审批
     * @参数与返回说明：
     * @算法描述：
     * @创建人：guobt
     * @创建时间：2021-05-13 上午 8:57
     * @修改记录：修改时间 修改人员  修改原因
     */
    List<LmtAppr> queryLmtApproveDataByParams(HashMap<String, String> queryMap);

    /**
     * 通过流水号授信申请信息
     *
     * @param
     * @return
     */
    LmtAppr selectByApproveSerno(String approveSerno);

    /**
     * 根据申请流水号获取审批数据
     * @param serno
     * @return
     */
    LmtAppr queryInfoBySerno(String serno);

    /**
     * @方法名称: updateSxkdAmtBySerno
     * @方法描述: 根据流水号更新省心快贷金额
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateSxkdAmtBySerno(LmtAppr record);

   
}