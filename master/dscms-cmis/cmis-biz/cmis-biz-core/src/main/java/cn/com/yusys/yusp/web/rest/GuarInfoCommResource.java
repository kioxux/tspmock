package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.service.GuarInfoCommService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @项目名称: cmis-guar-core模块
 * @类名称: GuarInfoCommResource
 * @类描述: #资源类(押品信息公共功能)
 * @功能描述:
 * @创建人: yangx
 * @创建时间: 2020-12-04 16:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/guarinfocomm")
public class GuarInfoCommResource {
    private static final Logger log = LoggerFactory.getLogger(GuarInfoCommResource.class);

    @Autowired
    private GuarInfoCommService guarInfoCommService;

    /**
     * 验证押品信息是否存在
     * @param tableName 表名
     * @param guarNo 押品统一编号
     * @return
     */
    /*@PostMapping("/checkGuarIsExist/{tableName}/{guarNo}")
    protected ResultDto<Boolean> checkGuarIsExist(@PathVariable String tableName,@PathVariable String guarNo){*/
    @PostMapping("/checkGuarIsExist")
    protected ResultDto<Boolean> checkGuarIsExist(@RequestParam(value = "tableName") String tableName,@RequestParam(value = "guarNo") String guarNo){
        log.info("验证押品信息是否存在，表名【{}】，押品统一编号【{}】", tableName,guarNo);
        return new ResultDto<Boolean>(guarInfoCommService.checkGuarIsExist(tableName,guarNo));
    }

    /**
     * 存在性查询新增保存时 根据存在性查询输入的条件新增押品详细信息
     * @param map
     * @return
     */
    @PostMapping("/saveGuarDetail")
    protected ResultDto<Map> saveGuarDetail(@RequestBody Map map) {
        log.info("**************************保存押品详细信息**************************，请求信息【{}】", map);
        return new ResultDto<Map>(guarInfoCommService.saveGuarDetail(map));
    }
}
