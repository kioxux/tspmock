/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCoopAppResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: liqichao
 * @创建时间: 2021-01-12 19:42:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtcoopapp")
public class LmtCoopAppResource {
    @Autowired
    private LmtCoopAppService lmtCoopAppService;

    @Autowired
    private LmtBuildingProService lmtBuildingProService;

    @Autowired
    private LmtCarProService lmtCarProService;

    @Autowired
    private LmtMachineEquipService lmtMachineEquipService;

    @Autowired
    private LmtCarSaleBrhRelService lmtCarSaleBrhRelService;

    @Autowired
    private LmtMachineSchedRelService lmtMachineSchedRelService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    private static final Logger log = LoggerFactory.getLogger(LmtCoopAppResource.class);

    /**
     * 全表查询.
     *
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtCoopApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtCoopApp> list = lmtCoopAppService.selectAll(queryModel);
        return new ResultDto<List<LmtCoopApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtCoopApp>> index(QueryModel queryModel) {
        List<LmtCoopApp> list = lmtCoopAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtCoopApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtCoopApp> show(@PathVariable("serno") String serno) {
        LmtCoopApp lmtCoopApp = lmtCoopAppService.selectByPrimaryKey(serno);
        return new ResultDto<LmtCoopApp>(lmtCoopApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtCoopApp> create(@RequestBody LmtCoopApp lmtCoopApp) throws URISyntaxException {
        lmtCoopAppService.insert(lmtCoopApp);
        return new ResultDto<LmtCoopApp>(lmtCoopApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtCoopApp lmtCoopApp) throws URISyntaxException {
        int result = lmtCoopAppService.update(lmtCoopApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtCoopAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtCoopAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: deleteLmtCoopApp
     * @函数描述: 合作方额度申请数据逻辑删除！
     * @参数: param
     * */
    @PostMapping("/deleteLmtCoopApp")
    protected ResultDto<Integer> deleteLmtCoopApp(@RequestBody Map param) {
        log.info("合作方额度申请数据逻辑删除！{}，入参信息为："+param.toString());
        int result = lmtCoopAppService.deleteLmtCoopAppBySerno(param);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: selectCountByOldSerno
     * @函数描述: 根据原业务流水号查询复议申请
     * @参数: serno 否决申请记录的业务流水号--复议申请原业务流水号
     * */
    @PostMapping("/selectcount/{serno}")
    protected ResultDto<LmtCoopApp> selectCountByOldSerno (@PathVariable("serno") String serno) {
        log.info("合作方额度申请流水号serno："+serno+"复议记录查询！");
        LmtCoopApp result = lmtCoopAppService.selectCountByOldSerno(serno);
        return new ResultDto<LmtCoopApp>(result);
    }

    /**
     * @函数名称: selectCountByOldSerno
     * @函数描述: 根据原业务流水号查询复议申请
     * @参数: serno 否决申请记录的业务流水号--复议申请原业务流水号
     * */
    @PostMapping("/saveReconsData")
    protected ResultDto<Integer> saveReconsData (@RequestBody LmtCoopApp lmtCoopApp) {
        log.info("合作方额度申请流水号serno："+lmtCoopApp.getOldSerno()+"复议申请数据保存！");
        int result = 0;
        //获取当前复议申请的合作方客户类型
        String coop_type = lmtCoopApp.getCoopType();
        //拼接序列号
        HashMap<String,String> param = new HashMap<>();
        //全局序列号业务类型 10-PC端
        param.put("biz",SeqConstant.YPSEQ_BIZ_10);
        if (CmisBizConstants.COOP_TYPE_001.equals(coop_type)) {//若合作方客户类型为 001-房地产开发商 则将原否决申请的楼盘项目信息关联复议申请记录并保存
            //根据原业务流水号查询出原来的楼盘项目信息
            LmtBuildingPro lmtBuildingPro = lmtBuildingProService.selectByPrimaryKey(lmtCoopApp.getOldSerno());
            //将原楼盘项目信息的主键替换为新生成的业务流水号，得到新申请关联的楼盘项目信息
            lmtBuildingPro.setSerno(lmtCoopApp.getSerno());
            //将新的楼盘项目信息保存
            result += lmtBuildingProService.insertSelective(lmtBuildingPro);
        } else if (CmisBizConstants.COOP_TYPE_002.equals(coop_type)) {//若合作方客户类型为 002-汽车经销商 则将原否决申请的汽车项目信息关联复议申请记录并保存
            //根据原业务流水号查询出原来的汽车项目信息
            LmtCarPro lmtCarPro = lmtCarProService.selectByPrimaryKey(lmtCoopApp.getOldSerno());
            //将原汽车项目信息的主键替换为新生成的业务流水号，得到新申请关联的汽车项目信息
            lmtCarPro.setSerno(lmtCoopApp.getSerno());
            //将新的汽车项目信息保存
            result += lmtCarProService.insertSelective(lmtCarPro);
            //销售网点信息新增
            List<LmtCarSaleBrhRel> lmtCarSaleBrhRelList = lmtCarSaleBrhRelService.selectBySerno(lmtCoopApp.getOldSerno());
            for (LmtCarSaleBrhRel lmtCarSaleBrhRel : lmtCarSaleBrhRelList) {
                lmtCarSaleBrhRel.setPkId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, param));
                lmtCarSaleBrhRel.setSerno(lmtCoopApp.getSerno());
                result += lmtCarSaleBrhRelService.insertSelective(lmtCarSaleBrhRel);
            }
        } else if (CmisBizConstants.COOP_TYPE_003.equals(coop_type)) {//若合作方客户类型为 003-购置机械设备担保 则将原否决申请的机械设备信息关联复议申请记录并保存
            //根据原业务流水号查询出原来的机械设备信息
            LmtMachineEquip lmtMachineEquip = lmtMachineEquipService.selectByPrimaryKey(lmtCoopApp.getOldSerno());
            //将原机械设备信息的主键替换为新生成的业务流水号，得到新申请关联的机械设备信息
            lmtMachineEquip.setSerno(lmtCoopApp.getSerno());
            //将新的机械设备信息保存
            result += lmtMachineEquipService.insertSelective(lmtMachineEquip);
            //机械设备与拟按揭设备关系新增
            List<LmtMachineSchedRel> lmtMachineSchedRelList = lmtMachineSchedRelService.selectBySerno(lmtCoopApp.getOldSerno());
            for (LmtMachineSchedRel lmtMachineSchedRel : lmtMachineSchedRelList) {
                lmtMachineSchedRel.setPkId(sequenceTemplateClient.getSequenceTemplate(SeqConstant.YPSEQ, param));
                lmtMachineSchedRel.setSerno(lmtCoopApp.getSerno());
                result += lmtMachineSchedRelService.insertSelective(lmtMachineSchedRel);
            }
        }else{
            result = 1;
        }
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称: checkProjectDetails
     * @函数描述: 根据业务申请流水号查询关联项目信息
     * @参数: serno 业务流水号
     * */
    @PostMapping("/checkProjectDetails/{serno}")
    protected ResultDto<Integer> checkProjectDetails (@PathVariable("serno") String serno) {
        ResultDto<Integer> resultDto = new ResultDto();
        int result = 0;
        result = lmtCoopAppService.checkProjectDetails(serno);
        resultDto.setData(result);
        return resultDto;
    }
}
