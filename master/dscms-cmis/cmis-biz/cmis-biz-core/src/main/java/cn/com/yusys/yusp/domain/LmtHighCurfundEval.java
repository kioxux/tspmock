/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz模块
 * @类名称: LmtHighCurfundEval
 * @类描述: lmt_high_curfund_eval数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-01 19:14:24
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_high_curfund_eval")
public class LmtHighCurfundEval extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 上年度销售收入 **/
	@Column(name = "LT_YEAR_SALE_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ltYearSaleIncome;
	
	/** 上年度销售利润率 **/
	@Column(name = "LT_YEAR_SALE_PROFIT_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ltYearSaleProfitRate;
	
	/** 预计销售收入年增长率 **/
	@Column(name = "FORE_SALE_INCOME_GROW_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal foreSaleIncomeGrowRate;
	
	/** 上年度预收款周转天数 **/
	@Column(name = "LT_YEAR_PPM_TURNO_DAY", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal ltYearPpmTurnoDay;
	
	/** 上年度存货周转天数 **/
	@Column(name = "LT_YEAR_IVT_TURNOV_DAY", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal ltYearIvtTurnovDay;
	
	/** 上年度应收款周转天数 **/
	@Column(name = "LT_YEAR_RCV_TURNOV_DAY", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal ltYearRcvTurnovDay;
	
	/** 上年度应付账款周转天数 **/
	@Column(name = "LT_YEAR_ACP_TURNOV_DAY", unique = false, nullable = true, length = 10)
	private BigDecimal ltYearAcpTurnovDay;
	
	/** 上年度预付款周转天数 **/
	@Column(name = "LT_YEAR_ADVANCE_TURNOV_DAY", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal ltYearAdvanceTurnovDay;
	
	/** 上年度营运资金周转天数 **/
	@Column(name = "LT_YEAR_OPRFUNDS_TURNOV_DAY", unique = false, nullable = true, length = 10)
	private java.math.BigDecimal ltYearOprfundsTurnovDay;
	
	/** 借款人自有资金 **/
	@Column(name = "BORROWER_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal borrowerCap;
	
	/** 现有流动资金贷款 **/
	@Column(name = "CURFUND_LOAN", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curfundLoan;
	
	/** 其他渠道提供的营运资金 **/
	@Column(name = "OTHER_CHNL_PROVID_OPR_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal otherChnlProvidOprCap;
	
	/** 营运资金量 **/
	@Column(name = "OPR_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oprCap;
	
	/** 流动资金贷款额度 **/
	@Column(name = "CURFUND_LOAN_LMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal curfundLoanLmt;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = false, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = false, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = false, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = false, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = false, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private Date updateTime;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param ltYearSaleIncome
	 */
	public void setLtYearSaleIncome(java.math.BigDecimal ltYearSaleIncome) {
		this.ltYearSaleIncome = ltYearSaleIncome;
	}
	
    /**
     * @return ltYearSaleIncome
     */
	public java.math.BigDecimal getLtYearSaleIncome() {
		return this.ltYearSaleIncome;
	}
	
	/**
	 * @param ltYearSaleProfitRate
	 */
	public void setLtYearSaleProfitRate(java.math.BigDecimal ltYearSaleProfitRate) {
		this.ltYearSaleProfitRate = ltYearSaleProfitRate;
	}
	
    /**
     * @return ltYearSaleProfitRate
     */
	public java.math.BigDecimal getLtYearSaleProfitRate() {
		return this.ltYearSaleProfitRate;
	}
	
	/**
	 * @param foreSaleIncomeGrowRate
	 */
	public void setForeSaleIncomeGrowRate(java.math.BigDecimal foreSaleIncomeGrowRate) {
		this.foreSaleIncomeGrowRate = foreSaleIncomeGrowRate;
	}
	
    /**
     * @return foreSaleIncomeGrowRate
     */
	public java.math.BigDecimal getForeSaleIncomeGrowRate() {
		return this.foreSaleIncomeGrowRate;
	}
	
	/**
	 * @param ltYearPpmTurnoDay
	 */
	public void setLtYearPpmTurnoDay(java.math.BigDecimal ltYearPpmTurnoDay) {
		this.ltYearPpmTurnoDay = ltYearPpmTurnoDay;
	}
	
    /**
     * @return ltYearPpmTurnoDay
     */
	public java.math.BigDecimal getLtYearPpmTurnoDay() {
		return this.ltYearPpmTurnoDay;
	}
	
	/**
	 * @param ltYearIvtTurnovDay
	 */
	public void setLtYearIvtTurnovDay(java.math.BigDecimal ltYearIvtTurnovDay) {
		this.ltYearIvtTurnovDay = ltYearIvtTurnovDay;
	}
	
    /**
     * @return ltYearIvtTurnovDay
     */
	public java.math.BigDecimal getLtYearIvtTurnovDay() {
		return this.ltYearIvtTurnovDay;
	}
	
	/**
	 * @param ltYearRcvTurnovDay
	 */
	public void setLtYearRcvTurnovDay(java.math.BigDecimal ltYearRcvTurnovDay) {
		this.ltYearRcvTurnovDay = ltYearRcvTurnovDay;
	}
	
    /**
     * @return ltYearRcvTurnovDay
     */
	public java.math.BigDecimal getLtYearRcvTurnovDay() {
		return this.ltYearRcvTurnovDay;
	}
	
	/**
	 * @param ltYearAcpTurnovDay
	 */
	public void setLtYearAcpTurnovDay(java.math.BigDecimal ltYearAcpTurnovDay) {
		this.ltYearAcpTurnovDay = ltYearAcpTurnovDay;
	}
	
    /**
     * @return ltYearAcpTurnovDay
     */
	public java.math.BigDecimal getLtYearAcpTurnovDay() {
		return this.ltYearAcpTurnovDay;
	}
	
	/**
	 * @param ltYearAdvanceTurnovDay
	 */
	public void setLtYearAdvanceTurnovDay(java.math.BigDecimal ltYearAdvanceTurnovDay) {
		this.ltYearAdvanceTurnovDay = ltYearAdvanceTurnovDay;
	}
	
    /**
     * @return ltYearAdvanceTurnovDay
     */
	public java.math.BigDecimal getLtYearAdvanceTurnovDay() {
		return this.ltYearAdvanceTurnovDay;
	}
	
	/**
	 * @param ltYearOprfundsTurnovDay
	 */
	public void setLtYearOprfundsTurnovDay(java.math.BigDecimal ltYearOprfundsTurnovDay) {
		this.ltYearOprfundsTurnovDay = ltYearOprfundsTurnovDay;
	}
	
    /**
     * @return ltYearOprfundsTurnovDay
     */
	public java.math.BigDecimal getLtYearOprfundsTurnovDay() {
		return this.ltYearOprfundsTurnovDay;
	}
	
	/**
	 * @param borrowerCap
	 */
	public void setBorrowerCap(java.math.BigDecimal borrowerCap) {
		this.borrowerCap = borrowerCap;
	}
	
    /**
     * @return borrowerCap
     */
	public java.math.BigDecimal getBorrowerCap() {
		return this.borrowerCap;
	}
	
	/**
	 * @param curfundLoan
	 */
	public void setCurfundLoan(java.math.BigDecimal curfundLoan) {
		this.curfundLoan = curfundLoan;
	}
	
    /**
     * @return curfundLoan
     */
	public java.math.BigDecimal getCurfundLoan() {
		return this.curfundLoan;
	}
	
	/**
	 * @param otherChnlProvidOprCap
	 */
	public void setOtherChnlProvidOprCap(java.math.BigDecimal otherChnlProvidOprCap) {
		this.otherChnlProvidOprCap = otherChnlProvidOprCap;
	}
	
    /**
     * @return otherChnlProvidOprCap
     */
	public java.math.BigDecimal getOtherChnlProvidOprCap() {
		return this.otherChnlProvidOprCap;
	}
	
	/**
	 * @param oprCap
	 */
	public void setOprCap(java.math.BigDecimal oprCap) {
		this.oprCap = oprCap;
	}
	
    /**
     * @return oprCap
     */
	public java.math.BigDecimal getOprCap() {
		return this.oprCap;
	}
	
	/**
	 * @param curfundLoanLmt
	 */
	public void setCurfundLoanLmt(java.math.BigDecimal curfundLoanLmt) {
		this.curfundLoanLmt = curfundLoanLmt;
	}
	
    /**
     * @return curfundLoanLmt
     */
	public java.math.BigDecimal getCurfundLoanLmt() {
		return this.curfundLoanLmt;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}