/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CoopPlanAppConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.repository.mapper.LmtIntbankReplyChgMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtIntbankReplyChgService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-20 21:18:04
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtIntbankReplyChgService extends BizInvestCommonService{
    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtIntbankReplyChgService.class);

    @Autowired
    private LmtIntbankReplyChgMapper lmtIntbankReplyChgMapper;
	@Autowired
    private LmtIntbankAccService lmtIntbankAccService;
	@Autowired
    private LmtIntbankAccSubService lmtIntbankAccSubService;
	@Autowired
    private LmtIntbankReplyChgSubService lmtIntbankReplyChgSubService;
    // 流水号生成服务
    @Autowired
    private SequenceTemplateService sequenceTemplateClient;

    @Autowired
    private LmtIntbankApprSubService lmtIntbankApprSubService;

    @Autowired
    private LmtIntbankApprService lmtIntbankApprService;

    @Autowired
    private LmtIntbankReplyService lmtIntbankReplyService;

    @Autowired
    private LmtIntbankReplySubService lmtIntbankReplySubService;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate ;

    @Autowired
    private MessageSendService messageSendService;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    MessageCommonService sendMessage;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtIntbankReplyChg selectByPrimaryKey(String pkId) {
        return lmtIntbankReplyChgMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtIntbankReplyChg> selectAll(QueryModel model) {
        List<LmtIntbankReplyChg> records = (List<LmtIntbankReplyChg>) lmtIntbankReplyChgMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtIntbankReplyChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtIntbankReplyChg> list = lmtIntbankReplyChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtIntbankReplyChg record) {
        return lmtIntbankReplyChgMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtIntbankReplyChg record) {
        return lmtIntbankReplyChgMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtIntbankReplyChg record) {
        return lmtIntbankReplyChgMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtIntbankReplyChg record) {
        return lmtIntbankReplyChgMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtIntbankReplyChgMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtIntbankReplyChgMapper.deleteByIds(ids);
    }

    /**
     * 根据条件查询
     * @param queryModel
     * @return
     */
    public List<LmtIntbankReplyChg> selectByCondition(QueryModel queryModel){
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LmtIntbankReplyChg> list = lmtIntbankReplyChgMapper.selectByCondition(queryModel);
        PageHelper.clearPage();
        return list;
    }


    /**
     * 根据条件查询
     * @param queryModel
     * @return
     */
    public List<LmtIntbankReplyChg> selectHisByCondition(QueryModel queryModel){
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LmtIntbankReplyChg> list=  lmtIntbankReplyChgMapper.selectHisByCondition(queryModel);
        PageHelper.clearPage();
        return list;
    }
    /**
     * 新增批复变更申请表
     */
    public  Map<String, Object> insertReplyChg(Map<String,Object> map){
        String replySerno = map.get("replySerno").toString();

        //存在在途的授信业务，不允许发起
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("replySerno",replySerno);
        BizInvestCommonService.checkParamsIsNull("replySerno",replySerno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        List<LmtIntbankReplyChg> lmtIntbankReplyChgs = selectByModel(queryModel);
        if (!CollectionUtils.isEmpty(lmtIntbankReplyChgs)){
            boolean present = lmtIntbankReplyChgs.stream().filter(a ->
                    !CmisBizConstants.APPLY_STATE_QUIT.equals(a.getApproveStatus())
                            && !CmisBizConstants.APPLY_STATE_REFUSE.equals(a.getApproveStatus()))
                    .findFirst().isPresent();
            if (present){
                throw BizException.error(null,EclEnum.LMT_SIG_INVESTAPP_EORROR000027.key,EclEnum.LMT_SIG_INVESTAPP_EORROR000027.value);
            }
        }

        //查询台账信息
        LmtIntbankAcc lmtIntbankAcc= lmtIntbankAccService.selectByReplyNo(replySerno);
        LmtIntbankReplyChg lmtIntbankReplyChg = BeanUtils.beanCopy(lmtIntbankAcc,LmtIntbankReplyChg.class);
        lmtIntbankReplyChg.setPkId(UUID.randomUUID().toString());
        String serno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_CHG_SERNO, new HashMap<>());
        lmtIntbankReplyChg.setSerno(serno);
        lmtIntbankReplyChg.setReplySerno(replySerno);
        lmtIntbankReplyChg.setApproveStatus(CoopPlanAppConstant.APPR_STATUS_START);
        lmtIntbankReplyChg.setReplyInureDate(lmtIntbankAcc.getStartDate());
        lmtIntbankReplyChg.setReplyStatus(lmtIntbankAcc.getAccStatus());
        lmtIntbankReplyChg.setReplyResult(lmtIntbankAcc.getApprResult());


        Date date = new Date();
        lmtIntbankReplyChg.setCreateTime(date);
        //插入批复变更表
        lmtIntbankReplyChgMapper.insert(lmtIntbankReplyChg);
        Map<String, Object> result =new HashMap<>();
        result.put("serno",serno);
        result.put("accNo",lmtIntbankAcc.getAccNo());
        //查询台账分项信息
        List<LmtIntbankAccSub> list= lmtIntbankAccSubService.selectByAccNo(lmtIntbankAcc.getAccNo());
        if(list!=null && list.size()>0){
            for (int i=0;i<list.size();i++){
                LmtIntbankReplyChgSub lmtIntbankReplyChgSub = BeanUtils.beanCopy(list.get(i),LmtIntbankReplyChgSub.class);
                lmtIntbankReplyChgSub.setPkId(UUID.randomUUID().toString());
                lmtIntbankReplyChgSub.setSerno(serno);
                String subSerno = sequenceTemplateClient.getSequenceTemplate(CmisCommonConstants.LMT_SUB_CHG_SERNO, new HashMap<>());
                lmtIntbankReplyChgSub.setSubSerno(subSerno);
                lmtIntbankReplyChgSub.setCreateTime(date);
                lmtIntbankReplyChgSubService.insert(lmtIntbankReplyChgSub);
            }
        }

        return result;
    }

    /**
     * 根据申请流水号查询
     * @param serno
     * @return
     */
    public LmtIntbankReplyChg selectBySerno(String serno){
        return  lmtIntbankReplyChgMapper.selectBySerno(serno);
    }

    /**
     * 根据批复编号查询
     * @param replySerno
     * @return
     */
    public LmtIntbankReplyChg selectByReplySerno(String replySerno){
        return  lmtIntbankReplyChgMapper.selectByReplySerno(replySerno);
    }

    /**
     * 更改批复变更信息
     * @param lmtIntbankReplyChg
     * @return
     */
    public int updateReplyChg(LmtIntbankReplyChg lmtIntbankReplyChg){
        return lmtIntbankReplyChgMapper.updateReplyChg(lmtIntbankReplyChg);
    }

    /**
     * 逻辑删除
     * 1.判断当前申请状态是否为未发起 逻辑删除
     * 2.判断当前申请状态是否为退回，修改为自行退出
     * @param lmtIntbankReplyChg
     * @return
     */
    @Transactional
    public int logicalDelete(LmtIntbankReplyChg lmtIntbankReplyChg){
        int count = 0;
        log.info("同业授信批复变更申请删除开始。。。");
        //.判断当前申请状态是否为未发起 逻辑删除
        if (CmisBizConstants.APPLY_STATE_TODO.equals(lmtIntbankReplyChg.getApproveStatus())){
            lmtIntbankReplyChgMapper.updateOprType(lmtIntbankReplyChg);
            count++;
            String serno = lmtIntbankReplyChg.getSerno();
            List<LmtIntbankReplyChgSub>  list =lmtIntbankReplyChgSubService.selectBySerno(serno);
            if(list !=null && list.size()>0){
                for(int i=0;i<list.size();i++){
                    LmtIntbankReplyChgSub lmtIntbankReplyChgSub = list.get(i);
                    lmtIntbankReplyChgSub.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
                    lmtIntbankReplyChgSubService.updateSelective(lmtIntbankReplyChgSub);
                    count++;
                }
            }
        }
        //2.判断当前申请状态是否为退回，修改为自行退出
        if (CmisBizConstants.APPLY_STATE_CALL_BACK.equals(lmtIntbankReplyChg.getApproveStatus())){
            //流程删除 修改为自行退出
            log.info("流程删除==》bizId：",lmtIntbankReplyChg.getSerno());
            workflowCoreClient.deleteByBizId(lmtIntbankReplyChg.getSerno());
            lmtIntbankReplyChg.setApproveStatus(CmisBizConstants.APPLY_STATE_QUIT);
            return lmtIntbankReplyChgMapper.updateByPrimaryKey(lmtIntbankReplyChg);
        }
        log.info("同业授信批复变更申请删除结束。。。");
        return count;
    }


    /**
     * 资金业务授信批复变更审批流程(通过、否决 录入批复表中)
     * 只更新批复结果到台账表中（不生成审批表和批复结果）
     * @param lmtIntbankReplyChg
     * @param currentUserId
     * @param currentOrgId
     * @param approveStatus
     */
    @Transactional(rollbackFor=Exception.class)
    public void handleAfterEnd(LmtIntbankReplyChg lmtIntbankReplyChg, String currentUserId, String currentOrgId, String approveStatus) throws Exception{
        log.info("批复变更流程审批处理结束开始.....");
        //更新申请表状态
        lmtIntbankReplyChg.setApproveStatus(approveStatus);

        update(lmtIntbankReplyChg);

        if(CmisBizConstants.APPLY_STATE_PASS.equals(approveStatus)){
            this.flowPassHandleAfterEnd(lmtIntbankReplyChg, currentUserId, currentOrgId) ;
        }
        log.info("批复变更流程审批处理结束.....");
    }

    /**
     * 批复变更-审批否决（生成批复记录）
     * @param lmtIntbankReplyChg
     * @param currentUserId
     * @param currentOrgId
     */
    private void flowRefuseHandleAfterEnd(LmtIntbankReplyChg lmtIntbankReplyChg, String currentUserId, String currentOrgId) throws Exception{
        String oldReplySerno = lmtIntbankReplyChg.getReplySerno();
        //生成批复变更批复信息
        //生成批复主表信息
        LmtIntbankReply newlmtIntbankReply = lmtIntbankReplyService.initLmtIntBankRelyInfoByReplyChg(lmtIntbankReplyChg) ;
        lmtIntbankReplyService.insert(newlmtIntbankReply);
        String newReplySerno = newlmtIntbankReply.getReplySerno();

        /**
         * 1.生成批复信息
         * 2.更新台账表（newReplySerno）
         */
        LmtIntbankAcc lmtIntbankAcc = lmtIntbankAccService.selectByReplyNo(oldReplySerno);
        if (lmtIntbankAcc == null){
            throw new Exception(EclEnum.ECL070072.value) ;
        }
        String serno = lmtIntbankAcc.getSerno();

        //获取批复变更分项信息
        List<LmtIntbankReplyChgSub> lmtIntbankReplyChgSubs = lmtIntbankReplyChgSubService.selectBySerno(lmtIntbankReplyChg.getSerno());
        for (LmtIntbankReplyChgSub lmtIntbankReplyChgSub : lmtIntbankReplyChgSubs) {
            //添加批复信息
            lmtIntbankReplySubService.insertReplySub(lmtIntbankReplyChgSub,serno,newReplySerno,currentOrgId,currentUserId);
        }
    }

    /**
     * 批复变更-审批通过（生成批复记录和更新原台账信息）
     * @param lmtIntbankReplyChg
     * @param currentUserId
     * @param currentOrgId
     * @throws Exception
     */
    private void flowPassHandleAfterEnd(LmtIntbankReplyChg lmtIntbankReplyChg, String currentUserId, String currentOrgId) throws Exception{

        String replySerno = lmtIntbankReplyChg.getReplySerno();
        if(StringUtils.isBlank(replySerno)){
            throw new Exception("批复流水号获取异常！");
        }
        LmtIntbankReply lmtIntbankReply = lmtIntbankReplyService.selectByReplySerno(replySerno);
        if(lmtIntbankReply==null){
            throw new Exception("批复信息获取异常！");
        }
        lmtIntbankReply.setLmtAmt(lmtIntbankReplyChg.getLmtAmt());
        lmtIntbankReply.setUpdDate(DateUtils.getCurrDateStr());
        lmtIntbankReply.setUpdateTime(DateUtils.getCurrTimestamp());
        //默认 同意
        lmtIntbankReply.setApprResult(CmisCommonConstants.commonSignMap.get("0-12"));
        lmtIntbankReplyService.update(lmtIntbankReply);

        /**
         * 2.更新台账表（newReplySerno）
         */
        LmtIntbankAcc lmtIntbankAcc = lmtIntbankAccService.selectByReplyNo(replySerno);
        if (lmtIntbankAcc == null){
            throw new Exception(EclEnum.ECL070072.value) ;
        }
        lmtIntbankAcc.setLmtAmt(lmtIntbankReplyChg.getLmtAmt());
        lmtIntbankAcc.setUpdDate(DateUtils.getCurrDateStr());
        lmtIntbankAcc.setUpdateTime(DateUtils.getCurrTimestamp());
        //审批结论 默认同意
        lmtIntbankAcc.setApprResult(CmisCommonConstants.commonSignMap.get("0-12"));
        lmtIntbankAccService.update(lmtIntbankAcc);

        //获取批复变更分项信息
        if(StringUtils.isBlank(lmtIntbankReplyChg.getSerno())){
            throw new Exception("批复变更申请流水号为空！");
        }

        QueryModel model = new QueryModel();
        model.addCondition("serno",lmtIntbankReplyChg.getSerno());
        List<LmtIntbankReplyChgSub> lmtIntbankReplyChgSubs = lmtIntbankReplyChgSubService.selectByModel(model);
        if (lmtIntbankReplyChgSubs!=null && lmtIntbankReplyChgSubs.size()>0) {

            for (LmtIntbankReplyChgSub lmtIntbankReplyChgSub : lmtIntbankReplyChgSubs) {
                String subSerno = lmtIntbankReplyChgSub.getReplySubSerno();
                //更新批复表
                LmtIntbankReplySub lmtIntbankReplySub = lmtIntbankReplySubService.selectByReplySubSerno(subSerno);
                if(lmtIntbankReplySub!=null){
                    lmtIntbankReplySub.setLmtAmt(lmtIntbankReplyChgSub.getLmtAmt());
                    lmtIntbankReplySub.setOprType(lmtIntbankReplyChgSub.getOprType());
                    lmtIntbankReplySub.setUpdDate(DateUtils.getCurrDateStr());
                    lmtIntbankReplySub.setUpdateTime(DateUtils.getCurrTimestamp());
                    lmtIntbankReplySubService.update(lmtIntbankReplySub);
                }else{
                    throw new Exception("获取批复分项明细异常！");
                }

                //更新台账表
                LmtIntbankAccSub lmtIntbankAccSub = lmtIntbankAccSubService.selectByReplySubSerno(subSerno);
                if(lmtIntbankAccSub!=null){
                    lmtIntbankAccSub.setLmtAmt(lmtIntbankReplyChgSub.getLmtAmt());
                    lmtIntbankAccSub.setOprType(lmtIntbankReplyChgSub.getOprType());
                    lmtIntbankAccSub.setUpdDate(DateUtils.getCurrDateStr());
                    lmtIntbankAccSub.setUpdateTime(DateUtils.getCurrTimestamp());
                    lmtIntbankAccSubService.update(lmtIntbankAccSub);
                }else{
                    throw new Exception("获取批复台账分项明细异常！");
                }
            }
        }

        //发送额度系统更新
        lmtIntbankAccService.lmtIntbankSendCmisLmt0003(lmtIntbankAcc.getSerno(),CmisBizConstants.STD_SX_LMT_TYPE_02);
    }

    /**
     * 同步更改授信额度
     * @param lmtIntbankReplyChg
     * @return
     */
    public int updateLmtAmt(LmtIntbankReplyChg lmtIntbankReplyChg){

        return lmtIntbankReplyChgMapper.updateLmtAmt( lmtIntbankReplyChg);
    }

    /**
     * 更新同业授信变更申请表数据
     * @param lmtIntbankReplyChg
     * @param approveStatus
     * @param currentUserId
     * @param currentOrgId
     */
    @Transactional
    public void updateReplyChgStatus(LmtIntbankReplyChg lmtIntbankReplyChg, String approveStatus, String currentUserId, String currentOrgId) {
        //更新同业授信变更申请表数据
        lmtIntbankReplyChg.setApproveStatus(approveStatus);
        initUpdateDomainProperties(lmtIntbankReplyChg,currentUserId,currentOrgId);
//        lmtIntbankReplyChg.setUpdBrId(currentOrgId);
//        lmtIntbankReplyChg.setUpdId(currentUserId);
//        lmtIntbankReplyChg.setUpdDate(getCurrrentDateStr());
//        lmtIntbankReplyChg.setUpdateTime(getCurrrentDate());
        update(lmtIntbankReplyChg);
    }

    /**
     * @作者:lizx
     * @方法名称: sendWbMsgNotice
     * @方法描述:  推送首页提醒事项
     * @参数与返回说明:
     * @算法描述: 无
     * @日期：2021/7/1 15:36
     * @param lmtIntbankReplyChg:
     * @param messageType:
     * @param comment:
     * @param inputId:
     * @param inputBrId:
     * @return: void
     * @算法描述: 无
     */
    @Transactional(rollbackFor=Exception.class)
    public void sendWbMsgNotice( LmtIntbankReplyChg lmtIntbankReplyChg,String messageType,String comment,String inputId,String inputBrId,String result) throws Exception {
        log.info("同业授信批复变更【{}】处理通知首页提醒事项处理...", lmtIntbankReplyChg.getSerno());

        Map<String, String> map = new HashMap<>();
        map.put("cusName", lmtIntbankReplyChg.getCusName());
        map.put("prdName", "同业授信批复变更");
        map.put("result", result);
        ResultDto<AdminSmUserDto> byLoginCode = adminSmUserService.getByLoginCode(lmtIntbankReplyChg.getManagerId());
        sendMessage.sendMessage("MSG_ZJ_M_0001",map,"1",lmtIntbankReplyChg.getManagerId(),byLoginCode.getData().getUserMobilephone());
    }
}
