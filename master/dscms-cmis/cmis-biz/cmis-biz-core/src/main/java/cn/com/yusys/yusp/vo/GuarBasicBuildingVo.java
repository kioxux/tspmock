package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfBuilProject;

public class GuarBasicBuildingVo extends PageQuery {
    private GuarBaseInfo guarBaseInfo;
    private GuarInfBuilProject guarInfBuilProject;

    public GuarBaseInfo getGuarBaseInfo() {
        return guarBaseInfo;
    }

    public void setGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        this.guarBaseInfo = guarBaseInfo;
    }

    public GuarInfBuilProject getGuarInfBuilProject() {
        return guarInfBuilProject;
    }

    public void setGuarInfBuilProject(GuarInfBuilProject guarInfBuilProject) {
        this.guarInfBuilProject = guarInfBuilProject;
    }
}
