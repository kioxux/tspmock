/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpLoanAppPro;
import cn.com.yusys.yusp.service.IqpLoanAppProService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanAppProResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-20 21:02:15
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqploanapppro")
public class IqpLoanAppProResource {
    @Autowired
    private IqpLoanAppProService iqpLoanAppProService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpLoanAppPro>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpLoanAppPro> list = iqpLoanAppProService.selectAll(queryModel);
        return new ResultDto<List<IqpLoanAppPro>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpLoanAppPro>> index(QueryModel queryModel) {
        List<IqpLoanAppPro> list = iqpLoanAppProService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanAppPro>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpLoanAppPro> show(@PathVariable("pkId") String pkId) {
        IqpLoanAppPro iqpLoanAppPro = iqpLoanAppProService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpLoanAppPro>(iqpLoanAppPro);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<IqpLoanAppPro> create(@RequestBody IqpLoanAppPro iqpLoanAppPro) throws URISyntaxException {
        iqpLoanAppProService.insert(iqpLoanAppPro);
        return new ResultDto<IqpLoanAppPro>(iqpLoanAppPro);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpLoanAppPro iqpLoanAppPro) throws URISyntaxException {
        int result = iqpLoanAppProService.update(iqpLoanAppPro);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpLoanAppProService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpLoanAppProService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectBySerno
     * @函数描述:通过流水号查询
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<IqpLoanAppPro> selectBySerno(@RequestBody Map map) {
        IqpLoanAppPro iqpLoanAppPro = iqpLoanAppProService.selectBySerno((String) map.get("serno"));
        if(iqpLoanAppPro == null){
            return new ResultDto<IqpLoanAppPro>(new IqpLoanAppPro());
        }else{
            return new ResultDto<IqpLoanAppPro>(iqpLoanAppPro);
        }
    }

    /**
     * @函数名称:addOrUpdateIqpLoanAppProDataBySerno
     * @函数描述:有则更新，无则插入
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addOrUpdateIqpLoanAppProDataBySerno")
    protected ResultDto<Integer> addOrUpdateIqpLoanAppProDataBySerno(@RequestBody IqpLoanAppPro iqpLoanAppPro) throws URISyntaxException {
        int result = iqpLoanAppProService.addOrUpdateIqpLoanAppProDataBySerno(iqpLoanAppPro);
        return new ResultDto<Integer>(result);
    }
}
