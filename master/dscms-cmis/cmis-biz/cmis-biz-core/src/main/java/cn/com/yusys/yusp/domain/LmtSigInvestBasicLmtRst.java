/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicLmtRst
 * @类描述: lmt_sig_invest_basic_lmt_rst数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-31 10:28:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_basic_lmt_rst")
public class LmtSigInvestBasicLmtRst extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 批复流水号 **/
	@Column(name = "REPLY_SERNO", unique = false, nullable = false, length = 40)
	private String replySerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 底层批复流水号 **/
	@Column(name = "BASIC_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String basicReplySerno;
	
	/** 底层申请流水号 **/
	@Column(name = "BASIC_SERNO", unique = false, nullable = true, length = 40)
	private String basicSerno;
	
	/** 原底层授信台账号 **/
	@Column(name = "ORIGI_BASIC_ACC_NO", unique = false, nullable = true, length = 40)
	private String origiBasicAccNo;
	
	/** 底层客户编号 **/
	@Column(name = "BASIC_CUS_ID", unique = false, nullable = true, length = 20)
	private String basicCusId;
	
	/** 底层客户名称 **/
	@Column(name = "BASIC_CUS_NAME", unique = false, nullable = true, length = 80)
	private String basicCusName;
	
	/** 底层客户大类 **/
	@Column(name = "BASIC_CUS_CATALOG", unique = false, nullable = true, length = 5)
	private String basicCusCatalog;
	
	/** 底层客户类型 **/
	@Column(name = "BASIC_CUS_TYPE", unique = false, nullable = true, length = 5)
	private String basicCusType;
	
	/** 底层授信品种编号 **/
	@Column(name = "BASIC_LMT_BIZ_TYPE", unique = false, nullable = true, length = 40)
	private String basicLmtBizType;
	
	/** 底层授信品种名称 **/
	@Column(name = "BASIC_LMT_BIZ_TYPE_NAME", unique = false, nullable = true, length = 80)
	private String basicLmtBizTypeName;
	
	/** 授信金额 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;
	
	/** 是否循环 **/
	@Column(name = "IS_REVOLV", unique = false, nullable = true, length = 5)
	private String isRevolv;
	
	/** 是否大额授信 **/
	@Column(name = "IS_LARGE_LMT", unique = false, nullable = true, length = 5)
	private String isLargeLmt;
	
	/** 是否需报备董事长 **/
	@Column(name = "IS_REPORT_CHAIRMAN", unique = false, nullable = true, length = 5)
	private String isReportChairman;
	
	/** 底层基础资产基本情况分析 **/
	@Column(name = "BASIC_ASSET_BASIC_CASE_ANALY", unique = false, nullable = true, length = 2000)
	private String basicAssetBasicCaseAnaly;
	
	/** 经营情况分析 **/
	@Column(name = "OPER_CASE_ANALY", unique = false, nullable = true, length = 2000)
	private String operCaseAnaly;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 2000)
	private String otherDesc;
	
	/** 其他情况分析 **/
	@Column(name = "OTHER_CASE_ANALY", unique = false, nullable = true, length = 2000)
	private String otherCaseAnaly;
	
	/** 其他担保/增信情况说明 **/
	@Column(name = "GUAR_DESC_EXT", unique = false, nullable = true, length = 4000)
	private String guarDescExt;
	
	/** 同业授信准入 **/
	@Column(name = "INTBANK_LMT_ADMIT", unique = false, nullable = true, length = 2000)
	private String intbankLmtAdmit;
	
	/** 调查结论 **/
	@Column(name = "INDGT_RESULT", unique = false, nullable = true, length = 2000)
	private String indgtResult;
	
	/** 终审机构类型 **/
	@Column(name = "FINAL_APPR_BR_TYPE", unique = false, nullable = true, length = 5)
	private String finalApprBrType;
	
	/** 审批模式 **/
	@Column(name = "APPR_MODE", unique = false, nullable = true, length = 5)
	private String apprMode;
	
	/** 审批结论 **/
	@Column(name = "APPR_RESULT", unique = false, nullable = true, length = 5)
	private String apprResult;
	
	/** 批复状态 **/
	@Column(name = "REPLY_STATUS", unique = false, nullable = true, length = 5)
	private String replyStatus;
	
	/** 批复生效日期 **/
	@Column(name = "REPLY_INURE_DATE", unique = false, nullable = true, length = 10)
	private String replyInureDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}
	
    /**
     * @return replySerno
     */
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param basicReplySerno
	 */
	public void setBasicReplySerno(String basicReplySerno) {
		this.basicReplySerno = basicReplySerno;
	}
	
    /**
     * @return basicReplySerno
     */
	public String getBasicReplySerno() {
		return this.basicReplySerno;
	}
	
	/**
	 * @param basicSerno
	 */
	public void setBasicSerno(String basicSerno) {
		this.basicSerno = basicSerno;
	}
	
    /**
     * @return basicSerno
     */
	public String getBasicSerno() {
		return this.basicSerno;
	}
	
	/**
	 * @param origiBasicAccNo
	 */
	public void setOrigiBasicAccNo(String origiBasicAccNo) {
		this.origiBasicAccNo = origiBasicAccNo;
	}
	
    /**
     * @return origiBasicAccNo
     */
	public String getOrigiBasicAccNo() {
		return this.origiBasicAccNo;
	}
	
	/**
	 * @param basicCusId
	 */
	public void setBasicCusId(String basicCusId) {
		this.basicCusId = basicCusId;
	}
	
    /**
     * @return basicCusId
     */
	public String getBasicCusId() {
		return this.basicCusId;
	}
	
	/**
	 * @param basicCusName
	 */
	public void setBasicCusName(String basicCusName) {
		this.basicCusName = basicCusName;
	}
	
    /**
     * @return basicCusName
     */
	public String getBasicCusName() {
		return this.basicCusName;
	}
	
	/**
	 * @param basicCusCatalog
	 */
	public void setBasicCusCatalog(String basicCusCatalog) {
		this.basicCusCatalog = basicCusCatalog;
	}
	
    /**
     * @return basicCusCatalog
     */
	public String getBasicCusCatalog() {
		return this.basicCusCatalog;
	}
	
	/**
	 * @param basicCusType
	 */
	public void setBasicCusType(String basicCusType) {
		this.basicCusType = basicCusType;
	}
	
    /**
     * @return basicCusType
     */
	public String getBasicCusType() {
		return this.basicCusType;
	}
	
	/**
	 * @param basicLmtBizType
	 */
	public void setBasicLmtBizType(String basicLmtBizType) {
		this.basicLmtBizType = basicLmtBizType;
	}
	
    /**
     * @return basicLmtBizType
     */
	public String getBasicLmtBizType() {
		return this.basicLmtBizType;
	}
	
	/**
	 * @param basicLmtBizTypeName
	 */
	public void setBasicLmtBizTypeName(String basicLmtBizTypeName) {
		this.basicLmtBizTypeName = basicLmtBizTypeName;
	}
	
    /**
     * @return basicLmtBizTypeName
     */
	public String getBasicLmtBizTypeName() {
		return this.basicLmtBizTypeName;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param isRevolv
	 */
	public void setIsRevolv(String isRevolv) {
		this.isRevolv = isRevolv;
	}
	
    /**
     * @return isRevolv
     */
	public String getIsRevolv() {
		return this.isRevolv;
	}
	
	/**
	 * @param isLargeLmt
	 */
	public void setIsLargeLmt(String isLargeLmt) {
		this.isLargeLmt = isLargeLmt;
	}
	
    /**
     * @return isLargeLmt
     */
	public String getIsLargeLmt() {
		return this.isLargeLmt;
	}
	
	/**
	 * @param isReportChairman
	 */
	public void setIsReportChairman(String isReportChairman) {
		this.isReportChairman = isReportChairman;
	}
	
    /**
     * @return isReportChairman
     */
	public String getIsReportChairman() {
		return this.isReportChairman;
	}
	
	/**
	 * @param basicAssetBasicCaseAnaly
	 */
	public void setBasicAssetBasicCaseAnaly(String basicAssetBasicCaseAnaly) {
		this.basicAssetBasicCaseAnaly = basicAssetBasicCaseAnaly;
	}
	
    /**
     * @return basicAssetBasicCaseAnaly
     */
	public String getBasicAssetBasicCaseAnaly() {
		return this.basicAssetBasicCaseAnaly;
	}
	
	/**
	 * @param operCaseAnaly
	 */
	public void setOperCaseAnaly(String operCaseAnaly) {
		this.operCaseAnaly = operCaseAnaly;
	}
	
    /**
     * @return operCaseAnaly
     */
	public String getOperCaseAnaly() {
		return this.operCaseAnaly;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param otherCaseAnaly
	 */
	public void setOtherCaseAnaly(String otherCaseAnaly) {
		this.otherCaseAnaly = otherCaseAnaly;
	}
	
    /**
     * @return otherCaseAnaly
     */
	public String getOtherCaseAnaly() {
		return this.otherCaseAnaly;
	}
	
	/**
	 * @param guarDescExt
	 */
	public void setGuarDescExt(String guarDescExt) {
		this.guarDescExt = guarDescExt;
	}
	
    /**
     * @return guarDescExt
     */
	public String getGuarDescExt() {
		return this.guarDescExt;
	}
	
	/**
	 * @param intbankLmtAdmit
	 */
	public void setIntbankLmtAdmit(String intbankLmtAdmit) {
		this.intbankLmtAdmit = intbankLmtAdmit;
	}
	
    /**
     * @return intbankLmtAdmit
     */
	public String getIntbankLmtAdmit() {
		return this.intbankLmtAdmit;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult;
	}
	
    /**
     * @return indgtResult
     */
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param finalApprBrType
	 */
	public void setFinalApprBrType(String finalApprBrType) {
		this.finalApprBrType = finalApprBrType;
	}
	
    /**
     * @return finalApprBrType
     */
	public String getFinalApprBrType() {
		return this.finalApprBrType;
	}
	
	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode;
	}
	
    /**
     * @return apprMode
     */
	public String getApprMode() {
		return this.apprMode;
	}
	
	/**
	 * @param apprResult
	 */
	public void setApprResult(String apprResult) {
		this.apprResult = apprResult;
	}
	
    /**
     * @return apprResult
     */
	public String getApprResult() {
		return this.apprResult;
	}
	
	/**
	 * @param replyStatus
	 */
	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}
	
    /**
     * @return replyStatus
     */
	public String getReplyStatus() {
		return this.replyStatus;
	}
	
	/**
	 * @param replyInureDate
	 */
	public void setReplyInureDate(String replyInureDate) {
		this.replyInureDate = replyInureDate;
	}
	
    /**
     * @return replyInureDate
     */
	public String getReplyInureDate() {
		return this.replyInureDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}