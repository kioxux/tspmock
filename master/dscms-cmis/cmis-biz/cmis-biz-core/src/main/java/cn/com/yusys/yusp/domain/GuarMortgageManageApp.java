/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarMortgageManageApp
 * @类描述: guar_mortgage_manage_app数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-05-17 16:23:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "guar_mortgage_manage_app")
public class GuarMortgageManageApp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 主合同编号 **/
	@Column(name = "MAIN_CONT_NO", unique = false, nullable = true, length = 40)
	private String mainContNo;
	
	/** 抵押办理类型 **/
	@Column(name = "REG_TYPE", unique = false, nullable = true, length = 5)
	private String regType;
	
	/** 抵押注销类型 **/
	@Column(name = "REG_SUB_TYPE", unique = false, nullable = true, length = 5)
	private String regSubType;
	
	/** 担保合同编号 **/
	@Column(name = "GUAR_CONT_NO", unique = false, nullable = true, length = 40)
	private String guarContNo;
	
	/** 担保合同类型 **/
	@Column(name = "GUAR_CONT_TYPE", unique = false, nullable = true, length = 5)
	private String guarContType;
	
	/** 担保方式 **/
	@Column(name = "GUAR_WAY", unique = false, nullable = true, length = 5)
	private String guarWay;
	
	/** 担保金额 **/
	@Column(name = "GUAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal guarAmt;
	
	/** 申请原因 **/
	@Column(name = "REG_REASON", unique = false, nullable = true, length = 500)
	private String regReason;
	
	/** 是否在线办理抵押登记/注销 **/
	@Column(name = "IS_REG_ONLINE", unique = false, nullable = true, length = 5)
	private String isRegOnline;
	
	/** 是否先放款后抵押 **/
	@Column(name = "BEFOREHAND_IND", unique = false, nullable = true, length = 5)
	private String beforehandInd;
	
	/** 审批状态 **/
	@Column(name = "APPROVE_STATUS", unique = false, nullable = true, length = 5)
	private String approveStatus;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 是否还款即解押 **/
	@Column(name = "IS_REPAY_REMOVE_GUAR", unique = false, nullable = true, length = 2)
	private String isRepayRemoveGuar;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param mainContNo
	 */
	public void setMainContNo(String mainContNo) {
		this.mainContNo = mainContNo;
	}
	
    /**
     * @return mainContNo
     */
	public String getMainContNo() {
		return this.mainContNo;
	}
	
	/**
	 * @param regType
	 */
	public void setRegType(String regType) {
		this.regType = regType;
	}
	
    /**
     * @return regType
     */
	public String getRegType() {
		return this.regType;
	}
	
	/**
	 * @param regSubType
	 */
	public void setRegSubType(String regSubType) {
		this.regSubType = regSubType;
	}
	
    /**
     * @return regSubType
     */
	public String getRegSubType() {
		return this.regSubType;
	}
	
	/**
	 * @param guarContNo
	 */
	public void setGuarContNo(String guarContNo) {
		this.guarContNo = guarContNo;
	}
	
    /**
     * @return guarContNo
     */
	public String getGuarContNo() {
		return this.guarContNo;
	}
	
	/**
	 * @param guarContType
	 */
	public void setGuarContType(String guarContType) {
		this.guarContType = guarContType;
	}
	
    /**
     * @return guarContType
     */
	public String getGuarContType() {
		return this.guarContType;
	}
	
	/**
	 * @param guarWay
	 */
	public void setGuarWay(String guarWay) {
		this.guarWay = guarWay;
	}
	
    /**
     * @return guarWay
     */
	public String getGuarWay() {
		return this.guarWay;
	}
	
	/**
	 * @param guarAmt
	 */
	public void setGuarAmt(java.math.BigDecimal guarAmt) {
		this.guarAmt = guarAmt;
	}
	
    /**
     * @return guarAmt
     */
	public java.math.BigDecimal getGuarAmt() {
		return this.guarAmt;
	}
	
	/**
	 * @param regReason
	 */
	public void setRegReason(String regReason) {
		this.regReason = regReason;
	}
	
    /**
     * @return regReason
     */
	public String getRegReason() {
		return this.regReason;
	}
	
	/**
	 * @param isRegOnline
	 */
	public void setIsRegOnline(String isRegOnline) {
		this.isRegOnline = isRegOnline;
	}
	
    /**
     * @return isRegOnline
     */
	public String getIsRegOnline() {
		return this.isRegOnline;
	}
	
	/**
	 * @param beforehandInd
	 */
	public void setBeforehandInd(String beforehandInd) {
		this.beforehandInd = beforehandInd;
	}
	
    /**
     * @return beforehandInd
     */
	public String getBeforehandInd() {
		return this.beforehandInd;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	
    /**
     * @return approveStatus
     */
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param isRepayRemoveGuar
	 */
	public void setIsRepayRemoveGuar(String isRepayRemoveGuar) {
		this.isRepayRemoveGuar = isRepayRemoveGuar;
	}
	
    /**
     * @return isRepayRemoveGuar
     */
	public String getIsRepayRemoveGuar() {
		return this.isRepayRemoveGuar;
	}


}