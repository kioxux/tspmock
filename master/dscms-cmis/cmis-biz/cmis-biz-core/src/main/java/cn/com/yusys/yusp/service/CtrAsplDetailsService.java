package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.req.CmisLmt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0012.resp.CmisLmt0012RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CtrAsplDetailsMapper;
import cn.com.yusys.yusp.repository.mapper.IqpAppAsplMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrAsplDetailsService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-01 20:55:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CtrAsplDetailsService {

    private static final Logger log = LoggerFactory.getLogger(IqpEntrustLoanAppService.class);

    @Autowired
    private CtrAsplDetailsMapper ctrAsplDetailsMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private IqpAppAsplService iqpAppAsplService;

    @Autowired
    private AsplBlacksService asplBlacksService;

    @Autowired
    private IqpAppAsplMapper iqpAppAsplMapper;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    @Autowired
    private BailAccInfoService bailAccInfoService;

    @Autowired
    private LmtReplyService lmtReplyService;

    @Autowired
    private AccLoanService accLoanServcie;

    @Autowired
    private AccAccpService accAccpServcie;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private AdminSmPropService adminSmPropService;

    @Autowired
    private AccAccpService accAccpService;

    @Autowired
    private AccLoanService accLoanService;

    @Autowired
    private ICusClientService iCusClientService;

    @Autowired
    private DocArchiveInfoService docArchiveInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;


    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CtrAsplDetails selectByPrimaryKey(String pkId) {
        return ctrAsplDetailsMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CtrAsplDetails> selectAll(QueryModel model) {
        List<CtrAsplDetails> records = (List<CtrAsplDetails>) ctrAsplDetailsMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CtrAsplDetails> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CtrAsplDetails> list = ctrAsplDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CtrAsplDetails record) {
        return ctrAsplDetailsMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CtrAsplDetails record) {
        return ctrAsplDetailsMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CtrAsplDetails record) {
        return ctrAsplDetailsMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CtrAsplDetails record) {
        return ctrAsplDetailsMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return ctrAsplDetailsMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return ctrAsplDetailsMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryCtrAsplDetailsDataByParams
     * @方法描述: 根据入参查询资产池协议数据
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CtrAsplDetails queryCtrAsplDetailsDataByParams(Map queryMap) {
        return ctrAsplDetailsMapper.queryCtrAsplDetailsDataByParams(queryMap);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<CtrAsplDetails> toSignlist(QueryModel model) throws ParseException {
        PageHelper.startPage(model.getPage(), model.getSize());
        model.setSort("input_date desc");
        model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
        List<CtrAsplDetails> list = ctrAsplDetailsMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 资产池协议新增页面点击下一步
     *
     * @param ctrAsplDetails
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map savectrAsplDetailsInfo(CtrAsplDetails ctrAsplDetails) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        try {
            if (ctrAsplDetails == null) {
                rtnCode = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_IQP_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            //根据选取的资产池协议编号得到相关数据
            Map queryData = new HashMap();
            queryData.put("oprType", CommonConstance.OPR_TYPE_ADD);
            queryData.put("contNo", ctrAsplDetails.getContNo());
            CtrAsplDetails ctrAsplDetailsNew = this.queryCtrAsplDetailsDataByParams(queryData);
            if (ctrAsplDetailsNew == null) {
                rtnCode = EcbEnum.E_CTR_ASPLDETAILSCHANGE_NOT_EXISTS.key;
                rtnMsg = EcbEnum.E_CTR_ASPLDETAILSCHANGE_NOT_EXISTS.value;
                return result;
            }

            //生成新的资产池协议申请
            IqpAppAspl iqpAppAspl = new IqpAppAspl();
            BeanUtils.copyProperties(ctrAsplDetailsNew, iqpAppAspl);

            Map seqMap = new HashMap();
            //生成业务流水号
            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);
            iqpAppAspl.setSerno(serno);
            iqpAppAspl.setPkId(StringUtils.uuid(true));
            iqpAppAspl.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            iqpAppAspl.setBusiType(CmisCommonConstants.STD_BUSI_TYPE_10);
            iqpAppAspl.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            iqpAppAspl.setContType(CommonConstance.CONT_TYPE_3);
            iqpAppAspl.setOrigiContNo(ctrAsplDetailsNew.getContNo());
            iqpAppAspl.setChgFlag(CommonConstance.STD_ZB_YES_NO_1);// 变更 1
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                iqpAppAspl.setInputId(userInfo.getLoginCode());
                iqpAppAspl.setInputBrId(userInfo.getOrg().getCode());
                iqpAppAspl.setManagerId(userInfo.getLoginCode());
                iqpAppAspl.setManagerBrId(userInfo.getOrg().getCode());
                iqpAppAspl.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            int insertCount = iqpAppAsplMapper.insertSelective(iqpAppAspl);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno", ctrAsplDetailsNew.getSerno());
            log.info("资产池协议" + serno + "-新增成功！");
        } catch (YuspException e) {
            log.error("资产池协议变更新增异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("资产池协议变更新增异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * 资产池协议申请通用保存方法
     *
     * @param params
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map commonSaveCtrAsplDetailsInfo(Map params) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        String serno = "";
        try {
            //获取申请流水号
            serno = (String) params.get("serno");
            if (StringUtils.isBlank(serno)) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value + "未获取到申请主键信息";
                return result;
            }

            String logPrefix = "资产池协议" + serno;

            log.info(logPrefix + "获取协议数据");
            CtrAsplDetails ctrAsplDetails = JSONObject.parseObject(JSON.toJSONString(params), CtrAsplDetails.class);
            if (ctrAsplDetails == null) {
                rtnCode = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.key;
                rtnMsg = EcbEnum.LU_UPDATE_PARAMS_EXCEPTION.value;
                return result;
            }


            log.info(logPrefix + "保存资产池协议变更-获取当前登录用户信息");
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                ctrAsplDetails.setUpdId(userInfo.getLoginCode());
                ctrAsplDetails.setUpdBrId(userInfo.getOrg().getCode());
                ctrAsplDetails.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            log.info(logPrefix + "资产池协议数据");
            int updCount = ctrAsplDetailsMapper.updateByPrimaryKeySelective(ctrAsplDetails);
            if (updCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }
            IqpAppAspl iqpAppAspl = new IqpAppAspl();
            iqpAppAspl.setSupshAgrAmt(ctrAsplDetails.getSupshAgrAmt());
            iqpAppAspl.setChrgRate(ctrAsplDetails.getChrgRate());
            iqpAppAspl.setPadRateYear(ctrAsplDetails.getPadRateYear());
            int insertCount = iqpAppAsplMapper.updateByPrimaryKeySelective(iqpAppAspl);
            if (insertCount < 0) {
                throw new YuspException(EcbEnum.LU_UPDATE_FAILED_EXCEPTION.key, EcbEnum.LU_UPDATE_FAILED_EXCEPTION.value);
            }

        } catch (Exception e) {
            log.error("保存资产池协议" + serno + "异常", e);
            rtnCode = EcbEnum.LIA_DEF_EXCEPTION.key;
            rtnMsg = EcbEnum.LIA_DEF_EXCEPTION.value;
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int logicDelete(CtrAsplDetails ctrAsplDetails) {
        ctrAsplDetails.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return ctrAsplDetailsMapper.updateByPrimaryKey(ctrAsplDetails);
    }


    /**
     * @方法名称: ctrAsplDetailsStartStopLogout
     * @方法描述: 资产池协议启用、停用和注销
     * @参数与返回说明:
     * @算法描述:
     * @创建人: quwen
     * @创建时间: 2021-05-27 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Map ctrAsplDetailsStartStopLogout(Map<String,Object> map) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "操作成功";
        log.info("资产池协议生效/注销开始！！");
        try {
            String contNo = (String)map.get("contNo");
            String contStatus = (String)map.get("contStatus");
            if (StringUtils.isEmpty(contNo) || StringUtils.isEmpty(contStatus)) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            }
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            CtrAsplDetails ctrAsplDetailsData = ctrAsplDetailsMapper.selectCtrAsplDetailsInfoByContNo(contNo);
            String cusId = ctrAsplDetailsData.getCusId();
            String serno = ctrAsplDetailsData.getSerno();
            // 判断合同状态
            if(Objects.equals("600",contStatus)){
                // 注销
                // 资产池协议注销会释放额度
                ResultDto<CmisLmt0012RespDto> resultDto = this.recoverLmt(ctrAsplDetailsData);
                if (!"0".equals(resultDto.getCode())) {
                    log.error("【额度系统CmisLmt0012】接口调用异常！");
                    throw BizException.error(null, "9999",resultDto.getMessage());
                }
                if (!"0000".equals(resultDto.getData().getErrorCode())) {
                    log.error("资产池协议【"+contNo+"】【额度系统CmisLmt0012】额度注销失败");
                    throw BizException.error(null, "9999", resultDto.getData().getErrorMsg());
                }
            }else if(Objects.equals("200",contStatus)){
                // 1、注销其他生效协议,根据客户编号查询名下是否存在其他生效的资产协议，如果存在 判断是否到期，如果到期 注销，没到期中止
                CtrAsplDetails origiCtrAsplDetails = selectInfoByCusId(cusId);// 资产池生效永远只有一条
                if(Objects.nonNull(origiCtrAsplDetails)){
                    Date endDayDate = DateUtils.parseDate(origiCtrAsplDetails.getEndDate(), DateFormatEnum.DEFAULT.getValue());
                    Date openDayDate = DateUtils.parseDate(openDay, DateFormatEnum.DEFAULT.getValue());
                    String origiContStatus = "";
                    if(openDayDate.compareTo(endDayDate)>1){
                        // 协议到期日 到期 注销
                        origiContStatus = CmisBizConstants.IQP_CONT_STS_600;
                    }else{
                        // 协议到期日 未到期 中止
                        origiContStatus = CmisBizConstants.IQP_CONT_STS_500;
                    }
                    origiCtrAsplDetails.setContStatus(origiContStatus);
                    origiCtrAsplDetails.setUpdDate(openDay);
                    origiCtrAsplDetails.setUpdateTime(DateUtils.parseDateByDef(openDay));
                    updateSelective(origiCtrAsplDetails);
                    // 原协议注销需要恢复额度
                    ResultDto<CmisLmt0012RespDto> resultDtoCmisLmt0012 =  this.recoverLmt(origiCtrAsplDetails);
                    if (!"0".equals(resultDtoCmisLmt0012.getCode())) {
                        log.error("【额度系统CmisLmt0012】接口调用异常！");
                        throw BizException.error(null, "9999",resultDtoCmisLmt0012.getMessage());
                    }
                    if (!"0000".equals(resultDtoCmisLmt0012.getData().getErrorCode())) {
                        log.error("资产池协议【"+contNo+"】【额度系统CmisLmt0012】额度注销失败");
                        throw BizException.error(null, "9999", resultDtoCmisLmt0012.getData().getErrorMsg());
                    }

                    // 2、原生效资产池担保合同业务关系表(资产池协议业务流水号唯一，业务合同和担保合同关系)
                    // 获取当前协议的担保合同编号（根据协议流水号）
                    GrtGuarBizRstRel grtGuarBizRstRel = grtGuarBizRstRelService.selectGrtGuarBizRstRelDataBySerno(ctrAsplDetailsData.getSerno());
                    String guarCont = grtGuarBizRstRel.getGuarContNo();
                    // 获取原生效协议的担保合同编号（根据协议流水号）
                    GrtGuarBizRstRel origiGrtGuarBizRstRel = grtGuarBizRstRelService.selectGrtGuarBizRstRelDataBySerno(origiCtrAsplDetails.getSerno());
                    String origiguarContNo = origiGrtGuarBizRstRel.getGuarContNo();
                    // 平移押品  担保合同关系 1、获取原担保合同下所有的押品关系 GrtGuarContRel 2、新增
                    QueryModel model =new QueryModel();
                    model.addCondition("guarContNo",origiguarContNo);
                    List<GrtGuarContRel> GrtGuarContRels = grtGuarContRelService.selectAll(model);
                    // 删除当前协议下的担保合同和押品信息
                    grtGuarContRelService.deleteByGuarCont(guarCont);
                    if(CollectionUtils.nonEmpty(GrtGuarContRels)){
                        GrtGuarContRels.forEach(e->{
                            e.setPkId(StringUtils.getUUID());
                            e.setGuarContNo(guarCont);
                            e.setContNo(contNo);
                            e.setInputDate(openDay);
                            grtGuarContRelService.insertSelective(e);
                        });
                    }
                    // 3、平移资产清单 获取原资产池协议下的所有资产
                    QueryModel queryModel=new QueryModel();
                    queryModel.addCondition("contNo",origiCtrAsplDetails.getContNo());
                    List<AsplAssetsList> asplAssetsLists = asplAssetsListService.selectAll(queryModel);
                    // 删除现有协议下所有的资产清单
                    asplAssetsListService.deleteByContNo(contNo);
                    // 重新新增
                    if(CollectionUtils.nonEmpty(asplAssetsLists)){
                        asplAssetsLists.forEach(e->{
                            // 判断是否存在
                            e.setPkId(StringUtils.getUUID());
                            e.setContNo(contNo);
                            e.setInputDate(openDay);
                            asplAssetsListService.insertSelective(e);
                        });
                    }
                }
                // 2、更新当前协议为（生效）
                CtrAsplDetails ctrAsplDetailsNew = new CtrAsplDetails();
                ctrAsplDetailsNew.setPkId(ctrAsplDetailsData.getPkId());
                ctrAsplDetailsNew.setContStatus(contStatus);
                ctrAsplDetailsNew.setUpdateTime(DateUtils.parseDateByDef(openDay));
                ctrAsplDetailsNew.setUpdDate(openDay);
                updateSelective(ctrAsplDetailsNew);
                // 协议生效需要占用额度系统
                ResultDto<CmisLmt0011RespDto> resultDtoCmisLmt0011 =  this.occupyLmt(ctrAsplDetailsData);
                if (!"0".equals(resultDtoCmisLmt0011.getCode())) {
                    log.error("【额度系统CmisLmt0011】接口调用异常！");
                    throw BizException.error(null, "9999",resultDtoCmisLmt0011.getMessage());
                }
                if (!"0000".equals(resultDtoCmisLmt0011.getData().getErrorCode())) {
                    log.error("资产池协议【"+contNo+"】【额度系统CmisLmt0011】额度占用失败");
                    throw BizException.error(null, "9999", resultDtoCmisLmt0011.getData().getErrorMsg());
                }
            }
            // 资产池协议生效生成正式归档任务
            if (Objects.equals(contStatus, CmisCommonConstants.CONT_STATUS_200)) {
                try {
                    log.info("开始系统生成资产池档案归档信息");
                    CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
                    DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
                    docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
                    docArchiveClientDto.setDocType("08");// 资产池业务
                    docArchiveClientDto.setBizSerno(ctrAsplDetailsData.getSerno());
                    docArchiveClientDto.setCusId(cusId);
                    docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
                    docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
                    docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
                    docArchiveClientDto.setManagerId(ctrAsplDetailsData.getManagerId());
                    docArchiveClientDto.setManagerBrId(ctrAsplDetailsData.getManagerBrId());
                    docArchiveClientDto.setInputId(ctrAsplDetailsData.getInputId());
                    docArchiveClientDto.setInputBrId(ctrAsplDetailsData.getInputBrId());
                    docArchiveClientDto.setContNo(ctrAsplDetailsData.getContNo());
                    docArchiveClientDto.setLoanAmt(ctrAsplDetailsData.getContAmt());
                    docArchiveClientDto.setStartDate(ctrAsplDetailsData.getStartDate());
                    docArchiveClientDto.setEndDate(ctrAsplDetailsData.getEndDate());
                    docArchiveClientDto.setPrdId(ctrAsplDetailsData.getPrdId());
                    docArchiveClientDto.setPrdName(ctrAsplDetailsData.getPrdName());
                    int num = docArchiveInfoService.createDocArchiveBySys(docArchiveClientDto);
                    if (num < 1) {
                        log.info("系统生成资产池档案归档信息失败");
                    }
                } catch (Exception e) {
                    log.info("系统生成资产池档案归档信息失败" + e);
                }
            }
        } catch (BizException e) {
            rtnCode = e.getErrorCode();
            rtnMsg = e.getMessage();
        } catch (Exception e){
            rtnMsg = "逻辑异常";
        }finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     *资产池额度占用 Cmislmt0011
     * @param ctrAsplDetails
     * @return
     */
    public ResultDto<CmisLmt0011RespDto> occupyLmt(CtrAsplDetails ctrAsplDetails) {
        // 资产池协议生效前占用额度系统
        String isLriskBiz = "";
        BigDecimal bizTotalAmt = BigDecimal.ZERO;
        BigDecimal bizSpacAmt = BigDecimal.ZERO;
        BigDecimal bizTotalAmtCny = BigDecimal.ZERO;
        BigDecimal bizSpacAmtCny = BigDecimal.ZERO;
        String guarMode = ctrAsplDetails.getGuarMode();
        if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
            isLriskBiz = CmisCommonConstants.STD_ZB_YES_NO_1;
            bizTotalAmt = ctrAsplDetails.getContAmt();
            bizSpacAmt = BigDecimal.ZERO;
            bizTotalAmtCny = ctrAsplDetails.getContAmt();
            bizSpacAmtCny = BigDecimal.ZERO;
        } else {
            isLriskBiz = CmisCommonConstants.STD_ZB_YES_NO_0;
            bizTotalAmt = ctrAsplDetails.getContAmt();
            bizSpacAmt = ctrAsplDetails.getContAmt();
            bizTotalAmtCny = ctrAsplDetails.getContAmt();
            bizSpacAmtCny = ctrAsplDetails.getContAmt();
        }
        CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
        cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrAsplDetails.getManagerBrId()));//金融机构代码
        cmisLmt0011ReqDto.setDealBizNo(ctrAsplDetails.getContNo());//合同编号
        cmisLmt0011ReqDto.setCusId(ctrAsplDetails.getCusId());//客户编号
        cmisLmt0011ReqDto.setCusName(ctrAsplDetails.getCusName());//客户名称
        cmisLmt0011ReqDto.setDealBizType(ctrAsplDetails.getContType());//交易业务类型
        cmisLmt0011ReqDto.setBizAttr(CmisCommonConstants.STD_ZB_BIZ_ATTR_1);//交易属性
        cmisLmt0011ReqDto.setPrdId(ctrAsplDetails.getPrdId());//产品编号
        cmisLmt0011ReqDto.setPrdName(ctrAsplDetails.getPrdName());//产品名称
        cmisLmt0011ReqDto.setIsLriskBiz(isLriskBiz);//是否低风险
        cmisLmt0011ReqDto.setIsFollowBiz(CmisCommonConstants.STD_ZB_YES_NO_0);//是否无缝衔接
        cmisLmt0011ReqDto.setIsBizRev(CmisCommonConstants.STD_ZB_YES_NO_1);//是否合同重签
        cmisLmt0011ReqDto.setOrigiDealBizNo(ctrAsplDetails.getOrigiContNo());//原交易业务编号
        cmisLmt0011ReqDto.setOrigiDealBizStatus("");//原交易业务状态
        cmisLmt0011ReqDto.setOrigiRecoverType("");//原交易业务恢复类型
        cmisLmt0011ReqDto.setOrigiBizAttr("");//原交易属性D
        cmisLmt0011ReqDto.setDealBizAmt(ctrAsplDetails.getContAmt());//交易业务金额
        cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal("0"));//保证金比例
        cmisLmt0011ReqDto.setDealBizBailPreAmt(new BigDecimal("0"));//保证金金额
        cmisLmt0011ReqDto.setStartDate(ctrAsplDetails.getStartDate());//合同起始日
        cmisLmt0011ReqDto.setEndDate(ctrAsplDetails.getEndDate());//合同到期日
        cmisLmt0011ReqDto.setDealBizStatus(CmisCommonConstants.CONT_STATUS_200);//合同状态
        cmisLmt0011ReqDto.setInputId(ctrAsplDetails.getInputId());//登记人
        cmisLmt0011ReqDto.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
        cmisLmt0011ReqDto.setInputDate(ctrAsplDetails.getInputDate());//登记日期

        List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();

        CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
        // 额度类型
        cmisLmt0011OccRelListDto.setLmtType(CmisCommonConstants.STD_ZB_LMT_TYPE_01);//额度类型
        cmisLmt0011OccRelListDto.setLmtSubNo(ctrAsplDetails.getLmtAccNo());//额度分项编号
        cmisLmt0011OccRelListDto.setPrdTypeProp("");//授信品种类型
        cmisLmt0011OccRelListDto.setBizTotalAmt(bizTotalAmt);//占用总额(原币种)
        cmisLmt0011OccRelListDto.setBizSpacAmt(bizSpacAmt);//占用敞口(原币种)
        cmisLmt0011OccRelListDto.setBizTotalAmtCny(bizTotalAmtCny);//占用总额(折人民币)
        cmisLmt0011OccRelListDto.setBizSpacAmtCny(bizSpacAmtCny);//占用敞口(折人民币)
        cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
        cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);
        log.info("资产池协议【"+ctrAsplDetails.getContNo()+"】生效，前往额度系统进行额度占用开始");
        ResultDto<CmisLmt0011RespDto> resultDtoDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
        log.info("资产池协议业务变更" + ctrAsplDetails.getContNo() + "，前往额度系统进行额度占用结束，返回：" + resultDtoDto);
        return resultDtoDto;
    }
    /**
     * 资产池额度释放 CmisLmt0012
     * @param ctrAsplDetailsData
     * @return
     */
    public ResultDto<CmisLmt0012RespDto> recoverLmt(CtrAsplDetails ctrAsplDetailsData) {
        //合同注销后，恢复占额
        String guarMode = ctrAsplDetailsData.getGuarMode();
        //恢复敞口
        BigDecimal recoverSpacAmtCny = BigDecimal.ZERO;
        //恢复总额
        BigDecimal recoverAmtCny = BigDecimal.ZERO;
        if (CmisCommonConstants.GUAR_MODE_60.equals(guarMode) || CmisCommonConstants.GUAR_MODE_21.equals(guarMode)
                || CmisCommonConstants.GUAR_MODE_40.equals(guarMode)) {
            recoverSpacAmtCny = BigDecimal.ZERO;
            recoverAmtCny = ctrAsplDetailsData.getContAmt();
        } else {
            recoverSpacAmtCny = ctrAsplDetailsData.getContAmt();
            recoverAmtCny = ctrAsplDetailsData.getContAmt();
        }
        //合同注销后，释放额度
        log.info("调用额度系统释放额度");
        CmisLmt0012ReqDto cmisLmt0012ReqDto = new CmisLmt0012ReqDto();
        cmisLmt0012ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
        cmisLmt0012ReqDto.setInstuCde(CmisCommonUtils.getInstucde(ctrAsplDetailsData.getManagerBrId()));//金融机构代码
        cmisLmt0012ReqDto.setBizNo(ctrAsplDetailsData.getContNo());//合同编号
        // TODO 01-结清注销 (有台账)02-未用注销(没台账)
        // 查看该协议下有没有 贷款 和 银票台账 （未结清）
        String contNo = ctrAsplDetailsData.getContNo();
        QueryModel model = new QueryModel();
        // 银票
        model.addCondition("contNo", contNo);// 合同编号
        model.addCondition("accStatusArray", "0,1,4,5,6");
        List<AccAccp> AccAccps= accAccpService.selectAll(model);
        // 贷款
        model.addCondition("accStatusArray", "1,2,3,4,6");
        List<AccLoan> AccLoans = accLoanServcie.selectAll(model);
        if (CollectionUtils.isEmpty(AccAccps) && CollectionUtils.isEmpty(AccLoans)) {
            log.info("资产池协议：" + contNo + ",下没有未结清业务！");
            cmisLmt0012ReqDto.setRecoverType("01");//恢复类型
        } else {
            cmisLmt0012ReqDto.setRecoverType("02");//恢复类型
        }

        cmisLmt0012ReqDto.setRecoverSpacAmtCny(recoverSpacAmtCny);//恢复敞口金额（人民币）
        cmisLmt0012ReqDto.setRecoverAmtCny(recoverAmtCny);//恢复总额（人民币）
        cmisLmt0012ReqDto.setInputId(ctrAsplDetailsData.getInputId());
        cmisLmt0012ReqDto.setInputBrId(ctrAsplDetailsData.getInputBrId());
        cmisLmt0012ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
        log.info("额度系统【cmisLmt0012】请求："+Objects.toString(cmisLmt0012ReqDto));
        ResultDto<CmisLmt0012RespDto> resultDto = cmisLmtClientService.cmisLmt0012(cmisLmt0012ReqDto);
        log.info("额度系统【cmisLmt0012】响应："+Objects.toString(resultDto));
        return resultDto;
    }


    /**
     * @方法名称: selectCtrAsplDetailsInfoByContNo
     * @方法描述: 根据合同编号查询资产池协议台账表
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-06-8 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CtrAsplDetails selectCtrAsplDetailsInfoByContNo(String contNo) {
        return ctrAsplDetailsMapper.selectCtrAsplDetailsInfoByContNo(contNo);
    }

    /**
     * @方法名称: isPvpAccpApp
     * @方法描述: 银承出票额度校验
     * @参数与返回说明: contNo资产池协议编号  sumAmt出票总金额
     * @算法描述: 资产池协议状态为有效；
     * 100 未生效  200 生效   500 中止 600 注销  700 撤回  800作废
     * 本次开票金额*（1 - 保证金留存比例）<= 资产池可用融资额度；
     * 到期日<=资产池协议到期日 + 授信批复宽限期。
     * @创建人: xs
     * @创建时间: 2021-06-10 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public OpRespDto isPvpAccpApp(String contNo, BigDecimal sumAmt) {
        OpRespDto opRespDto = new OpRespDto();
        // 检验开关
        if (!getAsplOpenAccp()) {
            opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            opRespDto.setOpMsg("资产池银票业务开关：关闭，详情请联系客户经理！");// 描述信息
            return opRespDto;
        }
        // 资产池协议状态为有效
        CtrAsplDetails ctrAsplDetails = this.selectCtrAsplDetailsInfoByContNo(contNo);
        if (null != ctrAsplDetails) {
            // 校验协议状态（合同状态）是否生效
            if (!CommonConstance.STD_ZB_CONT_TYP_200.equals(ctrAsplDetails.getContStatus())) {
                opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                opRespDto.setOpMsg("该资产池协议状态无效");// 描述信息
                return opRespDto;
            }
            // 校验当前营业日期 是否在协议期限内
            String opendayStr = stringRedisTemplate.opsForValue().get("openDay");
            String startDateStr = ctrAsplDetails.getStartDate();
            String endDateStr = ctrAsplDetails.getEndDate();
            Date startDate = DateUtils.parseDate(startDateStr,DateFormatEnum.DEFAULT.getValue());
            Date endDate = DateUtils.parseDate(endDateStr,DateFormatEnum.DEFAULT.getValue());
            Date opendayDate = DateUtils.parseDate(opendayStr,DateFormatEnum.DEFAULT.getValue());
            if (opendayDate.compareTo(startDate) == -1){
                // 当前营业日期小于协议起始日
                opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                opRespDto.setOpMsg("当前营业日期小于协议起始日");// 描述信息
                return opRespDto;
            }
            if (opendayDate.compareTo(endDate) == 1){
                // 当前营业日期大于协议到期日
                opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                opRespDto.setOpMsg("当前营业日期大于协议到期日");// 描述信息
                return opRespDto;
            }
        } else {
            opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            opRespDto.setOpMsg("未查询到资产池协议台账信息!");// 描述信息
            return opRespDto;
        }
        // 本次开票金额*（1 - 保证金留存比例）<= 资产池可用融资额度
        // 资产池保证金账户余额
        BigDecimal assetPoolBailAmt = asplAssetsListService.getAssetPoolBailAmt(ctrAsplDetails);
        // 资产池融资额度
        BigDecimal assetPoolFinAmt = asplAssetsListService.getAssetPoolFinAmt(ctrAsplDetails, assetPoolBailAmt);
        // 资产池可用融资额度
        BigDecimal assetPoolAvaAmt = asplAssetsListService.getAssetPoolAvaAmt(ctrAsplDetails, assetPoolFinAmt, assetPoolBailAmt);
        // 根据申请流水号查询对应的保证金账户信息(资产池只有一个保证金账号)
        List<BailAccInfo> bailAccInfoList = bailAccInfoService.selectBySerno(ctrAsplDetails.getSerno());
        BailAccInfo bailAccInfo = bailAccInfoList.get(0);
        BigDecimal bailRate = bailAccInfo.getBailRate(); // 保证金留存比例
        BigDecimal assetPoolAvaAmt1 = sumAmt.multiply(BigDecimal.ONE.subtract(bailRate));
        if (assetPoolAvaAmt1.compareTo(assetPoolAvaAmt) > 0) {
            opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            opRespDto.setOpMsg("出票金额超出了资产池可用融资额度");// 描述信息
            return opRespDto;
        }
        opRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
        opRespDto.setOpMsg("出票额度校验通过");// 描述信息
        return opRespDto;
    }

    /**
     * @方法名称: isPvpLoanApp
     * @方法描述: 超短贷出账校验
     * @参数与返回说明: contNo资产池协议编号  sumAmt出帐金额  endDate贷款到期日  loanTerm 出账期限
     * @算法描述: ►资产池协议状态为有效；
     * ►（本次出账金额+资产池下超短贷余额）<= 资产池下超短贷额度；
     * ►本次出账期限<=6个月，且到期日<=资产池协议到期日 + 授信批复宽限期；
     * 受托信息为必填项
     * @创建人: xs
     * @创建时间: 2021-06-10 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public OpRespDto isPvpLoanApp(String contNo, BigDecimal sumAmt, String loanEnd, int loanTerm) {
        OpRespDto opRespDto = new OpRespDto();
        opRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
        opRespDto.setOpMsg("超短贷出账校验通过");// 描述信息
        // 检验开关
        if (!getAsplOpenLoan()) {
            opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            opRespDto.setOpMsg("资产池超短业务开关：关闭，详情请联系客户经理！");// 描述信息
            return opRespDto;
        }
        // 资产池协议状态为有效
        CtrAsplDetails ctrAsplDetails = selectCtrAsplDetailsInfoByContNo(contNo);
        log.info("获取资产池协议详情：" + contNo);
        if (ctrAsplDetails != null) {
            // 校验协议状态（合同状态）是否生效
            if (!CommonConstance.STD_ZB_CONT_TYP_200.equals(ctrAsplDetails.getContStatus())) {
                opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                opRespDto.setOpMsg("该资产池协议状态无效" + contNo);// 描述信息
                return opRespDto;
            }
            // 校验当前营业日期 是否在协议期限内
            String opendayStr = stringRedisTemplate.opsForValue().get("openDay");
            String startDateStr = ctrAsplDetails.getStartDate();
            String endDateStr = ctrAsplDetails.getEndDate();
            Date startDate = DateUtils.parseDate(startDateStr,DateFormatEnum.DEFAULT.getValue());
            Date endDate = DateUtils.parseDate(endDateStr,DateFormatEnum.DEFAULT.getValue());
            Date opendayDate = DateUtils.parseDate(opendayStr,DateFormatEnum.DEFAULT.getValue());
            if (opendayDate.compareTo(startDate) == -1){
                // 当前营业日期小于协议起始日
                opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                opRespDto.setOpMsg("当前营业日期小于协议起始日");// 描述信息
                return opRespDto;
            }
            if (opendayDate.compareTo(endDate) == 1){
                // 当前营业日期大于协议到期日
                opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                opRespDto.setOpMsg("当前营业日期大于协议到期日");// 描述信息
                return opRespDto;
            }
        } else {
            opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            opRespDto.setOpMsg("未查到该笔台账" + contNo);// 描述信息
            return opRespDto;
        }
        // 本次出账金额 +资产池下超短贷余额）<= 资产池下超短贷额度
        // 资产池下超短贷额度 = 资产池协议金额
        BigDecimal supshAgrAmt = ctrAsplDetails.getContAmt();
        // 资产池下超短贷余额( 根据资产池的协议编号 去贷款台账acc_loan PRD_ID查询 贷款余额 )
        List<AccLoan> accLoanList = accLoanServcie.selectByContNo(contNo);
        BigDecimal loanBalance = BigDecimal.ZERO;
        if (CollectionUtils.nonEmpty(accLoanList)) {
            loanBalance = accLoanList.stream()
                    .filter(e -> CommonConstance.STD_ACC_STATUS_2.equals(e.getAccStatus())
                            && "012007".equals(e.getPrdId()))// 未结清 短期流动资金贷款
                    .map(e -> e.getLoanBalance())// 委托贷款台账余额
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
        }
        // 本次出账金额 +资产池下超短贷余额 <= 资产池下超短贷额度
        log.info("本次出账金额:" + Objects.toString(sumAmt) + " + 资产池下超短贷余额:" + Objects.toString(loanBalance) + "<= 资产池下超短贷额度:" + Objects.toString(supshAgrAmt));
        if (sumAmt.add(loanBalance).compareTo(supshAgrAmt) > 0) {
            opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            opRespDto.setOpMsg("出账金额超出可用金额,");// 描述信息
            return opRespDto;
        }
        // 到期日<=资产池协议到期日 + 授信批复宽限期。
        String endDate = ctrAsplDetails.getEndDate();// 资产池协议到期日
        String replySerno = ctrAsplDetails.getReplySerno();// 授信额度编号（取自授信批复 批复流水号 REPLY_SERNO）
        log.info("授信批复编号:" + replySerno);
        LmtReply lmtReply = lmtReplyService.queryLmtReplyByReplySerno(replySerno);
        int month = 0;
        if (Objects.nonNull(lmtReply)) {
            month = lmtReply.getLmtGraperTerm();
        }
        log.info("授信批复宽限期(月):" + month);
        Date date = DateUtils.parseDate(endDate, DateFormatEnum.DEFAULT.getValue());
        // 资产池协议到期日 + 授信批复宽限期
        Date resultDate = DateUtils.addMonth(date, month);
        // 到期日 (查询)
        Date loanEndDate = DateUtils.parseDate(loanEnd, DateFormatEnum.DEFAULT.getValue());
        log.info("到期日：" + loanEnd);
        if (loanEndDate.compareTo(resultDate) > 0) {
            log.info("不满足：到期日<=资产池协议到期日 + 授信批复宽限期。");
            opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            opRespDto.setOpMsg("贷款到期日不能超过:" + DateUtils.formatDate(resultDate, DateFormatEnum.DATETIME.getValue()));// 描述信
        }
        // 本次出账期限<=6个月
        log.info("本次出账期限(月):" + loanTerm);
        if (loanTerm > 6) {
            opRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            opRespDto.setOpMsg("本次出账期限不能大于6个月");// 描述信
        }
        return opRespDto;
    }

    /**
     * @方法名称: ctrAspiDetailsAndBailAccInfoList
     * @方法描述: 保证金信息列表
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<AsplBailAcctDto> ctrAspiDetailsAndBailAccInfoList(QueryModel model) {
        return ctrAsplDetailsMapper.ctrAspiDetailsAndBailAccInfoList(model);
    }

    /**
     * @方法名称: queryCtrAspiDetailsAndBailAccInfoByParams
     * @方法描述: 根据流水号得到资产池协议关联的保证金信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public AsplBailAcctDto queryCtrAspiDetailsAndBailAccInfoByParams(Map map) {
        return ctrAsplDetailsMapper.queryCtrAspiDetailsAndBailAccInfoByParams(map);
    }

    /**
     * @方法名称: selectInfoByCusId
     * @方法描述: 一个客户只有一笔有效的资产池协议
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-06-8 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CtrAsplDetails selectInfoByCusId(String cusId) {
        QueryModel model = new QueryModel();
        model.addCondition("cusId", cusId);
        model.addCondition("contStatus", CmisCommonConstants.CONT_STATUS_200);
        List<CtrAsplDetails> records = (List<CtrAsplDetails>) ctrAsplDetailsMapper.selectByModel(model);
        return records.size() > 0 ? records.get(0) : null;
    }

    /**
     * @方法名称: exportAsplAcc
     * @方法描述: 导出
     * @参数与返回说明:
     * @算法描述: 无
     */
    public ProgressDto exportAsplAcc(CtrAsplDetails ctrAsplDetails) {
        try {
            QueryModel queryModel = new QueryModel();
            queryModel.getCondition().put("cusId", ctrAsplDetails.getCusId());
            queryModel.getCondition().put("cusName", ctrAsplDetails.getCusName());
            queryModel.getCondition().put("contNo", ctrAsplDetails.getContNo());
            queryModel.getCondition().put("contStatus", ctrAsplDetails.getContStatus());
            queryModel.getCondition().put("oprType", CmisLmtConstants.OPR_TYPE_ADD);
            DataAcquisition dataAcquisition = (size, page, object) -> {
                QueryModel queryModeTemp = (QueryModel) object;
                queryModeTemp.setSize(page);
                queryModeTemp.setPage(size);
                return selectByModel(queryModeTemp);
            };
            ExportContext exportContext = ExportContext.of(CtrAsplDetailsExport.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, queryModel);
            return ExcelUtils.asyncExport(exportContext);
        } catch (Exception e) {
            throw new YuspException(EcbEnum.E_EXPORTASPLACCEXCEL_EXCEPTION_01.key, EcbEnum.E_EXPORTASPLACCEXCEL_EXCEPTION_01.value);
        }

    }

    /**
     * @方法名称: inPoolAccList
     * @方法描述: 池内业务台账列表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<AsplAccpEntrustAccDto> inPoolAccList(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        return ctrAsplDetailsMapper.inPoolAccList(model);
    }

    /**
     * @方法名称: queryAsplAccpDtoInfoByParams
     * @方法描述: 根据流水号贸易背景资料审核详情
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public AsplAccpDto queryAsplAccpDtoInfoByParams(Map map) {
        return ctrAsplDetailsMapper.queryAsplAccpDtoInfoByParams(map);
    }

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据授信额度编号查询资产池协议列表
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public List<CtrAsplDetails> selectByLmtAccNo(String lmtAccNo) {
        return ctrAsplDetailsMapper.selectByLmtAccNo(lmtAccNo);
    }

    /**
     * @方法名称: selectInfoBySerno
     * @方法描述: 根据业务流水号
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CtrAsplDetails selectInfoBySerno(String serno) {
        return ctrAsplDetailsMapper.selectBySerno(serno);
    }

    /**
     * @方法名称: selectInfoByContNo
     * @方法描述: 根据合同编号查询资产池协议
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public CtrAsplDetails selectInfoByContNo(String contNo) {
        return ctrAsplDetailsMapper.selectInfoByContNo(contNo);
    }


    /**
     * @方法名称: deleteByContNo
     * @方法描述: 根据协议编号删除
     * @参数与返回说明:xs
     * @算法描述:
     * @创建人:
     * @创建时间: 2021-08-28 14:44:44
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int deleteByContNo(String contNo) {
        // TODO 这边要校验
        return ctrAsplDetailsMapper.deleteByContNo(contNo);
    }

    public CmisLmt0010ReqDealBizListDto getDealBizList(String contNo) {
        return ctrAsplDetailsMapper.getDealBizList(contNo);
    }

    /**
     * 查询客户下所有的资产池协议编号
     *
     * @param cusId
     * @return
     */
    public List<String> selectContNoListByCusId(String cusId) {
        return ctrAsplDetailsMapper.selectContNoListByCusId(cusId);
    }

    /**
     * @param contNo
     * @函数名称:doComputePoolAmt
     * @函数描述:根据资产池协议查询融资额度
     * @参数与返回说明:
     * @算法描述:
     */
    public ResultDto<Map<String, Object>> doComputePoolAmt(String contNo) {
        Map<String, Object> map = new HashMap<>();
        String code = SuccessEnum.CMIS_SUCCSESS.key;
        String message = "计算成功";
        try {
            CtrAsplDetails ctrAsplDetails = selectCtrAsplDetailsInfoByContNo(contNo);
            // 核心保证金余额
            BigDecimal assetPoolBailAmt = Optional.ofNullable(asplAssetsListService.getAssetPoolBailAmt(ctrAsplDetails)).orElse(BigDecimal.ZERO).setScale(4,BigDecimal.ROUND_DOWN);
            // 资产池融资额度
            BigDecimal assetPoolFinAmt = Optional.ofNullable(asplAssetsListService.getAssetPoolFinAmt(ctrAsplDetails, assetPoolBailAmt)).orElse(BigDecimal.ZERO).setScale(4,BigDecimal.ROUND_DOWN);
            // 可用池融资额度
            BigDecimal assetPoolAvaAmt = Optional.ofNullable(asplAssetsListService.getAssetPoolAvaAmt(ctrAsplDetails, assetPoolFinAmt, assetPoolBailAmt)).orElse(BigDecimal.ZERO).setScale(4,BigDecimal.ROUND_DOWN);
            map.put("assetPoolFinAmt", assetPoolFinAmt);
            map.put("assetPoolAvaAmt", assetPoolAvaAmt);
            map.put("assetPoolBailAmt", assetPoolBailAmt);
        } catch (BizException e) {
            code = "9999";
            message = "融资计算失败:" + e.getMessage();
            throw e;
        } catch (Exception e) {
            code = "9999";
            message = "融资计算失败:" + e.getMessage();
            throw e;
        } finally {
            return new ResultDto<Map<String, Object>>(map).code(code).message(message);
        }
    }

    /**
     * @param
     * @函数名称: getAsplOpenLoan
     * @函数描述: 资产池超短贷开关
     * @参数与返回说明:
     * @算法描述:
     */
    public Boolean getAsplOpenLoan() {
        Boolean isOpen = false;
        try {
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(CmisCommonConstants.ASPL_OPEN_LOAN);
            String ASPL_OPEN_LOAN = adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
            if (Objects.equals("1", ASPL_OPEN_LOAN)) {
                isOpen = true;
            }
        } catch (Exception e) {
            log.info("系统参数【资产池超短贷款开关】【ASPL_OPEN_LOAN】获取失败");
            throw BizException.error(null, "9999", "系统参数【资产池超短贷款开关】【ASPL_OPEN_LOAN】获取失败");
        } finally {
            return isOpen;
        }
    }

    /**
     * @param
     * @函数名称: getAsplOpenAccp
     * @函数描述: 资产池网银开关
     * @参数与返回说明:
     * @算法描述:
     */
    public Boolean getAsplOpenAccp() {
        Boolean isOpen = false;
        try {
            AdminSmPropQueryDto adminSmPropQueryDto = new AdminSmPropQueryDto();
            adminSmPropQueryDto.setPropName(CmisCommonConstants.ASPL_OPEN_ACCP);
            String ASPL_OPEN_ACCP = adminSmPropService.getPropValue(adminSmPropQueryDto).getData().getPropValue();
            if (Objects.equals("1", ASPL_OPEN_ACCP)) {
                isOpen = true;
            }
        } catch (Exception e) {
            log.info("系统参数【资产池超短贷款开关】【ASPL_OPEN_LOAN】获取失败");
            throw BizException.error(null, "9999", "系统参数【资产池超短贷款开关】【ASPL_OPEN_LOAN】获取失败");
        } finally {
            return isOpen;
        }
    }

    /**
     * @param
     * @函数名称: checkAsplBlacks
     * @函数描述: 资产池客户拦截名单校验
     * @参数与返回说明: checkType 1超短贷 0银票  cusId 客户编号
     * @算法描述: true 为校验通过 false 校验不通过
     */
    public Boolean checkAsplBlacks(String bizType, String cusId) {
        Boolean isOpen = true;
        try {
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("bizType", bizType);
            queryModel.addCondition("cusId", cusId);
            queryModel.addCondition("inureStatus", CmisCommonConstants.YES_NO_1);
            List<AsplBlacks> list = asplBlacksService.selectAll(queryModel);
            if (CollectionUtils.nonEmpty(list)) {
                isOpen = false;
            }
        } catch (Exception e) {
            log.info("【资产池客户拦截名单校验】失败");
            isOpen = false;
            throw BizException.error(null, "9999", "资产池客户拦截名单校验】失败");
        } finally {
            return isOpen;
        }
    }

}
