/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.IqpDiffAffirm;
import cn.com.yusys.yusp.repository.mapper.IqpDiffAffirmMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDiffAffirmService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:17:11
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class IqpDiffAffirmService {

    @Autowired
    private IqpDiffAffirmMapper iqpDiffAffirmMapper;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public IqpDiffAffirm selectByPrimaryKey(String iqpSerno) {
        return iqpDiffAffirmMapper.selectByPrimaryKey(iqpSerno);
    }

	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<IqpDiffAffirm> selectAll(QueryModel model) {
        List<IqpDiffAffirm> records = (List<IqpDiffAffirm>) iqpDiffAffirmMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<IqpDiffAffirm> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpDiffAffirm> list = iqpDiffAffirmMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(IqpDiffAffirm record) {
        return iqpDiffAffirmMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpDiffAffirm record) {
        return iqpDiffAffirmMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(IqpDiffAffirm record) {
        return iqpDiffAffirmMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(IqpDiffAffirm record) {
        return iqpDiffAffirmMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String iqpSerno) {
        return iqpDiffAffirmMapper.deleteByPrimaryKey(iqpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return iqpDiffAffirmMapper.deleteByIds(ids);
    }

    public int updateByParams(Map delMap) {
        return iqpDiffAffirmMapper.updateByParams(delMap);
    }

    public List<IqpDiffAffirm> selectByIqpSerno(String iqpSernoOld) {
        return iqpDiffAffirmMapper.selectByIqpSerno(iqpSernoOld);
    }
}
