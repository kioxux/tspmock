package cn.com.yusys.yusp.service.client.bsp.core.dp2021;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2CoreDpClientService;
import cn.com.yusys.yusp.service.Dscms2CoreIbClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author lihh
 * @version 1.0.0
 * @date 2021/6/29 10:33
 * @desc 根据客户号查询客户账号
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Dp2021Service {

    private static final Logger logger = LoggerFactory.getLogger(Dp2021Service.class);
    // 1）注入：BSP封装调用核心系统的接口
    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;

    /**
     * @param dp2021ReqDto
     * @return
     * @author lihh
     * @date 2021/6/22 20:13
     * @version 1.0.0
     * @desc 调用子账户信息新查询接口
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Dp2021RespDto dp2021(Dp2021ReqDto dp2021ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2021.key, EsbEnum.TRADE_CODE_DP2021.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2021.key, EsbEnum.TRADE_CODE_DP2021.value, JSON.toJSONString(dp2021ReqDto));
        ResultDto<Dp2021RespDto> dp2021RespDtoResultDto = dscms2CoreDpClientService.dp2021(dp2021ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2021.key, EsbEnum.TRADE_CODE_DP2021.value, JSON.toJSONString(dp2021RespDtoResultDto));
        String dp2021Code = Optional.ofNullable(dp2021RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String dp2021Meesage = Optional.ofNullable(dp2021RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Dp2021RespDto dp2021RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, dp2021RespDtoResultDto.getCode())) {
            //  获取相关的值并解析
            dp2021RespDto = dp2021RespDtoResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(dp2021Code, dp2021Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2021.key, EsbEnum.TRADE_CODE_DP2021.value);
        return dp2021RespDto;
    }
}
