package cn.com.yusys.yusp.web.server.xdxw0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0016.req.Xdxw0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0016.resp.Xdxw0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0016.Xdxw0016Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:根据客户号查询查询统一管控额度接口（总额度）
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0016:根据客户号查询查询统一管控额度接口（总额度）")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0016Resource.class);

    @Autowired
    private Xdxw0016Service xdxw0016Service;

    /**
     * 交易码：xdxw0016
     * 交易描述：根据客户号查询查询统一管控额度接口（总额度）
     *
     * @param xdxw0016DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询查询统一管控额度接口（总额度）")
    @PostMapping("/xdxw0016")
    protected @ResponseBody
    ResultDto<Xdxw0016DataRespDto> xdxw0016(@Validated @RequestBody Xdxw0016DataReqDto xdxw0016DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, JSON.toJSONString(xdxw0016DataReqDto));
        Xdxw0016DataRespDto xdxw0016DataRespDto = new Xdxw0016DataRespDto();// 响应Dto:根据客户号查询查询统一管控额度接口（总额度）
        ResultDto<Xdxw0016DataRespDto> xdxw0016DataResultDto = new ResultDto<>();
        try {
            // 从xdxw0016DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, JSON.toJSONString(xdxw0016DataReqDto));
            xdxw0016DataRespDto = xdxw0016Service.xdxw0016(xdxw0016DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, JSON.toJSONString(xdxw0016DataRespDto));
            // 封装xdxw0016DataResultDto中正确的返回码和返回信息
            xdxw0016DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0016DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, e.getMessage());
            // 封装xdxw0016DataResultDto中异常返回码和返回信息
            xdxw0016DataResultDto.setCode(e.getErrorCode());
            xdxw0016DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, e.getMessage());
            // 封装xdxw0016DataResultDto中异常返回码和返回信息
            xdxw0016DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0016DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0016DataRespDto到xdxw0016DataResultDto中
        xdxw0016DataResultDto.setData(xdxw0016DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, JSON.toJSONString(xdxw0016DataResultDto));
        return xdxw0016DataResultDto;
    }
}
