/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAccInfo
 * @类描述: coop_plan_acc_info数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-18 16:26:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_plan_acc_info")
public class CoopPlanAccInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 60)
	private String serno;
	
	/** 合作方案编号 **/
	@Column(name = "COOP_PLAN_NO", unique = false, nullable = true, length = 60)
	private String coopPlanNo;
	
	/** 合作方类型 **/
	@Column(name = "PARTNER_TYPE", unique = false, nullable = true, length = 10)
	private String partnerType;
	
	/** 合作方编号 **/
	@Column(name = "PARTNER_NO", unique = false, nullable = true, length = 60)
	private String partnerNo;
	
	/** 合作方名称 **/
	@Column(name = "PARTNER_NAME", unique = false, nullable = true, length = 120)
	private String partnerName;
	
	/** 合作类型 **/
	@Column(name = "COOP_TYPE", unique = false, nullable = true, length = 10)
	private String coopType;
	
	/** 是否全行适用 **/
	@Column(name = "IS_WHOLE_BANK_SUIT", unique = false, nullable = true, length = 10)
	private String isWholeBankSuit;
	
	/** 总合作额度（元） **/
	@Column(name = "TOTL_COOP_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totlCoopLmtAmt;
	
	/** 一般担保额度（元） **/
	@Column(name = "COMMON_GRT_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal commonGrtLmtAmt;
	
	/** 合作期限(月) **/
	@Column(name = "COOP_TERM", unique = false, nullable = true, length = 10)
	private Integer coopTerm;
	
	/** 单户合作限度（元） **/
	@Column(name = "SINGLE_COOP_QUOTA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal singleCoopQuota;
	
	/** 单笔业务合作限额（元） **/
	@Column(name = "SIG_BUSI_COOP_QUOTA", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigBusiCoopQuota;
	
	/** 合作起始日 **/
	@Column(name = "COOP_START_DATE", unique = false, nullable = true, length = 20)
	private String coopStartDate;
	
	/** 合作到期日 **/
	@Column(name = "COOP_END_DATE", unique = false, nullable = true, length = 20)
	private String coopEndDate;
	
	/** 是否白名单控制 **/
	@Column(name = "IS_WHITE_LIST_CTRL", unique = false, nullable = true, length = 10)
	private String isWhiteListCtrl;
	
	/** 代偿宽限期(天) **/
	@Column(name = "SUBPAY_GRAPER", unique = false, nullable = true, length = 10)
	private Integer subpayGraper;
	
	/** 代偿比例 **/
	@Column(name = "SUBPAY_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal subpayPerc;
	
	/** 对外担保放大倍数 **/
	@Column(name = "OUTGUAR_MULTIPLE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outguarMultiple;
	
	/** 其他相关说明 **/
	@Column(name = "OTHER_CORRE_DESC", unique = false, nullable = true, length = 500)
	private String otherCorreDesc;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;
	
	/** 保证金账户最低金额(元) **/
	@Column(name = "BAIL_ACC_LOW_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAccLowAmt;
	
	/** 单笔最低缴存金额(元) **/
	@Column(name = "SIG_LOW_DEPOSIT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigLowDepositAmt;
	
	/** 保证金透支上限(元) **/
	@Column(name = "BAIL_OVERDRAFT_MAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailOverdraftMax;
	
	/** 保证金缴存方式 **/
	@Column(name = "BAIL_DEPOSIT_MODE", unique = false, nullable = true, length = 10)
	private String bailDepositMode;
	
	/** 保证金账号 **/
	@Column(name = "BAIL_ACC_NO", unique = false, nullable = true, length = 60)
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	@Column(name = "BAIL_ACC_NO_SUB_SEQ", unique = false, nullable = true, length = 60)
	private String bailAccNoSubSeq;
	
	/** 调查结论 **/
	@Column(name = "INDGT_RESULT", unique = false, nullable = true, length = 10)
	private String indgtResult;
	
	/** 调查意见 **/
	@Column(name = "INDGT_ADVICE", unique = false, nullable = true, length = 500)
	private String indgtAdvice;
	
	/** 合作方案状态 **/
	@Column(name = "COOP_PLAN_STATUS", unique = false, nullable = true, length = 10)
	private String coopPlanStatus;
	
	/** 影像编号 **/
	@Column(name = "IMAGE_NO", unique = false, nullable = true, length = 80)
	private String imageNo;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 10)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 60)
	private String certCode;
	
	/** 统一社会信用代码 **/
	@Column(name = "UNIFY_CREDIT_CODE", unique = false, nullable = true, length = 60)
	private String unifyCreditCode;
	
	/** 企业性质 **/
	@Column(name = "CORP_CHA", unique = false, nullable = true, length = 10)
	private String corpCha;
	
	/** 成立日期 **/
	@Column(name = "BUILD_DATE", unique = false, nullable = true, length = 20)
	private String buildDate;
	
	/** 注册资本 **/
	@Column(name = "REGI_CAP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal regiCapAmt;
	
	/** 营业期限（月） **/
	@Column(name = "BSINS_TERM", unique = false, nullable = true, length = 10)
	private Integer bsinsTerm;
	
	/** 经营范围 **/
	@Column(name = "OPER_RANGE", unique = false, nullable = true, length = 60)
	private String operRange;
	
	/** 企业规模 **/
	@Column(name = "CORP_SCALE", unique = false, nullable = true, length = 20)
	private String corpScale;
	
	/** 行业分类 **/
	@Column(name = "TRADE_CLASS", unique = false, nullable = true, length = 10)
	private String tradeClass;
	
	/** 法人代表 **/
	@Column(name = "LEGAL", unique = false, nullable = true, length = 30)
	private String legal;
	
	/** 是否集团型客户 **/
	@Column(name = "IS_GRP_CUS", unique = false, nullable = true, length = 10)
	private String isGrpCus;
	
	/** 负责人 **/
	@Column(name = "CHIEF", unique = false, nullable = true, length = 120)
	private String chief;
	
	/** 办公地址 **/
	@Column(name = "OFFICE_ADDR", unique = false, nullable = true, length = 100)
	private String officeAddr;
	
	/** 注册地址 **/
	@Column(name = "REGI_ADDR", unique = false, nullable = true, length = 100)
	private String regiAddr;
	
	/** 住所 **/
	@Column(name = "LIVING_ADDR", unique = false, nullable = true, length = 200)
	private String livingAddr;
	
	/** 是否我行关联方 **/
	@Column(name = "IS_BANK_CORRE", unique = false, nullable = true, length = 10)
	private String isBankCorre;
	
	/** 是否我行风险预警客户 **/
	@Column(name = "IS_BANK_RISK_ALT_CUS", unique = false, nullable = true, length = 10)
	private String isBankRiskAltCus;
	
	/** 合作说明 **/
	@Column(name = "COOP_DESC", unique = false, nullable = true, length = 500)
	private String coopDesc;
	
	/** 调查报告类型 **/
	@Column(name = "INDGT_REPORT_TYPE", unique = false, nullable = true, length = 10)
	private String indgtReportType;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 20)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo;
	}
	
    /**
     * @return coopPlanNo
     */
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerType
	 */
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	
    /**
     * @return partnerType
     */
	public String getPartnerType() {
		return this.partnerType;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	
    /**
     * @return partnerNo
     */
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
    /**
     * @return partnerName
     */
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param coopType
	 */
	public void setCoopType(String coopType) {
		this.coopType = coopType;
	}
	
    /**
     * @return coopType
     */
	public String getCoopType() {
		return this.coopType;
	}
	
	/**
	 * @param isWholeBankSuit
	 */
	public void setIsWholeBankSuit(String isWholeBankSuit) {
		this.isWholeBankSuit = isWholeBankSuit;
	}
	
    /**
     * @return isWholeBankSuit
     */
	public String getIsWholeBankSuit() {
		return this.isWholeBankSuit;
	}
	
	/**
	 * @param totlCoopLmtAmt
	 */
	public void setTotlCoopLmtAmt(java.math.BigDecimal totlCoopLmtAmt) {
		this.totlCoopLmtAmt = totlCoopLmtAmt;
	}
	
    /**
     * @return totlCoopLmtAmt
     */
	public java.math.BigDecimal getTotlCoopLmtAmt() {
		return this.totlCoopLmtAmt;
	}
	
	/**
	 * @param commonGrtLmtAmt
	 */
	public void setCommonGrtLmtAmt(java.math.BigDecimal commonGrtLmtAmt) {
		this.commonGrtLmtAmt = commonGrtLmtAmt;
	}
	
    /**
     * @return commonGrtLmtAmt
     */
	public java.math.BigDecimal getCommonGrtLmtAmt() {
		return this.commonGrtLmtAmt;
	}
	
	/**
	 * @param coopTerm
	 */
	public void setCoopTerm(Integer coopTerm) {
		this.coopTerm = coopTerm;
	}
	
    /**
     * @return coopTerm
     */
	public Integer getCoopTerm() {
		return this.coopTerm;
	}
	
	/**
	 * @param singleCoopQuota
	 */
	public void setSingleCoopQuota(java.math.BigDecimal singleCoopQuota) {
		this.singleCoopQuota = singleCoopQuota;
	}
	
    /**
     * @return singleCoopQuota
     */
	public java.math.BigDecimal getSingleCoopQuota() {
		return this.singleCoopQuota;
	}
	
	/**
	 * @param sigBusiCoopQuota
	 */
	public void setSigBusiCoopQuota(java.math.BigDecimal sigBusiCoopQuota) {
		this.sigBusiCoopQuota = sigBusiCoopQuota;
	}
	
    /**
     * @return sigBusiCoopQuota
     */
	public java.math.BigDecimal getSigBusiCoopQuota() {
		return this.sigBusiCoopQuota;
	}
	
	/**
	 * @param coopStartDate
	 */
	public void setCoopStartDate(String coopStartDate) {
		this.coopStartDate = coopStartDate;
	}
	
    /**
     * @return coopStartDate
     */
	public String getCoopStartDate() {
		return this.coopStartDate;
	}
	
	/**
	 * @param coopEndDate
	 */
	public void setCoopEndDate(String coopEndDate) {
		this.coopEndDate = coopEndDate;
	}
	
    /**
     * @return coopEndDate
     */
	public String getCoopEndDate() {
		return this.coopEndDate;
	}
	
	/**
	 * @param isWhiteListCtrl
	 */
	public void setIsWhiteListCtrl(String isWhiteListCtrl) {
		this.isWhiteListCtrl = isWhiteListCtrl;
	}
	
    /**
     * @return isWhiteListCtrl
     */
	public String getIsWhiteListCtrl() {
		return this.isWhiteListCtrl;
	}
	
	/**
	 * @param subpayGraper
	 */
	public void setSubpayGraper(Integer subpayGraper) {
		this.subpayGraper = subpayGraper;
	}
	
    /**
     * @return subpayGraper
     */
	public Integer getSubpayGraper() {
		return this.subpayGraper;
	}
	
	/**
	 * @param subpayPerc
	 */
	public void setSubpayPerc(java.math.BigDecimal subpayPerc) {
		this.subpayPerc = subpayPerc;
	}
	
    /**
     * @return subpayPerc
     */
	public java.math.BigDecimal getSubpayPerc() {
		return this.subpayPerc;
	}
	
	/**
	 * @param outguarMultiple
	 */
	public void setOutguarMultiple(java.math.BigDecimal outguarMultiple) {
		this.outguarMultiple = outguarMultiple;
	}
	
    /**
     * @return outguarMultiple
     */
	public java.math.BigDecimal getOutguarMultiple() {
		return this.outguarMultiple;
	}
	
	/**
	 * @param otherCorreDesc
	 */
	public void setOtherCorreDesc(String otherCorreDesc) {
		this.otherCorreDesc = otherCorreDesc;
	}
	
    /**
     * @return otherCorreDesc
     */
	public String getOtherCorreDesc() {
		return this.otherCorreDesc;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}
	
	/**
	 * @param bailAccLowAmt
	 */
	public void setBailAccLowAmt(java.math.BigDecimal bailAccLowAmt) {
		this.bailAccLowAmt = bailAccLowAmt;
	}
	
    /**
     * @return bailAccLowAmt
     */
	public java.math.BigDecimal getBailAccLowAmt() {
		return this.bailAccLowAmt;
	}
	
	/**
	 * @param sigLowDepositAmt
	 */
	public void setSigLowDepositAmt(java.math.BigDecimal sigLowDepositAmt) {
		this.sigLowDepositAmt = sigLowDepositAmt;
	}
	
    /**
     * @return sigLowDepositAmt
     */
	public java.math.BigDecimal getSigLowDepositAmt() {
		return this.sigLowDepositAmt;
	}
	
	/**
	 * @param bailOverdraftMax
	 */
	public void setBailOverdraftMax(java.math.BigDecimal bailOverdraftMax) {
		this.bailOverdraftMax = bailOverdraftMax;
	}
	
    /**
     * @return bailOverdraftMax
     */
	public java.math.BigDecimal getBailOverdraftMax() {
		return this.bailOverdraftMax;
	}
	
	/**
	 * @param bailDepositMode
	 */
	public void setBailDepositMode(String bailDepositMode) {
		this.bailDepositMode = bailDepositMode;
	}
	
    /**
     * @return bailDepositMode
     */
	public String getBailDepositMode() {
		return this.bailDepositMode;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo;
	}
	
    /**
     * @return bailAccNo
     */
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSubSeq
	 */
	public void setBailAccNoSubSeq(String bailAccNoSubSeq) {
		this.bailAccNoSubSeq = bailAccNoSubSeq;
	}
	
    /**
     * @return bailAccNoSubSeq
     */
	public String getBailAccNoSubSeq() {
		return this.bailAccNoSubSeq;
	}
	
	/**
	 * @param indgtResult
	 */
	public void setIndgtResult(String indgtResult) {
		this.indgtResult = indgtResult;
	}
	
    /**
     * @return indgtResult
     */
	public String getIndgtResult() {
		return this.indgtResult;
	}
	
	/**
	 * @param indgtAdvice
	 */
	public void setIndgtAdvice(String indgtAdvice) {
		this.indgtAdvice = indgtAdvice;
	}
	
    /**
     * @return indgtAdvice
     */
	public String getIndgtAdvice() {
		return this.indgtAdvice;
	}
	
	/**
	 * @param coopPlanStatus
	 */
	public void setCoopPlanStatus(String coopPlanStatus) {
		this.coopPlanStatus = coopPlanStatus;
	}
	
    /**
     * @return coopPlanStatus
     */
	public String getCoopPlanStatus() {
		return this.coopPlanStatus;
	}
	
	/**
	 * @param imageNo
	 */
	public void setImageNo(String imageNo) {
		this.imageNo = imageNo;
	}
	
    /**
     * @return imageNo
     */
	public String getImageNo() {
		return this.imageNo;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param unifyCreditCode
	 */
	public void setUnifyCreditCode(String unifyCreditCode) {
		this.unifyCreditCode = unifyCreditCode;
	}
	
    /**
     * @return unifyCreditCode
     */
	public String getUnifyCreditCode() {
		return this.unifyCreditCode;
	}
	
	/**
	 * @param corpCha
	 */
	public void setCorpCha(String corpCha) {
		this.corpCha = corpCha;
	}
	
    /**
     * @return corpCha
     */
	public String getCorpCha() {
		return this.corpCha;
	}
	
	/**
	 * @param buildDate
	 */
	public void setBuildDate(String buildDate) {
		this.buildDate = buildDate;
	}
	
    /**
     * @return buildDate
     */
	public String getBuildDate() {
		return this.buildDate;
	}
	
	/**
	 * @param regiCapAmt
	 */
	public void setRegiCapAmt(java.math.BigDecimal regiCapAmt) {
		this.regiCapAmt = regiCapAmt;
	}
	
    /**
     * @return regiCapAmt
     */
	public java.math.BigDecimal getRegiCapAmt() {
		return this.regiCapAmt;
	}
	
	/**
	 * @param bsinsTerm
	 */
	public void setBsinsTerm(Integer bsinsTerm) {
		this.bsinsTerm = bsinsTerm;
	}
	
    /**
     * @return bsinsTerm
     */
	public Integer getBsinsTerm() {
		return this.bsinsTerm;
	}
	
	/**
	 * @param operRange
	 */
	public void setOperRange(String operRange) {
		this.operRange = operRange;
	}
	
    /**
     * @return operRange
     */
	public String getOperRange() {
		return this.operRange;
	}
	
	/**
	 * @param corpScale
	 */
	public void setCorpScale(String corpScale) {
		this.corpScale = corpScale;
	}
	
    /**
     * @return corpScale
     */
	public String getCorpScale() {
		return this.corpScale;
	}
	
	/**
	 * @param tradeClass
	 */
	public void setTradeClass(String tradeClass) {
		this.tradeClass = tradeClass;
	}
	
    /**
     * @return tradeClass
     */
	public String getTradeClass() {
		return this.tradeClass;
	}
	
	/**
	 * @param legal
	 */
	public void setLegal(String legal) {
		this.legal = legal;
	}
	
    /**
     * @return legal
     */
	public String getLegal() {
		return this.legal;
	}
	
	/**
	 * @param isGrpCus
	 */
	public void setIsGrpCus(String isGrpCus) {
		this.isGrpCus = isGrpCus;
	}
	
    /**
     * @return isGrpCus
     */
	public String getIsGrpCus() {
		return this.isGrpCus;
	}
	
	/**
	 * @param chief
	 */
	public void setChief(String chief) {
		this.chief = chief;
	}
	
    /**
     * @return chief
     */
	public String getChief() {
		return this.chief;
	}
	
	/**
	 * @param officeAddr
	 */
	public void setOfficeAddr(String officeAddr) {
		this.officeAddr = officeAddr;
	}
	
    /**
     * @return officeAddr
     */
	public String getOfficeAddr() {
		return this.officeAddr;
	}
	
	/**
	 * @param regiAddr
	 */
	public void setRegiAddr(String regiAddr) {
		this.regiAddr = regiAddr;
	}
	
    /**
     * @return regiAddr
     */
	public String getRegiAddr() {
		return this.regiAddr;
	}
	
	/**
	 * @param livingAddr
	 */
	public void setLivingAddr(String livingAddr) {
		this.livingAddr = livingAddr;
	}
	
    /**
     * @return livingAddr
     */
	public String getLivingAddr() {
		return this.livingAddr;
	}
	
	/**
	 * @param isBankCorre
	 */
	public void setIsBankCorre(String isBankCorre) {
		this.isBankCorre = isBankCorre;
	}
	
    /**
     * @return isBankCorre
     */
	public String getIsBankCorre() {
		return this.isBankCorre;
	}
	
	/**
	 * @param isBankRiskAltCus
	 */
	public void setIsBankRiskAltCus(String isBankRiskAltCus) {
		this.isBankRiskAltCus = isBankRiskAltCus;
	}
	
    /**
     * @return isBankRiskAltCus
     */
	public String getIsBankRiskAltCus() {
		return this.isBankRiskAltCus;
	}
	
	/**
	 * @param coopDesc
	 */
	public void setCoopDesc(String coopDesc) {
		this.coopDesc = coopDesc;
	}
	
    /**
     * @return coopDesc
     */
	public String getCoopDesc() {
		return this.coopDesc;
	}
	
	/**
	 * @param indgtReportType
	 */
	public void setIndgtReportType(String indgtReportType) {
		this.indgtReportType = indgtReportType;
	}
	
    /**
     * @return indgtReportType
     */
	public String getIndgtReportType() {
		return this.indgtReportType;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}