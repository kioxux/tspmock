/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import io.swagger.annotations.ApiOperation;
import cn.com.yusys.yusp.domain.PvpAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CashAdvance;
import cn.com.yusys.yusp.service.CashAdvanceService;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CashAdvanceResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-11-15 15:43:42
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cashadvance")
public class CashAdvanceResource {
    @Autowired
    private CashAdvanceService cashAdvanceService;


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pvpSerno}")
    protected ResultDto<CashAdvance> show(@PathVariable("pvpSerno") String pvpSerno) {
        CashAdvance cashAdvance = cashAdvanceService.selectByPrimaryKey(pvpSerno);
        return ResultDto.success(cashAdvance);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @创建人:
     */
    @ApiOperation("垫款出账申请查看")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        CashAdvance cashAdvance = cashAdvanceService.selectByPrimaryKey((String)params.get(("pvpSerno")));
        if (cashAdvance != null) {
            resultDto.setCode(200);
            resultDto.setData(cashAdvance);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CashAdvance cashAdvance) {
        int result = cashAdvanceService.updateSelective(cashAdvance);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:saveCashAdvance
     * @函数描述:垫款出账申请新增
     * @参数与返回说明:cashAdvance
     * @创建人:hh
     */
    @ApiOperation("垫款出账申请新增")
    @PostMapping("/saveCashAdvance")
    protected ResultDto<Map> saveCashAdvance(@RequestBody CashAdvance cashAdvance) throws URISyntaxException {
        ResultDto<PvpLoanApp> resultDto = new ResultDto<PvpLoanApp>();
        Map result = cashAdvanceService.saveCashAdvance(cashAdvance);
        return  new ResultDto<>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pvpSerno}")
    protected ResultDto<Integer> delete(@PathVariable("pvpSerno") String pvpSerno) {
        int result = cashAdvanceService.deleteByPrimaryKey(pvpSerno);
        return ResultDto.success(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = cashAdvanceService.deleteByIds(ids);
        return ResultDto.success(result);
    }


    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryCashAdvanceSendCoreList")
    protected ResultDto<List<CashAdvance>> queryCashAdvanceSendCoreList(@RequestBody QueryModel queryModel) {
        List<CashAdvance> list = cashAdvanceService.selectByModel(queryModel);
        return new ResultDto<List<CashAdvance>>(list);
    }

    /**
     * @函数名称:sendcore
     * @函数描述:发送核心进行出账
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/sendCashAdvanceToCore/{pvpSerno}")
    protected  ResultDto sendCore(@PathVariable("pvpSerno") String pvpSerno) {
        ResultDto resultDto = cashAdvanceService.sendCashAdvanceToCore(pvpSerno);
        return resultDto;
    }
}
