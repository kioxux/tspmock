package cn.com.yusys.yusp.service.server.xdcz0020;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CfgGenerateTempFileDto;
import cn.com.yusys.yusp.dto.FrptPdfArgsDto;
import cn.com.yusys.yusp.dto.server.xdcz0020.req.Xdcz0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0020.resp.Xdcz0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.DscmsCfgClientService;
import cn.com.yusys.yusp.service.ICusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * 接口处理类:小贷用途承诺书文本生成pdf
 *
 * @author zoubiao
 * @version 1.0
 */
@Service
public class Xdcz0020Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0020Service.class);

    @Resource
    private ICusClientService icusClientService;
    @Resource
    private DscmsCfgClientService dscmsCfgClientService;

    /**
     * 交易描述：小贷用途承诺书文本生成pdf
     *
     * @param xdcz0020DataReqDto
     * @return
     * @throws Exception
     */
    @Transactional
    public Xdcz0020DataRespDto getXdcz0020(Xdcz0020DataReqDto xdcz0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020DataReqDto));
        //返回对象
        Xdcz0020DataRespDto xdcz0020DataRespDto = new Xdcz0020DataRespDto();
        try {
            //*****************************************获取传参*****************************************
            String cusName = xdcz0020DataReqDto.getCusName();//客户名称
            String certCode = xdcz0020DataReqDto.getCertCode();//身份证件号码
            String loanUseType = xdcz0020DataReqDto.getLoanUseType();//借款用途
            String applyDate = xdcz0020DataReqDto.getApplyDate();//申请日期
            String dkType = xdcz0020DataReqDto.getDkType();//产品类型

            //*****************************************请求参数必输校验*****************************************
            if (StringUtils.isBlank(cusName)) {
                throw new Exception("客户名称不能为空！");
            } else if (StringUtils.isBlank(applyDate)) {
                throw new Exception("申请日期不能为空！");
            }

            String certCodeNo = certCode.substring(certCode.length() - 4);
            String TempleteName = "xddkytcns.cpt";// 合同模板名字 xddkytcns.docx
            String saveFileName = "xdybjkht_" + certCodeNo + "";//用途承诺书另保存名字 （模板名字+身份证号码）

            //*****************************************查询服务器配置*****************************************
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("pkId", "00001");
            ResultDto<List<CfgGenerateTempFileDto>> listResultDto = dscmsCfgClientService.queryCfgFile(queryModel);
            List<CfgGenerateTempFileDto> dtoList = listResultDto.getData();
            CfgGenerateTempFileDto cfgGenerateTempFileDto = dtoList.get(0);

            //*****************************************查询存储地址信息*****************************************
            String ip = cfgGenerateTempFileDto.getLoginIp();
            String port = cfgGenerateTempFileDto.getLoginPort();
            String username = cfgGenerateTempFileDto.getLoginUsername();
            String password = cfgGenerateTempFileDto.getLoginPwd();
            String path = cfgGenerateTempFileDto.getFilePath();
            String url = cfgGenerateTempFileDto.getMemo();

            //*****************************************传入帆软报表生成需要的参数*****************************************
            HashMap<String, Object> parameterMap = new HashMap<String, Object>();
            parameterMap.put("cusName", cusName);
            parameterMap.put("year", applyDate);
            /***********************贷款用途逻辑处理**********************/
            String type = "";
            if ("YDD".equals(dkType) || "YQD".equals(dkType) || "YND".equals(dkType) || "HXD".equals(dkType)) {
                loanUseType = "经营类贷款";
                type = "1";
            } else if ("YXD".equals(dkType)) {
                loanUseType = "消费类贷款";
                type = "2";
            } else {
                loanUseType = "其他类贷款";
                type = "3";
            }
            parameterMap.put("loanUseType", loanUseType);
            parameterMap.put("type", type);
            //*****************************************调用公共方法生成pdf*****************************************
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            if (StringUtils.isEmpty(url)) {
                // url = "http://10.28.206.191:8090/dscms/frpt/api/frpt/createFrptPdf";//本地测试地址
            }
            try {
                FrptPdfArgsDto frptPdfArgsDto = new FrptPdfArgsDto();
                frptPdfArgsDto.setPdfFileName(TempleteName);//模板名称
                frptPdfArgsDto.setNewFileName(saveFileName);//待生成的PDF文件名称
                frptPdfArgsDto.setSerno(certCodeNo);//设置流水
                frptPdfArgsDto.setPath(path);//路径
                frptPdfArgsDto.setIp(ip);//IP地址
                frptPdfArgsDto.setPort(Integer.parseInt(port));//端口
                frptPdfArgsDto.setUserName(username);
                frptPdfArgsDto.setPassWord(password);
                frptPdfArgsDto.setMap(parameterMap);
                HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(frptPdfArgsDto), headers);
                ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, entity, String.class);
                String code = responseEntity.getBody();
            } catch (Exception e) {
                logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            }
            //*****************************************返回参数*****************************************
            //String pdfDepoAddr = saveFileName + "#" + ip + "#" + port + "#" + username + "#" + password + "#" + path;
            String pdfFileName = saveFileName + ".pdf";
            xdcz0020DataRespDto.setPdfDepoAddr(path);
            xdcz0020DataRespDto.setPdfFileName(pdfFileName);
            xdcz0020DataRespDto.setFtpAddr(ip);
            xdcz0020DataRespDto.setPort(port);
            xdcz0020DataRespDto.setUserName(username);
            xdcz0020DataRespDto.setPassword(password);
        } catch (Exception e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020DataRespDto));
        return xdcz0020DataRespDto;
    }
}
