/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.RepayBillRellDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.PvpLoanAppRepayBillRell;
import cn.com.yusys.yusp.service.PvpLoanAppRepayBillRellService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: PvpLoanAppRepayBillRellResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-15 14:34:25
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "偿还借据")
@RestController
@RequestMapping("/api/pvploanapprepaybillrell")
public class PvpLoanAppRepayBillRellResource {
    @Autowired
    private PvpLoanAppRepayBillRellService pvpLoanAppRepayBillRellService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<PvpLoanAppRepayBillRell>> query() {
        QueryModel queryModel = new QueryModel();
        List<PvpLoanAppRepayBillRell> list = pvpLoanAppRepayBillRellService.selectAll(queryModel);
        return new ResultDto<List<PvpLoanAppRepayBillRell>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<PvpLoanAppRepayBillRell>> index(QueryModel queryModel) {
        List<PvpLoanAppRepayBillRell> list = pvpLoanAppRepayBillRellService.selectByModel(queryModel);
        return new ResultDto<List<PvpLoanAppRepayBillRell>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<PvpLoanAppRepayBillRell> create(@RequestBody PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell) throws URISyntaxException {
        pvpLoanAppRepayBillRellService.insert(pvpLoanAppRepayBillRell);
        return new ResultDto<PvpLoanAppRepayBillRell>(pvpLoanAppRepayBillRell);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody PvpLoanAppRepayBillRell pvpLoanAppRepayBillRell) throws URISyntaxException {
        int result = pvpLoanAppRepayBillRellService.update(pvpLoanAppRepayBillRell);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String serno) {
        int result = pvpLoanAppRepayBillRellService.deleteByPrimaryKey(pkId, serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteRepaybillrell
     * @函数描述:根据借据号删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleterepaybillrell/{billNo}")
    protected ResultDto<Integer> deleteRepaybillrell(@PathVariable("billNo") String billNo) {
        int result = pvpLoanAppRepayBillRellService.deleteByBillNo(billNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:queryRepayBillRell
     * @函数描述:偿还借据分页查询
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("偿还借据分页查询")
    @PostMapping("/queryRepayBillRell")
    protected ResultDto<List<RepayBillRellDto>> queryRepayBillRell(@RequestBody QueryModel queryModel) {
        List<RepayBillRellDto> list = pvpLoanAppRepayBillRellService.queryRepayBillRell(queryModel);
        return new ResultDto<List<RepayBillRellDto>>(list);
    }

    /**
     * @函数名称:queryRepayBillRell
     * @函数描述:偿还借据分页查询
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("小微偿还借据分页查询")
    @PostMapping("/queryRepayBillRellForXw")
    protected ResultDto<List<RepayBillRellDto>> queryRepayBillRellForXw(@RequestBody QueryModel queryModel) {
        List<RepayBillRellDto> list = pvpLoanAppRepayBillRellService.queryRepayBillRellForXw(queryModel);
        return new ResultDto<List<RepayBillRellDto>>(list);
    }

    @ApiOperation("偿还借据引入贷款台账")
    @PostMapping("/leadInAccLoan")
    protected ResultDto<Object> leadInAccLoan(@RequestBody Map params) throws URISyntaxException {
        HashMap queryMap = new HashMap<String, String>();
        queryMap.put("billNo",params.get("billNo"));
        queryMap.put("pvpSerno",params.get("pvpSerno"));
        Map result = pvpLoanAppRepayBillRellService.leadInAccLoan(queryMap);
        return new ResultDto<Object>(result);
    }

    /**
     * @函数名称:deleteRepaybillrellAndCalculateResult
     * @函数描述:根据借据号删除偿还借据信息和关联的测算信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleterepaybillrellandcalculateresult")
    protected ResultDto<Integer> deleteRepaybillrellAndCalculateResult(@RequestBody Map map) {
        int result = pvpLoanAppRepayBillRellService.deleteRepaybillrellAndCalculateResult(map);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:selectByModel
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<PvpLoanAppRepayBillRell>> selectByModel(@RequestBody QueryModel queryModel) {
        List<PvpLoanAppRepayBillRell> list = pvpLoanAppRepayBillRellService.selectByModel(queryModel);
        return new ResultDto<List<PvpLoanAppRepayBillRell>>(list);
    }
}
