package cn.com.yusys.yusp.web.server.xdxw0025;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0025.req.Xdxw0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0025.resp.Xdxw0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0025.Xdxw0025Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:借据下抵押物信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0025:借据下抵押物信息列表查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0025Resource.class);

    @Autowired
    private Xdxw0025Service xdxw0025Service;

    /**
     * 交易码：xdxw0025
     * 交易描述：借据下抵押物信息列表查询
     *
     * @param xdxw0025DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借据下抵押物信息列表查询")
    @PostMapping("/xdxw0025")
    protected @ResponseBody
    ResultDto<Xdxw0025DataRespDto> xdxw0025(@Validated @RequestBody Xdxw0025DataReqDto xdxw0025DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, JSON.toJSONString(xdxw0025DataReqDto));
        Xdxw0025DataRespDto xdxw0025DataRespDto = new Xdxw0025DataRespDto();// 响应Dto:借据下抵押物信息列表查询
        ResultDto<Xdxw0025DataRespDto> xdxw0025DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, JSON.toJSONString(xdxw0025DataReqDto));
            xdxw0025DataRespDto = xdxw0025Service.getXdxw0025(xdxw0025DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, JSON.toJSONString(xdxw0025DataRespDto));

            // 封装xdxw0025DataResultDto中正确的返回码和返回信息
            xdxw0025DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0025DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, e.getMessage());
            // 封装xdxw0025DataResultDto中异常返回码和返回信息
            xdxw0025DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0025DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0025DataRespDto到xdxw0025DataResultDto中
        xdxw0025DataResultDto.setData(xdxw0025DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, JSON.toJSONString(xdxw0025DataResultDto));
        return xdxw0025DataResultDto;
    }
}
