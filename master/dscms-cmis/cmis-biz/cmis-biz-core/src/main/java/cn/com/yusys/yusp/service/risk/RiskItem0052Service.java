package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpDisAssetConInfo;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import cn.com.yusys.yusp.service.IqpDisAssetConInfoService;
import cn.com.yusys.yusp.service.IqpHouseService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import cn.com.yusys.yusp.util.IDCardUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0052Service
 * @类描述: 还款能力分析校验
 * @功能描述: 还款能力分析校验
 * @创建人: hubp
 * @创建时间: 2021年7月30日15:23:06
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0052Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0052Service.class);

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private RiskItem0049Service riskItem0049Service;

    @Autowired
    private IqpHouseService iqpHouseService;

    /**
     * @方法名称: riskItem0052
     * @方法描述:  商住房贷款年龄期限校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: hubp
     * @创建时间: 2021年7月30日15:23:06
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0052(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        log.info("*************商住房贷款年龄期限校验开始***********流水号：【{}】", iqpSerno);
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
        if(!Objects.isNull(iqpLoanApp) && ("022020".equals(iqpLoanApp.getPrdId()) || "022021".equals(iqpLoanApp.getPrdId()))) {
            riskResultDto = riskItem0052(iqpLoanApp);
            if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                return riskResultDto;
            }
        } else if ("022031".equals(iqpLoanApp.getPrdId())){
            // 如果为拍卖贷。且房屋类型为商业用房，也进行校验
            IqpHouse iqpHouse = iqpHouseService.selectByIqpSernos(iqpSerno);
            if(Objects.isNull(iqpHouse) || StringUtils.isBlank(iqpHouse.getHouseType())){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("房屋信息不能为空！");
                return riskResultDto;
            }
            // 05 为商业用房
            if("05".equals(iqpHouse.getHouseType())){
                riskResultDto = riskItem0052(iqpLoanApp);
                if(CmisRiskConstants.RISK_RESULT_TYPE_1.equals(riskResultDto.getRiskResultType())){
                    return riskResultDto;
                }
            }
        }
        log.info("*************商住房贷款年龄期限校验结束***********流水号：【{}】", iqpSerno);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }

    /**
     * @param iqpLoanApp
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/9/23 16:27
     * @version 1.0.0
     * @desc   内部方法，专用于判断借款人年龄加贷款期限
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private RiskResultDto riskItem0052(IqpLoanApp iqpLoanApp){
        RiskResultDto riskResultDto = new RiskResultDto();
        // 先判断借款人性别  --->性别(M - 男 ， F - 女 ， N - 未知)
        BigDecimal certCodeMonth = new BigDecimal(riskItem0049Service.handleAgeMonth(iqpLoanApp.getCertCode())); // 年龄（月）
        if ("M".equals(IDCardUtils.getGenderByIdCard(iqpLoanApp.getCertCode()))) {
            log.info("客户性别为男；客户年龄（月）【{}】：，申请期限：【{}】", certCodeMonth.toPlainString(),iqpLoanApp.getAppTerm().toPlainString());
            if (certCodeMonth.add(iqpLoanApp.getAppTerm()).compareTo(BigDecimal.valueOf(60 * 12.00)) > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("借款人年龄加贷款期限必须小于或等于60周岁！"); //借款人年龄必须小于或等于60周岁！
                return riskResultDto;
            }
        } else {
            log.info("客户性别为女或未知；客户年龄（月）【{}】：，申请期限：【{}】", certCodeMonth.toPlainString(),iqpLoanApp.getAppTerm().toPlainString());
            if (certCodeMonth.add(iqpLoanApp.getAppTerm()).compareTo(BigDecimal.valueOf(55 * 12.00)) > 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc("借款人年龄加贷款期限必须小于或等于55周岁！"); //借款人年龄必须小于或等于55周岁！
                return riskResultDto;
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
