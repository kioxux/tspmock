/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LoanEdCheck;
import cn.com.yusys.yusp.service.LoanEdCheckService;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LoanEdCheckResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-04 21:31:33
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/loanedcheck")
public class LoanEdCheckResource {
    @Autowired
    private LoanEdCheckService loanEdCheckService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LoanEdCheck>> query() {
        QueryModel queryModel = new QueryModel();
        List<LoanEdCheck> list = loanEdCheckService.selectAll(queryModel);
        return new ResultDto<List<LoanEdCheck>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LoanEdCheck>> index(QueryModel queryModel) {
        List<LoanEdCheck> list = loanEdCheckService.selectByModel(queryModel);
        return new ResultDto<List<LoanEdCheck>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LoanEdCheck> show(@PathVariable("serno") String serno) {
        LoanEdCheck loanEdCheck = loanEdCheckService.selectByPrimaryKey(serno);
        return new ResultDto<LoanEdCheck>(loanEdCheck);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LoanEdCheck> create(@RequestBody LoanEdCheck loanEdCheck) throws URISyntaxException {
        loanEdCheckService.insert(loanEdCheck);
        return new ResultDto<LoanEdCheck>(loanEdCheck);
    }

    /**
     * @函数名称:loanEdChecklist
     * @函数描述: 获取房抵e点贷受托信息审核
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取房抵e点贷受托信息审核")
    @PostMapping("/loanEdChecklist")
    protected ResultDto<List<LoanEdCheck>> loanEdChecklist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LoanEdCheck> list = loanEdCheckService.loanEdChecklist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<LoanEdCheck>>(list);
    }

    /**
     * @函数名称:riskXdGuarantyHislist
     * @函数描述: 获取申请历史押品查询查封
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("获取房抵e点贷受托信息审核历史")
    @PostMapping("/loanEdCheckHislist")
    protected ResultDto<List<LoanEdCheck>> riskXdGuarantyHislist(@RequestBody QueryModel queryModel) {
        queryModel.setSort("serno desc");
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<LoanEdCheck> list = loanEdCheckService.loanEdCheckHislist(queryModel);
        PageHelper.clearPage();
        return new ResultDto<List<LoanEdCheck>>(list);
    }

    /**
     * @函数名称:showDetial
     * @函数描述:通过面签流水号查询企业详情
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过查询查封流水号查询查封详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showDetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        LoanEdCheck loanEdCheck = loanEdCheckService.selectBySerno((String) params.get("serno"));
        if (loanEdCheck != null) {
            resultDto.setCode(200);
            resultDto.setData(loanEdCheck);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }


    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LoanEdCheck loanEdCheck) throws URISyntaxException {
        int result = loanEdCheckService.update(loanEdCheck);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = loanEdCheckService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = loanEdCheckService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:updateLoanedcheck
     * @函数描述:房抵e点贷受托支付受托信息审核 调用fb1146
     * @创建者 zhangliang15
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("房抵e点贷受托支付受托信息审核")
    @PostMapping("/updateLoanedcheck")
    protected ResultDto<String> updateLoanedcheck(@RequestBody  LoanEdCheck loanEdCheck) {
        return  loanEdCheckService.updateLoanedcheck(loanEdCheck);
    }
}
