package cn.com.yusys.yusp.web.server.xdca0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdca0004.req.Xdca0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0004.resp.Xdca0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.server.xdca0004.Xdca0004Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.github.pagehelper.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:大额分期合同详情查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0004:大额分期合同详情查询")
@RestController
@RequestMapping("/api/bizca4bsp")
public class BizXdca0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdca0004Resource.class);

    @Autowired
    private Xdca0004Service xdca0004Service;

    /**
     * 交易码：xdca0004
     * 交易描述：大额分期合同详情查询
     *
     * @param xdca0004DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大额分期合同详情查询")
    @PostMapping("/xdca0004")
    protected @ResponseBody
    ResultDto<Xdca0004DataRespDto> xdca0004(@Validated @RequestBody Xdca0004DataReqDto xdca0004DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, JSON.toJSONString(xdca0004DataReqDto));
        Xdca0004DataRespDto xdca0004DataRespDto = new Xdca0004DataRespDto();// 响应Dto:大额分期合同详情查询
        ResultDto<Xdca0004DataRespDto> xdca0004DataResultDto = new ResultDto<>();
        try {
            //请求报文
            String contNo = xdca0004DataReqDto.getContNo();//合同编号

            if (StringUtil.isNotEmpty(contNo)) {
                // 从xdca0004DataReqDto获取业务值进行业务逻辑处理
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, JSON.toJSONString(xdca0004DataReqDto));
                xdca0004DataRespDto = xdca0004Service.xdca0004(xdca0004DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, JSON.toJSONString(xdca0004DataRespDto));
                // 封装xdca0004DataResultDto中正确的返回码和返回信息
                xdca0004DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdca0004DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
                // 请求字段为空
                xdca0004DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdca0004DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, e.getMessage());
            // 封装xdca0004DataResultDto中异常返回码和返回信息
            //  EcsEnum.ECS049999 待调整 开始
            xdca0004DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdca0004DataResultDto.setMessage(EpbEnum.EPB099999.value);
            //  EcsEnum.ECS049999 待调整  结束
        }
        // 封装xdca0004DataRespDto到xdca0004DataResultDto中
        xdca0004DataResultDto.setData(xdca0004DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, JSON.toJSONString(xdca0004DataRespDto));
        return xdca0004DataResultDto;
    }
}
