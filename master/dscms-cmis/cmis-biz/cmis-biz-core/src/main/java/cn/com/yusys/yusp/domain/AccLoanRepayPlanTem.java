/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccLoanRepayPlanTem
 * @类描述: acc_loan_repay_plan_tem数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-06 23:07:57
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_loan_repay_plan_tem")
public class AccLoanRepayPlanTem extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = true, length = 40)
	private String billNo;
	
	/** 期号 **/
	@Column(name = "DATENO", unique = false, nullable = true, length = 20)
	private String dateno;
	
	/** 到期日 **/
	@Column(name = "ENDDATE", unique = false, nullable = true, length = 20)
	private String enddate;
	
	/** 期供金额 **/
	@Column(name = "INSTM_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal instmAmt;
	
	/** 本金 **/
	@Column(name = "CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cap;
	
	/** 利息 **/
	@Column(name = "INTEREST", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal interest;
	
	/** 罚息 **/
	@Column(name = "ODINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal odint;
	
	/** 复利 **/
	@Column(name = "CI", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ci;
	
	/** 剩余本金 **/
	@Column(name = "SURPLUS_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal surplusCap;
	
	/** 已还本金 **/
	@Column(name = "HASBC_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal hasbcCap;
	
	/** 已还利息 **/
	@Column(name = "HASBC_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal hasbcInt;
	
	/** 已还罚息 **/
	@Column(name = "HASBC_ODINT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal hasbcOdint;
	
	/** 已还复利 **/
	@Column(name = "HASBC_CI", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal hasbcCi;
	
	/** 本期状态 **/
	@Column(name = "STATUS", unique = false, nullable = true, length = 20)
	private String status;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param dateno
	 */
	public void setDateno(String dateno) {
		this.dateno = dateno;
	}
	
    /**
     * @return dateno
     */
	public String getDateno() {
		return this.dateno;
	}
	
	/**
	 * @param enddate
	 */
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	
    /**
     * @return enddate
     */
	public String getEnddate() {
		return this.enddate;
	}
	
	/**
	 * @param instmAmt
	 */
	public void setInstmAmt(java.math.BigDecimal instmAmt) {
		this.instmAmt = instmAmt;
	}
	
    /**
     * @return instmAmt
     */
	public java.math.BigDecimal getInstmAmt() {
		return this.instmAmt;
	}
	
	/**
	 * @param cap
	 */
	public void setCap(java.math.BigDecimal cap) {
		this.cap = cap;
	}
	
    /**
     * @return cap
     */
	public java.math.BigDecimal getCap() {
		return this.cap;
	}
	
	/**
	 * @param interest
	 */
	public void setInterest(java.math.BigDecimal interest) {
		this.interest = interest;
	}
	
    /**
     * @return interest
     */
	public java.math.BigDecimal getInterest() {
		return this.interest;
	}
	
	/**
	 * @param odint
	 */
	public void setOdint(java.math.BigDecimal odint) {
		this.odint = odint;
	}
	
    /**
     * @return odint
     */
	public java.math.BigDecimal getOdint() {
		return this.odint;
	}
	
	/**
	 * @param ci
	 */
	public void setCi(java.math.BigDecimal ci) {
		this.ci = ci;
	}
	
    /**
     * @return ci
     */
	public java.math.BigDecimal getCi() {
		return this.ci;
	}
	
	/**
	 * @param surplusCap
	 */
	public void setSurplusCap(java.math.BigDecimal surplusCap) {
		this.surplusCap = surplusCap;
	}
	
    /**
     * @return surplusCap
     */
	public java.math.BigDecimal getSurplusCap() {
		return this.surplusCap;
	}
	
	/**
	 * @param hasbcCap
	 */
	public void setHasbcCap(java.math.BigDecimal hasbcCap) {
		this.hasbcCap = hasbcCap;
	}
	
    /**
     * @return hasbcCap
     */
	public java.math.BigDecimal getHasbcCap() {
		return this.hasbcCap;
	}
	
	/**
	 * @param hasbcInt
	 */
	public void setHasbcInt(java.math.BigDecimal hasbcInt) {
		this.hasbcInt = hasbcInt;
	}
	
    /**
     * @return hasbcInt
     */
	public java.math.BigDecimal getHasbcInt() {
		return this.hasbcInt;
	}
	
	/**
	 * @param hasbcOdint
	 */
	public void setHasbcOdint(java.math.BigDecimal hasbcOdint) {
		this.hasbcOdint = hasbcOdint;
	}
	
    /**
     * @return hasbcOdint
     */
	public java.math.BigDecimal getHasbcOdint() {
		return this.hasbcOdint;
	}
	
	/**
	 * @param hasbcCi
	 */
	public void setHasbcCi(java.math.BigDecimal hasbcCi) {
		this.hasbcCi = hasbcCi;
	}
	
    /**
     * @return hasbcCi
     */
	public java.math.BigDecimal getHasbcCi() {
		return this.hasbcCi;
	}
	
	/**
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
    /**
     * @return status
     */
	public String getStatus() {
		return this.status;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}