/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: ToppAcctSub
 * @类描述: TOPP_ACCT_SUB数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-17 15:12:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "TOPP_ACCT_SUB")
public class ToppAcctSub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务主键号 **/
	@Column(name = "BIZ_SERNO", unique = false, nullable = false, length = 40)
	private String bizSerno;
	
	/** 业务场景 **/
	@Column(name = "BIZ_SENCE", unique = false, nullable = true, length = 5)
	private String bizSence;
	
	/** 是否线上 **/
	@Column(name = "IS_ONLINE", unique = false, nullable = true, length = 5)
	private String isOnline;
	
	/** 是否本行账户 **/
	@Column(name = "IS_BANK_ACCT", unique = false, nullable = true, length = 5)
	private String isBankAcct;
	
	/** 交易对手账号 **/
	@Column(name = "TOPP_ACCT_NO", unique = false, nullable = false, length = 40)
	private String toppAcctNo;
	
	/** 交易对手名称 **/
	@Column(name = "TOPP_NAME", unique = false, nullable = false, length = 80)
	private String toppName;
	
	/** 交易对手金额 **/
	@Column(name = "TOPP_AMT", unique = false, nullable = false, length = 16)
	private java.math.BigDecimal toppAmt;
	
	/** 开户行行号 **/
	@Column(name = "ACCTSVCR_NO", unique = false, nullable = true, length = 40)
	private String acctsvcrNo;
	
	/** 开户行名称 **/
	@Column(name = "ACCTSVCR_NAME", unique = false, nullable = true, length = 80)
	private String acctsvcrName;
	
	/** 操作类型   STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = false, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = false, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = false, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = false, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = false, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = false, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno;
	}
	
    /**
     * @return bizSerno
     */
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param bizSence
	 */
	public void setBizSence(String bizSence) {
		this.bizSence = bizSence;
	}
	
    /**
     * @return bizSence
     */
	public String getBizSence() {
		return this.bizSence;
	}
	
	/**
	 * @param isOnline
	 */
	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}
	
    /**
     * @return isOnline
     */
	public String getIsOnline() {
		return this.isOnline;
	}
	
	/**
	 * @param isBankAcct
	 */
	public void setIsBankAcct(String isBankAcct) {
		this.isBankAcct = isBankAcct;
	}
	
    /**
     * @return isBankAcct
     */
	public String getIsBankAcct() {
		return this.isBankAcct;
	}
	
	/**
	 * @param toppAcctNo
	 */
	public void setToppAcctNo(String toppAcctNo) {
		this.toppAcctNo = toppAcctNo;
	}
	
    /**
     * @return toppAcctNo
     */
	public String getToppAcctNo() {
		return this.toppAcctNo;
	}
	
	/**
	 * @param toppName
	 */
	public void setToppName(String toppName) {
		this.toppName = toppName;
	}
	
    /**
     * @return toppName
     */
	public String getToppName() {
		return this.toppName;
	}
	
	/**
	 * @param toppAmt
	 */
	public void setToppAmt(java.math.BigDecimal toppAmt) {
		this.toppAmt = toppAmt;
	}
	
    /**
     * @return toppAmt
     */
	public java.math.BigDecimal getToppAmt() {
		return this.toppAmt;
	}
	
	/**
	 * @param acctsvcrNo
	 */
	public void setAcctsvcrNo(String acctsvcrNo) {
		this.acctsvcrNo = acctsvcrNo;
	}
	
    /**
     * @return acctsvcrNo
     */
	public String getAcctsvcrNo() {
		return this.acctsvcrNo;
	}
	
	/**
	 * @param acctsvcrName
	 */
	public void setAcctsvcrName(String acctsvcrName) {
		this.acctsvcrName = acctsvcrName;
	}
	
    /**
     * @return acctsvcrName
     */
	public String getAcctsvcrName() {
		return this.acctsvcrName;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}