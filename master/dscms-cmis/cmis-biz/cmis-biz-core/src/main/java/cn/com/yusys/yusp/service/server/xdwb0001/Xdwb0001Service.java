package cn.com.yusys.yusp.service.server.xdwb0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.server.xdwb0001.req.Xdwb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdwb0001.resp.Xdwb0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.*;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务逻辑类:省联社金融服务平台借据信息查询接口
 *
 * @author zcrbank-fengjj
 * @version 1.0
 */
@Service
public class Xdwb0001Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdwb0001Service.class);

    @Resource
    private AccLoanMapper accLoanMapper;
    @Resource
    private LmtReplyAccSubPrdMapper lmtReplyAccSubPrdMapper;
    @Resource
    private LmtReplyAccSubMapper lmtReplyAccSubMapper;
    @Resource
    private LmtReplyAccMapper lmtReplyAccMapper;
    @Resource
    private GrtGuarContMapper grtGuarContMapper;
    @Resource
    private GuarGuaranteeMapper guarGuaranteeMapper;


    /**
     * 交易码：xdwb0001
     * 交易描述：省联社金融服务平台借据信息查询接口
     *
     * @param xdwb0001DataReqDto
     * @return
     */
    @Transactional
    public Xdwb0001DataRespDto xdwb0001(Xdwb0001DataReqDto xdwb0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001DataReqDto));
        Xdwb0001DataRespDto xdwb0001DataRespDto = new Xdwb0001DataRespDto();

        String billNo = xdwb0001DataReqDto.getBill_no();//借据编号
        AccLoan accLoan = accLoanMapper.queryAccLoanDataByBillNo(billNo);
        if(accLoan == null){
            throw BizException.error("",EpbEnum.EPB099999.key,"借据【" + billNo + "】不存在！");
        }
        String cusId = accLoan.getCusId();
        String prdName = accLoan.getPrdName();
        String prdId = accLoan.getPrdId();
        String loanStartDate = accLoan.getLoanStartDate();
        String loanEndDate = accLoan.getLoanEndDate();
        BigDecimal loanAmt = accLoan.getLoanAmt();
        BigDecimal execRateYear = accLoan.getExecRateYear();
        /**
         * 00	信用
         * 10	抵押
         * 20	质押
         * 21	低风险质押
         * 30	保证
         * 40	全额保证金
         * 60	低风险
         */
        String guarMode = accLoan.getGuarMode();
        String loanTerm = accLoan.getLoanTerm();
        String contNo = accLoan.getContNo();
        /**
         * 0	已关闭
         * 1	正常
         * 2	逾期
         * 3	呆滞
         * 4	呆账
         * 5	已核销
         * 6	未出账
         * 7	作废
         */
        String accStatus = accLoan.getAccStatus();
        /**
         * 五級分類
         * 正常	10
         * 关注	20
         * 次级	30
         * 可疑	40
         * 损失	50
         */
        String fiveClass = accLoan.getFiveClass();
        /**
         * 发放形式
         * 1	新增
         * 6	无还本续贷
         * 3	借新还旧
         * 4	无缝对接
         * 8	小企业无还本续贷
         */
        String loanModel = accLoan.getLoanModal();

        String accSubPrdNo = accLoan.getLmtAccNo();
        String accNo = lmtReplyAccSubPrdMapper.getAccNoByAccSubPrdNo(accSubPrdNo);
        LmtReplyAcc lmtReplyAcc = lmtReplyAccMapper.selectByAccNo(accNo);
        String lmtStartDate = StringUtils.EMPTY;
        String lmtEndDate = StringUtils.EMPTY;
        BigDecimal lmtAmt = null;
        if(lmtReplyAcc != null){
            lmtStartDate = lmtReplyAcc.getInureDate();
            Integer lmtTerm = lmtReplyAcc.getLmtTerm();
            if(lmtTerm != null){
                lmtEndDate = DateUtils.addMonth(lmtStartDate,"yyyymmdd",lmtTerm.intValue());
            }
        }
        String accSubNo = lmtReplyAccSubPrdMapper.getAccSubNoByAccSubPrdNo(accSubPrdNo);
        if(!StringUtils.isEmpty(accSubNo)){
            Map map = new HashMap();
            map.put("accSubNo", accSubNo);
            LmtReplyAccSub lmtReplyAccSub = lmtReplyAccSubMapper.getLmtReplyAccSubByAccSubNo(map);
            if(lmtReplyAccSub != null){
                lmtAmt = lmtReplyAccSub.getLmtAmt();
            }
        }

        int loanCount = accLoanMapper.selectContByCusId(cusId);
        String creloanflag = loanCount > 1 ? "0" : "1";

        List<GrtGuarCont> grtGuarContList = grtGuarContMapper.selectDataByContNo(contNo);
        String guarStartDate = StringUtils.EMPTY;
        String guarEndDate = StringUtils.EMPTY;
        BigDecimal grtAmt = new BigDecimal("0");
        String guarContNo = StringUtils.EMPTY;
        String guarName = StringUtils.EMPTY;

        for (GrtGuarCont grtGuarCont : grtGuarContList) {
            guarStartDate = guarStartDate + grtGuarCont.getGuarStartDate() + ",";
            guarEndDate = guarEndDate + grtGuarCont.getGuarEndDate() + ",";
            grtAmt = grtAmt.add(grtGuarCont.getGuarAmt());
            guarContNo = guarContNo + grtGuarCont.getGuarContNo() + ",";
            Map<String, Object> guarCusMap = guarGuaranteeMapper.selectlListByDbContNo(grtGuarCont.getGuarContNo());
            if(guarCusMap.containsKey("cusname_list")){
                guarName += guarCusMap.get("cusname_list").toString() + ",";
            }

        }
        String repaylatestt = StringUtils.EMPTY;
        if("6".equals(loanModel)||"8".equals(loanModel)){
            repaylatestt="1";	//续贷
        }else if("1".equals(accStatus)){
            repaylatestt="0";	//正常
        }else if("2".equals(accStatus)){
            repaylatestt="2"; //逾期
        }else{
            repaylatestt="3"; //其他
        }
        xdwb0001DataRespDto.setFlowno(billNo);//银行业务流水号
        xdwb0001DataRespDto.setLoanauditing("0");//贷审结果  0审核通过，1审核拒绝
        xdwb0001DataRespDto.setFprproname(prdName);//对接产品名称
        xdwb0001DataRespDto.setFprproid(prdId);//对接产品id
        xdwb0001DataRespDto.setCreditstt("0");//授信结果  0审核通过，1审核拒绝
        xdwb0001DataRespDto.setCreditno(accNo);//银行授信合同号
        xdwb0001DataRespDto.setCreloanflag(creloanflag);//是否首贷  0 否，1是
        xdwb0001DataRespDto.setCstartdate(lmtStartDate);//授信有效期起
        xdwb0001DataRespDto.setCenddate(lmtEndDate);//授信有效期止
        String guatype = StringUtils.EMPTY;
        if("00".equals(guarMode)){
            guatype = "4";
        }else if("10".equals(guarMode)){
            guatype = "0";
        }else if("20".equals(guarMode) || "21".equals(guarMode) ){
            guatype = "1";
        }else if("30".equals(guarMode)){
            guatype = "3";
        }else{
            guatype = "6";
        }
        xdwb0001DataRespDto.setGuatype(guatype);//担保类型0,抵押 1,质押 2,信保基金 3,一般保证 4,信用 5, 实际控制人夫妇提供个人连带担保 6,其他担保
        xdwb0001DataRespDto.setReditamt(lmtAmt);//授信金额（元）
        guarName = !StringUtils.isEmpty(guarName) ? guarName.substring(0,guarName.length() - 1) : StringUtils.EMPTY;
        xdwb0001DataRespDto.setGuabranchname(guarName);//担保公司/保险公司名称
        guarContNo = !StringUtils.isEmpty(guarContNo) ? guarContNo.substring(0,guarContNo.length() - 1) : StringUtils.EMPTY;
        xdwb0001DataRespDto.setFcppollicynum(guarContNo);//担保合同号/保单号
        xdwb0001DataRespDto.setFcpbeginrate(StringUtils.EMPTY);//担保费率起
        xdwb0001DataRespDto.setFcpendrate(StringUtils.EMPTY);//担保费率止
        guarStartDate = !StringUtils.isEmpty(guarStartDate) ? guarStartDate.substring(0,guarStartDate.length() - 1) : StringUtils.EMPTY;
        xdwb0001DataRespDto.setFcpguastatime(guarStartDate);//担保开始日期
        guarEndDate = !StringUtils.isEmpty(guarEndDate) ? guarEndDate.substring(0,guarEndDate.length() - 1) : StringUtils.EMPTY;
        xdwb0001DataRespDto.setFcpguaendtime(guarEndDate);//担保结束日期
        xdwb0001DataRespDto.setFcpguaamt(grtAmt);//担保金额（元）
        xdwb0001DataRespDto.setFcpmeasures(StringUtils.EMPTY);//反担保措施
        xdwb0001DataRespDto.setReceiptno(contNo);//银行放款合同号
        xdwb0001DataRespDto.setLoanamt(loanAmt);//放款金额（元）
        xdwb0001DataRespDto.setLoanrate(execRateYear);//执行年利率
        xdwb0001DataRespDto.setLstartdate(loanStartDate);//贷款起始日
        xdwb0001DataRespDto.setLenddate(loanEndDate);//贷款到期日
        xdwb0001DataRespDto.setLoanlimit(loanTerm);//放款期限（月）
        String repaystt = StringUtils.EMPTY;
        if("10".equals(fiveClass)){
            repaystt = "0";
        }else if("20".equals(guarMode)){
            repaystt = "1";
        }else if("30".equals(guarMode)){
            repaystt = "2";
        }else if("40".equals(guarMode)){
            repaystt = "3";
        }else if("50".equals(guarMode)){
            repaystt = "4";
        }
        xdwb0001DataRespDto.setRepaystt(repaystt);//还款状态 0:正常还款、1:续贷、2:逾期、3:其他
        xdwb0001DataRespDto.setRepaylatestt(repaylatestt);//逾期等级分类0：正常:/1：关注/2：次级/3：可疑/4：损失

        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001DataRespDto));
        return xdwb0001DataRespDto;
    }
}
