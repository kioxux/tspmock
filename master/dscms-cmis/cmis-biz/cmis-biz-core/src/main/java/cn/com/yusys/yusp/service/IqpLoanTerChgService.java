/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.IqpLoanTerChg;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpLoanTerChgMapper;
import cn.com.yusys.yusp.util.BizUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 贷款投向调整service
 */
@Service
public class IqpLoanTerChgService {
    private static final Logger log = LoggerFactory.getLogger(IqpLoanTerChgService.class);
    @Autowired
    private IqpLoanTerChgMapper iqpLoanTerChgMapper;


    public List<IqpLoanTerChg> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<IqpLoanTerChg> list = iqpLoanTerChgMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(IqpLoanTerChg iqpLoanTerChg) {
        return iqpLoanTerChgMapper.insertSelective(iqpLoanTerChg);
    }


    /**
     * 新增页面保存数据
     * @param param
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public Map saveIqpLoanTerChg(Map param) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        try {
            //获取业务流水号
            String iqpSerno = (String) param.get("iqpSerno");

            CtrLoanCont ctrLoanCont = JSONObject.parseObject(JSON.toJSONString(param), CtrLoanCont.class);
            BizUtils bizUtils = new BizUtils();
            IqpLoanTerChg iqpLoanTerChg = new IqpLoanTerChg();
            BeanUtils.copyProperties(ctrLoanCont, iqpLoanTerChg);
            iqpLoanTerChg.setOldStrategyNewType(ctrLoanCont.getStrategyNewLoan());
            iqpLoanTerChg.setOldLoanTer(ctrLoanCont.getLoanType());
            iqpLoanTerChg.setOldComUpIndtify(ctrLoanCont.getComUpIndtify());
            iqpLoanTerChg.setComUpIndtify("");
            iqpLoanTerChg.setIqpSerno(iqpSerno);
            iqpLoanTerChg.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//设置状态为"待发起"
            iqpLoanTerChg.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//设置操作状态为"新增"
            int insertCount = iqpLoanTerChgMapper.insertSelective(iqpLoanTerChg);

            if (insertCount < 0) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",保存失败！");
            }
            if (insertCount < 0) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",保存放款申请主办人信息异常！");
            }

        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存放款申请出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }

        return rtnData;
    }

    /**
     * 根据借据编号查询是否存在在途的变更业务
     * @param iqpLoanTerChg
     * @return
     */
    public int checkIsExistWFByBillNo(IqpLoanTerChg iqpLoanTerChg) {
        try {
            String contNo = iqpLoanTerChg.getContNo();
            log.info("根据合同编号查询在途的还款方式变更申请业务【{}】", JSONObject.toJSON(iqpLoanTerChg));
            int result = 0;
            result = iqpLoanTerChgMapper.checkIsExistWFByContNo(contNo);
            if (result > 0 ) {
                throw new YuspException(EcbEnum.IQP_REPAY_WAY_CHG_CHECK_EXCEPTION_3.key, EcbEnum.IQP_REPAY_WAY_CHG_CHECK_EXCEPTION_3.value);
            }
            return result;
        } catch (YuspException e) {
            log.error("还款方式变更申请校验其他变更业务失败！", e);
            return -1;
        }
    }

    /**
     *根据主键查询数据
     * @param iqpSerno
     * @return
     */
    public IqpLoanTerChg selectByPrimaryKey(String iqpSerno) {
        IqpLoanTerChg iqpLoanTerChg = iqpLoanTerChgMapper.selectByPrimaryKey(iqpSerno);
        return iqpLoanTerChg;
    }

    public int updateApproveStatusByIqpSerno(String iqpSerno, String wfStatus111) {
        return iqpLoanTerChgMapper.updateApproveStatusByIqpSerno(iqpSerno,wfStatus111);
    }

    /**
     * 流程审批通过，更新这笔借据的还款方式
     * @param iqpSerno
     */
    public void updateRepayWayAfterWfAppr(String iqpSerno) {
        IqpLoanTerChg iqpLoanTerChg = new IqpLoanTerChg();
        iqpLoanTerChg = selectByPrimaryKey(iqpSerno);
        updateAccLoanRepayWayByBillNo(iqpLoanTerChg);
    }

    /**
     * 通过借据号修改贷款台账中借据的还款方式
     * @reyPlan
     * @return
     */
    public void updateAccLoanRepayWayByBillNo(IqpLoanTerChg iqpLoanTerChg) {
        try{
            //想iqprepayplan表中插入数据
            /*AccLoan accLoan = reyPlanMapper.selectPvpLoanAppByBillNo(reyPlan.getBillNo());
            String pvpSerno = accLoan.getPvpSerno();
            reyPlanMapper.insertIntoIqpRepayPlan(reyPlan);*/
            System.out.println("审批结束后向借据表反插数据");
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("更新贷款台账还款方式信息处理发生异常！",e);
            throw new YuspException(EcbEnum.IQP_REPAY_UPDATE_EXCEPTION_1.key, EcbEnum.IQP_REPAY_UPDATE_EXCEPTION_1.value);
        }
    }

    /**
     * 更新申请状态
     * @param iqpLoanTerChg
     * @return
     */
    public int updateByPrimaryKeySelective(IqpLoanTerChg iqpLoanTerChg) {
        return iqpLoanTerChgMapper.updateByPrimaryKeySelective(iqpLoanTerChg);
    }
}
