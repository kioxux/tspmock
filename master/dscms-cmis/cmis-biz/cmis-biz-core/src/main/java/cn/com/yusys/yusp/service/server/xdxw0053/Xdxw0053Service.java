package cn.com.yusys.yusp.service.server.xdxw0053;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0053.resp.Xdxw0053DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportBasicInfoMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:查询经营性贷款客户基本信息
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdxw0053Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdxw0053Service.class);

    @Autowired
    private LmtSurveyReportBasicInfoMapper lmtSurveyReportBasicInfoMapper;

    /**
     * 查询经营性贷款客户基本信息
     * @param indgtSerno
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0053DataRespDto getSurveyReportByIndgtSerno(String indgtSerno) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, indgtSerno);
        Xdxw0053DataRespDto result = null;
        try {
            result = lmtSurveyReportBasicInfoMapper.getSurveyReportByIndgtSerno(indgtSerno);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, JSON.toJSONString(result));
        return result;
    }
}
