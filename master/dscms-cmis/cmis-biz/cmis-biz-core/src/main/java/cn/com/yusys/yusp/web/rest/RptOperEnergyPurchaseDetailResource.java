/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import cn.com.yusys.yusp.domain.RptOperProductionOper;
import cn.com.yusys.yusp.service.RptOperProductionOperService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptOperEnergyPurchaseDetail;
import cn.com.yusys.yusp.service.RptOperEnergyPurchaseDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptOperEnergyPurchaseDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 17:49:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptoperenergypurchasedetail")
public class RptOperEnergyPurchaseDetailResource {
    @Autowired
    private RptOperEnergyPurchaseDetailService rptOperEnergyPurchaseDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptOperEnergyPurchaseDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptOperEnergyPurchaseDetail> list = rptOperEnergyPurchaseDetailService.selectAll(queryModel);
        return new ResultDto<List<RptOperEnergyPurchaseDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptOperEnergyPurchaseDetail>> index(QueryModel queryModel) {
        List<RptOperEnergyPurchaseDetail> list = rptOperEnergyPurchaseDetailService.selectByModel(queryModel);
        return new ResultDto<List<RptOperEnergyPurchaseDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptOperEnergyPurchaseDetail> show(@PathVariable("pkId") String pkId) {
        RptOperEnergyPurchaseDetail rptOperEnergyPurchaseDetail = rptOperEnergyPurchaseDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<RptOperEnergyPurchaseDetail>(rptOperEnergyPurchaseDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptOperEnergyPurchaseDetail> create(@RequestBody RptOperEnergyPurchaseDetail rptOperEnergyPurchaseDetail) throws URISyntaxException {
        rptOperEnergyPurchaseDetailService.insert(rptOperEnergyPurchaseDetail);
        return new ResultDto<RptOperEnergyPurchaseDetail>(rptOperEnergyPurchaseDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptOperEnergyPurchaseDetail rptOperEnergyPurchaseDetail) throws URISyntaxException {
        int result = rptOperEnergyPurchaseDetailService.update(rptOperEnergyPurchaseDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptOperEnergyPurchaseDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptOperEnergyPurchaseDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptOperEnergyPurchaseDetail>> selectByModel(@RequestBody QueryModel model){
        return new ResultDto<List<RptOperEnergyPurchaseDetail>>(rptOperEnergyPurchaseDetailService.selectByModel(model));
    }

    @PostMapping("/insertPurchaseDetail")
    protected ResultDto<Integer> insertPurchaseDetail(@RequestBody RptOperEnergyPurchaseDetail rptOperEnergyPurchaseDetail){
        rptOperEnergyPurchaseDetail.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptOperEnergyPurchaseDetailService.insertPurchaseDetail(rptOperEnergyPurchaseDetail));
    }

    @PostMapping("/updatePurchaseDetail")
    protected ResultDto<Integer> updatePurchaseDetail(@RequestBody RptOperEnergyPurchaseDetail rptOperEnergyPurchaseDetail){
        return new ResultDto<Integer>(rptOperEnergyPurchaseDetailService.updateSelective(rptOperEnergyPurchaseDetail));
    }

    @PostMapping("/deletePurchaseDetail")
    protected ResultDto<Integer> deletePurchaseDetail(@RequestBody RptOperEnergyPurchaseDetail rptOperEnergyPurchaseDetail){
        String pkId = rptOperEnergyPurchaseDetail.getPkId();
        return new ResultDto<Integer>(rptOperEnergyPurchaseDetailService.deleteByPrimaryKey(pkId));
    }

    @PostMapping("/insert")
    protected ResultDto<Integer> insert(@RequestBody RptOperEnergyPurchaseDetail rptOperEnergyPurchaseDetail){
        rptOperEnergyPurchaseDetail.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptOperEnergyPurchaseDetailService.insertSelective(rptOperEnergyPurchaseDetail));
    }
}
