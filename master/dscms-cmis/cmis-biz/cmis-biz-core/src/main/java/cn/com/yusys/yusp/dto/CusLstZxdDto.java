package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusLstZxd
 * @类描述: cus_lst_zxd数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-07-26 20:11:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CusLstZxdDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	private String serno;
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 借据号 **/
	private String billNo;
	
	/** 合同号 **/
	private String contNo;
	
	/** 借据金额 **/
	private java.math.BigDecimal billAmt;
	
	/** 借据余额 **/
	private java.math.BigDecimal billBal;
	
	/** 贷款起始日 **/
	private String loanStartDate;
	
	/** 贷款截止日 **/
	private String loanEndDate;
	
	/** 申请日期 **/
	private String appDate;
	
	/** 联系电话 **/
	private String phone;
	
	/** 配偶名称 **/
	private String spouseCusName;
	
	/** 配偶证件号码 **/
	private String spouseCertCode;
	
	/** 办理状态 **/
	private String applyStatus;
	
	/** 转换原因 **/
	private String changeRs;
	
	/** 婚姻状态 **/
	private String spouseMarryVal;
	
	/** 配偶电话 **/
	private String spousePhone;
	
	/** 押品他项权证金额 **/
	private java.math.BigDecimal evalAmt;
	
	/** 批复余额 **/
	private java.math.BigDecimal approvalBal;
	
	/** 配偶客户编号 **/
	private String spouseCusId;
	
	/** 配偶借据号 **/
	private String spouseBillNo;
	
	/** 账务机构 **/
	private String finaBrId;
	
	/** 配偶的客户经理 **/
	private String spouseManagerId;
	
	/** 客户经理 **/
	private String managerName;
	
	/** 配偶客户经理 **/
	private String spouseManagerName;
	
	/** 执行利率(年) **/
	private java.math.BigDecimal execRateYear;
	
	/** 房产类型编码 **/
	private String landUseWay;
	
	/** 还款期数 **/
	private Integer repayTerm;
	
	/** 本金逾期记录 **/
	private Integer overdueCount;
	
	/** 借据号汇总 **/
	private String billNoAll;
	
	/** 配偶本金逾期记录 **/
	private Integer memberOverdueCount;
	
	/** 配偶借据号汇总 **/
	private String memberBillNoAll;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo == null ? null : billNo.trim();
	}
	
    /**
     * @return BillNo
     */	
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param billAmt
	 */
	public void setBillAmt(java.math.BigDecimal billAmt) {
		this.billAmt = billAmt;
	}
	
    /**
     * @return BillAmt
     */	
	public java.math.BigDecimal getBillAmt() {
		return this.billAmt;
	}
	
	/**
	 * @param billBal
	 */
	public void setBillBal(java.math.BigDecimal billBal) {
		this.billBal = billBal;
	}
	
    /**
     * @return BillBal
     */	
	public java.math.BigDecimal getBillBal() {
		return this.billBal;
	}
	
	/**
	 * @param loanStartDate
	 */
	public void setLoanStartDate(String loanStartDate) {
		this.loanStartDate = loanStartDate == null ? null : loanStartDate.trim();
	}
	
    /**
     * @return LoanStartDate
     */	
	public String getLoanStartDate() {
		return this.loanStartDate;
	}
	
	/**
	 * @param loanEndDate
	 */
	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate == null ? null : loanEndDate.trim();
	}
	
    /**
     * @return LoanEndDate
     */	
	public String getLoanEndDate() {
		return this.loanEndDate;
	}
	
	/**
	 * @param appDate
	 */
	public void setAppDate(String appDate) {
		this.appDate = appDate == null ? null : appDate.trim();
	}
	
    /**
     * @return AppDate
     */	
	public String getAppDate() {
		return this.appDate;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}
	
    /**
     * @return Phone
     */	
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param spouseCusName
	 */
	public void setSpouseCusName(String spouseCusName) {
		this.spouseCusName = spouseCusName == null ? null : spouseCusName.trim();
	}
	
    /**
     * @return SpouseCusName
     */	
	public String getSpouseCusName() {
		return this.spouseCusName;
	}
	
	/**
	 * @param spouseCertCode
	 */
	public void setSpouseCertCode(String spouseCertCode) {
		this.spouseCertCode = spouseCertCode == null ? null : spouseCertCode.trim();
	}
	
    /**
     * @return SpouseCertCode
     */	
	public String getSpouseCertCode() {
		return this.spouseCertCode;
	}
	
	/**
	 * @param applyStatus
	 */
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus == null ? null : applyStatus.trim();
	}
	
    /**
     * @return ApplyStatus
     */	
	public String getApplyStatus() {
		return this.applyStatus;
	}
	
	/**
	 * @param changeRs
	 */
	public void setChangeRs(String changeRs) {
		this.changeRs = changeRs == null ? null : changeRs.trim();
	}
	
    /**
     * @return ChangeRs
     */	
	public String getChangeRs() {
		return this.changeRs;
	}
	
	/**
	 * @param spouseMarryVal
	 */
	public void setSpouseMarryVal(String spouseMarryVal) {
		this.spouseMarryVal = spouseMarryVal == null ? null : spouseMarryVal.trim();
	}
	
    /**
     * @return SpouseMarryVal
     */	
	public String getSpouseMarryVal() {
		return this.spouseMarryVal;
	}
	
	/**
	 * @param spousePhone
	 */
	public void setSpousePhone(String spousePhone) {
		this.spousePhone = spousePhone == null ? null : spousePhone.trim();
	}
	
    /**
     * @return SpousePhone
     */	
	public String getSpousePhone() {
		return this.spousePhone;
	}
	
	/**
	 * @param evalAmt
	 */
	public void setEvalAmt(java.math.BigDecimal evalAmt) {
		this.evalAmt = evalAmt;
	}
	
    /**
     * @return EvalAmt
     */	
	public java.math.BigDecimal getEvalAmt() {
		return this.evalAmt;
	}
	
	/**
	 * @param approvalBal
	 */
	public void setApprovalBal(java.math.BigDecimal approvalBal) {
		this.approvalBal = approvalBal;
	}
	
    /**
     * @return ApprovalBal
     */	
	public java.math.BigDecimal getApprovalBal() {
		return this.approvalBal;
	}
	
	/**
	 * @param spouseCusId
	 */
	public void setSpouseCusId(String spouseCusId) {
		this.spouseCusId = spouseCusId == null ? null : spouseCusId.trim();
	}
	
    /**
     * @return SpouseCusId
     */	
	public String getSpouseCusId() {
		return this.spouseCusId;
	}
	
	/**
	 * @param spouseBillNo
	 */
	public void setSpouseBillNo(String spouseBillNo) {
		this.spouseBillNo = spouseBillNo == null ? null : spouseBillNo.trim();
	}
	
    /**
     * @return SpouseBillNo
     */	
	public String getSpouseBillNo() {
		return this.spouseBillNo;
	}
	
	/**
	 * @param finaBrId
	 */
	public void setFinaBrId(String finaBrId) {
		this.finaBrId = finaBrId == null ? null : finaBrId.trim();
	}
	
    /**
     * @return FinaBrId
     */	
	public String getFinaBrId() {
		return this.finaBrId;
	}
	
	/**
	 * @param spouseManagerId
	 */
	public void setSpouseManagerId(String spouseManagerId) {
		this.spouseManagerId = spouseManagerId == null ? null : spouseManagerId.trim();
	}
	
    /**
     * @return SpouseManagerId
     */	
	public String getSpouseManagerId() {
		return this.spouseManagerId;
	}
	
	/**
	 * @param managerName
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName == null ? null : managerName.trim();
	}
	
    /**
     * @return ManagerName
     */	
	public String getManagerName() {
		return this.managerName;
	}
	
	/**
	 * @param spouseManagerName
	 */
	public void setSpouseManagerName(String spouseManagerName) {
		this.spouseManagerName = spouseManagerName == null ? null : spouseManagerName.trim();
	}
	
    /**
     * @return SpouseManagerName
     */	
	public String getSpouseManagerName() {
		return this.spouseManagerName;
	}
	
	/**
	 * @param execRateYear
	 */
	public void setExecRateYear(java.math.BigDecimal execRateYear) {
		this.execRateYear = execRateYear;
	}
	
    /**
     * @return ExecRateYear
     */	
	public java.math.BigDecimal getExecRateYear() {
		return this.execRateYear;
	}
	
	/**
	 * @param landUseWay
	 */
	public void setLandUseWay(String landUseWay) {
		this.landUseWay = landUseWay == null ? null : landUseWay.trim();
	}
	
    /**
     * @return LandUseWay
     */	
	public String getLandUseWay() {
		return this.landUseWay;
	}
	
	/**
	 * @param repayTerm
	 */
	public void setRepayTerm(Integer repayTerm) {
		this.repayTerm = repayTerm;
	}
	
    /**
     * @return RepayTerm
     */	
	public Integer getRepayTerm() {
		return this.repayTerm;
	}
	
	/**
	 * @param overdueCount
	 */
	public void setOverdueCount(Integer overdueCount) {
		this.overdueCount = overdueCount;
	}
	
    /**
     * @return OverdueCount
     */	
	public Integer getOverdueCount() {
		return this.overdueCount;
	}
	
	/**
	 * @param billNoAll
	 */
	public void setBillNoAll(String billNoAll) {
		this.billNoAll = billNoAll == null ? null : billNoAll.trim();
	}
	
    /**
     * @return BillNoAll
     */	
	public String getBillNoAll() {
		return this.billNoAll;
	}
	
	/**
	 * @param memberOverdueCount
	 */
	public void setMemberOverdueCount(Integer memberOverdueCount) {
		this.memberOverdueCount = memberOverdueCount;
	}
	
    /**
     * @return MemberOverdueCount
     */	
	public Integer getMemberOverdueCount() {
		return this.memberOverdueCount;
	}
	
	/**
	 * @param memberBillNoAll
	 */
	public void setMemberBillNoAll(String memberBillNoAll) {
		this.memberBillNoAll = memberBillNoAll == null ? null : memberBillNoAll.trim();
	}
	
    /**
     * @return MemberBillNoAll
     */	
	public String getMemberBillNoAll() {
		return this.memberBillNoAll;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}