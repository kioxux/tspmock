/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.IqpWriteoffApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpWriteoffDetl;
import cn.com.yusys.yusp.service.IqpWriteoffDetlService;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpWriteoffDetlResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-01-19 19:31:12
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqpwriteoffdetl")
public class IqpWriteoffDetlResource {
    @Autowired
    private IqpWriteoffDetlService iqpWriteoffDetlService;

	/**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpWriteoffDetl>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpWriteoffDetl> list = iqpWriteoffDetlService.selectAll(queryModel);
        return new ResultDto<List<IqpWriteoffDetl>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpWriteoffDetl>> index(QueryModel queryModel) {
        List<IqpWriteoffDetl> list = iqpWriteoffDetlService.selectByModel(queryModel);
        return new ResultDto<List<IqpWriteoffDetl>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpWriteoffDetl> show(@PathVariable("pkId") String pkId) {
        IqpWriteoffDetl iqpWriteoffDetl = iqpWriteoffDetlService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpWriteoffDetl>(iqpWriteoffDetl);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpWriteoffDetl> create(@RequestBody IqpWriteoffDetl iqpWriteoffDetl) throws URISyntaxException {
        iqpWriteoffDetlService.insert(iqpWriteoffDetl);
        return new ResultDto<IqpWriteoffDetl>(iqpWriteoffDetl);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpWriteoffDetl iqpWriteoffDetl) throws URISyntaxException {
        int result = iqpWriteoffDetlService.update(iqpWriteoffDetl);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpWriteoffDetlService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpWriteoffDetlService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:saveIqpWriteOffAppLeadInfo
     * @函数描述:通过页面引导信息保存核销申请信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveIqpWriteOffDetailLeadInfo")
    protected ResultDto<Map> saveIqpWriteOffDetailLeadInfo(@RequestBody IqpWriteoffDetl iqpWriteoffDetl) throws URISyntaxException {
        Map map = iqpWriteoffDetlService.saveIqpWriteOffDetailLeadInfo(iqpWriteoffDetl);
        return new ResultDto<Map>(map);
    }


    /**
     * @函数名称:deleteIqpWriteOffDetailLogic
     * @函数描述:通过主键对核销明细进行逻辑删除
     * @参数与返回说明: pkId
     * @算法描述: 对数据类型进行更新，并重新计算呆账核销申请汇总金额
     */
    @GetMapping("/deleteIqpWriteOffDetailLogic/{pkId}")
    protected ResultDto<Map> deleteIqpWriteOffDetailLogic(@PathVariable("pkId") String pkId) {
        Map map = iqpWriteoffDetlService.deleteIqpWriteOffDetailLogic(pkId);
        return new ResultDto<Map>(map);
    }

    /**
     * @函数名称:updateIqpWriteOffDetail
     * @函数描述:更新呆账核销明细信息并更新呆账核销申请信息汇总金额信息
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updateIqpWriteOffDetail")
    protected ResultDto<Map> updateIqpWriteOffDetail(@RequestBody IqpWriteoffDetl iqpWriteoffDetl) throws URISyntaxException {
        Map map = iqpWriteoffDetlService.updateIqpWriteOffDetail(iqpWriteoffDetl);
        return new ResultDto<Map>(map);
    }

}
