package cn.com.yusys.yusp.service.server.xddh0008;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoanRepayPlanTem;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.LstLnDkhkmx;
import cn.com.yusys.yusp.dto.server.xddh0008.req.Xddh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0008.resp.Xddh0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanRepayPlanTemMapper;
import cn.com.yusys.yusp.service.client.bsp.core.ln3246.Ln3246Service;
import com.alibaba.fastjson.JSON;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:还款记录列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xddh0008Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0008Service.class);

    @Autowired
    private Ln3246Service ln3246Service;

    @Autowired
    private AccLoanRepayPlanTemMapper accLoanRepayPlanTemMapper;

    /**
     * 还款记录列表查询
     * @param xddh0008DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public ResultDto<Xddh0008DataRespDto> getRepayList(Xddh0008DataReqDto xddh0008DataReqDto) {

        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008DataReqDto));
        ResultDto<Xddh0008DataRespDto> respDtoResultDto = new ResultDto<>();
        Xddh0008DataRespDto xddh0008DataRespDto = new Xddh0008DataRespDto();
        String billNo = xddh0008DataReqDto.getBillNo();//借据号
        accLoanRepayPlanTemMapper.deleteByBillNo(billNo);
        Integer startPageNum = Optional.ofNullable(xddh0008DataReqDto.getStartPageNum()).orElse(1);//起始页数
        Integer pageSize = Optional.ofNullable(xddh0008DataReqDto.getPageSize()).orElse(10);//分页大小
        // 拼接调用核心参数
        Ln3246ReqDto ln3246ReqDto = new Ln3246ReqDto();
        ln3246ReqDto.setDkjiejuh(billNo);
        //ln3246ReqDto.setQishibis(startPageNum);
        //ln3246ReqDto.setChxunbis(pageSize);
        // 调用核心
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value, JSON.toJSONString(ln3246ReqDto));
        ResultDto<Ln3246RespDto> ln3246RespDtoResultDto = ln3246Service.ln3246(ln3246ReqDto);
        Ln3246RespDto data = ln3246RespDtoResultDto.getData();
        respDtoResultDto.setCode(ln3246RespDtoResultDto.getCode());
        respDtoResultDto.setMessage(Optional.ofNullable(ln3246RespDtoResultDto.getMessage()).orElse("新核心系统中无对应记录"));
        if(Objects.nonNull(data) && Objects.nonNull(data.getZongbish()) && Objects.nonNull(data.getLstLnDkhkmx())) {
            xddh0008DataRespDto.setTotalQnt(Optional.ofNullable(data.getZongbish()).orElse(0));
            //判断总笔数如果大于默认的20条则重新按照总笔数查询
            if(xddh0008DataRespDto.getTotalQnt()>20){
                ln3246ReqDto.setChxunbis(xddh0008DataRespDto.getTotalQnt());
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value, JSON.toJSONString(ln3246ReqDto));
                ln3246RespDtoResultDto = ln3246Service.ln3246(ln3246ReqDto);
                data = ln3246RespDtoResultDto.getData();
            }
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value, JSON.toJSONString(data));
            List<LstLnDkhkmx> lstLnDkhkmx = Optional.ofNullable(data.getLstLnDkhkmx()).orElse(new ArrayList<>());
            List<cn.com.yusys.yusp.dto.server.xddh0008.resp.List> respList = lstLnDkhkmx.parallelStream().map(lndkhkmx -> {
                cn.com.yusys.yusp.dto.server.xddh0008.resp.List temp = new cn.com.yusys.yusp.dto.server.xddh0008.resp.List();
                temp.setDateno(Optional.ofNullable(lndkhkmx.getBenqqish()).orElse(0));// 期号
                temp.setEndDate(Optional.ofNullable(lndkhkmx.getZhzhriqi()).orElse(StringUtils.EMPTY));// 到期日
                temp.setBillDate(Optional.ofNullable(lndkhkmx.getQishriqi()).orElse(StringUtils.EMPTY));// 账单日
                temp.setInstmAmt(Optional.ofNullable(lndkhkmx.getMeiqhkze()).orElse(new BigDecimal(0L)));// 期供金额
                temp.setCap(Optional.ofNullable(lndkhkmx.getChushibj()).orElse(new BigDecimal(0L)));// 本金
                temp.setInterest(Optional.ofNullable(lndkhkmx.getChushilx()).orElse(new BigDecimal(0L)));// 利息
                temp.setOdint(Optional.ofNullable(lndkhkmx.getYshofaxi()).orElse(new BigDecimal(0L)));// 罚息
                temp.setCi(Optional.ofNullable(lndkhkmx.getFuxiiiii()).orElse(new BigDecimal(0L)));// 复利
                temp.setLoanBalance(Optional.ofNullable(lndkhkmx.getZhanghye()).orElse(new BigDecimal(0L)));// 剩余本金
                temp.setHasbcCap(Optional.ofNullable(lndkhkmx.getBenjinfs()).orElse(new BigDecimal(0L)));// 已还本金
                temp.setHasbcInt(Optional.ofNullable(lndkhkmx.getGhysyjlx()).orElse(new BigDecimal(0L)));// 已还利息
                temp.setHasbcOdint(Optional.ofNullable(lndkhkmx.getGhynshfx()).orElse(new BigDecimal(0L)));// 已还罚息
                temp.setHasbcCi(Optional.ofNullable(lndkhkmx.getGhfxfuxi()).orElse(new BigDecimal(0L)));// 已还复利
                temp.setStatus(Optional.ofNullable(lndkhkmx.getBenqizht()).orElse(StringUtils.EMPTY));//本期状态
                temp.setIsSettlFlag(StringUtils.EMPTY);// 结清标记

                //将还款记录列表查询到的数据插入贷款台账还款计划临时表中
                AccLoanRepayPlanTem accLoanRepayPlanTem = new AccLoanRepayPlanTem();
                accLoanRepayPlanTem.setPkId(cn.com.yusys.yusp.commons.util.StringUtils.getUUID());
                //借据编号
                accLoanRepayPlanTem.setBillNo(lndkhkmx.getDkjiejuh());
                //期号
                accLoanRepayPlanTem.setDateno(lndkhkmx.getBenqqish().toString());
                //到期日
                accLoanRepayPlanTem.setEnddate(lndkhkmx.getZhzhriqi());
                //剩余本金
                accLoanRepayPlanTem.setSurplusCap(lndkhkmx.getZhanghye());
                BeanUtils.copyProperties(temp,accLoanRepayPlanTem);
                accLoanRepayPlanTemMapper.insert(accLoanRepayPlanTem);
                return temp;
            }).collect(Collectors.toList());
            xddh0008DataRespDto.setList(respList);
            logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008DataRespDto));
            respDtoResultDto.setData(xddh0008DataRespDto);
            return respDtoResultDto;
        }else{
            return respDtoResultDto;
        }
    }
}
