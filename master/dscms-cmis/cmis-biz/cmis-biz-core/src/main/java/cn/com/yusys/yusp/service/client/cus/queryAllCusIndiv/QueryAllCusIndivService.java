package cn.com.yusys.yusp.service.client.cus.queryAllCusIndiv;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.CusIndivAllDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisCusClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：查询个人客户基本信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class QueryAllCusIndivService {

    private static final Logger logger = LoggerFactory.getLogger(QueryAllCusIndivService.class);

    // 1）注入：封装的接口类:客户管理模块
    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 业务逻辑处理方法：查询个人客户基本信息
     *
     * @param queryModel
     * @return
     */
    @Transactional
    public List<CusIndivAllDto> queryAllCusIndiv(QueryModel queryModel) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(queryModel));
        ResultDto<List<CusIndivAllDto>> listResultDto = cmisCusClientService.queryAllCusIndiv(queryModel);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value, JSON.toJSONString(listResultDto));

        String code = Optional.ofNullable(listResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String meesage = Optional.ofNullable(listResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        List<CusIndivAllDto> result = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, listResultDto.getCode())) {
            //  获取相关的值并解析
            result = listResultDto.getData();
        } else {
            //  抛出错误异常
            throw BizException.error(null, code, meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0007.key, DscmsEnum.TRADE_CODE_CMISCUS0007.value);
        return result;
    }
}
