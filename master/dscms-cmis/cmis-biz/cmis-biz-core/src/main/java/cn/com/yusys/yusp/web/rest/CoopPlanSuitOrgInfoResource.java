/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPlanSuitOrgInfo;
import cn.com.yusys.yusp.service.CoopPlanSuitOrgInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanSuitOrgInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-13 10:08:37
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopplansuitorginfo")
public class CoopPlanSuitOrgInfoResource {
    @Autowired
    private CoopPlanSuitOrgInfoService coopPlanSuitOrgInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPlanSuitOrgInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPlanSuitOrgInfo> list = coopPlanSuitOrgInfoService.selectAll(queryModel);
        return new ResultDto<List<CoopPlanSuitOrgInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPlanSuitOrgInfo>> index(QueryModel queryModel) {
        List<CoopPlanSuitOrgInfo> list = coopPlanSuitOrgInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanSuitOrgInfo>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPlanSuitOrgInfo>> query(@RequestBody QueryModel queryModel) {
        List<CoopPlanSuitOrgInfo> list = coopPlanSuitOrgInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanSuitOrgInfo>>(list);
    }


    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CoopPlanSuitOrgInfo> show(@PathVariable("pkId") String pkId) {
        CoopPlanSuitOrgInfo coopPlanSuitOrgInfo = coopPlanSuitOrgInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<CoopPlanSuitOrgInfo>(coopPlanSuitOrgInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<CoopPlanSuitOrgInfo> create(@RequestBody CoopPlanSuitOrgInfo coopPlanSuitOrgInfo) throws URISyntaxException {
        coopPlanSuitOrgInfoService.insert(coopPlanSuitOrgInfo);
        return new ResultDto<CoopPlanSuitOrgInfo>(coopPlanSuitOrgInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CoopPlanSuitOrgInfo coopPlanSuitOrgInfo) throws URISyntaxException {
        int result = coopPlanSuitOrgInfoService.update(coopPlanSuitOrgInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = coopPlanSuitOrgInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPlanSuitOrgInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody CoopPlanSuitOrgInfo coopPlanSuitOrgInfo) {
        int result = coopPlanSuitOrgInfoService.deleteByPrimaryKey(coopPlanSuitOrgInfo.getPkId());
        return ResultDto.success(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteall")
    protected ResultDto<Integer> deleteAll(@RequestBody CoopPlanSuitOrgInfo coopPlanSuitOrgInfo) {
        int result = coopPlanSuitOrgInfoService.deleteAll(coopPlanSuitOrgInfo);
        return ResultDto.success(result);
    }
}
