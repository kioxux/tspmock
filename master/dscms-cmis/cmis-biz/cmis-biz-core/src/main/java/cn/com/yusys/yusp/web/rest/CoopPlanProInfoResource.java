/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.CoopPlanProInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPlanProInfo;
import cn.com.yusys.yusp.service.CoopPlanProInfoService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanProInfoResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-14 15:31:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/coopplanproinfo")
public class CoopPlanProInfoResource {
    @Autowired
    private CoopPlanProInfoService coopPlanProInfoService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CoopPlanProInfoDto>> query() {
        QueryModel queryModel = new QueryModel();
        List<CoopPlanProInfoDto> list = coopPlanProInfoService.selectAll(queryModel);
        return new ResultDto<List<CoopPlanProInfoDto>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CoopPlanProInfoDto>> index(QueryModel queryModel) {
        List<CoopPlanProInfoDto> list = coopPlanProInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanProInfoDto>>(list);
    }


    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<CoopPlanProInfoDto>> query(@RequestBody QueryModel queryModel) {
        List<CoopPlanProInfoDto> list = coopPlanProInfoService.selectByModel(queryModel);
        return new ResultDto<List<CoopPlanProInfoDto>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/queryforacc")
    protected ResultDto<List<CoopPlanProInfoDto>> queryForAcc(@RequestBody QueryModel queryModel) {
        List<CoopPlanProInfoDto> list = coopPlanProInfoService.queryForAcc(queryModel);
        return new ResultDto<List<CoopPlanProInfoDto>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CoopPlanProInfo> show(@PathVariable("pkId") String pkId) {
        CoopPlanProInfo coopPlanProInfo = coopPlanProInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<CoopPlanProInfo>(coopPlanProInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BigDecimal> create(@RequestBody CoopPlanProInfo coopPlanProInfo) throws URISyntaxException {
        BigDecimal ret = coopPlanProInfoService.insert(coopPlanProInfo);
        return new ResultDto<BigDecimal>(ret);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<BigDecimal> update(@RequestBody CoopPlanProInfo coopPlanProInfo) throws URISyntaxException {
        BigDecimal result = coopPlanProInfoService.update(coopPlanProInfo);
        return new ResultDto<BigDecimal>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<BigDecimal> delete(@PathVariable("pkId") String pkId) {
        BigDecimal result = coopPlanProInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<BigDecimal>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = coopPlanProInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
