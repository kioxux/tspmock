/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.CtrLoanContAndPvp;
import cn.com.yusys.yusp.domain.CtrLoanContForZhcxDto;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.*;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009ReqDto;
import cn.com.yusys.yusp.dto.server.xdht0005.req.Xdht0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0007.req.Xdht0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.GuarContList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.HxdLoanContList;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.LoanContList;
import cn.com.yusys.yusp.dto.server.xdht0014.resp.Xdht0014DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0024.resp.Xdht0024DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0031.resp.Xdht0031DataRespDto;
import cn.com.yusys.yusp.dto.server.xdht0038.req.Xdht0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0041.resp.Xdtz0041DataRespDto;
import cn.com.yusys.yusp.dto.server.xdtz0044.resp.Xdtz0044DataRespDto;
import cn.com.yusys.yusp.dto.server.xdxw0014.req.Xdxw0014DataReqDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrLoanContMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-09-04 21:10:47
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CtrLoanContMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CtrLoanCont selectByPrimaryKey(@Param("contNo") String contNo);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CtrLoanCont> selectByModel(QueryModel model);

    /**
     * @方法名称: selectCtrLoanContListData
     * @方法描述: 重写列表查询方法
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrLoanCont> selectCtrLoanContListData(QueryModel model);

    /**
     * @方法名称: selectCtrLoanContListDataForZhcx
     * @方法描述: 普通贷款合同查询方法
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrLoanContForZhcxDto> selectCtrLoanContListDataForZhcx(QueryModel model);

    /**
     * @方法名称: selectComLoanInfoByManagerId
     * @方法描述: 优企贷无还本名单查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<ComLoanInfoDto> selectComLoanInfoByManagerId(Map queryMap);

    /**
     * @方法名称: selectCtrLoanContHisListData
     * @方法描述: 重写历史列表查询方法
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrLoanCont> selectCtrLoanContHisListData(QueryModel model);

    List<CtrLoanCont> selectByModelCtr(QueryModel model);

    /**
     * @方法名称: selectWorryFreeLoanContinfo
     * @方法描述: 列表查询方法
     * @参数与返回说明:
     * @算法描述: 根据合同号查询省心快贷查封查验中符合条件的合同
     */
    List<CtrLoanCont> selectWorryFreeLoanContinfo(QueryModel model);

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CtrLoanCont record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CtrLoanCont record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CtrLoanCont record);

    /**
     * @方法名称: updateContStatus
     * @方法描述: 根据主键更新合同状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateContStatus(CtrLoanCont record);

    /**
     * @方法名称: updateContStatu
     * @方法描述: 根据申请流水号更新操作状态
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateContStatu(String iqpSerno);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CtrLoanCont record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("contNo") String contNo);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    int deleteByIds(@Param("ids") String ids);

    /**
     * @方法名称: querySxFdeddCtrloanConNumberBycusId
     * @方法描述: 查找出该客户是存在生效的房抵e点贷最高额合同
     * @参数与返回说明:
     * @算法描述: 无
     */

    int querySxFdeddCtrloanConNumberBycusId(HashMap<String, String> paramMap);

    /**
     * @方法名称: queryWqdFdeddCtrloanConNumberBycusId
     * @方法描述: 查找出该客户是存在未签订的房抵e点贷最高额合同
     * @参数与返回说明:
     * @算法描述: 无
     */

    int queryWqdFdeddCtrloanConNumberBycusId(HashMap<String, String> paramMap);

    /**
     * @方法名称: selectSxNotXfCtrLoanContCountByCusId
     * @方法描述: 根据客户号查询是否存在非消费类生效的贷款合同
     * @参数与返回说明:
     * @算法描述: 无
     */

    int selectSxNotXfCtrLoanContCountByCusId(HashMap<String, String> paramMap);

    /**
     * 通过入参查询合同信息
     *
     * @param params
     * @return
     */
    List<CtrLoanCont> selectCtrLoanContByParams(Map params);


    /**
     * @方法名称: selectDoubleViewFromCtrLoanCont
     * @方法描述: 根据条件查询h合同的双录信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<DoubleViewDto> selectDoubleViewFromCtrLoanCont(Map queryMap);

    /**
     * 查询引入入参的第三方额度的生效、待生效的合同数据
     *
     * @param params
     * @return
     */
    List<CtrLoanCont> selectCtrLoanContByThLimitId(Map params);

    /**
     * 更新合同状态
     *
     * @param
     * @return
     */

    int updateByContNoKey(Map params);

    /**
     * 获取待签合同信息
     *
     * @param userId
     * @return
     */
    List<BizCtrLoanContDto> selectCtrLoanContByCorreManagerId(@Param("userId") String userId);

    /**
     * 业务统计-笔数
     *
     * @param map
     * @return
     */
    List<Map> selectBusRows(Map map);

    /**
     * 业务统计-金额（万元）
     *
     * @param map
     * @return
     */
    List<Map> selectBusAmt(Map map);


    /**
     * 根据协议编号 获取下属所有合同编号
     *
     * @param lmtctrno
     * @return
     */
    List<String> getContNumbers(String lmtctrno);

    /**
     * 注销合作方授信协议  校验逻辑  返回生效状态的合同数量
     *
     * @param lmtCtrNo
     * @return
     */
    Integer getCount(String lmtCtrNo);

    /**
     * 通过授信台账编号 获取合同信息
     *
     * @param map
     * @return
     */
    List<BizCtrLoanContDto> getContDataByLmtLimitNo(Map map);

    /**
     * 交易描述：优企贷共借人、合同信息查询
     *
     * @param commonCertNo
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdxw0013.resp.List> selectCtrLoanContsByCommonCertNo(@Param("commonCertNo") String commonCertNo);

    /**
     * 交易描述：根据客户号前往信贷查找房贷借据信息
     *
     * @param cusId
     * @return
     */
    Xdtz0041DataRespDto getHouseLoanByCusId(String cusId);

    /**
     * 根据客户号前往信贷查找房贷借据信息
     *
     * @param cusId
     * @return
     */
    Xdtz0044DataRespDto getHouseLoanContByCusId(String cusId);

    /**
     * 获取合同信息
     *
     * @param indgtSerno
     * @return
     */
    Xdht0014DataRespDto getContSignInfo(@Param("indgtSerno") String indgtSerno);

    /**
     * 获取可用余额
     *
     * @param contNo
     * @return
     */
    BigDecimal calAvlLmt(@Param("contNo") String contNo);

    /**
     * 根据客户证件号查询合同状态
     *
     * @param certCode
     * @return
     */
    HashMap<String, Object> selectContStatuByCode(@Param("certCode") String certCode);

    /**
     * @return
     * @方法名称: selectByQryCode
     * @方法描述: 通过查询报表编号查询子表的参数配置
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<CtrLoanContDto> selectCtrLoanContWithPvpLoanApp(QueryModel model);
    /**
     * @return
     * @方法名称: selectCtrLoanContAll
     * @方法描述: 贷款出账申请关联合同信息 (保函对公,零售,小微)
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<CtrLoanContDto> selectCtrLoanContAll(QueryModel model);


    /**
     * @return
     * @方法名称: getContInfobyPvpContNo
     * @方法描述: 根据贷款出账表合同号获取合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    CtrLoanCont getContInfobyPvpContNo(@Param("contNo") String contNo);

    /**
     * @return
     * @方法名称: queryAllImageCont
     * @方法描述: 查询合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<CtrLoanContDto> queryAllImageCont(QueryModel model);

    /**
     * @return
     * @方法名称: getContInfobyreplySerno
     * @方法描述: 查询批复下是否存在有效合同
     * @参数与返回说明:
     * @算法描述: 无
     */

    int getContInfobyreplySerno(@Param("replySerno") String replySerno);

    /**
     * 交易描述：合同信息查看
     *
     * @param QueryMap
     * @return
     * @throws Exception
     */
    cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020DataRespDto queryCtrLoanContdetails(Map QueryMap);

    /**
     * 根据核心客户号查询我行信用类合同金额汇总
     *
     * @param queryModel
     * @return
     */
    BigDecimal queryApplyAmount(QueryModel queryModel);

    /**
     * 查询最近生效的一笔优企贷合同金额
     *
     * @param queryModel
     * @return
     */
    BigDecimal queryLastYqdContAmt(QueryModel queryModel);

    /**
     * 根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
     *
     * @param queryModel
     * @return
     */
    BigDecimal querySurveyContLoanAmt(QueryModel queryModel);

    /**
     * 根据核心客户号查询经营性贷款关联生效合同的批复额度
     *
     * @param queryModel
     * @return
     */
    BigDecimal queryOperatingContAmt(QueryModel queryModel);

    /**
     * 根据核心客户号查询经营性贷款关联生效合同的批复额度
     *
     * @param queryModel
     * @return
     */
    BigDecimal queryOperatingContPfAmt(QueryModel queryModel);

    /**
     * 查询贸易融资/福费廷合同列表
     *
     * @param param
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdht0004.resp.List> getContInfoByCusIdAndBizType(Map param);

    /**
     * 根据合同号查询客户经理的电话号码
     *
     * @param queryMap
     * @return
     */
    String queryCtrLoanContManagerIdByContNo(Map queryMap);

    /**
     * 根据合同类型查找贷款合同信息
     *
     * @param xdht0005DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdht0005.resp.List> getContInfoByContType(Xdht0005DataReqDto xdht0005DataReqDto);

    /**
     * 交易描述：优农贷合同信息查询（个人）
     *
     * @param cusId
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdht0042.resp.List> queryYqdCtrLoanContdetails(@Param("cusId") String cusId);

    /**
     * 交易描述：优农贷合同信息查询（公司）
     *
     * @param cusIds
     * @return
     * @throws Exception
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdht0042.resp.List> queryComYqdCtrLoanContdetails(@Param("cusIds") String cusIds);

    /**
     * 根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
     *
     * @param queryModel
     * @return
     */
    BigDecimal queryLastContAmt(QueryModel queryModel);

    /**
     * 根据核心客户号查询最近生效的一笔优农贷合同金额
     *
     * @param queryModel
     * @return
     */
    BigDecimal queryLastYndContAmt(QueryModel queryModel);

    /**
     * 查询优企贷贷款合同信息一览
     *
     * @param queryModel
     * @return
     */
    List queryYqdCtrloanContDetailsByCusId(QueryModel queryModel);

    /**
     * 查询优农贷贷款合同信息一览
     *
     * @param queryModel
     * @return
     */
    List queryYndCtrloanContDetailsByCusId(QueryModel queryModel);


    /**
     * 合同信息列表查询
     *
     * @param QueryMap
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdht0022.resp.List> queryCtrloanContDetailsByCusId(Map QueryMap);

    /**
     * 合同信息列表查询总记录条数
     *
     * @param QueryMap
     * @return
     */
    int queryCtrloanContAllNUmByCusId(Map QueryMap);

    /**
     * 根据证件号码查询客户是否是房群客户
     *
     * @param queryMap
     * @return
     */
    int queryLoanHouseFromCmis(Map queryMap);

    /**
     * 根据证件号码查询客户是否是房群客户(排除已结清)
     *
     * @param queryMap
     * @return
     */
    int queryNormalLoanHouseFromCmis(Map queryMap);

    /**
     * 根据流水号查询是否放款标记
     *
     * @param queryModel
     * @return
     */
    int queryishaveLoanRecordBySerno(QueryModel queryModel);

    /**
     * 合同详情查看
     *
     * @param queryMap
     * @return
     */
    Xdht0024DataRespDto getContDetail(Map queryMap);

    /**
     * 查询客户我行合作次数
     *
     * @param certCode
     * @return
     */
    int getCoopTimesByCertCode(String certCode);

    /**
     * 查询借款合同列表
     *
     * @param xdht0007DataReqDto
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdht0007.resp.List> getCrtContList(Xdht0007DataReqDto xdht0007DataReqDto);

    /**
     * 查询借款合同列表
     *
     * @param contNo
     * @return
     */
    List<cn.com.yusys.yusp.dto.server.xdht0007.resp.List> getCrtContByContNo(String contNo);

    /**
     * 根据申请人证件号码汇总客户及配偶信用类小微业务贷款授信金额
     *
     * @param queryMap
     * @return
     */
    BigDecimal queryCusAndSpouseForCreditCrdLoan(Map queryMap);

    /**
     * 乐悠金根据核心客户号查询房贷首付款比例进行额度计算
     *
     * @param xdht0038DataReqDto
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdht0038.resp.List> getFirstpayByCusId(Xdht0038DataReqDto xdht0038DataReqDto);

    /**
     * @方法名称: queryCtrForftinContDataByParams
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    CtrLoanCont queryCtrForftinContDataByParams(HashMap<String, String> params);

    CtrLoanCont selectContByIqpSerno(@Param("iqpSerno") String iqpSerno);


    /**
     * 检查该客户下是否存在有效的合同
     *
     * @param params
     * @return
     */
    Integer queryNormalContByCusId(HashMap<String, String> params);

    /**
     * 检查该客户下是否存在失效的合同
     *
     * @param params
     * @return
     */
    List<CtrLoanCont> queryUnuseContByCusId(HashMap<String, String> params);

    /**
     * 根据流水号查询
     *
     * @param iqpSerno
     * @return
     */
    CtrLoanCont selectBySerno(@Param("iqpSerno") String iqpSerno);

    /**
     * @方法名称: selectByLmtAccNo
     * @方法描述: 根据台账号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    List<CtrLoanCont> selectByLmtAccNo(@Param("lmtAccNo") String lmtAccNo);

    /**
     * @创建人 WH
     * @创建时间 2021/5/31 22:16
     * @注释 根据授信编号查在途中的合同和申请信息
     */
    List<CtrLoanContAndPvp> selectContAndPvp(@Param("iqpSerno") String iqpSerno);

    /**
     * @方法名称: updateDoubleRecordNoByContNo
     * @方法描述: 根据合同编号更新双录编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    int updateDoubleRecordNoByContNo(@Param("contNo") String contNo, @Param("docNo") String docNo);

    /**
     * 优企贷共借人签订
     *
     * @param xdxw0014DataReqDto
     * @return
     */
    int updateSignStatByCommonCertNo(Xdxw0014DataReqDto xdxw0014DataReqDto);

    /**
     * 普通贷款合同注销
     *
     * @param record
     * @return
     */
    int updateByPrimaryKeylogout(CtrLoanCont record);

    /**
     * 获取合同金额apply_amount loan_end_date，根据请求的合同编号CONT_NO
     *
     * @param cont_no
     * @return
     */
    Map<String, Object> getContData(@Param("contNo") String cont_no);

    /**
     * 根据客户号获取合同列表
     *
     * @param cusId
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdxw0015.resp.ListCont> selectListContByCusId(@Param("cusId") String cusId, @Param("prdId") String prdId);

    /**
     * 获取合同签订所需字段
     *
     * @param contNo
     * @return
     */
    Map<String, Object> getSignContInfo(String contNo);

    /**
     * 根据合同号更新合同表
     *
     * @param queryMap
     * @return
     */
    int updateByContNo(Map queryMap);

    /**
     * 推送微银失败,还原合同状态-未签订cont_status=‘100’
     * @param contNo
     * @return
     */
    int updateforContStatusByContNo(String contNo);

    /**
     * 查询信贷系统请求小V平台推送合同信息接口所需参数
     *
     * @param contNo
     * @return
     */
    Wxd009ReqDto getWxd009RedDto(String contNo);

    /**
     * 根据合同号更新合同状态为300
     *
     * @param contNo
     */
    int setContStatus300ByContNo(String contNo);

    /**
     * 根据合同号获取bizType
     *
     * @param loan_cont_no
     * @return
     */
    String getBizTypeByContNo(@Param("contNo") String loan_cont_no);

    /**
     * 查询工资贷和公积金贷额度信息
     *
     * @param params
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdls0006.resp.List> selectAmountByCertNo(HashMap<String, String> params);

    /**
     * 查询工薪贷额度信息
     *
     * @param params
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdls0006.resp.List> selectWagesAmountByCertNo(HashMap<String, String> params);

    /**
     * 根据合同号查询责任机构号
     *
     * @param queryMap
     * @return
     */
    String queryCtrLoanContManagerBrIdByContNo(Map queryMap);

    /**
     * 查询白领贷额度信息
     *
     * @param params
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdls0006.resp.List> selectWhiteCollarAmountByCertNo(HashMap<String, String> params);

    /**
     * 根据证件号码和产品号查询合同号
     *
     * @param queryMap
     * @return
     */
    String queryContNoByCertNo(Map queryMap);

    /**
     * 根据合同号查询额度信息
     *
     * @param params
     * @return
     */
    java.util.List<cn.com.yusys.yusp.dto.server.xdls0006.resp.List> selectAmountInfoByContNo(HashMap<String, String> params);

    CtrLoanCont selectContByContno(String loanContNo);

    /**
     * 根据合同号更新借款合同或担保合同签约状态
     *
     * @param queryMap
     * @return
     */
    int updateContStatusByContNo(Map queryMap);

    /**
     * 根据合同号查询合同状态
     *
     * @param contNo
     * @return
     */
    String queryContStatusByContNo(String contNo);

    /**
     * 根据客户号查询是否存在生效的贷款合同
     *
     * @param cusId
     * @return
     */
    BigDecimal checkCusSxContByCusId(String cusId);

    /**
     * 交易描述：优农贷合同信息查看
     *
     * @param QueryMap
     * @return
     * @throws Exception
     */
    cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020DataRespDto queryCtrLoanContdetailsFor5(Map QueryMap);

    /**
     * 交易描述：优抵贷合同信息查看
     *
     * @param QueryMap
     * @return
     * @throws Exception
     */

    cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020DataRespDto queryCtrLoanContdetailsFor6(Map QueryMap);
    /**
     * 交易描述：合同信额度息查看
     *
     * @param QueryMap
     * @return
     * @throws Exception
     */
    cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020LmtDto queryCtrLoanLmtByContTypeFor1(Map QueryMap);

    /**
     * 交易描述：合同信额度息查看
     *
     * @param QueryMap
     * @return
     * @throws Exception
     * */
    cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020LmtDto queryCtrLoanLmtByContTypeFor2(Map QueryMap);

    /**
     * 交易描述：合同信额度息查看
     *
     * @param QueryMap
     * @return
     * @throws Exception
     * */
    cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020LmtDto queryCtrLoanLmtByContTypeFor3(Map QueryMap);

    /**
     * 交易描述：查询优享贷的可用余额
     *
     * @param contNo
     * @return
     * @throws Exception
     */
    String queryZxdCtrLoanBalanceByContType(@Param("contNo") String contNo);


    /**
     * 交易描述：合同信额度息查看
     *
     * @param QueryMap
     * @return
     * @throws Exception
     * */
    cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020LmtDto queryCtrLoanLmtByContNo(Map QueryMap);

    /**
     * @方法名称: selectStartdaysByContNo
     * @方法描述: 查询合同起始日开始日
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectStartdaysByContNo(Map queryMap);

    /**
     * 惠享贷共借人身份证查询主借款合同
     *
     * @param certCode
     * @return
     */
    List<HxdLoanContList> selectLoanContByCertNo(@Param("certCode") String certCode);

    /**
     * 保证人担保合同查询
     *
     * @param queryMap
     * @return
     */
    List<GuarContList> selectGuarLoanContByCertNo(Map queryMap);

    /**
     * 担保合同是否签约查询
     *
     * @param queryMap
     * @return
     */
    int selectSignStateGuarLoanContByCertNo(Map queryMap);

    /**
     * 查询是否存在已签约的担保合同
     *
     * @param queryMap
     * @return
     */
    int selectSignStateGuarLoanContByCertNo2(Map queryMap);

    /**
     * 保证人担保合同查询
     *
     * @param queryMap
     * @return
     */
    List<LoanContList> selectLoanContListByCertNo(Map queryMap);
    /**
     * 查授信台账号对应贷款合同申请
     * @param lmtAccNo
     * @return
     */
    String selectCtrByLmtAccNo(String lmtAccNo);


    /**
     * 验证该合同是否被其他借款人占用
     *
     * @param ctrLoanContMap
     * @return
     */
    int countCtrLoanContByMap(Map ctrLoanContMap);

    /**
     * 根据借款合同号查询调查流水号
     *
     * @param contNo
     * @return
     */
    String querySurveySernoByContNo(String contNo);

    /**
     * @Description:根据客户号查询是否存在业务信息
     * @Author: YX-WJ
     * @Date: 2021/6/9 23:40
     * @param cusId: 客户号
     * @return: int
     **/
    int selectContByCusId(@Param("cusId") String cusId);

    /**
     * 使用授信协议编号查询贷款合同表ctr_loan_cont，判断是否只存在结清或者注销的线上房抵e点贷合同，不存在生效的线上房抵e点贷合同
     *若是则返回1否则返回0
     * @param lmtAccNo
     * @return
     */
    String checkCusSxContByAccSubNo(String lmtAccNo);


    /**
     * @方法名称：getCtrContByCusIdAndBizType
     * @方法描述：根据合同编号查询借据信息

     */
    java.util.List<CtrLoanCont> getCtrContByCusIdAndBizType(Map param);

    /**
     * @方法名称：getCtrContByCusIdAndBizType
     * @方法描述：根据合同编号查询借据信息

     */
    CtrLoanCont getCtrContByParam(Map param);


    /**
     * 根据调查流水查询合同数量
     * @param surveySerno
     * @return
     */
    int queryCtrLoanContCountBySurveySerno(@Param("surveySerno") String surveySerno);

    /**
     * 根据合同号查询合同表中合同状态，客户号，主管机构
     * @param contNo 合同号
     * @return 合同信息（合同状态，客户号，主管机构）
     */
    CtrLoanCont queryCtrLoanContInfoByContNo(@Param("contNo") String contNo);

    /**
     * @Description:根据业务流水号查询授信调查是否已创建合同
     * @param surveySerno:
     * @return: int
     **/
    int selectBySurveySerno(@Param("surveySerno") String surveySerno);

    /**
     * 根据核心客户号查询经营性贷款合同额度
     *
     * @param cusId
     * @return
     */
    Xdht0031DataRespDto queryOperatingLoanContAmt(String cusId);

    /**
     * 根据日期查询合同数量
     * 根据日期查询未签订合同数量
     * 根据日期查询未签订合同数量(关联授信批复)
     * @param map
     * @return
     */
    public int quertContSum(Map map) ;
    public int quertUnSignContSum(Map map) ;
    public int quertUnSignContSumByPf(Map map) ;


    /**
     * 根据日期查询未签订合同数量(关联授信批复)
     * @param map
     * @return
     */
    public int quertOtherPrdIdContSum(Map map);

    /**
     * 根据日期查询生效合同信息
     * 根据日期查询生效合同信息(关联授信批复)
     * @param map
     * @return
     */
    public HashMap quertContByDate(Map map) ;
    public HashMap quertContByDateByPf(Map map) ;
    /**
     * 根据客户号和业务类型，查询合同编号、担保合同号、押品编号、押品大类型
     *
     * @param queryMap
     * @return
     */
    cn.com.yusys.yusp.dto.server.xdht0015.resp.List selectCtrLoanContAndGrtGuarContByCustId(Map queryMap);

    /**
     * @Description:根据借据编号查询产品性质
     * @Author: YD
     * @Date: 2021/6/13 18:53
     * @param billNo: 台账编号
     * @return: java.lang.String
     **/
    String selectProductByBillNo(@Param("billNo") String billNo);

    /**
     * 根据客户号、业务类型、合同状态查询非小微业务经营性贷款总金额
     * @param queryMap
     * @return
     */
    BigDecimal selectCtrLoanContTotalAmtByCusIdAndBizType(Map queryMap);

    /**
     * @函数名称:selectNumByInputId
     * @函数描述:根据客户经理工号查询待处理数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    List<Map<String, Object>> selectNumByInputId(QueryModel model);


    /**
     * 关联优企贷续贷申请名单信息查询原合同号
     * @param queryMap
     * @return
     */
    Map<String,String> selectOldContNofromCtrLoanCont(Map queryMap);

    /**
     * @函数名称:getAllContByInputId
     * @函数描述:根据客户经理工号查询合同待签订数据
     * @参数与返回说明:
     * @算法描述:
     * @修改人:
     */
    List<Map<String, Object>> getAllContByInputId(QueryModel model);

    /**
     * 根据客户号查询已签订的合同
     * @param cusId
     */
    List<CtrLoanCont> getselectByCusIdOne(@Param("cusId")String cusId);

    /**
     *
     * @param queryMap
     * @return
     */
    String getSurveySerno(Map queryMap);
    /**
     * 查询是否存在有效合同
     */
    int selectEffectiveContByCusId(@Param("cusId")String cusId,@Param("prdId") String prdId);

    /**
     *查询押品对应的核销合同记录数
     * @param guarNo
     * @return
     */
    int countHxCtrLoanContRecords(@Param("guarNo")String guarNo);

    /**
     * 查询押品对应的合同状态
     * @param guarNo
     * @return
     */
    String selectContStatusByGuarNo(@Param("guarNo")String guarNo);

    /**
     * @方法名称: selectSxkdContList
     * @方法描述: 获取省心快贷合同信息
     * @参数与返回说明:
     * @算法描述: 无
     * @创建人：zl
     */
    List<CtrLoanCont> selectSxkdContList(QueryModel model);

    /*
     * selectNumByInputId 数据统计拆分
     * selectView200num 单一客户授信
     *
     * */
    Integer selectView200num(QueryModel model);

    /*
     * selectNumByInputId 数据统计拆分
     * selectView201num 集团客户授信
     *
     * */
    Integer selectView201num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView202num 同业机构准入年审
     *
     * */
    Integer selectView202num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView203num 成员客户申报
     *
     * */
    Integer selectView203num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView300num 最高额授信协议
     *
     * */
    Integer selectView300num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView301num 普通贷款合同
     *
     * */
    Integer selectView301num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView302num 贴现协议
     *
     * */
    Integer selectView302num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView303num 贸易融资合同
     *
     * */
    Integer selectView303num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView304num 福费廷合同
     *
     * */
    Integer selectView304num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView305num 开证合同
     *
     * */
    Integer selectView305num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView306num 保函合同
     *
     * */
    Integer selectView306num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView307num 银承合同
     *
     * */
    Integer selectView307num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView308num 委托贷款合同
     *
     * */
    Integer selectView308num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView309num 零售业务合同
     *
     * */
    Integer selectView309num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView400num 贷款出账通知
     *
     * */
    Integer selectView400num(QueryModel model);
    /*
     * selectNumByInputId 数据统计拆分
     * selectView401num 银承出账通知
     *
     * */
    Integer selectView401num(QueryModel model);/*
     * selectNumByInputId 数据统计拆分
     * selectView402num 委托贷款出账通知
     *
     * */
    Integer selectView402num(QueryModel model);

    List<CtrLoanCont> selectByGuarContNo(@Param("guarContNo")String guarContNo);

    /**
     * @Description:根据产品号，身份证号查询是否有有效合同
     * @param certCode:
     * @param prdId:
     * @return: int
     **/
    int checkByPrdIdAndCertCode(@Param("certCode") String certCode,@Param("prdId") String prdId);

    /**
     * 通过入参查询合同信息
     * @param model
     * @return
     */
    List<CtrLoanCont> queryCtrLoanListByCusId(QueryModel model);

    /**
     * 根据担保合同编号查询对应的借款合同的所属条线
     * @param guarContNo
     * @return
     */
    String selectBelgLineByGuarContNo(@Param("guarContNo") String guarContNo);

    String judgeQFBySerno(String serno);

    /**
     * 根据客户号查所有零售按揭贷款金额
     * @param cusId
     * @return
     */
    BigDecimal selectContAmtAj(@Param("cusId")String cusId);

    /**
     * 根据客户号查所有零售非按揭贷款金额
     * @param param
     * @return
     */
    BigDecimal selectContAmtFaj(Map param);

    /**
     * 查询省心快贷合同
     * @param param
     * @return
     */
    List<CtrLoanCont> getsxkdCont(@Param("param") Map param);

    /**
     * @return
     * @方法名称: selectOtherThingsAppLoan
     * @方法描述: 查询申请信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    List<Map> selectOtherThingsAppLoan(QueryModel model);

    /**
     * @return
     * @方法名称: selectContNoByLmtAccNo
     * @方法描述: 根据授信额度编号查询关联的所有非否决非自行退出的合同编号
     * @参数与返回说明:
     * @算法描述: 无
     */
    String selectContNosByLmtAccNo(@Param("lmtAccNo") String lmtAccNo);

    /**
     * 查询借款人名下未结清的合同信息（最高额授信协议、普通贷款合同、贸易融资合同、开证合同、保函合同、银承合同）
     * @param cusId
     * @return
     */
    List<CtrLoanCont> selectUnClearContByCusId(@Param("cusId") String cusId);

    /**
     * 根据合同编号列表查询合同信息（最高额授信协议、普通贷款合同、贸易融资合同、开证合同、保函合同、银承合同）
     * @param contNos
     * @return
     */
    List<CtrLoanCont> selectByContNos(@Param("contNos") String contNos);


    /**
     * 查询授信续作合同
     * @param lmtAccNo
     * @return
     */
    List<CtrLoanContDto> queryAllContByLmtAccNo(@Param("lmtAccNo") String lmtAccNo);

    /**
     * 根据合同编号查询产品编号
     * @param contNo
     * @return
     */
    String selectPrdIdByContNo(@Param("contNo") String contNo);

    /**
     * 根据客户号查所有零售白领贷贷款金额
     * @param param
     * @return
     */
    BigDecimal selectContAmtFajBld(Map param);

    /**
     * 根据额度编号查询合同金额总和
     *
     * @param lmtAccNo
     * @return
     */
    BigDecimal getSumContAmt(String lmtAccNo);

    /**
     * 根据合同编号查询合同信息
     *
     * @param queryMap
     * @return
     */
    CtrLoanContDto queryAllCont(Map queryMap);

    /**
     * 根据合同编号查询合同申请信息
     *
     * @param queryMap
     * @return
     */
    IqpLoanAppDto queryAllContApp(Map queryMap);

    /**
     * 根据客户号产品编号查询固定资产信息
     * @param model
     * @return
     */
    List<CtrLoanCont> selectCtrLoanContByCusId(QueryModel model);

    /**
     * 根据合同号集合查询线上合同启用标识
     * @param contNos
     * @return
     */
    List<Map> getCtrBeginFlagByContNos(@Param("list") List<String> contNos);

    /**
     * 根据合同号查询
     * @param contNo
     * @return
     */
    Map<String, String> selectMapByContNo(@Param("cotnNo")String contNo);
}