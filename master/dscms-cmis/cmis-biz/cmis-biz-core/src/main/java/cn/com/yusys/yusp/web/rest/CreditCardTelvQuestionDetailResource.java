/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardTelvQuestionDetail;
import cn.com.yusys.yusp.service.CreditCardTelvQuestionDetailService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardTelvQuestionDetailResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:44:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "电话调查问题明细信息")
@RestController
@RequestMapping("/api/creditcardtelvquestiondetail")
public class CreditCardTelvQuestionDetailResource {
    @Autowired
    private CreditCardTelvQuestionDetailService creditCardTelvQuestionDetailService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardTelvQuestionDetail>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardTelvQuestionDetail> list = creditCardTelvQuestionDetailService.selectAll(queryModel);
        return new ResultDto<List<CreditCardTelvQuestionDetail>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CreditCardTelvQuestionDetail>> index(QueryModel queryModel) {
        List<CreditCardTelvQuestionDetail> list = creditCardTelvQuestionDetailService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardTelvQuestionDetail>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<CreditCardTelvQuestionDetail> show(@PathVariable("pkId") String pkId) {
        CreditCardTelvQuestionDetail creditCardTelvQuestionDetail = creditCardTelvQuestionDetailService.selectByPrimaryKey(pkId);
        return new ResultDto<CreditCardTelvQuestionDetail>(creditCardTelvQuestionDetail);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("问题信息表单新增")
    @PostMapping("/")
    protected ResultDto<CreditCardTelvQuestionDetail> create(@RequestBody CreditCardTelvQuestionDetail creditCardTelvQuestionDetail) throws URISyntaxException {
        creditCardTelvQuestionDetailService.insert(creditCardTelvQuestionDetail);
        return new ResultDto<CreditCardTelvQuestionDetail>(creditCardTelvQuestionDetail);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardTelvQuestionDetail creditCardTelvQuestionDetail) throws URISyntaxException {
        int result = creditCardTelvQuestionDetailService.update(creditCardTelvQuestionDetail);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = creditCardTelvQuestionDetailService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardTelvQuestionDetailService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param  list
     * @return ResultDto
     * @author wzy
     * @date 2021/5/26 10:27
     * @version 1.0.0
     * @desc 信用卡审批问题信息保存
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("信用卡审批问题信息保存")
    @PostMapping("/savequestiondetail")
    protected ResultDto<Integer> saveQuestionAndOther(@RequestBody List<CreditCardTelvQuestionDetail> list) {
        int result = creditCardTelvQuestionDetailService.saveQuestionAndOther(list);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param queryModel
     * @return ResultDto
     * @author wzy
     * @date 2021/5/31 10:50
     * @version 1.0.0
     * @desc 根据条件进行分页查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据条件进行分页查询")
    @PostMapping("/querybymodel")
    protected ResultDto<List<CreditCardTelvQuestionDetail>> queryByModel(@RequestBody QueryModel queryModel) {
        List<CreditCardTelvQuestionDetail> list = creditCardTelvQuestionDetailService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardTelvQuestionDetail>>(list);
    }
}
