package cn.com.yusys.yusp.service.server.xdzc0003;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.BailAccInfo;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.domain.GrtGuarCont;
import cn.com.yusys.yusp.domain.LmtReplyAcc;
import cn.com.yusys.yusp.dto.server.xdzc0003.req.Xdzc0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0003.resp.Xdzc0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.BailAccInfoMapper;
import cn.com.yusys.yusp.repository.mapper.CtrAsplDetailsMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.service.AsplAssetsListService;
import cn.com.yusys.yusp.service.GrtGuarBizRstRelService;
import cn.com.yusys.yusp.service.LmtReplyAccService;
import org.apache.ibatis.javassist.bytecode.stackmap.BasicBlock;
import org.bouncycastle.pqc.math.linearalgebra.BigEndianConversions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:通过协议编号查询具体的协议详情信息
 *
 * @Author xs
 * @Date 2021/06/02 16:20
 * @Version 1.0
 */
@Service
public class Xdzc0003Service {

    @Autowired
    private CtrAsplDetailsMapper ctrAsplDetailsMapper;
    @Autowired
    private BailAccInfoMapper bailAccInfoMapper;

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    @Autowired
    private LmtReplyAccService lmtReplyAccService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0003.Xdzc0003Service.class);

    @Transactional
    public Xdzc0003DataRespDto xdzc0003Service(Xdzc0003DataReqDto xdzc0003DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value);
        Xdzc0003DataRespDto xdzc0003DataRespDto = new Xdzc0003DataRespDto();
        String contNo = xdzc0003DataReqDto.getContNo();//协议编号
        try {
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsMapper.selectCtrAsplDetailsInfoByContNo(contNo);
            if(Objects.isNull(ctrAsplDetails)){
                throw BizException.error(null,"9999","信贷系统未查询到,资产池："+contNo );
            }
            // 判断是否迁移数据，需要拦截重新签订协议
            if(Objects.equals(ctrAsplDetails.getContNo().substring(0,3),"PJC")){
                throw BizException.error(null,"9999","协议合同需要重新签订,资产池："+contNo );
            }
            //校验资产池协议到期日
            String endate = ctrAsplDetails.getEndDate();
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            int res = DateUtils.compare(DateUtils.parseDate(openDay,DateFormatEnum.DEFAULT.getValue()),DateUtils.parseDate(endate,DateFormatEnum.DEFAULT.getValue()));
            if(res == 1){
                throw BizException.error(null,"9999","资产池协议已经到期,资产池："+contNo + "到期日:" + endate);
            }
            xdzc0003DataRespDto.setContNo(ctrAsplDetails.getContNo());// 协议编号
            xdzc0003DataRespDto.setContCNNO(ctrAsplDetails.getContCnNo());// 中文协议编号
            xdzc0003DataRespDto.setCusId(ctrAsplDetails.getCusId());// 客户编号
            xdzc0003DataRespDto.setCusName(ctrAsplDetails.getCusName());// 客户名称
            xdzc0003DataRespDto.setPrdId(ctrAsplDetails.getPrdId());// 产品编号
            xdzc0003DataRespDto.setPrdName(ctrAsplDetails.getPrdName());// 产品名称
            xdzc0003DataRespDto.setGrtMode(ctrAsplDetails.getGuarMode());// 担保方式
            xdzc0003DataRespDto.setConrCurType(ctrAsplDetails.getCurType());// 合同币种
            xdzc0003DataRespDto.setContAmt(ctrAsplDetails.getContAmt());// 合同金额
            xdzc0003DataRespDto.setContTerm(ctrAsplDetails.getContTerm());// 合同期限
            xdzc0003DataRespDto.setStartDate(ctrAsplDetails.getStartDate());// 起始日期
            xdzc0003DataRespDto.setEndDate(ctrAsplDetails.getEndDate());// 到期日期
            xdzc0003DataRespDto.setContSignDate(ctrAsplDetails.getContSignDate());// 签订日期
            xdzc0003DataRespDto.setContStatus(ctrAsplDetails.getContStatus());// 合同状态
            xdzc0003DataRespDto.setLowRiskLmt(ctrAsplDetails.getLowRiskAmt());// 低风险授信额度
            xdzc0003DataRespDto.setCommenRiskLmt(ctrAsplDetails.getCommonRiskAmt());// 一般风险授信额度
            xdzc0003DataRespDto.setSupshAgrAmt(ctrAsplDetails.getSupshAgrAmt());// 超短贷协议金额
            // 资产池保证金账户余额
            BigDecimal assetPoolBailAmt = asplAssetsListService.getAssetPoolBailAmt(ctrAsplDetails);
            // 资产池融资额度
            BigDecimal assetPoolFinAmt = asplAssetsListService.getAssetPoolFinAmt(ctrAsplDetails,assetPoolBailAmt);
            logger.info("资产池"+contNo+", 池融资额度:"+String.valueOf(assetPoolFinAmt));
            xdzc0003DataRespDto.setAssetPoolFinAmt(assetPoolFinAmt);// 资产池融资额度
            // 可用池融资额度
            BigDecimal assetPoolAvaAmt = asplAssetsListService.getAssetPoolAvaAmt(ctrAsplDetails,assetPoolFinAmt,assetPoolBailAmt);
            logger.info("资产池"+contNo+", 可用池融资额度:"+String.valueOf(assetPoolAvaAmt));
            xdzc0003DataRespDto.setAssetPoolAvaAmt(assetPoolAvaAmt);// 可用资产池融资额度

            xdzc0003DataRespDto.setRateAdjMode(ctrAsplDetails.getRateAdjMode());// 利率调整方式
            xdzc0003DataRespDto.setPrPriceInterval(ctrAsplDetails.getLprPriceInterval());// LPR定价区间
            xdzc0003DataRespDto.setRateFloatPoint(ctrAsplDetails.getRateFloatPoint());// 浮动点数
            xdzc0003DataRespDto.setCurtLprRate(ctrAsplDetails.getCurtLprRate());// 当前LPR利率
            xdzc0003DataRespDto.setExecRateYear(Optional.ofNullable(ctrAsplDetails.getExecRateYear()).orElse(BigDecimal.ZERO).setScale(4,BigDecimal.ROUND_DOWN));// 执行年利率
            xdzc0003DataRespDto.setOverdueExecRate(ctrAsplDetails.getOverdueExecRate());// 逾期执行年利率
            xdzc0003DataRespDto.setCiExecRate(ctrAsplDetails.getCiExecRate());// 复息执行年利率
            xdzc0003DataRespDto.setChgrRate(Optional.ofNullable(ctrAsplDetails.getChrgRate()).orElse(BigDecimal.ZERO).setScale(4,BigDecimal.ROUND_DOWN));// 手续费率
            xdzc0003DataRespDto.setPadRateYear(ctrAsplDetails.getPadRateYear());// 垫款利率
            xdzc0003DataRespDto.setAcctNo(ctrAsplDetails.getAcctNo());// 账号
            xdzc0003DataRespDto.setAcctName(ctrAsplDetails.getAcctName());// 账号名称

            String serno = ctrAsplDetails.getSerno();//获取申请流水号
            // 根据合同编号查询合同业务关系 查询担保合同
            String guarContNo = grtGuarBizRstRelService.selectGuarContNoBycontNo(ctrAsplDetails.getContNo());
            xdzc0003DataRespDto.setGuarContNo(guarContNo);// 担保合同编号
            //xdzc0003DataRespDto.setGuarContName(grtGuarCont.getGuarContCnNo());// （担保合同中文编号）担保合同名称

            // 根据申请流水号查询对应的保证金账户信息
            List<BailAccInfo> bailAccInfoList = bailAccInfoMapper.selectBySerno(serno);
            // 资产池的保证金账号是为维一的
            if (CollectionUtils.nonEmpty(bailAccInfoList)) {
                BailAccInfo bailAccInfo = bailAccInfoList.get(0);
                xdzc0003DataRespDto.setBailAccNo(bailAccInfo.getBailAccNo());// 保证金账号
                xdzc0003DataRespDto.setAcctsvcrNo(bailAccInfo.getAcctsvcrNo());// 保证金账号开户行
                xdzc0003DataRespDto.setBailAccNoSubSeq(bailAccInfo.getBailAccNoSub());// 保证金账号子序号
                xdzc0003DataRespDto.setBailCurType(bailAccInfo.getBailCurType());// 保证金币种
                xdzc0003DataRespDto.setBailRate(Optional.ofNullable(bailAccInfo.getBailRate()).orElse(BigDecimal.ZERO).setScale(4,BigDecimal.ROUND_DOWN));// 保证金比例
            }else{
                throw BizException.error(null, "9999","信贷系统未查询到,资产池："+contNo+"的保证金账号" );
            }
            xdzc0003DataRespDto.setManagerId(ctrAsplDetails.getManagerId());// 主管客户经理
            xdzc0003DataRespDto.setManagerBrId(ctrAsplDetails.getManagerBrId());// 主管机构
            xdzc0003DataRespDto.setLmtStartDate(ctrAsplDetails.getLmtStartDate());// 授信起始日
            // 查询授信批复
            LmtReplyAcc lmtReplyAcc = lmtReplyAccService.getLastLmtReplyAcc(ctrAsplDetails.getCusId());
            int lmtGraperTerm = lmtReplyAcc.getLmtGraperTerm();
            String lmtDateString = DateUtils.addMonth(ctrAsplDetails.getLmtEndDate(), DateFormatEnum.DEFAULT.getValue(),lmtGraperTerm);
            xdzc0003DataRespDto.setLmtEndDate(lmtDateString);// 授信到期日( 到期+宽限期)
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value);
        return xdzc0003DataRespDto;
    }
}
