package cn.com.yusys.yusp.service.server.xdxw0082;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0082.req.Xdxw0082DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0082.resp.Xdxw0082DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 接口处理类:查询上一笔抵押贷款的余额接口（增享贷）
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Service
public class Xdxw0082Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0082Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 查询上一笔抵押贷款的余额接口（增享贷）
     *
     * @param xdxw0082DataReqDto
     * @return
     */
    public Xdxw0082DataRespDto xdxw0082(Xdxw0082DataReqDto xdxw0082DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082DataReqDto));
        Xdxw0082DataRespDto xdxw0082DataRespDto = new Xdxw0082DataRespDto();

        //获取请求参数
        String cusId = xdxw0082DataReqDto.getCus_id();//客户号
        BigDecimal loanBalance = accLoanMapper.getNewestGrtAccLoanLoanBalance(cusId);
        if(loanBalance == null){
            throw BizException.error("","0000","未查询到客户【" + cusId + "】余额大于零的抵押贷款！");
        }
        xdxw0082DataRespDto.setLoan_balance(loanBalance);

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082DataRespDto));
        return xdxw0082DataRespDto;
    }
}
