/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.AreaOrg;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AreaOrgService;
import cn.com.yusys.yusp.service.server.xdxt0006.Xdxt0006Service;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AreaUser;
import cn.com.yusys.yusp.service.AreaUserService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AreaUserResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-04-13 20:05:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "微贷用户区域关联")
@RequestMapping("/api/areauser")
public class AreaUserResource {
    @Autowired
    private AreaUserService areaUserService;

    @Autowired
    private AreaOrgService areaOrgService;

    private static final Logger logger = LoggerFactory.getLogger(Xdxt0006Service.class);
	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AreaUser>> query() {
        QueryModel queryModel = new QueryModel();
        List<AreaUser> list = areaUserService.selectAll(queryModel);
        return new ResultDto<List<AreaUser>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AreaUser>> index(QueryModel queryModel) {
        List<AreaUser> list = areaUserService.selectByModel(queryModel);
        return new ResultDto<List<AreaUser>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<AreaUser> show(@PathVariable("pkId") String pkId) {
        AreaUser areaUser = areaUserService.selectByPrimaryKey(pkId);
        return new ResultDto<AreaUser>(areaUser);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/insert")
    protected ResultDto<AreaUser> create(@RequestBody AreaUser areaUser) {
        areaUserService.insert(areaUser);
        return new ResultDto<AreaUser>(areaUser);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AreaUser areaUser) throws URISyntaxException {
        int result = areaUserService.update(areaUser);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = areaUserService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = areaUserService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
    /**
     * @author zlf
     * @date 2021/4/26 15:01
     * @version 1.0.0
     * @desc    根据区域编号查询表信息
     * @修改历史: 修改时间    修改人员    修改原因
    */
    @ApiOperation("根据区域编号查询表信息")
    @GetMapping("selectareano/{areaNo}")
    protected ResultDto<List<AreaUser>> showByAreaNo(@PathVariable("areaNo") String areaNo) {
        List<AreaUser> list = areaUserService.selectByAreaNo(areaNo);
        return new ResultDto<List<AreaUser>>(list);
    }
    /**
    * @author zlf
    * @date 2021/5/27 15:32
    * @version 1.0.0
    * @desc     根据客户号查询
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<AreaUser>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<AreaUser> list = areaUserService.selectByModel(queryModel);
        return new ResultDto<List<AreaUser>>(list);
    }
    /**
    * @author zlf
    * @date 2021/6/3 9:53
    * @version 1.0.0
    * @desc    根据区域编号删除
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/deletebyareano/{areaNo}")
    protected ResultDto<Integer> deleteByPrimaryAreaNo(@PathVariable("areaNo") String areaNo) {
        int result = areaUserService.deleteByPrimaryAreaNo(areaNo);
        return new ResultDto<Integer>(result);
    }
    /**
    * @author zlf
    * @date 2021/6/16 11:20
    * @version 1.0.0
    * @desc
    * @修改历史  修改时间 修改人员 修改原因
    */
    @PostMapping("/deleteone")
    protected ResultDto<Integer> deleteOne(@RequestBody AreaUser areaUser) {
        int result = areaUserService.deleteByPrimaryKey(areaUser.getPkId());
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/getAreaUsersByOrgId")
    protected String getAreaUsersByOrgId(@RequestBody String orgId) {

        String managerId = null;
        logger.info("根据机构号【{}】查询区域信息开始", orgId);
        List<AreaOrg> areaOrgDtos = areaOrgService.selectByOrgId(orgId);
        if (CollectionUtils.nonEmpty(areaOrgDtos)) {
            logger.info("根据区域编号【{}】查询区域负责人信息开始", areaOrgDtos.get(0).getAreaNo());
            List<AreaUser> areaUsers = areaUserService.selectByAreaNo(areaOrgDtos.get(0).getAreaNo());
            if (CollectionUtils.nonEmpty(areaUsers)) {
                logger.info("区域编号【{}】查询区域负责人信息结束,负责人工号【{}】", orgId, areaUsers.get(0).getUserNo());
                managerId = areaUsers.get(0).getUserNo();
            }
        }

        return managerId;
    }
}
