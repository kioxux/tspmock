package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.NumberUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.*;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0016.req.CmisLmt0016ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.req.CmisLmt0039ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0039.resp.CmisLmt0039RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.core.dp2099.Dp2099Service;
import cn.com.yusys.yusp.util.CmisCommonUtils;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Service
public class RiskItem0115Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0115Service.class);

    @Autowired
    private Dp2099Service dp2099Service;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private CoopPartnerAgrAccInfoService coopPartnerAgrAccInfoService;// 合作方协议台账信息

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private PvpAccpAppService pvpAccpAppService;

    @Autowired
    private IqpAccpAppService iqpAccpAppService;

    @Autowired
    private CoopReplyAccSubService  coopReplyAccSubService;

    @Autowired
    private CoopReplyAppSubService  coopReplyAppSubService;

    @Autowired
    private CoopReplyAgrSubService  coopReplyAgrSubService;

    @Autowired
    private CtrAccpContService  ctrAccpContService;

    @Autowired
    private CoopReplyAccService  coopReplyAccService;

    /**
     * @方法名称: riskItem0115
     * @方法描述: 合作方(集群贷市场方、专业担保公司)保证金校验
     * @参数与返回说明:
     * @算法描述:
     * 合作方：集群贷市场方、专业担保公司
     *
     * （1）系统只做提示，不做强制拦截
     * （2）检查规则如下：
     *      前提条件：合同占用合作方额度，并且合作方保证金比例大于0时，进行如下判断
     *      如果：合作方保证金账户余额 < 合作方下关联的所有贷款金额（含银票、保函、开证敞口金额） * 合作方保证金比例 + 本笔贷款发放金额（含银票、保函、开证敞口金额） * 合作方保证金比例  ----系统风险提示
     *      如果：合作方保证金账户余额 < 【合作方案台账中】保证金账户最低金额(元)  ---- 系统风险提示
     * 否则：通过
     * （3）检查环节：
     *     对公贷款出账申请、对公银承出账申请时：客户经理发起申请环节、集中作业放款初审岗环节、集中作业放款复审岗环节，进行风险拦截检查。
     * @创建人: yfs
     * @创建时间: 2021-09-11 15:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public  RiskResultDto riskItem0115(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        log.info("合作方(集群贷市场方、专业担保公司)保证金校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        RiskResultDto riskResultDto = new RiskResultDto();
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        String thirdPartyFlag = "";
        String isJQD = "";
        // 对公贷款出账流程
        if (Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX011,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_SGD02,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_DHD02,bizType)) {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
            if(Objects.isNull(pvpLoanApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            //根据合同编号获取合同详情
            //根据合同编号获取合同详情
            QueryModel model = new QueryModel();
            model.addCondition("contNo",pvpLoanApp.getContNo());
            model.addCondition("oprType",CmisCommonConstants.OPR_TYPE_ADD);
            List<IqpLoanApp> iqpLoanAppList = iqpLoanAppService.selectAll(model);
            if(CollectionUtils.isEmpty(iqpLoanAppList)) {
                List<IqpHighAmtAgrApp> iqpHighAmtAgrAppList = iqpHighAmtAgrAppService.selectAll(model);
                if(CollectionUtils.isEmpty(iqpHighAmtAgrAppList)) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0016);
                    return riskResultDto;
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
                    return riskResultDto;
                }
            } else {
                thirdPartyFlag = iqpLoanAppList.get(0).getIsOutstndTrdLmtAmt();
                // 集群贷
                if("P019".equals(iqpLoanAppList.get(0).getPrdTypeProp())){
                    isJQD = CmisCommonConstants.STD_ZB_YES_NO_1;
                }
            }
            // 如果占用第三方额度
            if(Objects.equals(CmisCommonConstants.STD_ZB_YES_NO_1,thirdPartyFlag)) {
                // 判断是否集群贷
                riskResultDto = riskItem0115Check(serno,iqpLoanAppList.get(0).getTdpAgrNo(),pvpLoanApp.getCvtCnyAmt(),isJQD);
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
            }
        } else if(CmisFlowConstants.FLOW_TYPE_TYPE_YX013.equals(bizType)) { //银承出账申请
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
            if (Objects.isNull(pvpAccpApp)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017);
                return riskResultDto;
            }
            CtrAccpCont ctrAccpCont = ctrAccpContService.selectByContNo(pvpAccpApp.getContNo());
            if(Objects.isNull(ctrAccpCont)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015);
                return riskResultDto;
            }
            // 集群贷
            if("P019".equals(ctrAccpCont.getPrdTypeProp())){
                isJQD = CmisCommonConstants.STD_ZB_YES_NO_1;
            }
            // 如果占用第三方额度
            if(Objects.equals(CmisCommonConstants.STD_ZB_YES_NO_1,ctrAccpCont.getIsOutstndTrdLmtAmt())) {
                riskResultDto = riskItem0115Check(serno,ctrAccpCont.getTdpAgrNo(),pvpAccpApp.getAppAmt(),isJQD);
                if(StringUtils.nonBlank(riskResultDto.getRiskResultType())) {
                    return riskResultDto;
                }
            }
        }

        log.info("合作方(集群贷市场方、专业担保公司)保证金校验结束*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param bzjAmt, totalAmt, bailPerc, pvpAmt
     * @return boolean
     * @author hubp
     * @date 2021/9/1 11:00
     * @version 1.0.0
     * @desc  计算结果
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private boolean getResult (BigDecimal bzjAmt,BigDecimal totalAmt,BigDecimal bailPerc,BigDecimal pvpAmt) {
        // 合作方保证金账户余额 < 合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例
        log.info("通过保证金余额:【{}】，合作方关联贷款总金额:【{}】，保证金比例:【{}】，本次放款金额:【{}】，开始计算是否通过", bzjAmt.toPlainString(),totalAmt.toPlainString(),bailPerc.toPlainString(),pvpAmt.toPlainString());
        // 先计算 --》合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例
        BigDecimal allAmt = totalAmt.multiply(bailPerc).add(pvpAmt.multiply(bailPerc));
        log.info("合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例之和为【{}】",allAmt.toPlainString());
        if(bzjAmt.compareTo(allAmt) < 0){
            return true;
        }
        return false;
    }

    /**
     *
     * @param serno
     * @param proNo 分项编号
     * @param pvpAmt
     * @return
     */
    public RiskResultDto riskItem0115Check(String serno,String proNo,BigDecimal pvpAmt,String isJQD) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String replyNo = "";
        String manangeBrId = "";
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey(serno);
        if(Objects.isNull(pvpLoanApp)){
            PvpAccpApp pvpAccpApp = pvpAccpAppService.selectBySerno(serno);
            if(Objects.isNull(pvpLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_011501);
                return riskResultDto;
            }
            manangeBrId = pvpAccpApp.getManagerBrId();
        }else {
            manangeBrId = pvpLoanApp.getManagerBrId();
        }
        // 调额度接口查询合作方分项信息
        CmisLmt0026ReqDto cmisLmt0026ReqDto = new CmisLmt0026ReqDto();
        cmisLmt0026ReqDto.setInstuCde(CmisCommonUtils.getInstucde(manangeBrId));// 金融机构代码
        cmisLmt0026ReqDto.setQueryType(CmisLmtConstants.STD_ZB_LMT_TYPE_03);// 分项类型
        cmisLmt0026ReqDto.setSubSerno(proNo);// 分项编号
        log.info("出账申请【{}】，前往额度系统查询合作方分项信息开始,请求报文为:【{}】", serno, cmisLmt0026ReqDto.toString());
        ResultDto<CmisLmt0026RespDto> resultDtoDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto);
        log.info("出账申请【{}】，前往额度系统查询合作方分项信息结束,响应报文为:【{}】", serno, resultDtoDto.toString());
        if(!"0".equals(resultDtoDto.getCode())){
            log.error("业务申请【{}】接口调用异常！",serno);
            throw BizException.error(null, EcbEnum.ECB019999.key,EcbEnum.ECB019999.value);
        }
        if(!"0000".equals(resultDtoDto.getData().getErrorCode())){
            log.error("根据合作方分项编号【{}】查询合作方信息失败！异常信息：【{}】",proNo,resultDtoDto.getData().getErrorMsg());
            throw BizException.error(null, resultDtoDto.getData().getErrorCode(),resultDtoDto.getData().getErrorMsg());
        }
        CmisLmt0026RespDto responseData = resultDtoDto.getData();
        if(Objects.isNull(responseData)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11501);
            return riskResultDto;
        }
        // 授信品种编号
        String limitSubNo = responseData.getLimitSubNo();
        // 产品类型属性
        String lmtBizTypeProp = responseData.getLmtBizTypeProp();
        CoopPartnerAgrAccInfo coopPartnerAgrAccInfo = null;
        String isRenew = CmisCommonConstants.STD_ZB_YES_NO_1;
        // 一般担保
        if("2".equals(limitSubNo) && "".equals(lmtBizTypeProp)){
            isRenew = CmisCommonConstants.STD_ZB_YES_NO_0;
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("replyNo", proNo);
            queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            queryModel.setSort("createTime desc");
            List<CoopReplyAcc> coopReplyAccList = coopReplyAccService.selectByModel(queryModel);
            if (CollectionUtils.isEmpty(coopReplyAccList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11501);
                return riskResultDto;
            } else {
                CoopReplyAcc coopReplyAcc = coopReplyAccList.get(0);
                if (coopReplyAcc.getBailPerc() != BigDecimal.ZERO) {
                    // 合作方账户信息（合作方类型、保证金账号、保证金账号子序号）不能为空
                    if (StringUtils.isBlank(coopReplyAcc.getPartnerType()) || StringUtils.isBlank(coopReplyAcc.getBailAccNo())
                            || StringUtils.isBlank(coopReplyAcc.getBailAccNoSubSeq())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11502);
                        return riskResultDto;
                    }
                    // 合作方类型	4	集群贷市场方
                    // 合作方类型	2	专业担保公司
                    if ((Objects.equals("4", coopReplyAcc.getPartnerType()) || Objects.equals("2", coopReplyAcc.getPartnerType()))
                            && BigDecimal.ZERO.compareTo(NumberUtils.nullDefaultZero(coopReplyAcc.getBailPerc())) < 0) {
                        // 保证金比例
                        BigDecimal bailPerc = NumberUtils.nullDefaultZero(coopReplyAcc.getBailPerc());
                        Dp2099ReqDto dp2099ReqDto = new Dp2099ReqDto();
                        dp2099ReqDto.setChaxleix("0"); // 查询类型 0--客户 1--非客户
                        dp2099ReqDto.setChaxfanw("3"); // 查询范围 1--指定机构 2--直辖 3--全辖 4--本机构
                        // dp2099ReqDto.setKehuhaoo(coopPartnerAgrAccInfo.getPartnerNo()); // 客户号
                        dp2099ReqDto.setKehuzhao(coopReplyAcc.getBailAccNo()); // 客户账号
                        dp2099ReqDto.setZhhaoxuh(coopReplyAcc.getBailAccNoSubSeq()); // 子账户序号
                        log.info("根据合作方编号【{}】前往核心系统查询保证金账户请求报文：【{}】", coopReplyAcc.getPartnerNo(), dp2099ReqDto.toString());
                        Dp2099RespDto dp2099RespDto = dp2099Service.dp2099(dp2099ReqDto);
                        log.info("根据合作方编号【{}】前往核心系统查询保证金账户返回报文：【{}】", coopReplyAcc.getPartnerNo(), JSON.toJSONString(dp2099RespDto));
                        if (Objects.isNull(dp2099RespDto) || CollectionUtils.isEmpty(dp2099RespDto.getList())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11503); // 未查询到合作方保证金余额信息！
                            return riskResultDto;
                        }
                        // 获取保证金余额
                        BigDecimal bzjAmt = NumberUtils.nullDefaultZero(dp2099RespDto.getList().get(0).getKeyongye());
                        // 获取合作方下关联的所有贷款金额
                        CmisLmt0039ReqDto cmisLmt0039ReqDto = new CmisLmt0039ReqDto();
                        cmisLmt0039ReqDto.setApprSerno(replyNo);
                        log.info("根据放款流水号【{}】前往额度系统-第三方额度查询请求报文：【{}】", serno, cmisLmt0039ReqDto.toString());
                        ResultDto<CmisLmt0039RespDto> cmisLmt0039RespDto = cmisLmtClientService.cmislmt0039(cmisLmt0039ReqDto);
                        log.info("根据放款流水号【{}】前往额度系统-第三方额度查询返回报文：【{}】", serno, JSON.toJSONString(cmisLmt0039RespDto));
                        if (Objects.isNull(cmisLmt0039RespDto) || Objects.isNull(cmisLmt0039RespDto.getData())) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11504);  // 前往额度系统查询合作方下关联的所有贷款金额失败！
                            return riskResultDto;
                        }
                        String code = cmisLmt0039RespDto.getData().getErrorCode();
                        if (!"0000".equals(code)) {
                            log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额失败！", replyNo);
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11505);  // 前往额度系统查询合作方下关联的所有贷款金额失败！
                            return riskResultDto;
                        } else {
                            log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额成功！", replyNo);
                            BigDecimal totalAmt = NumberUtils.nullDefaultZero(cmisLmt0039RespDto.getData().getSumLoanTotalCny()); //合作方下关联的所有贷款金额
                            log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额为：【{}】", replyNo, totalAmt.toPlainString());
                            if (getResult(bzjAmt, totalAmt, bailPerc, NumberUtils.nullDefaultZero(pvpAmt))) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11506);
                                return riskResultDto;
                            }
                            if(bzjAmt.compareTo(coopReplyAcc.getBailAccLowAmt())<0){
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11507);
                                return riskResultDto;
                            }
                        }
                    }
                }
            }
        } else if("2".equals(limitSubNo) && !"".equals(lmtBizTypeProp)){
            // 查询批复分项信息
            QueryModel modelCoopReplyAccSubService = new QueryModel();
            modelCoopReplyAccSubService.addCondition("subNo", proNo);
            modelCoopReplyAccSubService.setSort("createTime desc");
            List<CoopReplyAccSub> coopReplyAccSubList = coopReplyAccSubService.selectByModel(modelCoopReplyAccSubService);
            if (CollectionUtils.isEmpty(coopReplyAccSubList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11501);
                return riskResultDto;
            }else{
                replyNo = coopReplyAccSubList.get(0).getReplyNo();
            }

            // 查询批复分项申请信息,获取最新的一条
            QueryModel modelCoopReplyAgrSubService = new QueryModel();
            modelCoopReplyAgrSubService.addCondition("subNo", proNo);
            modelCoopReplyAgrSubService.setSort("createTime desc");
            List<CoopReplyAgrSub> coopReplyAgrSubList = coopReplyAgrSubService.selectByModel(modelCoopReplyAgrSubService);
            String coopAgrSerno;
            if (!CollectionUtils.isEmpty(coopReplyAccSubList)) {
                coopAgrSerno = coopReplyAgrSubList.get(0).getSerno();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11501);
                return riskResultDto;
            }

            //查询合作协议信息,获取最新的一条
            QueryModel model = new QueryModel();
            model.addCondition("coopAgrSerno", coopAgrSerno);
            model.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            model.setSort("createTime desc");
            List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfoList = coopPartnerAgrAccInfoService.selectByModel(model);
            // 未查询到合作方详细信息！
            if (CollectionUtils.isEmpty(coopPartnerAgrAccInfoList)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11501);
                return riskResultDto;
            } else {
                coopPartnerAgrAccInfo = coopPartnerAgrAccInfoList.get(0);
            }
        }else if("4".equals(limitSubNo)){
            //如果是产品属性是集群贷或一般担保额度,没有分项信息，可直接查询合作协议
            //查询合作协议信息,获取最新的一条
            QueryModel queryModel = new QueryModel();
            queryModel.addCondition("coopPlanNo", proNo);
            queryModel.addCondition("oprType", CmisCommonConstants.OPR_TYPE_ADD);
            queryModel.setSort("createTime desc");
            List<CoopPartnerAgrAccInfo> coopPartnerAgrAccInfoListData = coopPartnerAgrAccInfoService.selectByModel(queryModel);
            if (CollectionUtils.isEmpty(coopPartnerAgrAccInfoListData)) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11501);
                return riskResultDto;
            } else {
                coopPartnerAgrAccInfo = coopPartnerAgrAccInfoListData.get(0);
            }
            replyNo = proNo;
        }else{
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }

        if(CmisCommonConstants.STD_ZB_YES_NO_1.equals(isRenew)){
            log.info("根据分项编号【{}】查询出合作方协议详细信息：协议编号【{}】", replyNo, coopPartnerAgrAccInfo.getCoopAgrNo());
            BigDecimal bzjAmt = BigDecimal.ZERO;
            if (coopPartnerAgrAccInfo.getBailPerc() != BigDecimal.ZERO) {
                // 合作方账户信息（合作方类型、保证金账号、保证金账号子序号）不能为空
                if (StringUtils.isBlank(coopPartnerAgrAccInfo.getPartnerType()) || StringUtils.isBlank(coopPartnerAgrAccInfo.getBailAccNo())
                        || StringUtils.isBlank(coopPartnerAgrAccInfo.getBailAccNoSubSeq())) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11502);
                    return riskResultDto;
                }
                // 合作方类型	4	集群贷市场方
                // 合作方类型	2	专业担保公司
                if ((Objects.equals("4", coopPartnerAgrAccInfo.getPartnerType()) || Objects.equals("2", coopPartnerAgrAccInfo.getPartnerType()))
                        && BigDecimal.ZERO.compareTo(NumberUtils.nullDefaultZero(coopPartnerAgrAccInfo.getBailPerc())) < 0) {
                    // 保证金比例
                    BigDecimal bailPerc = NumberUtils.nullDefaultZero(coopPartnerAgrAccInfo.getBailPerc());
                    Dp2099ReqDto dp2099ReqDto = new Dp2099ReqDto();
                    dp2099ReqDto.setChaxleix("0"); // 查询类型 0--客户 1--非客户
                    dp2099ReqDto.setChaxfanw("3"); // 查询范围 1--指定机构 2--直辖 3--全辖 4--本机构
                    // dp2099ReqDto.setKehuhaoo(coopPartnerAgrAccInfo.getPartnerNo()); // 客户号
                    dp2099ReqDto.setKehuzhao(coopPartnerAgrAccInfo.getBailAccNo()); // 客户账号
                    dp2099ReqDto.setZhhaoxuh(coopPartnerAgrAccInfo.getBailAccNoSubSeq()); // 子账户序号
                    log.info("根据合作方编号【{}】前往核心系统查询保证金账户请求报文：【{}】", coopPartnerAgrAccInfo.getPartnerNo(), dp2099ReqDto.toString());
                    Dp2099RespDto dp2099RespDto = dp2099Service.dp2099(dp2099ReqDto);
                    log.info("根据合作方编号【{}】前往核心系统查询保证金账户返回报文：【{}】", coopPartnerAgrAccInfo.getPartnerNo(), JSON.toJSONString(dp2099RespDto));
                    if (Objects.isNull(dp2099RespDto) || CollectionUtils.isEmpty(dp2099RespDto.getList())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11503); // 未查询到合作方保证金余额信息！
                        return riskResultDto;
                    }
                    // 获取保证金余额
                    bzjAmt = NumberUtils.nullDefaultZero(dp2099RespDto.getList().get(0).getKeyongye());
                    // 获取合作方下关联的所有贷款金额
                    CmisLmt0039ReqDto cmisLmt0039ReqDto = new CmisLmt0039ReqDto();
                    cmisLmt0039ReqDto.setApprSerno(replyNo);
                    log.info("根据放款流水号【{}】前往额度系统-第三方额度查询请求报文：【{}】", serno, cmisLmt0039ReqDto.toString());
                    ResultDto<CmisLmt0039RespDto> cmisLmt0039RespDto = cmisLmtClientService.cmislmt0039(cmisLmt0039ReqDto);
                    log.info("根据放款流水号【{}】前往额度系统-第三方额度查询返回报文：【{}】", serno, JSON.toJSONString(cmisLmt0039RespDto));
                    if (Objects.isNull(cmisLmt0039RespDto) || Objects.isNull(cmisLmt0039RespDto.getData())) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11504);  // 前往额度系统查询合作方下关联的所有贷款金额失败！
                        return riskResultDto;
                    }
                    String code = cmisLmt0039RespDto.getData().getErrorCode();
                    if (!"0000".equals(code)) {
                        log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额失败！", replyNo);
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11505);  // 前往额度系统查询合作方下关联的所有贷款金额失败！
                        return riskResultDto;
                    } else {
                        log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额成功！", replyNo);
                        BigDecimal totalAmt = NumberUtils.nullDefaultZero(cmisLmt0039RespDto.getData().getSumLoanTotalCny()); //合作方下关联的所有贷款金额 * 合作方保证金比例
                        log.info("根据项目编号【{}】,前往额度系统查询合作方下关联的所有贷款金额为：【{}】", replyNo, totalAmt.toPlainString());
                        if (getResult(bzjAmt, totalAmt, bailPerc, NumberUtils.nullDefaultZero(pvpAmt))) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11506);
                            return riskResultDto;
                        }
                        if(bzjAmt.compareTo(coopPartnerAgrAccInfo.getBailAccLowAmt())<0){
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_11507);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }
}