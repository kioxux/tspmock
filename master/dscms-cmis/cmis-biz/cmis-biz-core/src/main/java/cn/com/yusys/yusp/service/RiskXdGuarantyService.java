/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.RiskXdGuaranty;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1168.req.Fb1168ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1168.resp.Fb1168RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.RiskXdGuarantyMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskXdGuarantyService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-05 23:15:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RiskXdGuarantyService {

    @Autowired
    private RiskXdGuarantyMapper riskXdGuarantyMapper;

    @Autowired
    private Dscms2CircpClientService dscms2CircpClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 日志
    private static final Logger log = LoggerFactory.getLogger(RiskXdGuarantyService.class);
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public RiskXdGuaranty selectByPrimaryKey(String serno) {
        return riskXdGuarantyMapper.selectByPrimaryKey(serno);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<RiskXdGuaranty> selectAll(QueryModel model) {
        List<RiskXdGuaranty> records = (List<RiskXdGuaranty>) riskXdGuarantyMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<RiskXdGuaranty> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RiskXdGuaranty> list = riskXdGuarantyMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: riskXdGuarantylist
     * @方法描述: 查询审批状态为待发起、打回、审批中数据（获取申请信息押品查询查封）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<RiskXdGuaranty> riskXdGuarantylist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_APP_LIST);
        return riskXdGuarantyMapper.selectByModel(model);
    }
    /**

     * @方法名称: sfResultInfoHislist
     * @方法描述: 查询审批状态为通过、否决、自行退出数据（获取申请信息押品查询查封）
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<RiskXdGuaranty> riskXdGuarantyHislist(QueryModel model) {
        HashMap<String, String> queyParam = new HashMap<String, String>();
        model.getCondition().put("applyExistsStatus", CmisCommonConstants.WF_STATUS_HIS_LIST);
        return riskXdGuarantyMapper.selectByModel(model);
    }

    /**
     * 获取基本信息
     *
     * @param serno
     * @return
     */
    public RiskXdGuaranty selectBySerno(String serno) {
        return riskXdGuarantyMapper.selectByPrimaryKey(serno);
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(RiskXdGuaranty record) {
        return riskXdGuarantyMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(RiskXdGuaranty record) {
        return riskXdGuarantyMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(RiskXdGuaranty record) {
        return riskXdGuarantyMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateRiskXdGuaranty
     * @方法描述: 押品查询查封将信息同步发送风控
     * @参数与返回说明: 交易码fb1168
     * @创建者：zhangliang15
     * @算法描述: 无
     */
    public ResultDto<String> updateRiskXdGuaranty(RiskXdGuaranty record) {
        ResultDto<String> resultDto = new ResultDto<>();
        if (record != null) {
            // 更新修改次数
            record.setIsUpdate("1");
            // 获取营业日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 查询查封提交时间
            record.setSubmitTime(openDay);
            // 更新押品查询查封表
            int count = riskXdGuarantyMapper.updateByPrimaryKey(record);
            if (count != 1) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EpbEnum.EPB099999.key, EpbEnum.EPB099999.value + ",房抵e点贷押品查询查封保存失败！");
            }
            // STD_CXCF_RESULT 1：正在进行查询查封 2:今日已进行查封查验且有冻 3:今日已进行查封查验且无冻结
            // 当为3:今日已进行查封查验且无冻结保存，提交流程并发送fb1168(抵押查封结果推送),否则保存并发送fb1168(抵押查封结果推送),不提交流程
            if(!"3".equals(record.getCxCfResult())){
                log.info("押品查询查封推送风控开始,流水号【{}】", record.getSerno());
                Fb1168ReqDto fb1168ReqDto = new Fb1168ReqDto();
                fb1168ReqDto.setCHANNEL_TYPE("13"); //渠道来源 默认‘13’ 参照老信贷代码逻辑
                fb1168ReqDto.setCO_PLATFORM("2002"); //合作平台 默认‘2002’ 参照老信贷代码逻辑
                fb1168ReqDto.setPRD_CODE ("2002000001"); //产品代码 默认‘2002000001’ 参照老信贷代码逻辑
                fb1168ReqDto.setDy_no(record.getDyNo()); //抵押品编号
                fb1168ReqDto.setBdc_no(record.getBdcNo()); //不动产证号
                fb1168ReqDto.setJk_cus_name(record.getJkCusName()); //借款人名称
                fb1168ReqDto.setYp_manager_id(record.getYpManagerId()); //押品所有人名称
                fb1168ReqDto.setYp_book_serno(record.getYpBookSerno()); //押品权证编号
                fb1168ReqDto.setCx_cf_result(record.getCxCfResult()); //查询查封结果
                fb1168ReqDto.setPre_app_no(record.getPreAppNo()); // 预授信流水号
                fb1168ReqDto.setCx_cf_time(openDay.replaceAll("-", "")); // 查询查封时间
                log.info("押品查询查封推送风控请求：" + fb1168ReqDto);
                ResultDto<Fb1168RespDto> fb1166RespResultDto = dscms2CircpClientService.fb1168(fb1168ReqDto);
                log.info("押品查询查封推送风控返回：" + fb1166RespResultDto);
                String fb1166Code = Optional.ofNullable(fb1166RespResultDto.getCode()).orElse(StringUtils.EMPTY);
                String fb1166Meesage = Optional.ofNullable(fb1166RespResultDto.getMessage()).orElse(StringUtils.EMPTY);
                log.info("押品查询查封推送风控返回信息：" + fb1166Meesage);
                if (Objects.equals(fb1166Code, SuccessEnum.CMIS_SUCCSESS.key)) {
                } else {
                    resultDto.setCode(fb1166Code);
                    resultDto.setMessage(fb1166Meesage);
                }
            }
        } else {
            throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
        }
        return resultDto;
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(RiskXdGuaranty record) {
        return riskXdGuarantyMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String serno) {
        return riskXdGuarantyMapper.deleteByPrimaryKey(serno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return riskXdGuarantyMapper.deleteByIds(ids);
    }
}
