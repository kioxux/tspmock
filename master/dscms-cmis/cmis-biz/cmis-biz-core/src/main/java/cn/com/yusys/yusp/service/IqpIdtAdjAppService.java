/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.IqpIdtAdjApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.IqpIdtAdjAppMapper;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * 授信审批通知书service
 */
@Service
public class IqpIdtAdjAppService {
    private static final Logger log = LoggerFactory.getLogger(IqpIdtAdjAppService.class);
    @Autowired
    private IqpIdtAdjAppMapper iqpIdtAdjAppMapper;
    /**
     * 插入所选项
     * @param record
     * @return
     */
    public int insertSelective(IqpIdtAdjApp record) {
        return iqpIdtAdjAppMapper.insertSelective(record);
    }

    /**
     * 保存
     * @param
     * @return
     */
    @Transactional(rollbackFor=Exception.class)
    public Map saveIqpIdtAdjApp(IqpIdtAdjApp iqpIdtAdjApp) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_CHG_SUCCESS_DEF.key;
        String rtnMsg = EcbEnum.IQP_CHG_SUCCESS_DEF.value;
        try {
            //获取业务流水号
            iqpIdtAdjApp.setApproveStatus(CmisCommonConstants.WF_STATUS_000);//设置状态为"待发起"
            iqpIdtAdjApp.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//设置操作状态为"新增"

            int insertCount = iqpIdtAdjAppMapper.insertSelective(iqpIdtAdjApp);

            if (insertCount < 0) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",保存失败！");
            }
            if (insertCount < 0) {
                //涉及数据库增删改的操作，需要抛出异常，然后回滚数据操作
                throw new YuspException(EcbEnum.IQP_CHG_EXCEPTION_DEF.key, EcbEnum.IQP_CHG_EXCEPTION_DEF.value + ",保存放款申请主办人信息异常！");
            }
        } catch (YuspException e) {
            rtnCode = e.getCode();
            rtnMsg = e.getMsg();
        } catch (Exception e) {
            log.error("保存修改授信审批通知书有效期出现异常！", e);
            rtnCode = EcbEnum.IQP_CHG_EXCEPTION_DEF.key;
            rtnMsg = EcbEnum.IQP_CHG_EXCEPTION_DEF.value + "," + e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
        }
        return rtnData;
    }
    /**
     * 校验
     * @param iqpIdtAdjApp
     * @return
     */
    public int checkIsExistWFByBillNo(IqpIdtAdjApp iqpIdtAdjApp) {
        try {
            String lmtCtrNo = iqpIdtAdjApp.getLmtCtrNo();
            log.info("根据借据编号查询在途的还款方式变更申请业务【{}】", JSONObject.toJSON(iqpIdtAdjApp));
            int result = 0;
            result = iqpIdtAdjAppMapper.checkIsExistWFByBillNo(lmtCtrNo);
            if (result > 0 ) {
                throw new YuspException(EcbEnum.IQP_REPAY_WAY_CHG_CHECK_EXCEPTION_3.key, EcbEnum.IQP_REPAY_WAY_CHG_CHECK_EXCEPTION_3.value);
            }
            return result;
        } catch (YuspException e) {
            log.error("还款方式变更申请校验其他变更业务失败！", e);
            return -1;
        }
    }
    /**
     * 根据主键查询
     * @param iqpSerno
     * @return
     */
    public IqpIdtAdjApp selectByPrimaryKey(String iqpSerno) {
        IqpIdtAdjApp iqpIdtAdjApp = iqpIdtAdjAppMapper.selectByPrimaryKey(iqpSerno);
        return iqpIdtAdjApp;
    }
    /**
     * 根据流水号更新
     * @param iqpSerno
     * @param approveStatus
     * @return
     */
    public int updateApproveStatusByIqpSerno(String iqpSerno, String approveStatus) {
        return iqpIdtAdjAppMapper.updateApproveStatusByIqpSerno(iqpSerno,approveStatus);
    }
    /**
     * 更改审批状态
     * @param iqpSerno
     */
    public void updateRepayWayAfterWfAppr(String iqpSerno) {
        IqpIdtAdjApp iqpIdtAdjApp = new IqpIdtAdjApp();
        iqpIdtAdjApp = selectByPrimaryKey(iqpSerno);
        updateAccLoanRepayWayByBillNo(iqpIdtAdjApp);
    }
    /**
     * 反插入
     * @param iqpIdtAdjApp
     */
    public void updateAccLoanRepayWayByBillNo(IqpIdtAdjApp iqpIdtAdjApp) {
        try{
            //想iqprepayplan表中插入数据
            /*AccLoan accLoan = reyPlanMapper.selectPvpLoanAppByBillNo(reyPlan.getBillNo());
            String pvpSerno = accLoan.getPvpSerno();
            reyPlanMapper.insertIntoIqpRepayPlan(reyPlan);*/
            System.out.println("审批结束后向借据表反插数据");
        }catch(YuspException e){
            throw e;
        }catch(Exception e){
            log.error("更新授信审批通知书信息处理发生异常！",e);
            throw new YuspException(EcbEnum.IQP_REPAY_UPDATE_EXCEPTION_1.key, EcbEnum.IQP_REPAY_UPDATE_EXCEPTION_1.value);
        }
    }
    /**
     * 更新申请状态
     * @param iqpIdtAdjApp
     * @return
     */
    public int updateByPrimaryKeySelective(IqpIdtAdjApp iqpIdtAdjApp) {
        return iqpIdtAdjAppMapper.updateByPrimaryKeySelective(iqpIdtAdjApp);
    }
}
