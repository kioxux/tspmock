package cn.com.yusys.yusp.service.server.xdzc0002;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.dto.DocArchiveClientDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011OccRelListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.req.CmisLmt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0011.resp.CmisLmt0011RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0002.req.Xdzc0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0002.resp.Xdzc0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrAsplDetailsMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarBizRstRelMapper;
import cn.com.yusys.yusp.repository.mapper.GrtGuarContMapper;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.service.client.bsp.ypxt.buscon.BusconService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 接口处理类:资产池协议激活接口
 * @Author xs
 * @Date 2021/06/01 16:20
 * @Version 1.0
 */
@Service
public class Xdzc0002Service {

    @Autowired
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;
    @Autowired
    private CtrAsplDetailsMapper ctrAsplDetailsMapper;
    @Autowired
    private GrtGuarContMapper grtGuarContMapper;
    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;
    @Autowired
    private IqpAppAsplService iqpAppAsplService;
    @Autowired
    private CmisLmtClientService cmisLmtClientService;
    @Autowired
    private ICusClientService iCusClientService;
    @Autowired
    private CmisBizClientService cmisBizClientService;
    @Autowired
    private GuarBusinessRelService guarBusinessRelService;
    @Autowired
    private GuarBizRelService guarBizRelService;
    @Autowired
    private BusconService busconService;
    @Autowired
    private GrtGuarBizRstRelService grtGuarBizRstRelService;

    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdzc0002.Xdzc0002Service.class);

    @Transactional
    public Xdzc0002DataRespDto xdzc0002Service(Xdzc0002DataReqDto xdzc0002DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value);
        Xdzc0002DataRespDto xdzc0002DataRespDto = new Xdzc0002DataRespDto();
        String contNo = xdzc0002DataReqDto.getContNo();//协议编号

        xdzc0002DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
        xdzc0002DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);// 描述信息
        int num = 0;
        try {
            //修改资产池合同状态为生效
            Map<String,String> ctrAsplDetailsMap =  new HashMap<String,String>();
            ctrAsplDetailsMap.put("contNo",contNo);//协议编号
            ctrAsplDetailsMap.put("conStatus", CommonConstance.STD_ZB_CONT_TYP_200);//合同状态
            ctrAsplDetailsMap.put("updateTime", DateUtils.getCurrDateTimeStr());//修改时间
            ctrAsplDetailsMap.put("updDate",DateUtils.getCurrDateTimeStr());//修改
            num = ctrAsplDetailsMapper.updateActiveByContNo(ctrAsplDetailsMap);
            if(num<1){
                xdzc0002DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                xdzc0002DataRespDto.setOpMsg("资产池协议激活失败");// 描述信息
                return xdzc0002DataRespDto;
            }
//担保合同 合同状态 目前先不要激活，新生成的担保合同是默认生效的，相关联的关系表是存放的 流水号
//            num = grtGuarContMapper.updateGuarContStatus(contNo);
//            if(num<1){
//                xdzc0002DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
//                xdzc0002DataRespDto.setOpMsg("资产池相关联的担保合同激活失败");// 描述信息
//                return xdzc0002DataRespDto;
//            }
            // 获取资产池协议
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectCtrAsplDetailsInfoByContNo(contNo);
            IqpAppAspl iqpAppAspl = iqpAppAsplService.selectBySerno(ctrAsplDetails.getSerno());
//
//            //1. 向额度系统发送接口：额度校验
//            CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
//            cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
//            cmisLmt0009ReqDto.setInstuCde(CmisCommonUtils.getInstucde(managerBrId));//金融机构代码
//            cmisLmt0009ReqDto.setDealBizNo(ctrAsplDetails.getContNo());//合同编号
//            cmisLmt0009ReqDto.setCusId(ctrAsplDetails.getCusId());//客户编号
//            cmisLmt0009ReqDto.setCusName(ctrAsplDetails.getCusName());//客户名称
//            cmisLmt0009ReqDto.setDealBizType(ctrAsplDetails.getContType());//交易业务类型
//            cmisLmt0009ReqDto.setPrdId(ctrAsplDetails.getPrdId());//产品编号
//            cmisLmt0009ReqDto.setPrdName(ctrAsplDetails.getPrdName());//产品名称
//            cmisLmt0009ReqDto.setIsLriskBiz("1");//是否低风险
//            cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
//            cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
//            cmisLmt0009ReqDto.setBizAttr("1");//交易属性
//            cmisLmt0009ReqDto.setOrigiDealBizNo(iqpAppAspl.getSerno());//原交易业务编号
//            cmisLmt0009ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
//            cmisLmt0009ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
//            cmisLmt0009ReqDto.setOrigiBizAttr("1");//原交易属性
//            cmisLmt0009ReqDto.setDealBizAmt(iqpAppAspl.getContAmt());//交易业务金额
//            cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
//            cmisLmt0009ReqDto.setDealBizBailPreAmt(new BigDecimal(0));//保证金
//            cmisLmt0009ReqDto.setStartDate(ctrAsplDetails.getStartDate());//合同起始日
//            cmisLmt0009ReqDto.setEndDate(ctrAsplDetails.getEndDate());//合同到期日
//            List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
//            CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
//            cmisLmt0009OccRelListReqDto.setLmtType("01");//额度类型
//            cmisLmt0009OccRelListReqDto.setLmtSubNo(ctrAsplDetails.getLmtAccNo());//额度分项编号
//            cmisLmt0009OccRelListReqDto.setBizTotalAmt(ctrAsplDetails.getContAmt());//占用总额(折人民币)
//            cmisLmt0009OccRelListReqDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(折人民币)
//            cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
//            cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);
//
//            logger.info("根据业务申请编号【" + iqpAppAspl.getSerno() + "】前往额度系统-额度校验请求报文：" + cmisLmt0009ReqDto.toString());
//            ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
//            logger.info("根据业务申请编号【" + iqpAppAspl.getSerno() + "】前往额度系统-额度校验返回报文：" + cmisLmt0009RespDto.toString());
//
//            String code = cmisLmt0009RespDto.getData().getErrorCode();
//            if (!"0000".equals(code)) {
//                logger.info("根据业务申请编号【{}】,前往额度系统-额度校验失败！", iqpAppAspl.getSerno());
//                xdzc0002DataRespDto.setOpFlag(CmisBizConstants.FAIL);
//                xdzc0002DataRespDto.setOpMsg("资产池-协议激活占用额度异常：" + cmisLmt0009RespDto.getData().getErrorMsg());
//                return xdzc0002DataRespDto;
//            }
//
//            //1. 向额度系统发送接口：占用额度
//            CmisLmt0011ReqDto cmisLmt0011ReqDto = new CmisLmt0011ReqDto();
//            cmisLmt0011ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
//            cmisLmt0011ReqDto.setInstuCde(CmisCommonUtils.getInstucde(managerBrId));//金融机构代码
//            cmisLmt0011ReqDto.setDealBizNo(ctrAsplDetails.getContNo());//合同编号
//            cmisLmt0011ReqDto.setCusId(ctrAsplDetails.getCusId());//客户编号
//            cmisLmt0011ReqDto.setCusName(ctrAsplDetails.getCusName());//客户名称
//            cmisLmt0011ReqDto.setDealBizType(ctrAsplDetails.getContType());//交易业务类型
//            cmisLmt0011ReqDto.setBizAttr("1");//交易属性
//            cmisLmt0011ReqDto.setPrdId(ctrAsplDetails.getPrdId());//产品编号
//            cmisLmt0011ReqDto.setPrdName(ctrAsplDetails.getPrdName());//产品名称
//            cmisLmt0011ReqDto.setIsLriskBiz("1");//是否低风险
//            cmisLmt0011ReqDto.setIsFollowBiz("0");//是否无缝衔接
//            cmisLmt0011ReqDto.setIsBizRev("0");//是否合同重签
//            cmisLmt0011ReqDto.setOrigiDealBizNo(ctrAsplDetails.getSerno());//原交易业务编号
//            cmisLmt0011ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
//            cmisLmt0011ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
//            cmisLmt0011ReqDto.setOrigiRecoverType("1");//原交易属性
//            cmisLmt0011ReqDto.setDealBizAmt(ctrAsplDetails.getContAmt());//交易业务金额
//            cmisLmt0011ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
//            cmisLmt0011ReqDto.setStartDate(ctrAsplDetails.getStartDate());//合同起始日
//            cmisLmt0011ReqDto.setEndDate(ctrAsplDetails.getEndDate());//合同到期日
//            cmisLmt0011ReqDto.setDealBizStatus("200");//合同状态
//            cmisLmt0011ReqDto.setInputId(ctrAsplDetails.getInputId());//登记人
//            cmisLmt0011ReqDto.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
//            cmisLmt0011ReqDto.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//登记日期
//
//            List<CmisLmt0011OccRelListDto> cmisLmt0011OccRelListDtoList = new ArrayList<CmisLmt0011OccRelListDto>();
//            CmisLmt0011OccRelListDto cmisLmt0011OccRelListDto = new CmisLmt0011OccRelListDto();
//            cmisLmt0011OccRelListDto.setLmtType("01");//额度类型
//            cmisLmt0011OccRelListDto.setLmtSubNo(ctrAsplDetails.getLmtAccNo());//额度分项编号
//            cmisLmt0011OccRelListDto.setBizTotalAmt(ctrAsplDetails.getContAmt());//占用总额(折人民币)
//            cmisLmt0011OccRelListDto.setBizSpacAmt(BigDecimal.ZERO);//占用敞口(折人民币)
//            cmisLmt0011OccRelListDto.setBizTotalAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
//            cmisLmt0011OccRelListDto.setBizSpacAmtCny(BigDecimal.ZERO);//占用敞口(折人民币)
//            cmisLmt0011OccRelListDtoList.add(cmisLmt0011OccRelListDto);
//            cmisLmt0011ReqDto.setCmisLmt0011OccRelListDtoList(cmisLmt0011OccRelListDtoList);
//
//            logger.info("根据业务申请编号【" + iqpAppAspl.getSerno() + "】前往额度系统-额度占用请求报文：" + cmisLmt0011ReqDto.toString());
//            ResultDto<CmisLmt0011RespDto> cmisLmt0011RespDto = cmisLmtClientService.cmisLmt0011(cmisLmt0011ReqDto);
//            logger.info("根据业务申请编号【" + iqpAppAspl.getSerno() + "】前往额度系统-额度占用请求报文：" + cmisLmt0011RespDto.toString());
//
//            code = cmisLmt0011RespDto.getData().getErrorCode();
//            if (!"0000".equals(code)) {
//                logger.info("根据业务申请编号【{}】,前往额度系统-额度占用失败！", iqpAppAspl.getSerno());
//                xdzc0002DataRespDto.setOpFlag(CmisBizConstants.FAIL);
//                xdzc0002DataRespDto.setOpMsg("资产池-协议激活占用额度异常：" + cmisLmt0009RespDto.getData().getErrorMsg());
//                return xdzc0002DataRespDto;
//
//            }
            // 生成归档任务
//            try {
//                logger.info("开始系统生成档案归档信息");
//                String cusId = ctrAsplDetails.getCusId();
//                CusBaseClientDto cusBaseClientDto = iCusClientService.queryCus(cusId);
//                DocArchiveClientDto docArchiveClientDto = new DocArchiveClientDto();
//                docArchiveClientDto.setDocClass("03");//01:基础资料档案,02:授信资料档案,03:重要信息档案
//                docArchiveClientDto.setDocType("08");// TODO 资产池协议激活
//                docArchiveClientDto.setBizSerno(ctrAsplDetails.getSerno());
//                docArchiveClientDto.setCusId(cusId);
//                docArchiveClientDto.setCusName(cusBaseClientDto.getCusName());
//                docArchiveClientDto.setCertType(cusBaseClientDto.getCertType());
//                docArchiveClientDto.setCertCode(cusBaseClientDto.getCertCode());
//                docArchiveClientDto.setManagerId(ctrAsplDetails.getManagerId());
//                docArchiveClientDto.setManagerBrId(ctrAsplDetails.getManagerBrId());
//                docArchiveClientDto.setInputId(ctrAsplDetails.getInputId());
//                docArchiveClientDto.setInputBrId(ctrAsplDetails.getInputBrId());
//                docArchiveClientDto.setContNo(ctrAsplDetails.getContNo());
//                docArchiveClientDto.setLoanAmt(ctrAsplDetails.getContAmt());
//                docArchiveClientDto.setStartDate(ctrAsplDetails.getStartDate());
//                docArchiveClientDto.setEndDate(ctrAsplDetails.getEndDate());
//                docArchiveClientDto.setPrdId(ctrAsplDetails.getPrdId());
//                docArchiveClientDto.setPrdName(ctrAsplDetails.getPrdName());
//                ResultDto<Integer> docArchiveBySys = cmisBizClientService.createDocArchiveBySys(docArchiveClientDto);
//                if(!docArchiveBySys.getCode().equals("0")){
//                    logger.info("系统生成档案归档信息失败" +docArchiveBySys.getMessage());
//                }
//            }catch (Exception e){
//                logger.info("系统生成档案归档信息失败"+ e);
//            }

            if(!"00".equals(ctrAsplDetails.getGuarMode())){

                try {
                    logger.info("调用信贷业务合同信息同步开始");
                    guarBusinessRelService.sendBusinf("03","10",ctrAsplDetails.getContNo());
                    logger.info("调用信贷业务合同信息同步结束");
                }catch (Exception e){
                    logger.info("调用信贷业务合同信息同步失败"+ e);
                }

                // 获取该协议下的担保合同编号
                java.util.List<GrtGuarBizRstRel> GrtGuarBizRstRelList = grtGuarBizRstRelService.getByContNo(ctrAsplDetails.getContNo());
                GrtGuarBizRstRel grtGuarBizRstRel = null;
                // 资产池关联的担保合同只有一个
                if(CollectionUtils.nonEmpty(GrtGuarBizRstRelList)){
                    grtGuarBizRstRel = GrtGuarBizRstRelList.get(0);
                }
                // 担保合同编号
                String guarContNo = null;
                if(null != grtGuarBizRstRel){
                    guarContNo = grtGuarBizRstRel.getGuarContNo();
                }
                try {
                    logger.info("调用信贷担保合同信息同步开始");
                    guarBusinessRelService.sendConinf("03",guarContNo);
                    logger.info("调用信贷担保合同信息同步结束");
                }catch (Exception e){
                    logger.info("调用信贷担保合同信息同步失败"+ e);
                }
            }
        } catch (BizException e) {
            xdzc0002DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
            xdzc0002DataRespDto.setOpMsg("激活失败");// 描述信息
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value);
        return xdzc0002DataRespDto;
    }
}
