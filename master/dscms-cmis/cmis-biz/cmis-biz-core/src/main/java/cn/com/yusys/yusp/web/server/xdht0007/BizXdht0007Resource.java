package cn.com.yusys.yusp.web.server.xdht0007;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0007.req.Xdht0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0007.resp.Xdht0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdht0007.Xdht0007Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:借款合同/担保合同列表信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0005:借款合同/担保合同列表信息查询")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0007Resource.class);

    @Autowired
    private Xdht0007Service xdht0007Service;

    /**
     * 交易码：xdht0007
     * 交易描述：借款合同/担保合同列表信息查询
     *
     * @param xdht0007DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借款合同/担保合同列表信息查询")
    @PostMapping("/xdht0007")
    protected @ResponseBody
    ResultDto<Xdht0007DataRespDto> xdht0007(@Validated @RequestBody Xdht0007DataReqDto xdht0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007DataReqDto));
        Xdht0007DataRespDto xdht0007DataRespDto = new Xdht0007DataRespDto();// 响应Dto:借款合同/担保合同列表信息查询
        ResultDto<Xdht0007DataRespDto> xdht0007DataResultDto = new ResultDto<>();

        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007DataReqDto));
            xdht0007DataRespDto = xdht0007Service.getXdht0007(xdht0007DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007DataRespDto));

            // 封装xdht0007DataResultDto中正确的返回码和返回信息
            xdht0007DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0007DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, e.getMessage());
            // 封装xdht0007DataResultDto中异常返回码和返回信息
            xdht0007DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0007DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0007DataRespDto到xdht0007DataResultDto中
        xdht0007DataResultDto.setData(xdht0007DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007DataRespDto));
        return xdht0007DataResultDto;
    }
}
