package cn.com.yusys.yusp.service.server.xdht0013;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.server.xdht0013.req.Xdht0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0013.resp.Xdht0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdht0013Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 徐超
 * @创建时间: 2021-05-06 11:36:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdht0013Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0013Service.class);

    @Resource
    private PvpLoanAppMapper pvpLoanAppMapper;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Resource
    private CtrLoanContMapper ctrLoanContMapper;
    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 合同信息查询(微信小程序)
     *
     * @param xdht0013DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdht0013DataRespDto getXdht0013(Xdht0013DataReqDto xdht0013DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013DataReqDto));
        Xdht0013DataRespDto xdht0013DataRespDto = null;
        try {
            xdht0013DataRespDto = pvpLoanAppMapper.getContInfoByBillNo(xdht0013DataReqDto);
            if (Objects.isNull(xdht0013DataRespDto)) {
                xdht0013DataRespDto = new Xdht0013DataRespDto();
            }
            String managerId = xdht0013DataRespDto.getManagerId();
            String contNo = xdht0013DataRespDto.getContNo();
            String loanUseType = "";//借款用途

            if (StringUtil.isNotEmpty(managerId)) {//客户经理不为空
                // 根据工号获取用户表中的联系电话
                logger.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                logger.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                String code = resultDto.getCode();//返回结果
                if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    String huser = adminSmUserDto.getUserName();
                    xdht0013DataRespDto.setHuser(huser);
                }
            }
            if(StringUtil.isNotEmpty(contNo)){//合同号不为空
                logger.info("根据合同编号【{}】查询合同信息开始", contNo);
                CtrLoanCont ctrLoanCont = Optional.ofNullable(ctrLoanContMapper.selectByPrimaryKey(contNo)).orElse(new CtrLoanCont());
                logger.info("***************根据合同编号【{}】查询合同信息结束,查询结果信息【{}】", contNo, JSON.toJSONString(ctrLoanCont));
                String prdId = ctrLoanCont.getPrdId();
                if (StringUtil.isNotEmpty(prdId)) {
                    LmtCrdReplyInfo lmtCrdReplyInfo = Optional.ofNullable(lmtCrdReplyInfoMapper.selectBySurveySerno(ctrLoanCont.getSurveySerno())).orElse(new LmtCrdReplyInfo());
                    String isWxbxd = lmtCrdReplyInfo.getIsWxbxd();
                    String limitType = lmtCrdReplyInfo.getLimitType();//[{"key":"01","value":"临时额度"},{"key":"02","value":"循环额度"}]
                    if ("1".equals(isWxbxd)) {//无还本续贷
                        loanUseType = "归还贷款";
                    } else {
                        logger.info("***********调用iCmisCfgClientService查询产品类别*START**************");
                        ResultDto<CfgPrdBasicinfoDto> prdresultDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                        String prdCode = prdresultDto.getCode();//返回结果
                        String prdType = net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils.EMPTY;
                        if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                            CfgPrdBasicinfoDto CfgPrdBasicinfoDto = prdresultDto.getData();
                            if (CfgPrdBasicinfoDto != null) {
                                prdType = CfgPrdBasicinfoDto.getPrdType();
                            }
                        }
                        if (DscmsBizTzEnum.PRDTYPE_08.key.equals(prdType)) {//经营
                            loanUseType = "资金周转";
                        } else if (DscmsBizTzEnum.PRDTYPE_09.key.equals(prdType)) {//消费
                            if ("02".equals(limitType)) {
                                loanUseType = "综合消费";
                            } else {
                                loanUseType = ctrLoanCont.getLoanPurp();
                            }
                        }else{
                            loanUseType = ctrLoanCont.getLoanPurp();
                        }
                        logger.info("***********调用iCmisCfgClientService查询产品类别*END**************");
                    }
                }
                xdht0013DataRespDto.setLoanUseType(loanUseType);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013DataRespDto));
        return xdht0013DataRespDto;
    }

}
