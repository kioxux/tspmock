package cn.com.yusys.yusp.service.server.xdxw0055;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmTreeDicDto;
import cn.com.yusys.yusp.dto.server.xdxw0055.req.Xdxw0055DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0055.resp.Xdxw0055DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportComInfoMapper;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0055Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: xull2
 * @创建时间: 2021-05-05 19:46:46
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0055Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0055Service.class);

    @Autowired
    private LmtSurveyReportComInfoMapper lmtSurveyReportComInfoMapper;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    /**
     * 经营地址查询
     *
     * @param Xdxw0055DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0055DataRespDto queryaddressBySurveySerno(Xdxw0055DataReqDto Xdxw0055DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value);
        Xdxw0055DataRespDto Xdxw0055DataRespDto = new Xdxw0055DataRespDto();
        String SurveySerno = Xdxw0055DataReqDto.getIndgtSerno();//客户调查表编号
        try {
            logger.info("根据业务唯一编号查询调查企业表信息开始,查询参数为:{}", JSON.toJSONString(SurveySerno));
            String address = lmtSurveyReportComInfoMapper.selectOperAddrBySurveySerno(SurveySerno);
            logger.info("根据业务唯一编号查询调查企业表信息结束,返回结果为:{}", JSON.toJSONString(address));
            //根据区域编码获取对应的地址信息
            AdminSmTreeDicDto adminSmTreeDicDto = new AdminSmTreeDicDto();
            adminSmTreeDicDto.setOptType("STD_ZB_AREA_CODE");
            adminSmTreeDicDto.setCode(address);
            ResultDto<Map<String, String>> resultDto = iCmisCfgClientService.querySingleLabelPath(adminSmTreeDicDto);
            String prdCode = resultDto.getCode();//返回结果
            if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                Map<String, String> map = resultDto.getData();
                logger.info("根据业务唯一编号查询调查结论表信息结束,返回结果为:{}", JSON.toJSONString(map));
                if (map != null && map.containsKey("labelPath")) {
                    address = map.get("labelPath");
                }
            }
            Xdxw0055DataRespDto.setOperAddr(address);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value);
        return Xdxw0055DataRespDto;
    }
}
