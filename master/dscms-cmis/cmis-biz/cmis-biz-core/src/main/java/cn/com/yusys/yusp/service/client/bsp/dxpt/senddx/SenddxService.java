package cn.com.yusys.yusp.service.client.bsp.dxpt.senddx;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：短信/微信发送批量接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Service
public class SenddxService {
    private static final Logger logger = LoggerFactory.getLogger(SenddxService.class);
    // 1）注入：BSP封装调用短信平台的接口
    @Autowired
    private Dscms2DxptClientService dscms2DxptClientService;

    /**
     * 业务逻辑处理方法：短信/微信发送批量接口
     *
     * @param senddxReqDto
     * @return
     */
    @Transactional
    public SenddxRespDto senddx(SenddxReqDto senddxReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxReqDto));
        ResultDto<SenddxRespDto> senddxResultDto = dscms2DxptClientService.senddx(senddxReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxResultDto));

        String senddxCode = Optional.ofNullable(senddxResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String senddxMeesage = Optional.ofNullable(senddxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        SenddxRespDto senddxRespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, senddxResultDto.getCode())) {
            //  获取相关的值并解析
            senddxRespDto = senddxResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(senddxCode, senddxMeesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
        return senddxRespDto;
    }
}
