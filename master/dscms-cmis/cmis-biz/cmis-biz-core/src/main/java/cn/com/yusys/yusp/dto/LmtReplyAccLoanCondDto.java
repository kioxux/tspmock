package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplyAccLoanCond
 * @类描述: lmt_reply_acc_loan_cond数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-27 16:06:45
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtReplyAccLoanCondDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 授信台账号 **/
	private String accNo;
	
	/** 条件类型 **/
	private String condType;
	
	/** 条件说明 **/
	private String condDesc;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param accNo
	 */
	public void setAccNo(String accNo) {
		this.accNo = accNo == null ? null : accNo.trim();
	}
	
    /**
     * @return AccNo
     */	
	public String getAccNo() {
		return this.accNo;
	}
	
	/**
	 * @param condType
	 */
	public void setCondType(String condType) {
		this.condType = condType == null ? null : condType.trim();
	}
	
    /**
     * @return CondType
     */	
	public String getCondType() {
		return this.condType;
	}
	
	/**
	 * @param condDesc
	 */
	public void setCondDesc(String condDesc) {
		this.condDesc = condDesc == null ? null : condDesc.trim();
	}
	
    /**
     * @return CondDesc
     */	
	public String getCondDesc() {
		return this.condDesc;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}