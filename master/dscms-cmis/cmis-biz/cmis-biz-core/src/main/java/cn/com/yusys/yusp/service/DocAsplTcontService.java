/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.AsplIoPool;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.domain.DocAsplTcont;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.repository.mapper.DocAsplTcontMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.math.BigDecimal;
import java.util.*;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: DocAsplTcontService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-12 11:15:46
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocAsplTcontService {
    private static final Logger log = LoggerFactory.getLogger(IqpEntrustLoanAppService.class);

    @Autowired
    private DocAsplTcontMapper docAsplTcontMapper;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private WorkflowCoreClient workflowCoreClient;

    @Autowired
    private AccTContRelService accTContRelService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public DocAsplTcont selectByPrimaryKey(String pkId) {
        return docAsplTcontMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<DocAsplTcont> selectAll(QueryModel model) {
        List<DocAsplTcont> records = (List<DocAsplTcont>) docAsplTcontMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<DocAsplTcont> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocAsplTcont> list = docAsplTcontMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(DocAsplTcont record) {
        return docAsplTcontMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    @Transactional
    public Map<String,String> insertSelective(DocAsplTcont record) {
        String rtnCode = "9999";
        String rtnMsg = "贸易合同新增失败";
        HashMap returnMap = new HashMap<String, String>();

        try {
            String cusId = record.getCusId();
            String tcontNo = record.getTcontNo();
            // 判断用户是否是资产池用户
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoByCusId(cusId);
            if(Objects.isNull(ctrAsplDetails)){
                rtnMsg = "客户编号："+cusId+"，未签订有效的资产池协议";
                returnMap.put("rtnCode",rtnCode);
                returnMap.put("rtnMsg",rtnMsg);
                return returnMap;
            }
            record.setPkId(UUID.randomUUID().toString().replace("-", ""));
            // 生成合同编号
            // String tcontNo = sequenceTemplateClient.getSequenceTemplate(SeqConstant.CONT_NO, new HashMap());
            String tcontImgId = sequenceTemplateClient.getSequenceTemplate(SeqConstant.YX_SERNO_SEQ, new HashMap());
            record.setTcontImgId(tcontImgId);
            record.setTcontNo(tcontNo);
            record.setCusId(cusId);
            record.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            record.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
            record.setContNo(ctrAsplDetails.getContNo());
            User userInfo = SessionUtils.getUserInformation();
            // 影像流水号,默认贸易合同编号
            if (userInfo == null) {
                throw new BizException(null, "", null, "后台未获取到当前登录人员信息");
            } else {
                record.setInputId(userInfo.getLoginCode());
                record.setInputBrId(userInfo.getOrg().getCode());
                record.setManagerId(userInfo.getLoginCode());
                record.setManagerBrId(userInfo.getOrg().getCode());
                record.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
            }
            if (docAsplTcontMapper.insertSelective(record) > 0) {
                rtnCode = "0000";
                rtnMsg = "贸易合同新增成功";
                // 新增成功，返回贸易合同编号
                returnMap.put("tcontNo",tcontNo);
                returnMap.put("tcontImgId",tcontImgId);
            }
        }catch(BizException e){
            log.error("贸易合同新增异常！",e.getMessage());
            throw new BizException(null, "", null, "贸易合同新增异常");
        }finally {
            returnMap.put("rtnCode",rtnCode);
            returnMap.put("rtnMsg",rtnMsg);
        }
        return returnMap;
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(DocAsplTcont record) {
        return docAsplTcontMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(DocAsplTcont record) {
        return docAsplTcontMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        DocAsplTcont docAsplTcont = selectByPrimaryKey(pkId);
        int rest = 0;
        if(Objects.equals(CmisCommonConstants.WF_STATUS_992,docAsplTcont.getApproveStatus())){
            rest = updateApproveStatusAfterFlow(docAsplTcont,CmisCommonConstants.WF_STATUS_996);
            workflowCoreClient.deleteByBizId(docAsplTcont.getTcontImgId());
        }else{
            rest = docAsplTcontMapper.deleteByPrimaryKey(pkId);
        }
        return rest;
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docAsplTcontMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: updateContNoByTcontNo
     * @方法描述: 根据贸易合同编号更新贸易合同表中的合同编号
     * @参数与返回说明:
     * @算法描述: 无
     */

    public void updateContNoByTcontNo(String tcontNo, String contNo) {
        HashMap map = new HashMap();
        map.put("tcontNo",tcontNo);
        map.put("contNo",contNo);
        docAsplTcontMapper.updateContNoByTcontNo(map);
    }
    /**
     * @方法名称: selectByAsplAccpTask
     * @方法描述: 根据贸易背景收集任务表去查询贸易合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    public java.util.List<cn.com.yusys.yusp.dto.server.xdzc0016.resp.List> selectByAsplAccpTask(QueryModel model) {
        return docAsplTcontMapper.xdzc0016(model);
    }

    /**
     * @方法名称: updateEmptyByTContNo
     * @方法描述: 根据贸易合同编号置空
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int  updateEmptyByTContNo(String tContNo) {
        return docAsplTcontMapper.updateEmptyByTContNo(tContNo);
    }

    /**
     * @方法名称: selectByTcontNo
     * @方法描述: 根据贸易合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public DocAsplTcont selectByTcontSerno(String tcontImgId) {
        return docAsplTcontMapper.selectByTcontSerno(tcontImgId);
    }
    /**
     * @方法名称: updateApproveStatusAfterFlow
     * @方法描述: 更新状态信息
     * @参数与返回说明:
     * @算法描述:
     * @创建人: css
     * @创建时间: 2021-06-15 15:20:45
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public int updateApproveStatusAfterFlow(DocAsplTcont docAsplTcont, String wfStatus) {
        // 流程结束后 更新审批状态
        docAsplTcont.setApproveStatus(wfStatus);
        return updateSelective(docAsplTcont);
    }

    /**
     * 可用信用总金额
     * @return
     */
    public BigDecimal getContHighAvlAmt(DocAsplTcont docAsplTcont) {
        BigDecimal sum = Optional.ofNullable(docAsplTcont.getContAmt()).orElse(BigDecimal.ZERO);
        // 通过购销合同去获取 1、出票金额 2、贷款金额 acc_t_cont_rel 购销合同业务关联表
        // 开始计算  查台账（ 银承、贷款）根据关联流水号查询 acc_t_cont_rel
        BigDecimal loanSum = Optional.ofNullable(accTContRelService.selectSumLoan(docAsplTcont.getCusId(),docAsplTcont.getTcontImgId())).orElse(BigDecimal.ZERO);
        BigDecimal accpSum = Optional.ofNullable(accTContRelService.selectSumAccp(docAsplTcont.getCusId(),docAsplTcont.getTcontImgId())).orElse(BigDecimal.ZERO);
        return sum.subtract(loanSum).subtract(accpSum);
    }
}
