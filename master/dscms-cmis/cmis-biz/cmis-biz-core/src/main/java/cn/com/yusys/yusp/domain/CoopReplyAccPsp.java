/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopReplyAccPsp
 * @类描述: coop_reply_acc_psp数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-25 13:44:17
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_reply_acc_psp")
public class CoopReplyAccPsp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 批复编号 **/
	@Id
	@Column(name = "REPLY_NO")
	private String replyNo;
	
	/** 序号 **/
	@Column(name = "serial", unique = false, nullable = true, length = 10)
	private Integer serial;
	
	/** 具体内容 **/
	@Column(name = "MANAGE_CONTENT", unique = false, nullable = true, length = 2000)
	private String manageContent;
	
	/** 创建时间 **/
	@Column(name = "CREAT_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date creatTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param serial
	 */
	public void setSerial(Integer serial) {
		this.serial = serial;
	}
	
    /**
     * @return serial
     */
	public Integer getSerial() {
		return this.serial;
	}
	
	/**
	 * @param manageContent
	 */
	public void setManageContent(String manageContent) {
		this.manageContent = manageContent;
	}
	
    /**
     * @return manageContent
     */
	public String getManageContent() {
		return this.manageContent;
	}
	
	/**
	 * @param creatTime
	 */
	public void setCreatTime(java.util.Date creatTime) {
		this.creatTime = creatTime;
	}
	
    /**
     * @return creatTime
     */
	public java.util.Date getCreatTime() {
		return this.creatTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}