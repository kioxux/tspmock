package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.IqpHouseService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0078Service
 * @类描述: 个人商用房贷款校验
 * @功能描述: 个人商用房贷款校验
 * @创建人: hubp
 * @创建时间: 2021年8月11日08:38:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0082Service {

    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private IqpHouseService iqpHouseService;

    /**
     * @方法名称: riskItem0083
     * @方法描述: 个人商用房贷款校验
     * @参数与返回说明:
     * @算法描述:  个人商用房贷款 申请金额最高不得大于房价的50%
     * @创建人: hubp
     * @创建时间: 2021年8月11日08:38:26
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0082(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String iqpSerno = queryModel.getCondition().get("bizId").toString();
        if (StringUtils.isBlank(iqpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(iqpSerno);
        if (Objects.isNull(iqpLoanApp) ||  StringUtils.isBlank(iqpLoanApp.getPrdId())
                || Objects.isNull(iqpLoanApp.getAppAmt())) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0003);
            return riskResultDto;
        } else {
            // 个人商用房贷款 申请金额最高不得大于房价的50%
            if ("022020".equals(iqpLoanApp.getPrdId()) || "022021".equals(iqpLoanApp.getPrdId())) {
                IqpHouse iqpHouse = iqpHouseService.selectByIqpSernos(iqpSerno);
                if(Objects.isNull(iqpHouse) || Objects.isNull(iqpHouse.getHouseTotal())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08202);
                    return riskResultDto;
                } else{
                    if (iqpLoanApp.getAppAmt().compareTo((iqpHouse.getHouseTotal().multiply(BigDecimal.valueOf(0.5)))) > 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08201);
                        return riskResultDto;
                    }
                }
                // 个人商用房贷款最长期限120个月
                if(iqpLoanApp.getAppTerm().compareTo(new BigDecimal(120)) > 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_08203);
                    return riskResultDto;
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
