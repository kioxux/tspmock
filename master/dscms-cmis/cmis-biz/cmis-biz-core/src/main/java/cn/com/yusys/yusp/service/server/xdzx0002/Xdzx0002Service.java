package cn.com.yusys.yusp.service.server.xdzx0002;

import cn.com.yusys.yusp.cmis.commons.uitls.DictTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtSurveyReportMainInfo;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.BillList;
import cn.com.yusys.yusp.dto.server.xdzx0002.req.Xdzx0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.resp.List;
import cn.com.yusys.yusp.dto.server.xdzx0002.resp.Xdzx0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CreditReportQryLstMapper;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:征信授权查看
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdzx0002Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdzx0002Service.class);

    @Autowired
    private CreditReportQryLstMapper creditReportQryLstMapper;
    @Autowired
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    /**
     * 征信授权查看
     *
     * @param xdzx0002DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdzx0002DataRespDto xdzx0002(Xdzx0002DataReqDto xdzx0002DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002DataReqDto));
        Xdzx0002DataRespDto xdzx0002DataRespDto = new Xdzx0002DataRespDto();
        try {

            logger.info("***********征信授权查看开始,查询参数为:{}", JSON.toJSONString(xdzx0002DataReqDto));
            java.util.List<List> list = Optional.ofNullable(creditReportQryLstMapper.selectZxAuth(xdzx0002DataReqDto)).orElse(new ArrayList<>());
            logger.info("***********征信授权查看结束,返回结果为:{}", JSON.toJSONString(list));
            //查询产品名称和产品编号
            list = list.parallelStream().map(ret -> {
                cn.com.yusys.yusp.dto.server.xdzx0002.resp.List temp = new cn.com.yusys.yusp.dto.server.xdzx0002.resp.List();
                BeanUtils.copyProperties(ret, temp);
                //查询产品名称和产品编号
                String crqlSerno = temp.getCrqlSerno();//查询流水号
                java.util.Map<String, String> map = lmtSurveyReportMainInfoMapper.selectByCrqlSerno(crqlSerno);
                if (map != null) {
                    if (map.containsKey("prdid")) {
                        temp.setPrdId(map.get("prdid"));
                    }
                    if (map.containsKey("prdname")) {
                        temp.setPrdName(map.get("prdname"));
                    }
                }
                //返回列表
                return temp;
            }).collect(Collectors.toList());
            logger.info("获取字典项缓存结束");

            xdzx0002DataRespDto.setList(list);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002DataRespDto));
        return xdzx0002DataRespDto;
    }
}
