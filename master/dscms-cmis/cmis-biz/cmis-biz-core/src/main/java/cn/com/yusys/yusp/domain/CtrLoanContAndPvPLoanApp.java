package cn.com.yusys.yusp.domain;

import java.math.BigDecimal;

/**
 * @创建人 WH
 * @创建时间 2021-04-17
 * @return CtrLoanContAndPvPLoanApp
 **/

public class CtrLoanContAndPvPLoanApp extends CtrLoanCont {
    /**
     * 放款金额
     */
    private java.math.BigDecimal pvpAmt;
    /**
     * 业务流水号
     */
    private String pvpSerno;

    /**
     * 借据编号
     */
    private String billNo;


    /**
     * 是否立即发起受托支付 STD_ZB_YES_NO
     */
    private String isCfirmPay;

    /**
     * 申请期限
     */
    private String approveTerm;
    /**
     * 授权状态 STD_ZB_AUTH_ST
     */
    private String authStatus;


    /**
     * 放款机构
     */
    private String acctBrId;

    /**
     * 申请状态 STD_ZB_APP_ST
     */
    private String approveStatus;
    /**
     * 到期日期
     */
    private String endDate;
    /**
     * 操作类型  STD_ZB_OPR_TYPE
     */
    private String oprType;
    /**
     * 客户评级系数
     */
    private java.math.BigDecimal customerRatingFactor;

    @Override
    public String toString() {
        return "CtrLoanContAndPvPLoanApp{" +
                "pvpAmt=" + pvpAmt +
                ", pvpSerno='" + pvpSerno + '\'' +
                ", billNo='" + billNo + '\'' +
                ", isCfirmPay='" + isCfirmPay + '\'' +
                ", approveTerm='" + approveTerm + '\'' +
                ", authStatus='" + authStatus + '\'' +
                ", acctBrId='" + acctBrId + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", endDate='" + endDate + '\'' +
                ", oprType='" + oprType + '\'' +
                ", customerRatingFactor=" + customerRatingFactor +
                '}';
    }

    public BigDecimal getPvpAmt() {
        return pvpAmt;
    }

    public void setPvpAmt(BigDecimal pvpAmt) {
        this.pvpAmt = pvpAmt;
    }

    public String getPvpSerno() {
        return pvpSerno;
    }

    public void setPvpSerno(String pvpSerno) {
        this.pvpSerno = pvpSerno;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getIsCfirmPay() {
        return isCfirmPay;
    }

    public void setIsCfirmPay(String isCfirmPay) {
        this.isCfirmPay = isCfirmPay;
    }

    public String getApproveTerm() {
        return approveTerm;
    }

    public void setApproveTerm(String approveTerm) {
        this.approveTerm = approveTerm;
    }

    public String getAuthStatus() {
        return authStatus;
    }

    public void setAuthStatus(String authStatus) {
        this.authStatus = authStatus;
    }

    public String getAcctBrId() {
        return acctBrId;
    }

    public void setAcctBrId(String acctBrId) {
        this.acctBrId = acctBrId;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOprType() {
        return oprType;
    }

    public void setOprType(String oprType) {
        this.oprType = oprType;
    }

    public BigDecimal getCustomerRatingFactor() {
        return customerRatingFactor;
    }

    public void setCustomerRatingFactor(BigDecimal customerRatingFactor) {
        this.customerRatingFactor = customerRatingFactor;
    }
}
