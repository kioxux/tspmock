package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.module.adapter.query.PageQuery;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.domain.GuarInfOtherHouse;

public class GuarBasicGuarInfOtherHouseVo extends PageQuery {
    private GuarBaseInfo guarBaseInfo;
    private GuarInfOtherHouse guarInfOtherHouse;

    public GuarBaseInfo getGuarBaseInfo() {
        return guarBaseInfo;
    }

    public void setGuarBaseInfo(GuarBaseInfo guarBaseInfo) {
        this.guarBaseInfo = guarBaseInfo;
    }

    public GuarInfOtherHouse getGuarInfOtherHouse() {
        return guarInfOtherHouse;
    }

    public void setGuarInfOtherHouse(GuarInfOtherHouse guarInfOtherHouse) {
        this.guarInfOtherHouse = guarInfOtherHouse;
    }
}
