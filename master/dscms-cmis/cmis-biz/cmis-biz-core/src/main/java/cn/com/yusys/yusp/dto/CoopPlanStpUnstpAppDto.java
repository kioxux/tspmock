package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanStpUnstpApp
 * @类描述: coop_plan_stp_unstp_app数据实体类
 * @功能描述: 
 * @创建人: AbsonZ
 * @创建时间: 2021-04-17 09:18:06
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class CoopPlanStpUnstpAppDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	private String serno;
	
	/** 申请类型 **/
	private String appType;
	
	/** 合作方案编号 **/
	private String coopPlanNo;
	
	/** 合作方编号 **/
	private String partnerNo;
	
	/** 合作方名称 **/
	private String partnerName;
	
	/** 总合作额度（元） **/
	private java.math.BigDecimal totlCoopLmtAmt;
	
	/** 合作起始日 **/
	private String coopStartDate;
	
	/** 合作到期日 **/
	private String coopEndDate;
	
	/** 申请原因 **/
	private String appResn;
	
	/** 审批状态 **/
	private String apprStatus;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最近修改人 **/
	private String updId;
	
	/** 最近修改机构 **/
	private String updBrId;
	
	/** 最近修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param appType
	 */
	public void setAppType(String appType) {
		this.appType = appType == null ? null : appType.trim();
	}
	
    /**
     * @return AppType
     */	
	public String getAppType() {
		return this.appType;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo == null ? null : coopPlanNo.trim();
	}
	
    /**
     * @return CoopPlanNo
     */	
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo == null ? null : partnerNo.trim();
	}
	
    /**
     * @return PartnerNo
     */	
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName == null ? null : partnerName.trim();
	}
	
    /**
     * @return PartnerName
     */	
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param totlCoopLmtAmt
	 */
	public void setTotlCoopLmtAmt(java.math.BigDecimal totlCoopLmtAmt) {
		this.totlCoopLmtAmt = totlCoopLmtAmt;
	}
	
    /**
     * @return TotlCoopLmtAmt
     */	
	public java.math.BigDecimal getTotlCoopLmtAmt() {
		return this.totlCoopLmtAmt;
	}
	
	/**
	 * @param coopStartDate
	 */
	public void setCoopStartDate(String coopStartDate) {
		this.coopStartDate = coopStartDate == null ? null : coopStartDate.trim();
	}
	
    /**
     * @return CoopStartDate
     */	
	public String getCoopStartDate() {
		return this.coopStartDate;
	}
	
	/**
	 * @param coopEndDate
	 */
	public void setCoopEndDate(String coopEndDate) {
		this.coopEndDate = coopEndDate == null ? null : coopEndDate.trim();
	}
	
    /**
     * @return CoopEndDate
     */	
	public String getCoopEndDate() {
		return this.coopEndDate;
	}
	
	/**
	 * @param appResn
	 */
	public void setAppResn(String appResn) {
		this.appResn = appResn == null ? null : appResn.trim();
	}
	
    /**
     * @return AppResn
     */	
	public String getAppResn() {
		return this.appResn;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus == null ? null : apprStatus.trim();
	}
	
    /**
     * @return ApprStatus
     */	
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}