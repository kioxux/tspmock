/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.LmtReplySub;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtReplySubMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:34:05
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface LmtReplySubMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    LmtReplySub selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<LmtReplySub> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(LmtReplySub record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(LmtReplySub record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(LmtReplySub record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(LmtReplySub record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
       * @方法名称：getReplySub
       * @方法描述：根据条件查询
       * @参数与返回说明：
       * @算法描述：
       * @创建人：zhangming12
       * @创建时间：2021-04-21 下午 10:22
       * @修改记录：修改时间   修改人员  修改原因
       */
    List<LmtReplySub> queryLmtReplySubByParams(Map queryMaps);

    /**
     * @方法名称：queryLmtReplySubByReplySubSerno
     * @方法描述：根据条件查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：mashun
     * @创建时间：2021-04-21 下午 10:22
     * @修改记录：修改时间   修改人员  修改原因
     */
    LmtReplySub queryLmtReplySubByReplySubSerno(String replySubNo);


    /**
     * @方法名称：queryByReplySerno
     * @方法描述：根据条件查询
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-04-21 下午 10:22
     * @修改记录：修改时间   修改人员  修改原因
     */
    LmtReplySub queryByReplySerno(String replySerno);


    List<LmtReplySub> selectBySerno(String serno);

    /**
     * 查询一般额度分项信息
     * @param serno
     * @return
     */
    List<LmtReplySub> getNormal(String serno);

    /**
     * 查询低风险额度分项信息
     * @param serno
     * @return
     */
    List<LmtReplySub> getLow(String serno);

    /**
     * @函数名称:queryLmtAppSubByGrpSerno
     * @函数描述:根据流水号查询授信分项信息
     * @参数与返回说明:
     * @算法描述:
     */
    List<cn.com.yusys.yusp.dto.server.xdsx0013.resp.List> querySubInfoBySerno(String serno);

    /**
     * @函数名称:deleteBySerno
     * @函数描述:根据流水号删除授信分项信息
     * @参数与返回说明:
     * @算法描述:
     */
    int deleteBySerno(String serno);
}
