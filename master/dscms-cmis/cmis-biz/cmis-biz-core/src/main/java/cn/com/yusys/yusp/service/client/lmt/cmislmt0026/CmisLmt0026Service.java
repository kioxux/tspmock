package cn.com.yusys.yusp.service.client.lmt.cmislmt0026;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0026.req.CmisLmt0026ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0026.resp.CmisLmt0026RespDto;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * 业务逻辑处理类：额度分项信息查询
 *
 * @author xll
 * @version 1.0
 * @since 2021年5月10日 下午1:22:06
 */
@Service
public class CmisLmt0026Service {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0026Service.class);

    // 1）注入：封装的接口类:额度管理模块
    @Autowired
    private CmisLmtClientService cmisLmtClientService;


    /**
     * 业务逻辑处理方法：额度分项信息查询
     *
     * @param cmisLmt0026ReqDto
     * @return
     */
    @Transactional
    public CmisLmt0026RespDto cmisLmt0026(CmisLmt0026ReqDto cmisLmt0026ReqDto) throws BizException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value, JSON.toJSONString(cmisLmt0026ReqDto));
        ResultDto<CmisLmt0026RespDto> cmisLmt0026ResultDto = cmisLmtClientService.cmislmt0026(cmisLmt0026ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value, JSON.toJSONString(cmisLmt0026ResultDto));

        String cmisLmt0026Code = Optional.ofNullable(cmisLmt0026ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String cmisLmt0026Meesage = Optional.ofNullable(cmisLmt0026ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        CmisLmt0026RespDto cmisLmt0026RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0026ResultDto.getCode()) && cmisLmt0026ResultDto.getData() != null
                && Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0026ResultDto.getData().getErrorCode())) {
            //  获取相关的值并解析
            cmisLmt0026RespDto = cmisLmt0026ResultDto.getData();
            if (!Objects.equals(SuccessEnum.SUCCESS.key, cmisLmt0026RespDto.getErrorCode())) {
                throw BizException.error(null, cmisLmt0026RespDto.getErrorCode(), cmisLmt0026RespDto.getErrorMsg());
            }
        } else {
            if (cmisLmt0026ResultDto.getData() != null) {
                cmisLmt0026Code = cmisLmt0026ResultDto.getData().getErrorCode();
                cmisLmt0026Meesage = cmisLmt0026ResultDto.getData().getErrorMsg();
            } else {
                cmisLmt0026Code = EpbEnum.EPB099999.key;
                cmisLmt0026Meesage = EpbEnum.EPB099999.value;
            }
            //  抛出错误异常
            throw BizException.error(null, cmisLmt0026Code, cmisLmt0026Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0026.value);
        return cmisLmt0026RespDto;
    }


}
