package cn.com.yusys.yusp.repository.mapper;


import cn.com.yusys.yusp.dto.server.xddb0017.resp.SlSernoQueryDto;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 双录流水号与押品编号关系维护mapper
 */
public interface SlSernoQueryMapper {

    /**
     * @param QueryMap
     * @return
     */
    int updateSlSernoQuery(Map QueryMap);


    /**
     * @param QueryMap
     * @return
     */
    int insertSlSernoQuery(Map QueryMap);

//    /**
//     * @param QueryMap
//     * @return
//     */
//    java.util.List<Map<String, String>> selectSlSernoQueryByGuarNo(Map QueryMap);

    /**
     * @param QueryMap
     * @return
     */
    java.util.List<SlSernoQueryDto> selectSlSernoQueryByGuarNo(Map QueryMap);

    /**
     * @param coreGrtNo
     * @return
     */
    String getCoreGrtNo(@Param("coreGrtNo") String coreGrtNo);
}
