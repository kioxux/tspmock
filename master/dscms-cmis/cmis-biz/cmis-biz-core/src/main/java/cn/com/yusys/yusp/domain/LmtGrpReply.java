/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReply
 * @类描述: lmt_grp_reply数据实体类
 * @功能描述: 
 * @创建人: DELL
 * @创建时间: 2021-04-20 11:06:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_grp_reply")
public class LmtGrpReply extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;

	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;

	/** 集团批复流水号 **/
	@Column(name = "GRP_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String grpReplySerno;

	/** 集团申请流水号 **/
	@Column(name = "GRP_SERNO", unique = false, nullable = true, length = 40)
	private String grpSerno;

	/** 原集团授信批复流水号 **/
	@Column(name = "ORIGI_GRP_REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String origiGrpReplySerno;

	/** 批复生效日期 **/
	@Column(name = "REPLY_INURE_DATE", unique = false, nullable = true, length = 10)
	private String replyInureDate;

	/** 授信类型 **/
	@Column(name = "LMT_TYPE", unique = false, nullable = true, length = 5)
	private String lmtType;

	/** 集团客户编号 **/
	@Column(name = "GRP_CUS_ID", unique = false, nullable = true, length = 40)
	private String grpCusId;

	/** 集团客户名称 **/
	@Column(name = "GRP_CUS_NAME", unique = false, nullable = true, length = 80)
	private String grpCusName;

	/** 客户类型 **/
	@Column(name = "CUS_TYPE", unique = false, nullable = true, length = 5)
	private String cusType;

	/** 审批模式 **/
	@Column(name = "APPR_MODE", unique = false, nullable = true, length = 5)
	private String apprMode;

	/** 终审机构 **/
	@Column(name = "FINAL_APPR_BR_TYPE", unique = false, nullable = true, length = 20)
	private String finalApprBrType;

	/** 审批结论 **/
	@Column(name = "APPR_RESULT", unique = false, nullable = true, length = 5)
	private String apprResult;

	/** 批复状态 **/
	@Column(name = "REPLY_STATUS", unique = false, nullable = true, length = 5)
	private String replyStatus;

	/** 用信审核方式 **/
	@Column(name = "LOAN_APPR_MODE", unique = false, nullable = true, length = 5)
	private String loanApprMode;

	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;

	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;

	/** 贷后管理要求 **/
	@Column(name = "PSP_MANA_NEED", unique = false, nullable = true, length = 4000)
	private String pspManaNeed;

	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;

	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;

	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;

	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;

	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;

	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;

	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;

	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;

	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;

	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;

	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 敞口额度合计 **/
	@Column(name = "OPEN_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal openTotalLmtAmt;

	/** 低风险额度合计 **/
	@Column(name = "LOW_RISK_TOTAL_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lowRiskTotalLmtAmt;


	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	/**
	 * @return pkId
	 */
	public String getPkId() {
		return this.pkId;
	}

	/**
	 * @param grpReplySerno
	 */
	public void setGrpReplySerno(String grpReplySerno) {
		this.grpReplySerno = grpReplySerno;
	}

	/**
	 * @return grpReplySerno
	 */
	public String getGrpReplySerno() {
		return this.grpReplySerno;
	}

	/**
	 * @param grpSerno
	 */
	public void setGrpSerno(String grpSerno) {
		this.grpSerno = grpSerno;
	}

	/**
	 * @return grpSerno
	 */
	public String getGrpSerno() {
		return this.grpSerno;
	}

	/**
	 * @param origiGrpReplySerno
	 */
	public void setOrigiGrpReplySerno(String origiGrpReplySerno) {
		this.origiGrpReplySerno = origiGrpReplySerno;
	}

	/**
	 * @return origiGrpReplySerno
	 */
	public String getOrigiGrpReplySerno() {
		return this.origiGrpReplySerno;
	}

	/**
	 * @param replyInureDate
	 */
	public void setReplyInureDate(String replyInureDate) {
		this.replyInureDate = replyInureDate;
	}

	/**
	 * @return replyInureDate
	 */
	public String getReplyInureDate() {
		return this.replyInureDate;
	}

	/**
	 * @param lmtType
	 */
	public void setLmtType(String lmtType) {
		this.lmtType = lmtType;
	}

	/**
	 * @return lmtType
	 */
	public String getLmtType() {
		return this.lmtType;
	}

	/**
	 * @param grpCusId
	 */
	public void setGrpCusId(String grpCusId) {
		this.grpCusId = grpCusId;
	}

	/**
	 * @return grpCusId
	 */
	public String getGrpCusId() {
		return this.grpCusId;
	}

	/**
	 * @param grpCusName
	 */
	public void setGrpCusName(String grpCusName) {
		this.grpCusName = grpCusName;
	}

	/**
	 * @return grpCusName
	 */
	public String getGrpCusName() {
		return this.grpCusName;
	}

	/**
	 * @param cusType
	 */
	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	/**
	 * @return cusType
	 */
	public String getCusType() {
		return this.cusType;
	}

	/**
	 * @param apprMode
	 */
	public void setApprMode(String apprMode) {
		this.apprMode = apprMode;
	}

	/**
	 * @return apprMode
	 */
	public String getApprMode() {
		return this.apprMode;
	}

	/**
	 * @param finalApprBrType
	 */
	public void setFinalApprBrType(String finalApprBrType) {
		this.finalApprBrType = finalApprBrType;
	}

	/**
	 * @return finalApprBrType
	 */
	public String getFinalApprBrType() {
		return this.finalApprBrType;
	}

	/**
	 * @param apprResult
	 */
	public void setApprResult(String apprResult) {
		this.apprResult = apprResult;
	}

	/**
	 * @return apprResult
	 */
	public String getApprResult() {
		return this.apprResult;
	}

	/**
	 * @param replyStatus
	 */
	public void setReplyStatus(String replyStatus) {
		this.replyStatus = replyStatus;
	}

	/**
	 * @return replyStatus
	 */
	public String getReplyStatus() {
		return this.replyStatus;
	}

	/**
	 * @param loanApprMode
	 */
	public void setLoanApprMode(String loanApprMode) {
		this.loanApprMode = loanApprMode;
	}

	/**
	 * @return loanApprMode
	 */
	public String getLoanApprMode() {
		return this.loanApprMode;
	}

	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}

	/**
	 * @return curType
	 */
	public String getCurType() {
		return this.curType;
	}

	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}

	/**
	 * @return lmtTerm
	 */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}

	/**
	 * @param pspManaNeed
	 */
	public void setPspManaNeed(String pspManaNeed) {
		this.pspManaNeed = pspManaNeed;
	}

	/**
	 * @return pspManaNeed
	 */
	public String getPspManaNeed() {
		return this.pspManaNeed;
	}

	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * @return oprType
	 */
	public String getOprType() {
		return this.oprType;
	}

	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}

	/**
	 * @return inputId
	 */
	public String getInputId() {
		return this.inputId;
	}

	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}

	/**
	 * @return inputBrId
	 */
	public String getInputBrId() {
		return this.inputBrId;
	}

	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	/**
	 * @return inputDate
	 */
	public String getInputDate() {
		return this.inputDate;
	}

	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}

	/**
	 * @return updId
	 */
	public String getUpdId() {
		return this.updId;
	}

	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}

	/**
	 * @return updBrId
	 */
	public String getUpdBrId() {
		return this.updBrId;
	}

	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}

	/**
	 * @return updDate
	 */
	public String getUpdDate() {
		return this.updDate;
	}

	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	/**
	 * @return managerId
	 */
	public String getManagerId() {
		return this.managerId;
	}

	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}

	/**
	 * @return managerBrId
	 */
	public String getManagerBrId() {
		return this.managerBrId;
	}

	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return createTime
	 */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}

	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return updateTime
	 */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	/**
	 * @param openTotalLmtAmt
	 */
	public void setOpenTotalLmtAmt(java.math.BigDecimal openTotalLmtAmt) {
		this.openTotalLmtAmt = openTotalLmtAmt;
	}

	/**
	 * @return openTotalLmtAmt
	 */
	public java.math.BigDecimal getOpenTotalLmtAmt() {
		return this.openTotalLmtAmt;
	}

	/**
	 * @param lowRiskTotalLmtAmt
	 */
	public void setLowRiskTotalLmtAmt(java.math.BigDecimal lowRiskTotalLmtAmt) {
		this.lowRiskTotalLmtAmt = lowRiskTotalLmtAmt;
	}

	/**
	 * @return lowRiskTotalLmtAmt
	 */
	public java.math.BigDecimal getLowRiskTotalLmtAmt() {
		return this.lowRiskTotalLmtAmt;
	}

}