/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.dto.IqpStockLoanInfoDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpStockLoanInfo;
import cn.com.yusys.yusp.service.IqpStockLoanInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpStockLoanInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 12393
 * @创建时间: 2021-05-08 14:39:40
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "业务申请-存量业务信息")
@RequestMapping("/api/iqpstockloaninfo")
public class IqpStockLoanInfoResource {
    @Autowired
    private IqpStockLoanInfoService iqpStockLoanInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpStockLoanInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpStockLoanInfo> list = iqpStockLoanInfoService.selectAll(queryModel);
        return new ResultDto<List<IqpStockLoanInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpStockLoanInfo>> index(QueryModel queryModel) {
        List<IqpStockLoanInfo> list = iqpStockLoanInfoService.selectByModel(queryModel);
        return new ResultDto<List<IqpStockLoanInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpStockLoanInfo> show(@PathVariable("pkId") String pkId) {
        IqpStockLoanInfo iqpStockLoanInfo = iqpStockLoanInfoService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpStockLoanInfo>(iqpStockLoanInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpStockLoanInfo> create(@RequestBody IqpStockLoanInfo iqpStockLoanInfo) throws URISyntaxException {
        iqpStockLoanInfoService.insert(iqpStockLoanInfo);
        return new ResultDto<IqpStockLoanInfo>(iqpStockLoanInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpStockLoanInfo iqpStockLoanInfo) throws URISyntaxException {
        int result = iqpStockLoanInfoService.update(iqpStockLoanInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpStockLoanInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpStockLoanInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /***
     * @param queryModel
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author hubp
     * @date 2021/5/8 14:52
     * @version 1.0.0
     * @desc  通过业务流水号查询存量业务
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("通过业务流水号查询存量业务")
    @PostMapping("/selectbyiqpserno")
    protected ResultDto<List<IqpStockLoanInfo>> selectByIqpSerno(@RequestBody QueryModel queryModel) {
        return new ResultDto<List<IqpStockLoanInfo>>(iqpStockLoanInfoService.selectByIqpSerno(queryModel));
    }


    /***
     * @param iqpStockLoanInfo
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Integer>
     * @author shenli
     * @date 2021-6-17
     * @version 1.0.0
     * @desc  通过业务流水号查询存量业务
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("通过业务流水号查询存量业务余额合计")
    @PostMapping("/selectLmtAmtTotal")
    protected ResultDto<IqpStockLoanInfoDto> selectLmtAmtTotal(@RequestBody IqpStockLoanInfo iqpStockLoanInfo) {
        return new ResultDto<IqpStockLoanInfoDto>(iqpStockLoanInfoService.selectLmtAmtTotal(iqpStockLoanInfo));
    }
}
