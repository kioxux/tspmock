package cn.com.yusys.yusp.service.server.xddh0005;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.req.Ln3041ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Ln3102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Ln3102RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Lstdkzqg;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108RespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0005.resp.Xddh0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.NewCoreEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.AccLoanService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提前还款
 *
 * @author leehuang
 * @version 1.0
 */
@Service
public class Xddh0005Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0005Service.class);
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3108.Ln3108Service ln3108Service;// 业务逻辑处理类：贷款组合查询
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3100.Ln3100Service ln3100Service;// 业务逻辑处理类：贷款信息查询
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3102.Ln3102Service ln3102Service;// 业务逻辑处理类：贷款期供查询试算
    @Autowired
    private cn.com.yusys.yusp.service.client.bsp.core.ln3041.Ln3041Service ln3041Service;//业务逻辑处理类：贷款归还
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 提前还款</br>
     *
     * @param xddh0005DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddh0005DataRespDto xddh0005(Xddh0005DataReqDto xddh0005DataReqDto) throws BizException, Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, JSON.toJSONString(xddh0005DataReqDto));
        Xddh0005DataRespDto xddh0005DataRespDto = new Xddh0005DataRespDto();
        String billNo = xddh0005DataReqDto.getBillNo();//借据号
        String repayType = xddh0005DataReqDto.getRepayType();//还款模式
        BigDecimal repayAmt = xddh0005DataReqDto.getRepayAmt();//还款金额
        String repayAcctNo = xddh0005DataReqDto.getRepayAcctNo();//还款账号
        String curType = xddh0005DataReqDto.getCurType();//币种
        BigDecimal repayTotlCap = xddh0005DataReqDto.getRepayTotlCap();//还款总本金
        String tranSerno = xddh0005DataReqDto.getTranSerno();//交易流水号
        // 响应Data：提前还款
        String accotTranSerno = StringUtils.EMPTY;//核心交易流水
        BigDecimal repayInt = null;//还款利息
        String nextRepayDate = StringUtils.EMPTY;//下次还款日
        BigDecimal repayCap = null;//还款本金
        try {
            /* 检查还款日期 */
            String currDate = DateUtils.getCurrDateStr();
            Integer day = Integer.valueOf(currDate.substring(8, 10));
            String time = DateUtils.getCurrTime();
            Integer hour = Integer.valueOf(time.substring(0, 2));
            if (hour >= 17 || hour <= 4) {
                // 处理[{}|{}]的Service逻辑,业务异常信息为:[{}]
                logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, EcbEnum.ECB010030.value);//每日17点到凌晨5点,贷款不能支用及归还
                throw BizException.error(null, EcbEnum.ECB010030.key, EcbEnum.ECB010030.value);
            }
            // 营业日期
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            AccLoan accLoan = accLoanService.queryAccLoanDataByBillNo(billNo);
            String prdId = "";
            if (accLoan != null) {
                prdId = accLoan.getPrdId();
                logger.info("**********XDDH0005**查询产品信息开始,查询参数为:{}", JSON.toJSONString(prdId));
                ResultDto<CfgPrdBasicinfoDto> cfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(prdId);
                logger.info("**********XDDH0005**查询产品信息开始,返回参数为:{}", JSON.toJSONString(cfgPrdBasicinfoDto));
                String prdCode = cfgPrdBasicinfoDto.getCode();//返回结果
                String advRepayEndDate = StringUtils.EMPTY;//提前还款截止日期
                if (StringUtil.isNotEmpty(prdCode) && CmisBizConstants.NUM_ZERO.equals(prdCode)) {
                    CfgPrdBasicinfoDto CfgPrdBasicinfoDto = cfgPrdBasicinfoDto.getData();
                    if (CfgPrdBasicinfoDto != null) {
                        advRepayEndDate = CfgPrdBasicinfoDto.getAdvRepayEndDate();
                    }
                }
                if (StringUtil.isNotEmpty(advRepayEndDate)) {//提前还款截止日期不为空
                    Integer today = Integer.valueOf(openDay.substring(8, 10));
                    Integer endday = Integer.valueOf(advRepayEndDate);
                    if (today > endday) {
                        logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, EcbEnum.ECB010029.value);//每月25日至月底,贷款不能支用及归还
                        throw BizException.error(null, EcbEnum.ECB010029.key, EcbEnum.ECB010029.value);
                    }
                }
            }

            /* 调用 ln3100贷款信息查询 */
            // 组装ln3100 贷款信息查询 接口请求报文
            Ln3100ReqDto ln3100ReqDto = ln3100Service.buildLn3100ReqDto(xddh0005DataReqDto);
            Ln3100RespDto ln3100RespDto = ln3100Service.ln3100(ln3100ReqDto);
            String huankzhh = ln3100RespDto.getHuankzhh();// 还款账号
            Optional.ofNullable(huankzhh).orElseThrow(() -> BizException.error(null, EcbEnum.ECB010031.key, EcbEnum.ECB010031.value));//还款账号为空
            if (!Objects.equals(repayAcctNo, huankzhh)) {
                // 处理[{}|{}]的Service逻辑,业务异常信息为:[{}]
                logger.error(TradeLogConstants.SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, EcbEnum.ECB010028.value);//还款账号与放款时预留不一致
                if (!"022097".equals(prdId) && !"022099".equals(prdId) && !"022100".equals(prdId) && !"022101".equals(prdId)) {
                    throw BizException.error(null, EcbEnum.ECB010028.key, EcbEnum.ECB010028.value);//还款账号与放款时预留不一致
                }
            }
            /* 调用  ln3041 贷款归还*/
            // 组装 ln3041 贷款归还 接口请求报文
            Ln3041ReqDto ln3041ReqDto = ln3041Service.buildLn3041ReqDto(xddh0005DataReqDto);
            ln3041ReqDto.setBrchno(accLoan.getFinaBrId());//    部门号,取账务机构号
            Ln3041RespDto ln3041RespDto = ln3041Service.ln3041(ln3041ReqDto);
            String jiaoyils = ln3041RespDto.getJiaoyils();//交易流水
            accotTranSerno = jiaoyils;//核心交易流水
            /* 调用 ln3108 贷款组合查询*/
            // 组装ln3108新核心系统贷款组合查询接口请求报文
            Ln3108ReqDto ln3108ReqDto = ln3108Service.buildLn3108ReqDto(xddh0005DataReqDto);
            Ln3108RespDto ln3108RespDto = ln3108Service.ln3108(ln3108ReqDto);
            boolean loanTermFlag = false;// 是否存在一期逾期数据
            List<cn.com.yusys.yusp.dto.client.esb.core.ln3108.Lstdkzhzb> ln3108Lstdkzhzb = ln3108RespDto.getLstdkzhzb();//贷款账户主表
            if (CollectionUtils.nonEmpty(ln3108Lstdkzhzb)) {
                Optional<cn.com.yusys.yusp.dto.client.esb.core.ln3108.Lstdkzhzb> lstdkzhzbOptional = ln3108Lstdkzhzb.parallelStream().findFirst();
                cn.com.yusys.yusp.dto.client.esb.core.ln3108.Lstdkzhzb lstdkzhzb = lstdkzhzbOptional.get();
                BigDecimal zhchbjin = lstdkzhzb.getZhchbjin();// 正常本金
                BigDecimal yuqibjin = lstdkzhzb.getYuqibjin();// 逾期本金
                BigDecimal dzhibjin = lstdkzhzb.getDzhibjin();// 呆滞本金
                BigDecimal daizbjin = lstdkzhzb.getDaizbjin();// 呆账本金
                String daikxtai = lstdkzhzb.getDaikxtai();// 贷款形态
                BigDecimal ysqianxi = lstdkzhzb.getYsqianxi();//应收欠息
                BigDecimal csqianxi = lstdkzhzb.getCsqianxi();//催收欠息
                BigDecimal ysyjfaxi = lstdkzhzb.getYsyjfaxi();//应收应计罚息
                BigDecimal csyjfaxi = lstdkzhzb.getCsyjfaxi();//催收应计罚息
                BigDecimal yshofaxi = lstdkzhzb.getYshofaxi();//应收罚息
                BigDecimal cshofaxi = lstdkzhzb.getCshofaxi();//催收罚息
                BigDecimal yingjifx = lstdkzhzb.getYingjifx();//应计复息
                BigDecimal fuxiiiii = lstdkzhzb.getFuxiiiii();//复息
                String jychgbzh = lstdkzhzb.getJychgbzh();//上一日借据状态标志  0-正常,1-逾期
                String dkzhhzht = lstdkzhzb.getDkzhhzht();// 贷款账户状态
                //逾期金额
                BigDecimal overdueAmt = yuqibjin // 逾期本金
                        .add(ysqianxi)//应收欠息
                        .add(csqianxi)//催收欠息
                        .add(ysyjfaxi)//应收应计罚息
                        .add(csyjfaxi)//催收应计罚息
                        .add(yshofaxi)//应收罚息
                        .add(cshofaxi)//催收罚息
                        .add(yingjifx)//应计复息
                        .add(fuxiiiii);//复息
                // 判断借据是否逾期
                if (Objects.equals(NewCoreEnum.Ln3108_DAIKXTAI_1.key, daikxtai)) {
                    loanTermFlag = true;
                } else if (Objects.equals(NewCoreEnum.Ln3108_DKZHHZHT_0.key, dkzhhzht)) {// 当贷款账户状态为正常时需要通过逾期金额进一步判断借据状态
                    // 判断逾期金额是否大于0和20日借据状态是否等于1-逾期,如果符合则逾期,否则正常
                    if (overdueAmt.compareTo(BigDecimal.ZERO) > 0) {
                        if (Objects.equals(NewCoreEnum.Ln3108_JYCHGBZH_1.key, jychgbzh)) {
                            loanTermFlag = true;
                        }
                    }
                }
                // 剩余本金
                BigDecimal remainAmt = zhchbjin.add(yuqibjin).add(dzhibjin).add(daizbjin);
                // 如果贷款账号状态不等于0正常,则这边贷款已处于已结清状态,故返回空数据给直销
                if (!Objects.equals(NewCoreEnum.Ln3108_DKZHHZHT_0.key, dkzhhzht)) {
                    xddh0005DataRespDto.setAccotTranSerno(accotTranSerno);//核心交易流水
                    xddh0005DataRespDto.setRepayInt(repayInt);//还款利息
                    xddh0005DataRespDto.setNextRepayDate(nextRepayDate);//下次还款日
                    xddh0005DataRespDto.setRepayCap(repayCap);//还款本金
                    return xddh0005DataRespDto;
                }
            }
            /* 调用 ln3102 贷款期供查询试算 */
            // 组装 ln3102 贷款期供查询试算 接口请求报文
            Ln3102ReqDto ln3102ReqDto = ln3102Service.buildLn3102ReqDto(xddh0005DataReqDto);
            Ln3102RespDto ln3102RespDto = ln3102Service.ln3102(ln3102ReqDto);
            List<Lstdkzqg> lstdkzqgs = ln3102RespDto.getLstdkzqg();//贷款账户期供
            if (CollectionUtils.nonEmpty(lstdkzqgs)) {
                Optional<cn.com.yusys.yusp.dto.client.esb.core.ln3102.Lstdkzqg> lstdkzqgOptional = lstdkzqgs.parallelStream().findFirst();
                cn.com.yusys.yusp.dto.client.esb.core.ln3102.Lstdkzqg lstdkzqg = lstdkzqgOptional.get();
                nextRepayDate = lstdkzqg.getZhzhriqi();// 终止日期
                repayCap = lstdkzqg.getChushibj();//初始本金
                repayInt = lstdkzqg.getChushilx();//初始利息
            }
            // 给xddh0005DataRespDto赋值
            xddh0005DataRespDto.setAccotTranSerno(accotTranSerno);//核心交易流水
            xddh0005DataRespDto.setRepayInt(repayInt);//还款利息
            xddh0005DataRespDto.setNextRepayDate(nextRepayDate);//下次还款日
            xddh0005DataRespDto.setRepayCap(repayCap);//还款本金
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, JSON.toJSONString(xddh0005DataRespDto));
        return xddh0005DataRespDto;
    }
}
