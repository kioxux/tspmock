package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.user.UserIdentity;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.domain.RepayDayRecordInfo;
import cn.com.yusys.yusp.repository.mapper.RepayDayRecordInfoMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RepayDayRecordInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-05-13 20:19:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class RepayDayRecordInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepayDayRecordInfoService.class);

    @Autowired
    private RepayDayRecordInfoMapper repayDayRecordInfoMapper;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public RepayDayRecordInfo selectByPrimaryKey(String cfgSerno) {
        return repayDayRecordInfoMapper.selectByPrimaryKey(cfgSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<RepayDayRecordInfo> selectAll(QueryModel model) {
        List<RepayDayRecordInfo> records = (List<RepayDayRecordInfo>) repayDayRecordInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<RepayDayRecordInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<RepayDayRecordInfo> list = repayDayRecordInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(RepayDayRecordInfo record) {
        return repayDayRecordInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(RepayDayRecordInfo record) {
        return repayDayRecordInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(RepayDayRecordInfo record) {
        return repayDayRecordInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(RepayDayRecordInfo record) {
        return repayDayRecordInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String cfgSerno) {
        return repayDayRecordInfoMapper.deleteByPrimaryKey(cfgSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return repayDayRecordInfoMapper.deleteByIds(ids);
    }

    /**
     * @创建人 WH
     * @创建时间 2021/5/14 13:51
     * @注释 新增接口  只需要传入还款日
     */
    @Transactional(rollbackFor = Exception.class)
    public ResultDto addData(RepayDayRecordInfo repayDayRecordInfo) {
        if (repayDayRecordInfo.getRepayDate() > 28 || repayDayRecordInfo.getRepayDate() < 1) {
            return new ResultDto(null).message("请输入1~28之间的日期");
        }
        //放入配置状态
        repayDayRecordInfo.setCfgStatus("01");
        //放入操作类型
        repayDayRecordInfo.setOprType("01");
        //登记人inputId
        User user = SessionUtils.getUserInformation();
        if (user != null) {
            //登记人
            repayDayRecordInfo.setUpdId(user.getLoginCode());
            //主管客户经理
            repayDayRecordInfo.setManagerId(user.getLoginCode());
            //最后修改人
            repayDayRecordInfo.setInputId(user.getUserName());
            //获取登记机构
            UserIdentity org = user.getOrg();
            //登记机构
            repayDayRecordInfo.setInputBrId(org.getId());
            //主管机构
            repayDayRecordInfo.setManagerBrId(org.getId());
            //最后修改机构
            repayDayRecordInfo.setUpdBrId(org.getId());
            Date date = new Date();
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");
            String s = LocalDate.now().toString();
            //登记日期
            repayDayRecordInfo.setInputDate(s);
            //最后修改日期
            repayDayRecordInfo.setUpdDate(s);
            //创建时间
            repayDayRecordInfo.setCreateTime(date);
            //修改日期
            repayDayRecordInfo.setUpdateTime(date);
            //修改前一条数据
            try {
                RepayDayRecordInfo newData = repayDayRecordInfoMapper.selectNewData();
                if (newData != null) {
                    newData.setCfgStatus("02");
                    repayDayRecordInfoMapper.updateByPrimaryKeySelective(newData);
                }//作废操作成功
                int i = repayDayRecordInfoMapper.insert(repayDayRecordInfo);
                if (i == 1) {
                    return new ResultDto(i).message("修改成功");
                }
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                throw e;
            }
        }
        return new ResultDto(null).message("操作异常！请刷新重试");
    }

    /**
     * @param
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto
     * @author hubp
     * @date 2021/5/29 19:35
     * @version 1.0.0
     * @desc 只为拿到还款日
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public ResultDto selectRepayDay(String iqpSerno) {
        //先通过申请流水号查询证件信息
//        IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(iqpSerno);
//        List<IqpLoanApp> list = iqpLoanAppService.selectByCusId(iqpLoanApp.getCusId());
//        if (list.size() > 0) {
//            for(IqpLoanApp iqpLoanApptemp:list){
//                Integer repayDay = iqpLoanApptemp.getRe();
//                if(null != ){
//
//                }
//            }
//            repayDay = list.get(0).getRepayDay();
//        }else{
        RepayDayRecordInfo repayDayRecordInfo = repayDayRecordInfoMapper.selectNewData();
        return new ResultDto(repayDayRecordInfo.getRepayDate());
        // }
    }
}
