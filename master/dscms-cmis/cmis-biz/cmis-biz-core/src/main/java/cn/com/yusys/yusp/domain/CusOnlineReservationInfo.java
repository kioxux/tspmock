/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CusOnlineReservationInfo
 * @类描述: cus_online_reservation_info数据实体类
 * @功能描述: 
 * @创建人: Acer
 * @创建时间: 2021-06-12 09:14:26
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "cus_online_reservation_info")
public class CusOnlineReservationInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 任务编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "TASK_NO")
	private String taskNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 10)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 60)
	private String cusName;
	
	/** 证件号码 **/
	@Column(name = "CERT_CODE", unique = false, nullable = true, length = 60)
	private String certCode;
	
	/** 主联系人姓名 **/
	@Column(name = "LINKMAN_NAME", unique = false, nullable = true, length = 80)
	private String linkmanName;
	
	/** 主联系人电话 **/
	@Column(name = "LINKMAN_PHONE", unique = false, nullable = true, length = 20)
	private String linkmanPhone;
	
	/** 登记时间 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 30)
	private String inputDate;
	
	/** 管户客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 所属机构 **/
	@Column(name = "BELG_ORG", unique = false, nullable = true, length = 20)
	private String belgOrg;
	
	/** 预约类型（01授信预约  02合同预约） **/
	@Column(name = "RESERVATION_TYPE", unique = false, nullable = true, length = 2)
	private String reservationType;
	
	/** 处理结果（01已完成，02已关闭） **/
	@Column(name = "PRC_RST", unique = false, nullable = true, length = 2)
	private String prcRst;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}
	
    /**
     * @return certCode
     */
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param linkmanName
	 */
	public void setLinkmanName(String linkmanName) {
		this.linkmanName = linkmanName;
	}
	
    /**
     * @return linkmanName
     */
	public String getLinkmanName() {
		return this.linkmanName;
	}
	
	/**
	 * @param linkmanPhone
	 */
	public void setLinkmanPhone(String linkmanPhone) {
		this.linkmanPhone = linkmanPhone;
	}
	
    /**
     * @return linkmanPhone
     */
	public String getLinkmanPhone() {
		return this.linkmanPhone;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param belgOrg
	 */
	public void setBelgOrg(String belgOrg) {
		this.belgOrg = belgOrg;
	}
	
    /**
     * @return belgOrg
     */
	public String getBelgOrg() {
		return this.belgOrg;
	}
	
	/**
	 * @param reservationType
	 */
	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}
	
    /**
     * @return reservationType
     */
	public String getReservationType() {
		return this.reservationType;
	}
	
	/**
	 * @param prcRst
	 */
	public void setPrcRst(String prcRst) {
		this.prcRst = prcRst;
	}
	
    /**
     * @return prcRst
     */
	public String getPrcRst() {
		return this.prcRst;
	}


}