package cn.com.yusys.yusp.web.server.xdtz0032;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0032.req.Xdtz0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0032.resp.Xdtz0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0032.Xdtz0032Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:询客户所担保的行内当前贷款逾期件数
 *
 * @author zhangpeng
 * @author xull
 * @version 1.0
 */
@Api(tags = "xdtz0032:询客户所担保的行内当前贷款逾期件数")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0032Resource.class);

    @Autowired
    private Xdtz0032Service xdtz0032Service;

    /**
     * 交易码：xdtz0032
     * 交易描述：询客户所担保的行内当前贷款逾期件数
     *
     * @param xdtz0032DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0032:询客户所担保的行内当前贷款逾期件数")
    @PostMapping("/xdtz0032")
    protected @ResponseBody
    ResultDto<Xdtz0032DataRespDto> xdtz0032(@Validated @RequestBody Xdtz0032DataReqDto xdtz0032DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032DataReqDto));
        Xdtz0032DataRespDto xdtz0032DataRespDto = new Xdtz0032DataRespDto();// 响应Dto:询客户所担保的行内当前贷款逾期件数
        ResultDto<Xdtz0032DataRespDto> xdtz0032DataResultDto = new ResultDto<>();
        // 从xdtz0032DataReqDto获取业务值进行业务逻辑处理
        try {
            String cusId = xdtz0032DataReqDto.getCusId();//客户编号
            if (StringUtil.isNotEmpty(cusId)) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032DataReqDto));
                xdtz0032DataRespDto = xdtz0032Service.queryGuaranteeOverDueFromCmis(xdtz0032DataReqDto);
                logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032DataRespDto));
                // 封装xdtz0032DataResultDto中正确的返回码和返回信息
                xdtz0032DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdtz0032DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
                logger.info("**************************调用XXXXXService层结束**END***********************");
            } else {
                logger.info("**************************请求字段为空*************************");
                xdtz0032DataResultDto.setCode(EcbEnum.ECB010001.key);
                xdtz0032DataResultDto.setMessage(EcbEnum.ECB010001.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, e.getMessage());
            // 封装xdtz0032DataResultDto中异常返回码和返回信息
            xdtz0032DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0032DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0032DataRespDto到xdtz0032DataResultDto中
        xdtz0032DataResultDto.setData(xdtz0032DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032DataResultDto));
        return xdtz0032DataResultDto;
    }
}
