package cn.com.yusys.yusp.service.server.xdzc0023;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0023.req.Xdzc0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0023.resp.Xdzc0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AsplWhtlsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * 接口处理类:资产池白名单
 *
 * @author chenyong
 * @version 1.0
 * @since 2021/06/10
 */
@Service
public class Xdzc0023Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdzc0023Service.class);

    @Resource
    private AsplWhtlsMapper asplWhtlsMapper;

    /**
     * 资产池白名单查询
     *
     * @param xdzc0023DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdzc0023DataRespDto xdzc0023(Xdzc0023DataReqDto xdzc0023DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value);
        Xdzc0023DataRespDto xdzc0023DataRespDto = new Xdzc0023DataRespDto();
        String cusId = xdzc0023DataReqDto.getCusId();
        try {
            if (StringUtil.isNotEmpty(cusId)) {
                int res = asplWhtlsMapper.selectByCusId(cusId);
                if (res > 0) {
                    xdzc0023DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);
                } else {
                    xdzc0023DataRespDto.setOpFlag(CmisBizConstants.FAIL);
                }
            } else {
                logger.info("********cusId为空******************");
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value() + "CusId为空");
            }
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.error(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value);
        return xdzc0023DataRespDto;
    }
}