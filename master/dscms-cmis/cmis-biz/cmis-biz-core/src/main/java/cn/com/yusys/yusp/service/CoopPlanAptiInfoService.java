/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;

import cn.com.yusys.yusp.dto.CoopPlanAptiInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CoopPlanAptiInfo;
import cn.com.yusys.yusp.repository.mapper.CoopPlanAptiInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAptiInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-13 21:32:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPlanAptiInfoService {

    @Autowired
    private CoopPlanAptiInfoMapper coopPlanAptiInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CoopPlanAptiInfo selectByPrimaryKey(String pkId) {
        return coopPlanAptiInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CoopPlanAptiInfoDto> selectAll(QueryModel model) {
        List<CoopPlanAptiInfoDto> records = (List<CoopPlanAptiInfoDto>) coopPlanAptiInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CoopPlanAptiInfoDto> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPlanAptiInfoDto> list = coopPlanAptiInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CoopPlanAptiInfo record) {
        return coopPlanAptiInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CoopPlanAptiInfo record) {
        return coopPlanAptiInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CoopPlanAptiInfo record) {
        return coopPlanAptiInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CoopPlanAptiInfo record) {
        return coopPlanAptiInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return coopPlanAptiInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopPlanAptiInfoMapper.deleteByIds(ids);
    }



    /**
     * @方法名称: selectByPartnerNo
     * @方法描述: 根据合作方编号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CoopPlanAptiInfo selectByPartnerNo(String partnerNo) {
        return coopPlanAptiInfoMapper.selectByPartnerNo(partnerNo);
    }

}
