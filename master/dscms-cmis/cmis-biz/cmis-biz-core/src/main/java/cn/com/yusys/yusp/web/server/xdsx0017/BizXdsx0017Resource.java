package cn.com.yusys.yusp.web.server.xdsx0017;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;

import cn.com.yusys.yusp.service.server.xdsx0017.Xdsx0017Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdsx0017.req.Xdsx0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0017.resp.Xdsx0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 接口处理类:信贷提供风控查询授信信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0017:信贷提供风控查询授信信息")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0017Resource.class);

    @Autowired
    private Xdsx0017Service xdsx0017Service;

    /**
     * 交易码：xdsx0017
     * 交易描述：信贷提供风控查询授信信息
     *
     * @param xdsx0017DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷提供风控查询授信信息")
    @PostMapping("/xdsx0017")
    protected @ResponseBody
    ResultDto<Xdsx0017DataRespDto> xdsx0017(@Validated @RequestBody Xdsx0017DataReqDto xdsx0017DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, JSON.toJSONString(xdsx0017DataReqDto));
        Xdsx0017DataRespDto xdsx0017DataRespDto = new Xdsx0017DataRespDto();// 响应Dto:信贷提供风控查询授信信息
        ResultDto<Xdsx0017DataRespDto> xdsx0017DataResultDto = new ResultDto<>();
        String cer_type = xdsx0017DataReqDto.getCer_type();//开户证件类型
        String local_no = xdsx0017DataReqDto.getLocal_no();//统一社会信用代码
        try {
            // 从xdsx0017DataReqDto获取业务值进行业务逻辑处理
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, JSON.toJSONString(xdsx0017DataReqDto));
            xdsx0017DataRespDto = xdsx0017Service.xdsx0017(xdsx0017DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, JSON.toJSONString(xdsx0017DataRespDto));
            // 封装xdsx0017DataResultDto中正确的返回码和返回信息
            xdsx0017DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0017DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, e.getMessage());
            // 封装xdsx0017DataResultDto中异常返回码和返回信息
            xdsx0017DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0017DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0017DataRespDto到xdsx0017DataResultDto中
        xdsx0017DataResultDto.setData(xdsx0017DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, JSON.toJSONString(xdsx0017DataResultDto));
        return xdsx0017DataResultDto;
    }
}
