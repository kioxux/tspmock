package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptAppInfoEffWar
 * @类描述: rpt_app_info_eff_war数据实体类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 09:41:47
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class RptAppInfoEffWarDto implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     **/
    private String pkId;

    /**
     * 申请流水号
     **/
    private String serno;

    /**
     * 集团成员名称
     **/
    private String cusName;

    /**
     * 预警时间
     **/
    private String altTime;

    /**
     * 预警种类
     **/
    private String altType;

    /**
     * 预警子项
     **/
    private String altSubType;

    /**
     * 预警输出描述
     **/
    private String altOutDesc;

    /**
     * 指标风险等级
     **/
    private String altRiskLvl;

    /**
     * 预警信号对企业影响
     **/
    private String altRiskEffectCus;


    /**
     * @param pkId
     */
    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    /**
     * @return PkId
     */
    public String getPkId() {
        return this.pkId;
    }

    /**
     * @param serno
     */
    public void setSerno(String serno) {
        this.serno = serno == null ? null : serno.trim();
    }

    /**
     * @return Serno
     */
    public String getSerno() {
        return this.serno;
    }

    /**
     * @param altTime
     */
    public void setAltTime(String altTime) {
        this.altTime = altTime == null ? null : altTime.trim();
    }

    /**
     * @return AltTime
     */
    public String getAltTime() {
        return this.altTime;
    }

    /**
     * @param altType
     */
    public void setAltType(String altType) {
        this.altType = altType == null ? null : altType.trim();
    }

    /**
     * @return AltType
     */
    public String getAltType() {
        return this.altType;
    }

    /**
     * @param altSubType
     */
    public void setAltSubType(String altSubType) {
        this.altSubType = altSubType == null ? null : altSubType.trim();
    }

    /**
     * @return AltSubType
     */
    public String getAltSubType() {
        return this.altSubType;
    }

    /**
     * @param altOutDesc
     */
    public void setAltOutDesc(String altOutDesc) {
        this.altOutDesc = altOutDesc == null ? null : altOutDesc.trim();
    }

    /**
     * @return AltOutDesc
     */
    public String getAltOutDesc() {
        return this.altOutDesc;
    }

    /**
     * @param altRiskLvl
     */
    public void setAltRiskLvl(String altRiskLvl) {
        this.altRiskLvl = altRiskLvl == null ? null : altRiskLvl.trim();
    }

    /**
     * @return AltRiskLvl
     */
    public String getAltRiskLvl() {
        return this.altRiskLvl;
    }

    /**
     * @param altRiskEffectCus
     */
    public void setAltRiskEffectCus(String altRiskEffectCus) {
        this.altRiskEffectCus = altRiskEffectCus == null ? null : altRiskEffectCus.trim();
    }

    /**
     * @return AltRiskEffectCus
     */
    public String getAltRiskEffectCus() {
        return this.altRiskEffectCus;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }
}