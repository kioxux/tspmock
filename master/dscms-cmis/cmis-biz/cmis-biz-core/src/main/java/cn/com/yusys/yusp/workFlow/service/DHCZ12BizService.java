package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 授信抵质押物价值认定申请流程业务处理类 - 东海村镇
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class DHCZ12BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DHCZ12BizService.class);

    @Autowired
    private SGCZ12BizService sGCZ12BizService;

    @Override
    public void bizOp(ResultInstanceDto instanceInfo) {
        sGCZ12BizService.bizOp(instanceInfo);
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.DHCZ12.equals(flowCode);
    }
}
