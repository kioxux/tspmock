/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.domain.IqpLoanSuspension;
import cn.com.yusys.yusp.service.IqpLoanSuspensionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpLoanSuspensionResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-07-29 10:33:09
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqploansuspension")
public class IqpLoanSuspensionResource {
    @Autowired
    private IqpLoanSuspensionService iqpLoanSuspensionService;

    /**
     * 全表查询.
     *
     * @return
     */
    @PostMapping("/query/all")
    protected ResultDto<List<IqpLoanSuspension>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpLoanSuspension> list = iqpLoanSuspensionService.selectAll(queryModel);
        return new ResultDto<List<IqpLoanSuspension>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/query")
    protected ResultDto<List<IqpLoanSuspension>> index(@RequestBody QueryModel queryModel) {
        List<IqpLoanSuspension> list = iqpLoanSuspensionService.selectByModel(queryModel);
        return new ResultDto<List<IqpLoanSuspension>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/querySingle")
    protected ResultDto<IqpLoanSuspension> querySingle(@RequestBody String ilsSerno) {
        IqpLoanSuspension iqpLoanSuspension = iqpLoanSuspensionService.selectByPrimaryKey(ilsSerno);
        return new ResultDto<IqpLoanSuspension>(iqpLoanSuspension);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/create")
    protected ResultDto<IqpLoanSuspension> create(@RequestBody IqpLoanSuspension iqpLoanSuspension) throws URISyntaxException {
        iqpLoanSuspensionService.insert(iqpLoanSuspension);
        return new ResultDto<IqpLoanSuspension>(iqpLoanSuspension);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpLoanSuspension iqpLoanSuspension) throws URISyntaxException {
        int result = iqpLoanSuspensionService.update(iqpLoanSuspension);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(@RequestBody String ilsSerno) {
        int result = iqpLoanSuspensionService.deleteByPrimaryKey(ilsSerno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpLoanSuspensionService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
