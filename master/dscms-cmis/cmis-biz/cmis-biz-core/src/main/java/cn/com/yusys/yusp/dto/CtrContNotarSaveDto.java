package cn.com.yusys.yusp.dto;

import java.io.Serializable;

public class CtrContNotarSaveDto implements Serializable {
	/*合同公证信息*/
	private CtrContNotarDto ctrContNotarDto;
	/*合同保险信息*/
	private CtrContInsurDto ctrContInsurDto;

	public CtrContNotarDto getCtrContNotarDto() {
		return ctrContNotarDto;
	}

	public void setCtrContNotarDto(CtrContNotarDto ctrContNotarDto) {
		this.ctrContNotarDto = ctrContNotarDto;
	}

	public CtrContInsurDto getCtrContInsurDto() {
		return ctrContInsurDto;
	}

	public void setCtrContInsurDto(CtrContInsurDto ctrContInsurDto) {
		this.ctrContInsurDto = ctrContInsurDto;
	}
}
