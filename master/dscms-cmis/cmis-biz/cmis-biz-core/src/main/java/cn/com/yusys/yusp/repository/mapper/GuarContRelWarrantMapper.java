/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.GuarContRelWarrant;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarContRelWarrantMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-13 15:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GuarContRelWarrantMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    GuarContRelWarrant selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<GuarContRelWarrant> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(GuarContRelWarrant record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(GuarContRelWarrant record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(GuarContRelWarrant record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(GuarContRelWarrant record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * 根据流水号删除
     * @param serno
     * @return
     */
    int deleteBySerno(@Param("serno") String serno);

    /**
     * 根据流水号更新权证编号
     * @param queryModel
     * @return
     */
    int updateWarrantNoByQueryModel(QueryModel queryModel);

    List<GuarContRelWarrant> selectBySerno(@Param("serno") String serno);

    /**
     * 根据核心担保编号查询
     * @param coreGuarantyNo
     * @return
     */
    List<GuarContRelWarrant> selectByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据核心担保编号查询对应的抵押物的借款人客户项下存在有效的“诚易融”贷款合同的押品编号
     * @param coreGuarantyNo
     * @return
     */
    String selectGuarNoByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据流水号逻辑删除记录
     * @param serno
     * @return
     */
    int deleteOnLogicBySerno(@Param("serno") String serno);

    List<GuarContRelWarrant> selectGuarNoByGuarNoAndGuarContNo(@Param("serno") String serno);

    /**
     * 根据合同号查询权证入库核心担保编号列表
     * @param contNo
     * @return
     */
    String selectCoreGuarantyNoByContNo(@Param("contNo") String contNo);

    /**
     * 根据核心担保编号查询押品编号
     * @param coreGuarantyNo
     * @return
     */
    String selectGuarNosByCoreGuarantyNo(@Param("coreGuarantyNo") String coreGuarantyNo);

    /**
     * 根据担保合同编号查询已入库的核心担保编号
     * @param guarContNo
     * @return
     */
    String selectCoreGuarantyNosByGuarContNo(@Param("guarContNo") String guarContNo);

    /**
     * 根据押品编号查询最新的核心担保编号
     * @param guarNo
     * @return
     */
    String selectCoreGuarantyNoByGuarNo(@Param("guarNo") String guarNo);

    /**
     * 根据担保合同编号和押品编号查询核心押品编号
     * @param model
     * @return
     */
    List<String> getCoreGuarNosByGuarContNoAndGuarNo(QueryModel model);

}