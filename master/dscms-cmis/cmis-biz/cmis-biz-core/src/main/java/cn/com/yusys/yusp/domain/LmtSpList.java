/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSpList
 * @类描述: lmt_sp_list数据实体类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-02-03 15:27:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sp_list")
public class LmtSpList extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "pk_id")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "serno", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 借据编号 **/
	@Column(name = "bill_no", unique = false, nullable = false, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "cont_no", unique = false, nullable = true, length = 40)
	private String contNo;
	
	/** 产品编号 **/
	@Column(name = "prd_id", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 借据金额 **/
	@Column(name = "bill_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billAmt;
	
	/** 借据余额 **/
	@Column(name = "bill_bal", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billBal;
	
	/** 欠息累计 **/
	@Column(name = "owe_int", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal oweInt;
	
	/** 代偿本金 **/
	@Column(name = "subpay_cap", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal subpayCap;
	
	/** 代偿利息 **/
	@Column(name = "subpay_int", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal subpayInt;
	
	/** 操作类型 **/
	@Column(name = "opr_type", unique = false, nullable = true, length = 200)
	private String oprType;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param billAmt
	 */
	public void setBillAmt(java.math.BigDecimal billAmt) {
		this.billAmt = billAmt;
	}
	
    /**
     * @return billAmt
     */
	public java.math.BigDecimal getBillAmt() {
		return this.billAmt;
	}
	
	/**
	 * @param billBal
	 */
	public void setBillBal(java.math.BigDecimal billBal) {
		this.billBal = billBal;
	}
	
    /**
     * @return billBal
     */
	public java.math.BigDecimal getBillBal() {
		return this.billBal;
	}
	
	/**
	 * @param oweInt
	 */
	public void setOweInt(java.math.BigDecimal oweInt) {
		this.oweInt = oweInt;
	}
	
    /**
     * @return oweInt
     */
	public java.math.BigDecimal getOweInt() {
		return this.oweInt;
	}
	
	/**
	 * @param subpayCap
	 */
	public void setSubpayCap(java.math.BigDecimal subpayCap) {
		this.subpayCap = subpayCap;
	}
	
    /**
     * @return subpayCap
     */
	public java.math.BigDecimal getSubpayCap() {
		return this.subpayCap;
	}
	
	/**
	 * @param subpayInt
	 */
	public void setSubpayInt(java.math.BigDecimal subpayInt) {
		this.subpayInt = subpayInt;
	}
	
    /**
     * @return subpayInt
     */
	public java.math.BigDecimal getSubpayInt() {
		return this.subpayInt;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}