/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz模块
 * @类名称: AccWriteoff
 * @类描述: acc_writeoff数据实体类
 * @功能描述: 
 * @创建人: mashun
 * @创建时间: 2021-01-19 19:41:22
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_writeoff")
public class AccWriteoff extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 台账编号 **/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ACC_NO")
	private String accNo;
	
	/** 业务流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = false, length = 40)
	private String iqpSerno;
	
	/** 借据编号 **/
	@Column(name = "BILL_NO", unique = false, nullable = false, length = 40)
	private String billNo;
	
	/** 合同编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = false, length = 40)
	private String contNo;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 40)
	private String prdId;
	
	/** 币种 STD_ZB_CUR_TYP **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 核销本金 **/
	@Column(name = "WRITEOFF_CAP", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal writeoffCap;
	
	/** 核销利息 **/
	@Column(name = "WRITEOFF_INT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal writeoffInt;
	
	/** 核销日期 **/
	@Column(name = "WRITEOFF_DATE", unique = false, nullable = true, length = 10)
	private String writeoffDate;
	
	/** 核销机构 **/
	@Column(name = "WRITEOFF_BR_ID", unique = false, nullable = true, length = 20)
	private String writeoffBrId;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 台账状态 STD_ZB_ACC_TYP **/
	@Column(name = "ACC_STATUS", unique = false, nullable = true, length = 5)
	private String accStatus;
	
	
	/**
	 * @param accNo
	 */
	public void setAccNo(String accNo) {
		this.accNo = accNo;
	}
	
    /**
     * @return accNo
     */
	public String getAccNo() {
		return this.accNo;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param billNo
	 */
	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
    /**
     * @return billNo
     */
	public String getBillNo() {
		return this.billNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param writeoffCap
	 */
	public void setWriteoffCap(java.math.BigDecimal writeoffCap) {
		this.writeoffCap = writeoffCap;
	}
	
    /**
     * @return writeoffCap
     */
	public java.math.BigDecimal getWriteoffCap() {
		return this.writeoffCap;
	}
	
	/**
	 * @param writeoffInt
	 */
	public void setWriteoffInt(java.math.BigDecimal writeoffInt) {
		this.writeoffInt = writeoffInt;
	}
	
    /**
     * @return writeoffInt
     */
	public java.math.BigDecimal getWriteoffInt() {
		return this.writeoffInt;
	}
	
	/**
	 * @param writeoffDate
	 */
	public void setWriteoffDate(String writeoffDate) {
		this.writeoffDate = writeoffDate;
	}
	
    /**
     * @return writeoffDate
     */
	public String getWriteoffDate() {
		return this.writeoffDate;
	}
	
	/**
	 * @param writeoffBrId
	 */
	public void setWriteoffBrId(String writeoffBrId) {
		this.writeoffBrId = writeoffBrId;
	}
	
    /**
     * @return writeoffBrId
     */
	public String getWriteoffBrId() {
		return this.writeoffBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param accStatus
	 */
	public void setAccStatus(String accStatus) {
		this.accStatus = accStatus;
	}
	
    /**
     * @return accStatus
     */
	public String getAccStatus() {
		return this.accStatus;
	}


}