/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.FileExportPostProcessor;
import cn.com.yusys.yusp.commons.excelcsv.async.ExportContext;
import cn.com.yusys.yusp.commons.excelcsv.model.DataAcquisition;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.DocCreditArchiveInfo;
import cn.com.yusys.yusp.dto.DocCreditArchiveClientDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.DocCreditArchiveInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.vo.DocCreditArchiveInfoListVo;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: DocCreditArchiveInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-21 15:17:19
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class DocCreditArchiveInfoService {

    @Autowired
    private DocCreditArchiveInfoMapper docCreditArchiveInfoMapper;
    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;
    @Autowired
    private CommonService commonService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public DocCreditArchiveInfo selectByPrimaryKey(String dcaiSerno) {
        return docCreditArchiveInfoMapper.selectByPrimaryKey(dcaiSerno);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly = true)
    public List<DocCreditArchiveInfo> selectAll(QueryModel model) {
        List<DocCreditArchiveInfo> records = (List<DocCreditArchiveInfo>) docCreditArchiveInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<DocCreditArchiveInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocCreditArchiveInfo> list = docCreditArchiveInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(DocCreditArchiveInfo record) {
        return docCreditArchiveInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(DocCreditArchiveInfo record) {
        return docCreditArchiveInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(DocCreditArchiveInfo record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docCreditArchiveInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(DocCreditArchiveInfo record) {
        User userInfo = SessionUtils.getUserInformation();
        if (userInfo != null) {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            record.setUpdId(userInfo.getLoginCode());// 更新人
            record.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
            record.setUpdDate(openDay);// 更新日期
        }
        return docCreditArchiveInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String dcaiSerno) {
        return docCreditArchiveInfoMapper.deleteByPrimaryKey(dcaiSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return docCreditArchiveInfoMapper.deleteByIds(ids);
    }

    /**
     * @函数名称:updateByReceive
     * @函数描述:点击接受更新人行征信档案归档数据
     * @参数与返回说明:
     * @算法描述:
     */
    public Map updateByReceive(List<DocCreditArchiveInfo> docCreditArchiveInfoList) {
        int count = 0;
        String rtnCode = EcbEnum.COMMON_SUCCESS_DEF.key;
        String rtnMsg = "接受成功！";
        Map result = new HashMap();
        if (docCreditArchiveInfoList != null && docCreditArchiveInfoList.size() > 0) {
            for (DocCreditArchiveInfo creditArchiveInfos : docCreditArchiveInfoList) {
                String dcaiSerno = creditArchiveInfos.getDcaiSerno();//档案流水号
                String docStatus = creditArchiveInfos.getDocStauts();//征信档案状态
                if (!"01".equals(docStatus)) {
                    throw BizException.error(null, EcbEnum.COMMON_EXCEPTION_DEF.key, "只有当征信档案状态为待接受时才可以接受！");
                }
                if (dcaiSerno != null && !"".equals(dcaiSerno)) {
                    User userInfo = SessionUtils.getUserInformation();
                    if (userInfo != null) {
                        String openDay = stringRedisTemplate.opsForValue().get("openDay");
                        creditArchiveInfos.setReceiverId(userInfo.getLoginCode());//接受人
                        creditArchiveInfos.setReceiverDate(openDay);//接受日期
                        creditArchiveInfos.setDocStauts("02");//征信档案状态 02-接受待核对
                        creditArchiveInfos.setUpdId(userInfo.getLoginCode());// 更新人
                        creditArchiveInfos.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                        creditArchiveInfos.setUpdDate(openDay);// 更新日期
                        count = docCreditArchiveInfoMapper.updateByPrimaryKeySelective(creditArchiveInfos);
                        if (count > 0) {
                            result.put("rtnCode", rtnCode);
                            result.put("rtnMsg", rtnMsg);

                        } else {
                            result.put("rtnCode", EcbEnum.COMMON_EXCEPTION_DEF.key);
                            result.put("rtnMsg", EcbEnum.COMMON_EXCEPTION_DEF.value);
                        }
                    }
                }
            }
        } else {
            result.put("rtnCode", EcbEnum.COMMON_EXCEPTION_DEF.key);
            result.put("rtnMsg", "数据为空，请至少选择一条数据！");
        }
        return result;
    }

    /**
     * @函数名称:updateByReceive
     * @函数描述:点击核对更新人行征信档案归档数据
     * @参数与返回说明:
     * @算法描述:
     */
    public Map updateByCheck(List<DocCreditArchiveInfo> docCreditArchiveInfoList) {
        int count = 0;
        String rtnCode = EcbEnum.COMMON_SUCCESS_DEF.key;
        String rtnMsg = "核对成功！";
        Map result = new HashMap();
        if (docCreditArchiveInfoList != null && docCreditArchiveInfoList.size() > 0) {
            for (DocCreditArchiveInfo creditArchiveInfos : docCreditArchiveInfoList) {
                String dcaiSerno = creditArchiveInfos.getDcaiSerno();//档案流水号
                String docStatus = creditArchiveInfos.getDocStauts();//征信档案状态
                if (!"02".equals(docStatus)) {
                    throw BizException.error(null, EcbEnum.COMMON_EXCEPTION_DEF.key, "只有当征信档案状态为接受待核对时才可以核对！");
                }
                if (dcaiSerno != null && !"".equals(dcaiSerno)) {
                    User userInfo = SessionUtils.getUserInformation();
                    if (userInfo != null) {
                        String openDay = stringRedisTemplate.opsForValue().get("openDay");
                        creditArchiveInfos.setCheckId(userInfo.getLoginCode());
                        creditArchiveInfos.setCheckDate(openDay);//核对日期
                        creditArchiveInfos.setDocStauts("03");//征信档案状态 03-已核对
                        creditArchiveInfos.setUpdId(userInfo.getLoginCode());// 更新人
                        creditArchiveInfos.setUpdBrId(userInfo.getOrg().getCode());// 更新机构
                        creditArchiveInfos.setUpdDate(openDay);// 更新日期
                        count = docCreditArchiveInfoMapper.updateByPrimaryKeySelective(creditArchiveInfos);
                        if (count > 0) {
                            result.put("rtnCode", rtnCode);
                            result.put("rtnMsg", rtnMsg);

                        } else {
                            result.put("rtnCode", EcbEnum.COMMON_EXCEPTION_DEF.key);
                            result.put("rtnMsg", EcbEnum.COMMON_EXCEPTION_DEF.value);
                        }
                    }
                }
            }
        } else {
            result.put("rtnCode", EcbEnum.COMMON_EXCEPTION_DEF.key);
            result.put("rtnMsg", "数据为空，请至少选择一条数据！");
        }
        return result;
    }

    /**
     * 系统生成人行征信档案归档任务
     *
     * @author jijian_yx
     * @date 2021/7/1 10:02
     **/
    public int createDocCreateArchiveBySys(DocCreditArchiveClientDto docCreditArchiveClientDto) {
        if (StringUtils.isEmpty(docCreditArchiveClientDto.getBizSerno())) {
            // 未传入关联流水号
            return 0;
        }
        // 判断该笔业务流水号是否已生成等待入库的归档任务
        Map<String, String> map = new HashMap<>();
        map.put("bizSerno", docCreditArchiveClientDto.getBizSerno());// 业务流水号
        map.put("docStauts", "01");// 征信档案状态 STD_CREDIT_DOC_STAUTS 01:待接收,02:接收待核对,03:已核对
        int count = docCreditArchiveInfoMapper.selectByBizSerno(map);
        if (count > 0) {
            // 已生成,防止重复生成
            return 1;
        } else {
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 生成人行征信档案归档信息
            DocCreditArchiveInfo docCreditArchiveInfo = new DocCreditArchiveInfo();
            BeanUtils.copyProperties(docCreditArchiveClientDto, docCreditArchiveInfo);
            docCreditArchiveInfo.setDcaiSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.DA_ZX_SEQ, new HashMap()));// 流水号
            docCreditArchiveInfo.setCreateDate(openDay);
            docCreditArchiveInfo.setDocStauts("01");// 征信档案状态 STD_CREDIT_DOC_STAUTS 01:待接收,02:接收待核对,03:已核对
            docCreditArchiveInfo.setInputDate(openDay);
            docCreditArchiveInfo.setCreateTime(DateUtils.parseDateByDef(openDay));
            return docCreditArchiveInfoMapper.insertSelective(docCreditArchiveInfo);
        }
    }

    /**
     * 导出
     *
     * @author jijian_yx
     * @date 2021/9/15 0:26
     **/
    public ProgressDto export(QueryModel queryModel) {
        DataAcquisition dataAcquisition = (page, size, object) -> {
            QueryModel queryModeTemp = (QueryModel) object;
            queryModeTemp.setPage(page);
            queryModeTemp.setSize(size);
            queryModeTemp.setSort("inputDate desc");
            String apiUrl = "/api/doccreditarchiveinfo/export";
            String dataAuth = commonService.setDataAuthority(apiUrl);
            if (StringUtils.nonBlank(dataAuth)) {
                queryModeTemp.setDataAuth(dataAuth);
            }
            return exportData(queryModeTemp);
        };
        ExportContext exportContext = ExportContext.of(DocCreditArchiveInfoListVo.class).exportPostProcessor(new FileExportPostProcessor()).data(dataAcquisition, queryModel);
        return ExcelUtils.asyncExport(exportContext);
    }

    private List<DocCreditArchiveInfoListVo> exportData(QueryModel model) {
        List<DocCreditArchiveInfoListVo> docCreditArchiveInfoListVoList = new ArrayList<>();
        PageHelper.startPage(model.getPage(), model.getSize());
        List<DocCreditArchiveInfo> list = docCreditArchiveInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        for (DocCreditArchiveInfo docCreditArchiveInfo : list) {
            DocCreditArchiveInfoListVo docCreditArchiveInfoListVo = new DocCreditArchiveInfoListVo();
            String qryUserName = OcaTranslatorUtils.getUserName(docCreditArchiveInfo.getQryUser());// 查询人
            String qryOrgName = OcaTranslatorUtils.getOrgName(docCreditArchiveInfo.getQryOrg());// 查询机构
            String receiverIdName = OcaTranslatorUtils.getUserName(docCreditArchiveInfo.getReceiverId());// 接收人
            String checkIdName = OcaTranslatorUtils.getUserName(docCreditArchiveInfo.getCheckId());// 核对人
            org.springframework.beans.BeanUtils.copyProperties(docCreditArchiveInfo, docCreditArchiveInfoListVo);
            docCreditArchiveInfoListVo.setQryUserName(qryUserName);
            docCreditArchiveInfoListVo.setQryOrgName(qryOrgName);
            docCreditArchiveInfoListVo.setReceiverIdName(receiverIdName);
            docCreditArchiveInfoListVo.setCheckIdName(checkIdName);
            docCreditArchiveInfoListVoList.add(docCreditArchiveInfoListVo);
        }
        return docCreditArchiveInfoListVoList;
    }
}
