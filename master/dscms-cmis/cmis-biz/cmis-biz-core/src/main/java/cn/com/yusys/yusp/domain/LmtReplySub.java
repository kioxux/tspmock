/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @项目名称: cmis-biz-dto模块
 * @类名称: LmtReplySub
 * @类描述: lmt_reply_sub数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-11 20:46:41
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_reply_sub")
public class LmtReplySub extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 批复流水号 **/
	@Column(name = "REPLY_SERNO", unique = false, nullable = true, length = 40)
	private String replySerno;
	
	/** 批复分项流水号 **/
	@Column(name = "REPLY_SUB_SERNO", unique = false, nullable = true, length = 40)
	private String replySubSerno;
	
	/** 分项流水号 **/
	@Column(name = "SUB_SERNO", unique = false, nullable = true, length = 40)
	private String subSerno;
	
	/** 分项名称 **/
	@Column(name = "SUB_NAME", unique = false, nullable = true, length = 255)
	private String subName;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 担保方式 **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 币种 **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 授信额度 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 授信期限 **/
	@Column(name = "LMT_TERM", unique = false, nullable = true, length = 10)
	private Integer lmtTerm;
	
	/** 是否预授信额度 **/
	@Column(name = "IS_PRE_LMT", unique = false, nullable = true, length = 5)
	private String isPreLmt;

	/** 是否循环额度 **/
	@Column(name = "IS_REVOLV_LIMIT", unique = false, nullable = true, length = 5)
	private String isRevolvLimit;

	/** 原额度台账分项编号 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_NO", unique = false, nullable = true, length = 40)
	private String origiLmtAccSubNo;
	
	/** 原额度台账分项金额 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal origiLmtAccSubAmt;
	
	/** 原额度台账分项期限 **/
	@Column(name = "ORIGI_LMT_ACC_SUB_TERM", unique = false, nullable = true, length = 10)
	private Integer origiLmtAccSubTerm;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param replySerno
	 */
	public void setReplySerno(String replySerno) {
		this.replySerno = replySerno;
	}
	
    /**
     * @return replySerno
     */
	public String getReplySerno() {
		return this.replySerno;
	}
	
	/**
	 * @param replySubSerno
	 */
	public void setReplySubSerno(String replySubSerno) {
		this.replySubSerno = replySubSerno;
	}
	
    /**
     * @return replySubSerno
     */
	public String getReplySubSerno() {
		return this.replySubSerno;
	}
	
	/**
	 * @param subSerno
	 */
	public void setSubSerno(String subSerno) {
		this.subSerno = subSerno;
	}
	
    /**
     * @return subSerno
     */
	public String getSubSerno() {
		return this.subSerno;
	}
	
	/**
	 * @param subName
	 */
	public void setSubName(String subName) {
		this.subName = subName;
	}
	
    /**
     * @return subName
     */
	public String getSubName() {
		return this.subName;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param lmtTerm
	 */
	public void setLmtTerm(Integer lmtTerm) {
		this.lmtTerm = lmtTerm;
	}
	
    /**
     * @return lmtTerm
     */
	public Integer getLmtTerm() {
		return this.lmtTerm;
	}
	
	/**
	 * @param isPreLmt
	 */
	public void setIsPreLmt(String isPreLmt) {
		this.isPreLmt = isPreLmt;
	}
	
    /**
     * @return isPreLmt
     */
	public String getIsPreLmt() {
		return this.isPreLmt;
	}

	/**
	 * @param isRevolvLimit
	 */
	public void setIsRevolvLimit(String isRevolvLimit) {
		this.isRevolvLimit = isRevolvLimit;
	}

	/**
	 * @return isRevolvLimit
	 */
	public String getIsRevolvLimit() {
		return this.isRevolvLimit;
	}

	/**
	 * @param origiLmtAccSubNo
	 */
	public void setOrigiLmtAccSubNo(String origiLmtAccSubNo) {
		this.origiLmtAccSubNo = origiLmtAccSubNo;
	}
	
    /**
     * @return origiLmtAccSubNo
     */
	public String getOrigiLmtAccSubNo() {
		return this.origiLmtAccSubNo;
	}
	
	/**
	 * @param origiLmtAccSubAmt
	 */
	public void setOrigiLmtAccSubAmt(java.math.BigDecimal origiLmtAccSubAmt) {
		this.origiLmtAccSubAmt = origiLmtAccSubAmt;
	}
	
    /**
     * @return origiLmtAccSubAmt
     */
	public java.math.BigDecimal getOrigiLmtAccSubAmt() {
		return this.origiLmtAccSubAmt;
	}
	
	/**
	 * @param origiLmtAccSubTerm
	 */
	public void setOrigiLmtAccSubTerm(Integer origiLmtAccSubTerm) {
		this.origiLmtAccSubTerm = origiLmtAccSubTerm;
	}
	
    /**
     * @return origiLmtAccSubTerm
     */
	public Integer getOrigiLmtAccSubTerm() {
		return this.origiLmtAccSubTerm;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}