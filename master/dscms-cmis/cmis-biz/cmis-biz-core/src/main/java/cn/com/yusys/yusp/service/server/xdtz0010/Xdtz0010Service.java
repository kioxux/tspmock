package cn.com.yusys.yusp.service.server.xdtz0010;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0010.req.Xdtz0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0010.resp.List;
import cn.com.yusys.yusp.dto.server.xdtz0010.resp.Xdtz0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:根据身份证号获取借据信息
 *
 * @author xs
 * @version 1.0
 */
@Service
public class Xdtz0010Service {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.service.server.xdtz0010.Xdtz0010Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0010DataRespDto xdtz0010(Xdtz0010DataReqDto xdtz0010DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, JSON.toJSONString(xdtz0010DataReqDto));
        Xdtz0010DataRespDto xdtz0010DataRespDto = new Xdtz0010DataRespDto();
        try {
            String certNo = xdtz0010DataReqDto.getCertNo();//身份证号
            //查询参数
            logger.info("根据身份证号查询借据编号,查询参数为:{}", certNo);
            java.util.List<List> list= accLoanMapper.getBillInfoByCertCode(certNo);
            xdtz0010DataRespDto.setList(list);
            logger.info("根据身份证号查询借据编号,反回参数为:{}", JSON.toJSONString(list));
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, JSON.toJSONString(xdtz0010DataRespDto));
        return xdtz0010DataRespDto;
    }
}