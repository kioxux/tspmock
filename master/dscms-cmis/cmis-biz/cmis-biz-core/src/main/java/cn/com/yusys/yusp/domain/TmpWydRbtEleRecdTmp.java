/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydRbtEleRecd
 * @类描述: tmp_wyd_rbt_ele_recd数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:35
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_rbt_ele_recd_tmp")
public class TmpWydRbtEleRecdTmp {
	
	/** 客户电话号码 **/
	@Column(name = "PHONE_NO")
	private String phoneNo;
	
	/** 本地呼叫号码 **/
	@Column(name = "CALLER_PHONE_NO")
	private String callerPhoneNo;
	
	/** 通话开始时间 **/
	@Column(name = "DEVICE_START_TIME")
	private java.util.Date deviceStartTime;
	
	/** 数据日期 **/
	@Column(name = "DATA_DT", unique = false, nullable = true, length = 10)
	private String dataDt;
	
	/** 客户身份证号码 **/
	@Column(name = "ID_NO", unique = false, nullable = true, length = 32)
	private String idNo;
	
	/** 企业名称 **/
	@Column(name = "ENTERPRISE_NAME", unique = false, nullable = true, length = 128)
	private String enterpriseName;
	
	
	/**
	 * @param dataDt
	 */
	public void setDataDt(String dataDt) {
		this.dataDt = dataDt;
	}
	
    /**
     * @return dataDt
     */
	public String getDataDt() {
		return this.dataDt;
	}
	
	/**
	 * @param phoneNo
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
    /**
     * @return phoneNo
     */
	public String getPhoneNo() {
		return this.phoneNo;
	}
	
	/**
	 * @param callerPhoneNo
	 */
	public void setCallerPhoneNo(String callerPhoneNo) {
		this.callerPhoneNo = callerPhoneNo;
	}
	
    /**
     * @return callerPhoneNo
     */
	public String getCallerPhoneNo() {
		return this.callerPhoneNo;
	}
	
	/**
	 * @param deviceStartTime
	 */
	public void setDeviceStartTime(java.util.Date deviceStartTime) {
		this.deviceStartTime = deviceStartTime;
	}
	
    /**
     * @return deviceStartTime
     */
	public java.util.Date getDeviceStartTime() {
		return this.deviceStartTime;
	}
	
	/**
	 * @param idNo
	 */
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	
    /**
     * @return idNo
     */
	public String getIdNo() {
		return this.idNo;
	}
	
	/**
	 * @param enterpriseName
	 */
	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}
	
    /**
     * @return enterpriseName
     */
	public String getEnterpriseName() {
		return this.enterpriseName;
	}


}