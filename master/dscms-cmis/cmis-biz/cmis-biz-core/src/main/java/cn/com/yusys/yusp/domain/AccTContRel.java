/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AccTContRel
 * @类描述: acc_t_cont_rel数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-09-04 16:43:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "acc_t_cont_rel")
public class AccTContRel extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** pkId **/
	@Id
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 出账流水号 **/
	@Id
	@Column(name = "PVP_SERNO")
	private String pvpSerno;
	
	/** 类型 **/
	@Column(name = "TYPE", unique = false, nullable = true, length = 5)
	private String type;
	
	/** 购销合同编号 **/
	@Column(name = "T_CONT_NO", unique = false, nullable = false, length = 40)
	private String tContNo;
	
	/** 协议编号编号 **/
	@Column(name = "CONT_NO", unique = false, nullable = false, length = 40)
	private String contNo;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 购销合同流水号 **/
	@Column(name = "T_CONT_SERNO", unique = false, nullable = true, length = 40)
	private String tContSerno;
	
	/** 核心银承编号(借据编号) **/
	@Column(name = "CORE_BILL_NO", unique = false, nullable = true, length = 40)
	private String coreBillNo;

	/** 放款金额（出账金额） **/
	@Column(name = "PVP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pvpAmt;
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param pvpSerno
	 */
	public void setPvpSerno(String pvpSerno) {
		this.pvpSerno = pvpSerno;
	}
	
    /**
     * @return pvpSerno
     */
	public String getPvpSerno() {
		return this.pvpSerno;
	}
	
	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
    /**
     * @return type
     */
	public String getType() {
		return this.type;
	}
	
	/**
	 * @param tContNo
	 */
	public void setTContNo(String tContNo) {
		this.tContNo = tContNo;
	}
	
    /**
     * @return tContNo
     */
	public String getTContNo() {
		return this.tContNo;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo;
	}
	
    /**
     * @return contNo
     */
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param tContSerno
	 */
	public void setTContSerno(String tContSerno) {
		this.tContSerno = tContSerno;
	}
	
    /**
     * @return tContSerno
     */
	public String getTContSerno() {
		return this.tContSerno;
	}
	
	/**
	 * @param coreBillNo
	 */
	public void setCoreBillNo(String coreBillNo) {
		this.coreBillNo = coreBillNo;
	}
	
    /**
     * @return coreBillNo
     */
	public String getCoreBillNo() {
		return this.coreBillNo;
	}

	public String gettContNo() {
		return tContNo;
	}

	public void settContNo(String tContNo) {
		this.tContNo = tContNo;
	}

	public String gettContSerno() {
		return tContSerno;
	}

	public void settContSerno(String tContSerno) {
		this.tContSerno = tContSerno;
	}

	public BigDecimal getPvpAmt() {
		return pvpAmt;
	}

	public void setPvpAmt(BigDecimal pvpAmt) {
		this.pvpAmt = pvpAmt;
	}
}