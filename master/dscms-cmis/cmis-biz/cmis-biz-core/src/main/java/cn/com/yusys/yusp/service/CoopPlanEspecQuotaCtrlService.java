package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CoopPlanEspecQuotaCtrl;
import cn.com.yusys.yusp.dto.CoopPlanEspecQuotaCtrlDto;
import cn.com.yusys.yusp.repository.mapper.CoopPlanEspecQuotaCtrlMapper;
import cn.com.yusys.yusp.server.xdqt0001.req.Xdqt0001DataReqDto;
import cn.com.yusys.yusp.server.xdqt0001.resp.Xdqt0001DataRespDto;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanEspecQuotaCtrlService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-05-13 10:08:35
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPlanEspecQuotaCtrlService {

    @Autowired
    private CoopPlanEspecQuotaCtrlMapper coopPlanEspecQuotaCtrlMapper;

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public CoopPlanEspecQuotaCtrl selectByPrimaryKey(String pkId) {
        return coopPlanEspecQuotaCtrlMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<CoopPlanEspecQuotaCtrlDto> selectAll(QueryModel model) {
        List<CoopPlanEspecQuotaCtrlDto> records = (List<CoopPlanEspecQuotaCtrlDto>) coopPlanEspecQuotaCtrlMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<CoopPlanEspecQuotaCtrlDto> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPlanEspecQuotaCtrlDto> list = coopPlanEspecQuotaCtrlMapper.selectByModel(model);
        list.stream().forEach(es -> {
            Xdqt0001DataReqDto dto = new Xdqt0001DataReqDto();
            dto.setPrdId(es.getCoopPrdId());
            ResultDto<Xdqt0001DataRespDto> respDto = dscmsCfgQtClientService.xdqt0001(dto);
            Xdqt0001DataRespDto res = respDto.getData();
            es.setCoopPrdName(res.getPrdName());
        });
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(CoopPlanEspecQuotaCtrl record) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno", record.getSerno());
        queryModel.addCondition("prdTypeProp", record.getPrdTypeProp());
        int cnt = coopPlanEspecQuotaCtrlMapper.validateCnt(queryModel);
        if (cnt > 0) {
            throw BizException.error(null, null, "产品【" + record.getPrdTypeProp() + "】已存在，禁止重复添加！");
        }
        return coopPlanEspecQuotaCtrlMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(CoopPlanEspecQuotaCtrl record) {
        return coopPlanEspecQuotaCtrlMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(CoopPlanEspecQuotaCtrl record) {
        return coopPlanEspecQuotaCtrlMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(CoopPlanEspecQuotaCtrl record) {
        return coopPlanEspecQuotaCtrlMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return coopPlanEspecQuotaCtrlMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return coopPlanEspecQuotaCtrlMapper.deleteByIds(ids);
    }


    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteBySerno(CoopPlanEspecQuotaCtrl coopPlanEspecQuotaCtrl) {
        return coopPlanEspecQuotaCtrlMapper.deleteBySerno(coopPlanEspecQuotaCtrl);
    }
}
