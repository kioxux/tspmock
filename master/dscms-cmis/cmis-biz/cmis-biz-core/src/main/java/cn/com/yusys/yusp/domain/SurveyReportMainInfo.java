/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: SurveyReportMainInfo
 * @类描述: survey_report_main_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-15 14:58:01
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "survey_report_main_info")
public class SurveyReportMainInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SURVEY_NO")
	private String surveyNo;
	
	/** 调查类型 **/
	@Column(name = "SURVEY_TYPE", unique = false, nullable = true, length = 5)
	private String surveyType;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 80)
	private String prdName;
	
	/** 产品编号 **/
	@Column(name = "PRD_CODE", unique = false, nullable = true, length = 40)
	private String prdCode;
	
	/** 产品类型 **/
	@Column(name = "PRD_TYPE", unique = false, nullable = true, length = 5)
	private String prdType;
	
	/** 客户编号 **/
	@Column(name = "CUS_NO", unique = false, nullable = true, length = 30)
	private String cusNo;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 80)
	private String cusName;
	
	/** 证件类型 **/
	@Column(name = "CERT_TYPE", unique = false, nullable = true, length = 5)
	private String certType;
	
	/** 证件号码 **/
	@Column(name = "CERT_NO", unique = false, nullable = true, length = 100)
	private String certNo;
	
	/** 申请金额 **/
	@Column(name = "APP_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal appAmt;
	
	/** 审批状态 **/
	@Column(name = "APPR_STATUS", unique = false, nullable = true, length = 5)
	private String apprStatus;
	
	/** 数据来源 **/
	@Column(name = "DATA_SOUR", unique = false, nullable = true, length = 20)
	private String dataSour;
	
	/** 进件时间 **/
	@Column(name = "INTO_TIME", unique = false, nullable = true, length = 10)
	private String intoTime;
	
	/** 营销人 **/
	@Column(name = "MAR_ID", unique = false, nullable = true, length = 40)
	private String marId;
	
	/** 登记人工号 **/
	@Column(name = "INPUT_ID_JOB_NO", unique = false, nullable = true, length = 20)
	private String inputIdJobNo;
	
	/** 责任人工号 **/
	@Column(name = "MANAGER_ID_JOB_NO", unique = false, nullable = true, length = 20)
	private String managerIdJobNo;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 80)
	private String inputId;
	
	/** 责任人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 80)
	private String managerId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 责任机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 50)
	private String managerBrId;
	
	/** 所属团队 **/
	@Column(name = "BELG_TEAM", unique = false, nullable = true, length = 5)
	private String belgTeam;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 原借据金额 **/
	@Column(name = "bill_amt", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billAmt;
	
	/** 原借据余额 **/
	@Column(name = "bill_balance", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billBalance;
	
	/** 原利率（年） **/
	@Column(name = "bill_rate", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal billRate;
	
	/** 授信批复状态 **/
	@Column(name = "lmt_status", unique = false, nullable = true, length = 5)
	private String lmtStatus;
	
	/** 是否需要线下调查 **/
	@Column(name = "online_survey", unique = false, nullable = true, length = 2)
	private String onlineSurvey;
	
	
	/**
	 * @param surveyNo
	 */
	public void setSurveyNo(String surveyNo) {
		this.surveyNo = surveyNo;
	}
	
    /**
     * @return surveyNo
     */
	public String getSurveyNo() {
		return this.surveyNo;
	}
	
	/**
	 * @param surveyType
	 */
	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}
	
    /**
     * @return surveyType
     */
	public String getSurveyType() {
		return this.surveyType;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdCode
	 */
	public void setPrdCode(String prdCode) {
		this.prdCode = prdCode;
	}
	
    /**
     * @return prdCode
     */
	public String getPrdCode() {
		return this.prdCode;
	}
	
	/**
	 * @param prdType
	 */
	public void setPrdType(String prdType) {
		this.prdType = prdType;
	}
	
    /**
     * @return prdType
     */
	public String getPrdType() {
		return this.prdType;
	}
	
	/**
	 * @param cusNo
	 */
	public void setCusNo(String cusNo) {
		this.cusNo = cusNo;
	}
	
    /**
     * @return cusNo
     */
	public String getCusNo() {
		return this.cusNo;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType;
	}
	
    /**
     * @return certType
     */
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certNo
	 */
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	
    /**
     * @return certNo
     */
	public String getCertNo() {
		return this.certNo;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return appAmt
     */
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus;
	}
	
    /**
     * @return apprStatus
     */
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param dataSour
	 */
	public void setDataSour(String dataSour) {
		this.dataSour = dataSour;
	}
	
    /**
     * @return dataSour
     */
	public String getDataSour() {
		return this.dataSour;
	}
	
	/**
	 * @param intoTime
	 */
	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime;
	}
	
    /**
     * @return intoTime
     */
	public String getIntoTime() {
		return this.intoTime;
	}
	
	/**
	 * @param marId
	 */
	public void setMarId(String marId) {
		this.marId = marId;
	}
	
    /**
     * @return marId
     */
	public String getMarId() {
		return this.marId;
	}
	
	/**
	 * @param inputIdJobNo
	 */
	public void setInputIdJobNo(String inputIdJobNo) {
		this.inputIdJobNo = inputIdJobNo;
	}
	
    /**
     * @return inputIdJobNo
     */
	public String getInputIdJobNo() {
		return this.inputIdJobNo;
	}
	
	/**
	 * @param managerIdJobNo
	 */
	public void setManagerIdJobNo(String managerIdJobNo) {
		this.managerIdJobNo = managerIdJobNo;
	}
	
    /**
     * @return managerIdJobNo
     */
	public String getManagerIdJobNo() {
		return this.managerIdJobNo;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param belgTeam
	 */
	public void setBelgTeam(String belgTeam) {
		this.belgTeam = belgTeam;
	}
	
    /**
     * @return belgTeam
     */
	public String getBelgTeam() {
		return this.belgTeam;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param billAmt
	 */
	public void setBillAmt(java.math.BigDecimal billAmt) {
		this.billAmt = billAmt;
	}
	
    /**
     * @return billAmt
     */
	public java.math.BigDecimal getBillAmt() {
		return this.billAmt;
	}
	
	/**
	 * @param billBalance
	 */
	public void setBillBalance(java.math.BigDecimal billBalance) {
		this.billBalance = billBalance;
	}
	
    /**
     * @return billBalance
     */
	public java.math.BigDecimal getBillBalance() {
		return this.billBalance;
	}
	
	/**
	 * @param billRate
	 */
	public void setBillRate(java.math.BigDecimal billRate) {
		this.billRate = billRate;
	}
	
    /**
     * @return billRate
     */
	public java.math.BigDecimal getBillRate() {
		return this.billRate;
	}
	
	/**
	 * @param lmtStatus
	 */
	public void setLmtStatus(String lmtStatus) {
		this.lmtStatus = lmtStatus;
	}
	
    /**
     * @return lmtStatus
     */
	public String getLmtStatus() {
		return this.lmtStatus;
	}
	
	/**
	 * @param onlineSurvey
	 */
	public void setOnlineSurvey(String onlineSurvey) {
		this.onlineSurvey = onlineSurvey;
	}
	
    /**
     * @return onlineSurvey
     */
	public String getOnlineSurvey() {
		return this.onlineSurvey;
	}


}