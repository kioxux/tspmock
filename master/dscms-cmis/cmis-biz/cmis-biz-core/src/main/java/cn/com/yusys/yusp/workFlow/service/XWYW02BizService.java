package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.TaskUrgentAppDto;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.CmisBizXwCommonService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.service.client.common.MessageCommonService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/31 14:31
 * @desc
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class XWYW02BizService implements ClientBizInterface {
    private final Logger log = LoggerFactory.getLogger(WfPvpLoanAppLsBiz.class);
    @Autowired
    private AmqpTemplate amqpTemplate;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;
    @Autowired
    private BGYW01BizService bgyw01BizService;
    @Autowired
    private BGYW02BizService bgyw02BizService;
    @Autowired
    private BGYW03Bizservice bgyw03Bizservice;
    @Autowired
    private BGYW07BizService bgyw07Bizservice;
    @Autowired
    private BGYW09BizService bgyw09Bizservice;
    @Autowired
    private DAGL08BizService dagl08BizService;
    @Autowired
    private DAGL10BizService dagl10BizService;
    @Autowired
    private AdminSmUserService adminSmUserService;
    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;
    @Autowired
    private MessageCommonService messageCommonService;
    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {

        String currentOpType = resultInstanceDto.getCurrentOpType();
        String bizType = resultInstanceDto.getBizType();

        String serno = resultInstanceDto.getBizId();

        log.info("进入业务类型【{}】、业务处理类型【{}】流程处理逻辑开始！", bizType, currentOpType);
        if(CmisFlowConstants.FLOW_TYPE_TYPE_XW002.equals(bizType)){
            this.handlerXW002(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG028.equals(bizType)){
            //BG028展期协议签订审核（小微）
            bgyw02BizService.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG031.equals(bizType)){
            //BG028展期协议签订审核（小微）
            bgyw03Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_DA010.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_DA028.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_DA031.equals(bizType)){
            /** DA010 放款影像补扫（小微） DA028 展期协议影像补扫（小微） DA031 担保变更协议影像补扫（小微） **/
            dagl08BizService.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG034.equals(bizType)){
            //BG034还款计划变更协议签订审核（小微）
            bgyw07Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG040.equals(bizType)){
            //BG040利率变更协议签订审核（小微）
            bgyw01BizService.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_BG037.equals(bizType)){
            //BG037延期还款协议签订审核（小微）
            bgyw09Bizservice.bizOp(resultInstanceDto);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_XW016.equals(bizType)){
            // XW016 小微对公客户出账申请
            this.handleXW016Biz(resultInstanceDto, currentOpType, serno);
        }

        //TODO 如果你要修改这个文件 请仔细确认是否存在bug 并在修改后通知 王浩
       // this.handlerXW002(resultInstanceDto);
        log.info("进入业务类型【{}】流程处理逻辑结束！", bizType, serno);

    }


    public void handlerXW002(ResultInstanceDto resultInstanceDto){
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String extSerno = resultInstanceDto.getBizId();
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + resultInstanceDto);
            }else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理：------");
                // 在这里处理 调用额度010--校验  013接口--占用
                ResultDto resultDto = pvpLoanAppService.cmisLmt0013ForXw(extSerno);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_111);
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + resultInstanceDto);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + resultInstanceDto);
                end(extSerno);
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,extSerno);
                pvpLoanAppService.createImageSpplInfo(extSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),CmisCommonConstants.STD_SPPL_BIZ_TYPE_04,"小微放款申请");
                log.info("结束操作完成:" + resultInstanceDto);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + resultInstanceDto);
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("打回操作:" + resultInstanceDto);
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_992);
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("拿回操作:" + resultInstanceDto);
                    updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("拿回初始节点操作:" + resultInstanceDto);
                updateStatus(extSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + resultInstanceDto);
                // 否决改变标志 审批中 111-> 审批不通过 998
                updateStatusRefuse(extSerno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    // 小微对公客户出账申请
    private void handleXW016Biz(ResultInstanceDto resultInstanceDto, String currentOpType, String pvpSerno) {
        log.info("小微对公客户出账申请"+pvpSerno+"流程操作:" + currentOpType+"后业务处理");
        try {
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("小微对公客户出账申请"+pvpSerno+"流程发起操作，流程参数：" + resultInstanceDto.toString());
            }else if (OpType.RUN.equals(currentOpType)) {
                if (BizCommonUtils.checkSubmitNodeIsFirst(resultInstanceDto)){
                    pvpLoanAppService.handleBusinessDataAfterStart(pvpSerno);
                }
            }else if (OpType.JUMP.equals(currentOpType)) {
                log.info("小微对公客户出账申请"+pvpSerno+"跳转操作，流程参数："+ resultInstanceDto.toString());
            } else if (OpType.END.equals(currentOpType)) {
                log.info("小微对公客户出账申请"+pvpSerno+"流程结束操作，流程参数："+ resultInstanceDto.toString());
                //放款申请通过，调用后续的业务处理过程
                pvpLoanAppService.handleBusinessAfterEnd(pvpSerno);
                // 资料未全生成影像补扫任务
                log.info("资料未全生成影像补扫任务,流程业务类型[{}],流程处理类型[{}],业务流水号[{}]",resultInstanceDto.getBizType(),currentOpType,pvpSerno);
                pvpLoanAppService.createImageSpplInfo(pvpSerno,resultInstanceDto.getBizType(),resultInstanceDto.getInstanceId(),CmisCommonConstants.STD_SPPL_BIZ_TYPE_01,"小微放款申请");
                //微信通知
                String managerId = pvpLoanApp.getManagerId();
                String mgrTel = "";
                if (StringUtil.isNotEmpty(managerId)) {
                    log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                    ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                    log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                    String code = resultDto.getCode();//返回结果
                    AdminSmUserDto adminSmUserDto = resultDto.getData();
                    if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                        adminSmUserDto = resultDto.getData();
                        mgrTel = adminSmUserDto.getUserMobilephone();
                    }
                    try {
                        //执行发送借款人操作
                        String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                        String messageType = "MSG_CF_M_0016";//短信编号
                        Map paramMap = new HashMap();//短信填充参数
                        paramMap.put("cusName", pvpLoanApp.getCusName());
                        paramMap.put("prdName", "一般贷款出账");
                        //执行发送客户经理操作
                        messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                    } catch (Exception e) {
                        throw new Exception("发送短信失败！");
                    }
                }
                log.info("小微对公客户出账申请"+pvpSerno+"发送用印，流程参数："+ resultInstanceDto.toString());
                cmisBizXwCommonService.sendYKTask(pvpSerno, resultInstanceDto.getBizType(), resultInstanceDto.getCurrentUserId(), resultInstanceDto.getBizUserName());
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("小微对公客户出账申请"+pvpSerno+"退回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                    //微信通知
                    String managerId = pvpLoanApp.getManagerId();
                    String mgrTel = "";
                    if (StringUtil.isNotEmpty(managerId)) {
                        log.info("***********调用AdminSmUserService用户信息查询服务开始*START**************");
                        ResultDto<AdminSmUserDto> resultDto = adminSmUserService.getByLoginCode(managerId);
                        log.info("***********调用AdminSmUserService用户信息查询服务结束*END*****************");
                        String code = resultDto.getCode();//返回结果
                        AdminSmUserDto adminSmUserDto = resultDto.getData();
                        if (StringUtil.isNotEmpty(code) && CmisBizConstants.NUM_ZERO.equals(code)) {
                            adminSmUserDto = resultDto.getData();
                            mgrTel = adminSmUserDto.getUserMobilephone();
                        }
                        try {
                            //执行发送借款人操作
                            String receivedUserType = "1";//接收人员类型 1--客户经理 2--借款人
                            String messageType = "MSG_CF_M_0016";//短信编号
                            Map paramMap = new HashMap();//短信填充参数
                            paramMap.put("cusName", pvpLoanApp.getCusName());
                            paramMap.put("prdName", "一般贷款出账");
                            //执行发送客户经理操作
                            messageCommonService.sendMessage(messageType, paramMap, receivedUserType, managerId, mgrTel);
                        } catch (Exception e) {
                            throw new Exception("发送短信失败！");
                        }
                    }
                }
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                log.info("小微对公客户出账申请"+pvpSerno+"打回操作，流程参数："+ resultInstanceDto.toString());
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.handleBusinessAfterCallBack(pvpSerno);
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_992);
                    pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(pvpSerno);
                }
            } else if (OpType.TACK_BACK.equals(currentOpType)) {
                log.info("小微对公客户出账申请"+pvpSerno+"拿回操作，流程参数："+ resultInstanceDto.toString());
                //针对客户经理拿回操作，应当将业务申请主表的业务状态更新为【打回-992】
                //判断流程下一节点是否为初始节点，若是初始节点，则流程主表的状态更新为打回
                if(BizCommonUtils.isFirstNodeCheck(resultInstanceDto)) {
                    pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
                }
            } else if (OpType.TACK_BACK_FIRST.equals(currentOpType)) {
                log.info("小微对公客户出账申请"+pvpSerno+"拿回初始节点操作，流程参数："+ resultInstanceDto.toString());
                //流程拿回到第一个节点，申请主表的业务
                pvpLoanAppService.updateApproveStatus(pvpSerno, CmisCommonConstants.WF_STATUS_991);
            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("小微对公客户出账申请"+pvpSerno+"否决操作，流程参数："+ resultInstanceDto.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                pvpLoanAppService.handleBusinessAfterRefuse(pvpSerno);
            } else {
                log.warn("小微对公客户出账申请"+pvpSerno+"未知操作:" + resultInstanceDto);
            }
        } catch (Exception e) {
            log.error("流程提交后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e,resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /**
     * @创建人 WH
     * @创建时间 2021-05-10 19:35
     * @注释 此处改为自己对应的流程名称 XWYW02
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return "XWYW02".equals(flowCode);
//        return "XD_PERFER_RATE_APPLY_TEST".equals(flowCode);

    }
    /**
     * @创建人 WH
     * @创建时间 2021-04-25 21:53
     * @注释 审批状态更换   替换对应的 service
     */
    public void updateStatus(String serno,String state){
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
        pvpLoanApp.setApproveStatus(state);
        pvpLoanAppService.updateSelective(pvpLoanApp);
    }

    /**
     * @param serno, state
     * @return void
     * @author hubp
     * @date 2021/11/17 20:37
     * @version 1.0.0
     * @desc 
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public void updateStatusRefuse(String serno,String state){
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);
        pvpLoanApp.setApproveStatus(state);
        pvpLoanAppService.updateSelective(pvpLoanApp);
        // 2021年11月17日20:41:24 hubp
        pvpLoanAppService.retailRestoreXw(serno);
    }
    /**
     * @创建人 WH
     * @创建时间 2021-04-25 21:53
     * @注释 审批通过 替换对应的service和 自己对应的修改完成后的方法 reviewend
     */
    public void end(String extSerno) {
        //审批通过的操作
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(extSerno);
        pvpLoanAppService.Xwyw02end(pvpLoanApp);
    }

}
