/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.domain.LmtSigInvestApp;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtSigInvestAcc;
import cn.com.yusys.yusp.service.LmtSigInvestAccService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestAccResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-26 18:27:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtsiginvestacc")
public class LmtSigInvestAccResource {
    @Autowired
    private LmtSigInvestAccService lmtSigInvestAccService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtSigInvestAcc>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtSigInvestAcc> list = lmtSigInvestAccService.selectAll(queryModel);
        return new ResultDto<List<LmtSigInvestAcc>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectByModel")
    protected ResultDto<List<LmtSigInvestAcc>> index(@RequestBody QueryModel queryModel) {
        if (StringUtils.isBlank(queryModel.getSort())){
            queryModel.setSort(" accNo desc ");
        }
        List<LmtSigInvestAcc> list = lmtSigInvestAccService.selectByModel(queryModel);
        return new ResultDto<List<LmtSigInvestAcc>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtSigInvestAcc> show(@PathVariable("pkId") String pkId) {
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtSigInvestAcc>(lmtSigInvestAcc);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtSigInvestAcc> create(@RequestBody LmtSigInvestAcc lmtSigInvestAcc) throws URISyntaxException {
        lmtSigInvestAccService.insert(lmtSigInvestAcc);
        return new ResultDto<LmtSigInvestAcc>(lmtSigInvestAcc);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtSigInvestAcc lmtSigInvestAcc) throws URISyntaxException {
        int result = lmtSigInvestAccService.update(lmtSigInvestAcc);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtSigInvestAccService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtSigInvestAccService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }


    /**
     * 更新不为属性不为空的值
     * @param lmtSigInvestAcc
     * @return
     */
    @PostMapping("/updateSelective")
    protected ResultDto<Integer> updateSelective(@RequestBody LmtSigInvestAcc lmtSigInvestAcc){
        int result = lmtSigInvestAccService.updateSelective(lmtSigInvestAcc);
        return new ResultDto<>(result);
    }

    /**
     * 更新资产编号
     * 更新不为属性不为空的值
     * @return
     */
    @PostMapping("/updateAssetNo")
    protected ResultDto<Integer> updateAssetNo(@RequestBody LmtSigInvestAcc lmtSigInvestAcc){
        int result = lmtSigInvestAccService.updateAssetNo(lmtSigInvestAcc);
        return new ResultDto<>(result);
    }

    /**
     * 获取台账编号
     * @return
     */
    @PostMapping("/selectAccNoByReplyNo")
    protected ResultDto<String> selectAccNoByReplyNo(@RequestBody String replySerno){
        String accNo = "";
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectByReplySerno(replySerno);
        if (lmtSigInvestAcc != null){
            accNo = lmtSigInvestAcc.getAccNo();
        }
        return new ResultDto<>(accNo);
    }

    /**
     * 根据台账编号获取数据
     * @return
     */
    @PostMapping("/selectByAccNo")
    protected ResultDto<LmtSigInvestAcc> selectByAccNo(@RequestBody Map condition){
        String accNo = "";
        if(condition.containsKey("accNo")){
            accNo = (String) condition.get("accNo");
        }else{
            return new ResultDto<>(null);
        }
        LmtSigInvestAcc lmtSigInvestAcc = lmtSigInvestAccService.selectByAccNo(accNo);
        return new ResultDto<>(lmtSigInvestAcc);
    }
}
