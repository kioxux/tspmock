package cn.com.yusys.yusp.web.server.xdht0012;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdht0012.req.Xdht0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0012.resp.Xdht0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.service.server.xdht0012.Xdht0012Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:借款担保合同签订/支用
 *
 * @author zoubiao
 * @version 1.0
 */
@Api(tags = "XDHT0012:借款担保合同签订/支用")
@RestController
@RequestMapping("/api/bizht4bsp")
public class BizXdht0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdht0012Resource.class);

    @Autowired
    private Xdht0012Service xdht0012Service;

    /**
     * 交易码：xdht0012
     * 交易描述：借款担保合同签订/支用
     *
     * @param xdht0012DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借款担保合同签订/支用")
    @PostMapping("/xdht0012")
    protected @ResponseBody
    ResultDto<Xdht0012DataRespDto> xdht0012(@Validated @RequestBody Xdht0012DataReqDto xdht0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012DataReqDto));
        Xdht0012DataRespDto xdht0012DataRespDto = new Xdht0012DataRespDto();// 响应Dto:借款担保合同签订/支用
        ResultDto<Xdht0012DataRespDto> xdht0012DataResultDto = new ResultDto<>();
        // 从xdht0012DataReqDto获取业务值进行业务逻辑处理
        try {
            //获取请求参数
            String oprType = xdht0012DataReqDto.getOprType();//操作类型
            String managerId = xdht0012DataReqDto.getManagerId();//客户经理号
            String grtContNo = xdht0012DataReqDto.getGrtContNo();//担保合同号
            String loanContNo = xdht0012DataReqDto.getLoanContNo();//借款合同号
            String pvpSerno = xdht0012DataReqDto.getPvpSerno();//出账流水号
            String cusId = xdht0012DataReqDto.getCusId();//客户号
            String cusName = xdht0012DataReqDto.getCusName();//客户名称
            String certNo = xdht0012DataReqDto.getCertNo();//证件号
            String curtPvpAmt = xdht0012DataReqDto.getCurtPvpAmt();//本次出账金额
            String billStartDate = xdht0012DataReqDto.getBillStartDate();//借据起始日
            String billEndDate = xdht0012DataReqDto.getBillEndDate();//借据到期日
            String term = xdht0012DataReqDto.getTerm();//期限
            String loanAcctNo = xdht0012DataReqDto.getLoanAcctNo();//贷款发放账户
            String loanAcctName = xdht0012DataReqDto.getLoanAcctName();//贷款账户名称
            String loanSubAcctNo = xdht0012DataReqDto.getLoanSubAcctNo();//贷款发放账户子账户序号
            String pvpStatus = xdht0012DataReqDto.getPvpStatus();//出账状态
            String chnlSour = xdht0012DataReqDto.getChnlSour();//来源渠道

            if (StringUtil.isEmpty(oprType)) {
                xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0012DataResultDto.setMessage("操作类型【oprType】不能为空！");
                return xdht0012DataResultDto;
            } else if (StringUtils.isEmpty(chnlSour)) {
                xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0012DataResultDto.setMessage("来源渠道【chnlSour】不能为空！");
                return xdht0012DataResultDto;
            } else if (!"01".equals(chnlSour) && !"02".equals(chnlSour)) {
                xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0012DataResultDto.setMessage("来源渠道【01手机/02pad】未知！");
                return xdht0012DataResultDto;
            } else if (StringUtil.isNotEmpty(loanContNo) && StringUtil.isNotEmpty(grtContNo)) {
                xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0012DataResultDto.setMessage("借款合同/担保合同不可全填，同时生成pdf！");
                return xdht0012DataResultDto;
            } else if (StringUtil.isEmpty(loanContNo) && StringUtil.isEmpty(grtContNo)) {
                xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdht0012DataResultDto.setMessage("借款合同/担保合同不可全为空！");
                return xdht0012DataResultDto;
            } else if (CmisBizConstants.XW_OP_TYPE_03.equals(oprType)) {
                if (StringUtils.isEmpty(loanContNo)) {
                    xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdht0012DataResultDto.setMessage("借款合同【loanContNo】不能为空！");
                    return xdht0012DataResultDto;
                } else if (StringUtils.isEmpty(billStartDate) || StringUtils.isEmpty(billEndDate) || StringUtils.isEmpty(term)) {
                    xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdht0012DataResultDto.setMessage("借据起始到期日及期限【billStartDate;billEndDate;term】不能为空！");
                    return xdht0012DataResultDto;
                } else if (StringUtils.isEmpty(cusId) || StringUtils.isEmpty(cusName)) {
                    xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdht0012DataResultDto.setMessage("客户信息【cusId;cusName】不能为空！");
                    return xdht0012DataResultDto;
                } else if (StringUtils.isEmpty(loanAcctNo) || StringUtils.isEmpty(loanAcctName) || StringUtils.isEmpty(loanSubAcctNo)) {
                    xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdht0012DataResultDto.setMessage("贷款发放账户相关信息【loanAcctNo;loanAcctName;loanSubAcctNo】不能为空！");
                    return xdht0012DataResultDto;
                } else if (StringUtils.isEmpty(curtPvpAmt)) {
                    xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdht0012DataResultDto.setMessage("出账金额【curtPvpAmt】不能为空！");
                    return xdht0012DataResultDto;
                }
            } else if (CmisBizConstants.XW_OP_TYPE_04.equals(oprType)) {
                if (StringUtils.isEmpty(certNo) && "01".equals(chnlSour)) {
                    xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdht0012DataResultDto.setMessage("身份证号码【certNo】手机端必填！");
                    return xdht0012DataResultDto;
                }
            } else if (CmisBizConstants.XW_OP_TYPE_05.equals(oprType)) {//放款(⊙o⊙)
                if (StringUtils.isEmpty(pvpSerno)) {
                    xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdht0012DataResultDto.setMessage("出账流水号【pvpSerno】不能为空！");
                    return xdht0012DataResultDto;
                }
            }

            // 调用xdht0012Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012DataReqDto));
            xdht0012DataRespDto = xdht0012Service.getXdht0012(xdht0012DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012DataRespDto));
            // 封装xdht0012DataResultDto中正确的返回码和返回信息
            xdht0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdht0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, e.getMessage());
            // 封装中异常返回码和返回信息
            xdht0012DataResultDto.setCode(e.getErrorCode());
            xdht0012DataResultDto.setMessage(e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, e.getMessage());
            // 封装xdht0012DataResultDto中异常返回码和返回信息
            xdht0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdht0012DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdht0012DataRespDto到xdht0012DataResultDto中
        xdht0012DataResultDto.setData(xdht0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012DataRespDto));
        return xdht0012DataResultDto;
    }
}
