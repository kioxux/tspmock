/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSigInvestBasicInfoAppr
 * @类描述: lmt_sig_invest_basic_info_appr数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-24 22:15:44
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sig_invest_basic_info_appr")
public class LmtSigInvestBasicInfoAppr extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 审批流水号 **/
	@Column(name = "APPROVE_SERNO", unique = false, nullable = false, length = 40)
	private String approveSerno;
	
	/** 申请流水号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 入池基础资产标准 **/
	@Column(name = "POOL_BASIC_ASSET_NORMAL", unique = false, nullable = true, length = 200)
	private String poolBasicAssetNormal;
	
	/** 初始起算日 **/
	@Column(name = "FIRST_START_DATE", unique = false, nullable = true, length = 20)
	private String firstStartDate;
	
	/** 贷款笔数 **/
	@Column(name = "LOAN_QNT", unique = false, nullable = true, length = 5)
	private String loanQnt;
	
	/** 借款人数量 **/
	@Column(name = "DEBIT_QNT", unique = false, nullable = true, length = 5)
	private String debitQnt;
	
	/** 资产池总本金余额 **/
	@Column(name = "POOL_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal poolTotalAmt;
	
	/** 资产池合同金额 **/
	@Column(name = "POOL_CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal poolContAmt;
	
	/** 单笔贷款平均本金余额 **/
	@Column(name = "SIG_LOAN_AVG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigLoanAvgAmt;
	
	/** 单个借款人平均本金余额 **/
	@Column(name = "SIG_DEBIT_AVG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigDebitAvgAmt;
	
	/** 按金额及剩余期限加权的贷款加权平均利率 **/
	@Column(name = "LOAN_WEIGHT_AVG_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanWeightAvgRate;
	
	/** 最晚一笔到期贷款期限 **/
	@Column(name = "LAST_SIG_LOAN_END_TERM", unique = false, nullable = true, length = 20)
	private String lastSigLoanEndTerm;
	
	/** 前3大借款人未偿还本金余额占比 **/
	@Column(name = "LAST_THR_UNREPAY_CAP_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastThrUnrepayCapPerc;
	
	/** 前5大借款人未偿还本金余额占比 **/
	@Column(name = "LAST_FIF_UNREPAY_CAP_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastFifUnrepayCapPerc;
	
	/** 是否穿透至底层资产 **/
	@Column(name = "IS_PASS_BASIC_ASSET", unique = false, nullable = true, length = 5)
	private String isPassBasicAsset;
	
	/** 是否全部穿透 **/
	@Column(name = "IS_TOTAL_PASS_BASIC", unique = false, nullable = true, length = 5)
	private String isTotalPassBasic;
	
	/** 是否能识别底层资产中单笔资产金额未超过1级资本净额0.15%部分 **/
	@Column(name = "IS_BASIC_ASSET", unique = false, nullable = true, length = 5)
	private String isBasicAsset;
	
	/** 底层资产中单笔资产金额未超过1级资本净额0.15%部分的合计金额 **/
	@Column(name = "BASIC_ASSET_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal basicAssetTotalAmt;
	
	/** 底层资产类型 **/
	@Column(name = "BASIC_ASSET_TYPE", unique = false, nullable = true, length = 5)
	private String basicAssetType;
	
	/** 资产包最大单项金额 **/
	@Column(name = "ASSET_PACK_MAX_SIG_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal assetPackMaxSigAmt;
	
	/** 资产包户数 **/
	@Column(name = "ASSET_PACK_QNT", unique = false, nullable = true, length = 5)
	private String assetPackQnt;
	
	/** 基本情况分析 **/
	@Column(name = "BASIC_CASE_ANALY", unique = false, nullable = true, length = 2000)
	private String basicCaseAnaly;
	
	/** 资产数量 **/
	@Column(name = "ASSET_QNT", unique = false, nullable = true, length = 5)
	private String assetQnt;
	
	/** 发行人数量 **/
	@Column(name = "ISSUE_QNT", unique = false, nullable = true, length = 5)
	private String issueQnt;
	
	/** 资产池投资金额 **/
	@Column(name = "POOL_INVEST_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal poolInvestAmt;
	
	/** 投资期限 **/
	@Column(name = "INVEST_TERM", unique = false, nullable = true, length = 10)
	private Integer investTerm;
	
	/** 剩余投资期限 **/
	@Column(name = "SURPLUS_INVEST_TERM", unique = false, nullable = true, length = 10)
	private Integer surplusInvestTerm;
	
	/** 底层基础资产基本情况分析 **/
	@Column(name = "BASIC_ASSET_BASIC_CASE_ANALY", unique = false, nullable = true, length = 2000)
	private String basicAssetBasicCaseAnaly;
	
	/** 其他说明 **/
	@Column(name = "OTHER_DESC", unique = false, nullable = true, length = 2000)
	private String otherDesc;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近更新人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近更新机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近更新日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param approveSerno
	 */
	public void setApproveSerno(String approveSerno) {
		this.approveSerno = approveSerno;
	}
	
    /**
     * @return approveSerno
     */
	public String getApproveSerno() {
		return this.approveSerno;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param poolBasicAssetNormal
	 */
	public void setPoolBasicAssetNormal(String poolBasicAssetNormal) {
		this.poolBasicAssetNormal = poolBasicAssetNormal;
	}
	
    /**
     * @return poolBasicAssetNormal
     */
	public String getPoolBasicAssetNormal() {
		return this.poolBasicAssetNormal;
	}
	
	/**
	 * @param firstStartDate
	 */
	public void setFirstStartDate(String firstStartDate) {
		this.firstStartDate = firstStartDate;
	}
	
    /**
     * @return firstStartDate
     */
	public String getFirstStartDate() {
		return this.firstStartDate;
	}
	
	/**
	 * @param loanQnt
	 */
	public void setLoanQnt(String loanQnt) {
		this.loanQnt = loanQnt;
	}
	
    /**
     * @return loanQnt
     */
	public String getLoanQnt() {
		return this.loanQnt;
	}
	
	/**
	 * @param debitQnt
	 */
	public void setDebitQnt(String debitQnt) {
		this.debitQnt = debitQnt;
	}
	
    /**
     * @return debitQnt
     */
	public String getDebitQnt() {
		return this.debitQnt;
	}
	
	/**
	 * @param poolTotalAmt
	 */
	public void setPoolTotalAmt(java.math.BigDecimal poolTotalAmt) {
		this.poolTotalAmt = poolTotalAmt;
	}
	
    /**
     * @return poolTotalAmt
     */
	public java.math.BigDecimal getPoolTotalAmt() {
		return this.poolTotalAmt;
	}
	
	/**
	 * @param poolContAmt
	 */
	public void setPoolContAmt(java.math.BigDecimal poolContAmt) {
		this.poolContAmt = poolContAmt;
	}
	
    /**
     * @return poolContAmt
     */
	public java.math.BigDecimal getPoolContAmt() {
		return this.poolContAmt;
	}
	
	/**
	 * @param sigLoanAvgAmt
	 */
	public void setSigLoanAvgAmt(java.math.BigDecimal sigLoanAvgAmt) {
		this.sigLoanAvgAmt = sigLoanAvgAmt;
	}
	
    /**
     * @return sigLoanAvgAmt
     */
	public java.math.BigDecimal getSigLoanAvgAmt() {
		return this.sigLoanAvgAmt;
	}
	
	/**
	 * @param sigDebitAvgAmt
	 */
	public void setSigDebitAvgAmt(java.math.BigDecimal sigDebitAvgAmt) {
		this.sigDebitAvgAmt = sigDebitAvgAmt;
	}
	
    /**
     * @return sigDebitAvgAmt
     */
	public java.math.BigDecimal getSigDebitAvgAmt() {
		return this.sigDebitAvgAmt;
	}
	
	/**
	 * @param loanWeightAvgRate
	 */
	public void setLoanWeightAvgRate(java.math.BigDecimal loanWeightAvgRate) {
		this.loanWeightAvgRate = loanWeightAvgRate;
	}
	
    /**
     * @return loanWeightAvgRate
     */
	public java.math.BigDecimal getLoanWeightAvgRate() {
		return this.loanWeightAvgRate;
	}
	
	/**
	 * @param lastSigLoanEndTerm
	 */
	public void setLastSigLoanEndTerm(String lastSigLoanEndTerm) {
		this.lastSigLoanEndTerm = lastSigLoanEndTerm;
	}
	
    /**
     * @return lastSigLoanEndTerm
     */
	public String getLastSigLoanEndTerm() {
		return this.lastSigLoanEndTerm;
	}
	
	/**
	 * @param lastThrUnrepayCapPerc
	 */
	public void setLastThrUnrepayCapPerc(java.math.BigDecimal lastThrUnrepayCapPerc) {
		this.lastThrUnrepayCapPerc = lastThrUnrepayCapPerc;
	}
	
    /**
     * @return lastThrUnrepayCapPerc
     */
	public java.math.BigDecimal getLastThrUnrepayCapPerc() {
		return this.lastThrUnrepayCapPerc;
	}
	
	/**
	 * @param lastFifUnrepayCapPerc
	 */
	public void setLastFifUnrepayCapPerc(java.math.BigDecimal lastFifUnrepayCapPerc) {
		this.lastFifUnrepayCapPerc = lastFifUnrepayCapPerc;
	}
	
    /**
     * @return lastFifUnrepayCapPerc
     */
	public java.math.BigDecimal getLastFifUnrepayCapPerc() {
		return this.lastFifUnrepayCapPerc;
	}
	
	/**
	 * @param isPassBasicAsset
	 */
	public void setIsPassBasicAsset(String isPassBasicAsset) {
		this.isPassBasicAsset = isPassBasicAsset;
	}
	
    /**
     * @return isPassBasicAsset
     */
	public String getIsPassBasicAsset() {
		return this.isPassBasicAsset;
	}
	
	/**
	 * @param isTotalPassBasic
	 */
	public void setIsTotalPassBasic(String isTotalPassBasic) {
		this.isTotalPassBasic = isTotalPassBasic;
	}
	
    /**
     * @return isTotalPassBasic
     */
	public String getIsTotalPassBasic() {
		return this.isTotalPassBasic;
	}
	
	/**
	 * @param isBasicAsset
	 */
	public void setIsBasicAsset(String isBasicAsset) {
		this.isBasicAsset = isBasicAsset;
	}
	
    /**
     * @return isBasicAsset
     */
	public String getIsBasicAsset() {
		return this.isBasicAsset;
	}
	
	/**
	 * @param basicAssetTotalAmt
	 */
	public void setBasicAssetTotalAmt(java.math.BigDecimal basicAssetTotalAmt) {
		this.basicAssetTotalAmt = basicAssetTotalAmt;
	}
	
    /**
     * @return basicAssetTotalAmt
     */
	public java.math.BigDecimal getBasicAssetTotalAmt() {
		return this.basicAssetTotalAmt;
	}
	
	/**
	 * @param basicAssetType
	 */
	public void setBasicAssetType(String basicAssetType) {
		this.basicAssetType = basicAssetType;
	}
	
    /**
     * @return basicAssetType
     */
	public String getBasicAssetType() {
		return this.basicAssetType;
	}
	
	/**
	 * @param assetPackMaxSigAmt
	 */
	public void setAssetPackMaxSigAmt(java.math.BigDecimal assetPackMaxSigAmt) {
		this.assetPackMaxSigAmt = assetPackMaxSigAmt;
	}
	
    /**
     * @return assetPackMaxSigAmt
     */
	public java.math.BigDecimal getAssetPackMaxSigAmt() {
		return this.assetPackMaxSigAmt;
	}
	
	/**
	 * @param assetPackQnt
	 */
	public void setAssetPackQnt(String assetPackQnt) {
		this.assetPackQnt = assetPackQnt;
	}
	
    /**
     * @return assetPackQnt
     */
	public String getAssetPackQnt() {
		return this.assetPackQnt;
	}
	
	/**
	 * @param basicCaseAnaly
	 */
	public void setBasicCaseAnaly(String basicCaseAnaly) {
		this.basicCaseAnaly = basicCaseAnaly;
	}
	
    /**
     * @return basicCaseAnaly
     */
	public String getBasicCaseAnaly() {
		return this.basicCaseAnaly;
	}
	
	/**
	 * @param assetQnt
	 */
	public void setAssetQnt(String assetQnt) {
		this.assetQnt = assetQnt;
	}
	
    /**
     * @return assetQnt
     */
	public String getAssetQnt() {
		return this.assetQnt;
	}
	
	/**
	 * @param issueQnt
	 */
	public void setIssueQnt(String issueQnt) {
		this.issueQnt = issueQnt;
	}
	
    /**
     * @return issueQnt
     */
	public String getIssueQnt() {
		return this.issueQnt;
	}
	
	/**
	 * @param poolInvestAmt
	 */
	public void setPoolInvestAmt(java.math.BigDecimal poolInvestAmt) {
		this.poolInvestAmt = poolInvestAmt;
	}
	
    /**
     * @return poolInvestAmt
     */
	public java.math.BigDecimal getPoolInvestAmt() {
		return this.poolInvestAmt;
	}
	
	/**
	 * @param investTerm
	 */
	public void setInvestTerm(Integer investTerm) {
		this.investTerm = investTerm;
	}
	
    /**
     * @return investTerm
     */
	public Integer getInvestTerm() {
		return this.investTerm;
	}
	
	/**
	 * @param surplusInvestTerm
	 */
	public void setSurplusInvestTerm(Integer surplusInvestTerm) {
		this.surplusInvestTerm = surplusInvestTerm;
	}
	
    /**
     * @return surplusInvestTerm
     */
	public Integer getSurplusInvestTerm() {
		return this.surplusInvestTerm;
	}
	
	/**
	 * @param basicAssetBasicCaseAnaly
	 */
	public void setBasicAssetBasicCaseAnaly(String basicAssetBasicCaseAnaly) {
		this.basicAssetBasicCaseAnaly = basicAssetBasicCaseAnaly;
	}
	
    /**
     * @return basicAssetBasicCaseAnaly
     */
	public String getBasicAssetBasicCaseAnaly() {
		return this.basicAssetBasicCaseAnaly;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc;
	}
	
    /**
     * @return otherDesc
     */
	public String getOtherDesc() {
		return this.otherDesc;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}