package cn.com.yusys.yusp.service.server.xdsx0023;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0023.req.Xdsx0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0023.resp.Xdsx0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrCvrgContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtReplyAccMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * 接口处理类:保函协议查询
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdsx0023Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0023Service.class);

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CtrCvrgContMapper ctrCvrgContMapper;

    @Autowired
    private LmtReplyAccMapper lmtReplyAccMapper;

    @Autowired
    private CommonService commonService;

    @Autowired
    private CmisCusClientService cmisCusClientService;


    /**
     * 保函协议查询
     *
     * @param xdsx0023DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public ResultDto<Xdsx0023DataRespDto> getCvrgContDetail(Xdsx0023DataReqDto xdsx0023DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023DataReqDto));
        ResultDto<Xdsx0023DataRespDto> xdsx0023DataResultDto = new ResultDto<>();
        String certno = xdsx0023DataReqDto.getCertno();//组织机构代码
        String queflg = xdsx0023DataReqDto.getQueflg();//查询条件
        String cusId = cmisCusClientService.queryCusIdByCertCode(certno);
        Map queryMap = new HashMap();
        if (StringUtils.isNotBlank(cusId)) {
            queryMap.put("cusId", cusId);
        } else {
            xdsx0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0023DataResultDto.setMessage("未查询到该企业信息！");
            return xdsx0023DataResultDto;
        }
        Xdsx0023DataRespDto result = null;
        try {
            Integer contImage = ctrCvrgContMapper.isContImage(queryMap);
            if (Objects.isNull(contImage)) {
                xdsx0023DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdsx0023DataResultDto.setMessage("未查询到该企业下生效的保函协议![信贷管理系统]");
                return xdsx0023DataResultDto;
            } else {
                if (contImage > 0) {
                    result = ctrCvrgContMapper.getCvrgContDetail(queryMap);
                    if (Objects.isNull(result)) {
                        xdsx0023DataResultDto.setCode(EpbEnum.EPB099999.key);
                        xdsx0023DataResultDto.setMessage("未查询到该企业下生效的保函协议![信贷管理系统]");
                        return xdsx0023DataResultDto;
                    } else {
                        if (CmisBizConstants.CORRE_REL_2.equals(queflg)) {
                            queryMap.put("contNo", result.getContno());
                            // 合同折算人民币金额
                            BigDecimal lmtAmt = Optional.ofNullable(ctrCvrgContMapper.getLmtAmt(queryMap)).orElse(new BigDecimal(0L));
                            //台账金额
                            BigDecimal bal = Optional.ofNullable(lmtReplyAccMapper.getBalance(queryMap)).orElse(new BigDecimal(0L));
                            result.setLmtamt(lmtAmt);
                            result.setKylmam(lmtAmt.subtract(bal));
                        }
                        if (CmisBizConstants.CORRE_REL_3.equals(queflg)) {
                            Map<String, String> param = new HashMap<>();
                            param.put("contNo", result.getContno());
                            result.setBillno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.BILL_NO_SEQ, param));
                        }
                    }
                } else {
                    xdsx0023DataResultDto.setCode(EpbEnum.EPB099999.key);
                    xdsx0023DataResultDto.setMessage("在线投标保函合同已签订生效，请打印合同并提交集中作业中心审核！[信贷管理系统]");
                    return xdsx0023DataResultDto;
                }
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, e.getMessage());
            xdsx0023DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0023DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        xdsx0023DataResultDto.setData(result);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023DataResultDto));
        return xdsx0023DataResultDto;
    }

}
