/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;

import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpAppMain
 * @类描述: iqp_app_main数据实体类
 * @功能描述: 
 * @创建人: user
 * @创建时间: 2021-04-03 15:23:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_app_main")
public class IqpAppMain extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 100)
	private String cusName;
	
	/** 产品编号 **/
	@Column(name = "PRD_ID", unique = false, nullable = true, length = 10)
	private String prdId;
	
	/** 产品名称 **/
	@Column(name = "PRD_NAME", unique = false, nullable = true, length = 50)
	private String prdName;
	
	/** 业务类型 **/
	@Column(name = "BUSI_TYPE", unique = false, nullable = true, length = 5)
	private String busiType;
	
	/** 担保方式 STD_ZB_ASSURE_MEANS **/
	@Column(name = "GUAR_MODE", unique = false, nullable = true, length = 5)
	private String guarMode;
	
	/** 合同币种 STD_ZX_CUR_TYPE **/
	@Column(name = "CUR_TYPE", unique = false, nullable = true, length = 5)
	private String curType;
	
	/** 合同金额 **/
	@Column(name = "CONT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal contAmt;
	
	/** 合同期限 **/
	@Column(name = "CONT_TERM", unique = false, nullable = true, length = 10)
	private String contTerm;
	
	/** 合同起始日 **/
	@Column(name = "START_DATE", unique = false, nullable = true, length = 10)
	private String startDate;
	
	/** 合同到期日 **/
	@Column(name = "END_DATE", unique = false, nullable = true, length = 10)
	private String endDate;
	
	/** 是否续签  STD_ZB_YES_NO **/
	@Column(name = "IS_RENEW", unique = false, nullable = true, length = 5)
	private String isRenew;
	
	/** 原合同编号 **/
	@Column(name = "ORIGI_CONT_NO", unique = false, nullable = true, length = 40)
	private String origiContNo;
	
	/** 是否使用授信额度 STD_ZB_YES_NO **/
	@Column(name = "IS_USE_LMT_AMT", unique = false, nullable = true, length = 5)
	private String isUseLmtAmt;
	
	/** 授信额度编号 **/
	@Column(name = "LMT_ACC_NO", unique = false, nullable = true, length = 40)
	private String lmtAccNo;
	
	/** 批复编号 **/
	@Column(name = "REPLY_NO", unique = false, nullable = true, length = 40)
	private String replyNo;
	
	/** 是否电子用印 STD_ZB_YES_NO **/
	@Column(name = "IS_E_SEAL", unique = false, nullable = true, length = 5)
	private String isESeal;
	
	/** 是否在线抵押 STD_ZB_YES_NO **/
	@Column(name = "IS_OL_PLD", unique = false, nullable = true, length = 5)
	private String isOlPld;
	
	/** 所属条线 **/
	@Column(name = "BELG_LINE", unique = false, nullable = true, length = 20)
	private String belgLine;
	
	/** 债项等级 **/
	@Column(name = "DEBT_LEVEL", unique = false, nullable = true, length = 40)
	private String debtLevel;
	
	/** 违约风险暴露EAD **/
	@Column(name = "EAD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal ead;
	
	/** 违约损失率LGD **/
	@Column(name = "LGD", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lgd;
	
	/** 联系人 **/
	@Column(name = "LINKMAN", unique = false, nullable = true, length = 80)
	private String linkman;
	
	/** 电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;
	
	/** 传真 **/
	@Column(name = "FAX", unique = false, nullable = true, length = 20)
	private String fax;
	
	/** 邮箱 **/
	@Column(name = "EMAIL", unique = false, nullable = true, length = 80)
	private String email;
	
	/** QQ **/
	@Column(name = "QQ", unique = false, nullable = true, length = 20)
	private String qq;
	
	/** 微信 **/
	@Column(name = "WECHAT", unique = false, nullable = true, length = 40)
	private String wechat;
	
	/** 送达地址 **/
	@Column(name = "SERVED_ADDR", unique = false, nullable = true, length = 500)
	private String servedAddr;
	
	/** 审批状态 WF_STATUS **/
	@Column(name = "APPR_STATUS", unique = false, nullable = true, length = 5)
	private String apprStatus;
	
	/** 操作类型 STD_ZB_OPER_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = false, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = false, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = false, length = 10)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UDP_ID", unique = false, nullable = false, length = 20)
	private String udpId;
	
	/** 最后修改机构 **/
	@Column(name = "UDP_BR_ID", unique = false, nullable = false, length = 20)
	private String udpBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UDP_DATE", unique = false, nullable = false, length = 10)
	private String udpDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = false, length = 19)
	private Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = false, length = 19)
	private Date updateTime;

	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId;
	}
	
    /**
     * @return prdId
     */
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName;
	}
	
    /**
     * @return prdName
     */
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param busiType
	 */
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}
	
    /**
     * @return busiType
     */
	public String getBusiType() {
		return this.busiType;
	}
	
	/**
	 * @param guarMode
	 */
	public void setGuarMode(String guarMode) {
		this.guarMode = guarMode;
	}
	
    /**
     * @return guarMode
     */
	public String getGuarMode() {
		return this.guarMode;
	}
	
	/**
	 * @param curType
	 */
	public void setCurType(String curType) {
		this.curType = curType;
	}
	
    /**
     * @return curType
     */
	public String getCurType() {
		return this.curType;
	}
	
	/**
	 * @param contAmt
	 */
	public void setContAmt(java.math.BigDecimal contAmt) {
		this.contAmt = contAmt;
	}
	
    /**
     * @return contAmt
     */
	public java.math.BigDecimal getContAmt() {
		return this.contAmt;
	}
	
	/**
	 * @param contTerm
	 */
	public void setContTerm(String contTerm) {
		this.contTerm = contTerm;
	}
	
    /**
     * @return contTerm
     */
	public String getContTerm() {
		return this.contTerm;
	}
	
	/**
	 * @param startDate
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
    /**
     * @return startDate
     */
	public String getStartDate() {
		return this.startDate;
	}
	
	/**
	 * @param endDate
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
    /**
     * @return endDate
     */
	public String getEndDate() {
		return this.endDate;
	}
	
	/**
	 * @param isRenew
	 */
	public void setIsRenew(String isRenew) {
		this.isRenew = isRenew;
	}
	
    /**
     * @return isRenew
     */
	public String getIsRenew() {
		return this.isRenew;
	}
	
	/**
	 * @param origiContNo
	 */
	public void setOrigiContNo(String origiContNo) {
		this.origiContNo = origiContNo;
	}
	
    /**
     * @return origiContNo
     */
	public String getOrigiContNo() {
		return this.origiContNo;
	}
	
	/**
	 * @param isUseLmtAmt
	 */
	public void setIsUseLmtAmt(String isUseLmtAmt) {
		this.isUseLmtAmt = isUseLmtAmt;
	}
	
    /**
     * @return isUseLmtAmt
     */
	public String getIsUseLmtAmt() {
		return this.isUseLmtAmt;
	}
	
	/**
	 * @param lmtAccNo
	 */
	public void setLmtAccNo(String lmtAccNo) {
		this.lmtAccNo = lmtAccNo;
	}
	
    /**
     * @return lmtAccNo
     */
	public String getLmtAccNo() {
		return this.lmtAccNo;
	}
	
	/**
	 * @param replyNo
	 */
	public void setReplyNo(String replyNo) {
		this.replyNo = replyNo;
	}
	
    /**
     * @return replyNo
     */
	public String getReplyNo() {
		return this.replyNo;
	}
	
	/**
	 * @param isESeal
	 */
	public void setIsESeal(String isESeal) {
		this.isESeal = isESeal;
	}
	
    /**
     * @return isESeal
     */
	public String getIsESeal() {
		return this.isESeal;
	}
	
	/**
	 * @param isOlPld
	 */
	public void setIsOlPld(String isOlPld) {
		this.isOlPld = isOlPld;
	}
	
    /**
     * @return isOlPld
     */
	public String getIsOlPld() {
		return this.isOlPld;
	}
	
	/**
	 * @param belgLine
	 */
	public void setBelgLine(String belgLine) {
		this.belgLine = belgLine;
	}
	
    /**
     * @return belgLine
     */
	public String getBelgLine() {
		return this.belgLine;
	}
	
	/**
	 * @param debtLevel
	 */
	public void setDebtLevel(String debtLevel) {
		this.debtLevel = debtLevel;
	}
	
    /**
     * @return debtLevel
     */
	public String getDebtLevel() {
		return this.debtLevel;
	}
	
	/**
	 * @param ead
	 */
	public void setEad(java.math.BigDecimal ead) {
		this.ead = ead;
	}
	
    /**
     * @return ead
     */
	public java.math.BigDecimal getEad() {
		return this.ead;
	}
	
	/**
	 * @param lgd
	 */
	public void setLgd(java.math.BigDecimal lgd) {
		this.lgd = lgd;
	}
	
    /**
     * @return lgd
     */
	public java.math.BigDecimal getLgd() {
		return this.lgd;
	}
	
	/**
	 * @param linkman
	 */
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}
	
    /**
     * @return linkman
     */
	public String getLinkman() {
		return this.linkman;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	
    /**
     * @return fax
     */
	public String getFax() {
		return this.fax;
	}
	
	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
    /**
     * @return email
     */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * @param qq
	 */
	public void setQq(String qq) {
		this.qq = qq;
	}
	
    /**
     * @return qq
     */
	public String getQq() {
		return this.qq;
	}
	
	/**
	 * @param wechat
	 */
	public void setWechat(String wechat) {
		this.wechat = wechat;
	}
	
    /**
     * @return wechat
     */
	public String getWechat() {
		return this.wechat;
	}
	
	/**
	 * @param servedAddr
	 */
	public void setServedAddr(String servedAddr) {
		this.servedAddr = servedAddr;
	}
	
    /**
     * @return servedAddr
     */
	public String getServedAddr() {
		return this.servedAddr;
	}
	
	/**
	 * @param apprStatus
	 */
	public void setApprStatus(String apprStatus) {
		this.apprStatus = apprStatus;
	}
	
    /**
     * @return apprStatus
     */
	public String getApprStatus() {
		return this.apprStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param udpId
	 */
	public void setUdpId(String udpId) {
		this.udpId = udpId;
	}
	
    /**
     * @return udpId
     */
	public String getUdpId() {
		return this.udpId;
	}
	
	/**
	 * @param udpBrId
	 */
	public void setUdpBrId(String udpBrId) {
		this.udpBrId = udpBrId;
	}
	
    /**
     * @return udpBrId
     */
	public String getUdpBrId() {
		return this.udpBrId;
	}
	
	/**
	 * @param udpDate
	 */
	public void setUdpDate(String udpDate) {
		this.udpDate = udpDate;
	}
	
    /**
     * @return udpDate
     */
	public String getUdpDate() {
		return this.udpDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public Date getUpdateTime() {
		return this.updateTime;
	}


}