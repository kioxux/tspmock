/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.commons.excelcsv.ExcelUtils;
import cn.com.yusys.yusp.commons.excelcsv.async.DefaultDataHandle;
import cn.com.yusys.yusp.commons.excelcsv.model.ProgressDto;
import cn.com.yusys.yusp.commons.file.FileInfo;
import cn.com.yusys.yusp.commons.file.util.FileInfoUtils;
import cn.com.yusys.yusp.commons.util.IdUtils;
import cn.com.yusys.yusp.enums.returncode.EcnEnum;
import cn.com.yusys.yusp.vo.BlankCertiInfoVo;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.BlankCertiInfo;
import cn.com.yusys.yusp.service.BlankCertiInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: BlankCertiInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 21:59:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/blankcertiinfo")
public class BlankCertiInfoResource {
    private static final Logger log = LoggerFactory.getLogger(BlankCertiInfoService.class);

    @Autowired
    private BlankCertiInfoService blankCertiInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<BlankCertiInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<BlankCertiInfo> list = blankCertiInfoService.selectAll(queryModel);
        return new ResultDto<>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<BlankCertiInfo>> index(QueryModel queryModel) {
        List<BlankCertiInfo> list = blankCertiInfoService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<BlankCertiInfo> create(@RequestBody BlankCertiInfo blankCertiInfo) throws URISyntaxException {
        blankCertiInfoService.insert(blankCertiInfo);
        return new ResultDto<>(blankCertiInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody BlankCertiInfo blankCertiInfo) throws URISyntaxException {
        int result = blankCertiInfoService.update(blankCertiInfo);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId) {
        int result = blankCertiInfoService.deleteByPrimaryKey(pkId);
        return new ResultDto<>(result);
    }

    /**
     * 查询凭证编号是否存在
     *
     * @return
     */
    @PostMapping("/checkcertinoisexist")
    protected ResultDto<String> checkCertiNoIsExist(@RequestBody QueryModel queryModel) {
        String result = blankCertiInfoService.checkCertiNoIsExist(queryModel);
        return new ResultDto<>(result);
    }

    /**
     * 查询凭证编号是否被权证入库引用
     *
     * @return
     */
    @PostMapping("/checkcertinoisused")
    protected ResultDto<String> checkCertiNoIsUsed(@RequestBody QueryModel queryModel) {
        String result = blankCertiInfoService.checkCertiNoIsUsed(queryModel);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:queryList
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/querylist")
    protected ResultDto<List<BlankCertiInfo>> queryList(@RequestBody QueryModel queryModel) {
        List<BlankCertiInfo> list = blankCertiInfoService.selectByModel(queryModel);
        return new ResultDto<>(list);
    }

    /**
     * Excel数据导入
     * @param fileId Excel文件信息ID
     * @return
     */
    @PostMapping("/importcertiinfo")
    public ResultDto<ProgressDto> asyncImportCertiinfo01(@RequestParam("fileId") String fileId) {

        FileInfo fileInfo = FileInfoUtils.fromIdentity(fileId);
        String fileName = fileInfo.getFileName();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
        File tempFile;
        try {
            tempFile = File.createTempFile(IdUtils.getId(), "." + suffix, null);
            FileUtils.copyInputStreamToFile(FileInfoUtils.openDownloadStream(fileInfo), tempFile);
            log.info("开始执行异步导入，导入fileId为[{}];", fileId);

            ExcelUtils.syncImport(BlankCertiInfoVo.class, tempFile, new DefaultDataHandle(), ExcelUtils.batchConsumer(dataList -> {
                return blankCertiInfoService.insertBlankCertiInfo(dataList);
            }), true);

        } catch (IOException e) {
            log.error("空白凭证登记簿批量导入失败",e);
            return ResultDto.error(EcnEnum.ECN069999.key, e.getMessage());
        }
        return ResultDto.success().message("导入成功！");
    }
}