/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptBasic
 * @类描述: rpt_basic数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-28 16:32:56
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_basic")
public class RptBasic extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 上期授信要求落实情况 **/
	@Column(name = "LAST_LMT_NEED_PACT_CASE", unique = false, nullable = true, length = 65535)
	private String lastLmtNeedPactCase;
	
	/** 上期压降计划落实情况 **/
	@Column(name = "LAST_PDO_ACT_CASE_MEMO", unique = false, nullable = true, length = 65535)
	private String lastPdoActCaseMemo;
	
	/** 其他风险信息及处置情况 **/
	@Column(name = "OTHER_RISK_EVENT_EXPL", unique = false, nullable = true, length = 65535)
	private String otherRiskEventExpl;
	
	/** 新增融资理由 **/
	@Column(name = "NEW_FINA_REASON", unique = false, nullable = true, length = 65535)
	private String newFinaReason;
	
	/** 其他需说明的事项 **/
	@Column(name = "OTHER_NEED_DESC", unique = false, nullable = true, length = 65535)
	private String otherNeedDesc;
	
	/** 额度的合理性分析和其他需说明事项 **/
	@Column(name = "LMT_RATION_ANALY", unique = false, nullable = true, length = 65535)
	private String lmtRationAnaly;
	
	/** 主要优势 **/
	@Column(name = "MAIN_ADVANT", unique = false, nullable = true, length = 65535)
	private String mainAdvant;
	
	/** 风险点及对应的风控措施 **/
	@Column(name = "RISK_MAIN_PACM_DESC", unique = false, nullable = true, length = 65535)
	private String riskMainPacmDesc;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 10)
	private String inputDate;
	
	/** 上期授信要求 **/
	@Column(name = "LMT_NEED_PACT", unique = false, nullable = true, length = 65535)
	private String lmtNeedPact;
	
	/** 压降计划 **/
	@Column(name = "PDO_ACT_CASE", unique = false, nullable = true, length = 65535)
	private String pdoActCase;
	
	/** 用信条件 **/
	@Column(name = "CREDIT_CONDITIONS", unique = false, nullable = true, length = 65535)
	private String creditConditions;
	
	/** 风控建议 **/
	@Column(name = "RISK_CONTROL", unique = false, nullable = true, length = 65535)
	private String riskControl;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lastLmtNeedPactCase
	 */
	public void setLastLmtNeedPactCase(String lastLmtNeedPactCase) {
		this.lastLmtNeedPactCase = lastLmtNeedPactCase;
	}
	
    /**
     * @return lastLmtNeedPactCase
     */
	public String getLastLmtNeedPactCase() {
		return this.lastLmtNeedPactCase;
	}
	
	/**
	 * @param lastPdoActCaseMemo
	 */
	public void setLastPdoActCaseMemo(String lastPdoActCaseMemo) {
		this.lastPdoActCaseMemo = lastPdoActCaseMemo;
	}
	
    /**
     * @return lastPdoActCaseMemo
     */
	public String getLastPdoActCaseMemo() {
		return this.lastPdoActCaseMemo;
	}
	
	/**
	 * @param otherRiskEventExpl
	 */
	public void setOtherRiskEventExpl(String otherRiskEventExpl) {
		this.otherRiskEventExpl = otherRiskEventExpl;
	}
	
    /**
     * @return otherRiskEventExpl
     */
	public String getOtherRiskEventExpl() {
		return this.otherRiskEventExpl;
	}
	
	/**
	 * @param newFinaReason
	 */
	public void setNewFinaReason(String newFinaReason) {
		this.newFinaReason = newFinaReason;
	}
	
    /**
     * @return newFinaReason
     */
	public String getNewFinaReason() {
		return this.newFinaReason;
	}
	
	/**
	 * @param otherNeedDesc
	 */
	public void setOtherNeedDesc(String otherNeedDesc) {
		this.otherNeedDesc = otherNeedDesc;
	}
	
    /**
     * @return otherNeedDesc
     */
	public String getOtherNeedDesc() {
		return this.otherNeedDesc;
	}
	
	/**
	 * @param lmtRationAnaly
	 */
	public void setLmtRationAnaly(String lmtRationAnaly) {
		this.lmtRationAnaly = lmtRationAnaly;
	}
	
    /**
     * @return lmtRationAnaly
     */
	public String getLmtRationAnaly() {
		return this.lmtRationAnaly;
	}
	
	/**
	 * @param mainAdvant
	 */
	public void setMainAdvant(String mainAdvant) {
		this.mainAdvant = mainAdvant;
	}
	
    /**
     * @return mainAdvant
     */
	public String getMainAdvant() {
		return this.mainAdvant;
	}
	
	/**
	 * @param riskMainPacmDesc
	 */
	public void setRiskMainPacmDesc(String riskMainPacmDesc) {
		this.riskMainPacmDesc = riskMainPacmDesc;
	}
	
    /**
     * @return riskMainPacmDesc
     */
	public String getRiskMainPacmDesc() {
		return this.riskMainPacmDesc;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param lmtNeedPact
	 */
	public void setLmtNeedPact(String lmtNeedPact) {
		this.lmtNeedPact = lmtNeedPact;
	}
	
    /**
     * @return lmtNeedPact
     */
	public String getLmtNeedPact() {
		return this.lmtNeedPact;
	}
	
	/**
	 * @param pdoActCase
	 */
	public void setPdoActCase(String pdoActCase) {
		this.pdoActCase = pdoActCase;
	}
	
    /**
     * @return pdoActCase
     */
	public String getPdoActCase() {
		return this.pdoActCase;
	}
	
	/**
	 * @param creditConditions
	 */
	public void setCreditConditions(String creditConditions) {
		this.creditConditions = creditConditions;
	}
	
    /**
     * @return creditConditions
     */
	public String getCreditConditions() {
		return this.creditConditions;
	}
	
	/**
	 * @param riskControl
	 */
	public void setRiskControl(String riskControl) {
		this.riskControl = riskControl;
	}
	
    /**
     * @return riskControl
     */
	public String getRiskControl() {
		return this.riskControl;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}