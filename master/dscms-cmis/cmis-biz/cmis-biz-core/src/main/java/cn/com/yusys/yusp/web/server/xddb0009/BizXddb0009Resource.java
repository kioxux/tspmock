package cn.com.yusys.yusp.web.server.xddb0009;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0009.req.Xddb0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0009.resp.Xddb0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xddb0008.Xddb0008Service;
import cn.com.yusys.yusp.service.server.xddb0009.Xddb0009Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:双录流水号与押品编号关系维护
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDDB0009:双录流水号与押品编号关系维护")
@RestController
@RequestMapping("/api/bizdb4bsp")
public class BizXddb0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddb0009Resource.class);
    @Autowired
    private Xddb0009Service xddb0009Service;

    /**
     * 交易码：xddb0009
     * 交易描述：双录流水号与押品编号关系维护
     *
     * @param xddb0009DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("双录流水号与押品编号关系维护")
    @PostMapping("/xddb0009")
    protected @ResponseBody
    ResultDto<Xddb0009DataRespDto> xddb0009(@Validated @RequestBody Xddb0009DataReqDto xddb0009DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value, JSON.toJSONString(xddb0009DataReqDto));
        Xddb0009DataRespDto xddb0009DataRespDto = new Xddb0009DataRespDto();// 响应Dto:双录流水号与押品编号关系维护
        ResultDto<Xddb0009DataRespDto> xddb0009DataResultDto = new ResultDto<>();
        try {
            String guarNo = xddb0009DataReqDto.getGuarNo();//抵押物编号
            String imagePk1SerNo = xddb0009DataReqDto.getImagePk1SerNo();//影像主键流水号
            String tranType = xddb0009DataReqDto.getTranType();//交易类型
            // 从xddb0009DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始
            xddb0009DataRespDto=xddb0009Service.xddb0009(xddb0009DataReqDto);
            // TODO 调用XXXXXService层结束
            // TODO 封装xddb0009DataRespDto对象开始
//            xddb0009DataRespDto.setGuarNo(StringUtils.EMPTY);// 抵押物编号
//            xddb0009DataRespDto.setImagePk1SerNo(StringUtils.EMPTY);// 影像主键流水号
            // TODO 封装xddb0009DataRespDto对象结束
            // 封装xddb0009DataResultDto中正确的返回码和返回信息
            xddb0009DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0009DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value, e.getMessage());
            // 封装xddb0009DataResultDto中异常返回码和返回信息
            // TODO EpbEnum.EPB099999 待调整 开始
            xddb0009DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddb0009DataResultDto.setMessage(EpbEnum.EPB099999.value);
            // TODO EpbEnum.EPB099999 待调整  结束
        }
        // 封装xddb0009DataRespDto到xddb0009DataResultDto中
        xddb0009DataResultDto.setData(xddb0009DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value, JSON.toJSONString(xddb0009DataRespDto));
        return xddb0009DataResultDto;
    }
}
