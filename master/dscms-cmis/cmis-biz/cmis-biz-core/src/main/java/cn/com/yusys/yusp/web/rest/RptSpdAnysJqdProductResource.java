/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysJqdProduct;
import cn.com.yusys.yusp.service.RptSpdAnysJqdProductService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJqdProductResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: ZRC
 * @创建时间: 2021-06-30 19:39:40
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanysjqdproduct")
public class RptSpdAnysJqdProductResource {
    @Autowired
    private RptSpdAnysJqdProductService rptSpdAnysJqdProductService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysJqdProduct>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysJqdProduct> list = rptSpdAnysJqdProductService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysJqdProduct>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysJqdProduct>> index(QueryModel queryModel) {
        List<RptSpdAnysJqdProduct> list = rptSpdAnysJqdProductService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysJqdProduct>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<RptSpdAnysJqdProduct> show(@PathVariable("pkId") String pkId) {
        RptSpdAnysJqdProduct rptSpdAnysJqdProduct = rptSpdAnysJqdProductService.selectByPrimaryKey(pkId);
        return new ResultDto<RptSpdAnysJqdProduct>(rptSpdAnysJqdProduct);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysJqdProduct> create(@RequestBody RptSpdAnysJqdProduct rptSpdAnysJqdProduct) throws URISyntaxException {
        rptSpdAnysJqdProductService.insert(rptSpdAnysJqdProduct);
        return new ResultDto<RptSpdAnysJqdProduct>(rptSpdAnysJqdProduct);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysJqdProduct rptSpdAnysJqdProduct) throws URISyntaxException {
        int result = rptSpdAnysJqdProductService.update(rptSpdAnysJqdProduct);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = rptSpdAnysJqdProductService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysJqdProductService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    @PostMapping("/selectByModel")
    protected ResultDto<List<RptSpdAnysJqdProduct>> selectByModel(@RequestBody QueryModel model) {
        return new ResultDto<List<RptSpdAnysJqdProduct>>(rptSpdAnysJqdProductService.selectByModel(model));
    }

    @PostMapping("/insertProduct")
    protected ResultDto<Integer> insertProduct(@RequestBody RptSpdAnysJqdProduct rptSpdAnysJqdProduct) {
        rptSpdAnysJqdProduct.setPkId(UUID.randomUUID().toString());
        return new ResultDto<Integer>(rptSpdAnysJqdProductService.insertSelective(rptSpdAnysJqdProduct));
    }

    @PostMapping("/updateProduct")
    protected ResultDto<Integer> updateProduct(@RequestBody RptSpdAnysJqdProduct rptSpdAnysJqdProduct) {
        return new ResultDto<Integer>(rptSpdAnysJqdProductService.updateSelective(rptSpdAnysJqdProduct));
    }

    @PostMapping("/deleteProduct")
    protected ResultDto<Integer> delete(@RequestBody RptSpdAnysJqdProduct rptSpdAnysJqdProduct) {
        String pkId = rptSpdAnysJqdProduct.getPkId();
        return new ResultDto<Integer>(rptSpdAnysJqdProductService.deleteByPrimaryKey(pkId));
    }
}
