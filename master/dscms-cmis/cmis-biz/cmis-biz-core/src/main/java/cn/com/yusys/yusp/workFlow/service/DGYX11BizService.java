package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.domain.GuarGcfApp;
import cn.com.yusys.yusp.domain.GuarGcfBook;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.*;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author zhangliang15
 * @version 1.0.0
 * @date
 * @desc 省心快贷线上提款查封查验流程
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class DGYX11BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(DGYX11BizService.class);//定义log


    @Autowired
    private GuarGcfAppService guarGcfAppService;
    @Autowired
    private GuarGcfBookService guarGcfBookService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String currentOpType = resultInstanceDto.getCurrentOpType();
        String serno = resultInstanceDto.getBizId();
        // 审批人审批机构
        String currentUserId = resultInstanceDto.getCurrentUserId();
        String currentOrgId = resultInstanceDto.getCurrentOrgId();
        String bizType = resultInstanceDto.getBizType();
        // 省心快贷押品查询查封
        if ("YX019".equals(bizType)){
            guarGcfAppBizApp(resultInstanceDto, currentOpType, serno, currentUserId, currentOrgId);
        }
        else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * @param instanceInfo, currentOpType, iqpSerno, currentUserId, currentOrgId
     * @return void
     * @author zhangliang15
     * @date 2021/6/21 21:38
     * @version 1.0.0
     * @desc 省心快贷押品查询查封
     * @修改历史: 修改时间    修改人员    修改原因
     */
    private void guarGcfAppBizApp(ResultInstanceDto instanceInfo, String currentOpType, String serno, String currentUserId, String currentOrgId) {
        log.info("后业务处理类型:" + currentOpType);
        try {
            GuarGcfApp guarGcfApp = guarGcfAppService.selectByPrimaryKey(serno);
            log.info("省心快贷押品查询查封:"+serno+"流程操作:"+currentOpType+"业务处理");
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("省心快贷押品查询查封:"+serno+"流程流转,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 111--审批中
                updateApproveStatus(guarGcfApp, CmisCommonConstants.WF_STATUS_111);
            }else if(OpType.RETURN_BACK.equals(currentOpType)){//流程退回
                log.info("省心快贷押品查询查封:"+serno+"流程退回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(guarGcfApp, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.CALL_BACK.equals(currentOpType)){//流程打回
                log.info("省心快贷押品查询查封:"+serno+"流程打回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 992--打回
                updateApproveStatus(guarGcfApp, CmisCommonConstants.WF_STATUS_992);
            }else if(OpType.TACK_BACK.equals(currentOpType)){//流程拿回
                log.info("省心快贷押品查询查封:"+serno+"流程拿回,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(guarGcfApp, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.TACK_BACK_FIRST.equals(currentOpType)){//流程拿回到初始节点
                log.info("省心快贷押品查询查封:"+serno+"流程拿回到初始节点,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 991--追回
                updateApproveStatus(guarGcfApp, CmisCommonConstants.WF_STATUS_991);
            }else if(OpType.END.equals(currentOpType)){//流程审批通过
                log.info("省心快贷押品查询查封:"+serno+"流程审批通过,参数:"+instanceInfo.toString());
                //申请表审批状态更新为 997--通过
                updateApproveStatus(guarGcfApp, CmisCommonConstants.WF_STATUS_997);
                //审批通过业务处理
                // 省心快贷查封登记簿落表
                // 获取营业日期
                String openday = stringRedisTemplate.opsForValue().get("openDay");
                GuarGcfBook guarGcfBook = new GuarGcfBook();
                guarGcfBook.setSerno(guarGcfApp.getPkId());//主键
                guarGcfBook.setCusId(guarGcfApp.getCusId());//客户号
                guarGcfBook.setContNo(guarGcfApp.getContNo());//合同号
                guarGcfBook.setAuditDate(openday);//审核日期
                guarGcfBook.setManagerId(guarGcfApp.getManagerId());//操作人
                guarGcfBook.setApprId(currentUserId);//审批人
                guarGcfBook.setUpdateTime(DateUtils.getCurrTimestamp());
                guarGcfBook.setCreateTime(DateUtils.getCurrTimestamp());
                guarGcfBookService.insert(guarGcfBook);

            }else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("省心快贷押品查询查封:"+serno+"流程否决，参数："+ instanceInfo.toString());
                //针对任一节点进行否决操作，业务主表的申请状态更新为【否决-998】
                updateApproveStatus(guarGcfApp, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("省心快贷押品查询查封:"+serno+"未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    /***
     * 流程审批状态更新
     * r 申请信息
     * approveStatus 审批状态
     * */
    public void updateApproveStatus (GuarGcfApp guarGcfApp, String approveStatus){
        guarGcfApp.setApproveStatus(approveStatus);
        guarGcfAppService.updateSelective(guarGcfApp);
    }

    /**
     * 判断当前流程类型是否匹配
     *
     * @param resultInstanceDto
     * @return
     */
    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
        String flowCode = resultInstanceDto.getFlowCode();
        return (CmisFlowConstants.DGYX11).equals(flowCode);
    }
}
