package cn.com.yusys.yusp.service.server.xdtz0021;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.BeanUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.domain.CtrHighAmtAgrCont;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.CfgSorgFinaDto;
import cn.com.yusys.yusp.dto.CusAccountInfoDto;
import cn.com.yusys.yusp.dto.server.xdtz0021.req.Xdtz0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0021.resp.Xdtz0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.CtrHighAmtAgrContMapper;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.PvpLoanAppMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.QueryMap;
import org.apache.commons.lang.StringUtils;
import org.checkerframework.checker.units.qual.A;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.management.Query;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.*;

/**
 * 接口处理类:台账入账（贸易融资\福费廷）
 ** 场景：国结系统推送贸易融资台账操作交易：01-登记贸易融资台账；02-贸易融资台账冲正；
 *  * 接口说明：
 *  * op_flag='01'时，登记贸易融资台账：
 *  * 	1）、根据接口参数【合同编号cont_no】查询CTR_LOAN_CONT表获取合同信息；
 *  * 	2）、将获取的合同信息、接口入参值插入贷款台账ACC_LOAN;
 *  * 	3）、贷款台账生成成功后同时更新其五级分类、十级分类状态
 *  * op_flag='02'时，作废贸易融资台账:
 *  * 	根据接口参数【借据编号bill_no】删除acc_loan
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0021Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0021Service.class);
    /**
     * 1.操作类型'01'：将传入参数保存至贷款台账表中，保存成功后修改五十级分类。
     * 2.操作类型'02'：通过借据号删除此条贷款台账信息。
     * 3./************修改五十级分类******
     * */

    @Autowired
    private AccLoanService accLoanService;


    @Autowired
    private CmisPspClientService cmisPspClientService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private PvpAuthorizeService pvpAuthorizeService;

    @Autowired
    private PvpLoanAppMapper pvpLoanAppMapper;

    @Autowired
    private CtrHighAmtAgrContMapper ctrHighAmtAgrContMapper;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;
    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0021DataRespDto xdtz0021(Xdtz0021DataReqDto xdtz0021DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, JSON.toJSONString(xdtz0021DataReqDto));
        Xdtz0021DataRespDto xdht0021DataRespDto = new Xdtz0021DataRespDto();
        try {
            AccLoan accLoan = BeanUtils.beanCopy(xdtz0021DataReqDto, AccLoan.class);
            String operType = xdtz0021DataReqDto.getOprtype();
            if ("01".equals(operType)) {
                //查询借据是否已存在
                        long start = System.currentTimeMillis();
                AccLoan accLoan1 = accLoanService.selectByBillNo(accLoan.getBillNo());
                        long end = System.currentTimeMillis();
                        logger.info("Xdtz0021Service : selectByBillNo, cost " + (end - start) + " ms");
                if (accLoan1 == null) {
                            start = System.currentTimeMillis();
                    CtrLoanCont ctrLoanCont = ctrLoanContMapper.selectByPrimaryKey(xdtz0021DataReqDto.getContNo());
                            end = System.currentTimeMillis();
                        logger.info("Xdtz0021Service : selectByPrimaryKey, cost " + (end - start) + " ms");
                    if (Objects.nonNull(ctrLoanCont)) {
                        accLoan.setGuarMode(ctrLoanCont.getGuarWay());
                        accLoan.setManagerId(ctrLoanCont.getManagerId());
                        accLoan.setManagerBrId(ctrLoanCont.getManagerBrId());
                        accLoan.setInputId(ctrLoanCont.getInputId());
                        accLoan.setInputBrId(ctrLoanCont.getInputBrId());
                        accLoan.setReplyNo(ctrLoanCont.getReplyNo());
                        accLoan.setLmtAccNo(ctrLoanCont.getLmtAccNo());
                    } else {
                            start = System.currentTimeMillis();
                        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContMapper.selectDataByContNo(xdtz0021DataReqDto.getContNo());
                            end = System.currentTimeMillis();
                            logger.info("Xdtz0021Service : selectDataByContNo, cost " + (end - start) + " ms");
                        if (Objects.nonNull(ctrHighAmtAgrCont)) {
                            accLoan.setGuarMode(ctrHighAmtAgrCont.getGuarMode());
                            accLoan.setManagerId(ctrHighAmtAgrCont.getManagerId());
                            accLoan.setManagerBrId(ctrHighAmtAgrCont.getManagerBrId());
                            accLoan.setInputId(ctrHighAmtAgrCont.getInputId());
                            accLoan.setInputBrId(ctrHighAmtAgrCont.getInputBrId());
                            accLoan.setReplyNo(ctrHighAmtAgrCont.getReplyNo());
                            accLoan.setLmtAccNo(ctrHighAmtAgrCont.getLmtAccNo());
                        }
                    }
                    //生成新借据
                    accLoan.setIsUtilLmt("1");
                    accLoan.setSubjectNo(xdtz0021DataReqDto.getSubjectNo());//科目号
                    accLoan.setExecRateYear(xdtz0021DataReqDto.getRealityIrY());//执行利率年
                    if (Objects.nonNull(xdtz0021DataReqDto.getRateFloat())) {
                        accLoan.setRateFloatPoint(xdtz0021DataReqDto.getRateFloat());//利率浮动值
                        accLoan.setRateAdjMode("02");//利率调整方式
                    } else {
                        accLoan.setRateAdjMode("01");//利率调整方式
                    }
                    accLoan.setIsSegInterest("0");//是否分段计息
//                    accLoan.setLprRateIntval("A1");//LRP区间
                    accLoan.setOverdueExecRate(xdtz0021DataReqDto.getRealityIrY().multiply(new BigDecimal("1.5")));//逾期执行利率(年利率)
                    accLoan.setOverdueRatePefloat(new BigDecimal("0.5"));//逾期利率浮动比
                    accLoan.setCiExecRate(xdtz0021DataReqDto.getRealityIrY().multiply(new BigDecimal("1.5")));//复息执行利率(年利率)
                    accLoan.setCiRatePefloat(new BigDecimal("0.5"));//复息利率浮动比
                    accLoan.setExchangeRate(xdtz0021DataReqDto.getCretQuotationExchgRate().divide(new BigDecimal(100), 6, BigDecimal.ROUND_HALF_UP));
                        start = System.currentTimeMillis();
                    String pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
                        end = System.currentTimeMillis();
                        System.out.println("Xdtz0021Service : getSequenceTemplate, cost " + (end - start) + " ms");
                    accLoan.setPkId(UUID.randomUUID().toString());
                    accLoan.setOprType("01");//操作类型 新增
                    accLoan.setPvpSerno(pvpSerno);
                    accLoan.setContNo(xdtz0021DataReqDto.getContNo());
                    accLoan.setBillNo(accLoan.getBillNo());
                    accLoan.setPrdName(xdtz0021DataReqDto.getPrdName());
                    accLoan.setCusId(xdtz0021DataReqDto.getCusId());
                    accLoan.setCusName(xdtz0021DataReqDto.getCusName());
                    accLoan.setLoanAmt(xdtz0021DataReqDto.getLoanAmt());
                    accLoan.setContCurType(xdtz0021DataReqDto.getCurType());
                    accLoan.setLoanBalance(xdtz0021DataReqDto.getLoanBalance());
                    accLoan.setLoanStartDate(xdtz0021DataReqDto.getLoanStartDate());
                    accLoan.setLoanEndDate(xdtz0021DataReqDto.getLoanEndDate());
                    if (StringUtils.isNotBlank(xdtz0021DataReqDto.getLoanStartDate())
                            && StringUtils.isNotBlank(xdtz0021DataReqDto.getLoanEndDate())) {
                        LocalDate startDate = LocalDate.parse(xdtz0021DataReqDto.getLoanStartDate());
                        LocalDate endDate = LocalDate.parse(xdtz0021DataReqDto.getLoanEndDate());
                        Period p = Period.between(startDate, endDate);
                        accLoan.setLoanTerm(String.valueOf(p.getYears() * 12 + p.getMonths()));
                        accLoan.setLoanTermUnit("M");
                    }
                    accLoan.setAccStatus("1"); // 国结推送台账默认生效状态 新信贷默认正常
                    accLoan.setLoanModal("1");//新增
                    accLoan.setExchangeRmbAmt(xdtz0021DataReqDto.getLoanAmt().multiply(accLoan.getExchangeRate()));//折合人民币金额
                    accLoan.setExchangeRmbBal(accLoan.getExchangeRmbAmt());//折合人民币余额
                    accLoan.setZcbjAmt(xdtz0021DataReqDto.getLoanAmt());//正常本金
                    accLoan.setOverdueCapAmt(BigDecimal.ZERO);//逾期本金
                    accLoan.setDebitInt(BigDecimal.ZERO);//欠息
                    accLoan.setPenalInt(BigDecimal.ZERO);//罚息
                    accLoan.setCompoundInt(BigDecimal.ZERO);//复息
                    accLoan.setExtTimes("0");//展期次数
                    accLoan.setOverdueDay(0);//逾期天数
                    accLoan.setOverdueTimes("0");//逾期期数
                    accLoan.setFinaBrId(xdtz0021DataReqDto.getFinaBrId());
                    QueryModel queryModel = new QueryModel();
                    queryModel.addCondition("finaBrNo", accLoan.getFinaBrId());
                    logger.info("根据机构编号获取机构名称开始，参数为：{}", accLoan.getFinaBrId());
                    ResultDto<List<CfgSorgFinaDto>> listResultDto = iCmisCfgClientService.selecSorgFina(queryModel);
                    logger.info("根据机构编号获取机构名称结束，返回为：{}", JSON.toJSONString(listResultDto));
                    if (Objects.nonNull(listResultDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, listResultDto.getCode())
                            && CollectionUtils.nonEmpty(listResultDto.getData())) {
                        CfgSorgFinaDto cfgSorgFinaDto = JSONObject.parseObject(JSON.toJSONString(listResultDto.getData().get(0)), CfgSorgFinaDto.class);
                        accLoan.setFinaBrIdName(cfgSorgFinaDto.getFinaBrName());
                    }
                    String openDay = stringRedisTemplate.opsForValue().get("openDay");
                    accLoan.setInputDate(openDay);//登记日期
                    accLoan.setUpdDate(openDay);//修改日期
                    Date date = new Date();
                    accLoan.setCreateTime(date);//创建时间
                    accLoan.setUpdateTime(date);//更新时间
                    ResultDto<List<CusAccountInfoDto>> cusDto = cmisCusClientService.queryAccount(xdtz0021DataReqDto.getCusId());
                    if (Objects.nonNull(cusDto) && Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cusDto.getCode()) && CollectionUtils.nonEmpty(cusDto.getData())) {
                        String s = JSON.toJSONString(cusDto.getData().get(0));
                        CusAccountInfoDto cusAccountInfoDto = JSON.parseObject(s, CusAccountInfoDto.class);
                        accLoan.setLoanPayoutAccno(cusAccountInfoDto.getSettlAcctNum());//贷款发放账号
                        accLoan.setLoanPayoutSubNo(cusAccountInfoDto.getAcctSubNum());//贷款发放账号子序号
                        accLoan.setPayoutAcctName(cusAccountInfoDto.getAcctName());//发放账号名称
                        accLoan.setRepayAccno(cusAccountInfoDto.getSettlAcctNum());//贷款还款账号
                        accLoan.setRepaySubAccno(cusAccountInfoDto.getAcctSubNum());//贷款还款账户子序号
                        accLoan.setRepayAcctName(cusAccountInfoDto.getAcctName());//还款账户名称
                    }
                    Map map = pvpLoanAppService.getFiveAndTenClass(xdtz0021DataReqDto.getCusId());
                    accLoan.setFiveClass((String) map.get("fiveClass"));
                    accLoan.setTenClass("11");//十级分类
                    accLoan.setClassDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//分类时间
                    accLoan.setBelgLine("03");//所属条线
                    int count = accLoanService.insert(accLoan);

                }
            } else if ("02".equals(operType)) {
                //根据结局号删除借据信息
                accLoanService.deleteByBillNo(accLoan.getBillNo());
            }
            xdht0021DataRespDto.setOpFlag(DscmsBizTzEnum.SUCCSEE.key);
            xdht0021DataRespDto.setOpMsg("操作成功");
        }catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        return xdht0021DataRespDto;
    }
}
