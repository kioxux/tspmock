package cn.com.yusys.yusp.service.client.bsp.rircp.fbxw03;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.Optional;

/**
 * @author hubp
 * @version 1.0.0
 * @date 2021/5/20 10:13
 * @desc    增享贷提交申请至风控
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class Fbxw03Service {
    private static final Logger logger = LoggerFactory.getLogger(Fbxw03Service.class);

    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    /**
     * @param fbxw03ReqDto
     * @return cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03RespDto
     * @author hubp
     * @date 2021/5/20 10:17
     * @version 1.0.0
     * @desc    增享贷提交申请至风控
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @Transactional
    public Fbxw03RespDto fbxw03(Fbxw03ReqDto fbxw03ReqDto) throws YuspException {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value);
        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value, JSON.toJSONString(fbxw03ReqDto));
        ResultDto<Fbxw03RespDto> fbxw03ResultDto = dscms2RircpClientService.fbxw03(fbxw03ReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value, JSON.toJSONString(fbxw03ResultDto));
        String fbxw03Code = Optional.ofNullable(fbxw03ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String fbxw03Meesage = Optional.ofNullable(fbxw03ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Fbxw03RespDto fbxw03RespDto = null;
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, fbxw03ResultDto.getCode())) {
            //  获取相关的值并解析
            fbxw03RespDto = fbxw03ResultDto.getData();
        } else {
            //  抛出错误异常
            throw new YuspException(fbxw03Code, fbxw03Meesage);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value);
        return fbxw03RespDto;
    }

}
