package cn.com.yusys.yusp.vo;

import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelCsv;
import cn.com.yusys.yusp.commons.excelcsv.annotation.ExcelField;

import java.math.BigDecimal;

@ExcelCsv(namePrefix = "开证台账列表导出", fileType = ExcelCsv.ExportFileType.XLS)
public class AccTfLocVo {

    /*
     借据编号
      */
    @ExcelField(title = "借据编号", viewLength = 20)
    private String billNo;

    /*
   合同编号
    */
    @ExcelField(title = "合同编号", viewLength = 20)
    private String contNo;

    /*
  信用证编号
   */
    @ExcelField(title = "信用证编号", viewLength = 20)
    private String creditNo;

    /*
    客户编号
     */
    @ExcelField(title = "客户编号", viewLength = 20)
    private String cusId;

    /*
    客户名称
     */
    @ExcelField(title = "客户名称", viewLength = 20)
    private String cusName;


    /*
    担保方式
     */
    @ExcelField(title = "担保方式", viewLength = 20 ,dictCode = "STD_ZB_GUAR_WAY")
    private String guarMode;

    /*
    产品名称
     */
    @ExcelField(title = "产品名称", viewLength = 20)
    private String prdName;

    /*
    币种
     */
    @ExcelField(title = "币种", viewLength = 20 ,dictCode = "STD_ZB_CUR_TYP")
    private String curType;

    /*
    开证金额
     */
    @ExcelField(title = "开证金额", viewLength = 20 ,format = "#0.00")
    private java.math.BigDecimal ocerAmt;

    /*
    起始日期
     */
    @ExcelField(title = "起始日期", viewLength = 20)
    private String startDate;

    /*
    到期日期
     */
    @ExcelField(title = "到期日期", viewLength = 20)
    private String endDate;

    /*
    责任人
     */
    @ExcelField(title = "责任人", viewLength = 20)
    private String managerIdName;

    /*
    责任机构
     */
    @ExcelField(title = "责任机构", viewLength = 20)
    private String managerBrIdName;

    /*
    台账状态
     */
    @ExcelField(title = "台账状态", viewLength = 20 ,dictCode = "STD_ACC_STATUS")
    private String accStatus;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCreditNo() {
        return creditNo;
    }

    public void setCreditNo(String creditNo) {
        this.creditNo = creditNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGuarMode() {
        return guarMode;
    }

    public void setGuarMode(String guarMode) {
        this.guarMode = guarMode;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getOcerAmt() {
        return ocerAmt;
    }

    public void setOcerAmt(BigDecimal ocerAmt) {
        this.ocerAmt = ocerAmt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getManagerIdName() {
        return managerIdName;
    }

    public void setManagerIdName(String managerIdName) {
        this.managerIdName = managerIdName;
    }

    public String getManagerBrIdName() {
        return managerBrIdName;
    }

    public void setManagerBrIdName(String managerBrIdName) {
        this.managerBrIdName = managerBrIdName;
    }

    public String getAccStatus() {
        return accStatus;
    }

    public void setAccStatus(String accStatus) {
        this.accStatus = accStatus;
    }
}
