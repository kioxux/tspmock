/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpCusLsnpInfo
 * @类描述: iqp_cus_lsnp_info数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-31 13:53:36
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_cus_lsnp_info")
public class IqpCusLsnpInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务流水号 **/
	@Column(name = "IQP_SERNO", unique = false, nullable = true, length = 40)
	private String iqpSerno;
	
	/** 数字解读值 **/
	@Column(name = "DIG_INT_VAL", unique = false, nullable = true, length = 10)
	private String digIntVal;
	
	/** 数字解读值风险等级 **/
	@Column(name = "DIG_INT_VAL_RISK_LVL", unique = false, nullable = true, length = 4)
	private String digIntValRiskLvl;
	
	/** 申请评分 **/
	@Column(name = "APP_SCORE", unique = false, nullable = true, length = 10)
	private String appScore;
	
	/** 申请评分风险等级 **/
	@Column(name = "APP_SCORE_RISK_LVL", unique = false, nullable = true, length = 4)
	private String appScoreRiskLvl;
	
	/** 规则风险等级 **/
	@Column(name = "RULE_RISK_LVL", unique = false, nullable = true, length = 4)
	private String ruleRiskLvl;
	
	/** 综合风险等级 **/
	@Column(name = "INTE_RISK_LVL", unique = false, nullable = true, length = 4)
	private String inteRiskLvl;
	
	/** 额度建议 **/
	@Column(name = "LMT_ADVICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAdvice;
	
	/** 定价建议 **/
	@Column(name = "PRICE_ADVICE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal priceAdvice;
	
	/** 客户等级 **/
	@Column(name = "CUS_LVL", unique = false, nullable = true, length = 5)
	private String cusLvl;
	
	/** 申请卡产品 **/
	@Column(name = "APPLY_CARD_PRD", unique = false, nullable = true, length = 6)
	private String applyCardPrd;
	
	/** 公积金缴存基数 **/
	@Column(name = "PUND_DEPOSIT_BASE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal pundDepositBase;
	
	/** AUM **/
	@Column(name = "AUM", unique = false, nullable = true, length = 20)
	private String aum;
	
	/** 代发工资 **/
	@Column(name = "PAYROLL_CREDIT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal payrollCredit;
	
	/** 我行房贷授信金额 **/
	@Column(name = "LOAN_CREDIR_AMT_BANK", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanCredirAmtBank;
	
	/** 他行房贷授信金额 **/
	@Column(name = "LOAN_CREDIR_AMT_OTHER_BANK", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal loanCredirAmtOtherBank;
	
	/** 消费贷款累计金额 **/
	@Column(name = "CONSUMER_LOAN_BAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal consumerLoanBalAmt;
	
	/** 日费率 **/
	@Column(name = "DAILY_FEE_RATE", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal dailyFeeRate;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;

	/** 自动化审批标志 **/
	@Column(name = "LIMIT_FLAG", unique = false, nullable = true, length = 5)
	private String limitFlag;

	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param digIntVal
	 */
	public void setDigIntVal(String digIntVal) {
		this.digIntVal = digIntVal;
	}
	
    /**
     * @return digIntVal
     */
	public String getDigIntVal() {
		return this.digIntVal;
	}
	
	/**
	 * @param digIntValRiskLvl
	 */
	public void setDigIntValRiskLvl(String digIntValRiskLvl) {
		this.digIntValRiskLvl = digIntValRiskLvl;
	}
	
    /**
     * @return digIntValRiskLvl
     */
	public String getDigIntValRiskLvl() {
		return this.digIntValRiskLvl;
	}
	
	/**
	 * @param appScore
	 */
	public void setAppScore(String appScore) {
		this.appScore = appScore;
	}
	
    /**
     * @return appScore
     */
	public String getAppScore() {
		return this.appScore;
	}
	
	/**
	 * @param appScoreRiskLvl
	 */
	public void setAppScoreRiskLvl(String appScoreRiskLvl) {
		this.appScoreRiskLvl = appScoreRiskLvl;
	}
	
    /**
     * @return appScoreRiskLvl
     */
	public String getAppScoreRiskLvl() {
		return this.appScoreRiskLvl;
	}
	
	/**
	 * @param ruleRiskLvl
	 */
	public void setRuleRiskLvl(String ruleRiskLvl) {
		this.ruleRiskLvl = ruleRiskLvl;
	}
	
    /**
     * @return ruleRiskLvl
     */
	public String getRuleRiskLvl() {
		return this.ruleRiskLvl;
	}
	
	/**
	 * @param inteRiskLvl
	 */
	public void setInteRiskLvl(String inteRiskLvl) {
		this.inteRiskLvl = inteRiskLvl;
	}
	
    /**
     * @return inteRiskLvl
     */
	public String getInteRiskLvl() {
		return this.inteRiskLvl;
	}
	
	/**
	 * @param lmtAdvice
	 */
	public void setLmtAdvice(java.math.BigDecimal lmtAdvice) {
		this.lmtAdvice = lmtAdvice;
	}
	
    /**
     * @return lmtAdvice
     */
	public java.math.BigDecimal getLmtAdvice() {
		return this.lmtAdvice;
	}
	
	/**
	 * @param priceAdvice
	 */
	public void setPriceAdvice(java.math.BigDecimal priceAdvice) {
		this.priceAdvice = priceAdvice;
	}
	
    /**
     * @return priceAdvice
     */
	public java.math.BigDecimal getPriceAdvice() {
		return this.priceAdvice;
	}
	
	/**
	 * @param cusLvl
	 */
	public void setCusLvl(String cusLvl) {
		this.cusLvl = cusLvl;
	}
	
    /**
     * @return cusLvl
     */
	public String getCusLvl() {
		return this.cusLvl;
	}
	
	/**
	 * @param applyCardPrd
	 */
	public void setApplyCardPrd(String applyCardPrd) {
		this.applyCardPrd = applyCardPrd;
	}
	
    /**
     * @return applyCardPrd
     */
	public String getApplyCardPrd() {
		return this.applyCardPrd;
	}
	
	/**
	 * @param pundDepositBase
	 */
	public void setPundDepositBase(java.math.BigDecimal pundDepositBase) {
		this.pundDepositBase = pundDepositBase;
	}
	
    /**
     * @return pundDepositBase
     */
	public java.math.BigDecimal getPundDepositBase() {
		return this.pundDepositBase;
	}
	
	/**
	 * @param aum
	 */
	public void setAum(String aum) {
		this.aum = aum;
	}
	
    /**
     * @return aum
     */
	public String getAum() {
		return this.aum;
	}
	
	/**
	 * @param payrollCredit
	 */
	public void setPayrollCredit(java.math.BigDecimal payrollCredit) {
		this.payrollCredit = payrollCredit;
	}
	
    /**
     * @return payrollCredit
     */
	public java.math.BigDecimal getPayrollCredit() {
		return this.payrollCredit;
	}
	
	/**
	 * @param loanCredirAmtBank
	 */
	public void setLoanCredirAmtBank(java.math.BigDecimal loanCredirAmtBank) {
		this.loanCredirAmtBank = loanCredirAmtBank;
	}
	
    /**
     * @return loanCredirAmtBank
     */
	public java.math.BigDecimal getLoanCredirAmtBank() {
		return this.loanCredirAmtBank;
	}
	
	/**
	 * @param loanCredirAmtOtherBank
	 */
	public void setLoanCredirAmtOtherBank(java.math.BigDecimal loanCredirAmtOtherBank) {
		this.loanCredirAmtOtherBank = loanCredirAmtOtherBank;
	}
	
    /**
     * @return loanCredirAmtOtherBank
     */
	public java.math.BigDecimal getLoanCredirAmtOtherBank() {
		return this.loanCredirAmtOtherBank;
	}
	
	/**
	 * @param consumerLoanBalAmt
	 */
	public void setConsumerLoanBalAmt(java.math.BigDecimal consumerLoanBalAmt) {
		this.consumerLoanBalAmt = consumerLoanBalAmt;
	}
	
    /**
     * @return consumerLoanBalAmt
     */
	public java.math.BigDecimal getConsumerLoanBalAmt() {
		return this.consumerLoanBalAmt;
	}
	
	/**
	 * @param dailyFeeRate
	 */
	public void setDailyFeeRate(java.math.BigDecimal dailyFeeRate) {
		this.dailyFeeRate = dailyFeeRate;
	}
	
    /**
     * @return dailyFeeRate
     */
	public java.math.BigDecimal getDailyFeeRate() {
		return this.dailyFeeRate;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getLimitFlag() {
		return limitFlag;
	}

	public void setLimitFlag(String limitFlag) {
		this.limitFlag = limitFlag;
	}


}