package cn.com.yusys.yusp.service.server.xdxw0074;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.AdminSmOrgDto;
import cn.com.yusys.yusp.dto.AdminSmUserDto;
import cn.com.yusys.yusp.dto.server.xdxw0074.req.Xdxw0074DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0074.resp.List;
import cn.com.yusys.yusp.dto.server.xdxw0074.resp.Xdxw0074DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.LmtSurveyReportMainInfoMapper;
import cn.com.yusys.yusp.service.AdminSmOrgService;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.client.common.SmLookupItemService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz模块
 * @类名称: Xdxw0074Service
 * @类描述: #服务类
 * @功能描述:
 * @创建人: ys
 * @创建时间: 2021-06-05 14:03:00
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class Xdxw0074Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0074Service.class);

    @Resource
    private LmtSurveyReportMainInfoMapper lmtSurveyReportMainInfoMapper;

    //用户信息模块
    @Autowired
    private AdminSmUserService adminSmUserService;

    // 机构信息模块
    @Autowired
    private AdminSmOrgService adminSmOrgService;

    // 字典项模块
    @Autowired
    private SmLookupItemService smLookupItemService;

    /**
     * 根据客户身份证号查询线上产品申请记录（在途需求）
     *
     * @param xdxw0074DataReqDto
     * @return xdxw0074DataRespDto
     * @throws Exception
     */
    public Xdxw0074DataRespDto xdxw0074(Xdxw0074DataReqDto xdxw0074DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, JSON.toJSONString(xdxw0074DataReqDto));
        Xdxw0074DataRespDto xdxw0074DataRespDto = new Xdxw0074DataRespDto();
        try {
            if (StringUtils.isEmpty(xdxw0074DataReqDto.getCertNo())) {
                throw BizException.error(null, EcbEnum.ECB010001.key, EcbEnum.ECB010001.value);
            } else {
                // 证件号
                String certCode = xdxw0074DataReqDto.getCertNo();
                String loanStatusName = "";
                String guarTypeMainName = "";
                // 初始化list
                java.util.List<List> applyList = null;
                // 查询基础信息
                logger.info("***********XDXW0074查询基础信息开始,查询参数为:{}", JSON.toJSONString(certCode));
                applyList = lmtSurveyReportMainInfoMapper.selectOnlineProjectApplyList(certCode);
                logger.info("***********XDXW0074查询基础信息结束,返回结果为:{}", JSON.toJSONString(applyList));
                if (applyList != null && applyList.size() > 0) {
                    for (int i = 0; i < applyList.size(); i++) {
                        List listDetail = applyList.get(i);
                        // 翻译客户经理号和机构号并放值
                        logger.info("***********XDXW0074翻译客户经理号和机构号并放值开始,查询参数为:{}", JSON.toJSONString(listDetail.getUserNo()));
                        ResultDto<AdminSmUserDto> adminSmUserDtoResultDto = adminSmUserService.getByLoginCode(listDetail.getUserNo());
                        if (adminSmUserDtoResultDto != null) {
                            AdminSmUserDto adminSmUserDto = adminSmUserDtoResultDto.getData();
                            listDetail.setManagerName(adminSmUserDto.getUserName());
                            ResultDto<cn.com.yusys.yusp.dto.AdminSmOrgDto> adminSmOrgDtoResultDto = adminSmOrgService.getByOrgCode(adminSmUserDto.getOrgId());
                            AdminSmOrgDto adminSmOrgDto = adminSmOrgDtoResultDto.getData();
                            String adminSmOrgCode = adminSmOrgDtoResultDto.getCode();
                            if (StringUtil.isNotEmpty(adminSmOrgCode) && CmisBizConstants.NUM_ZERO.equals(adminSmOrgCode)) {
                                listDetail.setOrgName(adminSmOrgDto.getOrgName());
                            }
                            logger.info("***********XDXW0074翻译客户经理号和机构号并放值结束,返回结果为:{}", JSON.toJSONString(adminSmOrgCode));
                        }
                        //翻译字典项
                        loanStatusName = smLookupItemService.getLookUpItemNameByLookUpCode("STD_ZB_APPR_STATUS", listDetail.getLoanStatusName());
                        listDetail.setLoanStatusName(loanStatusName);
                        guarTypeMainName = smLookupItemService.getLookUpItemNameByLookUpCode("STD_ZB_GUAR_WAY", listDetail.getGuarTypeMainName());
                        listDetail.setGuarTypeMainName(guarTypeMainName);
                    }
                }
                xdxw0074DataRespDto.setList(applyList);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, JSON.toJSONString(xdxw0074DataReqDto));
        return xdxw0074DataRespDto;
    }
}

