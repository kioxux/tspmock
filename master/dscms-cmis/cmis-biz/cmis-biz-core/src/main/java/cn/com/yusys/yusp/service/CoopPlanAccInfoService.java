/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CoopProAccInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.CoopPlanAccInfo;
import cn.com.yusys.yusp.repository.mapper.CoopPlanAccInfoMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPlanAccInfoService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-18 16:26:53
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CoopPlanAccInfoService {

    @Autowired
    private CoopPlanAccInfoMapper coopPlanAccInfoMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public CoopPlanAccInfo selectByPrimaryKey(String pkId) {
        return coopPlanAccInfoMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<CoopPlanAccInfo> selectAll(QueryModel model) {
        List<CoopPlanAccInfo> records = (List<CoopPlanAccInfo>) coopPlanAccInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<CoopPlanAccInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<CoopPlanAccInfo> list = coopPlanAccInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<CoopPlanAccInfo> selectByModel4Widget(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        if (model.getCondition().get("partnerTypes") != null){
            String partnerTypes = model.getCondition().get("partnerTypes").toString();
            List<String> partnerTypeList = Arrays.asList(partnerTypes.split(","));
            model.getCondition().put("partnerTypeList", partnerTypeList);
        }
        List<CoopPlanAccInfo> list = coopPlanAccInfoMapper.selectByModel4Widget(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(CoopPlanAccInfo record) {
        return coopPlanAccInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(CoopPlanAccInfo record) {
        return coopPlanAccInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(CoopPlanAccInfo record) {
        return coopPlanAccInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(CoopPlanAccInfo record) {
        return coopPlanAccInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return coopPlanAccInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return coopPlanAccInfoMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: queryListByPartnerNo
     * @方法描述: 根据合作方编号查询特色关联对公授信
     * @参数与返回说明:
     * @算法描述: 无
     * 2021-05-06
     */

    public List<Map<String, Object>> queryListByPartnerNo(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = coopPlanAccInfoMapper.queryListByPartnerNo(queryModel);
        PageHelper.clearPage();
        return mapList;
    }

    /**
     * @方法名称: queryLmtAppSubGuar
     * @方法描述: 根据合作方编号查询一般担保关联对公授信
     * @参数与返回说明:
     * @算法描述: 无
     * 2021-05-06
     */

    public List<Map<String, Object>> queryLmtAppSubGuar(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = coopPlanAccInfoMapper.queryLmtAppSubGuar(queryModel);
        PageHelper.clearPage();
        return mapList;
    }

    /**
     * @方法名称: byProNo
     * @方法描述: 根据分项编号查询关联零售授信
     * @参数与返回说明:
     * @算法描述: 无
     * 2021-05-06
     */

    public List<Map<String, Object>> byProNo(QueryModel queryModel) {
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<Map<String, Object>> mapList = coopPlanAccInfoMapper.byProNo(queryModel);
        PageHelper.clearPage();
        return mapList;
    }

    /**
     * @方法名称: getSumByLmtAmt
     * @方法描述: 根据合作方编号计算特色关联对公授信授信额度之和
     * @参数与返回说明:
     * @算法描述: 无
     * 2021-05-06
     */
    public Map<String, Object> getSumByLmtAmt(QueryModel queryModel) {
        String sumLmtAmt = coopPlanAccInfoMapper.getSumByLmtAmt(queryModel);
        Map map = new HashMap<>();
        map.put("sumLmtAmt",sumLmtAmt);
        return map;
    }

    /**
     * @方法名称: getSumByLmtAmtGuar
     * @方法描述: 根据合作方编号查询一般担保关联对公授信授信额度之和
     * @参数与返回说明:
     * @算法描述: 无
     * 2021-05-06
     */
    public Map<String, Object> getSumByLmtAmtGuar(QueryModel queryModel) {
        String sumLmtAmtGuar = coopPlanAccInfoMapper.getSumByLmtAmtGuar(queryModel);
        Map map = new HashMap<>();
        map.put("sumLmtAmtGuar",sumLmtAmtGuar);
        return map;
    }

    /**
     * @方法名称: getSumByAppAmt
     * @方法描述: 计算申请额度之和
     * @参数与返回说明:
     * @算法描述: 无
     * 2021-05-06
     */
    public Map<String, Object> getSumByAppAmt(QueryModel queryModel) {
        String sumAppAmt = coopPlanAccInfoMapper.getSumByAppAmt(queryModel);
        Map map = new HashMap<>();
        map.put("sumAppAmt",sumAppAmt);
        return map;
    }

    /**
     * @param coopPlanNo
     * @return cn.com.yusys.yusp.domain.CoopProAccInfo
     * @author hubp
     * @date 2021/9/30 16:45
     * @version 1.0.0
     * @desc    根据合作方案编号查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public CoopPlanAccInfo selectByCoopPlanNo(String coopPlanNo) {
        return coopPlanAccInfoMapper.selectByCoopPlanNo(coopPlanNo);
    }

    public CoopPlanAccInfo selectByPartnerNo(String partnerNo){
        return coopPlanAccInfoMapper.selectByPartnerNo(partnerNo);
    }
}
