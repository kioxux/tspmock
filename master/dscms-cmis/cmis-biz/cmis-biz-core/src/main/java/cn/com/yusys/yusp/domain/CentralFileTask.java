/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.cmis.commons.annonation.RedisCacheTranslator;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CentralFileTask
 * @类描述: central_file_task数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-08 22:12:29
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "central_file_task")
public class CentralFileTask extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 任务编号  **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "TASK_NO")
	private String taskNo;
	
	/** 业务流水号 **/
	@Column(name = "SERNO", unique = false, nullable = false, length = 40)
	private String serno;

	/** 全局流水号 **/
	@Column(name = "TRACE_ID", unique = false, nullable = true, length = 40)
	private String traceId;

	/** 业务类型 **/
	@Column(name = "BIZ_TYPE", unique = false, nullable = true, length = 5)
	private String bizType;
	
	/** 业务类型细分 **/
	@Column(name = "BIZ_SUB_TYPE", unique = false, nullable = true, length = 5)
	private String bizSubType;
	
	/** 任务类型 **/
	@Column(name = "TASK_TYPE", unique = false, nullable = true, length = 5)
	private String taskType;
	
	/** 操作类型STD_OPR_TYPE **/
	@Column(name = "OPT_TYPE", unique = false, nullable = true, length = 5)
	private String optType;
	
	/** 客户编号 **/
	@Column(name = "CUS_ID", unique = false, nullable = true, length = 40)
	private String cusId;
	
	/** 客户名称 **/
	@Column(name = "CUS_NAME", unique = false, nullable = true, length = 100)
	private String cusName;
	
	/** 任务生成时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "TASK_START_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date taskStartTime;
	
	/** 任务加急标识 **/
	@Column(name = "TASK_URGENT_FLAG", unique = false, nullable = true, length = 5)
	private String taskUrgentFlag;
	
	/** 接收人 **/
	@RedisCacheTranslator(redisCacheKey = "userName",refFieldName = "receiverIdName")
	@Column(name = "RECEIVER_ID", unique = false, nullable = true, length = 100)
	private String receiverId;
	
	/** 接收机构 **/
	@RedisCacheTranslator(redisCacheKey = "orgName",refFieldName="receiverOrgName" )
	@Column(name = "RECEIVER_ORG", unique = false, nullable = true, length = 40)
	private String receiverOrg;
	
	/** 任务状态 **/
	@Column(name = "TASK_STATUS", unique = false, nullable = true, length = 5)
	private String taskStatus;
	
	/** 作废原因 **/
	@Column(name = "CANCEL_RESN", unique = false, nullable = true, length = 2000)
	private String cancelResn;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 创建时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	/** 流程实例号 **/
	@Column(name = "INSTANCE_ID", unique = false, nullable = true, length = 32)
	private String instanceId;
	
	/** 流程节点ID **/
	@Column(name = "NODE_ID", unique = false, nullable = true, length = 10)
	private String nodeId;
	
	/** 差错原因 **/
	@Column(name = "ERROR_RESN", unique = false, nullable = true, length = 50)
	private String errorResn;
	
	/** 差错原因说明 **/
	@Column(name = "ERROR_RESN_DESC", unique = false, nullable = true, length = 1000)
	private String errorResnDesc;
	
	
	/**
	 * @param taskNo
	 */
	public void setTaskNo(String taskNo) {
		this.taskNo = taskNo;
	}
	
    /**
     * @return taskNo
     */
	public String getTaskNo() {
		return this.taskNo;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}

    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	/**
	 * @param bizType
	 */
	public void setBizType(String bizType) {
		this.bizType = bizType;
	}
	
    /**
     * @return bizType
     */
	public String getBizType() {
		return this.bizType;
	}
	
	/**
	 * @param bizSubType
	 */
	public void setBizSubType(String bizSubType) {
		this.bizSubType = bizSubType;
	}
	
    /**
     * @return bizSubType
     */
	public String getBizSubType() {
		return this.bizSubType;
	}
	
	/**
	 * @param taskType
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	
    /**
     * @return taskType
     */
	public String getTaskType() {
		return this.taskType;
	}
	
	/**
	 * @param optType
	 */
	public void setOptType(String optType) {
		this.optType = optType;
	}
	
    /**
     * @return optType
     */
	public String getOptType() {
		return this.optType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
    /**
     * @return cusId
     */
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName;
	}
	
    /**
     * @return cusName
     */
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param taskStartTime
	 */
	public void setTaskStartTime(java.util.Date taskStartTime) {
		this.taskStartTime = taskStartTime;
	}
	
    /**
     * @return taskStartTime
     */
	public java.util.Date getTaskStartTime() {
		return this.taskStartTime;
	}
	
	/**
	 * @param taskUrgentFlag
	 */
	public void setTaskUrgentFlag(String taskUrgentFlag) {
		this.taskUrgentFlag = taskUrgentFlag;
	}
	
    /**
     * @return taskUrgentFlag
     */
	public String getTaskUrgentFlag() {
		return this.taskUrgentFlag;
	}
	
	/**
	 * @param receiverId
	 */
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	
    /**
     * @return receiverId
     */
	public String getReceiverId() {
		return this.receiverId;
	}
	
	/**
	 * @param receiverOrg
	 */
	public void setReceiverOrg(String receiverOrg) {
		this.receiverOrg = receiverOrg;
	}
	
    /**
     * @return receiverOrg
     */
	public String getReceiverOrg() {
		return this.receiverOrg;
	}
	
	/**
	 * @param taskStatus
	 */
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	
    /**
     * @return taskStatus
     */
	public String getTaskStatus() {
		return this.taskStatus;
	}
	
	/**
	 * @param cancelResn
	 */
	public void setCancelResn(String cancelResn) {
		this.cancelResn = cancelResn;
	}
	
    /**
     * @return cancelResn
     */
	public String getCancelResn() {
		return this.cancelResn;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param instanceId
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
    /**
     * @return instanceId
     */
	public String getInstanceId() {
		return this.instanceId;
	}
	
	/**
	 * @param nodeId
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
    /**
     * @return nodeId
     */
	public String getNodeId() {
		return this.nodeId;
	}
	
	/**
	 * @param errorResn
	 */
	public void setErrorResn(String errorResn) {
		this.errorResn = errorResn;
	}
	
    /**
     * @return errorResn
     */
	public String getErrorResn() {
		return this.errorResn;
	}
	
	/**
	 * @param errorResnDesc
	 */
	public void setErrorResnDesc(String errorResnDesc) {
		this.errorResnDesc = errorResnDesc;
	}
	
    /**
     * @return errorResnDesc
     */
	public String getErrorResnDesc() {
		return this.errorResnDesc;
	}


}