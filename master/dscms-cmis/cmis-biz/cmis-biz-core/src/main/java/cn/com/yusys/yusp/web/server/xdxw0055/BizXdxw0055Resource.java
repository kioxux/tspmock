package cn.com.yusys.yusp.web.server.xdxw0055;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0055.req.Xdxw0055DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0055.resp.Xdxw0055DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0055.Xdxw0055Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 接口处理类:经营地址查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0055:经营地址查询")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0055Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0055Resource.class);
    @Resource
    private Xdxw0055Service xdxw0055Service;
    /**
     * 交易码：xdxw0055
     * 交易描述：经营地址查询
     *
     * @param xdxw0055DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("经营地址查询")
    @PostMapping("/xdxw0055")
    protected @ResponseBody
    ResultDto<Xdxw0055DataRespDto> xdxw0055(@Validated @RequestBody Xdxw0055DataReqDto xdxw0055DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, JSON.toJSONString(xdxw0055DataReqDto));
        Xdxw0055DataRespDto xdxw0055DataRespDto = new Xdxw0055DataRespDto();// 响应Dto:经营地址查询
        ResultDto<Xdxw0055DataRespDto> xdxw0055DataResultDto = new ResultDto<>();

        // 从xdxw0055DataReqDto获取业务值进行业务逻辑处理
        try {
            String indgtSerno = xdxw0055DataReqDto.getIndgtSerno();//客户调查表编号
            //  调用XXXXXService层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, JSON.toJSONString(xdxw0055DataReqDto));
            xdxw0055DataRespDto = xdxw0055Service.queryaddressBySurveySerno(xdxw0055DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, JSON.toJSONString(xdxw0055DataRespDto));
            // 封装xdxw0055DataResultDto中正确的返回码和返回信息
            xdxw0055DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0055DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, e.getMessage());
            // 封装xdxw0055DataResultDto中异常返回码和返回信息
            xdxw0055DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0055DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0055DataRespDto到xdxw0055DataResultDto中
        xdxw0055DataResultDto.setData(xdxw0055DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, JSON.toJSONString(xdxw0055DataResultDto));
        return xdxw0055DataResultDto;
    }
}
