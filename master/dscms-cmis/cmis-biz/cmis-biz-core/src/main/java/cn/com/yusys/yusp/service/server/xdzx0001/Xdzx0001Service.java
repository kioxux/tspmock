package cn.com.yusys.yusp.service.server.xdzx0001;

import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzx0001.req.Xdzx0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.resp.Xdzx0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CreditQryBizRealMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 接口处理类:查询征信业务流水号
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdzx0001Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdzx0001Service.class);

    @Autowired
    private CreditQryBizRealMapper creditQryBizRealMapper;

    /**
     * 查询征信业务流水号
     * @param xdzx0001DataReqDto
     * @return
     */
    @Transactional
    public Xdzx0001DataRespDto getZxSerno(Xdzx0001DataReqDto xdzx0001DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001DataReqDto));
        Xdzx0001DataRespDto result = creditQryBizRealMapper.getZxSerno(xdzx0001DataReqDto);
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(result));
        return result;
    }
}
