/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.RptSpdAnysWmd;
import cn.com.yusys.yusp.service.RptSpdAnysWmdService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysWmdResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-08-19 22:07:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/rptspdanyswmd")
public class RptSpdAnysWmdResource {
    @Autowired
    private RptSpdAnysWmdService rptSpdAnysWmdService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<RptSpdAnysWmd>> query() {
        QueryModel queryModel = new QueryModel();
        List<RptSpdAnysWmd> list = rptSpdAnysWmdService.selectAll(queryModel);
        return new ResultDto<List<RptSpdAnysWmd>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<RptSpdAnysWmd>> index(QueryModel queryModel) {
        List<RptSpdAnysWmd> list = rptSpdAnysWmdService.selectByModel(queryModel);
        return new ResultDto<List<RptSpdAnysWmd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<RptSpdAnysWmd> show(@PathVariable("serno") String serno) {
        RptSpdAnysWmd rptSpdAnysWmd = rptSpdAnysWmdService.selectByPrimaryKey(serno);
        return new ResultDto<RptSpdAnysWmd>(rptSpdAnysWmd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<RptSpdAnysWmd> create(@RequestBody RptSpdAnysWmd rptSpdAnysWmd) throws URISyntaxException {
        rptSpdAnysWmdService.insert(rptSpdAnysWmd);
        return new ResultDto<RptSpdAnysWmd>(rptSpdAnysWmd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody RptSpdAnysWmd rptSpdAnysWmd) throws URISyntaxException {
        int result = rptSpdAnysWmdService.update(rptSpdAnysWmd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = rptSpdAnysWmdService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = rptSpdAnysWmdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * 根据流水号查询
     * @param serno
     * @return
     */
    @PostMapping("/selectBySerno")
    protected ResultDto<RptSpdAnysWmd> selectBySerno(@RequestBody String serno){
        return new ResultDto<RptSpdAnysWmd>(rptSpdAnysWmdService.selectByPrimaryKey(serno));
    }

    /**
     * 初始化数据
     * @param rptSpdAnysWmd
     * @return
     */
    @PostMapping("/initWmd")
    protected ResultDto<Integer> initWmd(@RequestBody RptSpdAnysWmd rptSpdAnysWmd){
        return new ResultDto<Integer>(rptSpdAnysWmdService.insertSelective(rptSpdAnysWmd));
    }

    /**
     * 自动打分
     * @param params
     * @return
     */
    @PostMapping("/autoValue")
    protected ResultDto<Integer> autoValue(@RequestBody Map params){
        return new ResultDto<Integer>(rptSpdAnysWmdService.autoValue(params));
    }

    /**
     * 保存
     * @param rptSpdAnysWmd
     * @return
     */
    @PostMapping("/save")
    protected ResultDto<Integer> save(@RequestBody RptSpdAnysWmd rptSpdAnysWmd){
        return new ResultDto<Integer>(rptSpdAnysWmdService.updateSelective(rptSpdAnysWmd));
    }
}
