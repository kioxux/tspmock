/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;


import java.math.BigDecimal;
import java.util.List;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDisAssetIncomeDto
 * @类描述: 
 * @功能描述:
 * @创建人: zlf
 * @创建时间: 2021-05-06 20:33:24
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpDisAssetIncomeDto  {
	/** 月收入表主键 **/
	private String incomePk;
	/** 业务申请流水号 **/
	private String iqpSerno;
	/** 借款人月收入 **/
	private java.math.BigDecimal mearn;
	/** 配偶月收入 **/
	private java.math.BigDecimal spouseMearn;
	/** 所有共同借款人月收入小计 **/
	private java.math.BigDecimal commonMearn;
	/** 小计(元) **/
	private java.math.BigDecimal subtotal;
	/** 调查核实情况说明 **/
	private String indgtExpl;
	/** 资产折算收入类型 **/
	private String discountAssetType;
	private List<IqpDisAssetIncome> list1;
	private List<IqpDisAssetIncome> list2;

	@Override
	public String toString() {
		return "IqpDisAssetIncomeDto{" +
				"incomePk='" + incomePk + '\'' +
				", iqpSerno='" + iqpSerno + '\'' +
				", mearn=" + mearn +
				", spouseMearn=" + spouseMearn +
				", commonMearn=" + commonMearn +
				", subtotal=" + subtotal +
				", indgtExpl='" + indgtExpl + '\'' +
				", discountAssetType='" + discountAssetType + '\'' +
				", list1=" + list1 +
				", list2=" + list2 +
				'}';
	}

	public String getIncomePk() {
		return incomePk;
	}

	public void setIncomePk(String incomePk) {
		this.incomePk = incomePk;
	}

	public String getIqpSerno() {
		return iqpSerno;
	}

	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}

	public BigDecimal getMearn() {
		return mearn;
	}

	public void setMearn(BigDecimal mearn) {
		this.mearn = mearn;
	}

	public BigDecimal getSpouseMearn() {
		return spouseMearn;
	}

	public void setSpouseMearn(BigDecimal spouseMearn) {
		this.spouseMearn = spouseMearn;
	}

	public BigDecimal getCommonMearn() {
		return commonMearn;
	}

	public void setCommonMearn(BigDecimal commonMearn) {
		this.commonMearn = commonMearn;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public String getIndgtExpl() {
		return indgtExpl;
	}

	public void setIndgtExpl(String indgtExpl) {
		this.indgtExpl = indgtExpl;
	}

	public String getDiscountAssetType() {
		return discountAssetType;
	}

	public void setDiscountAssetType(String discountAssetType) {
		this.discountAssetType = discountAssetType;
	}

	public List<IqpDisAssetIncome> getList1() {
		return list1;
	}

	public void setList1(List<IqpDisAssetIncome> list1) {
		this.list1 = list1;
	}

	public List<IqpDisAssetIncome> getList2() {
		return list2;
	}

	public void setList2(List<IqpDisAssetIncome> list2) {
		this.list2 = list2;
	}
}