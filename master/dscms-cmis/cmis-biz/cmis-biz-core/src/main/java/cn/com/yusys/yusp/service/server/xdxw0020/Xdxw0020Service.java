package cn.com.yusys.yusp.service.server.xdxw0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.LmtCrdReplyInfo;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ApprSubListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.req.CmisLmt0008ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0008.resp.CmisLmt0008RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0020.req.Xdxw0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0020.resp.Xdxw0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.LmtCrdReplyInfoMapper;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class Xdxw0020Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0020Service.class);

    @Autowired
    private LmtCrdReplyInfoMapper lmtCrdReplyInfoMapper;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdxw0020DataRespDto xdxw0020(Xdxw0020DataReqDto xdxw0020DataReqDto) throws Exception {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, JSON.toJSONString(xdxw0020DataReqDto));
        Xdxw0020DataRespDto xdxw0020DataRespDto = new Xdxw0020DataRespDto();
        // 返回撤销状态 01-撤销成功；02-撤销失败
        String cancelStatus = "";
        try {
            // 业务流水号
            String serno = xdxw0020DataReqDto.getSerno();
            // 客户号
            String cusId = xdxw0020DataReqDto.getCusId();
            // 客户姓名
            String cusName = xdxw0020DataReqDto.getCusName();
            // 处理新信贷数据 为无效
            lmtCrdReplyInfoMapper.updateByCusIdSurveySeqNo(cusId, serno);
            // 取出台账编号
            logger.info("*****XDXW0020**取出台账编号开始,查询参数为:{}", JSON.toJSONString(cusId));
            List<String> replySerno = lmtCrdReplyInfoMapper.selectReplySernoByCusIdSurveySeqNo(cusId, serno);
            logger.info("*****XDXW0020**取出台账编号结束,返回参数为:{}", JSON.toJSONString(replySerno));
            // 调额度系统 撤销额度
            CmisLmt0008ReqDto cmisLmt0008ReqDto = new CmisLmt0008ReqDto();
            // 系统编号
            cmisLmt0008ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);
            List<CmisLmt0008ApprListReqDto> apprList = new ArrayList<>();
            List<CmisLmt0008ApprSubListReqDto> apprSubList = new ArrayList<>();
            for (String sernoDetail : replySerno) {
                //取出授信调查编号
                logger.info("*****XDXW0020**取出授信调查编号开始,查询参数为:{}", JSON.toJSONString(sernoDetail));
                LmtCrdReplyInfo lmtCrdReplyInfo = lmtCrdReplyInfoMapper.selectByPrimaryKey(sernoDetail);
                logger.info("*****XDXW0020**取出授信调查编号结束,返回参数为:{}", JSON.toJSONString(lmtCrdReplyInfo));
                //查询批复下是否存在有效合同
                int count = ctrLoanContMapper.getContInfobyreplySerno(lmtCrdReplyInfo.getSurveySerno());
                logger.info("*****XDXW0020**查询批复下是否存在有效合同为:{}", JSON.toJSONString(count));
                if (count > 0) {
                    cancelStatus = "03";//该批复已存在有效合同，不可撤销
                    xdxw0020DataRespDto.setCancelStatus(cancelStatus);
                    return xdxw0020DataRespDto;
                }
                CmisLmt0008ApprListReqDto cmisLmt0008ApprListReqDto = new CmisLmt0008ApprListReqDto();
                CmisLmt0008ApprSubListReqDto cmisLmt0008ApprSubListReqDto = new CmisLmt0008ApprSubListReqDto();
                cmisLmt0008ApprListReqDto.setCusId(cusId);
                cmisLmt0008ApprListReqDto.setApprSerno(lmtCrdReplyInfo.getSurveySerno());
                cmisLmt0008ApprListReqDto.setOptType("02");//全部终止
                apprList.add(cmisLmt0008ApprListReqDto);
                cmisLmt0008ApprSubListReqDto.setCusId(cusId);
                cmisLmt0008ApprSubListReqDto.setApprSubSerno(sernoDetail);
                apprSubList.add(cmisLmt0008ApprSubListReqDto);
            }
            cmisLmt0008ReqDto.setApprList(apprList);
            cmisLmt0008ReqDto.setApprSubList(apprSubList);
            cmisLmt0008ReqDto.setLmtType("01");
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, JSON.toJSONString(cmisLmt0008ReqDto));
            ResultDto<CmisLmt0008RespDto> cmisLmt0008RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmisLmt0008(cmisLmt0008ReqDto)).orElse(new ResultDto<>());
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0008.value, JSON.toJSONString(cmisLmt0008RespDtoResultDto));
            if (Objects.equals(cmisLmt0008RespDtoResultDto.getCode(), EpbEnum.EPB099999.key)) {
                cancelStatus = "02";
                xdxw0020DataRespDto.setCancelStatus(cancelStatus);
            } else {
                cancelStatus = "01";
                xdxw0020DataRespDto.setCancelStatus(cancelStatus);
                //额度恢复成功后,删除老的批复（新微贷会再次推这笔流水）
                int count = lmtCrdReplyInfoMapper.deleteBySurveySerno(serno);
                logger.info("***************XDXW0020*删除老的批复信息成功【{}】", JSON.toJSONString(count));
            }
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            cancelStatus = "02";
            xdxw0020DataRespDto.setCancelStatus(cancelStatus);
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.key);
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, JSON.toJSONString(xdxw0020DataReqDto));
        return xdxw0020DataRespDto;
    }
}
