/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.domain.LmtAppRelCusLimit;
import cn.com.yusys.yusp.domain.LmtAppRelGrpLimit;
import cn.com.yusys.yusp.dto.server.cmiscus0001.req.CmisCus0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001GrpMemberListRespDto;
import cn.com.yusys.yusp.dto.server.cmiscus0001.resp.CmisCus0001RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022CusListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.req.CmisLmt0022ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0022.resp.CmisLmt0022RespDto;
import cn.com.yusys.yusp.enums.returncode.EclEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.LmtAppRelGrpLimitMapper;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppRelGrpLimitService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-05-18 21:32:55
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class LmtAppRelGrpLimitService extends BizInvestCommonService{

    // 日志
    private static final Logger log = LoggerFactory.getLogger(LmtAppRelGrpLimitService.class);

    @Autowired
    private LmtAppRelGrpLimitMapper lmtAppRelGrpLimitMapper;
    /**
     * 统一额度服务接口
     */
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private LmtAppRelCusLimitService lmtAppRelCusLimitService;

    @Autowired
    private LmtAppRelGrpLimitService lmtAppRelGrpLimitService;

    /**
     * 申请关联客户额度明细信息
     * @param pkId
     * @return
     */
    @Autowired
    private LmtAppRelCusDetailsLimitService lmtAppRelCusDetailsLimitService;

	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public LmtAppRelGrpLimit selectByPrimaryKey(String pkId) {
        return lmtAppRelGrpLimitMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<LmtAppRelGrpLimit> selectAll(QueryModel model) {
        List<LmtAppRelGrpLimit> records = (List<LmtAppRelGrpLimit>) lmtAppRelGrpLimitMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<LmtAppRelGrpLimit> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<LmtAppRelGrpLimit> list = lmtAppRelGrpLimitMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(LmtAppRelGrpLimit record) {
        return lmtAppRelGrpLimitMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(LmtAppRelGrpLimit record) {
        return lmtAppRelGrpLimitMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(LmtAppRelGrpLimit record) {
        return lmtAppRelGrpLimitMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(LmtAppRelGrpLimit record) {
        return lmtAppRelGrpLimitMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return lmtAppRelGrpLimitMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return lmtAppRelGrpLimitMapper.deleteByIds(ids);
    }

    /**
     * 获取集团客户额度信息
     * @param serno
     * @return
     */
    public LmtAppRelGrpLimit getGrpLmtInfoBySerno(String serno) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        BizInvestCommonService.checkParamsIsNull("serno",serno);
        queryModel.addCondition("oprType",CmisBizConstants.OPR_TYPE_01);
        queryModel.setSize(1);
        queryModel.setPage(1);
        List<LmtAppRelGrpLimit> lmtAppRelGrpLimits = selectByModel(queryModel);
        if (lmtAppRelGrpLimits!= null && lmtAppRelGrpLimits.size()>0){
            return lmtAppRelGrpLimits.get(0);
        }
        return null;
    }


    /**
     * 添加集团额度相关信息（lmt_app_rel_grp_limit、lmt_app_rel_cus_limit、lmt_app_rel_cus_details_limit）
     * 授信主体客户号  -->  集团客户查询--->集团客户编号、子成员列表
     * 							    --->（不属于集团）返回空
     * -->成员列表/主体授信客户号  -->0022接口  -->客户额度明细
     * A -- 明细1   明细2   明细3
     * B -- 明细4   明细5   明细6
     * @param serno
     * @param cusId
     * @return
     */
    public int insertCusLmtInfo(String serno, String cusId, String cusType, String cusCatalog) {
        long start = System.currentTimeMillis();
        int result = 0;
        //保存各集团成员的额度信息
        Map<String, LmtAppRelCusLimit> membersLmt = new HashMap<>();
        //额度信息cusid列表
        ArrayList<CmisLmt0022CusListReqDto> cmisLmt0022CusListReqDtos = new ArrayList<>();
        try {
            /**
             * 1.获取集团客户号和成员列表 cmiscus0001
             */
            CmisCus0001ReqDto cmisCus0001ReqDto = new CmisCus0001ReqDto();
            cmisCus0001ReqDto.setCusId(cusId);
            ResultDto<CmisCus0001RespDto> cmisCus0001RespDtoResultDto = cmisCusClientService.cmiscus0001(cmisCus0001ReqDto);

            if (cmisCus0001RespDtoResultDto != null
                    && SuccessEnum.SUCCESS.key.equals(cmisCus0001RespDtoResultDto.getData().getErrorCode())
                    && !"40012".equals(cmisCus0001RespDtoResultDto.getData().getErrorCode())){
                CmisCus0001RespDto data = cmisCus0001RespDtoResultDto.getData();
                log.info("存在集团客户编号、子成员列表");
                String grpNo = data.getGrpNo();//集团客户号
                String grpName = data.getGrpName();
                //集团成员列表
                List<CmisCus0001GrpMemberListRespDto> grpMemberList = data.getGrpMemberList();
                if (grpMemberList != null && grpMemberList.size() > 0) {
                    grpMemberList.forEach(a -> {
                        CmisLmt0022CusListReqDto cmisLmt0022CusListReqDto = new CmisLmt0022CusListReqDto();
                        cmisLmt0022CusListReqDto.setCusId(a.getCusId());
                        cmisLmt0022CusListReqDtos.add(cmisLmt0022CusListReqDto);
                    });
                    //获取集团所有成员的额度明细列表
                    List<CmisLmt0022LmtListRespDto> lmtList = getLmtRelCusDetails(cmisLmt0022CusListReqDtos);
                    for (CmisLmt0022LmtListRespDto respDto0022 : lmtList) {
                        log.info("添加 （申请关联客户额度明细信息 lmt_app_rel_cus_details_limit）");
                        //添加 （申请关联客户额度明细信息	lmt_app_rel_cus_details_limit）
                        lmtAppRelCusDetailsLimitService.insertRelCusDetails(serno, respDto0022);
                        log.info("分别计算 各集团成员的总额度 并 重新计算和更新map中的该成员额度数据");
                        //分别计算 各集团成员的总额度 并 重新计算和更新map中的该成员额度数据
                        String lmtCatalog = respDto0022.getLmtCatalog();//额度类型
                        if(!CmisLmtConstants.STD_ZB_LMT_STATS_TYPE_13.equals(lmtCatalog)){//债券池用信不纳入授信总额计算内
                            membersLmt.put(respDto0022.getCusId(), calculateCusLimit(membersLmt.get(respDto0022.getCusId()), respDto0022, serno,grpNo, respDto0022.getCusId()));
                        }

                    }
                    //便利集团成员额度信息到数据库中
                    for (CmisCus0001GrpMemberListRespDto respDto : grpMemberList) {
                        log.info("获取已经计算好的各成员额度信息");
                        //获取已经计算好的各成员额度信息
                        LmtAppRelCusLimit lmtAppRelCusLimit = membersLmt.get(respDto.getCusId());
                        //存在该集团成员额度详情
                        if (lmtAppRelCusLimit!=null){
                            lmtAppRelCusLimitService.insert(lmtAppRelCusLimit);
                        }else{
                            log.info("不存在该集团成员额度信息 ->额度明细为空，初始化数据");
                            //不存在该集团成员额度信息 ->额度明细为空，初始化数据
//                            lmtAppRelCusLimit = calculateCusLimit(membersLmt.get(respDto.getCusId()), null, serno, grpNo, respDto.getCusId());
//                            lmtAppRelCusLimitService.insert(lmtAppRelCusLimit);
                        }
                    }
                    log.info("计算集团总额度 并添加");
                    //计算集团总额度 并添加
                    result = insertGrpLmtInfo(serno,grpNo,grpName,membersLmt);
                }
            }else{
                log.info("（不属于集团）返回空 调用0022 查询额度明细");
                // （不属于集团）返回空 调用0022 查询额度明细
                CmisLmt0022CusListReqDto cmisLmt0022CusListReqDto = new CmisLmt0022CusListReqDto();
                cmisLmt0022CusListReqDto.setCusId(cusId);
                cmisLmt0022CusListReqDtos.add(cmisLmt0022CusListReqDto);
                List<CmisLmt0022LmtListRespDto> lmtList = getLmtRelCusDetails(cmisLmt0022CusListReqDtos);
                if (lmtList!=null && lmtList.size()>0){
                    for (CmisLmt0022LmtListRespDto respDto0022 : lmtList) {
                        log.info("添加 （申请关联客户额度明细信息lmt_app_rel_cus_details_limit）");
                        //添加 （申请关联客户额度明细信息	lmt_app_rel_cus_details_limit）
                        lmtAppRelCusDetailsLimitService.insertRelCusDetails(serno, respDto0022);
                        log.info("分别计算 各集团成员的总额度 并 更新map中的集团成员额度数据");

                        log.info("respDto0022.getCusId()==>{}  cusId==={}", respDto0022.getCusId(),cusId);
                        log.info("respDto0022===>{}",respDto0022.toString());
                        //分别计算 各集团成员的总额度 并 更新map中的集团成员额度数据
                        membersLmt.put(respDto0022.getCusId(), calculateCusLimit(membersLmt.get(respDto0022.getCusId()), respDto0022, serno,null, respDto0022.getCusId()));
                    }
                    log.info("添加集团成员额度信息到数据库中");
                    //添加集团成员额度信息到数据库中
                    for (LmtAppRelCusLimit val : membersLmt.values()) {
                        lmtAppRelCusLimitService.insert(val);
                    }
                }else{
                    log.info("额度明细为空，初始化数据");
                    //额度明细为空，初始化数据
//                    LmtAppRelCusLimit lmtAppRelCusLimit = calculateCusLimit(membersLmt.get(cusId), null, serno, null,cusId);
//                    lmtAppRelCusLimitService.insert(lmtAppRelCusLimit);
                }
                result = 1;
            }
        } catch (Exception e) {
            log.error("添加集团额度相关信息报错==》",e);
            result = 0;
        }
        if (result == 0){
            //抛出额度报错信息
            throw BizException.error(null, EclEnum.LMT_INVEST_RELCUSINFO_GET_FAILED.key,EclEnum.LMT_INVEST_RELCUSINFO_GET_FAILED.value);
        }
        log.info(" insertCusLmtInfo 执行时间===> {}",System.currentTimeMillis()-start );
        return result;
    }

    /**
     * 计算集团总额度 并添加
     * @param serno
     * @param grpNo
     * @param grpName
     * @param membersLmt
     */
    private int insertGrpLmtInfo(String serno, String grpNo, String grpName, Map<String, LmtAppRelCusLimit> membersLmt) {
        BigDecimal grpLmtAmt = BigDecimal.ZERO;
        BigDecimal grpOutstandAmt = BigDecimal.ZERO;
        BigDecimal grpLmtBalanceAmt = BigDecimal.ZERO;
        BigDecimal grpAvlLmtAmt = BigDecimal.ZERO;
        //计算集团额度总额
        for (LmtAppRelCusLimit val : membersLmt.values()) {
            grpLmtAmt = grpLmtAmt.add(val.getLmtAmt());
            grpOutstandAmt = grpOutstandAmt.add(val.getOutstndAmt());
            grpLmtBalanceAmt = grpLmtBalanceAmt.add(val.getLmtBalanceAmt());
            grpAvlLmtAmt = grpAvlLmtAmt.add(val.getAvlLmtAmt());
        }
        //判断是否存在集团信息
        LmtAppRelGrpLimit lmtAppRelGrpLimit = new LmtAppRelGrpLimit();

        lmtAppRelGrpLimit.setSerno(serno);
        lmtAppRelGrpLimit.setGrpCusId(grpNo);
        lmtAppRelGrpLimit.setGrpCusName(grpName);
        lmtAppRelGrpLimit.setGrpLmtAmt(grpLmtAmt);
        lmtAppRelGrpLimit.setAvlLmtAmt(grpAvlLmtAmt);
        lmtAppRelGrpLimit.setGrpLmtBalanctAmt(grpLmtBalanceAmt);

        //初始化(新增)通用domain信息
        initInsertDomainProperties(lmtAppRelGrpLimit);
        //添加集团额度信息
        return lmtAppRelGrpLimitService.insert(lmtAppRelGrpLimit);
    }

    /**
     * 调用cmisLmt0022获取额度明细列表
     * @param cmisLmt0022CusListReqDtos
     * @return
     */
    private List<CmisLmt0022LmtListRespDto> getLmtRelCusDetails(ArrayList<CmisLmt0022CusListReqDto> cmisLmt0022CusListReqDtos) {
        //根据成员列表获取额度信息详细记录
        CmisLmt0022ReqDto cmisLmt0022ReqDto = new CmisLmt0022ReqDto();
        cmisLmt0022ReqDto.setCusList(cmisLmt0022CusListReqDtos);
        cmisLmt0022ReqDto.setInstuCde(getCurrentUser().getInstuOrg().getCode());
        CmisLmt0022RespDto data = cmisLmtClientService.cmisLmt0022(cmisLmt0022ReqDto).getData();
        log.info("cmisLmt0022====>{}",data);
        if (SuccessEnum.SUCCESS.key.equals(data.getErrorCode())){
            List<CmisLmt0022LmtListRespDto> lmtList = data.getLmtList();
            if (lmtList !=null && lmtList.size()>0){
                return lmtList;
            }
        }
        return new ArrayList<CmisLmt0022LmtListRespDto>();
    }

    /**
     * 计算集团成员相关额度
     * @param lmtAppRelCusLimit
     * @param respDto0022
     * @param serno
     * @param grpNo
     * @param cusId
     * @return
     */
    private LmtAppRelCusLimit calculateCusLimit(LmtAppRelCusLimit lmtAppRelCusLimit, CmisLmt0022LmtListRespDto respDto0022, String serno, String grpNo, String cusId) {
        if (lmtAppRelCusLimit == null){
            //不存在 则创建
            lmtAppRelCusLimit = new LmtAppRelCusLimit();

            lmtAppRelCusLimit.setSerno(serno);
            lmtAppRelCusLimit.setCusId(cusId);
            lmtAppRelCusLimit.setGrpNo(grpNo);

            //初始化(新增)通用domain信息
            initInsertDomainProperties(lmtAppRelCusLimit);

            if (respDto0022 != null){
                lmtAppRelCusLimit.setCusName(respDto0022.getCusName());
                //授信金额
                lmtAppRelCusLimit.setLmtAmt(changeValueToBigDecimal(respDto0022.getLmtAmt()));
                //已用额度
                lmtAppRelCusLimit.setOutstndAmt(changeValueToBigDecimal(respDto0022.getOutstandAmt()));
                //可用额度
                //可用额度=授信额度-已用额度
                BigDecimal avlLmtAmt = respDto0022.getLmtAmt().subtract(respDto0022.getOutstandAmt());
                lmtAppRelCusLimit.setAvlLmtAmt(changeValueToBigDecimal(avlLmtAmt));//可用额度
                //授信余额
                lmtAppRelCusLimit.setLmtBalanceAmt(changeValueToBigDecimal(respDto0022.getLmtBalanceAmt()));
            }else{
                //返回额度详情为空，初始化数据
                lmtAppRelCusLimit.setLmtAmt(BigDecimal.ZERO);
                lmtAppRelCusLimit.setOutstndAmt(BigDecimal.ZERO);
                lmtAppRelCusLimit.setAvlLmtAmt(BigDecimal.ZERO);//可用额度
                lmtAppRelCusLimit.setLmtBalanceAmt(BigDecimal.ZERO);
            }
        }else{
            if (respDto0022!=null){
                //存在 则根据最新重新计算数据
                //授信金额
                BigDecimal lmtAmt = lmtAppRelCusLimit.getLmtAmt().add(respDto0022.getLmtAmt());
                lmtAppRelCusLimit.setLmtAmt(changeValueToBigDecimal(lmtAmt));
                //已用额度
                BigDecimal outstndAmt = lmtAppRelCusLimit.getOutstndAmt().add(respDto0022.getOutstandAmt());
                lmtAppRelCusLimit.setOutstndAmt(changeValueToBigDecimal(outstndAmt));
                //授信余额
                BigDecimal lmtBalanceAmt = lmtAppRelCusLimit.getLmtBalanceAmt().add(respDto0022.getLmtBalanceAmt());
                lmtAppRelCusLimit.setLmtBalanceAmt(changeValueToBigDecimal(lmtBalanceAmt));
                //可用额度
                //计算授信余额 = 授信余额+（授信额度-已用额度）
                BigDecimal avlLmtAmt = lmtAppRelCusLimit.getAvlLmtAmt().add(respDto0022.getLmtAmt().subtract(respDto0022.getOutstandAmt()));
                lmtAppRelCusLimit.setAvlLmtAmt(changeValueToBigDecimal(avlLmtAmt));
            }
        }
        return lmtAppRelCusLimit;
    }

}
