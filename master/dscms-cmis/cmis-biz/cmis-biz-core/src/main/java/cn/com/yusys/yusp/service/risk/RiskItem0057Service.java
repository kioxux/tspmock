package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.domain.RepayCapPlan;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.CtrLoanContService;
import cn.com.yusys.yusp.service.IqpLoanAppService;
import cn.com.yusys.yusp.service.PvpLoanAppService;
import cn.com.yusys.yusp.service.RepayCapPlanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Queue;

/**
 * @author quwen
 * @version 1.0.0
 * @date 2021年8月3日16:04:21
 * @desc 还本计划中还本金额校验
 * @修改历史: 修改时间    修改人员    修改原因
 */
@Service
public class RiskItem0057Service {
    private static final Logger log = LoggerFactory.getLogger(RiskItem0057Service.class);
    @Autowired
    private CtrLoanContService ctrLoanContService;

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private RepayCapPlanService repayCapPlanService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author quwen
     * @date 2021年7月30日16:06:37
     * @version 1.0.0
     * @desc    贷款出账申请时，还本计划中还本金额校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0057(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String pvpSerno = (String)queryModel.getCondition().get("bizId");
        if (StringUtils.isBlank(pvpSerno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 通过申请流水号获取申请信息
        PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPvpLoanSernoKey(pvpSerno);
        if(Objects.isNull(pvpLoanApp)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0017); //通过合同申请流水号未获取到对应的合同申请信息
            return riskResultDto;
        }
        //判断还款方式是否为按期付息按计划还本
        String repayMode = pvpLoanApp.getRepayMode();
        if("A040".equals(repayMode)){
            QueryModel queryModelData = new QueryModel();
            queryModelData.addCondition("serno",pvpSerno);
            List<RepayCapPlan> repayCapPlanList = repayCapPlanService.selectByModel(queryModelData);
            if(repayCapPlanList.size()<=0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05701); //未定制还本计划
                return riskResultDto;
            }else{
                BigDecimal totalRepayAmt = BigDecimal.ZERO;
                for (RepayCapPlan repayCapPlan : repayCapPlanList) {
                    BigDecimal repayAmt = repayCapPlan.getRepayAmt();
                    totalRepayAmt = totalRepayAmt.add(repayAmt);
                }
                log.info("还本计划总金额为【{}】,出账金额折算金额为【{}】",totalRepayAmt,pvpLoanApp.getCvtCnyAmt());
                if(totalRepayAmt.compareTo(pvpLoanApp.getCvtCnyAmt()) != 0){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_05702); //还本金额与出账金额不一致
                    return riskResultDto;
                }
            }
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
