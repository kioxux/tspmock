/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.domain.AccLoan;
import cn.com.yusys.yusp.dto.client.esb.core.ln3025.Ln3025ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3025.Ln3025RespDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.enums.out.common.DicTranEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.bsp.core.ln3025.Ln3025Service;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CreditReportQryLstAndRealDto;
import cn.com.yusys.yusp.dto.CusBaseClientDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.com.yusys.yusp.repository.mapper.CashAdvanceMapper;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CashAdvanceService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: zrcbank
 * @创建时间: 2021-11-15 15:43:42
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class CashAdvanceService {

    private static final Logger log = LoggerFactory.getLogger(CashAdvanceService.class);

    @Resource
    private CashAdvanceMapper cashAdvanceMapper;
    @Resource
    private AccLoanMapper accLoanMapper;

    @Resource
    private Ln3025Service ln3025Service;
    @Resource
    private CmisLmtClientService cmisLmtClientService;
    @Resource
    private CtrCvrgContService ctrCvrgContService;

    @Resource
    private CtrTfLocContService ctrTfLocContService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号服务

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public CashAdvance selectByPrimaryKey(String pvpSerno) {
        return cashAdvanceMapper.selectByPrimaryKey(pvpSerno);
    }

    /**
     * 分页查询
     * @param queryModel
     * @return
     */
    public List<CashAdvance> selectByModel(QueryModel queryModel){
        PageHelper.startPage(queryModel.getPage(), queryModel.getSize());
        List<CashAdvance> list = cashAdvanceMapper.selectByModel(queryModel);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(CashAdvance record) {
        return cashAdvanceMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    /**
     * 垫款出账申请新增
     *
     * @param cashAdvance
     * @return CashAdvance cashAdvance
     * @author:hh
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveCashAdvance(CashAdvance cashAdvance) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.IQP_SUCCESS_DEF.key;
        String rtnMsg = "";
        String pvpSerno = "";
        String tradeCode = cashAdvance.getTradeCode();
        String contNo = cashAdvance.getContNo();
        BigDecimal exchangeRate = BigDecimal.ZERO;
        QueryModel queryModel = new QueryModel();
        //借据编号
        queryModel.addCondition("contNo", contNo);
        // 总记录数
        queryModel.setSize(Integer.valueOf(10));
        // 总页数
        queryModel.setPage(Integer.valueOf(1));

        if (tradeCode.equals("CRE928")) {//保函
            cashAdvance.setLoanSubjectNo("13081505");//科目号
            //查询合同对象
            List<CtrCvrgCont> ctrCvrgContList = ctrCvrgContService.selectByModel(queryModel);
            CtrCvrgCont ctrCvrgCont = ctrCvrgContList.get(0);
            //合同起始日
            cashAdvance.setStartDate(ctrCvrgCont.getStartDate());
            //合同到期日
            cashAdvance.setEndDate(ctrCvrgCont.getEndDate());
            //合同金额
            cashAdvance.setContAmt(ctrCvrgCont.getContAmt());
            //合同币种
            cashAdvance.setContCurType(ctrCvrgCont.getCurType());
           //担保方式
            cashAdvance.setGuarMode(ctrCvrgCont.getGuarMode());
            //合同最高可放金额，取合同余额
            cashAdvance.setContHighDisb(ctrCvrgCont.getContHighAvlAmt());

            cashAdvance.setIqpSerno(ctrCvrgCont.getSerno());
        } else if(tradeCode.equals("CRE929")){//信用证
            List<CtrTfLocCont> ctrTfLocContList = ctrTfLocContService.selectByModel(queryModel);

            CtrTfLocCont ctrTfLocCont = ctrTfLocContList.get(0);
            cashAdvance.setLoanSubjectNo("13081905");//科目号
            //担保方式
            cashAdvance.setGuarMode(ctrTfLocCont.getGuarMode());
            //合同起始日
            cashAdvance.setStartDate(ctrTfLocCont.getStartDate());
            //合同到期日
            cashAdvance.setEndDate(ctrTfLocCont.getEndDate());
            //合同金额
            cashAdvance.setContAmt(ctrTfLocCont.getContAmt());
            //币种
            cashAdvance.setContCurType(ctrTfLocCont.getCurType());
            //合同最高可放金额，取合同余额
            cashAdvance.setContHighDisb(ctrTfLocCont.getContHighAvlAmt());
        }
        //合同币种
        cashAdvance.setCurType("CNY");
        //执行利率
        cashAdvance.setExecRateYear(BigDecimal.valueOf(0.18));
        //利率调整方式
        cashAdvance.setRateAdjMode("01");//固定利率
        //是否分段计息
        cashAdvance.setIsSegInterest("0");//否
        //利率调整方式
        cashAdvance.setRateAdjMode("01");
        //LPR利率区间
        cashAdvance.setLprRateIntval("A1");//一年
        //LPR利率区间
        cashAdvance.setIsHolidayDelay("1");//是
        //还款方式
        cashAdvance.setRepayMode("A009");//利随本清
        //扣款方式
        cashAdvance.setDeductType("AUTO");//自动扣款
        //结息间隔周期
        cashAdvance.setEiIntervalCycle("1");
        // 借款用途类型
        cashAdvance.setLoanUseType("A05");//流动资金-置换债务
        // 借款用途类型
        cashAdvance.setLoanUseType("A05");//流动资金-置换债务
        //扣款日
        cashAdvance.setDeductDay("21");
        //逾期利率浮动比
        cashAdvance.setOverdueRatePefloat(BigDecimal.valueOf(0));
        //复息利率浮动比
        cashAdvance.setCiRatePefloat(BigDecimal.valueOf(0));
        cashAdvance.setIsHolidayDelay(CmisCommonConstants.YES_NO_1);//节假日是否顺延
        cashAdvance.setPrdId("DK");//垫款
        cashAdvance.setFtp("0");//否
        cashAdvance.setPrdName("垫款");
        // 产品类型属性
        cashAdvance.setPrdTypeProp("");
        // 贷款期限单位
        cashAdvance.setLoanTermUnit("M");
        //申请状态
        cashAdvance.setApproveStatus(CmisCommonConstants.WF_STATUS_000);
        //操作类型
        cashAdvance.setOprType(CmisCommonConstants.OPR_TYPE_ADD);

        cashAdvance.setInputDate(stringRedisTemplate.opsForValue().get("openDay"));
        //是否受托支付
        cashAdvance.setIsBeEntrustedPay("0");
        //渠道来源
        cashAdvance.setChnlSour("02");//02PC端
        //所属条线
        cashAdvance.setBelgLine(CmisBizConstants.BELGLINE_DG_03);
        //是否落实贷款
        cashAdvance.setIsPactLoan("0");
        //是否绿色产业
        cashAdvance.setIsGreenIndustry("0");
        //是否经营性物业贷款
        cashAdvance.setIsOperPropertyLoan("0");
        //是否钢贸行业贷款
        cashAdvance.setIsSteelLoan("0");
        //是否不锈钢行业贷款
        cashAdvance.setIsStainlessLoan("0");
        //是否扶贫贴息贷款
        cashAdvance.setIsLaborIntenSbsyLoan("0");
        //是否劳动密集型小企业贴息贷款
        cashAdvance.setIsPovertyReliefLoan("0");
        //保障性安居工程贷款
        cashAdvance.setGoverSubszHouseLoan("00");//非保障性安居工程贷款
        //项目贷款节能环保
        cashAdvance.setEngyEnviProteLoan("00");//非项目贷款
        //是否农村综合开发贷款标志
        cashAdvance.setIsCphsRurDelpLoan("0");
        //房地产贷款
        cashAdvance.setRealproLoan("00");//非房地产贷款
        try {
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {

                cashAdvance.setInputId(userInfo.getLoginCode());
                cashAdvance.setInputBrId(userInfo.getOrg().getCode());
                cashAdvance.setManagerId(userInfo.getLoginCode());
                cashAdvance.setManagerBrId(userInfo.getOrg().getCode());
            }
            //放款流水号
            pvpSerno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.PVP_SERNO, new HashMap<>());
            Map seqMap = new HashMap();
            seqMap.put("contNo", cashAdvance.getContNo());
            cashAdvance.setPvpSerno(pvpSerno);//主键为放款流水号
            //放款流水号
            String  cashBillNo = "DK"+sequenceTemplateClient.getSequenceTemplate(SeqConstant.PK_ID, new HashMap<>());
            cashAdvance.setCashBillNo(cashBillNo);
            int insertCount = this.insertSelective(cashAdvance);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("iqpSerno", cashAdvance.getIqpSerno());
            result.put("pvpSerno", pvpSerno);
            log.info("垫款出账申请" + pvpSerno + "-新增成功！");
        } catch (YuspException e) {
            log.error("垫款出账申请新增异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("垫款出账申请异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }


    public int update(CashAdvance record) {
        return cashAdvanceMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(CashAdvance record) {
        return cashAdvanceMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pvpSerno) {
        return cashAdvanceMapper.deleteByPrimaryKey(pvpSerno);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return cashAdvanceMapper.deleteByIds(ids);
    }

    /**
     * 发送核心
     * @param pvpSerno
     * @return
     */
    public ResultDto sendCashAdvanceToCore(String pvpSerno){
        CashAdvance cashAdvance = this.selectByPrimaryKey(pvpSerno);
        try{
            String dkjiejuh = cashAdvance.getCashBillNo(); //借据号
            String kehuhaoo = cashAdvance.getCusId(); //客户号
            String tradeCode = cashAdvance.getTradeCode(); //业务品种
            String kehmingc = cashAdvance.getCusName(); //客户名
            BigDecimal zdkuanje = cashAdvance.getCashAmt(); //垫款金额
            //String managerId = cashAdvance.getManagerId(); //登记人
            //String managerBrId = cashAdvance.getManagerBrId(); //登记机构
            String huankzhh = cashAdvance.getRepayAccno(); //还款账号
            String khzhhzxh = cashAdvance.getRepaySubAccno(); //还款账号子序号
            String chuzhhao = cashAdvance.getLoanPayoutAccno(); //出账号
            String contNo = cashAdvance.getContNo(); //合同编号
            //BigDecimal loan_amount = cashAdvance.getloan; //TODO 借据金额
            //String inputDate = cashAdvance.getInputDate(); //交易日期
            //BigDecimal loanBalance = (String)cashAdvance.getDataValue("loan_balance"); //TODO 借据余额
            //String startDate = cashAdvance.getStartDate(); //起始日
            //String endDate = cashAdvance.getEndDate(); //到期日
            String applyCurType = cashAdvance.getCurType();//币种
            BigDecimal execRateYear = cashAdvance.getExecRateYear();//利率
            String yngyjigo = cashAdvance.getFinaBrId();//机构
            String repayAccno = cashAdvance.getRepayAccno();//垫款账号
            String repaySubAccno =  cashAdvance.getRepaySubAccno(); //还款账号子序号

            String  ysywleix = "";//衍生业务类型
            String  ysywbhao = "";//衍生业务编号
            String kuaijilb = "";//会计类别
            String prdName = "";//产品名称
            if("CRE928".equals(tradeCode)){
                ysywleix = "5";
                kuaijilb = "LNHS125";
                ysywbhao = contNo;//保函时传递合同号
                prdName = "垫款保函";
            }else if("CRE929".equals(tradeCode)){
                String cashBillNo = cashAdvance.getIqpSerno(); //信用证时IqpSerno保存的是信用证编号
                ysywleix = "6";
                kuaijilb = "LNHS120";
                ysywbhao = cashBillNo;
                prdName = "垫款信用证";
            }
            DecimalFormat decimalFormat = new DecimalFormat("0.000000");
            Ln3025ReqDto ln3025ReqDto = new Ln3025ReqDto();
            ln3025ReqDto.setDkkhczbz("4");// 开户操作标志
            ln3025ReqDto.setDkjiejuh(dkjiejuh);// 贷款借据号
            ln3025ReqDto.setKehuhaoo(kehuhaoo);// 客户号
            ln3025ReqDto.setKehmingc(kehmingc);//客户名称
            ln3025ReqDto.setHuankzhh(huankzhh);// 还款账号
            ln3025ReqDto.setHkzhhzxh(khzhhzxh);// 还款账号子序号
            ln3025ReqDto.setChanpdma("110022");// 产品代码
            ln3025ReqDto.setChanpmch("自动垫款");// 产品名称
            ln3025ReqDto.setHuobdhao(DicTranEnum.lookup("CUR_TYPE_XDTOHX_" + applyCurType));// 货币代号
            ln3025ReqDto.setZdkuanje(zdkuanje);// 转垫款金额
            ln3025ReqDto.setYngyjigo(yngyjigo);// 营业机构
            ln3025ReqDto.setYsywleix(ysywleix);// 衍生业务类型
            ln3025ReqDto.setYsywbhao(ysywbhao);// 衍生业务编号
            ln3025ReqDto.setKehuzhao(repayAccno);// 客户账号
            ln3025ReqDto.setKhzhhzxh(repaySubAccno);// 客户账号子序号
            ln3025ReqDto.setSyzjclfs("");// 剩余资金处理方式
            ln3025ReqDto.setZhrujine(null);// 转入金额
            ln3025ReqDto.setLilvleix("6");// 利率类型
            ln3025ReqDto.setZclilvbh("");// 正常利率编号
            ln3025ReqDto.setNyuelilv("Y");// 年/月利率标识
            ln3025ReqDto.setZhchlilv(new BigDecimal(decimalFormat.format(execRateYear.multiply(new BigDecimal(100)))));// 正常利率
            ln3025ReqDto.setLilvtzfs("0");// 利率调整方式
            ln3025ReqDto.setLilvtzzq("");// 利率调整周期
            ln3025ReqDto.setLilvfdfs("0");// 利率浮动方式
            ln3025ReqDto.setLilvfdzh(null);// 利率浮动值
            ln3025ReqDto.setYqllcklx("");// 逾期利率参考类型
            ln3025ReqDto.setYuqillbh("");// 逾期利率编号
            ln3025ReqDto.setYuqinyll("");// 逾期年月利率
            ln3025ReqDto.setYuqililv(null);// 逾期利率
            ln3025ReqDto.setYuqitzfs("");// 逾期利率调整方式
            ln3025ReqDto.setYuqitzzq("");// 逾期利率调整周期
            ln3025ReqDto.setYqfxfdfs("");// 逾期罚息浮动方式
            ln3025ReqDto.setYqfxfdzh(null);// 逾期罚息浮动值
            ln3025ReqDto.setFlllcklx("");// 复利利率参考类型
            ln3025ReqDto.setFulilvbh("");// 复利利率编号
            ln3025ReqDto.setFulilvny("");// 复利利率年月标识
            ln3025ReqDto.setFulililv(null);// 复利利率
            ln3025ReqDto.setFulitzfs("");// 复利利率调整方式
            ln3025ReqDto.setFulitzzq("");// 复利利率调整周期
            ln3025ReqDto.setFulifdfs("");// 复利利率浮动方式
            ln3025ReqDto.setFulifdzh(null);// 复利利率浮动值
            ln3025ReqDto.setDkrzhzhh("");// 贷款入账账号
            ln3025ReqDto.setDkrzhzxh("");// 贷款入账账号子序号
            ln3025ReqDto.setLlqxkdfs("");// 利率期限靠档方式
            ln3025ReqDto.setLilvqixx("");// 利率期限
            ln3025ReqDto.setBenqqish(null);// 本期期数
            ln3025ReqDto.setGljgleib("");// 管理机构类别
            ln3025ReqDto.setChuzhhao(chuzhhao);// 出账号
            ln3025ReqDto.setHetongbh(contNo);// 合同编号
            ln3025ReqDto.setKuaijilb(kuaijilb);// 会计类别
            ln3025ReqDto.setFuhejgou("");// 复核机构
            log.info("请求核心接口放款3025请求报文是：【{}】", JSON.toJSONString(ln3025ReqDto));
            Ln3025RespDto ln3025RespDto = ln3025Service.ln3025(ln3025ReqDto);
            log.info("请求核心接口放款3025响应报文是：【{}】", JSON.toJSONString(ln3025RespDto));

            cashAdvance.setTradeStatus("2");
            cashAdvance.setAuthStatus("2");
            // 核心交易日期
            cashAdvance.setCoreTranDate(ln3025RespDto.getJiaoyirq());
            // 交易流水
            cashAdvance.setCoreTranSerno(ln3025RespDto.getJiaoyils());
            cashAdvance.setReturnDesc("交易成功");
            this.updateSelective(cashAdvance);
            //生成借据信息
            AccLoan accLoan = new AccLoan();
            BeanUtils.copyProperties(cashAdvance, accLoan);
            accLoan.setPkId(StringUtils.uuid(Boolean.TRUE));
            accLoan.setBillNo(cashAdvance.getCashBillNo());
            accLoan.setAdvanceBillNo(cashAdvance.getBillNo());

            accLoan.setLoanAmt(zdkuanje);
            accLoan.setLoanBalance(zdkuanje);
            accLoan.setExchangeRmbAmt(zdkuanje);
            accLoan.setExchangeRmbBal(zdkuanje);
            accLoan.setPrdId(tradeCode);
            accLoan.setPrdName(prdName);
            accLoan.setSubjectNo(cashAdvance.getLoanSubjectNo());
            accLoan.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            accLoan.setCreateTime(DateUtils.getCurrDate());
            accLoan.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));
            accLoan.setUpdateTime(DateUtils.getCurrDate());
            accLoanMapper.insertSelective(accLoan);

            //调额度出账通知接口
            CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();
            cmisLmt0029ReqDto.setDealBizNo(cashAdvance.getPvpSerno());//交易流水号
            cmisLmt0029ReqDto.setOriginAccNo(cashAdvance.getCashBillNo());//原台账编号
            cmisLmt0029ReqDto.setNewAccNo(cashAdvance.getCashBillNo());//新台账编号
            cmisLmt0029ReqDto.setStartDate("");//合同起始日
            cmisLmt0029ReqDto.setEndDate("");//合同到期日
            cmisLmt0029ReqDto.setIsPvpSucs(CmisCommonConstants.STD_ZB_YES_NO_1);//是否出账成功通知
            log.info("垫款出账调额度出账通知" + cashAdvance.getCashBillNo() + "，前往额度系统通知出账开始");
            ResultDto<CmisLmt0029RespDto> resultDto = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
            log.info("垫款款出账调额度出账通知" + cashAdvance.getCashBillNo() + "，前往额度系统通知出账结束");
            if (!"0".equals(resultDto.getCode())) {
                log.error("垫款出账调额度出账通知" + cashAdvance.getCashBillNo() + "前往额度系统通知出账异常！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
            String code = resultDto.getData().getErrorCode();
            if (!EcbEnum.ECB010000.key.equals(code)) {
                log.error("垫款出账调额度出账通知异常！");
                throw BizException.error(null, EcbEnum.ECB019999.key, EcbEnum.ECB019999.value);
            }
        } catch (BizException e) {
            // 更新授权信息开始
            String handErrorMsg = "";
            if(e.getMessage()!=null){
                if(e.getMessage().length()>500){
                    handErrorMsg = e.getMessage().substring(0,500);
                }else{
                    handErrorMsg = e.getMessage();
                }
            }else{
                handErrorMsg = null;
            }
            cashAdvance.setReturnDesc(handErrorMsg);
            cashAdvance.setReturnCode(e.getErrorCode());
            cashAdvance.setAuthStatus("3");
            this.updateSelective(cashAdvance);
            log.error(e.getMessage(), e);
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }catch (Exception e){
            // 更新授权信息开始
            cashAdvance.setReturnDesc(e.getMessage());
            cashAdvance.setAuthStatus("3");
            this.updateSelective(cashAdvance);
            log.error(e.getMessage(), e);
            throw BizException.error(null, "9999", "出账失败：" + e.getMessage());
        }
        return new ResultDto(cashAdvance).message("出账成功");
    }
}
