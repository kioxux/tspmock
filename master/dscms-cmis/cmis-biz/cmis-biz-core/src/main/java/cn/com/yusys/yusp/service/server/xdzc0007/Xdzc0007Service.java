package cn.com.yusys.yusp.service.server.xdzc0007;


import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.req.CmisLmt0025ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0025.resp.CmisLmt0025RespDto;
import cn.com.yusys.yusp.dto.server.xddb0007.req.Xddb0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.req.Xdzc0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.resp.Xdzc0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import io.netty.util.internal.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * 接口处理类:资产池出池
 *
 * @Author xs
 * @Date 2021/07/03 20:20
 * @Version 1.0
 */
@Service
public class Xdzc0007Service {

    @Autowired
    private AsplAssetsListService asplAssetsListService;

    @Autowired
    private AsplIoPoolService asplIoPoolService;

    @Autowired
    private AsplIoPoolDetailsService asplIoPoolDetailsService;

    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private GrtGuarContRelService grtGuarContRelService;

    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private Dscms2CoreCoClientService dscms2CoreCoClientService;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    private static final Logger logger = LoggerFactory.getLogger(Xdzc0007Service.class);
    @Transactional
    public Xdzc0007DataRespDto xdzc0007Service(Xdzc0007DataReqDto xdzc0007DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value);
        Xdzc0007DataRespDto xdzc0007DataRespDto = new Xdzc0007DataRespDto();
        //默认成功
        xdzc0007DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
        if("0".equals(xdzc0007DataReqDto.getOpType())){
            xdzc0007DataRespDto.setOpMsg("出池成功");// 描述信息
        }else if("2".equals(xdzc0007DataReqDto.getOpType())){
            xdzc0007DataRespDto.setOpMsg("票据托收出池成功");// 描述信息
        }else{
            xdzc0007DataRespDto.setOpMsg("解质押成功");// 描述信息
        }
        // 获取参数
        String cusId = xdzc0007DataReqDto.getCusId();// 客户编号
        String resn = xdzc0007DataReqDto.getResn();//出池原因
        String opType = xdzc0007DataReqDto.getOpType();// 操作类型 0、出池 1、解质押
        String serno = xdzc0007DataReqDto.getSerno();// 出池批次号 TODO
        java.util.List<cn.com.yusys.yusp.dto.server.xdzc0007.req.List> list = xdzc0007DataReqDto.getList();
        try {
            // 资产池协议信息
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoByCusId(cusId);
            // 资产池协议编号
            String contNo = ctrAsplDetails.getContNo();
            if (Objects.isNull(ctrAsplDetails)) {
                xdzc0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdzc0007DataRespDto.setOpMsg("未查询到客户" + cusId + "下存在有效的资产池协议");
                return xdzc0007DataRespDto;
            }
            if (list.size() > 1) {
                xdzc0007DataRespDto.setOpFlag(DscmsBizDbEnum.FLAG_FAILD.key);
                xdzc0007DataRespDto.setOpMsg("当前接口只支持单笔出池");
                return xdzc0007DataRespDto;
            }
            // 目前只支持一个
            String assetNo = list.get(0).getAssetNo();
            // 根据资产编号获取资产清单
            AsplAssetsList asplAssetsList = asplAssetsListService.selectByAssetNo(contNo, assetNo);
            List<String> reqAssetNoList = new ArrayList<>();
            reqAssetNoList.add(asplAssetsList.getAssetNo());
            // 审批状态
            String apprStatus = "";

            apprStatus = CmisCommonConstants.WF_STATUS_997;
            // 1、解质押
            logger.info("开始出池解质押，资产清单："+Objects.toString(asplAssetsList));

            if(Objects.equals(CommonConstance.STD_ZB_YES_NO_0,asplAssetsList.getIsPledge())){
                xdzc0007DataRespDto.setOpFlag(CmisBizConstants.FAIL);//  失败
                xdzc0007DataRespDto.setOpMsg("资产编号："+Objects.toString(asplAssetsList.getAssetNo())+"还未被质押，无法解质押");// 描述信息
                return xdzc0007DataRespDto;
            }
            // TODO 0、出池且解质押 1、解质押 2、受托支付解质押
            if("0".equals(opType) || "2".equals(opType)) {
                // 2、出池
                logger.info("创建出池记录,修改资产状态");
                doAsplIoPool(asplAssetsList,ctrAsplDetails,apprStatus,xdzc0007DataReqDto,reqAssetNoList);
                // 更新客户资产清单
                asplAssetsListService.updateOutPollAsplAssets(asplAssetsList.getAssetNo(),contNo);
            }
            // 根据统一押品编号查询押品
            GuarBaseInfo guarBaseInfo = guarBaseInfoService.selectInfoByGuarNo(asplAssetsList.getGuarNo());
            // 通过资产编号（票号，权证编号）去查询权证信息
            QueryModel model = new QueryModel();
            model.addCondition("warrantNo",asplAssetsList.getAssetNo());//权证编号 票号 资产编号
            model.addCondition("guarNo",CmisBizConstants.STD_ZB_CERTI_STATE_04);// 入库状态
            model.setSort("input_date desc");//
            List<GuarWarrantInfo> records = guarWarrantInfoService.selectAll(model);
            if(CollectionUtils.isEmpty(records)){
                xdzc0007DataRespDto.setOpFlag("F");
                xdzc0007DataRespDto.setOpMsg("【权证信息获取失败】");
                return xdzc0007DataRespDto;
            }
            GuarWarrantInfo guarWarrantInfo = records.get(0);
            // 核心担保编号
            String coreGuarantyNo = guarWarrantInfo.getCoreGuarantyNo();
            // 核心记表外账出库(该接口目前支持单押品出池)
            Co3202ReqDto co3202ReqDto = getCo3202ReqDto(guarBaseInfo,asplAssetsList,guarWarrantInfo);
            // 发送押品核心出库接口
            ResultDto<Co3202RespDto> co3202ResultDto = dscms2CoreCoClientService.co3202(co3202ReqDto);
            // 判断核心 出库是否成功，更新担保权证出库申请
            if("0".equals(co3202ResultDto.getCode())){
                // 更新权证 担保合同 押品关系表 （资产池的关系表用完就逻辑删除）
                //根据核心担保编号获取权证信息更新权证信息出库
                logger.info(" 更新权证出库");
                guarWarrantInfo.setCertiState(CmisBizConstants.STD_ZB_CERTI_STATE_10);//权证状态
                guarWarrantInfoService.updateSelective(guarWarrantInfo);
            }else{
                xdzc0007DataRespDto.setOpFlag("F");
                xdzc0007DataRespDto.setOpMsg("【核心Co3202】:"+co3202ResultDto.getMessage());
                return xdzc0007DataRespDto;
            }
            // grtGuarContRelService.deleteByAssetNo(asplAssetsList.getAssetNo());
            // 营业时间
            String openDay = stringRedisTemplate.opsForValue().get("openDay");
            // 调押品出库 (发送certis接口)
            logger.info("调押品出库 (发送certis接口)");
            CertisReqDto certisReqDto = new CertisReqDto();
//            List<CertisListInfo> certisListInfoList = guarBaseInfoList.stream().map(e -> {
//                CertisListInfo certisListInfo = new CertisListInfo();
//                certisListInfo.setSernoy(e.getCoreGuarantyNo());//核心担保编号
//                certisListInfo.setQlpzhm(e.getGuarNo());//权利凭证号
            // 权证类型 (一期只有银票)
//                certisListInfo.setQzlxyp("53");
//                if ("01".equals(map.get(e.getGuarNo()))) {
//                    // 银行承兑汇票
//                    certisListInfo.setQzlxyp("53");
//                }else if ("02".equals(map.get(e.getGuarNo()))) {
//                    // 本行存单(电子)
//                    certisListInfo.setQzlxyp("40");
//                } else if ("04".equals(map.get(e.getGuarNo()))) {
//                    // 国内信用证
//                    certisListInfo.setQzlxyp("06");
//                }
//                certisListInfo.setQzztyp("10008");//权证状态(出库)
//                certisListInfo.setQzrkrq(StringUtil.EMPTY_STRING);//权证入库日期
//                certisListInfo.setQzckrq(openDay);//权证正常出库日期
//                certisListInfo.setQzjyrm(StringUtil.EMPTY_STRING);//权证临时借用人名称
//                certisListInfo.setQzwjrq(StringUtil.EMPTY_STRING);//权证外借日期
//                certisListInfo.setYjghrq(StringUtil.EMPTY_STRING);//预计归还日期
//                certisListInfo.setSjghrq(StringUtil.EMPTY_STRING);//权证实际归还日期
//                certisListInfo.setQzwjyy(StringUtil.EMPTY_STRING);//权证外借原因
//                certisListInfo.setQtwbsr(StringUtil.EMPTY_STRING);//其他文本输入
//                return certisListInfo;
//            }).collect(Collectors.toList());
            List<CertisListInfo> certisListInfoList = new ArrayList<CertisListInfo>();
            CertisListInfo certisListInfo = new CertisListInfo();
            certisListInfo.setSernoy(coreGuarantyNo);//核心担保编号
            certisListInfo.setQlpzhm(coreGuarantyNo);//权利凭证号
            // 权证类型 (一期只有银票)
            if ("01".equals(asplAssetsList.getAssetType())) {
                // 银行承兑汇票
                certisListInfo.setQzlxyp("53");
            }else if ("02".equals(asplAssetsList.getAssetType())) {
                // 本行存单(电子)
                certisListInfo.setQzlxyp("40");
            } else if ("04".equals(asplAssetsList.getAssetType())) {
                // 国内信用证
                certisListInfo.setQzlxyp("06");
            }
            certisListInfo.setQzztyp("10008");//权证状态(出库)
            certisListInfo.setQzrkrq(StringUtil.EMPTY_STRING);//权证入库日期
            certisListInfo.setQzckrq(openDay);//权证正常出库日期
            certisListInfo.setQzjyrm(StringUtil.EMPTY_STRING);//权证临时借用人名称
            certisListInfo.setQzwjrq(StringUtil.EMPTY_STRING);//权证外借日期
            certisListInfo.setYjghrq(StringUtil.EMPTY_STRING);//预计归还日期
            certisListInfo.setSjghrq(StringUtil.EMPTY_STRING);//权证实际归还日期
            certisListInfo.setQzwjyy(StringUtil.EMPTY_STRING);//权证外借原因
            certisListInfo.setQtwbsr(StringUtil.EMPTY_STRING);//其他文本输入
            certisListInfoList.add(certisListInfo);
            certisReqDto.setList(certisListInfoList);
            ResultDto<CertisRespDto> certisResultDto = dscms2YpxtClientService.certis(certisReqDto);
            logger.info("【certis】接口："+ Objects.toString(certisResultDto));
            if (!"0".equals(certisResultDto.getCode())) {
                //默认成功
                xdzc0007DataRespDto.setOpFlag(CmisBizConstants.FAIL);// 成功失败标志
                xdzc0007DataRespDto.setOpMsg("【certis】:"+certisResultDto.getMessage());// 描述信息
                return xdzc0007DataRespDto;
            }
            // 承兑行白名单恢复
            this.sendCmisLmt0025(xdzc0007DataReqDto,ctrAsplDetails);
            xdzc0007DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
            if("1".equals(opType)){
                xdzc0007DataRespDto.setOpMsg("解质押成功");// 描述信息
            }else{
                xdzc0007DataRespDto.setOpMsg("出池成功");// 描述信息
            }

        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }  catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value);
        return xdzc0007DataRespDto;
    }
    /**
     * 承兑行白名单额度恢复
     * @param
     * @return
     */
    public void sendCmisLmt0025(Xdzc0007DataReqDto xdzc0007DataReqDto, CtrAsplDetails ctrAsplDetails){
        List<CmisLmt0025OccRelListReqDto> occRelList = new ArrayList<CmisLmt0025OccRelListReqDto>();
        CmisLmt0025OccRelListReqDto dealBiz = new CmisLmt0025OccRelListReqDto();
        xdzc0007DataReqDto.getList().forEach(e->{
            dealBiz.setDealBizNo(e.getAssetNo());//交易业务编号
            occRelList.add(dealBiz);
        });
        CmisLmt0025ReqDto cmisLmt0025ReqDto = new CmisLmt0025ReqDto();
        cmisLmt0025ReqDto.setOccRelList(occRelList);
        // 系统编号
        cmisLmt0025ReqDto.setSysNo(EsbEnum.SERVTP_XDG.key);
        // 金融机构代码
        cmisLmt0025ReqDto.setInstuCde("001");
        // 合同编号
        // 合同编号
        cmisLmt0025ReqDto.setInputId(ctrAsplDetails.getManagerId());
        cmisLmt0025ReqDto.setInputBrId(ctrAsplDetails.getManagerBrId());
        cmisLmt0025ReqDto.setInputDate(DateUtils.getCurrDateStr());

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025ReqDto));
        ResultDto<CmisLmt0025RespDto> cmisLmt0025RespDtoResultDto = Optional.ofNullable(cmisLmtClientService.cmislmt0025(cmisLmt0025ReqDto)).orElse(new ResultDto<>());
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0025.value, JSON.toJSONString(cmisLmt0025RespDtoResultDto));
        if (Objects.equals(cmisLmt0025RespDtoResultDto.getCode(), "0")) {
            if(!Objects.equals(cmisLmt0025RespDtoResultDto.getData().getErrorCode(), "0000")){
                throw BizException.error(null, "9999",cmisLmt0025RespDtoResultDto.getData().getErrorMsg());
            }
        }else{
            throw BizException.error(null, "9999",cmisLmt0025RespDtoResultDto.getMessage());
        }
    }
    /**
     *  生成Co3202ReqDto对象(资产池 解质押-chuku )
     * */
    public Co3202ReqDto getCo3202ReqDto(GuarBaseInfo guarBaseInfo, AsplAssetsList asplAssetsList,GuarWarrantInfo guarWarrantInfo){
        // 是否票据池业务
        Co3202ReqDto co3202ReqDto = new Co3202ReqDto();
        co3202ReqDto.setDaikczbz("3");//业务操作标志（资产池） 1录入 2复核 3直通
        co3202ReqDto.setDzywbhao(guarWarrantInfo.getCoreGuarantyNo());// 核心担保编号
        co3202ReqDto.setDzywminc(guarBaseInfo.getPldimnMemo());//抵质押物名称
        co3202ReqDto.setDizyfshi("2");//抵质押方式
        co3202ReqDto.setChrkleix("2");//出入库类型
        co3202ReqDto.setSyqrkehh(guarBaseInfo.getGuarCusId());//所有权人客户号
        co3202ReqDto.setSyqrkehm(guarBaseInfo.getGuarCusName());//所有权人客户名
        co3202ReqDto.setRuzjigou(guarWarrantInfo.getFinaBrId());//入账机构
        co3202ReqDto.setHuobdhao(toConverCurrency(guarBaseInfo.getCurType()));//货币代号
        co3202ReqDto.setMinyjiaz(guarBaseInfo.getConfirmAmt());//名义价值
        co3202ReqDto.setShijjiaz(guarBaseInfo.getConfirmAmt());//实际价值
        co3202ReqDto.setDizybilv(guarBaseInfo.getMortagageRate()); //抵质押比率
        co3202ReqDto.setKeyongje(null);//可用金额
        co3202ReqDto.setShengxrq(guarBaseInfo.getInputDate().replace("-", "").substring(0,8));//生效日期
        co3202ReqDto.setDaoqriqi(asplAssetsList.getAssetEndDate().replace("-", "").substring(0,8));// 到期日期
        co3202ReqDto.setDzywztai("2");//抵质押物状态
        co3202ReqDto.setZhaiyoms(""); //摘要
        return co3202ReqDto;
    }
    /*
     * 新核心改造 币种映射 信贷--->核心
     * @param xdbz 信贷币种码值
     * @return hxbz 核心币种码值
     */
    public static String toConverCurrency(String xdbz){
        String hxbz = "";
        if("CNY".equals(xdbz)){
            hxbz = "01";
        }else if("MOP".equals(xdbz)){//澳门币
            hxbz = "81";
        }else if("CAD".equals(xdbz)){//加元
            hxbz = "28";
        }else if("CHF".equals(xdbz)){//瑞士法郎
            hxbz = "15";
        }else if("JPY".equals(xdbz)){//日元
            hxbz = "27";
        }else if("EUR".equals(xdbz)){//欧元
            hxbz = "38";
        }else if("GBP".equals(xdbz)){//英镑
            hxbz = "12";
        }else if("HKD".equals(xdbz)){//港币
            hxbz = "13";
        }else if("AUD".equals(xdbz)){//澳元
            hxbz = "29";
        }else if("USD".equals(xdbz)){//美元
            hxbz = "14";
        }else if("SGD".equals(xdbz)){//新加坡元
            hxbz = "18";
        }else if("SEK".equals(xdbz)){//瑞典克郎
            hxbz = "21";
        }else if("DKK".equals(xdbz)){//丹麦克朗
            hxbz = "22";
        }else if("NOK".equals(xdbz)){//挪威克朗
            hxbz = "23";
        }else{
            hxbz = xdbz;//未匹配到的币种发信贷的币种过去（DEM 德国马克;MSD 克鲁赛罗;NLG 荷兰盾;BEF 比利时法郎;ITL 意大利里拉;FRF 法国法郎;ATS 奥地利先令;FIM 芬兰马克）
        }
        return hxbz;
    }
    public void doAsplIoPool(AsplAssetsList asplAssetsList,CtrAsplDetails ctrAsplDetails,String apprStatus,Xdzc0007DataReqDto xdzc0007DataReqDto,List<String> reqAssetNoList){
        String contNo = ctrAsplDetails.getContNo();
        String serno = xdzc0007DataReqDto.getSerno();
        String openDay = stringRedisTemplate.opsForValue().get("openDay");
        // 判断是否票据托收
        if(Objects.equals("2",xdzc0007DataReqDto.getOpType())){
            serno = "ZCCPJTS" + openDay.replace("-","");
        }else {
            // 出池额度校验（同时也会判断这些资产是否都是池内资产）
            OpRespDto opRespDto = asplAssetsListService.isOupPoolVaild(contNo, reqAssetNoList);
            logger.info("出池额度校验:"+Objects.toString(opRespDto));
            if (CmisBizConstants.FAIL.equals(opRespDto.getOpFlag())) {
                throw BizException.error(null, "9999", "出池额度校验未通过");
            }
        }
        String assetNo = asplAssetsList.getAssetNo();
        // 资产出入池明细表
        List<AsplIoPoolDetails> asplIoPoolDetailsList = new ArrayList<AsplIoPoolDetails>();
        // 判断资产池出入池记录是否存在
        AsplIoPool asplIoPool = asplIoPoolService.queryAsplIoPoolBySerno(serno);
        if(Objects.isNull(asplIoPool)){
            // 如果不存在，则新增，需要校验额度（这种情况一般都是 由网银客户端发起的出池）、
            asplIoPool = new AsplIoPool();
            asplIoPool.setPkId(UUID.randomUUID().toString().replace("-",""));//主键
            asplIoPool.setSerno(serno);//业务流水号
            asplIoPool.setInoutType(CmisCommonConstants.INOUT_TYPE_0);// 出入类型(出池)
            asplIoPool.setLmtAmt(BigDecimal.ZERO);// 出池额度
            asplIoPool.setOutstndAmt(BigDecimal.ZERO);//已用额度
            asplIoPool.setApprStatus(apprStatus);//审批状态(入池不需要走审批，默认通过)
            asplIoPool.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
            asplIoPool.setInputDate(openDay);//登记日期
            asplIoPool.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//最近修改日期
            asplIoPool.setCreateTime(DateUtils.getCurrDate());//创建时间
            asplIoPool.setUpdateTime(DateUtils.getCurrDate());//修改时间
            // 判断是票据托收，还是正常出池
            if(!Objects.equals("2",xdzc0007DataReqDto.getOpType())){
                asplIoPool.setContNo(contNo);//合同编号
                asplIoPool.setCusId(ctrAsplDetails.getCusId());// 客户编号
                asplIoPool.setCusName(ctrAsplDetails.getCusName());// 客户名称
                asplIoPool.setResn(xdzc0007DataReqDto.getResn());//原因
                asplIoPool.setStartDate(openDay);//起始日期
                asplIoPool.setEndDate(openDay);//到期日期
                asplIoPool.setInputId(ctrAsplDetails.getInputId());//登记人
                asplIoPool.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
                asplIoPool.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
                asplIoPool.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
                asplIoPool.setManagerId(ctrAsplDetails.getManagerId());//主管客户经理
                asplIoPool.setManagerBrId(ctrAsplDetails.getManagerBrId());//主管机构
            }else{
                asplIoPool.setContNo("ZCCPJCTS");//合同编号
                asplIoPool.setCusId("");// 客户编号
                asplIoPool.setCusName("票据托收（虚拟用户）");// 客户名称
                asplIoPool.setResn("票据到期自动托收");//原因
                asplIoPool.setInputId("admin");//登记人
                asplIoPool.setInputBrId("019800");//登记机构
                asplIoPool.setUpdId("admin");//最近修改人
                asplIoPool.setUpdBrId("019800");//最近修改机构
                asplIoPool.setManagerId("admin");//主管客户经理
                asplIoPool.setManagerBrId("019800");//主管机构
            }
            // 插入出入池表
            asplIoPoolService.insertSelective(asplIoPool);
        }
        // 判断是否存在出池明细
        QueryModel model = new QueryModel();
        model.addCondition("serno",serno);
        model.addCondition("assetNo",assetNo);
        model.addCondition("contNo",contNo);
        List<AsplIoPoolDetails> lists = asplIoPoolDetailsService.selectAll(model);
        // 如果不存在 新增明细 修改金额 ，存在则不操作
        if(CollectionUtils.isEmpty(lists)){
            AsplIoPoolDetails asplIoPoolDetails = new AsplIoPoolDetails();
            asplIoPoolDetails.setPkId(UUID.randomUUID().toString().replace("-",""));//主键
            asplIoPoolDetails.setSerno(serno);//业务流水号
            asplIoPoolDetails.setContNo(contNo);//合同编号
            asplIoPoolDetails.setAssetNo(asplAssetsList.getAssetNo());//资产编号
            asplIoPoolDetails.setAssetType(asplAssetsList.getAssetType());//资产类型
            asplIoPoolDetails.setAssetValue(asplAssetsList.getAssetValue());//资产价值
            asplIoPoolDetails.setAssetEndDate(asplAssetsList.getAssetEndDate());//资产到期日
            asplIoPoolDetails.setAssetStatus(asplAssetsList.getAssetStatus());//资产状态
            asplIoPoolDetails.setIsPool(CmisCommonConstants.INOUT_TYPE_0);//是否入池（出池）
            asplIoPoolDetails.setIsPledge(CmisCommonConstants.YES_NO_0);// 是否解质押（0 是 1 否）
            asplIoPoolDetails.setOprType(CmisCommonConstants.OPR_TYPE_ADD);//操作类型
            asplIoPoolDetails.setInputDate(openDay);//登记日期
            asplIoPoolDetails.setUpdDate(openDay);//最近修改日期
            asplIoPoolDetails.setCreateTime(DateUtils.getCurrDate());//创建时间
            asplIoPoolDetails.setUpdateTime(DateUtils.getCurrDate());//修改时间
            asplIoPoolDetailsList.add(asplIoPoolDetails);
            if(!Objects.equals("2",xdzc0007DataReqDto.getOpType())){
                asplIoPoolDetails.setInputId(ctrAsplDetails.getInputId());//登记人
                asplIoPoolDetails.setInputBrId(ctrAsplDetails.getInputBrId());//登记机构
                asplIoPoolDetails.setUpdId(ctrAsplDetails.getUpdId());//最近修改人
                asplIoPoolDetails.setUpdBrId(ctrAsplDetails.getUpdBrId());//最近修改机构
            }else{
                asplIoPoolDetails.setInputId("admin");//登记人
                asplIoPoolDetails.setInputBrId("019800");//登记机构
                asplIoPoolDetails.setUpdId("admin");//最近修改人
                asplIoPoolDetails.setUpdBrId("019800");//最近修改机构
            }
            // 插入出入池明细表
            asplIoPoolDetailsService.insertAsplIoPoolDetailsList(asplIoPoolDetailsList);
            // 更新批次总额金额
            BigDecimal lmtAmt = asplIoPool.getLmtAmt();
            asplIoPool.setLmtAmt(lmtAmt.add(asplAssetsList.getAssetValue()));
            asplIoPool.setUpdateTime(DateUtils.getCurrDate());//修改时间
            asplIoPool.setUpdDate(DateUtils.getCurrentDate(DateFormatEnum.DEFAULT));//最近修改日期
            // 更新出入池批次记录
            asplIoPoolService.updateSelective(asplIoPool);
        }
    }
}