/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.PvpLoanApp;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGrrLimitEval;
import cn.com.yusys.yusp.service.LmtGrrLimitEvalService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrrLimitEvalResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-07-31 17:50:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtgrrlimiteval")
public class LmtGrrLimitEvalResource {
    @Autowired
    private LmtGrrLimitEvalService lmtGrrLimitEvalService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGrrLimitEval>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGrrLimitEval> list = lmtGrrLimitEvalService.selectAll(queryModel);
        return new ResultDto<List<LmtGrrLimitEval>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGrrLimitEval>> index(QueryModel queryModel) {
        List<LmtGrrLimitEval> list = lmtGrrLimitEvalService.selectByModel(queryModel);
        return new ResultDto<List<LmtGrrLimitEval>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGrrLimitEval> show(@PathVariable("pkId") String pkId) {
        LmtGrrLimitEval lmtGrrLimitEval = lmtGrrLimitEvalService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGrrLimitEval>(lmtGrrLimitEval);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGrrLimitEval> create(@RequestBody LmtGrrLimitEval lmtGrrLimitEval) throws URISyntaxException {
        lmtGrrLimitEvalService.insert(lmtGrrLimitEval);
        return new ResultDto<LmtGrrLimitEval>(lmtGrrLimitEval);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrrLimitEval lmtGrrLimitEval) throws URISyntaxException {
        int result = lmtGrrLimitEvalService.update(lmtGrrLimitEval);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGrrLimitEvalService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGrrLimitEvalService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @创建人: zhanyb
     */
    @ApiOperation("保证人担保额度测算表查看详情")
    @PostMapping("/showdetial")
    protected ResultDto<Object> showdetial(@RequestBody Map params) {
        ResultDto<Object> resultDto = new ResultDto<Object>();
        LmtGrrLimitEval temp = new LmtGrrLimitEval();
        LmtGrrLimitEval studyDemo = lmtGrrLimitEvalService.selectByPrimaryKey((String)params.get(("pkId")));
        if (studyDemo != null) {
            resultDto.setCode(200);
            resultDto.setData(studyDemo);
            resultDto.setMessage("查询成功！");
        } else {
            resultDto.setCode(200);
            resultDto.setMessage("无对应的数据！");
        }
        return resultDto;
    }

    /**
     * @函数名称:queryModel
     * @函数描述:授信保证人担保额度测算表查询分页查询
     * @参数与返回说明:queryModel
     * @创建人:zhanyb
     */
    @ApiOperation("授信保证人担保额度测算表查询")
    @PostMapping("/queryLmtgrrlimiteval")
    protected ResultDto<List<LmtGrrLimitEval>> queryLmtGrrLimitEval(@RequestBody QueryModel queryModel) {
        List<LmtGrrLimitEval> list = lmtGrrLimitEvalService.queryLmtGrrLimitEval(queryModel);
        return new ResultDto<List<LmtGrrLimitEval>>(list);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("授信保证人担保额度测算表新增")
    @PostMapping("/savelmtgrrlimiteval")
    protected ResultDto<Map> saveLmtGrrLimitEval(@RequestBody LmtGrrLimitEval lmtGrrLimitEval) throws URISyntaxException {
        ResultDto<LmtGrrLimitEval> resultDto = new ResultDto<LmtGrrLimitEval>();
        Map result = lmtGrrLimitEvalService.saveLmtGrrLimitEval(lmtGrrLimitEval);
        return  new ResultDto<>(result);
    }

    /**
     * @函数名称:params
     * @函数描述:授信保证人担保额度测算表修改
     * @参数与返回说明:
     * @创建人: zhanyb
     */
    @ApiOperation("授信保证人担保额度测算表修改")
    @PostMapping("/updatelmtgrrlimiteval")
    public ResultDto<Map> updateLmtGrrLimitEval(@RequestBody LmtGrrLimitEval lmtGrrLimitEval){
        Map rtnData = lmtGrrLimitEvalService.updateLmtGrrLimitEval(lmtGrrLimitEval);
        return new ResultDto<>(rtnData);
    }

    /**
     * 初始化引入分项保证人数据
     *
     * @param serno
     * @return
     */
    @PostMapping("/initLmtGrrLimitEval")
    protected ResultDto<Integer> initLmtGrrLimitEval(@RequestBody String serno){
        return new ResultDto<Integer>(lmtGrrLimitEvalService.initLmtGrrLimitEval(serno));
    }
}
