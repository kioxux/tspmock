package cn.com.yusys.yusp.web.server.xdsx0008;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdsx0008.req.Xdsx0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0008.resp.Xdsx0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdsx0008.Xdsx0008Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;


/**
 * 接口处理类:单一客户人工限额同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDSX0008:单一客户人工限额同步")
@RestController
@RequestMapping("/api/bizsx4bsp")
public class BizXdsx0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdsx0008Resource.class);

    @Autowired
    private Xdsx0008Service xdsx0008Service;

    /**
     * 交易码：xdsx0008
     * 交易描述：单一客户人工限额同步
     *
     * @param xdsx0008DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdsx0008:单一客户人工限额同步")
    @PostMapping("/xdsx0008")
    protected @ResponseBody
    ResultDto<Xdsx0008DataRespDto> xdsx0008(@Validated @RequestBody Xdsx0008DataReqDto xdsx0008DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008DataReqDto));
        Xdsx0008DataRespDto xdsx0008DataRespDto = new Xdsx0008DataRespDto();// 响应Dto:单一客户人工限额同步
        ResultDto<Xdsx0008DataRespDto> xdsx0008DataResultDto = new ResultDto<>();
        // 从xdsx0008DataReqDto获取业务值进行业务逻辑处理
        String cusId = xdsx0008DataReqDto.getCusId();//客户编号
        BigDecimal singleCusQuota = xdsx0008DataReqDto.getSingleCusQuota();//单一客户人工限额
        Integer dtghFlag = xdsx0008DataReqDto.getDtghFlag();//区分标识
        try {
            if (CmisBizConstants.INT_ONE.equals(dtghFlag)) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008DataReqDto));
                xdsx0008Service.updateByCusId(xdsx0008DataReqDto);
            }
            if (CmisBizConstants.INT_TWO.equals(dtghFlag)) {
                logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008DataReqDto));
                xdsx0008Service.updateByCusId(xdsx0008DataReqDto);
            }
            xdsx0008DataRespDto.setOpFlag(CmisBizConstants.SUCCESS);// 成功失败标志
            xdsx0008DataRespDto.setOpMsg(CmisBizConstants.YES_MESSAGE);// 描述信息
            // 封装xdsx0008DataResultDto中正确的返回码和返回信息
            xdsx0008DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdsx0008DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, e.getMessage());
            // 封装xdsx0008DataResultDto中异常返回码和返回信息
            xdsx0008DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdsx0008DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdsx0008DataRespDto到xdsx0008DataResultDto中
        xdsx0008DataResultDto.setData(xdsx0008DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008DataResultDto));
        return xdsx0008DataResultDto;
    }
}