package cn.com.yusys.yusp.web.server.xdxw0001;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdxw0001.req.Xdxw0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0001.resp.Xdxw0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdxw0001.Xdxw0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:房屋估价信息同步
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDXW0001:房屋估价信息同步")
@RestController
@RequestMapping("/api/bizxw4bsp")
public class BizXdxw0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdxw0001Resource.class);

    @Autowired
    private Xdxw0001Service xdxw0001Service;

    /**
     * 交易码：xdxw0001
     * 交易描述：房屋估价信息同步
     *
     * @param xdxw0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("房屋估价信息同步")
    @PostMapping("/xdxw0001")
    protected @ResponseBody
    ResultDto<Xdxw0001DataRespDto> xdxw0001(@Validated @RequestBody Xdxw0001DataReqDto xdxw0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001DataReqDto));
        Xdxw0001DataRespDto xdxw0001DataRespDto = new Xdxw0001DataRespDto();// 响应Dto:房屋估价信息同步
        ResultDto<Xdxw0001DataRespDto> xdxw0001DataResultDto = new ResultDto<>();
        try {
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001DataReqDto));
            xdxw0001DataRespDto = xdxw0001Service.getXdxw0001(xdxw0001DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001DataRespDto));
            // 封装xdxw0001DataResultDto中正确的返回码和返回信息
            xdxw0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdxw0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, e.getMessage());
            xdxw0001DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdxw0001DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            xdxw0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, e.getMessage());
            // 封装xdxw0001DataResultDto中异常返回码和返回信息
            xdxw0001DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xdxw0001DataRespDto.setOpMsg(CommonConstance.OP_MSG_F);
            xdxw0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdxw0001DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdxw0001DataRespDto到xdxw0001DataResultDto中
        xdxw0001DataResultDto.setData(xdxw0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001DataResultDto));
        return xdxw0001DataResultDto;
    }
}
