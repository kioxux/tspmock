/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtFinSpApp;
import cn.com.yusys.yusp.service.LmtFinSpAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtFinSpAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: ASUS
 * @创建时间: 2021-02-03 09:44:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtfinspapp")
public class LmtFinSpAppResource {
    @Autowired
    private LmtFinSpAppService lmtFinSpAppService;

	/**
     * 全表查询.
     * 
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtFinSpApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtFinSpApp> list = lmtFinSpAppService.selectAll(queryModel);
        return new ResultDto<List<LmtFinSpApp>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtFinSpApp>> index(QueryModel queryModel) {
        List<LmtFinSpApp> list = lmtFinSpAppService.selectByModel(queryModel);
        return new ResultDto<List<LmtFinSpApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<LmtFinSpApp> show(@PathVariable("serno") String serno) {
        LmtFinSpApp lmtFinSpApp = lmtFinSpAppService.selectByPrimaryKey(serno);
        return new ResultDto<LmtFinSpApp>(lmtFinSpApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtFinSpApp> create(@RequestBody LmtFinSpApp lmtFinSpApp) throws URISyntaxException {
        lmtFinSpAppService.insert(lmtFinSpApp);
        return new ResultDto<LmtFinSpApp>(lmtFinSpApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtFinSpApp lmtFinSpApp) throws URISyntaxException {
        int result = lmtFinSpAppService.update(lmtFinSpApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = lmtFinSpAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtFinSpAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:checkIsExistLmtFinSpAppBySerno
     * @函数描述:提交，校验
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/checkIsExistLmtFinSpAppBySerno")
    protected ResultDto<Integer> checkIsExistLmtFinSpAppBySerno(@RequestBody LmtFinSpApp lmtFinSpApp) {
        int result = lmtFinSpAppService.checkIsExistLmtFinSpAppBySerno(lmtFinSpApp);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:deleteLmtFinSpAppBySerno
     * @函数描述:逻辑删除
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deleteLmtFinSpAppBySerno")
    protected ResultDto<Boolean> deleteLmtFinSpAppBySerno(@RequestBody LmtFinSpApp lmtFinSpApp) {
        Boolean result = lmtFinSpAppService.deleteLmtFinSpAppBySerno(lmtFinSpApp);
        return new ResultDto<Boolean>(result);
    }

    /**
     * @函数名称:reCalCurtCurSubpayQntBySerno
     * @函数描述:重新计算代偿笔数
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/reCalCurtCurSubpayQntBySerno/{serno}")
    protected ResultDto<Boolean> reCalCurtCurSubpayQntBySerno(@PathVariable String serno) {
        Boolean result = lmtFinSpAppService.reCalCurtCurSubpayQntBySerno(serno);
        return new ResultDto<Boolean>(result);
    }
}
