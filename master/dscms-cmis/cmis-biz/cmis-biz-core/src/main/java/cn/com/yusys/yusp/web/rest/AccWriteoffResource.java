/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.AccWriteoff;
import cn.com.yusys.yusp.service.AccWriteoffService;

/**
 * @项目名称: cmis-biz模块
 * @类名称: AccWriteoffResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: mashun
 * @创建时间: 2021-01-19 19:41:22
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/accwriteoff")
public class AccWriteoffResource {
    @Autowired
    private AccWriteoffService accWriteoffService;

    /**
     * 全表查询.
     *
     * @param id
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<AccWriteoff>> query() {
        QueryModel queryModel = new QueryModel();
        List<AccWriteoff> list = accWriteoffService.selectAll(queryModel);
        return new ResultDto<List<AccWriteoff>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param QueryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<AccWriteoff>> index(QueryModel queryModel) {
        List<AccWriteoff> list = accWriteoffService.selectByModel(queryModel);
        return new ResultDto<List<AccWriteoff>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{accNo}")
    protected ResultDto<AccWriteoff> show(@PathVariable("accNo") String accNo) {
        AccWriteoff accWriteoff = accWriteoffService.selectByPrimaryKey(accNo);
        return new ResultDto<AccWriteoff>(accWriteoff);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<AccWriteoff> create(@RequestBody AccWriteoff accWriteoff) throws URISyntaxException {
        accWriteoffService.insert(accWriteoff);
        return new ResultDto<AccWriteoff>(accWriteoff);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody AccWriteoff accWriteoff) throws URISyntaxException {
        int result = accWriteoffService.update(accWriteoff);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{accNo}")
    protected ResultDto<Integer> delete(@PathVariable("accNo") String accNo) {
        int result = accWriteoffService.deleteByPrimaryKey(accNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = accWriteoffService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }
}
