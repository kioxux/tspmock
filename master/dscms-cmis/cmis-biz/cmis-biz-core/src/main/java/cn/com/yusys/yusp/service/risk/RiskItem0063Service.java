package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpContExtBill;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.service.IqpContExtBillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0063Service
 * @类描述: 展期次数校验
 * @功能描述: 展期次数校验
 * @创建人: macm
 * @创建时间: 2021年7月28日23:32:20
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0063Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0062Service.class);

    @Autowired
    private IqpContExtBillService iqpContExtBillService;

    /**
     * @方法名称: riskItem0063
     * @方法描述: 展期次数校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: macm
     * @创建时间: 2021年7月28日23:32:20
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0063(QueryModel queryModel) {
        String serno = queryModel.getCondition().get("bizId").toString();
        log.info("展期次数开始*******************业务流水号：【{}】",serno);
        RiskResultDto riskResultDto = new RiskResultDto();
        // 执行结果描述
        String riskResultDesc ="";
        if (StringUtils.isEmpty(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        //  根据流水号获取展期申请中借据列表
        QueryModel queryMap = new QueryModel();
        queryMap.addCondition("iqpSerno",serno);
        List<IqpContExtBill> list = iqpContExtBillService.selectByModel(queryMap);
        if(CollectionUtils.isEmpty(list)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_06201);
            return riskResultDto;
        }

        for(IqpContExtBill iqpContExtBill :list) {
            riskResultDesc += "借据["+iqpContExtBill.getBillNo()+"]展期次数:"+iqpContExtBill.getExtTimes() +".";
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
        riskResultDto.setRiskResultDesc(riskResultDesc);
        return riskResultDto;
    }
}
