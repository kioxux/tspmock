/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.service.IqpRepayWayChgService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.IqpRepayPlanDtl;
import cn.com.yusys.yusp.service.IqpRepayPlanDtlService;

/**
 * @项目名称: cmis-biz模块
 * @类名称: IqpRepayPlanDtlResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: 乐友先生
 * @创建时间: 2021-02-03 15:10:27
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/iqprepayplandtl")
public class IqpRepayPlanDtlResource {
    @Autowired
    private IqpRepayPlanDtlService iqpRepayPlanDtlService;
    private static final Logger log = LoggerFactory.getLogger(IqpRepayWayChgService.class);

    /**
     * 全表查询.
     * 
     * @param
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<IqpRepayPlanDtl>> query() {
        QueryModel queryModel = new QueryModel();
        List<IqpRepayPlanDtl> list = iqpRepayPlanDtlService.selectAll(queryModel);
        return new ResultDto<List<IqpRepayPlanDtl>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<IqpRepayPlanDtl>> index(QueryModel queryModel) {
        List<IqpRepayPlanDtl> list = iqpRepayPlanDtlService.selectByModel(queryModel);
        return new ResultDto<List<IqpRepayPlanDtl>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<IqpRepayPlanDtl> show(@PathVariable("pkId") String pkId) {
        IqpRepayPlanDtl iqpRepayPlanDtl = iqpRepayPlanDtlService.selectByPrimaryKey(pkId);
        return new ResultDto<IqpRepayPlanDtl>(iqpRepayPlanDtl);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<IqpRepayPlanDtl> create(@RequestBody IqpRepayPlanDtl iqpRepayPlanDtl) throws URISyntaxException {
        iqpRepayPlanDtlService.insert(iqpRepayPlanDtl);
        return new ResultDto<IqpRepayPlanDtl>(iqpRepayPlanDtl);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody IqpRepayPlanDtl iqpRepayPlanDtl) throws URISyntaxException {
        int result = iqpRepayPlanDtlService.update(iqpRepayPlanDtl);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = iqpRepayPlanDtlService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = iqpRepayPlanDtlService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     *
     * @param iqpRepayPlanDtl
     * @return
     */
    @PostMapping("/saveReyPlanDtl")
    protected ResultDto<Integer> saveReyPlanDtl(@RequestBody IqpRepayPlanDtl iqpRepayPlanDtl) throws  URISyntaxException{
        log.info("新增还款计划信息数据【{}】", JSONObject.toJSON(iqpRepayPlanDtl));
        iqpRepayPlanDtl.setOprType("01");
        int result = iqpRepayPlanDtlService.insertIqpRepayPlanDtl(iqpRepayPlanDtl);
        return new ResultDto<Integer>(result);
    }
}
