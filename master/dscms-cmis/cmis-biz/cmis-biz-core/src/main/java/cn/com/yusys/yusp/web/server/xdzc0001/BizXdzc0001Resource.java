package cn.com.yusys.yusp.web.server.xdzc0001;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdzc0001.req.Xdzc0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0001.resp.Xdzc0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdzc0001.Xdzc0001Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:客户资产池协议列表查询
 *
 * @author xs
 * @version 1.0
 */
@Api(tags = "XDZC0001:客户资产池协议列表查询")
@RestController
@RequestMapping("/api/bizzc4bsp")
public class BizXdzc0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdzc0001Resource.class);
    @Autowired
    private Xdzc0001Service xdzc0001Service;
    /**
     * 交易码：xdzc0001
     * 交易描述：客户资产池协议列表查询
     *
     * @param xdzc0001DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户资产池协议列表查询")
    @PostMapping("/xdzc0001")
    protected @ResponseBody
    ResultDto<Xdzc0001DataRespDto> xdzc0001(@Validated @RequestBody Xdzc0001DataReqDto xdzc0001DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, JSON.toJSONString(xdzc0001DataReqDto));
        Xdzc0001DataRespDto xdzc0001DataRespDto = new Xdzc0001DataRespDto();// 响应Dto:客户资产池协议列表查询
        ResultDto<Xdzc0001DataRespDto> xdzc0001DataResultDto = new ResultDto<>();
        try {
            // 从xdzc0001DataReqDto获取业务值进行业务逻辑处理
            xdzc0001DataRespDto = xdzc0001Service.xdzc0001Service(xdzc0001DataReqDto);
            // 封装xdzc0001DataResultDto中正确的返回码和返回信息
            xdzc0001DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdzc0001DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, e.getMessage());
            xdzc0001DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdzc0001DataResultDto.setMessage(e.getMessage());
        }
        // 封装xdzc0001DataRespDto到xdzc0001DataResultDto中
        xdzc0001DataResultDto.setData(xdzc0001DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, JSON.toJSONString(xdzc0001DataResultDto));
        return xdzc0001DataResultDto;
    }
}
