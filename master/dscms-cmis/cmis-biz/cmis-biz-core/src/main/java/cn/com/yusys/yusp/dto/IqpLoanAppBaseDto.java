package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @className IqpLoanAppBaseDto
 * @Description 单笔单批业务申请 基础信息DTO
 * @Date 2020/12/3 : 14:52
 */

public class IqpLoanAppBaseDto implements Serializable {
    private String  iqpSerno         ;
    private String  serno             ;
    private String  appDate          ;
    private String  isAuthorize      ;
    private String  authedName       ;
    private String  authedCertCode  ;
    private String  authedCertType  ;
    private String  authedTelNo     ;
    private String  appApplCode     ;
    private String  iqpChnlSour     ;
    private String  equipNo          ;
    private String  prdId            ;
    private String  prdName          ;
    private String  loanModal        ;
    private String  bizType          ;
    private String  loanCha          ;
    private String  loanUse          ;
    private String  guarWay          ;
    private String  thirdLimitType  ;
    private String  thirdLimitId    ;
    private String  thirdLimitName  ;
    private String  isGreenType     ;
    private String  bizPri           ;
    private String  isCfirmPayWay  ;
    private String  defrayMode       ;
    private String  appCurType      ;
    private BigDecimal  appAmt       ;
    private BigDecimal  appRate      ;
    private BigDecimal  appRmbAmt   ;
    private String  bailSour         ;
    private BigDecimal  bailRate     ;
    private String  bailCurType     ;
    private BigDecimal  securityAmt  ;
    private BigDecimal  exchangeRate ;
    private BigDecimal  bailRmbAmt  ;
    private BigDecimal  riskOpenAmt ;
    private String  termType         ;
    private BigDecimal appTerm          ;
    private String  irAccordType    ;
    private String  irType           ;
    private BigDecimal rulingIr     ;
    private BigDecimal  rulingIrM  ;
    private String  loanRatType     ;
    private String  irAdjustType    ;
    private BigDecimal  irAdjustTerm;
    private String  praType          ;
    private String  rateType         ;
    private String  irFloatType     ;
    private BigDecimal  irFloatPoint;
    private BigDecimal  irFloatRate ;
    private BigDecimal  realityIrY  ;
    private BigDecimal  realityIrM  ;
    private String  overdueFloatType;
    private BigDecimal  overdueRate  ;
    private BigDecimal  overduePoint ;
    private BigDecimal  overdueRateY;
    private String  defaultFloatType;
    private BigDecimal  defaultRate  ;
    private BigDecimal  defaultPoint ;
    private BigDecimal  defaultRateY;
    private String      chnlSour;
    private String oldIqpSerno;
    private String isReconsid;

    private String cusId;//客户编号
    private String approveStatus;//申请状态
    private String oprType;//操作类型
    private String inputId;//登记人
    private String inputBrId;//登记机构
    private String inputDate;//登记时间
    private String updId;//最后修改人
    private String updBrId;//最后修改机构
    private String updDate;//最后修改时间
    private String especBizType;//特殊业务类型


    public String getOldIqpSerno() {
        return oldIqpSerno;
    }

    public void setOldIqpSerno(String oldIqpSerno) {
        this.oldIqpSerno = oldIqpSerno;
    }

    public String getIsReconsid() {
        return isReconsid;
    }

    public void setIsReconsid(String isReconsid) {
        this.isReconsid = isReconsid;
    }

    public String getUpdDate() { return updDate; }
    public void setUpdDate(String updDate) { this.updDate = updDate; }

    public String getUpdBrId() { return updBrId; }
    public void setUpdBrId(String updBrId) { this.updBrId = updBrId; }

    public String getUpdId() { return updId; }
    public void setUpdId(String updId) { this.updId = updId; }

    public String getInputDate() { return inputDate; }
    public void setInputDate(String inputDate) { this.inputDate = inputDate; }

    public String getInputBrId() { return inputBrId; }
    public void setInputBrId(String inputBrId) { this.inputBrId = inputBrId; }

    public String getInputId() { return inputId; }
    public void setInputId(String inputId) { this.inputId = inputId; }

    public String getOprType() { return oprType; }
    public void setOprType(String oprType) { this.oprType = oprType; }

    public String getApproveStatus() { return approveStatus; }
    public void setApproveStatus(String approveStatus) { this.approveStatus = approveStatus; }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getAppDate() {
        return appDate;
    }

    public void setAppDate(String appDate) {
        this.appDate = appDate;
    }

    public String getIsAuthorize() {
        return isAuthorize;
    }

    public void setIsAuthorize(String isAuthorize) {
        this.isAuthorize = isAuthorize;
    }

    public String getAuthedName() {
        return authedName;
    }

    public void setAuthedName(String authedName) {
        this.authedName = authedName;
    }

    public String getAuthedCertCode() {
        return authedCertCode;
    }

    public void setAuthedCertCode(String authedCertCode) {
        this.authedCertCode = authedCertCode;
    }

    public String getAuthedCertType() {
        return authedCertType;
    }

    public void setAuthedCertType(String authedCertType) {
        this.authedCertType = authedCertType;
    }

    public String getAuthedTelNo() {
        return authedTelNo;
    }

    public void setAuthedTelNo(String authedTelNo) {
        this.authedTelNo = authedTelNo;
    }

    public String getAppApplCode() {
        return appApplCode;
    }

    public void setAppApplCode(String appApplCode) {
        this.appApplCode = appApplCode;
    }

    public String getIqpChnlSour() {
        return iqpChnlSour;
    }

    public void setIqpChnlSour(String iqpChnlSour) {
        this.iqpChnlSour = iqpChnlSour;
    }

    public String getEquipNo() {
        return equipNo;
    }

    public void setEquipNo(String equipNo) {
        this.equipNo = equipNo;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getLoanCha() {
        return loanCha;
    }

    public void setLoanCha(String loanCha) {
        this.loanCha = loanCha;
    }

    public String getLoanUse() {
        return loanUse;
    }

    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse;
    }

    public String getGuarWay() {
        return guarWay;
    }

    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay;
    }

    public String getThirdLimitType() {
        return thirdLimitType;
    }

    public void setThirdLimitType(String thirdLimitType) {
        this.thirdLimitType = thirdLimitType;
    }

    public String getThirdLimitId() {
        return thirdLimitId;
    }

    public void setThirdLimitId(String thirdLimitId) {
        this.thirdLimitId = thirdLimitId;
    }

    public String getThirdLimitName() {
        return thirdLimitName;
    }

    public void setThirdLimitName(String thirdLimitName) {
        this.thirdLimitName = thirdLimitName;
    }

    public String getIsGreenType() {
        return isGreenType;
    }

    public void setIsGreenType(String isGreenType) {
        this.isGreenType = isGreenType;
    }

    public String getBizPri() {
        return bizPri;
    }

    public void setBizPri(String bizPri) {
        this.bizPri = bizPri;
    }

    public String getIsCfirmPayWay() {
        return isCfirmPayWay;
    }

    public void setIsCfirmPayWay(String isCfirmPayWay) {
        this.isCfirmPayWay = isCfirmPayWay;
    }

    public String getDefrayMode() {
        return defrayMode;
    }

    public void setDefrayMode(String defrayMode) {
        this.defrayMode = defrayMode;
    }

    public String getAppCurType() {
        return appCurType;
    }

    public void setAppCurType(String appCurType) {
        this.appCurType = appCurType;
    }

    public BigDecimal getAppAmt() {
        return appAmt;
    }

    public void setAppAmt(BigDecimal appAmt) {
        this.appAmt = appAmt;
    }

    public BigDecimal getAppRate() {
        return appRate;
    }

    public void setAppRate(BigDecimal appRate) {
        this.appRate = appRate;
    }

    public BigDecimal getAppRmbAmt() {
        return appRmbAmt;
    }

    public void setAppRmbAmt(BigDecimal appRmbAmt) {
        this.appRmbAmt = appRmbAmt;
    }

    public String getBailSour() {
        return bailSour;
    }

    public void setBailSour(String bailSour) {
        this.bailSour = bailSour;
    }

    public BigDecimal getBailRate() {
        return bailRate;
    }

    public void setBailRate(BigDecimal bailRate) {
        this.bailRate = bailRate;
    }

    public String getBailCurType() {
        return bailCurType;
    }

    public void setBailCurType(String bailCurType) {
        this.bailCurType = bailCurType;
    }

    public BigDecimal getSecurityAmt() {
        return securityAmt;
    }

    public void setSecurityAmt(BigDecimal securityAmt) {
        this.securityAmt = securityAmt;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public BigDecimal getBailRmbAmt() {
        return bailRmbAmt;
    }

    public void setBailRmbAmt(BigDecimal bailRmbAmt) {
        this.bailRmbAmt = bailRmbAmt;
    }

    public BigDecimal getRiskOpenAmt() {
        return riskOpenAmt;
    }

    public void setRiskOpenAmt(BigDecimal riskOpenAmt) {
        this.riskOpenAmt = riskOpenAmt;
    }

    public String getTermType() {
        return termType;
    }

    public void setTermType(String termType) {
        this.termType = termType;
    }

    public BigDecimal getAppTerm() {
        return appTerm;
    }

    public void setAppTerm(BigDecimal appTerm) {
        this.appTerm = appTerm;
    }

    public String getIrAccordType() {
        return irAccordType;
    }

    public void setIrAccordType(String irAccordType) {
        this.irAccordType = irAccordType;
    }

    public String getIrType() {
        return irType;
    }

    public void setIrType(String irType) {
        this.irType = irType;
    }

    public BigDecimal getRulingIr() {
        return rulingIr;
    }

    public void setRulingIr(BigDecimal rulingIr) {
        this.rulingIr = rulingIr;
    }

    public BigDecimal getRulingIrM() {
        return rulingIrM;
    }

    public void setRulingIrM(BigDecimal rulingIrM) {
        this.rulingIrM = rulingIrM;
    }

    public String getLoanRatType() {
        return loanRatType;
    }

    public void setLoanRatType(String loanRatType) {
        this.loanRatType = loanRatType;
    }

    public String getIrAdjustType() {
        return irAdjustType;
    }

    public void setIrAdjustType(String irAdjustType) {
        this.irAdjustType = irAdjustType;
    }

    public BigDecimal getIrAdjustTerm() {
        return irAdjustTerm;
    }

    public void setIrAdjustTerm(BigDecimal irAdjustTerm) {
        this.irAdjustTerm = irAdjustTerm;
    }

    public String getPraType() {
        return praType;
    }

    public void setPraType(String praType) {
        this.praType = praType;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public String getIrFloatType() {
        return irFloatType;
    }

    public void setIrFloatType(String irFloatType) {
        this.irFloatType = irFloatType;
    }

    public BigDecimal getIrFloatPoint() {
        return irFloatPoint;
    }

    public void setIrFloatPoint(BigDecimal irFloatPoint) {
        this.irFloatPoint = irFloatPoint;
    }

    public BigDecimal getIrFloatRate() {
        return irFloatRate;
    }

    public void setIrFloatRate(BigDecimal irFloatRate) {
        this.irFloatRate = irFloatRate;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public BigDecimal getRealityIrM() {
        return realityIrM;
    }

    public void setRealityIrM(BigDecimal realityIrM) {
        this.realityIrM = realityIrM;
    }

    public String getOverdueFloatType() {
        return overdueFloatType;
    }

    public void setOverdueFloatType(String overdueFloatType) {
        this.overdueFloatType = overdueFloatType;
    }

    public BigDecimal getOverdueRate() {
        return overdueRate;
    }

    public void setOverdueRate(BigDecimal overdueRate) {
        this.overdueRate = overdueRate;
    }

    public BigDecimal getOverduePoint() {
        return overduePoint;
    }

    public void setOverduePoint(BigDecimal overduePoint) {
        this.overduePoint = overduePoint;
    }

    public BigDecimal getOverdueRateY() {
        return overdueRateY;
    }

    public void setOverdueRateY(BigDecimal overdueRateY) {
        this.overdueRateY = overdueRateY;
    }

    public String getDefaultFloatType() {
        return defaultFloatType;
    }

    public void setDefaultFloatType(String defaultFloatType) {
        this.defaultFloatType = defaultFloatType;
    }

    public BigDecimal getDefaultRate() {
        return defaultRate;
    }

    public void setDefaultRate(BigDecimal defaultRate) {
        this.defaultRate = defaultRate;
    }

    public BigDecimal getDefaultPoint() {
        return defaultPoint;
    }

    public void setDefaultPoint(BigDecimal defaultPoint) {
        this.defaultPoint = defaultPoint;
    }

    public BigDecimal getDefaultRateY() {
        return defaultRateY;
    }

    public void setDefaultRateY(BigDecimal defaultRateY) {
        this.defaultRateY = defaultRateY;
    }

    public String getChnlSour() {
        return chnlSour;
    }

    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour;
    }

    public String getCusId() { return cusId; }
    public void setCusId(String cusId) { this.cusId = cusId; }

    public String getEspecBizType() {
        return especBizType;
    }

    public void setEspecBizType(String especBizType) {
        this.especBizType = especBizType;
    }
}
