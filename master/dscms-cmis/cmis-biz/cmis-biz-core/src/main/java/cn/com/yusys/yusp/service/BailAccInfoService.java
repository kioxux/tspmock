package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.SeqConstant;
import cn.com.yusys.yusp.domain.AccAccpDrftSub;
import cn.com.yusys.yusp.domain.BailAccInfo;
import cn.com.yusys.yusp.domain.CtrAsplDetails;
import cn.com.yusys.yusp.dto.AccAccpDto;
import cn.com.yusys.yusp.dto.BailAccInfoDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.req.Dp2098ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Dp2098RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Lstacctinfo;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2667.Dp2667ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2667.Dp2667RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.BailAccInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.sun.org.apache.regexp.internal.RE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: BailAccInfoService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: chenlong9
 * @创建时间: 2021-04-16 14:59:32
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class BailAccInfoService {

    private static final Logger log = LoggerFactory.getLogger(BailAccInfoService.class);

    @Autowired
    private BailAccInfoMapper bailAccInfoMapper;

    @Autowired
    private Dscms2CoreDpClientService dscms2CoreDpClientService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;//序列号接口

    @Autowired
    private Dscms2CoreLnClientService dscms2CoreLnClientService;

    @Autowired
    private Dscms2CoreIbClientService dscms2CoreIbClientService;//BSP封装调用核心系统的接口

    @Autowired
    private CtrAsplDetailsService ctrAsplDetailsService;

    @Autowired
    private AsplAssetsListService asplAssetsListService;
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public BailAccInfo selectByPrimaryKey(String pkId) {
        return bailAccInfoMapper.selectByPrimaryKey(pkId);
    }

    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional(readOnly = true)
    public List<BailAccInfo> selectAll(QueryModel model) {
        List<BailAccInfo> records = (List<BailAccInfo>) bailAccInfoMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<BailAccInfo> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BailAccInfo> list = bailAccInfoMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insert(BailAccInfo record) {
        return bailAccInfoMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertSelective(BailAccInfo record) {
        return bailAccInfoMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int update(BailAccInfo record) {
        return bailAccInfoMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int updateSelective(BailAccInfo record) {
        return bailAccInfoMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByPrimaryKey(String pkId) {
        return bailAccInfoMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int deleteByIds(String ids) {
        return bailAccInfoMapper.deleteByIds(ids);
    }

    /**
     * 保证金信息新增页面
     *
     * @param bailAccInfo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveBailAccInfo(BailAccInfo bailAccInfo) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (bailAccInfo == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();
            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                bailAccInfo.setInputId(userInfo.getLoginCode());
                bailAccInfo.setInputBrId(userInfo.getOrg().getCode());
                bailAccInfo.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }
            Dp2099ReqDto dp2099ReqDto = new Dp2099ReqDto();
            dp2099ReqDto.setChaxleix("0");//查询类型
            dp2099ReqDto.setZhhufenl("");//账户分类
            dp2099ReqDto.setZhshuxin("");//账户属性
            dp2099ReqDto.setChaxfanw("");//查询范围
            dp2099ReqDto.setKehuhaoo("");//客户号
            dp2099ReqDto.setKehuzhao(bailAccInfo.getBailAccNo());//客户账号
            dp2099ReqDto.setZhanghao("");//负债账号
            dp2099ReqDto.setDinhuobz("");//产品定活标志
            dp2099ReqDto.setZhhaoxuh("");//子账户序号
            dp2099ReqDto.setCunkzlei("");//存款种类
            dp2099ReqDto.setHuobdaih("");//币种
            dp2099ReqDto.setChaohubz("");//账户钞汇标志
            dp2099ReqDto.setZhhuztai("");////账户状态
            dp2099ReqDto.setChaxmima("");//查询密码
            dp2099ReqDto.setQishibis(1);//起始笔数
            dp2099ReqDto.setChxunbis(20);//查询笔数
            dp2099ReqDto.setShifoudy("");//是否打印
            dp2099ReqDto.setSfcxglzh("");//是否查询关联账户
            dp2099ReqDto.setZhjnzlei("");//证件种类
            dp2099ReqDto.setZhjhaoma("");//证件号码
            dp2099ReqDto.setKehuzhmc("");//客户账户名称
            dp2099ReqDto.setXgywbhao("");//相关业务编号
            ResultDto<Dp2099RespDto> resultDto = dscms2CoreDpClientService.dp2099(dp2099ReqDto);

            String code = resultDto.getCode();

            if ("0".equals(code)) {
                Map seqMap = new HashMap();
                int count = 0;
                if (bailAccInfo.getPkId() == null || bailAccInfo.getPkId() == "") {
                    bailAccInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
                    bailAccInfo.setPkId(StringUtils.uuid(true));
                    count = bailAccInfoMapper.insertSelective(bailAccInfo);
                } else {
                    count = bailAccInfoMapper.updateByPrimaryKey(bailAccInfo);
                }

                if (count <= 0) {
                    //若是出现异常则需要回滚，因此直接抛出异常
                    throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
                }
            } else if ("9999".equals(code)) {
                rtnCode = resultDto.getCode();
                rtnMsg = resultDto.getMessage();
                result.put("rtnCode", resultDto.getCode());
                result.put("rtnMsg", resultDto.getMessage());
            }

        } catch (YuspException e) {
            log.error("保证金信息保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存保证金信息异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int logicDelete(BailAccInfo bailAccInfo) {
        bailAccInfo.setOprType(CmisCommonConstants.OPR_TYPE_DELETE);
        return bailAccInfoMapper.updateByPrimaryKey(bailAccInfo);
    }

    /**
     * 按照流水号获取保证金信息
     *
     * @param serno
     * @return
     */
    public List<BailAccInfo> selectBySerno(String serno) {
        return bailAccInfoMapper.selectBySerno(serno);
    }

    /**
     * 发送核心系统查询保证金信息
     *
     * @param queryModel
     * @return
     */
    public ResultDto<List<BailAccInfoDto>> sendCoreForBail(QueryModel queryModel) {
        List<BailAccInfoDto> listBailAccInfo = new LinkedList<>();
        try {
            String dszhNo = "";
            String dszhName = "";
            String chaohubz = "";//钞汇标值
            String zhfutojn = "";//支付方式
            String bailAccNo = (String) queryModel.getCondition().get("bailAccNo");
//            String cusId = (String) queryModel.getCondition().get("cusId");
            String bailCurType = (String) queryModel.getCondition().get("bailCurType");
            Dp2099ReqDto dp2099ReqDto = new Dp2099ReqDto();
            dp2099ReqDto.setKehuhaoo("");//客户号
            dp2099ReqDto.setChaxleix("0");//查询类型
            dp2099ReqDto.setZhhufenl("");//账户分类
            dp2099ReqDto.setZhshuxin((String) queryModel.getCondition().get("zhshuxin"));//账户属性
            dp2099ReqDto.setChaxfanw("");//查询范围
            dp2099ReqDto.setKehuzhao(bailAccNo);//客户账号
            dp2099ReqDto.setZhanghao("");//负债账号
            dp2099ReqDto.setDinhuobz("");//产品定活标志
            dp2099ReqDto.setZhhaoxuh("");//子账户序号
            dp2099ReqDto.setCunkzlei("");//存款种类
            dp2099ReqDto.setHuobdaih(bailCurType);//币种
            dp2099ReqDto.setChaohubz("");//账户钞汇标志
            dp2099ReqDto.setZhhuztai("");//账户钞汇标志
            dp2099ReqDto.setChaxmima("");//查询密码
            dp2099ReqDto.setQishibis(1);//起始笔数
            dp2099ReqDto.setChxunbis(20);//查询笔数
            dp2099ReqDto.setShifoudy("");//是否打印
            dp2099ReqDto.setSfcxglzh("");//是否查询关联账户
            dp2099ReqDto.setZhjnzlei("");//证件种类
            dp2099ReqDto.setZhjhaoma("");//证件号码
            dp2099ReqDto.setKehuzhmc("");//客户账户名称
            dp2099ReqDto.setXgywbhao("");//相关业务编号

            log.info("开始调用dp2099接口");
            ResultDto<Dp2099RespDto> resultDto = dscms2CoreDpClientService.dp2099(dp2099ReqDto);
            log.info("结束调用dp2099接口，返回参数："+ Objects.toString(resultDto));
            Dp2667ReqDto dp2667ReqDto = new Dp2667ReqDto();
            dp2667ReqDto.setHuobdaih("01");
            dp2667ReqDto.setKehuzhao(bailAccNo);
            dp2667ReqDto.setTszhzhlx("1");
            log.info("开始调用dp2667接口");
            ResultDto<Dp2667RespDto> resultDto2667 = dscms2CoreDpClientService.dp2667(dp2667ReqDto);
            log.info("结束调用dp2667接口，返回参数："+ Objects.toString(resultDto));
            if (resultDto2667.getCode().equals("0")) {
                dszhNo = resultDto2667.getData().getZhhaoxuh();
            }

            Ib1253ReqDto ib1253ReqDto = new Ib1253ReqDto();
            ib1253ReqDto.setKehuzhao((bailAccNo));//客户账号
            ib1253ReqDto.setZhhaoxuh("");//子账户序号
            ib1253ReqDto.setYanmbzhi("0");//密码校验方式 0--不校验1--校验查询密码 2--校验交易密码

            ib1253ReqDto.setMimammmm("");//密码
            ib1253ReqDto.setKehzhao2("");//客户账号2
            ib1253ReqDto.setShifoubz("0");//是否标志 1--是 0--否

            ib1253ReqDto.setZhufldm1("");//账户分类代码1
            ib1253ReqDto.setZhufldm2("");//账户分类代码2

            log.info("开始调用ib1253接口");
            ResultDto<Ib1253RespDto> resultDto1253 = dscms2CoreIbClientService.ib1253(ib1253ReqDto);
            log.info("结束调用ib1253接口，返回参数："+ Objects.toString(resultDto));
            if (resultDto1253.getCode().equals("0")) {
                chaohubz = resultDto1253.getData().getChaohubz();//钞汇标值
                zhfutojn = resultDto1253.getData().getZhfutojn();//支付方式
            }

            Dp2099RespDto resultDtoData = resultDto.getData();
            List list = resultDtoData.getList();
            HashMap<String, String> map = new HashMap<String, String>();

            for (int i = 0; i < list.size(); i++) {
                cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.List temp = (cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.List) list.get(i);

                String cenjleix = temp.getCenjleix();
                String kehuzhao = temp.getKehuzhao();//保证金账号
                String zhhaoxuh = temp.getZhhaoxuh();//保证金账号子序号
                //主账户 获取所有的主账户和保证金子序号

                if ("0".equals(cenjleix)) {
                    map.put(kehuzhao, zhhaoxuh);
                }
            }

            for (int i = 0; i < list.size(); i++) {
                cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.List temp = (cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.List) list.get(i);

                String cenjleix = temp.getCenjleix();
                //管理账户
                if ("1".equals(cenjleix) || "".equals(cenjleix)) {
                    log.info("当前保证金账户为：" + JSON.toJSONString(temp));
                    BailAccInfoDto bailAccInfoDto = new BailAccInfoDto();
                    bailAccInfoDto.setCusId(resultDtoData.getKehuhaoo());// 客户号
                    bailAccInfoDto.setClearAccno(dszhNo);//待清算账号
                    bailAccInfoDto.setAccountType(chaohubz);
                    bailAccInfoDto.setZhfutojn(zhfutojn);
                    bailAccInfoDto.setAcctsvcrName(temp.getKaihjigo());//保证金开户行名称
                    String kehuzhao = temp.getKehuzhao();//保证金账号
                    bailAccInfoDto.setAcctsvcrNo(temp.getKaihjigo());//保证金开户行号
                    bailAccInfoDto.setBailAccName(temp.getZhhuzwmc());//保证金账号名称
                    bailAccInfoDto.setBailAccNo(temp.getKehuzhao());//保证金账号
                    bailAccInfoDto.setBailAccNoSub(temp.getZhhaoxuh());//保证金账号子序号
                    log.info("当前保证金子序号为：" + JSON.toJSONString(temp.getZhhaoxuh()));
                    bailAccInfoDto.setAccountAmount(temp.getZhanghye().toString());//账户余额
                    bailAccInfoDto.setBailCurType(changeCurrency(temp.getHuobdaih()));//保证金币种
                    bailAccInfoDto.setClearAccname(temp.getZhhuzwmc());//待清算账户名
                    bailAccInfoDto.setFirstAccount(map.get(kehuzhao));//母户序号
                    listBailAccInfo.add(bailAccInfoDto);
                }
            }
        } catch (Exception e) {
            log.error("发送核心异常！", e.getMessage());
        }
        return new ResultDto<List<BailAccInfoDto>>(listBailAccInfo);
    }

    /**
     * 保证金信息新增保存
     *
     * @param bailAccInfo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Map saveBailInfo(BailAccInfo bailAccInfo) {
        Map result = new HashMap();
        String rtnCode = EcbEnum.LIA_DEF_SUCCESS.key;
        String rtnMsg = "";
        try {
            if (bailAccInfo == null) {
                rtnCode = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.LIA_INSERT_PARAMSNULL_EXCEPTION.value;
                return result;
            }
            User userInfo = SessionUtils.getUserInformation();

            if (userInfo == null) {
                rtnCode = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.key;
                rtnMsg = EcbEnum.E_GETUSER_USERNULL_EXCEPTION.value;
                return result;
            } else {
                bailAccInfo.setInputId(userInfo.getLoginCode());
                bailAccInfo.setInputBrId(userInfo.getOrg().getCode());
                bailAccInfo.setInputDate(DateUtils.getCurrentDate(DateFormatEnum.DATETIME));
            }

            Map seqMap = new HashMap();

            String serno = sequenceTemplateClient.getSequenceTemplate(SeqConstant.IQP_SERNO, seqMap);
            bailAccInfo.setSerno(serno);
            bailAccInfo.setPkId(StringUtils.uuid(true));
            bailAccInfo.setOprType(CmisCommonConstants.OPR_TYPE_ADD);
            int insertCount = bailAccInfoMapper.insertSelective(bailAccInfo);
            if (insertCount <= 0) {
                //若是出现异常则需要回滚，因此直接抛出异常
                throw new YuspException(EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.key, EcbEnum.LIA_INSERT_INSERTAPPFAILED_EXCEPTION.value);
            }

            result.put("serno", serno);
            log.info("保证金账户" + serno + "-保存成功！");
        } catch (YuspException e) {
            log.error("保证金信息保存异常！", e.getMsg());
            throw e;
        } catch (Exception e) {
            log.error("保存保证金信息异常！", e.getMessage());
            throw e;
        } finally {
            result.put("rtnCode", rtnCode);
            result.put("rtnMsg", rtnMsg);
        }
        return result;
    }

    //币种转换
    public String changeCurrency(String cur) {
        if ("01".equals(cur)) {
            cur = "CNY";
        } else if ("12".equals(cur)) {
            cur = "GBP";
        } else if ("13".equals(cur)) {
            cur = "HKD";
        } else if ("14".equals(cur)) {
            cur = "USD";
        } else if ("15".equals(cur)) {
            cur = "CHF";
        } else if ("21".equals(cur)) {
            cur = "SEK";
        } else if ("27".equals(cur)) {
            cur = "JPY";
        } else if ("29".equals(cur)) {
            cur = "AUD";
        } else if ("38".equals(cur)) {
            cur = "EUR";
        }
        return cur;
    }

    /**
     * @方法名称: logicDelete
     * @方法描述: 逻辑删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateBailAccInfoBySerno(BailAccInfo bailAccInfo) {
        return bailAccInfoMapper.updateBailAccInfoBySerno(bailAccInfo);
    }


    public String selectAccNoBySerno(String serno) {
        return bailAccInfoMapper.selectAccNoBySerno(serno);
    }

    public BailAccInfo selectInfoBySerno(String serno) {
        return bailAccInfoMapper.selectInfoBySerno(serno);
    }

    /**
     * @方法名称: queryBySerno
     * @方法描述: 根据流水号回显银承台账保证金信息
     * @参数与返回说明:
     * @算法描述: 无
     * liuquan
     */

    public List<BailAccInfoDto> queryBySerno(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<BailAccInfoDto> bailAccInfoDto = bailAccInfoMapper.queryBySerno(model);
        PageHelper.clearPage();
        return bailAccInfoDto;
    }

    /**
     * 根据资产池流水号查询保证金账户信息
     * @param serno
     * @return
     */
    public Map<String, String> selectAsplBailBySerno(String serno) {
        Map<String, String> map = bailAccInfoMapper.selectAsplBailBySerno(serno);
        return map;
    }

    /**
     * @param map
     * @方法名称: sendCoreQueryBail
     * @方法描述: 保证金核心获取
     * @参数与返回说明:
     * @算法描述: 实时计算:
     *
     */
    public ResultDto<Map> sendCoreQueryBail(Map<String, Object> map) {
        BigDecimal assetPoolBailAmt = BigDecimal.ZERO;
        Map<String,Object> resultMap = new HashMap<String, Object>();
        try{
            String serno = (String)map.get("serno");
            if(StringUtils.isEmpty(serno)){
                throw BizException.error(null, "9999","未获取到有效参数serno");
            }
            assetPoolBailAmt = this.sendCoreQueryBail(serno);
        }catch(BizException e){
            return new ResultDto(resultMap).message(e.getMessage()).code("9999");
        }catch(Exception e){
            return new ResultDto(resultMap).message(e.getMessage()).code("9999");
        }finally{
            resultMap.put("assetPoolBailAmt",assetPoolBailAmt);
        }
        return new ResultDto(resultMap).message("核心查询成功").code("0");
    }
    /**
     * @param serno
     * @方法名称: sendCoreQueryBail
     * @方法描述: 保证金核心获取
     * @参数与返回说明:
     * @算法描述: 实时计算:
     *
     */
    public BigDecimal sendCoreQueryBail(String serno) {
        /**
         *  1、发核心 dp2099（查询保证金账户余额）
         */
        BigDecimal assetPoolBailAmt = BigDecimal.ZERO;
        List<BailAccInfo> bailAccInfoList = this.selectBySerno(serno);
        if (CollectionUtils.isEmpty(bailAccInfoList)) {
            return null;
        }
        BailAccInfo bailAccInfo = bailAccInfoList.get(0);
        return this.sendCoredp2099(bailAccInfo);
    }
    /**
     * @param bailAccInfo
     * @方法名称: sendCoredp2099
     * @方法描述: 保证金核心获取
     * @参数与返回说明:
     * @算法描述: 实时计算:
     *
     */
    public BigDecimal sendCoredp2099(BailAccInfo bailAccInfo){
        BigDecimal assetPoolBailAmt =BigDecimal.ZERO;
        // 发接口 dp2099
        Dp2099ReqDto dp2099ReqDto = new Dp2099ReqDto();
        // 保证金账号
        dp2099ReqDto.setKehuzhao(bailAccInfo.getBailAccNo());
        // 子账户序号
        dp2099ReqDto.setZhhaoxuh(bailAccInfo.getBailAccNoSub());
        ResultDto<Dp2099RespDto> Dp2099ResultDto = dscms2CoreDpClientService.dp2099(dp2099ReqDto);
        Dp2099RespDto dp2099RespDto = new Dp2099RespDto();
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,Dp2099ResultDto.getCode())) {
            dp2099RespDto = Dp2099ResultDto.getData();
            // 资产池保证金账户余额(累加所有保证金账户的账户与余额)
            if(CollectionUtils.nonEmpty(dp2099RespDto.getList())){
                assetPoolBailAmt = dp2099RespDto.getList()
                        .stream()
                        .map(e -> e.getZhanghye())
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                log.info("保证金账户余额总额："+ Objects.toString(assetPoolBailAmt));
            }
        }else {
            throw BizException.error("","9999",Dp2099ResultDto.getMessage());
        }
        return assetPoolBailAmt;
    }
    /**
     * @函数名称:computeAvalBail
     * @函数描述:计算资产池可提取保证金金额
     * @参数与返回说明:
     * @算法描述: 可提取保证金金额计算公式：MIN（（已质押入池资产 * 质押率 + 保证金账户余额 - 资产池下融资余额），（保证金账户余额 * 保证金可提取比例））
     * xs
     */
    public ResultDto<Map> computeAvalBail(Map<String, String> map) {
        /**
         *  1、发核心 dp2099（查询保证金账户余额）
         */
        BigDecimal bailAvaAmt = BigDecimal.ZERO;
        Map<String,Object> resultMap = new HashMap<String, Object>();
        try{
            String serno = (String)map.get("serno");// 保证金流水号
            String reqAssetPoolBailAmt = Objects.toString(map.get("assetPoolBailAmt")); //核心保证金金额
            String reqBailRate = Objects.toString(map.get("bailRate"));// 保证金比例
            if(StringUtils.isEmpty(serno)){
                throw BizException.error(null, "9999","未获取到有效参数【保证金流水号】【serno】");
            }
            if(StringUtils.isEmpty(reqBailRate)){
                throw BizException.error(null, "9999","未获取到有效参数【保证金比例】【bailRate】");
            }
            BigDecimal assetPoolBailAmtDeciaml = BigDecimal.ZERO;
            BigDecimal BailRateDecmial = new BigDecimal(reqBailRate);
            if(StringUtils.isEmpty(reqAssetPoolBailAmt)){
                // 如果没有传值，就去核心查询
                assetPoolBailAmtDeciaml  = this.sendCoreQueryBail(serno);
            }else{
                assetPoolBailAmtDeciaml =  new BigDecimal(reqAssetPoolBailAmt);
            }
            // 保证金账户余额 * 保证金比例
            BigDecimal assetPoolBailAmt = assetPoolBailAmtDeciaml.multiply(BailRateDecmial);
            // 保证金可提取金额
            BigDecimal assetAvalBailAmt = assetPoolBailAmtDeciaml.multiply(BigDecimal.ONE.subtract(BailRateDecmial));
            // 资产池可用融资额度 = 已质押入池资产 * 质押率 + 保证金账户余额 - 资产池下融资余额
            CtrAsplDetails ctrAsplDetails = ctrAsplDetailsService.selectInfoBySerno(serno);
            // 资产池融资额度
            BigDecimal assetPoolFinAmt = asplAssetsListService.getAssetPoolFinAmt(ctrAsplDetails,assetPoolBailAmtDeciaml);
            // 资产池可用额度
            BigDecimal assetPoolAvaAmt = asplAssetsListService.getAssetPoolAvaAmt(ctrAsplDetails,assetPoolFinAmt,assetPoolBailAmt);
            // 取最小值
            bailAvaAmt = assetAvalBailAmt.min(assetPoolAvaAmt);
        }catch(BizException e){
            return new ResultDto(resultMap).message(e.getMessage()).code("9999");
        }catch(Exception e){
            return new ResultDto(resultMap).message(e.getMessage()).code("9999");
        }finally{
            resultMap.put("bailAvalAmt",bailAvaAmt);
        }
        return new ResultDto(resultMap).message("核心查询成功").code("0");
    }

    public static void main(String[] args) {
        System.out.println(Objects.toString(null));
    }
    @Transactional
    public int deleteBySerno(String serno) {
        return bailAccInfoMapper.deleteBySerno(serno);
    }

    /**
     * 发送核心系统查询保证金信息
     *
     * @param queryModel
     * @return
     */
    public ResultDto<List<BailAccInfoDto>> getBailInfo(QueryModel queryModel) {
        List<BailAccInfoDto> listBailAccInfo = new ArrayList<>();
        // 客户号
        String cusId = (String) queryModel.getCondition().get("cusId");
        // 填充dp2098接口请求报文
        Dp2098ReqDto dp2098ReqDto = new Dp2098ReqDto();
        dp2098ReqDto.setKehuhaoo(cusId);
        ResultDto<Dp2098RespDto> dp2098ResultDto = dscms2CoreDpClientService.dp2098(dp2098ReqDto);
        if(Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,dp2098ResultDto.getCode())){
            List<Lstacctinfo> respDtoList = dp2098ResultDto.getData().getLstacctinfo();
            if(CollectionUtils.nonEmpty(respDtoList)){
                for (Lstacctinfo lstacctinfo : respDtoList) {
                    BailAccInfoDto bailAccInfoDto = new BailAccInfoDto();
                    bailAccInfoDto.setCusId(cusId);
                    bailAccInfoDto.setBailAccNo(lstacctinfo.getKehuzhao());//保证金账户
                    bailAccInfoDto.setBailAccName(lstacctinfo.getMuhuminc());//保证金账户名称
                    bailAccInfoDto.setFirstAccount(lstacctinfo.getMuhuxuho());//母户序号
                    bailAccInfoDto.setBailAccNoSub(lstacctinfo.getZhbuxuho());//保证金账户子序号

                    bailAccInfoDto.setClearAccno(lstacctinfo.getKehuzhao());//待清算账号
                    bailAccInfoDto.setClearAccname(lstacctinfo.getDaiqsmin());//待清算账户名
                    bailAccInfoDto.setClearAccnoSub(lstacctinfo.getDaiqsxuh());//待清算账户子序号

                    bailAccInfoDto.setBailCurType(changeCurrency(lstacctinfo.getHuobdaih()));//保证金币种
                    bailAccInfoDto.setAcctsvcrNo(lstacctinfo.getKaihjigo());//开户机构号
                    if (lstacctinfo.getKaihjigo().startsWith("80")) {
                        bailAccInfoDto.setAcctsvcrName("寿光村镇银行");
                    } else if (lstacctinfo.getKaihjigo().startsWith("81")) {
                        bailAccInfoDto.setAcctsvcrName("东海村镇银行");
                    } else {
                        bailAccInfoDto.setAcctsvcrName("张家港农村商业银行");
                    }
                    listBailAccInfo.add(bailAccInfoDto);
                }
            }
        }
        return new ResultDto<List<BailAccInfoDto>>(listBailAccInfo);
    }
}
