/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;

import javax.persistence.*;


import javax.persistence.Id;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: TmpWydCusInfo
 * @类描述: tmp_wyd_cus_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-08-20 13:56:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "tmp_wyd_cus_info_tmp")
public class TmpWydCusInfoTmp {
	
	/** 客户编号 **/
	@Id
	@Column(name = "CUSTOMERID")
	private String customerid;
	
	/** 客户类别 **/
	@Column(name = "F_ORG_NUMBER", unique = false, nullable = true, length = 2)
	private String fOrgNumber;
	
	/** 客户类型 **/
	@Column(name = "CUSTOMERTYPE", unique = false, nullable = true, length = 6)
	private String customertype;
	
	/** 客户子类型 **/
	@Column(name = "CUSTOMERSUBTYPE", unique = false, nullable = true, length = 6)
	private String customersubtype;
	
	/** 客户中文名称 **/
	@Column(name = "ENTERPRISENAME", unique = false, nullable = true, length = 100)
	private String enterprisename;
	
	/** 主证件类型 **/
	@Column(name = "CERTTYPE", unique = false, nullable = true, length = 3)
	private String certtype;
	
	/** 证件号码 **/
	@Column(name = "CERTID", unique = false, nullable = true, length = 50)
	private String certid;
	
	/** 证件到期日 **/
	@Column(name = "CERTID_EXPIRE_DATE", unique = false, nullable = true, length = 10)
	private String certidExpireDate;
	
	/** 机构信用代码 **/
	@Column(name = "INSTI_CREDIT_CODE", unique = false, nullable = true, length = 50)
	private String instiCreditCode;
	
	/** 营业执照号 **/
	@Column(name = "ORG_CERT_NUMBER", unique = false, nullable = true, length = 50)
	private String orgCertNumber;
	
	/** 营业执照有效截止日期 **/
	@Column(name = "EXPIRE_DATE", unique = false, nullable = true, length = 10)
	private String expireDate;
	
	/** 注册地址 **/
	@Column(name = "REGISTER_ADDR", unique = false, nullable = true, length = 100)
	private String registerAddr;
	
	/** 国别 **/
	@Column(name = "REGISTER_COUNTRY", unique = false, nullable = true, length = 3)
	private String registerCountry;
	
	/** 行政区划代码 **/
	@Column(name = "REGISTER_AREA_CODE", unique = false, nullable = true, length = 8)
	private String registerAreaCode;
	
	/** 行政区划名称 **/
	@Column(name = "REGISTER_AREA_NAME", unique = false, nullable = true, length = 300)
	private String registerAreaName;
	
	/** 成立日期 **/
	@Column(name = "REGISTER_DATE", unique = false, nullable = true, length = 10)
	private String registerDate;
	
	/** 经营年限 **/
	@Column(name = "OPERATING_LIFE", unique = false, nullable = true, length = 8)
	private String operatingLife;
	
	/** 实际控制人从业年限 **/
	@Column(name = "AC_OPERATING_LIFE", unique = false, nullable = true, length = 8)
	private String acOperatingLife;
	
	/** 经营范围 **/
	@Column(name = "MOSTBUSINESS", unique = false, nullable = true, length = 512)
	private String mostbusiness;
	
	/** 注册资本币种 **/
	@Column(name = "RCCURRENCY", unique = false, nullable = true, length = 3)
	private String rccurrency;
	
	/** 注册资本（元） **/
	@Column(name = "PCCURRENCY", unique = false, nullable = true, length = 20)
	private String pccurrency;
	
	/** 行业投向 **/
	@Column(name = "INDUSTRY_TYPE", unique = false, nullable = true, length = 10)
	private String industryType;
	
	/** 经济行业分类 **/
	@Column(name = "ECONOMIC_INDUSTRY_TYPE", unique = false, nullable = true, length = 10)
	private String economicIndustryType;
	
	/** 经济类型 **/
	@Column(name = "ECONOMIC_TYPE", unique = false, nullable = true, length = 10)
	private String economicType;
	
	/** 办公联系电话 **/
	@Column(name = "OFFICETEL", unique = false, nullable = true, length = 30)
	private String officetel;
	
	/** 财务联系电话 **/
	@Column(name = "FINANCEDEPTTEL", unique = false, nullable = true, length = 30)
	private String financedepttel;
	
	/** 更新日期 **/
	@Column(name = "UPDATEDATE", unique = false, nullable = true, length = 10)
	private String updatedate;
	
	/** 欠息汇总-余额 **/
	@Column(name = "OWEBALANCESUM_BALANCE", unique = false, nullable = true, length = 20)
	private String owebalancesumBalance;
	
	/** 不良、违约类汇总（余额） **/
	@Column(name = "BADSUM_BALANCE", unique = false, nullable = true, length = 20)
	private String badsumBalance;
	
	/** 风险预警信号 **/
	@Column(name = "RISK_WARNING", unique = false, nullable = true, length = 20)
	private String riskWarning;
	
	/** 基本存款账号 **/
	@Column(name = "BASIC_ACCOUT_CODE", unique = false, nullable = true, length = 32)
	private String basicAccoutCode;
	
	/** 基本账户开户行名称 **/
	@Column(name = "BASIC_ACCOUT_NAME", unique = false, nullable = true, length = 100)
	private String basicAccoutName;
	
	/** 上市公司标志 **/
	@Column(name = "LISTING_FLAG", unique = false, nullable = true, length = 1)
	private String listingFlag;
	
	/** 邮政编码 **/
	@Column(name = "ZIP_CODE", unique = false, nullable = true, length = 6)
	private String zipCode;
	
	/** 传真号码 **/
	@Column(name = "PHONE_NUMBER", unique = false, nullable = true, length = 30)
	private String phoneNumber;
	
	/** 员工人数 **/
	@Column(name = "STAFF_NUMBER", unique = false, nullable = true, length = 11)
	private String staffNumber;
	
	/** 企业控股类型 **/
	@Column(name = "ENTERPRISE_HOLDING_TYPE", unique = false, nullable = true, length = 6)
	private String enterpriseHoldingType;
	
	/** 是否关停企业 **/
	@Column(name = "OPEN_CLOSE_FLAG", unique = false, nullable = true, length = 2)
	private String openCloseFlag;
	
	/** 企业规模 **/
	@Column(name = "ORGANIZATION_SCALE", unique = false, nullable = true, length = 2)
	private String organizationScale;
	
	/** 境内境外标志 **/
	@Column(name = "IN_OUT_FLAG", unique = false, nullable = true, length = 2)
	private String inOutFlag;
	
	/** 劳动密集型企业标志 **/
	@Column(name = "LABOR_INTENSIVE_FLAG", unique = false, nullable = true, length = 2)
	private String laborIntensiveFlag;
	
	/** 纳税人类型 **/
	@Column(name = "TAXPAYER_TYPE", unique = false, nullable = true, length = 2)
	private String taxpayerType;
	
	/** 推荐人 **/
	@Column(name = "RECOMMEND", unique = false, nullable = true, length = 30)
	private String recommend;
	
	/** 纳税人识别号 **/
	@Column(name = "TAX_PAYER_ID", unique = false, nullable = true, length = 50)
	private String taxPayerId;
	
	/** 纳税等级 **/
	@Column(name = "CREDIT_RATE", unique = false, nullable = true, length = 8)
	private String creditRate;
	
	/** 营销渠道号 **/
	@Column(name = "PORTAL", unique = false, nullable = true, length = 20)
	private String portal;
	
	
	/**
	 * @param customerid
	 */
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	
    /**
     * @return customerid
     */
	public String getCustomerid() {
		return this.customerid;
	}
	
	/**
	 * @param fOrgNumber
	 */
	public void setFOrgNumber(String fOrgNumber) {
		this.fOrgNumber = fOrgNumber;
	}
	
    /**
     * @return fOrgNumber
     */
	public String getFOrgNumber() {
		return this.fOrgNumber;
	}
	
	/**
	 * @param customertype
	 */
	public void setCustomertype(String customertype) {
		this.customertype = customertype;
	}
	
    /**
     * @return customertype
     */
	public String getCustomertype() {
		return this.customertype;
	}
	
	/**
	 * @param customersubtype
	 */
	public void setCustomersubtype(String customersubtype) {
		this.customersubtype = customersubtype;
	}
	
    /**
     * @return customersubtype
     */
	public String getCustomersubtype() {
		return this.customersubtype;
	}
	
	/**
	 * @param enterprisename
	 */
	public void setEnterprisename(String enterprisename) {
		this.enterprisename = enterprisename;
	}
	
    /**
     * @return enterprisename
     */
	public String getEnterprisename() {
		return this.enterprisename;
	}
	
	/**
	 * @param certtype
	 */
	public void setCerttype(String certtype) {
		this.certtype = certtype;
	}
	
    /**
     * @return certtype
     */
	public String getCerttype() {
		return this.certtype;
	}
	
	/**
	 * @param certid
	 */
	public void setCertid(String certid) {
		this.certid = certid;
	}
	
    /**
     * @return certid
     */
	public String getCertid() {
		return this.certid;
	}
	
	/**
	 * @param certidExpireDate
	 */
	public void setCertidExpireDate(String certidExpireDate) {
		this.certidExpireDate = certidExpireDate;
	}
	
    /**
     * @return certidExpireDate
     */
	public String getCertidExpireDate() {
		return this.certidExpireDate;
	}
	
	/**
	 * @param instiCreditCode
	 */
	public void setInstiCreditCode(String instiCreditCode) {
		this.instiCreditCode = instiCreditCode;
	}
	
    /**
     * @return instiCreditCode
     */
	public String getInstiCreditCode() {
		return this.instiCreditCode;
	}
	
	/**
	 * @param orgCertNumber
	 */
	public void setOrgCertNumber(String orgCertNumber) {
		this.orgCertNumber = orgCertNumber;
	}
	
    /**
     * @return orgCertNumber
     */
	public String getOrgCertNumber() {
		return this.orgCertNumber;
	}
	
	/**
	 * @param expireDate
	 */
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	
    /**
     * @return expireDate
     */
	public String getExpireDate() {
		return this.expireDate;
	}
	
	/**
	 * @param registerAddr
	 */
	public void setRegisterAddr(String registerAddr) {
		this.registerAddr = registerAddr;
	}
	
    /**
     * @return registerAddr
     */
	public String getRegisterAddr() {
		return this.registerAddr;
	}
	
	/**
	 * @param registerCountry
	 */
	public void setRegisterCountry(String registerCountry) {
		this.registerCountry = registerCountry;
	}
	
    /**
     * @return registerCountry
     */
	public String getRegisterCountry() {
		return this.registerCountry;
	}
	
	/**
	 * @param registerAreaCode
	 */
	public void setRegisterAreaCode(String registerAreaCode) {
		this.registerAreaCode = registerAreaCode;
	}
	
    /**
     * @return registerAreaCode
     */
	public String getRegisterAreaCode() {
		return this.registerAreaCode;
	}
	
	/**
	 * @param registerAreaName
	 */
	public void setRegisterAreaName(String registerAreaName) {
		this.registerAreaName = registerAreaName;
	}
	
    /**
     * @return registerAreaName
     */
	public String getRegisterAreaName() {
		return this.registerAreaName;
	}
	
	/**
	 * @param registerDate
	 */
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	
    /**
     * @return registerDate
     */
	public String getRegisterDate() {
		return this.registerDate;
	}
	
	/**
	 * @param operatingLife
	 */
	public void setOperatingLife(String operatingLife) {
		this.operatingLife = operatingLife;
	}
	
    /**
     * @return operatingLife
     */
	public String getOperatingLife() {
		return this.operatingLife;
	}
	
	/**
	 * @param acOperatingLife
	 */
	public void setAcOperatingLife(String acOperatingLife) {
		this.acOperatingLife = acOperatingLife;
	}
	
    /**
     * @return acOperatingLife
     */
	public String getAcOperatingLife() {
		return this.acOperatingLife;
	}
	
	/**
	 * @param mostbusiness
	 */
	public void setMostbusiness(String mostbusiness) {
		this.mostbusiness = mostbusiness;
	}
	
    /**
     * @return mostbusiness
     */
	public String getMostbusiness() {
		return this.mostbusiness;
	}
	
	/**
	 * @param rccurrency
	 */
	public void setRccurrency(String rccurrency) {
		this.rccurrency = rccurrency;
	}
	
    /**
     * @return rccurrency
     */
	public String getRccurrency() {
		return this.rccurrency;
	}
	
	/**
	 * @param pccurrency
	 */
	public void setPccurrency(String pccurrency) {
		this.pccurrency = pccurrency;
	}
	
    /**
     * @return pccurrency
     */
	public String getPccurrency() {
		return this.pccurrency;
	}
	
	/**
	 * @param industryType
	 */
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	
    /**
     * @return industryType
     */
	public String getIndustryType() {
		return this.industryType;
	}
	
	/**
	 * @param economicIndustryType
	 */
	public void setEconomicIndustryType(String economicIndustryType) {
		this.economicIndustryType = economicIndustryType;
	}
	
    /**
     * @return economicIndustryType
     */
	public String getEconomicIndustryType() {
		return this.economicIndustryType;
	}
	
	/**
	 * @param economicType
	 */
	public void setEconomicType(String economicType) {
		this.economicType = economicType;
	}
	
    /**
     * @return economicType
     */
	public String getEconomicType() {
		return this.economicType;
	}
	
	/**
	 * @param officetel
	 */
	public void setOfficetel(String officetel) {
		this.officetel = officetel;
	}
	
    /**
     * @return officetel
     */
	public String getOfficetel() {
		return this.officetel;
	}
	
	/**
	 * @param financedepttel
	 */
	public void setFinancedepttel(String financedepttel) {
		this.financedepttel = financedepttel;
	}
	
    /**
     * @return financedepttel
     */
	public String getFinancedepttel() {
		return this.financedepttel;
	}
	
	/**
	 * @param updatedate
	 */
	public void setUpdatedate(String updatedate) {
		this.updatedate = updatedate;
	}
	
    /**
     * @return updatedate
     */
	public String getUpdatedate() {
		return this.updatedate;
	}
	
	/**
	 * @param owebalancesumBalance
	 */
	public void setOwebalancesumBalance(String owebalancesumBalance) {
		this.owebalancesumBalance = owebalancesumBalance;
	}
	
    /**
     * @return owebalancesumBalance
     */
	public String getOwebalancesumBalance() {
		return this.owebalancesumBalance;
	}
	
	/**
	 * @param badsumBalance
	 */
	public void setBadsumBalance(String badsumBalance) {
		this.badsumBalance = badsumBalance;
	}
	
    /**
     * @return badsumBalance
     */
	public String getBadsumBalance() {
		return this.badsumBalance;
	}
	
	/**
	 * @param riskWarning
	 */
	public void setRiskWarning(String riskWarning) {
		this.riskWarning = riskWarning;
	}
	
    /**
     * @return riskWarning
     */
	public String getRiskWarning() {
		return this.riskWarning;
	}
	
	/**
	 * @param basicAccoutCode
	 */
	public void setBasicAccoutCode(String basicAccoutCode) {
		this.basicAccoutCode = basicAccoutCode;
	}
	
    /**
     * @return basicAccoutCode
     */
	public String getBasicAccoutCode() {
		return this.basicAccoutCode;
	}
	
	/**
	 * @param basicAccoutName
	 */
	public void setBasicAccoutName(String basicAccoutName) {
		this.basicAccoutName = basicAccoutName;
	}
	
    /**
     * @return basicAccoutName
     */
	public String getBasicAccoutName() {
		return this.basicAccoutName;
	}
	
	/**
	 * @param listingFlag
	 */
	public void setListingFlag(String listingFlag) {
		this.listingFlag = listingFlag;
	}
	
    /**
     * @return listingFlag
     */
	public String getListingFlag() {
		return this.listingFlag;
	}
	
	/**
	 * @param zipCode
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
    /**
     * @return zipCode
     */
	public String getZipCode() {
		return this.zipCode;
	}
	
	/**
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
    /**
     * @return phoneNumber
     */
	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	/**
	 * @param staffNumber
	 */
	public void setStaffNumber(String staffNumber) {
		this.staffNumber = staffNumber;
	}
	
    /**
     * @return staffNumber
     */
	public String getStaffNumber() {
		return this.staffNumber;
	}
	
	/**
	 * @param enterpriseHoldingType
	 */
	public void setEnterpriseHoldingType(String enterpriseHoldingType) {
		this.enterpriseHoldingType = enterpriseHoldingType;
	}
	
    /**
     * @return enterpriseHoldingType
     */
	public String getEnterpriseHoldingType() {
		return this.enterpriseHoldingType;
	}
	
	/**
	 * @param openCloseFlag
	 */
	public void setOpenCloseFlag(String openCloseFlag) {
		this.openCloseFlag = openCloseFlag;
	}
	
    /**
     * @return openCloseFlag
     */
	public String getOpenCloseFlag() {
		return this.openCloseFlag;
	}
	
	/**
	 * @param organizationScale
	 */
	public void setOrganizationScale(String organizationScale) {
		this.organizationScale = organizationScale;
	}
	
    /**
     * @return organizationScale
     */
	public String getOrganizationScale() {
		return this.organizationScale;
	}
	
	/**
	 * @param inOutFlag
	 */
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	
    /**
     * @return inOutFlag
     */
	public String getInOutFlag() {
		return this.inOutFlag;
	}
	
	/**
	 * @param laborIntensiveFlag
	 */
	public void setLaborIntensiveFlag(String laborIntensiveFlag) {
		this.laborIntensiveFlag = laborIntensiveFlag;
	}
	
    /**
     * @return laborIntensiveFlag
     */
	public String getLaborIntensiveFlag() {
		return this.laborIntensiveFlag;
	}
	
	/**
	 * @param taxpayerType
	 */
	public void setTaxpayerType(String taxpayerType) {
		this.taxpayerType = taxpayerType;
	}
	
    /**
     * @return taxpayerType
     */
	public String getTaxpayerType() {
		return this.taxpayerType;
	}
	
	/**
	 * @param recommend
	 */
	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}
	
    /**
     * @return recommend
     */
	public String getRecommend() {
		return this.recommend;
	}
	
	/**
	 * @param taxPayerId
	 */
	public void setTaxPayerId(String taxPayerId) {
		this.taxPayerId = taxPayerId;
	}
	
    /**
     * @return taxPayerId
     */
	public String getTaxPayerId() {
		return this.taxPayerId;
	}
	
	/**
	 * @param creditRate
	 */
	public void setCreditRate(String creditRate) {
		this.creditRate = creditRate;
	}
	
    /**
     * @return creditRate
     */
	public String getCreditRate() {
		return this.creditRate;
	}
	
	/**
	 * @param portal
	 */
	public void setPortal(String portal) {
		this.portal = portal;
	}
	
    /**
     * @return portal
     */
	public String getPortal() {
		return this.portal;
	}


}