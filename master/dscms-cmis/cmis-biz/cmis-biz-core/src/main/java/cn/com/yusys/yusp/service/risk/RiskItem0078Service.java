package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisLmtConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.IqpHouse;
import cn.com.yusys.yusp.domain.IqpLoanApp;
import cn.com.yusys.yusp.domain.PvpLoanApp;
import cn.com.yusys.yusp.dto.CfgPrdBasicinfoDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.req.CmisLmt0009ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0009.resp.CmisLmt0009RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.req.CmisLmt0010ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0010.resp.CmisLmt0010RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.*;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: RiskItem0078Service
 * @类描述: 额度系统台账校验
 * @功能描述: 额度系统台账校验
 * @创建人: hubp
 * @创建时间: 2021年8月11日08:38:26
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class RiskItem0078Service {

    private static final Logger log = LoggerFactory.getLogger(RiskItem0078Service.class);

    @Autowired
    private PvpLoanAppService pvpLoanAppService;

    @Autowired
    private IqpLoanAppService iqpLoanAppService;

    @Autowired
    private IqpHouseService iqpHouseService;

    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Autowired
    private CmisBizXwCommonService cmisBizXwCommonService;

    /**
     * @方法名称: riskItem0078
     * @方法描述: 额度系统台账校验
     * @参数与返回说明:
     * @算法描述: 前往额度系统进行校验
     * @创建人: hubp
     * @创建时间: 2021年8月11日08:38:26
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0078(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString();
        String bizType = queryModel.getCondition().get("bizType").toString();
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }
        // 先行判断是授信申请进入，还是放款进入
        if ("LS001".equals(bizType) || "LS002".equals(bizType) || "LS003".equals(bizType)
                || "SGE01".equals(bizType) || "SGE02".equals(bizType) || "SGE03".equals(bizType)
                || "DHE01".equals(bizType) || "DHE02".equals(bizType) || "DHE03".equals(bizType)) {
            // 授信申请进入
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByPrimaryKey(serno);
            ResultDto<CfgPrdBasicinfoDto> CfgPrdBasicinfoDto = iCmisCfgClientService.queryCfgPrdBasicInfo(iqpLoanApp.getPrdId());
            String prdType = CfgPrdBasicinfoDto.getData().getPrdType();
            if ("10".equals(prdType)) {//零售按揭
                IqpHouse iqphouse = iqpHouseService.selectByIqpSernos(serno);
                String houseStatus = iqphouse.getHouseStatus();
                String thirdPartyFlag = iqpLoanApp.getIsOutstndTrdLmtAmt();//第三方标识

                //第三方额度占用
                if ("1".equalsIgnoreCase(thirdPartyFlag)) {//第三方启用
                    String proNo = iqpLoanApp.getProNo();//项目编号
                    String proSerno = iqpLoanApp.getTdpAgrNo();//项目流水号

                    //1. 向额度系统发送接口：校验额度
                    CmisLmt0009ReqDto cmisLmt0009ReqDto = new CmisLmt0009ReqDto();
                    cmisLmt0009ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                    cmisLmt0009ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(iqpLoanApp.getManagerBrId()));//金融机构代码
                    cmisLmt0009ReqDto.setDealBizNo(iqpLoanApp.getIqpSerno());//合同编号
                    cmisLmt0009ReqDto.setCusId(iqpLoanApp.getCusId());//客户编号
                    cmisLmt0009ReqDto.setCusName(iqpLoanApp.getCusName());//客户名称
                    cmisLmt0009ReqDto.setDealBizType(iqpLoanApp.getContType());//交易业务类型
                    cmisLmt0009ReqDto.setPrdId(iqpLoanApp.getPrdId());//产品编号
                    cmisLmt0009ReqDto.setPrdName(iqpLoanApp.getPrdName());//产品名称
                    cmisLmt0009ReqDto.setIsLriskBiz("0");//是否低风险
                    cmisLmt0009ReqDto.setIsFollowBiz("0");//是否无缝衔接
                    cmisLmt0009ReqDto.setIsBizRev("0");//是否合同重签
                    cmisLmt0009ReqDto.setBizAttr("1");//交易属性
                    cmisLmt0009ReqDto.setBussStageType("01");//申请阶段
                    // cmisLmt0009ReqDto.setOrigiDealBizNo(iqpLoanApp.getIqpSerno());//原交易业务编号
                    // cmisLmt0009ReqDto.setOrigiDealBizStatus("400");//原交易业务状态
                    // cmisLmt0009ReqDto.setOrigiRecoverType("06");//原交易业务恢复类型
                    // cmisLmt0009ReqDto.setOrigiBizAttr("1");//原交易属性
                    cmisLmt0009ReqDto.setInputId(iqpLoanApp.getInputId());
                    cmisLmt0009ReqDto.setInputBrId(iqpLoanApp.getInputBrId());
                    cmisLmt0009ReqDto.setInputDate(iqpLoanApp.getInputDate());
                    cmisLmt0009ReqDto.setDealBizAmt(iqpLoanApp.getAppAmt());//交易业务金额
                    cmisLmt0009ReqDto.setDealBizBailPreRate(new BigDecimal(0));//保证金比例
                    cmisLmt0009ReqDto.setStartDate(iqpLoanApp.getStartDate());//合同起始日
                    cmisLmt0009ReqDto.setEndDate(iqpLoanApp.getEndDate());//合同到期日
                    List<CmisLmt0009OccRelListReqDto> cmisLmt0009OccRelListReqDtoList = new ArrayList<CmisLmt0009OccRelListReqDto>();
                    CmisLmt0009OccRelListReqDto cmisLmt0009OccRelListReqDto = new CmisLmt0009OccRelListReqDto();
                    cmisLmt0009OccRelListReqDto.setLmtType(CmisLmtConstants.STD_ZB_LMT_TYPE_03);//额度类型
                    cmisLmt0009OccRelListReqDto.setLmtSubNo(proSerno);//额度分项编号
                    cmisLmt0009OccRelListReqDto.setBizTotalAmt(iqpLoanApp.getAppAmt());//占用总额(折人民币)
                    cmisLmt0009OccRelListReqDto.setBizSpacAmt(iqpLoanApp.getAppAmt());//占用敞口(折人民币)
                    cmisLmt0009OccRelListReqDtoList.add(cmisLmt0009OccRelListReqDto);
                    cmisLmt0009ReqDto.setCmisLmt0009OccRelListReqDtoList(cmisLmt0009OccRelListReqDtoList);

                    log.info("根据业务申请编号【{}】前往额度系统-第三方额度校验请求报文：【{}】", iqpLoanApp.getIqpSerno(), JSON.toJSONString(cmisLmt0009ReqDto));
                    ResultDto<CmisLmt0009RespDto> cmisLmt0009RespDto = cmisLmtClientService.cmisLmt0009(cmisLmt0009ReqDto);
                    log.info("根据业务申请编号【{}】前往额度系统-第三方额度校验返回报文：【{}】", iqpLoanApp.getIqpSerno(), JSON.toJSONString(cmisLmt0009RespDto));

                    String code = cmisLmt0009RespDto.getData().getErrorCode();

                    if ("0000".equals(code) || "70125".equals(code)) {
                        log.info("根据业务申请编号【{}】,前往额度系统-台账校验成功！", serno);
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
                    } else {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                        riskResultDto.setRiskResultDesc(cmisLmt0009RespDto.getData().getErrorMsg()); //校验失败
                    }
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
                }
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
            }

        } else if ("LS005".equals(bizType) || "LS006".equals(bizType)
                    || "SGE04".equals(bizType) || "DHE04".equals(bizType)) {
            // 放款进入
            PvpLoanApp pvpLoanApp = pvpLoanAppService.selectByPrimaryKey(serno);

            try {
                //1. 向额度系统发送接口：校验额度
                CmisLmt0010ReqDto cmisLmt0010ReqDto = new CmisLmt0010ReqDto();
                cmisLmt0010ReqDto.setSysId(EsbEnum.SERVTP_XDG.key);//系统编号
                cmisLmt0010ReqDto.setInstuCde(cmisBizXwCommonService.getInstuCde(pvpLoanApp.getManagerBrId()));//金融机构代码
                cmisLmt0010ReqDto.setBizNo(pvpLoanApp.getContNo());//合同编号

                CmisLmt0010ReqDealBizListDto cmisLmt0010ReqDealBizListDto = new CmisLmt0010ReqDealBizListDto();
                cmisLmt0010ReqDealBizListDto.setDealBizNo(pvpLoanApp.getBillNo());
                cmisLmt0010ReqDealBizListDto.setIsFollowBiz("0");
                // cmisLmt0010ReqDealBizListDto.setOrigiDealBizNo(pvpLoanApp.getBillNo());
                // cmisLmt0010ReqDealBizListDto.setOrigiRecoverType("06");
                // cmisLmt0010ReqDealBizListDto.setOrigiDealBizStatus("200");
                // cmisLmt0010ReqDealBizListDto.setOrigiBizAttr("2");
                cmisLmt0010ReqDealBizListDto.setCusId(pvpLoanApp.getCusId());
                cmisLmt0010ReqDealBizListDto.setCusName(pvpLoanApp.getCusName());
                cmisLmt0010ReqDealBizListDto.setPrdId(pvpLoanApp.getPrdId());
                cmisLmt0010ReqDealBizListDto.setPrdName(pvpLoanApp.getPrdName());
                cmisLmt0010ReqDealBizListDto.setDealBizAmt(pvpLoanApp.getPvpAmt());
                cmisLmt0010ReqDealBizListDto.setDealBizSpacAmt(pvpLoanApp.getPvpAmt());
                cmisLmt0010ReqDealBizListDto.setDealBizBailPreRate(new BigDecimal("0"));
                cmisLmt0010ReqDealBizListDto.setDealBizBailPreAmt(new BigDecimal("0"));
                cmisLmt0010ReqDealBizListDto.setEndDate(pvpLoanApp.getLoanEndDate());
                List<CmisLmt0010ReqDealBizListDto> cmisLmt0010ReqDealBizListDtoList = new ArrayList<CmisLmt0010ReqDealBizListDto>();
                cmisLmt0010ReqDealBizListDtoList.add(cmisLmt0010ReqDealBizListDto);
                cmisLmt0010ReqDto.setCmisLmt0010ReqDealBizListDtoList(cmisLmt0010ReqDealBizListDtoList);

                log.info("根据业务申请编号【{}】前往额度系统校验请求报文：【{}】", serno, JSON.toJSONString(cmisLmt0010ReqDto));
                ResultDto<CmisLmt0010RespDto> cmisLmt0010RespDto = cmisLmtClientService.cmisLmt0010(cmisLmt0010ReqDto);
                log.info("根据业务申请编号【{}】前往额度系统校验返回报文：【{}】", serno, JSON.toJSONString(cmisLmt0010RespDto));
                String code = cmisLmt0010RespDto.getData().getErrorCode();

                if ("0000".equals(code) || ("70125".equals(code))) {
                    log.info("根据业务申请编号【{}】,前往额度系统校验成功！", serno);
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
                } else {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(cmisLmt0010RespDto.getData().getErrorMsg()); //校验失败
                }
            } catch (Exception e) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //通过
                riskResultDto.setRiskResultDesc("额度系统校验失败"); //校验失败
                return riskResultDto;
            }
        } else {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_00021); //根据业务申请类型bizType没有匹配到对应的拦截内容
            return riskResultDto;
        }
        return riskResultDto;
    }
}
