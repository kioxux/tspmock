package cn.com.yusys.yusp.workFlow.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.constant.BizFlowConstant;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.domain.LmtReplyAccSubPrd;
import cn.com.yusys.yusp.domain.OtherCnyRateAppFinDetails;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.client.ClientBizInterface;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import cn.com.yusys.yusp.flow.other.enums.OpType;
import cn.com.yusys.yusp.service.LmtReplyAccSubPrdService;
import cn.com.yusys.yusp.service.OtherCnyRateAppFinDetailsService;
import cn.com.yusys.yusp.service.OtherCnyRateAppService;
import cn.com.yusys.yusp.util.BizCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 利率定价流程-展期 --寿光村镇
 *
 * @author lyh
 * @version 1.0
 */
@Service
public class SGCZ10BizService implements ClientBizInterface {

    private final Logger log = LoggerFactory.getLogger(SGCZ10BizService.class);

    @Autowired
    private OtherCnyRateAppService otherCnyRateAppService;

    @Autowired
    private OtherCnyRateAppFinDetailsService otherCnyRateAppFinDetailsService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Override
    public void bizOp(ResultInstanceDto resultInstanceDto) {
        String bizType = resultInstanceDto.getBizType();
        // 利率定价-展期 寿光村镇
        if (BizFlowConstant.SGF04.equals(bizType)) {
            QT002biz(resultInstanceDto);
        } else {
            log.error("非预期的流程对应业务类型");
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(BizException.error(null, EcbEnum.ECB010052.key, EcbEnum.ECB010052.value), resultInstanceDto);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    public void QT002biz(ResultInstanceDto instanceInfo) {
        String currentOpType = instanceInfo.getCurrentOpType();
        String serno = instanceInfo.getBizId();
        log.info("后业务处理类型:" + currentOpType);
        try {
            if (OpType.STRAT.equals(currentOpType)) {
                log.info("发起操作:" + instanceInfo);
                // 改变标志 待发起 -> 审批中
            } else if (OpType.RUN.equals(currentOpType)) {
                log.info("-------业务处理 正常下一步,不做任何操作：-- ----" + instanceInfo);
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_111);
            } else if (OpType.JUMP.equals(currentOpType)) {
                log.info("跳转操作:" + instanceInfo);
            } else if (OpType.END.equals(currentOpType)) {
                log.info("结束操作:" + instanceInfo);
                // 结束后处理
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_997);
                // 同步更新 人民币利率定价申请融资信息明细  other_cny_rate_app_fin_details
                QueryModel queryModel = new QueryModel();
                queryModel.addCondition("serno",serno);

                List<OtherCnyRateAppFinDetails> list = otherCnyRateAppFinDetailsService.selectByModel(queryModel);
                list.forEach(OtherCnyRateAppFinDetails->{
                    OtherCnyRateAppFinDetails.setStatus(CmisBizConstants.STD_CUS_LIST_STATUS_01);
                    otherCnyRateAppFinDetailsService.updateSelective(OtherCnyRateAppFinDetails);
                });
               //TODO hxl
                List<OtherCnyRateAppFinDetails> OtherCnyRateAppFinDetailslist = otherCnyRateAppFinDetailsService.selectByModel(queryModel);
                if(OtherCnyRateAppFinDetailslist.size()>0){
                    for (int i = 0; i < OtherCnyRateAppFinDetailslist.size(); i++) {
                        String subPrdSerno = OtherCnyRateAppFinDetailslist.get(i).getSubPrdSerno();//分项品种流水号
                        BigDecimal execRateYear = OtherCnyRateAppFinDetailslist.get(i).getExecRateYear();//执行年利率
                        QueryModel queryModelLra = new QueryModel();
                        queryModelLra.addCondition("subPrdSerno",subPrdSerno);
                        List<LmtReplyAccSubPrd> lmtReplyAccSubPrds = lmtReplyAccSubPrdService.selectAll(queryModelLra);
                        for(int j=0;j<lmtReplyAccSubPrds.size();j++){
                            lmtReplyAccSubPrds.get(j).setRateYear(execRateYear);
                            lmtReplyAccSubPrdService.updateSelective(lmtReplyAccSubPrds.get(j));
                        }
                    }
                }

                log.info("结束操作完成:" + instanceInfo);
            } else if (OpType.RETURN_BACK.equals(currentOpType)) {
                log.info("退回操作:" + instanceInfo);
                // 否决改变标志 审批中 111 -> 打回 992
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
            } else if (OpType.CALL_BACK.equals(currentOpType)) {
                // 否决改变标志 审批中111 -> 打回992
                log.info("打回操作:" + instanceInfo);
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_992);
            } else if (OpType.REFUSE.equals(currentOpType)) {
                log.info("否决操作:" + instanceInfo);
                // 否决改变标志 审批中 111-> 审批不通过 998
                log.info("否决操作结束:" + instanceInfo);
                otherCnyRateAppService.updateApproveStatus(serno, CmisCommonConstants.WF_STATUS_998);
            } else {
                log.warn("未知操作:" + instanceInfo);
            }
        } catch (Exception e) {
            log.error("后业务处理失败", e);
            try {
                BizCommonUtils bizCommonUtils = new BizCommonUtils();
                bizCommonUtils.getExceptionMessageAndSendExptionMQ(e, instanceInfo);
            } catch (Exception e1) {
                log.error("发送异常消息失败", e1);
            }
        }
    }

    @Override
    public boolean should(ResultInstanceDto resultInstanceDto) {
//        String bizType = resultInstanceDto.getBizType();
        String flowCode = resultInstanceDto.getFlowCode();
        return BizFlowConstant.SGCZ10.equals(flowCode);
    }
}
