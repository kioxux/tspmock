/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.GuarBizRel;
import cn.com.yusys.yusp.dto.GuarBizRelGuarBaseDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarBizRelMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-04-29 10:49:59
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface GuarBizRelMapper {


    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    GuarBizRel selectByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: selectGuarBizRelGuarBaseDto
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    GuarBizRelGuarBaseDto selectGuarBizRelGuarBaseDto(@Param("pkId") String pkId);

    /**
     * @方法名称: selectRecordByGuarNo
     * @方法描述: 根据押品编号查询记录数
     * @参数与返回说明:
     * @算法描述: 无
     */

    int selectRecordByGuarNo(@Param("GuarNo") String GuarNo);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<GuarBizRel> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(GuarBizRel record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(GuarBizRel record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(GuarBizRel record);

    /**
     * @方法名称: updateBySerno
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    int updateBySerno(Map params);


    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(GuarBizRel record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("pkId") String pkId);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    int updateByParams(Map delMap);
    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据流水与证件号查询是否存在相同保证人
     * @参数与返回说明:
     * @算法描述: 无
     */
    int selectGuarantee(Map delMap);

    /**
     * @方法名称: insertGuarBizRelList
     * @方法描述: 批量插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    int insertGuarBizRelList(@Param("list")List<GuarBizRel> guarBizRelList);
    /**
     * @函数名称:deleteByAccSubNo
     * @函数描述:根据授信协议分项流水号关联删除押品关联
     * @参数与返回说明:accSubNo 授信协议分项流水号
     * @算法描述:
     */
    int deleteByAccSubNo(String accSubNo);

    /**
     * @函数名称:queryGuarBizRelDataBySerno
     * @函数描述:根据业务流水号查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarBizRel> queryGuarBizRelDataBySerno(@Param("serno") String serno);

    /**
     * @函数名称:queryMainGuarBizRelDataBySerno
     * @函数描述:根据业务申请流水号以及担保标识查询数据
     * @参数与返回说明:
     * @算法描述:
     */
    List<GuarBizRel> queryMainGuarBizRelDataBySerno(Map map);

    /**
     * @方法名称: queryGuarBizRelDataBySernoAndGuarNo
     * @方法描述: 根据传入的流水号以及押品编号查询数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    GuarBizRel queryGuarBizRelDataBySernoAndGuarNo(Map map);

    /**
     * 查询押品编号
     * @param queryModel
     * @return
     */
    String selectGuarNosByModel(QueryModel queryModel);

    /**
     * 根据流水号和押品编号查询对应融资金额总和
     * @param queryModel
     * @return
     */
    BigDecimal selectTotalCorreFinAmtByModel(QueryModel queryModel);
}