/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtCrdReplyChg;
import cn.com.yusys.yusp.service.LmtCrdReplyChgService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtCrdReplyChgResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-24 11:20:02
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtcrdreplychg")
public class LmtCrdReplyChgResource {
    @Autowired
    private LmtCrdReplyChgService lmtCrdReplyChgService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtCrdReplyChg>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtCrdReplyChg> list = lmtCrdReplyChgService.selectAll(queryModel);
        return new ResultDto<List<LmtCrdReplyChg>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtCrdReplyChg>> index(QueryModel queryModel) {
        List<LmtCrdReplyChg> list = lmtCrdReplyChgService.selectByModel(queryModel);
        return new ResultDto<List<LmtCrdReplyChg>>(list);
    }


    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtCrdReplyChg> create(@RequestBody LmtCrdReplyChg lmtCrdReplyChg) throws URISyntaxException {
        lmtCrdReplyChgService.insert(lmtCrdReplyChg);
        return new ResultDto<LmtCrdReplyChg>(lmtCrdReplyChg);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtCrdReplyChg lmtCrdReplyChg) throws URISyntaxException {
        int result = lmtCrdReplyChgService.update(lmtCrdReplyChg);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete")
    protected ResultDto<Integer> delete(String pkId, String replyNo) {
        int result = lmtCrdReplyChgService.deleteByPrimaryKey(pkId, replyNo);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/saveorupdate")
    protected ResultDto<LmtCrdReplyChg> saveOrUpdate(@RequestBody LmtCrdReplyChg lmtCrdReplyChg) {
        LmtCrdReplyChg record = lmtCrdReplyChgService.saveOrUpdate(lmtCrdReplyChg);
        return new ResultDto<>(record);
    }
    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbypk")
    protected ResultDto<LmtCrdReplyChg> selectByPk(@RequestBody LmtCrdReplyChg lmtCrdReplyChg) {
        LmtCrdReplyChg record= lmtCrdReplyChgService.selectByPrimaryKey(lmtCrdReplyChg.getPkId(),lmtCrdReplyChg.getReplyNo());
        return new ResultDto<>(record);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectnew")
    protected ResultDto<LmtCrdReplyChg> selectnew(@RequestBody LmtCrdReplyChg lmtCrdReplyChg) {
        LmtCrdReplyChg record= lmtCrdReplyChgService.selectNew(lmtCrdReplyChg.getReplyNo());
        return new ResultDto<>(record);
    }

    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectWaitSubmit")
    protected ResultDto<LmtCrdReplyChg> selectWaitSubmit(@RequestBody LmtCrdReplyChg lmtCrdReplyChg) {
        LmtCrdReplyChg record= lmtCrdReplyChgService.selectWaitSubmit(lmtCrdReplyChg.getReplyNo());
        return new ResultDto<>(record);
    }
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<LmtCrdReplyChg>> selectbymodel(@RequestBody QueryModel queryModel) {
        List<LmtCrdReplyChg> list = lmtCrdReplyChgService.selectByModel(queryModel);
        return new ResultDto<List<LmtCrdReplyChg>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param lmtCrdReplyChg
     *            分页查询类
     * @算法描述:
     */
    @PostMapping("/handbiz")
    protected ResultDto<List<LmtCrdReplyChg>> handbiz(@RequestBody LmtCrdReplyChg lmtCrdReplyChg) throws Exception {
        lmtCrdReplyChgService.handleBizEnd(lmtCrdReplyChg.getPkId(),lmtCrdReplyChg.getReplyNo());
        return new ResultDto<List<LmtCrdReplyChg>>();
    }

    /**
     * @param  lmtCrdReplyChg
     * @return
     * @author wzy
     * @date 2021/8/24 14:09
     * @version 1.0.0
     * @desc  查询是否存在在途的流程
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/selectrun")
    protected ResultDto<LmtCrdReplyChg> selectRun(@RequestBody LmtCrdReplyChg lmtCrdReplyChg) {
        LmtCrdReplyChg record= lmtCrdReplyChgService.selectRun(lmtCrdReplyChg.getReplyNo());
        return new ResultDto<>(record);
    }
}
