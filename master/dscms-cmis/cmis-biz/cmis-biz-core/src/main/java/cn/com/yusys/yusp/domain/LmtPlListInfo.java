/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtPlListInfo
 * @类描述: lmt_pl_list_info数据实体类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-07-26 22:50:09
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_pl_list_info")
public class LmtPlListInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 调查流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 科目 **/
	@Column(name = "SUBJECT", unique = false, nullable = true, length = 100)
	private String subject;
	
	/** 科目值 **/
	@Column(name = "SUBJECT_VALUE", unique = false, nullable = true, length = 20)
	private String subjectValue;
	
	/** 上期金额 **/
	@Column(name = "PRE_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal preAmt;
	
	/** 最近一个完成年度金额 **/
	@Column(name = "NEAR_YEAR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal nearYearAmt;
	
	/** 备注信息 **/
	@Column(name = "MEMO", unique = false, nullable = true, length = 500)
	private String memo;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
    /**
     * @return subject
     */
	public String getSubject() {
		return this.subject;
	}
	
	/**
	 * @param subjectValue
	 */
	public void setSubjectValue(String subjectValue) {
		this.subjectValue = subjectValue;
	}
	
    /**
     * @return subjectValue
     */
	public String getSubjectValue() {
		return this.subjectValue;
	}
	
	/**
	 * @param preAmt
	 */
	public void setPreAmt(java.math.BigDecimal preAmt) {
		this.preAmt = preAmt;
	}
	
    /**
     * @return preAmt
     */
	public java.math.BigDecimal getPreAmt() {
		return this.preAmt;
	}
	
	/**
	 * @param nearYearAmt
	 */
	public void setNearYearAmt(java.math.BigDecimal nearYearAmt) {
		this.nearYearAmt = nearYearAmt;
	}
	
    /**
     * @return nearYearAmt
     */
	public java.math.BigDecimal getNearYearAmt() {
		return this.nearYearAmt;
	}
	
	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	
    /**
     * @return memo
     */
	public String getMemo() {
		return this.memo;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}