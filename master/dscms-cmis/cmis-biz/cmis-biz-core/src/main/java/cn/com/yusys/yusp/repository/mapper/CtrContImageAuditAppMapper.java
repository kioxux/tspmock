/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.repository.mapper;
import cn.com.yusys.yusp.domain.CtrContImageAuditAppDomain;
import cn.com.yusys.yusp.domain.CtrLoanCont;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.domain.CtrContImageAuditApp;
import  cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CtrContImageAuditAppMapper
 * @类描述: #Dao类
 * @功能描述: 
 * @创建人: zrcbank
 * @创建时间: 2021-06-24 10:06:37
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public interface CtrContImageAuditAppMapper {

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    CtrContImageAuditApp selectByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件列表查询
     * @参数与返回说明: 
     * @算法描述: 无
     */
    List<CtrContImageAuditApp> selectByModel(QueryModel model);
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    
    int insert(CtrContImageAuditApp record);

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int insertSelective(CtrContImageAuditApp record);

    /**
     * @方法名称: updateByPrimaryKey
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKey(CtrContImageAuditApp record);

    /**
     * @方法名称: updateByPrimaryKeySelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int updateByPrimaryKeySelective(CtrContImageAuditApp record);

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: sysId - 主键
     * @算法描述: 无
     */
    
    int deleteByPrimaryKey(@Param("serno") String serno);

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    int deleteByIds(@Param("ids") String ids);

    /**
     * @param contNo
     * @return int
     * @author 王玉坤
     * @date 2021/5/3 17:40
     * @version 1.0.0
     * @desc 根据合同号、申请状态查询申请信息
     * @修改历史: 修改时间 2021年6月24日11:08:58    修改人员 王浩   修改原因 增加了过滤条件 操作类型=01
     */
    int queryByContNo(@Param("contNo") String contNo);

    /**
     * @创建人 WH
     * @创建时间 2021-05-07 19:41
     * @注释 条件查询 已处理
     */
    List<CtrContImageAuditAppDomain> selectByModel2(QueryModel model);

    /**
     * @创建人 WH
     * @创建时间 2021-05-07 19:41
     * @注释 条件查询 已处理
     */
    List<CtrContImageAuditAppDomain> selectByModel3(QueryModel model);

    /**
     * @创建人 zxz
     * @创建时间 2021-05-11 19:41
     * @注释 条件查询 已处理
     */
    List<CtrContImageAuditAppDomain> selectCtrImageByMode(QueryModel model);

    /**
     * @创建人 zxz
     * @创建时间 2021-05-11 19:41
     * @注释 条件查询 已处理
     */
    List<CtrContImageAuditAppDomain> selectCtrImageHisByMode(QueryModel model);

    /**
     * 根据客户号查询生效的合同到期日超过当天的存在当天的通过的查封记录的省心快贷合同
     *
     * @param map
     * @return
     */
    List<CtrLoanCont> selectCtrContImageAuditAppByCusid(Map map);

    int updateApproveStatus(@Param("contNo") String contNo, @Param("approveStatus") String approveStatus);

    int updateIsfk(@Param("contNo") String contNo, @Param("isFk") String isFk);

    CtrContImageAuditApp selectModelByContKey(String contNo);

    /**
     * @param serno
     * @return CtrContImageAuditApp
     * @author quwen
     * @date 2021/8/2 17:00
     * @version 1.0.0
     * @desc 根据流水号查询合同影像申请信息
     * @修改历史:
     */
    CtrContImageAuditApp queryCtrContImageAuditAppBySerno(@Param("serno") String serno);

    /**
     * 根据合同号查询影像审核申请信息
     * @param contNo
     * @return
     */
    CtrContImageAuditApp selectByContNo(@Param("contNo") String contNo);
}