package cn.com.yusys.yusp.web.server.xdtz0051;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0051.req.Xdtz0051DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0051.resp.Xdtz0051DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.server.xdtz0051.Xdtz0051Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 接口处理类:对公客户关联业务检查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0051:对公客户关联业务检查")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0051Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0051Resource.class);

    @Autowired
    private Xdtz0051Service xdtz0051Service;


    /**
     * 交易码：xdtz0051
     * 交易描述：对公客户关联业务检查
     *
     * @param xdtz0051DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对公客户关联业务检查")
    @PostMapping("/xdtz0051")
    protected @ResponseBody
    ResultDto<Xdtz0051DataRespDto> xdtz0051(@Validated @RequestBody Xdtz0051DataReqDto xdtz0051DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, JSON.toJSONString(xdtz0051DataReqDto));
        // 响应Dto:对公客户关联业务检查
        Xdtz0051DataRespDto xdtz0051DataRespDto = new Xdtz0051DataRespDto();
        ResultDto<Xdtz0051DataRespDto> xdtz0051DataResultDto = new ResultDto<>();
        try {
            //校验传入的cusId是否有效（不为空串或null，暂时没有进行正则校验）
            String cusId = xdtz0051DataReqDto.getCusId();
            if (StringUtils.isBlank(cusId)) {
                xdtz0051DataResultDto.setCode(EpbEnum.EPB090009.key);
                xdtz0051DataResultDto.setMessage(EpbEnum.EPB090009.value);
                xdtz0051DataResultDto.setData(xdtz0051DataRespDto);
                return xdtz0051DataResultDto;
            }
            //执行主业务逻辑
            xdtz0051DataRespDto = xdtz0051Service.xdtz0051(cusId);
            if (StringUtils.isBlank(xdtz0051DataRespDto.getOpFlag())) {
                xdtz0051DataResultDto.setCode(DscmsBizTzEnum.DELLEET_FAIL.key);
                xdtz0051DataResultDto.setMessage(DscmsBizTzEnum.DELLEET_FAIL.value);
                xdtz0051DataResultDto.setData(xdtz0051DataRespDto);
                return xdtz0051DataResultDto;
            }
            // 封装xdtz0051DataResultDto中正确的返回码和返回信息
            if (DscmsBizTzEnum.YESNO_NO.key.equals(xdtz0051DataRespDto.getOpFlag())) {//删除失败
                xdtz0051DataResultDto.setCode(DscmsBizTzEnum.DELLEET_FAIL.key);
                xdtz0051DataResultDto.setMessage(DscmsBizTzEnum.DELLEET_FAIL.value);
            } else {//删除成功
                xdtz0051DataResultDto.setCode(DscmsBizTzEnum.DELLEET_SUCCESS.key);
                xdtz0051DataResultDto.setMessage(DscmsBizTzEnum.DELLEET_SUCCESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, e.getMessage());
            // 封装xdtz0051DataResultDto中异常返回码和返回信息
            xdtz0051DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0051DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0051DataRespDto到xdtz0051DataResultDto中
        xdtz0051DataResultDto.setData(xdtz0051DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, JSON.toJSONString(xdtz0051DataResultDto));
        return xdtz0051DataResultDto;
    }
}
