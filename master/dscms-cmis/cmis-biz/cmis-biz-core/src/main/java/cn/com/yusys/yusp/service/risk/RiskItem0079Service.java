package cn.com.yusys.yusp.service.risk;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo;
import cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.LoanList;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
@Service
public class RiskItem0079Service {
    @Autowired
    private BizMustCheckDetailsService bizMustCheckDetailsService;

    @Autowired
    private LmtAppSubService lmtAppSubService;

    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    public RiskResultDto riskItem0079(QueryModel queryModel) {
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = "";
        if(Objects.nonNull(queryModel.getCondition().get("bizId"))){
            serno= queryModel.getCondition().get("bizId").toString();
        }
        if (StringUtils.isBlank(serno)) {
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001); //业务流水号为空
            return riskResultDto;
        }

        List<BizMustCheckDetails> bizMustCheckDetailsList = bizMustCheckDetailsService.selectBySerno(serno);
        if(CollectionUtils.nonEmpty(bizMustCheckDetailsList)){
            for (BizMustCheckDetails bizMustCheckDetails : bizMustCheckDetailsList) {
                if("khxejzxpj".equals(bizMustCheckDetails.getPageId())){
                    boolean khxejzxpj = true;
                    List<LmtAppSub> subList = lmtAppSubService.queryLmtAppSubBySerno(serno);
                    for (LmtAppSub lmtAppSub : subList) {
                        if(!khxejzxpj){
                            break;
                        }
                        if(!CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsPreLmt()) && !lmtAppSub.getGuarMode().equals(CmisCommonConstants.GUAR_MODE_60)) {
                            List<LmtAppSubPrd> lmtAppSubPrds = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                            for (LmtAppSubPrd lmtAppSubPrd : lmtAppSubPrds) {
                                if(!lmtAppSubPrd.getLmtBizType().startsWith("140203") && !lmtAppSubPrd.getLmtBizType().startsWith("12010103")){
                                    khxejzxpj = false;
                                    break;
                                }
                            }
                        }
                    }
                    if(khxejzxpj){
                        continue;
                    }
                }else if("lsnp".equals(bizMustCheckDetails.getPageId())){
                    boolean lsnp = true;
                    List<LmtAppSub> subList = lmtAppSubService.queryLmtAppSubBySerno(serno);
                    for(LmtAppSub lmtAppSub :subList){
                        if(!lsnp){
                            break;
                        }
                        if(!CmisCommonConstants.YES_NO_1.equals(lmtAppSub.getIsPreLmt()) && !CmisCommonConstants.GUAR_MODE_60.equals(lmtAppSub.getGuarMode())){
                            List<LmtAppSubPrd> subPrdList = lmtAppSubPrdService.selectBySubSerno(lmtAppSub.getSubSerno());
                            for(LmtAppSubPrd lmtAppSubPrd:subPrdList) {
                                // 业务品种
                                String bizTYpe = lmtAppSubPrd.getLmtBizType();
                                if ("20010102".equals(bizTYpe) || "20020101".equals(bizTYpe)) {
                                    lsnp = false;
                                    break;
                                }
                            }
                        }
                    }
                    if(lsnp){
                        continue;
                    }
                }
                if(CmisCommonConstants.STD_ZB_YES_NO_0.equals(bizMustCheckDetails.getFinFlag())){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1); //不通过
                    riskResultDto.setRiskResultDesc(bizMustCheckDetails.getPageName()+"信息不完整！");
                    return riskResultDto;
                }
            }
        }else{
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
            return riskResultDto;
        }

        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0); //通过
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000); //校验通过
        return riskResultDto;
    }
}
