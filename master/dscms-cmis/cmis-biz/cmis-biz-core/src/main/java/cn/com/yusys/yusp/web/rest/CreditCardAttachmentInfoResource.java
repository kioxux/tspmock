/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardAttachmentInfo;
import cn.com.yusys.yusp.service.CreditCardAttachmentInfoService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardAttachmentInfoResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-24 19:24:00
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "信用卡申请附件证明信息")
@RestController
@RequestMapping("/api/creditcardattachmentinfo")
public class CreditCardAttachmentInfoResource {
    @Autowired
    private CreditCardAttachmentInfoService creditCardAttachmentInfoService;

	/**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardAttachmentInfo>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardAttachmentInfo> list = creditCardAttachmentInfoService.selectAll(queryModel);
        return new ResultDto<List<CreditCardAttachmentInfo>>(list);
    }
	
    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<CreditCardAttachmentInfo>> index(QueryModel queryModel) {
        List<CreditCardAttachmentInfo> list = creditCardAttachmentInfoService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardAttachmentInfo>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CreditCardAttachmentInfo> show(@PathVariable("serno") String serno) {
        CreditCardAttachmentInfo creditCardAttachmentInfo = creditCardAttachmentInfoService.selectByPrimaryKey(serno);
        return new ResultDto<CreditCardAttachmentInfo>(creditCardAttachmentInfo);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("信用卡申请-附件信息新增")
    @PostMapping("/")
    protected ResultDto<CreditCardAttachmentInfo> create(@RequestBody CreditCardAttachmentInfo creditCardAttachmentInfo) throws URISyntaxException {
        creditCardAttachmentInfoService.insert(creditCardAttachmentInfo);
        return new ResultDto<CreditCardAttachmentInfo>(creditCardAttachmentInfo);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("信用卡申请-附件信息修改")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardAttachmentInfo creditCardAttachmentInfo) throws URISyntaxException {
        int result = creditCardAttachmentInfoService.update(creditCardAttachmentInfo);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("信用卡申请-附件信息删除")
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = creditCardAttachmentInfoService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = creditCardAttachmentInfoService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param  creditCardAttachmentInfo
     * @return  ResultDto
     * @author wzy
     * @date 2021/5/25 20:49
     * @version 1.0.0
     * @desc 根据主键查询信用卡申请附录信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据主键查询信用卡申请附录信息")
    @PostMapping("/querybyserno")
    protected ResultDto<CreditCardAttachmentInfo> show(@RequestBody CreditCardAttachmentInfo creditCardAttachmentInfo) {
        CreditCardAttachmentInfo creditCardAttachmentInfo2 = creditCardAttachmentInfoService.selectByPrimaryKey(creditCardAttachmentInfo.getSerno());
        return new ResultDto<CreditCardAttachmentInfo>(creditCardAttachmentInfo2);
    }

}
