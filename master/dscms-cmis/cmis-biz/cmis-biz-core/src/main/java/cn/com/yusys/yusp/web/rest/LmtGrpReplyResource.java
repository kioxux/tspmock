/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.LmtGrpApp;
import cn.com.yusys.yusp.domain.LmtGrpReplyAcc;
import cn.com.yusys.yusp.domain.LmtReply;
import cn.com.yusys.yusp.dto.CusGrpMemberAppDto;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.LmtGrpAppService;
import cn.com.yusys.yusp.service.LmtReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtGrpReply;
import cn.com.yusys.yusp.service.LmtGrpReplyService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtGrpReplyResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-20 11:06:44
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/lmtgrpreply")
public class LmtGrpReplyResource {
    @Autowired
    private LmtGrpReplyService lmtGrpReplyService;

    @Autowired
    private LmtGrpAppService lmtGrpAppService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    @Autowired
    private LmtReplyService lmtReplyService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtGrpReply>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtGrpReply> list = lmtGrpReplyService.selectAll(queryModel);
        return new ResultDto<List<LmtGrpReply>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtGrpReply>> index(QueryModel queryModel) {
        List<LmtGrpReply> list = lmtGrpReplyService.selectByModel(queryModel);
        return new ResultDto<List<LmtGrpReply>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtGrpReply> show(@PathVariable("pkId") String pkId) {
        LmtGrpReply lmtGrpReply = lmtGrpReplyService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtGrpReply>(lmtGrpReply);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtGrpReply> create(@RequestBody LmtGrpReply lmtGrpReply) throws URISyntaxException {
        lmtGrpReplyService.insert(lmtGrpReply);
        return new ResultDto<LmtGrpReply>(lmtGrpReply);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtGrpReply lmtGrpReply) throws URISyntaxException {
        int result = lmtGrpReplyService.update(lmtGrpReply);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtGrpReplyService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtGrpReplyService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @方法名称：getHisReply
     * @方法描述：获取批复历史沿革
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-04-21 下午 8:06
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getHisReply")
    protected ResultDto<Map> getHisReply(@RequestBody QueryModel queryModel) {
        String grpReplySerno = (String) queryModel.getCondition().get("grpReplySerno");
        List<LmtGrpReply> originReply = lmtGrpReplyService.getOriginReply(grpReplySerno);
        return ResultDto.success(originReply);
    }

    /**
     * @方法名称：getChangeableReplyNo
     * @方法描述：查询可变更的批复号
     * @参数与返回说明：
     * @算法描述：
     * @创建人：ywl
     * @创建时间：2021-04-21 下午 8:06
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getChangeableReplyNo")
    protected ResultDto<List<LmtGrpReply>> getChangeableReplyNo(@RequestBody QueryModel queryModel) {
        List<LmtGrpReply> lmtGrpReplyList = lmtGrpReplyService.qryChangeableReply(queryModel);
        return new ResultDto<>(lmtGrpReplyList);
    }

    /**
     * @方法名称：getUnchangeableReplyNo
     * @方法描述：查询不可变更的批复号
     * @参数与返回说明：
     * @算法描述：
     * @创建人：qw
     * @创建时间：2021-05-21 下午 2:50
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/getunchangeablereplyno")
    protected ResultDto<List<LmtGrpReply>> getUnchangeableReplyNo(@RequestBody QueryModel queryModel) {
        List<LmtGrpReply> lmtGrpReplyList = lmtGrpReplyService.queryUnchangeableReply(queryModel);
        return new ResultDto<>(lmtGrpReplyList);
    }

    /**
     * @方法名称：replyForManager
     * @方法描述：获取当前登录人下的批复信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-04-29 上午 9:07
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/replyForManager")
    protected ResultDto<List<LmtGrpReply>> replyForManager() {
        List<LmtGrpReply> lmtGrpReplyList = lmtGrpReplyService.queryByManagerId();
        return new ResultDto<>(lmtGrpReplyList);
    }

    /**
     * @方法名称：viewLmtGrpReplyByGrpReplySerno
     * @方法描述：集团授信原批复查看
     * @参数与返回说明：
     * @算法描述：
     * @创建人：yangwl
     * @创建时间：2021-07-5 下午 9:07
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/viewlmtgrpreplybygrpreplyserno")
    protected ResultDto<LmtGrpReply> viewLmtGrpReplyByGrpReplySerno(@RequestBody String grpReplySerno) {
        LmtGrpReply lmtGrpReply = lmtGrpReplyService.viewLmtGrpReplyByGrpReplySerno(grpReplySerno);
        return ResultDto.success(lmtGrpReply);
    }

    @PostMapping("/getGrpMemberLmtAmt")
    protected BigDecimal getGrpMemberLmtAmt(@RequestParam("serno") String serno, @RequestParam("grpNo") String grpNo) {
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("grpNo", grpNo);
        queryModel.addCondition("serno", serno);
        queryModel.addCondition("oprType", "01");
        ResultDto<List<CusGrpMemberAppDto>> cusGrpMemberList = cmisCusClientService.queryCusGrpMemberApp(queryModel);
        BigDecimal totalAmount = BigDecimal.ZERO;
        if (CollectionUtils.nonEmpty(cusGrpMemberList.getData())) {
            for (int i = 0; i < cusGrpMemberList.getData().size(); i++) {
                Map data = (Map) cusGrpMemberList.getData().get(i);
                String cusId = (String) data.get("cusId");
                QueryModel model = new QueryModel();
                model.addCondition("cusId", cusId);
                List<LmtReply> list = lmtReplyService.selectByCusId(model);
                if (CollectionUtils.nonEmpty(list)) {
                    LmtReply lmtReply = list.get(0);
                    totalAmount = totalAmount.add(lmtReply.getOpenTotalLmtAmt());
                }
            }
        }

        return totalAmount;
    }

    /**
     * @方法名称：selectorigilmtgrpreplydatabygrpserno
     * @方法描述：根据授信申请流水号查询原批复信息
     * @参数与返回说明：
     * @算法描述：
     * @创建人：css
     * @创建时间：2021-10-29 下午 15:07
     * @修改记录：修改时间 修改人员  修改原因
     */
    @PostMapping("/selectorigilmtgrpreplydatabygrpserno")
    protected ResultDto<LmtGrpReply> selectOrigiLmtGrpReplyDataByGrpSerno(@RequestBody String grpSerno) {
        LmtGrpReply lmtGrpReply = lmtGrpReplyService.selectOrigiLmtGrpReplyDataByGrpSerno(grpSerno);
        return ResultDto.success(lmtGrpReply);
    }

    @PostMapping("/getbysingleserno")
    protected ResultDto<LmtGrpReply> getBySingleSerno(@RequestBody String singleSerno) throws Exception {
        LmtGrpReply lmtGrpReply=lmtGrpReplyService.getBySingleSerno(singleSerno);
        return new ResultDto<LmtGrpReply>(lmtGrpReply);
    }
}
