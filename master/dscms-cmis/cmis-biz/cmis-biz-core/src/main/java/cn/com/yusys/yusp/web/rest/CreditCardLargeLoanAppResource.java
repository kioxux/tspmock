/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import cn.com.yusys.yusp.domain.CreditCardLargeLoanApp;
import cn.com.yusys.yusp.dto.CreditcradShisuanDto;
import cn.com.yusys.yusp.service.CreditCardLargeLoanAppService;
import cn.com.yusys.yusp.service.CreditCtrLoanContService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.CreditCardLargeLoanApp;
import cn.com.yusys.yusp.service.CreditCardLargeLoanAppService;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CreditCardLargeLoanAppResource
 * @类描述: #资源类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-08-18 20:58:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@Api(tags = "信用卡大额分期申请")
@RequestMapping("/api/creditcardlargeloanapp")
public class CreditCardLargeLoanAppResource {
    @Autowired
    private CreditCardLargeLoanAppService CreditCardLargeLoanAppService;
    @Autowired
    private CreditCtrLoanContService creditCtrLoanContService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<CreditCardLargeLoanApp>> query() {
        QueryModel queryModel = new QueryModel();
        List<CreditCardLargeLoanApp> list = CreditCardLargeLoanAppService.selectAll(queryModel);
        return new ResultDto<List<CreditCardLargeLoanApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @GetMapping("//")
    protected ResultDto<List<CreditCardLargeLoanApp>> index(QueryModel queryModel) {
        List<CreditCardLargeLoanApp> list = CreditCardLargeLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardLargeLoanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{serno}")
    protected ResultDto<CreditCardLargeLoanApp> show(@PathVariable("serno") String serno) {
        CreditCardLargeLoanApp CreditCardLargeLoanApp = CreditCardLargeLoanAppService.selectByPrimaryKey(serno);
        return new ResultDto<CreditCardLargeLoanApp>(CreditCardLargeLoanApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("新增大额分期申请")
    @PostMapping("/")
    protected ResultDto<CreditCardLargeLoanApp> create(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApp) throws URISyntaxException {
        CreditCardLargeLoanAppService.insert(CreditCardLargeLoanApp);
        return new ResultDto<CreditCardLargeLoanApp>(CreditCardLargeLoanApp);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("自己写的新增大额分期申请")
    @PostMapping("/save")
    protected ResultDto<CreditCardLargeLoanApp> save(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApp) throws URISyntaxException {
        CreditCardLargeLoanAppService.save(CreditCardLargeLoanApp);
        return new ResultDto<CreditCardLargeLoanApp>(CreditCardLargeLoanApp);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("更新大额分期申请")
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApp) throws URISyntaxException {
        int result = CreditCardLargeLoanAppService.update(CreditCardLargeLoanApp);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过主键删除大额分期申请")
    @PostMapping("/delete/{serno}")
    protected ResultDto<Integer> delete(@PathVariable("serno") String serno) {
        int result = CreditCardLargeLoanAppService.deleteByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:cut
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("自己写的通过主键删除大额分期申请")
    @PostMapping("/cut")
    protected ResultDto<Integer> cut(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApp) {
        String serno = CreditCardLargeLoanApp.getSerno();
        int result = CreditCardLargeLoanAppService.cutByPrimaryKey(serno);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = CreditCardLargeLoanAppService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询大额分期申请为待发起")
    @PostMapping("/querymodel")
    protected ResultDto<List<CreditCardLargeLoanApp>> indexPost(@RequestBody QueryModel queryModel) {
        List<CreditCardLargeLoanApp> list = CreditCardLargeLoanAppService.selectByModelStatus(queryModel);
        return new ResultDto<List<CreditCardLargeLoanApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询大额分期申请")
    @PostMapping("/query")
    protected ResultDto<List<CreditCardLargeLoanApp>> queryPost(@RequestBody QueryModel queryModel) {
        List<CreditCardLargeLoanApp> list = CreditCardLargeLoanAppService.selectByModel(queryModel);
        return new ResultDto<List<CreditCardLargeLoanApp>>(list);
    }

    /**
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @param queryModel
     *            分页查询类
     * @算法描述:
     */
    @ApiOperation("查询大额分期申请不等于某状态")
    @PostMapping("/querybystatus")
    protected ResultDto<List<CreditCardLargeLoanApp>> queryByStatus(@RequestBody QueryModel queryModel) {
        List<CreditCardLargeLoanApp> list = CreditCardLargeLoanAppService.selectByNotStatus(queryModel);
        return new ResultDto<List<CreditCardLargeLoanApp>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("通过主键查询大额分期申请")
    @PostMapping("/selectbyserno")
    protected ResultDto<CreditCardLargeLoanApp> showPost(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApps) {
        CreditCardLargeLoanApp CreditCardLargeLoanApp = CreditCardLargeLoanAppService.selectByPrimaryKey(CreditCardLargeLoanApps.getSerno());
        return new ResultDto<CreditCardLargeLoanApp>(CreditCardLargeLoanApp);
    }

    /**
     * @函数名称:shisuan
     * @函数描述:试算
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("大额分期申请试算")
    @PostMapping("/shisuan")
    // TODO(“试算需要调信用卡接口") ；
    protected ResultDto<CreditcradShisuanDto> shisuan(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApps) {
        ResultDto<CreditcradShisuanDto> creditcradShisuanDto = CreditCardLargeLoanAppService.shisuan(CreditCardLargeLoanApps);
        return creditcradShisuanDto;
    }

    /**
     * @param  CreditCardLargeLoanApps
     * @return
     * @author wzy
     * @date 2021/8/9 21:11
     * @version 1.0.0
     * @desc 审批中大额分期申请试算
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("审批中大额分期申请试算")
    @PostMapping("/shisuanstep")
    // TODO(“试算需要调信用卡接口") ；
    protected ResultDto<CreditcradShisuanDto> shisuanstep(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApps) {
        ResultDto<CreditcradShisuanDto> creditcradShisuanDto = CreditCardLargeLoanAppService.shisuanStep(CreditCardLargeLoanApps);
        return creditcradShisuanDto;
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @ApiOperation("零售内评自动决策")
    @PostMapping("/lsnpzdjc")
    // TODO(“零售内评自动决策需要调别人的接口") ；
    protected Boolean lsnpzdjc(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApps) {
        // TODO(“需要走流程返回true，不需要走流程返回false") ；
        return true;
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/7/3 16:28
     * @version 1.0.0
     * @desc  大额分期申请征信查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("大额分期申请征信信息生成")
    @PostMapping("/getcreditreportinfo")
    protected ResultDto<Integer> getCreditReportInfo(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApps) {
        int result = CreditCardLargeLoanAppService.getCreditReportInfo(CreditCardLargeLoanApps);
        return new ResultDto<Integer>(result);
    }

    /**
     * @param
     * @return
     * @author wzy
     * @date 2021/7/3 16:28
     * @version 1.0.0
     * @desc  大额分期申请征信查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("流程结束处理")
    @PostMapping("/handleBusinessDataAfterEnd")
    protected ResultDto<Integer> handleBusinessDataAfterEnd(@RequestBody CreditCardLargeLoanApp CreditCardLargeLoanApps) {
        CreditCardLargeLoanAppService.handleBusinessDataAfterEnd(CreditCardLargeLoanApps.getSerno());
        return new ResultDto<Integer>(1);
    }
}
