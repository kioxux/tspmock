package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpHouse
 * @类描述: iqp_house数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2021-05-08 09:38:23
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class IqpHouseDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	
	/** 申请流水号 **/
	private String iqpSerno;
	
	/** 合同编号 **/
	private String contNo;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 交易类型 STD_ZB_TRADE_TYPE **/
	private String tradeType;
	
	/** 房屋交易价格 (元） **/
	private java.math.BigDecimal houseTotal;
	
	/** 首付款金额（元） **/
	private java.math.BigDecimal firstpayAmt;
	
	/** 首付比例（%） **/
	private java.math.BigDecimal firstpayPerc;
	
	/** 商贷月还款额(元) **/
	private java.math.BigDecimal monthRepayAmt;
	
	/** 购买楼盘/中介机构 **/
	private String houseBch;
	
	/** 竣工日期 **/
	private String competionDate;
	
	/** 建造年份 **/
	private String buldingYear;
	
	/** 二手房卖方姓名 **/
	private String secBuyerName;
	
	/** 二手房卖方联系电话 **/
	private String phone;
	
	/** 开发商名称 **/
	private String developersName;
	
	/** 建筑面积（平米） **/
	private java.math.BigDecimal houseSqu;
	
	/** 房屋详细地址 **/
	private String houseAddr;
	
	/** 我行认定的房屋价值（元） **/
	private java.math.BigDecimal houseValue;
	
	/** 购房合同编号 **/
	private String purHouseContNo;
	
	/** 按揭贷款成数（%） **/
	private java.math.BigDecimal schedLoanPct;
	
	/** 是否担保公司担保 STD_ZB_YES_NO **/
	private String isGuarCprtGuar;
	
	/** 原贷款银行 **/
	private String origiLoanBank;
	
	/** 原贷款余额（元） **/
	private java.math.BigDecimal origiLoanBal;
	
	/** 是否开发商担保 STD_ZB_YES_NO **/
	private String isDevelopersGua;
	
	/** 公积金贷款金额(元) **/
	private java.math.BigDecimal pundAmt;
	
	/** 公积金贷款期限(月) **/
	private Integer pundTerm;
	
	/** 公积金贷款利率(%) **/
	private java.math.BigDecimal pundRate;
	
	/** 公积金贷款还款方式 **/
	private String pundRepayMode;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	private String oprType;
	
	/** 预售许可证编号 **/
	private String presellLicNo;
	
	/** 每月物业费 **/
	private java.math.BigDecimal propertyExpense;
	
	/** 购房套数 **/
	private String purHouseNum;
	
	/** 购房次数 **/
	private String purHouseTimes;
	
	/** 房屋结构 **/
	private String houseStr;
	
	/** 土地使用方式 **/
	private String landUtilMode;
	
	/** 房屋建成时间 **/
	private String houseBuildTime;
	
	/** 土地剩余使用年限 **/
	private String landSurplusYears;
	
	/** 房龄 **/
	private String houseYears;
	
	/** 是否限购区域 **/
	private String purResFlag;
	
	/** 房屋坐落区县 **/
	private String houseCountry;
	
	/** 房屋状况 **/
	private String houseCondition;
	
	/** 房屋用途 **/
	private String houseUse;
	
	/** 房屋类型 **/
	private String houseType;
	
	/** 房屋单价(m2) **/
	private java.math.BigDecimal sizePrice;
	
	/** 特殊情况说明 **/
	private String speCaseDesc;
	
	/** 其他说明 **/
	private String otherDesc;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno == null ? null : iqpSerno.trim();
	}
	
    /**
     * @return IqpSerno
     */	
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param contNo
	 */
	public void setContNo(String contNo) {
		this.contNo = contNo == null ? null : contNo.trim();
	}
	
    /**
     * @return ContNo
     */	
	public String getContNo() {
		return this.contNo;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param tradeType
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType == null ? null : tradeType.trim();
	}
	
    /**
     * @return TradeType
     */	
	public String getTradeType() {
		return this.tradeType;
	}
	
	/**
	 * @param houseTotal
	 */
	public void setHouseTotal(java.math.BigDecimal houseTotal) {
		this.houseTotal = houseTotal;
	}
	
    /**
     * @return HouseTotal
     */	
	public java.math.BigDecimal getHouseTotal() {
		return this.houseTotal;
	}
	
	/**
	 * @param firstpayAmt
	 */
	public void setFirstpayAmt(java.math.BigDecimal firstpayAmt) {
		this.firstpayAmt = firstpayAmt;
	}
	
    /**
     * @return FirstpayAmt
     */	
	public java.math.BigDecimal getFirstpayAmt() {
		return this.firstpayAmt;
	}
	
	/**
	 * @param firstpayPerc
	 */
	public void setFirstpayPerc(java.math.BigDecimal firstpayPerc) {
		this.firstpayPerc = firstpayPerc;
	}
	
    /**
     * @return FirstpayPerc
     */	
	public java.math.BigDecimal getFirstpayPerc() {
		return this.firstpayPerc;
	}
	
	/**
	 * @param monthRepayAmt
	 */
	public void setMonthRepayAmt(java.math.BigDecimal monthRepayAmt) {
		this.monthRepayAmt = monthRepayAmt;
	}
	
    /**
     * @return MonthRepayAmt
     */	
	public java.math.BigDecimal getMonthRepayAmt() {
		return this.monthRepayAmt;
	}
	
	/**
	 * @param houseBch
	 */
	public void setHouseBch(String houseBch) {
		this.houseBch = houseBch == null ? null : houseBch.trim();
	}
	
    /**
     * @return HouseBch
     */	
	public String getHouseBch() {
		return this.houseBch;
	}
	
	/**
	 * @param competionDate
	 */
	public void setCompetionDate(String competionDate) {
		this.competionDate = competionDate == null ? null : competionDate.trim();
	}
	
    /**
     * @return CompetionDate
     */	
	public String getCompetionDate() {
		return this.competionDate;
	}
	
	/**
	 * @param buldingYear
	 */
	public void setBuldingYear(String buldingYear) {
		this.buldingYear = buldingYear == null ? null : buldingYear.trim();
	}
	
    /**
     * @return BuldingYear
     */	
	public String getBuldingYear() {
		return this.buldingYear;
	}
	
	/**
	 * @param secBuyerName
	 */
	public void setSecBuyerName(String secBuyerName) {
		this.secBuyerName = secBuyerName == null ? null : secBuyerName.trim();
	}
	
    /**
     * @return SecBuyerName
     */	
	public String getSecBuyerName() {
		return this.secBuyerName;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone == null ? null : phone.trim();
	}
	
    /**
     * @return Phone
     */	
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param developersName
	 */
	public void setDevelopersName(String developersName) {
		this.developersName = developersName == null ? null : developersName.trim();
	}
	
    /**
     * @return DevelopersName
     */	
	public String getDevelopersName() {
		return this.developersName;
	}
	
	/**
	 * @param houseSqu
	 */
	public void setHouseSqu(java.math.BigDecimal houseSqu) {
		this.houseSqu = houseSqu;
	}
	
    /**
     * @return HouseSqu
     */	
	public java.math.BigDecimal getHouseSqu() {
		return this.houseSqu;
	}
	
	/**
	 * @param houseAddr
	 */
	public void setHouseAddr(String houseAddr) {
		this.houseAddr = houseAddr == null ? null : houseAddr.trim();
	}
	
    /**
     * @return HouseAddr
     */	
	public String getHouseAddr() {
		return this.houseAddr;
	}
	
	/**
	 * @param houseValue
	 */
	public void setHouseValue(java.math.BigDecimal houseValue) {
		this.houseValue = houseValue;
	}
	
    /**
     * @return HouseValue
     */	
	public java.math.BigDecimal getHouseValue() {
		return this.houseValue;
	}
	
	/**
	 * @param purHouseContNo
	 */
	public void setPurHouseContNo(String purHouseContNo) {
		this.purHouseContNo = purHouseContNo == null ? null : purHouseContNo.trim();
	}
	
    /**
     * @return PurHouseContNo
     */	
	public String getPurHouseContNo() {
		return this.purHouseContNo;
	}
	
	/**
	 * @param schedLoanPct
	 */
	public void setSchedLoanPct(java.math.BigDecimal schedLoanPct) {
		this.schedLoanPct = schedLoanPct;
	}
	
    /**
     * @return SchedLoanPct
     */	
	public java.math.BigDecimal getSchedLoanPct() {
		return this.schedLoanPct;
	}
	
	/**
	 * @param isGuarCprtGuar
	 */
	public void setIsGuarCprtGuar(String isGuarCprtGuar) {
		this.isGuarCprtGuar = isGuarCprtGuar == null ? null : isGuarCprtGuar.trim();
	}
	
    /**
     * @return IsGuarCprtGuar
     */	
	public String getIsGuarCprtGuar() {
		return this.isGuarCprtGuar;
	}
	
	/**
	 * @param origiLoanBank
	 */
	public void setOrigiLoanBank(String origiLoanBank) {
		this.origiLoanBank = origiLoanBank == null ? null : origiLoanBank.trim();
	}
	
    /**
     * @return OrigiLoanBank
     */	
	public String getOrigiLoanBank() {
		return this.origiLoanBank;
	}
	
	/**
	 * @param origiLoanBal
	 */
	public void setOrigiLoanBal(java.math.BigDecimal origiLoanBal) {
		this.origiLoanBal = origiLoanBal;
	}
	
    /**
     * @return OrigiLoanBal
     */	
	public java.math.BigDecimal getOrigiLoanBal() {
		return this.origiLoanBal;
	}
	
	/**
	 * @param isDevelopersGua
	 */
	public void setIsDevelopersGua(String isDevelopersGua) {
		this.isDevelopersGua = isDevelopersGua == null ? null : isDevelopersGua.trim();
	}
	
    /**
     * @return IsDevelopersGua
     */	
	public String getIsDevelopersGua() {
		return this.isDevelopersGua;
	}
	
	/**
	 * @param pundAmt
	 */
	public void setPundAmt(java.math.BigDecimal pundAmt) {
		this.pundAmt = pundAmt;
	}
	
    /**
     * @return PundAmt
     */	
	public java.math.BigDecimal getPundAmt() {
		return this.pundAmt;
	}
	
	/**
	 * @param pundTerm
	 */
	public void setPundTerm(Integer pundTerm) {
		this.pundTerm = pundTerm;
	}
	
    /**
     * @return PundTerm
     */	
	public Integer getPundTerm() {
		return this.pundTerm;
	}
	
	/**
	 * @param pundRate
	 */
	public void setPundRate(java.math.BigDecimal pundRate) {
		this.pundRate = pundRate;
	}
	
    /**
     * @return PundRate
     */	
	public java.math.BigDecimal getPundRate() {
		return this.pundRate;
	}
	
	/**
	 * @param pundRepayMode
	 */
	public void setPundRepayMode(String pundRepayMode) {
		this.pundRepayMode = pundRepayMode == null ? null : pundRepayMode.trim();
	}
	
    /**
     * @return PundRepayMode
     */	
	public String getPundRepayMode() {
		return this.pundRepayMode;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param presellLicNo
	 */
	public void setPresellLicNo(String presellLicNo) {
		this.presellLicNo = presellLicNo == null ? null : presellLicNo.trim();
	}
	
    /**
     * @return PresellLicNo
     */	
	public String getPresellLicNo() {
		return this.presellLicNo;
	}
	
	/**
	 * @param propertyExpense
	 */
	public void setPropertyExpense(java.math.BigDecimal propertyExpense) {
		this.propertyExpense = propertyExpense;
	}
	
    /**
     * @return PropertyExpense
     */	
	public java.math.BigDecimal getPropertyExpense() {
		return this.propertyExpense;
	}
	
	/**
	 * @param purHouseNum
	 */
	public void setPurHouseNum(String purHouseNum) {
		this.purHouseNum = purHouseNum == null ? null : purHouseNum.trim();
	}
	
    /**
     * @return PurHouseNum
     */	
	public String getPurHouseNum() {
		return this.purHouseNum;
	}
	
	/**
	 * @param purHouseTimes
	 */
	public void setPurHouseTimes(String purHouseTimes) {
		this.purHouseTimes = purHouseTimes == null ? null : purHouseTimes.trim();
	}
	
    /**
     * @return PurHouseTimes
     */	
	public String getPurHouseTimes() {
		return this.purHouseTimes;
	}
	
	/**
	 * @param houseStr
	 */
	public void setHouseStr(String houseStr) {
		this.houseStr = houseStr == null ? null : houseStr.trim();
	}
	
    /**
     * @return HouseStr
     */	
	public String getHouseStr() {
		return this.houseStr;
	}
	
	/**
	 * @param landUtilMode
	 */
	public void setLandUtilMode(String landUtilMode) {
		this.landUtilMode = landUtilMode == null ? null : landUtilMode.trim();
	}
	
    /**
     * @return LandUtilMode
     */	
	public String getLandUtilMode() {
		return this.landUtilMode;
	}
	
	/**
	 * @param houseBuildTime
	 */
	public void setHouseBuildTime(String houseBuildTime) {
		this.houseBuildTime = houseBuildTime == null ? null : houseBuildTime.trim();
	}
	
    /**
     * @return HouseBuildTime
     */	
	public String getHouseBuildTime() {
		return this.houseBuildTime;
	}
	
	/**
	 * @param landSurplusYears
	 */
	public void setLandSurplusYears(String landSurplusYears) {
		this.landSurplusYears = landSurplusYears == null ? null : landSurplusYears.trim();
	}
	
    /**
     * @return LandSurplusYears
     */	
	public String getLandSurplusYears() {
		return this.landSurplusYears;
	}
	
	/**
	 * @param houseYears
	 */
	public void setHouseYears(String houseYears) {
		this.houseYears = houseYears == null ? null : houseYears.trim();
	}
	
    /**
     * @return HouseYears
     */	
	public String getHouseYears() {
		return this.houseYears;
	}
	
	/**
	 * @param purResFlag
	 */
	public void setPurResFlag(String purResFlag) {
		this.purResFlag = purResFlag == null ? null : purResFlag.trim();
	}
	
    /**
     * @return PurResFlag
     */	
	public String getPurResFlag() {
		return this.purResFlag;
	}
	
	/**
	 * @param houseCountry
	 */
	public void setHouseCountry(String houseCountry) {
		this.houseCountry = houseCountry == null ? null : houseCountry.trim();
	}
	
    /**
     * @return HouseCountry
     */	
	public String getHouseCountry() {
		return this.houseCountry;
	}
	
	/**
	 * @param houseCondition
	 */
	public void setHouseCondition(String houseCondition) {
		this.houseCondition = houseCondition == null ? null : houseCondition.trim();
	}
	
    /**
     * @return HouseCondition
     */	
	public String getHouseCondition() {
		return this.houseCondition;
	}
	
	/**
	 * @param houseUse
	 */
	public void setHouseUse(String houseUse) {
		this.houseUse = houseUse == null ? null : houseUse.trim();
	}
	
    /**
     * @return HouseUse
     */	
	public String getHouseUse() {
		return this.houseUse;
	}
	
	/**
	 * @param houseType
	 */
	public void setHouseType(String houseType) {
		this.houseType = houseType == null ? null : houseType.trim();
	}
	
    /**
     * @return HouseType
     */	
	public String getHouseType() {
		return this.houseType;
	}
	
	/**
	 * @param sizePrice
	 */
	public void setSizePrice(java.math.BigDecimal sizePrice) {
		this.sizePrice = sizePrice;
	}
	
    /**
     * @return SizePrice
     */	
	public java.math.BigDecimal getSizePrice() {
		return this.sizePrice;
	}
	
	/**
	 * @param speCaseDesc
	 */
	public void setSpeCaseDesc(String speCaseDesc) {
		this.speCaseDesc = speCaseDesc == null ? null : speCaseDesc.trim();
	}
	
    /**
     * @return SpeCaseDesc
     */	
	public String getSpeCaseDesc() {
		return this.speCaseDesc;
	}
	
	/**
	 * @param otherDesc
	 */
	public void setOtherDesc(String otherDesc) {
		this.otherDesc = otherDesc == null ? null : otherDesc.trim();
	}
	
    /**
     * @return OtherDesc
     */	
	public String getOtherDesc() {
		return this.otherDesc;
	}


}