package cn.com.yusys.yusp.service.server.xdtz0039;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0006.resp.CusBaseDto;
import cn.com.yusys.yusp.dto.server.xdtz0039.req.Xdtz0039DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0039.resp.Xdtz0039DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import cn.com.yusys.yusp.service.server.xdsx0014.Xdsx0014Service;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import java.util.*;

/**
 * 接口处理类:根据企业名称查询申请企业在本行是否存在当前逾期贷款
 *
 * @author lihh
 * @version 1.0
 */
@Service
public class Xdtz0039Service {

    private static final Logger logger = LoggerFactory.getLogger(Xdsx0014Service.class);

    @Autowired
    private AccLoanMapper accLoanMapper;

    @Autowired
    private CommonService commonService;

    /**
     * 交易码：xdtz0039
     * 交易描述：根据企业名称查询申请企业在本行是否存在当前逾期贷款
     *
     * @param xdtz0039DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0039DataRespDto getHasOverByCusName(Xdtz0039DataReqDto xdtz0039DataReqDto) {
        logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, JSON.toJSONString(xdtz0039DataReqDto));
        Xdtz0039DataRespDto xdtz0039DataRespDto = new Xdtz0039DataRespDto();
        String cusName = xdtz0039DataReqDto.getCusName();
        String certCodes = xdtz0039DataReqDto.getCertCodes();
        String queryCusIds = "";
        try {
            List<String> cusIds = new ArrayList<String>();
            if (StringUtil.isNotEmpty(certCodes) && certCodes.contains(",")) {
                List<String> list = Arrays.asList(certCodes.split(","));
                cusIds = commonService.getCusIdsByCertNos(list);
                for (int i = 0; i < cusIds.size(); i++) {
                    queryCusIds += cusIds.get(i);
                }
            } else if(StringUtil.isNotEmpty(certCodes)){
                List<CusBaseDto> cusBaseDtoList = commonService.getCusBaseListByCertCode(certCodes);
                for (int i = 0; i < cusBaseDtoList.size(); i++) {
                    queryCusIds += cusBaseDtoList.get(i).getCusId();
                }
            }
            xdtz0039DataRespDto = accLoanMapper.getHasOverByCusName(cusName, queryCusIds);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, e.getMessage());
        }
        logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, JSON.toJSONString(xdtz0039DataRespDto));
        return xdtz0039DataRespDto;
    }
}
