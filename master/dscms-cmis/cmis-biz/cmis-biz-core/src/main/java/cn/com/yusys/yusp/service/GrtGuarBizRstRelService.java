/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateFormatEnum;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.constants.CmisBizConstants;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.CmisFlowConstants;
import cn.com.yusys.yusp.constants.CmisRiskConstants;
import cn.com.yusys.yusp.domain.*;
import cn.com.yusys.yusp.dto.CtrLoanContDto;
import cn.com.yusys.yusp.dto.GrtGuarBizRstRelDto;
import cn.com.yusys.yusp.dto.GuarBizRelGuaranteeDto;
import cn.com.yusys.yusp.dto.IqpLoanAppDto;
import cn.com.yusys.yusp.dto.risk.RiskResultDto;
import cn.com.yusys.yusp.dto.server.cmislmt0060.req.CmisLmt0060ReqDto;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.repository.mapper.GrtGuarBizRstRelMapper;
import com.github.pagehelper.PageHelper;
import org.apache.ibatis.annotations.Param;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: GuarContLoanContRelService
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 刘权
 * @创建时间: 2021-04-08 16:50:30
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
public class GrtGuarBizRstRelService {

    private static final Logger log = LoggerFactory.getLogger(GrtGuarBizRstRelService.class);

    @Resource
    private GrtGuarBizRstRelMapper grtGuarBizRstRelMapper;
    @Autowired
    private CtrLoanContService ctrLoanContService; //合同主表
    @Autowired
    private GrtGuarContService grtGuarContService; // 担保合同
    @Autowired
    private GrtGuarContRelService grtGuarContRelService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;
    @Autowired
    private GuarGuaranteeService guarGuaranteeService;
    @Autowired
    private AccLoanService accLoanService;
    @Autowired
    private IqpLoanAppService iqpLoanAppService;
    @Autowired
    private IqpAccpAppService iqpAccpAppService;
    @Autowired
    private IqpTfLocAppService iqpTfLocAppService;//开证合同申请详情
    @Autowired
    private IqpCvrgAppService iqpCvrgAppService;//保函合同申请
    @Autowired
    private IqpEntrustLoanAppService iqpEntrustLoanAppService;//委托贷款
    @Autowired
    private CtrAccpContService ctrAccpContService;  // 银承合同详情
    @Autowired
    private AccAccpService accAccpService;
    @Autowired
    private CtrTfLocContService ctrTfLocContService;
    @Autowired
    private AccTfLocService accTfLocService;
    @Autowired
    private CtrCvrgContService ctrCvrgContService;//贴现合同主表
    @Autowired
    private AccCvrsService accCvrsService;

    @Autowired
    private IqpHighAmtAgrAppService iqpHighAmtAgrAppService;

    @Autowired
    private LmtReplyAccSubPrdService lmtReplyAccSubPrdService;

    @Autowired
    private AccAccpDrftSubService accAccpDrftSubService;

    @Autowired
    private AccEntrustLoanService accEntrustLoanService;

    @Autowired
    private CtrHighAmtAgrContService ctrHighAmtAgrContService;

    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */

    public GrtGuarBizRstRel selectByPrimaryKey(String pkId) {
        return grtGuarBizRstRelMapper.selectByPrimaryKey(pkId);
    }
    /**
     * @方法名称: queryContNoKey
     * @方法描述: 根据合同编号查询
     * @参数与返回说明:
     * @算法描述: 无
     * @return
     */
    public String queryContNoKey(String contNo,String oprType) {
        String rtnData = "";
        try {
            List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelMapper.queryContNoKey(contNo ,oprType);
            if (CollectionUtils.isEmpty(grtGuarBizRstRelList) || grtGuarBizRstRelList.size() == 0 ){
                return rtnData;
            }
            for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
                rtnData = rtnData + grtGuarBizRstRel.getPkId() + ",";
            }
            if (StringUtils.nonBlank(rtnData)) {
                rtnData = rtnData.substring(0, rtnData.lastIndexOf(","));
            }

        } catch (Exception e) {
            log.error("通过担保合同编号查询异常", e);
        }
        return rtnData;
    }


    /**
     * @方法名称: selectByGuarContNoKeys
     * @方法描述: 根据主键查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int selectByGuarContNoKeys(Map grtGuarContDto){
        int result = 0 ;
        List<GrtGuarBizRstRel> GrtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByGuarContNoKeys(grtGuarContDto);
        if(GrtGuarBizRstRelList !=null){
            result =GrtGuarBizRstRelList.size();
        }
        if(result >0){
            throw new YuspException(EcbEnum.GRT_GUAR_CONT_NO_DELETE.key, EcbEnum.GRT_GUAR_CONT_NO_DELETE.value);
        }
        return result;
    }
    /**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明:
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<GrtGuarBizRstRel> selectAll(QueryModel model) {
        List<GrtGuarBizRstRel> records = (List<GrtGuarBizRstRel>) grtGuarBizRstRelMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GrtGuarBizRstRel> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtGuarBizRstRel> list = grtGuarBizRstRelMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * 通过入参信息查询合同关系数据
     *
     * @param params
     * @return
     */
    public List<GrtGuarBizRstRel> selectByParams(Map params) {
        return grtGuarBizRstRelMapper.selectByParams(params);
    }
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insert(GrtGuarBizRstRel record) {
        return grtGuarBizRstRelMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int insertSelective(GrtGuarBizRstRel record) {
        return grtGuarBizRstRelMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int update(GrtGuarBizRstRel record) {
        return grtGuarBizRstRelMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int updateSelective(GrtGuarBizRstRel record) {
        return grtGuarBizRstRelMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return grtGuarBizRstRelMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明:
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return grtGuarBizRstRelMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: insertBizResRel
     * @方法描述: 根据入参插入
     * @参数与返回说明:
     * @算法描述: 无
     */
    public int insertBizResRel(GrtGuarBizRstRel grtGuarBizRstRel) {
        return grtGuarBizRstRelMapper.insertBizResRel(grtGuarBizRstRel);
    }
    /**
     * @方法名称: selectBySernoKeys
     * @方法描述: 根据入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GrtGuarBizRstRel selectBySernoKeys(GrtGuarBizRstRel grtGuarBizRstRel){
        return grtGuarBizRstRelMapper.selectBySernoKeys(grtGuarBizRstRel);
    }

    /**
     * 批量添加数据
     * @param grtGuarMapList
     * @return
     */
    public int insertForeach(List<Map> grtGuarMapList) {
        return grtGuarBizRstRelMapper.insertForeach(grtGuarMapList);
    }

    /**
     * @方法名称：selectByLmtAccNo
     * @方法描述：根据授信台账流水查询
     * @创建人：zhangming12
     * @创建时间：2021/5/18 10:31
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<GrtGuarBizRstRel> selectByLmtSerno(String serno){
        return grtGuarBizRstRelMapper.selectByLmtSerno(serno);
    }


    /**
     * @方法名称：getByContNo
     * @方法描述：根据合同号查询
     * @创建人：zhangming12
     * @创建时间：2021/5/22 10:04
     * @修改记录：修改时间 修改人员 修改时间
     */
    public List<GrtGuarBizRstRel> getByContNo(String contNo){
        return grtGuarBizRstRelMapper.selectByContNo(contNo);
    }

    /**
     * @param serno
     * @return int
     * @author hubp
     * @date 2021/5/27 15:19
     * @version 1.0.0
     * @desc    删除合同关联信息
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public int deleteBySerno(String serno){
        return grtGuarBizRstRelMapper.deleteBySerno(serno);
    }

    /**
     * @param contNo
     * @return String
     * @author zoubiao
     * @date 2021/06/10 20:11
     * @version 1.0.0
     * @desc    根据借款合同号查询项下的押品编号
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public String selectGuarantyIdByContNo(@Param("contNo") String contNo){
        return grtGuarBizRstRelMapper.selectGuarantyIdByContNo(contNo);
    }

    /**
     * @方法名称: selectGrtGuarBizRstRelDataBySerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public GrtGuarBizRstRel selectGrtGuarBizRstRelDataBySerno(String serno){
        return grtGuarBizRstRelMapper.selectGrtGuarBizRstRelDataBySerno(serno);
    }

    /**
     * @方法名称: selectDetailBySerno
     * @方法描述: 根据流水号查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GrtGuarBizRstRel> selectDetailBySerno(String serno){
        return grtGuarBizRstRelMapper.selectDetailBySerno(serno);
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/6/24 10:16
     * @version 1.0.0
     * @desc 担保合同与借款合同一致性校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0021(QueryModel queryModel) {
        /* 合同的担保方式=抵押/质押/保证/低风险质押时，执行以下校验：
        （1）担保合同列表必须存在“是否追加担保=否”的担保合同
        （2）最高额借款合同：主担保（非追加）合同金额之和必须大于等于借款合同金额
        （3）一般借款合同：主担保（非追加）合同金额必须等于借款合同金额
         */
        RiskResultDto riskResultDto = new RiskResultDto();
        String contNo = queryModel.getCondition().get("bizId").toString(); // 获取合同编号
        String bizType =  queryModel.getCondition().get("bizType").toString(); // 获取流程编号
        log.info("担保合同与借款合同一致性校验开始*******************业务流水号：【{}】",contNo);
        String guarWay = StringUtils.EMPTY;
        String contType = StringUtils.EMPTY;
        BigDecimal contAmt = BigDecimal.ZERO;
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = null;
        String startDate = null;
        String endDate = null;
        if (StringUtils.isBlank(contNo)){
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        /** 小微合同、普通贷款合同、贸易融资、申请是在申请表中 */
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_XWHT001,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX002,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX003,bizType)){
            // 查申请表
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(contNo);
            if(Objects.isNull(iqpLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpLoanApp.getStartDate();
            endDate  = iqpLoanApp.getEndDate();
            contType = iqpLoanApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpLoanApp.getContAmt()) ? iqpLoanApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpLoanApp.getGuarWay())) {
                guarWay = iqpLoanApp.getGuarWay();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02101);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);

        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX006,bizType)){ // 银承贷款合同申请流程
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(contNo);
            if(Objects.isNull(iqpAccpApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpAccpApp.getStartDate();
            endDate  = iqpAccpApp.getEndDate();
            contType = iqpAccpApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpAccpApp.getContAmt()) ? iqpAccpApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpAccpApp.getGuarMode())) {
                guarWay = iqpAccpApp.getGuarMode();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02101);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX005,bizType)){ // 开证贷款合同申请流程
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(contNo);
            if(Objects.isNull(iqpTfLocApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpTfLocApp.getStartDate();
            endDate  = iqpTfLocApp.getEndDate();
            contType = iqpTfLocApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpTfLocApp.getContAmt()) ? iqpTfLocApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpTfLocApp.getGuarMode())) {
                guarWay = iqpTfLocApp.getGuarMode();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02101);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX007,bizType)){ // 保函贷款合同申请流程
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(contNo);
            if(Objects.isNull(iqpCvrgApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpCvrgApp.getStartDate();
            endDate  = iqpCvrgApp.getEndDate();
            contType = iqpCvrgApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpCvrgApp.getContAmt()) ? iqpCvrgApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpCvrgApp.getGuarMode())) {
                guarWay = iqpCvrgApp.getGuarMode();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02101);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX008,bizType)){ // 委托贷款合同申请流程
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectBySerno(contNo);
            if(Objects.isNull(iqpEntrustLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            startDate = iqpEntrustLoanApp.getStartDate();
            endDate  = iqpEntrustLoanApp.getEndDate();
            contType = iqpEntrustLoanApp.getContType();
            contAmt = contAmt.add(Objects.nonNull(iqpEntrustLoanApp.getContAmt()) ? iqpEntrustLoanApp.getContAmt() : BigDecimal.ZERO);
            if (!StringUtils.isEmpty(iqpEntrustLoanApp.getGuarMode())) {
                guarWay = iqpEntrustLoanApp.getGuarMode();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02101);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(contNo);
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_LS004.equals(bizType)){ // 零售合同申请
            // 通过合同编号去查询合同信息，获取担保方式
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(contNo);
            if(ctrLoanCont == null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0104);
                return riskResultDto;
            }
            startDate = ctrLoanCont.getContStartDate();
            endDate  = ctrLoanCont.getContEndDate();
            contType = ctrLoanCont.getContType();
            contAmt = contAmt.add(ctrLoanCont.getContAmt());
            if (!StringUtils.isBlank(ctrLoanCont.getGuarWay())) {
                guarWay = ctrLoanCont.getGuarWay();
            } else {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02101);
                return riskResultDto;
            }
            // 通过申请流水号查询担保合同列表
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(ctrLoanCont.getIqpSerno());
        } else {
            log.info("担保合同与借款合同一致性校验结束*******************业务流水号：【{}】",contNo);
            // 通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }
        //合同的担保方式=抵押/质押/保证/低风险质押时，执行以下校验
        if (CmisCommonConstants.GUAR_MODE_10.equals(guarWay) || CmisCommonConstants.GUAR_MODE_20.equals(guarWay) || CmisCommonConstants.GUAR_MODE_21.equals(guarWay) || CmisCommonConstants.GUAR_MODE_30.equals(guarWay)) {
            // 用来存放非追加的担保合同
            List<GrtGuarCont> grtGuarContList = new ArrayList<>();
            // 担保合同列表必须存在“是否追加担保=否”的担保合同
            if (grtGuarBizRstRelList.size() == 0) {
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02102);
                return riskResultDto;
            } else {
                // 担保合同列表必须存在“是否追加担保=否”的担保合同
                // 先设定一个标志
                boolean flg = false;
                for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
                    GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                    if (CmisCommonConstants.STD_ZB_YES_NO_0.equals(grtGuarCont.getIsSuperaddGuar())) {
                        // 如果存在是否追加担保为否的合同，flg变为true;
                        flg = true;
                        //将非追加的担保合同放到集合中----给拦截2,3使用
                        grtGuarContList.add(grtGuarCont);
                    }
                }
                if (!flg) {
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02103);
                    return riskResultDto;
                }
                if (CmisCommonConstants.STD_CONT_TYPE_2.equals(contType)) {
                    // （2）最高额借款合同：主担保（非追加）合同金额之和必须大于等于借款合同金额
                    BigDecimal totalAmt = new BigDecimal("0.0");
                    for (GrtGuarCont grtGuarCont : grtGuarContList) {
                        totalAmt = totalAmt.add(Objects.nonNull(grtGuarCont.getGuarAmt()) ? grtGuarCont.getGuarAmt() : BigDecimal.ZERO);
                    }
                    if (totalAmt.compareTo(contAmt) < 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02104);
                        return riskResultDto;
                    }

                    // 最高额担保合同时，担保合同起始日应小于等于主合同起始日
                    for (GrtGuarCont grtGuarCont : grtGuarContList) {
                        // 起始日判断
                        if (LocalDate.parse(grtGuarCont.getGuarStartDate()).compareTo(LocalDate.parse(startDate)) > 0) {
                            log.info("担保合同起始日【{}】大于主合同起始日【{}】", grtGuarCont.getGuarStartDate(), startDate);
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02106);
                            return riskResultDto;
                        }
                        // 到期日判断
                        if (LocalDate.parse(grtGuarCont.getGuarEndDate()).compareTo(LocalDate.parse(endDate)) < 0) {
                            log.info("担保合同到期日【{}】小于主合同到期日【{}】", grtGuarCont.getGuarEndDate(), endDate);
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02107);
                            return riskResultDto;
                        }
                    }
                } else if (CmisCommonConstants.STD_CONT_TYPE_1.equals(contType)) {
                    // 一般借款合同：主担保（非追加）合同金额必须等于借款合同金额
                    BigDecimal totalAmt = new BigDecimal("0.0");
                    for (GrtGuarCont grtGuarCont : grtGuarContList) {
                        totalAmt = totalAmt.add(Objects.nonNull(grtGuarCont.getGuarAmt()) ? grtGuarCont.getGuarAmt() : BigDecimal.ZERO);
                    }
                    if (totalAmt.compareTo(contAmt) != 0) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02105);
                        return riskResultDto;
                    }
                    // 最高额担保合同时，担保合同起始日应小于等于主合同起始日
                    for (GrtGuarCont grtGuarCont : grtGuarContList) {
                        // 起始日判断
                        if (LocalDate.parse(grtGuarCont.getGuarStartDate()).compareTo(LocalDate.parse(startDate)) != 0) {
                            log.info("担保合同起始日【{}】不等于主合同起始日【{}】", grtGuarCont.getGuarStartDate(), startDate);
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02108);
                            return riskResultDto;
                        }
                        // 到期日判断
                        if (LocalDate.parse(grtGuarCont.getGuarEndDate()).compareTo(LocalDate.parse(endDate)) != 0) {
                            log.info("担保合同到期日【{}】不等于主合同到期日【{}】", grtGuarCont.getGuarEndDate(), endDate);
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02109);
                            return riskResultDto;
                        }
                    }
                }
            }
        }
        log.info("担保合同与借款合同一致性校验结束*******************业务流水号：【{}】",contNo);
        // 通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/6/24 14:27
     * @version 1.0.0
     * @desc 担保合同与担保品一致性校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0022(QueryModel queryModel) {
        /*  1、合同的担保方式=抵押/质押/保证/低风险质押时；主合同的担保方式=信用且担保合同列表必须存在“是否追加担保=是”的担保合同，担保合同必须存在担保品
            2、抵押/质押合同：担保品金额之和必须等于对应的担保合同金额。
            3、保证合同：
            （1）单人保证：担保品有且只能有一个，且担保金额必须等于对应的担保合同金额。
            （2）多人分保：担保品必须大于1且担保金额之和必须等于担保合同金额。
            （3）多人担保：担保品必须大于1且每个担保金额必须等于担保合同金额。
         */
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString(); // 获取合同编号
        String bizType =  queryModel.getCondition().get("bizType").toString(); // 获取流程编号
        log.info("担保合同与担保品一致性校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        String contGuarWay = StringUtils.EMPTY;
        String prdTypeProp = StringUtils.EMPTY;
        String guarContGuarWay = StringUtils.EMPTY;
        String contType = StringUtils.EMPTY;
        BigDecimal contAmt = BigDecimal.ZERO;
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = null;
        if (StringUtils.isEmpty(serno)) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        /** 小微合同、普通贷款合同、贸易融资申请是在申请表中 */
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_XWHT001,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX002,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX003,bizType)){
            // 查申请表
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(serno);
            if(Objects.isNull(iqpLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(serno);
            contGuarWay = iqpLoanApp.getGuarWay();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
            !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
                    // 通过
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02210);
                    return riskResultDto;
                }
            }
            // 获取产品类型属性
            prdTypeProp = iqpLoanApp.getPrdTypeProp();
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX006,bizType)){ // 银承贷款合同申请流程
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(serno);
            if(Objects.isNull(iqpAccpApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(serno);
            contGuarWay = iqpAccpApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
                    !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
                    // 通过
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02210);
                    return riskResultDto;
                }
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX005,bizType)){ // 开证贷款合同申请流程
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(serno);
            if(Objects.isNull(iqpTfLocApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(serno);
            contGuarWay = iqpTfLocApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
                    !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
                    // 通过
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02210);
                    return riskResultDto;
                }
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX007,bizType)){ // 保函贷款合同申请流程
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(serno);
            if(Objects.isNull(iqpCvrgApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(serno);
            contGuarWay = iqpCvrgApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
                    !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
                    // 通过
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02210);
                    return riskResultDto;
                }
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX008,bizType)){ // 委托贷款合同申请流程
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectBySerno(serno);
            if(Objects.isNull(iqpEntrustLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(serno);
            contGuarWay = iqpEntrustLoanApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
                    !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
                    // 通过
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02210);
                    return riskResultDto;
                }
            }
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_LS004.equals(bizType) || CmisFlowConstants.FLOW_TYPE_TYPE_LS008.equals(bizType)){ // 零售合同申请
            // 通过合同编号去查询合同信息，获取担保方式
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
            if(ctrLoanCont == null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0104);
                return riskResultDto;
            }
            // 通过申请流水号查询担保合同列表
            grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(ctrLoanCont.getIqpSerno());
            contGuarWay = ctrLoanCont.getGuarWay();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
                    !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
                    // 通过
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02210);
                    return riskResultDto;
                }
            }
        } else {
            // 通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }
        // 如果以上情况都满足，且担保合同还是空，则正常结束
        if (CollectionUtils.isEmpty(grtGuarBizRstRelList)) {
            // 通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }
        try{
            for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
                // 获取担保合同信息
                GrtGuarCont grtGuarCont = grtGuarContService.selectByGuarContNo(grtGuarBizRstRel.getGuarContNo());
                if(Objects.isNull(grtGuarCont)){
                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0014);
                    return riskResultDto;
                }
                guarContGuarWay = grtGuarCont.getGuarWay();
                // 担保合同金额
                BigDecimal guarAmt = Objects.nonNull(grtGuarCont.getGuarAmt()) ? grtGuarCont.getGuarAmt() : BigDecimal.ZERO; // 担保合同金额
                //合同的担保方式=抵押/质押/保证/低风险质押时，执行以下校验
                if (CmisCommonConstants.GUAR_MODE_10.equals(guarContGuarWay) || CmisCommonConstants.GUAR_MODE_20.equals(guarContGuarWay) || CmisCommonConstants.GUAR_MODE_21.equals(guarContGuarWay)) {
                    // 1、担保合同必须存在担保品
                    QueryModel model = new QueryModel();
                    model.getCondition().put("guarContNo", grtGuarBizRstRel.getGuarContNo());
                    model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                    List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelService.selectAll(model);
                    if (CollectionUtils.isEmpty(grtGuarContRelList)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_019);
                        return riskResultDto;
                    }
                    // 抵押/质押合同：担保品金额之和必须等于对应的担保合同金额。
                    if(CmisCommonConstants.GUAR_MODE_10.equals(guarContGuarWay) || CmisCommonConstants.GUAR_MODE_20.equals(guarContGuarWay)) {
                        BigDecimal totalAmt = BigDecimal.ZERO; // 担保品金额之和

                        for (GrtGuarContRel grtGuarContRel : grtGuarContRelList) {
                            totalAmt = totalAmt.add(Objects.nonNull(grtGuarContRel.getCertiAmt()) ? grtGuarContRel.getCertiAmt() : BigDecimal.ZERO);
                        }
                        // 2021年10月20日22:24:09 hubp BUG15172
                        if (totalAmt.compareTo(BigDecimal.ZERO) == 1 && totalAmt.compareTo(guarAmt) < 0) {//存量数据的权利价值为空,如果担保品金额之和为0则不拦
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02202);
                            return riskResultDto;
                        }
                    }

                    // 仅房抵e点贷产品校验：房抵e点贷押品信息是否完善校验
                    if (CmisCommonConstants.GUAR_MODE_10.equals(guarContGuarWay) && StringUtils.nonBlank(prdTypeProp)
                            && CmisBizConstants.GE_REN_JING_YING_XING_DAI_KUAN_PRD_022011_P034.equals(prdTypeProp)) {
                        for (GrtGuarContRel grtGuarContRel : grtGuarContRelList) {
                            String guarNo = grtGuarContRel.getGuarNo();
                            if(Objects.isNull(guarNo)){
                                log.info("担保合同与担保品一致性校验:担保合同和担保人(物)的关系表未获取押品编号*******************押品编号：【{}】，流程类型：【{}】",guarNo,bizType);
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_019);
                                return riskResultDto;
                            }
                            //根据押品编号，查询权证入库时的出入库流水号
                            queryModel.addCondition("guarNo",guarNo);//押品编号
                            List<GuarBaseInfo> guarBaseInfoList = guarBaseInfoService.selectAll(queryModel);
                            for (GuarBaseInfo guarBaseInfo : guarBaseInfoList){
                                // PldimnMemo:抵质押物名称 Newlabel:新押品编码名称 EvalAmt:押品评估价值
                                if (Objects.isNull(guarBaseInfo.getPldimnMemo()) || Objects.isNull(guarBaseInfo.getNewlabel()) || Objects.isNull(guarBaseInfo.getEvalAmt()) || !Objects.equals(CmisCommonConstants.WF_STATUS_997, guarBaseInfo.getApproveStatus())){
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_002201);
                                    return riskResultDto;
                                }
                            }
                        }
                    }
                } else if (CmisCommonConstants.GUAR_MODE_30.equals(guarContGuarWay)) {
                    // 初始化担保人集合
                    // 保证=单人担保
                    List<GuarGuarantee> assureMode0401List = new ArrayList<>();
                    // 保证=多人分保
                    List<GuarGuarantee> assureMode0402List = new ArrayList<>();
                    // 保证方式=多人联保
                    List<GuarGuarantee> assureMode0403List = new ArrayList<>();
                    BigDecimal totalAmt = new BigDecimal("0.0"); // 担保品金额之和
                    guarAmt = Objects.isNull(grtGuarCont.getGuarAmt()) ? BigDecimal.ZERO : grtGuarCont.getGuarAmt();
                    QueryModel model1 = new QueryModel();
                    model1.addCondition("guarContNo",grtGuarCont.getGuarContNo());
                    List<GuarGuarantee> guarGuaranteeList = guarGuaranteeService.selectGuarGuaranteeByGuarContNo(model1);
                    // 判定保证担保人信息是否为空
                    if (CollectionUtils.isEmpty(guarGuaranteeList)) {
                        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_019);
                        return riskResultDto;
                    }
                    for(GuarGuarantee guarGuarantee : guarGuaranteeList) {
                        if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0401,guarGuarantee.getGuarantyType())) {
                            assureMode0401List.add(guarGuarantee);
                        }else if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0402,guarGuarantee.getGuarantyType())) {
                            assureMode0402List.add(guarGuarantee);
                        }else if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0403,guarGuarantee.getGuarantyType())) {
                            assureMode0403List.add(guarGuarantee);
                        }else if(Objects.equals(CmisCommonConstants.STD_GUARANTY_TYPE_0404,guarGuarantee.getGuarantyType())) {
                            assureMode0403List.add(guarGuarantee);
                        }else{
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00112);
                            return riskResultDto;
                        }
                    }
                    // 保证=单人担保 担保合同金额需小于等于保证金额
                    if(CollectionUtils.nonEmpty(assureMode0401List)) {
                        log.info("担保合同与担保品一致性校验执行中*******************业务流水号：【{}】，保证=单人担保",serno);
                        if(assureMode0401List.size() > 1) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00107);
                            return riskResultDto;
                        }else {
                            if(guarAmt.compareTo(Objects.nonNull(assureMode0401List.get(0).getGuarAmt()) ? assureMode0401List.get(0).getGuarAmt() :
                                    new BigDecimal("0")) > 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02212);
                                return riskResultDto;
                            }
                        }
                    }
                    // 保证=多人分保
                    if(CollectionUtils.nonEmpty(assureMode0402List)) {
                        log.info("担保合同与担保品一致性校验执行中*******************业务流水号：【{}】，保证=多人分保",serno);
                        if(assureMode0402List.size() <= 1) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00108);
                            return riskResultDto;
                        }else {
                            for(GuarGuarantee guarBiz0402 : assureMode0402List) {
                                totalAmt = totalAmt.add(Objects.nonNull(guarBiz0402.getGuarAmt()) ? guarBiz0402.getGuarAmt() :
                                        new BigDecimal("0"));
                            }
                            if(guarAmt.compareTo(totalAmt) != 0) {
                                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02213);
                                return riskResultDto;
                            }
                        }
                    }
                    // 保证方式=多人联保
                    if(CollectionUtils.nonEmpty(assureMode0403List)) {
                        log.info("担保合同与担保品一致性校验执行中*******************业务流水号：【{}】，保证方式=多人联保",serno);
                        if(assureMode0403List.size() <= 1) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00110);
                            return riskResultDto;
                        }else {
                            for(GuarGuarantee guarBiz0403 : assureMode0403List) {
                                if(guarAmt.compareTo(Objects.nonNull(guarBiz0403.getGuarAmt()) ? guarBiz0403.getGuarAmt() :
                                        new BigDecimal("0")) > 0) {
                                    riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                                    riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02214);
                                    return riskResultDto;
                                }
                            }
                        }
                    }
                }
                if(Objects.equals("1",grtGuarBizRstRel.getIsAddGuar())) {
                    if(CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay)) {
                        // 1、担保合同必须存在担保品
                        QueryModel model = new QueryModel();
                        model.getCondition().put("guarContNo", grtGuarBizRstRel.getGuarContNo());
                        model.getCondition().put("oprType", CmisCommonConstants.OPR_TYPE_ADD);
                        List<GrtGuarContRel> grtGuarContRelList = grtGuarContRelService.selectAll(model);
                        if (CollectionUtils.isEmpty(grtGuarContRelList)) {
                            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_00113);
                            return riskResultDto;
                        }
                    }
                }
            }
        } catch (Exception e){
            log.error("担保合同与担保品一致性校验执行异常",e);
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_016);
            return riskResultDto;
        }

        // 通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @param queryModel
     * @return cn.com.yusys.yusp.dto.risk.RiskResultDto
     * @author hubp
     * @date 2021/6/24 14:27
     * @version 1.0.0
     * @desc 最高额担保合同可用金额校验
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public RiskResultDto riskItem0023(QueryModel queryModel) {
        /*
         *  最高额担保合同金额必须大于等于最高额担保合同项下（在途借款合同+生效借款合同金额之和-非循环借款合同项下已还款金额之和）
         */
        // 获取合同编号
        RiskResultDto riskResultDto = new RiskResultDto();
        String serno = queryModel.getCondition().get("bizId").toString(); // 获取合同编号
        String bizType =  queryModel.getCondition().get("bizType").toString(); // 获取流程编号
        String contGuarWay = StringUtils.EMPTY;
        log.info("最高额担保合同可用金额校验开始*******************业务流水号：【{}】，流程类型：【{}】",serno,bizType);
        if (StringUtils.isEmpty(serno)) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0001);
            return riskResultDto;
        }
        if (StringUtils.isEmpty(bizType)) {
            // 为空不通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0002);
            return riskResultDto;
        }
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = null;
        /** 小微合同、普通贷款合同、贸易融资申请是在申请表中 */
        if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_XWHT001,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX002,bizType)
                || Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX003,bizType)){
            // 查申请表
            IqpLoanApp iqpLoanApp = iqpLoanAppService.selectByIqpSerno(serno);
            if(Objects.isNull(iqpLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            contGuarWay = iqpLoanApp.getGuarWay();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay)
                    && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)
                    && !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay)) {
                return this.checkHighGuarContAmt(iqpLoanApp.getContNo(),iqpLoanApp.getContAmt());
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX001,bizType)){ // 最高额授信协议申请流程
            IqpHighAmtAgrApp iqpHighAmtAgrApp = iqpHighAmtAgrAppService.selectByHighAmtAgrSernoKey(serno);

            if(Objects.isNull(iqpHighAmtAgrApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            contGuarWay = iqpHighAmtAgrApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay)
                    && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)
                    && !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay)) {
                return this.checkHighGuarContAmt(iqpHighAmtAgrApp.getContNo(),iqpHighAmtAgrApp.getAgrAmt());
            }
        }else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX006,bizType)){ // 银承贷款合同申请流程
            IqpAccpApp iqpAccpApp = iqpAccpAppService.selectByAccpSernoKey(serno);
            if(Objects.isNull(iqpAccpApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            contGuarWay = iqpAccpApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay)
                    && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)
                    && !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay)) {
                return this.checkHighGuarContAmt(iqpAccpApp.getContNo(),iqpAccpApp.getContAmt());
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX005,bizType)){ // 开证贷款合同申请流程
            IqpTfLocApp iqpTfLocApp = iqpTfLocAppService.selectByIqpSerno(serno);
            if(Objects.isNull(iqpTfLocApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            contGuarWay = iqpTfLocApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
                    !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                return this.checkHighGuarContAmt(iqpTfLocApp.getContNo(),iqpTfLocApp.getContAmt());
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX007,bizType)){ // 保函贷款合同申请流程
            IqpCvrgApp iqpCvrgApp = iqpCvrgAppService.selectBySerno(serno);
            if(Objects.isNull(iqpCvrgApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            contGuarWay = iqpCvrgApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
                    !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                return this.checkHighGuarContAmt(iqpCvrgApp.getContNo(),iqpCvrgApp.getContAmt());
            }
        } else if(Objects.equals(CmisFlowConstants.FLOW_TYPE_TYPE_YX008,bizType)){ // 委托贷款合同申请流程
            IqpEntrustLoanApp iqpEntrustLoanApp = iqpEntrustLoanAppService.selectBySerno(serno);
            if(Objects.isNull(iqpEntrustLoanApp)){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_0007);
                return riskResultDto;
            }
            contGuarWay = iqpEntrustLoanApp.getGuarMode();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay) &&
                    !CmisCommonConstants.GUAR_MODE_60.equals(contGuarWay) && !CmisCommonConstants.GUAR_MODE_40.equals(contGuarWay)) {
                return this.checkHighGuarContAmt(iqpEntrustLoanApp.getContNo(),iqpEntrustLoanApp.getContAmt());
            }
        }else if(CmisFlowConstants.FLOW_TYPE_TYPE_LS004.equals(bizType)){ // 零售合同申请
            // 通过合同编号去查询合同信息，获取担保方式
            CtrLoanCont ctrLoanCont = ctrLoanContService.selectByPrimaryKey(serno);
            if(ctrLoanCont == null){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0104);
                return riskResultDto;
            }
            contGuarWay = ctrLoanCont.getGuarWay();
            if (StringUtils.nonBlank(contGuarWay) && !CmisCommonConstants.GUAR_MODE_00.equals(contGuarWay)) {
                return this.checkHighGuarContAmt(ctrLoanCont.getContNo(),ctrLoanCont.getContAmt());
            }
        } else {
            // 通过
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
            return riskResultDto;
        }
        log.info("最高额担保合同可用金额校验结束*******************业务流水号：【{}】",serno);
        // 通过
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /**
     * @方法名称: queryGrtGuarContByParams
     * @方法描述: 根据担保合同编号获取担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<GrtGuarCont> queryGrtGuarContByParams(String contNo) {
        List<GrtGuarCont> grtGuarContList = new ArrayList();
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = this.getByContNo(contNo);
        if(CollectionUtils.isEmpty(grtGuarBizRstRelList)){
            return grtGuarContList;
        };
        // guarContNo集合
        List<String> guarContNos= grtGuarBizRstRelList.parallelStream().map(e->{
            String guarContNo = e.getGuarContNo();
            return guarContNo;
        }).collect(Collectors.toList());
        grtGuarContList = grtGuarContService.selectByGuarContNos(guarContNos);
        return grtGuarContList;
    }

    /**
     * @方法名称: queryGrtGuarContByContNo
     * @方法描述: 根据合同号获取担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<GrtGuarCont> queryGrtGuarContByContNo(String serno) {
        List<GrtGuarCont> grtGuarContList = new ArrayList();
        QueryModel queryModel = new QueryModel();
        queryModel.addCondition("serno",serno);
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = this.selectAll(queryModel);
        if(!CollectionUtils.isEmpty(grtGuarBizRstRelList)){
            // guarPkId集合
            List<String> guarPkIds= grtGuarBizRstRelList.parallelStream().map(e->{
                String guarPkId = e.getGuarPkId();
                return guarPkId;
            }).collect(Collectors.toList());
            grtGuarContList = grtGuarContService.selectByGuarPkIds(guarPkIds);
        };
        return grtGuarContList;
    }

    /**
     * 根据资产编号删除担保合同和押品信息
     * @param assetNo
     * @return
     */
    public int deleteByAssetNo(String assetNo) {
        return grtGuarBizRstRelMapper.deleteByAssetNo(assetNo);
    }


    /**
     * 根据担保合同编号删除担保合同
     * @param guarNo
     * @return
     */
    public int deleteByGuarNo(String guarContNo) {
        return grtGuarBizRstRelMapper.deleteByGuarNo(guarContNo);
    }

    /**
     * 通过入参信息获取担保合同金额信息
     *
     * @param params
     * @return
     */
    public Map getAmtInfoByGuarContNo(Map params) {
        Map rtnData = new HashMap();
        String rtnCode = EcbEnum.IQP_GBRA_SUCCESS.key;//初始化标志位
        String rtnMsg = EcbEnum.IQP_GBRA_SUCCESS.value;//初始化信息
        BigDecimal usedAmt = new BigDecimal(0);
        try {
            String guarContNo = (String) params.get("guarContNo");
            if (StringUtils.isBlank(guarContNo)) {
                throw BizException.error(null, EcbEnum.IQP_GBRA_GETAMT_PARAM_EXCEPTION.key, EcbEnum.IQP_GBRA_GETAMT_PARAM_EXCEPTION.value);
            }

            params.put("oprType", CmisCommonConstants.OPR_TYPE_ADD);//操作类型为新增的数据

            List<GrtGuarBizRstRel> amtDataList = grtGuarBizRstRelMapper.selectAmtDataByParams(params);

            //关系数据为空的场景直接返回
            if (amtDataList == null) {
                return rtnData;
            }

            //循环处理
            for (GrtGuarBizRstRel grtGuarBizRstRel : amtDataList) {
                usedAmt = usedAmt.add(grtGuarBizRstRel.getGuarAmt());
            }

        } catch (BizException e) {
            log.error("获取担保合同额度信息出现异常！", e);
            rtnCode = e.getErrorCode();
            rtnMsg = e.getMessage();
        } finally {
            rtnData.put("rtnCode", rtnCode);
            rtnData.put("rtnMsg", rtnMsg);
            rtnData.put("usedAmt", usedAmt);
        }

        return rtnData;
    }

    /**
     * @方法名称: getGrtContAndBizRel
     * @方法描述: 根据担保合同号编号获取完整合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GrtGuarBizRstRelDto> getGrtContAndBizRel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GrtGuarBizRstRelDto> list = grtGuarBizRstRelMapper.getGrtContAndBizRel(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: getGrtBaseByGarContNo
     * @方法描述: 根据担保合同编号获取押品信息
     * @参数与返回说明:
     * @算法描述: 无
     */

    public List<GuarBaseInfo> getGrtBaseByGarContNo(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<GuarBaseInfo> list = grtGuarBizRstRelMapper.getGrtBaseByGarContNo(model);
        PageHelper.clearPage();
        return list;
    }

    /**
     * @方法名称: selectContNoByGuarContNo
     * @方法描述: 根据担保合同号编号获取最早的合同编号
     * @参数与返回说明:
     * @算法描述: 无
     */

    public String selectContNoByGuarContNo(QueryModel model) {
        return grtGuarBizRstRelMapper.selectContNoByGuarContNo(model);
    }

    /**
     * @方法名称: selectSernoByModel
     * @方法描述: 根据担保合同号编号和合同编号查询最新的流水号
     * @参数与返回说明:
     * @算法描述: 无
     */

    public String selectSernoByModel(QueryModel model) {
        return grtGuarBizRstRelMapper.selectSernoByModel(model);
    }

    /**
     * @方法名称: selectGuarContNoBycontNo
     * @方法描述: 根据担保合同号编号获取最早的合同编号(取第一条)
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectGuarContNoBycontNo(String contNo){
        return grtGuarBizRstRelMapper.selectGuarContNoBycontNo(contNo);
    }

    /**
     * @方法名称: selectGuarContNosByContNo
     * @方法描述: 根据担保合同号编号获取最早的合同编号(取第一条)
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectGuarContNosByContNo(String contNo){
        return grtGuarBizRstRelMapper.selectGuarContNosByContNo(contNo);
    }

    /**
     * 根据合同编号查询关联的担保合同
     * @param contNos
     * @return
     */
    public String selectGuarContNosByContNos(String contNos){
        return grtGuarBizRstRelMapper.selectGuarContNosByContNos(contNos);
    }

    /**
     * @方法名称: selectGuarContNosByContNo
     * @方法描述: 根据担保合同号编号获取最早的合同编号(取第一条)
     * @参数与返回说明:
     * @算法描述: 无
     */
    public String selectGuarContNosByContNoForCreditQuery(String contNo){
        return grtGuarBizRstRelMapper.selectGuarContNosByContNoForCreditQuery(contNo);
    }

    /**
     * 查询担保合同对应的借款合同数
     * @param guarContNo
     * @return
     */
    public int countCtrLoanContRecordsByGuarContNo(String guarContNo){
        return grtGuarBizRstRelMapper.countCtrLoanContRecordsByGuarContNo(guarContNo);
    }

    /**
     * @方法名称: queryGrtGuarContBySerno
     * @方法描述: 根据业务流水号获取担保合同信息
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<GrtGuarCont> queryGrtGuarContBySerno(String serno) {
        List<GrtGuarCont> grtGuarContList = new ArrayList();
        List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelMapper.selectByLmtSerno(serno);
        if(CollectionUtils.isEmpty(grtGuarBizRstRelList)){
            return grtGuarContList;
        };
        // guarContNo集合
        List<String> guarContNos= grtGuarBizRstRelList.parallelStream().map(e->{
            String guarContNo = e.getGuarContNo();
            return guarContNo;
        }).collect(Collectors.toList());
        grtGuarContList = grtGuarContService.selectByGuarContNos(guarContNos);
        return grtGuarContList;
    }

    /**
     * 获取关联业务合同信息（真分页速度太慢）
     * @param model
     * @return
     */
    public List<GrtGuarBizRstRelDto> getGrtContBizRelInfo(QueryModel model) {
        List<GrtGuarBizRstRelDto> list = grtGuarBizRstRelMapper.getGrtContBizRelInfo(model);
        int total = list.size() ;
        int startPage = (model.getPage()-1) * model.getSize() ;
        int endPage = model.getPage() * model.getSize() ;
        if(endPage>total) endPage = total ;
        list = list.subList(startPage, endPage) ;
        return list;
    }

    /**
     * @方法名称: riskItem0023
     * @方法描述: 最高额担保合同可用金额校验
     * @参数与返回说明:
     * @算法描述:
     * @创建人: qw
     * @创建时间: 2021-10-11 22:05:02
     * @修改记录: 修改时间    修改人员    修改原因
     */
    public RiskResultDto checkHighGuarContAmt(String contNo,BigDecimal contAmt) {
        RiskResultDto riskResultDto = new RiskResultDto();
        BigDecimal finalAmt = BigDecimal.ZERO;
        List<GrtGuarCont> grtGuarContList = this.queryGrtGuarContByParams(contNo);
        if(CollectionUtils.isEmpty(grtGuarContList)){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_02210);
            return riskResultDto;
        }
        // 最高额担保合同总金额
        BigDecimal totalGuarAmt = BigDecimal.ZERO;
        // 一般合同(生效的借款合同金额之和)
        BigDecimal yBExectiveContAmt = BigDecimal.ZERO;
        // 一般合同(在途的借款合同金额之和)
        BigDecimal yBOnlineContAmt = BigDecimal.ZERO;
        // 一般合同(合同已还款金额)
        BigDecimal yBAlreadyRepayAmt = BigDecimal.ZERO;

        // 最高额合同(生效的借款合同金额之和)
        BigDecimal zGExectiveContAmt = BigDecimal.ZERO;
        // 最高额合同(在途的借款合同金额之和)
        BigDecimal zGOnlineContAmt = BigDecimal.ZERO;
        // 最高额合同(已到期合同对应的台账金额)
        BigDecimal zGAlreadyRepayAmt = BigDecimal.ZERO;

        // 最高额授信协议(生效的借款合同金额之和)
        BigDecimal zGSXExectiveContAmt = BigDecimal.ZERO;
        // 最高额授信协议(在途的借款合同金额之和)
        BigDecimal zGSXOnlineContAmt = BigDecimal.ZERO;
        // 最高额授信协议(已到期合同对应的台账金额)
        BigDecimal zGSXAlreadyRepayAmt = BigDecimal.ZERO;
        for (GrtGuarCont grtGuarCont : grtGuarContList) {
            if(grtGuarCont.getGuarAmt()==null || BigDecimal.ZERO.compareTo(grtGuarCont.getGuarAmt())==0){
                riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
                riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0023001);
                return riskResultDto;
            }
            if(CmisCommonConstants.STD_ZB_GUAR_CONT_TYPE_B.equals(grtGuarCont.getGuarContType())){
                totalGuarAmt = totalGuarAmt.add(grtGuarCont.getGuarAmt());
                List<GrtGuarBizRstRel> grtGuarBizRstRelList = grtGuarBizRstRelMapper.queryGrtGuarBizRstRelDataByGuarContNo(grtGuarCont.getGuarContNo());
                for (GrtGuarBizRstRel grtGuarBizRstRel : grtGuarBizRstRelList) {
                    yBExectiveContAmt = yBExectiveContAmt.add(this.getYBExectiveContAmt(grtGuarBizRstRel.getContNo(),CmisCommonConstants.STD_CONT_TYPE_1));
                    yBOnlineContAmt = yBOnlineContAmt.add(this.getYBtOnlineContAmt(grtGuarBizRstRel.getContNo(),CmisCommonConstants.STD_CONT_TYPE_1));
                    yBAlreadyRepayAmt = yBAlreadyRepayAmt.add(this.getYBAlreadyRepayAmt(grtGuarBizRstRel.getContNo()));

                    zGExectiveContAmt = yBExectiveContAmt.add(this.getYBExectiveContAmt(grtGuarBizRstRel.getContNo(),CmisCommonConstants.STD_CONT_TYPE_2));
                    zGSXExectiveContAmt = yBExectiveContAmt.add(this.getYBExectiveContAmt(grtGuarBizRstRel.getContNo(),CmisCommonConstants.STD_CONT_TYPE_3));

                    zGOnlineContAmt = yBOnlineContAmt.add(this.getYBtOnlineContAmt(grtGuarBizRstRel.getContNo(),CmisCommonConstants.STD_CONT_TYPE_2));
                    zGSXOnlineContAmt = yBOnlineContAmt.add(this.getYBtOnlineContAmt(grtGuarBizRstRel.getContNo(),CmisCommonConstants.STD_CONT_TYPE_3));

                    zGAlreadyRepayAmt = zGAlreadyRepayAmt.add(this.getZGAlreadyRepayAmt(grtGuarBizRstRel.getContNo()));
                    zGSXAlreadyRepayAmt = zGSXAlreadyRepayAmt.add(this.getZGSXAlreadyRepayAmt(grtGuarBizRstRel.getContNo()));
                }
            }
        }
        log.info("最高额担保合同金额之和为【{}】",totalGuarAmt);
        log.info("一般合同生效合同金额之和为【{}】",yBExectiveContAmt);
        log.info("一般合同(在途的借款合同金额之和)为【{}】",yBOnlineContAmt);
        log.info("一般合同(合同已还款金额)为【{}】",yBAlreadyRepayAmt);
        log.info("最高额合同(生效的借款合同金额之和)为【{}】",zGExectiveContAmt);
        log.info("最高额合同(在途的借款合同金额之和)为【{}】",zGOnlineContAmt);
        log.info("最高额合同(已到期合同对应的台账金额或未到期的合同金额)为【{}】",zGAlreadyRepayAmt);
        log.info("最高额授信协议(生效的借款合同金额之和)为【{}】",zGSXExectiveContAmt);
        log.info("最高额授信协议(在途的借款合同金额之和)为【{}】",zGSXOnlineContAmt);
        log.info("最高额授信协议(已到期合同对应的台账金额或未到期的合同金额)为【{}】",zGSXAlreadyRepayAmt);
        finalAmt = yBExectiveContAmt.add(yBOnlineContAmt).subtract(yBAlreadyRepayAmt).add(zGExectiveContAmt).add(zGOnlineContAmt).add(zGAlreadyRepayAmt).add(zGSXExectiveContAmt).add(zGSXOnlineContAmt).add(zGSXAlreadyRepayAmt);
        log.info("最高额担保合同已用金额为【{}】",finalAmt);
        if(BigDecimal.ZERO.compareTo(totalGuarAmt)==0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_COMMON_015);
            return riskResultDto;
        }
        if(totalGuarAmt.compareTo(finalAmt.add(contAmt))<0){
            riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_1);
            riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_002301);
            return riskResultDto;
        }
        riskResultDto.setRiskResultType(CmisRiskConstants.RISK_RESULT_TYPE_0);
        riskResultDto.setRiskResultDesc(CmisRiskConstants.RISK_ERROR_0000);
        return riskResultDto;
    }

    /*
     * 查询担保合同项下生效的合同的合同金额
     */
    public BigDecimal getYBExectiveContAmt(String contNo,String contType){
        BigDecimal ptExectiveContAmt = BigDecimal.ZERO;
        Map map = new HashMap();
        map.put("contNo",contNo);
        map.put("contStatus",CmisCommonConstants.CONT_STATUS_200);
        map.put("contType",contType);
        CtrLoanContDto ctrLoanContDto = ctrLoanContService.queryAllCont(map);
        if(Objects.nonNull(ctrLoanContDto)){
            ptExectiveContAmt = ctrLoanContDto.getContAmt();
        }
        return ptExectiveContAmt;
    }

    /*
     * 查询担保合同项下在途的合同的合同金额
     */
    public BigDecimal getYBtOnlineContAmt(String contNo,String contType){
        BigDecimal yBOnlineContAmt = BigDecimal.ZERO;
        Map map = new HashMap();
        map.put("contNo",contNo);
        map.put("contType",contType);
        IqpLoanAppDto iqpLoanAppDto = ctrLoanContService.queryAllContApp(map);
        if(Objects.nonNull(iqpLoanAppDto)){
            yBOnlineContAmt = iqpLoanAppDto.getContAmt();
        }
        return yBOnlineContAmt;
    }

    /*
     * 查询合同已还款金额
     */
    public BigDecimal getYBAlreadyRepayAmt(String contNo) {
        BigDecimal yBAlreadyRepayAmt = BigDecimal.ZERO;
        Map map = new HashMap();
        map.put("contNo",contNo);
        map.put("contType",CmisCommonConstants.STD_CONT_TYPE_1);
        CtrLoanContDto ctrLoanContDto = ctrLoanContService.queryAllCont(map);
        if(Objects.nonNull(ctrLoanContDto)){
            String busiType = ctrLoanContDto.getBizType();
            String guarWay = ctrLoanContDto.getGuarWay();
            BigDecimal accAmt = BigDecimal.ZERO;
            BigDecimal balanceAmt = BigDecimal.ZERO;

            // 最高额授信协议、普通贷款合同、贸易融资合同
            if(CmisCommonConstants.STD_BUSI_TYPE_01.equals(busiType) || CmisCommonConstants.STD_BUSI_TYPE_02.equals(busiType)
                    || CmisCommonConstants.STD_BUSI_TYPE_04.equals(busiType)){
                List<AccLoan> accLoanList = accLoanService.selectByContNo(contNo);
                if(CollectionUtils.nonEmpty(accLoanList)){
                    for (AccLoan accLoan : accLoanList) {
                        accAmt = accAmt.add(accLoan.getExchangeRmbAmt());
                        balanceAmt = balanceAmt.add(accLoan.getExchangeRmbBal());
                    }
                }
            }else if(CmisCommonConstants.STD_BUSI_TYPE_06.equals(busiType)){
                // 开证合同
                List<AccTfLoc> accTfLocList = accTfLocService.selectByContNo(contNo);
                if(CollectionUtils.nonEmpty(accTfLocList)){
                    for (AccTfLoc accTfLoc : accTfLocList) {
                        accAmt = accAmt.add(accTfLoc.getOcerAmt());
                        balanceAmt = balanceAmt.add(accTfLoc.getCreditBal());
                    }
                }
            }else if(CmisCommonConstants.STD_BUSI_TYPE_07.equals(busiType)){
                // 银承合同
                List<AccAccp> accAccpList = accAccpService.selectByContNo(contNo);
                if(CollectionUtils.nonEmpty(accAccpList)){
                    for (AccAccp accAccp : accAccpList) {
                        accAmt = accAmt.add(accAccp.getDrftTotalAmt());
                        List<AccAccpDrftSub> accAccpDrftSubList = accAccpDrftSubService.selectByBillContNo(contNo);
                        if(CollectionUtils.nonEmpty(accAccpDrftSubList)){
                            for (AccAccpDrftSub accAccpDrftSub : accAccpDrftSubList) {
                                if(CmisCommonConstants.STD_ACC_ACCP_STATUS_0.equals(accAccpDrftSub.getAccStatus())||
                                        CmisCommonConstants.STD_ACC_ACCP_STATUS_1.equals(accAccpDrftSub.getAccStatus()) ||
                                    CmisCommonConstants.STD_ACC_ACCP_STATUS_4.equals(accAccpDrftSub.getAccStatus()) ||
                                    CmisCommonConstants.STD_ACC_ACCP_STATUS_5.equals(accAccpDrftSub.getAccStatus())){
                                    balanceAmt = balanceAmt.add(accAccpDrftSub.getDraftAmt());
                                }
                            }
                        }
                    }
                }
            }else if(CmisCommonConstants.STD_BUSI_TYPE_08.equals(busiType)){
                // 保函合同
                List<AccCvrs> accCvrsList = accCvrsService.selectByContNo(contNo);
                if(CollectionUtils.nonEmpty(accCvrsList)){
                    for (AccCvrs accCvrs : accCvrsList) {
                        if(CmisCommonConstants.ACC_STATUS_0.equals(accCvrs) || CmisCommonConstants.ACC_STATUS_7.equals(accCvrs)){
                            accAmt = accAmt.add(accCvrs.getGuarantAmt());
                            balanceAmt = BigDecimal.ZERO;
                        }else{
                            accAmt = accAmt.add(accCvrs.getGuarantAmt());
                            balanceAmt = balanceAmt.add(accCvrs.getGuarantAmt());
                        }
                    }
                }
            }else {
                // 委托贷款合同
                List<AccEntrustLoan> accEntrustLoanList = accEntrustLoanService.selectByContNo(contNo);
                if(CollectionUtils.nonEmpty(accEntrustLoanList)){
                    for (AccEntrustLoan accEntrustLoan : accEntrustLoanList) {
                        accAmt = accAmt.add(accEntrustLoan.getExchangeRmbAmt());
                        balanceAmt = accAmt.add(accEntrustLoan.getExchangeRmbBal());
                    }
                }
            }
            yBAlreadyRepayAmt = accAmt.subtract(balanceAmt);
        }
        return yBAlreadyRepayAmt;
    }

    /*
     * 最高额授信协议已到期合同对应的台账余额
     */
    public BigDecimal getZGAlreadyRepayAmt(String contNo) {
        BigDecimal zGAlreadyRepayAmt = BigDecimal.ZERO;
        Map map = new HashMap();
        map.put("contNo",contNo);
        map.put("contType",CmisCommonConstants.STD_CONT_TYPE_2);
        CtrLoanContDto ctrLoanContDto = ctrLoanContService.queryAllCont(map);
        if(Objects.nonNull(ctrLoanContDto)){
            String busiType = ctrLoanContDto.getBizType();
            Date endDate = DateUtils.parseDate(ctrLoanContDto.getContEndDate(),"yyyy-MM-dd");
            if(DateUtils.getCurrDate().compareTo(endDate)<0){
                if(CmisCommonConstants.STD_BUSI_TYPE_02.equals(busiType) || CmisCommonConstants.STD_BUSI_TYPE_04.equals(busiType)){
                    List<AccLoan> accLoanList = accLoanService.selectByContNo(contNo);
                    if(CollectionUtils.nonEmpty(accLoanList)){
                        for (AccLoan accLoan : accLoanList) {
                            zGAlreadyRepayAmt = zGAlreadyRepayAmt.add(accLoan.getExchangeRmbBal());
                        }
                    }
                }else if(CmisCommonConstants.STD_BUSI_TYPE_06.equals(busiType)){
                    // 开证合同
                    List<AccTfLoc> accTfLocList = accTfLocService.selectByContNo(contNo);
                    if(CollectionUtils.nonEmpty(accTfLocList)){
                        for (AccTfLoc accTfLoc : accTfLocList) {
                            zGAlreadyRepayAmt = zGAlreadyRepayAmt.add(accTfLoc.getCreditBal());
                        }
                    }
                }else if(CmisCommonConstants.STD_BUSI_TYPE_07.equals(busiType)){
                    // 银承合同
                    List<AccAccp> accAccpList = accAccpService.selectByContNo(contNo);
                    if(CollectionUtils.nonEmpty(accAccpList)){
                        for (AccAccp accAccp : accAccpList) {
                            List<AccAccpDrftSub> accAccpDrftSubList = accAccpDrftSubService.selectByBillContNo(contNo);
                            if(CollectionUtils.nonEmpty(accAccpDrftSubList)){
                                for (AccAccpDrftSub accAccpDrftSub : accAccpDrftSubList) {
                                    if(CmisCommonConstants.STD_ACC_ACCP_STATUS_0.equals(accAccpDrftSub.getAccStatus())||
                                            CmisCommonConstants.STD_ACC_ACCP_STATUS_1.equals(accAccpDrftSub.getAccStatus()) ||
                                            CmisCommonConstants.STD_ACC_ACCP_STATUS_4.equals(accAccpDrftSub.getAccStatus()) ||
                                            CmisCommonConstants.STD_ACC_ACCP_STATUS_5.equals(accAccpDrftSub.getAccStatus())){
                                        zGAlreadyRepayAmt = zGAlreadyRepayAmt.add(accAccpDrftSub.getDraftAmt());
                                    }
                                }
                            }
                        }
                    }
                }else if(CmisCommonConstants.STD_BUSI_TYPE_08.equals(busiType)){
                    // 保函合同
                    List<AccCvrs> accCvrsList = accCvrsService.selectByContNo(contNo);
                    if(CollectionUtils.nonEmpty(accCvrsList)){
                        for (AccCvrs accCvrs : accCvrsList) {
                            if(CmisCommonConstants.ACC_STATUS_0.equals(accCvrs) || CmisCommonConstants.ACC_STATUS_7.equals(accCvrs)){
                                zGAlreadyRepayAmt = BigDecimal.ZERO;
                            }else{
                                zGAlreadyRepayAmt = zGAlreadyRepayAmt.add(accCvrs.getGuarantAmt());
                            }
                        }
                    }
                }else {
                    // 委托贷款合同
                    List<AccEntrustLoan> accEntrustLoanList = accEntrustLoanService.selectByContNo(contNo);
                    if(CollectionUtils.nonEmpty(accEntrustLoanList)){
                        for (AccEntrustLoan accEntrustLoan : accEntrustLoanList) {
                            zGAlreadyRepayAmt = zGAlreadyRepayAmt.add(accEntrustLoan.getExchangeRmbBal());
                        }
                    }
                }
            }else{
                zGAlreadyRepayAmt = ctrLoanContDto.getContAmt();
            }
        }
        return zGAlreadyRepayAmt;
    }

    /*
     * 查询最高额授信协议已到期合同对应的台账余额
     */
    public BigDecimal getZGSXAlreadyRepayAmt(String contNo) {
        BigDecimal zGSXAlreadyRepayAmt = BigDecimal.ZERO;
        CtrHighAmtAgrCont ctrHighAmtAgrCont = ctrHighAmtAgrContService.selectDataByContNo(contNo);
        if (Objects.nonNull(ctrHighAmtAgrCont)) {
            Date endDate = DateUtils.parseDate(ctrHighAmtAgrCont.getEndDate(), "yyyy-MM-dd");
            if (DateUtils.getCurrDate().compareTo(endDate) < 0) {
                List<AccLoan> accLoanList = accLoanService.selectByContNo(contNo);
                if (CollectionUtils.nonEmpty(accLoanList)) {
                    for (AccLoan accLoan : accLoanList) {
                        zGSXAlreadyRepayAmt = zGSXAlreadyRepayAmt.add(accLoan.getExchangeRmbBal());
                    }
                }
            }
        }
        return zGSXAlreadyRepayAmt;
    }

    /**
     * @方法名称: queryGrtGuarBizRstRelDataByGuarContNo
     * @方法描述: 根据担保合同编号查询担保合同与业务关系数据
     * @参数与返回说明:
     * @算法描述: 无
     */
    public List<GrtGuarBizRstRel> queryGrtGuarBizRstRelDataByGuarContNo(String guarContNo){
        return grtGuarBizRstRelMapper.queryGrtGuarBizRstRelDataByGuarContNo(guarContNo);
    }

    /**
     * 根据担保合同编号查询关联的合同编号
     * @param guarContNo
     * @return
     */
    public String selectContNosByGuarContNo(String guarContNo){
        return grtGuarBizRstRelMapper.selectContNosByGuarContNo(guarContNo);
    }
}
