/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysYsd
 * @类描述: rpt_spd_anys_ysd数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-24 15:11:38
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_spd_anys_ysd")
public class RptSpdAnysYsd extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 主办客户经理编号 **/
	@Column(name = "MAIN_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String mainManagerId;
	
	/** 协办客户经理编号 **/
	@Column(name = "ASSIST_MANAGER_ID", unique = false, nullable = true, length = 40)
	private String assistManagerId;

	/** 纳税评级 **/
	@Column(name = "TAX_RATE", unique = false, nullable = true, length = 40)
	private String taxRate;

	/** 去年销售前三客户一名称 **/
	@Column(name = "LAST_YEAR_TOPSELL1_CUS_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell1CusName;
	
	/** 去年销售前三客户一合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL1_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell1CoopTerm;
	
	/** 去年销售前三客户一上年度贡献销售收入 **/
	@Column(name = "LAST_YEAR_TOPSELL1_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell1Income;
	
	/** 去年销售前三客户二名称 **/
	@Column(name = "LAST_YEAR_TOPSELL2_CUS_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell2CusName;
	
	/** 去年销售前三客户二合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL2_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell2CoopTerm;
	
	/** 去年销售前三客户二上年度贡献销售收入 **/
	@Column(name = "LAST_YEAR_TOPSELL2_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell2Income;
	
	/** 去年销售前三客户三名称 **/
	@Column(name = "LAST_YEAR_TOPSELL3_CUS_NAME", unique = false, nullable = true, length = 80)
	private String lastYearTopsell3CusName;
	
	/** 去年销售前三客户三合作年限 **/
	@Column(name = "LAST_YEAR_TOPSELL3_COOP_TERM", unique = false, nullable = true, length = 10)
	private String lastYearTopsell3CoopTerm;
	
	/** 去年销售前三客户三上年度贡献销售收入 **/
	@Column(name = "LAST_YEAR_TOPSELL3_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTopsell3Income;
	
	/** 最近二年企业主要经营场所性质 **/
	@Column(name = "LAST_TWO_YEAR_MAIN_BUSINESS_PREMISES_NATURE", unique = false, nullable = true, length = 80)
	private String lastTwoYearMainBusinessPremisesNature;
	
	/** 超过180天应收账款余额 **/
	@Column(name = "ARBO_180_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal arbo180Days;
	
	/** 企业去年纳税总额 **/
	@Column(name = "LAST_YEAR_TAX_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearTaxTotalAmt;
	
	/** 去年在我行日均贷款 **/
	@Column(name = "ADLIOBLY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal adliobly;
	
	/** 风险评估得分 **/
	@Column(name = "RISK_ASSESS_GRADE", unique = false, nullable = true, length = 10)
	private String riskAssessGrade;
	
	/** 近两年经营场所变更次数 **/
	@Column(name = "LAST_TWO_YEAR_BUSINESS_PREMISES_CHG_TIMES", unique = false, nullable = true, length = 80)
	private String lastTwoYearBusinessPremisesChgTimes;
	
	/** 前年耗用电量能耗值 **/
	@Column(name = "LAST_TWO_TEAR_ECVOPCITPY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoTearEcvopcitpy;
	
	/** 企业前年纳税总额 **/
	@Column(name = "LAST_TWO_YEAR_TAX_TOTAL_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastTwoYearTaxTotalAmt;
	
	/** 客户对外负债 **/
	@Column(name = "CUS_OUTSIDE_DEBT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal cusOutsideDebt;
	
	/** 系统测算最大授信额度 **/
	@Column(name = "CAL_MAX_LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal calMaxLmtAmt;
	
	/** 企业去年收入占比不低于5%省份个数 **/
	@Column(name = "TNOPWTIOEAFNLT5PLY", unique = false, nullable = true, length = 80)
	private String tnopwtioeafnlt5ply;
	
	/** 去年耗用电量能耗值 **/
	@Column(name = "LAST_YEAR_ECVOPCITPY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lastYearEcvopcitpy;
	
	/** 可核实营业收入 **/
	@Column(name = "VERIFIABLE_OPERATING_INCOME", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal verifiableOperatingIncome;
	
	/** 对外保证金额 **/
	@Column(name = "OUTER_GRT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outerGrtAmt;
	
	/** 授信额度 **/
	@Column(name = "LMT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal lmtAmt;
	
	/** 企业上年度所有供应商总供应量 **/
	@Column(name = "TSOASITPY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal tsoasitpy;
	
	/** 前年发放工资总额 **/
	@Column(name = "TOTAL_WAGES_PAID_LAST_TWO_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalWagesPaidLastTwoYear;
	
	/** 借款人在我行结算账户销售回款 **/
	@Column(name = "SCOTBSAIOB", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal scotbsaiob;
	
	/** 对外抵押金额 **/
	@Column(name = "OUTER_PLD_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outerPldAmt;
	
	/** 超过90天应收账款余额 **/
	@Column(name = "ARBO_90_DAYS", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal arbo90Days;
	
	/** 去年发放工资总额 **/
	@Column(name = "TOTAL_WAGES_PAID_LAST_YEAR", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal totalWagesPaidLastYear;
	
	/** 去年在我行日均存款 **/
	@Column(name = "ADDIOBLY", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal addiobly;
	
	/** 对外质押金额 **/
	@Column(name = "OUTER_IMN_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal outerImnAmt;
	
	/** 通过银行结算流水的分析，对借款人销售收入的交叉检验 **/
	@Column(name = "SALE_INCOME_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String saleIncomeAnalysis;
	
	/** 通过借款人纳税记录的分析，对借款人销售收入的交叉检验分析借款人纳税情况，并收集相关证据，交叉检验借款人销售收入的真实性 **/
	@Column(name = "TAX_INCOME_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String taxIncomeAnalysis;
	
	/** 通过存货、应收账款对借款人销售收入的交叉检验 **/
	@Column(name = "STOCK_INCOME_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String stockIncomeAnalysis;
	
	/** 通过管理方直接获取的数据对借款人销售收入的交叉检验 **/
	@Column(name = "MANAGE_INCOME_ANALYSIS", unique = false, nullable = true, length = 65535)
	private String manageIncomeAnalysis;
	
	/** 企业及实际控制人征信信用情况简要情况描述 **/
	@Column(name = "PFK_YSD_1_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd1Desc;
	
	/** 企业及实际控制人征信信用情况主办客户经理评分 **/
	@Column(name = "PFK_YSD_1_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd1Grade1;
	
	/** 企业及实际控制人征信信用情况协办客户经理评分 **/
	@Column(name = "PFK_YSD_1_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd1Grade2;
	
	/** 企业及实际控制人负债与核心资产比率简要情况描述 **/
	@Column(name = "PFK_YSD_2_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd2Desc;
	
	/** 企业及实际控制人负债与核心资产比率主办客户经理评分 **/
	@Column(name = "PFK_YSD_2_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd2Grade1;
	
	/** 企业及实际控制人负债与核心资产比率协办客户经理评分 **/
	@Column(name = "PFK_YSD_2_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd2Grade2;
	
	/** 企业及实际控制人负债与自有资产比率简要情况描述 **/
	@Column(name = "PFK_YSD_3_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd3Desc;
	
	/** 企业及实际控制人负债与自有资产比率主办客户经理评分 **/
	@Column(name = "PFK_YSD_3_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd3Grade1;
	
	/** 企业及实际控制人负债与自有资产比率协办客户经理评分 **/
	@Column(name = "PFK_YSD_3_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd3Grade2;
	
	/** 企业现有融资情况简要情况描述 **/
	@Column(name = "PFK_YSD_4_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd4Desc;
	
	/** 企业现有融资情况主办客户经理评分 **/
	@Column(name = "PFK_YSD_4_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd4Grade1;
	
	/** 企业现有融资情况协办客户经理评分 **/
	@Column(name = "PFK_YSD_4_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd4Grade2;
	
	/** 对外担保情况简要情况描述
企业现有融资情况主办客户经理评分
企业现有融资情况协办客户经理评分简要情况描述 **/
	@Column(name = "PFK_YSD_5_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd5Desc;
	
	/** 对外担保情况主办客户经理评分 **/
	@Column(name = "PFK_YSD_5_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd5Grade1;
	
	/** 对外担保情况协办客户经理评分 **/
	@Column(name = "PFK_YSD_5_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd5Grade2;
	
	/** 实际控制人从事本行业年限简要情况描述 **/
	@Column(name = "PFK_YSD_6_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd6Desc;
	
	/** 实际控制人从事本行业年限主办客户经理评分 **/
	@Column(name = "PFK_YSD_6_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd6Grade1;
	
	/** 实际控制人从事本行业年限协办客户经理评分 **/
	@Column(name = "PFK_YSD_6_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd6Grade2;
	
	/** 企业所属行业简要情况描述 **/
	@Column(name = "PFK_YSD_7_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd7Desc;
	
	/** 企业所属行业主办客户经理评分 **/
	@Column(name = "PFK_YSD_7_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd7Grade1;
	
	/** 企业所属行业协办客户经理评分 **/
	@Column(name = "PFK_YSD_7_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd7Grade2;
	
	/** 企业经营状况简要情况描述 **/
	@Column(name = "PFK_YSD_8_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd8Desc;
	
	/** 企业经营状况主办客户经理评分 **/
	@Column(name = "PFK_YSD_8_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd8Grade1;
	
	/** 企业经营状况协办客户经理评分 **/
	@Column(name = "PFK_YSD_8_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd8Grade2;
	
	/** 企业上下游合作情况简要情况描述 **/
	@Column(name = "PFK_YSD_9_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd9Desc;
	
	/** 企业上下游合作情况主办客户经理评分 **/
	@Column(name = "PFK_YSD_9_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd9Grade1;
	
	/** 企业上下游合作情况协办客户经理评分 **/
	@Column(name = "PFK_YSD_9_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd9Grade2;
	
	/** 法人或实际控制人其他产业经营情况简要情况描述 **/
	@Column(name = "PFK_YSD_10_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd10Desc;
	
	/** 法人或实际控制人其他产业经营情况主办客户经理评分 **/
	@Column(name = "PFK_YSD_10_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd10Grade1;
	
	/** 法人或实际控制人其他产业经营情况协办客户经理评分 **/
	@Column(name = "PFK_YSD_10_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd10Grade2;
	
	/** 企业及实际控人名下房产抵押情况简要情况描述 **/
	@Column(name = "PFK_YSD_11_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd11Desc;
	
	/** 企业及实际控人名下房产抵押情况主办客户经理评分 **/
	@Column(name = "PFK_YSD_11_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd11Grade1;
	
	/** 企业及实际控人名下房产抵押情况协办客户经理评分 **/
	@Column(name = "PFK_YSD_11_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd11Grade2;
	
	/** 申请金额与企业主名下可抵押房产净值+自有资产净值比率简要情况描述 **/
	@Column(name = "PFK_YSD_12_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd12Desc;
	
	/** 申请金额与企业主名下可抵押房产净值+自有资产净值比率主办客户经理评分 **/
	@Column(name = "PFK_YSD_12_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd12Grade1;
	
	/** 申请金额与企业主名下可抵押房产净值+自有资产净值比率协办客户经理评分 **/
	@Column(name = "PFK_YSD_12_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd12Grade2;
	
	/** 法人面谈情况简要情况描述 **/
	@Column(name = "PFK_YSD_13_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd13Desc;
	
	/** 法人面谈情况主办客户经理评分 **/
	@Column(name = "PFK_YSD_13_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd13Grade1;
	
	/** 法人面谈情况协办客户经理评分 **/
	@Column(name = "PFK_YSD_13_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd13Grade2;
	
	/** 法人或实际控制人个人情况简要情况描述 **/
	@Column(name = "PFK_YSD_14_DESC", unique = false, nullable = true, length = 500)
	private String pfkYsd14Desc;
	
	/** 法人或实际控制人个人情况主办客户经理评分 **/
	@Column(name = "PFK_YSD_14_GRADE1", unique = false, nullable = true, length = 10)
	private Integer pfkYsd14Grade1;
	
	/** 法人或实际控制人个人情况协办客户经理评分 **/
	@Column(name = "PFK_YSD_14_GRADE2", unique = false, nullable = true, length = 10)
	private Integer pfkYsd14Grade2;
	
	/** 企业是否按时交纳各项税款，有无被税务机关查处和处罚 **/
	@Column(name = "FOCUS_YSD_1", unique = false, nullable = true, length = 65535)
	private String focusYsd1;
	
	/** 实际控制人有无吸毒、赌博等不良嗜好，其信用卡是否经常在境外大额支付等 **/
	@Column(name = "FOCUS_YSD_2", unique = false, nullable = true, length = 65535)
	private String focusYsd2;
	
	/** 企业员工人数是否稳定，员工待遇是否合理 **/
	@Column(name = "FOCUS_YSD_3", unique = false, nullable = true, length = 65535)
	private String focusYsd3;
	
	/** 有无其他影响企业稳定经营的情况 **/
	@Column(name = "FOCUS_YSD_4", unique = false, nullable = true, length = 65535)
	private String focusYsd4;
	
	/** 近两年是否存在工商，税务，银行不良记录 **/
	@Column(name = "FOCUS_YSD_5", unique = false, nullable = true, length = 65535)
	private String focusYsd5;
	
	/** 企业或实际控制人夫妇是否在业务申办行本地有住宅，商铺，厂房等实物资产 **/
	@Column(name = "FOCUS_YSD_6", unique = false, nullable = true, length = 65535)
	private String focusYsd6;
	
	/** 如有其它不利情况请简述 **/
	@Column(name = "FOCUS_YSD_7", unique = false, nullable = true, length = 65535)
	private String focusYsd7;
	
	/** 企业有无违规排污，是否切实做好环保治理工作，有无被环保部门查处和处罚 **/
	@Column(name = "FOCUS_YSD_8", unique = false, nullable = true, length = 65535)
	private String focusYsd8;
	
	/** 实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为 **/
	@Column(name = "FOCUS_YSD_9", unique = false, nullable = true, length = 65535)
	private String focusYsd9;
	
	/** 有无异常工商股权变更情况 **/
	@Column(name = "FOCUS_YSD_10", unique = false, nullable = true, length = 65535)
	private String focusYsd10;
	
	/** 实际控制人是否参与民间融资、投资高风险行业等行为 **/
	@Column(name = "FOCUS_YSD_11", unique = false, nullable = true, length = 65535)
	private String focusYsd11;
	
	/** 银行家数是否超过3家或资产负债率超过75%  **/
	@Column(name = "FOCUS_YSD_12", unique = false, nullable = true, length = 65535)
	private String focusYsd12;
	
	/** 企业及实际控制人他行信用贷款余额，我行信用贷款余额，信用卡用信额，优税贷申请额(含小贷)合计是否高于500万元  **/
	@Column(name = "FOCUS_YSD_13", unique = false, nullable = true, length = 65535)
	private String focusYsd13;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param mainManagerId
	 */
	public void setMainManagerId(String mainManagerId) {
		this.mainManagerId = mainManagerId;
	}
	
    /**
     * @return mainManagerId
     */
	public String getMainManagerId() {
		return this.mainManagerId;
	}
	
	/**
	 * @param assistManagerId
	 */
	public void setAssistManagerId(String assistManagerId) {
		this.assistManagerId = assistManagerId;
	}
	
    /**
     * @return assistManagerId
     */
	public String getAssistManagerId() {
		return this.assistManagerId;
	}
	
	/**
	 * @param lastYearTopsell1CusName
	 */
	public void setLastYearTopsell1CusName(String lastYearTopsell1CusName) {
		this.lastYearTopsell1CusName = lastYearTopsell1CusName;
	}
	
    /**
     * @return lastYearTopsell1CusName
     */
	public String getLastYearTopsell1CusName() {
		return this.lastYearTopsell1CusName;
	}
	
	/**
	 * @param lastYearTopsell1CoopTerm
	 */
	public void setLastYearTopsell1CoopTerm(String lastYearTopsell1CoopTerm) {
		this.lastYearTopsell1CoopTerm = lastYearTopsell1CoopTerm;
	}
	
    /**
     * @return lastYearTopsell1CoopTerm
     */
	public String getLastYearTopsell1CoopTerm() {
		return this.lastYearTopsell1CoopTerm;
	}
	
	/**
	 * @param lastYearTopsell1Income
	 */
	public void setLastYearTopsell1Income(java.math.BigDecimal lastYearTopsell1Income) {
		this.lastYearTopsell1Income = lastYearTopsell1Income;
	}
	
    /**
     * @return lastYearTopsell1Income
     */
	public java.math.BigDecimal getLastYearTopsell1Income() {
		return this.lastYearTopsell1Income;
	}
	
	/**
	 * @param lastYearTopsell2CusName
	 */
	public void setLastYearTopsell2CusName(String lastYearTopsell2CusName) {
		this.lastYearTopsell2CusName = lastYearTopsell2CusName;
	}
	
    /**
     * @return lastYearTopsell2CusName
     */
	public String getLastYearTopsell2CusName() {
		return this.lastYearTopsell2CusName;
	}
	
	/**
	 * @param lastYearTopsell2CoopTerm
	 */
	public void setLastYearTopsell2CoopTerm(String lastYearTopsell2CoopTerm) {
		this.lastYearTopsell2CoopTerm = lastYearTopsell2CoopTerm;
	}
	
    /**
     * @return lastYearTopsell2CoopTerm
     */
	public String getLastYearTopsell2CoopTerm() {
		return this.lastYearTopsell2CoopTerm;
	}
	
	/**
	 * @param lastYearTopsell2Income
	 */
	public void setLastYearTopsell2Income(java.math.BigDecimal lastYearTopsell2Income) {
		this.lastYearTopsell2Income = lastYearTopsell2Income;
	}
	
    /**
     * @return lastYearTopsell2Income
     */
	public java.math.BigDecimal getLastYearTopsell2Income() {
		return this.lastYearTopsell2Income;
	}
	
	/**
	 * @param lastYearTopsell3CusName
	 */
	public void setLastYearTopsell3CusName(String lastYearTopsell3CusName) {
		this.lastYearTopsell3CusName = lastYearTopsell3CusName;
	}
	
    /**
     * @return lastYearTopsell3CusName
     */
	public String getLastYearTopsell3CusName() {
		return this.lastYearTopsell3CusName;
	}
	
	/**
	 * @param lastYearTopsell3CoopTerm
	 */
	public void setLastYearTopsell3CoopTerm(String lastYearTopsell3CoopTerm) {
		this.lastYearTopsell3CoopTerm = lastYearTopsell3CoopTerm;
	}
	
    /**
     * @return lastYearTopsell3CoopTerm
     */
	public String getLastYearTopsell3CoopTerm() {
		return this.lastYearTopsell3CoopTerm;
	}
	
	/**
	 * @param lastYearTopsell3Income
	 */
	public void setLastYearTopsell3Income(java.math.BigDecimal lastYearTopsell3Income) {
		this.lastYearTopsell3Income = lastYearTopsell3Income;
	}
	
    /**
     * @return lastYearTopsell3Income
     */
	public java.math.BigDecimal getLastYearTopsell3Income() {
		return this.lastYearTopsell3Income;
	}
	
	/**
	 * @param lastTwoYearMainBusinessPremisesNature
	 */
	public void setLastTwoYearMainBusinessPremisesNature(String lastTwoYearMainBusinessPremisesNature) {
		this.lastTwoYearMainBusinessPremisesNature = lastTwoYearMainBusinessPremisesNature;
	}
	
    /**
     * @return lastTwoYearMainBusinessPremisesNature
     */
	public String getLastTwoYearMainBusinessPremisesNature() {
		return this.lastTwoYearMainBusinessPremisesNature;
	}
	
	/**
	 * @param arbo180Days
	 */
	public void setArbo180Days(java.math.BigDecimal arbo180Days) {
		this.arbo180Days = arbo180Days;
	}
	
    /**
     * @return arbo180Days
     */
	public java.math.BigDecimal getArbo180Days() {
		return this.arbo180Days;
	}
	
	/**
	 * @param lastYearTaxTotalAmt
	 */
	public void setLastYearTaxTotalAmt(java.math.BigDecimal lastYearTaxTotalAmt) {
		this.lastYearTaxTotalAmt = lastYearTaxTotalAmt;
	}
	
    /**
     * @return lastYearTaxTotalAmt
     */
	public java.math.BigDecimal getLastYearTaxTotalAmt() {
		return this.lastYearTaxTotalAmt;
	}
	
	/**
	 * @param adliobly
	 */
	public void setAdliobly(java.math.BigDecimal adliobly) {
		this.adliobly = adliobly;
	}
	
    /**
     * @return adliobly
     */
	public java.math.BigDecimal getAdliobly() {
		return this.adliobly;
	}
	
	/**
	 * @param riskAssessGrade
	 */
	public void setRiskAssessGrade(String riskAssessGrade) {
		this.riskAssessGrade = riskAssessGrade;
	}
	
    /**
     * @return riskAssessGrade
     */
	public String getRiskAssessGrade() {
		return this.riskAssessGrade;
	}
	
	/**
	 * @param lastTwoYearBusinessPremisesChgTimes
	 */
	public void setLastTwoYearBusinessPremisesChgTimes(String lastTwoYearBusinessPremisesChgTimes) {
		this.lastTwoYearBusinessPremisesChgTimes = lastTwoYearBusinessPremisesChgTimes;
	}
	
    /**
     * @return lastTwoYearBusinessPremisesChgTimes
     */
	public String getLastTwoYearBusinessPremisesChgTimes() {
		return this.lastTwoYearBusinessPremisesChgTimes;
	}
	
	/**
	 * @param lastTwoTearEcvopcitpy
	 */
	public void setLastTwoTearEcvopcitpy(java.math.BigDecimal lastTwoTearEcvopcitpy) {
		this.lastTwoTearEcvopcitpy = lastTwoTearEcvopcitpy;
	}
	
    /**
     * @return lastTwoTearEcvopcitpy
     */
	public java.math.BigDecimal getLastTwoTearEcvopcitpy() {
		return this.lastTwoTearEcvopcitpy;
	}
	
	/**
	 * @param lastTwoYearTaxTotalAmt
	 */
	public void setLastTwoYearTaxTotalAmt(java.math.BigDecimal lastTwoYearTaxTotalAmt) {
		this.lastTwoYearTaxTotalAmt = lastTwoYearTaxTotalAmt;
	}
	
    /**
     * @return lastTwoYearTaxTotalAmt
     */
	public java.math.BigDecimal getLastTwoYearTaxTotalAmt() {
		return this.lastTwoYearTaxTotalAmt;
	}
	
	/**
	 * @param cusOutsideDebt
	 */
	public void setCusOutsideDebt(java.math.BigDecimal cusOutsideDebt) {
		this.cusOutsideDebt = cusOutsideDebt;
	}
	
    /**
     * @return cusOutsideDebt
     */
	public java.math.BigDecimal getCusOutsideDebt() {
		return this.cusOutsideDebt;
	}
	
	/**
	 * @param calMaxLmtAmt
	 */
	public void setCalMaxLmtAmt(java.math.BigDecimal calMaxLmtAmt) {
		this.calMaxLmtAmt = calMaxLmtAmt;
	}
	
    /**
     * @return calMaxLmtAmt
     */
	public java.math.BigDecimal getCalMaxLmtAmt() {
		return this.calMaxLmtAmt;
	}
	
	/**
	 * @param tnopwtioeafnlt5ply
	 */
	public void setTnopwtioeafnlt5ply(String tnopwtioeafnlt5ply) {
		this.tnopwtioeafnlt5ply = tnopwtioeafnlt5ply;
	}
	
    /**
     * @return tnopwtioeafnlt5ply
     */
	public String getTnopwtioeafnlt5ply() {
		return this.tnopwtioeafnlt5ply;
	}
	
	/**
	 * @param lastYearEcvopcitpy
	 */
	public void setLastYearEcvopcitpy(java.math.BigDecimal lastYearEcvopcitpy) {
		this.lastYearEcvopcitpy = lastYearEcvopcitpy;
	}
	
    /**
     * @return lastYearEcvopcitpy
     */
	public java.math.BigDecimal getLastYearEcvopcitpy() {
		return this.lastYearEcvopcitpy;
	}
	
	/**
	 * @param verifiableOperatingIncome
	 */
	public void setVerifiableOperatingIncome(java.math.BigDecimal verifiableOperatingIncome) {
		this.verifiableOperatingIncome = verifiableOperatingIncome;
	}
	
    /**
     * @return verifiableOperatingIncome
     */
	public java.math.BigDecimal getVerifiableOperatingIncome() {
		return this.verifiableOperatingIncome;
	}
	
	/**
	 * @param outerGrtAmt
	 */
	public void setOuterGrtAmt(java.math.BigDecimal outerGrtAmt) {
		this.outerGrtAmt = outerGrtAmt;
	}
	
    /**
     * @return outerGrtAmt
     */
	public java.math.BigDecimal getOuterGrtAmt() {
		return this.outerGrtAmt;
	}
	
	/**
	 * @param lmtAmt
	 */
	public void setLmtAmt(java.math.BigDecimal lmtAmt) {
		this.lmtAmt = lmtAmt;
	}
	
    /**
     * @return lmtAmt
     */
	public java.math.BigDecimal getLmtAmt() {
		return this.lmtAmt;
	}
	
	/**
	 * @param tsoasitpy
	 */
	public void setTsoasitpy(java.math.BigDecimal tsoasitpy) {
		this.tsoasitpy = tsoasitpy;
	}
	
    /**
     * @return tsoasitpy
     */
	public java.math.BigDecimal getTsoasitpy() {
		return this.tsoasitpy;
	}
	
	/**
	 * @param totalWagesPaidLastTwoYear
	 */
	public void setTotalWagesPaidLastTwoYear(java.math.BigDecimal totalWagesPaidLastTwoYear) {
		this.totalWagesPaidLastTwoYear = totalWagesPaidLastTwoYear;
	}
	
    /**
     * @return totalWagesPaidLastTwoYear
     */
	public java.math.BigDecimal getTotalWagesPaidLastTwoYear() {
		return this.totalWagesPaidLastTwoYear;
	}
	
	/**
	 * @param scotbsaiob
	 */
	public void setScotbsaiob(java.math.BigDecimal scotbsaiob) {
		this.scotbsaiob = scotbsaiob;
	}
	
    /**
     * @return scotbsaiob
     */
	public java.math.BigDecimal getScotbsaiob() {
		return this.scotbsaiob;
	}
	
	/**
	 * @param outerPldAmt
	 */
	public void setOuterPldAmt(java.math.BigDecimal outerPldAmt) {
		this.outerPldAmt = outerPldAmt;
	}
	
    /**
     * @return outerPldAmt
     */
	public java.math.BigDecimal getOuterPldAmt() {
		return this.outerPldAmt;
	}
	
	/**
	 * @param arbo90Days
	 */
	public void setArbo90Days(java.math.BigDecimal arbo90Days) {
		this.arbo90Days = arbo90Days;
	}
	
    /**
     * @return arbo90Days
     */
	public java.math.BigDecimal getArbo90Days() {
		return this.arbo90Days;
	}
	
	/**
	 * @param totalWagesPaidLastYear
	 */
	public void setTotalWagesPaidLastYear(java.math.BigDecimal totalWagesPaidLastYear) {
		this.totalWagesPaidLastYear = totalWagesPaidLastYear;
	}
	
    /**
     * @return totalWagesPaidLastYear
     */
	public java.math.BigDecimal getTotalWagesPaidLastYear() {
		return this.totalWagesPaidLastYear;
	}
	
	/**
	 * @param addiobly
	 */
	public void setAddiobly(java.math.BigDecimal addiobly) {
		this.addiobly = addiobly;
	}
	
    /**
     * @return addiobly
     */
	public java.math.BigDecimal getAddiobly() {
		return this.addiobly;
	}
	
	/**
	 * @param outerImnAmt
	 */
	public void setOuterImnAmt(java.math.BigDecimal outerImnAmt) {
		this.outerImnAmt = outerImnAmt;
	}
	
    /**
     * @return outerImnAmt
     */
	public java.math.BigDecimal getOuterImnAmt() {
		return this.outerImnAmt;
	}
	
	/**
	 * @param saleIncomeAnalysis
	 */
	public void setSaleIncomeAnalysis(String saleIncomeAnalysis) {
		this.saleIncomeAnalysis = saleIncomeAnalysis;
	}
	
    /**
     * @return saleIncomeAnalysis
     */
	public String getSaleIncomeAnalysis() {
		return this.saleIncomeAnalysis;
	}
	
	/**
	 * @param taxIncomeAnalysis
	 */
	public void setTaxIncomeAnalysis(String taxIncomeAnalysis) {
		this.taxIncomeAnalysis = taxIncomeAnalysis;
	}
	
    /**
     * @return taxIncomeAnalysis
     */
	public String getTaxIncomeAnalysis() {
		return this.taxIncomeAnalysis;
	}
	
	/**
	 * @param stockIncomeAnalysis
	 */
	public void setStockIncomeAnalysis(String stockIncomeAnalysis) {
		this.stockIncomeAnalysis = stockIncomeAnalysis;
	}
	
    /**
     * @return stockIncomeAnalysis
     */
	public String getStockIncomeAnalysis() {
		return this.stockIncomeAnalysis;
	}
	
	/**
	 * @param manageIncomeAnalysis
	 */
	public void setManageIncomeAnalysis(String manageIncomeAnalysis) {
		this.manageIncomeAnalysis = manageIncomeAnalysis;
	}
	
    /**
     * @return manageIncomeAnalysis
     */
	public String getManageIncomeAnalysis() {
		return this.manageIncomeAnalysis;
	}
	
	/**
	 * @param pfkYsd1Desc
	 */
	public void setPfkYsd1Desc(String pfkYsd1Desc) {
		this.pfkYsd1Desc = pfkYsd1Desc;
	}
	
    /**
     * @return pfkYsd1Desc
     */
	public String getPfkYsd1Desc() {
		return this.pfkYsd1Desc;
	}
	
	/**
	 * @param pfkYsd1Grade1
	 */
	public void setPfkYsd1Grade1(Integer pfkYsd1Grade1) {
		this.pfkYsd1Grade1 = pfkYsd1Grade1;
	}
	
    /**
     * @return pfkYsd1Grade1
     */
	public Integer getPfkYsd1Grade1() {
		return this.pfkYsd1Grade1;
	}
	
	/**
	 * @param pfkYsd1Grade2
	 */
	public void setPfkYsd1Grade2(Integer pfkYsd1Grade2) {
		this.pfkYsd1Grade2 = pfkYsd1Grade2;
	}
	
    /**
     * @return pfkYsd1Grade2
     */
	public Integer getPfkYsd1Grade2() {
		return this.pfkYsd1Grade2;
	}
	
	/**
	 * @param pfkYsd2Desc
	 */
	public void setPfkYsd2Desc(String pfkYsd2Desc) {
		this.pfkYsd2Desc = pfkYsd2Desc;
	}
	
    /**
     * @return pfkYsd2Desc
     */
	public String getPfkYsd2Desc() {
		return this.pfkYsd2Desc;
	}
	
	/**
	 * @param pfkYsd2Grade1
	 */
	public void setPfkYsd2Grade1(Integer pfkYsd2Grade1) {
		this.pfkYsd2Grade1 = pfkYsd2Grade1;
	}
	
    /**
     * @return pfkYsd2Grade1
     */
	public Integer getPfkYsd2Grade1() {
		return this.pfkYsd2Grade1;
	}
	
	/**
	 * @param pfkYsd2Grade2
	 */
	public void setPfkYsd2Grade2(Integer pfkYsd2Grade2) {
		this.pfkYsd2Grade2 = pfkYsd2Grade2;
	}
	
    /**
     * @return pfkYsd2Grade2
     */
	public Integer getPfkYsd2Grade2() {
		return this.pfkYsd2Grade2;
	}
	
	/**
	 * @param pfkYsd3Desc
	 */
	public void setPfkYsd3Desc(String pfkYsd3Desc) {
		this.pfkYsd3Desc = pfkYsd3Desc;
	}
	
    /**
     * @return pfkYsd3Desc
     */
	public String getPfkYsd3Desc() {
		return this.pfkYsd3Desc;
	}
	
	/**
	 * @param pfkYsd3Grade1
	 */
	public void setPfkYsd3Grade1(Integer pfkYsd3Grade1) {
		this.pfkYsd3Grade1 = pfkYsd3Grade1;
	}
	
    /**
     * @return pfkYsd3Grade1
     */
	public Integer getPfkYsd3Grade1() {
		return this.pfkYsd3Grade1;
	}
	
	/**
	 * @param pfkYsd3Grade2
	 */
	public void setPfkYsd3Grade2(Integer pfkYsd3Grade2) {
		this.pfkYsd3Grade2 = pfkYsd3Grade2;
	}
	
    /**
     * @return pfkYsd3Grade2
     */
	public Integer getPfkYsd3Grade2() {
		return this.pfkYsd3Grade2;
	}
	
	/**
	 * @param pfkYsd4Desc
	 */
	public void setPfkYsd4Desc(String pfkYsd4Desc) {
		this.pfkYsd4Desc = pfkYsd4Desc;
	}
	
    /**
     * @return pfkYsd4Desc
     */
	public String getPfkYsd4Desc() {
		return this.pfkYsd4Desc;
	}
	
	/**
	 * @param pfkYsd4Grade1
	 */
	public void setPfkYsd4Grade1(Integer pfkYsd4Grade1) {
		this.pfkYsd4Grade1 = pfkYsd4Grade1;
	}
	
    /**
     * @return pfkYsd4Grade1
     */
	public Integer getPfkYsd4Grade1() {
		return this.pfkYsd4Grade1;
	}
	
	/**
	 * @param pfkYsd4Grade2
	 */
	public void setPfkYsd4Grade2(Integer pfkYsd4Grade2) {
		this.pfkYsd4Grade2 = pfkYsd4Grade2;
	}
	
    /**
     * @return pfkYsd4Grade2
     */
	public Integer getPfkYsd4Grade2() {
		return this.pfkYsd4Grade2;
	}
	
	/**
	 * @param pfkYsd5Desc
	 */
	public void setPfkYsd5Desc(String pfkYsd5Desc) {
		this.pfkYsd5Desc = pfkYsd5Desc;
	}
	
    /**
     * @return pfkYsd5Desc
     */
	public String getPfkYsd5Desc() {
		return this.pfkYsd5Desc;
	}
	
	/**
	 * @param pfkYsd5Grade1
	 */
	public void setPfkYsd5Grade1(Integer pfkYsd5Grade1) {
		this.pfkYsd5Grade1 = pfkYsd5Grade1;
	}
	
    /**
     * @return pfkYsd5Grade1
     */
	public Integer getPfkYsd5Grade1() {
		return this.pfkYsd5Grade1;
	}
	
	/**
	 * @param pfkYsd5Grade2
	 */
	public void setPfkYsd5Grade2(Integer pfkYsd5Grade2) {
		this.pfkYsd5Grade2 = pfkYsd5Grade2;
	}
	
    /**
     * @return pfkYsd5Grade2
     */
	public Integer getPfkYsd5Grade2() {
		return this.pfkYsd5Grade2;
	}
	
	/**
	 * @param pfkYsd6Desc
	 */
	public void setPfkYsd6Desc(String pfkYsd6Desc) {
		this.pfkYsd6Desc = pfkYsd6Desc;
	}
	
    /**
     * @return pfkYsd6Desc
     */
	public String getPfkYsd6Desc() {
		return this.pfkYsd6Desc;
	}
	
	/**
	 * @param pfkYsd6Grade1
	 */
	public void setPfkYsd6Grade1(Integer pfkYsd6Grade1) {
		this.pfkYsd6Grade1 = pfkYsd6Grade1;
	}
	
    /**
     * @return pfkYsd6Grade1
     */
	public Integer getPfkYsd6Grade1() {
		return this.pfkYsd6Grade1;
	}
	
	/**
	 * @param pfkYsd6Grade2
	 */
	public void setPfkYsd6Grade2(Integer pfkYsd6Grade2) {
		this.pfkYsd6Grade2 = pfkYsd6Grade2;
	}
	
    /**
     * @return pfkYsd6Grade2
     */
	public Integer getPfkYsd6Grade2() {
		return this.pfkYsd6Grade2;
	}
	
	/**
	 * @param pfkYsd7Desc
	 */
	public void setPfkYsd7Desc(String pfkYsd7Desc) {
		this.pfkYsd7Desc = pfkYsd7Desc;
	}
	
    /**
     * @return pfkYsd7Desc
     */
	public String getPfkYsd7Desc() {
		return this.pfkYsd7Desc;
	}
	
	/**
	 * @param pfkYsd7Grade1
	 */
	public void setPfkYsd7Grade1(Integer pfkYsd7Grade1) {
		this.pfkYsd7Grade1 = pfkYsd7Grade1;
	}
	
    /**
     * @return pfkYsd7Grade1
     */
	public Integer getPfkYsd7Grade1() {
		return this.pfkYsd7Grade1;
	}
	
	/**
	 * @param pfkYsd7Grade2
	 */
	public void setPfkYsd7Grade2(Integer pfkYsd7Grade2) {
		this.pfkYsd7Grade2 = pfkYsd7Grade2;
	}
	
    /**
     * @return pfkYsd7Grade2
     */
	public Integer getPfkYsd7Grade2() {
		return this.pfkYsd7Grade2;
	}
	
	/**
	 * @param pfkYsd8Desc
	 */
	public void setPfkYsd8Desc(String pfkYsd8Desc) {
		this.pfkYsd8Desc = pfkYsd8Desc;
	}
	
    /**
     * @return pfkYsd8Desc
     */
	public String getPfkYsd8Desc() {
		return this.pfkYsd8Desc;
	}
	
	/**
	 * @param pfkYsd8Grade1
	 */
	public void setPfkYsd8Grade1(Integer pfkYsd8Grade1) {
		this.pfkYsd8Grade1 = pfkYsd8Grade1;
	}
	
    /**
     * @return pfkYsd8Grade1
     */
	public Integer getPfkYsd8Grade1() {
		return this.pfkYsd8Grade1;
	}
	
	/**
	 * @param pfkYsd8Grade2
	 */
	public void setPfkYsd8Grade2(Integer pfkYsd8Grade2) {
		this.pfkYsd8Grade2 = pfkYsd8Grade2;
	}
	
    /**
     * @return pfkYsd8Grade2
     */
	public Integer getPfkYsd8Grade2() {
		return this.pfkYsd8Grade2;
	}
	
	/**
	 * @param pfkYsd9Desc
	 */
	public void setPfkYsd9Desc(String pfkYsd9Desc) {
		this.pfkYsd9Desc = pfkYsd9Desc;
	}
	
    /**
     * @return pfkYsd9Desc
     */
	public String getPfkYsd9Desc() {
		return this.pfkYsd9Desc;
	}
	
	/**
	 * @param pfkYsd9Grade1
	 */
	public void setPfkYsd9Grade1(Integer pfkYsd9Grade1) {
		this.pfkYsd9Grade1 = pfkYsd9Grade1;
	}
	
    /**
     * @return pfkYsd9Grade1
     */
	public Integer getPfkYsd9Grade1() {
		return this.pfkYsd9Grade1;
	}
	
	/**
	 * @param pfkYsd9Grade2
	 */
	public void setPfkYsd9Grade2(Integer pfkYsd9Grade2) {
		this.pfkYsd9Grade2 = pfkYsd9Grade2;
	}
	
    /**
     * @return pfkYsd9Grade2
     */
	public Integer getPfkYsd9Grade2() {
		return this.pfkYsd9Grade2;
	}
	
	/**
	 * @param pfkYsd10Desc
	 */
	public void setPfkYsd10Desc(String pfkYsd10Desc) {
		this.pfkYsd10Desc = pfkYsd10Desc;
	}
	
    /**
     * @return pfkYsd10Desc
     */
	public String getPfkYsd10Desc() {
		return this.pfkYsd10Desc;
	}
	
	/**
	 * @param pfkYsd10Grade1
	 */
	public void setPfkYsd10Grade1(Integer pfkYsd10Grade1) {
		this.pfkYsd10Grade1 = pfkYsd10Grade1;
	}
	
    /**
     * @return pfkYsd10Grade1
     */
	public Integer getPfkYsd10Grade1() {
		return this.pfkYsd10Grade1;
	}
	
	/**
	 * @param pfkYsd10Grade2
	 */
	public void setPfkYsd10Grade2(Integer pfkYsd10Grade2) {
		this.pfkYsd10Grade2 = pfkYsd10Grade2;
	}
	
    /**
     * @return pfkYsd10Grade2
     */
	public Integer getPfkYsd10Grade2() {
		return this.pfkYsd10Grade2;
	}
	
	/**
	 * @param pfkYsd11Desc
	 */
	public void setPfkYsd11Desc(String pfkYsd11Desc) {
		this.pfkYsd11Desc = pfkYsd11Desc;
	}
	
    /**
     * @return pfkYsd11Desc
     */
	public String getPfkYsd11Desc() {
		return this.pfkYsd11Desc;
	}
	
	/**
	 * @param pfkYsd11Grade1
	 */
	public void setPfkYsd11Grade1(Integer pfkYsd11Grade1) {
		this.pfkYsd11Grade1 = pfkYsd11Grade1;
	}
	
    /**
     * @return pfkYsd11Grade1
     */
	public Integer getPfkYsd11Grade1() {
		return this.pfkYsd11Grade1;
	}
	
	/**
	 * @param pfkYsd11Grade2
	 */
	public void setPfkYsd11Grade2(Integer pfkYsd11Grade2) {
		this.pfkYsd11Grade2 = pfkYsd11Grade2;
	}
	
    /**
     * @return pfkYsd11Grade2
     */
	public Integer getPfkYsd11Grade2() {
		return this.pfkYsd11Grade2;
	}
	
	/**
	 * @param pfkYsd12Desc
	 */
	public void setPfkYsd12Desc(String pfkYsd12Desc) {
		this.pfkYsd12Desc = pfkYsd12Desc;
	}
	
    /**
     * @return pfkYsd12Desc
     */
	public String getPfkYsd12Desc() {
		return this.pfkYsd12Desc;
	}
	
	/**
	 * @param pfkYsd12Grade1
	 */
	public void setPfkYsd12Grade1(Integer pfkYsd12Grade1) {
		this.pfkYsd12Grade1 = pfkYsd12Grade1;
	}
	
    /**
     * @return pfkYsd12Grade1
     */
	public Integer getPfkYsd12Grade1() {
		return this.pfkYsd12Grade1;
	}
	
	/**
	 * @param pfkYsd12Grade2
	 */
	public void setPfkYsd12Grade2(Integer pfkYsd12Grade2) {
		this.pfkYsd12Grade2 = pfkYsd12Grade2;
	}
	
    /**
     * @return pfkYsd12Grade2
     */
	public Integer getPfkYsd12Grade2() {
		return this.pfkYsd12Grade2;
	}
	
	/**
	 * @param pfkYsd13Desc
	 */
	public void setPfkYsd13Desc(String pfkYsd13Desc) {
		this.pfkYsd13Desc = pfkYsd13Desc;
	}
	
    /**
     * @return pfkYsd13Desc
     */
	public String getPfkYsd13Desc() {
		return this.pfkYsd13Desc;
	}
	
	/**
	 * @param pfkYsd13Grade1
	 */
	public void setPfkYsd13Grade1(Integer pfkYsd13Grade1) {
		this.pfkYsd13Grade1 = pfkYsd13Grade1;
	}
	
    /**
     * @return pfkYsd13Grade1
     */
	public Integer getPfkYsd13Grade1() {
		return this.pfkYsd13Grade1;
	}
	
	/**
	 * @param pfkYsd13Grade2
	 */
	public void setPfkYsd13Grade2(Integer pfkYsd13Grade2) {
		this.pfkYsd13Grade2 = pfkYsd13Grade2;
	}
	
    /**
     * @return pfkYsd13Grade2
     */
	public Integer getPfkYsd13Grade2() {
		return this.pfkYsd13Grade2;
	}
	
	/**
	 * @param pfkYsd14Desc
	 */
	public void setPfkYsd14Desc(String pfkYsd14Desc) {
		this.pfkYsd14Desc = pfkYsd14Desc;
	}
	
    /**
     * @return pfkYsd14Desc
     */
	public String getPfkYsd14Desc() {
		return this.pfkYsd14Desc;
	}
	
	/**
	 * @param pfkYsd14Grade1
	 */
	public void setPfkYsd14Grade1(Integer pfkYsd14Grade1) {
		this.pfkYsd14Grade1 = pfkYsd14Grade1;
	}
	
    /**
     * @return pfkYsd14Grade1
     */
	public Integer getPfkYsd14Grade1() {
		return this.pfkYsd14Grade1;
	}
	
	/**
	 * @param pfkYsd14Grade2
	 */
	public void setPfkYsd14Grade2(Integer pfkYsd14Grade2) {
		this.pfkYsd14Grade2 = pfkYsd14Grade2;
	}
	
    /**
     * @return pfkYsd14Grade2
     */
	public Integer getPfkYsd14Grade2() {
		return this.pfkYsd14Grade2;
	}
	
	/**
	 * @param focusYsd1
	 */
	public void setFocusYsd1(String focusYsd1) {
		this.focusYsd1 = focusYsd1;
	}
	
    /**
     * @return focusYsd1
     */
	public String getFocusYsd1() {
		return this.focusYsd1;
	}
	
	/**
	 * @param focusYsd2
	 */
	public void setFocusYsd2(String focusYsd2) {
		this.focusYsd2 = focusYsd2;
	}
	
    /**
     * @return focusYsd2
     */
	public String getFocusYsd2() {
		return this.focusYsd2;
	}
	
	/**
	 * @param focusYsd3
	 */
	public void setFocusYsd3(String focusYsd3) {
		this.focusYsd3 = focusYsd3;
	}
	
    /**
     * @return focusYsd3
     */
	public String getFocusYsd3() {
		return this.focusYsd3;
	}
	
	/**
	 * @param focusYsd4
	 */
	public void setFocusYsd4(String focusYsd4) {
		this.focusYsd4 = focusYsd4;
	}
	
    /**
     * @return focusYsd4
     */
	public String getFocusYsd4() {
		return this.focusYsd4;
	}
	
	/**
	 * @param focusYsd5
	 */
	public void setFocusYsd5(String focusYsd5) {
		this.focusYsd5 = focusYsd5;
	}
	
    /**
     * @return focusYsd5
     */
	public String getFocusYsd5() {
		return this.focusYsd5;
	}
	
	/**
	 * @param focusYsd6
	 */
	public void setFocusYsd6(String focusYsd6) {
		this.focusYsd6 = focusYsd6;
	}
	
    /**
     * @return focusYsd6
     */
	public String getFocusYsd6() {
		return this.focusYsd6;
	}
	
	/**
	 * @param focusYsd7
	 */
	public void setFocusYsd7(String focusYsd7) {
		this.focusYsd7 = focusYsd7;
	}
	
    /**
     * @return focusYsd7
     */
	public String getFocusYsd7() {
		return this.focusYsd7;
	}
	
	/**
	 * @param focusYsd8
	 */
	public void setFocusYsd8(String focusYsd8) {
		this.focusYsd8 = focusYsd8;
	}
	
    /**
     * @return focusYsd8
     */
	public String getFocusYsd8() {
		return this.focusYsd8;
	}
	
	/**
	 * @param focusYsd9
	 */
	public void setFocusYsd9(String focusYsd9) {
		this.focusYsd9 = focusYsd9;
	}
	
    /**
     * @return focusYsd9
     */
	public String getFocusYsd9() {
		return this.focusYsd9;
	}
	
	/**
	 * @param focusYsd10
	 */
	public void setFocusYsd10(String focusYsd10) {
		this.focusYsd10 = focusYsd10;
	}
	
    /**
     * @return focusYsd10
     */
	public String getFocusYsd10() {
		return this.focusYsd10;
	}
	
	/**
	 * @param focusYsd11
	 */
	public void setFocusYsd11(String focusYsd11) {
		this.focusYsd11 = focusYsd11;
	}
	
    /**
     * @return focusYsd11
     */
	public String getFocusYsd11() {
		return this.focusYsd11;
	}
	
	/**
	 * @param focusYsd12
	 */
	public void setFocusYsd12(String focusYsd12) {
		this.focusYsd12 = focusYsd12;
	}
	
    /**
     * @return focusYsd12
     */
	public String getFocusYsd12() {
		return this.focusYsd12;
	}
	
	/**
	 * @param focusYsd13
	 */
	public void setFocusYsd13(String focusYsd13) {
		this.focusYsd13 = focusYsd13;
	}
	
    /**
     * @return focusYsd13
     */
	public String getFocusYsd13() {
		return this.focusYsd13;
	}

	public String getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(String taxRate) {
		this.taxRate = taxRate;
	}
}