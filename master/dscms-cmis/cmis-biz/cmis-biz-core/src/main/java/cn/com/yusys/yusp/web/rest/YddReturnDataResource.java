///*
// * 代码生成器自动生成的
// * Since 2008 - 2021
// *
// */
//package cn.com.yusys.yusp.web.rest;
//
//import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
//import cn.com.yusys.yusp.domain.LmtSurveyConInfo;
//import cn.com.yusys.yusp.domain.SurveyConInfo;
//import com.mysql.cj.util.StringUtils;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * @version 1.0.0
// * @项目名称: cmis-biz-core模块
// * @类名称: YddResource
// * @类描述: 优抵贷临时返回数据专用
// * @功能描述:
// * @创建人: hubp
// * @创建时间: 2021-04-14 19:53:28
// * @修改备注:
// * @修改记录: 修改时间    修改人员    修改原因
// * -------------------------------------------------------------
// * @Copyright (c) 宇信科技-版权所有
// */
//@RestController
//@RequestMapping("/api/yddreturndataresource")
//public class YddReturnDataResource {
//    /**
//     * @param commenOwnerName, commenOwnerCertCode
//     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.String>
//     * @author hubp
//     * @date 2021/4/16 10:42
//     * @version 1.0.0
//     * @desc 通过共有人姓名和共有人身份证号查询客户编号
//     * @修改历史: 修改时间    修改人员    修改原因
//     */
//    @PostMapping("/getcusid/{commenOwnerName}/{commenOwnerCertCode}")
//    public ResultDto<String> getCusId(@PathVariable("commenOwnerName") String commenOwnerName, @PathVariable("commenOwnerCertCode") String commenOwnerCertCode) {
//        String backData = "";
//        if ("qweq".equals(commenOwnerName) && "513436200004136499".equals(commenOwnerCertCode)) {
//            backData = "2039412312";
//        }
//        return new ResultDto<String>(backData);
//    }
//
//    /**
//     * @param lmtSurveyConInfo
//     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<java.lang.Double>
//     * @author hubp
//     * @date 2021/4/16 10:47
//     * @version 1.0.0
//     * @desc 获取参考利率
//     * @修改历史: 修改时间    修改人员    修改原因
//     */
//    @PostMapping("/getrefrate")
//    public ResultDto<Double> getRefRate(@RequestBody LmtSurveyConInfo lmtSurveyConInfo) {
//        Double backData = 0.1;
//        if (null == lmtSurveyConInfo.getSurveySerno() || "".equals(lmtSurveyConInfo.getSurveySerno())) {
//            backData = 0.0;
//        }
//        return new ResultDto<Double>(backData);
//    }
//
//    @PostMapping("/opanorgname")
//    public ResultDto<Map> getOpanOrgName(@RequestBody String loanPayoutAccNoData) {
//        Map map = new HashMap();
//        map.put("loanPayoutAccName", "张三");
//        map.put("accobName", "中国银行");
//        return new ResultDto<Map>(map);
//    }
//}
