package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSurveyReportMainInfo
 * @类描述: lmt_survey_report_main_info数据实体类
 * @功能描述: 
 * @创建人: 李志敏
 * @创建时间: 2021-05-10 21:56:51
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class LmtSurveyReportMainInfoDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 调查流水号 **/
	private String surveySerno;
	
	/** 第三方业务流水号 **/
	private String bizSerno;
	
	/** 产品名称 **/
	private String prdName;
	
	/** 产品类型 **/
	private String prdType;
	
	/** 产品编号 **/
	private String prdId;
	
	/** 小微调查报告类型 **/
	private String surveyType;
	
	/** 客户编号 **/
	private String cusId;
	
	/** 客户名称 **/
	private String cusName;
	
	/** 证件类型 **/
	private String certType;
	
	/** 证件号码 **/
	private String certCode;
	
	/** 申请金额 **/
	private java.math.BigDecimal appAmt;
	
	/** 审批状态 **/
	private String approveStatus;
	
	/** 数据来源 **/
	private String dataSource;
	
	/** 进件时间 **/
	private String intoTime;
	
	/** 是否自动分配 **/
	private String isAutodivis;
	
	/** 是否线下调查 **/
	private String isStopOffline;
	
	/** 操作类型 **/
	private String oprType;
	
	/** 营销人 **/
	private String marId;
	
	/** 主管机构 **/
	private String managerBrId;
	
	/** 主管客户经理 **/
	private String managerId;
	
	/** 登记人 **/
	private String inputId;
	
	/** 登记机构 **/
	private String inputBrId;
	
	/** 登记日期 **/
	private String inputDate;
	
	/** 最后修改人 **/
	private String updId;
	
	/** 最后修改机构 **/
	private String updBrId;
	
	/** 最后修改日期 **/
	private String updDate;
	
	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	/** 营销人工号 **/
	private String marketingId;
	
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno == null ? null : surveySerno.trim();
	}
	
    /**
     * @return SurveySerno
     */	
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param bizSerno
	 */
	public void setBizSerno(String bizSerno) {
		this.bizSerno = bizSerno == null ? null : bizSerno.trim();
	}
	
    /**
     * @return BizSerno
     */	
	public String getBizSerno() {
		return this.bizSerno;
	}
	
	/**
	 * @param prdName
	 */
	public void setPrdName(String prdName) {
		this.prdName = prdName == null ? null : prdName.trim();
	}
	
    /**
     * @return PrdName
     */	
	public String getPrdName() {
		return this.prdName;
	}
	
	/**
	 * @param prdType
	 */
	public void setPrdType(String prdType) {
		this.prdType = prdType == null ? null : prdType.trim();
	}
	
    /**
     * @return PrdType
     */	
	public String getPrdType() {
		return this.prdType;
	}
	
	/**
	 * @param prdId
	 */
	public void setPrdId(String prdId) {
		this.prdId = prdId == null ? null : prdId.trim();
	}
	
    /**
     * @return PrdId
     */	
	public String getPrdId() {
		return this.prdId;
	}
	
	/**
	 * @param surveyType
	 */
	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType == null ? null : surveyType.trim();
	}
	
    /**
     * @return SurveyType
     */	
	public String getSurveyType() {
		return this.surveyType;
	}
	
	/**
	 * @param cusId
	 */
	public void setCusId(String cusId) {
		this.cusId = cusId == null ? null : cusId.trim();
	}
	
    /**
     * @return CusId
     */	
	public String getCusId() {
		return this.cusId;
	}
	
	/**
	 * @param cusName
	 */
	public void setCusName(String cusName) {
		this.cusName = cusName == null ? null : cusName.trim();
	}
	
    /**
     * @return CusName
     */	
	public String getCusName() {
		return this.cusName;
	}
	
	/**
	 * @param certType
	 */
	public void setCertType(String certType) {
		this.certType = certType == null ? null : certType.trim();
	}
	
    /**
     * @return CertType
     */	
	public String getCertType() {
		return this.certType;
	}
	
	/**
	 * @param certCode
	 */
	public void setCertCode(String certCode) {
		this.certCode = certCode == null ? null : certCode.trim();
	}
	
    /**
     * @return CertCode
     */	
	public String getCertCode() {
		return this.certCode;
	}
	
	/**
	 * @param appAmt
	 */
	public void setAppAmt(java.math.BigDecimal appAmt) {
		this.appAmt = appAmt;
	}
	
    /**
     * @return AppAmt
     */	
	public java.math.BigDecimal getAppAmt() {
		return this.appAmt;
	}
	
	/**
	 * @param approveStatus
	 */
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus == null ? null : approveStatus.trim();
	}
	
    /**
     * @return ApproveStatus
     */	
	public String getApproveStatus() {
		return this.approveStatus;
	}
	
	/**
	 * @param dataSource
	 */
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource == null ? null : dataSource.trim();
	}
	
    /**
     * @return DataSource
     */	
	public String getDataSource() {
		return this.dataSource;
	}
	
	/**
	 * @param intoTime
	 */
	public void setIntoTime(String intoTime) {
		this.intoTime = intoTime == null ? null : intoTime.trim();
	}
	
    /**
     * @return IntoTime
     */	
	public String getIntoTime() {
		return this.intoTime;
	}
	
	/**
	 * @param isAutodivis
	 */
	public void setIsAutodivis(String isAutodivis) {
		this.isAutodivis = isAutodivis == null ? null : isAutodivis.trim();
	}
	
    /**
     * @return IsAutodivis
     */	
	public String getIsAutodivis() {
		return this.isAutodivis;
	}
	
	/**
	 * @param isStopOffline
	 */
	public void setIsStopOffline(String isStopOffline) {
		this.isStopOffline = isStopOffline == null ? null : isStopOffline.trim();
	}
	
    /**
     * @return IsStopOffline
     */	
	public String getIsStopOffline() {
		return this.isStopOffline;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType == null ? null : oprType.trim();
	}
	
    /**
     * @return OprType
     */	
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param marId
	 */
	public void setMarId(String marId) {
		this.marId = marId == null ? null : marId.trim();
	}
	
    /**
     * @return MarId
     */	
	public String getMarId() {
		return this.marId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId == null ? null : managerBrId.trim();
	}
	
    /**
     * @return ManagerBrId
     */	
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId == null ? null : managerId.trim();
	}
	
    /**
     * @return ManagerId
     */	
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId == null ? null : inputId.trim();
	}
	
    /**
     * @return InputId
     */	
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId == null ? null : inputBrId.trim();
	}
	
    /**
     * @return InputBrId
     */	
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate == null ? null : inputDate.trim();
	}
	
    /**
     * @return InputDate
     */	
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId == null ? null : updId.trim();
	}
	
    /**
     * @return UpdId
     */	
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId == null ? null : updBrId.trim();
	}
	
    /**
     * @return UpdBrId
     */	
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate == null ? null : updDate.trim();
	}
	
    /**
     * @return UpdDate
     */	
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}
	
	/**
	 * @param marketingId
	 */
	public void setMarketingId(String marketingId) {
		this.marketingId = marketingId == null ? null : marketingId.trim();
	}
	
    /**
     * @return MarketingId
     */	
	public String getMarketingId() {
		return this.marketingId;
	}


}