package cn.com.yusys.yusp.web.rest;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.domain.HomePageDto;
import cn.com.yusys.yusp.dto.CfgUserOftUseFuncClientDto;
import cn.com.yusys.yusp.service.CmisHomePageService;
import cn.com.yusys.yusp.service.ICmisCfgClientService;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0.0
 * @项目名称: cmis-data模块
 * @类名称: CmisHomePageResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: yangx
 * @创建时间: 2020-11-14 14:41:39
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@RestController
@RequestMapping("/api/cmishomepage")
public class CmisHomePageResource {
    private static final Logger log = LoggerFactory.getLogger(CmisHomePageResource.class);

    @Autowired
    private CmisHomePageService cmisHomePageService;
    @Autowired
    private ICmisCfgClientService iCmisCfgClientService;

    @Value("${application.ypxt.url}")
    private String ypxt; // 押品管理系统
    @Value("${application.irs.url}")
    private String irs;// 非零内评系统
    @Value("${application.fxyjxt.url}")
    private String fxyjxt;// 风险预警系统
    @Value("${application.znsp.url}")
    private String znsp;// 智能审批系统
    @Value("${application.jxxt.url}")
    private String jxxt;// 绩效系统
    @Value("${application.bdcdj.url}")
    private String bdcdj;// 不动产登记
    @Value("${application.xwdxt.url}")
    private String xwdxt;// 新微贷系统

    /**
     * 待办任务
     *
     * @param queryModel
     * @return
     */
    @PostMapping("/todo")
    protected ResultDto<List<HomePageDto>> todo(@RequestBody QueryModel queryModel) {
        log.info("待办任务请求信息：【{}】", JSONObject.toJSONString(queryModel));
        List<HomePageDto> homePageDtos = cmisHomePageService.todo(queryModel);
        log.info("待办任务响应信息：【{}】", JSONObject.toJSONString(homePageDtos));
        return new ResultDto<List<HomePageDto>>(homePageDtos);
    }

    /**
     * 业务提醒
     *
     * @param queryModel
     * @return
     */
    @PostMapping("/busremind")
    protected ResultDto<List<HomePageDto>> busRemind(@RequestBody QueryModel queryModel) {
        log.info("业务提醒请求信息：【{}】", JSONObject.toJSONString(queryModel));
        List<HomePageDto> homePageDtos = cmisHomePageService.busRemind(queryModel);
        log.info("业务提醒响应信息：【{}】", JSONObject.toJSONString(homePageDtos));
        return new ResultDto<List<HomePageDto>>(homePageDtos);
    }

    /**
     * 权证借出详情界面查询
     *
     * @param queryModel
     * @return
     */
    @PostMapping("/warrantLoan")
    protected ResultDto<HomePageDto> warrantLoan(@RequestBody Map map) {
        List<HomePageDto> ls = new ArrayList<HomePageDto>();
        cmisHomePageService.warrantLoan(map, ls);
        HomePageDto hp = new HomePageDto();
        if (!CollectionUtils.isEmpty(ls) || ls.size() > 0) {
            hp = ls.get(0);
        }
        return new ResultDto<HomePageDto>(hp);
    }

    /**
     * 业务统计
     *
     * @param queryModel
     * @return
     */
    @PostMapping("/busstatistics")
    protected ResultDto<List<HomePageDto>> busStatistics(@RequestBody QueryModel queryModel) {
        log.info("业务统计请求信息：【{}】", JSONObject.toJSONString(queryModel));
        List<HomePageDto> homePageDtos = cmisHomePageService.busStatistics(queryModel);
        log.info("业务统计响应信息：【{}】", JSONObject.toJSONString(homePageDtos));
        return new ResultDto<List<HomePageDto>>(homePageDtos);
    }

    /**
     * 新增用户常用功能
     *
     * @param cfgUserOftUseFuncClientDto
     * @return
     */
    @PostMapping(value = "/insertUserOftUseFunc")
    protected ResultDto<Integer> insertUserOftUseFunc(@RequestBody CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto) {
        return iCmisCfgClientService.insertUserOftUseFunc(cfgUserOftUseFuncClientDto);
    }

    /**
     * 删除用户常用功能
     *
     * @param cfgUserOftUseFuncClientDto
     * @return
     */
    @PostMapping(value = "/deleteUserOftUseFunc")
    protected ResultDto<Integer> deleteUserOftUseFunc(@RequestBody CfgUserOftUseFuncClientDto cfgUserOftUseFuncClientDto) {
        return new ResultDto<Integer>(iCmisCfgClientService.deleteUserOftUseFunc(cfgUserOftUseFuncClientDto));
    }

    /**
     * 获取用户常用功能
     *
     * @param loginCode
     * @return
     */
    @PostMapping(value = "/getUserOftUseFunc/{loginCode}")
    protected ResultDto<List<CfgUserOftUseFuncClientDto>> getUserOftUseFunc(@PathVariable("loginCode") String loginCode) {
        List<CfgUserOftUseFuncClientDto> list = iCmisCfgClientService.getUserOftUseFunc(loginCode);
        return new ResultDto<List<CfgUserOftUseFuncClientDto>>(iCmisCfgClientService.getUserOftUseFunc(loginCode));
    }

    /**
     * 获取各系统对应的URL
     *
     * @param
     * @return
     */
    @PostMapping("/getprefixurl/{systemId}")
    protected ResultDto<String> getPrefixUrl(@PathVariable("systemId") String systemId) {
        String prefixUrl = null;
        if (Objects.equals(systemId, "ypxt")) { // 押品管理系统
            log.info("押品管理系统URL为:[{}]", ypxt);
            prefixUrl = ypxt;
        } else if (Objects.equals(systemId, "irs")) {// 非零内评系统
            log.info("非零内评系统URL为:[{}]", irs);
            prefixUrl = irs;
        } else if (Objects.equals(systemId, "fxyjxt")) { // 风险预警系统
            log.info("风险预警系统URL为:[{}]", fxyjxt);
            prefixUrl = fxyjxt;
        } else if (Objects.equals(systemId, "znsp")) { // 智能审批系统
            log.info("智能审批系统URL为:[{}]", znsp);
            prefixUrl = znsp;
        } else if (Objects.equals(systemId, "jxxt")) { // 绩效系统
            log.info("绩效系统URL为:[{}]", jxxt);
            prefixUrl = jxxt;
        } else if (Objects.equals(systemId, "bdcdj")) {  // 不动产登记
            log.info("不动产登记URL为:[{}]", bdcdj);
            prefixUrl = bdcdj;
        } else if (Objects.equals(systemId, "xwdxt")) {   // 新微贷系统
            log.info("新微贷系统URL为:[{}]", xwdxt);
            prefixUrl = xwdxt;
        }
        return new ResultDto<String>(prefixUrl);
    }
}

