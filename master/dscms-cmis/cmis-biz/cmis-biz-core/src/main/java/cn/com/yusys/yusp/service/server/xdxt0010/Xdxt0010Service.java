package cn.com.yusys.yusp.service.server.xdxt0010;

import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.UserAndOrgInfoReqDto;
import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.req.Xdxt0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.resp.Xdxt0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.AreaUserMapper;
import cn.com.yusys.yusp.service.client.common.CommonService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

//import cn.com.yusys.yusp.dto.UserAndOrgInfoReqDto;
//import cn.com.yusys.yusp.dto.UserAndOrgInfoRespDto;

/**
 * 业务逻辑类:根据分中心负责人工号查询客户经理名单，包括工号、姓名
 *
 * @author xuchao
 * @version 1.0
 */
@Service
public class Xdxt0010Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0010Service.class);

    @Resource
    private AreaUserMapper areaUserMapper;

    @Resource
    private CommonService commonService;
    /**
     * 根据分中心负责人工号查询客户经理名单，包括工号、姓名
     * @param xdxt0010DataReqDto
     * @return
     */
    @Transactional
    public Xdxt0010DataRespDto getXdxt0010(Xdxt0010DataReqDto xdxt0010DataReqDto){
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010DataReqDto));
        Xdxt0010DataRespDto xdxt0010DataRespDto = new Xdxt0010DataRespDto();
        try {
            List<cn.com.yusys.yusp.dto.server.xdxt0010.resp.List> lists = new ArrayList<>();
            //分中心负责人工号
            String userNo = xdxt0010DataReqDto.getDeptChiefId();
            //根据用户号查询机构号列表
            logger.info("************XDXT0010*第一步根据用户号【{}】查询机构号列表开始", userNo);
            List<String> orgIds = areaUserMapper.getOrgIdsByUserNo(userNo);
            logger.info("************XDXT0010*第一步根据用户号【{}】查询机构号列表结束,机构号列表信息【{}】", userNo, orgIds);
            UserAndOrgInfoReqDto userAndOrgInfoReqDto = new UserAndOrgInfoReqDto();
            userAndOrgInfoReqDto.setOrgIds(orgIds);
            //页码
            userAndOrgInfoReqDto.setPageNum(xdxt0010DataReqDto.getStartPageNum());
            //条数
            userAndOrgInfoReqDto.setPageSize(xdxt0010DataReqDto.getPageSize());
            //调用oca接口查询信息
            logger.info("************XDXT0010*第二步根据参数【{}】调用oca接口查询信息开始", JSON.toJSONString(userAndOrgInfoReqDto));
            List<UserAndOrgInfoRespDto> userAndOrgInfoRespDtos = commonService.getUserAndOrgInfo(userAndOrgInfoReqDto);
            logger.info("************XDXT0010*第二步根据参数【{}】调用oca接口查询信息结束,返回信息【{}】", JSON.toJSONString(userAndOrgInfoReqDto), JSON.toJSONString(userAndOrgInfoRespDtos));
            userAndOrgInfoRespDtos = JSON.parseObject(JSON.toJSONString(userAndOrgInfoRespDtos), new TypeReference<List<UserAndOrgInfoRespDto>>(){});
            if(CollectionUtils.nonEmpty(userAndOrgInfoRespDtos)){
                lists = userAndOrgInfoRespDtos.stream().map(userAndOrgInfoRespDto->{
                    cn.com.yusys.yusp.dto.server.xdxt0010.resp.List list1 = new cn.com.yusys.yusp.dto.server.xdxt0010.resp.List();
                    list1.setManagerId(userAndOrgInfoRespDto.getManagerId());
                    list1.setManagerName(userAndOrgInfoRespDto.getManagerName());
                    list1.setOrgName(userAndOrgInfoRespDto.getOrgName());
                    list1.setOrgNo(userAndOrgInfoRespDto.getOrgNo());
                    return list1;
                }).collect(Collectors.toList());
            }
            xdxt0010DataRespDto.setList(lists);
        } catch (Exception e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, e.getMessage());
        }
        //通过机构号列表 分页信息查询机构和用户信息
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010DataRespDto));
        return xdxt0010DataRespDto;
    }

}
