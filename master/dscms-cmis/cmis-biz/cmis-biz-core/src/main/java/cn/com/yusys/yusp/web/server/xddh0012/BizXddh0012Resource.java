package cn.com.yusys.yusp.web.server.xddh0012;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddh0012.req.Xddh0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0012.resp.Xddh0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * 接口处理类:推送优享贷预警信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0011:推送优享贷预警信息")
@RestController
@RequestMapping("/api/bizdh4bsp")
public class BizXddh0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXddh0012Resource.class);

    /**
     * 交易码：xddh0012
     * 交易描述：推送优享贷预警信息
     *
     * @param xddh0012DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送优享贷预警信息")
    @PostMapping("/xddh0012")
    protected @ResponseBody
    ResultDto<Xddh0012DataRespDto> xddh0012(@Validated @RequestBody Xddh0012DataReqDto xddh0012DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, JSON.toJSONString(xddh0012DataReqDto));
        Xddh0012DataRespDto xddh0012DataRespDto = new Xddh0012DataRespDto();// 响应Dto:推送优享贷预警信息
        ResultDto<Xddh0012DataRespDto> xddh0012DataResultDto = new ResultDto<>();
        String yw_date = xddh0012DataReqDto.getYw_date();//任务日期
        BigDecimal dqrw_num = xddh0012DataReqDto.getDqrw_num();//任务数量
        String doc_name = xddh0012DataReqDto.getDoc_name();//文件名称
        try {
            // 从xddh0012DataReqDto获取业务值进行业务逻辑处理
            // TODO 调用XXXXXService层开始

            // TODO 调用XXXXXService层结束
            // TODO 封装xddh0012DataRespDto对象开始
            xddh0012DataRespDto.setOpFlag(StringUtils.EMPTY);// 操作成功标志位
            xddh0012DataRespDto.setOpMsg(StringUtils.EMPTY);// 描述信息
            // TODO 封装xddh0012DataRespDto对象结束
            // 封装xddh0012DataResultDto中正确的返回码和返回信息
            xddh0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddh0012DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, e.getMessage());
            // 封装xddh0012DataResultDto中异常返回码和返回信息
            xddh0012DataResultDto.setCode(EpbEnum.EPB099999.key);
            xddh0012DataResultDto.setMessage(EpbEnum.EPB099999.value);

        }
        // 封装xddh0012DataRespDto到xddh0012DataResultDto中
        xddh0012DataResultDto.setData(xddh0012DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, JSON.toJSONString(xddh0012DataResultDto));
        return xddh0012DataResultDto;
    }
}
