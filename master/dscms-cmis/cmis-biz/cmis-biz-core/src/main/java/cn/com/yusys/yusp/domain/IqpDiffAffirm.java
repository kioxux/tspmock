/*
 * 代码生成器自动生成的
 * Since 2008 - 2020
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;

import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: IqpDiffAffirm
 * @类描述: iqp_diff_affirm数据实体类
 * @功能描述: 
 * @创建人: Administrator
 * @创建时间: 2020-12-22 09:17:08
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "iqp_diff_affirm")
public class IqpDiffAffirm extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 业务申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "IQP_SERNO")
	private String iqpSerno;
	
	/** 借款人户籍类型
STD_ZB_APPLYER_REGIST_KIND **/
	@Column(name = "APLYER_REGIST_KIND", unique = false, nullable = true, length = 5)
	private String aplyerRegistKind;
	
	/** 目前家庭已有房屋套数(套) STD_ZB_HAS_HOUSE_NUM **/
	@Column(name = "HAS_HOUSE", unique = false, nullable = true, length = 5)
	private String hasHouse;
	
	/** 家庭已有商贷记录  STD_ZB_HAS_LOAN_RECORD **/
	@Column(name = "HAS_C_LOAN", unique = false, nullable = true, length = 5)
	private String hasCLoan;
	
	/** 本笔贷款执行政策 STD_ZB_CURR_POLICY_TYPE **/
	@Column(name = "CURR_LOAN_POLICY", unique = false, nullable = true, length = 5)
	private String currLoanPolicy;
	
	/** 所购房屋类型 STD_ZB_BUY_KIND_TYPE **/
	@Column(name = "BUY_HOUSE_KIND", unique = false, nullable = true, length = 5)
	private String buyHouseKind;
	
	/** 本地房产情况 **/
	@Column(name = "LOCAL_HOUSE", unique = false, nullable = true, length = 40)
	private String localHouse;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型  STD_ZB_OPR_TYPE **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	
	/**
	 * @param iqpSerno
	 */
	public void setIqpSerno(String iqpSerno) {
		this.iqpSerno = iqpSerno;
	}
	
    /**
     * @return iqpSerno
     */
	public String getIqpSerno() {
		return this.iqpSerno;
	}
	
	/**
	 * @param aplyerRegistKind
	 */
	public void setAplyerRegistKind(String aplyerRegistKind) {
		this.aplyerRegistKind = aplyerRegistKind;
	}
	
    /**
     * @return aplyerRegistKind
     */
	public String getAplyerRegistKind() {
		return this.aplyerRegistKind;
	}
	
	/**
	 * @param hasHouse
	 */
	public void setHasHouse(String hasHouse) {
		this.hasHouse = hasHouse;
	}
	
    /**
     * @return hasHouse
     */
	public String getHasHouse() {
		return this.hasHouse;
	}
	
	/**
	 * @param hasCLoan
	 */
	public void setHasCLoan(String hasCLoan) {
		this.hasCLoan = hasCLoan;
	}
	
    /**
     * @return hasCLoan
     */
	public String getHasCLoan() {
		return this.hasCLoan;
	}
	
	/**
	 * @param currLoanPolicy
	 */
	public void setCurrLoanPolicy(String currLoanPolicy) {
		this.currLoanPolicy = currLoanPolicy;
	}
	
    /**
     * @return currLoanPolicy
     */
	public String getCurrLoanPolicy() {
		return this.currLoanPolicy;
	}
	
	/**
	 * @param buyHouseKind
	 */
	public void setBuyHouseKind(String buyHouseKind) {
		this.buyHouseKind = buyHouseKind;
	}
	
    /**
     * @return buyHouseKind
     */
	public String getBuyHouseKind() {
		return this.buyHouseKind;
	}
	
	/**
	 * @param localHouse
	 */
	public void setLocalHouse(String localHouse) {
		this.localHouse = localHouse;
	}
	
    /**
     * @return localHouse
     */
	public String getLocalHouse() {
		return this.localHouse;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}


}