package cn.com.yusys.yusp.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptSpdAnysJyxwydRentIncome
 * @类描述: rpt_spd_anys_jyxwyd_rent_income数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-10-12 21:53:30
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
public class RptSpdAnysJyxwydRentIncomeDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	private String pkId;
	/** 流水号 **/
	private String serno;
	
	/** 承租人 **/
	private String lessee;
	
	/** 现有租赁合同期限 **/
	private Integer termOfExistingLeaseContract;

	/** 年份 **/
	private String year;

	/** 金额 **/
	private BigDecimal amt;

	/** 创建时间 **/
	private java.util.Date createTime;
	
	/** 修改时间 **/
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId == null ? null : pkId.trim();
	}
	
    /**
     * @return PkId
     */	
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno == null ? null : serno.trim();
	}
	
    /**
     * @return Serno
     */	
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param lessee
	 */
	public void setLessee(String lessee) {
		this.lessee = lessee == null ? null : lessee.trim();
	}
	
    /**
     * @return Lessee
     */	
	public String getLessee() {
		return this.lessee;
	}
	
	/**
	 * @param termOfExistingLeaseContract
	 */
	public void setTermOfExistingLeaseContract(Integer termOfExistingLeaseContract) {
		this.termOfExistingLeaseContract = termOfExistingLeaseContract;
	}
	
    /**
     * @return TermOfExistingLeaseContract
     */	
	public Integer getTermOfExistingLeaseContract() {
		return this.termOfExistingLeaseContract;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return CreateTime
     */	
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return UpdateTime
     */	
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
}