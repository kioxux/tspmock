/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.web.rest;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.LmtGrpAppSubDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.domain.LmtAppSubPrd;
import cn.com.yusys.yusp.service.LmtAppSubPrdService;

/**
 * @version 1.0.0
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtAppSubPrdResource
 * @类描述: #资源类
 * @功能描述:
 * @创建人: DELL
 * @创建时间: 2021-04-08 19:23:31
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
@Api(tags = "授信申请分项品种")
@RestController
@RequestMapping("/api/lmtappsubprd")
public class LmtAppSubPrdResource {
    @Autowired
    private LmtAppSubPrdService lmtAppSubPrdService;

    /**
     * 全表查询.
     *
     * @return
     */
    @GetMapping("/query/all")
    protected ResultDto<List<LmtAppSubPrd>> query() {
        QueryModel queryModel = new QueryModel();
        List<LmtAppSubPrd> list = lmtAppSubPrdService.selectAll(queryModel);
        return new ResultDto<List<LmtAppSubPrd>>(list);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:index
     * @函数描述:查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/")
    protected ResultDto<List<LmtAppSubPrd>> index(QueryModel queryModel) {
        List<LmtAppSubPrd> list = lmtAppSubPrdService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppSubPrd>>(list);
    }

    /**
     * @函数名称:show
     * @函数描述:查询单个对象，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @GetMapping("/{pkId}")
    protected ResultDto<LmtAppSubPrd> show(@PathVariable("pkId") String pkId) {
        LmtAppSubPrd lmtAppSubPrd = lmtAppSubPrdService.selectByPrimaryKey(pkId);
        return new ResultDto<LmtAppSubPrd>(lmtAppSubPrd);
    }

    /**
     * @函数名称:create
     * @函数描述:实体类创建，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/")
    protected ResultDto<LmtAppSubPrd> create(@RequestBody LmtAppSubPrd lmtAppSubPrd) throws URISyntaxException {
        lmtAppSubPrdService.insert(lmtAppSubPrd);
        return new ResultDto<LmtAppSubPrd>(lmtAppSubPrd);
    }

    /**
     * @函数名称:update
     * @函数描述:对象修改，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/update")
    protected ResultDto<Integer> update(@RequestBody LmtAppSubPrd lmtAppSubPrd) throws URISyntaxException {
        int result = lmtAppSubPrdService.update(lmtAppSubPrd);
        return new ResultDto<Integer>(result);
    }


    /**
     * @函数名称:delete
     * @函数描述:单个对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/delete/{pkId}")
    protected ResultDto<Integer> delete(@PathVariable("pkId") String pkId) {
        int result = lmtAppSubPrdService.deleteByPrimaryKey(pkId);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:batchdelete
     * @函数描述:批量对象删除，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/batchdelete/{ids}")
    protected ResultDto<Integer> deletes(@PathVariable String ids) {
        int result = lmtAppSubPrdService.deleteByIds(ids);
        return new ResultDto<Integer>(result);
    }

    /**
     * @函数名称:addlmtappsubprd
     * @函数描述:新增适用授信产品
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/addlmtappsubprd")
    protected ResultDto<Map> saveLmtAppSubPrd(@RequestBody LmtAppSubPrd lmtAppSubPrd) throws URISyntaxException {
        Map result = lmtAppSubPrdService.saveLmtAppSubPrd(lmtAppSubPrd);
        return new ResultDto<Map>(result);
    }

    /**
     * @函数名称:deleteLmtAppSubPrd
     * @函数描述:删除适用授信产品
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/deletelmtappsubprd")
    protected ResultDto<Map> deleteLmtAppSubPrd(@RequestBody LmtAppSubPrd lmtAppSubPrd) throws URISyntaxException {
        Map result = lmtAppSubPrdService.deleteByPkId(lmtAppSubPrd);
        return new ResultDto<>(result);
    }

    /**
     * @函数名称:updateLmtAppSubPrd
     * @函数描述:删除适用授信产品
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/updatelmtappsubprd")
    protected ResultDto<Map> updateLmtAppSubPrd(@RequestBody LmtAppSubPrd lmtAppSubPrd) throws URISyntaxException {
        Map result = lmtAppSubPrdService.updateLmtAppSubPrd(lmtAppSubPrd);
        return new ResultDto<>(result);
    }

    /**
     * @param queryModel 分页查询类
     * @函数名称:selectByModel
     * @函数描述:根据入参查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbymodel")
    protected ResultDto<List<LmtAppSubPrd>> selectByModel(@RequestBody QueryModel queryModel) {
        List<LmtAppSubPrd> list = lmtAppSubPrdService.selectByModel(queryModel);
        return new ResultDto<List<LmtAppSubPrd>>(list);
    }

    /**
     * @param subSerno 分页查询类
     * @函数名称:selectBySubSerno
     * @函数描述:根据入参查询对象列表，公共API接口
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbysubserno")
    protected ResultDto<List<LmtAppSubPrd>> selectBySubSerno(@RequestBody String subSerno) {
        List<LmtAppSubPrd> list = lmtAppSubPrdService.selectBySubSerno(subSerno.replaceAll("\"", ""));
        return new ResultDto<List<LmtAppSubPrd>>(list);
    }

    /**
     * @param subPrdSerno 分页查询类
     * @函数名称:selectBySubPrdSerno
     * @函数描述:根据授信适用产品流水号获取对象
     * @参数与返回说明:
     * @算法描述:
     */
    @PostMapping("/selectbysubprdserno")
    protected ResultDto<LmtAppSubPrd> selectBySubPrdSerno(@RequestBody String subPrdSerno) {
        LmtAppSubPrd lmtAppSubPrd = lmtAppSubPrdService.selectBySubPrdSerno(subPrdSerno);
        return new ResultDto<>(lmtAppSubPrd);
    }


    /**
     * @函数名称: queryLmtGrpAppSubSumByGrpSerno
     * @函数描述: 根据集团授信申请流水号获取集团分项明细
     * @参数与返回说明:
     * @算法描述:
     * @创建人: mashun
     * @创建时间: 2021-05-17 20:34:31
     * @修改记录: 修改时间    修改人员    修改原因
     */
    @ApiOperation("根据集团授信申请流水号获取集团分项明细")
    @PostMapping("/querylmtgrpappsubsumbygrpserno")
    protected ResultDto<List<LmtGrpAppSubDto>> queryLmtGrpAppSubSumByGrpSerno(@RequestBody String grpSerno) {
        return lmtAppSubPrdService.queryLmtGrpAppSubSumByGrpSerno(grpSerno);
    }

    /**
     * 查询本次授信信息
     *
     * @param model
     * @return
     */
    @PostMapping("/selectThisLmt")
    protected ResultDto<List<Map>> selectThisLmt(@RequestBody QueryModel model) {
        return new ResultDto<List<Map>>(lmtAppSubPrdService.selectThisLmt(model));
    }

    /**
     * 查看一般授信额度或委托贷款授信额度
     *
     * @param model
     * @return
     */
    @PostMapping("/getNormalOrBailLmt")
    protected ResultDto<List<Map>> getNormalOrBailLmt(@RequestBody QueryModel model) {
        return new ResultDto<List<Map>>(lmtAppSubPrdService.getNormalOrBailLmt(model));
    }

    /**
     * 查看集团一般授信额度或委托贷款授信额度
     *
     * @param model
     * @return
     */
    @PostMapping("/getGrpNormalOrBailLmt")
    protected ResultDto<List<Map>> getGrpNormalOrBailLmt(@RequestBody QueryModel model) {
        return new ResultDto<List<Map>>(lmtAppSubPrdService.getGrpNormalOrBailLmt(model));
    }

    /**
     * 根据适用品种编号获取可选产品属性
     *
     * @param params
     * @return
     */
    @PostMapping("/getselectpropbyprdno")
    protected ResultDto<Map> getSelectPropByPrdNo(@RequestBody Map<String,String> params) {
        return new ResultDto<Map>(lmtAppSubPrdService.getSelectPropByPrdNo(params));
    }

    @PostMapping("/selectSubPrdBySerno")
    protected ResultDto<List<LmtAppSubPrd>> selectSubPrdBySerno(@RequestBody String serno){
        return new ResultDto<List<LmtAppSubPrd>>(lmtAppSubPrdService.selectSubPrdBySerno(serno));
    }
    @PostMapping("/getGuarModeDetail")
    protected ResultDto<Map> getGuarModeDetail(@RequestBody String serno){
        return new ResultDto<Map>(lmtAppSubPrdService.getGuarModeDetail(serno));
    }

    /**
     * 根据申请流水号查询品种明细
     * @param map
     * @return
     */
    @PostMapping("/selectAppSubPrdBySerno")
    protected ResultDto<List<LmtAppSubPrd>> selectSubPrdBySerno(@RequestBody Map map){
        return new ResultDto<List<LmtAppSubPrd>>(lmtAppSubPrdService.selectSubPrdBySerno((String) map.get("serno")));
    }


    /**
     * 根据申请流水号查询品种明细
     * @param map
     * @return
     */
    @PostMapping("/test")
    protected ResultDto<List<LmtAppSubPrd>> test(@RequestBody Map map){
        return new ResultDto<List<LmtAppSubPrd>>(lmtAppSubPrdService.selectLmtAppSubPrdByParams(map));
    }
}
