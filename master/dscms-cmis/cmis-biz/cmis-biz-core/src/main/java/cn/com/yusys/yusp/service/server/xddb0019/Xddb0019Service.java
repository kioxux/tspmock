package cn.com.yusys.yusp.service.server.xddb0019;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constant.CommonConstance;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.domain.GuarBaseInfo;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhReqDto;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhRespDto;
import cn.com.yusys.yusp.dto.server.xddb0019.req.Xddb0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0019.resp.Xddb0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.repository.mapper.GuarWarrantInfoMapper;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import cn.com.yusys.yusp.service.GuarBaseInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

import javax.annotation.Resource;

@Service
public class Xddb0019Service {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0019Service.class);
    @Resource
    private GuarWarrantInfoMapper guarWarrantInfoMapper;
    @Autowired
    private Dscms2YpxtClientService dscms2YpxtClientService;
    @Autowired
    private GuarBaseInfoService guarBaseInfoService;

    /**
     * 押品状态同步
     *
     * @param xdkh0019DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xddb0019DataRespDto xddb0019(Xddb0019DataReqDto xdkh0019DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value);
        Xddb0019DataRespDto xddb0019DataRespDto = new Xddb0019DataRespDto();
        String drftNo = xdkh0019DataReqDto.getDrftNo();
        String bill_no = "";//押品编号
        try {
            if (StringUtil.isNotEmpty(drftNo)) {
                CxypbhReqDto cxypbhReqDto = new CxypbhReqDto();
                cxypbhReqDto.setBill_no(drftNo);
                logger.info("*******************通过票据号码查询押品编号****************************");
                ResultDto<CxypbhRespDto> cxypbh = dscms2YpxtClientService.cxypbh(cxypbhReqDto);
                logger.info("***********************票据编号:" + cxypbh + "****************************");
                bill_no = cxypbh.getData().getGuar_no();

                String status = guarWarrantInfoMapper.xddb0019(bill_no);//押品状态:未入库、已出库、入库
                if (StringUtil.isNotEmpty(status)) {
                    if ("08".equals(status) || "09".equals(status) || "10".equals(status)) {//已出库
                        xddb0019DataRespDto.setGuarStatus("已出库");
                    } else {
                        xddb0019DataRespDto.setGuarStatus("未出库");
                    }
                } else {
                    GuarBaseInfo guarBaseInfo = guarBaseInfoService.getGuarBaseInfoByGuarNo(bill_no);
                    //核心担保编号
                    String coreGuarantyNo = guarBaseInfo.getCoreGuarantyNo();

                    if (!StringUtils.isEmpty(coreGuarantyNo)){
                        status = guarWarrantInfoMapper.xddb0019(coreGuarantyNo);//押品状态:未入库、已出库、入库
                        if (StringUtil.isNotEmpty(status)) {
                            if ("08".equals(status) || "09".equals(status) || "10".equals(status)) {//已出库
                                xddb0019DataRespDto.setGuarStatus("已出库");
                            } else {
                                xddb0019DataRespDto.setGuarStatus("未出库");
                            }
                        } else {
                            throw new BizException(null,EcbEnum.E_PVP_UPDATE_GUARNULL.key,null, EcbEnum.E_PVP_UPDATE_GUARNULL.value());
                        }
                    }else{
                        throw new BizException(null,EcbEnum.E_PVP_UPDATE_GUARNULL.key, null,EcbEnum.E_PVP_UPDATE_GUARNULL.value());
                    }
                }
            } else {
                logger.info("**************************不存在对应票号结束*END*************************");
                //请求参数不存在,返回空值
                logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value);
                throw new BizException(null,EcbEnum.ECB010001.key,null, EcbEnum.ECB010001.value());
            }

            xddb0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_S);
            xddb0019DataRespDto.setOpMsg(CommonConstance.OP_MSG_S);
        } catch (BizException e) {
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
            xddb0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xddb0019DataRespDto.setOpMsg(e.getMessage());
        } catch (Exception e) {
            xddb0019DataRespDto.setOpFlag(CommonConstance.OP_FLAG_F);
            xddb0019DataRespDto.setOpMsg(e.getMessage());
            logger.error(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
        }
        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value);
        return xddb0019DataRespDto;
    }
}
