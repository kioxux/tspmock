/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtSxkdPlusFksp
 * @类描述: lmt_sxkd_plus_fksp数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-10-16 10:36:19
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_sxkd_plus_fksp")
public class LmtSxkdPlusFksp extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 主键 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "PK_ID")
	private String pkId;
	
	/** 业务编号 **/
	@Column(name = "SERNO", unique = false, nullable = true, length = 40)
	private String serno;
	
	/** 审批结果 **/
	@Column(name = "APPROVE_RESULT", unique = false, nullable = true, length = 10)
	private String approveResult;
	
	/** 审批结果名称 **/
	@Column(name = "APPROVE_RESULT_NAME", unique = false, nullable = true, length = 40)
	private String approveResultName;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 创建时间 **/
	@Column(name = "RECEIVE_TIME", unique = false, nullable = true, length = 20)
	private String receiveTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param pkId
	 */
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	
    /**
     * @return pkId
     */
	public String getPkId() {
		return this.pkId;
	}
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param approveResult
	 */
	public void setApproveResult(String approveResult) {
		this.approveResult = approveResult;
	}
	
    /**
     * @return approveResult
     */
	public String getApproveResult() {
		return this.approveResult;
	}
	
	/**
	 * @param approveResultName
	 */
	public void setApproveResultName(String approveResultName) {
		this.approveResultName = approveResultName;
	}
	
    /**
     * @return approveResultName
     */
	public String getApproveResultName() {
		return this.approveResultName;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param receiveTime
	 */
	public void setReceiveTime(String receiveTime) {
		this.receiveTime = receiveTime;
	}
	
    /**
     * @return receiveTime
     */
	public String getReceiveTime() {
		return this.receiveTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}