/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: CoopPartnerAgrAccInfo
 * @类描述: coop_partner_agr_acc_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-08-08 23:47:34
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "coop_partner_agr_acc_info")
public class CoopPartnerAgrAccInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 合作协议编号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "COOP_AGR_NO")
	private String coopAgrNo;
	
	/** 合作协议流水号 **/
	@Column(name = "COOP_AGR_SERNO", unique = false, nullable = true, length = 40)
	private String coopAgrSerno;
	
	/** 合作协议金额 **/
	@Column(name = "COOP_AGR_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal coopAgrAmt;
	
	/** 合作协议起始日 **/
	@Column(name = "COOP_AGR_START_DATE", unique = false, nullable = true, length = 20)
	private String coopAgrStartDate;
	
	/** 合作协议到期日 **/
	@Column(name = "COOP_AGR_END_DATE", unique = false, nullable = true, length = 20)
	private String coopAgrEndDate;
	
	/** 合作协议签订日期 **/
	@Column(name = "COOP_AGR_SIGN_DATE", unique = false, nullable = true, length = 20)
	private String coopAgrSignDate;
	
	/** 协议状态 **/
	@Column(name = "AGR_STATUS", unique = false, nullable = true, length = 10)
	private String agrStatus;
	
	/** 合作方案编号 **/
	@Column(name = "COOP_PLAN_NO", unique = false, nullable = true, length = 60)
	private String coopPlanNo;
	
	/** 合作方案申请流水号 **/
	@Column(name = "COOP_PLAN_SERNO", unique = false, nullable = true, length = 60)
	private String coopPlanSerno;
	
	/** 合作方类型 **/
	@Column(name = "PARTNER_TYPE", unique = false, nullable = true, length = 10)
	private String partnerType;
	
	/** 合作方编号 **/
	@Column(name = "PARTNER_NO", unique = false, nullable = true, length = 60)
	private String partnerNo;
	
	/** 合作方名称 **/
	@Column(name = "PARTNER_NAME", unique = false, nullable = true, length = 120)
	private String partnerName;
	
	/** 合作类型 **/
	@Column(name = "COOP_TYPE", unique = false, nullable = true, length = 10)
	private String coopType;
	
	/** 保证金比例 **/
	@Column(name = "BAIL_PERC", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailPerc;

	/** 一般担保额度(元) **/
	@Column(name = "COMMON_GRT_LMT_AMT", unique = false, nullable = true, length = 60)
	private java.math.BigDecimal commonGrtLmtAmt;
	
	/** 保证金账户最低金额(元) **/
	@Column(name = "BAIL_ACC_LOW_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailAccLowAmt;
	
	/** 单笔最低缴存金额(元) **/
	@Column(name = "SIG_LOW_DEPOSIT_AMT", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal sigLowDepositAmt;
	
	/** 保证金透支上限(元) **/
	@Column(name = "BAIL_OVERDRAFT_MAX", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal bailOverdraftMax;
	
	/** 保证金缴存方式 **/
	@Column(name = "BAIL_DEPOSIT_MODE", unique = false, nullable = true, length = 10)
	private String bailDepositMode;
	
	/** 保证金账号 **/
	@Column(name = "BAIL_ACC_NO", unique = false, nullable = true, length = 60)
	private String bailAccNo;
	
	/** 保证金账号子序号 **/
	@Column(name = "BAIL_ACC_NO_SUB_SEQ", unique = false, nullable = true, length = 60)
	private String bailAccNoSubSeq;
	
	/** 主办人 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 主办机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最近修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最近修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最近修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 20)
	private String oprType;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param coopAgrNo
	 */
	public void setCoopAgrNo(String coopAgrNo) {
		this.coopAgrNo = coopAgrNo;
	}
	
    /**
     * @return coopAgrNo
     */
	public String getCoopAgrNo() {
		return this.coopAgrNo;
	}
	
	/**
	 * @param coopAgrSerno
	 */
	public void setCoopAgrSerno(String coopAgrSerno) {
		this.coopAgrSerno = coopAgrSerno;
	}
	
    /**
     * @return coopAgrSerno
     */
	public String getCoopAgrSerno() {
		return this.coopAgrSerno;
	}
	
	/**
	 * @param coopAgrAmt
	 */
	public void setCoopAgrAmt(java.math.BigDecimal coopAgrAmt) {
		this.coopAgrAmt = coopAgrAmt;
	}
	
    /**
     * @return coopAgrAmt
     */
	public java.math.BigDecimal getCoopAgrAmt() {
		return this.coopAgrAmt;
	}
	
	/**
	 * @param coopAgrStartDate
	 */
	public void setCoopAgrStartDate(String coopAgrStartDate) {
		this.coopAgrStartDate = coopAgrStartDate;
	}
	
    /**
     * @return coopAgrStartDate
     */
	public String getCoopAgrStartDate() {
		return this.coopAgrStartDate;
	}
	
	/**
	 * @param coopAgrEndDate
	 */
	public void setCoopAgrEndDate(String coopAgrEndDate) {
		this.coopAgrEndDate = coopAgrEndDate;
	}
	
    /**
     * @return coopAgrEndDate
     */
	public String getCoopAgrEndDate() {
		return this.coopAgrEndDate;
	}
	
	/**
	 * @param coopAgrSignDate
	 */
	public void setCoopAgrSignDate(String coopAgrSignDate) {
		this.coopAgrSignDate = coopAgrSignDate;
	}
	
    /**
     * @return coopAgrSignDate
     */
	public String getCoopAgrSignDate() {
		return this.coopAgrSignDate;
	}
	
	/**
	 * @param agrStatus
	 */
	public void setAgrStatus(String agrStatus) {
		this.agrStatus = agrStatus;
	}
	
    /**
     * @return agrStatus
     */
	public String getAgrStatus() {
		return this.agrStatus;
	}
	
	/**
	 * @param coopPlanNo
	 */
	public void setCoopPlanNo(String coopPlanNo) {
		this.coopPlanNo = coopPlanNo;
	}
	
    /**
     * @return coopPlanNo
     */
	public String getCoopPlanNo() {
		return this.coopPlanNo;
	}
	
	/**
	 * @param coopPlanSerno
	 */
	public void setCoopPlanSerno(String coopPlanSerno) {
		this.coopPlanSerno = coopPlanSerno;
	}
	
    /**
     * @return coopPlanSerno
     */
	public String getCoopPlanSerno() {
		return this.coopPlanSerno;
	}
	
	/**
	 * @param partnerType
	 */
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	
    /**
     * @return partnerType
     */
	public String getPartnerType() {
		return this.partnerType;
	}
	
	/**
	 * @param partnerNo
	 */
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	
    /**
     * @return partnerNo
     */
	public String getPartnerNo() {
		return this.partnerNo;
	}
	
	/**
	 * @param partnerName
	 */
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	
    /**
     * @return partnerName
     */
	public String getPartnerName() {
		return this.partnerName;
	}
	
	/**
	 * @param coopType
	 */
	public void setCoopType(String coopType) {
		this.coopType = coopType;
	}
	
    /**
     * @return coopType
     */
	public String getCoopType() {
		return this.coopType;
	}
	
	/**
	 * @param bailPerc
	 */
	public void setBailPerc(java.math.BigDecimal bailPerc) {
		this.bailPerc = bailPerc;
	}
	
    /**
     * @return bailPerc
     */
	public java.math.BigDecimal getBailPerc() {
		return this.bailPerc;
	}

	public void setCommonGrtLmtAmt(BigDecimal commonGrtLmtAmt) {
		this.commonGrtLmtAmt = commonGrtLmtAmt;
	}

	public BigDecimal getCommonGrtLmtAmt() {
		return commonGrtLmtAmt;
	}

	/**
	 * @param bailAccLowAmt
	 */
	public void setBailAccLowAmt(java.math.BigDecimal bailAccLowAmt) {
		this.bailAccLowAmt = bailAccLowAmt;
	}
	
    /**
     * @return bailAccLowAmt
     */
	public java.math.BigDecimal getBailAccLowAmt() {
		return this.bailAccLowAmt;
	}
	
	/**
	 * @param sigLowDepositAmt
	 */
	public void setSigLowDepositAmt(java.math.BigDecimal sigLowDepositAmt) {
		this.sigLowDepositAmt = sigLowDepositAmt;
	}
	
    /**
     * @return sigLowDepositAmt
     */
	public java.math.BigDecimal getSigLowDepositAmt() {
		return this.sigLowDepositAmt;
	}
	
	/**
	 * @param bailOverdraftMax
	 */
	public void setBailOverdraftMax(java.math.BigDecimal bailOverdraftMax) {
		this.bailOverdraftMax = bailOverdraftMax;
	}
	
    /**
     * @return bailOverdraftMax
     */
	public java.math.BigDecimal getBailOverdraftMax() {
		return this.bailOverdraftMax;
	}
	
	/**
	 * @param bailDepositMode
	 */
	public void setBailDepositMode(String bailDepositMode) {
		this.bailDepositMode = bailDepositMode;
	}
	
    /**
     * @return bailDepositMode
     */
	public String getBailDepositMode() {
		return this.bailDepositMode;
	}
	
	/**
	 * @param bailAccNo
	 */
	public void setBailAccNo(String bailAccNo) {
		this.bailAccNo = bailAccNo;
	}
	
    /**
     * @return bailAccNo
     */
	public String getBailAccNo() {
		return this.bailAccNo;
	}
	
	/**
	 * @param bailAccNoSubSeq
	 */
	public void setBailAccNoSubSeq(String bailAccNoSubSeq) {
		this.bailAccNoSubSeq = bailAccNoSubSeq;
	}
	
    /**
     * @return bailAccNoSubSeq
     */
	public String getBailAccNoSubSeq() {
		return this.bailAccNoSubSeq;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}