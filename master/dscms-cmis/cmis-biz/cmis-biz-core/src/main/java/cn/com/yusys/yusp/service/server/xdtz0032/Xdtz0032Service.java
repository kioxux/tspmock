package cn.com.yusys.yusp.service.server.xdtz0032;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0032.req.Xdtz0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0032.resp.Xdtz0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.repository.mapper.AccLoanMapper;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.util.StringUtil;

/**
 * 接口处理类:查询客户所担保的行内当前贷款逾期件数
 *
 * @author xll
 * @version 1.0
 */
@Service
public class Xdtz0032Service {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0032Service.class);
    @Autowired
    private AccLoanMapper accLoanMapper;

    /**
     * 查询客户所担保的行内当前贷款逾期件数
     *
     * @param xdtz0032DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public Xdtz0032DataRespDto queryGuaranteeOverDueFromCmis(Xdtz0032DataReqDto xdtz0032DataReqDto) throws Exception {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032DataReqDto));
        Xdtz0032DataRespDto xdtz0032DataRespDto = new Xdtz0032DataRespDto();
        String cusId = xdtz0032DataReqDto.getCusId();//获取请求参数-客户号
        try {
            int curtCnt = 0;//所担保的行内当前贷款逾期件数
            logger.info("**********Xddb0032Service查询所担保的行内当前贷款逾期件数开始*START**************");
            if (StringUtil.isNotEmpty(cusId)) {//客户调查主键押空校验
                curtCnt = accLoanMapper.queryGuaranteeOverDueFromCmis(cusId);
                xdtz0032DataRespDto.setCurtOverdueLoanCnt(String.valueOf(curtCnt));
            } else {
                //请求参数不存在
                xdtz0032DataRespDto.setCurtOverdueLoanCnt(StringUtils.EMPTY);// 所担保的行内当前贷款逾期件数
                throw new YuspException(EcbEnum.ECB010001.key, EcbEnum.ECB010001.value());
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, e.getMessage());
            throw new Exception(EpbEnum.EPB099999.value);
        }
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032DataRespDto));
        return xdtz0032DataRespDto;
    }
}
