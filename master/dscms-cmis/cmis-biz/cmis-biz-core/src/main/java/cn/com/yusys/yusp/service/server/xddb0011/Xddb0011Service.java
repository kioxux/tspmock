package cn.com.yusys.yusp.service.server.xddb0011;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xddb0011.req.Xddb0011DataReqDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.repository.mapper.CtrLoanContMapper;
import cn.com.yusys.yusp.repository.mapper.GuarBaseInfoMapper;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateClient;
import cn.com.yusys.yusp.service.AdminSmUserService;
import cn.com.yusys.yusp.service.GuarWarrantInfoService;
import cn.com.yusys.yusp.service.client.bsp.dxpt.senddx.SenddxService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

/**
 * 接口处理类:提前出库押品信息维护
 *
 * @author zhangpeng
 * @version 1.0
 */
@Service
public class Xddb0011Service {

    private static final Logger logger = LoggerFactory.getLogger(Xddb0011Service.class);

    @Autowired
    private GuarBaseInfoMapper guarBaseInfoMapper;

    @Autowired
    private GuarWarrantInfoService guarWarrantInfoService;

    @Autowired
    private SequenceTemplateClient sequenceTemplateClient;

    @Autowired
    private CtrLoanContMapper ctrLoanContMapper;

    @Autowired
    private AdminSmUserService adminSmUserService;

    @Autowired
    private SenddxService senddxService;

    /**
     * 提前出库押品信息维护
     *
     * @param xddb0011DataReqDto
     * @return
     */
    @Transactional(rollbackFor = {BizException.class, Exception.class})
    public int saveGrarWarrantOutApps(Xddb0011DataReqDto xddb0011DataReqDto) {
        logger.info(TradeLogConstants.SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, JSON.toJSONString(xddb0011DataReqDto));
        String now = LocalDate.now().toString();
        int result = 0;
//        try {
//            final List<ListGrt> list = xddb0011DataReqDto.getListGrt();
//            // 获取押品编号
//            final List<String> ypbhs = list.parallelStream().map(ListGrt::getYpbh).collect(Collectors.toList());
//            // 查询押品基本信息
//            final List<GuarBaseInfo> guarBaseInfos = Optional.ofNullable(guarBaseInfoMapper.selectByGuarNo(ypbhs)).orElse(new ArrayList<>());
//            final Map<String, GuarBaseInfo> guarBaseInfoMap = Optional.ofNullable(guarBaseInfos.parallelStream().collect(Collectors.toMap(GuarBaseInfo::getGuarNo, e -> e))).orElse(new HashMap<>());
//            // 查询押品入库信息
//            final List<GuarWarrantInfo> guarWarrantInfos = Optional.ofNullable(guarWarrantInfoService.selectByCoreGuarantyNos(ypbhs)).orElse(new ArrayList<>());
//
//            final Map<String, GuarWarrantInfo> guarWarrantInfoMap = Optional.ofNullable(guarWarrantInfos.parallelStream().collect(Collectors.toMap(GuarWarrantInfo::getCoreGuarantyNo, e -> e))).orElse(new HashMap<>());
//            // 查询押品出库信息
//            final List<GuarWarrantOutApp> guarWarrantOutApps = Optional.ofNullable(guarWarrantInfoService.selectByCoreGuarantyNos(ypbhs)).orElse(new ArrayList<>());
//            final Map<String, GuarWarrantOutApp> guarWarrantOutAppMap = guarWarrantOutApps.parallelStream().collect(Collectors.toMap(GuarWarrantOutApp::getGuarNo, e -> e));
//            List<GuarWarrantOutApp> inserts = new ArrayList<>();
//            List<String> input = new ArrayList<>();
//            for (ListGrt grt : list) {
//                String ypbh = grt.getYpbh();
//                String khth = grt.getKhth();
//                GuarBaseInfo guarBaseInfo = Optional.ofNullable(guarBaseInfoMap.get(ypbh)).orElse(new GuarBaseInfo());
//                GuarWarrantInfo guarWarrantInfo = Optional.ofNullable(guarWarrantInfoMap.get(ypbh)).orElse(new GuarWarrantInfo());
//                GuarWarrantOutApp guarWarrantOutApp = guarWarrantOutAppMap.get(ypbh);
//                if (null == guarWarrantOutApp) {
//                    if (!input.contains(khth)) {
//                        input.add(khth);
//                    }
//                    guarWarrantOutApp = new GuarWarrantOutApp();
//                    guarWarrantOutApp.setSerno(sequenceTemplateClient.getSequenceTemplate(SeqConstant.SERNO, new HashMap<>()));
//                    guarWarrantOutApp.setCoreGrtNo(guarWarrantInfo.getCoreGrtNo());
//                    guarWarrantOutApp.setGuarNo(ypbh);
//                    guarWarrantOutApp.setPldimnMemo(guarBaseInfo.getPldimnMemo());
//                    guarWarrantOutApp.setGuarType(guarBaseInfo.getGuarType());
//                    guarWarrantOutApp.setCertiAmt(guarWarrantInfo.getCertiAmt());
//                    guarWarrantOutApp.setGuarCusId(guarWarrantInfo.getGuarCusId());
//                    guarWarrantOutApp.setGuarCusName(guarWarrantInfo.getGuarCusName());
//                    guarWarrantOutApp.setFinaBrId(guarWarrantInfo.getFinaBrId());
//                    guarWarrantOutApp.setWarrantState(guarWarrantInfo.getFinaBrId());
//                    guarWarrantOutApp.setWarrantOutDate(now);
//                    guarWarrantOutApp.setHorg(guarWarrantInfo.getHorg());
//                    guarWarrantOutApp.setHuser(guarWarrantInfo.getHuser());
//                    inserts.add(guarWarrantOutApp);
//                }
//            }
//            if (!CollectionUtils.isEmpty(inserts)) {
//                result += guarWarrantOutAppMapper.insertGuarWarrantOutApps(inserts);
//            }
//            for (String cont_no : input) {
//                if (StringUtils.isNotBlank(cont_no)) {
//                    CtrLoanCont ctrLoanCont = Optional.ofNullable(ctrLoanContMapper.selectByPrimaryKey(cont_no)).orElse(new CtrLoanCont());
//                    String managerId = ctrLoanCont.getManagerId();
//                    String cusName = ctrLoanCont.getCusName();
//                    logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, managerId);
//                    ResultDto<AdminSmUserDto> resultDto = Optional.ofNullable(adminSmUserService.getByLoginCode(managerId)).orElse(new ResultDto<>());
//                    logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.key, DscmsEnum.TRADE_CODE_GETADMINBYLOGINCODE.value, JSON.toJSONString(resultDto));
//                    AdminSmUserDto adminSmUserDto = Optional.ofNullable(resultDto.getData()).orElse(new AdminSmUserDto());
//                    if (StringUtils.isNotBlank(adminSmUserDto.getUserMobilephone())) {
//                        SenddxReqDto senddxReqDto = new SenddxReqDto();
//                        SenddxReqList senddxReqList = new SenddxReqList();
//                        senddxReqList.setMobile(adminSmUserDto.getUserMobilephone());
//                        senddxReqList.setSmstxt("尊敬的:" + adminSmUserDto.getUserName() + "，您管户的 " + cusName + " 客户发起一笔物联动产贷提前出库申请，请尽快处理，您可通过信贷系统首页的物联动产贷待办事宜中进行处理");
//                        senddxReqDto.setSenddxReqList(Arrays.asList(senddxReqList));
//                        senddxReqDto.setInfopt("dx");
//                        senddxService.senddx(senddxReqDto);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, e.getMessage());
//        }

        logger.info(TradeLogConstants.SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, result);
        return result;
    }
}
