/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.service;

import java.util.List;
import java.util.Map;

import cn.com.yusys.yusp.dto.AsplTotlIncomeDto;
import cn.com.yusys.yusp.dto.InPoolAssetListDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;

import cn.com.yusys.yusp.commons.module.adapter.query.QueryModel;
import cn.com.yusys.yusp.domain.AsplTotlIncome;
import cn.com.yusys.yusp.repository.mapper.AsplTotlIncomeMapper;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: AsplTotlIncomeService
 * @类描述: #服务类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-13 14:05:50
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Service
@Transactional
public class AsplTotlIncomeService {

    @Autowired
    private AsplTotlIncomeMapper asplTotlIncomeMapper;
	
    /**
     * @方法名称: selectByPrimaryKey
     * @方法描述: 根据主键查询
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public AsplTotlIncome selectByPrimaryKey(String pkId) {
        return asplTotlIncomeMapper.selectByPrimaryKey(pkId);
    }
	
	/**
     * @方法名称: selectAll
     * @方法描述: 查询所有数据
     * @参数与返回说明: 
     * @算法描述: 无
     */

    @Transactional(readOnly=true)
    public List<AsplTotlIncome> selectAll(QueryModel model) {
        List<AsplTotlIncome> records = (List<AsplTotlIncome>) asplTotlIncomeMapper.selectByModel(model);
        return records;
    }

    /**
     * @方法名称: selectByModel
     * @方法描述: 条件查询 - 查询进行分页
     * @参数与返回说明: 
     * @算法描述: 无
     */
    
    public List<AsplTotlIncome> selectByModel(QueryModel model) {
        PageHelper.startPage(model.getPage(), model.getSize());
        List<AsplTotlIncome> list = asplTotlIncomeMapper.selectByModel(model);
        PageHelper.clearPage();
        return list;
    }	
	
    /**
     * @方法名称: insert
     * @方法描述: 插入
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insert(AsplTotlIncome record) {
        return asplTotlIncomeMapper.insert(record);
    }

    /**
     * @方法名称: insertSelective
     * @方法描述: 插入 - 只插入非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int insertSelective(AsplTotlIncome record) {
        return asplTotlIncomeMapper.insertSelective(record);
    }

    /**
     * @方法名称: update
     * @方法描述: 根据主键更新 
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int update(AsplTotlIncome record) {
        return asplTotlIncomeMapper.updateByPrimaryKey(record);
    }

    /**
     * @方法名称: updateSelective
     * @方法描述: 根据主键更新 - 只更新非空字段
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int updateSelective(AsplTotlIncome record) {
        return asplTotlIncomeMapper.updateByPrimaryKeySelective(record);
    }

    /**
     * @方法名称: deleteByPrimaryKey
     * @方法描述: 根据主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByPrimaryKey(String pkId) {
        return asplTotlIncomeMapper.deleteByPrimaryKey(pkId);
    }

    /**
     * @方法名称: deleteByIds
     * @方法描述: 根据多个主键删除
     * @参数与返回说明: 
     * @算法描述: 无
     */

    public int deleteByIds(String ids) {
        return asplTotlIncomeMapper.deleteByIds(ids);
    }

    /**
     * @方法名称: toSignlist
     * @方法描述: 池收益对比分析表
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<AsplTotlIncomeDto> toSignlist(QueryModel model) {
        return asplTotlIncomeMapper.toSignlist(model);
    }

    /**
     * @方法名称: queryAsplTotlIncomeDataByParams
     * @方法描述: 通过入参查询
     * @参数与返回说明:
     * @算法描述: 无
     */
    @Transactional
    public List<AsplTotlIncome> queryAsplTotlIncomeDataByParams(Map map) {
        return asplTotlIncomeMapper.queryAsplTotlIncomeDataByParams(map);
    }
}
