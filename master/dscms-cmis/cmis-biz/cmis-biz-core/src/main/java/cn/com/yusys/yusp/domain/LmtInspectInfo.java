/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: LmtInspectInfo
 * @类描述: lmt_inspect_info数据实体类
 * @功能描述: 
 * @创建人: zrc
 * @创建时间: 2021-05-13 17:13:43
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "lmt_inspect_info")
public class LmtInspectInfo extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 勘验流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "INSPECT_SERNO")
	private String inspectSerno;
	
	/** 调查流水号 **/
	@Column(name = "SURVEY_SERNO", unique = false, nullable = true, length = 40)
	private String surveySerno;
	
	/** 云评估编号 **/
	@Column(name = "CLOUD_EVAL_NO", unique = false, nullable = true, length = 40)
	private String cloudEvalNo;
	
	/** 勘验人 **/
	@Column(name = "INSPECTOR", unique = false, nullable = true, length = 80)
	private String inspector;
	
	/** 勘验人证件类型 **/
	@Column(name = "INSPECTOR_CERT_TYPE", unique = false, nullable = true, length = 5)
	private String inspectorCertType;
	
	/** 勘验人证件号码 **/
	@Column(name = "INSPECTOR_CERT_CODE", unique = false, nullable = true, length = 40)
	private String inspectorCertCode;
	
	/** 抵押物所有人 **/
	@Column(name = "PAWN_OWNER", unique = false, nullable = true, length = 80)
	private String pawnOwner;
	
	/** 楼盘名称 **/
	@Column(name = "BUILDING_NAME", unique = false, nullable = true, length = 200)
	private String buildingName;
	
	/** 楼栋 **/
	@Column(name = "BUILDING", unique = false, nullable = true, length = 100)
	private String building;
	
	/** 面积 **/
	@Column(name = "SQU", unique = false, nullable = true, length = 16)
	private java.math.BigDecimal squ;
	
	/** 地址 **/
	@Column(name = "ADDR", unique = false, nullable = true, length = 500)
	private String addr;
	
	/** 视频流水号 **/
	@Column(name = "VIDEO_SERNO", unique = false, nullable = true, length = 40)
	private String videoSerno;
	
	/** 勘验状态 **/
	@Column(name = "INSPECT_STATUS", unique = false, nullable = true, length = 5)
	private String inspectStatus;
	
	/** 操作类型 **/
	@Column(name = "OPR_TYPE", unique = false, nullable = true, length = 5)
	private String oprType;
	
	/** 主管机构 **/
	@Column(name = "MANAGER_BR_ID", unique = false, nullable = true, length = 20)
	private String managerBrId;
	
	/** 主管客户经理 **/
	@Column(name = "MANAGER_ID", unique = false, nullable = true, length = 20)
	private String managerId;
	
	/** 登记人 **/
	@Column(name = "INPUT_ID", unique = false, nullable = true, length = 20)
	private String inputId;
	
	/** 登记机构 **/
	@Column(name = "INPUT_BR_ID", unique = false, nullable = true, length = 20)
	private String inputBrId;
	
	/** 登记日期 **/
	@Column(name = "INPUT_DATE", unique = false, nullable = true, length = 20)
	private String inputDate;
	
	/** 最后修改人 **/
	@Column(name = "UPD_ID", unique = false, nullable = true, length = 20)
	private String updId;
	
	/** 最后修改机构 **/
	@Column(name = "UPD_BR_ID", unique = false, nullable = true, length = 20)
	private String updBrId;
	
	/** 最后修改日期 **/
	@Column(name = "UPD_DATE", unique = false, nullable = true, length = 20)
	private String updDate;
	
	/** 创建时间 **/
	@Column(name = "CREATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date createTime;
	
	/** 修改时间 **/
	@Column(name = "UPDATE_TIME", unique = false, nullable = true, length = 19)
	private java.util.Date updateTime;
	
	
	/**
	 * @param inspectSerno
	 */
	public void setInspectSerno(String inspectSerno) {
		this.inspectSerno = inspectSerno;
	}
	
    /**
     * @return inspectSerno
     */
	public String getInspectSerno() {
		return this.inspectSerno;
	}
	
	/**
	 * @param surveySerno
	 */
	public void setSurveySerno(String surveySerno) {
		this.surveySerno = surveySerno;
	}
	
    /**
     * @return surveySerno
     */
	public String getSurveySerno() {
		return this.surveySerno;
	}
	
	/**
	 * @param cloudEvalNo
	 */
	public void setCloudEvalNo(String cloudEvalNo) {
		this.cloudEvalNo = cloudEvalNo;
	}
	
    /**
     * @return cloudEvalNo
     */
	public String getCloudEvalNo() {
		return this.cloudEvalNo;
	}
	
	/**
	 * @param inspector
	 */
	public void setInspector(String inspector) {
		this.inspector = inspector;
	}
	
    /**
     * @return inspector
     */
	public String getInspector() {
		return this.inspector;
	}
	
	/**
	 * @param inspectorCertType
	 */
	public void setInspectorCertType(String inspectorCertType) {
		this.inspectorCertType = inspectorCertType;
	}
	
    /**
     * @return inspectorCertType
     */
	public String getInspectorCertType() {
		return this.inspectorCertType;
	}
	
	/**
	 * @param inspectorCertCode
	 */
	public void setInspectorCertCode(String inspectorCertCode) {
		this.inspectorCertCode = inspectorCertCode;
	}
	
    /**
     * @return inspectorCertCode
     */
	public String getInspectorCertCode() {
		return this.inspectorCertCode;
	}
	
	/**
	 * @param pawnOwner
	 */
	public void setPawnOwner(String pawnOwner) {
		this.pawnOwner = pawnOwner;
	}
	
    /**
     * @return pawnOwner
     */
	public String getPawnOwner() {
		return this.pawnOwner;
	}
	
	/**
	 * @param buildingName
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	
    /**
     * @return buildingName
     */
	public String getBuildingName() {
		return this.buildingName;
	}
	
	/**
	 * @param building
	 */
	public void setBuilding(String building) {
		this.building = building;
	}
	
    /**
     * @return building
     */
	public String getBuilding() {
		return this.building;
	}
	
	/**
	 * @param squ
	 */
	public void setSqu(java.math.BigDecimal squ) {
		this.squ = squ;
	}
	
    /**
     * @return squ
     */
	public java.math.BigDecimal getSqu() {
		return this.squ;
	}
	
	/**
	 * @param addr
	 */
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
    /**
     * @return addr
     */
	public String getAddr() {
		return this.addr;
	}
	
	/**
	 * @param videoSerno
	 */
	public void setVideoSerno(String videoSerno) {
		this.videoSerno = videoSerno;
	}
	
    /**
     * @return videoSerno
     */
	public String getVideoSerno() {
		return this.videoSerno;
	}
	
	/**
	 * @param inspectStatus
	 */
	public void setInspectStatus(String inspectStatus) {
		this.inspectStatus = inspectStatus;
	}
	
    /**
     * @return inspectStatus
     */
	public String getInspectStatus() {
		return this.inspectStatus;
	}
	
	/**
	 * @param oprType
	 */
	public void setOprType(String oprType) {
		this.oprType = oprType;
	}
	
    /**
     * @return oprType
     */
	public String getOprType() {
		return this.oprType;
	}
	
	/**
	 * @param managerBrId
	 */
	public void setManagerBrId(String managerBrId) {
		this.managerBrId = managerBrId;
	}
	
    /**
     * @return managerBrId
     */
	public String getManagerBrId() {
		return this.managerBrId;
	}
	
	/**
	 * @param managerId
	 */
	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}
	
    /**
     * @return managerId
     */
	public String getManagerId() {
		return this.managerId;
	}
	
	/**
	 * @param inputId
	 */
	public void setInputId(String inputId) {
		this.inputId = inputId;
	}
	
    /**
     * @return inputId
     */
	public String getInputId() {
		return this.inputId;
	}
	
	/**
	 * @param inputBrId
	 */
	public void setInputBrId(String inputBrId) {
		this.inputBrId = inputBrId;
	}
	
    /**
     * @return inputBrId
     */
	public String getInputBrId() {
		return this.inputBrId;
	}
	
	/**
	 * @param inputDate
	 */
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
    /**
     * @return inputDate
     */
	public String getInputDate() {
		return this.inputDate;
	}
	
	/**
	 * @param updId
	 */
	public void setUpdId(String updId) {
		this.updId = updId;
	}
	
    /**
     * @return updId
     */
	public String getUpdId() {
		return this.updId;
	}
	
	/**
	 * @param updBrId
	 */
	public void setUpdBrId(String updBrId) {
		this.updBrId = updBrId;
	}
	
    /**
     * @return updBrId
     */
	public String getUpdBrId() {
		return this.updBrId;
	}
	
	/**
	 * @param updDate
	 */
	public void setUpdDate(String updDate) {
		this.updDate = updDate;
	}
	
    /**
     * @return updDate
     */
	public String getUpdDate() {
		return this.updDate;
	}
	
	/**
	 * @param createTime
	 */
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	
    /**
     * @return createTime
     */
	public java.util.Date getCreateTime() {
		return this.createTime;
	}
	
	/**
	 * @param updateTime
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	
    /**
     * @return updateTime
     */
	public java.util.Date getUpdateTime() {
		return this.updateTime;
	}


}