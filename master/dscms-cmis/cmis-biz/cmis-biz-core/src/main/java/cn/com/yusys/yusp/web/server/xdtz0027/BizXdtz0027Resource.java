package cn.com.yusys.yusp.web.server.xdtz0027;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.xdtz0027.req.Xdtz0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0027.resp.Xdtz0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.server.xdtz0027.Xdtz0027Service;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.util.StringUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0027:根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款")
@RestController
@RequestMapping("/api/biztz4bsp")
public class BizXdtz0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(BizXdtz0027Resource.class);

    @Autowired
    private Xdtz0027Service xdtz0027Service;

    /**
     * 交易码：xdtz0027
     * 交易描述：根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
     *
     * @param xdtz0027DataReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款")
    @PostMapping("/xdtz0027")
    protected @ResponseBody
    ResultDto<Xdtz0027DataRespDto> xdtz0027(@Validated @RequestBody Xdtz0027DataReqDto xdtz0027DataReqDto) throws Exception {
        logger.info(TradeLogConstants.RESOURCE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027DataReqDto));
        Xdtz0027DataRespDto xdtz0027DataRespDto = new Xdtz0027DataRespDto();// 响应Dto:根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
        ResultDto<Xdtz0027DataRespDto> xdtz0027DataResultDto = new ResultDto<>();
        String cusId = xdtz0027DataReqDto.getCusId();
        try {
            if (StringUtil.isEmpty(cusId)) {
                xdtz0027DataResultDto.setCode(EpbEnum.EPB099999.key);
                xdtz0027DataResultDto.setMessage("查询参数客户号为空");
                return xdtz0027DataResultDto;
            }
            // 从xdtz0027DataReqDto获取业务值进行业务逻辑处理
            // 调用xdtz0027Service层开始
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027DataReqDto));
            xdtz0027DataRespDto = xdtz0027Service.xdtz0027(xdtz0027DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027DataRespDto));
            // 封装xdtz0027DataResultDto中正确的返回码和返回信息
            xdtz0027DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xdtz0027DataResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
        } catch (Exception e) {
            logger.info(TradeLogConstants.RESOURCE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, e.getMessage());
            // 封装xdtz0027DataResultDto中异常返回码和返回信息
            xdtz0027DataResultDto.setCode(EpbEnum.EPB099999.key);
            xdtz0027DataResultDto.setMessage(EpbEnum.EPB099999.value);
        }
        // 封装xdtz0027DataRespDto到xdtz0027DataResultDto中
        xdtz0027DataResultDto.setData(xdtz0027DataRespDto);
        logger.info(TradeLogConstants.RESOURCE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027DataResultDto));
        return xdtz0027DataResultDto;
    }
}
