/*
 * 代码生成器自动生成的
 * Since 2008 - 2021
 *
 */
package cn.com.yusys.yusp.domain;
import cn.com.yusys.yusp.commons.mapper.key.annotation.Generated;
import cn.com.yusys.yusp.commons.mapper.key.KeyConstants;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * @项目名称: cmis-biz-core模块
 * @类名称: RptPersonalBusiness
 * @类描述: rpt_personal_business数据实体类
 * @功能描述: 
 * @创建人: ZRC
 * @创建时间: 2021-06-11 16:13:54
 * @修改备注: 
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @version 1.0.0
 * @Copyright (c) 宇信科技-版权所有
 */
@Table(name = "rpt_personal_business")
public class RptPersonalBusiness extends BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
	
	/** 申请流水号 **/
	@Id
	@Generated(KeyConstants.UUID)
	@Column(name = "SERNO")
	private String serno;
	
	/** 借款人姓名 **/
	@Column(name = "BORR_CUS_NAME", unique = false, nullable = true, length = 40)
	private String borrCusName;
	
	/** 联系电话 **/
	@Column(name = "PHONE", unique = false, nullable = true, length = 20)
	private String phone;
	
	/** 信用评级 **/
	@Column(name = "CREDIT_RATE", unique = false, nullable = true, length = 5)
	private String creditRate;
	
	/** 出生年月 **/
	@Column(name = "DT_OF_BIRTH", unique = false, nullable = true, length = 20)
	private String dtOfBirth;
	
	/** 婚姻状况 **/
	@Column(name = "MAR_STATUS", unique = false, nullable = true, length = 5)
	private String marStatus;
	
	/** 户籍所在地 **/
	@Column(name = "PERMANENT_ADDR", unique = false, nullable = true, length = 40)
	private String permanentAddr;
	
	/** 实际居住地 **/
	@Column(name = "REAL_LIVING_ADDR", unique = false, nullable = true, length = 40)
	private String realLivingAddr;
	
	/** 借款人从业经历 **/
	@Column(name = "BORR_FJOB_CASE", unique = false, nullable = true, length = 65535)
	private String borrFjobCase;
	
	/** 配偶姓名、工作单位 **/
	@Column(name = "SPOUSE_NAME_WORK_UNIT", unique = false, nullable = true, length = 65535)
	private String spouseNameWorkUnit;
	
	/** 子女姓名、工作单位 **/
	@Column(name = "CHILD_NAME_WORK_UNIT", unique = false, nullable = true, length = 65535)
	private String childNameWorkUnit;
	
	/** 家庭资产情况 **/
	@Column(name = "HOME_ASSET_EXPL", unique = false, nullable = true, length = 65535)
	private String homeAssetExpl;
	
	/** 个人融资情况 **/
	@Column(name = "SELF_FINA_CASE", unique = false, nullable = true, length = 65535)
	private String selfFinaCase;
	
	/** 个人对外担保情况 **/
	@Column(name = "SELF_OUTGUAR_SITU_DESC", unique = false, nullable = true, length = 65535)
	private String selfOutguarSituDesc;
	
	/** 个人信用情况 **/
	@Column(name = "SELF_CDT_EXPL", unique = false, nullable = true, length = 65535)
	private String selfCdtExpl;
	
	/** 和我行合作情况 **/
	@Column(name = "SELF_COOP_CASE", unique = false, nullable = true, length = 65535)
	private String selfCoopCase;
	
	
	/**
	 * @param serno
	 */
	public void setSerno(String serno) {
		this.serno = serno;
	}
	
    /**
     * @return serno
     */
	public String getSerno() {
		return this.serno;
	}
	
	/**
	 * @param borrCusName
	 */
	public void setBorrCusName(String borrCusName) {
		this.borrCusName = borrCusName;
	}
	
    /**
     * @return borrCusName
     */
	public String getBorrCusName() {
		return this.borrCusName;
	}
	
	/**
	 * @param phone
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
    /**
     * @return phone
     */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * @param creditRate
	 */
	public void setCreditRate(String creditRate) {
		this.creditRate = creditRate;
	}
	
    /**
     * @return creditRate
     */
	public String getCreditRate() {
		return this.creditRate;
	}
	
	/**
	 * @param dtOfBirth
	 */
	public void setDtOfBirth(String dtOfBirth) {
		this.dtOfBirth = dtOfBirth;
	}
	
    /**
     * @return dtOfBirth
     */
	public String getDtOfBirth() {
		return this.dtOfBirth;
	}
	
	/**
	 * @param marStatus
	 */
	public void setMarStatus(String marStatus) {
		this.marStatus = marStatus;
	}
	
    /**
     * @return marStatus
     */
	public String getMarStatus() {
		return this.marStatus;
	}
	
	/**
	 * @param permanentAddr
	 */
	public void setPermanentAddr(String permanentAddr) {
		this.permanentAddr = permanentAddr;
	}
	
    /**
     * @return permanentAddr
     */
	public String getPermanentAddr() {
		return this.permanentAddr;
	}
	
	/**
	 * @param realLivingAddr
	 */
	public void setRealLivingAddr(String realLivingAddr) {
		this.realLivingAddr = realLivingAddr;
	}
	
    /**
     * @return realLivingAddr
     */
	public String getRealLivingAddr() {
		return this.realLivingAddr;
	}
	
	/**
	 * @param borrFjobCase
	 */
	public void setBorrFjobCase(String borrFjobCase) {
		this.borrFjobCase = borrFjobCase;
	}
	
    /**
     * @return borrFjobCase
     */
	public String getBorrFjobCase() {
		return this.borrFjobCase;
	}
	
	/**
	 * @param spouseNameWorkUnit
	 */
	public void setSpouseNameWorkUnit(String spouseNameWorkUnit) {
		this.spouseNameWorkUnit = spouseNameWorkUnit;
	}
	
    /**
     * @return spouseNameWorkUnit
     */
	public String getSpouseNameWorkUnit() {
		return this.spouseNameWorkUnit;
	}
	
	/**
	 * @param childNameWorkUnit
	 */
	public void setChildNameWorkUnit(String childNameWorkUnit) {
		this.childNameWorkUnit = childNameWorkUnit;
	}
	
    /**
     * @return childNameWorkUnit
     */
	public String getChildNameWorkUnit() {
		return this.childNameWorkUnit;
	}
	
	/**
	 * @param homeAssetExpl
	 */
	public void setHomeAssetExpl(String homeAssetExpl) {
		this.homeAssetExpl = homeAssetExpl;
	}
	
    /**
     * @return homeAssetExpl
     */
	public String getHomeAssetExpl() {
		return this.homeAssetExpl;
	}
	
	/**
	 * @param selfFinaCase
	 */
	public void setSelfFinaCase(String selfFinaCase) {
		this.selfFinaCase = selfFinaCase;
	}
	
    /**
     * @return selfFinaCase
     */
	public String getSelfFinaCase() {
		return this.selfFinaCase;
	}
	
	/**
	 * @param selfOutguarSituDesc
	 */
	public void setSelfOutguarSituDesc(String selfOutguarSituDesc) {
		this.selfOutguarSituDesc = selfOutguarSituDesc;
	}
	
    /**
     * @return selfOutguarSituDesc
     */
	public String getSelfOutguarSituDesc() {
		return this.selfOutguarSituDesc;
	}
	
	/**
	 * @param selfCdtExpl
	 */
	public void setSelfCdtExpl(String selfCdtExpl) {
		this.selfCdtExpl = selfCdtExpl;
	}
	
    /**
     * @return selfCdtExpl
     */
	public String getSelfCdtExpl() {
		return this.selfCdtExpl;
	}
	
	/**
	 * @param selfCoopCase
	 */
	public void setSelfCoopCase(String selfCoopCase) {
		this.selfCoopCase = selfCoopCase;
	}
	
    /**
     * @return selfCoopCase
     */
	public String getSelfCoopCase() {
		return this.selfCoopCase;
	}


}